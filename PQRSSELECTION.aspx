﻿<%@ Page Title="" Language="C#" MasterPageFile="~/PIMMasterPage.master" AutoEventWireup="true" CodeFile="PQRSSELECTION.aspx.cs" Inherits="abo_PQRSSELECTION" %>

<asp:Content ID="Content1" ContentPlaceHolderID="head" Runat="Server">
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder1" Runat="Server">
<!-- module detaile -->
            <div class="main_content" style="padding-right: 100px;">
            <div class="heading_box" >
               
              <h2 >PQRS Incentive Program</h2>

<p>Several of the ABO PIP Activities align with the CMS PQRS Incentive Program.  To learn more about PQRS, click here.  
</p>
<p>If you would like to use your data from the ABO PIP for the CMS PQRS program, you have 3 options:</p>
<ol><li><em>Cataract Surgery</em> – You can select this activity and qualify for your incentive by entering 30 charts</li>
<li><em>POAG</em><br>
<ul><li>POAG – if you choose this activity, you will need to complete one additional measure</li>
<li>POAGS
Note if you choose this option, you will need to choose one additional measure
 (please choose one of the Retina & Vitreous activities below or you will be prompted to enter an additional measure selection 
   in the PQRS system)</li>
   </ul>
   </li>
   

<li><em>Retina & Vitreous</em>
<ul><li>Exudative AMD – 80 percent of all diagnosed patients seen throughout the year</li>
<li>Diabetic Retinopathy– 80 percent of all diagnosed patients seen throughout the year</li>

    <asp:Label ID="Label1" runat="server" Text="Would you like to participate in PQRS?"></asp:Label>
    <asp:RadioButtonList ID="RadioButtonList1" RepeatDirection="Horizontal" runat="server">
    <asp:ListItem Value="True" Text="Yes"></asp:ListItem>
     <asp:ListItem Value="False" Text="No"></asp:ListItem>
    </asp:RadioButtonList>
    <br />
    <asp:RequiredFieldValidator ID="RequiredFieldValidator1" runat="server" ControlToValidate="RadioButtonList1" ValidationGroup="some"  ErrorMessage="Field is Required"></asp:RequiredFieldValidator>
    <br /><br />
    <asp:Button ID="Button1" runat="server" Text="Submit" ValidationGroup="some" onclick="Button1_Click" />
    <asp:HyperLink ID="HyperLink1" NavigateUrl="~/abo/ModuleSelection.aspx"  runat="server">Activity Selection</asp:HyperLink>
</asp:Content>

