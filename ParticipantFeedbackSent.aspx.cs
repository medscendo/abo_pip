﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

public partial class ParticipantFeedbackSent : BasePage
{
    protected void Page_Load(object sender, EventArgs e)
    {
        if (!IsPostBack)
        {
            InitializePage();
        }
    }

    protected void InitializePage()
    {
        ((PIMMasterPage)Page.Master).SetTitle("Password Recovery");
        Session[Constants.SESSION_PAGE_ROLETYPE_LEVEL] = Constants.ROLETYPEID_GUEST;
    }

    protected void ButtonOK_Click(object sender, EventArgs e)
    {
        // Redirect to the default page based on role.
        Response.Redirect(GetDefaultRedirectByRole(ctx.RoleTypeID));
    }
}
