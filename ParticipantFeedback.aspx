﻿<%@ Page Title="" Language="C#" MasterPageFile="~/PIMMasterPage.master" AutoEventWireup="true" CodeFile="ParticipantFeedback.aspx.cs" Inherits="copd_ParticipantFeedback" EnableTheming="true" %>

<asp:Content ID="Content1" ContentPlaceHolderID="head" Runat="Server">
</asp:Content>
<asp:Content ID="Content3" ContentPlaceHolderID="ContentPlaceHolder1" Runat="Server">
<table class="main_table table_border center_area" cellpadding="0" cellspacing="0" width="800px">
<tr>
<td>
    We welcome your input about this activity. Please provide us with your feedback below.<br />
    <br />
    <b>Comments:</b><br />
    <asp:TextBox ID="TextBoxFeedbackText" runat="server" Columns="100" Rows="8" TextMode="MultiLine"></asp:TextBox>
    <br />
    <asp:RequiredFieldValidator ID="RequiredFieldValidatorText" runat="server" ControlToValidate="TextBoxFeedbackText"
        ErrorMessage="Please enter a messge or comments." Display="Dynamic"></asp:RequiredFieldValidator><br />

    <b>Would you like us to contact you regarding your feedback?</b>
    <asp:RequiredFieldValidator ID="RequiredFieldValidatorContactRequested" runat="server" ControlToValidate="RadioButtonListContactRequested"
        ErrorMessage="Please select an option." Display="Dynamic"></asp:RequiredFieldValidator>
    <br />
    <asp:RadioButtonList ID="RadioButtonListContactRequested" runat="server">
        <asp:ListItem Text="Yes" Value="1" /><asp:ListItem Text="No" Value="0" />
    </asp:RadioButtonList>
    
    <b>Your email address:</b>
    <br />
    <asp:TextBox ID="TextBoxEmailAddress" runat="server" Width="200"></asp:TextBox>
    <asp:RegularExpressionValidator ID="RegExpEmailValidator" runat="server" Display="Dynamic"
        ToolTip="Invalid e-mail address." ErrorMessage="Invalid e-mail address"
        ValidationExpression="^[\w-]+@[\w-]+\.(com|net|org|edu|mil)$" ControlToValidate="TextBoxEmailAddress"></asp:RegularExpressionValidator>
    <br />

    <b>Your phone number:</b><br />
    <asp:TextBox ID="TextBoxPhoneNumber" runat="server" Width="200"></asp:TextBox><br />
    <b>Best way to reach you:</b><br />
    <asp:RadioButtonList ID="RadioButtonListContactOption" runat="server">
        <asp:ListItem Text="E-Mail" Value="1" /><asp:ListItem Text="Phone" Value="2" />
    </asp:RadioButtonList>
    <br />
    Contact NetHealth at 610.590.2229 or send an e-mail to <a href="mailto:asn@nethealthinc.com">asn@nethealthinc.com</a> for support.
    <br />
    <br />
    Assistance is available Monday to Friday from 8 a.m. - 5 p.m (Eastern Time).
    <br />
    <br />
    <center>
    <asp:Button ID="ButtonSubmit" runat="server" Text="Submit" onclick="ButtonSubmit_Click" />
    </center>
    <br />
</td>
</tr>
</table>
</asp:Content>

