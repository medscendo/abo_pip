﻿<%@ Page Language="C#" AutoEventWireup="true" CodeFile="ModuleDefinition.aspx.cs" Inherits="abo_ModuleDefinition" %>

<head id="Head1" runat="server">
<link href="../common/css/normalize.css" rel="stylesheet" />
<link href="../common/css/style.css" rel="stylesheet" />

<style type="text/css">
    body {
        background: none;
    }
</style>


</head>
<body>
<form id="Form1" runat="server">
        <!-- module overview -->
<%--        <div class="module_overview">
          <div class="heading_box">
            <div class="module_selection">
              <ul>
                <li><a href="ModuleSelection.aspx"><img src="../common/images/return.jpg" alt="" /></a></li>
                <!--<li><a href="ModuleDetails.aspx"><img src="../common/images/confirm.jpg" alt="" /></a></li>-->
              </ul>
            </div>
          </div>--%>

    <h3><asp:Literal runat="server" ID="LiteralModuleName2"></asp:Literal> Activity</h3>
    <%--<p><asp:Literal runat="server" ID="LiteralModuleDescription"></asp:Literal></p>--%>
                    
    <h4><asp:Literal runat="server" ID="LiteralModuleName3"></asp:Literal> Patient Definition</h4>
    <p><asp:Literal runat="server" ID="LiteralPatientDefinition"></asp:Literal></p>
    
    <h4>Key Outcome Measures &nbsp;<img id="OM1" src="../common/images/tip.gif" alt="Tip" title="Outcome Measures refer to the desired result of treatment and can be used to consider quality of care and desired health outcomes." /></h4>
        
    <asp:ListView runat="server" ID="ListViewOutcomeMeasures">
        <LayoutTemplate>
            <ul>
                <asp:PlaceHolder runat="server" ID="itemPlaceholder"></asp:PlaceHolder>
            </ul>
        </LayoutTemplate>                           
        <ItemTemplate>
            <li>
                <%# Eval("MeasureLongDescription")%>                         
            </li>
        </ItemTemplate>                                
    </asp:ListView>
                   
    <h4 id="ResNameID" runat="server"><asp:Literal runat="server" ID="LiteralModuleName4" /> Resources</h4>
    <asp:ListView runat="server" ID="ListViewResources">
        <LayoutTemplate>
            <ul>
                <asp:PlaceHolder runat="server" ID="itemPlaceholder"></asp:PlaceHolder>
            </ul>
        </LayoutTemplate>    
        <ItemTemplate>
            <p>
                <strong><asp:Literal ID="LabelCheckBoxResource" runat="server" Text='<%# Eval("ResourceName") %> '></asp:Literal></strong><br />
                <asp:Label ID="ResourceDescription" runat="server" Text='<%# Eval("ResourceDescription") %>' /><br />
                <asp:HyperLink runat="server" ID="HyperLinkURL" Text='<%# Eval("URL") %>' NavigateUrl='<%# Eval("URL") %>' Target="_blank"></asp:HyperLink>
            </p>
        </ItemTemplate>
    </asp:ListView>                           
    
    <asp:Panel runat="server" ID="AmblyopiaResources" visible="false">
            <p><strong>American Academy of Ophthalmology
                        <br />Preferred Practice Patterns<br />
                        <br />Guidelines for Amblyopia Process Measures
                        <br />
                        <br />Recognized Optotypes</strong>
                        <br />
                            The AAO PPP for amblyopia states, &quot;When possible, monocular distance visual 
                            acuity should be determined utilizing a recognized optotype, such as tumbling-E, 
                            Lea figures, or Snellen letters.&quot;
                            Allen figures are not recommended.
                            <br>
                            <br />
                
                            <b style="mso-bidi-font-weight:normal">Sensory Testing</b>
                            <br />
                            The AAO PPP for amblyopia states &quot;In general, the examination may include. 
                            Binocularity/stereoacuity testing.<span style="mso-spacerun:yes">&nbsp; </span>
                            Testing for binocular fusion (e.g., Worth 4-dot test) or the presence of 
                            stereopsis (EG Random-Dot E test or Stereo Fly test) may be useful in detecting 
                            ocular misalignment or amblyopia.&quot;
                            <br />
                            <br /><b>Alignment</b>
                            <br />

                            The AAO PPP for amblyopia states, &quot;In general, the examination may 
                            include…ocular alignment and motility.<span style="mso-spacerun:yes">&nbsp;
                            </span>When strabismus is suspect or revealed, a strabismus evaluation is 
                            warranted.&quot;
                            <br />
                            <br />
                            <b style="mso-bidi-font-weight:normal">Cycolpegic Refraction</b>
                            <br />
                            The AAO PPP for amblyopia states &quot;Determination of refractive errors is 
                            important in the diagnosis and treatment of amblyopia or strabismus.<span 
                                style="mso-spacerun:yes">&nbsp; </span>Patients should receive an accurate 
                            cycloplegic refraction either by retinoscopy or by subjective refraction.<span 
                                style="mso-spacerun:yes">&nbsp; </span>Cycloplegia is necessary for accurate 
                            refraction in children.&quot;
                            <br />
                            <br />
                            <b style="mso-bidi-font-weight:normal">Compliance</b>
                            <br />
                            The AAO PPP for amblyopia states, &quot;Outcomes is dependent on patient compliance.<span 
                                style="mso-spacerun:yes">&nbsp; </span>Parents/caregivers of pediatric 
                            patients who understand the diagnosis and rationale for treatment are more 
                            likely to adhere to treatment recommendations.<span style="mso-spacerun:yes">&nbsp;
                            </span>Follow-up evaluation includes interval history and tolerance to therapy.&quot;<span 
                                style="mso-spacerun:yes">&nbsp; </span>Inadequate compliance is recognized 
                            as a major cause of poor outcomes for treatment of amblyopia.
                            <br />
                            <br />
                            <b style="mso-bidi-font-weight:normal">PEDIG Resource Guidelines for Outcome 
                            Measures</b>
                            <br />
                            <br />
                            <b style="mso-bidi-font-weight:normal">Lines of Improvement</b>
                            <br />
                            The PEDIG Amblyopia Treatment Studies provide the following data for children 
                            aged 3-7 years.<span style="mso-spacerun:yes">&nbsp; </span>In ATS1, after 6 
                            months of treatment, 65% of subjects improved more than two lines, increasing to 
                            76% at 2 years.<span style="mso-spacerun:yes">&nbsp; </span>ATS2A showed that 
                            48% of moderate amblyopes improved greater than two lines after 4 months of 
                            treatment.<span style="mso-spacerun:yes">&nbsp; </span>In the ATS2B study, 84% 
                            of severe (20-100-20/400) amblyopes improved more than two lines after 4 months 
                            of treatment.<span style="mso-spacerun:yes">&nbsp;&nbsp; </span>(&lt; 50%)
                            <br />
                            <br />
                            <b style="mso-bidi-font-weight:normal">Mean Improvement</b>
                            <br />
                            Among moderate amblyopes aged 3-7 years, ATS1 demonstrated a mean improvement of 
                            3 lines after 6 months and 3.7 lines after 2 years of treatment.<span 
                                style="mso-spacerun:yes">&nbsp;&nbsp; </span>ATS2A showed a mean improvement 
                            of 2.4 lines of vision after 4 months of treatment in moderate amblyopes aged 
                            3-7, and ATS2B reported a mean improvement of 4.7 lines in severe amblyopes aged 
                            3-7 after 4 months of treatment.<span style="mso-spacerun:yes">&nbsp; </span>
                            ATS6 showed an improvement of 2.5 lines of vision in children treated for 4 
                            months.<span style="mso-spacerun:yes">&nbsp; </span>(&lt; 2 lines) Among children 
                            aged 7-12, ATS3 showed improvement of approximately 1.5-2.5 lines after 6 months 
                            of treatment, and among children aged 13-17, of approximately 1-2 lines after 6 
                            months of treatment.<span style="mso-spacerun:yes">&nbsp;&nbsp; </span>(&lt; 1 
                            line)
                            <br />
                            <br />
                            <b style="mso-bidi-font-weight:normal">Acuity</b>
                            <br />
                            ATS1 demonstrated an increase in the proportion of children with at least 2 
                            lines of visual improvement from 65% at 6 months to 76% at 2 years, with an 
                            increase in the mean improvement from 3.01 to 3.7 lines during that time.<span 
                                style="mso-spacerun:yes">&nbsp; </span>In the 6-month and 2-year follow-up 
                            studies of the ATS1 population, 1.6-2.2% of children had acuity less than 20/40 
                            at 6 months, and 85-91% of subjects continued treatment between 6 months and 2 
                            years after initial enrollment.<span style="mso-spacerun:yes">&nbsp;&nbsp;
                            </span>Depending on the age and prior treatment history of the patient, 
                            continued treatment should be considered after 6-12 months in both strabismic 
                            and refractive amblyopes if acuity is less than 20/40.<span 
                                style="mso-spacerun:yes">&nbsp; </span>(&lt; 75%)
                                <br />
                                <br />
                            <b style="mso-bidi-font-weight:normal">Complications</b>
                            <br />
                            a. Atropine reaction:<span style="mso-spacerun:yes">&nbsp; </span>In the ATS1 
                            and ATS3 studies, 0.5-1.0% of subjects suffered facial flushing, tachycardia, or 
                            vomiting after atropine treatment.<span style="mso-spacerun:yes">&nbsp; </span>
                            Atropine use was discontinued in 4 % of patients in ATS3 due to visual symptoms 
                            for near work. (&gt; 5%)<br />
                            <br />
                            b. Reverse amblyopia:<span style="mso-spacerun:yes">&nbsp; </span>ATS1 reported 
                            reverse amblyopia requiring cessation of treatment in &lt; 1% of subjects.<span 
                                style="mso-spacerun:yes">&nbsp; </span>(&gt; 5%)
                                <br />
                                <br />
                            c. New onset of strabismus/increased deviation:<span style="mso-spacerun:yes">&nbsp;
                            </span>ATS1 reported this complication in approximately 3% of subjects, with 
                            other ATS studies showing increase in the distance angle of strabismus in 9-12 % 
                            of subjects.<span style="mso-spacerun:yes">&nbsp; </span>(&gt; 20%)
                            <br />
                            <br />
                            d. Recurrence:<span style="mso-spacerun:yes">&nbsp; </span>ATS2C defined 
                            recurrence as &gt; or equal to a 2 line reduction in acuity after stopping 
                            treatment in 24% of cases, with no significant different difference between the 
                            patching and atropine groups. (&gt; 50%)
                            <br />
                            <br />
                            <b style="mso-bidi-font-weight:normal">Amblyopia Citations</b>
                            <br />
                            <a href="http://pedig.jaeb.org/Studies.aspx?RecID=16">Ophthalmol 2002; 
                            120:268-78</a>
                            <br />

                            To compare patching and atropine as treatments for moderate amblyopia in 
                            children less than 7 years of age over a two-year follow-up period.
                            <br />
                            <br />
                            <a href="http://agapelearning.eyehub.com/media/Article%20%202yr%20FU%20Atropine%20Patching%20Study.pdf">
                            Ophthalmol 2005; 123:149-157</a>
                            <br />
                            To compare patching and atropine sulfate as treatments for moderate amblyopia in 
                            children 18 months after completion of a 6-month randomized trial.
                            <br />
                            <br />
                       
                            <span class="style1" style="text-underline: single;">
                            <a href="http://med.brown.edu/surgery/ophthalmology/resident/Exams/documents/PPPAmblyopia.pdf" target="_blank">
                            Ophthalmology 2003; 110:2075-87</a></span>
                            <br />
                            To prevent or reverse vision impairment caused by amblyopia.
                            <br />
                            <br />
                            <a href="http://agapelearning.eyehub.com/media/Article%20%202yr%20FU%20Atropine%20Patching%20Study.pdf" target="_blank">
                            Ophthalmol 2003; 121:603-11</a>
                            <br />
                            To compare 2 hours versus 6 hours of daily patching as treatments for moderate 
                            amblyopia in children less than 7 years of age over a 4-month follow-up period
                            <br />
                            <br />
                     
                            <a href="http://www.iovs.org/content/early/2010/11/09/iovs.10-5550.full.pdf" target="_blank">
                            Ophthalmol 2008; 126:1039-44</a>
                            <br />
                            To compare macular thickness of the normal fellow eye to that of the amblyopic 
                            eye using optical coherence tomography (OCT) in children with unilateral high 
                            myopia. Relationships between macular thickness and magnitude of myopic 
                            anisometropia, axial length and visual acuity (VA) were investigated.
                            <br />
                            <br />
                            <a href="http://www.ncbi.nlm.nih.gov/pubmed/16751033" target="_blank">Ophthalmology 2006; 
                            904-912</a>
                            <br />
                            To compare 2 hours of daily patching (combined with 1 hour of concurrent near 
                            visual activities) with a control group of spectacle wear alone (if needed) for 
                            treatment of moderate to severe amblyopia in children 3 to 7 years old.
                            <br />
                            <br />
                         
                            <a href="http://www.visionhelp.com/vh_resources_04.html" target="_blank">Ophthalmol 2005; 123: 
                            437-447</a> <!--[if supportFields]><![endif]-->
                        <br />
                            To evaluate the effectiveness of treatment of amblyopia in children aged 7 to 17 
                            years.<br />
                            <br />
                            <a href="http://archopht.ama-assn.org/cgi/content/abstract/archophthalmol.2011.179v1" target="_blank">
                            Ophthalmol 2011; online First July 12:<span style="mso-spacerun:yes">&nbsp;
                            </span>Effect of age on response to amblyopia treatment in children, Holmes et 
                            al.</a> 
                        <br />
                            To determine whether age at initiation of treatment for amblyopia influences the 
                            response among children 3 to less than 13 years of age with unilateral amblyopia 
                            who have 20/40 to 20/400 amblyopic eye visual acuity.
                            </asp:Panel>

        
    <h4><asp:Literal runat="server" ID="LiteralModuleName1"></asp:Literal> Minimum Improvement Period</h4>
    <p><asp:Literal runat="server" ID="LiteralMinimumImprovementPeriod"></asp:Literal> months minimum improvement period after initial abstraction</p>
    
    <!--<asp:LinkButton ID="LinkButtonPeerData" runat="server" Text="View Peer Data"  target="_blank"/>
            <div class="bginput"><asp:LinkButton ID="LinkButtonChartAbstractionDetail"  runat="server" Text="View Chart Abstraction Detail" target="_blank"/></div>-->
        
        <!-- module overview ends -->
    </form>

<script src="//ajax.googleapis.com/ajax/libs/jquery/1.10.2/jquery.min.js"></script>
<script src="//ajax.googleapis.com/ajax/libs/jqueryui/1.10.3/jquery-ui.min.js"></script> 
<link href="../common/css/jquery-ui-1.10.3.custom.min.css" rel="stylesheet" />
<script>
    $(function () {
        $(document).tooltip();
    });
</script>
</body>