﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using NetHealthPIMModel;

public partial class abo_BusinessAssociateAgreement : BasePage
{
    protected void Page_Load(object sender, EventArgs e)
    {
        ((PIMMasterPage)Page.Master).SetHeader("Business Associate Addendum");
        ((PIMMasterPage)Page.Master).SetStatus(1);
    }
    protected void ButtonSubmit_Click(object sender, EventArgs e)
    {
        using (NetHealthPIMEntities pim = new NetHealthPIMEntities())
        {
            ParticipantModuleCycle cycle = pim.ParticipantModuleCycle.First(c => c.ParticipantModuleCycleID == ctx.ActiveModuleCycleID);
            cycle.BusinessAssociateAgreementCompletedDate = DateTime.Today;
            cycle.BusinessAssociateAgreementComplete = true;
            pim.SaveChanges();


            if (ctx.SoftwareVersion == 0)
            {
                Response.Redirect("FirstTutorial.aspx");
            }
            else
            {
                Response.Redirect("Frontmatter.aspx");
            }
        }
        
    }
}
