﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using NetHealthPIMModel;
using System.Web.UI.HtmlControls;

public partial class abo_SelectPriorModules : BasePage
{
    protected void Page_Load(object sender, EventArgs e)
    {
       

        if (!IsPostBack)
        {
            string breadCrumb = Constants.BC_CREATE_PLAN_LINK + "" + Constants.BC_PQRS_INCENTIVE_PROGRAM_LINK + "" +
                        Constants.BC_IMPORT_DATA;
            ((PIMMasterPage)Page.Master).SetTitle(breadCrumb);
            ((PIMMasterPage)Page.Master).SetStatus(1);
            ((PIMMasterPage)Page.Master).SetHeader("Import Data");
            using (NetHealthPIMEntities pim = new NetHealthPIMEntities())
            {
                int cycleID = ctx.ActiveModuleCycleID;
                ParticipantModuleCycle cycle = pim.ParticipantModuleCycle.First(c => c.ParticipantModuleCycleID == cycleID);
                Participant participant = pim.Participant.First(p => p.ParticipantID == ctx.ParticipantID);
                if (participant.SoftwareVersion > 2015)
                {
                    Response.Redirect("ModuleSelection.aspx");
                }
                ParticipantModule mod = (from c in pim.ParticipantModule where c.ParticipantID == participant.ParticipantID select c).FirstOrDefault();
                if ((cycle.CycleNumber == 1) && (mod.PQRS==null))
                {
                    Response.Redirect("~/" + module.ModuleCode.ToLower() + "/PQRSSelection.aspx");
                }

                if ((cycle.CycleNumber == 2) || (cycle.ImprovementPlanComplete == true))
                {
                    Response.Redirect("~/" + module.ModuleCode.ToLower() + "/ModuleDetails.aspx");
                }

                var priorModules = (from pms in pim.PriorModuleSelectionDataPerParticipant_V
                                         where pms.CandidateID == participant.CandidateID
                                         select pms).Count();
                if ((priorModules > 0))
                {
                    var pqrsselected = pim.ParticipantModule.First(p => p.ParticipantID == participant.ParticipantID);
                    bool pqrsused = Convert.ToBoolean(pqrsselected.PQRS);
                    System.Linq.IQueryable report = null;
                    report = from pms in pim.PriorModuleSelectionDataPerParticipant_V
                             where pms.CandidateID == participant.CandidateID
                             select pms;
                    RepeaterPriorModuleSelection.DataSource = report;
                    RepeaterPriorModuleSelection.DataBind();

                                             
                    int ParticipantModuleCycleID = ctx.ActiveModuleCycleID;
                    var participantModulesSelected = (from pms in pim.ParticipantModulesSelected_V
                                                      where pms.ParticipantModuleCycleID == ParticipantModuleCycleID
                                                      && pms.PreviousChartData > 0
                                                      select pms);
                    foreach (var pms in participantModulesSelected)
                    {
                        foreach (RepeaterItem item in RepeaterPriorModuleSelection.Items)
                        {
                            HiddenField HiddenFieldModuleID = (HiddenField)item.FindControl("HiddenFieldModuleID");
                            if (HiddenFieldModuleID.Value.ToString() == pms.ModuleID.ToString())
                            {
                                HtmlInputCheckBox chkModule = (HtmlInputCheckBox)item.FindControl("chkModule");
                                chkModule.Checked = true;
                                TextBox TextBoxPreviousChartData = (TextBox)item.FindControl("TextBoxPreviousChartData");
                                TextBoxPreviousChartData.Text = pms.PreviousChartData.ToString();
                                                            

                            }
                        }
                    }
                    PreviousData1.Visible = true;
                    PreviousData2.Visible = true;
                }
                else
                {
                    NoPreviousData.Visible = true;
                    //Response.Redirect("ModuleSelection.aspx");
                }
            }
        }
    }

    protected void LinkButtonSave_Click(object sender, CommandEventArgs e)
    {
        string PreviousModulesSelected = "";
        string PreviousNumberOfCharts = "";

        using (NetHealthPIMEntities pim = new NetHealthPIMEntities())
        {
            //NetHealthPIMEntities db = new NetHealthPIMEntities();

            if (HiddenFieldModuleSelectionChanged.Value == "0")
            {
                ParticipantModuleCycle pmc = pim.ParticipantModuleCycle.First(c => c.ParticipantModuleCycleID == ctx.ActiveModuleCycleID);
                if (pmc.SelectPreviousChartDataComplete == null)
                {
                    pmc.SelectPreviousChartDataComplete = true;
                    pmc.SelectPreviousChartDataCompletedDate = DateTime.Today;
                    pim.SaveChanges();
                }
                Response.Redirect("ModuleSelection.aspx");
            }

            CycleManager.DeletePreviousChartRegistration(ctx.ActiveModuleCycleID, 0);
            //Participant participant = null;

            Participant participant = pim.Participant.First(p => p.ParticipantID == ctx.ParticipantID);
            ParticipantModuleCycle cycle = pim.ParticipantModuleCycle.First(c => c.ParticipantModuleCycleID == ctx.ActiveModuleCycleID);
            int ModuleID = 0;
            string mid = (String)e.CommandArgument;
            foreach (RepeaterItem item in RepeaterPriorModuleSelection.Items)
            {
                HtmlInputCheckBox chkModule = (HtmlInputCheckBox)item.FindControl("chkModule");
                HiddenField HiddenFieldModuleID = (HiddenField)item.FindControl("HiddenFieldModuleID");
                HiddenField HiddenFieldNumberOfCharts = (HiddenField)item.FindControl("HiddenFieldNumberOfCharts");
                TextBox TextBoxPreviousChartData = (TextBox)item.FindControl("TextBoxPreviousChartData");

                if (chkModule.Checked)
                {
                    
                    PreviousModulesSelected = PreviousModulesSelected + HiddenFieldModuleID.Value + ",";
                    PreviousNumberOfCharts = PreviousNumberOfCharts + HiddenFieldNumberOfCharts.Value + ",";
                    ModuleID = Convert.ToInt32(HiddenFieldModuleID.Value);
                    Module module = pim.Module.First(m => m.ModuleID == ModuleID);
                    CycleManager.AddPreviousChartRegistration(ctx.ActiveModuleCycleID, participant.CandidateID, ModuleID, Convert.ToInt32(TextBoxPreviousChartData.Text));
                    CycleManager.CopyChartAbstraction(ctx.ActiveModuleCycleID, participant.CandidateID, ModuleID, module.CopyChartAbstractionsProcedure);
                    
                }
            }
            if (PreviousModulesSelected != "") // remove the last comma
            {
                PreviousModulesSelected = PreviousModulesSelected.Substring(0, PreviousModulesSelected.Length - 1);
                PreviousNumberOfCharts = PreviousNumberOfCharts.Substring(0, PreviousNumberOfCharts.Length - 1);
            }
        }

        Session[Constants.SESSION_PREVIOUS_MODULES] = PreviousModulesSelected;
        Session[Constants.SESSION_PREVIOUS_NUMBEROFCHARTS] = PreviousNumberOfCharts;

        using (NetHealthPIMEntities pim = new NetHealthPIMEntities())
        {
            int cycleID = ctx.ActiveModuleCycleID;
            ParticipantModuleCycle cycle = pim.ParticipantModuleCycle.First(c => c.ParticipantModuleCycleID == cycleID);
            cycle.SelectPreviousChartDataComplete = true;
            cycle.SelectPreviousChartDataCompletedDate = DateTime.Today;
            cycle.ModuleSelectionComplete = false;
            cycle.ModuleSelectionCompletedDate = null;
            cycle.SelectNewModulesComplete = false;
            cycle.SelectNewModulesCompletedDate = null;
            cycle.AbstractDataCompletedDate = null;
            cycle.AbstractDataComplete = false;
            pim.SaveChanges();
        }

        Response.Redirect("ModuleSelection.aspx");
    }

    
}
