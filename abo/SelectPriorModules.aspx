﻿<%@ Page Title="" Language="C#" MasterPageFile="~/PIMMasterPage.master" AutoEventWireup="true" CodeFile="SelectPriorModules.aspx.cs" Inherits="abo_SelectPriorModules" %>

<asp:Content ID="Content1" ContentPlaceHolderID="head" Runat="Server">

    <style>
    .table tr th, .table tr td {
            border-right:none;
            border-left:none;
        }

        .table tr th.first, .table tr td.first {
            border-left: 1px solid #ccc;
        }
        .table tr th.last, .table tr td.last {
            border-right: 1px solid #ccc;
        }
    </style>

</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder1" Runat="Server">

    <asp:HiddenField runat="server" ID="HiddenFieldChartsSelected" />
    <asp:HiddenField ID="HiddenFieldCurrentSelectedModules" runat="server" value="" />
    <asp:HiddenField ID="HiddenFieldModuleSelectionChanged" runat="server" value="1" />

    <div class="heading_box">
        <h2><asp:Literal ID="LiteralShortModuleDescription" runat="server"></asp:Literal>Import Data</h2>
        <p>
            If you have already completed a improvement in medical practice activity, you can elect to use the data from your last assessment in combination with new data as the baseline data for your new assessment.  
        </p>
        <p runat="server" id="PreviousData1" visible="false">
            Below is a table that shows your previously completed improvement in medical practice activities.  
      		To include the data from the last assessment for any of these activities, click on the button next to that activity to include all the charts.
		If you only want to use a portion of these charts, enter the appropriate number in the entry box next to the activity name.  The most
		recent charts will be imported.
        </p>
        <p runat="server" id="PreviousData2" visible="false">
            If you do not want to use data from a previous assessment, click Continue Without Importing Data. 
        </p> 
        <p runat="server" id="NoPreviousData" visible="false">No previous data exists.</p>
    </div>
    <%-- Activity Start --%>
    <asp:Repeater ID="RepeaterPriorModuleSelection" runat="server">
    <HeaderTemplate>
    <table class="table">
        <tr>
            <th class="first"></th>
            <th>PEA</th>
            <th>Activity</th>
            <th>Charts</th>
            <th class="last">Date Abstracted</th>
        </tr>
    </HeaderTemplate>
    <ItemTemplate>
        <tr>
            <td class="first">
                <asp:HiddenField ID="HiddenFieldModuleID" Value='<%# Eval("ModuleID") %>' runat="server" />
                <asp:HiddenField ID="HiddenFieldNumberOfCharts" Value='<%# Eval("NumberOfCharts") %>' runat="server" />
                <%--<asp:CheckBox id = "CheckBoxModuleSelected" runat="server" class="<%# Eval(&quot;NumberOfCharts&quot;) %>" tag='' onClick="checkModules(this);" />--%>
                <input type="checkbox" runat="server" id="chkModule" value="<%# Eval(&quot;NumberOfCharts&quot;) %>" onclick="checkModules(this);countChecked();"/>
            </td>
            <td>
                <span class="text"><%# Eval("ModuleCategoryName")%></span>
            </td>
            <td>
                <span class="text"><%# Eval("ModuleDescription")%></span>
            </td>
            <td>
                <span class="text">
                <asp:TextBox runat="server" ID='TextBoxPreviousChartData' Text='0' class="NumberOfCharts" onchange="return MaxCharts(this);" MaxLength="2" Width="30px"></asp:TextBox> of <asp:Label><%# Eval("NumberOfCharts")%></asp:Label> Charts
                </span>
            </td>
            <td class="last">
                <span class="text"><%# Convert.ToDateTime(Eval("AbstractDataCompletedDate")).ToShortDateString()%></span>
            </td>
        </tr>
    </ItemTemplate>
    <FooterTemplate>
        </table>                        
    </FooterTemplate>
    </asp:Repeater>
    <%-- Activity End --%>
    <div class="button-box">
        <%--<asp:LinkButton ID="LinkButtonBack" runat="server" Text="Back To Confirm Activities" Visible="false" CssClass="button" />--%>
        <asp:LinkButton ID="LinkButtonSave" runat="server" OnCommand="LinkButtonSave_Click" Text=" Continue Without Importing Data " CssClass="button" OnClientClick="return readyToSubmit();" />
    </div>
</asp:Content>

<asp:Content runat="server" ID="Content4" ContentPlaceHolderID="javascript">
    <script type="text/javascript" language="javascript">
        var totalcharts = 0;
        var MAX_NUMBEROFCHARTS = 30;

        function checkModules(chkModule) {
            //alert(chkModule.value);
            var ret = true;
            //alert(chkModule.checked);
            //alert(chkModule.id);
            //alert(chkModule.className);
            //alert(document.all(chkModule.id).className);
            if (chkModule.checked)
                totalcharts = totalcharts + parseInt(chkModule.value);
            else
                totalcharts = totalcharts - parseInt(chkModule.value);

            var ctrlName = chkModule.id.toString();
            var textBoxPreviousChartData = ctrlName.replace("chkModule", "TextBoxPreviousChartData");
            var hiddenFieldNumberOfCharts = ctrlName.replace("chkModule", "HiddenFieldNumberOfCharts");

            if (chkModule.checked)
                document.getElementById(textBoxPreviousChartData).value = document.getElementById(hiddenFieldNumberOfCharts).value.toString();
            else
                document.getElementById(textBoxPreviousChartData).value = "0";

            //alert(totalcharts);

//            if (totalcharts > MAX_NUMBEROFCHARTS) {
//                alert('The maximun number of charts must be ' + MAX_NUMBEROFCHARTS.toString());
//                totalcharts = totalcharts - parseInt(chkModule.value);
//                chkModule.checked = false;
//            }

            var HiddenFieldChartsSelected = document.getElementById("<%= HiddenFieldChartsSelected.ClientID %>");
            HiddenFieldChartsSelected.value = totalcharts.toString();

            //alert(HiddenFieldNumberOfCharts);


        }

        var countChecked = function () {

            var n = $("input:checked").length;

            if (n > 0)
                document.getElementById("<%= LinkButtonSave.ClientID %>").innerHTML = " Import Selected Data ";
            else
                document.getElementById("<%= LinkButtonSave.ClientID %>").innerHTML = " Continue Without Importing Data ";


        };

        function readyToSubmit() {
            var n = $("input:checked").length;
            if (n > 3) {
                alert("Please select up to 3 activities");
                return false;
            }

            totalcharts = 0;
            $('.NumberOfCharts').each(function() {
                var currentValue = parseInt(this.value);
                totalcharts = totalcharts + currentValue;
            });
            
            if (totalcharts > MAX_NUMBEROFCHARTS) {
                alert('The maximun number of charts must be ' + MAX_NUMBEROFCHARTS.toString());
                //totalcharts = totalcharts - parseInt(chkModule.value);
                return false;
            }

            var HiddenFieldCurrentSelectedModules = document.getElementById("<%= HiddenFieldCurrentSelectedModules.ClientID %>");
            var HiddenFieldModuleSelectionChanged = document.getElementById("<%= HiddenFieldModuleSelectionChanged.ClientID %>");
            var currentSelectedModules = getCurrentSelectedModules();
            //HiddenFieldCurrentSelectedModules.value = getCurrentSelectedModules();
            if (currentSelectedModules == HiddenFieldCurrentSelectedModules.value)
                HiddenFieldModuleSelectionChanged.value = "0";
            //alert(HiddenFieldModuleSelectionChanged.value);


            return true;      
        }


        //        $(document).ready(function() {
        //            
        //            $(".NumberOfCharts").click(function() {
        //                //var modulesAreSelected = 0;
        //                $('.NumberOfCharts').each(function() {
        //                    if (this.checked)
        //                        modulesAreSelected = 1
        //                });
        //                if (modulesAreSelected == 1)
        //                    document.getElementById("<%= LinkButtonSave.ClientID %>").Text = " Import Selected Data ";
        //                else
        //                    document.getElementById("<%= LinkButtonSave.ClientID %>").Text = " Continue Without Importing Data ";
        //                //alert(numberOfCharts);
        //            })
        //        })

        function MaxCharts(_self) {
            //alert(_self.value);
            var ctrlName = _self.id.toString();

            var newControlName = ctrlName.replace("TextBoxPreviousChartData", "HiddenFieldNumberOfCharts");
            var currentCheckBox = ctrlName.replace("TextBoxPreviousChartData", "chkModule"); 

            var myfocus = document.getElementById(_self.id);

            var mycontrolName = document.getElementById(newControlName).value.toString();

            var currentValue = _self.value;
            if (currentValue == Number(currentValue)) {
                if (parseInt(Number(currentValue)) > parseInt(Number(mycontrolName))) {
                    alert("Maximun number of previous charts for this activity is " + parseInt(Number(mycontrolName)).toString());
                    //myfocus.focus();
                    _self.value = parseInt(Number(mycontrolName)).toString();
                    //return false;
                }
//                if ((parseInt(Number(currentValue)) < MIN_NUMBER_OF_CHARTS) && (parseInt(Number(currentValue)) > 0)) {
//                    alert("Minimun number of charts for any activity is " + MIN_NUMBER_OF_CHARTS.toString());
//                    self.focus();
//                    currentTextBox = myfocus;
//                    return false;
//                }
//                if ((parseInt(Number(currentValue)) > MAX_NUMBER_OF_CHARTS) && (parseInt(Number(currentValue)) > 0)) {
//                    alert("Maximun number of charts for any activity is " + MAX_NUMBER_OF_CHARTS.toString());
//                    self.focus();
//                    currentTextBox = myfocus;
//                    return false;
//                }
            }
            else {
                alert("Please enter a valid number");
                _self.value = "0";
                //return false;
            }
            
            if ($.trim(currentValue) == "") {
                _self.value = "0";
                }
                
            if (currentValue>0)
                document.getElementById(currentCheckBox).checked = true;
            else
                document.getElementById(currentCheckBox).checked = false;

            countChecked();
            
            return true;

        }

        $(document).ready(function() {
            countChecked();
            var HiddenFieldCurrentSelectedModules = document.getElementById("<%= HiddenFieldCurrentSelectedModules.ClientID %>");
            HiddenFieldCurrentSelectedModules.value = getCurrentSelectedModules();
            //alert(HiddenFieldCurrentSelectedModules.value);

        })

        function getCurrentSelectedModules() {
            var currentList = "";

            $('.NumberOfCharts').each(function() {
                var currentValue = this.value;
                currentList = currentList + currentValue;
            });
            return currentList;
        }


    </script>
</asp:Content>