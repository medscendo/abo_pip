﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using NetHealthPIMModel;
using System.Data.Objects;
using System.Transactions;

public partial class copd_ImprovementPlan : BasePage
{
    bool viewMode;
    static int rowIndex = 1;
    static string MeasureTitle = "";

    protected void Page_Load(object sender, EventArgs e)
    {
        if (!IsPostBack)
        {
            InitializePage();
            int cycleID = ctx.ActiveModuleCycleID;
            if (Request[Constants.QUERYSTRING_CYCLEID] != null)
            {
                cycleID = Int32.Parse(Request[Constants.QUERYSTRING_CYCLEID]);
            }

            using (NetHealthPIMEntities pim = new NetHealthPIMEntities())
            {
                ParticipantModuleCycle cycle = pim.ParticipantModuleCycle.First(c => c.ParticipantModuleCycleID == cycleID);
                ParticipantModuleCycle cycle2;

                if (cycle.CycleNumber == 2)
                {
                    cycle2 = cycle;

                    cycle.PrevParticipantModuleCycleReference.Load();
                    ParticipantModuleCycle prev = cycle.PrevParticipantModuleCycle;
                    cycle = prev;

                    cycleID = cycle.ParticipantModuleCycleID;

                    var chartRegistrationEntered = (from cr in pim.ChartRegistration
                                                    where cr.ParticipantModuleCycleID == ctx.ActiveModuleCycleID
                                             && cr.AbstractCompleted != null
                                             select cr).Count();
                    if (chartRegistrationEntered == 0)
                    {
                        LinkButtonRemeasure.Visible = true;
                        LinkButtonRemoveMeasure.Visible = true;
                    }

                }
                else
                {
                    var meausuresSelected = (from pmr in pim.ParticipantMeasure
                                             where pmr.ParticipantModuleCycleID == cycleID
                                             && pmr.IncludeInImprovementPlan == true
                                         select pmr).Count();
                    if (meausuresSelected==0)
                        Response.Redirect("SelectMeasures.aspx");

                    if (cycle.IsComplete == true)
                    {
                        LinkButtonRemeasure.Visible = true;
                        LinkButtonRemoveMeasure.Visible = true;
                    }
                }

               
                bool P2Flag = cycle.Phase2Flag ?? false;
                if (Request[Constants.QUERYSTRING_PHASE] != null)
                {
                    if (Request[Constants.QUERYSTRING_PHASE] == "1")
                    {
                        P2Flag = false;
                    }
                }
                else
                {
                    rowIndex = 1; 
                    System.Linq.IQueryable report;

                    var isGroupMeasures = false;
                    if (ctx.ParticipantGroupID != 0) 
                        if (ctx.ParticipantID == ctx.LeaderParticipantID)
                            isGroupMeasures = true;
                    if (isGroupMeasures == true)
                    {
                        
                        report = from pm in pim.ParticipantGroupMeasure
                                 from ma in pim.MeasureAggregate
                                 from mgm in pim.MeasureGroupModule
                                 from m in pim.Module
                                 where pm.MeasureID == ma.Measure.MeasureID
                                    && pm.ParticipantGroupID == ctx.ParticipantGroupID
                                    && pm.Measure.MeasureType.MeasureTypeID == 1
                                    && pm.IncludeInImprovementPlan == true
                                    && ma.Module.ModuleID == ctx.ActiveModuleID
                                    && ma.MeasureGroup.MeasureGroupID == mgm.MeasureGroupID
                                    && ma.MeasureAggregateType.MeasureAggregateTypeID == 1
                                    && ma.Module.ModuleID == m.ModuleID
                                 orderby mgm.MeasureGroupSortOrder
                                 select new PerformanceReportData
                                 {
                                     MeasureID = pm.MeasureID,
                                     MeasureGroupSortOrder = mgm.MeasureGroupSortOrder,
                                     MeasureTitle = pm.Measure.MeasureTitle,
                                     MeasureClinicalRecommendation = pm.Measure.MeasureClinicalRecommendation,
                                     MeasureLongDescription = pm.Measure.MeasureLongDescription,
                                     AbstractRecordsIncluded =Convert.ToInt32(pm.AbstractRecordsIncluded),
                                     MeasurePercentCurrent = pm.MeasurePercent,
                                     MeasureGoal = pm.MeasureGoal,
                                     PeerData = ma.MeasurePercent,
                                     IncludeInImprovementPlan = pm.IncludeInImprovementPlan,
                                     ModuleName = m.ModuleName,
                                     OUT20= ""
                                 };
                    }
                    else
                    {
                        // Get measure aggregates 
                        report = from pm in pim.ParticipantMeasure
                                     from ma in pim.MeasureAggregate
                                     from m in pim.Module
                                     from me in pim.Measure
                                     where pm.MeasureID == ma.Measure.MeasureID
                                        && pm.ParticipantModuleCycle.ParticipantModuleCycleID == cycleID
                                        && pm.IncludeInImprovementPlan == true
                                        && ma.MeasureAggregateType.MeasureAggregateTypeID == 1
                                        && ma.Module.ModuleID == m.ModuleID
                                        && pm.MeasureID == me.MeasureID
                                     orderby pm.MeasureID 
                                     select new PerformanceReportData
                                     {
                                         MeasureID = pm.MeasureID,
                                         MeasureGroupSortOrder = 1,
                                         MeasureTitle = pm.Measure.MeasureTitle,
                                         MeasureQualityIndicator = pm.Measure.MeasureQualityIndicator,
                                         MeasureClinicalRecommendation = pm.Measure.MeasureClinicalRecommendation,
                                         MeasureLongDescription = pm.Measure.MeasureLongDescription,
                                         AbstractRecordsIncluded = (int)pm.AbstractRecordsIncluded,
                                         MeasurePercentCurrent = pm.MeasurePercent,
                                         MeasureGoal = pm.MeasureGoal,
                                         PeerData = ma.MeasurePercent,
                                         IsPhase2Flag = pm.ParticipantModuleCycle.Phase2Flag,
                                         IncludeInImprovementPlan = pm.IncludeInImprovementPlan,
                                         ModuleName = m.ModuleName,
                                         MeanDisplayFlag = me.IsNumericMeasure == null ? false : me.IsNumericMeasure,
                                         OUT20 = me.NumericFullGraph == true ? "20/" : ""
                                     };
                    }
                    viewMode = cycle.ImprovementPlanComplete;
                    string q = Support.TraceLinqSQL(report);
                    ListViewMeasureGoals.DataSource = report;

                    
                    var resources = from pm in pim.ParticipantMeasure
                                    join m in pim.Measure on pm.MeasureID equals m.MeasureID
                                    join mr in pim.MeasureResource on pm.MeasureID equals mr.MeasureID
                                    join r in pim.Resource on mr.ResourceID equals r.ResourceID
                                    where pm.ParticipantModuleCycleID == cycleID && pm.IncludeInImprovementPlan == true
                                    && r.RecommendedFlag == false
                                    orderby pm.MeasureID, r.ResourceName 
                                    let selectedItems = (from mrc in mr.MeasureResourceCycle
                                                         select mrc.ResourceID
                                    )
                                    select new
                                    {
                                        MeasureGroupSortOrder = 1,
                                        MeasureTitle = m.MeasureTitle,
                                        MeasureID = m.MeasureID,
                                        ResourceID = r.ResourceID,
                                        ResourceTypeID = r.ResourceType.ResourceTypeID,
                                        ResourceName = r.ResourceName,
                                        ResourceDescription = r.ResourceDescription,
                                        URL = r.URL,
                                        RecommendedFlag = r.RecommendedFlag,
                                        ClientFlag = r.ClientFlag,
                                        CostFlag = r.CostFlag,
                                        AAOResourceFlag = r.AAOResourceFlag,
                                        SelectedItem = (selectedItems.Count() > 0),
                                        OUT20 = m.NumericFullGraph == true ? "20/" : ""
                                    };


                    viewMode = cycle.ImprovementPlanComplete;
                    ListViewAdditionalResources.DataSource =
                        from r in resources
                        where r.ClientFlag == false && r.ResourceTypeID != Constants.RESOURCETYPE_ACTIVITY && (viewMode == false || (viewMode == true && r.SelectedItem == true))
                        orderby r.MeasureGroupSortOrder, r.MeasureID, !r.RecommendedFlag, r.ResourceName
                        select r;


                    if (resources.Count() == 0)
                    {
                        LabelQ1.Text = "1";
                        LabelQ2.Text = "2";
                        LabelQ4.Text = "3";
                        resourcesdiv.Visible = false;
                    }
                    else
                    {
                        LabelQ1.Text = "1";
                        LabelQ2.Text = "2";
                        LabelQ3.Text = "3";
                        LabelQ4.Text = "4";

                    }


                    //Step V
                    viewMode = cycle.ImprovementPlanComplete;
                    DataBind();
       

                    if (viewMode)
                    {
                        ButtonSaveForLater.Visible = false;
                        LinkDashboard.Visible = true;
                    }

                    if (cycle.ImprovementPlanCompletedDate != null && cycle.RemeasureDate!=null)
                    {
                        labelDateForRemeasure.Text = ((DateTime)(cycle.RemeasureDate)).ToShortDateString();
                    }
                    else
                    {
                        var daysForRemeasure = (from pcs in pim.ParticipantChartStatus_V
                                                join mo in pim.Module on pcs.ModuleID equals mo.ModuleID
                                                join me in pim.Measure on mo.ModuleID equals me.Module.ModuleID
                                                join pm in pim.ParticipantMeasure on pcs.ParticipantModuleCycleID equals pm.ParticipantModuleCycleID  
                                                where pcs.ParticipantModuleCycleID == ctx.ActiveModuleCycleID 
                                                && pm.IncludeInImprovementPlan == true  
                                                && me.MeasureID == pm.MeasureID
                                                select mo.MinimumImprovementPlan).Max();

                        double dblDaysForRemeasure = Convert.ToDouble(daysForRemeasure);
                        labelDateForRemeasure.Text = DateTime.Now.AddDays(dblDaysForRemeasure).ToShortDateString();
                        if (ctx.SoftwareVersion==0 )
                            labelDateForRemeasure.Text = DateTime.Today.ToShortDateString();
                        else
                            labelDateForRemeasure.Text = DateTime.Now.AddDays(dblDaysForRemeasure).ToShortDateString();
                        
                    }



                    //Report date
                    if (cycle.ImprovementPlanCompletedDate != null)
                    {
                        LiteralCompletedDate.Text = " - Completed on " + cycle.ImprovementPlanCompletedDate.Value.ToString("d");
                    }

                    if (cycle.ImportantChangesComments != null) TextBoxImportantChanges.Text = cycle.ImportantChangesComments;

                    if (viewMode)
                    {
                        LabelImportantChanges.Text = TextBoxImportantChanges.Text;
                        LabelImportantChanges.Visible = true;
                        TextBoxImportantChanges.Visible = false;
                        ButtonSaveForLater.Visible = false;
                        PanelInformation.Visible = false;
                        ButtonSubmit.Visible = false;
                    }
                }
            }
        }
    }

    protected void InitializePage()
    {

        string breadCrumb = Constants.BC_DASHBOARD_LINK + "" + Constants.BC_PERFORMANCE_REPORT_LINK + "" +
                    Constants.BC_IMPROVEMENT_PLAN;
        ((PIMMasterPage)Page.Master).SetTitle(breadCrumb);
        ((PIMMasterPage)Page.Master).SetStatus(2);
        ((PIMMasterPage)Page.Master).SetHeader("Improvement Plan");
    }

    protected void GridViewMeasureGoals_RowDataBound(object sender, GridViewRowEventArgs e)
    {
        if (e.Row.RowType == DataControlRowType.DataRow)
        {
            ((Label)e.Row.Cells[0].FindControl("LabelMeasureNumber")).Text = (e.Row.RowIndex + 1).ToString();
            if (viewMode)
            {
                ((TextBox)e.Row.Cells[4].FindControl("TextBoxMeasureGoal")).Visible = false;
                ((Label)e.Row.Cells[4].FindControl("LabelPercentSign")).Visible = false;
                ((Label)e.Row.Cells[4].FindControl("LabelMeasureGoal")).Visible = true;
            }
        }
    }

    protected void ListViewMeasureGoals_OnItemDataBound(object sender, ListViewItemEventArgs e)
    {
        if (e.Item.ItemType == ListViewItemType.DataItem)
        {
            ((Label)e.Item.FindControl("LabelMeasureNumber")).Text = (rowIndex++).ToString();
            HyperLink chartsIncluded = (HyperLink)e.Item.FindControl("LinkAbstractRecordsIncluded");
            if (viewMode)
            {
                ((TextBox)e.Item.FindControl("TextBoxMeasureGoal")).Visible = false;
                ((Label)e.Item.FindControl("LabelPercentSign")).Visible = false;
                ((Label)e.Item.FindControl("LabelMeasureGoal")).Visible = true;
            }
        }
    }
    protected void ListViewAdditionalResources_RowDataBound(object sender, ListViewItemEventArgs e)
    {
        if (e.Item.ItemType == ListViewItemType.DataItem)
        {
            if (MeasureTitle != ((HiddenField)e.Item.FindControl("HiddenFieldMeasureTitle")).Value)
            {
                ((System.Web.UI.HtmlControls.HtmlGenericControl)e.Item.FindControl("liMeasureTitle")).Visible = true;
                MeasureTitle = ((HiddenField)e.Item.FindControl("HiddenFieldMeasureTitle")).Value;
            }

            bool CostFlag = Boolean.Parse(((HiddenField)e.Item.FindControl("HiddenFieldCostFlag")).Value);
            bool AAOResourceFlag = Boolean.Parse(((HiddenField)e.Item.FindControl("HiddenFieldAAOResourceFlag")).Value);

            if (CostFlag)
                ((Image)e.Item.FindControl("ImageCost")).Visible= true;
            if (AAOResourceFlag)
                ((Image)e.Item.FindControl("ImageAAO")).Visible = true;
            
            if (viewMode)
            {
                ((CheckBox)e.Item.FindControl("CheckBoxResource")).Enabled = false;
            }
        }
    }

    protected bool validForm()
    {
        return true;
    }

    protected bool isValid()
    {
        return true;
    }

    protected bool isComplete()
    {
        return true;
    }

    protected void SaveImprovementPlan(bool FinalPlan)
    {
        if (validForm())
        {
            bool completeRecord = isComplete();
            bool validRecord = isValid();
            int currentCycleID = 0;
            using (NetHealthPIMEntities pim = new NetHealthPIMEntities())
            {
                using (TransactionScope transaction = new TransactionScope())
                {
                    ParticipantModuleCycle cycle = pim.ParticipantModuleCycle.First(c => c.ParticipantModuleCycleID == ctx.ActiveModuleCycleID);
                    ParticipantMeasure[] measures = new ParticipantMeasure[ListViewMeasureGoals.Items.Count];
                    ParticipantGroupMeasure[] measuresGroup = new ParticipantGroupMeasure[ListViewMeasureGoals.Items.Count];
                    var isGroupMeasures = false;
                    if (ctx.ParticipantGroupID != 0) //user belong to a group
                        if (ctx.ParticipantID == ctx.LeaderParticipantID)
                            isGroupMeasures = true;
                    if (isGroupMeasures == true)
                    {
                        for (int i = 0; i < ListViewMeasureGoals.Items.Count; i++)
                        {
                            int mid = Int32.Parse(((HiddenField)ListViewMeasureGoals.Items[i].FindControl("HiddenFieldMeasureID")).Value);
                            measuresGroup[i] = pim.ParticipantGroupMeasure.First(pm => pm.MeasureID == mid && pm.ParticipantGroupID == ctx.ParticipantGroupID);

                            // New goal
                            TextBox goal = (TextBox)ListViewMeasureGoals.Items[i].FindControl("TextBoxMeasureGoal");
                            if (goal.Text.Trim().Length > 0)
                            {
                                measuresGroup[i].MeasureGoal = Int32.Parse(goal.Text);
                                measuresGroup[i].LastUpdateDate = DateTime.Now;
                            }
                            else
                                completeRecord = false;
                        }
                    }
                    else
                    {
                        for (int i = 0; i < ListViewMeasureGoals.Items.Count; i++)
                        {
                            int mid = Int32.Parse(((HiddenField)ListViewMeasureGoals.Items[i].FindControl("HiddenFieldMeasureID")).Value);
                            measures[i] = pim.ParticipantMeasure.First(pm => pm.MeasureID == mid && pm.ParticipantModuleCycleID == ctx.ActiveModuleCycleID);

                            // New goal
                            TextBox goal = (TextBox)ListViewMeasureGoals.Items[i].FindControl("TextBoxMeasureGoal");
                            if (goal.Text.Trim().Length > 0)
                            {
                                try
                                {
                                    measures[i].MeasureGoal = Int32.Parse(goal.Text);
                                    measures[i].LastUpdateDate = DateTime.Now;
                                }
                                catch (Exception e)
                                {
                                    completeRecord = false;
                                }
                            }
                            else
                                completeRecord = false;
                        }

                    }
                    if (TextBoxImportantChanges.Text.Trim().Length > 0)
                        cycle.ImportantChangesComments = TextBoxImportantChanges.Text.Trim();
                    else
                    {
                        cycle.ImportantChangesComments = "";
                        completeRecord = false;
                    }

                    cycle.ImprovementPlanLastUpdatedDate = DateTime.Now;
                    cycle.LastUpdateDate = DateTime.Now;
                    // saved curent Plan Part Completed 
                    if (cycle.ImprovementPlanPartCompleted <= Constants.ASN_IMPROVEMENTPLAN_PARTI) cycle.ImprovementPlanPartCompleted = Constants.ASN_IMPROVEMENTPLAN_PARTI;
                    if (cycle.ImprovementPlanPartCompleted == null) cycle.ImprovementPlanPartCompleted = Constants.ASN_IMPROVEMENTPLAN_PARTI;
                    pim.SaveChanges();


                    // Resources
                    // Look for selected resources
                    for (int i = 0; i < ListViewAdditionalResources.Items.Count; i++)
                    {
                        int mid = Int32.Parse(((HiddenField)ListViewAdditionalResources.Items[i].FindControl("HiddenFieldMeasureID")).Value);
                        int rid = Int32.Parse(((HiddenField)ListViewAdditionalResources.Items[i].FindControl("HiddenFieldResourceID")).Value);
                        bool resourceSelected = ((CheckBox)ListViewAdditionalResources.Items[i].FindControl("CheckBoxResource")).Checked;
                        var count = (pim.MeasureResourceCycle.Where(mr => mr.MeasureID == mid && mr.ResourceID == rid).Count());
                        if (resourceSelected)
                        {
                            // Add the measure resource cycle record if not already in the database. 
                            if (count == 0)
                            {
                                MeasureResource mr = pim.MeasureResource.First(m => m.MeasureID == mid && m.ResourceID == rid);
                                MeasureResourceCycle mrc = new MeasureResourceCycle();
                                mrc.MeasureResource = mr;
                                mrc.ParticipantModuleCycle = cycle;
                                mrc.LastUpdateDate = DateTime.Now;
                            }
                        }
                        else if (count > 0)
                        {
                            var mrc = pim.MeasureResourceCycle.First(mr => mr.MeasureID == mid && mr.ResourceID == rid);
                            pim.DeleteObject(mrc);
                        }
                    }

                    cycle.ImprovementPlanLastUpdatedDate = DateTime.Now;
                    cycle.LastUpdateDate = DateTime.Now;
                    // saved curent Plan Part Completed 
                    if (cycle.ImprovementPlanPartCompleted <= Constants.ASN_IMPROVEMENTPLAN_PARTIV) cycle.ImprovementPlanPartCompleted = Constants.ASN_IMPROVEMENTPLAN_PARTIV;
                    pim.SaveChanges();

                    cycle.ImprovementPlanLastUpdatedDate = DateTime.Now;
                    cycle.LastUpdateDate = DateTime.Now;
                    // saved curent Plan Part Completed 
                    if (cycle.ImprovementPlanPartCompleted <= Constants.ASN_IMPROVEMENTPLAN_PARTV) cycle.ImprovementPlanPartCompleted = Constants.ASN_IMPROVEMENTPLAN_PARTV;
                    pim.SaveChanges();

                    // submit Final Plan
                    if ((FinalPlan) && (completeRecord))
                    {
                        cycle.ImprovementPlanComplete = true;
                        cycle.ImprovementPlanCompletedDate = DateTime.Now;
                        // TODO: this works. just need to change code to accept this
                        cycle.IsComplete = true;
                        cycle.CycleEndDate = DateTime.Now;
                        if (isGroupMeasures == true) //group?
                            CycleManager.ApprovedImprovementGroupPlan(ctx.ParticipantGroupID, 1);

                        currentCycleID = cycle.ParticipantModuleCycleID;
                    }
                    cycle.ImprovementPlanLastUpdatedDate = DateTime.Now;
                    cycle.LastUpdateDate = DateTime.Now;
                    cycle.RemeasureDate = DateTime.Today;
                    pim.SaveChanges();

                    transaction.Complete();
                }
            }
            // If we completed the first two phases, create a new cycle
            if ((FinalPlan) && (completeRecord))
            {
                // TODO: this works. just need to change code to accept this

                if (ctx.SoftwareVersion > 0)
                    CycleManager.SetReOpenModule_NextCycle(currentCycleID);
                else
                {

                    CycleManager.CreateNewCycle();
                    Response.Redirect("Dashboard3.aspx");
                }
                
            }
            if ((FinalPlan) && !(completeRecord))
            {
                string script = " var answer = confirm('All fields are not complete.  Are you sure you want to Submit Final Plan? The Final Plan does required that all questions have been answered.'); if (answer==true) window.location = 'Dashboard.aspx';";
                ScriptManager.RegisterStartupScript(this, GetType(),
                              "ServerControlScript", script, true);
            }

        }
    }

    protected void ButtonSaveForLater_Click(object sender, EventArgs e)
    {
        SaveImprovementPlan(false);
        Response.Redirect("~/" + module.ModuleCode.ToLower() + "/Dashboard.aspx");
    }

    protected void ButtonNext_Click(object sender, EventArgs e)
    {
        SaveImprovementPlan(false);
        Response.Redirect("~/" + module.ModuleCode.ToLower() + "/ImprovementPlanPartII.aspx");
    }

    protected void ButtonSubmit_Click(object sender, EventArgs e)
    {
        SaveImprovementPlan(true);
        Response.Redirect("ImprovementPlanComplete.aspx");
    }

    
    protected void LinkButtonDashboard_Click(object sender, EventArgs e)
    {
        Response.Redirect("Dashboard.aspx");
    }

    protected void ButtonRemeasure_Click(object sender, EventArgs e)
    {
        CycleManager.RemoveSelectedMeasures(ctx.ActiveModuleCycleID);
        ctx.ActiveModuleCycleID = 0;
        CycleManager.CheckCycle(); 
        Response.Redirect("SelectMeasures.aspx");
    }
   
    protected void ButtonRemoveMeasure_Click(object sender, EventArgs e)
    {
        int cycleID = ctx.ActiveModuleCycleID;
        using (NetHealthPIMEntities pim = new NetHealthPIMEntities())
        {
            ParticipantModuleCycle cycle = pim.ParticipantModuleCycle.First(c => c.ParticipantModuleCycleID == cycleID); 
                Response.Redirect("MeasuresSelected.aspx");
        }
    }
}
