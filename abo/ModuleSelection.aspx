﻿<%@ Page Title="" Language="C#" MasterPageFile="~/PIMMasterPage.master" EnableEventValidation="false"  CodeFile="ModuleSelection.aspx.cs" Inherits="abo_ModuleSelection"%>

<asp:Content ID="Content1" ContentPlaceHolderID="head" Runat="Server">
<%--.spanDescription { margin-left:20px; }--%>
 <style >
    .floating {
        position:fixed;
        top:0;
        zoom: 1;
	    filter: alpha(opacity=90);
	    opacity: 0.9;
    }
 
    .chekInner { margin-left:20px; }
    

    .details {
        float:left;
        width:578px;
    }

    .progress-panel h2 {
        margin:0;
    }

    .progress-panel p {
        margin: 5px 0;
    }

    .progress-panel input[type='submit'] {
        margin:0 auto;
    }

    .table {
        width: 100%;
    }

    .table input {
        width: 30px;
            
        -webkit-box-shadow: inset 0px 0px 2px 0px rgb(151, 151, 151);
        box-shadow: inset 0px 0px 2px 0px rgb(151, 151, 151);
            
        text-align:center;
    }

    .table {
        margin: 15px 0;
        font-size: 0.9em;
    }

    .table .icon-cell {
        width:3.5%;
    }

    .table tr.active {
        border: 2px solid #0f588e;
    }

    .table tr.active td {
        border:none;
        background-color: #e5ecf4;
    }

    .firstwidth {
        /*width:66.5%;*/
    }
        
    .midwidth, .lastwidth {
        width:18%
    }

    #iframeModal {
        width:100%;
        height:100%;
        border:none;
    }
    #moduleOverview {
        padding-right:0;
    }

    .legend {
        float:left;
        border: 3px solid #333;
        padding: 10px;
    }
</style>
<style>
    #<%= ButtonSubmit.ClientID %> {
        display: inline-block !important;
    }
        
</style>
<link rel="stylesheet" href="http://code.jquery.com/ui/1.10.3/themes/smoothness/jquery-ui.css">
</asp:Content>


<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder1" Runat="Server">

<asp:HiddenField ID="HiddenFieldRecordID" runat="server" />
<asp:HiddenField ID="HiddenFieldModulesCurrentlySelected" runat="server" value="0"/>
<asp:HiddenField ID="HiddenFieldDeleteCurrentData" runat="server" value="0"/>
<asp:HiddenField ID="HiddenFieldDataEntered" runat="server" value="0"/>
<asp:HiddenField ID="HiddenFieldModuleSelectionConfirmed" runat="server" value="0"/>
<asp:HiddenField ID="HiddenFieldModuleSelection1" runat="server" value="0"/>
<asp:HiddenField ID="HiddenFieldModuleSelection2" runat="server" value="0"/>
<asp:HiddenField ID="HiddenFieldModuleSelection3" runat="server" value="0"/>
<asp:HiddenField ID="HiddenFieldModuleCharts1" runat="server" value="0"/>
<asp:HiddenField ID="HiddenFieldModuleCharts2" runat="server" value="0"/>
<asp:HiddenField ID="HiddenFieldModuleCharts3" runat="server" value="0"/>
<asp:HiddenField ID="HiddenFieldPhatologySelected" runat="server" Value="0" />
<asp:HiddenField ID="HiddenFieldCurrentSelectedModules" runat="server" value="" />
<asp:HiddenField ID="HiddenFieldPlanCreated" runat="server" value="0" />
<asp:HiddenField ID="HiddenFieldOverwriteCharts" runat="server" value="0" />
<asp:HiddenField ID="HiddenFieldModuleSelectionChanged" runat="server" value="1" />
<asp:UpdatePanel runat="server" ID="UpdatePanel1" RenderMode="Inline">
    <ContentTemplate>
    <asp:HiddenField ID="HiddenFieldMaxModulesToSelect" runat="server" value="3"/>
    </ContentTemplate>
</asp:UpdatePanel>
        
<div class="row clearfix" id="widthForFloating">
    <div class="details">
        <h3>
            
            ABO Activity Library: <span >
                    <span id="NumberOfModules" class="green1"></span>
            Activities Selected</span>
        </h3>
        <p>
            Below is the library of improvement in medical practice activities.  Here you will choose 1-3 activities respective to your practice experience.
        </p>
        <ul>
            <li>You must review at least 30 consecutive patient records associated with a diagnosis or diagnoses that you see in your practice.</li>
            <li>For each activity chosen, you must have a minimum of 10 patient records with that diagnosis within the given timeframe.</li>
	    
        </ul>
        <p>You can click on each activity name to view details of that activity.</p>
        <p>
            Next to each activity you select, indicate the number of charts you will review for this activity (minimum 10). The box to the right shows the total of the charts you indicate that you will review (total minimum is 30; no maximum).  If you elected to use data from a previous improvement in medical practice activities, that information will be automatically populated both in the table below and in the box to the right. 
        </p>
        <p>
            If you opted to participate in PQRS reporting by using PQRS-enabled activities, those activities appear below. 
        </p>
    
        <p id="PreviousModulesAreSelected" runat="server" visible="false">
            To complete your PIP, the first step will be to select activities from which you provide information from 30 de-identified patient charts.
        </p>   
        <ul style="padding-left: 20px;" id="Ul1" runat="server">
        <asp:Repeater ID="RepeaterModules" runat="server">
            <HeaderTemplate>
            </HeaderTemplate>
            <ItemTemplate>
                <li id="liModuleDescription" runat="server" visible="true">You have chosen to utilize <%# Eval("PreviousChartData") %> charts from your prior <%# Eval("ModuleName")%> Activity</li>            
            </ItemTemplate>
            <FooterTemplate>
            </FooterTemplate>
        </asp:Repeater>    
        </ul>        
       
        </p>
<div class="legend clearfix">
	<div>Legend:</div>
        <div><img src="../common/images/icon-check.png" /> - activity selected</div>
        <div><img src="../common/images/icon-dash.png" /> - to be completed</div>
    </div>
    </div>
    <div class="progress-panel">
        <h2><u><asp:Label runat="server" ID="LabelChartSelected"></asp:Label></u>/<asp:Label runat="server" ID="LabelChartsRequired"></asp:Label> Charts Selected</h2>
        <p><asp:Label runat="server" ID="LabelAllChartsSelected">All Charts Selected</asp:Label></p>
        <asp:Button ID="ButtonSubmit" runat="server" Text="Continue" OnClick="ButtonSubmit_Click" OnClientClick="return checkModules2();" Visible="true" />
    </div>
</div>


<form action="" method="get">
    <div id='moduleOverview'><iframe src="" id="iframeModal"></iframe></div>
    <table class="table">
        
        <asp:Repeater ID="RepeaterCategoryCataractAndAnterior" runat="server" OnItemDataBound="RepeaterModule_ItemDataBound">
        <HeaderTemplate>
        <tr>
            <th colspan="2" class="firstwidth">
                Cataract &amp; Anterior Segment
            </th>
            <th class="midwidth">Prior Activity Data</th>
            <th class="lastwidth">Add New Charts</th>
        </tr>
        </HeaderTemplate>
        <ItemTemplate>
        <asp:HiddenField ID="HiddenFieldModuleID" Value='<%# Eval("ModuleID") %>' runat="server" />
        <asp:HiddenField ID="HiddenFieldPreviousMaxCharts" Value='<%# Eval("PreviousModuleChartsAvailable") %>' runat="server" />
        <tr id="currentRow">
            <td class="icon-cell"><asp:Image ID="Image1" runat="server" ImageUrl="~/common/images/icon-dash.png" /></td>
            <td>
                <span class="text">
                    <!-- ID needs to be unique for each item (a) -->
                    <a href='javascript:void();' onclick='openwindowj(<%# Eval("ModuleID") %>);' class="example"><%# Eval("ModuleName")%></a> &nbsp; <%# Eval("PQRSEnabled")%>
                </span>
            </td>
            <td><asp:Literal runat="server" ID="LiteralNA"></asp:Literal><asp:TextBox runat="server" ID='TextBoxPreviousChartData' Text='<%# Eval("PreviousChartData")%>' class="NumberOfCharts" onchange="javascript:return MaxCharts(this);" MaxLength="2"></asp:TextBox><asp:Literal runat="server" ID="Literalof"></asp:Literal><asp:Label><%# Eval("PreviousModuleChartsAvailable")%></asp:Label><asp:Literal runat="server" ID="LiteralCharts"></asp:Literal></td>
            <td><asp:TextBox runat="server" ID="TextBoxNumberOfCharts" lang='<%# Eval("ModuleCode")%>' Text='<%# Eval("NumberOfCharts")%>' class="NumberOfCharts" onchange="return MaxCharts(this);" MaxLength="2"></asp:TextBox> Charts</td>
        </tr>
        
        <!-- activity box where AJAX info in loaded into -->
        <div style="display:none;" title="<%# Eval("ModuleName")%>" id="moduleInfo"></div>
        
        

        </ItemTemplate>
        </asp:Repeater>
    </table>
    
    
    <!-- Cornea External Disease -->
    <table class="table">
        <asp:Repeater ID="RepeaterCategoryCornea" runat="server" OnItemDataBound="RepeaterModule_ItemDataBound">
        <HeaderTemplate>
        <tr>
            <th colspan="2" class="firstwidth">
                Cornea/External Disease
            </th>
            <th class="midwidth">Prior Activity Data</th>
            <th class="lastwidth">Add New Charts</th>
        </tr>
    </HeaderTemplate>
    <ItemTemplate>
        <asp:HiddenField ID="HiddenFieldModuleID" Value='<%# Eval("ModuleID") %>' runat="server" />
        <asp:HiddenField ID="HiddenFieldPreviousMaxCharts" Value='<%# Eval("PreviousModuleChartsAvailable") %>' runat="server" />
        <tr id="currentRow">
            <td class="icon-cell"><asp:Image ID="Image1" runat="server" ImageUrl="~/common/images/icon-dash.png" /></td>
            <td><span class="text"><a href='javascript:void();' onclick='openwindowj(<%# Eval("ModuleID") %>);' class="example"><%# Eval("ModuleName")%></a> &nbsp; <%# Eval("PQRSEnabled")%></span></td>
            <td><asp:Literal runat="server" ID="LiteralNA"></asp:Literal><asp:TextBox runat="server" ID='TextBoxPreviousChartData' Text='<%# Eval("PreviousChartData")%>' class="NumberOfCharts" onchange="return MaxCharts(this);" MaxLength="2"></asp:TextBox><asp:Literal runat="server" ID="Literalof"></asp:Literal><asp:Label><%# Eval("PreviousModuleChartsAvailable")%></asp:Label><asp:Literal runat="server" ID="LiteralCharts"></asp:Literal></td>
            <td><asp:TextBox runat="server" ID="TextBoxNumberOfCharts" lang='<%# Eval("ModuleCode")%>' Text='<%# Eval("NumberOfCharts")%>' class="NumberOfCharts" onchange="return MaxCharts(this);" MaxLength="2"></asp:TextBox> Charts</td>
        </tr>
    </ItemTemplate>
    </asp:Repeater>
    </table>
    <%-- Activity End --%>     
    
    <%-- Activity Start --%>
    <table class="table">
    <asp:Repeater ID="RepeaterCategoryGlaucoma" runat="server" OnItemDataBound="RepeaterModule_ItemDataBound">
    <HeaderTemplate>
        <tr>
            <th colspan="2" class="firstwidth">
                Glaucoma
            </th>
            <th class="midwidth">Prior Activity Data</th>
            <th class="lastwidth">Add New Charts</th>
        </tr>                                     
    </HeaderTemplate>
    <ItemTemplate>
        <asp:HiddenField ID="HiddenFieldModuleID" Value='<%# Eval("ModuleID") %>' runat="server" />
        <asp:HiddenField ID="HiddenFieldPreviousMaxCharts" Value='<%# Eval("PreviousModuleChartsAvailable") %>' runat="server" />
        <tr id="currentRow">
            <td class="icon-cell"><asp:Image ID="Image1" runat="server" ImageUrl="~/common/images/icon-dash.png" /></td>
            <td><span class="text"><a href='javascript:void();' onclick='openwindowj(<%# Eval("ModuleID") %>);' class="example"><%# Eval("ModuleName")%></a> &nbsp; <%# Eval("PQRSEnabled")%></span></td>
            <td><asp:Literal runat="server" ID="LiteralNA"></asp:Literal><asp:TextBox runat="server" ID='TextBoxPreviousChartData' Text='<%# Eval("PreviousChartData")%>' class="NumberOfCharts" onchange="return MaxCharts(this);" MaxLength="2"></asp:TextBox><asp:Literal runat="server" ID="Literalof"></asp:Literal><asp:Label><%# Eval("PreviousModuleChartsAvailable")%></asp:Label><asp:Literal runat="server" ID="LiteralCharts"></asp:Literal></td>
            <td><asp:TextBox runat="server" ID="TextBoxNumberOfCharts" lang='<%# Eval("ModuleCode")%>'  Text='<%# Eval("NumberOfCharts")%>' class="NumberOfCharts" onchange="return MaxCharts(this);" MaxLength="2"></asp:TextBox> Charts</td>
        </tr>
    </ItemTemplate>
    </asp:Repeater>
    </table>
    <%-- Activity End --%>           
        
    <%-- Activity Start --%>
    <%-- Both activities for Neuro-Opthalmogy are now discontinued. 3/12/18 --%>
    <%--<table class="table">
    <asp:Repeater ID="RepeaterCategoryNeuroOpthalmogy" runat="server" OnItemDataBound="RepeaterModule_ItemDataBound">
	<HeaderTemplate>
	<tr>
		<th colspan="2" class="firstwidth">
			Neuro-Ophthalmology
		</th>
		<th class="midwidth">Prior Activity Data</th>
		<th class="lastwidth">Add New Charts</th>
	</tr>
	</HeaderTemplate>
    <ItemTemplate>
        <asp:HiddenField ID="HiddenFieldModuleID" Value='<%# Eval("ModuleID") %>' runat="server" />
        <asp:HiddenField ID="HiddenFieldPreviousMaxCharts" Value='<%# Eval("PreviousModuleChartsAvailable") %>' runat="server" />
        <tr id="currentRow">
            <td class="icon-cell"><asp:Image ID="Image1" runat="server" ImageUrl="~/common/images/icon-dash.png" /></td>
            <td><span class="text"><a href='javascript:void();' onclick='openwindowj(<%# Eval("ModuleID") %>);' class="example"><%# Eval("ModuleName")%></a> &nbsp; <%# Eval("PQRSEnabled")%></span></td>
            <td><asp:Literal runat="server" ID="LiteralNA"></asp:Literal><asp:TextBox runat="server" ID='TextBoxPreviousChartData' Text='<%# Eval("PreviousChartData")%>' class="NumberOfCharts" onchange="return MaxCharts(this);" MaxLength="2"></asp:TextBox><asp:Literal runat="server" ID="Literalof"></asp:Literal><asp:Label><%# Eval("PreviousModuleChartsAvailable")%></asp:Label><asp:Literal runat="server" ID="LiteralCharts"></asp:Literal></td>
            <td><asp:TextBox runat="server" ID="TextBoxNumberOfCharts" lang='<%# Eval("ModuleCode")%>' Text='<%# Eval("NumberOfCharts")%>' class="NumberOfCharts" onchange="return MaxCharts(this);" MaxLength="2"></asp:TextBox> Charts</td>
        </tr>
    </ItemTemplate>
    </asp:Repeater>
    </table>--%>
    <%-- Activity End --%>

    <%-- Activity Start --%>
	<table class="table">
    <asp:Repeater ID="RepeaterCategoryOculoplastics" runat="server" OnItemDataBound="RepeaterModule_ItemDataBound">
    <HeaderTemplate>
	<tr>
		<th colspan="2" class="firstwidth">
			Oculoplastics
		</th>
		<th class="midwidth">Prior Activity Data</th>
		<th class="lastwidth">Add New Charts</th>
	</tr>
    </HeaderTemplate>
    <ItemTemplate>
        <asp:HiddenField ID="HiddenFieldModuleID" Value='<%# Eval("ModuleID") %>' runat="server" />
        <asp:HiddenField ID="HiddenFieldPreviousMaxCharts" Value='<%# Eval("PreviousModuleChartsAvailable") %>' runat="server" />
        <tr id="currentRow">
            <td class="icon-cell"><asp:Image ID="Image1" runat="server" ImageUrl="~/common/images/icon-dash.png" /></td>
            <td><span class="text"><a href='javascript:void();' onclick='openwindowj(<%# Eval("ModuleID") %>);' class="example"><%# Eval("ModuleName")%></a> &nbsp; <%# Eval("PQRSEnabled")%></span></td>
            <td><asp:Literal runat="server" ID="LiteralNA"></asp:Literal><asp:TextBox runat="server" ID='TextBoxPreviousChartData' Text='<%# Eval("PreviousChartData")%>' class="NumberOfCharts" onchange="return MaxCharts(this);" MaxLength="2"></asp:TextBox><asp:Literal runat="server" ID="Literalof"></asp:Literal><asp:Label><%# Eval("PreviousModuleChartsAvailable")%></asp:Label><asp:Literal runat="server" ID="LiteralCharts"></asp:Literal></td>
            <td><asp:TextBox runat="server" ID="TextBoxNumberOfCharts" lang='<%# Eval("ModuleCode")%>' Text='<%# Eval("NumberOfCharts")%>' class="NumberOfCharts" onchange="return MaxCharts(this);" MaxLength="2"></asp:TextBox> Charts</td>
        </tr>
    </ItemTemplate>
    </asp:Repeater>
	</table>
    <%-- Activity End --%> 
    <div class="chart_common_container">
                     
    <%-- Activity Start --%>
	<table class="table">
    <asp:Repeater ID="RepeaterCategoryPathologyOncology" runat="server"  OnItemDataBound="RepeaterModule_ItemDataBound">
    <HeaderTemplate>
	<tr>
		<th colspan="2" class="firstwidth">
			Pathology &amp; Oncology
		</th>
		<th class="midwidth">Prior Activity Data</th>
		<th class="lastwidth">Add New Charts</th>
	</tr>
    </HeaderTemplate>
    <ItemTemplate>
        <asp:HiddenField ID="HiddenFieldModuleID" Value='<%# Eval("ModuleID") %>' runat="server" />
        <asp:HiddenField ID="HiddenFieldPreviousMaxCharts" Value='<%# Eval("PreviousModuleChartsAvailable") %>' runat="server" />
        <tr id="currentRow">
            <td class="icon-cell"><asp:Image ID="Image1" runat="server" ImageUrl="~/common/images/icon-dash.png" /></td>
            <td><span class="text" runat="server" id="spanDescription"><a href='javascript:void();' onclick='openwindowj(<%# Eval("ModuleID") %>);' class="example"><%# Eval("ModuleName")%></a> &nbsp; <%# Eval("PQRSEnabled")%></span></td>
            <td><asp:Literal runat="server" ID="LiteralNA"></asp:Literal><asp:TextBox runat="server" ID='TextBoxPreviousChartData' Text='<%# Eval("PreviousChartData")%>' class="NumberOfCharts" onchange="return MaxCharts(this);" MaxLength="2"></asp:TextBox><asp:Literal runat="server" ID="Literalof"></asp:Literal><asp:Label><%# Eval("PreviousModuleChartsAvailable")%></asp:Label><asp:Literal runat="server" ID="LiteralCharts"></asp:Literal></td>
            <td><asp:TextBox runat="server" ID="TextBoxNumberOfCharts" lang='<%# Eval("ModuleCode")%>' Text='<%# Eval("NumberOfCharts")%>' class="NumberOfCharts" onchange="return MaxCharts(this);" MaxLength="2"></asp:TextBox><asp:Literal runat="server" ID="LiteralNumberOfCharts"> Charts</asp:Literal></td>
        </tr>
		<asp:HiddenField ID="HiddenFieldParentModuleID" Value='<%# Eval("ParentModuleID") %>' runat="server" />
		<asp:HiddenField ID="HiddenFieldIsParent" Value='<%# Eval("IsParent") %>' runat="server" />
    </ItemTemplate>
    </asp:Repeater>
	</table>
    <%-- Activity End --%>    
	
    <%-- Activity Start --%>
	<table class="table">
    <asp:Repeater ID="RepeaterCategoryPediatrics" runat="server" OnItemDataBound="RepeaterModule_ItemDataBound">
    <HeaderTemplate>
	<tr>
		<th colspan="2" class="firstwidth">
			Pediatric Ophthalmology
		</th>
		<th class="midwidth">Prior Activity Data</th>
		<th class="lastwidth">Add New Charts</th>
	</tr>
    </HeaderTemplate>
    <ItemTemplate>
        <asp:HiddenField ID="HiddenFieldModuleID" Value='<%# Eval("ModuleID") %>' runat="server" />
        <asp:HiddenField ID="HiddenFieldPreviousMaxCharts" Value='<%# Eval("PreviousModuleChartsAvailable") %>' runat="server" />
        <tr id="currentRow">
            <td class="icon-cell"><asp:Image ID="Image1" runat="server" ImageUrl="~/common/images/icon-dash.png" /></td>
            <td><span class="text"><a href='javascript:void();' onclick='openwindowj(<%# Eval("ModuleID") %>);' class="example"><%# Eval("ModuleName")%></a> &nbsp; <%# Eval("PQRSEnabled")%></span></td>
            <td><asp:Literal runat="server" ID="LiteralNA"></asp:Literal><asp:TextBox runat="server" ID='TextBoxPreviousChartData' Text='<%# Eval("PreviousChartData")%>' class="NumberOfCharts" onchange="return MaxCharts(this);" MaxLength="2"></asp:TextBox><asp:Literal runat="server" ID="Literalof"></asp:Literal><asp:Label><%# Eval("PreviousModuleChartsAvailable")%></asp:Label><asp:Literal runat="server" ID="LiteralCharts"></asp:Literal></td>
            <td><asp:TextBox runat="server" ID="TextBoxNumberOfCharts" lang='<%# Eval("ModuleCode")%>' Text='<%# Eval("NumberOfCharts")%>' class="NumberOfCharts" onchange="return MaxCharts(this);" MaxLength="2"></asp:TextBox> Charts</td>
        </tr>
    </ItemTemplate>
    </asp:Repeater>
	</table>
    <%-- Activity End --%>   
	
    <%-- Activity Start --%>
	<table class="table">
    <asp:Repeater ID="RepeaterCategoryRefractive" runat="server" OnItemDataBound="RepeaterModule_ItemDataBound">
    <HeaderTemplate>
	<tr>
		<th colspan="2" class="firstwidth">
			Refractive
		</th>
		<th class="midwidth">Prior Activity Data</th>
		<th class="lastwidth">Add New Charts</th>
	</tr>    
	</HeaderTemplate>
    <ItemTemplate>
        <asp:HiddenField ID="HiddenFieldModuleID" Value='<%# Eval("ModuleID") %>' runat="server" />
        <asp:HiddenField ID="HiddenFieldPreviousMaxCharts" Value='<%# Eval("PreviousModuleChartsAvailable") %>' runat="server" />
        <tr id="currentRow">
            <td class="icon-cell"><asp:Image ID="Image1" runat="server" ImageUrl="~/common/images/icon-dash.png" /></td>
            <td><span class="text"><a href='javascript:void();' onclick='openwindowj(<%# Eval("ModuleID") %>);' class="example"><%# Eval("ModuleName")%></a> &nbsp; <%# Eval("PQRSEnabled")%></span></td>
            <td><asp:Literal runat="server" ID="LiteralNA"></asp:Literal><asp:TextBox runat="server" ID='TextBoxPreviousChartData' Text='<%# Eval("PreviousChartData")%>' class="NumberOfCharts" onchange="return MaxCharts(this);" MaxLength="2"></asp:TextBox><asp:Literal runat="server" ID="Literalof"></asp:Literal><asp:Label><%# Eval("PreviousModuleChartsAvailable")%></asp:Label><asp:Literal runat="server" ID="LiteralCharts"></asp:Literal></td>
            <td><asp:TextBox runat="server" ID="TextBoxNumberOfCharts" lang='<%# Eval("ModuleCode")%>' Text='<%# Eval("NumberOfCharts")%>' class="NumberOfCharts" onchange="return MaxCharts(this);" MaxLength="2"></asp:TextBox> Charts</td>
        </tr>
    </ItemTemplate>
    </asp:Repeater>
	</table>
    <%-- Activity End --%>  
	
    <%-- Activity Start --%>
	<table class="table">
    <asp:Repeater ID="RepeaterCategoryRetinaVitreous" runat="server" OnItemDataBound="RepeaterModule_ItemDataBound">
    <HeaderTemplate>
	<tr>
		<th colspan="2" class="firstwidth">
			Retina & Vitreous
		</th>
		<th class="midwidth">Prior Activity Data</th>
		<th class="lastwidth">Add New Charts</th>
	</tr>
    </HeaderTemplate>
    <ItemTemplate>
        <asp:HiddenField ID="HiddenFieldModuleID" Value='<%# Eval("ModuleID") %>' runat="server" />
        <asp:HiddenField ID="HiddenFieldPreviousMaxCharts" Value='<%# Eval("PreviousModuleChartsAvailable") %>' runat="server" />
        <tr id="currentRow">
            <td class="icon-cell"><asp:Image ID="Image1" runat="server" ImageUrl="~/common/images/icon-dash.png" /></td>
            <td><span class="text"><a href='javascript:void();' onclick='openwindowj(<%# Eval("ModuleID") %>);' class="example"><%# Eval("ModuleName")%></a> &nbsp; <%# Eval("PQRSEnabled")%></span></td>
            <td><asp:Literal runat="server" ID="LiteralNA"></asp:Literal><asp:TextBox runat="server" ID='TextBoxPreviousChartData' Text='<%# Eval("PreviousChartData")%>' class="NumberOfCharts" onchange="return MaxCharts(this);" MaxLength="2"></asp:TextBox><asp:Literal runat="server" ID="Literalof"></asp:Literal><asp:Label><%# Eval("PreviousModuleChartsAvailable")%></asp:Label><asp:Literal runat="server" ID="LiteralCharts"></asp:Literal></td>
            <td><asp:TextBox runat="server" ID="TextBoxNumberOfCharts" lang='<%# Eval("ModuleCode")%>' Text='<%# Eval("NumberOfCharts")%>' class="NumberOfCharts" onchange="return MaxCharts(this);" MaxLength="2"></asp:TextBox> Charts</td>
        </tr>
    </ItemTemplate>
    </asp:Repeater>
	</table>
    <%-- Activity End --%> 
	
	<%-- Activity Start --%>
    <%-- Both activities for Uveitis are now discontinued. 3/12/18 --%>
	<%--<table class="table">
    <asp:Repeater ID="RepeaterCategoryUveitis" runat="server" OnItemDataBound="RepeaterModule_ItemDataBound">
    <HeaderTemplate>
	<tr>
		<th colspan="2" class="firstwidth">
			Uveitis
		</th>
		<th class="midwidth">Prior Activity Data</th>
		<th class="lastwidth">Add New Charts</th>
	</tr>    
	</HeaderTemplate>
    <ItemTemplate>
        <asp:HiddenField ID="HiddenFieldModuleID" Value='<%# Eval("ModuleID") %>' runat="server" />
        <asp:HiddenField ID="HiddenFieldPreviousMaxCharts" Value='<%# Eval("PreviousModuleChartsAvailable") %>' runat="server" />
        <tr id="currentRow">
            <td class="icon-cell"><asp:Image ID="Image1" runat="server" ImageUrl="~/common/images/icon-dash.png" /></td>
            <td><span class="text"><a href='javascript:void();' onclick='openwindowj(<%# Eval("ModuleID") %>);' class="example"><%# Eval("ModuleName")%></a> &nbsp; <%# Eval("PQRSEnabled")%></span></td>
            <td><asp:Literal runat="server" ID="LiteralNA"></asp:Literal><asp:TextBox runat="server" ID='TextBoxPreviousChartData' Text='<%# Eval("PreviousChartData")%>' class="NumberOfCharts" onchange="return MaxCharts(this);" MaxLength="2"></asp:TextBox><asp:Literal runat="server" ID="Literalof"></asp:Literal><asp:Label><%# Eval("PreviousModuleChartsAvailable")%></asp:Label><asp:Literal runat="server" ID="LiteralCharts"></asp:Literal></td>
            <td><asp:TextBox runat="server" ID="TextBoxNumberOfCharts"  lang='<%# Eval("ModuleCode")%>' Text='<%# Eval("NumberOfCharts")%>' class="NumberOfCharts" onchange="return MaxCharts(this);" MaxLength="2"></asp:TextBox> Charts</td>
        </tr>
    </ItemTemplate>
    </asp:Repeater>
	</table>--%>
    <%-- Activity End --%>    
        
    <div class="button-box">
        <asp:LinkButton ID="LinkButtonBack" runat="server" Text="Back" CssClass="button" />
        <asp:Button ID="ButtonAdditionalSubmit" runat="server" Text="Continue" OnClick="ButtonSubmit_Click" OnClientClick="return checkModules2();" Visible="true" />
<%--        <asp:LinkButton ID="LinkButtonPriorModules" runat="server" Text=" Previous " OnClick="ButtonPriorModules_Click" CssClass="button" />
        <asp:LinkButton ID="ButtonSubmit2" runat="server" Text="Next" OnClick="ButtonSubmit_Click" OnClientClick="return checkModules();" CssClass="button" />
--%>    </div>
    
    <div style="display:none">

           
    </div>
    
    
    <div id="dialog" title="PQRS Activity Selection Conflict">
        POAG, POAGS, CSME and AMD are PQRS-enabled activities that cannot be selected with the Cataract Surgery PQRS-enabled activity.  To proceed with PQRS-enabled activities, you must de-select either the Cataract Surgery activity or the POAG, POAGS, CSME, and AMD activities.  
        <br />
        <br /><span style="color:Red; font-weight:bold;">OR</span>
        <br />
        <br />Of the POAG/POAGS, CSME and AMD activities, only CSME can be chosen by itself.  POAG/POAGS or AMD must be paired with each other or with CSME.
    </div>
    </div> 
     <script>
         $("#dialog").dialog({ autoOpen: false });
    </script>
<%--                  <div class="submit_brn_area">
        <asp:UpdatePanel runat="server" ID="UpdatePanel3" RenderMode="Inline"><ContentTemplate>
        <asp:Button runat="server" class="save" id="ButtonSubmitold" OnClick="ButtonSubmit_Click" OnClientClick="return checkModules();"/>
        </ContentTemplate></asp:UpdatePanel>
    </div>
--%>            </form>

        <!-- chart registration ends -->

        
</asp:Content>

<asp:Content runat="server" ID="Content3" ContentPlaceHolderID="javascript">
    <script type="text/javascript" language="javascript">
        // Floating box javascript
        var prog = $('.progress-panel');
        var progEdge = $('.progress-panel').offset().top;
        var innerWidth = $('#widthForFloating');
        var rightEdge = $(window).width() - (innerWidth.offset().left + innerWidth.outerWidth());
        $(window).scroll(function () {
            var edge = $(window).scrollTop();
            rightEdge = $(window).width() - (innerWidth.offset().left + innerWidth.outerWidth());
            if ($(window).width() < innerWidth.outerWidth()) {
                rightEdge = 0;
            }

            if (progEdge <= edge) {
                prog.addClass('floating');
                $('.floating').css('right', rightEdge)
            } else {
                prog.removeClass('floating')
            }
        });
        $(window).resize(function () {
            var rightEdge = $(window).width() - (innerWidth.offset().left + innerWidth.outerWidth());
            if ($(window).width() < innerWidth.outerWidth()) {
                rightEdge = 0;
            }
            $('.floating').css("right", rightEdge);
        });
    </script>


  <script src="http://code.jquery.com/ui/1.10.3/jquery-ui.js"></script>
    <script type="text/javascript" language="javascript">

        $(document).ready(function () {
            $("#moduleOverview").dialog({
                autoOpen: false,
                modal: true,
                height: 550,
                width: 800
            });
        });

        function openwindowj(moduleid) {
            $("#iframeModal").removeAttr("src");
            $("#iframeModal").attr("src", "ModuleDefinition.aspx?mid=" + moduleid);
            $("#moduleOverview").dialog("open");
        }


        var MIN_NUMBER_OF_CHARTS = 1;
        var MAX_NUMBER_OF_CHARTS = 1;
        var MAX_PATHOLOGY_NUMBER_OF_CHARTS = 40;
        var PATHOLOGY_SELECTED = false;
        var NUMBER_PATHOLOGY_SELECTED = 0;
        var currentTextBox = null;


        function checkModules() {
            var ret = true;
            var HiddenFieldModulesCurrentlySelected = document.getElementById("<%= HiddenFieldModulesCurrentlySelected.ClientID %>").value;
            var HiddenFieldModulesSelected = 0;
            var DataEnteredCount = document.getElementById("<%= HiddenFieldDataEntered.ClientID %>").value;
            var HiddenFieldModuleSelectionConfirmed = document.getElementById("<%= HiddenFieldModuleSelectionConfirmed.ClientID %>").value;

            if (HiddenFieldModulesSelected > 0) {

                if ((HiddenFieldModulesCurrentlySelected != HiddenFieldModulesSelected) &&
            (HiddenFieldModulesCurrentlySelected > 0) && (HiddenFieldModuleSelectionConfirmed == "1")) {
                    if (confirm("Are you sure you want to change the number of activities? By changing the number of activities selected from " + HiddenFieldModulesCurrentlySelected + " to " + HiddenFieldModulesSelected + ", some chart data may get lost.")) {
                        document.getElementById("<%= HiddenFieldDeleteCurrentData.ClientID %>").value = "1";
                    }
                    else
                        ret = false;
                }
                else {
                    if ((DataEnteredCount > 0) && (HiddenFieldModuleSelectionConfirmed == "1")) {
                        if (confirm("Are you sure you want to change activities? By changing activities, some chart data entered may get lost.")) {
                            document.getElementById("<%= HiddenFieldDeleteCurrentData.ClientID %>").value = "1";
                        }
                        else
                            ret = false;
                    }
                }
            }
            else {
                alert("You must select at least one activity to continue");
                ret = false;
            }
            return ret;
        }
        function checkPQRS() {
            var cataract;
            var diabRet;
            var POAG;
            var POAGS;
            var AMD;

            cataract = (parseInt($("input[lang='CATARACT']").val()) > 0);
            diabRet = (parseInt($("input[lang='CSME']").val()) > 0);
            POAG = (parseInt($("input[lang='POAG']").val()) > 0);
            POAGS = (parseInt($("input[lang='POAGS']").val()) > 0);
            AMD = (parseInt($("input[lang='AMD']").val()) > 0);

            if (cataract && !(AMD || POAG || POAGS || diabRet)) {
                return true;
            }

            else if (diabRet && !cataract) {
                return true;
            }

            else if (POAG && (AMD || diabRet || POAGS) && !cataract) {
                return true;
            }

            else if (POAGS && (AMD || diabRet) && !cataract) {
                return true;
            }

            else if (AMD && diabRet && !cataract) {
                return true;
            }

            else {
                return false;
            }
        }

        function verifyMinimumChartsPerModule() {
            var ret = true;
         
            $('.NumberOfCharts').each(function () {
                var currentValue = this.value;
                var ctrlName = this.id;
                var newControlName = ctrlName.replace("TextBoxPreviousChartData", "TextBoxNumberOfCharts");
                var textBoxNumberOfCharts = "";
                if (this.id.indexOf("TextBoxPreviousChartData") != -1) // if previous activity has been found, then the sum of previous + new should be greater than 10  
                {
                    textBoxNumberOfCharts = document.getElementById(newControlName).value.toString();
                    if ((((parseInt(currentValue) + parseInt(textBoxNumberOfCharts)) < 1) && ((parseInt(currentValue) + parseInt(textBoxNumberOfCharts)) > 0))
                 || (parseInt(currentValue) < 0) || (parseInt(textBoxNumberOfCharts) < 0)) {
                        alert("Minimum number of charts for any activity is " + MIN_NUMBER_OF_CHARTS.toString() + ". Please enter the correct number of activities and try again.");
                        this.focus();
                        ret = false;
                        return false;
                        //break;
                    }

                }
                else {
                    
                    textBoxNumberOfCharts = document.getElementById(newControlName).value.toString();
                    var newControlName = ctrlName.replace("TextBoxNumberOfCharts", "TextBoxPreviousChartData");
                    if (document.getElementById(newControlName) != null) {
                    }
                    else
                        //if ((parseInt(textBoxNumberOfCharts) < MIN_NUMBER_OF_CHARTS) && (parseInt(currentValue) > 0)) {
                        if (parseInt(textBoxNumberOfCharts) > 0) {
                            if ((parseInt(textBoxNumberOfCharts) < 1)) {
                                alert("Minimum number of charts for any activity is " + MIN_NUMBER_OF_CHARTS.toString() + ". Please enter the correct number of activities and try again.");
                                this.focus();
                                ret = false;
                                return false;
                            }
                        }
                }
                if (parseInt(currentValue) < 0) {
                    alert("You can only use positive numbers. Please enter the correct number and try again.");
                    this.focus();
                    ret = false;
                    return false;
                }
            });
            return ret;
        }

        function checkModules2() {
            var ret = true;

            ret = verifyMinimumChartsPerModule();
            if (ret) {
                HiddenFieldCurrentSelectedModules = document.getElementById("<%= HiddenFieldCurrentSelectedModules.ClientID %>").value;
                HiddenFieldPlanCreated = document.getElementById("<%= HiddenFieldPlanCreated.ClientID %>").value;
                HiddenFieldOverwriteCharts = document.getElementById("<%= HiddenFieldOverwriteCharts.ClientID %>");

                if (HiddenFieldPlanCreated == "1") { // Plan Created/Completed? Check if any of the activities have changed

                    if (HiddenFieldCurrentSelectedModules == getCurrentSelectedModules()) // no changes made to selection?
                    {
                        document.getElementById("<%= HiddenFieldModuleSelectionChanged.ClientID %>").value = "0";
                    }
                    else {
                        if (confirm("Are you sure you want to change activities? By changing activities, some chart data entered may get lost.")) {
                            document.getElementById("<%= HiddenFieldDeleteCurrentData.ClientID %>").value = "1";
                            HiddenFieldOverwriteCharts.Value = "1";
                        }
                        else
                            return false;
                    }
                }
            }
            return ret;
        }

        function getCurrentSelectedModules() {
            var currentList = "";

            $('.NumberOfCharts').each(function () {
                var currentValue = this.value;
                currentList = currentList + currentValue;
            });
            return currentList;
        }

        function setNumberOfChartsSelected() {
            var numberOfCharts = 0;
            var numberOfModules = 0;
            NUMBER_PATHOLOGY_SELECTED = 0;
            document.getElementById("NumberOfModules").innerHTML = numberOfModules.toString();
            $('.NumberOfCharts').each(function () {
                var currentValue = this.value;
                if (currentValue == Number(currentValue))
                    numberOfCharts = numberOfCharts + parseInt(Number(currentValue));
                if (currentValue > 0) {
                    $(this).parent().parent()[0].className = "active";
                    var imgValue = ($(this).parent().parent()[0].children[0].innerHTML.toString()).replace("icon-dash.png", "icon-check.png");
                    $(this).parent().parent()[0].children[0].innerHTML = imgValue;
                    numberOfModules++;
                    // if current control is a New Chart Data, then check for previous data, if previous data exists then activity should only b counted ONCE
                    if (this.id.indexOf("TextBoxNumberOfCharts") != -1) {
                        if ($(this).parent().parent()[0].children[2].children[0].value > 0) {
                            numberOfModules--;
                        }
                    }
                    if (($(this).parent().parent()[0].children[0].innerHTML.toString()).indexOf("Pathology") > 0) {
                        var currentModule = $(this).parent().parent()[0].children[1].innerHTML.toString();
                        if (currentModule.indexOf("openwindowj(62)") > 0 ||
                            currentModule.indexOf("openwindowj(63)") > 0 ||
                            currentModule.indexOf("openwindowj(64)") > 0 ||
                            currentModule.indexOf("openwindowj(65)") > 0)
                            NUMBER_PATHOLOGY_SELECTED++;
                    }
                    document.getElementById("NumberOfModules").innerHTML = numberOfModules.toString();
                }
                else {

                    if (this.id.indexOf("TextBoxNumberOfCharts") != -1) // if found, then check if previous data has a value
                    {
                        if ($(this).parent().parent()[0].children[2].children[0].value > 0) {
                            $(this).parent().parent()[0].className = "active";
                            var imgValue = ($(this).parent().parent()[0].children[0].innerHTML.toString()).replace("icon-dash.png", "icon-check.png");
                            $(this).parent().parent()[0].children[0].innerHTML = imgValue;
                        }
                        else {
                            $(this).parent().parent()[0].className = "";
                            var imgValue = ($(this).parent().parent()[0].children[0].innerHTML.toString()).replace("icon-check.png", "icon-dash.png");
                            $(this).parent().parent()[0].children[0].innerHTML = imgValue;
                        }
                    }
                    else {
                        $(this).parent().parent()[0].className = "";
                        var imgValue = ($(this).parent().parent()[0].children[0].innerHTML.toString()).replace("icon-check.png", "icon-dash.png");
                        $(this).parent().parent()[0].children[0].innerHTML = imgValue;
                    }
                }

                var ctrlName = this.id;
                var newControlName = ctrlName.replace("TextBoxPreviousChartData", "HiddenFieldPreviousMaxCharts");
                var pathologyModule = ctrlName.replace("TextBoxPreviousChartData", "HiddenFieldParentModuleID");

                if (pathologyModule.indexOf("HiddenFieldParentModuleID") == -1) {
                    pathologyModule = ctrlName.replace("TextBoxNumberOfCharts", "HiddenFieldParentModuleID");
                }

                // Pathology activity found?
                if (pathologyModule.indexOf("HiddenFieldParentModuleID") != -1) {
                    if (document.getElementById(pathologyModule) != null) {
                        var pathologyModuleValue = document.getElementById(pathologyModule).value.toString();
                        if ((pathologyModuleValue > 0) && (currentValue > 0)) {
                            PATHOLOGY_SELECTED = true;
                        }
                    }
                }

            });
            //document.getElementById("<%= LabelChartSelected.ClientID %>").innerText = numberOfCharts.toString();
            $("#<%= LabelChartSelected.ClientID %>").html(numberOfCharts.toString());
            var submitButton = document.getElementById("<%= ButtonSubmit.ClientID %>");
            var submitButtonAdditional = document.getElementById("<%= ButtonAdditionalSubmit.ClientID %>");

            if ((numberOfCharts >= MAX_NUMBER_OF_CHARTS) && (numberOfModules <= 3)) {
                submitButton.style.display = "block";
                submitButton.style.visibility = 'visible';
                submitButtonAdditional.style.visibility = 'visible';
                $("#<%= LabelAllChartsSelected.ClientID %>").html("All Charts Selected");
            }
            else {
                if (((PATHOLOGY_SELECTED) && (numberOfCharts >= MAX_PATHOLOGY_NUMBER_OF_CHARTS) && (numberOfModules <= 4))
                    || ((NUMBER_PATHOLOGY_SELECTED == 4) && (numberOfModules <= 5))) {
                    submitButton.style.visibility = 'visible';
                    submitButtonAdditional.style.visibility = 'visible';
                    $("#<%= LabelAllChartsSelected.ClientID %>").html("All Charts Selected");
                }
                else {
                    submitButton.style.display = "none";
                    submitButton.style.visibility = 'hidden';
                    submitButtonAdditional.style.visibility = 'hidden';
                    $("#<%= LabelAllChartsSelected.ClientID %>").html("You will be able to continue once you've selected charts for 1 to 3 activities");
                }
            }
            if (currentTextBox != null)
                currentTextBox.focus();

        }

        $(document).ready(function () {
            PATHOLOGY_SELECTED = false;
            document.getElementById("<%= HiddenFieldCurrentSelectedModules.ClientID %>").value = getCurrentSelectedModules();
            setNumberOfChartsSelected();

            //$(".NumberOfCharts").change(function () {
            $(".NumberOfCharts").keyup(function () {

                setNumberOfChartsSelected();
                //alert(numberOfCharts);
            })


            $("#<%= ButtonSubmit.ClientID %>").click(function (event) {
                var isPQRS = false;
                $('.pqrs').each(function (index) {
                    isPQRS = true;
                });
                if (isPQRS) {
                    if (!checkPQRS()) {
                        //alert("POAG, POAGS, CSME and AMD are PQRS-enabled activities that are incompatible with the Cataract Surgery PQRS-enabled activity.  To proceed with PQRS-enabled activities, you must de-select either the Cataract Surgery activity or the POAG, POAGS, CSME, and AMD activities.");
                        event.preventDefault();
                        $("#dialog").dialog("open");
                    }
                }
            });
            $("#<%= ButtonAdditionalSubmit.ClientID %>").click(function (event) {
                var isPQRS = false;
                $('.pqrs').each(function (index) {
                    isPQRS = true;
                });
                if (isPQRS) {
                    if (!checkPQRS()) {
                        //alert("POAG, POAGS, CSME and AMD are PQRS-enabled activities that are incompatible with the Cataract Surgery PQRS-enabled activity.  To proceed with PQRS-enabled activities, you must de-select either the Cataract Surgery activity or the POAG, POAGS, CSME, and AMD activities.");
                        event.preventDefault();
                        $("#dialog").dialog("open");
                    }
                }
            });
            var HiddenFieldPhatologySelected = document.getElementById("<%= HiddenFieldPhatologySelected.ClientID %>").value;
            if (HiddenFieldPhatologySelected == "0") {
                //$("div[id=rowModule61]").hide();
            }
            else {
            }
        })
        function MaxCharts(_self) {
            //alert(_self.value);
            var ctrlName = _self.id.toString();
            PATHOLOGY_SELECTED = false;

            var newControlName = ctrlName.replace("TextBoxPreviousChartData", "HiddenFieldPreviousMaxCharts");


            var myfocus = document.getElementById(_self.id);

            var mycontrolName = document.getElementById(newControlName).value.toString();

            var currentValue = _self.value;
            if (currentValue == Number(currentValue)) {
                if (parseInt(Number(currentValue)) > parseInt(Number(mycontrolName))) {
                    alert("Maximum number of previous charts for this activity is " + parseInt(Number(mycontrolName)).toString());
                    _self.value = parseInt(Number(mycontrolName)).toString();
                    //myfocus.focus();
                    self.focus();
                    currentTextBox = myfocus;
                    return false;
                }

            }
            else {
                alert("Please enter a valid number");
                _self.value = "0";
                self.focus();
                currentTextBox = myfocus;
                return false;
            }
            if ($.trim(currentValue) == "")
                _self.value = "0";


        }

        function getAll(_self) {
            $("div[id=rowModule61]").css("visibility", "visible");

            if (!_self.checked)
                $("div[id=rowModule61]").hide();
            else
                $("div[id=rowModule61]").show();


        }


</script>


<script>
    $("#ContentPlaceHolder1_RepeaterCategoryPathologyOncology_CheckBoxModulePathologyOncology_2").click(function () {
        if (!$("#ContentPlaceHolder1_RepeaterCategoryPathologyOncology_CheckBoxModulePathologyOncology_2").prop("checked")) {
            $("#ContentPlaceHolder1_RepeaterCategoryPathologyOncology_CheckBoxModulePathologyOncology_3").removeAttr("checked");
        }
    });
    $("#ContentPlaceHolder1_RepeaterCategoryPathologyOncology_CheckBoxModulePathologyOncology_2").click(function () {
        if (!$("#ContentPlaceHolder1_RepeaterCategoryPathologyOncology_CheckBoxModulePathologyOncology_2").prop("checked")) {
            $("#ContentPlaceHolder1_RepeaterCategoryPathologyOncology_CheckBoxModulePathologyOncology_4").removeAttr("checked");
        }
    });
    $("#ContentPlaceHolder1_RepeaterCategoryPathologyOncology_CheckBoxModulePathologyOncology_2").click(function () {
        if (!$("#ContentPlaceHolder1_RepeaterCategoryPathologyOncology_CheckBoxModulePathologyOncology_2").prop("checked")) {
            $("#ContentPlaceHolder1_RepeaterCategoryPathologyOncology_CheckBoxModulePathologyOncology_5").removeAttr("checked");
        }
    });

    $("#ContentPlaceHolder1_RepeaterCategoryPathologyOncology_CheckBoxModulePathologyOncology_2").click(function () {
        if (!$("#ContentPlaceHolder1_RepeaterCategoryPathologyOncology_CheckBoxModulePathologyOncology_2").prop("checked")) {
            $("#ContentPlaceHolder1_RepeaterCategoryPathologyOncology_CheckBoxModulePathologyOncology_6").removeAttr("checked");
        }
    });
</script>

</asp:Content>