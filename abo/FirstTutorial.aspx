﻿<%@ Page Title="ABO PIP Tutorial" Language="C#" MasterPageFile="~/PIMMasterPage.master" AutoEventWireup="true" CodeFile="FirstTutorial.aspx.cs" Inherits="abo_PIMTutorial" %>

<asp:Content ID="Content1" ContentPlaceHolderID="head" Runat="Server">
<style type="text/css">
.tg  {border-collapse:collapse;border-spacing:0;}
.tg td{font-family:Arial, sans-serif;font-size:14px;padding:10px 5px;border-style:solid;border-width:1px;overflow:hidden;word-break:normal;}
.tg th{font-family:Arial, sans-serif;font-size:14px;font-weight:normal;padding:10px 5px;border-style:solid;border-width:1px;overflow:hidden;word-break:normal;}
.tg .tg-74fe{background-color:#fbd4b4;vertical-align:top; font-weight:bold;}
.tg .tg-9hbo{font-weight:bold;vertical-align:top}
.tg .tg-yw4l{vertical-align:top}
.tg .tg-v5b3{background-color:#b6dde8;vertical-align:top; font-weight:bold;}
.data-table {
    width: 100%;
    margin: 0 0 20px;
    border-right: 1px solid #eee;
}
.data-table thead td {
    color: #fff;
    font-weight: bold;
    background: #565656;
    vertical-align: top;
    border-bottom: none;
    border-left: 1px solid #565656;
}
.data-table td {
    padding: 6px 8px;
    vertical-align: top;
    border-bottom: 1px solid #eee;
    border-left: 1px solid #eee;
}
</style>
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder1" Runat="Server">

<!-- First way of displaying data.  New way below.  JH 4/29/2016 -->
<div style="display:none;">
<p>Please review the different activities, the activity definitions and the time required to complete the activity. You should select activities that best meet your needs.
<br /><br />
<strong>Improvement in Medical Practice Activities:</strong>
<br />These improvement in medical practice activities include 20-30 data fields related to the care and management of the diagnoses, conditions, and surgical procedures covered in the activity. The information that you will provide for each activity includes general information about the patient population and the patient’s history and information related to the processes of care and outcomes.
<br /><br />
<strong>Available in May 2016: PQRS Enabled Improvement in Medical Practice Activities:</strong>
<br />These improvement in medical practice activities are modified versions of the simplified Improvement in Medical Practice Activities and provide the ability to participate in the Centers for Medicare and Medicaid Services (CMS)'s PQRS reporting.
</p>

<table class="tg">
  <tr>
    <th class="tg-9hbo" colspan="6">Standard Improvement in Medical Practice Activities<br></th>
  </tr>
  <tr>
    <td class="tg-v5b3">Cataract &amp; Anterior Segment Activities<br></td>
    <td class="tg-v5b3">Minimum Time To Complete (Days)<br></td>
    <td class="tg-v5b3">Neuro-Ophthalmology<br></td>
    <td class="tg-v5b3">Minimum Time To Complete (Days)<br></td>
    <td class="tg-v5b3">Refractive<br></td>
    <td class="tg-v5b3">Minimum Time To Complete (Days)</td>
  </tr>
  <tr>
    <%--<td class="tg-yw4l">Cataract (Surgical Management)</td>--%>
    <td class="tg-yw4l"><a href="http://abop.org/media/56326/cataract_surgery.pdf" target="_blank">Cataract (Surgical Management)</a></td>
    <td class="tg-yw4l">120</td>
    <%--<td class="tg-yw4l">Optic Neuritis</td>--%>
    <td class="tg-yw4l"><a href="http://abop.org/media/56362/optic_neuritis.pdf" target="_blank">Optic Neuritis</a></td>
    <td class="tg-yw4l">120</td>
    <%--<td class="tg-yw4l">Lasik for Myopia</td>--%>
    <td class="tg-yw4l"><a href="http://abop.org/media/56359/lasik_for_myopia.pdf" target="_blank">Lasik for Myopia</a></td>
    <td class="tg-yw4l">120</td>
  </tr>
  <tr>
    <%--<td class="tg-yw4l">YAG Laser Capsulotomy</td>--%>
      <td class="tg-yw4l"><a href="http://abop.org/media/56404/yag.pdf" target="_blank">YAG Laser Capsulotomy</a></td>
    <td class="tg-yw4l">120</td>
    <%--<td class="tg-yw4l">Sixth Nerve Palsy<br></td>--%>
      <td class="tg-yw4l"><a href="http://abop.org/media/56395/sixth_nerve_palsy.pdf" target="_blank">Sixth Nerve Palsy</a><br></td>
    <td class="tg-yw4l">120</td>
    <%--<td class="tg-yw4l">Toric IOL<br></td>--%>
      <td class="tg-yw4l"><a href="http://abop.org/media/56401/toric.pdf" target="_blank">Toric IOL</a><br></td>
    <td class="tg-yw4l">120</td>
  </tr>
  <tr>
    <td class="tg-v5b3">Cornea External Disease</td>
    <td class="tg-v5b3">Minimum Time To Complete (Days)<br></td>
    <td class="tg-v5b3">Oculoplastics</td>
    <td class="tg-v5b3">Minimum Time To Complete (Days)<br></td>
    <td class="tg-v5b3">Pediatric Ophthalmology<br></td>
    <td class="tg-v5b3">Minimum Time To Complete (Days)<br></td>
  </tr>
  <tr>
    <%--<td class="tg-yw4l">Corneal Edema<br></td>--%>
      <td class="tg-yw4l"><a href="http://abop.org/media/56335/corneal_edema.pdf" target="_blank">Corneal Edema</a><br></td>
    <td class="tg-yw4l">395</td>
    <%--<td class="tg-yw4l">Eyelid Malignancy<br></td>--%>
      <td class="tg-yw4l"><a href="http://abop.org/media/56353/eyelid_malignancy.pdf" target="_blank">Eyelid Malignancy</a><br></td>
    <td class="tg-yw4l">120</td>
    <%--<td class="tg-yw4l">Amblyopia</td>--%>
      <td class="tg-yw4l"><a href="http://abop.org/media/56323/amblyopia.pdf" target="_blank">Amblyopia</a></td>
    <td class="tg-yw4l">212</td>
  </tr>
  <tr>
    <%--<td class="tg-yw4l">Corneal Ulcer/Bacterial (Infectious) Keratitis<br></td>--%>
      <td class="tg-yw4l" rowspan="2"><a href="http://abop.org/media/56356/infectious_keratitis.pdf" target="_blank">Corneal Ulcer/Bacterial (Infectious) Keratitis</a><br></td>
    <td class="tg-yw4l" rowspan="2">395</td>
    <%--<td class="tg-yw4l">Ptosis (Observational Management)<br></td>--%>
      <td class="tg-yw4l"><a href="http://abop.org/media/56386/ptosis.pdf" target="_blank">Ptosis (Observational Management)</a><br></td>
    <td class="tg-yw4l">120</td>
    <%--<td class="tg-yw4l">Esotropia</td>--%>
      <td class="tg-yw4l" rowspan="2"><a href="http://abop.org/media/56347/esotropia.pdf" target="_blank">Esotropia</a></td>
    <td class="tg-yw4l" rowspan="2">270</td>
  </tr>
  <tr>
    <%--<td class="tg-yw4l">Thyroid Ophthalmopathy<br></td>--%>
    <td class="tg-yw4l"><a href="http://abop.org/media/56398/thyroid_ophthalmopathy.pdf" target="_blank">Thyroid Ophthalmopathy</a><br></td>
    <td class="tg-yw4l">212</td>
  </tr>
  <tr>
    <td class="tg-v5b3">Glaucoma</td>
    <td class="tg-v5b3">Minimum Time To Complete (Days)<br></td>
    <td class="tg-v5b3">Pathology &amp; Oncology</td>
    <td class="tg-v5b3">Minimum Time To Complete (Days)<br></td>
    <td class="tg-v5b3">Retina<br></td>
    <td class="tg-v5b3">Minimum Time To Complete (Days)<br></td>
  </tr>
  <tr>
    <%--<td class="tg-yw4l">Primary Open Angle Glaucoma</td>--%>
    <td class="tg-yw4l"><a href="http://abop.org/media/56380/POAG.pdf" target="_blank">Primary Open Angle Glaucoma</a></td>
    <td class="tg-yw4l">120</td>
    <td class="tg-yw4l"></td>
    <td class="tg-yw4l"></td>
    <%--<td class="tg-yw4l">Diabetic Retinopathy</td>--%>
    <td class="tg-yw4l"><a href="http://abop.org/media/56341/diabetic_retinopathy.pdf" target="_blank">Diabetic Retinopathy</a></td>
    <td class="tg-yw4l">395</td>
  </tr>
  <tr>
    <%--<td class="tg-yw4l">Primary Angle Closure Glaucoma</td>--%>
      <td class="tg-yw4l"><a href="http://abop.org/media/56365/pacg.pdf" target="_blank">Primary Angle Closure Glaucoma</a></td>
    <td class="tg-yw4l">120</td>
    <%--<td class="tg-yw4l">Retinoblastoma</td>--%>
      <td class="tg-yw4l"><a href="http://abop.org/media/56392/retinoblastoma.pdf" target="_blank">Retinoblastoma</a></td>
    <td class="tg-yw4l">395</td>
    <%--<td class="tg-yw4l">Central Retinal Vein Occlusion (CRVO)</td>--%>
      <td class="tg-yw4l"><a href="http://abop.org/media/56338/crvo.pdf" target="_blank">Central Retinal Vein Occlusion (CRVO)</a></td>
    <td class="tg-yw4l">395</td>
  </tr>
  <tr>
    <%--<td class="tg-yw4l">Primary Open Angle Glaucoma Suspect</td>--%>
      <td class="tg-yw4l"><a href="http://abop.org/media/56383/POAGS.pdf" target="_blank">Primary Open Angle Glaucoma Suspect</a></td>
    <td class="tg-yw4l">120</td>
    <%--<td class="tg-yw4l">Choroidal Melanoma</td>--%>
      <td class="tg-yw4l"><a href="http://abop.org/media/56332/choroidal_melanoma.pdf" target="_blank">Choroidal Melanoma</a></td>
    <td class="tg-yw4l">395</td>
    <%--<td class="tg-yw4l">Exudative AMD</td>--%>
      <td class="tg-yw4l"><a href="http://abop.org/media/56350/exudative_amd.pdf" target="_blank">Exudative AMD</a></td>
    <td class="tg-yw4l">395</td>
  </tr>
  <tr>
    <th class="tg-9hbo" colspan="6">Simplified Improvement in Medical Practice Activities<br></th>
  </tr>
  <tr>
    <td class="tg-74fe">Cataract &amp; Anterior Segment Activities</td>
    <td class="tg-74fe">Minimum Time To Complete (Days)<br></td>
    <td class="tg-74fe">Pediatric Ophthalmology</td>
    <td class="tg-74fe">Minimum Time To Complete (Days)<br></td>
    <td class="tg-74fe">Retina</td>
    <td class="tg-74fe">Minimum Time To Complete (Days)<br></td>
  </tr>
  <tr>
    <td class="tg-yw4l">Cataract (Surgical Management)</td>
      <%--<td class="tg-yw4l"><a href="http://abop.org/media/56326/cataract_surgery.pdf" target="_blank">Cataract (Surgical Management)</a></td>--%>
    <td class="tg-yw4l">50</td>
    <td class="tg-yw4l">Amblyopia</td>
      <%--<td class="tg-yw4l"><a href="http://abop.org/media/56323/amblyopia.pdf" target="_blank">Amblyopia</a></td>--%>
    <td class="tg-yw4l">180</td>
    <td class="tg-yw4l">Diabetic Retinopathy</td>
      <%--<td class="tg-yw4l"><a href="http://abop.org/media/56341/diabetic_retinopathy.pdf" target="_blank">Diabetic Retinopathy</a></td>--%>
    <td class="tg-yw4l">212</td>
  </tr>
  <tr>
    <td class="tg-yw4l">YAG Laser Capsulotomy</td>
      <%--<td class="tg-yw4l"><a href="http://abop.org/media/56404/yag.pdf" target="_blank">YAG Laser Capsulotomy</a></td>--%>
    <td class="tg-yw4l">30</td>
    <td class="tg-yw4l">Esotropia</td>
      <%--<td class="tg-yw4l"><a href="http://abop.org/media/56347/esotropia.pdf" target="_blank">Esotropia</a></td>--%>
    <td class="tg-yw4l">120</td>
    <td class="tg-yw4l">Central Retinal Vein Occlusion (CRVO)</td>
      <%--<td class="tg-yw4l"><a href="http://abop.org/media/56338/crvo.pdf" target="_blank">Central Retinal Vein Occlusion (CRVO)</a></td>--%>
    <td class="tg-yw4l">212</td>
  </tr>
  <tr>
    <td class="tg-yw4l"></td>
    <td class="tg-yw4l"></td>
    <td class="tg-yw4l"></td>
    <td class="tg-yw4l"></td>
    <td class="tg-yw4l">Exudative AMD</td>
      <%--<td class="tg-yw4l"><a href="http://abop.org/media/56350/exudative_amd.pdf" target="_blank">Exudative AMD</a></td>--%>
    <td class="tg-yw4l">180</td>
  </tr>
  <tr>
    <td class="tg-74fe">Glaucoma</td>
    <td class="tg-74fe">Minimum Time To Complete (Days)<br></td>
    <td class="tg-74fe">Neuro-Ophthalmology</td>
    <td class="tg-74fe">Minimum Time To Complete (Days)<br></td>
    <td class="tg-74fe">Oculoplastics</td>
    <td class="tg-74fe">Minimum Time To Complete (Days)<br></td>
  </tr>
  <tr>
    <td class="tg-yw4l">Primary Open Angle Glaucoma</td>
      <%--<td class="tg-yw4l"><a href="http://abop.org/media/56380/POAG.pdf" target="_blank">Primary Open Angle Glaucoma</a></td>--%>
    <td class="tg-yw4l">120</td>
    <td class="tg-yw4l">Sixth Nerve Palsy</td>
      <%--<td class="tg-yw4l"><a href="http://abop.org/media/56395/sixth_nerve_palsy.pdf" target="_blank">Sixth Nerve Palsy</a></td>--%>
    <td class="tg-yw4l">120</td>
    <td class="tg-yw4l">Eyelid Malignancy</td>
      <%--<td class="tg-yw4l"><a href="http://abop.org/media/56353/eyelid_malignancy.pdf" target="_blank">Eyelid Malignancy</a></td>--%>
    <td class="tg-yw4l">120</td>
  </tr>
  <tr>
    <td class="tg-yw4l">Primary Angle Closure Glaucoma</td>
      <%--<td class="tg-yw4l"><a href="http://abop.org/media/56365/pacg.pdf" target="_blank">Primary Angle Closure Glaucoma</a></td>--%>
    <td class="tg-yw4l">120</td>
    <td class="tg-yw4l"></td>
    <td class="tg-yw4l"></td>
    <td class="tg-yw4l">Ptosis (Observational Management)</td>
      <%--<td class="tg-yw4l"><a href="http://abop.org/media/56386/ptosis.pdf" target="_blank">Ptosis (Observational Management)</a></td>--%>
    <td class="tg-yw4l">120</td>
  </tr>
  <tr>
    <td class="tg-74fe">General</td>
    <td class="tg-74fe">Minimum Time To Complete (Days)<br></td>
    <td class="tg-yw4l"></td>
    <td class="tg-yw4l"></td>
    <td class="tg-yw4l"></td>
    <td class="tg-yw4l"></td>
  </tr>
  <tr>
    <td class="tg-yw4l">Dry Eye</td>
    <td class="tg-yw4l">120</td>
    <td class="tg-yw4l"></td>
    <td class="tg-yw4l"></td>
    <td class="tg-yw4l"></td>
    <td class="tg-yw4l"></td>
  </tr>
  <tr>
    <th class="tg-9hbo" colspan="6">PQRS Enabled Improvement in Medical Practice Activities (TBD)<br></th>
  </tr>
</table>

<p><strong>How to select the right activity for you:</strong>
<br /><br />
Ophthalmologists practice in diverse settings and subspecialty areas. This menu of ABO Improvement in Medical Practice Activities has been designed to address some of the most common areas of ophthalmic practice focus. Your choice of activity(s) should closely align with the focus of your daily practice. For example, if you are a pediatric ophthalmologist, you will find the amblyopia and esotropia activities (designed by practicing pediatric ophthalmologists) to be more relevant and helpful for improving your practice than the general ophthalmology activity in dry eye. 
<br /><br />
<strong>It is recommended that you carefully read the activity descriptions and patient parameters before committing to a particular activity.</strong>
<br /><br />
Should you find yourself unable to meet any Improvement in Medical Practice Activity criteria, you are encouraged to develop a Self-Directed Improvement in Medical Practice Project. More information on the self-directed improvement in medical practice project can be found <a href="http://abop.org/maintain-certification/part-4-improvement-in-medical-practice/self-directed-qi-program/" target="_blank">here.</a>
</p>
</div>
         
<div class="body-text"><!--<p>All available Improvement in Medical Practice Activity are listed in table below. Click the blue link to view a printable PDF of the complete activity. The middle column provides a brief description of the patient definition for each activity and the final column provides the associated Minimum Improvement Period so that you can select the activity or activities that most closely match your practice profile AND meet your MOC timeframe requirements.</p>-->
<h3>How to select the right activity for you:</h3>
<p>Ophthalmologists practice in diverse settings and subspecialty areas. This menu of ABO Improvement in Medical Practice Activities has been designed to address some of the most common areas of ophthalmic practice focus. Your choice of activity(s) should closely align with the focus of your daily practice. For example, if you are a pediatric ophthalmologist, you will find the amblyopia and esotropia activities (designed by practicing pediatric ophthalmologists) to be more relevant and helpful for improving your practice than the general ophthalmology activity in dry eye.</p>
<p>These improvement in medical practice activities include data fields related to the care and management of the diagnoses, conditions, and surgical procedures covered in the activity. The information that you will provide for each activity includes general information about the patient population and the patient’s history and information related to the processes of care and outcomes.</p>
<p><strong>It is recommended that you carefully read the activity descriptions and patient parameters before committing to a particular activity. </strong></p>
<p>Minimum improvement timeframes range from 30 days to 395 days depending on the activity. The minimum improvement timeframe is the time period during which you are expected to implement your improvement plan before re-measuring patient data and assessing the impact of your improvement efforts.</p>
<p>Should you find yourself unable to meet any Improvement in Medical Practice Activity criteria, you are encouraged to develop a Self-Directed Improvement in Medical Practice Project. More information on the self-directed improvement in medical practice project can be found <a href="http://abop.org/maintain-certification/part-4-improvement-in-medical-practice/self-directed-qi-program/" title="Self-Directed QI Program" target="_blank">here</a>.</p>
<p><strong>Available in May 2016: PQRS Enabled Improvement in Medical Practice Activities:</strong><br /> PQRS Enabled improvement in medical practice activities provide the ability to participate in the Centers for Medicare and Medicaid Services (CMS)'s PQRS reporting.</p>
<hr />
<h2>Improvement in Medical Practice Activities</h2>
<table border="0" cellspacing="0" cellpadding="0" class="data-table">
<thead>
<tr style="height: 19px;">
<td class="xl63" width="275" height="19">Activity</td>
<td class="xl64" width="434">Patient Definition</td>
<td class="xl63" width="205">Minimum Improvement Period (Days)</td>
</tr>
</thead>
<tbody>
<tr height="57">
<td class="xl65" height="57"><a href="http://abop.org/media/90135/cataract_surgical_management.pdf" target="_blank" title="Cataract (Surgical Management)">Cataract (Surgical Management)</a></td>
<td class="xl66" width="434">A patient over the age of 40 for whom you have performed cataract surgery and have examined between 20–90 days after surgery. Please complete a Improvement in Medical Practice Activity for only one eye per patient.</td>
<td class="xl65" align="right">50</td>
</tr>
<tr height="57">
<td class="xl65" height="57"><a href="http://abop.org/media/90156/yag_laser_capsulotomy.pdf" target="_blank" title="YAG Laser Capsulotomy">YAG Laser Capsulotomy</a></td>
<td class="xl66" width="434">A patient over the age of 40 with a posterior chamber intraocular lens for whom you have performed YAG-laser capsulotomy and examined within 90 days postoperatively.</td>
<td class="xl65" align="right">30</td>
</tr>
<tr height="38">
<td class="xl65" height="38"><a href="http://abop.org/media/56335/corneal_edema.pdf" target="_blank">Corneal Edema</a></td>
<td class="xl66" width="434">Patients followed for at least 6 months after corneal transplantation for Corneal Edema.</td>
<td class="xl65" align="right">395</td>
</tr>
<tr height="19">
<td class="xl65" height="19"><a href="http://abop.org/media/56356/infectious_keratitis.pdf" target="_blank">Corneal Ulcer/Bacterial (Infectious)  Keratitis</a></td>
<td class="xl66" width="434">Patients treated and followed for Non-Viral Infectious Keratitis</td>
<td class="xl65" align="right">395</td>
</tr>
<tr height="96">
<td class="xl65" height="96"><a href="http://abop.org/media/56380/POAG.pdf" target="_blank">Primary Open Angle Glaucoma</a></td>
<td class="xl66" width="434">Patients in whom the ophthalmologist has diagnosed primary open-angle glaucoma (POAG).   Eligible patients for inclusion include those patients aged 18 or older in which POAG is diagnosed.  Patients diagnosed with pseudoexfoliation, pigment dispersion,  uveitis, or other secondary forms of glaucoma are NOT eligible for inclusion.</td>
<td class="xl65" align="right">120</td>
</tr>
<tr height="180">
<td class="xl65" height="180"><a href="http://abop.org/media/56365/pacg.pdf" target="_blank">Primary Angle Closure Glaucoma</a></td>
<td class="xl66" width="434">Patients age 18 and older who have a diagnosis of “angle closure glaucoma.”  Because of inconsistency and non-standardization of terms within the ophthalmology community, include ANY of the following: 1) those with narrow angles suspected of having angle closure without glaucoma; 2) those suspected of angle closure glaucoma; 3) those with definite angle closure glaucoma.   This encompasses all PRIMARY forms of angle closure.  It does NOT include secondary forms, such as uveitic glaucoma, neovascular glaucoma, or other causes of synechial angle closure.</td>
<td class="xl65" align="right">120</td>
</tr>
<tr height="160">
<td class="xl65" height="160"><a href="http://abop.org/media/56383/POAGS.pdf" target="_blank">Primary Open Angle Glaucoma Suspect</a></td>
<td class="xl66" width="434">Patients in whom the ophthalmologist has diagnosed a patient as a “glaucoma suspect.”  This can be on the basis of any one or more of the following:  1) elevated IOP; 2) optic nerve  status; or 3) visual field status.  Eligible patients for inclusion include those patients aged 18 or older in which POAG is suspected.   Patients diagnosed with pseudoexfoliation, pigment dispersion,  uveitis, or other conditions that might be associated with a secondary form of glaucoma are NOT eligible for inclusion.</td>
<td class="xl65" align="right">120</td>
</tr>
<tr height="38">
<td class="xl65" height="38"><a href="http://abop.org/media/56362/optic_neuritis.pdf" target="_blank">Optic Neuritis</a></td>
<td class="xl66" width="434">Patients age 18 or older you have diagnosed with unilateral optic neuritis of less than 2 weeks duration.</td>
<td class="xl65" align="right">120</td>
</tr>
<tr height="38">
<td class="xl65" height="38"><a href="http://abop.org/media/56395/sixth_nerve_palsy.pdf" target="_blank">Sixth Nerve Palsy</a></td>
<td class="xl66" width="434">Patients 18 years or older you have diagnosed with unilateral 6th nerve palsy of less than 3 months duration. This Improvement in Medical Practice Activity takes into consideration that many neuro-ophthalmologists only do initial consultations.</td>
<td class="xl65" align="right">120</td>
</tr>
<tr height="38">
<td class="xl65" height="38"><a href="http://abop.org/media/56329/cataract_surgery_uveitis.pdf" target="_blank">Cataract Surgery &amp; Uveitis</a></td>
<td class="xl66" width="434">Patients with Uveitis who undergo cataract surgery and are followed for at least 6 months post operatively.</td>
<td class="xl65" align="right">212</td>
</tr>
<tr height="57">
<td class="xl65" height="57"><a href="http://abop.org/media/56389/raau.pdf" target="_blank">Recurrent Acute Anterior Uveitis</a></td>
<td class="xl66" width="434">Patients with at least one prior episode and 6 months followup. {Suggest also that last episode resolved in 3 months or less to avoid confusion with Chronic Uveitis}</td>
<td class="xl65" align="right">212</td>
</tr>
<tr height="57">
<td class="xl65" height="57"><a href="http://abop.org/media/56353/eyelid_malignancy.pdf" target="_blank">Eyelid Malignancy</a></td>
<td class="xl66" width="434">Patients with an eyelid malignancy who have undergone treatment and have either been followed for a minimum of 90 days or have been released from your care.</td>
<td class="xl65" align="right">120</td>
</tr>
<tr height="57">
<td class="xl65" height="57"><a href="http://abop.org/media/56386/ptosis.pdf" target="_blank">Ptosis (Observational Management)</a></td>
<td class="xl66" width="434">Patients 18 years or older with acquired blepharoptosis, operated on by you in the past 24 months, who have been followed for a minimum of 90 days or have been released from your care.</td>
<td class="xl65" align="right">120</td>
</tr>
<tr height="76">
<td class="xl65" height="76"><a href="http://abop.org/media/56398/thyroid_ophthalmopathy.pdf" target="_blank">Thyroid Ophthalmopathy</a></td>
<td class="xl66" width="434">Patients diagnosed with thyroid orbitopathy on whom you have advised medical or surgical treatment and have followed for at least 6 months (including treatment with topical, systemic, radiation or surgical treatments).</td>
<td class="xl65" align="right">212</td>
</tr>
<tr height="38">
<td class="xl65" height="38"><a href="http://abop.org/media/56392/retinoblastoma.pdf" target="_blank">Retinoblastoma</a></td>
<td class="xl66" width="434">Patients  diagnosed and treated with globe sparing therapy for heritable retinoblastoma and that you have followed for 5 years.</td>
<td class="xl65" align="right">395</td>
</tr>
<tr height="57">
<td class="xl65" height="57"><a href="http://abop.org/media/56332/choroidal_melanoma.pdf" target="_blank">Choroidal Melanoma</a></td>
<td class="xl66" width="434">Patient diagnosed with choroidal melanoma and treated with radiation.  The patient must have been followed for a minimum of 5 years following radiation treatment.</td>
<td class="xl65" align="right">395</td>
</tr>
<tr height="57">
<td class="xl65" height="57"><a href="http://abop.org/media/56323/amblyopia.pdf" target="_blank">Amblyopia</a></td>
<td class="xl66" width="434">Patients between 3-11 years old for whom you have diagnosed monocular amblyopia with an initial acuity of 20/40 or worse, initiated therapy (non-surgical), and have followed for at least 6 months with optotype recognition acuity for all exams.</td>
<td class="xl65" align="right">180</td>
</tr>
<tr height="57">
<td class="xl65" height="57"><a href="http://abop.org/media/56347/esotropia.pdf" target="_blank">Esotropia</a></td>
<td class="xl66" width="434">Patients diagnosed with non-accommodative or partially accommodative esotropia on whom you have performed surgery and have followed for a least 6 months</td>
<td class="xl65" align="right">270</td>
</tr>
<tr height="57">
<td class="xl65" height="57"><a href="http://abop.org/media/56359/lasik_for_myopia.pdf" target="_blank">Lasik for Myopia</a></td>
<td class="xl66" width="434">Patients over the age of 18 years for whom you have a) diagnosed with MYOPIA b) performed LASIK SURGERY in both eyes, and c) followed for at least 90 days.</td>
<td class="xl65" align="right">120</td>
</tr>
<tr height="60">
<td class="xl65" height="60"><a href="http://abop.org/media/56401/toric.pdf" target="_blank">Toric IOL</a></td>
<td class="xl66" width="434">Patients over the age of 18 years for whom you have a) performed cataract surgery and b) placed a toric intraocular lens and c) followed for at least 90 days.</td>
<td class="xl65" align="right">120</td>
</tr>
<tr height="120">
<td class="xl65" height="120"><a href="http://abop.org/media/56350/exudative_amd.pdf" target="_blank">Exudative AMD</a></td>
<td class="xl66" width="434">Patients 50 years or older who present with active* exudative subfoveal Age-Related Macular Degeneration seen by you and followed for at least 6 months. Active Exudative is defined  by presence of  blood, hard exudate or serous fluid.</td>
<td class="xl65" align="right">180</td>
</tr>
<tr height="100">
<td class="xl65" height="100"><a href="http://abop.org/media/56341/diabetic_retinopathy.pdf" target="_blank">Diabetic Retinopathy</a></td>
<td class="xl66" width="434">Patients 18 years or older with diabetic retinopathy (NPDR or PDR that is not high risk) and clinically significant macular edema involving the center of the macula, seen and treated by you in the past 5 years and subsequently followed by you for at least 12 months.</td>
<td class="xl65" align="right">395</td>
</tr>
<tr height="60">
<td class="xl65" height="60"><a href="http://abop.org/media/56338/crvo.pdf" target="_blank">Central Retinal Vein Occlusion (CRVO)</a></td>
<td class="xl66" width="434">New patients who presented with perfused CRVO and macular edema, seen by you within the past 5 years and followed for at least one year.</td>
<td class="xl65" align="right">395</td>
</tr>
<tr height="60">
<td class="xl65" height="60"><a href="http://abop.org/media/56344/dry_eye.pdf" target="_blank">Dry Eye and Ocular Surface Disease</a></td>
<td class="xl66" width="434">Patients with dry eye syndromes or ocular surface diseases due to neurotrophic keratopathy, abnormality of lid position, or abnormality of lid function seen for initial evaluation, and re-examined at least once within the next 12 months. For patients with bilateral disease, include data for both eyes but perform chart abstraction only once.</td>
<td class="xl65" align="right">120</td>
</tr>
<tr height="20">
<td class="xl65" height="20"><a href="http://abop.org/media/56371/pathology_biopsies.pdf" target="_blank">Pathology-Biopsies for temporal arteritis</a></td>
<td class="xl66" width="434">Patients for whom biopsies for temporal arteritis performed</td>
<td class="xl65" align="right">120</td>
</tr>
<tr height="20">
<td class="xl65" height="20"><a href="http://abop.org/media/56377/pathology_stains.pdf" target="_blank">Pathology-Special Stains</a></td>
<td class="xl66" width="434">Patients for whom biopsies including special stain were performed </td>
<td class="xl65" align="right">120</td>
</tr>
<tr height="20">
<td class="xl65" height="20"><a href="http://abop.org/media/56374/pathology_globes.pdf" target="_blank">Pathology-Globes</a></td>
<td class="xl66" width="434">Patient globes with the final diagnosis of retinoblastoma</td>
<td class="xl65" align="right">120</td>
</tr>
<tr height="20">
<td class="xl65" height="20"><a href="http://abop.org/media/56371/pathology_biopsies.pdf" target="_blank">Pathology-Routine Biopsies</a></td>
<td class="xl66" width="434">Patients for whom routine biopsy performed</td>
<td class="xl65" align="right">120</td>
</tr>
<!--<tr height="20">
<td class="xl65" height="20">PEC</td>
<td class="xl66" width="434">All patients seen in your office</td>
<td class="xl65" align="right">90</td>
</tr>-->
</tbody>
</table>
<!--<table border="0" cellspacing="0" cellpadding="0" class="data-table" style="width: 625px;">
<thead>
<tr style="height: 19px;">
<td class="xl63" width="275" height="19">Activity</td>
<td class="xl64" width="434">Patient Definition</td>
<td class="xl63" width="205">Minimum Improvement Period (Days)</td>
</tr>
</thead>
<tbody>
<tr height="57">
<td class="xl65" height="57"><a href="/media/89669/cataract_surgical_management.pdf" target="_blank" title="Cataract (Surgical Management)">Cataract (Surgical Management)</a></td>
<td class="xl66" width="434">A patient over the age of 40 for whom you have performed cataract surgery and have examined between 20–90 days after surgery. Please complete a Improvement in Medical Practice Activity for only one eye per patient.</td>
<td class="xl65" align="right">50</td>
</tr>
<tr height="57">
<td class="xl65" height="57"><a href="/media/89690/yag_laser_capsulotomy.pdf" target="_blank" title="YAG Laser Capsulotomy">YAG Laser Capsulotomy</a></td>
<td class="xl66" width="434">A patient over the age of 40 with a posterior chamber intraocular lens for whom you have performed YAG-laser capsulotomy and examined within 90 days postoperatively.</td>
<td class="xl65" align="right">30</td>
</tr>
<tr height="160">
<td class="xl65" height="160">Primary Open Angle Glaucoma</td>
<td class="xl66" width="434">Patients 18 years or older whom you have diagnosed with unilateral or bilateral primary open angle glaucoma (POAG) and followed for more than 12 months. Patients diagnosed with pseudoexfoliation, pigment dispersion, uveitis, or other secondary forms of glaucoma in either eye are NOT eligible for inclusion.</td>
<td class="xl65" align="right">120</td>
</tr>
<tr height="160">
<td class="xl65" height="160">Glaucoma Suspect</td>
<td class="xl66" width="434">Patients over 18 years of age whom you have diagnosed as a “glaucoma suspect” and followed for more than 12 months. This can be on the basis of any one or more of the following: 1) elevated IOP; 2) optic nerve status; or 3) visual field status. Patients diagnosed with pseudoexfoliation, pigment dispersion, uveitis, or previously diagnosed glaucoma in EITHER eye are NOT eligible.</td>
<td class="xl65" align="right">120</td>
</tr>
<tr height="57">
<td class="xl65" height="57">Dry Eye</td>
<td class="xl66" width="434">Patients with dry eye syndromes or ocular surface diseases due to neurotrophic keratopathy, abnormality of lid position, or abnormality of lid function seen for initial evaluation, and re-examined at least once within the next 12 months. For patients with bilateral disease, include data for both eyes but perform chart abstraction only once.</td>
<td class="xl65" align="right">120</td>
</tr>
<tr height="57">
<td class="xl65" height="57">Sixth Nerve Palsy</td>
<td class="xl66" width="434">Patients 18 years or older you have diagnosed with unilateral 6th nerve palsy of less than 3 months duration. This Improvement in Medical Practice Activity takes into consideration that many neuro-ophthalmologists only do initial consultations.</td>
<td class="xl65" align="right">120</td>
</tr>
<tr height="57">
<td class="xl65" height="57">Amblyopia</td>
<td class="xl66" width="434">Patients between 3-11 years old for whom you have diagnosed monocular amblyopia with an initial acuity of 20/40 or worse, initiated therapy (non-surgical), and have followed for at least 6 months with optotype recognition acuity for all exams.</td>
<td class="xl65" align="right">180</td>
</tr>
<tr height="57">
<td class="xl65" height="57">Esotropia</td>
<td class="xl66" width="434">Select patients that you have diagnosed with comitant esotropia, on whom you have performed initial strabismus surgery, and have performed a follow-up exam between 3 and 6 months after surgery.</td>
<td class="xl65" align="right">120</td>
</tr>
<tr height="57">
<td class="xl65" height="57">Ptosis (Observational Management)</td>
<td class="xl66" width="434">A patient 18 years or older with acquired blepharoptosis, operated on by you and either followed for 90 days or released from your care. If bilateral surgery was performed, please choose the RIGHT eye.</td>
<td class="xl65" align="right">120</td>
</tr>
<tr height="57">
<td class="xl65" height="57">Exudative AMD</td>
<td class="xl66" width="434">Patients 50 years or older who present with active* exudative subfoveal Age-Related Macular Degeneration seen by you and followed for at least 6 months.</td>
<td class="xl65" align="right">180</td>
</tr></tbody>
</table>
<p> </p>
<hr />
<h2><a name="pqrs"></a><img src="/"/>PQRS Enabled Improvement in Medical Practice Activities</h2>
<p>These improvement in medical practice activities are modified versions of the simplified Improvement in Medical Practice Activities and provide the ability to participate in the Centers for Medicare and Medicaid Services (CMS)'s PQRS reporting.</p>
<ul>
<li>Cataract Measures Group</li>
<li>Glaucoma Measures Group</li>
</ul>
<p>Should you find yourself unable to meet any improvement in medical practice activity criteria, you are encouraged to develop a Self-Directed Improvement in Medical Practice Project. More information on the self-directed improvement in medical practice project can be found <a href="/maintain-certification/part-4-improvement-in-medical-practice/self-directed-qi-program/" title="Self-Directed QI Program">here</a>.</p>-->

</div>

<div class="button-box"><asp:LinkButton ID="ButtonSubmit" runat="server" Text="Proceed" OnClick="ButtonSubmit_Click" CssClass="button" /></div>

</asp:Content>

