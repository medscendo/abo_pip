﻿<%@ Page Title="" Language="C#" MasterPageFile="~/PIMMasterPage.master" AutoEventWireup="true" CodeFile="PasswordRecovery.aspx.cs" Inherits="copd_PasswordRecovery" %>

<asp:Content ID="Content1" ContentPlaceHolderID="head" Runat="Server">
</asp:Content>
<asp:Content ID="Content3" ContentPlaceHolderID="ContentPlaceHolder1" Runat="Server">

<center>
    <br />
    <h3>Forgot your Password?</h3>
    <asp:Panel ID="PanelPasswordRecoveryRequest" runat="server">
        Please provide your e-mail address to receive your password.
        <table>
            <tr>
                <td>e-mail:</td>
                <td>
                    <asp:TextBox ID="TextEmailAddress" runat="server" MaxLength="100" Width="220px" SkinID="RequiredFieldSkin"></asp:TextBox>
                    <asp:RequiredFieldValidator ID="RequiredEmail" runat="server" Display="Dynamic"
                        ErrorMessage="E-mail Address is Required." ToolTip="E-mail Address is required." ControlToValidate="TextEmailAddress"></asp:RequiredFieldValidator>
                    <asp:RegularExpressionValidator ID="RegExpEmailValidator" runat="server" Display="Dynamic"
                        ToolTip="Invalid e-mail address." ErrorMessage="Invalid e-mail address"
                        ValidationExpression="^[\w-]+@[\w-]+\.(com|net|org|edu|mil)$" ControlToValidate="TextEmailAddress"></asp:RegularExpressionValidator>            
                </td>
            </tr>
            <tr>
                <td>&nbsp;</td>
                <td><asp:Button ID="ButtonSubmit" runat="server" Text="Submit" 
                        onclick="ButtonSubmit_Click" /></td>
            </tr>
        </table>
    </asp:Panel>
    
    <asp:Panel ID="PanelEmailConfirmation" runat="server" Visible="false">
    An e-mail was sent with instructions to reset your password. Close this window and follow the instructions in the e-mail.
    </asp:Panel>
    <asp:Label ID="LabelError" runat="server" Text="" CssClass="error_label" Visible="false"></asp:Label>
    <br />
    <br />

</center>
</asp:Content>

