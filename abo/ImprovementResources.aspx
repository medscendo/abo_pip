﻿<%@ Page Title="" Language="C#" MasterPageFile="~/PIMMasterPage.master" AutoEventWireup="true" CodeFile="ImprovementResources.aspx.cs" Inherits="abo_ImprovementResources" %>

<asp:Content ID="Content1" ContentPlaceHolderID="head" Runat="Server">
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder1" Runat="Server">
         <!-- module detaile -->
           <div class="improvement_plan">
            <div class="heading_box">
              <h2>Improvement Resources</h2>
            </div>
            <div class="improvement_plan_details">
              <div class="top_details">
              
                    <asp:ListView runat="server" ID="ListViewResources">
                        <LayoutTemplate>
                            <ul>
                            <asp:PlaceHolder runat="server" ID="itemPlaceholder"></asp:PlaceHolder>
                            </ul>
                        </LayoutTemplate>    
                        <ItemTemplate>
                                    <p>
                                        <strong><asp:Literal ID="LabelCheckBoxResource" runat="server" Text='<%# Eval("ResourceName") %> '></asp:Literal></strong><br />
                                        <asp:Label ID="ResourceDescription" runat="server" Text='<%# Eval("ResourceDescription") %>' /><br />
                                        <asp:HyperLink runat="server" ID="HyperLinkURL" Text='<%# Eval("URL") %>' NavigateUrl='<%# Eval("URL") %>' Target="_blank"></asp:HyperLink>
                                    </p>
                        </ItemTemplate>
                    </asp:ListView>    
              
              </div>
              <div class="bginput"><asp:LinkButton ID="ButtonSubmit" runat="server" Text="Back" PostBackUrl="Dashboard.aspx"/></div>
              </div>
            </div>
          <!-- module detaile ends --> 
</asp:Content>

