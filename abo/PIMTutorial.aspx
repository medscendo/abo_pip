﻿<%@ Page Title="" Language="C#" MasterPageFile="~/PIMMasterPage.master" AutoEventWireup="true" CodeFile="PIMTutorial.aspx.cs" Inherits="abo_PIMTutorial" %>

<asp:Content ID="Content1" ContentPlaceHolderID="head" Runat="Server"></asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder1" Runat="Server">
               
<p>This tutorial provides a 12 minute overview of the Part IV platform and process.</p>
<p>Note: If you would like to view this tutorial in a larger window, <a href="../ABO_Tutorials/ABO_PIP_Tutorial/player.html" target="_blank">Click Here</a></p>              
<iframe src="../ABO_Tutorials/ABO_PIP_Tutorial/player.html" height=600 width=900></iframe> 
         
<div class="button-box"><asp:LinkButton ID="ButtonSubmit" runat="server" Text="Back" PostBackUrl="Dashboard.aspx" CssClass="button" /></div>

</asp:Content>

