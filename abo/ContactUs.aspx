﻿<%@ Page Title="" Language="C#" MasterPageFile="~/PIMMasterPage.master" AutoEventWireup="true" CodeFile="ContactUs.aspx.cs" Inherits="abo_ContactUs" %>

<asp:Content runat="server" ID="Content1" ContentPlaceHolderID="head">
    <style>
        textarea {
            width: 100%;
        }
        label {
            padding-left:5px;
        }

        .first-para {
            margin-top:0;
        }

        .last-para {
            margin-bottom:0;
        }
    </style>
</asp:Content>

<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder1" Runat="Server">

<p>
    For non-technical questions related to content, MOC Part IV or CME, or for website and technical questions, please contact: 
    <br>The American Board of Ophthalmology at 610-664-1175 during the hours of 8:30 – 4:30 EST or email at 
    <a href="mailto:Part4Support@abop.org?subject=ABO Improvement in Medical Practice Activity Support">Part4Support@abop.org</a>.
</p>
<p>
    Or leave feedback and we will respond within one business day:
</p>


<div class="action-box">
    <p class="first-para">
        <strong>Comments:</strong>
        <asp:TextBox runat="server" ID="Comments" TextMode="MultiLine"></asp:TextBox>
    </p>
    <p>
        <strong>Would you like us to contact you regarding your feedback?</strong>
        <br /><asp:RadioButton ID="rdoYes" runat="server" GroupName="Feedback" Text="Yes"/>
        <br /><asp:RadioButton ID="rdoNo" runat="server" GroupName="Feedback" Text="No"/>
    </p>
    <p>
        <strong>Your email address:</strong>
        <br /><asp:TextBox ID="EmailAddress" runat="server" Width="300px"></asp:TextBox>
    </p> 
    <p>
        <strong>Your phone number:</strong>
        <br /><asp:TextBox ID="PhoneNumber" runat="server" Width="300px"></asp:TextBox>
    </p>
    <p class="last-para">
        <strong>Best way to reach you:</strong>
        <br /><asp:RadioButton ID="rdoEmail" runat="server" GroupName="Contact" Text="Email"/>
        <br /><asp:RadioButton ID="rdoPhone" runat="server" GroupName="Contact" Text="Phone"/>
    </p>

    <asp:Label ID="lblMessage" runat="server" Visible="false" ForeColor="Blue" Text="Your feedback has been sent. Thank you for your feedback." />
</div>
<div class="button-box">
    <asp:LinkButton ID="ButtonSubmit" runat="server" Text="Back" PostBackUrl="Dashboard.aspx" CssClass="button" />
    <asp:LinkButton ID="LinkButtonSendEmail" runat="server" Text="Submit" OnClientClick="return validateForm();" OnClick="ButtonSubmit_Click" CssClass="button" />
</div>
              
</asp:Content>

<asp:Content ID="Content3" ContentPlaceHolderID="javascript" Runat="Server">

<script>

    function validateForm() {
        var re_email = /^([\w-]+(?:\.[\w-]+)*)@((?:[\w-]+\.)*\w[\w-]{0,66})\.([a-z]{2,6}(?:\.[a-z]{2})?)$/i
        var email = document.getElementById("<%= EmailAddress.ClientID %>").value;
        var comments = document.getElementById("<%= Comments.ClientID %>").value;

        var errors = [];
        if (comments == 0) {
            errors[errors.length] = "Please provide feedback.";
        }
        if (!re_email.test(email) && email != 0) {
            errors[errors.length] = "Please enter a valid email address.";
        }
        if (errors.length > 0) {

            reportErrors(errors);
            return false;
        }
        return true;
    }

    function reportErrors(errors) {
        var msg = "Please Enter Valid Data...\n";
        for (var i = 0; i < errors.length; i++) {
            var numError = i + 1;
            msg += "\n" + numError + ". " + errors[i];
        }
        alert(msg);
    }

</script>

</asp:Content>