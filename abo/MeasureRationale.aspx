﻿<%@ Page Language="C#" AutoEventWireup="true" CodeFile="MeasureRationale.aspx.cs" Inherits="abo_MeasureRationale" %>

<head id="Head1" runat="server">
<link href="../common/css/normalize.css" rel="stylesheet" />
<link href="../common/css/style.css" rel="stylesheet" />

<style type="text/css">
    body {
        background: none;
    }
</style>


</head>
<body>
<form id="Form1" runat="server">

    <h2 style="padding:20px; padding-bottom:0px; color:#013671;">Rationale</h2>
     
    <p class="blue1" style="padding-left:20px;"><b><asp:Literal ID="LiteralMeasureQualityIndicator2" runat="server"></asp:Literal>:</b> <asp:Literal ID="LiteralMeasureLongDescription" runat="server"></asp:Literal></p>
    <%--<p class="blue1"><asp:Literal ID="LiteralRationaleMeasureDescription" runat="server"></asp:Literal></p>--%>
    
    <p style="text-align:right; padding:20px;" ><button type="button" onclick="window.open('', '_self', ''); window.close();">Close</button>

    </p>
</form>

<script src="//ajax.googleapis.com/ajax/libs/jquery/1.10.2/jquery.min.js"></script>
<script src="//ajax.googleapis.com/ajax/libs/jqueryui/1.10.3/jquery-ui.min.js"></script> 
<link href="../common/css/jquery-ui-1.10.3.custom.min.css" rel="stylesheet" />
<script>
    $(function () {
        $(document).tooltip();
    });
</script>
</body>