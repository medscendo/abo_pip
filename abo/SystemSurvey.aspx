﻿<%@ Page Title="" Language="C#" MasterPageFile="~/PIMMasterPage.master" AutoEventWireup="true" CodeFile="SystemSurvey.aspx.cs" Inherits="abo_SystemSurvey" %>

<asp:Content ID="Content1" ContentPlaceHolderID="head" Runat="Server">
    <style>

        table {
            width:100%;
        }

        td {
            padding: 5px 10px;
            border: 1px solid #ccc;
        }

        td:nth-child(1) {
            width:50%;
        }
        
        td:nth-child(2) {
            text-align: left;
        }

        select {
            width: 50%;
        }

        textarea {
            width: 100% !important;
        }

        table table td {
            border:none;
        }

        .header-box .body {
            padding:0;
        }

        .header-box .head {
            padding-left:20px;
            text-align:left;
            font-weight:bold;
        }
        .tableyesno
        {
            width:120px;
        }
       .chktable   td{
           
            text-align:left;
            float:left;
            width: 400px;
        }
    </style>
</asp:Content>
<asp:Content ID="Content3" ContentPlaceHolderID="javascript" Runat="Server">
    <script src="http://code.jquery.com/jquery-latest.js"></script>
    <script type="text/javascript">
       function Checkform(o) {
           if ($('input:radio[name=' + o + ']:checked').length < 1) {
               return false;
           }
           else
               return true;
       }
       $(document).ready(function () {
           $("#<%=ButtonSubmit.ClientID%>").click(function (evt) {
               //alert(document.getElementById('ctl00_ContentPlaceHolder1_choices21').value);
               $('input:radio').each(function (index) {
                   var name = $(this).attr("name");

                   var is = Checkform(name);
                   if (is == false) {

                       var answer = confirm('All fields are not complete.  Are you sure you want to submit?');
                       // answer is a boolean
                       if (answer == false) {
                           evt.preventDefault();

                       }
                       return false;
                   }
               });



           });
       });
</script>

</asp:Content>

<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder1" Runat="Server">
                
    <!--<div class="heading_box">
        <h2>System Survey:  <asp:Label ID="Label1" runat="server" Text=""></asp:Label></h2>
        <h3> </h3>
    </div>-->
    <p>Please answer the following questions while considering the current systems, tools, and strategies employed within your practice. Your systems survey answers will be presented in conjunction with the results of the actual performance when you abstract the patient charts you registered for this activity.</p>
    <div class="header-box">
        <div class="head">
            System Survey
        </div>
        <h3>
        <asp:Label ID="LabelError"  Visible="false"  runat="server" Text="Label"></asp:Label></h3>
        <div class="body">
            <fieldset>
                <asp:Panel ID="Panel1" runat="server" />
            </fieldset>
        </div>
    </div>

    <div class="button-box">
        <asp:LinkButton ID="LinkButtonReturnToStatus" runat="server" OnCommand="LinkButtonReturnToStatus_Click" CssClass="button" Text="Return to Status Page" />
        <%--<asp:LinkButton ID="LinkButtonBackToDashboard" runat="server" Text="Dashboard" CssClass="button" PostBackUrl="Dashboard.aspx"/>--%>
        <asp:LinkButton ID="ButtonSubmit" runat="server" OnCommand="ButtonSubmit_Click" CssClass="button" Text="Save System Survey" />
    </div>

</asp:Content>

