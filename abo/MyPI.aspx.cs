﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using NetHealthPIMModel;

// TODO: Should be a common screen (when time allows it).
// TODO: We do need to have a participant profile that is generic as well as an abstract table. 
public partial class copd_MyPI : BasePage
{
    protected void Page_Load(object sender, EventArgs e)
    {
        if (!IsPostBack)
        {
            InitializePage();
        }
    }

    protected void InitializePage()
    {
        ((PIMMasterPage)Page.Master).SetTitle("Dialysis Activity Index");
        int activeCycle = ctx.ActiveModuleCycleID;
        using (NetHealthPIMEntities pim = new NetHealthPIMEntities())
        {
            ParticipantModuleCycle cycle = pim.ParticipantModuleCycle.First(p => p.ParticipantModuleCycleID == activeCycle);
            
            bool P2Flag = cycle.Phase2Flag ?? false;
            var countAbstract = 0;
            var countTotalGroupAbstract = 0;
            if (ctx.ParticipantGroupID != 0)
            {
                if ((from chart in pim.AbstractCompletedByGroup_V
                     where chart.ParticipantGroupID == ctx.ParticipantGroupID
                        && (chart.Phase2Flag == P2Flag || chart.Phase2Flag == null)
                        && chart.MinAbstractsRequired <= chart.Completed
                     select chart).Count() > 0)
                    countTotalGroupAbstract = (int)(from chart in pim.AbstractCompletedByGroup_V
                                          where chart.ParticipantGroupID == ctx.ParticipantGroupID
                                             && (chart.Phase2Flag == P2Flag || chart.Phase2Flag == null)
                                             && chart.MinAbstractsRequired <= chart.Completed
                                          select chart.Completed).Sum();
                else
                    countTotalGroupAbstract = 0;
            }
            //else
            countAbstract = pim.Abstract_COPD.Count(a => a.ParticipantModuleCycle.ParticipantModuleCycleID == activeCycle && a.IsValid == true && a.IsComplete == true);
            var countParticipantProfile = pim.ParticipantProfile_COPD.Count(p => p.ParticipantModuleCycleID == activeCycle);
            var countSystemExamination = pim.SystemExamination.Count(s => s.ParticipantModuleCycleID == activeCycle);
            bool isPhase2 = cycle.Phase2Flag ?? false;
            bool abstractRequired = (countAbstract >= module.MinAbstractsRequired);
            if (ctx.ParticipantGroupID != 0)
            {
                var reportReadyToApprove = (from chart in pim.ParticipantGroupReportReadyToApprove_V
                     where chart.ParticipantGroupID == ctx.ParticipantGroupID
                        && (chart.CycleNumber == cycle.CycleNumber)
                        && (chart.PracticeSystemAnalysisCompleted == false
                        || chart.ParticipantProfileComplete == false )
                     select chart).Count();
                if (reportReadyToApprove == 0)
                    abstractRequired = (countTotalGroupAbstract >= module.MinAbstractsRequired);
                else
                    abstractRequired = false;
            }
            LinkP2Resources.NavigateUrl = "~/" + module.ModuleCode.ToLower() + "/Resources.aspx";
            LinkP2Strategies.NavigateUrl = "~/" + module.ModuleCode.ToLower() + "/Strategies.aspx";

            // Abstract information
            if (isPhase2)
            {
                // This cycle is complete so only show the dates and the links to view the read-only reports. 
                cycle.PrevParticipantModuleCycleReference.Load();
                ParticipantModuleCycle prev = cycle.PrevParticipantModuleCycle;
                
                // Abstract data
                LiteralP1AbstractCount.Text = pim.Abstract_COPD.Count(
                    chart => chart.ParticipantModuleCycle.ParticipantModuleCycleID == prev.ParticipantModuleCycleID 
                        && chart.IsComplete == true && chart.IsValid == true).ToString();
                if (ctx.ParticipantGroupID != 0) //user belong to a group
                    LiteralP1AbstractTarget.Text = ctx.MinAbstractsRequired.ToString();
                else
                    LiteralP1AbstractTarget.Text = module.MinAbstractsRequired.ToString();
                LiteralP1AbstractTargetDate.Text = prev.AbstractDataTargetDate.Value.ToString("d");
                LiteralP1AbstractCompleteDate.Text = prev.AbstractDataCompletedDate.Value.ToString("d");
                ImageP1Abstract.ImageUrl = "~/images/" + module.ModuleCode.ToLower() + "/complete_" + prev.AbstractDataComplete.ToString() + ".png";

                // Participant profile
                LiteralP1ProfileCount.Text = "1";
                LiteralP1ProfileTargetDate.Text = prev.ParticipantProfileTargetDate.Value.ToString("d");
                LiteralP1ProfileCompleteDate.Text = prev.ParticipantProfileCompetedDate.Value.ToString("d");
                ImageP1Profile.ImageUrl = "~/images/" + module.ModuleCode.ToLower() + "/complete_" + prev.ParticipantProfileComplete.ToString() + ".png";

                //  Current Practice System Analysis
                LiteralP1SystemAnalysisCount.Text = "1";
                LiteralP1SystemAnalysisTargetDate.Text = prev.ParticipantProfileTargetDate.Value.ToString("d");
                LiteralP1SystemAnalysisCompletedDate.Text = prev.PracticeSystemAnalysisCompletedDate.Value.ToString("d");
                ImageP1SystemAnalysis.ImageUrl = "~/images/" + module.ModuleCode.ToLower() + "/complete_" + prev.PracticeSystemAnalysisCompleted.ToString() + ".png";

                // Review and Approve Report
                LiteralP1ReviewReportCount.Text = "1";
                LiteralP1ReviewReportTargetDate.Text = prev.ReportApprovedTargetDate.Value.ToString("d");
                LiteralP1ReviewReportCompleteDate.Text = prev.ReportApprovedCompletedDate.Value.ToString("d");
                ImageP1ReviewReport.ImageUrl = "~/images/" + module.ModuleCode.ToLower() + "/complete_" + prev.ReportApproved.ToString() + ".png";

                // View Part1 Report (?)
                LinkP2ViewReport.NavigateUrl = "~/" + module.ModuleCode.ToLower() + "/PerformanceReport.aspx?" +
                    Constants.QUERYSTRING_ACTION + "=" + Constants.QUERYSTRING_ACTION_VIEW + "&" + 
                    Constants.QUERYSTRING_PHASE + "=1&" + Constants.QUERYSTRING_CYCLEID + "=" + prev.ParticipantModuleCycleID.ToString(); 

                // Select measures
                LiteralP2MeasuresCount.Text = "1";
                LiteralP2MeasuresTargetDate.Text = prev.MeasuresSelectedTargetDate.Value.ToString("d");
                LiteralP2MeasuresCompleteDate.Text = prev.MeasuresSelectedCompletedDate.Value.ToString("d");
                ImageP2Measures.ImageUrl = "~/images/" + module.ModuleCode.ToLower() + "/complete_" + prev.MeasuresSelected.ToString() + ".png";

                // Develop Improvement Plan
                LiteralP2DevelopPlanCount.Text = "1";
                LiteralP2DevelopPlanTargetDate.Text = prev.ImprovementPlanTargetDate.Value.ToString("d");
                LiteralP2DevelopPlanCompleteDate.Text = prev.ImprovementPlanCompletedDate.Value.ToString("d");
                ImageP2DevelopPlan.ImageUrl = "~/images/" + module.ModuleCode.ToLower() + "/complete_" + prev.ImprovementPlanComplete.ToString() + ".png";

                // View Improvement Plan
                LinkP2ViewPlan.NavigateUrl = "~/" + module.ModuleCode.ToLower() + "/ImprovementPlanReview.aspx?" + 
                    Constants.QUERYSTRING_PHASE + "=1&" + Constants.QUERYSTRING_CYCLEID + "=" + prev.ParticipantModuleCycleID.ToString();


                // Abstract Data
                LiteralP3AbstractCount.Text = countAbstract.ToString();
                if (ctx.ParticipantGroupID != 0) //user belong to a group
                    LiteralP3AbstractTarget.Text = ctx.MinAbstractsRequired.ToString();
                else
                    LiteralP3AbstractTarget.Text = module.MinAbstractsRequired.ToString();
                LiteralP3AbstractTargetDate.Text = (cycle.AbstractDataTargetDate != null) ? cycle.AbstractDataTargetDate.Value.ToString("d") : "";
                LiteralP3AbstractCompleteDate.Text = (cycle.AbstractDataCompletedDate != null) ? cycle.AbstractDataCompletedDate.Value.ToString("d") : "";
                ImageP3Abstract.ImageUrl = "~/images/" + module.ModuleCode.ToLower() + "/complete_" + cycle.AbstractDataComplete.ToString() + ".png";

                // Participant Profile
                LiteralP3ProfileTargetDate.Text = (cycle.ParticipantProfileTargetDate != null) ? cycle.ParticipantProfileTargetDate.Value.ToString("d") : "";
                LiteralP3ProfileCompleteDate.Text = (cycle.ParticipantProfileCompetedDate != null) ? cycle.ParticipantProfileCompetedDate.Value.ToString("d") : "";
                if (cycle.ParticipantProfileComplete) LiteralP3ProfileCount.Text = "1";
                ImageP3Profile.ImageUrl = "~/images/" + module.ModuleCode.ToLower() + "/complete_" + cycle.ParticipantProfileComplete.ToString() + ".png";

                // Current Practice System Analysis
                LiteralP3SystemAnalysisTargetDate.Text = (cycle.ParticipantProfileTargetDate != null) ? cycle.ParticipantProfileTargetDate.Value.ToString("d") : "";
                LiteralP3SystemAnalysisCompletedDate.Text = (cycle.PracticeSystemAnalysisCompletedDate != null) ? cycle.PracticeSystemAnalysisCompletedDate.Value.ToString("d") : "";
                if (cycle.PracticeSystemAnalysisCompleted==true) LiteralP3SystemAnalysisCount.Text = "1";
                if (cycle.PracticeSystemAnalysisCompleted == null) cycle.PracticeSystemAnalysisCompleted = false;
                ImageP3SystemAnalysis.ImageUrl = "~/images/" + module.ModuleCode.ToLower() + "/complete_" + cycle.PracticeSystemAnalysisCompleted.ToString() + ".png";

                // Review/Approve Report    
                LiteralP3ReviewReportTargetDate.Text = (cycle.ReportApprovedTargetDate != null) ? cycle.ReportApprovedTargetDate.Value.ToString("d") : "";
                LiteralP3ReviewReportCompleteDate.Text = (cycle.ReportApprovedCompletedDate != null) ? cycle.ReportApprovedCompletedDate.Value.ToString("d") : "";
                ImageP3ReviewReport.ImageUrl = "~/images/" + module.ModuleCode.ToLower() + "/complete_" + cycle.ReportApproved.ToString() + ".png";

                // P2 (dates only)
                LiteralP4ImpactTargetDate.Text = (cycle.ImpactAssessmentTargetDate != null) ? cycle.ImpactAssessmentTargetDate.Value.ToString("d") : "";
                LiteralP4SubmitModuleTargetDate.Text = (cycle.ModuleCompleteTargetDate != null) ? cycle.ModuleCompleteTargetDate.Value.ToString("d") : "";

                // Impact assessment complete 
                ImageP4Impact.ImageUrl = "~/images/" + module.ModuleCode.ToLower() + "/complete_" + cycle.ImpactAssessmentComplete.ToString() + ".png";
                ImageP4SubmitCompletedModule.ImageUrl = "~/images/" + module.ModuleCode.ToLower() + "/complete_" + cycle.ModuleComplete.ToString() + ".png";

                // Initialize the URLs for the links that are active.
                //ECG if (!cycle.SelfAssessmentSurveyComplete) LinkP3Survey.NavigateUrl = "~/" + module.ModuleCode.ToLower() + "/SelfAssessmentStart.aspx";
                if (!cycle.ReportApproved) LinkP3Abstract.NavigateUrl = "~/" + module.ModuleCode.ToLower() + "/AbstractList.aspx";
                if (!cycle.ReportApproved) LinkP3Profile.NavigateUrl = "~/" + module.ModuleCode.ToLower() + "/ParticipantProfile.aspx";
                if (!cycle.ReportApproved) LinkP3SystemAnalysis.NavigateUrl = "~/" + module.ModuleCode.ToLower() + "/CurrentPracticeSystemAnalysis.aspx";
                if (!cycle.ReportApproved && abstractRequired &&
                    cycle.ParticipantProfileComplete && (bool)cycle.PracticeSystemAnalysisCompleted) 
                {
                    LinkP3ReviewReport.NavigateUrl = "~/" + module.ModuleCode.ToLower() + "/PerformanceReport.aspx";
                }

                if (cycle.ReportApproved)
                {

                    LiteralP3ReviewReportCount.Text = "1";
                    LiteralP3ReviewReportCompleteDate.Text = cycle.ReportApprovedCompletedDate.Value.ToString("d");
                    LinkP4ViewReport.NavigateUrl = "~/" + module.ModuleCode.ToLower() + "/PerformanceReport.aspx";

                    if (cycle.ImpactAssessmentComplete)
                    {
                        LiteralP4ImpactCount.Text = "1";
                        LiteralP4ImpactCompleteDate.Text = cycle.ImpactAssessmentCompletedDate.Value.ToString("d");
                        LinkP4VeiwImpact.NavigateUrl = "~/" + module.ModuleCode.ToLower() + "/ImpactStatement.aspx";
                        if (cycle.ModuleComplete)
                        {
                            LiteralP4SubmitModuleCompleteDate.Text = cycle.ModuleCompleteDate.Value.ToString("d");
                            LiteralP4SubmitModuleCount.Text = "1";
                            if (cycle.Phase4CreditsClaimed == true)
                            {
                                ButtonP4Credits.Visible = false;
                                LiteralP4Credits.Text = "Credits for Completion Claimed on " + cycle.Phase4CreditsClaimedDate.Value.ToString("d");

                                // Show the button to start a new module along with the separator. 
                                TableRowP4StartNewModule.Visible = true;
                                TableRowP4StartNewModuleSeparator.Visible = true;

                                string showStartNewModuleAlert = (string)Session[Constants.SESSION_START_NEW_MODULE_ALERT];
                                if (showStartNewModuleAlert == null || showStartNewModuleAlert == Constants.SESSION_Y)
                                {
                                    Session[Constants.SESSION_START_NEW_MODULE_ALERT] = Constants.SESSION_N;
                                    PanelWindowOnLoad.Visible = true;
                                }
                            }
                            else 
                            {
                                ButtonP4Credits.Enabled = true;
                            }
                        }
                        else
                        {
                            LinkP4SubmitCompletedModule.NavigateUrl = "~/" + module.ModuleCode.ToLower() + "/SubmitCompletedModule.aspx";
                        }
                    }
                    else
                    {
                        LinkP4Impact.NavigateUrl = "~/" + module.ModuleCode.ToLower() + "/ImpactStatement.aspx";
                    }
                }
                if (ctx.ParticipantGroupID != 0) //user belong to a group?
                {
                    if (ctx.ParticipantID != ctx.LeaderParticipantID)
                    {
                        LinkP3ReviewReport.Text = "Review Report";
                        
                    }
                }
            }
            else // Phase1 starts here...
            {

                // Abstract Data
                LiteralP1AbstractCount.Text = countAbstract.ToString();
                if (ctx.ParticipantGroupID != 0) //user belong to a group
                    LiteralP1AbstractTarget.Text = ctx.MinAbstractsRequired.ToString();
                else
                    LiteralP1AbstractTarget.Text = module.MinAbstractsRequired.ToString();
                LiteralP1AbstractTargetDate.Text = (cycle.AbstractDataTargetDate != null) ? cycle.AbstractDataTargetDate.Value.ToString("d") : "";
                LiteralP1AbstractCompleteDate.Text = (cycle.AbstractDataCompletedDate != null) ? cycle.AbstractDataCompletedDate.Value.ToString("d") : "";
                ImageP1Abstract.ImageUrl = "~/images/" + module.ModuleCode.ToLower() + "/complete_" + cycle.AbstractDataComplete.ToString() + ".png";

                // Participant Profile
                LiteralP1ProfileTargetDate.Text = (cycle.ParticipantProfileTargetDate != null) ? cycle.ParticipantProfileTargetDate.Value.ToString("d") : "";
                LiteralP1ProfileCompleteDate.Text = (cycle.ParticipantProfileCompetedDate != null) ? cycle.ParticipantProfileCompetedDate.Value.ToString("d") : "";
                if (cycle.ParticipantProfileComplete) LiteralP1ProfileCount.Text = "1";
                ImageP1Profile.ImageUrl = "~/images/" + module.ModuleCode.ToLower() + "/complete_" + cycle.ParticipantProfileComplete.ToString() + ".png";


                // Current Practice System Analysis
                LiteralP1SystemAnalysisTargetDate.Text = (cycle.ParticipantProfileTargetDate != null) ? cycle.ParticipantProfileTargetDate.Value.ToString("d") : "";
                LiteralP1SystemAnalysisCompletedDate.Text = (cycle.PracticeSystemAnalysisCompletedDate != null) ? cycle.PracticeSystemAnalysisCompletedDate.Value.ToString("d") : "";
                if (cycle.PracticeSystemAnalysisCompleted == true) LiteralP1SystemAnalysisCount.Text = "1";
                if (cycle.PracticeSystemAnalysisCompleted == null) cycle.PracticeSystemAnalysisCompleted = false;
                ImageP1SystemAnalysis.ImageUrl = "~/images/" + module.ModuleCode.ToLower() + "/complete_" + cycle.PracticeSystemAnalysisCompleted.ToString() + ".png";

                // Review/Approve Report
                LiteralP1ReviewReportTargetDate.Text = (cycle.ReportApprovedTargetDate != null) ? cycle.ReportApprovedTargetDate.Value.ToString("d") : "";
                LiteralP1ReviewReportCompleteDate.Text = (cycle.ReportApprovedCompletedDate != null) ? cycle.ReportApprovedCompletedDate.Value.ToString("d") : "";
                ImageP1ReviewReport.ImageUrl = "~/images/" + module.ModuleCode.ToLower() + "/complete_" + cycle.ReportApproved.ToString() + ".png";

                // P2 (dates only)
                LiteralP2DevelopPlanTargetDate.Text = (cycle.ImprovementPlanTargetDate != null) ? cycle.ImprovementPlanTargetDate.Value.ToString("d") : "";
                LiteralP2MeasuresTargetDate.Text = (cycle.MeasuresSelectedTargetDate != null) ? cycle.MeasuresSelectedTargetDate.Value.ToString("d") : "";

                // Initialize the URLs for the links that are active.
                //ECG if (!cycle.SelfAssessmentSurveyComplete) LinkP1Survey.NavigateUrl = "~/" + module.ModuleCode.ToLower() + "/SelfAssessmentStart.aspx";
                if (!cycle.ReportApproved) LinkP1Abstract.NavigateUrl = "~/" + module.ModuleCode.ToLower() + "/AbstractList.aspx";
                if (!cycle.ReportApproved) LinkP1Profile.NavigateUrl = "~/abo/ParticipantProfile.aspx";
                if (!cycle.ReportApproved) LinkP1SystemAnalysis.NavigateUrl = "~/" + module.ModuleCode.ToLower() + "/CurrentPracticeSystemAnalysis.aspx";
                if (!cycle.ReportApproved && abstractRequired &&
                    cycle.ParticipantProfileComplete && (bool)cycle.PracticeSystemAnalysisCompleted) 
                {
                    LinkP1ReviewReport.NavigateUrl = "~/" + module.ModuleCode.ToLower() + "/PerformanceReport.aspx";
                }

                // If the report is already approved show the "view" link
                if (cycle.ReportApproved)
                {
                    LiteralP1ReviewReportCount.Text = "1";
                    if (cycle.MeasuresSelected)
                    {
                        LiteralP2MeasuresCount.Text = "1";
                        LiteralP2MeasuresCompleteDate.Text = cycle.MeasuresSelectedCompletedDate.Value.ToString("d");
                    }
                    if (!cycle.MeasuresSelected) LinkP2Measures.NavigateUrl = "~/" + module.ModuleCode.ToLower() + "/SelectMeasures.aspx";
                    LinkP2ViewReport.NavigateUrl = "~/" + module.ModuleCode.ToLower() + "/PerformanceReport.aspx?" +
                        Constants.QUERYSTRING_ACTION + "=" + Constants.QUERYSTRING_ACTION_VIEW;

                    string ImprovementPage = "ImprovementPlanPartI.aspx"; ;
                    switch (cycle.ImprovementPlanPartCompleted)
                    {
                        case Constants.ASN_IMPROVEMENTPLAN_PARTI:
                            ImprovementPage = "ImprovementPlanPartII.aspx";
                            break;
                        case Constants.ASN_IMPROVEMENTPLAN_PARTII:
                            ImprovementPage = "ImprovementPlanPartIII.aspx";
                            break;
                        case Constants.ASN_IMPROVEMENTPLAN_PARTIII:
                            ImprovementPage = "ImprovementPlanPartIV.aspx";
                            break;
                        case Constants.ASN_IMPROVEMENTPLAN_PARTIV:
                            ImprovementPage = "ImprovementPlanPartV.aspx";
                            break;
                        case Constants.ASN_IMPROVEMENTPLAN_PARTV:
                            ImprovementPage = "ImprovementPlanReview.aspx";
                            break;

                    }

                    if (cycle.ImprovementPlanComplete)
                    {
                        LinkP2ViewPlan.NavigateUrl = "~/" + module.ModuleCode.ToLower() + "/" + ImprovementPage;
                        LiteralP2DevelopPlanCompleteDate.Text = cycle.ImprovementPlanCompletedDate.Value.ToString("d");
                        LiteralP2DevelopPlanCount.Text = "1";
                    }
                    else
                    {
                        LinkP2DevelopPlan.NavigateUrl = "~/" + module.ModuleCode.ToLower() + "/" + ImprovementPage;
                    }
                }
                if (ctx.ParticipantGroupID != 0) //user belong to a group?
                {
                    if (ctx.ParticipantID != ctx.LeaderParticipantID)
                    {
                        LinkP1ReviewReport.Text = "Review Report";
                        LinkP2Measures.NavigateUrl = "";
                        LinkP2DevelopPlan.NavigateUrl = "";
                    }
                }
            }
        }
    }

    protected void ButtonP1Credits_Click(object sender, EventArgs e)
    {
        using (NetHealthPIMEntities pim = new NetHealthPIMEntities())
        {
            ParticipantModuleCycle cycle = pim.ParticipantModuleCycle.First(pmc => pmc.ParticipantModuleCycleID == ctx.ActiveModuleCycleID);
            if (cycle.Phase2Flag.Value == true)
            {
                cycle.PrevParticipantModuleCycleReference.Load();
                cycle = pim.ParticipantModuleCycle.First(pmc => pmc.ParticipantModuleCycleID == cycle.PrevParticipantModuleCycle.ParticipantModuleCycleID);
            }
            cycle.Phase1CreditsClaimed = true;
            cycle.Phase1CreditsClaimedDate = DateTime.Now;
            cycle.LastUpdateDate = DateTime.Now;
            pim.SaveChanges();
        }
    }

    protected void ButtonP2Credits_Click(object sender, EventArgs e)
    {
        using (NetHealthPIMEntities pim = new NetHealthPIMEntities())
        {
            ParticipantModuleCycle cycle = pim.ParticipantModuleCycle.First(pmc => pmc.ParticipantModuleCycleID == ctx.ActiveModuleCycleID);
            if (cycle.Phase2Flag.Value == true)
            {
                cycle.PrevParticipantModuleCycleReference.Load();
                cycle = pim.ParticipantModuleCycle.First(pmc => pmc.ParticipantModuleCycleID == cycle.PrevParticipantModuleCycle.ParticipantModuleCycleID);
            }
            cycle.Phase2CreditsClaimed = true;
            cycle.Phase2CreditsClaimedDate = DateTime.Now;
            cycle.LastUpdateDate = DateTime.Now;
            pim.SaveChanges();
        }
    }

    protected void ButtonP3Credits_Click(object sender, EventArgs e)
    {
        using (NetHealthPIMEntities pim = new NetHealthPIMEntities())
        {
            ParticipantModuleCycle cycle = pim.ParticipantModuleCycle.First(pmc => pmc.ParticipantModuleCycleID == ctx.ActiveModuleCycleID);
            cycle.Phase3CreditsClaimed = true;
            cycle.Phase3CreditsClaimedDate = DateTime.Now;
            cycle.LastUpdateDate = DateTime.Now;
            pim.SaveChanges();
        }
    }

    protected void ButtonP4Credits_Click(object sender, EventArgs e)
    {
        using (NetHealthPIMEntities pim = new NetHealthPIMEntities())
        {
            ParticipantModuleCycle cycle = pim.ParticipantModuleCycle.First(pmc => pmc.ParticipantModuleCycleID == ctx.ActiveModuleCycleID);
            cycle.Phase4CreditsClaimed = true;
            cycle.Phase4CreditsClaimedDate = DateTime.Now;
            cycle.LastUpdateDate = DateTime.Now;
            ButtonP4Credits.Visible = false;
            LiteralP4Credits.Text = "Credits for Completion Claimed on " + DateTime.Now; //cycle.Phase3CreditsClaimedDate.Value.ToString("d");
            pim.SaveChanges();
        }
    }

    protected void ButtonP4StartNewModule_Click(object sender, EventArgs e)
    {
        using (NetHealthPIMEntities pim = new NetHealthPIMEntities())
        {
            ParticipantModuleCycle cycle = pim.ParticipantModuleCycle.First(pmc => pmc.ParticipantModuleCycleID == ctx.ActiveModuleCycleID);
            cycle.LastUpdateDate = DateTime.Now;
            cycle.Phase2Flag = false;
            pim.SaveChanges();
        }
        Response.Redirect("~/" + module.ModuleCode.ToLower() + "/MyPI.aspx");
    }
}
