﻿<%@ Page Title="" Language="C#" MasterPageFile="~/PIMMasterPage.master" AutoEventWireup="true" CodeFile="CSUChart.aspx.cs" Inherits="abo_CSUChart" %>

<asp:Content ID="Content1" ContentPlaceHolderID="head" Runat="Server">

    <link type="text/css" href="../../common/css/atooltip.css" rel="stylesheet"  media="screen" />
	
    <style type="text/css">
        
        .right {
            text-align:right;
        }
    </style>
    
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder1" Runat="Server">
    <asp:HiddenField ID="HiddenFieldRecordIdentifier" runat="server"/>
        <!--  pim -->
        <div class="pim">
            <div class="record-ident clearfix">
                <h3 class="record-first">RECORD IDENTIFIER: <asp:Literal runat="server" ID="LiteralRecordIdentifier"></asp:Literal></h3>
                <h3 class="record-second"><asp:Literal runat="server" ID="LiteralAbstractionNumber"></asp:Literal></h3>
            </div>
            <!-- History -->
            <table>
                <tr>
                    <th colspan="3" width="910px"><p>History</p></th>
                </tr>
                <!-- Question 1 -->
                <tr class="table_body">
                    <td colspan="2">
                        <strong>1. Date of birth</strong>
                        <asp:Label ID="LabelMonthOfBirth" runat="server" Visible="false"  ForeColor="Red" Text="<br />Please complete month" />
                        <asp:Label ID="LabelYearOfBirth" runat="server" Visible="false"  ForeColor="Red" Text="<br />Please complete year" />
                    </td>
                    <td>
                        <asp:DropDownList ID='DropDownListMonthOfBirth' DataValueField='MonthID' DataTextField='MonthName' runat='server' />
                        <asp:DropDownList ID='DropDownListYearOfBirth' DataValueField='YearID' DataTextField='YearName' runat='server' />
                    </td>
                </tr>
                <!-- Question 2 -->
                <tr class="table_body_bg">
                    <td colspan="2">
                        <strong>2. Date of onset of uveitis (diagnosis or first episode)</strong>
                        <asp:Label ID="LabelMonthOfOnsetFirstEpisode" runat="server" Visible="false"  ForeColor="Red" Text="<br />Please complete" />
                        <asp:Label ID="LabelYearOfOnsetFirstEpisode" runat="server" Visible="false"  ForeColor="Red" Text="<br />Please complete" />
                    </td>
                    <td>
                        <asp:DropDownList ID='DropDownListMonthOfOnsetFirstEpisode' DataValueField='MonthID' DataTextField='MonthName' runat='server' />
                        <asp:DropDownList ID='DropDownListYearOfOnsetFirstEpisode' DataValueField='YearID' DataTextField='YearName' runat='server' />
                    </td>
                </tr>
                <!-- Question 3 -->
                <tr class="table_body">
                    <td colspan="2">
                        <strong>3. Date of cataract surgery for this eye</strong>
                        <asp:Label ID="LabelMonthOfCataractSurgery" runat="server" Visible="false"  ForeColor="Red" Text="<br />Please complete" />
                        <asp:Label ID="LabelYearOfCataractSurgery" runat="server" Visible="false"  ForeColor="Red" Text="<br />Please complete" />

                    </td>
                    <td>
                        <asp:DropDownList ID='DropDownListMonthOfCataractSurgery' DataValueField='MonthID' DataTextField='MonthName' runat='server' />
                        <asp:DropDownList ID='DropDownListYearOfCataractSurgery' DataValueField='YearID' DataTextField='YearName' runat='server' />
                    </td>
                </tr>
                <!-- Question 4 -->
                <tr class="table_body_bg">
                    <td colspan="2">
                        <strong>4. Gender</strong>
                        <asp:Label ID="LabelRBGender" runat="server" Visible="false"  ForeColor="Red" Text="<br />Please complete" />    
                    </td>
                    <td>
                        <asp:RadioButtonList id='RadioButtonListRBGender' RepeatDirection='Horizontal' CssClass='aspxList' runat='server'>
                            <asp:ListItem Value='42'>&nbsp;Male</asp:ListItem>
                            <asp:ListItem Value='43'>&nbsp;Female</asp:ListItem>
                        </asp:RadioButtonList>
                    </td>
                </tr>
                <!-- Question 5 -->
                <tr class="table_body">
                    <td colspan="2"><strong>5. Symptoms</strong></td>
                    <td>
                        <asp:CheckBox runat='server' ID='CheckBoxPresentingSymptomDecreasedVision' text='&nbsp;Decreased vision' />
                        <br /><asp:CheckBox runat='server' ID='CheckBoxPresentingSymptomGlare' text='&nbsp;Glare' />
                        <br /><asp:CheckBox runat='server' ID='CheckBoxPresentingSymptomDailyLiving' text='&nbsp;Difficulty with activities of daily living' />
                        <br /><asp:CheckBox runat='server' ID='CheckBoxPresentingSymptomVisualizePosterior' text='&nbsp;Inability to visualize posterior segment' />
                        <br /><asp:CheckBox runat='server' ID='CheckBoxPresentingSymptomOther' text='&nbsp;Other' />
                    </td>
                </tr>
                <!-- Question 6 -->
                <tr class="table_body_bg">
                    <td width="50%" rowspan="2"><strong>6. Uveitis</strong></td>
                    <td width="20%"><span class="right">Anatomic classification</span></td>
                    <td>
                        <asp:CheckBox runat='server' ID='CheckBoxUveitisAnterior' text='&nbsp;Anterior' />
                        <br /><asp:CheckBox runat='server' ID='CheckBoxUveitisIntermediate' text='&nbsp;Intermediate' />
                        <br /><asp:CheckBox runat='server' ID='CheckBoxUveitisPosterior' text='&nbsp;Posterior' />
                        <br /><asp:CheckBox runat='server' ID='CheckBoxUveitisPanuveitis' text='&nbsp;Panuveitis' />
                    </td>
                </tr>
                <tr class="table_body_bg">
                    <td><span class="right">Course</span></td>
                    <td>
                        <asp:CheckBox runat='server' ID='CheckBoxUveitisRecurrent' text='&nbsp;Recurrent (more than 3 months off meds before relapse)' />
                        <br /><asp:CheckBox runat='server' ID='CheckBoxUveitisCourse' text='&nbsp;Chronic (&le;3 months off medical before relapse)' />
                    </td>
                </tr>
                <!-- Question 7 -->
                <tr class="table_body">
                    <td colspan="2">
                        <strong>7. How long has the disease been quiescent?</strong>
                        <asp:Label ID="LabelDiseaseDurationMonths" runat="server" Visible="false"  ForeColor="Red" Text="<br />Please complete" />
                    </td>
                    <td>
                        <asp:TextBox runat="server" ID="TextBoxDiseaseDurationMonths" size="5" />&nbsp;weeks
                    </td>
                </tr>
                <!-- Question 8 -->
                <tr class="table_body_bg">
                    <td colspan="2"><strong>8. Etiology of uveitis</strong></td>
                    <td>
                        <asp:CheckBox runat='server' ID='CheckBoxEtiologyFuchs' text='&nbsp;Fuchs Heterochromic Iridocyclitis' />
                        <br /><asp:CheckBox runat='server' ID='CheckBoxEtiologyHLA' text='&nbsp;HLA B-27 +' />
                        <br /><asp:CheckBox runat='server' ID='CheckBoxEtiologySarcoidosis' text='&nbsp;Sarcoidosis' />
                        <br /><asp:CheckBox runat='server' ID='CheckBoxEtiologyPars' text='&nbsp;Pars Planitis' />
                        <br /><asp:CheckBox runat='server' ID='CheckBoxEtiologyScleritisRA' text='&nbsp;Scleritis Associated RA' />
                        <br /><asp:CheckBox runat='server' ID='CheckBoxEtiologyCMVRetinitis' text='&nbsp;CMV Retinitis with Immune Recovery Uveitis' />
                        <br /><asp:CheckBox runat='server' ID='CheckBoxEtiologyJIA' text='&nbsp;JIA' />
                        <br /><asp:CheckBox runat='server' ID='CheckBoxEtiologyIdiopathic' text='&nbsp;Idiopathic' />
                        <br /><asp:CheckBox runat='server' ID='CheckBoxEtiologyOther' text='&nbsp;Other' />
                    </td>
                </tr>
                <!-- Question 9 -->
                <tr class="table_body">
                    <td colspan="2"><strong>9. What therapy has been utilized to achieve disease control to date?</strong></td>
                    <td>
                        <asp:CheckBox runat='server' ID='CheckBoxTherapyToDateCorticosteroids' text='&nbsp;Corticosteroids' />
                        <span style="margin-left:20px;" id="QH9A">
                            <br />Route:
                            <br /><asp:CheckBox runat='server' ID='CheckBoxTherapyToDateCSTopical' text='&nbsp;Topical' />
                            <br /><asp:CheckBox runat='server' ID='CheckBoxTherapyToDateCSPeriocular' text='&nbsp;Periocular' />
                            <br /><asp:CheckBox runat='server' ID='CheckBoxTherapyToDateCSSystemic' text='&nbsp;Systemic' />
                            <br /><asp:CheckBox runat='server' ID='CheckBoxTherapyToDateCSIntravitreal' text='&nbsp; Intravitreal (device or injection)' />
                        </span>
                        <br />
                        <br /><asp:CheckBox runat='server' ID='CheckBoxTherapyToDateImmu' text='&nbsp;Immunomodulatory Therapy' />
                        <br /><span id="QH9B">Name of drug:&nbsp;<asp:TextBox runat='server' ID='TextBoxTherapyToDateImmuName' size='25' /></span>
                    </td>
                </tr>
                <!-- Question 10 -->
                <tr class="table_body_bg">
                    <td colspan="2">
                        <strong>10. Glaucoma</strong>
                        <asp:Label ID="LabelHistoryGlaucoma" runat="server" Visible="false"  ForeColor="Red" Text="<br />Please complete" />
                    </td>
                    <td>
                        <asp:RadioButton runat='server' ID='RadioButtonHistoryGlaucomaYes' GroupName='HistoryGlaucoma' text='&nbsp;Yes' />&nbsp;&nbsp;
                        <asp:RadioButton runat='server' ID='RadioButtonHistoryGlaucomaNo' GroupName='HistoryGlaucoma' text='&nbsp;No' />
                    </td>
                </tr>
                <!-- Question 11 -->
                <tr class="table_body">
                    <td colspan="2">
                        <strong>11. Previous glaucoma surgery?</strong>
                        <asp:Label ID="LabelHistoryGlaucomaSurgery" runat="server" Visible="false"  ForeColor="Red" Text="<br />Please complete" />
                    </td>
                    <td>
                        <asp:RadioButton runat='server' ID='RadioButtonHistoryGlaucomaSurgeryYes' GroupName='HistoryGlaucomaSurgery' text='&nbsp;Yes' />&nbsp;&nbsp;
                        <asp:RadioButton runat='server' ID='RadioButtonHistoryGlaucomaSurgeryNo' GroupName='HistoryGlaucomaSurgery' text='&nbsp;No' />
                    </td>
                </tr>
            </table>
            <br />
            <!-- Clinical Examination Finding in Affected Eye -->
            <table>
                <tr>
                    <th colspan="3" width="910px"><p>Clinical Examination Finding in Affected Eye</p></th>
                </tr>
                <!-- Question 1 -->
                     <tr class="table_body_bg">
                    <td colspan="2"><strong>1. Which is affected eye?</strong></td>
                    <td>
                        <asp:RadioButtonList ID="RadioButtonListAffectedEye" CssClass='aspxList' RepeatDirection="Horizontal" runat="server">
                        <asp:ListItem Value="108">Right eye</asp:ListItem>
                        <asp:ListItem Value="109">Left eye</asp:ListItem>
                        </asp:RadioButtonList>
                    </td>
                </tr>
                <tr class="table_body">
                    <td colspan="2"><strong>2. Best corrected visual acuity?</strong>&nbsp;<a href="#"><img id="QCE2" src="../../common/images/tip.gif" alt="Tip" /></a></td>
                    <td>
                        Snellen equivalent: 20 /&nbsp;<asp:DropDownList ID='DropDownListBCVASnellen' DataValueField='examValue' DataTextField='examLabel' runat='server' />
                    </td>
                </tr>
                <!-- Question 2 -->
                <tr class="table_body_bg">
                    <td colspan="2">
                        <strong>3. Cornea</strong>
                        <asp:Label ID="LabelRBExamcornea" runat="server" Visible="false"  ForeColor="Red" Text="<br />Please complete" />    
                    </td>
                    <td> 
                        <asp:RadioButtonList id='RadioButtonListRBExamcornea' RepeatDirection='Vertical' CssClass='aspxList' runat='server'>
                            <asp:ListItem Value='104'>&nbsp;Normal</asp:ListItem>
                            <asp:ListItem Value='105'>&nbsp;Abnormal</asp:ListItem>
                            <asp:ListItem Value='132'>&nbsp;Not Examined</asp:ListItem>
                        </asp:RadioButtonList>
                    </td>
                </tr>
                <!-- Question 2a -->
                <tr class="table_body_bg">
                    <td colspan="2">
                        <strong><span id="QC2aA">3a. Calcific band keratopathy</span></strong>
                        <asp:Label ID="LabelRBExamCorneaKeratopathy" runat="server" Visible="false"  ForeColor="Red" Text="<br />Please complete" />
                    </td>
                    <td>
                        <span id="QC2aB">
                        <asp:RadioButtonList id='RadioButtonListRBExamCorneaKeratopathy' RepeatDirection='Horizontal' CssClass='aspxList' runat='server'>
                            <asp:ListItem Value="1">&nbsp;Yes</asp:ListItem>
	                        <asp:ListItem Value="2">&nbsp;No</asp:ListItem>
	                        <asp:ListItem Value="3">&nbsp;Not recorded</asp:ListItem>
                        </asp:RadioButtonList>
                        </span>
                    </td>
                </tr>
                <!-- Question 2b -->
                <tr class="table_body_bg">
                    <td colspan="2">
                        <strong><span id="QC2bA">3b. Stromal scars</span></strong>
                        <asp:Label ID="LabelRBExamCorneaScars" runat="server" Visible="false"  ForeColor="Red" Text="<br />Please complete" />
                    </td>
                    <td>
                        <span id="QC2bB">
                        <asp:RadioButtonList id='RadioButtonListRBExamCorneaScars' RepeatDirection='Horizontal' CssClass='aspxList' runat='server'>
                            <asp:ListItem Value="1">&nbsp;Yes</asp:ListItem>
	                        <asp:ListItem Value="2">&nbsp;No</asp:ListItem>
	                        <asp:ListItem Value="3">&nbsp;Not recorded</asp:ListItem>
                        </asp:RadioButtonList>
                        </span>
                    </td>
                </tr>
                <!-- Question 2c -->
                <tr class="table_body_bg">
                    <td colspan="2">
                        <strong><span id="QC2cA">3c. KPs</span></strong>
                        <asp:Label ID="LabelRBKPs" runat="server" Visible="false"  ForeColor="Red" Text="<br />Please complete" />
                    </td>
                    <td>
                        <span id="QC2cB">
                        <asp:RadioButtonList id='RadioButtonListRBKPs' RepeatDirection='Horizontal' CssClass='aspxList' runat='server'>
                            <asp:ListItem Value="1">&nbsp;Yes</asp:ListItem>
	                        <asp:ListItem Value="2">&nbsp;No</asp:ListItem>
	                        <asp:ListItem Value="3">&nbsp;Not recorded</asp:ListItem>
                        </asp:RadioButtonList>
                        </span>
                    </td>
                </tr>
                <!-- Question 2d -->
                <tr class="table_body_bg">
                    <td colspan="2">
                        <strong><span id="QC2dA">3d. Appearance</span></strong>
                        <asp:Label ID="LabelRBKPsAppearance" runat="server" Visible="false"  ForeColor="Red" Text="<br />Please complete" />
                    </td>
                    <td>
                        <span id="QC2dB">
                        <asp:RadioButtonList id='RadioButtonListRBKPsAppearance' RepeatDirection='Vertical' CssClass='aspxList' runat='server'>
                            <asp:ListItem Value='159'>&nbsp;Granulomatous</asp:ListItem>
                            <asp:ListItem Value='160'>&nbsp;Fine</asp:ListItem>
                            <asp:ListItem Value='161'>&nbsp;Pigmented</asp:ListItem>
                            <asp:ListItem Value='128'>&nbsp;None</asp:ListItem>
                        </asp:RadioButtonList>
                        </span>
                    </td>
                </tr>
                <!-- Question 2e -->
                <tr class="table_body_bg">
                    <td colspan="2">
                        <strong><span id="QC2eA">3e. Location</span></strong>
                        <asp:Label ID="LabelRBKPsLocation" runat="server" Visible="false"  ForeColor="Red" Text="<br />Please complete" />
                    </td>
                    <td>
                        <span id="QC2eB">
                        <asp:RadioButtonList id='RadioButtonListRBKPsLocation' RepeatDirection='Vertical' CssClass='aspxList' runat='server'>
                            <asp:ListItem Value='162'>&nbsp;Inferior (Arlt's Triangle)</asp:ListItem>
                            <asp:ListItem Value='163'>&nbsp;Diffuse</asp:ListItem>
                        </asp:RadioButtonList>
                        </span>
                    </td>
                </tr>
                <!-- Question 3 -->
                <tr class="table_body">
                    <td rowspan="2" width="50%">
                        <strong>4. Anterior chamber</strong>
                    </td>
                    <td width="20%">
                        <span class="right">Flare description by sun criteria</span>
                        <asp:Label ID="LabelRBAnteriorFlare" runat="server" Visible="false"  ForeColor="Red" Text="<br />Please complete" />
                    </td>
                    <td>
                        <asp:RadioButtonList id='RadioButtonListRBAnteriorFlare' RepeatDirection='Vertical' CssClass='aspxList' runat='server'>
                            <asp:ListItem Value='168'>&nbsp;-0 None</asp:ListItem>
                            <asp:ListItem Value='169'>&nbsp;1+ Faint</asp:ListItem>
                            <asp:ListItem Value='170'>&nbsp;2+ Moderate Iris &amp; Lens Clear</asp:ListItem>
                            <asp:ListItem Value='171'>&nbsp;3+ Marked Iris &amp; Lens Hazy</asp:ListItem>
                            <asp:ListItem Value='172'>&nbsp;4+ Intense Fibrin/Plasmoid Aqueous</asp:ListItem>
                        </asp:RadioButtonList>
                    </td>
                </tr>
                <tr class="table_body">
                    <td>
                        <span class="right">Cells by sun criteria</span>
                        <asp:Label ID="LabelRBAnteriorCells" runat="server" Visible="false"  ForeColor="Red" Text="<br />Please complete" />    
                    </td>
                    <td>
                        <asp:RadioButtonList id='RadioButtonListRBAnteriorCells' RepeatDirection='Vertical' CssClass='aspxList' runat='server'>
                            <asp:ListItem Value='230'>&nbsp;-0 &lt;1 Criteria Cells</asp:ListItem>
                            <asp:ListItem Value='231'>&nbsp;0.5+ 1-5</asp:ListItem>
                            <asp:ListItem Value='232'>&nbsp;1+ 6-15</asp:ListItem>
                            <asp:ListItem Value='233'>&nbsp;2+ 16-25</asp:ListItem>
                            <asp:ListItem Value='234'>&nbsp;3+ 26-50</asp:ListItem>
                            <asp:ListItem Value='235'>&nbsp;4+ &gt;50</asp:ListItem>
                        </asp:RadioButtonList>
                    </td>
                </tr>
                <!-- Question 4 -->
                <tr class="table_body_bg">
                    <td colspan="2">
                        <strong>5. Iris</strong>
                        <asp:Label ID="LabelRBIris" runat="server" Visible="false"  ForeColor="Red" Text="<br />Please complete" />
                    </td>
                    <td>
                        <asp:RadioButtonList id='RadioButtonListRBIris' RepeatDirection='Vertical' CssClass='aspxList' runat='server'>
                            <asp:ListItem Value='104'>&nbsp;Normal</asp:ListItem>
                            <asp:ListItem Value='105'>&nbsp;Abnormal</asp:ListItem>
                            <asp:ListItem Value='341'>&nbsp;Atrophy</asp:ListItem>
                            <asp:ListItem Value='14'>&nbsp;Other</asp:ListItem>
                            <asp:ListItem Value='132'>&nbsp;Not Examined</asp:ListItem>
                        </asp:RadioButtonList>
                    </td>
                </tr>
                <!-- Question 4a -->
                <tr class="table_body_bg">
                    <td colspan="2"><strong><span id="QC4aA">5a. Posterior synechiae</span></strong></td>
                    <td>
                        <span id="QC4aB">
                        Extent in clock hours:&nbsp;
                        <asp:TextBox runat="server" ID="TextBoxRBIrisClockHours" size="5" onkeyup="toggleQC4aCB();" />
                        <br /><asp:CheckBox runat='server' ID='CheckBoxIrisBombe' text='&nbsp;Iris bombe' onclick="toggleQC4aTB();" />
                    </td>
                </tr>
                <!-- Question 4b -->
                <tr class="table_body_bg">
                    <td colspan="2">
                        <strong><span id="QC4bA">5b. Neovascularization</span></strong>
                        <asp:Label ID="LabelRBIrisNeovascularization" runat="server" Visible="false"  ForeColor="Red" Text="<br />Please complete" />
                    </td>
                    <td>
                        <span id="QC4bB">
                        <asp:RadioButtonList id='RadioButtonListRBIrisNeovascularization' RepeatDirection='Horizontal' CssClass='aspxList' runat='server'>
                            <asp:ListItem Value='175'>&nbsp;Present</asp:ListItem>
                            <asp:ListItem Value='176'>&nbsp;Absent</asp:ListItem>
                        </asp:RadioButtonList>
                        </span>
                    </td>
                </tr>
                <!-- Question 4c -->
                <tr class="table_body_bg">
                    <td colspan="2">
                        <strong><span id="QC4cA">5c. Pupillary membrane</span></strong>
                        <asp:Label ID="LabelRBPupillaryMembrane" runat="server" Visible="false"  ForeColor="Red" Text="<br />Please complete" />
                    </td>
                    <td>
                        <span id="QC4cB">
                        <asp:RadioButtonList id='RadioButtonListRBPupillaryMembrane' RepeatDirection='Horizontal' CssClass='aspxList' runat='server'>
                            <asp:ListItem Value='175'>&nbsp;Present</asp:ListItem>
                            <asp:ListItem Value='176'>&nbsp;Absent</asp:ListItem>
                        </asp:RadioButtonList>
                        </span>
                    </td>
                </tr>
                <!-- Question 4d -->
                <tr class="table_body_bg">
                    <td colspan="2">
                        <strong><span id="QC4dA">5d. Anterior capsule pigment</span></strong>
                        <asp:Label ID="LabelRBAnteriorCapsule" runat="server" Visible="false"  ForeColor="Red" Text="<br />Please complete" />    
                    </td>
                    <td>
                        <span id="QC4dB">
                        <asp:RadioButtonList id='RadioButtonListRBAnteriorCapsule' RepeatDirection='Horizontal' CssClass='aspxList' runat='server'>
                            <asp:ListItem Value='175'>&nbsp;Present</asp:ListItem>
                            <asp:ListItem Value='176'>&nbsp;Absent</asp:ListItem>
                        </asp:RadioButtonList>
                        </span>
                    </td>
                </tr>
                <!-- Question 4e -->
                <tr class="table_body_bg">
                    <td colspan="2">
                        <strong><span id="QC4eAA">5e. Cataract grade</span></strong>
                        <asp:Label ID="LabelRBCataractGrade" runat="server" Visible="false"  ForeColor="Red" Text="<br />Please complete" />
                    </td>
                    <td>
                        <span id="QC4eAB">
                            <asp:CheckBox ID="CheckBoxNuclear" Text="Nuclear" runat="server" />
                            <br /><asp:CheckBox ID="Cortical" Text="Cortical" runat="server" />
                            <br /><asp:CheckBox ID="Posterior" Text="Posterior Subcapsular" runat="server" />
                        </span>
                        <br /><br />
                        <span id="QC4eA">
                        Nuclear:
                        <asp:RadioButtonList id='RadioButtonListRBCataractGradeNuclear' RepeatDirection='Horizontal' CssClass='aspxList' runat='server'>
                            <asp:ListItem Value='179'>&nbsp;0</asp:ListItem>
                            <asp:ListItem Value='185'>&nbsp;1+</asp:ListItem>
                            <asp:ListItem Value='186'>&nbsp;2+</asp:ListItem>
                            <asp:ListItem Value='290'>&nbsp;3+</asp:ListItem>
                            <asp:ListItem Value='291'>&nbsp;4+</asp:ListItem>
                        </asp:RadioButtonList>
                        </span>
                        <span id="QC4eB">
                        Cortical:
                        <asp:RadioButtonList id='RadioButtonListRBCataractGradeCortical' RepeatDirection='Horizontal' CssClass='aspxList' runat='server'>
                            <asp:ListItem Value='179'>&nbsp;0</asp:ListItem>
                            <asp:ListItem Value='185'>&nbsp;1+</asp:ListItem>
                            <asp:ListItem Value='186'>&nbsp;2+</asp:ListItem>
                            <asp:ListItem Value='290'>&nbsp;3+</asp:ListItem>
                            <asp:ListItem Value='291'>&nbsp;4+</asp:ListItem>
                        </asp:RadioButtonList>
                        </span>
                        <span id="QC4eC">
                        Posterior:
                        <asp:RadioButtonList id='RadioButtonListRBCataractGradePosterior' RepeatDirection='Horizontal' CssClass='aspxList' runat='server'>
                            <asp:ListItem Value='179'>&nbsp;0</asp:ListItem>
                            <asp:ListItem Value='185'>&nbsp;1+</asp:ListItem>
                            <asp:ListItem Value='186'>&nbsp;2+</asp:ListItem>
                            <asp:ListItem Value='290'>&nbsp;3+</asp:ListItem>
                            <asp:ListItem Value='291'>&nbsp;4+</asp:ListItem>
                        </asp:RadioButtonList>
                        </span>
                    </td>
                </tr>
                <!-- Question 4f -->
                <tr class="table_body_bg">
                    <td colspan="2">
                        <strong><span id="QC4fA">5f. Intraocular pressure</span></strong>
                        <asp:Label ID="LabelIOP" runat="server" Visible="false"  ForeColor="Red" Text="<br />Please complete" />
                    </td>
                    <td>
                        <span id="QC4fB">
                        <asp:TextBox runat='server' ID='TextBoxIOP' size='25' /> mmHg
                        </span>
                    </td>
                </tr>
                <!-- Question 4g -->
                <tr class="table_body_bg">
                    <td colspan="2">
                        <strong><span id="QC4gA">5g. Gonioscopy performed</span></strong>
                        <asp:Label ID="LabelGonioscopyPerformed" runat="server" Visible="false"  ForeColor="Red" Text="<br />Please complete" />
                    </td>
                    <td>
                        <span id="QC4gB">
                        <asp:RadioButton runat='server' ID='RadioButtonGonioscopyPerformedYes' GroupName='GonioscopyPerformed' text='&nbsp;Yes' />&nbsp;&nbsp;
                        <asp:RadioButton runat='server' ID='RadioButtonGonioscopyPerformedNo' GroupName='GonioscopyPerformed' text='&nbsp;No' />
                        </span>
                    </td>
                </tr>
                <!-- Question 5 -->
                <tr class="table_body">
                    <td colspan="2">
                        <strong>6. Vitreous cells</strong>
                        <asp:Label ID="LabelVitreousCells" runat="server" Visible="false"  ForeColor="Red" Text="<br />Please complete" />
                    </td>
                    <td>
                        <asp:RadioButton runat='server' ID='RadioButtonVitreousCellsYes' GroupName='VitreousCells' text='&nbsp;Yes' />&nbsp;&nbsp;
                        <asp:RadioButton runat='server' ID='RadioButtonVitreousCellsNo' GroupName='VitreousCells' text='&nbsp;No' />
                        <br /><span id="QC5A">
                        <br />Grade:
                        <asp:RadioButtonList id='RadioButtonListRBVitreousCellsGrade' RepeatDirection='Vertical' CssClass='aspxList' runat='server'>
                            <asp:ListItem Value='179'>&nbsp;0</asp:ListItem>
                            <asp:ListItem Value='184'>&nbsp;0.5+</asp:ListItem>
                            <asp:ListItem Value='185'>&nbsp;1+</asp:ListItem>
                            <asp:ListItem Value='186'>&nbsp;2+</asp:ListItem>
                            <asp:ListItem Value='290'>&nbsp;3+</asp:ListItem>
                            <asp:ListItem Value='291'>&nbsp;4+</asp:ListItem>
                        </asp:RadioButtonList>
                        </span>
                    </td>
                </tr>
                <!-- Question 6 -->
                <tr class="table_body_bg">
                    <td rowspan="3"><strong>7. Optic disc</strong></td>
                    <td>
                        <span class="right">Are glaucomatous changes present?</span>
                        <asp:Label ID="LabelOpticDiscGlaucomatousChanges" runat="server" Visible="false"  ForeColor="Red" Text="<br />Please complete" />
                    </td>
                    <td>
                        <asp:RadioButton runat='server' ID='RadioButtonOpticDiscGlaucomatousChangesYes' GroupName='OpticDiscGlaucomatousChanges' text='&nbsp;Yes' />&nbsp;&nbsp;
                        <asp:RadioButton runat='server' ID='RadioButtonOpticDiscGlaucomatousChangesNo' GroupName='OpticDiscGlaucomatousChanges' text='&nbsp;No' />
                    </td>
                </tr>
                <tr class="table_body_bg">
                    <td>
                        <span class="right">Were other abnormalities noted during the examination?</span>
                        <asp:Label ID="LabelOpticDiscAbnormalities" runat="server" Visible="false"  ForeColor="Red" Text="<br />Please complete" />
                    </td>
                    <td>
                        <asp:RadioButton runat='server' ID='RadioButtonOpticDiscAbnormalitiesYes' GroupName='OpticDiscAbnormalities' text='&nbsp;Yes' />&nbsp;&nbsp;
                        <asp:RadioButton runat='server' ID='RadioButtonOpticDiscAbnormalitiesNo' GroupName='OpticDiscAbnormalities' text='&nbsp;No' />
                    </td>
                </tr>
                <tr class="table_body_bg">
                    <td>
                        <span class="right" id="QC6A">If "Yes," which one has been identified?</span>
                    </td>
                    <td>
                        <span id="QC6B">
                        <asp:RadioButtonList id='RadioButtonListRBOpticDiscAbnormalityType' RepeatDirection='Vertical' CssClass='aspxList' runat='server'>
                            <asp:ListItem Value='266'>&nbsp;Edema</asp:ListItem>
                            <asp:ListItem Value='267'>&nbsp;Neovascularization</asp:ListItem>
                            <asp:ListItem Value='268'>&nbsp;Optic Atrophy</asp:ListItem>
                            <asp:ListItem Value='14'>&nbsp;Other</asp:ListItem>
                        </asp:RadioButtonList>
                        </span>
                        <asp:TextBox runat='server' ID='TextBoxRBOpticDiscAbnormalityOther' size='25' />
                        
                    </td>
                </tr>
                <!-- Question 7 -->
                <tr class="table_body">
                    <td rowspan="4"><strong>8. Retina and choroid</strong></td>
                    <td>
                        <span class="right">Was a dilated fundus exam performed?</span>
                        <asp:Label ID="LabelExamDilatedFundus" runat="server" Visible="false"  ForeColor="Red" Text="<br />Please complete" />    
                    </td>
                    <td>
                        <asp:RadioButton runat='server' ID='RadioButtonExamDilatedFundusYes' GroupName='ExamDilatedFundus' text='&nbsp;Yes' />&nbsp;&nbsp;
                        <asp:RadioButton runat='server' ID='RadioButtonExamDilatedFundusNo' GroupName='ExamDilatedFundus' text='&nbsp;No' />
                    </td>
                </tr>
                <tr class="table_body">
                    <td>
                        <span class="right">Was a fluorescein angiography performed?</span>
                        <asp:Label ID="LabelExamFluorescein" runat="server" Visible="false"  ForeColor="Red" Text="<br />Please complete" />
                    </td>
                    <td>
                        <asp:RadioButton runat='server' ID='RadioButtonExamFluoresceinYes' GroupName='ExamFluorescein' text='&nbsp;Yes' />&nbsp;&nbsp;
                        <asp:RadioButton runat='server' ID='RadioButtonExamFluoresceinNo' GroupName='ExamFluorescein' text='&nbsp;No' />
                    </td>
                </tr>
                <tr class="table_body">
                    <td>
                        <span class="right">Was OCT performed?</span>
                        <asp:Label ID="LabelExamOCT" runat="server" Visible="false"  ForeColor="Red" Text="<br />Please complete" />
                    </td>
                    <td>
                        <asp:RadioButton runat='server' ID='RadioButtonExamOCTYes' GroupName='ExamOCT' text='&nbsp;Yes' />&nbsp;&nbsp;
                        <asp:RadioButton runat='server' ID='RadioButtonExamOCTNo' GroupName='ExamOCT' text='&nbsp;No' />
                    </td>
                </tr>
                <tr class="table_body">
                    <td>
                        <span class="right">Was B-scan performed?</span>
                        <asp:Label ID="LabelExamBscan" runat="server" Visible="false"  ForeColor="Red" Text="<br />Please complete" />
                    </td>
                    <td>
                        <asp:RadioButton runat='server' ID='RadioButtonExamBscanYes' GroupName='ExamBscan' text='&nbsp;Yes' />&nbsp;&nbsp;
                        <asp:RadioButton runat='server' ID='RadioButtonExamBscanNo' GroupName='ExamBscan' text='&nbsp;No' />
                    </td>
                </tr>
                <!-- Question 8 -->
                <tr class="table_body_bg">
                    <td rowspan="3"><strong>9. Pre-operative assessment</strong></td>
                    <td>
                        <span class="right">Is cataract the main cause of visual loss?</span>
                        <asp:Label ID="LabelPreopCataractMainCause" runat="server" Visible="false"  ForeColor="Red" Text="<br />Please complete" />
                    </td>
                    <td>
                        <asp:RadioButton runat='server' ID='RadioButtonPreopCataractMainCauseYes' GroupName='PreopCataractMainCause' text='&nbsp;Yes' />&nbsp;&nbsp;
                        <asp:RadioButton runat='server' ID='RadioButtonPreopCataractMainCauseNo' GroupName='PreopCataractMainCause' text='&nbsp;No' />
                    </td>
                </tr>
                <tr class="table_body_bg">
                    <td>
                        <span class="right">Were there other pre-operative ocular co-morbidities causing visual loss?</span>
                        <asp:Label ID="LabelPreopOcularCoMorbilities" runat="server" Visible="false"  ForeColor="Red" Text="<br />Please complete" />
                    </td>
                    <td>
                        <asp:RadioButton runat='server' ID='RadioButtonPreopOcularCoMorbilitiesYes' GroupName='PreopOcularCoMorbilities' text='&nbsp;Yes' />&nbsp;&nbsp;
                        <asp:RadioButton runat='server' ID='RadioButtonPreopOcularCoMorbilitiesNo' GroupName='PreopOcularCoMorbilities' text='&nbsp;No' />
                    </td>
                </tr>
                <tr class="table_body_bg">
                    <td><span class="right" id="QC8A">If "Yes," please indicate</span></td>
                    <td>
                        <span id="QC8B">
                        <asp:CheckBox runat='server' ID='CheckBoxPreopComorbidEpiretinal' text='&nbsp;Epiretinal Membrane' />
                        <br /><asp:CheckBox runat='server' ID='CheckBoxPreopCormorbidCystoid' text='&nbsp;Cystoid Macular Edema' />
                        <br /><asp:CheckBox runat='server' ID='CheckBoxPreopCormorbidChoroidal' text='&nbsp;Choroidal Neovascularization' />
                        <br /><asp:CheckBox runat='server' ID='CheckBoxPreopCormorbidNeovasc' text='&nbsp;Retinal Neovascularization' />
                        <br /><asp:CheckBox runat='server' ID='CheckBoxPreopCormorbidTears' text='&nbsp;Retinal Tears, Detachment' />
                        <br /><asp:CheckBox runat='server' ID='CheckBoxPreopCormorbidGlaucoma' text='&nbsp;Glaucoma' />
                        <br /><asp:CheckBox runat='server' ID='CheckBoxProlifilativeDiabeticRetinopathy' text='&nbsp;Proliferative Diabetic Retinopathy' />
                        <br /><asp:CheckBox runat='server' ID='CheckBoxNonProlifilativeDiabeticRetinopathy' text='&nbsp;Nonproliferative Diabetic Retinopathy' />
                        <br /><asp:CheckBox runat='server' ID='CheckBoxPreopCormorbidKerotopathy' text='&nbsp;Band Kerotopathy' />
                        <br /><asp:CheckBox runat='server' ID='CheckBoxPreopCormorbidOpacification' text='&nbsp;Vitreous Opacification' />
                        <br /><asp:CheckBox runat='server' ID='CheckBoxPreopCormorbidOther' text='&nbsp;Other' />
                        </span>
                        &nbsp;<asp:TextBox runat='server' ID='TextBoxPreopComorbidOtherText' size='15' />
                    </td>
                </tr>
            </table>
            <br />
            <!-- Management -->
            <table>
                <tr>
                    <th colspan="3" width="910px"><p>Management</p></th>
                </tr>
                <!-- Question 1 -->
                <tr class="table_body">
                    <td colspan="2">
                        <strong>1. Pre-operative consent and extensive discussion of possibility of poor outcomes?</strong>
                        <asp:Label ID="LabelMgntConsent" runat="server" Visible="false"  ForeColor="Red" Text="<br />Please complete" />    
                    </td>
                    <td>
                        <asp:RadioButton runat='server' ID='RadioButtonMgntConsentYes' GroupName='MgntConsent' text='&nbsp;Yes' />&nbsp;&nbsp;
                        <asp:RadioButton runat='server' ID='RadioButtonMgntConsentNo' GroupName='MgntConsent' text='&nbsp;No' />
                    </td>
                </tr>
                <!-- Question 2 -->
                <tr class="table_body_bg">
                    <td colspan="2">
                        <strong>2. Perioperative therapy - corticosteroids</strong>
                        <asp:Label ID="LabelMgntPeriopCS" runat="server" Visible="false"  ForeColor="Red" Text="<br />Please complete" />
                    </td>
                    <td>
                        <asp:RadioButton runat='server' ID='RadioButtonMgntPeriopCSYes' GroupName='MgntPeriopCS' text='&nbsp;Yes' />&nbsp;&nbsp;
                        <asp:RadioButton runat='server' ID='RadioButtonMgntPeriopCSNo' GroupName='MgntPeriopCS' text='&nbsp;No' />
                    </td>
                </tr>
                <!-- Question 2a -->
                <tr class="table_body_bg">
                    <td colspan="2"><strong><span id="QM2aA">2a. Topical</span></strong></td>
                    <td>
                        <span id="QM2aB">
                        <asp:RadioButton runat='server' ID='RadioButtonMgntPeriopCSTopicalYes' GroupName='MgntPeriopCSTopical' text='&nbsp;Yes' />&nbsp;&nbsp;
                        <asp:RadioButton runat='server' ID='RadioButtonMgntPeriopCSTopicalNo' GroupName='MgntPeriopCSTopical' text='&nbsp;No' />
                        </span>
                    </td>
                </tr>
                <!-- Question 2b -->
                <tr class="table_body_bg">
                    <td colspan="2"><strong><span id="QM2bA">2b. Periocular injections</strong></td>
                    <td>
                        <span id="QM2bB">
                        <asp:RadioButton runat='server' ID='RadioButtonMgntPeriopCSInjectionYes' GroupName='MgntPeriopCSInjection' text='&nbsp;Yes' />&nbsp;&nbsp;
                        <asp:RadioButton runat='server' ID='RadioButtonMgntPeriopCSInjectionNo' GroupName='MgntPeriopCSInjection' text='&nbsp;No' />
                        </span>
                    </td>
                </tr>
                <!-- Question 2c -->
                <tr class="table_body_bg">
                    <td colspan="2"><strong><span id="QM2cA">2c. Systemic</span></strong></td>
                    <td>
                        <span id="QM2cB">
                        <asp:RadioButton runat='server' ID='RadioButtonMgntPeriopCSSystemicYes' GroupName='MgntPeriopCSSystemic' text='&nbsp;Yes' />&nbsp;&nbsp;
                        <asp:RadioButton runat='server' ID='RadioButtonMgntPeriopCSSystemicNo' GroupName='MgntPeriopCSSystemic' text='&nbsp;No' />
                        </span>
                    </td>
                </tr>
                <!-- Question 2d -->
                <tr class="table_body_bg">
                    <td colspan="2"><strong><span id="QM2dA">2d. Intravitreal injection or device</span></strong></td>
                    <td>
                        <span id="QM2dB">
                        <asp:RadioButton runat='server' ID='RadioButtonMgntPeriopCSIntravitrealYes' GroupName='MgntPeriopCSIntravitreal' text='&nbsp;Yes' />&nbsp;&nbsp;
                        <asp:RadioButton runat='server' ID='RadioButtonMgntPeriopCSIntravitrealNo' GroupName='MgntPeriopCSIntravitreal' text='&nbsp;No' />
                        </span>
                    </td>
                </tr>
                <!-- Question 3 -->
                <tr class="table_body">
                    <td colspan="2"><strong>3. Post-operative cycloplegics</strong></td>
                    <td>
                        <asp:RadioButton runat='server' ID='RadioButtonMgntPeriopCycloYes' GroupName='MgntPeriopCyclo' text='&nbsp;Yes' />&nbsp;&nbsp;
                        <asp:RadioButton runat='server' ID='RadioButtonMgntPeriopCycloNo' GroupName='MgntPeriopCyclo' text='&nbsp;No' />
                    </td>
                </tr>
                <!-- Question 4 -->
                <tr class="table_body_bg">
                    <td rowspan="6" width="50%"><strong>4. Surgical intervention</strong></td>
                    <td width="20%">
                        <span class="right">Was capsular staining used?</span>
                        <asp:Label ID="LabelSurgCapsularStaining" runat="server" Visible="false"  ForeColor="Red" Text="<br />Please complete" />
                    </td>
                    <td>
                    <asp:RadioButton runat='server' ID='RadioButtonSurgCapsularStainingYes' GroupName='SurgCapsularStaining' text='&nbsp;Yes' />&nbsp;&nbsp;
                    <asp:RadioButton runat='server' ID='RadioButtonSurgCapsularStainingNo' GroupName='SurgCapsularStaining' text='&nbsp;No' />
                    </td>
                </tr>
                <tr class="table_body_bg">
                    <td colspan="2"><strong>Iris manipulation</strong></td>
                </tr>
                <tr class="table_body_bg">
                    <td>
                        <span class="right">Iris hooks used?</span>
                        <asp:Label ID="LabelSurgIrisHooks" runat="server" Visible="false"  ForeColor="Red" Text="<br />Please complete" />
                    </td>
                    <td>
                        <asp:RadioButton runat='server' ID='RadioButtonSurgIrisHooksYes' GroupName='SurgIrisHooks' text='&nbsp;Yes' />&nbsp;&nbsp;
                        <asp:RadioButton runat='server' ID='RadioButtonSurgIrisHooksNo' GroupName='SurgIrisHooks' text='&nbsp;No' />
                    </td>
                </tr>
                <tr class="table_body_bg">
                    <td>
                        <span class="right">Pupillary expanders (e.g. malyugin ring)</span>
                        <asp:Label ID="LabelSurgIPupillaryExpanders" runat="server" Visible="false"  ForeColor="Red" Text="<br />Please complete" />
                    </td>
                    <td>
                        <asp:RadioButton runat='server' ID='RadioButtonSurgIPupillaryExpandersYes' GroupName='SurgIPupillaryExpanders' text='&nbsp;Yes' />&nbsp;&nbsp;
                        <asp:RadioButton runat='server' ID='RadioButtonSurgIPupillaryExpandersNo' GroupName='SurgIPupillaryExpanders' text='&nbsp;No' />
                    </td>
                </tr>
                <tr class="table_body_bg">
                    <td>
                        <span class="right">Posterior synechiae lysed?</span>
                        <asp:Label ID="LabelSurgPosteriorSynechiae" runat="server" Visible="false"  ForeColor="Red" Text="<br />Please complete" />
                    </td>
                    <td>
                        <asp:RadioButton runat='server' ID='RadioButtonSurgPosteriorSynechiaeYes' GroupName='SurgPosteriorSynechiae' text='&nbsp;Yes' />&nbsp;&nbsp;
                        <asp:RadioButton runat='server' ID='RadioButtonSurgPosteriorSynechiaeNo' GroupName='SurgPosteriorSynechiae' text='&nbsp;No' />
                    </td>
                </tr>
                <tr class="table_body_bg">
                    <td>
                        <span class="right">Pupillary membrane stripped</span>
                        <asp:Label ID="LabelSurgPupillarymembrane" runat="server" Visible="false"  ForeColor="Red" Text="<br />Please complete" />
                    </td>
                    <td>
                        <asp:RadioButton runat='server' ID='RadioButtonSurgPupillarymembraneYes' GroupName='SurgPupillarymembrane' text='&nbsp;Yes' />&nbsp;&nbsp;
                        <asp:RadioButton runat='server' ID='RadioButtonSurgPupillarymembraneNo' GroupName='SurgPupillarymembrane' text='&nbsp;No' />
                    </td>
                </tr>
                <!-- Question 5 -->
                <tr class="table_body">
                    <td colspan="2">
                        <strong>5. Cataract extraction</strong>
                        <asp:Label ID="LabelRBCataractExtraction" runat="server" Visible="false"  ForeColor="Red" Text="<br />Please complete" />
                    </td>
                    <td>
                        <asp:RadioButtonList id='RadioButtonListRBCataractExtraction' RepeatDirection='Vertical' CssClass='aspxList' runat='server'>
                            <asp:ListItem Value='269'>&nbsp;Clear corneal phaco</asp:ListItem>
                            <asp:ListItem Value='270'>&nbsp;Scleral tunnel phaco</asp:ListItem>
                            <asp:ListItem Value='271'>&nbsp;Extracapsular cataract extraction</asp:ListItem>
                            <asp:ListItem Value='272'>&nbsp;Pars plana lensectomy and vitrectomy</asp:ListItem>
                        </asp:RadioButtonList>
                    </td>
                </tr>
                <!-- Question 6 -->
                <tr class="table_body_bg">
                    <td colspan="2">
                        <strong>6. IOL implantation</strong>
                        <asp:Label ID="LabelRBIOLImplant" runat="server" Visible="false"  ForeColor="Red" Text="<br />Please complete" />    
                    </td>
                    <td>
                        <asp:RadioButton runat='server' ID='RadioButtonIOLImplantationYes' GroupName='IOLImplantation' text='&nbsp;Yes' />&nbsp;&nbsp;
                        <asp:RadioButton runat='server' ID='RadioButtonIOLImplantationNo' GroupName='IOLImplantation' text='&nbsp;No' />
                    </td>
                </tr>
                <!-- Question 6a -->
                <tr class="table_body_bg">
                    <td colspan="2"><strong><span id="QM6aA">6a. Material</span></strong></td>
                    <td>
                        <span id="QM6aB">
                        <asp:RadioButtonList id='RadioButtonListRBIOLImplantMaterial' RepeatDirection='Vertical' CssClass='aspxList' runat='server'>
                            <asp:ListItem Value='261'>&nbsp;Silicone</asp:ListItem>
                            <asp:ListItem Value='262'>&nbsp;PMMA</asp:ListItem>
                            <asp:ListItem Value='263'>&nbsp;Acrylic</asp:ListItem>
                        </asp:RadioButtonList>
                        </span>
                    </td>
                </tr>
                <!-- Question 6b -->
                <tr class="table_body_bg">
                    <td colspan="2"><strong><span id="QM6bA">6b. Location of implant</span></strong></td>
                    <td>
                        <span id="QM6bB">
                        <asp:RadioButtonList id='RadioButtonListRBIOLImplantLocation' RepeatDirection='Vertical' CssClass='aspxList' runat='server'>
                            <asp:ListItem Value='264'>&nbsp;Capsular Bag</asp:ListItem>
                            <asp:ListItem Value='259'>&nbsp;Sulcus</asp:ListItem>
                            <asp:ListItem Value='14'>&nbsp;Other</asp:ListItem>
                        </asp:RadioButtonList>
                        </span>
                    </td>
                </tr>
                <!-- Question 6c -->
                <tr class="table_body_bg">
                    <td colspan="2"><strong><span id="QM6cA">6c. Was a primary posterior capsulotomy performed?</span></strong></td>
                    <td>
                        <span id="QM6cB">
                        <asp:RadioButton runat='server' ID='RadioButtonIOLCapsulotomyYes' GroupName='IOLCapsulotomy' text='&nbsp;Yes' />&nbsp;&nbsp;
                        <asp:RadioButton runat='server' ID='RadioButtonIOLCapsulotomyNo' GroupName='IOLCapsulotomy' text='&nbsp;No' />
                        </span>
                    </td>
                </tr>
                <!-- Question 6d -->
                <tr class="table_body_bg">
                    <td colspan="2"><strong><span id="QM6dA">6d. Was a capsular tension ring used?</span></strong></td>
                    <td>
                        <span id="QM6dB">
                        <asp:RadioButton runat='server' ID='RadioButtonCapsularTensionRingYes' GroupName='CapsularTensionRing' text='&nbsp;Yes' />&nbsp;&nbsp;
                        <asp:RadioButton runat='server' ID='RadioButtonCapsularTensionRingNo' GroupName='CapsularTensionRing' text='&nbsp;No' />
                        </span>
                    </td>
                </tr>
                <!-- Question 7 -->
                <tr class="table_body">
                    <td colspan="2"><strong>7. Were there intraoperative or immediate post-operative complications?</strong></td>
                    <td>
                        <asp:RadioButton runat='server' ID='RadioButtonIOLComplicationsYes' GroupName='IOLComplications' text='&nbsp;Yes' />&nbsp;&nbsp;
                        <asp:RadioButton runat='server' ID='RadioButtonIOLComplicationsNo' GroupName='IOLComplications' text='&nbsp;No' />
                    </td>
                </tr>
                <!-- Question 7a -->
                <tr class="table_body">
                    <td colspan="2"><strong><span id="QM7aA">7a. If "Yes," please identify the complication:</span></strong></td>
                    <td>
                        <span id="QM7aB">
                        
                            <asp:CheckBox ID="CheckBoxUnplannedAnteriorVitrectomy" Text="Unplanned anterior vitrectomy" runat="server" />
                            <br /><asp:CheckBox ID="CheckBoxSuprachorodialHemorrhage" Text="Suprachorodial hemorrhage" runat="server" />
                             <br /><asp:CheckBox ID="CheckBoxEndophthalmitis" Text="Endophthalmitis" runat="server" />
                             <br /><asp:CheckBox ID="CheckBoxRetinalDetachment" Text="Retinal detachment" runat="server" />
                             <br /><asp:CheckBox ID="CheckBoxVitreousHemorrhage" Text="Vitreous hemorrhage" runat="server" />
                             <br /><asp:CheckBox ID="CheckBoxCornealEdema" Text="Corneal edema" runat="server" />
                             <br /><asp:CheckBox ID="CheckBoxRetainedLensMaterial" Text="Retained lens material" runat="server" />
                             <br /><asp:CheckBox ID="CheckBoxIOLDislocation" Text="IOL dislocation" runat="server" />
                             <br /><asp:CheckBox ID="CheckBoxIOPElevation" Text="IOP elevation" runat="server" />
                             <br /><asp:CheckBox ID="CheckBoxIOPOther" Text="Other" runat="server" />
                        </span>
                    </td>
                </tr>
                <!-- Question 7b -->
                <tr class="table_body">
                    <td colspan="2"><strong><span id="QM7bA">7b. If "Other," please specify:</span></strong></td>
                    <td>
                        <span id="QM7bB">
                        <asp:TextBox runat="server" ID="TextBoxIOLComplicationOtherText" size="25" />
                        </span>
                    </td>
                </tr>
            </table>
            <br />
            <!-- Outcomes -->
            <table>
                <tr>
                    <th colspan="3" width="910px"><p>Outcomes</p></th>
                </tr>
                <!-- Question 1 -->
                <tr class="table_body">
                    <td colspan="2">
                        <strong>1. Months after surgery when outcomes assessed?</strong>
                        <asp:Label ID="LabelOutcomeMonthsAssessed" runat="server" Visible="false"  ForeColor="Red" Text="<br />Please complete" />
                    </td>
                    <td>
                        <asp:TextBox runat='server' ID='TextBoxOutcomeMonthsAssessed' size='5' />&nbsp;months
                    </td>
                </tr>
                <!-- Question 2 -->
                <tr class="table_body_bg">
                    <td colspan="2"><strong>2. Best corrected visual acuity?</strong>&nbsp;<a href="#"><img id="QO2" src="../../common/images/tip.gif" alt="Tip" /></a></td>
                    <td>
                        <br />Snellen equivalent: 20 /&nbsp;<asp:DropDownList ID='DropDownListOutcomeBCVASnellen' DDataValueField='examValue' DataTextField='examLabel' runat='server' />
                    </td>
                </tr>
                <!-- Question 3 -->
                <tr class="table_body">
                    <td colspan="2">
                        <strong>3. When did inflammation return to baseline?</strong>
                        <asp:Label ID="LabelRBOutcomeInflammation" runat="server" Visible="false"  ForeColor="Red" Text="<br />Please complete" />    
                    </td>
                    <td>
                        <asp:RadioButtonList id='RadioButtonListRBOutcomeInflammation' RepeatDirection='Vertical' CssClass='aspxList' runat='server'>
                            <asp:ListItem Value='283'>&nbsp;&le;1 month post-surgery</asp:ListItem>
                            <asp:ListItem Value='284'>&nbsp;1 to 3 months post-surgery</asp:ListItem>
                            <asp:ListItem Value='285'>&nbsp;&ge;3 months post-surgery</asp:ListItem>
                            <asp:ListItem Value='265'>&nbsp;Never</asp:ListItem>
                        </asp:RadioButtonList>
                    </td>
                </tr>
                <!-- Question 4 -->
                <tr class="table_body_bg">
                    <td colspan="2"><strong>4. Final medications required for this level of control</strong></td>
                    <td>
                        Medication:
                        <br /><asp:CheckBox runat='server' ID='CheckBoxOutcomeMedCS' text='&nbsp;Corticosteroids' />
                        <br /><asp:CheckBox runat='server' ID='CheckBoxNoMedication' text='&nbsp;None' />
                        <span style="margin-left:20px;">
                            <br />Route:
                            <br /><asp:CheckBox runat='server' ID='CheckBoxOutcomeMedCSTopical' text='&nbsp;Topical' />
                            <br /><asp:CheckBox runat='server' ID='CheckBoxOutcomeMedCSPeriocular' text='&nbsp;Periocular' />
                            <br /><asp:CheckBox runat='server' ID='CheckBoxOutcomeMedCSSystemic' text='&nbsp;Systemic' />
                            <br /><asp:CheckBox runat='server' ID='CheckBoxOutcomeMedCSIntravitreal' text='&nbsp;Intravitreal (device)' />
                            <br /><asp:CheckBox runat='server' ID='CheckBoxOutcomeMedCSImmu' text='&nbsp;Immunomodulatory therapy' />
                        </span>
                    </td>
                </tr>
                <!-- Question 5 -->
                <tr class="table_body">
                    <td rowspan="2" width="50%"><strong>5. Iris</strong></td>
                    <td width="20%">
                        <span class="right">Posterior synechiae</span>
                        <asp:Label ID="LabelOutcomePosteriorSynechiaeClockHours" runat="server" Visible="false"  ForeColor="Red" Text="<br />Please complete" />    
                    </td>
                    <td>
                        Extent in clock hours: <asp:TextBox runat='server' ID='TextBoxOutcomePosteriorSynechiaeClockHours' size='5' />
                    </td>
                </tr>
                <tr class="table_body">
                    <td>
                        <span class="right">Neovascularization</span>
                        <asp:Label ID="LabelRBOutcomeNeovasc" runat="server" Visible="false"  ForeColor="Red" Text="<br />Please complete" />
                    </td>
                    <td>
                        
                        <asp:RadioButtonList id='RadioButtonListRBOutcomeNeovasc' RepeatDirection='Horizontal' CssClass='aspxList' runat='server'>
                            <asp:ListItem Value='175'>&nbsp;Present</asp:ListItem>
                            <asp:ListItem Value='176'>&nbsp;Absent</asp:ListItem>
                        </asp:RadioButtonList>
                    </td>
                </tr>
                <!-- Question 6 -->
                <tr class="table_body_bg">
                    <td rowspan="2">
                        <strong>6. Posterior chamber IOL</strong>
                    </td>
                    <td>
                        <span class="right">Inflammatory deposits on IOL</span>
                        <asp:Label ID="LabelOutcomeCapsularPhimosis" runat="server" Visible="false"  ForeColor="Red" Text="<br />Please complete" />
                    </td>
                    <td>
                        <asp:RadioButton runat='server' ID='RadioButtonOutcomeCapsularPhimosisYes' GroupName='OutcomeCapsularPhimosis' text='&nbsp;Yes' />&nbsp;&nbsp;
                        <asp:RadioButton runat='server' ID='RadioButtonOutcomeCapsularPhimosisNo' GroupName='OutcomeCapsularPhimosis' text='&nbsp;No' />
                    </td>
                </tr>
                <tr class="table_body_bg">
                    <td id="QO6bA">
                        <span class="right">Inflammatory deposits on IOL</span>
                        <asp:Label ID="LabelRBOutcomeInflamDeposits" runat="server" Visible="false"  ForeColor="Red" Text="<br />Please complete" />
                    </td>
                    <td id="QO6bB">
                        <asp:RadioButtonList id='RadioButtonListRBOutcomeInflamDeposits' RepeatDirection='Horizontal' CssClass='aspxList' runat='server'>
                            <asp:ListItem Value='128'>&nbsp;None</asp:ListItem>
                            <asp:ListItem Value='241'>&nbsp;Mild</asp:ListItem>
                            <asp:ListItem Value='286'>&nbsp;Severe</asp:ListItem>
                        </asp:RadioButtonList>
                    </td>
                </tr>
                <!-- Question 7 -->
                <tr class="table_body">
                    <td rowspan="3"><strong>7. Centration</strong></td>
                    <td>
                        <span class="right">Well-centered or subluxed?</span>
                        <asp:Label ID="LabelRBOutcomeWellCentered" runat="server" Visible="false"  ForeColor="Red" Text="<br />Please complete" />
                    </td>
                    <td>
                        <asp:RadioButtonList id='RadioButtonListRBOutcomeWellCentered' RepeatDirection='Horizontal' CssClass='aspxList' runat='server'>
                            <asp:ListItem Value='287'>&nbsp;Well-centered</asp:ListItem>
                            <asp:ListItem Value='288'>&nbsp;Subluxed</asp:ListItem>
                        </asp:RadioButtonList>
                    </td>
                </tr>
                <tr class="table_body">
                    <td>
                        <span class="right">Entire IOL in the bag?</span>
                        <asp:Label ID="LabelOutcomeEntireIOLinBag" runat="server" Visible="false"  ForeColor="Red" Text="<br />Please complete" />
                    </td>
                    <td>
                        <asp:RadioButton runat='server' ID='RadioButtonOutcomeEntireIOLinBagYes' GroupName='OutcomeEntireIOLinBag' text='&nbsp;Yes' />&nbsp;&nbsp;
                        <asp:RadioButton runat='server' ID='RadioButtonOutcomeEntireIOLinBagNo' GroupName='OutcomeEntireIOLinBag' text='&nbsp;No' />
                    </td>
                </tr>
                <tr class="table_body">
                    <td>
                        <span class="right" id="QO7cA">If "No," optic capture or haptic capture?</span>
                        <asp:Label ID="LabelRBOutcomeOpticCapture" runat="server" Visible="false"  ForeColor="Red" Text="<br />Please complete" />
                    </td>
                    <td>
                        <span id="QO7cB">
                        <asp:RadioButtonList id='RadioButtonListRBOutcomeOpticCapture' RepeatDirection='Horizontal' CssClass='aspxList' runat='server'>
                            <asp:ListItem Value='292'>&nbsp;Optic capture</asp:ListItem>
                            <asp:ListItem Value='293'>&nbsp;Haptic capture</asp:ListItem>
                        </asp:RadioButtonList>
                        </span>
                    </td>
                </tr>
                <!-- Question 8 -->
                <tr class="table_body">
                    <td colspan="2">
                        <strong>8. Intraocular pressure</strong>
                        <asp:Label ID="LabelOutcomeIOP" runat="server" Visible="false"  ForeColor="Red" Text="<br />Please complete" />
                    </td>
                    <td>
                        <asp:TextBox runat='server' ID='TextBoxOutcomeIOP' size='5' />&nbsp;mmHg
                    </td>
                </tr>
                <!-- Question 9 -->
                <tr class="table_body_bg">
                    <td colspan="2">
                        <strong>9. Onset of glaucoma after surgery</strong>
                        <asp:Label ID="LabelOutcomeOnsetGlaucoma" runat="server" Visible="false"  ForeColor="Red" Text="<br />Please complete" />
                    </td>
                    <td>
                        <asp:RadioButton runat='server' ID='RadioButtonOutcomeOnsetGlaucomaYes' GroupName='OutcomeOnsetGlaucoma' text='&nbsp;Yes' />&nbsp;&nbsp;
                        <asp:RadioButton runat='server' ID='RadioButtonOutcomeOnsetGlaucomaNo' GroupName='OutcomeOnsetGlaucoma' text='&nbsp;No' />
                    </td>
                </tr>
                <!-- Question 10 -->
                <tr class="table_body">
                    <td colspan="2">
                        <strong>10. Vitreous cells</strong>
                        <asp:Label ID="LabelOutcomeVitreousCells" runat="server" Visible="false"  ForeColor="Red" Text="<br />Please complete" />
                    </td>
                    <td>
                        <asp:RadioButton runat='server' ID='RadioButtonOutcomeVitreousCellsYes' GroupName='OutcomeVitreousCells' text='&nbsp;Yes' />&nbsp;&nbsp;
                        <asp:RadioButton runat='server' ID='RadioButtonOutcomeVitreousCellsNo' GroupName='OutcomeVitreousCells' text='&nbsp;No' />
                        <span id="QO10">
                        <br />
                        <br />Grade:
                        <asp:RadioButtonList id='RadioButtonListRBOutcomeVitreousCellGrade' RepeatDirection='Vertical' CssClass='aspxList' runat='server'>
                            <asp:ListItem Value='179'>&nbsp;0</asp:ListItem>
                            <asp:ListItem Value='184'>&nbsp;0.5+</asp:ListItem>
                            <asp:ListItem Value='185'>&nbsp;1+</asp:ListItem>
                            <asp:ListItem Value='186'>&nbsp;2+</asp:ListItem>
                            <asp:ListItem Value='290'>&nbsp;3+</asp:ListItem>
                            <asp:ListItem Value='291'>&nbsp;4+</asp:ListItem>
                        </asp:RadioButtonList>
                        </span>
                    </td>
                </tr>
                <!-- Question 11 -->
                <tr class="table_body_bg">
                    <td rowspan="5"><strong>11. Retina and choroid</strong></td>
                    <td>
                        <span class="right">Was a dilated fundus exam performed?</span>
                        <asp:Label ID="LabelOutcomeDilatedFundusExamPerformed" runat="server" Visible="false"  ForeColor="Red" Text="<br />Please complete" />
                    </td>
                    <td>
                        <asp:RadioButton runat='server' ID='RadioButtonOutcomeDilatedFundusExamPerformedYes' GroupName='OutcomeDilatedFundusExamPerformed' text='&nbsp;Yes' />&nbsp;&nbsp;
                        <asp:RadioButton runat='server' ID='RadioButtonOutcomeDilatedFundusExamPerformedNo' GroupName='OutcomeDilatedFundusExamPerformed' text='&nbsp;No' />
                    </td>
                </tr>
                <tr class="table_body_bg">
                    <td>
                        <span class="right">Was fluorescein angiography performed?</span>
                        <asp:Label ID="LabelOutcomeFluoresceinPerformed" runat="server" Visible="false"  ForeColor="Red" Text="<br />Please complete" />
                    </td>
                    <td>    
                        <asp:RadioButton runat='server' ID='RadioButtonOutcomeFluoresceinPerformedYes' GroupName='OutcomeFluoresceinPerformed' text='&nbsp;Yes' />&nbsp;&nbsp;
                        <asp:RadioButton runat='server' ID='RadioButtonOutcomeFluoresceinPerformedNo' GroupName='OutcomeFluoresceinPerformed' text='&nbsp;No' />
                    </td>
                </tr>
                <tr class="table_body_bg">
                    <td>
                        <span class="right">Was OCT performed?</span>
                        <asp:Label ID="LabelOutcomeOCTPerformed" runat="server" Visible="false"  ForeColor="Red" Text="<br />Please complete" />
                    </td>
                    <td>
                        <asp:RadioButton runat='server' ID='RadioButtonOutcomeOCTPerformedYes' GroupName='OutcomeOCTPerformed' text='&nbsp;Yes' />&nbsp;&nbsp;
                        <asp:RadioButton runat='server' ID='RadioButtonOutcomeOCTPerformedNo' GroupName='OutcomeOCTPerformed' text='&nbsp;No' />
                    </td>
                </tr>
                <tr class="table_body_bg">
                    <td>
                        <span class="right">Was CME present?</span>
                        <asp:Label ID="LabelOutcomeCMEPresent" runat="server" Visible="false"  ForeColor="Red" Text="<br />Please complete" />
                    </td>
                    <td>
                        <asp:RadioButton runat='server' ID='RadioButtonOutcomeCMEPresentYes' GroupName='OutcomeCMEPresent' text='&nbsp;Yes' />&nbsp;&nbsp;
                        <asp:RadioButton runat='server' ID='RadioButtonOutcomeCMEPresentNo' GroupName='OutcomeCMEPresent' text='&nbsp;No' />
                    </td>
                </tr>
                <tr class="table_body_bg">
                    <td>
                        <span class="right">Was epiretinal membrane present</span>
                        <asp:Label ID="LabelOutcomeEpiretinalMembrane" runat="server" Visible="false"  ForeColor="Red" Text="<br />Please complete" />
                    </td>
                    <td>
                        <asp:RadioButton runat='server' ID='RadioButtonOutcomeEpiretinalMembraneYes' GroupName='OutcomeEpiretinalMembrane' text='&nbsp;Yes' />&nbsp;&nbsp;
                        <asp:RadioButton runat='server' ID='RadioButtonOutcomeEpiretinalMembraneNo' GroupName='OutcomeEpiretinalMembrane' text='&nbsp;No' />
                    </td>
                </tr>
                <!-- Question 12 -->
                <tr class="table_body">
                    <td colspan="2">
                        <strong>12. Is there evidence of additional posterior segment disease activity?</strong>
                        <asp:Label ID="LabelOutcomePosteriorDiseaseActive" runat="server" Visible="false"  ForeColor="Red" Text="<br />Please complete" />
                    </td>
                    <td>
                        <asp:RadioButton runat='server' ID='RadioButtonOutcomePosteriorDiseaseActiveYes' GroupName='OutcomePosteriorDiseaseActive' text='&nbsp;Yes' />&nbsp;&nbsp;
                        <asp:RadioButton runat='server' ID='RadioButtonOutcomePosteriorDiseaseActiveNo' GroupName='OutcomePosteriorDiseaseActive' text='&nbsp;No' />
                    </td>
                </tr>
                <!-- Question 12a -->
                <tr class="table_body">
                    <td colspan="2"><strong><span id="QO12aA">12a. If "Yes,"  please indicate posterior segment disease activity identified</span></strong></td>
                    <td>
                        <span id="QO12aB">
                        <asp:RadioButtonList id='RadioButtonListRBOutcomePosteriorDiseaseType' RepeatDirection='Vertical' CssClass='aspxList' runat='server'>
                            <asp:ListItem Value='294'>&nbsp;Retinitis</asp:ListItem>
                            <asp:ListItem Value='295'>&nbsp;Choroiditis</asp:ListItem>
                            <asp:ListItem Value='296'>&nbsp;Retinal Vasculitis</asp:ListItem>
                            <asp:ListItem Value='342'>&nbsp;Posterior Capsular Opacification</asp:ListItem>
                            <asp:ListItem Value='14'>&nbsp;Other</asp:ListItem>
                        </asp:RadioButtonList>
                        </span>
                    </td>
                </tr>
                <!-- Question 12b -->
                <tr class="table_body">
                    <td colspan="2"><strong><span id="QO12bA">12b. If "Other," please indicate:</span></strong></td>
                    <td>
                        <span id="QO12bB">
                        <asp:TextBox runat='server' ID='TextBoxOutcomePosteriorDiseaseOther' size='25' />
                        </span>
                    </td>
                </tr>
                <!-- Question 13 -->
                <tr class="table_body_bg">
                    <td colspan="2">
                        <strong>13. Is patient satisfied with surgical results</strong>
                        <asp:Label ID="LabelRBOutcomePatientSatisfied" runat="server" Visible="false"  ForeColor="Red" Text="<br />Please complete" />
                    </td>
                    <td>
                        <asp:RadioButtonList id='RadioButtonListRBOutcomePatientSatisfied' RepeatDirection='Horizontal' CssClass='aspxList' runat='server'>
                            <asp:ListItem Value='1'>&nbsp;Yes</asp:ListItem>
                            <asp:ListItem Value='2'>&nbsp;No</asp:ListItem>
                            <asp:ListItem Value='289'>&nbsp;Not asked</asp:ListItem>
                        </asp:RadioButtonList>
                    </td>
                </tr>
                <!-- Question 14 -->
                <tr class="table_body">
                    <td colspan="2">
                        <strong>14. Was any additional surgical procedure required?</strong>
                        <asp:Label ID="LabelOutcomeAdditionalSurgery" runat="server" Visible="false"  ForeColor="Red" Text="<br />Please complete" />
                    </td>
                    <td>
                        <asp:RadioButton runat='server' ID='RadioButtonOutcomeAdditionalSurgeryYes' GroupName='OutcomeAdditionalSurgery' text='&nbsp;Yes' />&nbsp;&nbsp;
                        <asp:RadioButton runat='server' ID='RadioButtonOutcomeAdditionalSurgeryNo' GroupName='OutcomeAdditionalSurgery' text='&nbsp;No' />
                    </td>
                </tr>
                <!-- Question 14a -->
                <tr class="table_body">
                    <td colspan="2"><strong><span id="QO14aA">14a. If "Yes," please indicate</span></strong></td>
                    <td>
                        <span id="QO14aB">
                        <asp:CheckBox runat='server' ID='CheckBoxRBOutcomeAdditionalYag' text='&nbsp;YAG Capsulotomy' />
                        <br /><asp:CheckBox runat='server' ID='CheckBoxRBOutcomeAdditionalIOL' text='&nbsp;Reposition, replace or remove IOL' />
                        <br /><asp:CheckBox runat='server' ID='CheckBoxRBOutcomeAdditionalAspiration' text='&nbsp;Intraocular aspiration or injection' />
                        <br /><asp:CheckBox runat='server' ID='CheckBoxRBOutcomeAdditionalLaser' text='&nbsp;Retinal laser' />
                        <br /><asp:CheckBox runat='server' ID='CheckBoxRBOutcomeAdditionalRepairDetachment' text='&nbsp;Repair retinal detachment' />
                        <br /><asp:CheckBox runat='server' ID='CheckBoxRBOutcomeAdditionalParsPlana' text='&nbsp;Pars plana vitrectomy' />
                        <br /><asp:CheckBox runat='server' ID='CheckBoxRBOutcomeAdditionalOther' text='&nbsp;Other ' />
                        </span>
                        &nbsp;&nbsp;<asp:TextBox runat='server' ID='TextBoxOutcomeAdditionalOtherText' size='20' />
                    </td>
                </tr>
            </table>
            <div class="button-box">
                <asp:LinkButton ID="LinkButtonBackToDashboard" runat="server" Text="Back To Chart Registration" PostBackUrl="../PatientChartRegistration.aspx?CycleNumber=1" Visible="false" CssClass="button" />
                <asp:LinkButton ID="ButtonSubmit" OnClick="ButtonSubmit_Click" runat="server" Text="Submit Chart" CssClass="button" />
            </div>
        </div>
        <!-- ION pim ends -->
</asp:Content>

<asp:Content runat="server" ID="Content4" ContentPlaceHolderID="javascript">
    <script type="text/javascript" src="../../common/js/jquery.atooltip.js"></script>
    <script type="text/javascript" language="javascript">
        //      Either enables or disables input boxes
        function enabledControl(el, disabled, checked) {
            //alert("Hello world from enabled control");
            try {
                if (disabled) {
                    el.disabled = disabled;
                    el.setAttribute("disabled", true);
                }
                else {
                    el.disabled = disabled;
                    el.removeAttribute("disabled");
                }

                if (checked == true)
                    el.checked = false;
            }
            catch (E) {
            }
            if (el.childNodes && el.childNodes.length > 0) {
                for (var x = 0; x < el.childNodes.length; x++) {
                    enabledControl(el.childNodes[x], disabled, checked);
                }
            }
        }
        //      Either enables or disables dropdown boxes
        function enabledControlDropDown(el, disabled, checked) {
            //alert("Hello world from enabled control");
            try {
                if (disabled) {
                    el.disabled = disabled;
                    el.setAttribute("disabled", true);
                }
                else {
                    el.disabled = disabled;
                    el.removeAttribute("disabled");
                }

                if (checked == true)
                    el.options[0].selected = true;
            }
            catch (E) {
            }
            if (el.childNodes && el.childNodes.length > 0) {
                for (var x = 0; x < el.childNodes.length; x++) {
                    enabledControl(el.childNodes[x], disabled, checked);
                }
            }
        }
        //      Either enables or disables text boxes
        function enabledControlTextField(el, disabled, checked) {
            //alert("Hello world from enabled control");
            try {
                if (disabled) {
                    el.disabled = disabled;
                    el.setAttribute("disabled", true);
                }
                else {
                    el.disabled = disabled;
                    el.removeAttribute("disabled");
                }

                if (checked == true) {
                    el.value = '';
                }
            }
            catch (E) {
            }
            if (el.childNodes && el.childNodes.length > 0) {
                for (var x = 0; x < el.childNodes.length; x++) {
                    enabledControl(el.childNodes[x], disabled, checked);
                }
            }
        }

        function generate(arr1, arr2, istrue) {
            if (istrue == true) {
                for (var i = 0; i < arr1.length; i++) {
                    document.getElementById(arr1[i]).style.color = "gray";
                }
                for (var i = 0; i < arr2.length; i++) {
                    $(arr2[i] + ' :input').attr('disabled', true);
                    $(arr2[i] + ' :input').removeAttr("checked");
                }
            }
            if (istrue == false) {
                for (var i = 0; i < arr1.length; i++) {
                    document.getElementById(arr1[i]).style.color = "#333";
                }
                for (var i = 0; i < arr2.length; i++) {
                    $(arr2[i] + ' :input').removeAttr('disabled');
                }
            }
        }

        // Question 4a - Clinical Examination ***************************
        function toggleQC4aTB() {
            document.getElementById("<%= TextBoxRBIrisClockHours.ClientID %>").value = '';
        }
        function toggleQC4aCB() {
            document.getElementById("<%= CheckBoxIrisBombe.ClientID %>").checked = false;
        }

    </script>

    <script  type="text/javascript" language="javascript">
        $(document).ready(function () {
            // Question 9 - History ****************************************
            $("#<%= CheckBoxTherapyToDateCorticosteroids.ClientID %>").click(function () {
                if ($('#<%= CheckBoxTherapyToDateCorticosteroids.ClientID %>').prop("checked") == true) {
                    var myaray = new Array("QH9A");
                    var myarray2 = new Array('#QH9A');
                    generate(myaray, myarray2, false);
                }
                else {
                    var myaray = new Array("QH9A");
                    var myarray2 = new Array('#QH9A');
                    generate(myaray, myarray2, true);
                }
            });
            if ($('#<%= CheckBoxTherapyToDateCorticosteroids.ClientID %>').prop("checked") == true) {
                var myaray = new Array("QH9A");
                var myarray2 = new Array('#QH9A');
                generate(myaray, myarray2, false);
            }
            else {
                var myaray = new Array("QH9A");
                var myarray2 = new Array('#QH9A');
                generate(myaray, myarray2, true);
            }
            // Question 9 - History ****************************************
            $("#<%= CheckBoxTherapyToDateImmu.ClientID %>").click(function () {
                if ($('#<%= CheckBoxTherapyToDateImmu.ClientID %>').prop("checked") == true) {
                    var myaray = new Array("QH9B");
                    var myarray2 = new Array('#QH9B');
                    generate(myaray, myarray2, false);

                    enabledControlTextField(document.getElementById("<%= TextBoxTherapyToDateImmuName.ClientID %>"), false, false);
                }
                else {
                    var myaray = new Array("QH9B");
                    var myarray2 = new Array('#QH9B');
                    generate(myaray, myarray2, true);

                    enabledControlTextField(document.getElementById("<%= TextBoxTherapyToDateImmuName.ClientID %>"), true, true);
                }
            });
            if ($('#<%= CheckBoxTherapyToDateImmu.ClientID %>').prop("checked") == true) {
                var myaray = new Array("QH9B");
                var myarray2 = new Array('#QH9B');
                generate(myaray, myarray2, false);

                enabledControlTextField(document.getElementById("<%= TextBoxTherapyToDateImmuName.ClientID %>"), false, false);
            }
            else {
                var myaray = new Array("QH9B");
                var myarray2 = new Array('#QH9B');
                generate(myaray, myarray2, true);

                enabledControlTextField(document.getElementById("<%= TextBoxTherapyToDateImmuName.ClientID %>"), true, true);
            }






            // Question 6 - Clinical Examination ***************************
            $("#<%= RadioButtonOpticDiscAbnormalitiesYes.ClientID %>").click(function () {
                if ($('#<%= RadioButtonOpticDiscAbnormalitiesYes.ClientID %>').prop("checked") == true) {
                    var myaray = new Array("QC6A", "QC6B");
                    var myarray2 = new Array('#QC6B');
                    generate(myaray, myarray2, false);

                    //enabledControlTextField(document.getElementById("<%= TextBoxRBOpticDiscAbnormalityOther.ClientID %>"), false, false);
                }
                else {
                    var myaray = new Array("QC6A", "QC6B");
                    var myarray2 = new Array('#QC6B');
                    generate(myaray, myarray2, true);

                    enabledControlTextField(document.getElementById("<%= TextBoxRBOpticDiscAbnormalityOther.ClientID %>"), true, true);
                }
            });
            $("#<%= RadioButtonOpticDiscAbnormalitiesNo.ClientID %>").click(function () {
                if ($('#<%= RadioButtonOpticDiscAbnormalitiesYes.ClientID %>').prop("checked") == true) {
                    var myaray = new Array("QC6A", "QC6B");
                    var myarray2 = new Array('#QC6B');
                    generate(myaray, myarray2, false);

                    //enabledControlTextField(document.getElementById("<%= TextBoxRBOpticDiscAbnormalityOther.ClientID %>"), false, false);
                }
                else {
                    var myaray = new Array("QC6A", "QC6B");
                    var myarray2 = new Array('#QC6B');
                    generate(myaray, myarray2, true);

                    enabledControlTextField(document.getElementById("<%= TextBoxRBOpticDiscAbnormalityOther.ClientID %>"), true, true);
                }
            });
            if ($('#<%= RadioButtonOpticDiscAbnormalitiesYes.ClientID %>').prop("checked") == true) {
                var myaray = new Array("QC6A", "QC6B");
                var myarray2 = new Array('#QC6B');
                generate(myaray, myarray2, false);
            }
            else {
                var myaray = new Array("QC6A", "QC6B");
                var myarray2 = new Array('#QC6B');
                generate(myaray, myarray2, true);
            }

            // Question 6, other - Clinical Examination **********************
            $("#<%= RadioButtonListRBOpticDiscAbnormalityType.ClientID %>").click(function () {
                if ($('#<%= RadioButtonListRBOpticDiscAbnormalityType.ClientID %>').find('input:checked').val() == ('14')) {
                    enabledControlTextField(document.getElementById("<%= TextBoxRBOpticDiscAbnormalityOther.ClientID %>"), false, false);
                }
                else {
                    enabledControlTextField(document.getElementById("<%= TextBoxRBOpticDiscAbnormalityOther.ClientID %>"), true, true);
                }
            });
            if ($('#<%= RadioButtonListRBOpticDiscAbnormalityType.ClientID %>').find('input:checked').val() == ('14')) {
                enabledControlTextField(document.getElementById("<%= TextBoxRBOpticDiscAbnormalityOther.ClientID %>"), false, false);
            }
            else {
                enabledControlTextField(document.getElementById("<%= TextBoxRBOpticDiscAbnormalityOther.ClientID %>"), true, true);
            }


            // Question 8 - Clinical Examination ***************************
            $("#<%= RadioButtonPreopOcularCoMorbilitiesYes.ClientID %>").click(function () {
                if ($('#<%= RadioButtonPreopOcularCoMorbilitiesYes.ClientID %>').prop("checked") == true) {
                    var myaray = new Array("QC8A", "QC8B");
                    var myarray2 = new Array('#QC8B');
                    generate(myaray, myarray2, false);
                }
                else {
                    var myaray = new Array("QC8A", "QC8B");
                    var myarray2 = new Array('#QC8B');
                    generate(myaray, myarray2, true);

                    enabledControlTextField(document.getElementById("<%= TextBoxPreopComorbidOtherText.ClientID %>"), true, true);
                }
            });
            $("#<%= RadioButtonPreopOcularCoMorbilitiesNo.ClientID %>").click(function () {
                if ($('#<%= RadioButtonPreopOcularCoMorbilitiesYes.ClientID %>').prop("checked") == true) {
                    var myaray = new Array("QC8A", "QC8B");
                    var myarray2 = new Array('#QC8B');
                    generate(myaray, myarray2, false);
                }
                else {
                    var myaray = new Array("QC8A", "QC8B");
                    var myarray2 = new Array('#QC8B');
                    generate(myaray, myarray2, true);

                    enabledControlTextField(document.getElementById("<%= TextBoxPreopComorbidOtherText.ClientID %>"), true, true);
                }
            });
            if ($('#<%= RadioButtonPreopOcularCoMorbilitiesYes.ClientID %>').prop("checked") == true) {
                var myaray = new Array("QC8A", "QC8B");
                var myarray2 = new Array('#QC8B');
                generate(myaray, myarray2, false);
            }
            else {
                var myaray = new Array("QC8A", "QC8B");
                var myarray2 = new Array('#QC8B');
                generate(myaray, myarray2, true);

                enabledControlTextField(document.getElementById("<%= TextBoxPreopComorbidOtherText.ClientID %>"), true, true);
            }

            // Question 8, other - Clinical Examination **********************
            $("#<%= CheckBoxPreopCormorbidOther.ClientID %>").click(function () {
                if ($('#<%= CheckBoxPreopCormorbidOther.ClientID %>').prop("checked") == true) {
                    enabledControlTextField(document.getElementById("<%= TextBoxPreopComorbidOtherText.ClientID %>"), false, false);
                }
                else {
                    enabledControlTextField(document.getElementById("<%= TextBoxPreopComorbidOtherText.ClientID %>"), true, true);
                }
            });
            if ($('#<%= CheckBoxPreopCormorbidOther.ClientID %>').prop("checked") == true) {
                enabledControlTextField(document.getElementById("<%= TextBoxPreopComorbidOtherText.ClientID %>"), false, false);
            }
            else {
                enabledControlTextField(document.getElementById("<%= TextBoxPreopComorbidOtherText.ClientID %>"), true, true);
            }


            // Question 7 - Management ***************************
            $("#<%= RadioButtonIOLComplicationsYes.ClientID %>").click(function () {
                if ($('#<%= RadioButtonIOLComplicationsYes.ClientID %>').prop("checked") == true) {
                    var myaray = new Array("QM7aA", "QM7aB");
                    var myarray2 = new Array('#QM7aB');
                    generate(myaray, myarray2, false);
                }
                else {
                    var myaray = new Array("QM7aA", "QM7aB", "QM7bA", "QM7bB");
                    var myarray2 = new Array('#QM7aB', '#QM7bB');
                    generate(myaray, myarray2, true);

                    enabledControlTextField(document.getElementById("<%= TextBoxIOLComplicationOtherText.ClientID %>"), true, true);
                }
            });
            $("#<%= RadioButtonIOLComplicationsNo.ClientID %>").click(function () {
                if ($('#<%= RadioButtonIOLComplicationsYes.ClientID %>').prop("checked") == true) {
                    var myaray = new Array("QM7aA", "QM7aB");
                    var myarray2 = new Array('#QM7aB');
                    generate(myaray, myarray2, false);
                }
                else {
                    var myaray = new Array("QM7aA", "QM7aB", "QM7bA", "QM7bB");
                    var myarray2 = new Array('#QM7aB', '#QM7bB');
                    generate(myaray, myarray2, true);

                    enabledControlTextField(document.getElementById("<%= TextBoxIOLComplicationOtherText.ClientID %>"), true, true);
                }
            });
            if ($('#<%= RadioButtonIOLComplicationsYes.ClientID %>').prop("checked") == true) {
                var myaray = new Array("QM7aA", "QM7aB");
                var myarray2 = new Array('#QM7aB');
                generate(myaray, myarray2, false);
            }
            else {
                var myaray = new Array("QM7aA", "QM7aB", "QM7bA", "QM7bB");
                var myarray2 = new Array('#QM7aB', '#QM7bB');
                generate(myaray, myarray2, true);

                enabledControlTextField(document.getElementById("<%= TextBoxIOLComplicationOtherText.ClientID %>"), true, true);
            }


            // Question 7 - Management ***************************
            $("#<%= CheckBoxIOPOther.ClientID %>").click(function () {
                if ($('#<%= CheckBoxIOPOther.ClientID %>').prop("checked") == true) {
                    var myaray = new Array("QM7bA", "QM7bB");
                    var myarray2 = new Array('#QM7bB');
                    generate(myaray, myarray2, false);

                    enabledControlTextField(document.getElementById("<%= TextBoxIOLComplicationOtherText.ClientID %>"), false, false);
                }
                else {
                    var myaray = new Array("QM7bA", "QM7bB");
                    var myarray2 = new Array('#QM7bB');
                    generate(myaray, myarray2, true);

                    enabledControlTextField(document.getElementById("<%= TextBoxIOLComplicationOtherText.ClientID %>"), true, true);
                }
            });

            if ($('#<%= CheckBoxIOPOther.ClientID %>').prop("checked") == true) {
                var myaray = new Array("QM7bA", "QM7bB");
                var myarray2 = new Array('#QM7bB');
                generate(myaray, myarray2, false);

                enabledControlTextField(document.getElementById("<%= TextBoxIOLComplicationOtherText.ClientID %>"), false, false);
            }
            else {
                var myaray = new Array("QM7bA", "QM7bB");
                var myarray2 = new Array('#QM7bB');
                generate(myaray, myarray2, true);

                enabledControlTextField(document.getElementById("<%= TextBoxIOLComplicationOtherText.ClientID %>"), true, true);
            }


            // Question 7c - Outcomes ***************************
            function centrationEnable(enableObject) {
                if (enableObject) {
                    var myaray = new Array("QO7cA", "QO7cB");
                    var myarray2 = new Array('#QO7cB');
                    generate(myaray, myarray2, false);
                }
                else {
                    var myaray = new Array("QO7cA", "QO7cB");
                    var myarray2 = new Array('#QO7cB');
                    generate(myaray, myarray2, true);
                }
            }

            $("#<%= RadioButtonOutcomeEntireIOLinBagYes.ClientID %>").click(function () {
                if (($('#<%= RadioButtonOutcomeEntireIOLinBagNo.ClientID %>').prop("checked") == true) || ($('#<%= RadioButtonListRBOutcomeWellCentered.ClientID %>').find('input:checked').val() == ('288'))) {
                    centrationEnable(true);
                }
                else {
                    centrationEnable(false);
                }
            });
            $("#<%= RadioButtonOutcomeEntireIOLinBagNo.ClientID %>").click(function () {
                if (($('#<%= RadioButtonOutcomeEntireIOLinBagNo.ClientID %>').prop("checked") == true) || ($('#<%= RadioButtonListRBOutcomeWellCentered.ClientID %>').find('input:checked').val() == ('288'))) {
                    centrationEnable(true);
                }
                else {
                    centrationEnable(false);
                }
            });
            $("#<%= RadioButtonListRBOutcomeWellCentered.ClientID %>").click(function () {
                if (($('#<%= RadioButtonOutcomeEntireIOLinBagNo.ClientID %>').prop("checked") == true) || ($('#<%= RadioButtonListRBOutcomeWellCentered.ClientID %>').find('input:checked').val() == ('288'))) {
                    centrationEnable(true);
                }
                else {
                    centrationEnable(false);
                }
            });
            if (($('#<%= RadioButtonOutcomeEntireIOLinBagNo.ClientID %>').prop("checked") == true) || ($('#<%= RadioButtonListRBOutcomeWellCentered.ClientID %>').find('input:checked').val() == ('288'))) {
                centrationEnable(true);
            }
            else {
                centrationEnable(false);
            }

            // Question 10 - Outcomes ***************************
            $("#<%= RadioButtonOutcomeVitreousCellsYes.ClientID %>").click(function () {
                if ($('#<%= RadioButtonOutcomeVitreousCellsYes.ClientID %>').prop("checked") == true) {
                    var myaray = new Array("QO10");
                    var myarray2 = new Array('#QO10');
                    generate(myaray, myarray2, false);
                }
                else {
                    var myaray = new Array("QO10");
                    var myarray2 = new Array('#QO10');
                    generate(myaray, myarray2, true);
                }
            });
            $("#<%= RadioButtonOutcomeVitreousCellsNo.ClientID %>").click(function () {
                if ($('#<%= RadioButtonOutcomeVitreousCellsYes.ClientID %>').prop("checked") == true) {
                    var myaray = new Array("QO10");
                    var myarray2 = new Array('#QO10');
                    generate(myaray, myarray2, false);
                }
                else {
                    var myaray = new Array("QO10");
                    var myarray2 = new Array('#QO10');
                    generate(myaray, myarray2, true);
                }
            });
            if ($('#<%= RadioButtonOutcomeVitreousCellsYes.ClientID %>').prop("checked") == true) {
                var myaray = new Array("QO10");
                var myarray2 = new Array('#QO10');
                generate(myaray, myarray2, false);
            }
            else {
                var myaray = new Array("QO10");
                var myarray2 = new Array('#QO10');
                generate(myaray, myarray2, true);
            }


            // Question 12 - Outcomes ***************************
            $("#<%= RadioButtonOutcomePosteriorDiseaseActiveYes.ClientID %>").click(function () {
                if ($('#<%= RadioButtonOutcomePosteriorDiseaseActiveYes.ClientID %>').prop("checked") == true) {
                    var myaray = new Array("QO12aA", "QO12aB");
                    var myarray2 = new Array('#QO12aB');
                    generate(myaray, myarray2, false);
                }
                else {
                    var myaray = new Array("QO12aA", "QO12aB", "QO12bA", "QO12bB");
                    var myarray2 = new Array('#QO12aB', "#QO12bB");
                    generate(myaray, myarray2, true);

                    enabledControlTextField(document.getElementById("<%= TextBoxOutcomePosteriorDiseaseOther.ClientID %>"), true, true);
                }
            });
            $("#<%= RadioButtonOutcomePosteriorDiseaseActiveNo.ClientID %>").click(function () {
                if ($('#<%= RadioButtonOutcomePosteriorDiseaseActiveYes.ClientID %>').prop("checked") == true) {
                    var myaray = new Array("QO12aA", "QO12aB");
                    var myarray2 = new Array('#QO12aB');
                    generate(myaray, myarray2, false);
                }
                else {
                    var myaray = new Array("QO12aA", "QO12aB", "QO12bA", "QO12bB");
                    var myarray2 = new Array('#QO12aB', "#QO12bB");
                    generate(myaray, myarray2, true);

                    enabledControlTextField(document.getElementById("<%= TextBoxOutcomePosteriorDiseaseOther.ClientID %>"), true, true);
                }
            });
            if ($('#<%= RadioButtonOutcomePosteriorDiseaseActiveYes.ClientID %>').prop("checked") == true) {
                var myaray = new Array("QO12aA", "QO12aB");
                var myarray2 = new Array('#QO12aB');
                generate(myaray, myarray2, false);
            }
            else {
                var myaray = new Array("QO12aA", "QO12aB", "QO12bA", "QO12bB");
                var myarray2 = new Array('#QO12aB', "#QO12bB");
                generate(myaray, myarray2, true);

                enabledControlTextField(document.getElementById("<%= TextBoxOutcomePosteriorDiseaseOther.ClientID %>"), true, true);
            }


            // Question 12 other - Outcomes ***************************
            $("#<%= RadioButtonListRBOutcomePosteriorDiseaseType.ClientID %>").click(function () {
                if ($('#<%= RadioButtonListRBOutcomePosteriorDiseaseType.ClientID %>').find('input:checked').val() == ('14')) {
                    var myaray = new Array("QO12bA", "QO12bB");
                    var myarray2 = new Array('#QO12bB');
                    generate(myaray, myarray2, false);

                    enabledControlTextField(document.getElementById("<%= TextBoxOutcomePosteriorDiseaseOther.ClientID %>"), false, false);
                }
                else {
                    var myaray = new Array("QO12bA", "QO12bB");
                    var myarray2 = new Array('#QO12bB');
                    generate(myaray, myarray2, true);

                    enabledControlTextField(document.getElementById("<%= TextBoxOutcomePosteriorDiseaseOther.ClientID %>"), true, true);
                }
            });
            if ($('#<%= RadioButtonListRBOutcomePosteriorDiseaseType.ClientID %>').find('input:checked').val() == ('14')) {
                var myaray = new Array("QO12bA", "QO12bB");
                var myarray2 = new Array('#QO12bB');
                generate(myaray, myarray2, false);

                enabledControlTextField(document.getElementById("<%= TextBoxOutcomePosteriorDiseaseOther.ClientID %>"), false, false);
            }
            else {
                var myaray = new Array("QO12bA", "QO12bB");
                var myarray2 = new Array('#QO12bB');
                generate(myaray, myarray2, true);

                enabledControlTextField(document.getElementById("<%= TextBoxOutcomePosteriorDiseaseOther.ClientID %>"), true, true);
            }


            // Question 14 - Outcomes ***************************
            $("#<%= RadioButtonOutcomeAdditionalSurgeryYes.ClientID %>").click(function () {
                if ($('#<%= RadioButtonOutcomeAdditionalSurgeryYes.ClientID %>').prop("checked") == true) {
                    var myaray = new Array("QO14aA", "QO14aB");
                    var myarray2 = new Array('#QO14aB');
                    generate(myaray, myarray2, false);
                }
                else {
                    var myaray = new Array("QO14aA", "QO14aB");
                    var myarray2 = new Array('#QO14aB');
                    generate(myaray, myarray2, true);

                    enabledControlTextField(document.getElementById("<%= TextBoxOutcomeAdditionalOtherText.ClientID %>"), true, true);
                }
            });
            $("#<%= RadioButtonOutcomeAdditionalSurgeryNo.ClientID %>").click(function () {
                if ($('#<%= RadioButtonOutcomeAdditionalSurgeryYes.ClientID %>').prop("checked") == true) {
                    var myaray = new Array("QO14aA", "QO14aB");
                    var myarray2 = new Array('#QO14aB');
                    generate(myaray, myarray2, false);
                }
                else {
                    var myaray = new Array("QO14aA", "QO14aB");
                    var myarray2 = new Array('#QO14aB');
                    generate(myaray, myarray2, true);

                    enabledControlTextField(document.getElementById("<%= TextBoxOutcomeAdditionalOtherText.ClientID %>"), true, true);
                }
            });
            if ($('#<%= RadioButtonOutcomeAdditionalSurgeryYes.ClientID %>').prop("checked") == true) {
                var myaray = new Array("QO14aA", "QO14aB");
                var myarray2 = new Array('#QO14aB');
                generate(myaray, myarray2, false);
            }
            else {
                var myaray = new Array("QO14aA", "QO14aB");
                var myarray2 = new Array('#QO14aB');
                generate(myaray, myarray2, true);

                enabledControlTextField(document.getElementById("<%= TextBoxOutcomeAdditionalOtherText.ClientID %>"), true, true);
            }


            // Question 14 other - Outcomes ***************************
            $("#<%= CheckBoxRBOutcomeAdditionalOther.ClientID %>").click(function () {
                if ($('#<%= CheckBoxRBOutcomeAdditionalOther.ClientID %>').prop("checked") == true) {
                    enabledControlTextField(document.getElementById("<%= TextBoxOutcomeAdditionalOtherText.ClientID %>"), false, false);
                }
                else {
                    enabledControlTextField(document.getElementById("<%= TextBoxOutcomeAdditionalOtherText.ClientID %>"), true, true);
                }
            });
            if ($('#<%= CheckBoxRBOutcomeAdditionalOther.ClientID %>').prop("checked") == true) {
                enabledControlTextField(document.getElementById("<%= TextBoxOutcomeAdditionalOtherText.ClientID %>"), false, false);
            }
            else {
                enabledControlTextField(document.getElementById("<%= TextBoxOutcomeAdditionalOtherText.ClientID %>"), true, true);
            }


            // Question 5 - Clinical Examination *******************
            $("#<%= RadioButtonVitreousCellsYes.ClientID %>").click(function () {
                if ($('#<%= RadioButtonVitreousCellsYes.ClientID %>').prop("checked") == true) {
                    var myaray = new Array("QC5A");
                    var myarray2 = new Array('#QC5A');
                    generate(myaray, myarray2, false);
                }
                else {
                    var myaray = new Array("QC5A");
                    var myarray2 = new Array('#QC5A');
                    generate(myaray, myarray2, true);
                }
            });
            $("#<%= RadioButtonVitreousCellsNo.ClientID %>").click(function () {
                if ($('#<%= RadioButtonVitreousCellsYes.ClientID %>').prop("checked") == true) {
                    var myaray = new Array("QC5A");
                    var myarray2 = new Array('#QC5A');
                    generate(myaray, myarray2, false);
                }
                else {
                    var myaray = new Array("QC5A");
                    var myarray2 = new Array('#QC5A');
                    generate(myaray, myarray2, true);
                }
            });
            if ($('#<%= RadioButtonVitreousCellsYes.ClientID %>').prop("checked") == true) {
                var myaray = new Array("QC5A");
                var myarray2 = new Array('#QC5A');
                generate(myaray, myarray2, false);
            }
            else {
                var myaray = new Array("QC5A");
                var myarray2 = new Array('#QC5A');
                generate(myaray, myarray2, true);
            }


            // Question 2 - Management *******************
            var q2mArrayA = new Array("QM2aA", "QM2aB", "QM2bA", "QM2bB", "QM2cA", "QM2cB", "QM2dA", "QM2dB");
            var q2mArrayB = new Array('#QM2aB', '#QM2bB', '#QM2cB', '#QM2dB');

            function q2MQuestion() {
                if ($('#<%= RadioButtonMgntPeriopCSYes.ClientID %>').prop("checked") == true)
                    generate(q2mArrayA, q2mArrayB, false);
                else
                    generate(q2mArrayA, q2mArrayB, true);
            }

            $("#<%= RadioButtonMgntPeriopCSYes.ClientID %>").click(function () {
                q2MQuestion();
            });

            $("#<%= RadioButtonMgntPeriopCSNo.ClientID %>").click(function () {
                q2MQuestion();
            });

            q2MQuestion();


            // Question 6 - Management *******************
            $("#<%= RadioButtonIOLImplantationYes.ClientID %>").click(function () {
                if ($('#<%= RadioButtonIOLImplantationYes.ClientID %>').prop("checked") == true) {
                    var myaray = new Array("QM6aA", "QM6aB", "QM6bA", "QM6bB", "QM6cA", "QM6cB", "QM6dA", "QM6dB");
                    var myarray2 = new Array('#QM6aB', '#QM6bB', '#QM6cB', '#QM6dB');
                    generate(myaray, myarray2, false);
                }
                else {
                    var myaray = new Array("QM6aA", "QM6aB", "QM6bA", "QM6bB", "QM6cA", "QM6cB", "QM6dA", "QM6dB");
                    var myarray2 = new Array('#QM6aB', '#QM6bB', '#QM6cB', '#QM6dB');
                    generate(myaray, myarray2, true);
                }
            });
            $("#<%= RadioButtonIOLImplantationNo.ClientID %>").click(function () {
                if ($('#<%= RadioButtonIOLImplantationYes.ClientID %>').prop("checked") == true) {
                    var myaray = new Array("QM6aA", "QM6aB", "QM6bA", "QM6bB", "QM6cA", "QM6cB", "QM6dA", "QM6dB");
                    var myarray2 = new Array('#QM6aB', '#QM6bB', '#QM6cB', '#QM6dB');
                    generate(myaray, myarray2, false);
                }
                else {
                    var myaray = new Array("QM6aA", "QM6aB", "QM6bA", "QM6bB", "QM6cA", "QM6cB", "QM6dA", "QM6dB");
                    var myarray2 = new Array('#QM6aB', '#QM6bB', '#QM6cB', '#QM6dB');
                    generate(myaray, myarray2, true);
                }
            });
            if ($('#<%= RadioButtonIOLImplantationYes.ClientID %>').prop("checked") == true) {
                var myaray = new Array("QM6aA", "QM6aB", "QM6bA", "QM6bB", "QM6cA", "QM6cB", "QM6dA", "QM6dB");
                var myarray2 = new Array('#QM6aB', '#QM6bB', '#QM6cB', '#QM6dB');
                generate(myaray, myarray2, false);
            }
            else {
                var myaray = new Array("QM6aA", "QM6aB", "QM6bA", "QM6bB", "QM6cA", "QM6cB", "QM6dA", "QM6dB");
                var myarray2 = new Array('#QM6aB', '#QM6bB', '#QM6cB', '#QM6dB');
                generate(myaray, myarray2, true);
            }


            // Question 2 - Clinical Examination
            $("#<%= RadioButtonListRBExamcornea.ClientID %>").click(function () {
                if (($('#<%= RadioButtonListRBExamcornea.ClientID %>').find('input:checked').val() == ('104')) || ($('#<%= RadioButtonListRBExamcornea.ClientID %>').find('input:checked').val() == ('105'))) {
                    var myaray = new Array("QC2aA", "QC2aB", "QC2bA", "QC2bB", "QC2cA", "QC2cB");
                    var myarray2 = new Array('#QC2aB', '#QC2bB', '#QC2cB');
                    generate(myaray, myarray2, false);
                }
                else {
                    var myaray = new Array("QC2aA", "QC2aB", "QC2bA", "QC2bB", "QC2cA", "QC2cB", "QC2dA", "QC2dB", "QC2eA", "QC2eB");
                    var myarray2 = new Array('#QC2aB', '#QC2bB', '#QC2cB', '#QC2dB', '#QC2eB');
                    generate(myaray, myarray2, true);
                }
            });
            if (($('#<%= RadioButtonListRBExamcornea.ClientID %>').find('input:checked').val() == ('104')) || ($('#<%= RadioButtonListRBExamcornea.ClientID %>').find('input:checked').val() == ('105'))) {
                var myaray = new Array("QC2aA", "QC2aB", "QC2bA", "QC2bB", "QC2cA", "QC2cB");
                var myarray2 = new Array('#QC2aB', '#QC2bB', '#QC2cB');
                generate(myaray, myarray2, false);
            }
            else {
                var myaray = new Array("QC2aA", "QC2aB", "QC2bA", "QC2bB", "QC2cA", "QC2cB", "QC2dA", "QC2dB", "QC2eA", "QC2eB");
                var myarray2 = new Array('#QC2aB', '#QC2bB', '#QC2cB', '#QC2dB', '#QC2eB');
                generate(myaray, myarray2, true);
            }


            // Question 2c - Clinical Examination
            $("#<%= RadioButtonListRBKPs.ClientID %>").click(function () {
                if ($('#<%= RadioButtonListRBKPs.ClientID %>').find('input:checked').val() == ('1')) {
                    var myaray = new Array("QC2dA", "QC2dB", "QC2eA", "QC2eB");
                    var myarray2 = new Array('#QC2dB', '#QC2eB');
                    generate(myaray, myarray2, false);
                }
                else {
                    var myaray = new Array("QC2dA", "QC2dB", "QC2eA", "QC2eB");
                    var myarray2 = new Array('#QC2dB', '#QC2eB');
                    generate(myaray, myarray2, true);
                }
            });
            if ($('#<%= RadioButtonListRBKPs.ClientID %>').find('input:checked').val() == ('1')) {
                var myaray = new Array("QC2dA", "QC2dB", "QC2eA", "QC2eB");
                var myarray2 = new Array('#QC2dB', '#QC2eB');
                generate(myaray, myarray2, false);
            }
            else {
                var myaray = new Array("QC2dA", "QC2dB", "QC2eA", "QC2eB");
                var myarray2 = new Array('#QC2dB', '#QC2eB');
                generate(myaray, myarray2, true);
            }

            // Question 4 - Clinical Examination
            $("#<%= RadioButtonListRBIris.ClientID %>").click(function () {
                if (($('#<%= RadioButtonListRBIris.ClientID %>').find('input:checked').val() == ('105')) || ($('#<%= RadioButtonListRBIris.ClientID %>').find('input:checked').val() == ('341')) || ($('#<%= RadioButtonListRBIris.ClientID %>').find('input:checked').val() == ('14'))) {
                    var myaray = new Array("QC4aA", "QC4aB", "QC4bA", "QC4bB", "QC4cA", "QC4cB", "QC4dA", "QC4dB", "QC4eAA", "QC4eAB", "QC4fA", "QC4fB", "QC4gA", "QC4gB");
                    var myarray2 = new Array('#QC4aB', '#QC4bB', '#QC4cB', '#QC4dB', '#QC4eAB', '#QC4fB', '#QC4gB');
                    generate(myaray, myarray2, false);

                    enabledControlTextField(document.getElementById("<%= TextBoxRBIrisClockHours.ClientID %>"), false, false);
                    enabledControlTextField(document.getElementById("<%= TextBoxIOP.ClientID %>"), false, false);
                }
                else {
                    "QC4eA", "QC4eB", "QC4eC"
                    var myaray = new Array("QC4aA", "QC4aB", "QC4bA", "QC4bB", "QC4cA", "QC4cB", "QC4dA", "QC4dB", "QC4eAA", "QC4eAB", "QC4eA", "QC4eB", "QC4eC", "QC4fA", "QC4fB", "QC4gA", "QC4gB");
                    var myarray2 = new Array('#QC4aB', '#QC4bB', '#QC4cB', '#QC4dB', '#QC4eAB', '#QC4eA', '#QC4eB', '#QC4eC', '#QC4fB', '#QC4gB');
                    generate(myaray, myarray2, true);

                    enabledControlTextField(document.getElementById("<%= TextBoxRBIrisClockHours.ClientID %>"), true, true);
                    enabledControlTextField(document.getElementById("<%= TextBoxIOP.ClientID %>"), true, true);
                }
            });
            if (($('#<%= RadioButtonListRBIris.ClientID %>').find('input:checked').val() == ('105')) || ($('#<%= RadioButtonListRBIris.ClientID %>').find('input:checked').val() == ('341')) || ($('#<%= RadioButtonListRBIris.ClientID %>').find('input:checked').val() == ('14'))) {
                var myaray = new Array("QC4aA", "QC4aB", "QC4bA", "QC4bB", "QC4cA", "QC4cB", "QC4dA", "QC4dB", "QC4eAA", "QC4eAB", "QC4fA", "QC4fB", "QC4gA", "QC4gB");
                var myarray2 = new Array('#QC4aB', '#QC4bB', '#QC4cB', '#QC4dB', '#QC4eAB', '#QC4fB', '#QC4gB');
                generate(myaray, myarray2, false);

                enabledControlTextField(document.getElementById("<%= TextBoxRBIrisClockHours.ClientID %>"), false, false);
                enabledControlTextField(document.getElementById("<%= TextBoxIOP.ClientID %>"), false, false);
            }
            else {
                var myaray = new Array("QC4aA", "QC4aB", "QC4bA", "QC4bB", "QC4cA", "QC4cB", "QC4dA", "QC4dB", "QC4eAA", "QC4eAB", "QC4eA", "QC4eB", "QC4eC", "QC4fA", "QC4fB", "QC4gA", "QC4gB");
                var myarray2 = new Array('#QC4aB', '#QC4bB', '#QC4cB', '#QC4dB', '#QC4eAB', '#QC4eA', '#QC4eB', '#QC4eC', '#QC4fB', '#QC4gB');
                generate(myaray, myarray2, true);

                enabledControlTextField(document.getElementById("<%= TextBoxRBIrisClockHours.ClientID %>"), true, true);
                enabledControlTextField(document.getElementById("<%= TextBoxIOP.ClientID %>"), true, true);
            }

            // Question 4e - Nuclear *******************
            $("#<%= CheckBoxNuclear.ClientID %>").click(function () {
                if ($('#<%= CheckBoxNuclear.ClientID %>').prop("checked") == true) {
                    var myaray = new Array("QC4eA");
                    var myarray2 = new Array('#QC4eA');
                    generate(myaray, myarray2, false);
                }
                else {
                    var myaray = new Array("QC4eA");
                    var myarray2 = new Array('#QC4eA');
                    generate(myaray, myarray2, true);
                }
            });
            if ($('#<%= CheckBoxNuclear.ClientID %>').prop("checked") == true) {
                var myaray = new Array("QC4eA");
                var myarray2 = new Array('#QC4eA');
                generate(myaray, myarray2, false);
            }
            else {
                var myaray = new Array("QC4eA");
                var myarray2 = new Array('#QC4eA');
                generate(myaray, myarray2, true);
            }

            // Question 4e - Cortical *******************
            $("#<%= Cortical.ClientID %>").click(function () {
                if ($('#<%= Cortical.ClientID %>').prop("checked") == true) {
                    var myaray = new Array("QC4eB");
                    var myarray2 = new Array('#QC4eB');
                    generate(myaray, myarray2, false);
                }
                else {
                    var myaray = new Array("QC4eB");
                    var myarray2 = new Array('#QC4eB');
                    generate(myaray, myarray2, true);
                }
            });
            if ($('#<%= Cortical.ClientID %>').prop("checked") == true) {
                var myaray = new Array("QC4eB");
                var myarray2 = new Array('#QC4eB');
                generate(myaray, myarray2, false);
            }
            else {
                var myaray = new Array("QC4eB");
                var myarray2 = new Array('#QC4eB');
                generate(myaray, myarray2, true);
            }

            // Question 4e - Posterior *******************
            $("#<%= Posterior.ClientID %>").click(function () {
                if ($('#<%= Posterior.ClientID %>').prop("checked") == true) {
                    var myaray = new Array("QC4eC");
                    var myarray2 = new Array('#QC4eC');
                    generate(myaray, myarray2, false);
                }
                else {
                    var myaray = new Array("QC4eC");
                    var myarray2 = new Array('#QC4eC');
                    generate(myaray, myarray2, true);
                }
            });
            if ($('#<%= Posterior.ClientID %>').prop("checked") == true) {
                var myaray = new Array("QC4eC");
                var myarray2 = new Array('#QC4eC');
                generate(myaray, myarray2, false);
            }
            else {
                var myaray = new Array("QC4eC");
                var myarray2 = new Array('#QC4eC');
                generate(myaray, myarray2, true);
            }


            // Question 6 - Outcomes ***************************
            $("#<%= RadioButtonOutcomeCapsularPhimosisYes.ClientID %>").click(function () {
                if ($('#<%= RadioButtonOutcomeCapsularPhimosisYes.ClientID %>').prop("checked") == true) {
                    var myaray = new Array("QO6bA", "QO6bB");
                    var myarray2 = new Array('#QO6bB');
                    generate(myaray, myarray2, false);
                }
                else {
                    var myaray = new Array("QO6bA", "QO6bB");
                    var myarray2 = new Array('#QO6bB');
                    generate(myaray, myarray2, true);
                }
            });
            $("#<%= RadioButtonOutcomeCapsularPhimosisNo.ClientID %>").click(function () {
                if ($('#<%= RadioButtonOutcomeCapsularPhimosisYes.ClientID %>').prop("checked") == true) {
                    var myaray = new Array("QO6bA", "QO6bB");
                    var myarray2 = new Array('#QO6bB');
                    generate(myaray, myarray2, false);
                }
                else {
                    var myaray = new Array("QO6bA", "QO6bB");
                    var myarray2 = new Array('#QO6bB');
                    generate(myaray, myarray2, true);
                }
            });
            if ($('#<%= RadioButtonOutcomeCapsularPhimosisYes.ClientID %>').prop("checked") == true) {
                var myaray = new Array("QO6bA", "QO6bB");
                var myarray2 = new Array('#QO6bB');
                generate(myaray, myarray2, false);
            }
            else {
                var myaray = new Array("QO6bA", "QO6bB");
                var myarray2 = new Array('#QO6bB');
                generate(myaray, myarray2, true);
            }
        });
    </script>

    <script type="text/javascript">
        $(function () {
            $('#QCE2').aToolTip({
                clickIt: true,
                tipContent: '20/800 or count fingers @ 5 ft<br />20/1000 or count fingers @ 4 ft<br />20/1600 or count fingers @ 3ft<br />20/2000 or count fingers @ 2 ft<br />20/4000 or count fingers @ 1 ft<br />20/7777 =CF<br />20/8888 =HM<br />20/9999 =LP<br />20/0000 =NLP'
            });

            $('#QO2').aToolTip({
                clickIt: true,
                tipContent: '20/800 or count fingers @ 5 ft<br />20/1000 or count fingers @ 4 ft<br />20/1600 or count fingers @ 3ft<br />20/2000 or count fingers @ 2 ft<br />20/4000 or count fingers @ 1 ft<br />20/7777 =CF<br />20/8888 =HM<br />20/9999 =LP<br />20/0000 =NLP'
            });
        });
    </script>
</asp:Content>