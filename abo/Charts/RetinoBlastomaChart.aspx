﻿<%@ Page Title="" Language="C#" MasterPageFile="~/PIMMasterPage.master" AutoEventWireup="true" CodeFile="RetinoBlastomaChart.aspx.cs" Inherits="abo_RetinoBlastomaChart" %>

<asp:Content ID="Content1" ContentPlaceHolderID="head" Runat="Server">

    <script type="text/javascript" language="javascript" src="http://ajax.googleapis.com/ajax/libs/jquery/1.7.1/jquery.min.js"></script>
    <link type="text/css" href="../common/css/atooltip.css" rel="stylesheet"  media="screen" />
	<script type="text/javascript" src="../common/js/jquery.atooltip.js"></script>
    <script type="text/javascript" language="javascript">
        //      Either enables or disables input boxes
        function enabledControl(el, disabled, checked) {
            //alert("Hello world from enabled control");
            try {
                if (disabled) {
                    el.disabled = disabled;
                    el.setAttribute("disabled", true);
                }
                else {
                    el.disabled = disabled;
                    el.removeAttribute("disabled");
                }

                if (checked == true)
                    el.checked = false;
            }
            catch (E) {
            }
            if (el.childNodes && el.childNodes.length > 0) {
                for (var x = 0; x < el.childNodes.length; x++) {
                    enabledControl(el.childNodes[x], disabled, checked);
                }
            }
        }
        //      Either enables or disables dropdown boxes
        function enabledControlDropDown(el, disabled, checked) {
            //alert("Hello world from enabled control");
            try {
                if (disabled) {
                    el.disabled = disabled;
                    el.setAttribute("disabled", true);
                }
                else {
                    el.disabled = disabled;
                    el.removeAttribute("disabled");
                }

                if (checked == true)
                    el.options[0].selected = true;
            }
            catch (E) {
            }
            if (el.childNodes && el.childNodes.length > 0) {
                for (var x = 0; x < el.childNodes.length; x++) {
                    enabledControl(el.childNodes[x], disabled, checked);
                }
            }
        }
        //      Either enables or disables text boxes
        function enabledControlTextField(el, disabled, checked) {
            //alert("Hello world from enabled control");
            try {
                if (disabled) {
                    el.disabled = disabled;
                    el.setAttribute("disabled", true);
                }
                else {
                    el.disabled = disabled;
                    el.removeAttribute("disabled");
                }

                if (checked == true) {
                    el.value = '';
                }
            }
            catch (E) {
            }
            if (el.childNodes && el.childNodes.length > 0) {
                for (var x = 0; x < el.childNodes.length; x++) {
                    enabledControl(el.childNodes[x], disabled, checked);
                }
            }
        }

        function generate(arr1, arr2, istrue) {
            if (istrue == true) {
                for (var i = 0; i < arr1.length; i++) {
                    document.getElementById(arr1[i]).style.color = "gray";
                }
                for (var i = 0; i < arr2.length; i++) {
                    $(arr2[i] + ' :input').attr('disabled', true);
                    $(arr2[i] + ' :input').removeAttr("checked");
                }
            }
            if (istrue == false) {
                for (var i = 0; i < arr1.length; i++) {
                    document.getElementById(arr1[i]).style.color = "#333";
                }
                for (var i = 0; i < arr2.length; i++) {
                    $(arr2[i] + ' :input').removeAttr('disabled');
                }
            }
        }

        function toggleQH3DD() {
            document.getElementById("<%= DropDownListMonthOfFirstSymptoms.ClientID %>").options[0].selected = true;
            document.getElementById("<%= DropDownListYearOfFirstSymptoms.ClientID %>").options[0].selected = true;
        }

        function toggleQH3CB() {
            document.getElementById("<%= CheckBoxFirstSymptomsDateNA.ClientID %>").checked = false;
        }
        
    </script>

    <script  type="text/javascript" language="javascript">
        $(document).ready(function() {

            $("#<%= RadioButtonListRBPrimaryTreatmentModalityOD.ClientID %>").click(function() {
                if ($('#<%= RadioButtonListRBPrimaryTreatmentModalityOD.ClientID %>').find('input:checked').val() == ('14')) {
                    enabledControlTextField(document.getElementById("<%= TextBoxRBPrimaryTreatmentOtherOD.ClientID %>"), false, false);
                }
                else {
                    enabledControlTextField(document.getElementById("<%= TextBoxRBPrimaryTreatmentOtherOD.ClientID %>"), true, true);
                }
            });

            $("#<%= RadioButtonListRBPrimaryTreatmentModalityOS.ClientID %>").click(function() {
                if ($('#<%= RadioButtonListRBPrimaryTreatmentModalityOS.ClientID %>').find('input:checked').val() == ('14')) {
                    enabledControlTextField(document.getElementById("<%= TextBoxRBPrimaryTreatmentOtherOS.ClientID %>"), false, false);
                }
                else {
                    enabledControlTextField(document.getElementById("<%= TextBoxRBPrimaryTreatmentOtherOS.ClientID %>"), true, true);
                }
            });

            if ($('#<%= RadioButtonListRBPrimaryTreatmentModalityOD.ClientID %>').find('input:checked').val() == ('14')) {
                enabledControlTextField(document.getElementById("<%= TextBoxRBPrimaryTreatmentOtherOD.ClientID %>"), false, false);
            }
            else {
                enabledControlTextField(document.getElementById("<%= TextBoxRBPrimaryTreatmentOtherOD.ClientID %>"), true, true);
            }

            if ($('#<%= RadioButtonListRBPrimaryTreatmentModalityOS.ClientID %>').find('input:checked').val() == ('14')) {
                enabledControlTextField(document.getElementById("<%= TextBoxRBPrimaryTreatmentOtherOS.ClientID %>"), false, false);
            }
            else {
                enabledControlTextField(document.getElementById("<%= TextBoxRBPrimaryTreatmentOtherOS.ClientID %>"), true, true);
            }

            // Surgery OD Q1
            $("#<%= RadioButtonListRBTumorStageOD.ClientID %>").click(function () {
                if ($('#<%= RadioButtonListRBTumorStageOD.ClientID %>').find('input:checked').val() == ('48')) {
                    enabledControlDropDown(document.getElementById("<%= TextBoxTumorStageODOther.ClientID %>"), true, true);
                }
                else {
                    enabledControlDropDown(document.getElementById("<%= TextBoxTumorStageODOther.ClientID %>"), false, false);
                }
            });
            if ($('#<%= RadioButtonListRBTumorStageOD.ClientID %>').find('input:checked').val() == ('48')) {
                enabledControlDropDown(document.getElementById("<%= TextBoxTumorStageODOther.ClientID %>"), true, true);
            }
            else {
                enabledControlDropDown(document.getElementById("<%= TextBoxTumorStageODOther.ClientID %>"), false, false);
            }

            // Surgery OS Q1
            $("#<%= RadioButtonListRBTumorStageOS.ClientID %>").click(function () {
                if ($('#<%= RadioButtonListRBTumorStageOS.ClientID %>').find('input:checked').val() == ('48')) {
                    enabledControlDropDown(document.getElementById("<%= TextBoxTumorStageOSOther.ClientID %>"), true, true);
                }
                else {
                    enabledControlDropDown(document.getElementById("<%= TextBoxTumorStageOSOther.ClientID %>"), false, false);
                }
            });
            if ($('#<%= RadioButtonListRBTumorStageOS.ClientID %>').find('input:checked').val() == ('48')) {
                enabledControlDropDown(document.getElementById("<%= TextBoxTumorStageOSOther.ClientID %>"), true, true);
            }
            else {
                enabledControlDropDown(document.getElementById("<%= TextBoxTumorStageOSOther.ClientID %>"), false, false);
            }
        });
    </script>
    <style type="text/css">
        .bginputa{
	        float:right;
	        margin:21px 0 0 1px;
	        background: url(../common/images/orange_button_with_arrow.png) no-repeat;
	        overflow:hidden;
	        height:35px;

        }
        .bginputa a{
	        color: white;
	        text-align:center;
	        height:35px;
	        line-height:28px;
	        padding:0 14px 15px  ;
	        cursor:pointer;
	        float:left;
	        border:none;
	        text-decoration:none;
	        font-family:Arial, Helvetica, sans-serif; 
	        font-size:12px; 
	        font-weight:bold; 
	        letter-spacing: 0px;
        }
        .bginputa a:hover{
	        text-decoration:none;
        }
        .right {
            text-align:right;
        }
    </style>
    <script type="text/javascript">
        $(function() {
            $('#Q').aToolTip({
                clickIt: true,
                tipContent: ''
            });
        });
    </script>
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder1" Runat="Server">
    <asp:HiddenField ID="HiddenFieldRecordIdentifier" runat="server"/>
        <!--  pim -->
        <div class="pim">
            <div class="heading_box" style="vertical-align:middle">
                <h2>Retinoblastoma Chart Abstraction</h2>
                <h3>RECORD IDENTIFIER: <asp:Literal runat="server" ID="LiteralRecordIdentifier"></asp:Literal></h3>
            </div>
            <br />
            <!-- History -->
            <table>
                <tr>
                    <th colspan="2" width="910px"><p>History</p></th>
                </tr>
                <!-- Question 1 -->
                <tr class="table_body">
                    <td style="width:70%;"><strong>1. Date of birth</strong>  
                        <asp:Label ID="LabelMonthOfBirth" runat="server" Visible="false"  ForeColor="Red" Text="<br />Please enter Month"></asp:Label>
                        <asp:Label ID="LabelYearOfBirth" runat="server" Visible="false"  ForeColor="Red"  Text="<br />Please enter Year "></asp:Label>
                    </td>
                    <td>
                        <asp:DropDownList ID="DropDownListMonthOfBirth" DataValueField="MonthID" DataTextField="MonthName" runat="server" />
                        <asp:DropDownList ID="DropDownListYearOfBirth" DataValueField="YearID" DataTextField="YearName" runat="server" />
                    </td>
                </tr>
                <!-- Question 2 -->
                <tr class="table_body_bg">
                    <td><strong>2. Date of treatment</strong>
                        <asp:Label ID="LabelMonthOfTreatment" runat="server" Visible="false"  ForeColor="Red" Text="<br />Please enter Month"></asp:Label>
                        <asp:Label ID="LabelYearOfTreatment" runat="server" Visible="false"  ForeColor="Red"  Text="<br />Please enter Year "></asp:Label>
                    </td>
                    <td>
                        <asp:DropDownList ID="DropDownListMonthOfTreatment" DataValueField="MonthID" DataTextField="MonthName" runat="server" />
                        <asp:DropDownList ID="DropDownListYearOfTreatment" DataValueField="YearID" DataTextField="YearName" runat="server" />
                    </td>
                </tr>
                <!-- Question 3 -->
                <tr class="table_body">
                    <td><strong>3. When did the patient first note symptoms?</strong>
                        <asp:Label ID="LabelMonthOfFirstSymptoms" runat="server" Visible="false"  ForeColor="Red" Text="<br />Please enter Month"></asp:Label>
                        <asp:Label ID="LabelYearOfFirstSymptoms" runat="server" Visible="false"  ForeColor="Red"  Text="<br />Please enter Year"></asp:Label>
                   </td>
                    <td>
                        <asp:DropDownList ID="DropDownListMonthOfFirstSymptoms" DataValueField="MonthID" DataTextField="MonthName" runat="server" onchange="toggleQH3CB();" />
                        <asp:DropDownList ID="DropDownListYearOfFirstSymptoms" DataValueField="YearID" DataTextField="YearName" runat="server" onchange="toggleQH3CB();" />
                        <br /><asp:CheckBox runat="server" ID="CheckBoxFirstSymptomsDateNA" text="&nbsp;Not Documented" onclick="toggleQH3DD();" />
                    </td>
                </tr>
                <!-- Question 4 -->
                <tr class="table_body_bg">
                    <td><strong>4. Gender</strong>
                         <asp:Label ID="LabelRBGender" runat="server" Visible="false"  ForeColor="Red" Text="<br />Please complete"></asp:Label>
                    </td>
                    <td>
                        <asp:RadioButtonList id="RadioButtonListRBGender" RepeatDirection="Horizontal" CssClass="aspxList" runat="server">
                            <asp:ListItem Value="42">&nbsp;Male</asp:ListItem>
                            <asp:ListItem Value="43">&nbsp;Female</asp:ListItem>
                        </asp:RadioButtonList>

                    </td>
                </tr>
                <!-- Question 5 -->
                <tr class="table_body">
                    <td><strong>5. Was the patient’s reason for the visit documented?</strong>
                         <asp:Label ID="LabelRBVisitReason" runat="server" Visible="false"  ForeColor="Red" Text="<br />Please complete"></asp:Label>
                    </td>
                    <td>
                        <asp:RadioButtonList id="RadioButtonListRBVisitReason" RepeatDirection="Horizontal" CssClass="aspxList" runat="server">
                            <asp:ListItem Value="1">&nbsp;Yes</asp:ListItem>
                            <asp:ListItem Value="2">&nbsp;No</asp:ListItem>
                        </asp:RadioButtonList>
                    </td>
                </tr>
                <!-- Question 6 -->
                <tr class="table_body_bg">
                    <td><strong>6. Was the patient’s chief visual complaint or symptoms documented?</strong>
                         <asp:Label ID="LabelRBComplaintDocumented" runat="server" Visible="false"  ForeColor="Red" Text="<br />Please complete"></asp:Label>
                    </td>
                    <td>
                        <asp:RadioButtonList id="RadioButtonListRBComplaintDocumented" RepeatDirection="Horizontal" CssClass="aspxList" runat="server">
                            <asp:ListItem Value="1">&nbsp;Yes</asp:ListItem>
                            <asp:ListItem Value="2">&nbsp;No</asp:ListItem>
                        </asp:RadioButtonList>
                    </td>
                </tr>
            </table>
            <br />
            <!-- Examination -->
            <table>
                <tr>
                    <th colspan="3" width="910px"><p>Examination</p></th>
                </tr>
                <!-- Question 1 -->
                <tr class="table_body">
                    <td width="70%"><strong>1. How was the fundus appearance documented?</strong>
                         <asp:Label ID="LabelFundusDocDrawing" runat="server" Visible="false"  ForeColor="Red" Text="<br />Please complete"></asp:Label>
                    </td>
                    <td>
                        <asp:CheckBox runat="server" ID="CheckBoxFundusDocDrawing" text="&nbsp;Drawing " />
                        <br /><asp:CheckBox runat="server" ID="CheckBoxFundusDocPhotography" text="&nbsp;Photography" />
                    </td>
                </tr>
                <!-- Question 2 -->
                <tr class="table_body_bg">
                    <td><strong>2. Was echography performed?</strong>
                         <asp:Label ID="LabelEchographyPerformed" runat="server" Visible="false"  ForeColor="Red" Text="<br />Please complete"></asp:Label>
                    </td>
                    <td>
                        <asp:RadioButton runat="server" ID="RadioButtonEchographyPerformedYes" GroupName="EchographyPerformed" text="&nbsp;Yes" />&nbsp;&nbsp;
                        <asp:RadioButton runat="server" ID="RadioButtonEchographyPerformedNo" GroupName="EchographyPerformed" text="&nbsp;No" />
                    </td>
                </tr>
                <!-- Question 3 -->
                <tr class="table_body">
                    <td><strong>3. How was the optic nerve and pineal gland imaged?</strong>
                         <asp:Label ID="LabelOpticNerve" runat="server" Visible="false"  ForeColor="Red" Text="<br />Please complete"></asp:Label>
                    </td>
                    <td>
                        <asp:CheckBox runat="server" ID="CheckBoxOpticNerveImagedCT" text="&nbsp;CT Scan" />
                        <br /><asp:CheckBox runat="server" ID="CheckBoxOpticNerveImagedMRI" text="&nbsp;MRI" />
                        <br /><asp:CheckBox runat="server" ID="CheckBoxOpticNerveNotImaged" text="&nbsp;Not Imaged" />
                    </td>
                </tr>
            </table>
            <br />
            <!-- Surgery (THESE QUESTIONS PERTAIN TO THE RIGHT EYE) -->
            <table>
                <tr>
                    <th colspan="2" width="910px"><p>Surgery (these questions pertain to the right eye)</p></th>
                </tr>
                <!-- Question 1 -->
                <tr class="table_body">
                    <td width="70%"><strong>1. What was the stage of the tumor?</strong>
                         <asp:Label ID="LabelRBTumorStageOD" runat="server" Visible="false"  ForeColor="Red" Text="<br />Please complete"></asp:Label>
                    </td>
                    <td>
                        <asp:RadioButtonList id="RadioButtonListRBTumorStageOD" RepeatDirection="Vertical" CssClass="aspxList" runat="server">
                            <asp:ListItem Value="96">&nbsp;Reese-Ellsworth</asp:ListItem>
                            <asp:ListItem Value="97">&nbsp;ABC</asp:ListItem>
                            <asp:ListItem Value="48">&nbsp;Not Documented</asp:ListItem>
                        </asp:RadioButtonList>
                        <asp:DropDownList ID="TextBoxTumorStageODOther" runat="server" style="margin-top:5px;">
                            <asp:ListItem Value="0"  Text="Choose"></asp:ListItem>
                            <asp:ListItem Value="1"  Text="Group A/Group I"></asp:ListItem>
                            <asp:ListItem Value="2"  Text="Group B/Group II"></asp:ListItem>
                            <asp:ListItem Value="3"  Text="Group C/Group III"></asp:ListItem>
                            <asp:ListItem Value="4"  Text="Group D/Group IV"></asp:ListItem>
                            <asp:ListItem Value="5"  Text="Group E/Group V"></asp:ListItem>
                        </asp:DropDownList>
                    </td>
                </tr>
                <!-- Question 2 -->
                <tr class="table_body_bg">
                    <td><strong>2. What was the primary treatment modality?</strong>
                         <asp:Label ID="LabelRBPrimaryTreatmentModalityOD" runat="server" Visible="false"  ForeColor="Red" Text="<br />Please complete"></asp:Label>
                    </td>
                    <td>
                        <asp:RadioButtonList id="RadioButtonListRBPrimaryTreatmentModalityOD" RepeatDirection="Vertical" CssClass="aspxList" runat="server">
                            <asp:ListItem Value="98">&nbsp;Chemoreduction and Laser</asp:ListItem>
                            <asp:ListItem Value="99">&nbsp;Enucleation</asp:ListItem>
                            <asp:ListItem Value="100">&nbsp;Intra-Ophthalmic Artery Chemotherapy</asp:ListItem>
                            <asp:ListItem Value="14">&nbsp;Other</asp:ListItem>
                            <asp:ListItem Value="48">&nbsp;Not Documented</asp:ListItem>
                        </asp:RadioButtonList>
                    </td>
                </tr>
                <!-- Question 2a -->
                <tr class="table_body_bg">
                    <td><strong>2a. If other, please specify:</strong>
                         <asp:Label ID="LabelRBPrimaryTreatmentOtherOD" runat="server" Visible="false"  ForeColor="Red" Text="<br />Please complete"></asp:Label>
                    </td>
                    <td>
                        <asp:TextBox runat="server" ID="TextBoxRBPrimaryTreatmentOtherOD" size="25"/>
                    </td>
                </tr>
            </table>
            <br />
            <!-- Surgery (THESE QUESTIONS PERTAIN TO THE LEFT EYE) -->
            <table>
                <tr>
                    <th colspan="2" width="910px"><p>Surgery (these questions pertain to the left eye)</p></th>
                </tr>
                <!-- Question 1 -->
                <tr class="table_body">
                    <td width="70%">
                        <strong>1. What was the stage of the tumor?</strong> 
                        <asp:Label ID="LabelRBTumorStageOS" runat="server" Visible="false"  ForeColor="Red" Text="<br />Please complete"></asp:Label>
                    </td>
                    <td>
                        <asp:RadioButtonList id="RadioButtonListRBTumorStageOS" RepeatDirection="Vertical" CssClass="aspxList" runat="server">
                            <asp:ListItem Value="96">&nbsp;Reese-Ellsworth</asp:ListItem>
                            <asp:ListItem Value="97">&nbsp;ABC</asp:ListItem>
                            <asp:ListItem Value="48">&nbsp;Not Documented</asp:ListItem>
                        </asp:RadioButtonList>
                        <asp:DropDownList ID="TextBoxTumorStageOSOther" runat="server" style="margin-top:5px;">
                            <asp:ListItem Value="0"  Text="Choose"></asp:ListItem>
                            <asp:ListItem Value="1"  Text="Group A/Group I"></asp:ListItem>
                            <asp:ListItem Value="2"  Text="Group B/Group II"></asp:ListItem>
                            <asp:ListItem Value="3"  Text="Group C/Group III"></asp:ListItem>
                            <asp:ListItem Value="4"  Text="Group D/Group IV"></asp:ListItem>
                            <asp:ListItem Value="5"  Text="Group E/Group V"></asp:ListItem>
                        </asp:DropDownList>
                    </td>
                </tr>
                <!-- Question 2 -->
                <tr class="table_body_bg">
                    <td>
                        <strong>2. What was the primary treatment modality?</strong> 
                        <asp:Label ID="LabelRBPrimaryTreatmentModalityOS" runat="server" Visible="false"  ForeColor="Red" Text="<br />Please complete"></asp:Label>
                    </td>
                    <td>
                        <asp:RadioButtonList id="RadioButtonListRBPrimaryTreatmentModalityOS" RepeatDirection="Vertical" CssClass="aspxList" runat="server" >
                            <asp:ListItem Value="98">&nbsp;Chemoreduction and Laser</asp:ListItem>
                            <asp:ListItem Value="99">&nbsp;Enucleation</asp:ListItem>
                            <asp:ListItem Value="100">&nbsp;Intra-Ophthalmic Artery Chemotherapy</asp:ListItem>
                            <asp:ListItem Value="14">&nbsp;Other</asp:ListItem>
                            <asp:ListItem Value="48">&nbsp;Not Documented</asp:ListItem>
                        </asp:RadioButtonList>
                    </td>
                </tr>
                <!-- Question 2a -->
                <tr class="table_body_bg">
                    <td>
                        <strong>2a. If other, please specify:</strong>
                        <asp:Label ID="LabelRBPrimaryTreatmentOtherOS" runat="server" Visible="false"  ForeColor="Red" Text="<br />Please complete"></asp:Label>
                    </td>
                    <td>
                        <asp:TextBox runat="server" ID="TextBoxRBPrimaryTreatmentOtherOS" size="25" />
                    </td>
                </tr>
            </table>
            <br />
            <!-- Communication -->
            <table>
                <tr>
                    <th colspan="2" width="910px"><p>Communication</p></th>
                </tr>
                <!-- Question 1 -->
                <tr class="table_body">
                    <td width="70%">
                        <strong>1. Was there documentation of informed consent for surgery?</strong>
                        <asp:Label ID="LabelDocInformedConsent" runat="server" Visible="false"  ForeColor="Red" Text="<br />Please complete"></asp:Label>
                    </td>
                    <td>
                        <asp:RadioButton runat="server" ID="RadioButtonDocInformedConsentYes" GroupName="DocInformedConsent" text="&nbsp;Yes" />&nbsp;&nbsp;
                        <asp:RadioButton runat="server" ID="RadioButtonDocInformedConsentNo" GroupName="DocInformedConsent" text="&nbsp;No" />
                    </td>
                </tr>
                <!-- Question 2 -->
                <tr class="table_body_bg">
                    <td>
                        <strong>2. Was there documentation of communication with the patient’s primary care physician or oncologist within 1 month of the initial diagnosis?</strong>
                        <asp:Label ID="LabelDocPCP" runat="server" Visible="false"  ForeColor="Red" Text="<br />Please complete"></asp:Label>
                    </td>
                    <td>
                        <asp:RadioButton runat="server" ID="RadioButtonDocPCPYes" GroupName="DocPCP" text="&nbsp;Yes" />&nbsp;&nbsp;
                        <asp:RadioButton runat="server" ID="RadioButtonDocPCPNo" GroupName="DocPCP" text="&nbsp;No" />
                    </td>
                </tr>
            </table>
            <br />
            <!-- Outcomes -->
            <table>
                <tr>
                    <th colspan="2" width="910px"><p>Outcomes</p></th>
                </tr>
                <!-- Question 1 -->
                <tr class="table_body">
                    <td width="70%">
                        <strong>1. If the primary treatment was enucleation, does your chart contain documentation of pathologic evaluation of all of the following features: retinoblastoma was the correct diagnosis, degree of optic nerve invasion, and presence or absence of choroidal invasion?</strong>
                        <asp:Label ID="LabelDocPathologicEval" runat="server" Visible="false"  ForeColor="Red" Text="<br />Please complete"></asp:Label>
                    </td>
                    <td>
                        <asp:RadioButton runat="server" ID="RadioButtonDocPathologicEvalYes" GroupName="DocPathologicEval" text="&nbsp;Yes" />&nbsp;&nbsp;
                        <asp:RadioButton runat="server" ID="RadioButtonDocPathologicEvalNo" GroupName="DocPathologicEval" text="&nbsp;No" />
                    </td>
                </tr>
                <!-- Question 2 -->
                <tr class="table_body_bg">
                    <td>
                        <strong>2. If the primary treatment was a globe sparing treatment, was the eye subsequently enucleated in the 5 years of follow-up?</strong>
                        <asp:Label ID="LabelGlobSparingEnucleated" runat="server" Visible="false"  ForeColor="Red" Text="<br />Please complete"></asp:Label>
                    </td>
                    <td>
                        <asp:RadioButton runat="server" ID="RadioButtonGlobSparingEnucleatedYes" GroupName="GlobSparingEnucleated" text="&nbsp;Yes" />&nbsp;&nbsp;
                        <asp:RadioButton runat="server" ID="RadioButtonGlobSparingEnucleatedNo" GroupName="GlobSparingEnucleated" text="&nbsp;No" />
                    </td>
                </tr>
                <!-- Question 3 -->
                <tr class="table_body">
                    <td>
                        <strong>3. Does your chart contain documentation of genetic counseling by one of the following individuals:  Ophthalmologist, genetic counselor, or geneticist?</strong>
                        <asp:Label ID="LabelDocGeneticCounseling" runat="server" Visible="false"  ForeColor="Red" Text="<br />Please complete"></asp:Label>
                    </td>
                    <td>
                        <asp:RadioButton runat="server" ID="RadioButtonDocGeneticCounselingYes" GroupName="DocGeneticCounseling" text="&nbsp;Yes" />&nbsp;&nbsp;
                        <asp:RadioButton runat="server" ID="RadioButtonDocGeneticCounselingNo" GroupName="DocGeneticCounseling" text="&nbsp;No" />
                    </td>
                </tr>
                <!-- Question 4 -->
                <tr class="table_body_bg">
                    <td>
                        <strong>4. If there are younger siblings, was genetic screening performed to determine the risk for retinoblastoma?</strong>
                        <asp:Label ID="LabelRBDocGeneticScreening" runat="server" Visible="false"  ForeColor="Red" Text="<br />Please complete"></asp:Label>
                    </td>
                    <td>
                        <asp:RadioButtonList id="RadioButtonListRBDocGeneticScreening" RepeatDirection="Horizontal" CssClass="aspxList" runat="server">
                            <asp:ListItem Value="1">&nbsp;Yes</asp:ListItem>
	                        <asp:ListItem Value="2">&nbsp;No</asp:ListItem>
	                        <asp:ListItem Value="3">&nbsp;Not Recorded</asp:ListItem>
                        </asp:RadioButtonList>
                    </td>
                </tr>
            </table>
            <br />
              <div class="button-box">
            <asp:LinkButton ID="LinkButtonBackToDashboard" runat="server" Text="Back To Chart Registration" PostBackUrl="PatientChartRegistration.aspx?CycleNumber=1" Visible="false"/>
            <div style="display:none" class="sumbit_chart_btn"><asp:ImageButton ID="ButtonSubmit" OnClick="ButtonSubmit_Click" runat="server" ImageUrl="../../common/images/submit_chart_btn.jpg"  /></div>
            <asp:LinkButton ID="LinkButton1"  OnClick="ButtonSubmit_Click" runat="server" Text="Submit Chart" CssClass="button" />
                  </div>
        </div>
        <!-- ION pim ends -->
</asp:Content>

