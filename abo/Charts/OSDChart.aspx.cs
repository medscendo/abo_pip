using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Transactions;
using NetHealthPIMModel;

public partial class abo_DryeyeChart : BasePage
{
    protected void Page_Load(object sender, EventArgs e)
    {

        if (!IsPostBack)
        {
            InitializePage();
        }


    }
    protected void InitializePage()
    {
        ((PIMMasterPage)Page.Master).SetTitle("Chart");

        using (NetHealthPIMEntities pim = new NetHealthPIMEntities())
        {
            int ModuleID = (int)Session[Constants.SESSION_WORKINGMODULEID];
            Module module = pim.Module.First(c => c.ModuleID == ModuleID);
            ((PIMMasterPage)Page.Master).SetHeader(module.ModuleName + " Chart Abstraction");

            ParticipantModuleCycle prev = (ParticipantModuleCycle)Session[Constants.SESSION_PREVIOUSCYCLE];
            String CycleNumber = Request.QueryString["CycleNumber"];

            int cycleID = ctx.ActiveModuleCycleID;
            if (CycleNumber == "1")
            {
                cycleID = prev.ParticipantModuleCycleID;
                LinkButtonBackToDashboard.Visible = true;
                ButtonSubmit.Visible = false;
            }

            ParticipantModuleCycle cycle = pim.ParticipantModuleCycle.First(c => c.ParticipantModuleCycleID == cycleID);
            String numberOfRecord = Request.QueryString["nr"];
            String totalRecords = Request.QueryString["t"];
            if (cycle.CycleNumber == 1)
            {
                ((PIMMasterPage)Page.Master).SetStatus(2);
                LiteralAbstractionNumber.Text = "Initial Abstraction: Chart " + numberOfRecord + " of " + totalRecords;
            }
            else
            {
                ((PIMMasterPage)Page.Master).SetStatus(3);
                LiteralAbstractionNumber.Text = "Second Abstraction: Chart " + numberOfRecord + " of " + totalRecords;
            }
            DropDownListMonthOfBirth.DataSource = CycleManager.getMonthList();
            DropDownListYearOfBirth.DataSource = CycleManager.getDOBYearList(0, 100);
            DropDownListOutcomeDateMonth.DataSource = CycleManager.getMonthList();
            DropDownListOutcomeDateYear.DataSource = CycleManager.getDOBYearList(0, 100);
            DataBind();
            DropDownListYearOfBirth.Items.Insert(0, new ListItem("Year", "0"));
            DropDownListOutcomeDateYear.Items.Insert(0, new ListItem("Year", "0"));
            string recordIdentifier = Request.QueryString["ri"];
            HiddenFieldRecordIdentifier.Value = recordIdentifier;
            LiteralRecordIdentifier.Text = recordIdentifier;
            var count = pim.ChartAbstractionOSD.Where(ps => ps.ParticipantModuleCycle.ParticipantModuleCycleID == cycleID & ps.RecordIdentifier == recordIdentifier).Count();
            if (count == 0)
            {
                using (TransactionScope transaction = new TransactionScope())
                {
                    //Module module = pim.Module.First(m => m.ModuleID == Constants.ABO_AMBLYOPIA_MODULEID);

                    NetHealthPIMModel.ChartRegistration chartRegistration = (from cr in pim.ChartRegistration
                                                           where cr.ParticipantModuleCycleID == cycleID
                                                           & cr.RecordIdentifier == recordIdentifier
                                                           & cr.ModuleID == 60
                                                           select cr).First<NetHealthPIMModel.ChartRegistration>();

                    DateTime initialVisit = Convert.ToDateTime(chartRegistration.InitialVisit);
                    DateTime DOB = Convert.ToDateTime(chartRegistration.DOB);


                    ChartAbstractionOSD abstractRecord = new ChartAbstractionOSD();
                    abstractRecord.ParticipantModuleCycle = cycle;
                    abstractRecord.RecordIdentifier = recordIdentifier;
                    abstractRecord.MonthOfBirth = DOB.Month;
                    abstractRecord.YearOfBirth = DOB.Year;
                    DropDownListYearOfBirth.SelectedValue = abstractRecord.YearOfBirth.ToString();
                    DropDownListMonthOfBirth.SelectedValue = abstractRecord.MonthOfBirth.ToString();
                    DropDownListOutcomeDateMonth.DataSource = abstractRecord.OutcomeDateMonth.ToString();
                    DropDownListOutcomeDateYear.DataSource = abstractRecord.OutcomeDateYear.ToString();
                    abstractRecord.CreatedOn = DateTime.Now;
                    abstractRecord.LastUpdateDate = DateTime.Now;
                    pim.SaveChanges();

                    transaction.Complete();
                }
            }
            else
            {

                ChartAbstractionOSD abstractRecord = (from c in pim.ChartAbstractionOSD
                                                        where c.ParticipantModuleCycle.ParticipantModuleCycleID == cycleID & c.RecordIdentifier == recordIdentifier
                                                     select c).First<ChartAbstractionOSD>();

               
                if (abstractRecord.PresentingSymptomBurning != null) CheckBoxPresentingSymptomBurning.Checked = ((bool)abstractRecord.PresentingSymptomBurning);
                if (abstractRecord.PresentingSymptomBlurredVision != null) CheckBoxPresentingSymptomBlurredVision.Checked = ((bool)abstractRecord.PresentingSymptomBlurredVision);
                if (abstractRecord.PresentingSymptomDryness != null) CheckBoxPresentingSymptomDryness.Checked = ((bool)abstractRecord.PresentingSymptomDryness);
                
                
                
                 if (abstractRecord.TherapyNotDoc != null) CheckBoxTherapyNotDoc.Checked = ((bool)abstractRecord.TherapyNotDoc);


                 if (abstractRecord.OutcomeFindingsImprovedOcularSurface != null) CheckBoxOutcomeFindingsImprovedOcularSurface.Checked = ((bool)abstractRecord.OutcomeFindingsImprovedOcularSurface);
                 if (abstractRecord.AlterNotDocumented != null) CheckBoxAlterNotDocumented.Checked = ((bool)abstractRecord.AlterNotDocumented);
                 if (abstractRecord.OutcomeAlterProcIntervOtherP != null) CheckBoxOutcomeAlterProcIntervOtherP.Checked = ((bool)abstractRecord.OutcomeAlterProcIntervOtherP);
                 if (abstractRecord.OutcomeAlterProcIntervOther != null) CheckBoxOutcomeAlterProcIntervOther.Checked = ((bool)abstractRecord.OutcomeAlterProcIntervOther);
                 if (abstractRecord.SurgeryOtherP != null) RadioButtonSurgeryOtherP.Checked = ((bool)abstractRecord.SurgeryOtherP);
                 if (abstractRecord.OralTherapyOtherP != null) RadioButtonOralTherapyOtherP.Checked = ((bool)abstractRecord.OralTherapyOtherP);
                 if (abstractRecord.TopicalTherapyOtherP != null) RadioButtonTopicalTherapyOtherP.Checked = ((bool)abstractRecord.TopicalTherapyOtherP);


                 if (abstractRecord.AlterTopicalTherapyOtherP != null) CheckBoxOutcomeAlterTopicalTherapyOtherP.Checked = ((bool)abstractRecord.AlterTopicalTherapyOtherP);
                 if (abstractRecord.AlterTopicalTherapyOther != null) CheckBoxOutcomeAlterTopicalTherapyOther.Checked = ((bool)abstractRecord.AlterTopicalTherapyOther);	



                
                if (abstractRecord.PresentingSymptomForeignBody != null) CheckBoxPresentingSymptomForeignBody.Checked = ((bool)abstractRecord.PresentingSymptomForeignBody);
                if (abstractRecord.PresentingSymptomFluctuatingVision != null) CheckBoxPresentingSymptomFluctuatingVision.Checked = ((bool)abstractRecord.PresentingSymptomFluctuatingVision);
                if (abstractRecord.PresentingSymptomItching != null) CheckBoxPresentingSymptomItching.Checked = ((bool)abstractRecord.PresentingSymptomItching);
                if (abstractRecord.PresentingSymptomPhotophobia != null) CheckBoxPresentingSymptomPhotophobia.Checked = ((bool)abstractRecord.PresentingSymptomPhotophobia);
                if (abstractRecord.PresentingSymptomTearing != null) CheckBoxPresentingSymptomTearing.Checked = ((bool)abstractRecord.PresentingSymptomTearing);
                if (abstractRecord.PresentingSymptomNotDoc != null) CheckBoxPresentingSymptomNotDoc.Checked = ((bool)abstractRecord.PresentingSymptomNotDoc);
                if (abstractRecord.PresentingExacDriving != null) CheckBoxPresentingExacDriving.Checked = ((bool)abstractRecord.PresentingExacDriving);
                if (abstractRecord.PresentingExacReading != null) CheckBoxPresentingExacReading.Checked = ((bool)abstractRecord.PresentingExacReading);
                if (abstractRecord.PresentingExacComputer != null) CheckBoxPresentingPresentingExacComputer.Checked = ((bool)abstractRecord.PresentingExacComputer);
                if (abstractRecord.PresentingExacTV != null) CheckBoxPresentingExacTV.Checked = ((bool)abstractRecord.PresentingExacTV);
                if (abstractRecord.PresentingExacWindSun != null) CheckBoxPresentingExacWindSun.Checked = ((bool)abstractRecord.PresentingExacWindSun);
                if (abstractRecord.PresentingExacLowHumid != null) CheckBoxPresentingExacLowHumid.Checked = ((bool)abstractRecord.PresentingExacLowHumid);
                if (abstractRecord.PresentingExacOther != null) CheckBoxPresentingExacOther.Checked = ((bool)abstractRecord.PresentingExacOther);
                if (abstractRecord.PresentingExacNotDoc != null) CheckBoxPresentingExacNotDoc.Checked = ((bool)abstractRecord.PresentingExacNotDoc);
                if (abstractRecord.PresentingDiseaseDiabetes != null) CheckBoxPresentingDiseaseDiabetes.Checked = ((bool)abstractRecord.PresentingDiseaseDiabetes);
                if (abstractRecord.PresentingDiseaseFacialNerve != null) CheckBoxPresentingDiseaseFacialNerve.Checked = ((bool)abstractRecord.PresentingDiseaseFacialNerve);
                if (abstractRecord.PresentingDiseaseGraft != null) CheckBoxPresentingDiseaseGraft.Checked = ((bool)abstractRecord.PresentingDiseaseGraft);
                if (abstractRecord.PresentingDiseaseImmun != null) CheckBoxPresentingDiseaseImmun.Checked = ((bool)abstractRecord.PresentingDiseaseImmun);
                if (abstractRecord.PresentingDiseaseHypothyroid != null) CheckBoxPresentingDiseaseHypothyroid.Checked = ((bool)abstractRecord.PresentingDiseaseHypothyroid);
                if (abstractRecord.PresentingDiseaseNeurologic != null) CheckBoxPresentingDiseaseNeurologic.Checked = ((bool)abstractRecord.PresentingDiseaseNeurologic);
                if (abstractRecord.PresentingDiseaseRA != null) CheckBoxPresentingDiseaseRA.Checked = ((bool)abstractRecord.PresentingDiseaseRA);
                if (abstractRecord.PresentingDiseaseSjogren != null) CheckBoxPresentingDiseaseSjogren.Checked = ((bool)abstractRecord.PresentingDiseaseSjogren);
                if (abstractRecord.PresentingDiseaseInfection != null) CheckBoxPresentingDiseaseInfection.Checked = ((bool)abstractRecord.PresentingDiseaseInfection);
                if (abstractRecord.PresentingDiseaseOtherAID != null) CheckBoxPresentingDiseaseOtherAID.Checked = ((bool)abstractRecord.PresentingDiseaseOtherAID);
                if (abstractRecord.PresentingDiseaseSkin != null) CheckBoxPresentingDiseaseSkin.Checked = ((bool)abstractRecord.PresentingDiseaseSkin);
                if (abstractRecord.PresentingDiseaseOther != null) CheckBoxPresentingDiseaseOther.Checked = ((bool)abstractRecord.PresentingDiseaseOther);
                if (abstractRecord.PresentingDiseaseNotDoc != null) CheckBoxPresentingDiseaseNotDoc.Checked = ((bool)abstractRecord.PresentingDiseaseNotDoc);
                if (abstractRecord.ExamConjFindNormal != null) CheckBoxExamConjFindNormal.Checked = ((bool)abstractRecord.ExamConjFindNormal);
                if (abstractRecord.ExamConjFindConj != null) CheckBoxExamConjFindConj.Checked = ((bool)abstractRecord.ExamConjFindConj);
                if (abstractRecord.ExamConjFindInjection != null) CheckBoxExamConjFindInjection.Checked = ((bool)abstractRecord.ExamConjFindInjection);
                if (abstractRecord.ExamConjFindSuperior != null) CheckBoxExamConjFindSuperior.Checked = ((bool)abstractRecord.ExamConjFindSuperior);
                if (abstractRecord.ExamConjFindSymblepharon != null) CheckBoxExamConjFindSymblepharon.Checked = ((bool)abstractRecord.ExamConjFindSymblepharon);
                if (abstractRecord.ExamConjFindOther != null) CheckBoxExamConjFindOther.Checked = ((bool)abstractRecord.ExamConjFindOther);
                if (abstractRecord.ExamConjFindNotDoc != null) CheckBoxExamConjFindNotDoc.Checked = ((bool)abstractRecord.ExamConjFindNotDoc);
               //// if (abstractRecord.ExamCornealSensation != null) CheckBoxExamCornealSensation.Checked = ((bool)abstractRecord.ExamCornealSensation);
                if (abstractRecord.ExamCornealNormal != null) CheckBoxExamCornealNormal.Checked = ((bool)abstractRecord.ExamCornealNormal);
                if (abstractRecord.ExamCornealPunctate != null) CheckBoxExamCornealPunctate.Checked = ((bool)abstractRecord.ExamCornealPunctate);
                if (abstractRecord.ExamCornealEpithelial != null) CheckBoxExamCornealEpithelial.Checked = ((bool)abstractRecord.ExamCornealEpithelial);
                if (abstractRecord.ExamCornealFilaments != null) CheckBoxExamCornealFilaments.Checked = ((bool)abstractRecord.ExamCornealFilaments);
                if (abstractRecord.ExamCornealNeovascularizations != null) CheckBoxExamCornealNeovascularizations.Checked = ((bool)abstractRecord.ExamCornealNeovascularizations);
                if (abstractRecord.ExamCornealScar != null) CheckBoxExamCornealScar.Checked = ((bool)abstractRecord.ExamCornealScar);
                if (abstractRecord.ExamCornealStromal != null) CheckBoxExamCornealStromal.Checked = ((bool)abstractRecord.ExamCornealStromal);
                if (abstractRecord.ExamCornealOther != null) CheckBoxExamCornealOther.Checked = ((bool)abstractRecord.ExamCornealOther);
                if (abstractRecord.ExamCornealNotDoc != null) CheckBoxExamCornealNotDoc.Checked = ((bool)abstractRecord.ExamCornealNotDoc);
                if (abstractRecord.DiagnosisAqueousTear != null) CheckBoxDiagnosisAqueousTear.Checked = ((bool)abstractRecord.DiagnosisAqueousTear);
                if (abstractRecord.DiagnosisEvaporativeDryEye != null) CheckBoxDiagnosisEvaporativeDryEye.Checked = ((bool)abstractRecord.DiagnosisEvaporativeDryEye);
                if (abstractRecord.DiagnosisEvaporativeDueToOther != null) CheckBoxDiagnosisEvaporativeDueToOther.Checked = ((bool)abstractRecord.DiagnosisEvaporativeDueToOther);
                if (abstractRecord.DiagnosisNeurotropic != null) CheckBoxDiagnosisNeurotropic.Checked = ((bool)abstractRecord.DiagnosisNeurotropic);
                if (abstractRecord.DiagnosisOcular != null) CheckBoxDiagnosisOcular.Checked = ((bool)abstractRecord.DiagnosisOcular);
                if (abstractRecord.DiagnosisUnspec != null) CheckBoxDiagnosisUnspec.Checked = ((bool)abstractRecord.DiagnosisUnspec);
                if (abstractRecord.DiagnosisOther != null) CheckBoxDiagnosisOther.Checked = ((bool)abstractRecord.DiagnosisOther);
                if (abstractRecord.DiagnosisNotDoc != null) CheckBoxDiagnosisNotDoc.Checked = ((bool)abstractRecord.DiagnosisNotDoc);




                if (abstractRecord.DiscussArticialTears != null) RadioButtonDiscussArticialTears.Checked = ((bool)abstractRecord.DiscussArticialTears);
                if (abstractRecord.DiscussAqueousGelLubricant != null) RadioButtonDiscussAqueousGelLubricant.Checked = ((bool)abstractRecord.DiscussAqueousGelLubricant);
                if (abstractRecord.DiscussNonMedOintment != null) RadioButtonDiscussNonMedOintment.Checked = ((bool)abstractRecord.DiscussNonMedOintment);
                if (abstractRecord.DiscussPreservativeFreeTears != null) RadioButtonDiscussPreservativeFreeTears.Checked = ((bool)abstractRecord.DiscussPreservativeFreeTears);
                if (abstractRecord.DiscussTopicalCyclo != null) RadioButtonDiscussTopicalCyclo.Checked = ((bool)abstractRecord.DiscussTopicalCyclo);
                if (abstractRecord.PrescribeTopicalCyclo != null) RadioButtonPrescribeTopicalCyclo.Checked = ((bool)abstractRecord.PrescribeTopicalCyclo);
                if (abstractRecord.DiscussTopicalCortico != null) RadioButtonDiscussTopicalCortico.Checked = ((bool)abstractRecord.DiscussTopicalCortico);
                if (abstractRecord.PrescribeTopicalCortico != null) RadioButtonPrescribeTopicalCortico.Checked = ((bool)abstractRecord.PrescribeTopicalCortico);
                if (abstractRecord.DiscussTopicalAzithromycin != null) RadioButtonDiscussTopicalAzithromycin.Checked = ((bool)abstractRecord.DiscussTopicalAzithromycin);
                if (abstractRecord.PrescribeTopicalAzithromycin != null) RadioButtonPrescribeTopicalAzithromycin.Checked = ((bool)abstractRecord.PrescribeTopicalAzithromycin);
                if (abstractRecord.DiscussTopicalOther != null) RadioButtonDiscussTopicalOther.Checked = ((bool)abstractRecord.DiscussTopicalOther);
                if (abstractRecord.PrescribeTopicalOther != null) RadioButtonPrescribeTopicalOther.Checked = ((bool)abstractRecord.PrescribeTopicalOther);
                if (abstractRecord.DiscussTopicalMucolytics != null) RadioButtonDiscussTopicalMucolytics.Checked = ((bool)abstractRecord.DiscussTopicalMucolytics);
                if (abstractRecord.PrescribeTopicalucolytics != null) RadioButtonPrescribeTopicalucolytics.Checked = ((bool)abstractRecord.PrescribeTopicalucolytics);
                if (abstractRecord.DiscussAutologousSerum != null) RadioButtonDiscussAutologousSerum.Checked = ((bool)abstractRecord.DiscussAutologousSerum);
                if (abstractRecord.PrescribeAutologousSerum != null) RadioButtonPrescribeAutologousSerum.Checked = ((bool)abstractRecord.PrescribeAutologousSerum);
                if (abstractRecord.DiscussChangeTopical != null) RadioButtonDiscussChangeTopical.Checked = ((bool)abstractRecord.DiscussChangeTopical);
                if (abstractRecord.PrescribeChangeTopical != null) RadioButtonPrescribeChangeTopical.Checked = ((bool)abstractRecord.PrescribeChangeTopical);
                if (abstractRecord.TopicalTherapyOther != null) RadioButtonTopicalTherapyOther.Checked = ((bool)abstractRecord.TopicalTherapyOther);
                if (abstractRecord.DiscussOralTetra != null) RadioButtonDiscussOralTetra.Checked = ((bool)abstractRecord.DiscussOralTetra);
                if (abstractRecord.PrescribeOralTetra != null) RadioButtonPrescribeOralTetra.Checked = ((bool)abstractRecord.PrescribeOralTetra);
                if (abstractRecord.DiscussOralNutritionSup != null) RadioButtonDiscussOralNutritionSup.Checked = ((bool)abstractRecord.DiscussOralNutritionSup);
                if (abstractRecord.DiscussOralSecretagogues != null) RadioButtonDiscussOralSecretagogues.Checked = ((bool)abstractRecord.DiscussOralSecretagogues);
                if (abstractRecord.PrescribeOralSecretagogues != null) RadioButtonPrescribeOralSecretagogues.Checked = ((bool)abstractRecord.PrescribeOralSecretagogues);
                if (abstractRecord.DiscussChangeSystemic != null) RadioButtonDiscussChangeSystemic.Checked = ((bool)abstractRecord.DiscussChangeSystemic);
                if (abstractRecord.PrescribeChangeSystemic != null) RadioButtonPrescribeChangeSystemic.Checked = ((bool)abstractRecord.PrescribeChangeSystemic);
                if (abstractRecord.OralTherapyOther != null) RadioButtonOralTherapyOther.Checked = ((bool)abstractRecord.OralTherapyOther);
                if (abstractRecord.DiscussTemporaryPunctal != null) RadioButtonDiscussTemporaryPunctal.Checked = ((bool)abstractRecord.DiscussTemporaryPunctal);
                if (abstractRecord.PrescribeTemporaryPunctal != null) RadioButtonPrescribeTemporaryPunctal.Checked = ((bool)abstractRecord.PrescribeTemporaryPunctal);
                if (abstractRecord.DiscussPermanentPunctal != null) RadioButtonDiscussPermanentPunctal.Checked = ((bool)abstractRecord.DiscussPermanentPunctal);
                if (abstractRecord.PrescribePermanentPunctal != null) RadioButtonPrescribePermanentPunctal.Checked = ((bool)abstractRecord.PrescribePermanentPunctal);
                if (abstractRecord.DiscussLaser != null) RadioButtonDiscussLaser.Checked = ((bool)abstractRecord.DiscussLaser);
                if (abstractRecord.PrescribeLaser != null) RadioButtonPrescribeLaser.Checked = ((bool)abstractRecord.PrescribeLaser);
                if (abstractRecord.DiscussThermocautery != null) RadioButtonDiscussThermocautery.Checked = ((bool)abstractRecord.DiscussThermocautery);
                if (abstractRecord.PrescribeThermocautery != null) RadioButtonPrescribeThermocautery.Checked = ((bool)abstractRecord.PrescribeThermocautery);
                if (abstractRecord.DiscussSurgery != null) RadioButtonDiscussSurgery.Checked = ((bool)abstractRecord.DiscussSurgery);
                if (abstractRecord.PrescribeSurgery != null) RadioButtonPrescribeSurgery.Checked = ((bool)abstractRecord.PrescribeSurgery);
                if (abstractRecord.DiscussGlandProcedure != null) RadioButtonDiscussGlandProcedure.Checked = ((bool)abstractRecord.DiscussGlandProcedure);
                if (abstractRecord.PrescribeGlandProcedure != null) RadioButtonPrescribeGlandProcedure.Checked = ((bool)abstractRecord.PrescribeGlandProcedure);
                if (abstractRecord.DiscussConjunctivalSurgery != null) RadioButtonDiscussConjunctivalSurgery.Checked = ((bool)abstractRecord.DiscussConjunctivalSurgery);
                if (abstractRecord.PrescribeConjunctivalSurgery != null) RadioButtonPrescribeConjunctivalSurgery.Checked = ((bool)abstractRecord.PrescribeConjunctivalSurgery);
                if (abstractRecord.DiscussEyelidSurgery != null) RadioButtonDiscussEyelidSurgery.Checked = ((bool)abstractRecord.DiscussEyelidSurgery);
                if (abstractRecord.PrescribeEyelidSurgery != null) RadioButtonPrescribeEyelidSurgery.Checked = ((bool)abstractRecord.PrescribeEyelidSurgery);
                if (abstractRecord.SurgeryOther != null) RadioButtonSurgeryOther.Checked = ((bool)abstractRecord.SurgeryOther);
                if (abstractRecord.DiscussEyelidHygiene != null) RadioButtonDiscussEyelidHygiene.Checked = ((bool)abstractRecord.DiscussEyelidHygiene);
                if (abstractRecord.DiscussEnvironmentMod != null) RadioButtonDiscussEnvironmentMod.Checked = ((bool)abstractRecord.DiscussEnvironmentMod);
                if (abstractRecord.DiscussContactLensSoft != null) RadioButtonDiscussContactLensSoft.Checked = ((bool)abstractRecord.DiscussContactLensSoft);
                if (abstractRecord.PrescribeContactLensSoft != null) RadioButtonPrescribeContactLensSoft.Checked = ((bool)abstractRecord.PrescribeContactLensSoft);
                if (abstractRecord.DiscussContactLensScleral != null) RadioButtonDiscussContactLensScleral.Checked = ((bool)abstractRecord.DiscussContactLensScleral);
                if (abstractRecord.PrescribeContactLensScleral != null) RadioButtonPrescribeContactLensScleral.Checked = ((bool)abstractRecord.PrescribeContactLensScleral);
                if (abstractRecord.DiscussContactLensOther != null) RadioButtonDiscussContactLensOther.Checked = ((bool)abstractRecord.DiscussContactLensOther);
                if (abstractRecord.PrescribeContactLensOther != null) RadioButtonPrescribeContactLensOther.Checked = ((bool)abstractRecord.PrescribeContactLensOther);
                if (abstractRecord.DiscussMoistureChamber != null) RadioButtonDiscussMoistureChamber.Checked = ((bool)abstractRecord.DiscussMoistureChamber);
                if (abstractRecord.PrescribeAdjunctiveTherapyOther != null) RadioButtonPrescribeAdjunctiveTherapyOther.Checked = ((bool)abstractRecord.PrescribeAdjunctiveTherapyOther);
                if (abstractRecord.DiscussAdjunctiveTherapyOther != null) RadioButtonDiscussAdjunctiveTherapyOther.Checked = ((bool)abstractRecord.DiscussAdjunctiveTherapyOther);



                if (abstractRecord.OutcomeDiscussArtificialTears != null) CheckBoxOutcomeAlterArtificialTears.Checked = ((bool)abstractRecord.OutcomeDiscussArtificialTears);
                if (abstractRecord.OutcomeAqueousGelLubricant != null) CheckBoxOutcomeAqueousGelLubricant.Checked = ((bool)abstractRecord.OutcomeAqueousGelLubricant);
                if (abstractRecord.OutcomeDiscussNonMedOintment != null) CheckBoxOutcomeAlterNonMedOintment.Checked = ((bool)abstractRecord.OutcomeDiscussNonMedOintment);
                if (abstractRecord.OutcomeDiscussPreservativeFreeTears != null) CheckBoxOutcomeAlterPreservativeFreeTears.Checked = ((bool)abstractRecord.OutcomeDiscussPreservativeFreeTears);
                if (abstractRecord.OutcomeDiscussEyelidHygiene != null) CheckBoxOutcomeAlterEyelidHygiene.Checked = ((bool)abstractRecord.OutcomeDiscussEyelidHygiene);
                if (abstractRecord.OutcomeDiscussTopicalCyclo != null) CheckBoxOutcomeAlterTopicalCyclo.Checked = ((bool)abstractRecord.OutcomeDiscussTopicalCyclo);
                if (abstractRecord.OutcomeDiscussTopicalCortico != null) CheckBoxOutcomeAlterTopicalCortico.Checked = ((bool)abstractRecord.OutcomeDiscussTopicalCortico);
                if (abstractRecord.OutcomeDiscussTopicalAzithromycin != null) CheckBoxOutcomeAlterTopicalAzithromycin.Checked = ((bool)abstractRecord.OutcomeDiscussTopicalAzithromycin);
                if (abstractRecord.OutcomeDiscussTopicalOther != null) CheckBoxOutcomeAlterTopicalOther.Checked = ((bool)abstractRecord.OutcomeDiscussTopicalOther);
                //if (abstractRecord.OutcomeAlterTopicalTherapyOtherText != null) CheckBoxOutcomeAlterTopicalTherapyOtherText.Checked = ((bool)abstractRecord.OutcomeAlterTopicalTherapyOtherText);
                if (abstractRecord.OutcomeDiscussTopicalMucolytics != null) CheckBoxOutcomeAlterTopicalMucolytics.Checked = ((bool)abstractRecord.OutcomeDiscussTopicalMucolytics);
                if (abstractRecord.OutcomeDiscussOralTetra != null) CheckBoxOutcomeAlterOralTetra.Checked = ((bool)abstractRecord.OutcomeDiscussOralTetra);
                if (abstractRecord.OutcomeDiscussOralNutritionSup != null) CheckBoxOutcomeAlterOralNutritionSup.Checked = ((bool)abstractRecord.OutcomeDiscussOralNutritionSup);
                if (abstractRecord.OutcomeDiscussOralSecretagogues != null) CheckBoxOutcomeAlterOralSecretagogues.Checked = ((bool)abstractRecord.OutcomeDiscussOralSecretagogues);
                if (abstractRecord.OutcomeDiscussOralOther != null) CheckBoxOutcomeAlterOralOther.Checked = ((bool)abstractRecord.OutcomeDiscussOralOther);
                //if (abstractRecord.OutcomeAlterOralOtherText != null) CheckBoxOutcomeAlterOralOtherText.Checked = ((bool)abstractRecord.OutcomeAlterOralOtherText);
                if (abstractRecord.OutcomeDiscussTemporaryPunctal != null) CheckBoxOutcomeAlterTemporaryPunctal.Checked = ((bool)abstractRecord.OutcomeDiscussTemporaryPunctal);
                if (abstractRecord.OutcomeDiscussPermanentPunctal != null) CheckBoxOutcomeAlterPermanentPunctal.Checked = ((bool)abstractRecord.OutcomeDiscussPermanentPunctal);
                if (abstractRecord.OutcomeDiscussLaser != null) CheckBoxOutcomeAlterLaser.Checked = ((bool)abstractRecord.OutcomeDiscussLaser);
                if (abstractRecord.OutcomeDiscussThermocautery != null) CheckBoxOutcomeAlterThermocautery.Checked = ((bool)abstractRecord.OutcomeDiscussThermocautery);
                if (abstractRecord.OutcomeDiscussSurgery != null) CheckBoxOutcomeAlterSurgery.Checked = ((bool)abstractRecord.OutcomeDiscussSurgery);
                if (abstractRecord.OutcomeDiscussContactLensSoft != null) CheckBoxOutcomeAlterContactLensSoft.Checked = ((bool)abstractRecord.OutcomeDiscussContactLensSoft);
                if (abstractRecord.OutcomeDiscussContactLensScleral != null) CheckBoxOutcomeAlterContactLensScleral.Checked = ((bool)abstractRecord.OutcomeDiscussContactLensScleral);
                if (abstractRecord.OutcomeDiscussContactLensOther != null) CheckBoxOutcomeAlterContactLensOther.Checked = ((bool)abstractRecord.OutcomeDiscussContactLensOther);
                if (abstractRecord.OutcomeDiscussAutologousSerum != null) CheckBoxOutcomeAlterAutologousSerum.Checked = ((bool)abstractRecord.OutcomeDiscussAutologousSerum);
                if (abstractRecord.OutcomeDiscussChangeTopical != null) CheckBoxOutcomeAlterChangeTopical.Checked = ((bool)abstractRecord.OutcomeDiscussChangeTopical);
                if (abstractRecord.OutcomeDiscussChangeSystemic != null) CheckBoxOutcomeAlterChangeSystemic.Checked = ((bool)abstractRecord.OutcomeDiscussChangeSystemic);
                if (abstractRecord.OutcomeDiscussMoistureChamber != null) CheckBoxOutcomeAlterMoistureChamber.Checked = ((bool)abstractRecord.OutcomeDiscussMoistureChamber);
                if (abstractRecord.OutcomeDiscussEnvironmentMod != null) CheckBoxOutcomeAlterEnvironmentMod.Checked = ((bool)abstractRecord.OutcomeDiscussEnvironmentMod);
                if (abstractRecord.OutcomeDiscussEyelidSurgery != null) CheckBoxOutcomeAlterEyelidSurgery.Checked = ((bool)abstractRecord.OutcomeDiscussEyelidSurgery);
                if (abstractRecord.OutcomeDiscussConjunctivalSurgery != null) CheckBoxOutcomeAlterConjunctivalSurgery.Checked = ((bool)abstractRecord.OutcomeDiscussConjunctivalSurgery);
                if (abstractRecord.OutcomeDiscussGlandProcedure != null) CheckBoxOutcomeAlterGlandProcedure.Checked = ((bool)abstractRecord.OutcomeDiscussGlandProcedure);
                if (abstractRecord.OutcomeDiscussOther != null) CheckBoxOutcomeAlterOther.Checked = ((bool)abstractRecord.OutcomeDiscussOther);





                if (abstractRecord.OutcomeAlterTopicalCyclo != null) CheckBoxOutcomeAlterTopicalCycloP.Checked = ((bool)abstractRecord.OutcomeAlterTopicalCyclo);
                if (abstractRecord.OutcomeAlterTopicalCortico != null) CheckBoxOutcomeAlterTopicalCorticoP.Checked = ((bool)abstractRecord.OutcomeAlterTopicalCortico);
                if (abstractRecord.OutcomeAlterTopicalAzithromycin != null) CheckBoxOutcomeAlterTopicalAzithromycinP.Checked = ((bool)abstractRecord.OutcomeAlterTopicalAzithromycin);
                if (abstractRecord.OutcomeAlterTopicalOther != null) CheckBoxOutcomeAlterTopicalOtherP.Checked = ((bool)abstractRecord.OutcomeAlterTopicalOther);
              //  if (abstractRecord.OutcomeAlterTopicalTherapyOtherText != null) CheckBoxOutcomeAlterTopicalTherapyOther.Checked = ((bool)abstractRecord.OutcomeAlterTopicalTherapyOtherText);
                if (abstractRecord.OutcomeAlterTopicalMucolytics != null) CheckBoxOutcomeAlterTopicalMucolyticsP.Checked = ((bool)abstractRecord.OutcomeAlterTopicalMucolytics);
                if (abstractRecord.OutcomeAlterOralTetra != null) CheckBoxOutcomeAlterOralTetraP.Checked = ((bool)abstractRecord.OutcomeAlterOralTetra);
                if (abstractRecord.OutcomeAlterOralSecretagogues != null) CheckBoxOutcomeAlterOralSecretagoguesP.Checked = ((bool)abstractRecord.OutcomeAlterOralSecretagogues);
                if (abstractRecord.OutcomeAlterOralOther != null) CheckBoxOutcomeAlterOralOtherP.Checked = ((bool)abstractRecord.OutcomeAlterOralOther);
                //if (abstractRecord.OutcomeAlterOralOtherText != null) CheckBoxOutcomeAlterOralOtherTextP.Checked = ((bool)abstractRecord.OutcomeAlterOralOtherText);
                if (abstractRecord.OutcomeAlterTemporaryPunctal != null) CheckBoxOutcomeAlterTemporaryPunctalP.Checked = ((bool)abstractRecord.OutcomeAlterTemporaryPunctal);
                if (abstractRecord.OutcomeAlterPermanentPunctal != null) CheckBoxOutcomeAlterPermanentPunctalP.Checked = ((bool)abstractRecord.OutcomeAlterPermanentPunctal);
                if (abstractRecord.OutcomeAlterLaser != null) CheckBoxOutcomeAlterLaserP.Checked = ((bool)abstractRecord.OutcomeAlterLaser);
                if (abstractRecord.OutcomeAlterThermocautery != null) CheckBoxOutcomeAlterThermocauteryP.Checked = ((bool)abstractRecord.OutcomeAlterThermocautery);
                if (abstractRecord.OutcomeAlterSurgery != null) CheckBoxOutcomeAlterSurgeryP.Checked = ((bool)abstractRecord.OutcomeAlterSurgery);
                if (abstractRecord.OutcomeAlterContactLensSoft != null) CheckBoxOutcomeAlterContactLensSoftP.Checked = ((bool)abstractRecord.OutcomeAlterContactLensSoft);
                if (abstractRecord.OutcomeAlterContactLensScleral != null) CheckBoxOutcomeAlterContactLensScleralP.Checked = ((bool)abstractRecord.OutcomeAlterContactLensScleral);
                if (abstractRecord.OutcomeAlterContactLensOther != null) CheckBoxOutcomeAlterContactLensOtherP.Checked = ((bool)abstractRecord.OutcomeAlterContactLensOther);
                if (abstractRecord.OutcomeAlterAutologousSerum != null) CheckBoxOutcomeAlterAutologousSerumP.Checked = ((bool)abstractRecord.OutcomeAlterAutologousSerum);
                if (abstractRecord.OutcomeAlterChangeTopical != null) CheckBoxOutcomeAlterChangeTopicalP.Checked = ((bool)abstractRecord.OutcomeAlterChangeTopical);
                if (abstractRecord.OutcomeAlterChangeSystemic != null) CheckBoxOutcomeAlterChangeSystemicP.Checked = ((bool)abstractRecord.OutcomeAlterChangeSystemic);
                if (abstractRecord.OutcomeAlterEyelidSurgery != null) CheckBoxOutcomeAlterEyelidSurgeryP.Checked = ((bool)abstractRecord.OutcomeAlterEyelidSurgery);
                if (abstractRecord.OutcomeAlterConjunctivalSurgery != null) CheckBoxOutcomeAlterConjunctivalSurgeryP.Checked = ((bool)abstractRecord.OutcomeAlterConjunctivalSurgery);
                if (abstractRecord.OutcomeAlterGlandProcedure != null) CheckBoxOutcomeAlterGlandProcedureP.Checked = ((bool)abstractRecord.OutcomeAlterGlandProcedure);
                if (abstractRecord.OutcomeAlterOther != null) CheckBoxOutcomeAlterOtherP.Checked = ((bool)abstractRecord.OutcomeAlterOther);

               
                if (abstractRecord.OutcomePunctalStillOccluded != null) CheckBoxOutcomePunctalStillOccluded.Checked = ((bool)abstractRecord.OutcomePunctalStillOccluded);

                if (abstractRecord.ExamTearOsmolarity != null) CheckBoxExamTearOsmolarity.Checked = (Convert.ToBoolean(abstractRecord.ExamTearOsmolarity));




                if (abstractRecord.OutcomeFindingsImprovedEyelid != null) CheckBoxOutcomeFindingsImprovedEyelid.Checked = ((bool)abstractRecord.OutcomeFindingsImprovedEyelid);
                if (abstractRecord.OutcomeFindingsImprovedBlink != null) CheckBoxOutcomeFindingsImprovedBlink.Checked = ((bool)abstractRecord.OutcomeFindingsImprovedBlink);
                if (abstractRecord.OutcomeFindingsImprovedLashed != null) CheckBoxOutcomeFindingsImprovedLashed.Checked = ((bool)abstractRecord.OutcomeFindingsImprovedLashed);
                if (abstractRecord.OutcomeFindingsImprovedGlands != null) CheckBoxOutcomeFindingsImprovedGlands.Checked = ((bool)abstractRecord.OutcomeFindingsImprovedGlands);
                if (abstractRecord.OutcomeFindingsImprovedTearMeniscus != null) CheckBoxOutcomeFindingsImprovedTearMeniscus.Checked = ((bool)abstractRecord.OutcomeFindingsImprovedTearMeniscus);
                if (abstractRecord.OutcomeFindingsImprovedTearTime != null) CheckBoxOutcomeFindingsImprovedTearTime.Checked = ((bool)abstractRecord.OutcomeFindingsImprovedTearTime);
                if (abstractRecord.OutcomeFindingsImprovedSchirmer != null) CheckBoxOutcomeFindingsImprovedSchirmer.Checked = ((bool)abstractRecord.OutcomeFindingsImprovedSchirmer);
                if (abstractRecord.OutcomeFindingsImprovedTearOsmolarity != null) CheckBoxOutcomeFindingsImprovedTearOsmolarity.Checked = ((bool)abstractRecord.OutcomeFindingsImprovedTearOsmolarity);
                if (abstractRecord.OutcomeFindingsImprovedConjunctival != null) CheckBoxOutcomeFindingsImprovedConjunctival.Checked = ((bool)abstractRecord.OutcomeFindingsImprovedConjunctival);
                if (abstractRecord.OutcomeFindingsImprovedCorneal != null) CheckBoxOutcomeFindingsImprovedCorneal.Checked = ((bool)abstractRecord.OutcomeFindingsImprovedCorneal);
                if (abstractRecord.OutcomeFindingsImprovedOther != null) CheckBoxOutcomeFindingsImprovedOther.Checked = ((bool)abstractRecord.OutcomeFindingsImprovedOther);
                if (abstractRecord.PresentingDryMouth != null)
                    RadioButtonListPresentingDryMouth.SelectedValue = abstractRecord.PresentingDryMouth.ToString();
                if (abstractRecord.Smoking != null)
                    RadioButtonListSmoking.SelectedValue = abstractRecord.Smoking.ToString();
                if (abstractRecord.ContactLens != null)
                    RadioButtonListContactLens.SelectedValue = abstractRecord.ContactLens.ToString();
                if (abstractRecord.EyeSurgery != null)
                    RadioButtonListEyeSurgery.SelectedValue = abstractRecord.EyeSurgery.ToString();
                if (abstractRecord.TopicalOralMed != null)
                    RadioButtonListTopicalOralMed.SelectedValue = abstractRecord.TopicalOralMed.ToString();
                if (abstractRecord.ExamAbEyelidPosition != null)
                    RadioButtonListExamAbEyelidPosition.SelectedValue = abstractRecord.ExamAbEyelidPosition.ToString();
                if (abstractRecord.ExamAbnormalBlink != null)
                    RadioButtonListExamAbnormalBlink.SelectedValue = abstractRecord.ExamAbnormalBlink.ToString();
                if (abstractRecord.ExamLashes != null)
                    RadioButtonListExamLashes.SelectedValue = abstractRecord.ExamLashes.ToString();
                if (abstractRecord.ExamEyelidsEverted != null)
                    RadioButtonListExamEyelidsEverted.SelectedValue = abstractRecord.ExamEyelidsEverted.ToString();
                if (abstractRecord.ExamMeibomianDisease != null)
                    RadioButtonListExamMeibomianDisease.SelectedValue = abstractRecord.ExamMeibomianDisease.ToString();
                if (abstractRecord.ExamTearHeight != null)
                    RadioButtonListExamTearHeight.SelectedValue = abstractRecord.ExamTearHeight.ToString();


                if (abstractRecord.RBGender != null)
                    RadioButtonListRBGender.SelectedValue = abstractRecord.RBGender.ToString();
                if (abstractRecord.MonthOfBirth != null)
                    DropDownListMonthOfBirth.SelectedValue = abstractRecord.MonthOfBirth.ToString();
                if (abstractRecord.YearOfBirth != null)
                    DropDownListYearOfBirth.SelectedValue = abstractRecord.YearOfBirth.ToString();
                if (abstractRecord.OutcomeDateMonth != null)
                    DropDownListOutcomeDateMonth.SelectedValue = abstractRecord.OutcomeDateMonth.ToString();
                if (abstractRecord.OutcomeDateYear != null)
                    DropDownListOutcomeDateYear.SelectedValue = abstractRecord.OutcomeDateYear.ToString();

                if (abstractRecord.ExamTearBreakupTime != null)
                    RadioButtonListExamTearBreakupTime.SelectedValue = abstractRecord.ExamTearBreakupTime.ToString();
                if (abstractRecord.ExamOcularSurfaceStain != null)
                    RadioButtonListExamOcularSurfaceStain.SelectedValue = abstractRecord.ExamOcularSurfaceStain.ToString();
                if (abstractRecord.ExamSchirmer != null)
                    RadioButtonListExamSchirmer.SelectedValue = abstractRecord.ExamSchirmer.ToString();
                if (abstractRecord.ExamCornealSensation != null)
                    RadioButtonListExamCornealSensation.SelectedValue = abstractRecord.ExamCornealSensation.ToString();
                if (abstractRecord.AssessDiseaseSeverity != null)
                    RadioButtonListAssessDiseaseSeverity.SelectedValue = abstractRecord.AssessDiseaseSeverity.ToString();
                if (abstractRecord.EMSystemicDisease != null)
                    RadioButtonListEMSystemicDisease.SelectedValue = abstractRecord.EMSystemicDisease.ToString();
                if (abstractRecord.OutcomePunctal != null)
                    RadioButtonListOutcomePunctal.SelectedValue = abstractRecord.OutcomePunctal.ToString();
                if (abstractRecord.OutcomePunctalFollowup != null)
                    RadioButtonListOutcomePunctalFollowup.SelectedValue = abstractRecord.OutcomePunctalFollowup.ToString();
                if (abstractRecord.OutcomeSymptomsImproved != null)
                    RadioButtonListOutcomeSymptomsImproved.SelectedValue = abstractRecord.OutcomeSymptomsImproved.ToString();
                if (abstractRecord.OutcomeFindingsImproved != null)
                    RadioButtonListOutcomeFindingsImproved.SelectedValue = abstractRecord.OutcomeFindingsImproved.ToString();

                if (abstractRecord.TextBoxPresentingExacOtherText != null) TextBoxPresentingExacOtherText.Text = abstractRecord.TextBoxPresentingExacOtherText.ToString();
                if (abstractRecord.PresentingDiseaseOtherText != null) TextBoxPresentingDiseaseOtherText.Text = abstractRecord.PresentingDiseaseOtherText.ToString();
                if (abstractRecord.ExamLashesText != null) TextBoxExamLashesText.Text = abstractRecord.ExamLashesText.ToString();
                if (abstractRecord.ExamTearOsmolarityOsmolesNbr != null) TextBoxExamTearOsmolarityOsmolesNbr.Text = abstractRecord.ExamTearOsmolarityOsmolesNbr.ToString();
                if (abstractRecord.ExamConjFindOtherText != null) TextBoxExamConjFindOtherText.Text = abstractRecord.ExamConjFindOtherText.ToString();
                if (abstractRecord.ExamCornealOtherText != null) TextBoxExamCornealOtherText.Text = abstractRecord.ExamCornealOtherText.ToString();
                if (abstractRecord.DiagnosisOtherText != null) TextBoxDiagnosisOtherText.Text = abstractRecord.DiagnosisOtherText.ToString();
                if (abstractRecord.TopicalTherapyOtherText != null) TextBoxTopicalTherapyOtherText.Text = abstractRecord.TopicalTherapyOtherText.ToString();
                if (abstractRecord.OralTherapyOtherText != null) TextBoxOralTherapyOtherText.Text = abstractRecord.OralTherapyOtherText.ToString();
                if (abstractRecord.SurgeryOtherText != null) TextBoxSurgeryOtherText.Text = abstractRecord.SurgeryOtherText.ToString();
                if (abstractRecord.DiscussAdjunctiveTherapyOtherText != null) TextBoxDiscussAdjunctiveTherapyOtherText.Text = abstractRecord.DiscussAdjunctiveTherapyOtherText.ToString();
                if (abstractRecord.OutcomeAlterTopicalTherapyOtherText != null) TextBoxOutcomeAlterTopicalTherapyOtherText.Text = abstractRecord.OutcomeAlterTopicalTherapyOtherText.ToString();
                if (abstractRecord.OutcomeAlterOralOtherText != null) TextBoxOutcomeAlterOralOtherText.Text = abstractRecord.OutcomeAlterOralOtherText.ToString();
                if (abstractRecord.OutcomeAlterProcIntervOtherText != null) TextBoxOutcomeAlterProcIntervOtherText.Text = abstractRecord.OutcomeAlterProcIntervOtherText.ToString();
                if (abstractRecord.OutcomeAlterOtherText != null) TextBoxOutcomeAlterOtherText.Text = abstractRecord.OutcomeAlterOtherText.ToString();
                if (abstractRecord.OutcomeOtherText != null) TextBoxOutcomeOtherText.Text = abstractRecord.OutcomeOtherText.ToString();

            }
        }
    }


    public void savedata()
    {
        using (NetHealthPIMEntities pim = new NetHealthPIMEntities())
        {
            int ModuleID = (int)Session[Constants.SESSION_WORKINGMODULEID];
            int cycleID = ctx.ActiveModuleCycleID;
            ParticipantModuleCycle cycle = pim.ParticipantModuleCycle.First(c => c.ParticipantModuleCycleID == cycleID);
            string recordIdentifier = HiddenFieldRecordIdentifier.Value;
            using (TransactionScope transaction = new TransactionScope())
            {
                ChartAbstractionOSD abstractRecord = (from c in pim.ChartAbstractionOSD
                                                        where c.ParticipantModuleCycle.ParticipantModuleCycleID == cycleID & c.RecordIdentifier == recordIdentifier
                                                      select c).First<ChartAbstractionOSD>();


                abstractRecord.PresentingSymptomBurning = CheckBoxPresentingSymptomBurning.Checked;
                abstractRecord.PresentingSymptomBlurredVision = CheckBoxPresentingSymptomBlurredVision.Checked;
                abstractRecord.PresentingSymptomDryness = CheckBoxPresentingSymptomDryness.Checked;
                abstractRecord.PresentingSymptomForeignBody = CheckBoxPresentingSymptomForeignBody.Checked;
                abstractRecord.PresentingSymptomFluctuatingVision = CheckBoxPresentingSymptomFluctuatingVision.Checked;
                abstractRecord.PresentingSymptomItching = CheckBoxPresentingSymptomItching.Checked;
                abstractRecord.PresentingSymptomPhotophobia = CheckBoxPresentingSymptomPhotophobia.Checked;
                abstractRecord.PresentingSymptomTearing = CheckBoxPresentingSymptomTearing.Checked;
                abstractRecord.PresentingSymptomNotDoc = CheckBoxPresentingSymptomNotDoc.Checked;
                abstractRecord.PresentingExacDriving = CheckBoxPresentingExacDriving.Checked;
                abstractRecord.PresentingExacReading = CheckBoxPresentingExacReading.Checked;
                abstractRecord.PresentingExacComputer = CheckBoxPresentingPresentingExacComputer.Checked;
                abstractRecord.PresentingExacTV = CheckBoxPresentingExacTV.Checked;
                abstractRecord.PresentingExacWindSun = CheckBoxPresentingExacWindSun.Checked;
                abstractRecord.PresentingExacLowHumid = CheckBoxPresentingExacLowHumid.Checked;
                abstractRecord.PresentingExacOther = CheckBoxPresentingExacOther.Checked;
                abstractRecord.PresentingExacNotDoc = CheckBoxPresentingExacNotDoc.Checked;
                abstractRecord.PresentingDiseaseDiabetes = CheckBoxPresentingDiseaseDiabetes.Checked;
                abstractRecord.PresentingDiseaseFacialNerve = CheckBoxPresentingDiseaseFacialNerve.Checked;
                abstractRecord.PresentingDiseaseGraft = CheckBoxPresentingDiseaseGraft.Checked;
                abstractRecord.PresentingDiseaseImmun = CheckBoxPresentingDiseaseImmun.Checked;
                abstractRecord.PresentingDiseaseHypothyroid = CheckBoxPresentingDiseaseHypothyroid.Checked;
                abstractRecord.PresentingDiseaseNeurologic = CheckBoxPresentingDiseaseNeurologic.Checked;
                abstractRecord.PresentingDiseaseRA = CheckBoxPresentingDiseaseRA.Checked;
                abstractRecord.PresentingDiseaseSjogren = CheckBoxPresentingDiseaseSjogren.Checked;
                abstractRecord.PresentingDiseaseInfection = CheckBoxPresentingDiseaseInfection.Checked;
                abstractRecord.PresentingDiseaseOtherAID = CheckBoxPresentingDiseaseOtherAID.Checked;
                abstractRecord.PresentingDiseaseSkin = CheckBoxPresentingDiseaseSkin.Checked;
                abstractRecord.PresentingDiseaseOther = CheckBoxPresentingDiseaseOther.Checked;
                abstractRecord.PresentingDiseaseNotDoc = CheckBoxPresentingDiseaseNotDoc.Checked;
                abstractRecord.ExamConjFindNormal = CheckBoxExamConjFindNormal.Checked;
                abstractRecord.ExamConjFindConj = CheckBoxExamConjFindConj.Checked;
                abstractRecord.ExamConjFindInjection = CheckBoxExamConjFindInjection.Checked;
                abstractRecord.ExamConjFindSuperior = CheckBoxExamConjFindSuperior.Checked;
                abstractRecord.ExamConjFindSymblepharon = CheckBoxExamConjFindSymblepharon.Checked;
                abstractRecord.ExamConjFindOther = CheckBoxExamConjFindOther.Checked;


                abstractRecord.OutcomeFindingsImprovedOcularSurface = CheckBoxOutcomeFindingsImprovedOcularSurface.Checked;
                abstractRecord.AlterNotDocumented = CheckBoxAlterNotDocumented.Checked;
                abstractRecord.OutcomeAlterProcIntervOtherP = CheckBoxOutcomeAlterProcIntervOtherP.Checked;
                abstractRecord.OutcomeAlterProcIntervOther = CheckBoxOutcomeAlterProcIntervOther.Checked;
                abstractRecord.SurgeryOtherP = RadioButtonSurgeryOtherP.Checked;
                abstractRecord.OralTherapyOtherP = RadioButtonOralTherapyOtherP.Checked;
                abstractRecord.TopicalTherapyOtherP = RadioButtonTopicalTherapyOtherP.Checked;
               abstractRecord.AlterTopicalTherapyOtherP = CheckBoxOutcomeAlterTopicalTherapyOtherP.Checked;
               abstractRecord.AlterTopicalTherapyOther = CheckBoxOutcomeAlterTopicalTherapyOther.Checked;	

                abstractRecord.TherapyNotDoc = CheckBoxTherapyNotDoc.Checked;
                abstractRecord.ExamConjFindNotDoc = CheckBoxExamConjFindNotDoc.Checked;
                ////////abstractRecord.ExamCornealSensation = CheckBoxExamCornealSensation.Checked;
                abstractRecord.ExamCornealNormal = CheckBoxExamCornealNormal.Checked;
                abstractRecord.ExamCornealPunctate = CheckBoxExamCornealPunctate.Checked;
                abstractRecord.ExamCornealEpithelial = CheckBoxExamCornealEpithelial.Checked;
                abstractRecord.ExamCornealFilaments = CheckBoxExamCornealFilaments.Checked;
                abstractRecord.ExamCornealNeovascularizations = CheckBoxExamCornealNeovascularizations.Checked;
                abstractRecord.ExamCornealScar = CheckBoxExamCornealScar.Checked;
                abstractRecord.ExamCornealStromal = CheckBoxExamCornealStromal.Checked;
                abstractRecord.ExamCornealOther = CheckBoxExamCornealOther.Checked;
                abstractRecord.ExamCornealNotDoc = CheckBoxExamCornealNotDoc.Checked;
                abstractRecord.DiagnosisAqueousTear = CheckBoxDiagnosisAqueousTear.Checked;
                abstractRecord.DiagnosisEvaporativeDryEye = CheckBoxDiagnosisEvaporativeDryEye.Checked;
                abstractRecord.DiagnosisEvaporativeDueToOther = CheckBoxDiagnosisEvaporativeDueToOther.Checked;
                abstractRecord.DiagnosisNeurotropic = CheckBoxDiagnosisNeurotropic.Checked;
                abstractRecord.DiagnosisOcular = CheckBoxDiagnosisOcular.Checked;
                abstractRecord.DiagnosisUnspec = CheckBoxDiagnosisUnspec.Checked;
                abstractRecord.DiagnosisOther = CheckBoxDiagnosisOther.Checked;
                abstractRecord.DiagnosisNotDoc = CheckBoxDiagnosisNotDoc.Checked;


                abstractRecord.DiscussArticialTears = RadioButtonDiscussArticialTears.Checked;
                abstractRecord.DiscussAqueousGelLubricant = RadioButtonDiscussAqueousGelLubricant.Checked;
                abstractRecord.DiscussNonMedOintment = RadioButtonDiscussNonMedOintment.Checked;
                abstractRecord.DiscussPreservativeFreeTears = RadioButtonDiscussPreservativeFreeTears.Checked;
                abstractRecord.DiscussTopicalCyclo = RadioButtonDiscussTopicalCyclo.Checked;
                abstractRecord.PrescribeTopicalCyclo = RadioButtonPrescribeTopicalCyclo.Checked;
                abstractRecord.DiscussTopicalCortico = RadioButtonDiscussTopicalCortico.Checked;
                abstractRecord.PrescribeTopicalCortico = RadioButtonPrescribeTopicalCortico.Checked;
                abstractRecord.DiscussTopicalAzithromycin = RadioButtonDiscussTopicalAzithromycin.Checked;
                abstractRecord.PrescribeTopicalAzithromycin = RadioButtonPrescribeTopicalAzithromycin.Checked;
                abstractRecord.DiscussTopicalOther = RadioButtonDiscussTopicalOther.Checked;
                abstractRecord.PrescribeTopicalOther = RadioButtonPrescribeTopicalOther.Checked;
                abstractRecord.DiscussTopicalMucolytics = RadioButtonDiscussTopicalMucolytics.Checked;
                abstractRecord.PrescribeTopicalucolytics = RadioButtonPrescribeTopicalucolytics.Checked;
                abstractRecord.DiscussAutologousSerum = RadioButtonDiscussAutologousSerum.Checked;
                abstractRecord.PrescribeAutologousSerum = RadioButtonPrescribeAutologousSerum.Checked;
                abstractRecord.DiscussChangeTopical = RadioButtonDiscussChangeTopical.Checked;
                abstractRecord.PrescribeChangeTopical = RadioButtonPrescribeChangeTopical.Checked;
                abstractRecord.TopicalTherapyOther = RadioButtonTopicalTherapyOther.Checked;
                abstractRecord.DiscussOralTetra = RadioButtonDiscussOralTetra.Checked;
                abstractRecord.PrescribeOralTetra = RadioButtonPrescribeOralTetra.Checked;
                abstractRecord.DiscussOralNutritionSup = RadioButtonDiscussOralNutritionSup.Checked;
                abstractRecord.DiscussOralSecretagogues = RadioButtonDiscussOralSecretagogues.Checked;
                abstractRecord.PrescribeOralSecretagogues = RadioButtonPrescribeOralSecretagogues.Checked;
                abstractRecord.DiscussChangeSystemic = RadioButtonDiscussChangeSystemic.Checked;
                abstractRecord.PrescribeChangeSystemic = RadioButtonPrescribeChangeSystemic.Checked;
                abstractRecord.OralTherapyOther = RadioButtonOralTherapyOther.Checked;
                abstractRecord.DiscussTemporaryPunctal = RadioButtonDiscussTemporaryPunctal.Checked;
                abstractRecord.PrescribeTemporaryPunctal = RadioButtonPrescribeTemporaryPunctal.Checked;
                abstractRecord.DiscussPermanentPunctal = RadioButtonDiscussPermanentPunctal.Checked;
                abstractRecord.PrescribePermanentPunctal = RadioButtonPrescribePermanentPunctal.Checked;
                abstractRecord.DiscussLaser = RadioButtonDiscussLaser.Checked;
                abstractRecord.PrescribeLaser = RadioButtonPrescribeLaser.Checked;
                abstractRecord.DiscussThermocautery = RadioButtonDiscussThermocautery.Checked;
                abstractRecord.PrescribeThermocautery = RadioButtonPrescribeThermocautery.Checked;
                abstractRecord.DiscussSurgery = RadioButtonDiscussSurgery.Checked;
                abstractRecord.PrescribeSurgery = RadioButtonPrescribeSurgery.Checked;
                abstractRecord.DiscussGlandProcedure = RadioButtonDiscussGlandProcedure.Checked;
                abstractRecord.PrescribeGlandProcedure = RadioButtonPrescribeGlandProcedure.Checked;
                abstractRecord.DiscussConjunctivalSurgery = RadioButtonDiscussConjunctivalSurgery.Checked;
                abstractRecord.PrescribeConjunctivalSurgery = RadioButtonPrescribeConjunctivalSurgery.Checked;
                abstractRecord.DiscussEyelidSurgery = RadioButtonDiscussEyelidSurgery.Checked;
                abstractRecord.PrescribeEyelidSurgery = RadioButtonPrescribeEyelidSurgery.Checked;
                abstractRecord.SurgeryOther = RadioButtonSurgeryOther.Checked;
                abstractRecord.DiscussEyelidHygiene = RadioButtonDiscussEyelidHygiene.Checked;
                abstractRecord.DiscussEnvironmentMod = RadioButtonDiscussEnvironmentMod.Checked;
                abstractRecord.DiscussContactLensSoft = RadioButtonDiscussContactLensSoft.Checked;
                abstractRecord.PrescribeContactLensSoft = RadioButtonPrescribeContactLensSoft.Checked;
                abstractRecord.DiscussContactLensScleral = RadioButtonDiscussContactLensScleral.Checked;
                abstractRecord.PrescribeContactLensScleral = RadioButtonPrescribeContactLensScleral.Checked;
                abstractRecord.DiscussContactLensOther = RadioButtonDiscussContactLensOther.Checked;
                abstractRecord.PrescribeContactLensOther = RadioButtonPrescribeContactLensOther.Checked;
                abstractRecord.DiscussMoistureChamber = RadioButtonDiscussMoistureChamber.Checked;
                abstractRecord.PrescribeAdjunctiveTherapyOther = RadioButtonPrescribeAdjunctiveTherapyOther.Checked;
                abstractRecord.DiscussAdjunctiveTherapyOther = RadioButtonDiscussAdjunctiveTherapyOther.Checked;





                abstractRecord.OutcomeDiscussArtificialTears = CheckBoxOutcomeAlterArtificialTears.Checked;
                abstractRecord.OutcomeAqueousGelLubricant = CheckBoxOutcomeAqueousGelLubricant.Checked;
                abstractRecord.OutcomeDiscussNonMedOintment = CheckBoxOutcomeAlterNonMedOintment.Checked;
                abstractRecord.OutcomeDiscussPreservativeFreeTears = CheckBoxOutcomeAlterPreservativeFreeTears.Checked;
                abstractRecord.OutcomeDiscussEyelidHygiene = CheckBoxOutcomeAlterEyelidHygiene.Checked;
                abstractRecord.OutcomeDiscussTopicalCyclo = CheckBoxOutcomeAlterTopicalCyclo.Checked;
                abstractRecord.OutcomeDiscussTopicalCortico = CheckBoxOutcomeAlterTopicalCortico.Checked;
                abstractRecord.OutcomeDiscussTopicalAzithromycin = CheckBoxOutcomeAlterTopicalAzithromycin.Checked;
                abstractRecord.OutcomeDiscussTopicalOther = CheckBoxOutcomeAlterTopicalOther.Checked;
                //abstractRecord.OutcomeAlterTopicalTherapyOtherText = CheckBoxOutcomeAlterTopicalTherapyOtherText.Checked;
                abstractRecord.OutcomeDiscussTopicalMucolytics = CheckBoxOutcomeAlterTopicalMucolytics.Checked;
                abstractRecord.OutcomeDiscussOralTetra = CheckBoxOutcomeAlterOralTetra.Checked;
                abstractRecord.OutcomeDiscussOralNutritionSup = CheckBoxOutcomeAlterOralNutritionSup.Checked;
                abstractRecord.OutcomeDiscussOralSecretagogues = CheckBoxOutcomeAlterOralSecretagogues.Checked;
                abstractRecord.OutcomeDiscussOralOther = CheckBoxOutcomeAlterOralOther.Checked;
                //abstractRecord.OutcomeAlterOralOtherText = CheckBoxOutcomeAlterOralOtherText.Checked;
                abstractRecord.OutcomeDiscussTemporaryPunctal = CheckBoxOutcomeAlterTemporaryPunctal.Checked;
                abstractRecord.OutcomeDiscussPermanentPunctal = CheckBoxOutcomeAlterPermanentPunctal.Checked;
                abstractRecord.OutcomeDiscussLaser = CheckBoxOutcomeAlterLaser.Checked;
                abstractRecord.OutcomeDiscussThermocautery = CheckBoxOutcomeAlterThermocautery.Checked;
                abstractRecord.OutcomeDiscussSurgery = CheckBoxOutcomeAlterSurgery.Checked;
                abstractRecord.OutcomeDiscussContactLensSoft = CheckBoxOutcomeAlterContactLensSoft.Checked;
                abstractRecord.OutcomeDiscussContactLensScleral = CheckBoxOutcomeAlterContactLensScleral.Checked;
                abstractRecord.OutcomeDiscussContactLensOther = CheckBoxOutcomeAlterContactLensOther.Checked;
                abstractRecord.OutcomeDiscussAutologousSerum = CheckBoxOutcomeAlterAutologousSerum.Checked;
                abstractRecord.OutcomeDiscussChangeTopical = CheckBoxOutcomeAlterChangeTopical.Checked;
                abstractRecord.OutcomeDiscussChangeSystemic = CheckBoxOutcomeAlterChangeSystemic.Checked;
                abstractRecord.OutcomeDiscussMoistureChamber = CheckBoxOutcomeAlterMoistureChamber.Checked;
                abstractRecord.OutcomeDiscussEnvironmentMod = CheckBoxOutcomeAlterEnvironmentMod.Checked;
                abstractRecord.OutcomeDiscussEyelidSurgery = CheckBoxOutcomeAlterEyelidSurgery.Checked;
                abstractRecord.OutcomeDiscussConjunctivalSurgery = CheckBoxOutcomeAlterConjunctivalSurgery.Checked;
                abstractRecord.OutcomeDiscussGlandProcedure = CheckBoxOutcomeAlterGlandProcedure.Checked;
                abstractRecord.OutcomeDiscussOther = CheckBoxOutcomeAlterOther.Checked;
                abstractRecord.OutcomePunctalStillOccluded = CheckBoxOutcomePunctalStillOccluded.Checked;

            
                abstractRecord.OutcomeAlterTopicalCyclo = CheckBoxOutcomeAlterTopicalCycloP.Checked;
                abstractRecord.OutcomeAlterTopicalCortico = CheckBoxOutcomeAlterTopicalCorticoP.Checked;
                abstractRecord.OutcomeAlterTopicalAzithromycin = CheckBoxOutcomeAlterTopicalAzithromycinP.Checked;
                abstractRecord.OutcomeAlterTopicalOther = CheckBoxOutcomeAlterTopicalOtherP.Checked;
                //abstractRecord.OutcomeAlterTopicalTherapyOtherText = CheckBoxOutcomeAlterTopicalTherapyOtherTextP.Checked;
                abstractRecord.OutcomeAlterTopicalMucolytics = CheckBoxOutcomeAlterTopicalMucolyticsP.Checked;
                abstractRecord.OutcomeAlterOralTetra = CheckBoxOutcomeAlterOralTetraP.Checked;
                abstractRecord.OutcomeAlterOralSecretagogues = CheckBoxOutcomeAlterOralSecretagoguesP.Checked;
                abstractRecord.OutcomeAlterOralOther = CheckBoxOutcomeAlterOralOtherP.Checked;
                //abstractRecord.OutcomeAlterOralOtherText = CheckBoxOutcomeAlterOralOtherTextP.Checked;
                abstractRecord.OutcomeAlterTemporaryPunctal = CheckBoxOutcomeAlterTemporaryPunctalP.Checked;
                abstractRecord.OutcomeAlterPermanentPunctal = CheckBoxOutcomeAlterPermanentPunctalP.Checked;
                abstractRecord.OutcomeAlterLaser = CheckBoxOutcomeAlterLaserP.Checked;
                abstractRecord.OutcomeAlterThermocautery = CheckBoxOutcomeAlterThermocauteryP.Checked;
                abstractRecord.OutcomeAlterSurgery = CheckBoxOutcomeAlterSurgeryP.Checked;
                abstractRecord.OutcomeAlterContactLensSoft = CheckBoxOutcomeAlterContactLensSoftP.Checked;
                abstractRecord.OutcomeAlterContactLensScleral = CheckBoxOutcomeAlterContactLensScleralP.Checked;
                abstractRecord.OutcomeAlterContactLensOther = CheckBoxOutcomeAlterContactLensOtherP.Checked;
                abstractRecord.OutcomeAlterAutologousSerum = CheckBoxOutcomeAlterAutologousSerumP.Checked;
                abstractRecord.OutcomeAlterChangeTopical = CheckBoxOutcomeAlterChangeTopicalP.Checked;
                abstractRecord.OutcomeAlterChangeSystemic = CheckBoxOutcomeAlterChangeSystemicP.Checked;
                abstractRecord.OutcomeAlterEyelidSurgery = CheckBoxOutcomeAlterEyelidSurgeryP.Checked;
                abstractRecord.OutcomeAlterConjunctivalSurgery = CheckBoxOutcomeAlterConjunctivalSurgeryP.Checked;
                abstractRecord.OutcomeAlterGlandProcedure = CheckBoxOutcomeAlterGlandProcedureP.Checked;
                abstractRecord.OutcomeAlterOther = CheckBoxOutcomeAlterOtherP.Checked;

                abstractRecord.ExamTearOsmolarity = Convert.ToInt32(CheckBoxExamTearOsmolarity.Checked);
              

                abstractRecord.OutcomeFindingsImprovedEyelid = CheckBoxOutcomeFindingsImprovedEyelid.Checked;
                abstractRecord.OutcomeFindingsImprovedBlink = CheckBoxOutcomeFindingsImprovedBlink.Checked;
                abstractRecord.OutcomeFindingsImprovedLashed = CheckBoxOutcomeFindingsImprovedLashed.Checked;
                abstractRecord.OutcomeFindingsImprovedGlands = CheckBoxOutcomeFindingsImprovedGlands.Checked;
                abstractRecord.OutcomeFindingsImprovedTearMeniscus = CheckBoxOutcomeFindingsImprovedTearMeniscus.Checked;
                abstractRecord.OutcomeFindingsImprovedTearTime = CheckBoxOutcomeFindingsImprovedTearTime.Checked;
                abstractRecord.OutcomeFindingsImprovedSchirmer = CheckBoxOutcomeFindingsImprovedSchirmer.Checked;
                abstractRecord.OutcomeFindingsImprovedTearOsmolarity = CheckBoxOutcomeFindingsImprovedTearOsmolarity.Checked;
                abstractRecord.OutcomeFindingsImprovedConjunctival = CheckBoxOutcomeFindingsImprovedConjunctival.Checked;
                abstractRecord.OutcomeFindingsImprovedCorneal = CheckBoxOutcomeFindingsImprovedCorneal.Checked;
                abstractRecord.OutcomeFindingsImprovedOther = CheckBoxOutcomeFindingsImprovedOther.Checked;
                if (RadioButtonListPresentingDryMouth.SelectedIndex != -1) abstractRecord.PresentingDryMouth= Convert.ToInt32(RadioButtonListPresentingDryMouth.SelectedValue);
                else abstractRecord.PresentingDryMouth = 0;
                if (RadioButtonListSmoking.SelectedIndex != -1) abstractRecord.Smoking= Convert.ToInt32(RadioButtonListSmoking.SelectedValue);
                else abstractRecord.Smoking = 0;
                if (RadioButtonListContactLens.SelectedIndex != -1) abstractRecord.ContactLens= Convert.ToInt32(RadioButtonListContactLens.SelectedValue);
                else abstractRecord.ContactLens = 0;

                if (RadioButtonListRBGender.SelectedIndex != -1) abstractRecord.RBGender = Convert.ToInt32(RadioButtonListRBGender.SelectedValue);
                else abstractRecord.RBGender = 0;
                if (DropDownListMonthOfBirth.SelectedIndex != 0) abstractRecord.MonthOfBirth = Convert.ToInt32(DropDownListMonthOfBirth.SelectedValue);
                else abstractRecord.MonthOfBirth = 0;
                if (DropDownListYearOfBirth.SelectedIndex != 0) abstractRecord.YearOfBirth = Convert.ToInt32(DropDownListYearOfBirth.SelectedValue);
                else abstractRecord.YearOfBirth = 0;
                if (DropDownListOutcomeDateMonth.SelectedIndex != 0) abstractRecord.OutcomeDateMonth = Convert.ToInt32(DropDownListOutcomeDateMonth.SelectedValue);
                else abstractRecord.OutcomeDateMonth = 0;
                if (DropDownListOutcomeDateYear.SelectedIndex != 0) abstractRecord.OutcomeDateYear = Convert.ToInt32(DropDownListOutcomeDateYear.SelectedValue);
                else abstractRecord.OutcomeDateYear = 0;


                if (RadioButtonListEyeSurgery.SelectedIndex != -1) abstractRecord.EyeSurgery= Convert.ToInt32(RadioButtonListEyeSurgery.SelectedValue);
                else abstractRecord.EyeSurgery = 0;
                if (RadioButtonListTopicalOralMed.SelectedIndex != -1) abstractRecord.TopicalOralMed= Convert.ToInt32(RadioButtonListTopicalOralMed.SelectedValue);
                else abstractRecord.TopicalOralMed = 0;
                if (RadioButtonListExamAbEyelidPosition.SelectedIndex != -1) abstractRecord.ExamAbEyelidPosition= Convert.ToInt32(RadioButtonListExamAbEyelidPosition.SelectedValue);
                else abstractRecord.ExamAbEyelidPosition = 0;
                if (RadioButtonListExamAbnormalBlink.SelectedIndex != -1) abstractRecord.ExamAbnormalBlink= Convert.ToInt32(RadioButtonListExamAbnormalBlink.SelectedValue);
                else abstractRecord.ExamAbnormalBlink = 0;
                if (RadioButtonListExamLashes.SelectedIndex != -1) abstractRecord.ExamLashes= Convert.ToInt32(RadioButtonListExamLashes.SelectedValue);
                else abstractRecord.ExamLashes = 0;
                if (RadioButtonListExamEyelidsEverted.SelectedIndex != -1) abstractRecord.ExamEyelidsEverted= Convert.ToInt32(RadioButtonListExamEyelidsEverted.SelectedValue);
                else abstractRecord.ExamEyelidsEverted = 0;
                if (RadioButtonListExamMeibomianDisease.SelectedIndex != -1) abstractRecord.ExamMeibomianDisease= Convert.ToInt32(RadioButtonListExamMeibomianDisease.SelectedValue);
                else abstractRecord.ExamMeibomianDisease = 0;
                if (RadioButtonListExamTearHeight.SelectedIndex != -1) abstractRecord.ExamTearHeight= Convert.ToInt32(RadioButtonListExamTearHeight.SelectedValue);
                else abstractRecord.ExamTearHeight = 0;
                if (RadioButtonListExamTearBreakupTime.SelectedIndex != -1) abstractRecord.ExamTearBreakupTime= Convert.ToInt32(RadioButtonListExamTearBreakupTime.SelectedValue);
                else abstractRecord.ExamTearBreakupTime = 0;
                if (RadioButtonListExamOcularSurfaceStain.SelectedIndex != -1) abstractRecord.ExamOcularSurfaceStain= Convert.ToInt32(RadioButtonListExamOcularSurfaceStain.SelectedValue);
                else abstractRecord.ExamOcularSurfaceStain = 0;
                if (RadioButtonListExamSchirmer.SelectedIndex != -1) abstractRecord.ExamSchirmer= Convert.ToInt32(RadioButtonListExamSchirmer.SelectedValue);
                else abstractRecord.ExamSchirmer = 0;
                if (RadioButtonListExamCornealSensation.SelectedIndex != -1) abstractRecord.ExamCornealSensation= Convert.ToInt32(RadioButtonListExamCornealSensation.SelectedValue);
                else abstractRecord.ExamCornealSensation = 0;
                if (RadioButtonListAssessDiseaseSeverity.SelectedIndex != -1) abstractRecord.AssessDiseaseSeverity= Convert.ToInt32(RadioButtonListAssessDiseaseSeverity.SelectedValue);
                else abstractRecord.AssessDiseaseSeverity = 0;
                if (RadioButtonListEMSystemicDisease.SelectedIndex != -1) abstractRecord.EMSystemicDisease= Convert.ToInt32(RadioButtonListEMSystemicDisease.SelectedValue);
                else abstractRecord.EMSystemicDisease = 0;
                if (RadioButtonListOutcomePunctal.SelectedIndex != -1) abstractRecord.OutcomePunctal= Convert.ToInt32(RadioButtonListOutcomePunctal.SelectedValue);
                else abstractRecord.OutcomePunctal = 0;
                if (RadioButtonListOutcomePunctalFollowup.SelectedIndex != -1) abstractRecord.OutcomePunctalFollowup= Convert.ToInt32(RadioButtonListOutcomePunctalFollowup.SelectedValue);
                else abstractRecord.OutcomePunctalFollowup = 0;
                if (RadioButtonListOutcomeSymptomsImproved.SelectedIndex != -1) abstractRecord.OutcomeSymptomsImproved= Convert.ToInt32(RadioButtonListOutcomeSymptomsImproved.SelectedValue);
                else abstractRecord.OutcomeSymptomsImproved = 0;
                if (RadioButtonListOutcomeFindingsImproved.SelectedIndex != -1) abstractRecord.OutcomeFindingsImproved = Convert.ToInt32(RadioButtonListOutcomeFindingsImproved.SelectedValue);
                else abstractRecord.OutcomeFindingsImproved = 0;



                try
                {
                if (TextBoxPresentingExacOtherText.Text != null)
                abstractRecord.TextBoxPresentingExacOtherText = TextBoxPresentingExacOtherText.Text;
                }
                catch (Exception ex)
                {
                abstractRecord.TextBoxPresentingExacOtherText = null;
                TextBoxPresentingExacOtherText.Text = "";
                }


                try
                {
                if (TextBoxPresentingDiseaseOtherText.Text != null)
                abstractRecord.PresentingDiseaseOtherText = TextBoxPresentingDiseaseOtherText.Text;
                }
                catch (Exception ex)
                {
                abstractRecord.PresentingDiseaseOtherText = null;
                TextBoxPresentingDiseaseOtherText.Text = "";
                }


                        try
                        {
                        if (TextBoxExamLashesText.Text != null)
                        abstractRecord.ExamLashesText = TextBoxExamLashesText.Text;
                        }
                        catch (Exception ex)
                        {
                        abstractRecord.ExamLashesText = null;
                        TextBoxExamLashesText.Text = "";
                        }




                        try
                        {
                        if (TextBoxExamTearOsmolarityOsmolesNbr.Text != null)
                        abstractRecord.ExamTearOsmolarityOsmolesNbr = Convert.ToInt32(TextBoxExamTearOsmolarityOsmolesNbr.Text);
                        }
                        catch (Exception ex)
                        {
                        abstractRecord.ExamTearOsmolarityOsmolesNbr = null;
                        TextBoxExamTearOsmolarityOsmolesNbr.Text = "";
                        }


                        try
                        {
                            if (TextBoxExamTearOsmolarityOsmolesNbr.Text != null)
                                abstractRecord.ExamTearOsmolarityOsmolesNbr = Convert.ToInt32(TextBoxExamTearOsmolarityOsmolesNbr.Text);
                        }
                        catch (Exception ex)
                        {
                            abstractRecord.ExamTearOsmolarityOsmolesNbr = null;
                            TextBoxExamTearOsmolarityOsmolesNbr.Text = "";
                        }



                        try
                        {
                        if (TextBoxExamConjFindOtherText.Text != null)
                        abstractRecord.ExamConjFindOtherText = TextBoxExamConjFindOtherText.Text;
                        }
                        catch (Exception ex)
                        {
                        abstractRecord.ExamConjFindOtherText = null;
                        TextBoxExamConjFindOtherText.Text = "";
                        }
                        try
                        {
                        if (TextBoxExamCornealOtherText.Text != null)
                        abstractRecord.ExamCornealOtherText = TextBoxExamCornealOtherText.Text;
                        }
                        catch (Exception ex)
                        {
                        abstractRecord.ExamCornealOtherText = null;
                        TextBoxExamCornealOtherText.Text = "";
                        }
                        try
                        {
                        if (TextBoxDiagnosisOtherText.Text != null)
                        abstractRecord.DiagnosisOtherText = TextBoxDiagnosisOtherText.Text;
                        }
                        catch (Exception ex)
                        {
                        abstractRecord.DiagnosisOtherText = null;
                        TextBoxDiagnosisOtherText.Text = "";
                        }
                        try
                        {
                        if (TextBoxTopicalTherapyOtherText.Text != null)
                        abstractRecord.TopicalTherapyOtherText = TextBoxTopicalTherapyOtherText.Text;
                        }
                        catch (Exception ex)
                        {
                        abstractRecord.TopicalTherapyOtherText = null;
                        TextBoxTopicalTherapyOtherText.Text = "";
                        }
                        try
                        {
                        if (TextBoxOralTherapyOtherText.Text != null)
                        abstractRecord.OralTherapyOtherText = TextBoxOralTherapyOtherText.Text;
                        }
                        catch (Exception ex)
                        {
                        abstractRecord.OralTherapyOtherText = null;
                        TextBoxOralTherapyOtherText.Text = "";
                        }
                        try
                        {
                        if (TextBoxSurgeryOtherText.Text != null)
                        abstractRecord.SurgeryOtherText = TextBoxSurgeryOtherText.Text;
                        }
                        catch (Exception ex)
                        {
                        abstractRecord.SurgeryOtherText = null;
                        TextBoxSurgeryOtherText.Text = "";
                        }
                        try
                        {
                        if (TextBoxDiscussAdjunctiveTherapyOtherText.Text != null)
                        abstractRecord.DiscussAdjunctiveTherapyOtherText = TextBoxDiscussAdjunctiveTherapyOtherText.Text;
                        }
                        catch (Exception ex)
                        {
                        abstractRecord.DiscussAdjunctiveTherapyOtherText = null;
                        TextBoxDiscussAdjunctiveTherapyOtherText.Text = "";
                        }
                        try
                        {
                        if (TextBoxOutcomeAlterTopicalTherapyOtherText.Text != null)
                        abstractRecord.OutcomeAlterTopicalTherapyOtherText = TextBoxOutcomeAlterTopicalTherapyOtherText.Text;
                        }
                        catch (Exception ex)
                        {
                        abstractRecord.OutcomeAlterTopicalTherapyOtherText = null;
                        TextBoxOutcomeAlterTopicalTherapyOtherText.Text = "";
                        }
                        try
                        {
                        if (TextBoxOutcomeAlterOralOtherText.Text != null)
                        abstractRecord.OutcomeAlterOralOtherText = TextBoxOutcomeAlterOralOtherText.Text;
                        }
                        catch (Exception ex)
                        {
                        abstractRecord.OutcomeAlterOralOtherText = null;
                        TextBoxOutcomeAlterOralOtherText.Text = "";
                        }
                        try
                        {
                        if (TextBoxOutcomeAlterProcIntervOtherText.Text != null)
                        abstractRecord.OutcomeAlterProcIntervOtherText = TextBoxOutcomeAlterProcIntervOtherText.Text;
                        }
                        catch (Exception ex)
                        {
                        abstractRecord.OutcomeAlterProcIntervOtherText = null;
                        TextBoxOutcomeAlterProcIntervOtherText.Text = "";
                        }
                        try
                        {
                        if (TextBoxOutcomeAlterOtherText.Text != null)
                        abstractRecord.OutcomeAlterOtherText = TextBoxOutcomeAlterOtherText.Text;
                        }
                        catch (Exception ex)
                        {
                        abstractRecord.OutcomeAlterOtherText = null;
                        TextBoxOutcomeAlterOtherText.Text = "";
                        }
                        try
                        {
                        if (TextBoxOutcomeOtherText.Text != null)
                        abstractRecord.OutcomeOtherText = TextBoxOutcomeOtherText.Text;
                        }
                        catch (Exception ex)
                        {
                        abstractRecord.OutcomeOtherText = null;
                        TextBoxOutcomeOtherText.Text = "";
                        }




                abstractRecord.LastUpdateDate = DateTime.Now;
                bool ChartCompleted = isComplete();
                abstractRecord.Complete = ChartCompleted;
                var chartRegistration = (from cr in pim.ChartRegistration
                                         where cr.ParticipantModuleCycleID == cycle.ParticipantModuleCycleID
                                          & cr.ModuleID == ModuleID
                                          & cr.RecordIdentifier == recordIdentifier
                                         select cr).First<NetHealthPIMModel.ChartRegistration>();
                chartRegistration.AbstractCompleted = ChartCompleted;


                if ((DropDownListYearOfBirth.SelectedIndex > 0) && (DropDownListYearOfBirth.SelectedIndex > 0))
                {
                    chartRegistration.DOB = abstractRecord.MonthOfBirth + "/" + abstractRecord.YearOfBirth;
                }

                pim.SaveChanges();

                transaction.Complete();




            }

            CycleManager.UpdateParticipantCompletedModules(cycleID, ModuleID);
        }
    }

    protected void ButtonSubmit_Click(object sender, EventArgs e)
    {
        savedata();
        isComplete();
        if (!isComplete())
        {
            isComplete();
            string script = " var answer = confirm('All fields are not complete.  Are you sure you want to submit?'); if (answer==true) window.location = '../PatientChartRegistration.aspx';";
            ScriptManager.RegisterStartupScript(this, GetType(),
                          "ServerControlScript", script, true);
        }
        else
        {

            Response.Redirect("../PatientChartRegistration.aspx");
        }
    }

    protected bool isComplete()
    {

        bool ChartCompleted = true;
        LabelOutcomePunctal.Visible = false;
        LabelOutcomePunctalFollowup.Visible = false;
        LabelGender.Visible = false;
        LabelMonthOfBirth.Visible = false;
        LabelYearOfBirth.Visible = false;
        LabelPresentingDryMouth.Visible = false;
        LabelSmoking.Visible = false;
        LabelContactLens.Visible = false;
        LabelEyeSurgery.Visible = false;
        LabelTopicalOralMed.Visible = false;
        LabelAbEyelidPosition.Visible = false;
        LabelExamAbnormalBlink.Visible = false;
        LabelExamLashes.Visible = false;
        LabelExamLashesText.Visible = false;
        LabelExamEyelidsEverted.Visible = false;
        LabelExamMeibomianDisease.Visible = false;
        LabelExamTearHeight.Visible = false;
        LabelExamTearBreakupTime.Visible = false;
        LabelExamOcularSurfaceStain.Visible = false;
        LabelExamSchirmer.Visible = false;
        LabelExamTearOsmolarity.Visible = false;
        LabelExamCornealSensation.Visible = false;
        LabelAssessDiseaseSeverity.Visible = false;
        LabelEMSystemicDisease.Visible = false;
        LabelOutcomePunctal.Visible = false;
        LabelOutcomePunctalFollowup.Visible = false;
        LabelOutcomeSymptomsImproved.Visible = false;
        LabelOutcomeFindingsImproved.Visible = false;

        if (RadioButtonListOutcomePunctal.SelectedIndex == -1)
        {
            LabelOutcomePunctal.Visible = true;
            ChartCompleted = false;
        }
        if(RadioButtonListOutcomePunctal.SelectedIndex!=-1)
        {
           if (RadioButtonListOutcomePunctal.SelectedItem.Value != "300" && RadioButtonListOutcomePunctal.SelectedItem.Value != "362")
           {
            if (RadioButtonListOutcomePunctalFollowup.SelectedIndex == -1 && CheckBoxOutcomePunctalStillOccluded.Checked == false)
            {
                LabelOutcomePunctalFollowup.Visible = true;
                ChartCompleted = false;
            }
            
           }
          if ((RadioButtonListOutcomePunctal.SelectedItem.Value != "300" && RadioButtonListOutcomePunctal.SelectedItem.Value != "362"))
           {
            if (RadioButtonListOutcomePunctalFollowup.SelectedIndex == -1 && CheckBoxOutcomePunctalStillOccluded.Checked == false)
            {
                LabelOutcomePunctalFollowup.Visible = true;
                ChartCompleted = false;
            }

           }
        } 
        if (RadioButtonListRBGender.SelectedIndex == -1)
        {
            LabelGender.Visible = true;
            ChartCompleted = false;
        }
        if (DropDownListMonthOfBirth.SelectedIndex == 0)
        {
            LabelMonthOfBirth.Visible = true;
            ChartCompleted = false;
        }
        if (DropDownListYearOfBirth.SelectedIndex == 0)
        {
            LabelYearOfBirth.Visible = true;
            ChartCompleted = false;
        }
        if (RadioButtonListPresentingDryMouth.SelectedIndex == -1)
        {
            LabelPresentingDryMouth.Visible = true;
            ChartCompleted = false;
        }
        if (RadioButtonListSmoking.SelectedIndex == -1)
        {
            LabelSmoking.Visible = true;
            ChartCompleted = false;
        }
        if (RadioButtonListContactLens.SelectedIndex == -1)
        {
            LabelContactLens.Visible = true;
            ChartCompleted = false;
        }
        if (RadioButtonListEyeSurgery.SelectedIndex == -1)
        {
            LabelEyeSurgery.Visible = true;
            ChartCompleted = false;
        }
        if (RadioButtonListTopicalOralMed.SelectedIndex == -1)
        {
            LabelTopicalOralMed.Visible = true;
            ChartCompleted = false;
        }
        if (RadioButtonListExamAbEyelidPosition.SelectedIndex == -1)
        {
            LabelAbEyelidPosition.Visible = true;
            ChartCompleted = false;
        }
        if (RadioButtonListExamAbnormalBlink.SelectedIndex == -1)
        {
            LabelExamAbnormalBlink.Visible = true;
            ChartCompleted = false;
        }
        if (RadioButtonListExamLashes.SelectedIndex == -1)
        {
            LabelExamLashes.Visible = true;
            ChartCompleted = false;
        }
        if (RadioButtonListExamLashes.SelectedIndex != -1 && RadioButtonListExamLashes.SelectedValue == "13")
        {
            if (TextBoxExamLashesText.Text=="")
            {
                LabelExamLashesText.Visible = true;
                ChartCompleted = false;
            }
        }
        if (RadioButtonListExamEyelidsEverted.SelectedIndex == -1)
        {
            LabelExamEyelidsEverted.Visible = true;
            ChartCompleted = false;
        }
        if (RadioButtonListExamMeibomianDisease.SelectedIndex == -1)
        {
            LabelExamMeibomianDisease.Visible = true;
            ChartCompleted = false;
        }
        if (RadioButtonListExamTearHeight.SelectedIndex == -1)
        {
            LabelExamTearHeight.Visible = true;
            ChartCompleted = false;
        }
        if (RadioButtonListExamTearBreakupTime.SelectedIndex == -1)
        {
            LabelExamTearBreakupTime.Visible = true;
            ChartCompleted = false;
        }
        if (RadioButtonListExamOcularSurfaceStain.SelectedIndex == -1)
        {
            LabelExamOcularSurfaceStain.Visible = true;
            ChartCompleted = false;
        }
        if (RadioButtonListExamSchirmer.SelectedIndex == -1)
        {
            LabelExamSchirmer.Visible = true;
            ChartCompleted = false;
        }
        if (TextBoxExamTearOsmolarityOsmolesNbr.Text == "" && CheckBoxExamTearOsmolarity.Checked==false)
        {
            LabelExamTearOsmolarity.Visible = true;
            ChartCompleted = false;
        }
        if (RadioButtonListExamCornealSensation.SelectedIndex == -1)
        {
            LabelExamCornealSensation.Visible = true;
            ChartCompleted = false;
        }
        if (RadioButtonListAssessDiseaseSeverity.SelectedIndex == -1)
        {
            LabelAssessDiseaseSeverity.Visible = true;
            ChartCompleted = false;
        }
        if (RadioButtonListEMSystemicDisease.SelectedIndex == -1)
        {
            LabelEMSystemicDisease.Visible = true;
            ChartCompleted = false;
        }
        if (RadioButtonListOutcomePunctal.SelectedIndex == -1)
        {
            LabelOutcomePunctal.Visible = true;
            ChartCompleted = false;
        }
        if (RadioButtonListOutcomePunctalFollowup.SelectedIndex == -1 && RadioButtonListOutcomePunctalFollowup.SelectedValue == "300")
        {
            if (RadioButtonListOutcomePunctalFollowup.SelectedIndex == -1)
            {
                LabelOutcomePunctalFollowup.Visible = true;
                ChartCompleted = false;
            }
        }

        if (RadioButtonListOutcomeSymptomsImproved.SelectedIndex == -1)
        {
            LabelOutcomeSymptomsImproved.Visible = true;
            ChartCompleted = false;
        }
        if (RadioButtonListOutcomeFindingsImproved.SelectedIndex == -1)
        {
            LabelOutcomeFindingsImproved.Visible = true;
            ChartCompleted = false;
        }
        

        return ChartCompleted;
    }
}