﻿<%@ Page Title="" Language="C#" MasterPageFile="~/PIMMasterPage.master" AutoEventWireup="true" CodeFile="POAGSChart.aspx.cs" Inherits="abo_POAGSChart" %>

<asp:Content ID="Content1" ContentPlaceHolderID="head" Runat="Server">
    <link type="text/css" href="../../common/css/atooltip.css" rel="stylesheet"  media="screen" />    
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder1" runat="Server">
    <asp:HiddenField ID="HiddenFieldRecordIdentifier" runat="server" />
    <!-- POAG pim -->
    <div class="pim">
        <div class="record-ident clearfix">
            <h3 class="record-first">RECORD IDENTIFIER:
                <asp:Literal runat="server" ID="LiteralRecordIdentifier"></asp:Literal></h3>
            <h3 class="record-second">
                <asp:Literal runat="server" ID="LiteralAbstractionNumber"></asp:Literal></h3>
        </div>
        <!-- PQRS Panel -->
        <asp:Panel runat="server" Visible="true" ID="PQRSPanel" CssClass="clear">
            <table>
                <tr>
                    <th colspan="3" style="width: 910px;">
                        <p>Measure #12: Optic Nerve Evaluation</p>
                    </th>
                </tr>
                <!-- Question 1 -->
                <tr class="table_body">
                    <td style="width: 70%;">
                        <strong>1. Is this a Physician Fee Schedule (PFS) Medicare Part B Fee-For-Service (FFS) beneficiary?</strong>&nbsp;<a href="#"><img id="PQRSQ2" src="../../common/images/tip.gif" alt="Tip" /></a>
                        <asp:Label ID="LabelPQRSMedicare" runat="server" ForeColor="Red" Visible="false" Text="<br />Please Complete"></asp:Label>
                    </td>
                    <td>
                        <asp:RadioButtonList runat="server" ID="PQRSMedicare" RepeatDirection="Horizontal" RepeatLayout="Flow">
                            <asp:ListItem Value="1" Text=" Yes &nbsp;" />
                            <asp:ListItem Value="0" Text=" No" />
                        </asp:RadioButtonList>
                    </td>
                </tr>
                <!-- Question 2 -->
                <tr class="table_body_bg" id="PQRS12Q2">
                    <td>
                        <strong>2. Did the patient have one of the noted diagnosis codes?</strong> (see tip for codes)&nbsp;<a href="#"><img id="PQRSQ3" src="../../common/images/tip.gif" alt="Tip" /></a>
                        <asp:Label ID="LabelPQRSDiagnosis" runat="server" ForeColor="Red" Visible="false" Text="<br />Please Complete"></asp:Label>
                    </td>
                    <td>
                        <asp:RadioButtonList runat="server" ID="PQRSDiagnosis" RepeatDirection="Horizontal" RepeatLayout="Flow">
                            <asp:ListItem Value="1" Text=" Yes &nbsp;" />
                            <asp:ListItem Value="0" Text=" No" />
                        </asp:RadioButtonList>
                    </td>
                </tr>
                <!-- Question 3 -->
                <tr class="table_body" id="PQRS12Q3">
                    <td>
                        <strong>3. Did the patient have one of the noted encounter codes (CPT)?</strong>
                        <asp:Label ID="LabelPQRSEncounter" runat="server" ForeColor="Red" Visible="false" Text="<br />Please Complete"></asp:Label>
                    </td>
                    <td>
                        <asp:CheckBox runat='server' ID='CheckBoxPQRSEncounterA' Text='&nbsp;A)	92002, 92004, 92012, 92014, 99201, 99202, 99203, 99204, 99205, 99212, 99213, 99214, 99215, 99307, 99308, 99309, 99310, 99324, 99325, 99326, 99327, 99328, 99334, 99335, 99336, 99337' />
                        <br />
                        <asp:CheckBox runat='server' ID='CheckBoxPQRSEncounterB' Text='&nbsp;B) 99304, 99305, 99306' />
                    </td>
                </tr>
                <!-- Question 4 -->
                <tr class="table_body_bg" id="PQRS12Q4">
                    <td>
                        <strong>4. Date of patient visit with one of the specified encounter codes</strong>
                        <asp:Label ID="LabelPQRSVisitMonth" runat="server" ForeColor="Red" Visible="false" Text="<br />Please Complete Month"></asp:Label>
                        <asp:Label ID="LabelPQRSVisitYear" runat="server" ForeColor="Red" Visible="false" Text="<br />Please Complete Year"></asp:Label>

                    </td>
                    <td>
                        <asp:DropDownList ID='DropDownListPQRSVisitMonth' DataValueField='MonthID' DataTextField='MonthName' runat='server' />
                        <asp:DropDownList ID='DropDownListPQRSVisitYear' DataValueField='YearID' DataTextField='YearName' runat='server' />
                    </td>
                </tr>
                <!-- Question 5 -->
                <tr class="table_body" id="PQRS12Q5">
                    <td>
                        <strong>5. Did the patient have an optic nerve head evaluation on the visit date or within 12 months prior to the visit date?</strong>
                        <asp:Label ID="LabelPQRSOpticNerveEval" runat="server" ForeColor="Red" Visible="false" Text="<br />Please Complete"></asp:Label>

                    </td>
                    <td>
                        <asp:RadioButtonList runat="server" ID="PQRSOpticNerveEval" RepeatDirection="Horizontal" RepeatLayout="Flow">
                            <asp:ListItem Value="1" Text=" Yes &nbsp;" />
                            <asp:ListItem Value="0" Text=" No" />
                        </asp:RadioButtonList>
                    </td>
                </tr>
                <!-- Question 6 -->
                <tr class="table_body_bg" id="PQRS12Q6">
                    <td>
                        <strong>6. If an optic nerve head evaluation was not performed in the last 12 months, was this due to a documented medical reason?</strong>&nbsp;<a href="#"><img id="PQRSQ7" src="../../common/images/tip.gif" alt="Tip" /></a>
                        <asp:Label ID="LabelNerveNo" runat="server" ForeColor="Red" Visible="false" Text="<br />Please Complete"></asp:Label>
                    </td>
                    <td>
                        <asp:RadioButtonList runat="server" ID="PQRSOpticNerveReason" RepeatDirection="Horizontal" RepeatLayout="Flow">
                            <asp:ListItem Value="1" Text=" Yes &nbsp;" />
                            <asp:ListItem Value="0" Text=" No" />
                        </asp:RadioButtonList>
                    </td>
                </tr>
            </table>
            <br />
            <table>
                <tr>
                    <th colspan="3" style="width: 910px;">
                        <p>Measure #141: Reduction of Intraocular Pressure (IOP) by 15% OR Documentation of a Plan of Care</p>
                    </th>
                </tr>
                <!-- Question 1 -->
                <tr class="table_body" id="PQRS141Q1">
                    <td colspan="2" style="width: 70%;">
                        <strong>1. Was an IOP measurement documented on the Visit Date?</strong>
                        <asp:Label ID="LabelPQRSAffectedEye" runat="server" ForeColor="Red" Visible="false" Text="<br />Please Complete"></asp:Label>
                    </td>
                    <td>
                        <asp:RadioButtonList runat="server" ID="PQRSIOPDocumented" RepeatDirection="Horizontal" RepeatLayout="Flow">
                            <asp:ListItem Value="1" Text=" Yes &nbsp;" />
                            <asp:ListItem Value="0" Text=" No" />
                        </asp:RadioButtonList>
                    </td>
                </tr>

                <!-- Question 2 -->
                <tr class="table_body_bg" id="PQRS141Q2">
                    <td colspan="2">
                        <strong>2. Was the IOP reduced by at least 15% from the pre-intervention level in the affected eye? If both eyes were affected, the reduction of at least 15% must have occurred in both eyes.</strong>
                        <asp:Label ID="PQRSRecentODIOPNoPerformOS" runat="server" ForeColor="Red" Visible="false" Text="<br />Please Complete"></asp:Label>
                    </td>
                    <td>
                        <asp:RadioButtonList runat="server" ID="PQRSIOPReduced" RepeatDirection="Horizontal" RepeatLayout="Flow">
                            <asp:ListItem Value="1" Text=" Yes &nbsp;" />
                            <asp:ListItem Value="0" Text=" No" />
                        </asp:RadioButtonList>
                    </td>
                </tr>
                <!-- Question 3 -->
                <tr class="table_body" id="PQRS141Q3">
                    <td colspan="2">
                        <strong>3. If IOP measurement was documented and IOP was not reduced by at least 15% from the pre-intervention level, was a Glaucoma plan of care documented?</strong>&nbsp;<a href="#"><img id="PQRS141Q4" src="../../common/images/tip.gif" alt="Tip" /></a>
                    </td>
                    <td>
                        <asp:RadioButtonList runat="server" ID="PQRSGlaucomaPlan" RepeatDirection="Horizontal" RepeatLayout="Flow">
                            <asp:ListItem Value="1" Text=" Yes &nbsp;" />
                            <asp:ListItem Value="0" Text=" No" />
                        </asp:RadioButtonList>
                    </td>
                </tr>

            </table>
            <br />
            <script>
                $(function () {
                    $('#PQRSQ2').aToolTip({
                        clickIt: true,
                        tipContent: 'Answer "Yes" to this question if the patient is being billed for this visit to Medicare Part B Fee For Service as either their primary or secondary insurance, including Railroad Retirement Board. This does not include Medicare Advantage.'
                    });
                });

                $(function () {
                    $('#PQRSQ3').aToolTip({
                        clickIt: true,
                        tipContent: '<p><strong>ICD-9-CM [for use 1/1/2015-9/30/2015]:</strong> 365.10, 365.11, 365.12, 365.15</p><p><strong>ICD-10-CM [for use 10/1/2015-12/31/2015]:</strong> H40.10X0, H40.10X1, H40.10X2, H40.10X3, H40.10X4, H40.11X0, H40.11X1, H40.11X2, H40.11X3, H40.11X4, H40.1210, H40.1211, H40.1212, H40.1213, H40.1214, H40.1220, H40.1221, H40.1222, H40.1223, H40.1224, H40.1230, H40.1231, H40.1232, H40.1233, H40.1234, H40.1290, H40.1291, H40.1292, H40.1293, H40.1294, H40.151, H40.152, H40.153, H40.159</p>'
                    });
                });

                $(function () {
                    $('#PQRSQ7').aToolTip({
                        clickIt: true,
                        tipContent: 'The medical reason must be documented in the medical record.'
                    });
                });

                $(function () {
                    $('#PQRS141Q4').aToolTip({
                        clickIt: true,
                        tipContent: 'A plan of care may include: recheck of IOP at specified time, change in therapy, perform additional diagnostic evaluations, monitoring per patient decisions or health system reasons, and/or referral to a specialist.'
                    });
                });
            </script>
            <script>

                function PQRSMedicareValidation() {
                    var arrClickM12Q1 = new Array("PQRS12Q2", "PQRS12Q3", "PQRS12Q4", "PQRS12Q5", "PQRS12Q6", "PQRS141Q1", "PQRS141Q2", "PQRS141Q3");
                    var arrClickM12Q1_2 = new Array("#PQRS12Q2", "#PQRS12Q3", "#PQRS12Q4", "#PQRS12Q5", "#PQRS12Q6", "#PQRS141Q1", "#PQRS141Q2", "#PQRS141Q3");
                    var arrClickM12Q1_3 = new Array("PQRS12Q2");
                    var arrClickM12Q1_4 = new Array("#PQRS12Q2");


                    if ($('#<%= PQRSMedicare.ClientID %> input:checked').val() == "0") {
                        generate(arrClickM12Q1, arrClickM12Q1_2, true);
                    }
                    else {
                        generate(arrClickM12Q1_3, arrClickM12Q1_4, false);
                    }
                }

                function PQRSDiagnosisValidation() {
                    var arrClickM12Q2 = new Array("PQRS12Q3", "PQRS12Q4", "PQRS12Q5", "PQRS12Q6", "PQRS141Q1", "PQRS141Q2", "PQRS141Q3");
                    var arrClickM12Q2_2 = new Array("#PQRS12Q3", "#PQRS12Q4", "#PQRS12Q5", "#PQRS12Q6", "#PQRS141Q1", "#PQRS141Q2", "#PQRS141Q3");
                    var arrClickM12Q2_3 = new Array("PQRS12Q3");
                    var arrClickM12Q2_4 = new Array("#PQRS12Q3");

                    if ($('#<%= PQRSDiagnosis.ClientID %> input:checked').val() == "0") {
                        generate(arrClickM12Q2, arrClickM12Q2_2, true);
                    }
                    else {
                        generate(arrClickM12Q2_3, arrClickM12Q2_4, false);
                    }
                }

                function PQRSEncounterValidation() {
                    var $checkboxA = $("#<%= CheckBoxPQRSEncounterA.ClientID %>");
                        var $checkboxB = $("#<%= CheckBoxPQRSEncounterB.ClientID %>");

                        var arrM12 = new Array("PQRS12Q4", "PQRS12Q5", "PQRS12Q6");
                        var arrM12_2 = new Array("#PQRS12Q4", "#PQRS12Q5", "#PQRS12Q6");
                        var arrM12_3 = new Array("PQRS12Q4", "PQRS12Q5");
                        var arrM12_4 = new Array("#PQRS12Q4", "#PQRS12Q5");

                        var arrM141 = new Array("PQRS141Q1", "PQRS141Q2", "PQRS141Q3");
                        var arrM141_2 = new Array("#PQRS141Q1", "#PQRS141Q2", "#PQRS141Q3");
                        var arrM141_3 = new Array("PQRS141Q1");
                        var arrM141_4 = new Array("#PQRS141Q1");

                        if ($checkboxA.prop("checked") == true) {
                            generate(arrM12_3, arrM12_4, false);
                            generate(arrM141_3, arrM141_4, false);
                        } else if ($checkboxB.prop("checked") == true) {
                            generate(arrM12_3, arrM12_4, false);
                            generate(arrM141, arrM141_2, true);
                        } else {
                            generate(arrM12, arrM12_2, true);
                            generate(arrM141, arrM141_2, true);
                        }
                    }

                    function PQRSOpticNerveEvalValidation() {
                        var arrClickM12Q5 = new Array("PQRS12Q6");
                        var arrClickM12Q5_2 = new Array("#PQRS12Q6");

                        if ($('#<%= PQRSOpticNerveEval.ClientID %> input:checked').val() == "0") {
                            generate(arrClickM12Q5, arrClickM12Q5_2, false);
                        }
                        else {
                            generate(arrClickM12Q5, arrClickM12Q5_2, true);
                        }
                    }

                    function PQRSIOPDocumentedValidation() {
                        var arrClickM141Q1 = new Array("PQRS141Q2", "PQRS141Q3");
                        var arrClickM141Q1_2 = new Array("#PQRS141Q2", "#PQRS141Q3");

                        if ($('#<%= PQRSIOPDocumented.ClientID %> input:checked').val() == "0") {
                            generate(arrClickM141Q1, arrClickM141Q1_2, true);
                        }
                        else {
                            generate(arrClickM141Q1, arrClickM141Q1_2, false);
                        }
                    }

                    function PQRSIOPReducedValidation() {
                        var arrClickM141Q3 = new Array("PQRS141Q3");
                        var arrClickM141Q3_2 = new Array("#PQRS141Q3");

                        if ($('#<%= PQRSIOPReduced.ClientID %> input:checked').val() == "1") {
                            generate(arrClickM141Q3, arrClickM141Q3_2, true);
                        }
                        else {
                            generate(arrClickM141Q3, arrClickM141Q3_2, false);
                        }
                    }

                    $(document).ready(function () {

                        $("#<%= PQRSMedicare.ClientID %>").click(function () {
                            PQRSMedicareValidation();
                        });

                        $("#<%= PQRSDiagnosis.ClientID %>").click(function () {
                            PQRSDiagnosisValidation();
                        });

                        $("#<%= CheckBoxPQRSEncounterA.ClientID %>").click(function () {
                            PQRSEncounterValidation();
                        });
                        $("#<%= CheckBoxPQRSEncounterB.ClientID %>").click(function () {
                            PQRSEncounterValidation();
                        });

                        $("#<%= PQRSOpticNerveEval.ClientID %>").click(function () {
                            PQRSOpticNerveEvalValidation();
                        });

                        $("#<%= PQRSIOPDocumented.ClientID %>").click(function () {
                            PQRSIOPDocumentedValidation();
                        });

                        $("#<%= PQRSIOPReduced.ClientID %>").click(function () {
                            PQRSIOPReducedValidation();
                        });

                        PQRSIOPReducedValidation();
                        PQRSIOPDocumentedValidation();
                        PQRSOpticNerveEvalValidation();
                        PQRSEncounterValidation();
                        PQRSDiagnosisValidation();
                        PQRSMedicareValidation();

                        /* Check that user is inputting correct year */
                        var rightYearArr = new Array("PQRS12Q5", "PQRS12Q6", "PQRS141Q1", "PQRS141Q2", "PQRS141Q3");
                        var rightYearArr2 = new Array("#PQRS12Q5", "#PQRS12Q6", "#PQRS141Q1", "#PQRS141Q2", "#PQRS141Q3");
                        $("#<%= DropDownListPQRSVisitYear.ClientID %>").change(function () {
                            var year = $("#<%= DropDownListPQRSVisitYear.ClientID %>").val();

                            if (year != 2015 && year != 0) {
                                generate(rightYearArr, rightYearArr2, true);
                                alert("Date of procedure must be in 2015 to be valid for PQRS Reporting.");
                            } else {
                                generate(rightYearArr, rightYearArr2, false);
                            }
                        });
                    });
            </script>
        </asp:Panel>

        <!-- History -->
            <table>
                <tr>
                    <th colspan="2" style="width:910px;">
                        <p>History</p>
                    </th>
                </tr>
                <!-- Question 1 -->
                <tr class="table_body">
                    <td width="70%"><strong>1. Date of Birth</strong>
                    <br />
                    <asp:Label ID="LabelMonthOfBirth" runat="server" Visible="false"  ForeColor="Red" Text="Please enter Month  "></asp:Label><br />
                    <asp:Label ID="LabelYearOfBirth" runat="server" Visible="false"  ForeColor="Red"  Text="Please enter Year "></asp:Label>
                    </td>
                    <td>
                        <asp:DropDownList ID="DropDownListMonthOfBirth" DataValueField="MonthID" DataTextField="MonthName" runat="server" />
                        <asp:DropDownList ID="DropDownListYearOfBirth" DataValueField="YearID" DataTextField="YearName" runat="server" />
                    </td>
                </tr>
                <!-- Question 2 -->
                <tr class="table_body_bg">
                    <td><strong>2. Date of first exam</strong>
                     <br />
                    <asp:Label ID="LabelMonthOfExam" runat="server" Visible="false"  ForeColor="Red" Text="Please enter Month  "></asp:Label><br />
                    <asp:Label ID="LabelYearOfExam" runat="server" Visible="false"  ForeColor="Red"  Text="Please enter Year "></asp:Label>
                        <br />
                    <asp:Label ID="Labelinitialage" runat="server" Visible="false"  ForeColor="Red"  
                            Text=""></asp:Label>
                    </td>
                    <td>
                        <asp:DropDownList ID="DropDownListMonthOfExam" DataValueField="MonthID" DataTextField="MonthName" runat="server" />
                        <asp:DropDownList ID="DropDownListYearOfExam" DataValueField="YearID" DataTextField="YearName" runat="server" />
                    </td>
                </tr>
           
                <!-- Question 3 -->
                <tr class="table_body">
                    <td style="width:60%;">
                        <strong>3. History of glaucoma in a first degree relative?</strong>
                        
                    <asp:Label ID="LabelRBHistoryFirstDegreeRelative" runat="server" Visible="false"  ForeColor="Red" Text="<br />Please complete"></asp:Label>
                    </td>

                    <td>
                        <asp:RadioButtonList id="RadioButtonListRBHistoryFirstDegreeRelative" RepeatDirection="Vertical" CssClass="aspxList" runat="server">
	                        <asp:ListItem Value="1">&nbsp;Yes</asp:ListItem>
	                        <asp:ListItem Value="2">&nbsp;No</asp:ListItem>
	                        <asp:ListItem Value="3">&nbsp;Not Recorded</asp:ListItem>
                            <asp:ListItem Value="353">&nbsp;Unknown</asp:ListItem>
                        </asp:RadioButtonList>
                    </td>
                </tr>
                <!-- Question 2 -->
                <tr class="table_body_bg">
                    <td>
                        <strong>4. Was a medical history obtained?</strong>&nbsp;<a href="#"><img id="QH2" src="../../common/images/tip.gif" alt="Tip" /></a>
                        <br />
                        <asp:Label ID="LabelMedicalHistory" runat="server" Visible="false"  ForeColor="Red" Text="Please complete"></asp:Label>
                    </td>
                    <td>
                        <asp:RadioButton runat="server" ID="RadioButtonMedicalHistoryYes" GroupName="MedicalHistory" text="&nbsp;Yes" />
                        <asp:RadioButton runat="server" ID="RadioButtonMedicalHistoryNo" GroupName="MedicalHistory" text="&nbsp;No" />
                    </td>
                </tr>
                <!-- Question 3 -->
                <tr class="table_body">
                    <td>
                        <strong>5. Was an ocular history performed?</strong>&nbsp;<a href="#"><img id="QH3" src="../../common/images/tip.gif" alt="Tip" /></a>
                        <br />
                        <asp:Label ID="LabelOcularHistory" runat="server" Visible="false"  ForeColor="Red" Text="Please complete"></asp:Label>
                   
                    </td>
                    <td>
                        <asp:RadioButton runat="server" ID="RadioButtonOcularHistoryYes" GroupName="OcularHistory" text="&nbsp;Yes" />
                        <asp:RadioButton runat="server" ID="RadioButtonOcularHistoryNo" GroupName="OcularHistory" text="&nbsp;No" />
                    </td>
                </tr>
            </table>
            <br />
            <!-- Examination - 1st Visit -->
            <table width="910px">
                <!-- Header - Examination - 1st Visit -->
                <tr>
                    <th colspan="3"><p>Examination - 1<sup>st</sup> Visit</p></th>
                </tr>
                <!-- Question 1 -->
                <tr class="table_body">
                    <td rowspan="2" width="55%"><strong>1. Best-corrected visual acuity on initial examination</strong>&nbsp;<a href="#"><img id="QE2" src="../../common/images/tip.gif" alt="Tip" /></a>
                    <br />
                        <asp:Label ID="LabelBCVA" runat="server" Visible="false"  ForeColor="Red" Text="Please complete"></asp:Label>
                    </td>
                    <td width="15%"><span class="right">OD</span></td>
                    <td>
                        <label>20/</label>
                        <asp:DropDownList ID="DropDownListBCVAOD" DataValueField="examValue" DataTextField="examLabel" onchange="QE1ODtoggleRB();" runat="server" />
                        <asp:RadioButtonList id="RadioButtonListRBBCVAODNotAbletoPerform" RepeatDirection="Vertical" CssClass="aspxList" runat="server" onclick="QE1ODtoggleDD();">
                            <asp:ListItem Value="4">&nbsp;Unable to Perform</asp:ListItem>
                            <asp:ListItem Value="3">&nbsp;Not Recorded</asp:ListItem>
                        </asp:RadioButtonList>
                    </td>
                </tr>
                <tr class="table_body">
                    <td><span class="right">OS</span></td>
                    <td>
                        <label>20/</label>
                        <asp:DropDownList ID="DropDownListBCVAOS" DataValueField="examValue" DataTextField="examLabel" onchange="QE1OStoggleRB();" runat="server" />
                        <asp:RadioButtonList id="RadioButtonListRBBCVAOSNotAbletoPerform" RepeatDirection="Vertical" CssClass="aspxList" runat="server" onclick="QE1OStoggleDD();">
                            <asp:ListItem Value="4">&nbsp;Unable to Perform</asp:ListItem>
                            <asp:ListItem Value="3">&nbsp;Not Recorded</asp:ListItem>
                        </asp:RadioButtonList>
                    </td>
                </tr>
                <!-- Question 2 -->
                <tr class="table_body_bg">
                    <td colspan="2"><strong>2. Is an afferent pupillary defect present?</strong>
                        <br />
                        <asp:Label ID="LabelAfferentPupillaryDefect" runat="server" Visible="false"  ForeColor="Red" Text="Please complete"></asp:Label>
                    </td>
                    <td>
                        <asp:RadioButtonList id="RadioButtonListAfferentPupillaryDefect" RepeatDirection="Horizontal" CssClass="aspxList" runat="server">
	                        <asp:ListItem Value="1">&nbsp;Yes</asp:ListItem>
	                        <asp:ListItem Value="2">&nbsp;No</asp:ListItem>
	                        <asp:ListItem Value="3">&nbsp;Not Recorded</asp:ListItem>
                        </asp:RadioButtonList>
                    </td>
                </tr>
                <!-- Question 3 -->
                <tr class="table_body">
                    <td rowspan="2"><strong>3. What is IOP on initial visit?</strong>
                        <br />
                        <asp:Label ID="LabelIOP" runat="server" Visible="false"  ForeColor="Red" Text="Please complete"></asp:Label>
                    </td>
                    <td><span class="right">OD</span></td>
                    <td>
                        <asp:TextBox runat="server" ID="TextBoxIOPODmmHg" size="5" onkeyup="QE3ODtoggleRB();" /><label>&nbsp;mmHg</label>
                        <asp:RadioButtonList id="RadioButtonListRBIOPODNotAbletoPerform" RepeatDirection="Vertical" CssClass="aspxList" runat="server" onclick="QE3ODtoggleTB();">
                            <asp:ListItem Value="4">&nbsp;Unable to Perform</asp:ListItem>
                            <asp:ListItem Value="3">&nbsp;Not Recorded</asp:ListItem>
                        </asp:RadioButtonList>
                    </td>
                </tr>
                <tr class="table_body">
                    <td><span class="right">OS</span></td>
                    <td>
                        <asp:TextBox runat="server" ID="TextBoxIOPOSmmHg" size="5" onkeyup="QE3OStoggleRB();" /><label>&nbsp;mmHg</label>
                        <asp:RadioButtonList id="RadioButtonListRBIOPOSNotAbletoPerform" RepeatDirection="Vertical" CssClass="aspxList" runat="server" onclick="QE3OStoggleTB();" >
                            <asp:ListItem Value="4">&nbsp;Unable to Perform</asp:ListItem>
                            <asp:ListItem Value="3">&nbsp;Not Recorded</asp:ListItem>
                        </asp:RadioButtonList>
                    </td>
                </tr>
                <!-- Question 4 -->
                <tr class="table_body_bg">
                    <td rowspan="2"><strong>4. Was gonioscopy performed?</strong>
                         <br />
                        <asp:Label ID="LabelRBGonioscopy" runat="server" Visible="false"  ForeColor="Red" Text="Please complete"></asp:Label></td>
                    <td><span class="right">OD</span></td>
                    <td>
                        <asp:RadioButtonList id="RadioButtonListRBGonioscopyOD" RepeatDirection="Vertical" CssClass="aspxList" runat="server">
	                        <asp:ListItem Value="1">&nbsp;Yes</asp:ListItem>
	                        <asp:ListItem Value="2">&nbsp;No</asp:ListItem>
	                        <asp:ListItem Value="4">&nbsp;Unable to Perform</asp:ListItem>
                            <asp:ListItem Value="3">&nbsp;Not Recorded</asp:ListItem>
                        </asp:RadioButtonList>
                    </td>
                </tr>
                <tr class="table_body_bg">
                    <td><span class="right">OS</span></td>
                    <td>
                        <asp:RadioButtonList id="RadioButtonListRBGonioscopyOS" RepeatDirection="Vertical" CssClass="aspxList" runat="server">
	                        <asp:ListItem Value="1">&nbsp;Yes</asp:ListItem>
	                        <asp:ListItem Value="2">&nbsp;No</asp:ListItem>
	                        <asp:ListItem Value="4">&nbsp;Unable to Perform</asp:ListItem>
                            <asp:ListItem Value="3">&nbsp;Not Recorded</asp:ListItem>
                        </asp:RadioButtonList>
                    </td>
                </tr>
                <!-- Question 5 -->
                <tr class="table_body">
                    <td rowspan="2"><strong>5. Was the optic nerve appearance documented</strong>&nbsp;<a href="#"><img id="QE6" src="../../common/images/tip.gif" alt="Tip" /></a>
                        <br />
                        <asp:Label ID="LabelRBOpticNerveApperance" runat="server" Visible="false"  ForeColor="Red" Text="Please complete"></asp:Label>
                    </td>
                    <td><span class="right">OD</span></td>
                    <td>
                        <asp:RadioButtonList id="RadioButtonListRBOpticNerveApperanceOD" RepeatDirection="Horizontal" CssClass="aspxList" runat="server">
	                        <asp:ListItem Value="1" >&nbsp;Yes</asp:ListItem>
	                        <asp:ListItem Value="2" >&nbsp;No</asp:ListItem>
	                        <asp:ListItem Value="4" >&nbsp;Unable to Perform</asp:ListItem>
                        </asp:RadioButtonList>
                    </td>
                </tr>
                <tr class="table_body">
                    <td><span class="right">OS</span></td>
                    <td>
                        <asp:RadioButtonList id="RadioButtonListRBOpticNerveApperanceOS" RepeatDirection="Horizontal" CssClass="aspxList" runat="server">
	                        <asp:ListItem Value="1" >&nbsp;Yes</asp:ListItem>
	                        <asp:ListItem Value="2" >&nbsp;No</asp:ListItem>
	                        <asp:ListItem Value="4" >&nbsp;Unable to Perform</asp:ListItem>
                        </asp:RadioButtonList>
                    </td>
                </tr>
                <!-- Question 5 -->
                <tr class="table_body_bg"><span id="QE8txt">
                    <td rowspan="2"><strong>6. What is the central corneal thickness?</strong>
                        <br />
                        <asp:Label ID="LabelCentralCornealThickness" runat="server" Visible="false"  ForeColor="Red" Text="Please complete"></asp:Label>
                    </td>
                    <td><span class="right">OD</span></td>
                    <td>
                        <asp:TextBox runat="server" ID="TextBoxCentralCornealThicknessOD" size="5" onkeyup="QE6ODtoggleRB();" /><label>&nbsp;microns</label>
                        <br /><asp:RadioButtonList id="RadioButtonListCentralCornealNotAbletoPerformOD" RepeatDirection="Vertical" CssClass="aspxList" runat="server" onclick="QE6ODtoggleTB();">
	                        <asp:ListItem Value="4">&nbsp;Unable to perform</asp:ListItem>
	                        <asp:ListItem Value="3">&nbsp;Not Recorded</asp:ListItem>
                        </asp:RadioButtonList>
                    </td>
                </tr>
                <tr class="table_body_bg">
                    <td><span class="right">OS</span></td>
                    <td>
                        <asp:TextBox runat="server" ID="TextBoxCentralCornealThicknessOS" size="5" onkeyup="QE6OStoggleRB();" /><label>&nbsp;microns</label>
                        <br /><asp:RadioButtonList id="RadioButtonListRBCentralCornealNotAbletoPerformOS" RepeatDirection="Vertical" CssClass="aspxList" runat="server" onclick="QE6OStoggleTB();">
	                        <asp:ListItem Value="4">&nbsp;Unable to perform</asp:ListItem>
	                        <asp:ListItem Value="3">&nbsp;Not Recorded</asp:ListItem>
                        </asp:RadioButtonList>
                    </td></span>
                </tr>
                <!-- Question 7 -->
                <tr class="table_body">
                    <td colspan="2">
                        <strong>7. Were automated visual fields performed at the diagnosis<br /> visit or the immediate next visit?</strong>
                        <asp:Label ID="LabelAutomatedVisualFields" runat="server" Visible="false"  ForeColor="Red" Text="<br />Please complete"></asp:Label>
                    </td>
                    <td>
                        <asp:RadioButtonList id="RadioButtonListAutomatedVisualFields" RepeatDirection="Horizontal" CssClass="aspxList" runat="server">
	                        <asp:ListItem Value="1">&nbsp;Yes</asp:ListItem>
	                        <asp:ListItem Value="2">&nbsp;No</asp:ListItem>
	                        <asp:ListItem Value="3">&nbsp;Not documented</asp:ListItem>    
                        </asp:RadioButtonList>
                    </td>
                </tr>
                <!-- Question 7a -->
                <tr class="table_body_bg">
                    <td rowspan="4">
                        <span id="QE10txta"><strong>7a. If &quot;Yes,&quot; what is the mean deviation and PSD of the baseline visual fields?</strong></span>                        
                        <asp:Label ID="LabelVisualField" runat="server" Visible="false"  ForeColor="Red" Text="<br />Please complete"></asp:Label>
                    </td>
                    <td><span id="QE10txtb" class="right">OD MD</span></td>
                    <td>
                        <span id="QE10txtc">
                        <asp:RadioButton runat="server" ID="RadioButtonVisualFieldMeanDeviationSignODPlus" GroupName="VisualFieldMeanDeviationSignOD" text="+" />
                        /
                        <asp:RadioButton runat="server" ID="RadioButtonVisualFieldMeanDeviationSignODMinus" GroupName="VisualFieldMeanDeviationSignOD" text="-" />
                        &nbsp;<asp:TextBox runat="server" ID="TextBoxVisualFieldMeanDeviationValueOD" size="5" /><label>dB</label>
                        </span>
                    </td>
                </tr>
                <tr class="table_body_bg">
                    <td><span id="QE10txtd" class="right">PSD or LV</span></td>
                    <td>
                        <span id="QE10txte">
                        <asp:TextBox runat="server" ID="TextBoxVisualFieldPSDValueOD" size="5" onkeyup="QE7aODtoggleCB();" /><label>dB</label>
                        <br /><asp:CheckBox runat="server" ID="CheckBoxVisualFieldPSDODNA" text="&nbsp;Not Applicable" class="check" onclick="QE7aODtoggleTB();" />
                        </span>
                    </td>
                </tr>
                <tr class="table_body_bg">
                    <td><span id="QE10txtf" class="right">OS MD</span></td>
                    <td>
                        <span id="QE10txtg">
                        <asp:RadioButton runat="server" ID="RadioButtonVisualFieldMeanDeviationSignOSPlus" GroupName="VisualFieldMeanDeviationSignOS" text="+" />
                        /
                        <asp:RadioButton runat="server" ID="RadioButtonVisualFieldMeanDeviationSignOSMinus" GroupName="VisualFieldMeanDeviationSignOS" text="-" />
                        &nbsp;<asp:TextBox runat="server" ID="TextBoxVisualFieldMeanDeviationValueOS" size="5" /><label>dB</label>
                        </span>
                    </td>
                </tr>
                <tr class="table_body_bg">
                    <td><span id="QE10txth" class="right">PSD or LV</span></td>
                    <td>
                        <span id="QE10txti">
                        <asp:TextBox runat="server" ID="TextBoxVisualFieldPSDValueOS" size="5" onkeyup="QE7aOStoggleCB();"/><label>dB</label>
                        <br /><asp:CheckBox runat="server" ID="CheckBoxVisualFieldPSDOSNA" text="&nbsp;Not Applicable" class="check" onclick="QE7aOStoggleTB();"/>
                        </span>
                    </td>
                </tr>
            </table>
            <br />
            <!-- Assessment and Management -->
            <table width="910px">
                <!-- Header - Assessment and Management -->
                <tr>
                    <th colspan="3"><p>Assessment and Management</p></th>
                </tr>
                <!-- Question 1 -->
                <tr class="table_body">
                    <td rowspan="2" width="55%"><strong>1. Likelihood of Glaucoma at initial examination</strong>
                    <br />
                        <asp:Label ID="LabelLikelihoodGlaucoma" runat="server" Visible="false"  ForeColor="Red" Text="Please complete"></asp:Label>
                    </td>
                    <td width="15%"><span class="right">OD</span></td>
                    <td>
                        <asp:RadioButtonList id="RadioButtonListLikelihoodGlaucomaOD" RepeatDirection="Vertical" CssClass="aspxList" runat="server">
	                        <asp:ListItem Value="52">&nbsp;High</asp:ListItem>
	                        <asp:ListItem Value="53">&nbsp;Moderate</asp:ListItem>
	                        <asp:ListItem Value="54">&nbsp;Low</asp:ListItem>
                        </asp:RadioButtonList>
                    </td>
                </tr>
                <tr class="table_body">
                    <td><span class="right">OS</span></td>
                    <td>
                        <asp:RadioButtonList id="RadioButtonListLikelihoodGlaucomaOS" RepeatDirection="Vertical" CssClass="aspxList" runat="server">
	                        <asp:ListItem Value="52">&nbsp;High</asp:ListItem>
	                        <asp:ListItem Value="53">&nbsp;Moderate</asp:ListItem>
	                        <asp:ListItem Value="54">&nbsp;Low</asp:ListItem>
                        </asp:RadioButtonList>
                    </td>
                </tr>
                <!-- Question 2 -->
                <tr class="table_body_bg">
                    <td colspan="2"><strong>2. Was a management plan formulated?</strong>
                    <br />
                        <asp:Label ID="LabelManagementPlanFormulated" runat="server" Visible="false"  ForeColor="Red" Text="Please complete"></asp:Label>
                    </td>
                    <td>
                        <asp:RadioButton runat="server" ID="RadioButtonManagementPlanFormulatedYes" GroupName="ManagementPlanFormulated" text="&nbsp;Yes" />
                        <asp:RadioButton runat="server" ID="RadioButtonManagementPlanFormulatedNo" GroupName="ManagementPlanFormulated" text="&nbsp;No" />
                    </td>
                </tr>
                <!-- Question 3 -->
                <tr class="table_body">
                    <td colspan="2"><strong>3. Was a follow-up examination scheduled?</strong>
                    <br />
                        <asp:Label ID="LabelFollowUpExamScheduled" runat="server" Visible="false"  ForeColor="Red" Text="Please complete"></asp:Label>
                    </td>
                    <td>
                        <asp:RadioButton runat="server" ID="RadioButtonFollowUpExamScheduledYes" GroupName="FollowUpExamScheduled" text="&nbsp;Yes" onclick="activateQA4();" />
                        <asp:RadioButton runat="server" ID="RadioButtonFollowUpExamScheduledNo" GroupName="FollowUpExamScheduled" text="&nbsp;No" onclick="deActivateQA4();" />
                    </td>
                </tr>
                <!-- Question 4 -->
                <tr class="table_body_bg">
                    <td colspan="2"><span id="QA4txt"><strong>4. When is the next examination scheduled?</strong></span>
                    <br />
                        <asp:Label ID="LabelFollowUpExamScheduledMonth" runat="server" Visible="false"  ForeColor="Red" Text="Please complete"></asp:Label><br />
                        <asp:Label ID="LabelFollowUpExamScheduledYear" runat="server" Visible="false"  ForeColor="Red" Text="Please complete"></asp:Label>
                    
                    </td>
                    <td>
                        <asp:DropDownList ID="DropDownListFollowUpExamScheduledMonth" DataValueField="MonthID" DataTextField="MonthName" runat="server" />
                        <asp:DropDownList ID="DropDownListFollowUpExamScheduledYear" DataValueField="YearID" DataTextField="YearName" runat="server" />
                    </td>
                </tr>
            </table>
            <br />
            <!-- Follow-up / Outcomes -->
            <table width="910px">
                <!-- Header - Follow-up / Outcomes -->
                <tr>
                    <th colspan="3"><p>Follow-up / Outcomes</p></th>
                </tr>
                <!-- Question 1 -->
                <tr class="table_body">
                    <td colspan="2"><strong>1. Date of most recent exam</strong>
                    <br />
                        <asp:Label ID="LabelFollowUpExamMonth" runat="server" Visible="false"  ForeColor="Red" Text="Please complete"></asp:Label><br />
                        <asp:Label ID="LabelFollowUpExamYear" runat="server" Visible="false"  ForeColor="Red" Text="Please complete"></asp:Label>
                    
                    </td>
                    <td>
                        <asp:DropDownList ID="DropDownListFollowUpExamMonth" DataValueField="MonthID" DataTextField="MonthName" runat="server" />
                        <asp:DropDownList ID="DropDownListFollowUpExamYear" DataValueField="YearID" DataTextField="YearName" runat="server" />
                    </td>
                </tr>
                <!-- Question 2 -->
                <tr class="table_body_bg">
                    <td rowspan="2"><strong>2. Best-corrected visual acuity on most recent examination</strong>&nbsp;<a href="#"><img id="QF2" src="../../common/images/tip.gif" alt="Tip" /></a>
                    <br />
                        <asp:Label ID="LabelOutcomeBCVA" runat="server" Visible="false"  ForeColor="Red" Text="Please complete"></asp:Label>
                    </td>
                    <td><span class="right">OD</span></td>
                    <td>
                        <label>20/</label>
                        <asp:DropDownList ID="DropDownListOutcomeBCVAOD" DataValueField="examValue" DataTextField="examLabel" onchange="QF2ODtoggleRB();" runat="server" />
                        <asp:RadioButtonList id="RadioButtonListOutcomeRBBCVAODNotAbletoPerform" RepeatDirection="Vertical" CssClass="aspxList" runat="server" onclick="QF2ODtoggleDD();" >
                            <asp:ListItem Value="4">&nbsp;Unable to Perform</asp:ListItem>
                            <asp:ListItem Value="3">&nbsp;Not Recorded</asp:ListItem>
                        </asp:RadioButtonList>
                    </td>
                </tr>
                <tr class="table_body_bg">
                    <td><span class="right">OS</span></td>
                    <td>
                        <label>20/</label>
                        <asp:DropDownList ID="DropDownListOutcomeBCVAOS" DataValueField="examValue" DataTextField="examLabel" runat="server" onchange="QF2OStoggleRB();" />
                        <asp:RadioButtonList id="RadioButtonListOutcomeRBBCVAOSNotAbletoPerform" RepeatDirection="Vertical" CssClass="aspxList" runat="server" onclick="QF2OStoggleDD();">
                            <asp:ListItem Value="4">&nbsp;Unable to Perform</asp:ListItem>
                            <asp:ListItem Value="3">&nbsp;Not Recorded</asp:ListItem>
                        </asp:RadioButtonList>
                    </td>
                </tr>
                <!-- Question 3 -->
                <tr class="table_body">
                    <td width="55%" rowspan="2"><strong>3. What is the IOP at the most recent visit?</strong>
                    <br />
                        <asp:Label ID="LabelOutcomeIOP" runat="server" Visible="false"  ForeColor="Red" Text="Please complete"></asp:Label>
                    </td>
                    <td width="15%"><span class="right">OD</span></td>
                    <td>
                        <asp:TextBox runat="server" ID="TextBoxOutcomeIOPODmmHg" size="5" onkeyup="QF3ODtoggleRB();" />
                        <asp:RadioButtonList id="RadioButtonListRBOutcomeIOPODNotAbletoPerform" RepeatDirection="Vertical" CssClass="aspxList" runat="server" onclick="QF3ODtoggleTB();" >
                            <asp:ListItem Value="4">&nbsp;Unable to Perform</asp:ListItem>
                            <asp:ListItem Value="3">&nbsp;Not Recorded</asp:ListItem>
                        </asp:RadioButtonList>
                    </td>
                </tr>
                <tr class="table_body">
                    <td><span class="right">OS</span></td>
                    <td>
                        <asp:TextBox runat="server" ID="TextBoxOutcomeIOPOSmmHg" size="5" onkeyup="QF3OStoggleRB();" />
                        <asp:RadioButtonList id="RadioButtonListRBOutcomeIOPOSNotAbletoPerform" RepeatDirection="Vertical" CssClass="aspxList" runat="server" onclick="QF3OStoggleTB();" >
                            <asp:ListItem Value="4">&nbsp;Unable to Perform</asp:ListItem>
                            <asp:ListItem Value="3">&nbsp;Not Recorded</asp:ListItem>
                        </asp:RadioButtonList>
                    </td>
                </tr>
                <!-- Question 4 -->
                <tr class="table_body_bg">
                    <td rowspan="2"><strong>4. Date of most recent visual field?</strong>
                    <br />
                        <asp:Label ID="LabelOutcomeVisualFieldODMonth" runat="server" Visible="false"  ForeColor="Red" Text="Please complete"></asp:Label><br />
                        <asp:Label ID="LabelOutcomeVisualFieldODYear" runat="server" Visible="false"  ForeColor="Red" Text="Please complete"></asp:Label><br />
                        <asp:Label ID="LabelOutcomeVisualFieldOSMonth" runat="server" Visible="false"  ForeColor="Red" Text="Please complete"></asp:Label><br />
                        <asp:Label ID="LabelOutcomeVisualFieldOSYear" runat="server" Visible="false"  ForeColor="Red" Text="Please complete"></asp:Label>
                    
                    </td>
                    <td><span class="right">OD</span></td>
                    <td>
                        <asp:DropDownList ID="DropDownListOutcomeVisualFieldODMonth" DataValueField="MonthID" DataTextField="MonthName" runat="server" />
                        <asp:DropDownList ID="DropDownListOutcomVisualFieldODYear" DataValueField="YearID" DataTextField="YearName" runat="server" />    
                    </td>
                </tr>
                <tr class="table_body_bg">
                    <td><span class="right">OS</span></td>
                    <td>
                        <asp:DropDownList ID="DropDownListOutcomeVisualFieldOSMonth" DataValueField="MonthID" DataTextField="MonthName" runat="server" />
                        <asp:DropDownList ID="DropDownListOutcomeVisualFieldOSYear" DataValueField="YearID" DataTextField="YearName" runat="server" />
                    </td>
                </tr>
                <!-- Question 5 -->
                <tr class="table_body">
                    <td rowspan="4"><strong>5. What is the pattern standard and mean deviation and PSD of the most recent visual field?</strong>
                    <br />
                        <asp:Label ID="LabelOutcomeVisualField" runat="server" Visible="false"  ForeColor="Red" Text="Please complete"></asp:Label>
                    </td>
                    <td><span class="right">OD MD</span></td>
                    <td>
                        <asp:RadioButton runat="server" ID="RadioButtonOutcomeVisualFieldMeanDeviationSignODPlus" GroupName="OutcomeVisualFieldMeanDeviationSignOD" text="+" />
                        /
                        <asp:RadioButton runat="server" ID="RadioButtonOutcomeVisualFieldMeanDeviationSignODMinus" GroupName="OutcomeVisualFieldMeanDeviationSignOD" text="-" />
                        &nbsp;<asp:TextBox runat="server" ID="TextBoxOutcomeVisualFieldMeanDeviationValueOD" size="5" /><label>dB</label>
                    </td>
                </tr>
                <tr class="table_body">
                    <td><span class="right">PSD or LV</span></td>
                    <td>
                        <asp:TextBox runat="server" ID="TextBoxOutcomeVisualFieldPSDValueOD" size="5" onkeyup="QF5ODtoggleCB();" /><label>dB</label>
                        <br /><asp:CheckBox runat="server" ID="CheckBoxOutcomeVisualFieldPSDODNA" text="&nbsp;Not Applicable" class="check" onclick="QF5ODtoggleTB();" />
                    </td>
                </tr>
                <tr class="table_body">
                    <td><span class="right">OS MD</span></td>
                    <td>
                        <asp:RadioButton runat="server" ID="RadioButtonOutcomeVisualFieldMeanDeviationSignOSPlus" GroupName="OutcomeVisualFieldMeanDeviationSignOS" text="+" />
                        /
                        <asp:RadioButton runat="server" ID="RadioButtonOutcomeVisualFieldMeanDeviationSignOSMinus" GroupName="OutcomeVisualFieldMeanDeviationSignOS" text="-" />
                        &nbsp;<asp:TextBox runat="server" ID="TextBoxOutcomeVisualFieldMeanDeviationValueOS" size="5" /><label>dB</label>
                    </td>
                </tr>
                <tr class="table_body">
                    <td><span class="right">PSD or LV</span></td>
                    <td>
                        <asp:TextBox runat="server" ID="TextBoxOutcomeVisualFieldPSDValueOS" size="5" onkeyup="QF5OStoggleCB();" /><label>dB</label>
                        <br /><asp:CheckBox runat="server" ID="CheckBoxOutcomeVisualFieldPSDOSNA" text="&nbsp;Not Applicable" class="check" onclick="QF5OStoggleTB();"/>
                    </td>
                </tr>
                <!-- Question 6 -->
                <tr class="table_body_bg">
                    <td rowspan="2"><strong>6. Date of most recent optic nerve head exam?</strong>
                    <br />
                        <asp:Label ID="LabelOutcomeOpticNerveExamODMonth" runat="server" Visible="false"  ForeColor="Red" Text="Please complete"></asp:Label><br />
                        <asp:Label ID="LabelOutcomeOpticNerveExamODYear" runat="server" Visible="false"  ForeColor="Red" Text="Please complete"></asp:Label><br />
                        <asp:Label ID="LabelOutcomeOpticNerveExamOSMonth" runat="server" Visible="false"  ForeColor="Red" Text="Please complete"></asp:Label><br />
                        <asp:Label ID="LabelOutcomeOpticNerveExamOSYear" runat="server" Visible="false"  ForeColor="Red" Text="Please complete"></asp:Label>
                    </td>
                    <td><span class="right">OD</span></td>
                    <td>
                        <asp:DropDownList ID="DropDownListOutcomeOpticNerveExamODMonth" DataValueField="MonthID" DataTextField="MonthName" runat="server" />
                        <asp:DropDownList ID="DropDownListOutcomeOpticNerveExamODYear" DataValueField="YearID" DataTextField="YearName" runat="server" />
                    </td>
                </tr>
                <tr class="table_body_bg">
                    <td><span class="right">OS</span></td>
                    <td>
                        <asp:DropDownList ID="DropDownListOutcomeOpticNerveExamOSMonth" DataValueField="MonthID" DataTextField="MonthName" runat="server" />
                        <asp:DropDownList ID="DropDownListOutcomeOpticNerveExamOSYear" DataValueField="YearID" DataTextField="YearName" runat="server" />
                    </td>
                </tr>
                <!-- Question 7 -->
                <tr class="table_body">
                    <td colspan="2"><strong>7. Has the appearance changed from baseline?</strong>
                    <br />
                        <asp:Label ID="LabelRBOpticNerveAppearanceChangeFromBaseline" runat="server" Visible="false"  ForeColor="Red" Text="Please complete"></asp:Label>
                    </td>
                    <td>
                        <asp:RadioButtonList id="RadioButtonListRBOpticNerveAppearanceChangeFromBaseline" RepeatDirection="Vertical" CssClass="aspxList" runat="server">
	                        <asp:ListItem Value="40">&nbsp;Better</asp:ListItem>
	                        <asp:ListItem Value="41">&nbsp;Worse</asp:ListItem>
                            <asp:ListItem Value="51">&nbsp;No Change</asp:ListItem>
	                        <asp:ListItem Value="4">&nbsp;Not Recorded</asp:ListItem>
                        </asp:RadioButtonList>
                    </td>
                </tr>
            </table>
            <div class="button-box">
                <asp:LinkButton ID="LinkButtonBackToDashboard" runat="server" Text="Back To Chart Registration" PostBackUrl="../PatientChartRegistration.aspx?CycleNumber=1" Visible="false" CssClass="button" />
                <asp:Button ID="ButtonSubmit" OnClientClick="return ButtonSubmit_PQRS();" OnClick="ButtonSubmit_Click" runat="server" Text="Submit Chart" CssClass="button" />
            </div>
        </div>
        <!-- ION pim ends -->
</asp:Content>

<asp:Content runat="server" ID="Content4" ContentPlaceHolderID="javascript">

    <script type="text/javascript" src="../../common/js/jquery.atooltip.js"></script>
    <script type="text/javascript" src="../../common/js/jquery.metadata.js"></script>
    <!--when changing defaults-->
    <script type="text/javascript" src="../../common/js/autoNumeric-1.7.5.js"></script>

    <script type="text/javascript">
        $(document).ready(function () {
            /** instruct the metadata plugin where to look the metadata
            * jQuery.metadata.setType( type, name );
            * please read the metadata instructions for additional information
            * http://plugins.jquery.com/project/metadata
            */
            $.metadata.setType('attr', 'meta');

            /** To call autoNumeric
            * $(selector).autoNumeric({options}); 
            * The below example uses the input & class selector
            */
            $('input.auto').autoNumeric();
            /* scripts for metadata code generator  */

            /* rountine that prevents  numeric characters from being entered the the altDec field  */
            $('#altDecb').keypress(function (e) {
                var cc = String.fromCharCode(e.which);
                if (e.which != 32 && cc >= 0 && cc <= 9) {
                    e.preventDefault();
                }
            });

            /* rountine that prevents  apostrophe, comma, more than one period (full stop) or numeric characters from being entered the the aSign field  */
            $('#aSignb').keypress(function (e) {
                var cc = String.fromCharCode(e.which);
                if ((e.which != 32 && cc >= 0 && cc <= 9) || cc == "," || cc == "'" || cc == "." && this.value.lastIndexOf('.') != -1) {
                    e.preventDefault();
                }
            });

            $("input.md").bind('click keyup blur', function () {
                var metaCode = '', aSep = '', dGroup = '', aDec = '', altDec = '', aSign = '', pSign = '', vMin = '', vMax = '', mDec = '', mRound = '', aPad = '', wEmpty = '', aForm = '';
                if ($("input:radio[name=aSep]:checked").attr('id') == 'aSepc') {
                    $('input:radio[name=aDec]:nth(0)').removeAttr("disabled");
                    $('input:radio[name=aDec]:nth(0)').attr('checked', true);
                    $('input:radio[name=aDec]:nth(1)').attr("disabled", true);
                }
                if ($("input:radio[name=aSep]:checked").attr('id') == 'aSepp') {
                    $('input:radio[name=aDec]:nth(1)').removeAttr("disabled");
                    $('input:radio[name=aDec]:nth(1)').attr('checked', true);
                    $('input:radio[name=aDec]:nth(0)').attr("disabled", true);
                }
                if ($("input:radio[name=aSep]:checked").attr('id') != 'aSepc' || $("input:radio[name=aSep]:checked").attr('id') != 'aSepp') {
                    $('input:radio[name=aDec]:nth(0)').removeAttr("disabled");
                    $('input:radio[name=aDec]:nth(1)').removeAttr("disabled");
                }
                aSep = $("input:radio[name=aSep]:checked").val();
                dGroup = $("input:radio[name=dGroup]:checked").val();
                aDec = $("input:radio[name=aDec]:checked").val();

                if ($("input:radio[name=altDec]:checked").attr('id') == 'altDecd') {
                    $('#altDecb').val('');
                    $('#altDecb').attr("disabled", true);
                }
                if ($("input:radio[name=altDec]:checked").attr('id') == 'altDeca') {
                    $('#altDecb').removeAttr("disabled");
                    altDec = $('#altDecb').val();
                }

                if ($("input:radio[name=aSign]:checked").attr('id') == 'aSignd') {
                    $('#aSignb').val('');
                    $('#aSignb').attr("disabled", true);
                }
                if ($("input:radio[name=aSign]:checked").attr('id') == 'aSigna') {
                    $('#aSignb').removeAttr("disabled");
                    aSign = $('#aSignb').val();
                }

                pSign = $("input:radio[name=pSign]:checked").val();
                if ($("input:radio[name=vMin]:checked").attr('id') == 'vMind') {
                    $('#vMinb').val('');
                    $('#vMinb').attr("disabled", true);
                }
                if ($("input:radio[name=vMin]:checked").attr('id') == 'vMina') {
                    $('#vMinb').removeAttr("disabled");
                    vMin = $('#vMinb').val();
                }
                if ($("input:radio[name=vMax]:checked").attr('id') == 'vMaxd') {
                    $('#vMaxb').val('');
                    $('#vMaxb').attr("disabled", true);
                }
                if ($("input:radio[name=vMax]:checked").attr('id') == 'vMaxa') {
                    $('#vMaxb').removeAttr("disabled");
                    vMax = $('#vMaxb').val();
                }
                if ($("input:radio[name=mDec]:checked").attr('id') == 'mDecd') {
                    $('#mDecbb').val('');
                    $('#mDecbb').attr("disabled", true);
                }
                if ($("input:radio[name=mDec]:checked").attr('id') == 'mDeca') {
                    $('#mDecbb').removeAttr("disabled");
                    mDec = $('#mDecbb').val();
                }
                mRound = $("input:radio[name=mRound]:checked").val();
                aPad = $("input:radio[name=aPad]:checked").val();
                wEmpty = $("input:radio[name=wEmpty]:checked").val();
                if (aSep != '') {
                    metaCode = aSep;
                }
                if (dGroup != '') {
                    if (metaCode != '') {
                        metaCode = metaCode + ", " + dGroup;
                    }
                    else {
                        metaCode = dGroup;
                    }
                }
                if (aDec != '') {
                    if (metaCode != '') {
                        metaCode = metaCode + ", " + aDec;
                    }
                    else {
                        metaCode = aDec;
                    }
                }
                if (altDec != '') {
                    if (metaCode != '') {
                        metaCode = metaCode + ", altDec: '" + altDec + "'";
                    }
                    else {
                        metaCode = "altDec: '" + altDec + "'";
                    }
                }
                if (aSign != '') {
                    if (metaCode != '') {
                        metaCode = metaCode + ", aSign: '" + aSign + "'";
                    }
                    else {
                        metaCode = "aSign: '" + aSign + "'";
                    }
                }
                if (pSign != '') {
                    if (metaCode != '') {
                        metaCode = metaCode + ", " + pSign;
                    }
                    else {
                        metaCode = pSign;
                    }
                }
                if (vMin != '') {
                    if (metaCode != '') {
                        metaCode = metaCode + ", vMin: '" + vMin + "'";
                    }
                    else {
                        metaCode = "vMin: '" + vMin + "'";
                    }
                }
                if (vMax != '') {
                    if (metaCode != '') {
                        metaCode = metaCode + ", vMax: '" + vMax + "'";
                    }
                    else {
                        metaCode = "vMax: '" + vMax + "'";
                    }
                }
                if (mDec != '') {
                    if (metaCode != '') {
                        metaCode = metaCode + ", mDec: '" + $('#mDecbb').val() + "'";
                    }
                    else {
                        metaCode = "mDec: '" + $('#mDecbb').val() + "'";
                    }
                }

                if (mRound != '') {
                    if (metaCode != '') {
                        metaCode = metaCode + ", " + mRound;
                    }
                    else {
                        metaCode = mRound;
                    }
                }
                if (aPad != '') {
                    if (metaCode != '') {
                        metaCode = metaCode + ", " + aPad;
                    }
                    else {
                        metaCode = aPad;
                    }
                }
                if (wEmpty != '') {
                    if (metaCode != '') {
                        metaCode = metaCode + ", " + wEmpty;
                    }
                    else {
                        metaCode = wEmpty;
                    }
                }
                $('#metaCode').text('');
                if (metaCode != '') {
                    $('#metaCode').text('meta="{' + metaCode + '}"');
                }
            });

            /* clears the metadata code  */
            $('#rd').click(function () {
                $('#metaCode').text('');
            });
            /* ends scripts for metadata code generator  */

            /* script  for defaults demo  */
            $('#d_noMeta').blur(function () {
                var convertInput = '';
                convertInput = $(this).autoNumericGet();
                $('#d_Get').val(convertInput);
                $('#d_Set').autoNumericSet(convertInput);
            });
            /* end script  for defaults demo  */

            /* script  for various samples demo  */
            $('input[name$="sample"]').blur(function () {
                var convertInput = '';
                var row = 'row_' + this.id.charAt(4);
                convertInput = $(this).autoNumericGet();
                $('#' + row + 'b').val(convertInput);
                $('#' + row + 'c').autoNumericSet(convertInput);
            });
            /* end script  for various samples demo  */

            /* script  for rounding methods  */
            $('#roundValue').blur(function () {
                if (this.value != '') {
                    convertInput = $('#roundValue').autoNumericGet();
                    var i = 1;
                    for (i = 1; i <= 9; i++) {
                        $('#roundMethod' + i).autoNumericSet(convertInput);
                    }
                }
            });

            $('#roundDecimal').change(function () { /* changes decimal places */
                convertInput = $('#roundValue').autoNumericGet();
                if (convertInput > 0) {
                    var i = 1;
                    for (i = 1; i <= 9; i++) {
                        $('#roundMethod' + i).autoNumericSet(convertInput);
                    }
                }
            });
            /* end script  for rounding methods  */

            /* script for dynamically loaded values  demo*/
            $.getJSON("test_JSON.php", function (data) {
                var valueFormatted = '';
                $.each(data, function (key, value) { // loops through JSON keys and returns value	
                    $('#' + key).autoNumericSet(value);
                });
            });
            /* end script for dynamically loaded values demo*/

            /* script for callback demo*/
            $.autoNumeric.get_mDec = function () { /* get_mDec function attached to autoNumeric() */
                var set_mDec = $('#get_metricUnit').val();
                if (set_mDec == ' km') {
                    set_mDec = 3;
                } else {
                    set_mDec = 0;
                }
                return set_mDec; /* set mDec decimal places */
            }

            var get_vMax = function () { /* set the maximum value allowed based on the metric unit */
                var set_vMax = $('#get_metricUnit').val();
                if (set_vMax == ' km') {
                    set_vMax = '99999.999';
                } else {
                    set_vMax = '99999999';
                }
                return set_vMax;
            }

            $('#length').autoNumeric({ vMax: get_vMax }); /* calls autoNumeric and passes function get_vMax */

            $('#get_metricUnit').change(function () {
                var set_value = $('#length').autoNumericGet();
                if (this.value == ' km') {
                    set_value = set_value / 1000;
                } else {
                    set_value = set_value * 1000;
                }
                $('#length').autoNumericSet(set_value);
            });
            /* end script for callback demo*/
        });
    </script>
    <script type="text/javascript" language="javascript">
        //      Either enables or disables input boxes
        function enabledControl(el, disabled, checked) {
            //alert("Hello world from enabled control");
            try {
                if (disabled) {
                    el.disabled = disabled;
                    el.setAttribute("disabled", true);
                }
                else {
                    el.disabled = disabled;
                    el.removeAttribute("disabled");
                }

                if (checked == true)
                    el.checked = false;
            }
            catch (E) {
            }
            if (el.childNodes && el.childNodes.length > 0) {
                for (var x = 0; x < el.childNodes.length; x++) {
                    enabledControl(el.childNodes[x], disabled, checked);
                }
            }
        }
        //      Either enables or disables dropdown boxes
        function enabledControlDropDown(el, disabled, checked) {
            //alert("Hello world from enabled control");
            try {
                if (disabled) {
                    el.disabled = disabled;
                    el.setAttribute("disabled", true);
                }
                else {
                    el.disabled = disabled;
                    el.removeAttribute("disabled");
                }

                if (checked == true)
                    el.options[0].selected = true;
            }
            catch (E) {
            }
            if (el.childNodes && el.childNodes.length > 0) {
                for (var x = 0; x < el.childNodes.length; x++) {
                    enabledControl(el.childNodes[x], disabled, checked);
                }
            }
        }
        //      Either enables or disables text boxes
        function enabledControlTextField(el, disabled, checked) {
            //alert("Hello world from enabled control");
            try {
                if (disabled) {
                    el.disabled = disabled;
                    el.setAttribute("disabled", true);
                }
                else {
                    el.disabled = disabled;
                    el.removeAttribute("disabled");
                }

                if (checked == true) {
                    el.value = '';
                }
            }
            catch (E) {
            }
            if (el.childNodes && el.childNodes.length > 0) {
                for (var x = 0; x < el.childNodes.length; x++) {
                    enabledControl(el.childNodes[x], disabled, checked);
                }
            }
        }

        function activateQE10() {
            enabledControl(document.getElementById("<%= RadioButtonVisualFieldMeanDeviationSignODPlus.ClientID %>"), false, true);
            enabledControl(document.getElementById("<%= RadioButtonVisualFieldMeanDeviationSignODMinus.ClientID %>"), false, true);
            enabledControl(document.getElementById("<%= TextBoxVisualFieldMeanDeviationValueOD.ClientID %>"), false, true);
            enabledControl(document.getElementById("<%= TextBoxVisualFieldPSDValueOD.ClientID %>"), false, true);
            enabledControl(document.getElementById("<%= CheckBoxVisualFieldPSDODNA.ClientID %>"), false, true);
            enabledControl(document.getElementById("<%= RadioButtonVisualFieldMeanDeviationSignOSPlus.ClientID %>"), false, true);
            enabledControl(document.getElementById("<%= RadioButtonVisualFieldMeanDeviationSignOSMinus.ClientID %>"), false, true);
            enabledControl(document.getElementById("<%= TextBoxVisualFieldMeanDeviationValueOS.ClientID %>"), false, true);
            enabledControl(document.getElementById("<%= TextBoxVisualFieldPSDValueOS.ClientID %>"), false, true);
            enabledControl(document.getElementById("<%= CheckBoxVisualFieldPSDOSNA.ClientID %>"), false, true);
            document.getElementById("QE10txta").style.color = "#333";
            document.getElementById("QE10txtb").style.color = "#333";
            document.getElementById("QE10txtc").style.color = "#333";
            document.getElementById("QE10txtd").style.color = "#333";
            document.getElementById("QE10txte").style.color = "#333";
            document.getElementById("QE10txtf").style.color = "#333";
            document.getElementById("QE10txtg").style.color = "#333";
            document.getElementById("QE10txth").style.color = "#333";
            document.getElementById("QE10txti").style.color = "#333";
        }

        function deActivateQE10() {
            enabledControl(document.getElementById("<%= RadioButtonVisualFieldMeanDeviationSignODPlus.ClientID %>"), true, false);
            enabledControl(document.getElementById("<%= RadioButtonVisualFieldMeanDeviationSignODMinus.ClientID %>"), true, false);
            enabledControl(document.getElementById("<%= TextBoxVisualFieldMeanDeviationValueOD.ClientID %>"), true, false);
            enabledControl(document.getElementById("<%= TextBoxVisualFieldPSDValueOD.ClientID %>"), true, false);
            enabledControl(document.getElementById("<%= CheckBoxVisualFieldPSDODNA.ClientID %>"), true, false);
            enabledControl(document.getElementById("<%= RadioButtonVisualFieldMeanDeviationSignOSPlus.ClientID %>"), true, false);
            enabledControl(document.getElementById("<%= RadioButtonVisualFieldMeanDeviationSignOSMinus.ClientID %>"), true, false);
            enabledControl(document.getElementById("<%= TextBoxVisualFieldMeanDeviationValueOS.ClientID %>"), true, false);
            enabledControl(document.getElementById("<%= TextBoxVisualFieldPSDValueOS.ClientID %>"), true, false);
            enabledControl(document.getElementById("<%= CheckBoxVisualFieldPSDOSNA.ClientID %>"), true, false);
            document.getElementById("QE10txta").style.color = "gray";
            document.getElementById("QE10txtb").style.color = "gray";
            document.getElementById("QE10txtc").style.color = "gray";
            document.getElementById("QE10txtd").style.color = "gray";
            document.getElementById("QE10txte").style.color = "gray";
            document.getElementById("QE10txtf").style.color = "gray";
            document.getElementById("QE10txtg").style.color = "gray";
            document.getElementById("QE10txth").style.color = "gray";
            document.getElementById("QE10txti").style.color = "gray";
        }

        function activateQA4() {
            enabledControlDropDown(document.getElementById("<%= DropDownListFollowUpExamScheduledMonth.ClientID %>"), false, true);
            enabledControlDropDown(document.getElementById("<%= DropDownListFollowUpExamScheduledYear.ClientID %>"), false, true);
            document.getElementById("QA4txt").style.color = "#333";
        }

        function deActivateQA4() {
            enabledControlDropDown(document.getElementById("<%= DropDownListFollowUpExamScheduledMonth.ClientID %>"), true, false);
            enabledControlDropDown(document.getElementById("<%= DropDownListFollowUpExamScheduledYear.ClientID %>"), true, false);
            document.getElementById("QA4txt").style.color = "gray";
        }

        // QE1OD toggle controls
        function QE1ODtoggleDD() {
            enabledControlDropDown(document.getElementById("<%= DropDownListBCVAOD.ClientID %>"), false, true);
        }

        function QE1ODtoggleRB() {
            enabledControl(document.getElementById("<%= RadioButtonListRBBCVAODNotAbletoPerform.ClientID %>"), false, true);
        }

        // QE1OS toggle controls
        function QE1OStoggleDD() {
            enabledControlDropDown(document.getElementById("<%= DropDownListBCVAOS.ClientID %>"), false, true);
        }

        function QE1OStoggleRB() {
            enabledControl(document.getElementById("<%= RadioButtonListRBBCVAOSNotAbletoPerform.ClientID %>"), false, true);
        }

        // QE3OD toggle controls
        function QE3ODtoggleTB() {
            enabledControlTextField(document.getElementById("<%= TextBoxIOPODmmHg.ClientID %>"), false, true);
        }

        function QE3ODtoggleRB() {
            enabledControl(document.getElementById("<%= RadioButtonListRBIOPODNotAbletoPerform.ClientID %>"), false, true);
        }

        // QE3OS toggle controls
        function QE3OStoggleTB() {
            enabledControlTextField(document.getElementById("<%= TextBoxIOPOSmmHg.ClientID %>"), false, true);
        }

        function QE3OStoggleRB() {
            enabledControl(document.getElementById("<%= RadioButtonListRBIOPOSNotAbletoPerform.ClientID %>"), false, true);
        }

        // QE6OD toggle controls
        function QE6ODtoggleTB() {
            enabledControlTextField(document.getElementById("<%= TextBoxCentralCornealThicknessOD.ClientID %>"), false, true);
        }

        function QE6ODtoggleRB() {
            enabledControl(document.getElementById("<%= RadioButtonListCentralCornealNotAbletoPerformOD.ClientID %>"), false, true);
        }

        // QE6OS toggle controls
        function QE6OStoggleTB() {
            enabledControlTextField(document.getElementById("<%= TextBoxCentralCornealThicknessOS.ClientID %>"), false, true);
        }

        function QE6OStoggleRB() {
            enabledControl(document.getElementById("<%= RadioButtonListRBCentralCornealNotAbletoPerformOS.ClientID %>"), false, true);
        }

        // QE7aOD toggle controls
        function QE7aODtoggleTB() {
            enabledControlTextField(document.getElementById("<%= TextBoxVisualFieldPSDValueOD.ClientID %>"), false, true);
        }

        function QE7aODtoggleCB() {
            enabledControl(document.getElementById("<%= CheckBoxVisualFieldPSDODNA.ClientID %>"), false, true);
        }

        // QE7aOS toggle controls
        function QE7aOStoggleTB() {
            enabledControlTextField(document.getElementById("<%= TextBoxVisualFieldPSDValueOS.ClientID %>"), false, true);
        }

        function QE7aOStoggleCB() {
            enabledControl(document.getElementById("<%= CheckBoxVisualFieldPSDOSNA.ClientID %>"), false, true);
        }

        // QF2OD toggle controls
        function QF2ODtoggleDD() {
            enabledControlDropDown(document.getElementById("<%= DropDownListOutcomeBCVAOD.ClientID %>"), false, true);
        }

        function QF2ODtoggleRB() {
            enabledControl(document.getElementById("<%= RadioButtonListOutcomeRBBCVAODNotAbletoPerform.ClientID %>"), false, true);
        }

        // QF2OS toggle controls
        function QF2OStoggleDD() {
            enabledControlDropDown(document.getElementById("<%= DropDownListOutcomeBCVAOS.ClientID %>"), false, true);
        }

        function QF2OStoggleRB() {
            enabledControl(document.getElementById("<%= RadioButtonListOutcomeRBBCVAOSNotAbletoPerform.ClientID %>"), false, true);
        }

        // QF3OD toggle controls
        function QF3ODtoggleTB() {
            enabledControlTextField(document.getElementById("<%= TextBoxOutcomeIOPODmmHg.ClientID %>"), false, true);
        }

        function QF3ODtoggleRB() {
            enabledControl(document.getElementById("<%= RadioButtonListRBOutcomeIOPODNotAbletoPerform.ClientID %>"), false, true);
        }

        // QF3OS toggle controls
        function QF3OStoggleTB() {
            enabledControlTextField(document.getElementById("<%= TextBoxOutcomeIOPOSmmHg.ClientID %>"), false, true);
        }

        function QF3OStoggleRB() {
            enabledControl(document.getElementById("<%= RadioButtonListRBOutcomeIOPOSNotAbletoPerform.ClientID %>"), false, true);
        }

        // QF5OD toggle controls
        function QF5ODtoggleTB() {
            enabledControlTextField(document.getElementById("<%= TextBoxOutcomeVisualFieldPSDValueOD.ClientID %>"), false, true);
        }

        function QF5ODtoggleCB() {
            enabledControl(document.getElementById("<%= CheckBoxOutcomeVisualFieldPSDODNA.ClientID %>"), false, true);
        }

        // QF5OS toggle controls
        function QF5OStoggleTB() {
            enabledControlTextField(document.getElementById("<%= TextBoxOutcomeVisualFieldPSDValueOS.ClientID %>"), false, true);
        }

        function QF5OStoggleCB() {
            enabledControl(document.getElementById("<%= CheckBoxOutcomeVisualFieldPSDOSNA.ClientID %>"), false, true);
        }

        window.onload = function onLoadForm() {

            if (document.getElementById("<%= RadioButtonFollowUpExamScheduledYes.ClientID %>").checked != true) {
                deActivateQA4();
            }
        }

        function generate(arr1, arr2, istrue) {
            if (istrue == true) {
                for (var i = 0; i < arr1.length; i++) {
                    document.getElementById(arr1[i]).style.color = "gray";
                }
                for (var i = 0; i < arr2.length; i++) {
                    $(arr2[i] + ' :input').attr('disabled', true);
                    $(arr2[i] + ' :input').removeAttr("checked");
                }
            }
            if (istrue == false) {
                for (var i = 0; i < arr1.length; i++) {
                    document.getElementById(arr1[i]).style.color = "#333";
                }
                for (var i = 0; i < arr2.length; i++) {
                    $(arr2[i] + ' :input').removeAttr('disabled');
                }
            }
        }

        $(document).ready(function () {

            // Question 1a other - Pre-operative management ***************************
            $("#<%= RadioButtonListAutomatedVisualFields.ClientID %>").click(function () {
                if ($('#<%= RadioButtonListAutomatedVisualFields.ClientID %>').find('input:checked').val() == ('1')) {
                    var myaray = new Array("QE10txta", "QE10txtb", "QE10txtc", "QE10txtd", "QE10txte", "QE10txtf", "QE10txtg", "QE10txth", "QE10txti");
                    var myarray2 = new Array('#QE10txtc', '#QE10txtg', '#QE10txti');
                    generate(myaray, myarray2, false);

                    enabledControlTextField(document.getElementById("<%= TextBoxVisualFieldMeanDeviationValueOD.ClientID %>"), false, false);
                    enabledControlTextField(document.getElementById("<%= TextBoxVisualFieldMeanDeviationValueOS.ClientID %>"), false, false);
                    enabledControlTextField(document.getElementById("<%= TextBoxVisualFieldPSDValueOS.ClientID %>"), false, false);
                    enabledControlTextField(document.getElementById("<%= TextBoxVisualFieldPSDValueOD.ClientID %>"), false, false);
                }
                else {
                    var myaray = new Array("QE10txta", "QE10txtb", "QE10txtc", "QE10txtd", "QE10txte", "QE10txtf", "QE10txtg", "QE10txth", "QE10txti");
                    var myarray2 = new Array('#QE10txtc', '#QE10txtg', '#QE10txti');
                    generate(myaray, myarray2, true);

                    enabledControlTextField(document.getElementById("<%= TextBoxVisualFieldMeanDeviationValueOD.ClientID %>"), true, true);
                    enabledControlTextField(document.getElementById("<%= TextBoxVisualFieldMeanDeviationValueOS.ClientID %>"), true, true);
                    enabledControlTextField(document.getElementById("<%= TextBoxVisualFieldPSDValueOS.ClientID %>"), true, true);
                    enabledControlTextField(document.getElementById("<%= TextBoxVisualFieldPSDValueOD.ClientID %>"), true, true);
                }
            });
            if ($('#<%= RadioButtonListAutomatedVisualFields.ClientID %>').find('input:checked').val() == ('1')) {
                var myaray = new Array("QE10txta", "QE10txtb", "QE10txtc", "QE10txtd", "QE10txte", "QE10txtf", "QE10txtg", "QE10txth", "QE10txti");
                var myarray2 = new Array('#QE10txtc', '#QE10txtg', '#QE10txti');
                generate(myaray, myarray2, false);

                enabledControlTextField(document.getElementById("<%= TextBoxVisualFieldMeanDeviationValueOD.ClientID %>"), false, false);
                enabledControlTextField(document.getElementById("<%= TextBoxVisualFieldMeanDeviationValueOS.ClientID %>"), false, false);
                enabledControlTextField(document.getElementById("<%= TextBoxVisualFieldPSDValueOS.ClientID %>"), false, false);
                enabledControlTextField(document.getElementById("<%= TextBoxVisualFieldPSDValueOD.ClientID %>"), false, false);
            }
            else {
                var myaray = new Array("QE10txta", "QE10txtb", "QE10txtc", "QE10txtd", "QE10txte", "QE10txtf", "QE10txtg", "QE10txth", "QE10txti");
                var myarray2 = new Array('#QE10txtc', '#QE10txtg', '#QE10txti');
                generate(myaray, myarray2, true);

                enabledControlTextField(document.getElementById("<%= TextBoxVisualFieldMeanDeviationValueOD.ClientID %>"), true, true);
                enabledControlTextField(document.getElementById("<%= TextBoxVisualFieldMeanDeviationValueOS.ClientID %>"), true, true);
                enabledControlTextField(document.getElementById("<%= TextBoxVisualFieldPSDValueOS.ClientID %>"), true, true);
                enabledControlTextField(document.getElementById("<%= TextBoxVisualFieldPSDValueOD.ClientID %>"), true, true);
            }
        });

    </script>
    <script type="text/javascript">
        function ButtonSubmit_PQRS() {
            var pqrs = ("<%= (PQRSEnabled && PQRSUsed) %>" == "True");
            if (pqrs) {
                var year = $("#<%= DropDownListPQRSVisitYear.ClientID %>").val();
                var month = $("#<%= DropDownListPQRSVisitMonth.ClientID %>").val();
                if (year == 0 || month == 0)
                    return confirm("If there is no PQRS visit date selected, all of your PQRS data will not be saved, is that okay?");
                if (year != 2015)
                    return confirm("If the PQRS visit date selected is not 2015, all of your PQRS data will not be saved, is that okay?");
            }
            return true;
        }

        $(function () {
            $('#QH2').aToolTip({
                clickIt: true,
                tipContent: 'A comprehensive medical history should be obtained [with particular attention to glaucoma risk factors including systemic hypotension (cardiovascular disease) and contraindications to some glaucoma medications including reactive airways disease, bradyarrythmia, and pregnancy].'
            });
        });
        $(function () {
            $('#QH3').aToolTip({
                clickIt: true,
                tipContent: 'In particular, the patient should be asked about previous ocular trauma, laser treatment, and incisional surgery, including laser refractive surgery.'
            });
        });
        $(function () {
            $('#QE2').aToolTip({
                clickIt: true,
                tipContent: '20/800 or count fingers @ 5 ft<br />20/1000 or count fingers @ 4 ft<br />20/1600 or count fingers @ 3ft<br />20/2000 or count fingers @ 2 ft<br />20/4000 or count fingers @ 1 ft<br />20/7777 =CF<br />20/8888 =HM<br />20/9999 =LP<br />20/0000 =NLP'
            });
        });
        $(function () {
            $('#QE6').aToolTip({
                clickIt: true,
                tipContent: 'In examining the optic nerve, particular attention should be paid to the neuroretinal rim, including generalized or focal thinning, notching, and disc hemorrhage.  The presence and extent of peripapillary atrophy should also be noted, as it is a risk factor for glaucoma progression.'
            });
        });
        $(function () {
            $('#QF2').aToolTip({
                clickIt: true,
                tipContent: '20/800 or count fingers @ 5 ft<br />20/1000 or count fingers @ 4 ft<br />20/1600 or count fingers @ 3ft<br />20/2000 or count fingers @ 2 ft<br />20/4000 or count fingers @ 1 ft<br />20/7777 =CF<br />20/8888 =HM<br />20/9999 =LP<br />20/0000 =NLP'
            });
        });
    </script>

</asp:Content>
