﻿<%@ Page Title="" Language="C#" MasterPageFile="~/PIMMasterPage.master" AutoEventWireup="true" CodeFile="YAGChart.aspx.cs" Inherits="abo_YAGChart" %>

<asp:Content ID="Content1" ContentPlaceHolderID="head" Runat="Server">

    <script type="text/javascript" language="javascript" src="http://ajax.googleapis.com/ajax/libs/jquery/1.7.1/jquery.min.js"></script>
    <link type="text/css" href="../../common/css/atooltip.css" rel="stylesheet"  media="screen" />
	<script type="text/javascript" src="../../common/js/jquery.atooltip.js"></script>
    
    <style type="text/css">
        .bginputa{
	        float:right;
	        margin:21px 0 0 1px;
	        background: url(../common/images/orange_button_with_arrow.png) no-repeat;
	        overflow:hidden;
	        height:35px;

        }
        .bginputa a{
	        color: white;
	        text-align:center;
	        height:35px;
	        line-height:28px;
	        padding:0 14px 15px  ;
	        cursor:pointer;
	        float:left;
	        border:none;
	        text-decoration:none;
	        font-family:Arial, Helvetica, sans-serif; 
	        font-size:12px; 
	        font-weight:bold; 
	        letter-spacing: 0px;
        }
        .bginputa a:hover{
	        text-decoration:none;
        }
        .right {
            text-align:right;
        }
    </style>
    <script type="text/javascript">
        $(function() {
            $('#QE1').aToolTip({
                clickIt: true,
                tipContent: '20/800 or count fingers @ 5 ft<br />20/1000 or count fingers @ 4 ft<br />20/1600 or count fingers @ 3ft<br />20/2000 or count fingers @ 2 ft<br />20/4000 or count fingers @ 1 ft<br />20/7777 =CF<br />20/8888 =HM<br />20/9999 =LP<br />20/0000 =NLP'
            });

            $('#QO5').aToolTip({
                clickIt: true,
                tipContent: '20/800 or count fingers @ 5 ft<br />20/1000 or count fingers @ 4 ft<br />20/1600 or count fingers @ 3ft<br />20/2000 or count fingers @ 2 ft<br />20/4000 or count fingers @ 1 ft<br />20/7777 =CF<br />20/8888 =HM<br />20/9999 =LP<br />20/0000 =NLP'
            });
        });
    </script>
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder1" Runat="Server">
    <asp:HiddenField ID="HiddenFieldRecordIdentifier" runat="server"/>
        <!--  pim -->
        <div class="pim">
            <div class="record-ident clearfix">
                <h3 class="record-first">RECORD IDENTIFIER: <asp:Literal runat="server" ID="LiteralRecordIdentifier"></asp:Literal></h3>
                <h3 class="record-second"><asp:Literal runat="server" ID="LiteralAbstractionNumber"></asp:Literal></h3>
            </div>
            <!-- History -->
            <table>
                <tr>
                    <th colspan="3" width="910px"><p>History</p></th>
                </tr>
                <!-- Question 1 -->
                <tr class="table_body">
                    <td width="70%" colspan="2">
                        <strong>1. Date of birth</strong>
                        <asp:Label ID='LabelMonthOfBirth' ForeColor="Red" Visible="false" runat='server' Text='<br />Please Complete'></asp:Label>
                        <asp:Label ID='LabelYearOfBirth' ForeColor="Red" Visible="false" runat='server' Text='<br />Please Complete'></asp:Label>
                        <asp:Label ID='Labelinitialage' ForeColor="Red" Visible="false" runat='server' Text='<br />Please complete initial age' />
                    </td>
                    <td>
                        <asp:DropDownList ID='DropDownListMonthOfBirth' DataValueField="MonthID" DataTextField="MonthName" runat='server' />
                        <asp:DropDownList ID='DropDownListYearOfBirth' DataValueField='YearID' DataTextField='YearName' runat='server' />
                    </td>
                </tr>
                <!-- Question 2 -->
                <tr class="table_body_bg">
                    <td  colspan="2">
                        <strong>2. Gender</strong>
                        <asp:Label ID='LabelRBGender' ForeColor="Red" Visible="false" runat='server' Text='<br />Please Complete' />
                    </td>
                    <td>
                        <asp:RadioButtonList id='RadioButtonListRBGender' RepeatDirection='Horizontal' CssClass='aspxList' runat='server'>
                            <asp:ListItem Value="42">&nbsp;Male</asp:ListItem>
                            <asp:ListItem Value="43">&nbsp;Female</asp:ListItem>
                        </asp:RadioButtonList>
                    </td>
                </tr>
                <!-- Visual Complaint Header -->
                <tr class="table_body">
                    <td rowspan="2"><strong>3. Visual complaint</strong></td>
               
                    <td>
                       Do symptoms impact activities of daily life?<br />
                        <asp:Label ID='LabelRBSymptomsDailyLiving' runat='server' ForeColor="Red" Visible="false" Text='Please Complete' />
                    </td>
                    <td>
                        <asp:RadioButtonList id='RadioButtonListRBSymptomsDailyLiving' RepeatDirection='Horizontal' CssClass='aspxList' runat='server'>
                            <asp:ListItem Value="1">&nbsp;Yes</asp:ListItem>
                            <asp:ListItem Value="2">&nbsp;No</asp:ListItem>
                            <asp:ListItem Value="3">&nbsp;Not Documented</asp:ListItem>
                        </asp:RadioButtonList>
                    </td>
                </tr>
                <!-- Question 4 -->
                <tr class="table_body">
                    <td>
                        Previous history of glaucoma
                        <asp:Label ID='LabelRBHistoryGlaucoma' ForeColor="Red" Visible="false" runat='server' Text='<br />Please Complete' />
                    </td>
                    <td>
                        <asp:RadioButtonList id='RadioButtonListRBHistoryGlaucoma' RepeatDirection='Horizontal' CssClass='aspxList' runat='server'>
                            <asp:ListItem Value="1">&nbsp;Yes</asp:ListItem>
                            <asp:ListItem Value="2">&nbsp;No</asp:ListItem>
                            <asp:ListItem Value="3">&nbsp;Not Documented</asp:ListItem>
                        </asp:RadioButtonList>
                    </td>
                </tr>
            </table>
            <br />
            <!-- Pre-Operative Examination -->
            <table>
                <tr>
                    <th colspan="2" width="910px"><p>Pre-Operative Examination</p></th>
                </tr>
                <!-- Question 1 -->
                <tr class="table_body">
                    <td width="70%">
                        <strong>1. Corrected visual acuity</strong>&nbsp;<a href="#"><img id="QE1" src="../../common/images/tip.gif" alt="Tip" /></a>
                        <asp:Label Visible="false" ID="LabelBCVA" runat="server" ForeColor="Red" Text="<br />Please Complete" />
                    </td>
                    <td>
                        20&nbsp;/&nbsp;<asp:DropDownList ID='DropDownListBCVA' DataValueField='examValue' DataTextField='examLabel' runat='server' />
                    </td>
                </tr>
                <!-- Question 2 -->
                <tr class="table_body_bg">
                    <td>
                        <strong>2. Was a refraction performed within last 12 months (or results of recent refraction documented)?</strong>
                        <asp:Label ID='LabelRBExamRefraction' ForeColor="Red" Visible="false"  runat='server' Text='<br />Please Complete' />
                    </td>
                    <td>
                        <asp:RadioButtonList id='RadioButtonListRBExamRefraction' RepeatDirection='Horizontal' CssClass='aspxList' runat='server'>
                            <asp:ListItem Value="1">&nbsp;Yes</asp:ListItem>
                            <asp:ListItem Value="2">&nbsp;No</asp:ListItem>
                            <asp:ListItem Value="3">&nbsp;Not Documented</asp:ListItem>
                        </asp:RadioButtonList>
                    </td>
                </tr>
                <tr class="table_body_bg">
                    <td>
                        <strong><span id="QP2aA">2a.  If a refraction was not performed, is a reason documented (i.e. dense posterior capsule opacity precluding useful vision)?</span></strong>
                        <asp:Label ID='LabelRBRefractionNotPerformedReason' ForeColor="Red" Visible="false"  runat='server' Text='<br />Please Complete' />
                    </td>
                    <td>
                        <span id="QP2aB">
                        <asp:RadioButtonList id='RadioButtonListRBRefractionNotPerformedReason' RepeatDirection='Horizontal' CssClass='aspxList' runat='server'>
                            <asp:ListItem Value="1">&nbsp;Yes</asp:ListItem>
                            <asp:ListItem Value="2">&nbsp;No</asp:ListItem>
                            <asp:ListItem Value="3">&nbsp;Not Documented</asp:ListItem>
                        </asp:RadioButtonList>
                        </span>
                    </td>
                </tr>
                <!-- Question 3 -->
                <tr class="table_body">
                    <td>
                        <strong>3.  What was the patient’s IOP?</strong>
                    </td>
                    <td>
                        <asp:TextBox runat='server' ID='TextBoxExamIOPMeasurement' size='' Width="92px" onkeyup="toggleQP3CB();" />&nbsp;mmHg
                        <br /><asp:CheckBox runat='server' ID='CheckBoxExamIOPNA' text='&nbsp;Not Performed' onclick="toggleQP3TB();" />
                    </td>
                </tr>
                <!-- Question 4 -->
                <tr class="table_body_bg">
                    <td>
                        <strong>4. Does the posterior capsule opacity involve the visual axis?</strong>
                        <asp:Label ID='LabelRBPCOVisualAxis' ForeColor="Red" Visible="false"  runat='server' Text='<br />Please Complete' />
                    </td>
                    <td>
                        <asp:RadioButtonList id='RadioButtonListRBPCOVisualAxis' RepeatDirection='Horizontal' CssClass='aspxList' runat='server'>
                            <asp:ListItem Value="1">&nbsp;Yes</asp:ListItem>
                            <asp:ListItem Value="2">&nbsp;No</asp:ListItem>
                            <asp:ListItem Value="3">&nbsp;Not Documented</asp:ListItem>
                        </asp:RadioButtonList>
                    </td>
                </tr>
                <!-- Question 5 -->
                <tr class="table_body">
                    <td>
                        <strong>5. Is there evidence of capsular block syndrome?</strong>
                        <asp:Label ID='LabelExamCapsularBlock' ForeColor="Red" Visible="false"  runat='server' Text='<br />Please Complete' />
                    </td>
                    <td>
                        <asp:RadioButton runat='server' ID='RadioButtonExamCapsularBlockYes' GroupName='ExamCapsularBlock' text='&nbsp;Yes' />&nbsp;&nbsp;
                        <asp:RadioButton runat='server' ID='RadioButtonExamCapsularBlockNo' GroupName='ExamCapsularBlock' text='&nbsp;No' />
                    </td>
                </tr>
                <!-- Question 6 -->
                <tr class="table_body_bg">
                    <td>
                        <strong>6. Was a dilated fundus exam performed?</strong>
                        <asp:Label ID='LabelExamDilatedFundus' ForeColor="Red" Visible="false"  runat='server' Text='<br />Please Complete' />
                    </td>
                    <td>
                        <asp:RadioButton runat='server' ID='RadioButtonExamDilatedFundusYes' GroupName='ExamDilatedFundus' text='&nbsp;Yes' />&nbsp;&nbsp;
                        <asp:RadioButton runat='server' ID='RadioButtonExamDilatedFundusNo' GroupName='ExamDilatedFundus' text='&nbsp;No' />
                    </td>
                </tr>
                <!-- Question 7 -->
                <tr class="table_body">
                    <td>
                        <strong>7. Were the risks, benefits and alternatives of laser capsulotomy discussed with the patient?</strong>
                        <asp:Label ID='LabelRBExamPatientDiscuss' ForeColor="Red" Visible="false"  runat='server' Text='<br />Please Complete' />
                    </td>
                    <td>
                        <asp:RadioButtonList id='RadioButtonListRBExamPatientDiscuss' RepeatDirection='Horizontal' CssClass='aspxList' runat='server'>
                            <asp:ListItem Value="1">&nbsp;Yes</asp:ListItem>
                            <asp:ListItem Value="2">&nbsp;No</asp:ListItem>
                            <asp:ListItem Value="3">&nbsp;Not Documented</asp:ListItem>
                        </asp:RadioButtonList>
                    </td>
                </tr>
                <!-- Question 8 -->
                <tr class="table_body_bg">
                    <td>
                        <strong>8. Was signed informed consent obtained?</strong>
                        <asp:Label ID='LabelExamSignedConsent' ForeColor="Red" Visible="false"  runat='server' Text='<br />Please Complete' />
                    </td>
                    <td>
                        <asp:RadioButton runat='server' ID='RadioButtonExamSignedConsentYes' GroupName='ExamSignedConsent ' text='&nbsp;Yes' />&nbsp;&nbsp;
                        <asp:RadioButton runat='server' ID='RadioButtonExamSignedConsentNo' GroupName='ExamSignedConsent ' text='&nbsp;No' />
                    </td>
                </tr>
            </table>
            <br />
            <!-- Surgery and Early Postoperative Care -->
            <table>
                <tr>
                    <th colspan="3" width="910px"><p>Surgery and Early Postoperative Care</p></th>
                </tr>
                <!-- Question 1 -->
                <tr class="table_body">
                    <td width="70%">
                        <strong>1. Date of surgery</strong>
                        <asp:Label ID='LabelMonthOfSurgery' ForeColor="Red" Visible="false" runat='server' Text='<br />Please Complete' />
                        <asp:Label ID='LabelYearOfSurgery' ForeColor="Red" Visible="false" runat='server' Text='<br />Please Complete' />
                    </td>
                    <td>
                        <asp:DropDownList ID='DropDownListMonthOfSurgery' DataValueField="MonthID" DataTextField="MonthName" runat='server' />
                        <asp:DropDownList ID='DropDownListYearOfSurgery' DataValueField='YearID' DataTextField='YearName' runat='server' />
                    </td>
                </tr>
                <!-- Question 2 -->
                <tr class="table_body_bg">
                    <td>
                        <strong>2. Total energy used (millijoules per burst x total number of bursts)</strong>
                    </td>
                    <td>
                        <asp:TextBox runat='server' ID='TextBoxSurgeryEnergy' size='' Width="112px" onkeyup="toggleQP2CB();" />&nbsp;mJ
                        <br /><asp:CheckBox runat='server' ID='CheckBoxSurgeryEnergyNA' text='&nbsp;Not Documented' onclick="toggleQP2TB();"/>
                    </td>
                </tr>
                <!-- Question 3 -->
                <tr class="table_body">
                    <td>
                        <strong>3. Was an aqueous suppressant instilled in the eye prior to or immediately following surgery?</strong>
                        <asp:Label ID='LabelAqueousSuppressant' ForeColor="Red" Visible="false" runat='server' Text='<br />Please Complete' />
                    </td>
                    <td>
                        <asp:RadioButton runat='server' ID='RadioButtonAqueousSuppressantYes' GroupName='AqueousSuppressant ' text='&nbsp;Yes' />&nbsp;&nbsp;
                        <asp:RadioButton runat='server' ID='RadioButtonAqueousSuppressantNo' GroupName='AqueousSuppressant ' text='&nbsp;No' />
                    </td>
                </tr>
                <!-- Question 4 -->
                <tr class="table_body_bg">
                    <td>
                        <strong>4. What was the IOP within 24 hours after surgery?</strong>
                    </td>
                    <td>
                        <asp:TextBox runat='server' ID='TextBoxSurgeryIOP24hrs' size='' Width="119px" onkeyup="toggleQP44CB();" />&nbsp;mmHg
                        <br /><asp:CheckBox runat='server' ID='CheckBoxSurgeryIOP24hrsNA' text='&nbsp;Not Documented' onclick="toggleQP44TB();" />
                    </td>
                </tr>
                <!-- Question 5 -->
                <tr class="table_body">
                    <td>
                        <strong>5. Were verbal and/or written instructions given on the symptoms that should lead the patient to contact the ophthalmologist immediately?</strong>
                        <asp:Label ID='LabelRBSurgeryPatientInstruction' ForeColor="Red" Visible="false" runat='server' Text='<br />Please Complete' />
                    </td>
                    <td>
                        <asp:RadioButtonList id='RadioButtonListRBSurgeryPatientInstruction' RepeatDirection='Horizontal' CssClass='aspxList' runat='server'>
                            <asp:ListItem Value="1">&nbsp;Yes</asp:ListItem>
                            <asp:ListItem Value="2">&nbsp;No</asp:ListItem>
                            <asp:ListItem Value="3">&nbsp;Not Documented</asp:ListItem>
                        </asp:RadioButtonList>
                    </td>
                </tr>
            </table>
            <br />
            <!-- Outcomes -->
            <table>
                <tr>
                    <th colspan="3" width="910px"><p>Outcomes</p></th>
                </tr>
                <!-- Question 1 -->
                <tr class="table_body">
                    <td width="70%">
                        <strong>1. Was a postoperative appointment arranged?</strong>
                        <asp:Label ID='LabelRBOutcomeAppointment' ForeColor="Red" Visible="false" runat='server' Text='<br />Please Complete' />
                    </td>
                    <td>
                        <asp:RadioButtonList id='RadioButtonListRBOutcomeAppointment' RepeatDirection='Vertical' CssClass='aspxList' runat='server'>
                            <asp:ListItem Value="327">&nbsp;Yes with you</asp:ListItem>
                            <asp:ListItem Value="328">&nbsp;Yes with another provider</asp:ListItem>
                            <asp:ListItem Value="2">&nbsp;No</asp:ListItem>
                        </asp:RadioButtonList>
                    </td>
                </tr>
                <!-- Question 2 -->
                <tr class="table_body_bg">
                    <td>
                        <span id="QO2A">
                        <strong>2. Date of postoperative examination</strong>
                        </span>
                    </td>
                    <td>
                        <span id="QO2B">
                        <asp:DropDownList ID='DropDownListMonthOutcomeExam' DataValueField="MonthID" DataTextField="MonthName" runat='server' />
                        &nbsp;<asp:DropDownList ID='DropDownListYearOutcomeExam' DataValueField='YearID' DataTextField='YearName' runat='server' />
                        </span>
                    </td>
                </tr>
                <!-- Question 3 -->
                <tr class="table_body">
                    <td>
                        <span id="QO3A"><strong>3. Did subjective visual function improve following the procedure?</strong></span>
                    </td>
                    <td>
                        <span id="QO3B">
                        <asp:RadioButtonList id='RadioButtonListRBOutcomeSubImprove' RepeatDirection='Horizontal' CssClass='aspxList' runat='server'>
                            <asp:ListItem Value="1">&nbsp;Yes</asp:ListItem>
                            <asp:ListItem Value="2">&nbsp;No</asp:ListItem>
                            <asp:ListItem Value="3">&nbsp;Not Documented</asp:ListItem>
                        </asp:RadioButtonList>
                        </span>
                    </td>
                </tr>
                <!-- Question 4 -->
                <tr class="table_body_bg">
                    <td>
                        <span id="QO4A"><strong>4. Was a post-operative refraction performed?</strong></span>
                    </td>
                    <td>
                        <span id="QO4B">
                        <asp:RadioButton runat='server' ID='RadioButtonOutcomeRefractionYes' GroupName='OutcomeRefraction ' text='&nbsp;Yes' />&nbsp;&nbsp;
                        <asp:RadioButton runat='server' ID='RadioButtonOutcomeRefractionNo' GroupName='OutcomeRefraction ' text='&nbsp;No' />
                        </span>
                    </td>
                </tr>
                <!-- Question 5 -->
                <tr class="table_body">
                    <td>
                        <span id="QO5A"><strong>4a. If not performed, is a reason documented (i.e. patient sees well and does not desire correction)?</strong></span>&nbsp;<a href="#"><img id="QO5" src="../../common/images/tip.gif" alt="Tip" /></a>
                    </td>
                    <td>
                        <span id="QO5B">
                        <asp:RadioButton runat='server' ID='RadioButtonOutcomeRefractionNAYes' GroupName='OutcomeRefractionNA ' text='&nbsp;Yes' />&nbsp;&nbsp;
                        <asp:RadioButton runat='server' ID='RadioButtonOutcomeRefractionNANo' GroupName='OutcomeRefractionNA ' text='&nbsp;No' />
                        </span>
                    </td>
                </tr>
                <!-- Question 6 -->
                <tr class="table_body_bg">
                    <td>
                        <span id="QO6A"><strong>5. Corrected visual acuity</strong></span>
                    </td>
                    <td>
                        <span id="QO6B">
                        20&nbsp;/&nbsp;<asp:DropDownList ID='DropDownListOutcomeBVCA' DataValueField='examValue' DataTextField='examLabel' runat='server' onchange="toggleQO5CB();" />
                        <br /><asp:CheckBox runat='server' ID='CheckBoxOutcomeBVCANone' text='&nbsp;Not Documented' onclick="toggleQO5DD();" />
                        </span>
                    </td>
                </tr>
                <!-- Question 7 -->
                <tr class="table_body">
                    <td>
                        <span id="QO7A"><strong>6. Was the capsulotomy of adequate size and centration?</strong></span>
                    </td>
                    <td>
                        <span id="QO7B">
                        <asp:RadioButtonList id='RadioButtonListRBOutcomeCapsulotomyAdequate' RepeatDirection='Horizontal' CssClass='aspxList' runat='server'>
                            <asp:ListItem Value="1">&nbsp;Yes</asp:ListItem>
                            <asp:ListItem Value="2">&nbsp;No</asp:ListItem>
                            <asp:ListItem Value="3">&nbsp;Not Documented</asp:ListItem>
                        </asp:RadioButtonList>
                        </span>
                    </td>
                </tr>
                <!-- Question 8 -->
                <tr class="table_body_bg">
                    <td>
                        <span id="QO8A"><strong>7. Was a dilated fundus exam performed?</strong></span>
                    </td>
                    <td>
                        <span id="QO8B">
                        <asp:RadioButtonList id='RadioButtonListRBOutcomeDilatedFundus' RepeatDirection='Horizontal' CssClass='aspxList' runat='server'>
                            <asp:ListItem Value="1">&nbsp;Yes</asp:ListItem>
                            <asp:ListItem Value="2">&nbsp;No</asp:ListItem>
                            <asp:ListItem Value="3">&nbsp;Not Documented</asp:ListItem>
                        </asp:RadioButtonList>
                        </span>
                    </td>
                </tr>
                <!-- Question 9 -->
                <tr class="table_body">
                    <td><span id="QO9A"><strong>8. Were any post-operative complications encountered?</strong></span></td>

                    <td>
                        <span id="QO9B">
                        <asp:CheckBox runat='server' ID='CheckBoxOutcomeComplicationsNone' text='&nbsp;None' />
                        <br /><asp:CheckBox runat='server' ID='CheckBoxOutcomeComplicationsIOLdefects' text='&nbsp;IOL Defects' />
                        <br /><asp:CheckBox runat='server' ID='CheckBoxOutcomeComplicationsIOLdecentration' text='&nbsp;IOL Decentration or dislocation' />
                        <br /><asp:CheckBox runat='server' ID='CheckBoxOutcomeComplicationsVitreous' text='&nbsp;Vitreous Prolapse' />
                        <br /><asp:CheckBox runat='server' ID='CheckBoxOutcomeComplicationsMacular' text='&nbsp;Macular Edema' />
                        <br /><asp:CheckBox runat='server' ID='CheckBoxOutcomeComplicationsRetinal' text='&nbsp;Retinal Detachment' />
                        <br /><asp:CheckBox runat='server' ID='CheckBoxOutcomeComplicationsIOPelevation' text='&nbsp;IOP elevation requiring addition of medication beyond this examination' />
                        </span>
                    </td>
                </tr>
                 <tr class="table_body_bg">
                    <td>
                        <span id="QO10A"><strong>9. Was further surgery necessary within 90 days of the procedure?</strong></span>
                    </td>
                    <td>
                        <span id="QO10B">
                        <asp:RadioButton runat='server' ID='RadioButtonOutcomeFurtherSurgeryYes' GroupName='OutcomeFurtherSurgery ' text='&nbsp;Yes' />&nbsp;&nbsp;
                        <asp:RadioButton runat='server' ID='RadioButtonOutcomeFurtherSurgeryNo' GroupName='OutcomeFurtherSurgery ' text='&nbsp;No' />
                        </span>
                    </td>
                </tr>
            </table>
            <div class="button-box">
                <asp:LinkButton ID="LinkButtonBackToDashboard" runat="server" Text="Back To Chart Registration" PostBackUrl="../PatientChartRegistration.aspx?CycleNumber=1" Visible="false" CssClass="button" />
                <asp:LinkButton ID="ButtonSubmit"  OnClick="ButtonSubmit_Click" runat="server" Text="Submit Chart" CssClass="button" />
            </div>
        </div>
        <!-- ION pim ends -->

    <script type="text/javascript" language="javascript">
        //      Either enables or disables input boxes
        function enabledControl(el, disabled, checked) {
            //alert("Hello world from enabled control");
            try {
                if (disabled) {
                    el.disabled = disabled;
                    el.setAttribute("disabled", true);
                }
                else {
                    el.disabled = disabled;
                    el.removeAttribute("disabled");
                }

                if (checked == true)
                    el.checked = false;
            }
            catch (E) {
            }
            if (el.childNodes && el.childNodes.length > 0) {
                for (var x = 0; x < el.childNodes.length; x++) {
                    enabledControl(el.childNodes[x], disabled, checked);
                }
            }
        }
        //      Either enables or disables dropdown boxes
        function enabledControlDropDown(el, disabled, checked) {
            //alert("Hello world from enabled control");
            try {
                if (disabled) {
                    el.disabled = disabled;
                    el.setAttribute("disabled", true);
                }
                else {
                    el.disabled = disabled;
                    el.removeAttribute("disabled");
                }

                if (checked == true)
                    el.options[0].selected = true;
            }
            catch (E) {
            }
            if (el.childNodes && el.childNodes.length > 0) {
                for (var x = 0; x < el.childNodes.length; x++) {
                    enabledControl(el.childNodes[x], disabled, checked);
                }
            }
        }
        //      Either enables or disables text boxes
        function enabledControlTextField(el, disabled, checked) {
            //alert("Hello world from enabled control");
            try {
                if (disabled) {
                    el.disabled = disabled;
                    el.setAttribute("disabled", true);
                }
                else {
                    el.disabled = disabled;
                    el.removeAttribute("disabled");
                }

                if (checked == true) {
                    el.value = '';
                }
            }
            catch (E) {
            }
            if (el.childNodes && el.childNodes.length > 0) {
                for (var x = 0; x < el.childNodes.length; x++) {
                    enabledControl(el.childNodes[x], disabled, checked);
                }
            }
        }

        function generate(arr1, arr2, istrue) {
            if (istrue == true) {
                for (var i = 0; i < arr1.length; i++) {
                    document.getElementById(arr1[i]).style.color = "gray";
                }
                for (var i = 0; i < arr2.length; i++) {
                    $(arr2[i] + ' :input').attr('disabled', true);
                    $(arr2[i] + ' :input').removeAttr("checked");
                }
            }
            if (istrue == false) {
                for (var i = 0; i < arr1.length; i++) {
                    document.getElementById(arr1[i]).style.color = "#333";
                }
                for (var i = 0; i < arr2.length; i++) {
                    $(arr2[i] + ' :input').removeAttr('disabled');
                }
            }
        }

        function toggleQP3CB() {
            document.getElementById("<%= CheckBoxExamIOPNA.ClientID %>").checked = false;
        }

        function toggleQP3TB() {
            document.getElementById("<%= TextBoxExamIOPMeasurement.ClientID %>").value = '';
        }
        function toggleQP2CB() {
            document.getElementById("<%= CheckBoxSurgeryEnergyNA.ClientID %>").checked = false;
        }

        function toggleQP2TB() {
            document.getElementById("<%= TextBoxSurgeryEnergy.ClientID %>").value = '';
        }
        function toggleQP44CB() {
            document.getElementById("<%= CheckBoxSurgeryIOP24hrsNA.ClientID %>").checked = false;
        }

        function toggleQP44TB() {
            document.getElementById("<%= TextBoxSurgeryIOP24hrs.ClientID %>").value = '';
        }

        function toggleQO5CB() {
            document.getElementById("<%= CheckBoxOutcomeBVCANone.ClientID %>").checked = false;
        }

        function toggleQO5DD() {
            document.getElementById("<%= DropDownListOutcomeBVCA.ClientID %>").options[0].selected = true;
        }
    </script>

    <script  type="text/javascript" language="javascript">
        $(document).ready(function () {

            // Question 2 - Pre-op ***********************
            $("#<%= RadioButtonListRBExamRefraction.ClientID %>").click(function () {
                if ($('#<%= RadioButtonListRBExamRefraction.ClientID %>').find('input:checked').val() == ('2')) {
                    var myaray = new Array("QP2aA", "QP2aB");
                    var myarray2 = new Array('#QP2aB');
                    generate(myaray, myarray2, false);
                }
                else {
                    var myaray = new Array("QP2aA", "QP2aB");
                    var myarray2 = new Array('#QP2aB');
                    generate(myaray, myarray2, true);
                }
            });
            if ($('#<%= RadioButtonListRBExamRefraction.ClientID %>').find('input:checked').val() == ('2')) {
                var myaray = new Array("QP2aA", "QP2aB");
                var myarray2 = new Array('#QP2aB');
                generate(myaray, myarray2, false);
            }
            else {
                var myaray = new Array("QP2aA", "QP2aB");
                var myarray2 = new Array('#QP2aB');
                generate(myaray, myarray2, true);
            }


            $("#<%= RadioButtonListRBOutcomeAppointment.ClientID %>").click(function () {
                if ($('#<%= RadioButtonListRBOutcomeAppointment.ClientID %>').find('input:checked').val() == ('327')) {
                    var myaray = new Array("QO2A", "QO2B", "QO3A", "QO3B", "QO4A", "QO4B", "QO5A", "QO5B", "QO6A", "QO6B", "QO7A", "QO7B", "QO8A", "QO8B", "QO9A", "QO9B", "QO10A", "QO10B");
                    var myarray2 = new Array("#QO2B", "#QO3B", "#QO4B", "#QO5B", "#QO6B", "#QO7B", "#QO8B", "#QO9B", "#QO10B");
                    generate(myaray, myarray2, false);

                    enabledControlTextField(document.getElementById("<%= DropDownListMonthOutcomeExam.ClientID %>"), false, false);
                    enabledControlTextField(document.getElementById("<%= DropDownListYearOutcomeExam.ClientID %>"), false, false);
                    enabledControlTextField(document.getElementById("<%= DropDownListOutcomeBVCA.ClientID %>"), false, false);

                }
                else {
                    var myaray = new Array("QO2A", "QO2B", "QO3A", "QO3B", "QO4A", "QO4B", "QO5A", "QO5B", "QO6A", "QO6B", "QO7A", "QO7B", "QO8A", "QO8B", "QO9A", "QO9B", "QO10A", "QO10B");
                    var myarray2 = new Array("#QO2B", "#QO3B", "#QO4B", "#QO5B", "#QO6B", "#QO7B", "#QO8B", "#QO9B", "#QO10B");
                    generate(myaray, myarray2, true);

                    enabledControlTextField(document.getElementById("<%= DropDownListMonthOutcomeExam.ClientID %>"), true, true);
                    enabledControlTextField(document.getElementById("<%= DropDownListYearOutcomeExam.ClientID %>"), true, true);
                    enabledControlTextField(document.getElementById("<%= DropDownListOutcomeBVCA.ClientID %>"), true, true);
                }
            });

            if ($('#<%= RadioButtonListRBOutcomeAppointment.ClientID %>').find('input:checked').val() == ('327')) {
                var myaray = new Array("QO2A", "QO2B", "QO3A", "QO3B", "QO4A", "QO4B", "QO5A", "QO5B", "QO6A", "QO6B", "QO7A", "QO7B", "QO8A", "QO8B", "QO9A", "QO9B", "QO10A", "QO10B");
                var myarray2 = new Array("#QO2B", "#QO3B", "#QO4B", "#QO5B", "#QO6B", "#QO7B", "#QO8B", "#QO9B", "#QO10B");
                generate(myaray, myarray2, false);

                enabledControlTextField(document.getElementById("<%= DropDownListMonthOutcomeExam.ClientID %>"), false, false);
                enabledControlTextField(document.getElementById("<%= DropDownListYearOutcomeExam.ClientID %>"), false, false);
                enabledControlTextField(document.getElementById("<%= DropDownListOutcomeBVCA.ClientID %>"), false, false);

            }
            else {
                var myaray = new Array("QO2A", "QO2B", "QO3A", "QO3B", "QO4A", "QO4B", "QO5A", "QO5B", "QO6A", "QO6B", "QO7A", "QO7B", "QO8A", "QO8B", "QO9A", "QO9B", "QO10A", "QO10B");
                var myarray2 = new Array("#QO2B", "#QO3B", "#QO4B", "#QO5B", "#QO6B", "#QO7B", "#QO8B", "#QO9B", "#QO10B");
                generate(myaray, myarray2, true);

                enabledControlTextField(document.getElementById("<%= DropDownListMonthOutcomeExam.ClientID %>"), true, true);
                enabledControlTextField(document.getElementById("<%= DropDownListYearOutcomeExam.ClientID %>"), true, true);
                enabledControlTextField(document.getElementById("<%= DropDownListOutcomeBVCA.ClientID %>"), true, true);
            }


            // Question 1a other - Pre-operative management ***************************
            $("#<%= RadioButtonOutcomeRefractionYes.ClientID %>").click(function () {
                if ($('#<%= RadioButtonOutcomeRefractionNo.ClientID %>').attr("checked") == "checked") {
                    var myaray = new Array("QO5A", "QO5B");
                    var myarray2 = new Array('#QO5B');
                    generate(myaray, myarray2, false);
                }
                else {
                    var myaray = new Array("QO5A", "QO5B");
                    var myarray2 = new Array('#QO5B');
                    generate(myaray, myarray2, true);
                }
            });
            $("#<%= RadioButtonOutcomeRefractionNo.ClientID %>").click(function () {
                if ($('#<%= RadioButtonOutcomeRefractionNo.ClientID %>').attr("checked") == "checked") {
                    var myaray = new Array("QO5A", "QO5B");
                    var myarray2 = new Array('#QO5B');
                    generate(myaray, myarray2, false);
                }
                else {
                    var myaray = new Array("QO5A", "QO5B");
                    var myarray2 = new Array('#QO5B');
                    generate(myaray, myarray2, true);
                }
            });
            if ($('#<%= RadioButtonOutcomeRefractionNo.ClientID %>').attr("checked") == "checked") {
                var myaray = new Array("QO5A", "QO5B");
                var myarray2 = new Array('#QO5B');
                generate(myaray, myarray2, false);
            }
            else {
                var myaray = new Array("QO5A", "QO5B");
                var myarray2 = new Array('#QO5B');
                generate(myaray, myarray2, true);
            }
        });

    </script>
</asp:Content>

