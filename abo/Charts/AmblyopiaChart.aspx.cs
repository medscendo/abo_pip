﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Transactions;
using NetHealthPIMModel;

public partial class abo_AmblyopiaChart : BasePage
{
    protected void Page_Load(object sender, EventArgs e)
    {
        if (!IsPostBack)
        {
            InitializePage();
        }

    }
    protected void InitializePage()
    {
        ((PIMMasterPage)Page.Master).SetTitle(" <A href=../PatientChartRegistration.aspx>Patient Chart Registration Form »</A> Chart"); 
        //((PIMMasterPage)Page.Master).SetTitle("Chart");

        using (NetHealthPIMEntities pim = new NetHealthPIMEntities())
        {
            int ModuleID = (int)Session[Constants.SESSION_WORKINGMODULEID];
            Module module = pim.Module.First(c => c.ModuleID == ModuleID);
            ((PIMMasterPage)Page.Master).SetHeader(module.ModuleName + " Chart Abstraction");

            ParticipantModuleCycle prev = (ParticipantModuleCycle)Session[Constants.SESSION_PREVIOUSCYCLE];
            String CycleNumber = Request.QueryString["CycleNumber"];

            int cycleID = ctx.ActiveModuleCycleID;
            if (CycleNumber == "1")
            {
                cycleID = prev.ParticipantModuleCycleID;
                LinkButtonBackToDashboard.Visible = true;
                LinkButtonSave.Visible = false;
            }

            ParticipantModuleCycle cycle = pim.ParticipantModuleCycle.First(c => c.ParticipantModuleCycleID == cycleID);
            String numberOfRecord = Request.QueryString["nr"];
            String totalRecords = Request.QueryString["t"];
            if (cycle.CycleNumber == 1)
            {
                ((PIMMasterPage)Page.Master).SetStatus(2);
                LiteralAbstractionNumber.Text = "Initial Abstraction: Chart " + numberOfRecord + " of " + totalRecords;
            }
            else
            {
                ((PIMMasterPage)Page.Master).SetStatus(3);
                LiteralAbstractionNumber.Text = "Second Abstraction: Chart " + numberOfRecord + " of " + totalRecords;
            }
            DropDownListDOBYear.DataSource = CycleManager.getDOBYearList(0, 100);
            DropDownListFirstExamYear.DataSource = CycleManager.getDOBYearList(0, 100);
            DropDownListFollowUpYear.DataSource = CycleManager.getDOBYearList(0, 100);
            DropDownListDOBMonth.DataSource = CycleManager.getMonthList();
            DropDownListFirstExamMonth.DataSource = CycleManager.getMonthList();
            DropDownListFollowUpMonth.DataSource = CycleManager.getMonthList();
            DropDownListBestCorrectedVisualOD.DataSource = CycleManager.getExamValues();
            DropDownListBestCorrectedVisualOS.DataSource = CycleManager.getExamValues();
            DropDownListMethodNonSnellenOptotypesType.DataSource = CycleManager.getNonSnellenOptotypes();
            DropDownListWorth4DotDistanceDescription.DataSource = CycleManager.getWorth4dot();
            DropDownListWorth4DotNearDescription.DataSource = CycleManager.getWorth4dot();
            DropDownListOutcomeBestCorrectedVisualOD.DataSource = CycleManager.getExamValues();
            DropDownListOutcomeBestCorrectedVisualOS.DataSource = CycleManager.getExamValues();
            //DropDownListOutcomeMethodStereoacuityDescription.DataSource = CycleManager.getNonSnellenOptotypes();
            DropDownListOutcomeWorth4DotDistanceDescription.DataSource = CycleManager.getWorth4dot();
            DropDownListOutcomeWorth4DotNearDescription.DataSource = CycleManager.getWorth4dot();
            DataBind();

            DropDownListDOBYear.Items.Insert(0, new ListItem("Year", "0"));
            DropDownListFirstExamYear.Items.Insert(0, new ListItem("Year", "0"));
            DropDownListFollowUpYear.Items.Insert(0, new ListItem("Year", "0"));


            string recordIdentifier = Request.QueryString["ri"];
            HiddenFieldRecordIdentifier.Value = recordIdentifier;
            LiteralRecordIdentifier.Text = recordIdentifier;

            CycleManager.UpdateChartDates(cycleID, recordIdentifier, Constants.ABO_AMBLYOPIA_MODULEID, false);

            var count = pim.ChartAbstractionAmblyopia.Where(ps => ps.ParticipantModuleCycle.ParticipantModuleCycleID == cycleID & ps.RecordIdentifier == recordIdentifier).Count();
            if (count == 0)
            {
                using (TransactionScope transaction = new TransactionScope())
                {
                    //Module module = pim.Module.First(m => m.ModuleID == Constants.ABO_AMBLYOPIA_MODULEID);

                    NetHealthPIMModel.ChartRegistration chartRegistration = (from cr in pim.ChartRegistration
                                                           where cr.ParticipantModuleCycleID == cycleID
                                                           & cr.RecordIdentifier == recordIdentifier
                                                           & cr.ModuleID == Constants.ABO_AMBLYOPIA_MODULEID
                                                           select cr).First<NetHealthPIMModel.ChartRegistration>();

                    DateTime initialVisit = Convert.ToDateTime(chartRegistration.InitialVisit);
                    DateTime DOB = Convert.ToDateTime(chartRegistration.DOB);
                    DateTime lastVisit = Convert.ToDateTime(chartRegistration.LastVisit);

                    ChartAbstractionAmblyopia abstractRecord = new ChartAbstractionAmblyopia();
                    abstractRecord.ParticipantModuleCycle = cycle;
                    abstractRecord.RecordIdentifier = recordIdentifier;
                    abstractRecord.MonthOfFirstExam = initialVisit.Month;
                    abstractRecord.YearOfFirstExam = initialVisit.Year;
                    abstractRecord.MonthOfBirth = DOB.Month;
                    abstractRecord.YearOfBirth = DOB.Year;
                    abstractRecord.MonthFollowUp = lastVisit.Month;
                    abstractRecord.YearFollowUp = lastVisit.Year;
                    DropDownListFirstExamMonth.SelectedValue = abstractRecord.MonthOfFirstExam.ToString();
                    DropDownListFirstExamYear.SelectedValue = abstractRecord.YearOfFirstExam.ToString();
                    DropDownListDOBMonth.SelectedValue = abstractRecord.MonthOfBirth.ToString();
                    DropDownListDOBYear.SelectedValue = abstractRecord.YearOfBirth.ToString();
                    DropDownListFollowUpMonth.SelectedValue = abstractRecord.MonthFollowUp.ToString();
                    DropDownListFollowUpYear.SelectedValue = abstractRecord.YearFollowUp.ToString();
                    abstractRecord.CreatedOn = DateTime.Now;
                    abstractRecord.LastUpdateDate = DateTime.Now;
                    pim.SaveChanges();

                    transaction.Complete();
                }
            }
            else
            {
                ChartAbstractionAmblyopia abstractRecord = (from c in pim.ChartAbstractionAmblyopia
                                                            where c.ParticipantModuleCycle.ParticipantModuleCycleID == cycleID & c.RecordIdentifier == recordIdentifier
                                                            select c).First<ChartAbstractionAmblyopia>();
                // History
                if (abstractRecord.MonthOfBirth != null) DropDownListDOBMonth.SelectedValue = abstractRecord.MonthOfBirth.ToString();
                if (abstractRecord.YearOfBirth != null) DropDownListDOBYear.SelectedValue = abstractRecord.YearOfBirth.ToString();
                if (abstractRecord.MonthOfFirstExam != null) DropDownListFirstExamMonth.SelectedValue = abstractRecord.MonthOfFirstExam.ToString();
                if (abstractRecord.YearOfFirstExam != null) DropDownListFirstExamYear.SelectedValue = abstractRecord.YearOfFirstExam.ToString();
                if (abstractRecord.MonthFollowUp != null) DropDownListFollowUpMonth.SelectedValue = abstractRecord.MonthFollowUp.ToString();
                if (abstractRecord.YearFollowUp != null) DropDownListFollowUpYear.SelectedValue = abstractRecord.YearFollowUp.ToString();

                //Diagnoses / Assessment 
                // 1
                if (abstractRecord.BestCorrectedVisualOD != null) DropDownListBestCorrectedVisualOD.SelectedValue = abstractRecord.BestCorrectedVisualOD.ToString();
                if (abstractRecord.BestCorrectedVisualOS != null) DropDownListBestCorrectedVisualOS.SelectedValue = abstractRecord.BestCorrectedVisualOS.ToString();
                //if (abstractRecord.MethodNonSnellenOptotypes != null)
                //{
                //    RadioButtonMethodNonSnellenOptotypesYes.Checked = ((bool)abstractRecord.MethodNonSnellenOptotypes);
                //    RadioButtonMethodNonSnellenOptotypesNo.Checked = !((bool)abstractRecord.MethodNonSnellenOptotypes);
                //}
                if (abstractRecord.MethodNonSnellenOptotypesType != null) DropDownListMethodNonSnellenOptotypesType.SelectedValue = abstractRecord.MethodNonSnellenOptotypesType.ToString();
                if (abstractRecord.MethodSnellenOptotypes != null)
                {
                    RadioButtonMethodSnellenOptotypesYes.Checked = ((bool)abstractRecord.MethodSnellenOptotypes);
                    RadioButtonMethodSnellenOptotypesNo.Checked = !((bool)abstractRecord.MethodSnellenOptotypes);
                }
                // 2
                if (abstractRecord.SensoryTestingPerformed != null)
                {
                    RadioButtonSensoryTestingPerformedYes.Checked = ((bool)abstractRecord.SensoryTestingPerformed);
                    RadioButtonSensoryTestingPerformedNo.Checked = !((bool)abstractRecord.SensoryTestingPerformed);
                }
                // 2a
                if (abstractRecord.MethodStereoacuityDescription != null) TextBoxMethodStereoacuityDescription.Text = abstractRecord.MethodStereoacuityDescription.ToString();
                if (abstractRecord.MethodStereoacuityUnableToComplete != null) CheckBoxMethodStereoacuityUnableToComplete.Checked = abstractRecord.MethodStereoacuityUnableToComplete.Value;
                if (abstractRecord.Worth4DotDistanceDescription != null) DropDownListWorth4DotDistanceDescription.SelectedValue = abstractRecord.Worth4DotDistanceDescription.ToString();
                if (abstractRecord.Worth4DotDistanceUnableToComplete != null) CheckBoxWorth4DotDistanceUnableToComplete.Checked = abstractRecord.Worth4DotDistanceUnableToComplete.Value;
                if (abstractRecord.Worth4DotNearDescription != null) DropDownListWorth4DotNearDescription.SelectedValue = abstractRecord.Worth4DotNearDescription.ToString();
                if (abstractRecord.Worth4DotNearUnableToComplete != null) CheckBoxWorth4DotNearUnableToComplete.Checked = abstractRecord.Worth4DotNearUnableToComplete.Value;
                // 3
                if (abstractRecord.AlignmentMeasured != null)
                {
                    RadioButtonAlignmentMeasuredYes.Checked = ((bool)abstractRecord.AlignmentMeasured);
                    RadioButtonAlignmentMeasuredNo.Checked = !((bool)abstractRecord.AlignmentMeasured);
                }

                // 3a
                if (abstractRecord.PrimaryPositionDistanceLeftPD != null) TextBoxPrimaryPositionDistanceLeftPD.Text = abstractRecord.PrimaryPositionDistanceLeftPD.ToString();

                if (abstractRecord.PrimaryPositionDistanceLeftET != null)
                {
                    RadioButtonPrimaryPositionDistanceLeftETYes.Checked = ((bool)abstractRecord.PrimaryPositionDistanceLeftET);
                    RadioButtonPrimaryPositionDistanceLeftETNo.Checked = !((bool)abstractRecord.PrimaryPositionDistanceLeftET);
                }

                if(abstractRecord.RadioButtonAligmentOrthophoriaDistance != null) RadioButtonAlligmentOrthoporiaDistance.Checked = (bool)abstractRecord.RadioButtonAligmentOrthophoriaDistance;
                if (abstractRecord.RadioButtonAligmentOrthophoriaNear != null) RadioButtonAlligmentOrthoporiaNear.Checked = (bool)abstractRecord.RadioButtonAligmentOrthophoriaNear;
                if (abstractRecord.PrimaryPositionNearLeftPD != null) TextBoxPrimaryPositionNearLeftPD.Text = abstractRecord.PrimaryPositionNearLeftPD.ToString();
                if (abstractRecord.PrimaryPositionNearLeftET != null)
                {
                    RadioButtonPrimaryPositionNearLeftETYes.Checked = ((bool)abstractRecord.PrimaryPositionNearLeftET);
                    RadioButtonPrimaryPositionNearLeftETNo.Checked = !((bool)abstractRecord.PrimaryPositionNearLeftET);
                }
                if (abstractRecord.PrimaryPositionNearRightPD != null) TextBoxPrimaryPositionNearRightPD.Text = abstractRecord.PrimaryPositionNearRightPD.ToString();
                if (abstractRecord.PrimaryPositionNearRightET != null)
                {
                    RadioButtonPrimaryPositionNearRightETYes.Checked = ((bool)abstractRecord.PrimaryPositionNearRightET);
                    RadioButtonPrimaryPositionNearRightETNo.Checked = !((bool)abstractRecord.PrimaryPositionNearRightET);
                }
                // 4
                if (abstractRecord.CyclopegicRetinoscopyPerformed != null)
                {
                    RadioButtonCyclopegicRetinoscopyPerformedYes.Checked = ((bool)abstractRecord.CyclopegicRetinoscopyPerformed);
                    RadioButtonCyclopegicRetinoscopyPerformedNo.Checked = !((bool)abstractRecord.CyclopegicRetinoscopyPerformed);
                }
                //4a
                if (abstractRecord.CycloplegicRefractionODsph != null) TextBoxCycloplegicRefractionODsph.Text = abstractRecord.CycloplegicRefractionODsph.ToString();
                if (abstractRecord.CycloplegicRefractionODcyl != null) TextBoxCycloplegicRefractionODcyl.Text = abstractRecord.CycloplegicRefractionODcyl.ToString();
                if (abstractRecord.CycloplegicRefractionODaxis != null) TextBoxCycloplegicRefractionODaxis.Text = abstractRecord.CycloplegicRefractionODaxis.ToString();
                if (abstractRecord.CycloplegicRefractionOSsph != null) TextBoxCycloplegicRefractionOSsph.Text = abstractRecord.CycloplegicRefractionOSsph.ToString();
                if (abstractRecord.CycloplegicRefractionOScyl != null) TextBoxCycloplegicRefractionOScyl.Text = abstractRecord.CycloplegicRefractionOScyl.ToString();
                if (abstractRecord.CycloplegicRefractionOSaxis != null) TextBoxCycloplegicRefractionOSaxis.Text = abstractRecord.CycloplegicRefractionOSaxis.ToString();
                if (abstractRecord.CycloplegicRefractionOSsphSign != null)
                {
                    RadioButtonTextBoxCycloplegicRefractionOSsphSignPlus.Checked = ((bool)abstractRecord.CycloplegicRefractionOSsphSign);
                    RadioButtonTextBoxCycloplegicRefractionOSsphSignMinus.Checked = !((bool)abstractRecord.CycloplegicRefractionOSsphSign);
                }
                if (abstractRecord.CycloplegicRefractionODsphSign != null)
                {
                    RadioButtonCycloplegicRefractionODcylSignPlus.Checked = ((bool)abstractRecord.CycloplegicRefractionODsphSign);
                    RadioButtonCycloplegicRefractionODcylSignMinus.Checked = !((bool)abstractRecord.CycloplegicRefractionODsphSign);
                }

                // Outcomes
                // 1
                if (abstractRecord.OutcomeBestCorrectedVisualOD != null) DropDownListOutcomeBestCorrectedVisualOD.SelectedValue = abstractRecord.OutcomeBestCorrectedVisualOD.ToString();
                if (abstractRecord.OutcomeBestCorrectedVisualOS != null) DropDownListOutcomeBestCorrectedVisualOS.SelectedValue = abstractRecord.OutcomeBestCorrectedVisualOS.ToString();
                // 2
                //if (abstractRecord.OutcomeMethodStereoacuityDescription != null) DropDownListOutcomeMethodStereoacuityDescription.SelectedValue = abstractRecord.OutcomeMethodStereoacuityDescription.ToString();
                if (abstractRecord.OutcomeMethodStereoacuityDescription != null) TextBoxOutcomeMethodStereoacuityDescription.Text = abstractRecord.OutcomeMethodStereoacuityDescription.ToString();
                if (abstractRecord.OutcomeMethodStereoacuityUnableToComplete != null) CheckBoxOutcomeMethodStereoacuityUnableToComplete.Checked = abstractRecord.OutcomeMethodStereoacuityUnableToComplete.Value;
                if (abstractRecord.OutcomeWorth4DotDistanceDescription != null) DropDownListOutcomeWorth4DotDistanceDescription.SelectedValue = abstractRecord.OutcomeWorth4DotDistanceDescription.ToString();
                if (abstractRecord.OutcomeWorth4DotDistanceUnableToComplete != null) CheckBoxOutcomeWorth4DotDistanceUnableToComplete.Checked = abstractRecord.OutcomeWorth4DotDistanceUnableToComplete.Value;
                if (abstractRecord.OutcomeWorth4DotNearDescription != null) DropDownListOutcomeWorth4DotNearDescription.SelectedValue = abstractRecord.OutcomeWorth4DotNearDescription.ToString();
                if (abstractRecord.OutcomeWorth4DotNearUnableToComplete != null) CheckBoxOutcomeWorth4DotNearUnableToComplete.Checked = abstractRecord.OutcomeWorth4DotNearUnableToComplete.Value;

                if (abstractRecord.PrimaryPositionDistanceRightPD != null) TextBoxPrimaryPositionDistanceRightPD.Text = abstractRecord.PrimaryPositionDistanceRightPD.ToString();
                
                
                
                // 3
                if (abstractRecord.ComplianceAssessedAndDocumented != null)
                {
                    RadioButtonComplianceAssessedAndDocumentedYes.Checked = ((bool)abstractRecord.ComplianceAssessedAndDocumented);
                    RadioButtonComplianceAssessedAndDocumentedNo.Checked = !((bool)abstractRecord.ComplianceAssessedAndDocumented);
                }
                // 4
                if (abstractRecord.ContinuedTreatmentRecommended != null)
                {
                    RadioButtonContinuedTreatmentRecommendedYes.Checked = ((bool)abstractRecord.ContinuedTreatmentRecommended);
                    RadioButtonContinuedTreatmentRecommendedNo.Checked = !((bool)abstractRecord.ContinuedTreatmentRecommended);
                }

                //Complications 
                // 1
                if (abstractRecord.AtropineReactionRequiringDiscontinuation != null)
                {
                    RadioButtonAtropineReactionRequiringDiscontinuationYes.Checked = ((bool)abstractRecord.AtropineReactionRequiringDiscontinuation);
                    RadioButtonAtropineReactionRequiringDiscontinuationNo.Checked = !((bool)abstractRecord.AtropineReactionRequiringDiscontinuation);
                }
                // 2
                if (abstractRecord.ReverseAmblyopia != null)
                {
                    RadioButtonReverseAmblyopiaYes.Checked = ((bool)abstractRecord.ReverseAmblyopia);
                    RadioButtonReverseAmblyopiaNo.Checked = !((bool)abstractRecord.ReverseAmblyopia);
                }
                // 3
                if (abstractRecord.NewOnsetOfStrabismus != null)
                {
                    RadioButtonNewOnsetOfStrabismusYes.Checked = ((bool)abstractRecord.NewOnsetOfStrabismus);
                    RadioButtonNewOnsetOfStrabismusNo.Checked = !((bool)abstractRecord.NewOnsetOfStrabismus);
                }
                // 4
                if (abstractRecord.RecurrenceOfAmblyopia != null)
                {
                    RadioButtonRecurrenceOfAmblyopiaYes.Checked = ((bool)abstractRecord.RecurrenceOfAmblyopia);
                    RadioButtonRecurrenceOfAmblyopiaNo.Checked = !((bool)abstractRecord.RecurrenceOfAmblyopia);
                }
                
            }
            //int exists = (from ca in pim.ChartAbstractionAmblyopia
            //                                            where ca.ParticipantModuleCycleID == cycleID
            //                                            select ca).Count();

        }
    }
    protected void SaveChart()
    {
        using (NetHealthPIMEntities pim = new NetHealthPIMEntities())
        {
            int ModuleID = (int)Session[Constants.SESSION_WORKINGMODULEID];
            int cycleID = ctx.ActiveModuleCycleID;
            ParticipantModuleCycle cycle = pim.ParticipantModuleCycle.First(c => c.ParticipantModuleCycleID == cycleID);
            string recordIdentifier = HiddenFieldRecordIdentifier.Value;
            using (TransactionScope transaction = new TransactionScope())
            {
                ChartAbstractionAmblyopia abstractRecord = (from c in pim.ChartAbstractionAmblyopia
                                                            where c.ParticipantModuleCycle.ParticipantModuleCycleID == cycleID & c.RecordIdentifier == recordIdentifier
                                                select c).First<ChartAbstractionAmblyopia>();
                // History
                if (DropDownListDOBMonth.SelectedIndex > 0) abstractRecord.MonthOfBirth = Convert.ToInt32(DropDownListDOBMonth.SelectedValue);
                else abstractRecord.MonthOfBirth = 0;
                if (DropDownListDOBYear.SelectedIndex > 0) abstractRecord.YearOfBirth = Convert.ToInt32(DropDownListDOBYear.SelectedValue);
                else abstractRecord.YearOfBirth = 0;
                if (DropDownListFirstExamMonth.SelectedIndex > 0) abstractRecord.MonthOfFirstExam = Convert.ToInt32(DropDownListFirstExamMonth.SelectedValue);
                else abstractRecord.MonthOfFirstExam = 0;
                if (DropDownListFirstExamYear.SelectedIndex > 0) abstractRecord.YearOfFirstExam = Convert.ToInt32(DropDownListFirstExamYear.SelectedValue);
                else abstractRecord.YearOfFirstExam = 0;
                if (DropDownListFollowUpMonth.SelectedIndex > 0) abstractRecord.MonthFollowUp = Convert.ToInt32(DropDownListFollowUpMonth.SelectedValue);
                else abstractRecord.MonthFollowUp = 0;
                if (DropDownListFollowUpYear.SelectedIndex > 0) abstractRecord.YearFollowUp = Convert.ToInt32(DropDownListFollowUpYear.SelectedValue);
                else abstractRecord.YearFollowUp = 0;

                //Diagnoses / Assessment 
                // 1
                if (DropDownListBestCorrectedVisualOD.SelectedIndex > 0) abstractRecord.BestCorrectedVisualOD = Convert.ToInt32(DropDownListBestCorrectedVisualOD.SelectedValue);
                else abstractRecord.BestCorrectedVisualOD = 0;
                if (DropDownListBestCorrectedVisualOS.SelectedIndex > 0) abstractRecord.BestCorrectedVisualOS = Convert.ToInt32(DropDownListBestCorrectedVisualOS.SelectedValue);
                else abstractRecord.BestCorrectedVisualOS = 0;
                if (RadioButtonAlligmentOrthoporiaDistance.Checked) abstractRecord.RadioButtonAligmentOrthophoriaDistance = true;
                if (RadioButtonAlligmentOrthoporiaNear.Checked) abstractRecord.RadioButtonAligmentOrthophoriaNear = true;
                //if ((RadioButtonMethodNonSnellenOptotypesYes.Checked==false) && (RadioButtonMethodNonSnellenOptotypesNo.Checked==false))
                //    abstractRecord.MethodNonSnellenOptotypes = null;
                if (DropDownListMethodNonSnellenOptotypesType.SelectedIndex > 0) abstractRecord.MethodNonSnellenOptotypesType = Convert.ToInt32(DropDownListMethodNonSnellenOptotypesType.SelectedValue);
                else abstractRecord.MethodNonSnellenOptotypesType = null;
                if (RadioButtonMethodSnellenOptotypesYes.Checked) abstractRecord.MethodSnellenOptotypes = true;
                if (RadioButtonMethodSnellenOptotypesNo.Checked) abstractRecord.MethodSnellenOptotypes = false;
                if ((RadioButtonMethodSnellenOptotypesYes.Checked == false) && (RadioButtonMethodSnellenOptotypesNo.Checked == false))
                    abstractRecord.MethodSnellenOptotypes = null;
                // 2
                if (RadioButtonSensoryTestingPerformedYes.Checked) abstractRecord.SensoryTestingPerformed = true;
                if (RadioButtonSensoryTestingPerformedNo.Checked) abstractRecord.SensoryTestingPerformed = false;
                // 2a
                try
                {
                    if (Decimal.Parse(TextBoxMethodStereoacuityDescription.Text) >= 0)
                        abstractRecord.MethodStereoacuityDescription = Convert.ToDecimal(TextBoxMethodStereoacuityDescription.Text);
                }
                catch (Exception ex) { 
                    abstractRecord.MethodStereoacuityDescription = null;
                    TextBoxMethodStereoacuityDescription.Text = "";
                }
                abstractRecord.MethodStereoacuityUnableToComplete = CheckBoxMethodStereoacuityUnableToComplete.Checked;
                if (DropDownListWorth4DotDistanceDescription.SelectedIndex > 0) abstractRecord.Worth4DotDistanceDescription = Convert.ToInt32(DropDownListWorth4DotDistanceDescription.SelectedValue);
                abstractRecord.Worth4DotDistanceUnableToComplete = CheckBoxWorth4DotDistanceUnableToComplete.Checked;
                if (DropDownListWorth4DotNearDescription.SelectedIndex > 0) abstractRecord.Worth4DotNearDescription = Convert.ToInt32(DropDownListWorth4DotNearDescription.SelectedValue);
                abstractRecord.Worth4DotNearUnableToComplete = CheckBoxWorth4DotNearUnableToComplete.Checked;
                // 3
                if (RadioButtonAlignmentMeasuredYes.Checked) abstractRecord.AlignmentMeasured = true;
                if (RadioButtonAlignmentMeasuredNo.Checked) abstractRecord.AlignmentMeasured = false;
                // 3a
                try
                {
                    if (Int32.Parse(TextBoxPrimaryPositionDistanceLeftPD.Text) >= 0)
                        abstractRecord.PrimaryPositionDistanceLeftPD = Convert.ToInt32(TextBoxPrimaryPositionDistanceLeftPD.Text);
                }
                catch (Exception ex) { 
                    abstractRecord.PrimaryPositionDistanceLeftPD = null;
                    TextBoxPrimaryPositionDistanceLeftPD.Text = "";
                }

                try
                {
                    if (Int32.Parse(TextBoxPrimaryPositionDistanceRightPD.Text) >= 0)
                        abstractRecord.PrimaryPositionDistanceRightPD = Convert.ToInt32(TextBoxPrimaryPositionDistanceRightPD.Text);
                }
                catch (Exception ex)
                {
                    abstractRecord.PrimaryPositionDistanceRightPD = null;
                    TextBoxPrimaryPositionDistanceRightPD.Text = "";
                }





                if (RadioButtonPrimaryPositionDistanceLeftETYes.Checked) abstractRecord.PrimaryPositionDistanceLeftET = true;
                if (RadioButtonPrimaryPositionDistanceLeftETNo.Checked) abstractRecord.PrimaryPositionDistanceLeftET = false;
               
                try
                {
                    if (Int32.Parse(TextBoxPrimaryPositionNearLeftPD.Text) >= 0)
                        abstractRecord.PrimaryPositionNearLeftPD = Convert.ToInt32(TextBoxPrimaryPositionNearLeftPD.Text);
                }
                catch (Exception ex) { 
                    abstractRecord.PrimaryPositionNearLeftPD = null;
                    TextBoxPrimaryPositionNearLeftPD.Text = "";
                }
                if (RadioButtonPrimaryPositionNearLeftETYes.Checked) abstractRecord.PrimaryPositionNearLeftET = true;
                if (RadioButtonPrimaryPositionNearLeftETNo.Checked) abstractRecord.PrimaryPositionNearLeftET = false;
                try
                {
                    if (Int32.Parse(TextBoxPrimaryPositionNearRightPD.Text) >= 0)
                        abstractRecord.PrimaryPositionNearRightPD = Convert.ToInt32(TextBoxPrimaryPositionNearRightPD.Text);
                }
                catch (Exception ex) { 
                    abstractRecord.PrimaryPositionNearRightPD = null;
                    TextBoxPrimaryPositionNearRightPD.Text = "";
                }
                if (RadioButtonPrimaryPositionNearRightETYes.Checked) abstractRecord.PrimaryPositionNearRightET = true;
                if (RadioButtonPrimaryPositionNearRightETNo.Checked) abstractRecord.PrimaryPositionNearRightET = false;
                // 4
                if (RadioButtonCyclopegicRetinoscopyPerformedYes.Checked) abstractRecord.CyclopegicRetinoscopyPerformed = true;
                if (RadioButtonCyclopegicRetinoscopyPerformedNo.Checked) abstractRecord.CyclopegicRetinoscopyPerformed = false;
                //4a
                try
                {
                    if (Decimal.Parse(TextBoxCycloplegicRefractionODsph.Text) >= 0)
                        abstractRecord.CycloplegicRefractionODsph = Convert.ToDecimal(TextBoxCycloplegicRefractionODsph.Text);
                }
                catch (Exception ex) { 
                    abstractRecord.CycloplegicRefractionODsph = null;
                    TextBoxCycloplegicRefractionODsph.Text = "";
                }
                try
                {
                    if (Decimal.Parse(TextBoxCycloplegicRefractionODcyl.Text) >= 0)
                        abstractRecord.CycloplegicRefractionODcyl = Convert.ToDecimal(TextBoxCycloplegicRefractionODcyl.Text);
                }
                catch (Exception ex) { 
                    abstractRecord.CycloplegicRefractionODcyl = null;
                    TextBoxCycloplegicRefractionODcyl.Text = "";
                }
                try
                {
                    if (Decimal.Parse(TextBoxCycloplegicRefractionODaxis.Text) >= 0)
                        abstractRecord.CycloplegicRefractionODaxis = Decimal.Parse(TextBoxCycloplegicRefractionODaxis.Text);
                }
                catch (Exception ex) { 
                    abstractRecord.CycloplegicRefractionODaxis = null;
                    TextBoxCycloplegicRefractionODaxis.Text = "";
                }
                try
                {
                    if (Decimal.Parse(TextBoxCycloplegicRefractionOSsph.Text) >= 0)
                        abstractRecord.CycloplegicRefractionOSsph = Convert.ToDecimal(TextBoxCycloplegicRefractionOSsph.Text);
                }
                catch (Exception ex) { 
                    abstractRecord.CycloplegicRefractionOSsph = null;
                    TextBoxCycloplegicRefractionOSsph.Text = "";
                }
                try
                {
                    if (Decimal.Parse(TextBoxCycloplegicRefractionOScyl.Text) >= 0)
                        abstractRecord.CycloplegicRefractionOScyl = Convert.ToDecimal(TextBoxCycloplegicRefractionOScyl.Text);
                }
                catch (Exception ex) { 
                    abstractRecord.CycloplegicRefractionOScyl = null;
                    TextBoxCycloplegicRefractionOScyl.Text = "";
                }
                try
                {
                    if (Decimal.Parse(TextBoxCycloplegicRefractionOSaxis.Text) >= 0)
                        abstractRecord.CycloplegicRefractionOSaxis = Decimal.Parse(TextBoxCycloplegicRefractionOSaxis.Text);
                }
                catch (Exception ex) { 
                    abstractRecord.CycloplegicRefractionOSaxis = null;
                    TextBoxCycloplegicRefractionOSaxis.Text = "";
                }
                if (RadioButtonCycloplegicRefractionODcylSignPlus.Checked) abstractRecord.CycloplegicRefractionODsphSign = true;
                if (RadioButtonCycloplegicRefractionODcylSignMinus.Checked) abstractRecord.CycloplegicRefractionODsphSign = false;
                if (RadioButtonTextBoxCycloplegicRefractionOSsphSignPlus.Checked) abstractRecord.CycloplegicRefractionOSsphSign = true;
                if (RadioButtonTextBoxCycloplegicRefractionOSsphSignMinus.Checked) abstractRecord.CycloplegicRefractionOSsphSign = false;

                // Outcomes
                // 1
                if (DropDownListOutcomeBestCorrectedVisualOD.SelectedIndex > 0) abstractRecord.OutcomeBestCorrectedVisualOD = Convert.ToInt32(DropDownListOutcomeBestCorrectedVisualOD.SelectedValue);
                if (DropDownListOutcomeBestCorrectedVisualOS.SelectedIndex > 0) abstractRecord.OutcomeBestCorrectedVisualOS = Convert.ToInt32(DropDownListOutcomeBestCorrectedVisualOS.SelectedValue);
                // 2
                //if (DropDownListOutcomeMethodStereoacuityDescription.SelectedIndex > 0) abstractRecord.OutcomeMethodStereoacuityDescription = Convert.ToInt32(DropDownListOutcomeMethodStereoacuityDescription.SelectedValue);
                try
                {
                    if (Decimal.Parse(TextBoxOutcomeMethodStereoacuityDescription.Text) >= 0)
                        abstractRecord.OutcomeMethodStereoacuityDescription = Convert.ToDecimal(TextBoxOutcomeMethodStereoacuityDescription.Text);
                }
                catch (Exception ex)
                {
                    abstractRecord.OutcomeMethodStereoacuityDescription = null;
                    TextBoxOutcomeMethodStereoacuityDescription.Text = "";
                }
                abstractRecord.OutcomeMethodStereoacuityUnableToComplete = CheckBoxOutcomeMethodStereoacuityUnableToComplete.Checked;
                if (DropDownListOutcomeWorth4DotDistanceDescription.SelectedIndex > 0) abstractRecord.OutcomeWorth4DotDistanceDescription = Convert.ToInt32(DropDownListOutcomeWorth4DotDistanceDescription.SelectedValue);
                abstractRecord.OutcomeWorth4DotDistanceUnableToComplete = CheckBoxOutcomeWorth4DotDistanceUnableToComplete.Checked;
                if (DropDownListOutcomeWorth4DotNearDescription.SelectedIndex > 0) abstractRecord.OutcomeWorth4DotNearDescription = Convert.ToInt32(DropDownListOutcomeWorth4DotNearDescription.SelectedValue);
                abstractRecord.OutcomeWorth4DotNearUnableToComplete = CheckBoxOutcomeWorth4DotNearUnableToComplete.Checked;
                // 3
                if (RadioButtonComplianceAssessedAndDocumentedYes.Checked) abstractRecord.ComplianceAssessedAndDocumented = true;
                if (RadioButtonComplianceAssessedAndDocumentedNo.Checked) abstractRecord.ComplianceAssessedAndDocumented = false;
                // 4
                if (RadioButtonContinuedTreatmentRecommendedYes.Checked) abstractRecord.ContinuedTreatmentRecommended = true;
                if (RadioButtonContinuedTreatmentRecommendedNo.Checked) abstractRecord.ContinuedTreatmentRecommended = false;

                //Complications 
                // 1
                if (RadioButtonAtropineReactionRequiringDiscontinuationYes.Checked) abstractRecord.AtropineReactionRequiringDiscontinuation = true;
                if (RadioButtonAtropineReactionRequiringDiscontinuationNo.Checked) abstractRecord.AtropineReactionRequiringDiscontinuation = false;
                // 2
                if (RadioButtonReverseAmblyopiaYes.Checked) abstractRecord.ReverseAmblyopia = true;
                if (RadioButtonReverseAmblyopiaNo.Checked) abstractRecord.ReverseAmblyopia = false;
                // 3
                if (RadioButtonNewOnsetOfStrabismusYes.Checked) abstractRecord.NewOnsetOfStrabismus = true;
                if (RadioButtonNewOnsetOfStrabismusNo.Checked) abstractRecord.NewOnsetOfStrabismus = false;
                // 4
                if (RadioButtonRecurrenceOfAmblyopiaYes.Checked) abstractRecord.RecurrenceOfAmblyopia = true;
                if (RadioButtonRecurrenceOfAmblyopiaNo.Checked) abstractRecord.RecurrenceOfAmblyopia = false;

                abstractRecord.LastUpdateDate = DateTime.Now;


                bool ChartCompleted = isComplete();
                abstractRecord.Complete = ChartCompleted;
                //var chartRegistrationRecord = (from cr in pim.ChartRegistration
                //                         where cr.ParticipantModuleCycleID == cycle.ParticipantModuleCycleID
                //                          & cr.ModuleID == ModuleID
                //                          & cr.RecordIdentifier == recordIdentifier
                //                         select cr).First<ChartRegistration>();
                //chartRegistrationRecord.AbstractCompleted = ChartCompleted;
                //if ((DropDownListDOBMonth.SelectedIndex > 0) && (DropDownListDOBYear.SelectedIndex > 0)) {
                //    chartRegistrationRecord.DOB = abstractRecord.MonthOfBirth + "/" + abstractRecord.YearOfBirth;
                //}
                //if ((DropDownListFirstExamMonth.SelectedIndex > 0) && (DropDownListFirstExamYear.SelectedIndex > 0)) {
                //    chartRegistrationRecord.InitialVisit = abstractRecord.MonthOfFirstExam + "/" + abstractRecord.YearOfFirstExam;
                //}

                pim.SaveChanges();

                transaction.Complete();
            }

            CycleManager.UpdateChartDates(cycleID, recordIdentifier, Constants.ABO_AMBLYOPIA_MODULEID, true);
            CycleManager.UpdateParticipantCompletedModules(cycleID, ModuleID);

            //var missingChartRegistration = (from cr in pim.ChartRegistration
            //                                where cr.ParticipantModuleCycleID == cycle.ParticipantModuleCycleID
            //                                 & cr.ModuleID == ModuleID
            //                                 & cr.Completed == false
            //                                select cr).Count();
            //if (missingChartRegistration > 0)
                
            //else
            //    Response.Redirect("Dashboard.aspx");
        }
    }


    protected void ButtonSubmit_Click(object sender, EventArgs e)
    {
        SaveChart();
        isComplete();
        if (!isComplete())
        {
            isComplete();
            string script = " var answer = confirm('Chart data is not complete.\\nClick OK to save entries and exit chart.\\nClick CANCEL to save entries and review the chart. '); if (answer==true) window.location = '../PatientChartRegistration.aspx';";
            ScriptManager.RegisterStartupScript(this, GetType(),
                          "ServerControlScript", script, true);
        }
        else
        {

            //using (NetHealthPIMEntities pim = new NetHealthPIMEntities())
            //{
            //    int ModuleID = (int)Session[Constants.SESSION_WORKINGMODULEID];
            //    int cycleID = ctx.ActiveModuleCycleID;
            //    ParticipantModuleCycle cycle = pim.ParticipantModuleCycle.First(c => c.ParticipantModuleCycleID == cycleID);
            //    string recordIdentifier = HiddenFieldRecordIdentifier.Value;
            //    CycleManager.UpdateChartDates(cycleID, recordIdentifier, Constants.ABO_AMBLYOPIA_MODULEID, true);
            //    CycleManager.UpdateParticipantCompletedModules(cycleID, ModuleID);
            //}
            Response.Redirect("../PatientChartRegistration.aspx");
        }
    }
    protected bool isComplete()
    {

        bool ChartCompleted = true;
        LabelMonthOfBirth.Visible = false;

        LabelYearOfBirth.Visible = false;

        LabelMonthOfFirstExam.Visible = false;

        LabelYearOfFirstExam.Visible = false;

        LabelMonthFollowUp.Visible = false;

        LabelYearFollowUp.Visible = false;

        LabelBestCorrectedVisualOD.Visible = false;

        LabelBestCorrectedVisualOS.Visible = false;

        LabelOutcomeBestCorrectedVisualOD.Visible = false;

        LabelOutcomeBestCorrectedVisualOS.Visible = false;




        LabelMethodSnellenOptotypes.Visible = false;

        LabelSensoryTestingPerformed.Visible = false;

        LabelAlignmentMeasured.Visible = false;

        LabelCyclopegicRetinoscopyPerformed.Visible = false;


        LabelContinuedTreatmentRecommended.Visible = false;

        LabelAtropineReactionRequiringDiscontinuation.Visible = false;

        LabelReverseAmblyopia.Visible = false;

        LabelNewOnsetOfStrabismus.Visible = false;

        LabelRecurrenceOfAmblyopia.Visible = false;

        LabelPrimaryPositionDistanceLeftPD.Visible = false;

        LabelPrimaryPositionDistanceRightPD.Visible = false;

        LabelPrimaryPositionNearLeftPD.Visible = false;

        LabelPrimaryPositionNearRightPD.Visible = false;

        LabelCycloplegicRefractionODsph.Visible = false;

        LabelCycloplegicRefractionODcyl.Visible = false;

        LabelCycloplegicRefractionODaxis.Visible = false;

        LabelCycloplegicRefractionOSsph.Visible = false;

        LabelCycloplegicRefractionOScyl.Visible = false;

        LabelCycloplegicRefractionOSaxis.Visible = false;

        LabelSnellenoptotypes.Visible = false;

        LabelPrimaryPositionDistanceLeftET.Visible = false;

        LabelPrimaryPositionNearLeftET.Visible = false;

        LabelCycloplegicRefractionODsphSign.Visible = false;
        Labelinitialfol.Visible = false;
        Labelinitialvis.Visible = false;
        Labelinitialage.Visible = false;
        LabelCycloplegicRefractionOSsphSign.Visible = false;
        NetHealthPIMEntities pim = new NetHealthPIMEntities();
        var moduleinfo = (from c in pim.Module where c.ModuleID == 48 select new { c.RegistrationFollowUpVisitMonths, c.RegistrationMaxAge, c.RegistrationInitialVisitMonths, c.RegistrationMinAge }).FirstOrDefault();
        string message = "Patient must be  at least " + moduleinfo.RegistrationMinAge + " years old<br />";
        string message1 = "Patient cannot be older than " + moduleinfo.RegistrationMaxAge + " years<br />";
        string message2 = "Follow up visit must be at least 3 months after initial visit<br />";
        if (DropDownListFirstExamYear.SelectedIndex != 0 && DropDownListFirstExamMonth.SelectedIndex != 0 && DropDownListFollowUpYear.SelectedIndex != 0 && DropDownListFollowUpMonth.SelectedIndex != 0)
        {
            DateTime monthofsurgery = new DateTime(Convert.ToInt32(DropDownListFirstExamYear.SelectedValue), Convert.ToInt32(DropDownListFirstExamMonth.SelectedValue), 01);
            DateTime folowup = new DateTime(Convert.ToInt32(DropDownListFollowUpYear.SelectedValue), Convert.ToInt32(DropDownListFollowUpMonth.SelectedValue), 01);
            string registrationminage2 = Validation.validateRegistrationFolowUpMonth(monthofsurgery, folowup, Convert.ToInt32(moduleinfo.RegistrationFollowUpVisitMonths), message2);
            string registrationminage = Validation.validateRegistrationMinAge(Convert.ToInt32(DropDownListFirstExamYear.SelectedValue), Convert.ToInt32(DropDownListDOBYear.SelectedValue), Convert.ToInt32(moduleinfo.RegistrationMinAge), message);
            string registrationminage1 = Validation.validateRegistrationMaxAge(Convert.ToInt32(DropDownListFirstExamYear.SelectedValue), Convert.ToInt32(DropDownListDOBYear.SelectedValue), Convert.ToInt32(moduleinfo.RegistrationMaxAge), message);

            if (registrationminage != "")
            {
                Labelinitialage.Text = message;
                Labelinitialage.Visible = true;
                ChartCompleted = false;
            }

            if (registrationminage1 != "")
            {
                Labelinitialvis.Text = message1;
                Labelinitialvis.Visible = true;
                ChartCompleted = false;
            }
            if (registrationminage2 != "")
            {
                Labelinitialfol.Text = message2;
                Labelinitialfol.Visible = true;
                ChartCompleted = false;
            }

        }


        if (DropDownListDOBMonth.SelectedIndex == 0)
        {
            LabelMonthOfBirth.Visible = true;
            ChartCompleted = false;
        }
        if (DropDownListDOBYear.SelectedIndex == 0)
        {
            LabelYearOfBirth.Visible = true;
            ChartCompleted = false;
        }
        if (DropDownListFirstExamMonth.SelectedIndex == 0)
        {
            LabelMonthOfFirstExam.Visible = true;
            ChartCompleted = false;
        }
        if (DropDownListFirstExamYear.SelectedIndex == 0)
        {
            LabelYearOfFirstExam.Visible = true;
            ChartCompleted = false;
        }
        if (DropDownListFollowUpMonth.SelectedIndex == 0)
        {
            LabelMonthFollowUp.Visible = true;
            ChartCompleted = false;
        }
        if (DropDownListFollowUpYear.SelectedIndex == 0)
        {
            LabelYearFollowUp.Visible = true;
            ChartCompleted = false;
        }
        if (DropDownListBestCorrectedVisualOD.SelectedIndex == 0)
        {
            LabelBestCorrectedVisualOD.Visible = true;
            ChartCompleted = false;
        }
        if (DropDownListBestCorrectedVisualOS.SelectedIndex == 0)
        {
            LabelBestCorrectedVisualOS.Visible = true;
            ChartCompleted = false;
        }
        if (DropDownListOutcomeBestCorrectedVisualOD.SelectedIndex == 0)
        {
            LabelOutcomeBestCorrectedVisualOD.Visible = true;
            ChartCompleted = false;
        }
        if (DropDownListOutcomeBestCorrectedVisualOS.SelectedIndex == 0)
        {
            LabelOutcomeBestCorrectedVisualOS.Visible = true;
            ChartCompleted = false;
        }

        if (RadioButtonMethodSnellenOptotypesYes.Checked == false && RadioButtonMethodSnellenOptotypesNo.Checked == false)
        {
            LabelSnellenoptotypes.Visible = true;
            ChartCompleted = false;
        
        }

       
        if (RadioButtonSensoryTestingPerformedNo.Checked == false && RadioButtonSensoryTestingPerformedYes.Checked == false)
        {
            LabelSensoryTestingPerformed.Visible = true;
            ChartCompleted = false;
        }
        if (RadioButtonAlignmentMeasuredNo.Checked == false && RadioButtonAlignmentMeasuredYes.Checked == false)
        {
            LabelAlignmentMeasured.Visible = true;
            ChartCompleted = false;
        }
        if (RadioButtonCyclopegicRetinoscopyPerformedNo.Checked == false && RadioButtonCyclopegicRetinoscopyPerformedYes.Checked == false)
        {
            LabelCyclopegicRetinoscopyPerformed.Visible = true;
            ChartCompleted = false;
        }
       
        if (RadioButtonContinuedTreatmentRecommendedNo.Checked == false && RadioButtonContinuedTreatmentRecommendedYes.Checked == false)
        {
            LabelContinuedTreatmentRecommended.Visible = true;
            ChartCompleted = false;
        }
        if (RadioButtonAtropineReactionRequiringDiscontinuationNo.Checked == false && RadioButtonAtropineReactionRequiringDiscontinuationYes.Checked == false)
        {
            LabelAtropineReactionRequiringDiscontinuation.Visible = true;
            ChartCompleted = false;
        }
        if (RadioButtonReverseAmblyopiaNo.Checked == false && RadioButtonReverseAmblyopiaYes.Checked == false)
        {
            LabelReverseAmblyopia.Visible = true;
            ChartCompleted = false;
        }
        if (RadioButtonNewOnsetOfStrabismusNo.Checked == false && RadioButtonNewOnsetOfStrabismusYes.Checked == false)
        {
            LabelNewOnsetOfStrabismus.Visible = true;
            ChartCompleted = false;
        }
        if (RadioButtonRecurrenceOfAmblyopiaNo.Checked == false && RadioButtonRecurrenceOfAmblyopiaYes.Checked == false)
        {
            LabelRecurrenceOfAmblyopia.Visible = true;
            ChartCompleted = false;
        }

        if (RadioButtonAlignmentMeasuredYes.Checked == true && string.IsNullOrEmpty(TextBoxPrimaryPositionDistanceLeftPD.Text) && RadioButtonAlligmentOrthoporiaDistance.Checked==false)
        {
            LabelPrimaryPositionDistanceLeftPD.Visible = true;
            ChartCompleted = false;
        }

        if (RadioButtonAlignmentMeasuredYes.Checked == true && string.IsNullOrEmpty(TextBoxPrimaryPositionNearLeftPD.Text) && RadioButtonAlligmentOrthoporiaNear.Checked==false)
        {
            LabelPrimaryPositionNearLeftPD.Visible = true;
            ChartCompleted = false;
        }
        if (RadioButtonAlignmentMeasuredYes.Checked == true && string.IsNullOrEmpty(TextBoxPrimaryPositionNearRightPD.Text) && RadioButtonAlligmentOrthoporiaNear.Checked == false)
        {
            LabelPrimaryPositionNearRightPD.Visible = true;
            ChartCompleted = false;
        }
        if (RadioButtonCyclopegicRetinoscopyPerformedYes.Checked == true && string.IsNullOrEmpty(TextBoxCycloplegicRefractionODsph.Text))
        {
            LabelCycloplegicRefractionODsph.Visible = true;
            ChartCompleted = false;
        }
        if (RadioButtonCyclopegicRetinoscopyPerformedYes.Checked == true && string.IsNullOrEmpty(TextBoxCycloplegicRefractionODcyl.Text))
        {
            LabelCycloplegicRefractionODcyl.Visible = true;
            ChartCompleted = false;
        }
        if (RadioButtonCyclopegicRetinoscopyPerformedYes.Checked == true && string.IsNullOrEmpty(TextBoxCycloplegicRefractionODaxis.Text))
        {
            LabelCycloplegicRefractionODaxis.Visible = true;
            ChartCompleted = false;
        }
        if (RadioButtonCyclopegicRetinoscopyPerformedYes.Checked == true && string.IsNullOrEmpty(TextBoxCycloplegicRefractionOSsph.Text))
        {
            LabelCycloplegicRefractionOSsph.Visible = true;
            ChartCompleted = false;
        }
        if (RadioButtonCyclopegicRetinoscopyPerformedYes.Checked == true && string.IsNullOrEmpty(TextBoxCycloplegicRefractionOScyl.Text))
        {
            LabelCycloplegicRefractionOScyl.Visible = true;
            ChartCompleted = false;
        }
        if (RadioButtonCyclopegicRetinoscopyPerformedYes.Checked == true && string.IsNullOrEmpty(TextBoxCycloplegicRefractionOSaxis.Text))
        {
            LabelCycloplegicRefractionOSaxis.Visible = true;
            ChartCompleted = false;
        }



        if (RadioButtonAlignmentMeasuredYes.Checked == true && RadioButtonPrimaryPositionDistanceLeftETNo.Checked == false && RadioButtonPrimaryPositionDistanceLeftETYes.Checked == false && RadioButtonAlligmentOrthoporiaDistance.Checked==false)
        {
            LabelPrimaryPositionDistanceLeftET.Visible = true;
            ChartCompleted = false;
        }
        if (RadioButtonAlignmentMeasuredYes.Checked == true && RadioButtonPrimaryPositionNearLeftETNo.Checked == false && RadioButtonPrimaryPositionNearLeftETYes.Checked == false && RadioButtonAlligmentOrthoporiaNear.Checked==false)
        {
            LabelPrimaryPositionNearLeftET.Visible = true;
            ChartCompleted = false;
        }
        if (RadioButtonCyclopegicRetinoscopyPerformedYes.Checked == true && RadioButtonCycloplegicRefractionODcylSignPlus.Checked == false && RadioButtonCycloplegicRefractionODcylSignMinus.Checked == false)
        {
            LabelCycloplegicRefractionODsphSign.Visible = true;
            ChartCompleted = false;
        }
        if (RadioButtonCyclopegicRetinoscopyPerformedYes.Checked == true && RadioButtonTextBoxCycloplegicRefractionOSsphSignPlus.Checked == false && RadioButtonTextBoxCycloplegicRefractionOSsphSignMinus.Checked == false)
        {
            LabelCycloplegicRefractionOSsphSign.Visible = true;
            ChartCompleted = false;
        }



        return ChartCompleted;
    }



    }

