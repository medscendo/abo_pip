﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Transactions;
using NetHealthPIMModel;

public partial class abo_CMChart : BasePage
{
    protected void Page_Load(object sender, EventArgs e)
    {
    if (!IsPostBack)
        {
            InitializePage();
        }


    }
    protected void InitializePage()
    {
        
        ((PIMMasterPage)Page.Master).SetTitle("Chart");

        using (NetHealthPIMEntities pim = new NetHealthPIMEntities())
        {
            int ModuleID = (int)Session[Constants.SESSION_WORKINGMODULEID];
            Module module = pim.Module.First(c => c.ModuleID == ModuleID);
            ((PIMMasterPage)Page.Master).SetHeader(module.ModuleName + " Chart Abstraction");

            ParticipantModuleCycle prev = (ParticipantModuleCycle)Session[Constants.SESSION_PREVIOUSCYCLE];
            String CycleNumber = Request.QueryString["CycleNumber"];

            int cycleID = ctx.ActiveModuleCycleID;
            if (CycleNumber == "1")
            {
                cycleID = prev.ParticipantModuleCycleID;
                LinkButtonBackToDashboard.Visible = true;
                ButtonSubmit.Visible = false;
            }

            ParticipantModuleCycle cycle = pim.ParticipantModuleCycle.First(c => c.ParticipantModuleCycleID == cycleID);
            String numberOfRecord = Request.QueryString["nr"];
            String totalRecords = Request.QueryString["t"];
            if (cycle.CycleNumber == 1)
            {
                ((PIMMasterPage)Page.Master).SetStatus(2);
                LiteralAbstractionNumber.Text = "Initial Abstraction: Chart " + numberOfRecord + " of " + totalRecords;
            }
            else
            {
                ((PIMMasterPage)Page.Master).SetStatus(3);
                LiteralAbstractionNumber.Text = "Second Abstraction: Chart " + numberOfRecord + " of " + totalRecords;
            }
            DropDownListMonthOfBirth.DataSource = CycleManager.getMonthList();
            DropDownListYearOfBirth.DataSource = CycleManager.getDOBYearList(0, 100);
            DropDownListMonthOfTreatment.DataSource = CycleManager.getMonthList();
            DropDownListYearOfTreatment.DataSource = CycleManager.getDOBYearList(0, 100);





            DataBind();
            DropDownListYearOfBirth.Items.Insert(0, new ListItem("Year", "0"));
          
            DropDownListYearOfTreatment.Items.Insert(0, new ListItem("Year", "0"));
            string recordIdentifier = Request.QueryString["ri"];
            HiddenFieldRecordIdentifier.Value = recordIdentifier;
            LiteralRecordIdentifier.Text = recordIdentifier;
            var count = pim.ChartAbstractionCMelanoma.Where(ps => ps.ParticipantModuleCycle.ParticipantModuleCycleID == cycleID & ps.RecordIdentifier == recordIdentifier).Count();
            if (count == 0)
            {
                using (TransactionScope transaction = new TransactionScope())
                {
                    //Module module = pim.Module.First(m => m.ModuleID == Constants.ABO_AMBLYOPIA_MODULEID);

                    NetHealthPIMModel.ChartRegistration chartRegistration = (from cr in pim.ChartRegistration
                                                           where cr.ParticipantModuleCycleID == cycleID
                                                           & cr.RecordIdentifier == recordIdentifier
                                                           & cr.ModuleID == 43
                                                           select cr).First<NetHealthPIMModel.ChartRegistration>();

                    DateTime initialVisit = Convert.ToDateTime(chartRegistration.InitialVisit);
                    DateTime DOB = Convert.ToDateTime(chartRegistration.DOB);
                    ChartAbstractionCMelanoma abstractRecord = new ChartAbstractionCMelanoma();
                    abstractRecord.ParticipantModuleCycle = cycle;
                    abstractRecord.RecordIdentifier = recordIdentifier;
                    abstractRecord.MonthOfBirth = DOB.Month;
                    abstractRecord.YearOfBirth = DOB.Year;
                    abstractRecord.MonthOfTreatment = initialVisit.Month;
                    abstractRecord.YearOfTreatment = initialVisit.Year;
                    DropDownListYearOfBirth.SelectedValue = abstractRecord.YearOfBirth.ToString();
                    DropDownListMonthOfBirth.SelectedValue = abstractRecord.MonthOfBirth.ToString();

                    DropDownListMonthOfTreatment.SelectedValue = abstractRecord.MonthOfTreatment.ToString();
                    DropDownListYearOfTreatment.SelectedValue = abstractRecord.YearOfTreatment.ToString();
                    abstractRecord.CreatedOn = DateTime.Now;
                    abstractRecord.LastUpdateDate = DateTime.Now;
                    pim.SaveChanges();

                    transaction.Complete();
                }
            }
            else
            {
                ChartAbstractionCMelanoma abstractRecord = (from c in pim.ChartAbstractionCMelanoma  
                                                     where c.ParticipantModuleCycle.ParticipantModuleCycleID == cycleID & c.RecordIdentifier == recordIdentifier
                                                            select c).First<ChartAbstractionCMelanoma>();


                



                if (abstractRecord.RBGender != null) RadioButtonListRBGender.SelectedValue = abstractRecord.RBGender.ToString();
    
                if (abstractRecord.RBSurgeryRadiationForm != null) RadioButtonListRBSurgeryRadiationForm.SelectedValue = abstractRecord.RBSurgeryRadiationForm.ToString();

                if (abstractRecord.RBLSurgeryRadiationOtherNO != null) RadioButtonListSurgeryRadiationOtherNO.SelectedValue = abstractRecord.RBLSurgeryRadiationOtherNO.ToString();

                if (abstractRecord.RBExamTumorThicknessNo != null) RadioButtonListExamTumorThicknessNo.SelectedValue = abstractRecord.RBExamTumorThicknessNo.ToString();



                if (abstractRecord.RBExamBasalDiameterNO !=null) RadioButtonListExamBasalDiameterNO.SelectedValue=abstractRecord.RBExamBasalDiameterNO.ToString();	



                if (abstractRecord.MonthOfBirth != null) DropDownListMonthOfBirth.SelectedValue = abstractRecord.MonthOfBirth.ToString();
                if (abstractRecord.YearOfBirth != null) DropDownListYearOfBirth.SelectedValue = abstractRecord.YearOfBirth.ToString();
                if (abstractRecord.MonthOfTreatment != null) DropDownListMonthOfTreatment.SelectedValue = abstractRecord.MonthOfTreatment.ToString();
                if (abstractRecord.YearOfTreatment != null) DropDownListYearOfTreatment.SelectedValue = abstractRecord.YearOfTreatment.ToString();
              


                if (abstractRecord.ExamPreopLiver != null)
                {
                    RadioButtonExamPreopLiverYes.Checked = ((bool)abstractRecord.ExamPreopLiver);
                    RadioButtonExamPreopLiverNo.Checked = !((bool)abstractRecord.ExamPreopLiver);
                }
                if (abstractRecord.SurgeryBiopsy != null)
                {
                    RadioButtonSurgeryBiopsyYes.Checked = ((bool)abstractRecord.SurgeryBiopsy);
                    RadioButtonSurgeryBiopsyNo.Checked = !((bool)abstractRecord.SurgeryBiopsy);
                }
                if (abstractRecord.SurgeryMuscleDetached != null)
                {
                    RadioButtonSurgeryMuscleDetachedYes.Checked = ((bool)abstractRecord.SurgeryMuscleDetached);
                    RadioButtonSurgeryMuscleDetachedNo.Checked = !((bool)abstractRecord.SurgeryMuscleDetached);
                }
                if (abstractRecord.DocInformedConsent != null)
                {
                    RadioButtonDocInformedConsentYes.Checked = ((bool)abstractRecord.DocInformedConsent);
                    RadioButtonDocInformedConsentNo.Checked = !((bool)abstractRecord.DocInformedConsent);
                }
                if (abstractRecord.DocPCP != null)
                {
                    RadioButtonDocPCPYes.Checked = ((bool)abstractRecord.DocPCP);
                    RadioButtonDocPCPNo.Checked = !((bool)abstractRecord.DocPCP);
                }
                if (abstractRecord.OutcomeSizeIncrease != null)
                {
                    RadioButtonOutcomeSizeIncreaseYes.Checked = ((bool)abstractRecord.OutcomeSizeIncrease);
                    RadioButtonOutcomeSizeIncreaseNo.Checked = !((bool)abstractRecord.OutcomeSizeIncrease);
                }
                if (abstractRecord.OutcomeNeovascularization != null)
                {
                    RadioButtonOutcomeNeovascularizationYes.Checked = ((bool)abstractRecord.OutcomeNeovascularization);
                    RadioButtonOutcomeNeovascularizationNo.Checked = !((bool)abstractRecord.OutcomeNeovascularization);
                }
                if (abstractRecord.OutcomeDiplopia != null)
                {
                    RadioButtonOutcomeDiplopiaYes.Checked = ((bool)abstractRecord.OutcomeDiplopia);
                    RadioButtonOutcomeDiplopiaNo.Checked = !((bool)abstractRecord.OutcomeDiplopia);
                }
                if (abstractRecord.OutcomeEnucleated != null)
                {
                    RadioButtonOutcomeEnucleatedYes.Checked = ((bool)abstractRecord.OutcomeEnucleated);
                    RadioButtonOutcomeEnucleatedNo.Checked = !((bool)abstractRecord.OutcomeEnucleated);
                }
                if (abstractRecord.OutcomeMetastasis != null)
                {
                    RadioButtonOutcomeMetastasisYes.Checked = ((bool)abstractRecord.OutcomeMetastasis);
                    RadioButtonOutcomeMetastasisNo.Checked = !((bool)abstractRecord.OutcomeMetastasis);
                }
                
                if (abstractRecord.SurgeryRadiationDose != null)
                {
                    TextBoxSurgeryRadiationDose.Text=abstractRecord.SurgeryRadiationDose.ToString();

                
                }
                if (abstractRecord.DocMolecularTesting != null)
                {
                    RadioButtonDocMolecularTestingYes.Checked = ((bool)abstractRecord.DocMolecularTesting);
                    RadioButtonDocMolecularTestingNo.Checked = !((bool)abstractRecord.DocMolecularTesting);
                }
                if (abstractRecord.ExamTumorThickness != null) TextBoxExamTumorThickness.Text = abstractRecord.ExamTumorThickness.ToString();
                if (abstractRecord.ExamBasalDiameter != null) TextBoxExamBasalDiameter.Text = abstractRecord.ExamBasalDiameter.ToString();
             
                if (abstractRecord.SurgeryRadiationOther != null) TextBoxSurgeryRadiationOther.Text = abstractRecord.SurgeryRadiationOther.ToString();														
														







            }
        }
    }

    public void savedata()
    {
        using (NetHealthPIMEntities pim = new NetHealthPIMEntities())
        {
            int ModuleID = (int)Session[Constants.SESSION_WORKINGMODULEID];
            int cycleID = ctx.ActiveModuleCycleID;
            ParticipantModuleCycle cycle = pim.ParticipantModuleCycle.First(c => c.ParticipantModuleCycleID == cycleID);
            string recordIdentifier = HiddenFieldRecordIdentifier.Value;
            using (TransactionScope transaction = new TransactionScope())
            {
                ChartAbstractionCMelanoma abstractRecord = (from c in pim.ChartAbstractionCMelanoma
                                                         where c.ParticipantModuleCycle.ParticipantModuleCycleID == cycleID & c.RecordIdentifier == recordIdentifier
                                                            select c).First<ChartAbstractionCMelanoma>();


        ;															
															
															
															
                        if (RadioButtonListRBGender.SelectedIndex != -1) abstractRecord.RBGender = Convert.ToInt32(RadioButtonListRBGender.SelectedValue);															
                        else abstractRecord.RBGender = 0;															
                      														
                        if (RadioButtonListRBSurgeryRadiationForm.SelectedIndex != -1) abstractRecord.RBSurgeryRadiationForm = Convert.ToInt32(RadioButtonListRBSurgeryRadiationForm.SelectedValue);															
                        else abstractRecord.RBSurgeryRadiationForm = 0;

                        if (RadioButtonListSurgeryRadiationOtherNO.SelectedIndex != -1) abstractRecord.RBLSurgeryRadiationOtherNO = Convert.ToInt32(RadioButtonListSurgeryRadiationOtherNO.SelectedValue);
                        else abstractRecord.RBLSurgeryRadiationOtherNO = 0;

                        if (RadioButtonListExamTumorThicknessNo.SelectedIndex != -1) abstractRecord.RBExamTumorThicknessNo = Convert.ToInt32(RadioButtonListExamTumorThicknessNo.SelectedValue);
                        else abstractRecord.RBExamTumorThicknessNo = 0;



                        if (RadioButtonListExamBasalDiameterNO.SelectedIndex != -1) abstractRecord.RBExamBasalDiameterNO = Convert.ToInt32(RadioButtonListExamBasalDiameterNO.SelectedValue);
                        else abstractRecord.RBExamBasalDiameterNO = 0;	
                        															
                        															
                        															
                        if (DropDownListMonthOfBirth.SelectedIndex > 0) abstractRecord.MonthOfBirth = Convert.ToInt32(DropDownListMonthOfBirth.SelectedValue);															
                        else abstractRecord.MonthOfBirth = 0;															
                        if (DropDownListYearOfBirth.SelectedIndex > 0) abstractRecord.YearOfBirth = Convert.ToInt32(DropDownListYearOfBirth.SelectedValue);															
                        else abstractRecord.YearOfBirth = 0;															
                        if (DropDownListMonthOfTreatment.SelectedIndex > 0) abstractRecord.MonthOfTreatment = Convert.ToInt32(DropDownListMonthOfTreatment.SelectedValue);															
                        else abstractRecord.MonthOfTreatment = 0;															
                        if (DropDownListYearOfTreatment.SelectedIndex > 0) abstractRecord.YearOfTreatment = Convert.ToInt32(DropDownListYearOfTreatment.SelectedValue);															
                        else abstractRecord.YearOfTreatment = 0;															
                       

                

                        if (RadioButtonDocMolecularTestingYes.Checked) abstractRecord.DocMolecularTesting = true;
                        if (RadioButtonDocMolecularTestingNo.Checked) abstractRecord.DocMolecularTesting = false;	
				        	
               													
                        if (RadioButtonExamPreopLiverYes.Checked) abstractRecord.ExamPreopLiver = true;															
                        if (RadioButtonExamPreopLiverNo.Checked) abstractRecord.ExamPreopLiver = false;															
                        if (RadioButtonSurgeryBiopsyYes.Checked) abstractRecord.SurgeryBiopsy = true;															
                        if (RadioButtonSurgeryBiopsyNo.Checked) abstractRecord.SurgeryBiopsy = false;															
                        if (RadioButtonSurgeryMuscleDetachedYes.Checked) abstractRecord.SurgeryMuscleDetached = true;															
                        if (RadioButtonSurgeryMuscleDetachedNo.Checked) abstractRecord.SurgeryMuscleDetached = false;															
                        if (RadioButtonDocInformedConsentYes.Checked) abstractRecord.DocInformedConsent = true;															
                        if (RadioButtonDocInformedConsentNo.Checked) abstractRecord.DocInformedConsent = false;															
                        if (RadioButtonDocPCPYes.Checked) abstractRecord.DocPCP = true;															
                        if (RadioButtonDocPCPNo.Checked) abstractRecord.DocPCP = false;															
                        if (RadioButtonOutcomeSizeIncreaseYes.Checked) abstractRecord.OutcomeSizeIncrease = true;															
                        if (RadioButtonOutcomeSizeIncreaseNo.Checked) abstractRecord.OutcomeSizeIncrease = false;															
                        if (RadioButtonOutcomeNeovascularizationYes.Checked) abstractRecord.OutcomeNeovascularization = true;															
                        if (RadioButtonOutcomeNeovascularizationNo.Checked) abstractRecord.OutcomeNeovascularization = false;															
                        if (RadioButtonOutcomeDiplopiaYes.Checked) abstractRecord.OutcomeDiplopia = true;															
                        if (RadioButtonOutcomeDiplopiaNo.Checked) abstractRecord.OutcomeDiplopia = false;															
                        if (RadioButtonOutcomeEnucleatedYes.Checked) abstractRecord.OutcomeEnucleated = true;															
                        if (RadioButtonOutcomeEnucleatedNo.Checked) abstractRecord.OutcomeEnucleated = false;															
                        if (RadioButtonOutcomeMetastasisYes.Checked) abstractRecord.OutcomeMetastasis = true;															
                        if (RadioButtonOutcomeMetastasisNo.Checked) abstractRecord.OutcomeMetastasis = false;



                        try
                        {
                            if (TextBoxSurgeryRadiationDose.Text != null)
                                abstractRecord.SurgeryRadiationDose = Convert.ToDouble(TextBoxSurgeryRadiationDose.Text);
                        }
                        catch (Exception ex)
                        {
                            abstractRecord.SurgeryRadiationDose = null;
                            TextBoxSurgeryRadiationDose.Text = "";
                        }
				
			


                        															
                        															
                        try															
                        {															
                        if (TextBoxExamTumorThickness.Text != null)															
                        abstractRecord.ExamTumorThickness = Convert.ToDouble(TextBoxExamTumorThickness.Text);															
                        }															
                        catch (Exception ex)															
                        {															
                        abstractRecord.ExamTumorThickness = null;															
                        TextBoxExamTumorThickness.Text = "";															
                        }															
                        try															
                        {															
                        if (TextBoxExamBasalDiameter.Text != null)															
                        abstractRecord.ExamBasalDiameter = Convert.ToDouble(TextBoxExamBasalDiameter.Text);															
                        }															
                        catch (Exception ex)															
                        {															
                        abstractRecord.ExamBasalDiameter = null;															
                        TextBoxExamBasalDiameter.Text = "";															
                        }															
                   															
                        try															
                        {															
                        if (TextBoxSurgeryRadiationOther.Text != null)															
                        abstractRecord.SurgeryRadiationOther = TextBoxSurgeryRadiationOther.Text;															
                        }															
                        catch (Exception ex)															
                        {															
                        abstractRecord.SurgeryRadiationOther = null;
                        TextBoxSurgeryRadiationOther.Text = "";															
                        }

                        abstractRecord.LastUpdateDate = DateTime.Now;
                        bool ChartCompleted = isComplete();
                        abstractRecord.Complete = ChartCompleted;
                        var chartRegistration = (from cr in pim.ChartRegistration
                                                 where cr.ParticipantModuleCycleID == cycle.ParticipantModuleCycleID
                                                  & cr.ModuleID == ModuleID
                                                  & cr.RecordIdentifier == recordIdentifier
                                                 select cr).First<NetHealthPIMModel.ChartRegistration>();
                        chartRegistration.AbstractCompleted = ChartCompleted;

                        if ((DropDownListYearOfBirth.SelectedIndex > 0) && (DropDownListYearOfBirth.SelectedIndex > 0))
                        {
                            chartRegistration.DOB = abstractRecord.MonthOfBirth + "/" + abstractRecord.YearOfBirth;
                        }
                     
                        pim.SaveChanges();

                        transaction.Complete();




            }

            CycleManager.UpdateParticipantCompletedModules(cycleID, ModuleID);
        }


    }

    protected void ButtonSubmit_Click(object sender, EventArgs e)
    {
        savedata();
        isComplete();
        if (!isComplete())
        {
            isComplete();
            string script = " var answer = confirm('Chart data is not complete.\\nClick OK to save entries and exit chart.\\nClick CANCEL to save entries and review the chart. '); if (answer==true) window.location = '../PatientChartRegistration.aspx';";
            ScriptManager.RegisterStartupScript(this, GetType(),
                          "ServerControlScript", script, true);
        }
        else
        {

            Response.Redirect("../PatientChartRegistration.aspx");
        }
    }


    protected bool isComplete()
    {

        bool ChartCompleted = true;

        
            LabelRBGender.Visible = false;
          
           
            LabelRBSurgeryRadiationForm.Visible = false;
            
            LabelDocMolecularTesting.Visible = false;
            
             LabelSurgeryRadiationDose.Visible=false;
                
            LabelMonthOfBirth.Visible = false;
          
            LabelYearOfBirth.Visible = false;
           
            LabelMonthOfTreatment.Visible = false;
           
            LabelYearOfTreatment.Visible = false;
            
         
           
            LabelExamTumorThickness.Visible = false;




            LabelExamBasalDiameter.Visible = false;
            LabelExamPreopLiver.Visible = false;
           
            LabelSurgeryBiopsy.Visible = false;
           
            LabelSurgeryMuscleDetached.Visible = false;
           
            LabelDocInformedConsent.Visible = false;
           
            LabelDocPCP.Visible = false;
            
            LabelOutcomeSizeIncrease.Visible = false;
           
            LabelOutcomeNeovascularization.Visible = false;
          
            LabelOutcomeDiplopia.Visible = false;
            
            LabelOutcomeEnucleated.Visible = false;
           
            LabelOutcomeMetastasis.Visible = false;
           

        if (RadioButtonListRBGender.SelectedIndex == -1)
        {
            LabelRBGender.Visible = true;
            ChartCompleted = false;
        }
       
        if (RadioButtonListRBSurgeryRadiationForm.SelectedIndex == -1)
        {
            LabelRBSurgeryRadiationForm.Visible = true;
            ChartCompleted = false;
        }
        if (RadioButtonDocMolecularTestingYes.Checked==false && RadioButtonDocMolecularTestingNo.Checked==false)
        {
            LabelDocMolecularTesting.Visible = true;
            ChartCompleted = false;
        }
      
        if (DropDownListMonthOfBirth.SelectedIndex == 0)
        {
            LabelMonthOfBirth.Visible = true;
            ChartCompleted = false;
        }
        if (DropDownListYearOfBirth.SelectedIndex == 0)
        {
            LabelYearOfBirth.Visible = true;
            ChartCompleted = false;
        }
        if (DropDownListMonthOfTreatment.SelectedIndex == 0 )
        {
            LabelMonthOfTreatment.Visible = true;
            ChartCompleted = false;
        }
        if (DropDownListYearOfTreatment.SelectedIndex == 0)
        {
            LabelYearOfTreatment.Visible = true;
            ChartCompleted = false;
        }

        if (string.IsNullOrEmpty(TextBoxExamTumorThickness.Text) && RadioButtonListExamTumorThicknessNo.SelectedIndex==-1)
        {
            LabelExamTumorThickness.Visible = true;
            ChartCompleted = false;
        }
        if (string.IsNullOrEmpty(TextBoxExamBasalDiameter.Text) && RadioButtonListExamBasalDiameterNO.SelectedIndex==-1)
        {

            LabelExamBasalDiameter.Visible = true;
            ChartCompleted = false;
        
        }



        if (RadioButtonExamPreopLiverNo.Checked == false && RadioButtonExamPreopLiverYes.Checked == false)
        {
            LabelExamPreopLiver.Visible = true;
            ChartCompleted = false;
        }
        if (RadioButtonSurgeryBiopsyNo.Checked == false && RadioButtonSurgeryBiopsyYes.Checked == false)
        {
            LabelSurgeryBiopsy.Visible = true;
            ChartCompleted = false;
        }
        if (RadioButtonSurgeryMuscleDetachedNo.Checked == false && RadioButtonSurgeryMuscleDetachedYes.Checked == false)
        {
            LabelSurgeryMuscleDetached.Visible = true;
            ChartCompleted = false;
        }
        if (RadioButtonDocInformedConsentNo.Checked == false && RadioButtonDocInformedConsentYes.Checked == false)
        {
            LabelDocInformedConsent.Visible = true;
            ChartCompleted = false;
        }
        if (RadioButtonDocPCPNo.Checked == false && RadioButtonDocPCPYes.Checked == false)
        {
            LabelDocPCP.Visible = true;
            ChartCompleted = false;
        }
        if (RadioButtonOutcomeSizeIncreaseNo.Checked == false && RadioButtonOutcomeSizeIncreaseYes.Checked == false)
        {
            LabelOutcomeSizeIncrease.Visible = true;
            ChartCompleted = false;
        }
        if (RadioButtonOutcomeNeovascularizationNo.Checked == false && RadioButtonOutcomeNeovascularizationYes.Checked == false)
        {
            LabelOutcomeNeovascularization.Visible = true;
            ChartCompleted = false;
        }
        if (RadioButtonOutcomeDiplopiaNo.Checked == false && RadioButtonOutcomeDiplopiaYes.Checked == false)
        {
            LabelOutcomeDiplopia.Visible = true;
            ChartCompleted = false;
        }
        if (RadioButtonOutcomeEnucleatedNo.Checked == false && RadioButtonOutcomeEnucleatedYes.Checked == false)
        {
            LabelOutcomeEnucleated.Visible = true;
            ChartCompleted = false;
        }
        if (RadioButtonOutcomeMetastasisNo.Checked == false && RadioButtonOutcomeMetastasisYes.Checked == false)
        {
            LabelOutcomeMetastasis.Visible = true;
            ChartCompleted = false;
        }




        return ChartCompleted;

    }




}

