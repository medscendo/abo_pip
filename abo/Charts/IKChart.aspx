﻿<%@ Page Title="" Language="C#" MasterPageFile="~/PIMMasterPage.master" AutoEventWireup="true" CodeFile="IKChart.aspx.cs" Inherits="abo_IKChart" %>

<asp:Content ID="Content1" ContentPlaceHolderID="head" Runat="Server">

    <link type="text/css" href="../../common/css/atooltip.css" rel="stylesheet"  media="screen" />
	<script type="text/javascript" src="../../common/js/jquery.atooltip.js"></script>
    
    <style type="text/css">
        .bginputa{
	        float:right;
	        margin:21px 0 0 1px;
	        background: url(../common/images/orange_button_with_arrow.png) no-repeat;
	        overflow:hidden;
	        height:35px;

        }
        .bginputa a{
	        color: white;
	        text-align:center;
	        height:35px;
	        line-height:28px;
	        padding:0 14px 15px  ;
	        cursor:pointer;
	        float:left;
	        border:none;
	        text-decoration:none;
	        font-family:Arial, Helvetica, sans-serif; 
	        font-size:12px; 
	        font-weight:bold; 
	        letter-spacing: 0px;
        }
        .bginputa a:hover{
	        text-decoration:none;
        }
        .right {
            text-align:right;
        }
    </style>
    
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder1" Runat="Server">
    <asp:HiddenField ID="HiddenFieldRecordIdentifier" runat="server"/>
        <!-- IK pim -->
        <div class="pim">
            <div class="record-ident clearfix">
                <h3 class="record-first">RECORD IDENTIFIER: <asp:Literal runat="server" ID="LiteralRecordIdentifier"></asp:Literal></h3>
                <h3 class="record-second"><asp:Literal runat="server" ID="LiteralAbstractionNumber"></asp:Literal></h3>
            </div>
            <!-- History -->
            <table>
                <tr>
                    <th colspan="3" width="910px"><p>History</p></th>
                </tr>
                <!-- Question 1 -->
                <tr class="table_body">
                    <td colspan="2"><strong>1. Date of birth:</strong>
                        <br />
                        <asp:Label ID="LabelMonthOfBirth" runat="server" Visible="false"  ForeColor="Red" Text="Please enter Month  "></asp:Label><br />
                        <asp:Label ID="LabelYearOfBirth" runat="server" Visible="false"  ForeColor="Red"  Text="Please enter Year "></asp:Label>
                    </td>
                    
                    <td>
                        <asp:DropDownList ID="DropDownListMonthOfBirth" DataValueField="MonthID" DataTextField="MonthName" runat="server" />
                        <asp:DropDownList ID="DropDownListYearOfBirth" DataValueField="YearID" DataTextField="YearName" runat="server" />
                    </td>
                </tr>
                <!-- Question 2 -->
                <tr class="table_body_bg">
                    <td colspan="2"><strong>2. Date of first exam:</strong>
                        <br />
                        <asp:Label ID="LabelMonthOfExam" runat="server" Visible="false"  ForeColor="Red" Text="Please enter Month  "></asp:Label><br />
                        <asp:Label ID="LabelYearOfExam" runat="server" Visible="false"  ForeColor="Red"  Text="Please enter Year "></asp:Label>
                    </td>
                    <td>
                        <asp:DropDownList ID="DropDownListMonthOfExam" DataValueField="MonthID" DataTextField="MonthName" runat="server" />
                        <asp:DropDownList ID="DropDownListYearOfExam" DataValueField="YearID" DataTextField="YearName" runat="server" />
                    </td>
                </tr>
                <!-- Question 3 -->
                <tr class="table_body">
                    <td colspan="2">
                        <strong>3. Is onset unilateral or bilateral?</strong>
                        <br />(Note: If bilateral, please answer all following questions in regards to the more severely affected eye.)
                        <br />
                        <asp:Label ID="LabelRBOnsetUnilateral" runat="server" Visible="false"  ForeColor="Red"  Text="Please complete "></asp:Label>
                    </td>
                    <td>
                        <asp:RadioButtonList id="RadioButtonListRBOnsetUnilateral" RepeatDirection="Vertical" CssClass="aspxList" runat="server">
	                        <asp:ListItem Value="55">&nbsp;Unilateral</asp:ListItem>
	                        <asp:ListItem Value="56">&nbsp;Bilateral</asp:ListItem>
                        </asp:RadioButtonList>
                    </td>
                </tr>
                <!-- Question 4 -->
                <tr class="table_body_bg">
                    <td colspan="2"><strong>4. Has the patient had previous ocular surgery in the affected eye?</strong>
                        <br />
                        <asp:Label ID="LabelPreviousOcularSurgery" runat="server" Visible="false"  ForeColor="Red"  Text="Please complete "></asp:Label>
                    </td>
                    <td>
                        <asp:RadioButtonList id="RadioButtonListPreviousOcularSurgery" RepeatDirection="Horizontal" CssClass="aspxList" runat="server">
	                        <asp:ListItem Value="1">&nbsp;Yes</asp:ListItem>
	                        <asp:ListItem Value="2">&nbsp;No</asp:ListItem>
	                        <asp:ListItem Value="3">&nbsp;Not Recorded</asp:ListItem>
                        </asp:RadioButtonList>
                    </td>
                </tr>
                <!-- Question 5 -->
                <tr class="table_body">
                    <td colspan="2"><strong>5. Has the patient had prior visual impairment in the affected eye?</strong>
                        <br />
                        <asp:Label ID="LabelPriorVisualImpairment" runat="server" Visible="false"  ForeColor="Red"  Text="Please complete "></asp:Label>
                    </td>
                    <td>
                        <asp:RadioButtonList id="RadioButtonListPriorVisualImpairment" RepeatDirection="Horizontal" CssClass="aspxList" runat="server">
	                        <asp:ListItem Value="1">&nbsp;Yes</asp:ListItem>
	                        <asp:ListItem Value="2">&nbsp;No</asp:ListItem>
	                        <asp:ListItem Value="3">&nbsp;Not Recorded</asp:ListItem>
                        </asp:RadioButtonList>
                    </td>
                </tr>
                <!-- Question 6 -->
                <tr class="table_body_bg">
                    <td colspan="2"><strong>6. Did the patient report a history of trauma?</strong>
                        <br />
                        <asp:Label ID="LabelHistoryTrauma" runat="server" Visible="false"  ForeColor="Red"  Text="Please complete "></asp:Label>
                    </td>
                    <td>
                        <asp:RadioButtonList id="RadioButtonListHistoryTrauma" RepeatDirection="Horizontal" CssClass="aspxList" runat="server">
	                        <asp:ListItem Value="1">&nbsp;Yes</asp:ListItem>
	                        <asp:ListItem Value="2">&nbsp;No</asp:ListItem>
	                        <asp:ListItem Value="3">&nbsp;Not Recorded</asp:ListItem>
                        </asp:RadioButtonList>
                    </td>
                </tr>
                <!-- Question 7 -->
                <tr class="table_body">
                    <td colspan="2"><strong>7. What was the level of pain reported by the patient?&nbsp;<a href="#"><img id="QH7" src="../../common/images/tip.gif" alt="Tip" /></a></strong>
                        <br />
                        <asp:Label ID="LabelPainLevel" runat="server" Visible="false"  ForeColor="Red"  Text="Please complete "></asp:Label>
                    </td>
                    <td>
                        <asp:TextBox runat="server" ID="TextBoxPainLevel" size="5" onkeyup="QH7toggleCB();" /><label> 1-10</label>
                        <br /><asp:CheckBox runat="server" ID="CheckBoxPainLevelNotDocumented" text="&nbsp;Not Documented" class="check" onclick="QH7toggleTB();" />
                    </td>
                </tr>
                <!-- Question 8 -->
                <tr class="table_body_bg">
                    <td colspan="2"><strong>8. Did the patient report a history of contact lens wear?</strong>
                        <br />
                        <asp:Label ID="LabelHistoryContactLens" runat="server" Visible="false"  ForeColor="Red"  Text="Please complete "></asp:Label>
                    </td>
                    <td>
                        <asp:RadioButtonList id="RadioButtonListHistoryContactLens" RepeatDirection="Vertical" CssClass="aspxList" runat="server">
	                        <asp:ListItem Value="2">&nbsp;No</asp:ListItem>
	                        <asp:ListItem Value="57">&nbsp;Yes, rigid lens</asp:ListItem>
	                        <asp:ListItem Value="58">&nbsp;Yes, soft lens</asp:ListItem>
	                        <asp:ListItem Value="3">&nbsp;Not Recorded</asp:ListItem>	               
                        </asp:RadioButtonList>
                    </td>
                </tr>
             
                <!-- Question 8b -->
                <tr class="table_body_bg">
                    <td colspan="2" id="Q8bA">
                        <strong>8a. If yes, overnight lens wear?</strong>
                        <asp:Label ID="LabelRBOvernightLensWear" runat="server" Visible="false"  ForeColor="Red"  Text="<br />Please complete "></asp:Label>
                    </td>
                    <td id="Q8bB">
                        <asp:RadioButtonList id="RadioButtonListRBOvernightLensWear" RepeatDirection="Vertical" CssClass="aspxList" runat="server">
	                        <asp:ListItem Value="1">&nbsp;Yes</asp:ListItem>
	                        <asp:ListItem Value="2">&nbsp;No</asp:ListItem>
	                        <asp:ListItem Value="3">&nbsp;Not Recorded</asp:ListItem>
                            <asp:ListItem Value="48">&nbsp;Not Applicable</asp:ListItem>
                        </asp:RadioButtonList>
                    </td>
                </tr>
                <!-- Question 9 -->
                <tr class="table_body">
                    <td colspan="2"><strong>9. Had antimicrobial therapy been prescribed prior to presentation to your office?</strong>
                        <br />
                        <asp:Label ID="LabelAntimicrobialTherapy" runat="server" Visible="false"  ForeColor="Red"  Text="Please complete "></asp:Label>
                    </td>
                    <td>
                        <asp:RadioButton runat="server" ID="RadioButtonAntimicrobialTherapyYes" GroupName="AntimicrobialTherapy" text="&nbsp;Yes" />&nbsp;&nbsp;
                        <asp:RadioButton runat="server" ID="RadioButtonAntimicrobialTherapyNo" GroupName="AntimicrobialTherapy" text="&nbsp;No" />
                    </td>
                </tr>
                <!-- Question 9a -->
                <tr class="table_body">
                    <td rowspan="2" width="50%"><span id="QH9aA">
                        <strong>9a. If yes, what type of medication was prescribed?</strong>
                        <br />(Check all that apply)
                        </span>
                    </td>
                    <td width="20%"><span class="right" id="QH9aB">Topical</span></td>
                    <td>
                        <span id="QH9aC">
                        <asp:CheckBox runat="server" ID="CheckBoxMedAminoglycoside" text="&nbsp;Aminoglycoside" class="check"/>
                        <br /><asp:CheckBox runat="server" ID="CheckBoxMedAntifungal" text="&nbsp;Antifungal" class="check"/>
                        <br /><asp:CheckBox runat="server" ID="CheckBoxMedAntiparasitic" text="&nbsp;Antiparasitic" class="check"/>
                        <br /><asp:CheckBox runat="server" ID="CheckBoxMedcephalosporin" text="&nbsp;Cephalosporin" class="check"/>
                        <br /><asp:CheckBox runat="server" ID="CheckBoxFluoroquinolone" text="&nbsp;Fluoroquinolone" class="check"/>
                        <br /><asp:CheckBox runat="server" ID="CheckBoxMedMacrolide" text="&nbsp;Macrolide" class="check"/>
                        <br /><asp:CheckBox runat="server" ID="CheckBoxMedVancomycin" text="&nbsp;Vancomycin" class="check"/>
                        <br />Combination drug:
                        <br /><asp:CheckBox runat="server" ID="CheckBoxMedPolymyxin" text="&nbsp;Polymyxin / trimethoprim" class="check"/>
                        <br /><asp:CheckBox runat="server" ID="CheckBoxMedNeomycin" text="&nbsp;Neomycin / polymixin / gramicidin" class="check"/>
                        <br /><asp:CheckBox runat="server" ID="CheckBoxMedOther" text="&nbsp;Other " class="check"/>
                        <asp:TextBox runat="server" ID="TextBoxMedOtherText" size="15" />
                        </span>
                    </td>
                </tr>
                <tr class="table_body">
                    <td><span class="right" id="QH9aD">Systemic</span></td>
                    <td>
                        <span id="QH9aE">
                        <asp:CheckBox runat="server" ID="CheckBoxMedSysAntibacterial" text="&nbsp;Antibacterial " class="check"/>
                        <br /><asp:CheckBox runat="server" ID="CheckBoxMedSysAntifungal" text="&nbsp;Antifungal" class="check"/>
                        <br /><asp:CheckBox runat="server" ID="CheckBoxMedSysAntiparasitic" text="&nbsp;Antiparasitic" class="check"/>
                        </span>
                    </td>
                </tr>
            </table>
            <br />
            <!-- Diagnosis / Assessment -->
            <table>
                <tr>
                    <th colspan="2" width="910px"><p>Diagnosis / Assessment</p></th>
                </tr>
                <!-- Question 1 -->
                <tr class="table_body">
                    <td width="70%"><strong>1. Affected Eye (or more severely affected) eye?</strong>
                        <br />
                        <asp:Label ID="LabelRBAffectedEye" runat="server" Visible="false"  ForeColor="Red"  Text="Please complete "></asp:Label>
                    </td>
                    <td>
                        <asp:RadioButtonList id="RadioButtonListRBAffectedEye" RepeatDirection="Horizontal" CssClass="aspxList" runat="server">
	                        <asp:ListItem Value="49">&nbsp;Right</asp:ListItem>
	                        <asp:ListItem Value="50">&nbsp;Left</asp:ListItem>
                        </asp:RadioButtonList>
                    </td>
                </tr>
                <!-- Question 2 -->
                <tr class="table_body_bg">
                    <td>
                        <strong>2. Corrected distance visual acuity on initial examination</strong>&nbsp;<a href="#"><img id="QD2" src="../../common/images/tip.gif" alt="Tip" /></a>
                        <asp:Label ID="LabelBCVA" runat="server" Visible="false"  ForeColor="Red"  Text="<br />Please complete"></asp:Label>
                    </td>
                    <td>
                        <label>20 / </label><asp:DropDownList ID="DropDownListBCVA" DataValueField="examValue" DataTextField="examLabel" runat="server" onchange="" />
                    </td>
                </tr>
            </table>
            <br />
            <!-- Slit Lamp Examination -->
            <table>
                <tr>
                    <th colspan="2" width="910px"><p>Slit Lamp Examination</p></th>
                </tr>
                <!-- Question 1 -->
                <tr class="table_body">
                    <td width="70%"><strong>1. What was the greatest dimension of the epithelial defect?</strong>
                        <br />
                        <asp:Label ID="LabelRBGreatestDimEpithelial" runat="server" Visible="false"  ForeColor="Red"  Text="Please complete "></asp:Label>
                    </td>
                    <td>
                        <asp:RadioButtonList id="RadioButtonListRBGreatestDimEpithelial" RepeatDirection="Vertical" CssClass="aspxList" runat="server">
	                        <asp:ListItem Value="64">&nbsp;&lt; 2 mm</asp:ListItem>
	                        <asp:ListItem Value="65">&nbsp;2 - 5 mm</asp:ListItem>
	                        <asp:ListItem Value="66">&nbsp;&gt; 5 mm</asp:ListItem>
                            <asp:ListItem Value="128">&nbsp;None Present</asp:ListItem>
	                        <asp:ListItem Value="3">&nbsp;Not Recorded</asp:ListItem>
                        </asp:RadioButtonList>
                    </td>
                </tr>
                <!-- Question 2 -->
                <tr class="table_body_bg">
                    <td>
                        <strong>2. What was the greatest dimension of the infiltrate?</strong>
                        <asp:Label ID="LabelRBGreatestDimInfiltrate" runat="server" Visible="false"  ForeColor="Red"  Text="<br />Please complete"></asp:Label>
                    </td>
                    <td>
                        <asp:RadioButtonList id="RadioButtonListRBGreatestDimInfiltrate" RepeatDirection="Vertical" CssClass="aspxList" runat="server">
	                        <asp:ListItem Value="64">&nbsp;&lt; 2 mm</asp:ListItem>
	                        <asp:ListItem Value="65">&nbsp;2 - 5 mm</asp:ListItem>
	                        <asp:ListItem Value="66">&nbsp;&gt; 5 mm</asp:ListItem>
	                        <asp:ListItem Value="3">&nbsp;Not Recorded</asp:ListItem>
                        </asp:RadioButtonList>
                    </td>
                </tr>
                <!-- Question 3 -->
                <tr class="table_body">
                    <td><strong>3. Did the infiltrate involve the central 4mm of the cornea?</strong>
                        <br />
                        <asp:Label ID="LabelRBInfiltrateCentral4mm" runat="server" Visible="false"  ForeColor="Red"  Text="Please complete "></asp:Label>
                    </td>
                    <td>
                        <asp:RadioButtonList id="RadioButtonListRBInfiltrateCentral4mm" RepeatDirection="Horizontal" CssClass="aspxList" runat="server">
	                        <asp:ListItem Value="1">&nbsp;Yes</asp:ListItem>
	                        <asp:ListItem Value="2">&nbsp;No</asp:ListItem>
	                        <asp:ListItem Value="3">&nbsp;Not Recorded</asp:ListItem>
                        </asp:RadioButtonList>
                    </td>
                </tr>
                <!-- Question 4 -->
                <tr class="table_body_bg">
                    <td><strong>4. Were single or multiple infiltrates present?</strong>
                        <br />
                        <asp:Label ID="LabelRBSingleMultipleInfiltrates" runat="server" Visible="false"  ForeColor="Red"  Text="Please complete "></asp:Label>
                    </td>
                    <td>
                        <asp:RadioButtonList id="RadioButtonListRBSingleMultipleInfiltrates" RepeatDirection="Vertical" CssClass="aspxList" runat="server">
	                        <asp:ListItem Value="62">&nbsp;Single Lesion</asp:ListItem>
	                        <asp:ListItem Value="63">&nbsp;Multiple Lesions</asp:ListItem>
                        </asp:RadioButtonList>
                    </td>
                </tr>
                <!-- Question 5 -->
                <tr class="table_body">
                    <td><strong>5. What was the greatest depth of stromal tissue loss?</strong>
                        <br />
                        <asp:Label ID="LabelRBGreatestDepthStromalTissue" runat="server" Visible="false"  ForeColor="Red"  Text="Please complete "></asp:Label>
                    </td>
                    <td>
                        <asp:RadioButtonList id="RadioButtonListRBGreatestDepthStromalTissue" RepeatDirection="Vertical" CssClass="aspxList" runat="server">
	                        <asp:ListItem Value="128">&nbsp;None</asp:ListItem>
	                        <asp:ListItem Value="64">&nbsp;&lt; 1/3 of the corneal stroma</asp:ListItem>
                            <asp:ListItem Value="65">&nbsp;1/3 to 2/3 of the corneal stroma</asp:ListItem>
                            <asp:ListItem Value="66">&nbsp;&gt; 2/3 of the corneal stroma</asp:ListItem>
	                        <asp:ListItem Value="88">&nbsp;Corneal perforation present</asp:ListItem>
                            <asp:ListItem Value="3">&nbsp;Not Documented</asp:ListItem>
                        </asp:RadioButtonList>
                    </td>
                </tr>
                <!-- Question 6 -->
                <tr class="table_body_bg">
                    <td><strong>6. Was hypopyon present?</strong>
                        <br />
                        <asp:Label ID="LabelRBHypopyonPresent" runat="server" Visible="false"  ForeColor="Red"  Text="Please complete "></asp:Label>
                    </td>
                    <td>
                        <asp:RadioButtonList id="RadioButtonListRBHypopyonPresent" RepeatDirection="Horizontal" CssClass="aspxList" runat="server">
	                        <asp:ListItem Value="1">&nbsp;Yes</asp:ListItem>
	                        <asp:ListItem Value="2">&nbsp;No</asp:ListItem>
                            <asp:ListItem Value="3">&nbsp;Not Documented</asp:ListItem>
                        </asp:RadioButtonList>
                    </td>
                </tr>
            </table>
            <br />
            <!-- Diagnostic Testing -->
            <table>
                <tr>
                    <th colspan="2" width="910px"><p>Diagnostic Testing</p></th>
                </tr>
                <!-- Question 1 -->
                <tr class="table_body">
                    <td width="70%"><strong>1. Was a diagnostic corneal scraping performed? (either by you or by prior physician)</strong>
                        <br />
                        <asp:Label ID="LabelDiagnosticCornealScraping" runat="server" Visible="false"  ForeColor="Red"  Text="Please complete "></asp:Label>
                    </td>
                    <td>
                        <asp:RadioButton runat="server" ID="RadioButtonDiagnosticCornealScrapingYes" GroupName="DiagnosticCornealScraping" text="&nbsp;Yes" />&nbsp;&nbsp;
                        <asp:RadioButton runat="server" ID="RadioButtonDiagnosticCornealScrapingNo" GroupName="DiagnosticCornealScraping" text="&nbsp;No" />
                    </td>
                </tr>
                <!-- Question 2 -->
                <tr class="table_body_bg">
                    <td><strong>2. What type of microbiological evaluation was performed?</strong>
                        <br />
                        <asp:Label ID="LabelMicro" runat="server" Visible="false"  ForeColor="Red"  Text="Please complete "></asp:Label>
                    </td>
                    <td>
                        <asp:CheckBox runat="server" ID="CheckBoxMicroSmear" text="&nbsp;Smear" class="check"/>
                        <br /><asp:CheckBox runat="server" ID="CheckBoxMicroBacterialCulture" text="&nbsp;Bacterial culture" class="check"/>
                        <br /><asp:CheckBox runat="server" ID="CheckBoxMicroFungal" text="&nbsp;Fungal culture" class="check"/>
                        <br /><asp:CheckBox runat="server" ID="CheckBoxMicroAmoeba" text="&nbsp;Amoeba culture" class="check"/>
                        <br /><asp:CheckBox runat="server" ID="CheckBoxMicroViral" text="&nbsp;Viral culture" class="check"/>
                        <br /><asp:CheckBox runat="server" ID="CheckBoxMicroPCR" text="&nbsp;PCR" class="check"/>
                        <br /><asp:CheckBox runat="server" ID="CheckBoxMicroCorneal" text="&nbsp;Corneal biopsy" class="check"/>
                        <br /><asp:CheckBox runat="server" ID="CheckBoxMicroConfocal" text="&nbsp;Confocal microscopy" class="check"/>
                        <br /><asp:CheckBox runat="server" ID="CheckBoxMicroOther" text="&nbsp;Other&nbsp;" class="check"/>
                        <asp:TextBox runat="server" ID="TextBoxMicroOtherText" size="15" />
                        <br /><asp:CheckBox runat="server" ID="CheckBoxMicroNone" text="&nbsp;None" class="check"/>
                    </td>
                </tr>
                <!-- Question 3 -->
                <tr class="table_body">
                    <td><strong>3. Was a working diagnosis documented? </strong>
                        <br />
                        <asp:Label ID="LabelWorkingDiagnosis" runat="server" Visible="false"  ForeColor="Red"  Text="Please complete "></asp:Label>
                    </td>
                    <td>
                        <asp:RadioButton runat="server" ID="RadioButtonWorkingDiagnosisYes" GroupName="RBAffectedEye" text="&nbsp;Yes" />&nbsp;&nbsp;
                        <asp:RadioButton runat="server" ID="RadioButtonWorkingDiagnosisNo" GroupName="RBAffectedEye" text="&nbsp;No" />
                        
                    </td> 
                </tr>
                <!-- Question 3a -->
                <tr class="table_body">
                    <td>
                        <strong><span id="QD3aA">3a. If so, please select one:</span></strong>
                        <asp:Label ID="LabelRBWorkingDiagnosis" runat="server" Visible="false"  ForeColor="Red"  Text="<br />Please complete "></asp:Label>
                    </td>
                    <td>
                        <span id="QD3aB">
                        <asp:RadioButtonList id="RadioButtonListRBWorkingDiagnosis" RepeatDirection="Vertical" CssClass="aspxList" runat="server">
	                        <asp:ListItem Value="67">&nbsp;Bacterial</asp:ListItem>
	                        <asp:ListItem Value="68">&nbsp;Fungal</asp:ListItem>
                            <asp:ListItem Value="69">&nbsp;Parasitic</asp:ListItem>
                            <asp:ListItem Value="3">&nbsp;Not Documented</asp:ListItem>
                        </asp:RadioButtonList>
                        </span>
                    </td>
                </tr>
                <!-- Question 4 -->
                <tr class="table_body_bg">
                    <td><strong>4. Was a causative factor for corneal ulceration identified</strong>&nbsp;<a href="#"><img id="QD4" src="../../common/images/tip.gif" alt="Tip" /></a>
                        <br />
                        <asp:Label ID="LabelRBCausativeFactor" runat="server" Visible="false"  ForeColor="Red"  Text="Please complete "></asp:Label>
                    </td>
                    <td>
                        <asp:RadioButtonList id="RadioButtonListRBCausativeFactor" RepeatDirection="Horizontal" CssClass="aspxList" runat="server">
	                        <asp:ListItem Value="1">&nbsp;Yes</asp:ListItem>
	                        <asp:ListItem Value="2">&nbsp;No</asp:ListItem>
                            <asp:ListItem Value="3">&nbsp;Not Documented</asp:ListItem>
                        </asp:RadioButtonList>
                    </td>
                </tr>
                <!-- Question 4a -->
                <tr class="table_body">
                    <td><span id="QD5aA"><strong>4a. If yes, what was the causative factor?</strong></span>
                        <br />
                        <asp:Label ID="LabelCausative" runat="server" Visible="false"  ForeColor="Red"  Text="Please complete "></asp:Label>
                    </td>
                    <td>
                        <span id="QD5aB">
                        <asp:CheckBox runat="server" ID="CheckBoxCausativeContactLens" text="&nbsp;Contact lens use" class="check"/>
                        <br /><asp:CheckBox runat="server" ID="CheckBoxCausativeDryEye" text="&nbsp;Dry eye / ocular surface disease" class="check"/>
                        <br /><asp:CheckBox runat="server" ID="CheckBoxCausativeCorticosteroid" text="&nbsp;Corticosteroid use" class="check"/>
                        <br /><asp:CheckBox runat="server" ID="CheckBoxCausativeExposureKer" text="&nbsp;Exposure keratopathy / eyelid malposition" class="check"/>
                        <br /><asp:CheckBox runat="server" ID="CheckBoxCausativeNeurotrophicKer" text="&nbsp;Neurotrophic keratitis" class="check"/>
                        <br /><asp:CheckBox runat="server" ID="CheckBoxCausativePostKer" text="&nbsp;Post–keratoplasty" class="check"/>
                        <br /><asp:CheckBox runat="server" ID="CheckBoxCausativeSuture" text="&nbsp;Suture infection" class="check"/>
                        <br /><asp:CheckBox runat="server" ID="CheckBoxCausativeOtherPS" text="&nbsp;Other post-surgical" class="check"/>
                        <br /><asp:CheckBox runat="server" ID="CheckBoxCausativeTrauma" text="&nbsp;Trauma" class="check"/>
                        <br /><asp:CheckBox runat="server" ID="CheckBoxCausativePrior" text="&nbsp;Prior herpes simplex / zoster" class="check"/>
                        <br /><asp:CheckBox runat="server" ID="CheckBoxCausativeOther" text="&nbsp;Other&nbsp;" class="check"/>
                        <asp:TextBox runat="server" ID="TextBoxCausativeOtherText" size="15" />
                        </span>
                    </td>
                </tr>
            </table>
            <br />
            <!-- Management -->
            <table>
                <tr>
                    <th colspan="3" width="910px"><p>Management</p></th>
                </tr>
                <!-- Question 1 -->
                <tr class="table_body">
                    <td rowspan="2" width="50%">
                        <strong>1. Initial antimicrobial therapy</strong>
                        <br />(Check all that apply)
                            <br />
                        <asp:Label ID="LabelInitial" runat="server" Visible="false"  ForeColor="Red"  Text="Please complete "></asp:Label>
                    
                    </td>
                    <td width="20%"><span class="right">Topical</span></td>
                    <td>
                        <asp:CheckBox runat="server" ID="CheckBoxInitialAminoglycoside" text="&nbsp;Aminoglycoside" class="check"/>
                        <br /><asp:CheckBox runat="server" ID="CheckBoxInitialAntifungal" text="&nbsp;Antifungal" class="check"/>
                        <br /><asp:CheckBox runat="server" ID="CheckBoxInitialAntiparasitic" text="&nbsp;Antiparasitic" class="check"/>
                        <br /><asp:CheckBox runat="server" ID="CheckBoxInitialCephalosporin" text="&nbsp;Cephalosporin" class="check"/>
                        <br /><asp:CheckBox runat="server" ID="CheckBoxInitialFluoroquinolone" text="&nbsp;Fluoroquinolone" class="check"/>
                        <br /><asp:CheckBox runat="server" ID="CheckBoxInitialMacrolide" text="&nbsp;Macrolide" class="check"/>
                        <br /><asp:CheckBox runat="server" ID="CheckBoxInitialVancomycin" text="&nbsp;Vancomycin" class="check"/>
                        <br />Combination drug:
                        <br /><asp:CheckBox runat="server" ID="CheckBoxInitialPolymyxin" text="&nbsp;Polymyxin / trimethoprim" class="check"/>
                        <br /><asp:CheckBox runat="server" ID="CheckBoxInitialNeomycin" text="&nbsp;Neomycin / polymixin / gramicidin" class="check"/>
                        <br /><asp:CheckBox runat="server" ID="CheckBoxInitialOther" text="&nbsp;Other " class="check"/>
                        <asp:TextBox runat="server" ID="TextBoxInitialOtherText" size="15" />
                    </td>
                </tr>
                <tr class="table_body">
                    <td><span class="right">Systemic</span></td>
                    <td>
                        <asp:CheckBox runat="server" ID="CheckBoxInitialSysAntibacterial" text="&nbsp;Antibacterial " class="check"/>
                        <br /><asp:CheckBox runat="server" ID="CheckBoxInitialSysAntifungal" text="&nbsp;Antifungal" class="check"/>
                        <br /><asp:CheckBox runat="server" ID="CheckBoxInitialSysAntiparasitic" text="&nbsp;Antiparasitic" class="check"/>
                </tr>
                <!-- Question 2 -->
                <tr class="table_body_bg">
                    <td colspan="2">
                        <strong>2. Frequency of antimicrobial eye drop administration</strong>
                        <br />(Note: if two medications were prescribed and each prescribed to be given every two hours, then consider this hourly administration)
                            <br />
                        <asp:Label ID="LabelRBFrequencyEyeDrop" runat="server" Visible="false"  ForeColor="Red"  Text="Please complete "></asp:Label>
                    
                    </td>
                    <td>
                        <asp:RadioButtonList id="RadioButtonListRBFrequencyEyeDrop" RepeatDirection="Vertical" CssClass="aspxList" runat="server">
	                        <asp:ListItem Value="70">&nbsp;Hourly or more frequent</asp:ListItem>
	                        <asp:ListItem Value="71">&nbsp;Every two hours</asp:ListItem>
                            <asp:ListItem Value="72">&nbsp;Less frequent than every two hours</asp:ListItem>
                        </asp:RadioButtonList>
                    </td>
                </tr>
                <!-- Question 3 -->
                <tr class="table_body">
                    <td colspan="2"><strong>3. Was topical antimicrobial therapy prescribed around the clock?</strong>
                        <br />
                        <asp:Label ID="LabelRBTopicalPrescribed" runat="server" Visible="false"  ForeColor="Red"  Text="Please complete "></asp:Label>
                    </td>
                    <td>
                        <asp:RadioButtonList id="RadioButtonListRBTopicalPrescribed" RepeatDirection="Horizontal" CssClass="aspxList" runat="server">
	                        <asp:ListItem Value="1">&nbsp;Yes</asp:ListItem>
	                        <asp:ListItem Value="2">&nbsp;No</asp:ListItem>
	                        <asp:ListItem Value="3">&nbsp;Not Recorded</asp:ListItem>
                        </asp:RadioButtonList>
                    </td>
                </tr>
                <!-- Question 4 -->
                <tr class="table_body_bg">
                    <td colspan="2"><strong>4. If initial therapy included medication other than empiric broad-spectrum antibacterial drops, is rationale documented?</strong>
                        <br />
                        <asp:Label ID="LabelInitialTherapyNotEmpiric" runat="server" Visible="false"  ForeColor="Red"  Text="Please complete "></asp:Label>
                    </td>
                    <td>
                        <asp:RadioButtonList id="RadioButtonListInitialTherapyNotEmpiric" RepeatDirection="Horizontal" CssClass="aspxList" runat="server">
	                        <asp:ListItem Value="1">&nbsp;Yes</asp:ListItem>
	                        <asp:ListItem Value="2">&nbsp;No</asp:ListItem>
	                        <asp:ListItem Value="48">&nbsp;Not applicable</asp:ListItem>
                        </asp:RadioButtonList>
                    </td>
                </tr>
                <!-- Question 5 -->
                <tr class="table_body">
                    <td colspan="2">
                        <strong>5. What were the findings of the microbiologic workup?</strong>
                        <br />(Check all that apply)    
                            <br />
                        <asp:Label ID="LabelFind" runat="server" Visible="false"  ForeColor="Red"  Text="Please complete "></asp:Label>
                    
                    </td>
                    <td>
                        <asp:CheckBox runat="server" ID="CheckBoxFindAcanthamoeba" text="&nbsp;Acanthamoeba " class="check"/>
                        <br /><asp:CheckBox runat="server" ID="CheckBoxFindFilamentous" text="&nbsp;Filamentous fungus" class="check"/>
                        <br /><asp:CheckBox runat="server" ID="CheckBoxFindGramPositive" text="&nbsp;Gram positive bacteria" class="check"/>
                        <br /><asp:CheckBox runat="server" ID="CheckBoxFindGramNegative" text="&nbsp;Gram negative bacteria" class="check"/>
                        <br /><asp:CheckBox runat="server" ID="CheckBoxFindMixedOrganisms" text="&nbsp;Mixed organisms" class="check"/>
                        <br /><asp:CheckBox runat="server" ID="CheckBoxFindMycobacteria" text="&nbsp;Mycobacteria" class="check"/>
                        <br /><asp:CheckBox runat="server" ID="CheckBoxFindNocardia" text="&nbsp;Nocardia" class="check"/>
                        <br /><asp:CheckBox runat="server" ID="CheckBoxFindYeast" text="&nbsp;Yeast" class="check"/>
                        <br /><asp:CheckBox runat="server" ID="CheckBoxFindNoOrganisms" text="&nbsp;No organisms identified" class="check"/>
                        <br /><asp:CheckBox runat="server" ID="CheckBoxFindDiagnostic" text="&nbsp;Diagnostic workup not performed" class="check"/>
                        <br /><asp:CheckBox runat="server" ID="CheckBoxFindOther" text="&nbsp;Other&nbsp;" class="check"/>
                        <asp:TextBox runat="server" ID="TextBoxFindOtherText" size="15" />
                    </td>
                </tr>
                <!-- Question 6 -->
                <tr class="table_body_bg">
                    <td colspan="2"><strong>6. How many days after the initial visit was the first follow-up examination conducted?</strong>
                        <br />
                        <asp:Label ID="LabelDaysUntilInitialVisit" runat="server" Visible="false"  ForeColor="Red"  Text="Please complete "></asp:Label>
                    </td>
                    <td>
                        <label>Number of days after initial visit </label>
                        <asp:TextBox runat="server" ID="TextBoxDaysUntilInitialVisit" size="5" />
                    </td>
                </tr>
                <!-- Question 7 -->
                <tr class="table_body">
                    <td colspan="2"><strong>7. Was a change in medication prescribed based upon results of corneal scraping or initial clinical course?</strong>
                        <br />
                        <asp:Label ID="LabelChangeMed" runat="server" Visible="false"  ForeColor="Red"  Text="Please complete "></asp:Label>
                    </td>
                    <td>
                        <asp:RadioButton runat="server" ID="RadioButtonChangeMedYes" GroupName="ChangeMed" text="&nbsp;Yes" />&nbsp;&nbsp;
                        <asp:RadioButton runat="server" ID="RadioButtonChangeMedNo" GroupName="ChangeMed" text="&nbsp;No" />
                    </td>
                </tr>
                <!-- Question 7a -->
                <tr class="table_body">
                    <td colspan="2"><strong><span id="QM7aA">7a. If yes, please list date of first medication change:</span></strong>
                        <br />
                        <asp:Label ID="LabelMedChange" runat="server" Visible="false"  ForeColor="Red"  Text="Please complete "></asp:Label>
                    </td>
                    <td>
                        <span id="QM7aB">
                        <asp:DropDownList ID="DropDownListMedChangeMonth" DataValueField="MonthID" DataTextField="MonthName" runat="server" />
                        <asp:DropDownList ID="DropDownListMedChangeYear" DataValueField="YearID" DataTextField="YearName" runat="server" />                       
                        </span>
                    </td>
                </tr>
                <!-- Question 7b -->
                <tr class="table_body">
                    <td rowspan="2">
                        <span id="QM7bA"><strong>7b. If yes, list medications added or discontinued</strong>
                        <br />(Check all that apply)</span>
                            <br />
                        <asp:Label ID="LabelMedAdd" runat="server" Visible="false"  ForeColor="Red"  Text="Please complete "></asp:Label>
                    
                    </td>
                    <td><span class="right" id="QM7bB">Topical</span></td>
                    <td>
                    <span id="QM7bC">
                        Aminoglycoside
                        <br /><asp:CheckBox runat="server" ID="CheckBoxMedDiscAminoglycoside"  text="&nbsp;added" style="margin-left: 30px;"/>&nbsp;&nbsp;
                        <asp:CheckBox runat="server" ID="CheckBoxMedAddAminoglycoside" text="&nbsp;discontinued" />
                        <br />Antifungal
                        <br /><asp:CheckBox runat="server" ID="CheckBoxMedDiscAntifungal" text="&nbsp;added" style="margin-left: 30px;"/>&nbsp;&nbsp;
                        <asp:CheckBox runat="server" ID="CheckBoxMedAddAntifungal" text="&nbsp;discontinued" />
                        <br />Antiparasitic
                        <br /><asp:CheckBox runat="server" ID="CheckBoxMedDiscAntiparasitic" text="&nbsp;added" style="margin-left: 30px;"/>&nbsp;&nbsp;
                        <asp:CheckBox runat="server" ID="CheckBoxMedAddAntiparasitic" text="&nbsp;discontinued" />
                        <br />Cephalosporin
                        <br /><asp:CheckBox runat="server" ID="CheckBoxMedDiscCephalosporin" text="&nbsp;added" style="margin-left: 30px;"/>&nbsp;&nbsp;
                        <asp:CheckBox runat="server" ID="CheckBoxMedAddCephalosporin" text="&nbsp;discontinued" />
                        <br />Fluoroquinolone
                        <br /><asp:CheckBox runat="server" ID="CheckBoxMedDiscFluoroquinolone" text="&nbsp;added" style="margin-left: 30px;"/>&nbsp;&nbsp;
                        <asp:CheckBox runat="server" ID="CheckBoxMedAddFluoroquinolone" text="&nbsp;discontinued" />
                        <br />Macrolide
                        <br /><asp:CheckBox runat="server" ID="CheckBoxMedDiscMacrolide" text="&nbsp;added" style="margin-left: 30px;"/>&nbsp;&nbsp;
                        <asp:CheckBox runat="server" ID="CheckBoxMedAddMacrolide" text="&nbsp;discontinued" />
                        <br />Vancomycin
                        <br /><asp:CheckBox runat="server" ID="CheckBoxMedDiscVancomycin" text="&nbsp;added" style="margin-left: 30px;"/>&nbsp;&nbsp;
                        <asp:CheckBox runat="server" ID="CheckBoxMedAddVancomycin" text="&nbsp;discontinued" />
                        <br />Combination drug
                        <br />Polymyxin / Trimethoprim
                        <br /><asp:CheckBox runat="server" ID="CheckBoxMedDiscPolymyxin" text="&nbsp;added" style="margin-left: 30px;"/>&nbsp;&nbsp;
                        <asp:CheckBox runat="server" ID="CheckBoxMedAddPolymyxin" text="&nbsp;discontinued" />
                        <br />Neomycin / Polymyxin / Gramicidin
                        <br /><asp:CheckBox runat="server" ID="CheckBoxMedAddNeomycin" text="&nbsp;added" style="margin-left: 30px;"/>&nbsp;&nbsp;
                        <asp:CheckBox runat="server" ID="CheckBoxMedDiscNeomycin" text="&nbsp;discontinued" />
                        <br />Other: 
                        <asp:TextBox runat="server" ID="TextBoxMedAddOtherText" size="20" />
                        <br /><asp:CheckBox runat="server" ID="CheckBoxMedDiscOther" text="&nbsp;added" style="margin-left: 30px;"/>&nbsp;&nbsp;
                        <asp:CheckBox runat="server" ID="CheckBoxMedAddOther" text="&nbsp;discontinued" />
                    </span>
                    </td>   
                </tr>
                <tr class="table_body">
                    <td><span class="right" id="QM7bD">Systemic</span></td>
                    <td> <!-- Convert these radio buttons back to checkboxes and finish the jquery method, I am in the middle of a few and I still need to do the other function for the other function here -->
                        <span id="QM7bE">
                        <br />Antibacterial
                        <br /><asp:CheckBox runat="server" ID="CheckBoxMedDiscSysAntibacterial" text="&nbsp;added" style="margin-left: 30px;"/>&nbsp;&nbsp;
                        <asp:CheckBox runat="server" ID="CheckBoxMedAddSysAntibacterial" text="&nbsp;discontinued" />
                        <br />Antifungal
                        <br /><asp:CheckBox runat="server" ID="CheckBoxMedDiscSysAntifungal" text="&nbsp;added" style="margin-left: 30px;"/>&nbsp;&nbsp;
                        <asp:CheckBox runat="server" ID="CheckBoxMedAddSysAntifungal" text="&nbsp;discontinued" />
                        <br />Antiparasitic
                        <br /><asp:CheckBox runat="server" ID="CheckBoxMedAddSysAntiparasitic" text="&nbsp;added" style="margin-left: 30px;"/>&nbsp;&nbsp;
                        <asp:CheckBox runat="server" ID="CheckBoxMedDiscSysAntiparasitic" text="&nbsp;discontinued" />
                        </span>
                    </td>
                </tr>
                <!-- Question 7c -->
                <tr class="table_body">
                    <td colspan="2"><strong><span id="QM7cA">7c. If yes, what was the rationale for change in management?</span></strong>
                        <br />
                        <asp:Label ID="LabelRBMedChangeRationale" runat="server" Visible="false"  ForeColor="Red"  Text="Please complete "></asp:Label>
                    </td>
                    <td>
                        <span id="QM7cB">
                        <asp:RadioButtonList id="RadioButtonListRBMedChangeRationale" RepeatDirection="Vertical" CssClass="aspxList" runat="server">
	                        <asp:ListItem Value="73">&nbsp;Microbiology results</asp:ListItem>
	                        <asp:ListItem Value="74">&nbsp;Clinical progression or non-responsiveness to therapy</asp:ListItem>
	                        <asp:ListItem Value="75">&nbsp;Non-compliance or intolerance of therapy (includes medication toxicity or in order to prevent medication toxicity)</asp:ListItem>
                        </asp:RadioButtonList>
                        </span>
                    </td>
                </tr>
                <!-- Question 8 -->
                <tr class="table_body_bg">
                    <td colspan="2"><strong>8. Were topical corticosteroids prescribed?</strong>
                        <br />
                        <asp:Label ID="LabelMedTopicalCorticosteroids" runat="server" Visible="false"  ForeColor="Red"  Text="Please complete "></asp:Label>
                    </td>
                    <td>
                        <asp:RadioButton runat="server" ID="RadioButtonMedTopicalCorticosteroidsYes" GroupName="MedTopicalCorticosteroids" text="&nbsp;Yes" />&nbsp;&nbsp;
                        <asp:RadioButton runat="server" ID="RadioButtonMedTopicalCorticosteroidsNo" GroupName="MedTopicalCorticosteroids" text="&nbsp;No" />
                    </td>
                </tr>
                <!-- Question 8a -->
                <tr class="table_body_bg">
                    <td colspan="2"><strong><span id="QM8aA">8a. If yes, please list date started:</span></strong>
                    <br />
                        <asp:Label ID="LabelMedTopicalMonth" runat="server" Visible="false"  ForeColor="Red"  Text="Please complete Month "></asp:Label><br />
                    
                    </td>
                    <td>
                        <span id="QM8aB">
                        <asp:DropDownList ID="DropDownListMedTopicalMonth" DataValueField="MonthID" DataTextField="MonthName" runat="server" />
                        <asp:DropDownList ID="DropDownListMedTopicalYear" DataValueField="YearID" DataTextField="YearName" runat="server" />
                        </span>
                    </td>
                </tr>
            </table>
            <br />
            <!-- Outcomes and Complications -->
            <table>
                <tr>
                    <th colspan="2" width="910px"><p>Outcomes and Complications</p></th>
                </tr>
                <!-- Question 1 -->
                <tr class="table_body">
                    <td width="70%"><strong>1. Did a corneal perforation occur?</strong>
                        <br />
                        <asp:Label ID="LabelCornealPerforation" runat="server" Visible="false"  ForeColor="Red"  Text="Please complete "></asp:Label>
                    </td>
                    <td>
                        <asp:RadioButton runat="server" ID="RadioButtonCornealPerforationYes" GroupName="CornealPerforation" text="&nbsp;Yes" />&nbsp;&nbsp;
                        <asp:RadioButton runat="server" ID="RadioButtonCornealPerforationNo" GroupName="CornealPerforation" text="&nbsp;No" />
                    </td>
                </tr>
                <!-- Question 2 -->
                <tr class="table_body_bg">
                    <td><strong>2. Was a corneal biopsy or corneal scraping performed after your initial antimicrobial therapy was started?</strong>
                        <br />
                        <asp:Label ID="LabelRBCornealBiopsy" runat="server" Visible="false"  ForeColor="Red"  Text="Please complete "></asp:Label>
                    </td>
                    <td>
                        <asp:RadioButtonList id="RadioButtonListRBCornealBiopsy" RepeatDirection="Vertical" CssClass="aspxList" runat="server">
	                        <asp:ListItem Value="76">&nbsp;Corneal biopsy</asp:ListItem>
	                        <asp:ListItem Value="77">&nbsp;Microbiologic examination</asp:ListItem>
	                        <asp:ListItem Value="2">&nbsp;No</asp:ListItem>
                        </asp:RadioButtonList>
                    </td>
                </tr>
                <!-- Question 2a -->
                <tr class="table_body">
                    <td><strong><span id="QO2aA">2a. If yes, were pathologic findings documented?</span></strong>
                        <br />
                        <asp:Label ID="LabelPathologicFindingsDocuments" runat="server" Visible="false"  ForeColor="Red"  Text="Please complete "></asp:Label>
                    </td>
                    <td>
                        <span id="QO2aB">
                        <asp:RadioButtonList id="RadioButtonListPathologicFindingsDocuments" RepeatDirection="Horizontal" CssClass="aspxList" runat="server">
	                        <asp:ListItem Value="1">&nbsp;Yes</asp:ListItem>
	                        <asp:ListItem Value="2">&nbsp;No</asp:ListItem>
	                        <asp:ListItem Value="48">&nbsp;Not applicable</asp:ListItem>
                        </asp:RadioButtonList>
                        </span>
                    </td>
                </tr>
                <!-- Question 2b -->
                <tr class="table_body_bg">
                    <td><strong><span id="QO2bA">2b. If yes, what types of pathologic findings were detected?</span></strong>
                        <br />
                        <asp:Label ID="LabelPath" runat="server" Visible="false"  ForeColor="Red"  Text="Please complete "></asp:Label>
                    </td>
                    <td>
                        <span id="QO2bB">
                        <asp:CheckBox runat="server" ID="CheckBoxPathAcanthamoeba" text="&nbsp;Acanthamoeba" class="check"/>
                        <br /><asp:CheckBox runat="server" ID="CheckBoxPathFilamentous" text="&nbsp;Filamentous fungus" class="check"/>
                        <br /><asp:CheckBox runat="server" ID="CheckBoxPathGramPositive" text="&nbsp;Gram positive Bacteria" class="check"/>
                        <br /><asp:CheckBox runat="server" ID="CheckBoxPathGramNegative" text="&nbsp;Gram negative bacteria" class="check"/>
                        <br /><asp:CheckBox runat="server" ID="CheckBoxPathMixedOrganisms" text="&nbsp;Mixed Organisms" class="check"/>
                        <br /><asp:CheckBox runat="server" ID="CheckBoxPathMycobacteria" text="&nbsp;Mycobacteria" class="check"/>
                        <br /><asp:CheckBox runat="server" ID="CheckBoxPathNocardia" text="&nbsp;Nocardia" class="check"/>
                        <br /><asp:CheckBox runat="server" ID="CheckBoxPathYeast" text="&nbsp;Yeast" class="check"/>
                        <br /><asp:CheckBox runat="server" ID="CheckBoxPathNoOrganisms" text="&nbsp;No Organisms" class="check"/>
                        <br /><asp:CheckBox runat="server" ID="CheckBoxOther" text="&nbsp;Other:" class="check"/>&nbsp;
                        <asp:TextBox runat="server" ID="TextBoxPathOtherText" size="20" />
                        </span>
                    </td>
                </tr>
                <!-- Question 3 -->
                <tr class="table_body">
                    <td><strong>3. Was therapeutic surgical intervention required to treat infection or perforation?</strong>
                        <br />
                        <asp:Label ID="LabelRBTherapeuticSurg" runat="server" Visible="false"  ForeColor="Red"  Text="Please complete "></asp:Label>
                    </td>
                    <td>
                        <asp:RadioButtonList id="RadioButtonListRBTherapeuticSurg" RepeatDirection="Vertical" CssClass="aspxList" runat="server">
	                        <asp:ListItem Value="78">&nbsp;Penetrating keratoplasty</asp:ListItem>
	                        <asp:ListItem Value="79">&nbsp;Lamellar keratoplasty</asp:ListItem>
	                        <asp:ListItem Value="80">&nbsp;Corneal adhesives</asp:ListItem>
                            <asp:ListItem Value="81">&nbsp;Conjunctival flap</asp:ListItem>
                            <asp:ListItem Value="82">&nbsp;Enucleation / evisceration</asp:ListItem>
	                        <asp:ListItem Value="2">&nbsp;No</asp:ListItem>
	                        <asp:ListItem Value="14">&nbsp;Other</asp:ListItem>
                        </asp:RadioButtonList>
                        <label>Other: </label>
                        <asp:TextBox runat="server" ID="TextBoxTherapeuticSurgOther" size="25" />
                    </td>
                </tr>
                <!-- Question 4 -->
                <tr class="table_body_bg">
                    <td><strong>4. Was surgical intervention performed for optical rehabilitation?</strong>
                        <br />
                        <asp:Label ID="LabelSurgicalIntOpticalRehab" runat="server" Visible="false"  ForeColor="Red"  Text="Please complete "></asp:Label>
                    </td>
                    <td>
                        <asp:RadioButton runat="server" ID="RadioButtonSurgicalIntOpticalRehabYes" GroupName="SurgicalIntOpticalRehab" text="&nbsp;Yes" />&nbsp;&nbsp;
                        <asp:RadioButton runat="server" ID="RadioButtonSurgicalIntOpticalRehabNo" GroupName="SurgicalIntOpticalRehab" text="&nbsp;No" />
                    </td>
                </tr>
                <!-- Question 5 -->
                <tr class="table_body">
                    <td>
                        <strong>5. Final spectacle or contact lens corrected distance visual acuity</strong>&nbsp;<a href="#"><img id="QO5" src="../../common/images/tip.gif" alt="Tip" /></a>
                        <br />(3-12 months after clinical resolution or subsequent surgery)
                        <asp:Label ID="LabelFinalBCVA" runat="server" Visible="false"  ForeColor="Red"  Text="<br />Please complete "></asp:Label>
                    </td>
                    <td>
                        <label>20 / </label>
                        <asp:DropDownList ID="DropDownListFinalBCVA" DataValueField="examValue" DataTextField="examLabel" runat="server" />
                        <br />
                        <br />Duration from initial presentation to final acuity: 
                          <asp:Label ID="LabelDurationInitialFinalMonths" runat="server" Visible="false"  ForeColor="Red"  Text="Please complete "></asp:Label>
                        <asp:TextBox runat="server" ID="TextBoxDurationInitialFinalMonths" size="3" /><label> # of months</label>
                    </td>
                </tr>
                <!-- Question 6 -->
                <tr class="table_body_bg">
                    <td><strong>6. Does documentation exist that the patient was counseled regarding elimination of risk factors for infection&nbsp;<a href="#"><img id="QO8" src="../../common/images/tip.gif" alt="Tip" /></a> or was predisposing factor to infection addressed and/or corrected</strong> (i.e.  correction of eyelid malposition?)
                        <br />
                        <asp:Label ID="LabelRBPatientCounseled" runat="server" Visible="false"  ForeColor="Red"  Text="Please complete "></asp:Label>
                    </td>
                    <td>
                        <asp:RadioButtonList id="RadioButtonListRBPatientCounseled" RepeatDirection="Vertical" CssClass="aspxList" runat="server">
	                        <asp:ListItem Value="1">&nbsp;Yes</asp:ListItem>
	                        <asp:ListItem Value="2">&nbsp;No</asp:ListItem>
	                        <asp:ListItem Value="83">&nbsp;Risk factor not identified</asp:ListItem>
                        </asp:RadioButtonList>
                    </td>
                </tr>
            </table>
            <div class="button-box">
                <asp:LinkButton ID="LinkButtonBackToDashboard" runat="server" Text="Back To Chart Registration" PostBackUrl="../PatientChartRegistration.aspx?CycleNumber=1" Visible="false" CssClass="button" />
                <asp:ImageButton ID="ButtonSubmit" OnClick="ButtonSubmit_Click" runat="server" AlternateText="Submit Chart" CssClass="button" />
            </div>
        </div>
        <!-- CUK pim ends -->
</asp:Content>
<asp:Content runat="server" ID="Content4" ContentPlaceHolderID="javascript">
    <script type="text/javascript" language="javascript">
        //      Either enables or disables input boxes
        function enabledControl(el, disabled, checked) {
            //alert("Hello world from enabled control");
            try {
                if (disabled) {
                    el.disabled = disabled;
                    el.setAttribute("disabled", true);
                }
                else {
                    el.disabled = disabled;
                    el.removeAttribute("disabled");
                }

                if (checked == true)
                    el.checked = false;
            }
            catch (E) {
            }
            if (el.childNodes && el.childNodes.length > 0) {
                for (var x = 0; x < el.childNodes.length; x++) {
                    enabledControl(el.childNodes[x], disabled, checked);
                }
            }
        }
        //      Either enables or disables dropdown boxes
        function enabledControlDropDown(el, disabled, checked) {
            //alert("Hello world from enabled control");
            try {
                if (disabled) {
                    el.disabled = disabled;
                    el.setAttribute("disabled", true);
                }
                else {
                    el.disabled = disabled;
                    el.removeAttribute("disabled");
                }

                if (checked == true)
                    el.options[0].selected = true;
            }
            catch (E) {
            }
            if (el.childNodes && el.childNodes.length > 0) {
                for (var x = 0; x < el.childNodes.length; x++) {
                    enabledControl(el.childNodes[x], disabled, checked);
                }
            }
        }
        //      Either enables or disables text boxes
        function enabledControlTextField(el, disabled, checked) {
            //alert("Hello world from enabled control");
            try {
                if (disabled) {
                    el.disabled = disabled;
                    el.setAttribute("disabled", true);
                }
                else {
                    el.disabled = disabled;
                    el.removeAttribute("disabled");
                }

                if (checked == true) {
                    el.value = '';
                }
            }
            catch (E) {
            }
            if (el.childNodes && el.childNodes.length > 0) {
                for (var x = 0; x < el.childNodes.length; x++) {
                    enabledControl(el.childNodes[x], disabled, checked);
                }
            }
        }

        function generate(arr1, arr2, istrue) {
            if (istrue == true) {
                for (var i = 0; i < arr1.length; i++) {
                    document.getElementById(arr1[i]).style.color = "gray";
                }
                for (var i = 0; i < arr2.length; i++) {
                    $(arr2[i] + ' :input').attr('disabled', true);
                    $(arr2[i] + ' :input').removeAttr("checked");
                }
            }
            if (istrue == false) {
                for (var i = 0; i < arr1.length; i++) {
                    document.getElementById(arr1[i]).style.color = "#333";
                }
                for (var i = 0; i < arr2.length; i++) {
                    $(arr2[i] + ' :input').removeAttr('disabled');
                }
            }
        }

        function QH7toggleTB() {
            enabledControlTextField(document.getElementById("<%= TextBoxPainLevel.ClientID %>"), false, true);
        }

        function QH7toggleCB() {
            enabledControl(document.getElementById("<%= CheckBoxPainLevelNotDocumented.ClientID %>"), false, true);
        }
    </script>

    <script  type="text/javascript" language="javascript">
        $(document).ready(function () {

            // Question 8 History **************
            $("#<%= RadioButtonListHistoryContactLens.ClientID %>").click(function () {
                if (($('#<%= RadioButtonListHistoryContactLens.ClientID %>').find('input:checked').val() == '57') || ($('#<%= RadioButtonListHistoryContactLens.ClientID %>').find('input:checked').val() == '58')) {
                    var myaray = new Array("Q8bA", "Q8bB");
                    var myarray2 = new Array('#Q8bB');
                    generate(myaray, myarray2, false);
                }
                else {
                    var myaray = new Array("Q8bA", "Q8bB");
                    var myarray2 = new Array('#Q8bB');
                    generate(myaray, myarray2, true);
                }
            });
            if (($('#<%= RadioButtonListHistoryContactLens.ClientID %>').find('input:checked').val() == '57') || ($('#<%= RadioButtonListHistoryContactLens.ClientID %>').find('input:checked').val() == '58')) {
                var myaray = new Array("Q8bA", "Q8bB");
                var myarray2 = new Array('#Q8bB');
                generate(myaray, myarray2, false);
            }
            else {
                var myaray = new Array("Q8bA", "Q8bB");
                var myarray2 = new Array('#Q8bB');
                generate(myaray, myarray2, true);
            }

            // Question 9 History **************
            $("#<%= RadioButtonAntimicrobialTherapyNo.ClientID %>").click(function () {
                if ($('#<%= RadioButtonAntimicrobialTherapyNo.ClientID %>').prop("checked") == true) {

                    var myaray = new Array("QH9aA", "QH9aB", "QH9aC", "QH9aD", "QH9aE");
                    var myarray2 = new Array('#QH9aC', '#QH9aE');

                    generate(myaray, myarray2, true);
                    enabledControlTextField(document.getElementById("<%= TextBoxMedOtherText.ClientID %>"), true, true);

                }
            });
            $("#<%= RadioButtonAntimicrobialTherapyYes.ClientID %>").click(function () {
                if ($('#<%= RadioButtonAntimicrobialTherapyYes.ClientID %>').prop("checked") == true) {
                    var myaray = new Array("QH9aA", "QH9aB", "QH9aC", "QH9aD", "QH9aE");
                    var myarray2 = new Array('#QH9aC', '#QH9aE');
                    generate(myaray, myarray2, false);
                    //enabledControlTextField(document.getElementById("<%= TextBoxMedOtherText.ClientID %>"), false, false);
                }
            });

            // History, Question 9, other checkbox toggle
            $("#<%= CheckBoxMedOther.ClientID %>").click(function () {
                if ($('#<%= CheckBoxMedOther.ClientID %>').prop("checked") == true) {
                    enabledControlTextField(document.getElementById("<%= TextBoxMedOtherText.ClientID %>"), false, false);
                }
                else {
                    enabledControlTextField(document.getElementById("<%= TextBoxMedOtherText.ClientID %>"), true, true);
                }
            });

            // Diagnostic Testing, Question 2, other checkbox toggle
            $("#<%= CheckBoxMicroOther.ClientID %>").click(function () {
                if ($('#<%= CheckBoxMicroOther.ClientID %>').prop("checked") == true) {
                    enabledControlTextField(document.getElementById("<%= TextBoxMicroOtherText.ClientID %>"), false, false);
                }
                else {
                    enabledControlTextField(document.getElementById("<%= TextBoxMicroOtherText.ClientID %>"), true, true);
                }
            });

            // Question 3 Diagnostic **************
            $("#<%= RadioButtonWorkingDiagnosisNo.ClientID %>").click(function () {
                if ($('#<%= RadioButtonWorkingDiagnosisNo.ClientID %>').prop("checked") == true) {

                    var myaray = new Array("QD3aA", "QD3aB");
                    var myarray2 = new Array('#QD3aB');

                    generate(myaray, myarray2, true);
                }
            });
            $("#<%= RadioButtonWorkingDiagnosisYes.ClientID %>").click(function () {
                if ($('#<%= RadioButtonWorkingDiagnosisYes.ClientID %>').prop("checked") == true) {

                    var myaray = new Array("QD3aA", "QD3aB");
                    var myarray2 = new Array('#QD3aB');

                    generate(myaray, myarray2, false);
                }
            });

            // Question 4 Diagnostic **************
            $("#<%= RadioButtonListRBCausativeFactor.ClientID %>").click(function () {
                if (($('#<%= RadioButtonListRBCausativeFactor.ClientID %>').find('input:checked').val() == '2') || ($('#<%= RadioButtonListRBCausativeFactor.ClientID %>').find('input:checked').val() == '3')) {

                    var myaray = new Array("QD5aA", "QD5aB");
                    var myarray2 = new Array('#QD5aB');

                    generate(myaray, myarray2, true);
                    enabledControlTextField(document.getElementById("<%= TextBoxCausativeOtherText.ClientID %>"), true, true);
                }
            });
            $("#<%= RadioButtonListRBCausativeFactor.ClientID %>").click(function () {
                if ($('#<%= RadioButtonListRBCausativeFactor.ClientID %>').find('input:checked').val() == ('1')) {

                    var myaray = new Array("QD5aA", "QD5aB");
                    var myarray2 = new Array('#QD5aB');

                    generate(myaray, myarray2, false);
                }
            });

            // Diagnostic Testing, Question 4 other checkbox toggle
            $("#<%= CheckBoxCausativeOther.ClientID %>").click(function () {
                if ($('#<%= CheckBoxCausativeOther.ClientID %>').prop("checked") == true) {
                    enabledControlTextField(document.getElementById("<%= TextBoxCausativeOtherText.ClientID %>"), false, false);
                }
                else {
                    enabledControlTextField(document.getElementById("<%= TextBoxCausativeOtherText.ClientID %>"), true, true);
                }
            });

            // Management, Question 1 other checkbox toggle
            $("#<%= CheckBoxInitialOther.ClientID %>").click(function () {
                if ($('#<%= CheckBoxInitialOther.ClientID %>').prop("checked") == true) {
                    enabledControlTextField(document.getElementById("<%= TextBoxInitialOtherText.ClientID %>"), false, false);
                }
                else {
                    enabledControlTextField(document.getElementById("<%= TextBoxInitialOtherText.ClientID %>"), true, true);
                }
            });

            // Management, Question 5 other checkbox toggle
            $("#<%= CheckBoxFindOther.ClientID %>").click(function () {
                if ($('#<%= CheckBoxFindOther.ClientID %>').prop("checked") == true) {
                    enabledControlTextField(document.getElementById("<%= TextBoxFindOtherText.ClientID %>"), false, false);
                }
                else {
                    enabledControlTextField(document.getElementById("<%= TextBoxFindOtherText.ClientID %>"), true, true);
                }
            });

            // Question 7 Management ************** 
            $("#<%= RadioButtonChangeMedNo.ClientID %>").click(function () {
                if ($('#<%= RadioButtonChangeMedNo.ClientID %>').prop("checked") == true) {

                    var myaray = new Array("QM7aA", "QM7aB", "QM7bA", "QM7bB", "QM7bC", "QM7bD", "QM7bE", "QM7cA", "QM7cB");
                    var myarray2 = new Array('#QM7aB', '#QM7bC', '#QM7bE', '#QM7cB');

                    generate(myaray, myarray2, true);
                    enabledControlTextField(document.getElementById("<%= TextBoxMedAddOtherText.ClientID %>"), true, true);
                    enabledControlDropDown(document.getElementById("<%= DropDownListMedChangeMonth.ClientID %>"), true, true);
                    enabledControlDropDown(document.getElementById("<%= DropDownListMedChangeYear.ClientID %>"), true, true);
                }
            });
            $("#<%= RadioButtonChangeMedYes.ClientID %>").click(function () {
                if ($('#<%= RadioButtonChangeMedYes.ClientID %>').prop("checked") == true) {

                    var myaray = new Array("QM7aA", "QM7aB", "QM7bA", "QM7bB", "QM7bC", "QM7bD", "QM7bE", "QM7cA", "QM7cB");
                    var myarray2 = new Array('#QM7aB', '#QM7bC', '#QM7bE', '#QM7cB');

                    generate(myaray, myarray2, false);
                    enabledControlDropDown(document.getElementById("<%= DropDownListMedChangeMonth.ClientID %>"), false, false);
                    enabledControlDropDown(document.getElementById("<%= DropDownListMedChangeYear.ClientID %>"), false, false);
                    if (($('#<%= CheckBoxMedAddOther.ClientID %>').prop("checked") == true) || ($('#<%= CheckBoxMedDiscOther.ClientID %>').prop("checked") == true)) {
                        enabledControlTextField(document.getElementById("<%= TextBoxMedAddOtherText.ClientID %>"), false, false);
                    }
                    else {
                        enabledControlTextField(document.getElementById("<%= TextBoxMedAddOtherText.ClientID %>"), true, true);
                    }
                }
            });

            // Management, Question 7 other checkbox toggle
            $("#<%= CheckBoxMedAddOther.ClientID %>").click(function () {
                if (($('#<%= CheckBoxMedAddOther.ClientID %>').prop("checked") == true) || ($('#<%= CheckBoxMedDiscOther.ClientID %>').prop("checked") == true)) {
                    enabledControlTextField(document.getElementById("<%= TextBoxMedAddOtherText.ClientID %>"), false, false);
                }
                else {
                    enabledControlTextField(document.getElementById("<%= TextBoxMedAddOtherText.ClientID %>"), true, true);
                }
            });
            $("#<%= CheckBoxMedDiscOther.ClientID %>").click(function () {
                if (($('#<%= CheckBoxMedAddOther.ClientID %>').prop("checked") == true) || ($('#<%= CheckBoxMedDiscOther.ClientID %>').prop("checked") == true)) {
                    enabledControlTextField(document.getElementById("<%= TextBoxMedAddOtherText.ClientID %>"), false, false);
                }
                else {
                    enabledControlTextField(document.getElementById("<%= TextBoxMedAddOtherText.ClientID %>"), true, true);
                }
            });

            // Question 8 Management **************
            $("#<%= RadioButtonMedTopicalCorticosteroidsNo.ClientID %>").click(function () {
                if ($('#<%= RadioButtonMedTopicalCorticosteroidsNo.ClientID %>').prop("checked") == true) {

                    var myaray = new Array("QM8aA", "QM8aB");
                    var myarray2 = new Array('#QM8aB');

                    generate(myaray, myarray2, true);
                    enabledControlDropDown(document.getElementById("<%= DropDownListMedTopicalMonth.ClientID %>"), true, true);
                    enabledControlDropDown(document.getElementById("<%= DropDownListMedTopicalYear.ClientID %>"), true, true);
                }
            });
            $("#<%= RadioButtonMedTopicalCorticosteroidsYes.ClientID %>").click(function () {
                if ($('#<%= RadioButtonMedTopicalCorticosteroidsYes.ClientID %>').prop("checked") == true) {

                    var myaray = new Array("QM8aA", "QM8aB");
                    var myarray2 = new Array('#QM8aB');

                    enabledControlDropDown(document.getElementById("<%= DropDownListMedTopicalMonth.ClientID %>"), false, false);
                    enabledControlDropDown(document.getElementById("<%= DropDownListMedTopicalYear.ClientID %>"), false, false);

                    generate(myaray, myarray2, false);
                }
            });
            // Question 2 Diagnostic **************
            $("#<%= RadioButtonListRBCornealBiopsy.ClientID %>").click(function () {
                if (($('#<%= RadioButtonListRBCornealBiopsy.ClientID %>').find('input:checked').val() == '2')) {
                    var myaray = new Array("QO2aA", "QO2aB", "QO2bA", "QO2bB");
                    var myarray2 = new Array('#QO2aB', '#QO2bB');

                    generate(myaray, myarray2, true);
                    enabledControlTextField(document.getElementById("<%= TextBoxPathOtherText.ClientID %>"), true, true);
                }
            });
            $("#<%= RadioButtonListRBCornealBiopsy.ClientID %>").click(function () {
                if (($('#<%= RadioButtonListRBCornealBiopsy.ClientID %>').find('input:checked').val() == ('76')) || ($('#<%= RadioButtonListRBCornealBiopsy.ClientID %>').find('input:checked').val() == '77')) {
                    var myaray = new Array("QO2aA", "QO2aB", "QO2bA", "QO2bB");
                    var myarray2 = new Array('#QO2aB', '#QO2bB');

                    generate(myaray, myarray2, false);
                }
            });

            // Management, Question 2b other checkbox toggle
            $("#<%= CheckBoxOther.ClientID %>").click(function () {
                if ($('#<%= CheckBoxOther.ClientID %>').prop("checked") == true) {
                    enabledControlTextField(document.getElementById("<%= TextBoxPathOtherText.ClientID %>"), false, false);
                }
                else {
                    enabledControlTextField(document.getElementById("<%= TextBoxPathOtherText.ClientID %>"), true, true);
                }
            });

            // Management, Question 3 other checkbox toggle
            $("#<%= RadioButtonListRBTherapeuticSurg.ClientID %>").click(function () {
                if ($('#<%= RadioButtonListRBTherapeuticSurg.ClientID %>').find('input:checked').val() == ('14')) {
                    enabledControlTextField(document.getElementById("<%= TextBoxTherapeuticSurgOther.ClientID %>"), false, false);
                }
                else {
                    enabledControlTextField(document.getElementById("<%= TextBoxTherapeuticSurgOther.ClientID %>"), true, true);
                }
            });

            if ($('#<%= RadioButtonAntimicrobialTherapyNo.ClientID %>').prop("checked") == true) {

                var myaray = new Array("QH9aA", "QH9aB", "QH9aC", "QH9aD", "QH9aE");
                var myarray2 = new Array('#QH9aC', '#QH9aE');

                generate(myaray, myarray2, true);
                enabledControlTextField(document.getElementById("<%= TextBoxMedOtherText.ClientID %>"), true, true);

            }

            if ($('#<%= RadioButtonAntimicrobialTherapyYes.ClientID %>').prop("checked") == true) {
                var myaray = new Array("QH9aA", "QH9aB", "QH9aC", "QH9aD", "QH9aE");
                var myarray2 = new Array('#QH9aC', '#QH9aE');
                generate(myaray, myarray2, false);
                enabledControlTextField(document.getElementById("<%= TextBoxMedOtherText.ClientID %>"), false, false);
            }

            if ($('#<%= CheckBoxMedOther.ClientID %>').prop("checked") == true) {
                enabledControlTextField(document.getElementById("<%= TextBoxMedOtherText.ClientID %>"), false, false);
            }
            else {
                enabledControlTextField(document.getElementById("<%= TextBoxMedOtherText.ClientID %>"), true, true);
            }

            if ($('#<%= CheckBoxMicroOther.ClientID %>').prop("checked") == true) {
                enabledControlTextField(document.getElementById("<%= TextBoxMicroOtherText.ClientID %>"), false, false);
            }
            else {
                enabledControlTextField(document.getElementById("<%= TextBoxMicroOtherText.ClientID %>"), true, true);
            }

            if ($('#<%= RadioButtonWorkingDiagnosisNo.ClientID %>').prop("checked") == true) {

                var myaray = new Array("QD3aA", "QD3aB");
                var myarray2 = new Array('#QD3aB');

                generate(myaray, myarray2, true);
            }

            if ($('#<%= RadioButtonWorkingDiagnosisYes.ClientID %>').prop("checked") == true) {

                var myaray = new Array("QD3aA", "QD3aB");
                var myarray2 = new Array('#QD3aB');

                generate(myaray, myarray2, false);
            }

            if (($('#<%= RadioButtonListRBCausativeFactor.ClientID %>').find('input:checked').val() == '2') || ($('#<%= RadioButtonListRBCausativeFactor.ClientID %>').find('input:checked').val() == '3')) {

                var myaray = new Array("QD5aA", "QD5aB");
                var myarray2 = new Array('#QD5aB');

                generate(myaray, myarray2, true);
                enabledControlTextField(document.getElementById("<%= TextBoxCausativeOtherText.ClientID %>"), true, true);
            }

            if ($('#<%= RadioButtonListRBCausativeFactor.ClientID %>').find('input:checked').val() == ('1')) {

                var myaray = new Array("QD5aA", "QD5aB");
                var myarray2 = new Array('#QD5aB');

                generate(myaray, myarray2, false);
            }

            if ($('#<%= CheckBoxCausativeOther.ClientID %>').prop("checked") == true) {
                enabledControlTextField(document.getElementById("<%= TextBoxCausativeOtherText.ClientID %>"), false, false);
            }
            else {
                enabledControlTextField(document.getElementById("<%= TextBoxCausativeOtherText.ClientID %>"), true, true);
            }

            if ($('#<%= CheckBoxInitialOther.ClientID %>').prop("checked") == true) {
                enabledControlTextField(document.getElementById("<%= TextBoxInitialOtherText.ClientID %>"), false, false);
            }
            else {
                enabledControlTextField(document.getElementById("<%= TextBoxInitialOtherText.ClientID %>"), true, true);
            }

            if ($('#<%= CheckBoxFindOther.ClientID %>').prop("checked") == true) {
                enabledControlTextField(document.getElementById("<%= TextBoxFindOtherText.ClientID %>"), false, false);
            }
            else {
                enabledControlTextField(document.getElementById("<%= TextBoxFindOtherText.ClientID %>"), true, true);
            }

            if ($('#<%= RadioButtonChangeMedNo.ClientID %>').prop("checked") == true) {

                var myaray = new Array("QM7aA", "QM7aB", "QM7bA", "QM7bB", "QM7bC", "QM7bD", "QM7bE", "QM7cA", "QM7cB");
                var myarray2 = new Array('#QM7aB', '#QM7bC', '#QM7bE', '#QM7cB');

                generate(myaray, myarray2, true);
                enabledControlTextField(document.getElementById("<%= TextBoxMedAddOtherText.ClientID %>"), true, true);
                enabledControlDropDown(document.getElementById("<%= DropDownListMedChangeMonth.ClientID %>"), true, true);
                enabledControlDropDown(document.getElementById("<%= DropDownListMedChangeYear.ClientID %>"), true, true);
            }

            if ($('#<%= RadioButtonChangeMedYes.ClientID %>').prop("checked") == true) {

                var myaray = new Array("QM7aA", "QM7aB", "QM7bA", "QM7bB", "QM7bC", "QM7bD", "QM7bE", "QM7cA", "QM7cB");
                var myarray2 = new Array('#QM7aB', '#QM7bC', '#QM7bE', '#QM7cB');

                generate(myaray, myarray2, false);
            }

            if (($('#<%= CheckBoxMedAddOther.ClientID %>').prop("checked") == true) || ($('#<%= CheckBoxMedAddOther.ClientID %>').prop("checked") == true)) {
                enabledControlTextField(document.getElementById("<%= TextBoxMedAddOtherText.ClientID %>"), false, false);
            }

            if (($('#<%= CheckBoxMedAddOther.ClientID %>').prop("checked") == true) || ($('#<%= CheckBoxMedAddOther.ClientID %>').prop("checked") == true)) {
                enabledControlTextField(document.getElementById("<%= TextBoxMedAddOtherText.ClientID %>"), false, false);
            }

            if ($('#<%= RadioButtonMedTopicalCorticosteroidsNo.ClientID %>').prop("checked") == true) {

                var myaray = new Array("QM8aA", "QM8aB");
                var myarray2 = new Array('#QM8aB');

                generate(myaray, myarray2, true);
                enabledControlDropDown(document.getElementById("<%= DropDownListMedTopicalMonth.ClientID %>"), true, true);
                enabledControlDropDown(document.getElementById("<%= DropDownListMedTopicalYear.ClientID %>"), true, true);
            }

            if ($('#<%= RadioButtonMedTopicalCorticosteroidsYes.ClientID %>').prop("checked") == true) {

                var myaray = new Array("QM8aA", "QM8aB");
                var myarray2 = new Array('#QM8aB');

                generate(myaray, myarray2, false);
            }

            if (($('#<%= RadioButtonListRBCornealBiopsy.ClientID %>').find('input:checked').val() == '2')) {
                var myaray = new Array("QO2aA", "QO2aB", "QO2bA", "QO2bB");
                var myarray2 = new Array('#QO2aB', '#QO2bB');

                generate(myaray, myarray2, true);
                enabledControlTextField(document.getElementById("<%= TextBoxPathOtherText.ClientID %>"), true, true);
            }

            if (($('#<%= RadioButtonListRBCornealBiopsy.ClientID %>').find('input:checked').val() == ('76')) || ($('#<%= RadioButtonListRBCornealBiopsy.ClientID %>').find('input:checked').val() == '77')) {
                var myaray = new Array("QO2aA", "QO2aB", "QO2bA", "QO2bB");
                var myarray2 = new Array('#QO2aB', '#QO2bB');

                generate(myaray, myarray2, false);
            }

            if ($('#<%= CheckBoxOther.ClientID %>').prop("checked") == true) {
                enabledControlTextField(document.getElementById("<%= TextBoxPathOtherText.ClientID %>"), false, false);
            }
            else {
                enabledControlTextField(document.getElementById("<%= TextBoxPathOtherText.ClientID %>"), true, true);
            }

            if ($('#<%= RadioButtonListRBTherapeuticSurg.ClientID %>').find('input:checked').val() == ('14')) {
                enabledControlTextField(document.getElementById("<%= TextBoxTherapeuticSurgOther.ClientID %>"), false, false);
            }
            else {
                enabledControlTextField(document.getElementById("<%= TextBoxTherapeuticSurgOther.ClientID %>"), true, true);
            }
        });
    </script>

    <script type="text/javascript">
        $(function () {
            $('#QD4').aToolTip({
                clickIt: true,
                tipContent: 'i.e. contact lens wear, exposure keratopathy, trauma.'
            });

            $('#QO8').aToolTip({
                clickIt: true,
                tipContent: 'i.e. contact lens wearing habits / hygiene, eyeglass wear to avoid trauma.'
            });

            $('#QH7').aToolTip({
                clickIt: true,
                tipContent: 'on a scale of 1-10 with 1 being the lowest amount of pain'
            });

            $('#QD2').aToolTip({
                clickIt: true,
                tipContent: '20/800 or count fingers @ 5 ft<br />20/1000 or count fingers @ 4 ft<br />20/1600 or count fingers @ 3ft<br />20/2000 or count fingers @ 2 ft<br />20/4000 or count fingers @ 1 ft<br />20/7777 =CF<br />20/8888 =HM<br />20/9999 =LP<br />20/0000 =NLP'
            });

            $('#QO5').aToolTip({
                clickIt: true,
                tipContent: '20/800 or count fingers @ 5 ft<br />20/1000 or count fingers @ 4 ft<br />20/1600 or count fingers @ 3ft<br />20/2000 or count fingers @ 2 ft<br />20/4000 or count fingers @ 1 ft<br />20/7777 =CF<br />20/8888 =HM<br />20/9999 =LP<br />20/0000 =NLP'
            });
        });
    </script>
</asp:Content>