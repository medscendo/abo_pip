﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Transactions;
using NetHealthPIMModel;

public partial class abo_POAGChart : BasePage {

    public bool PQRSEnabled { get; set; }

    public bool PQRSUsed { get; set; }

    protected void Page_Load(object sender, EventArgs e) {
        NetHealthPIMEntities pim1 = new NetHealthPIMEntities();
        var pqrsselected = pim1.ParticipantModule.First(p => p.ParticipantID == ctx.ParticipantID);
        PQRSPanel.Visible = Convert.ToBoolean(pqrsselected.PQRS);

        if (!IsPostBack) {
            InitializePage();
            PQRSUsed = Convert.ToBoolean(pqrsselected.PQRS);
            PQRSEnabled = Convert.ToBoolean(System.Configuration.ConfigurationManager.AppSettings["PQRSEnabled"]);
        }
    }

    protected void InitializePage() {
        ((PIMMasterPage)Page.Master).SetTitle("Chart");

        using (NetHealthPIMEntities pim = new NetHealthPIMEntities()) {
            int ModuleID = (int)Session[Constants.SESSION_WORKINGMODULEID];
            Module module = pim.Module.First(c => c.ModuleID == ModuleID);
            ((PIMMasterPage)Page.Master).SetHeader(module.ModuleName + " Chart Abstraction");

            ParticipantModuleCycle prev = (ParticipantModuleCycle)Session[Constants.SESSION_PREVIOUSCYCLE];
            String CycleNumber = Request.QueryString["CycleNumber"];

            int cycleID = ctx.ActiveModuleCycleID;
            if (CycleNumber == "1") {
                cycleID = prev.ParticipantModuleCycleID;
                LinkButtonBackToDashboard.Visible = true;
                ButtonSubmit.Visible = false;
            }

            ParticipantModuleCycle cycle = pim.ParticipantModuleCycle.First(c => c.ParticipantModuleCycleID == cycleID);
            String numberOfRecord = Request.QueryString["nr"];
            String totalRecords = Request.QueryString["t"];
            if (cycle.CycleNumber == 1)
            {
                ((PIMMasterPage)Page.Master).SetStatus(2);
                LiteralAbstractionNumber.Text = "Initial Abstraction: Chart " + numberOfRecord + " of " + totalRecords;
            }
            else
            {
                ((PIMMasterPage)Page.Master).SetStatus(3);
                LiteralAbstractionNumber.Text = "Second Abstraction: Chart " + numberOfRecord + " of " + totalRecords;
            }
            DropDownListMonthOfBirth.DataSource = CycleManager.getMonthList();
            DropDownListYearOfBirth.DataSource = CycleManager.getDOBYearList(0, 100);
            DropDownListMonthOfExam.DataSource = CycleManager.getMonthList();
            DropDownListYearOfExam.DataSource = CycleManager.getDOBYearList(0, 100);
            DropDownListFollowUpExamScheduledMonth.DataSource = CycleManager.getMonthList();
            DropDownListFollowUpExamScheduledYear.DataSource = CycleManager.getYearList();
            DropDownListFollowUpRecentExamMonth.DataSource = CycleManager.getMonthList();
            DropDownListFollowUpRecentExamYear.DataSource = CycleManager.getDOBYearList(0, 100);
            DropDownListOutcomeVisualFieldODMonth.DataSource = CycleManager.getMonthList();
            DropDownListOutcomVisualFieldODYear.DataSource = CycleManager.getDOBYearList(0, 100);
            DropDownListOutcomeVisualFieldOSMonth.DataSource = CycleManager.getMonthList();
            DropDownListOutcomeVisualFieldOSYear.DataSource = CycleManager.getDOBYearList(0, 100);
            DropDownListOutcomeOpticNerveExamMonth.DataSource = CycleManager.getMonthList();
            DropDownListOutcomeOpticNerveExamYear.DataSource = CycleManager.getDOBYearList(0, 100);
            //DropDownListPQRSDOBMonth.DataSource = CycleManager.getMonthList();
            //DropDownListPQRSDOBYear.DataSource = CycleManager.getDOBYearList(0, 100);
            DropDownListPQRSVisitMonth.DataSource = CycleManager.getMonthList();
            DropDownListPQRSVisitYear.DataSource = CycleManager.getDOBYearList(0, 100);
            DropDownListOutcomeBCVAOD.DataSource = CycleManager.getExamValues();
            DropDownListOutcomeBCVAOS.DataSource = CycleManager.getExamValues();
            DropDownListBCVAOD.DataSource = CycleManager.getExamValues();
            DropDownListBCVAOS.DataSource = CycleManager.getExamValues();
            DataBind();
            DropDownListPQRSVisitYear.Items.Insert(0, new ListItem("Year", "0")); //Alex, I added this so the default can be "Year", not 2015. Lauren thinks this is where the problem <https://healthmonix.atlassian.net/browse/ABO-8> is coming from.
            DropDownListYearOfBirth.Items.Insert(0, new ListItem("Year", "0"));
            DropDownListFollowUpRecentExamYear.Items.Insert(0, new ListItem("Year", "0"));
            DropDownListFollowUpExamScheduledYear.Items.Insert(0, new ListItem("Year", "0"));
            DropDownListYearOfExam.Items.Insert(0, new ListItem("Year", "0"));
            DropDownListOutcomVisualFieldODYear.Items.Insert(0, new ListItem("Year", "0"));
            DropDownListOutcomeVisualFieldOSYear.Items.Insert(0, new ListItem("Year", "0"));
            DropDownListOutcomeOpticNerveExamYear.Items.Insert(0, new ListItem("Year", "0"));
            //DropDownListPQRSDOBYear.Items.Insert(0, new ListItem("Year", "0"));
            //DropDownListPQRSVisitYear.Items.Insert(0, new ListItem("Year", "0"));
            string recordIdentifier = Request.QueryString["ri"];
            HiddenFieldRecordIdentifier.Value = recordIdentifier;
            LiteralRecordIdentifier.Text = recordIdentifier;

            var count = pim.ChartAbstractionPOAG.Where(ps => ps.ParticipantModuleCycle.ParticipantModuleCycleID == cycleID & ps.RecordIdentifier == recordIdentifier).Count();
            if (count == 0) {
                using (TransactionScope transaction = new TransactionScope()) {
                    //Module module = pim.Module.First(m => m.ModuleID == Constants.ABO_AMBLYOPIA_MODULEID);

                    NetHealthPIMModel.ChartRegistration chartRegistration = (from cr in pim.ChartRegistration
                                                           where cr.ParticipantModuleCycleID == cycleID
                                                           & cr.RecordIdentifier == recordIdentifier
                                                           & cr.ModuleID == 20///// NEEDS TO BE CHANGED
                                                           select cr).First<NetHealthPIMModel.ChartRegistration>();

                    DateTime initialVisit = Convert.ToDateTime(chartRegistration.InitialVisit);
                    DateTime DOB = Convert.ToDateTime(chartRegistration.DOB);
                    DateTime lastVisit = Convert.ToDateTime(chartRegistration.LastVisit);
                    ChartAbstractionPOAG abstractRecord = new ChartAbstractionPOAG();
                    abstractRecord.ParticipantModuleCycle = cycle;
                    abstractRecord.RecordIdentifier = recordIdentifier;
                    abstractRecord.MonthOfExam = initialVisit.Month;
                    abstractRecord.YearOfExam = initialVisit.Year;
                    abstractRecord.MonthOfBirth = DOB.Month;
                    abstractRecord.YearOfBirth = DOB.Year;
                    abstractRecord.FollowUpRecentExamMonth = lastVisit.Month;
                    abstractRecord.FollowUpRecentExamYear = lastVisit.Year;
                    
                    //Birthday
                    DropDownListYearOfBirth.SelectedValue = abstractRecord.YearOfBirth.ToString();
                    DropDownListMonthOfBirth.SelectedValue = abstractRecord.MonthOfBirth.ToString();
                    //DropDownListPQRSDOBYear.SelectedValue = DropDownListYearOfBirth.SelectedValue;
                    //DropDownListPQRSDOBMonth.SelectedValue = DropDownListMonthOfBirth.SelectedValue;
                    
                    DropDownListMonthOfExam.SelectedValue = abstractRecord.MonthOfExam.ToString();
                    DropDownListYearOfExam.SelectedValue = abstractRecord.YearOfExam.ToString();
                    DropDownListFollowUpRecentExamYear.SelectedValue = abstractRecord.FollowUpRecentExamYear.ToString();
                    DropDownListFollowUpRecentExamMonth.SelectedValue = abstractRecord.FollowUpRecentExamMonth.ToString();
                    DropDownListFollowUpExamScheduledMonth.SelectedValue = abstractRecord.FollowUpExamScheduledMonth.ToString();
                    DropDownListFollowUpExamScheduledYear.SelectedValue = abstractRecord.FollowUpExamScheduledYear.ToString();
                    DropDownListFollowUpRecentExamMonth.SelectedValue = abstractRecord.FollowUpRecentExamMonth.ToString();
                    DropDownListFollowUpRecentExamYear.SelectedValue = abstractRecord.FollowUpRecentExamYear.ToString();
                    DropDownListOutcomeVisualFieldODMonth.SelectedValue = abstractRecord.OutcomeVisualFieldODMonth.ToString();
                    DropDownListOutcomVisualFieldODYear.SelectedValue = abstractRecord.OutcomVisualFieldODYear.ToString();
                    DropDownListOutcomeVisualFieldOSMonth.SelectedValue = abstractRecord.OutcomeVisualFieldOSYear.ToString();
                    DropDownListOutcomeVisualFieldOSYear.SelectedValue = abstractRecord.OutcomeVisualFieldOSMonth.ToString();
                    DropDownListOutcomeOpticNerveExamMonth.SelectedValue = abstractRecord.OutcomeOpticNerveExamMonth.ToString();
                    DropDownListOutcomeOpticNerveExamYear.SelectedValue = abstractRecord.OutcomeOpticNerveExamYear.ToString();

                    abstractRecord.CreatedOn = DateTime.Now;
                    abstractRecord.LastUpdateDate = DateTime.Now;
                    pim.SaveChanges();

                    transaction.Complete();
                }
            }
            else {
                ChartAbstractionPOAG abstractRecord = (from c in pim.ChartAbstractionPOAG
                                                       where c.ParticipantModuleCycle.ParticipantModuleCycleID == cycleID & c.RecordIdentifier == recordIdentifier
                                                       select c).First<ChartAbstractionPOAG>();

                Participant participant = pim.Participant.First(p => p.ParticipantID == ctx.ParticipantID);
                var pqrsselected = pim.ParticipantModule.First(p => p.ParticipantID == participant.ParticipantID);
                bool pqrsused = Convert.ToBoolean(pqrsselected.PQRS);
                bool pqrsenabled = Convert.ToBoolean(System.Configuration.ConfigurationManager.AppSettings["PQRSEnabled"]);
                if (pqrsused && pqrsenabled) {
                    
                        PQRSPanel.Visible = true;
                        PQRSWebService.PPWebServiceClient client = new PQRSWebService.PPWebServiceClient();
                        client.ClientCredentials.UserName.UserName = "partner:2015";
                        client.ClientCredentials.UserName.Password = "48C1A443-54B0-4D00-A4A2-FE5E46C2F02E";



                        Int32 participantid = client.AddUser(0, "0", participant.ParticipantFirstName, participant.ParticipantLastName, participant.ParticipantEmailAddress, participant.ParticipantEmailAddress, "abo2015", "ABO"); 
                            String patient_id = Request.QueryString["ri"].ToString();
                            if (!(cycle.CycleNumber == 1))
                            {
                                patient_id = "C2" + patient_id;
                            }


                           DataTable values = client.GetPatientVisit(participantid, patient_id, 2015);

                           if (values != null && values.Rows.Count>0)
                        {
                            DataRow visit = values.Rows[0];
                            DropDownListPQRSVisitMonth.SelectedValue = visit["VisitDate"].ToString().Substring(0, 2).TrimStart('0');
                            DropDownListPQRSVisitYear.SelectedValue = visit["VisitDate"].ToString().Substring(6);
                            PQRSMedicare.SelectedValue = (Boolean)visit["IsMedicarePartB"] ? "1" : "0";

                            String question_id = "";
                            try {question_id = "Q3229"; //Diagnosis 12.2, 141.2
                            if (visit[question_id] != null) PQRSDiagnosis.SelectedValue = visit[question_id].ToString();
							} catch  {}
							//  Encounter
							try {question_id = "Q3230";
							if (visit[question_id] != null) {
								//  get measure 12 denom
								string Encounter12 = visit[question_id].ToString();
								//  get measure 141 denom
								try {question_id = "Q3542";
								string Encounter141 = visit[question_id].ToString();
								//  if 141 is 'true' then Encounter A is true
								if (Encounter141 != null){
								//{ //Encounter 12.3, 141.3
								   CheckBoxPQRSEncounterA.Checked = Encounter141 == "1";
								}
								if (Encounter12 != null){
								//{ //Encounter 12.3, 141.3
								   CheckBoxPQRSEncounterB.Checked = (Encounter12 == "1"  & Encounter141 == "0");
								}
								//    CheckBoxPQRSEncounterB.Checked = visit[question_id].ToString().Contains("b");
								//}
							} catch  {}
							}
							} catch  {}
                            try {question_id = "Q3231"; //YesNo 12.4
                            if (visit[question_id] != null) PQRSOpticNerveEval.SelectedValue = visit[question_id].ToString();
							} catch  {}
							try {
                            question_id = "Q3232"; //Reason 12.5
                           if (visit[question_id] != null) PQRSOpticNerveReason.SelectedValue = visit[question_id].ToString();
						   } catch  {}
							//  was IOP documented
							try {question_id = "Q3543";
                            if (visit[question_id] != null) PQRSIOPDocumented.SelectedValue = visit[question_id].ToString();
							} catch  {}
                            //IOP Measurement 141.4 - Was the IOP reduced by at least 15% from the pre-intervention level in the affected eye?
							try {question_id = "Q3544";
                            if (visit[question_id] != null) PQRSIOPReduced.SelectedValue = visit[question_id].ToString();
							} catch  {}
							//   not done
							
                            try {question_id = "Q3545";
                            if (visit[question_id] != null)
                            { //IOP Reduced 141.5
                                if (visit[question_id] != null) PQRSGlaucomaPlan.SelectedValue = visit[question_id].ToString();
                            }
							}
							catch  {}


                        }
                    //}
                    //catch
                    //{
                    //    ViewState["PQRSBroken"] = true;
                    //}
                }

                if (abstractRecord.ManagementPlanFormulated != null) {
                    RadioButtonManagementPlanFormulatedYes.Checked = ((bool)abstractRecord.ManagementPlanFormulated);
                    RadioButtonManagementPlanFormulatedNo.Checked = !((bool)abstractRecord.ManagementPlanFormulated);
                }
                if (abstractRecord.MedicalHistory != null) {
                    RadioButtonMedicalHistoryYes.Checked = ((bool)abstractRecord.MedicalHistory);
                    RadioButtonMedicalHistoryNo.Checked = !((bool)abstractRecord.MedicalHistory);
                }
                if (abstractRecord.OcularHistory != null) {
                    RadioButtonOcularHistoryYes.Checked = ((bool)abstractRecord.OcularHistory);
                    RadioButtonOcularHistoryNo.Checked = !((bool)abstractRecord.OcularHistory);
                }
                if (abstractRecord.AutomatedVisualFields != null) {
                    RadioButtonListAutomatedVisualFields.SelectedValue = abstractRecord.AutomatedVisualFields.ToString();
                }
                if (abstractRecord.FollowUpExamScheduled != null) {
                    RadioButtonFollowUpExamScheduledYes.Checked = ((bool)abstractRecord.FollowUpExamScheduled);
                    RadioButtonFollowUpExamScheduledNo.Checked = !((bool)abstractRecord.FollowUpExamScheduled);
                }


                if (abstractRecord.VisualFieldMeanDeviationSignOD != null) {
                    RadioButtonVisualFieldMeanDeviationSignODPlus.Checked = ((bool)abstractRecord.VisualFieldMeanDeviationSignOD);
                    RadioButtonVisualFieldMeanDeviationSignODMinus.Checked = !((bool)abstractRecord.VisualFieldMeanDeviationSignOD);
                }
                if (abstractRecord.ImpactDailyLiving != null) {
                    RadioButtonImpactDailyLivingYes.Checked = ((bool)abstractRecord.ImpactDailyLiving);
                    RadioButtonImpactDailyLivingNo.Checked = !((bool)abstractRecord.ImpactDailyLiving);
                }
                if (abstractRecord.VisualFieldMeanDeviationSignOS != null) {
                    RadioButtonVisualFieldMeanDeviationSignOSPlus.Checked = ((bool)abstractRecord.VisualFieldMeanDeviationSignOS);
                    RadioButtonVisualFieldMeanDeviationSignOSMinus.Checked = !((bool)abstractRecord.VisualFieldMeanDeviationSignOS);
                }
                if (abstractRecord.OutcomeVisualFieldMeanDeviationSignOD != null) {
                    RadioButtonOutcomeVisualFieldMeanDeviationSignODPlus.Checked = ((bool)abstractRecord.OutcomeVisualFieldMeanDeviationSignOD);
                    RadioButtonOutcomeVisualFieldMeanDeviationSignODMinus.Checked = !((bool)abstractRecord.OutcomeVisualFieldMeanDeviationSignOD);
                }
                if (abstractRecord.OutcomeVisualFieldMeanDeviationSignOS != null) {
                    RadioButtonOutcomeVisualFieldMeanDeviationSignOSPlus.Checked = ((bool)abstractRecord.OutcomeVisualFieldMeanDeviationSignOS);
                    RadioButtonOutcomeVisualFieldMeanDeviationSignOSMinus.Checked = !((bool)abstractRecord.OutcomeVisualFieldMeanDeviationSignOS);
                }
                if (abstractRecord.OpticNerveFindingsInterpreted != null) {


                    RadioButtonListOpticNerveFindingsInterpreted.SelectedValue = abstractRecord.OpticNerveFindingsInterpreted.ToString();

                }
                if (abstractRecord.BCVAOD != null) DropDownListBCVAOD.SelectedValue = abstractRecord.BCVAOD.ToString();
                if (abstractRecord.BCVAOS != null) DropDownListBCVAOS.SelectedValue = abstractRecord.BCVAOS.ToString();
                if (abstractRecord.FollowUpRecentExamMonth != null) DropDownListFollowUpRecentExamMonth.SelectedValue = abstractRecord.FollowUpRecentExamMonth.ToString();
                if (abstractRecord.FollowUpExamScheduledMonth != null) DropDownListFollowUpExamScheduledMonth.SelectedValue = abstractRecord.FollowUpExamScheduledMonth.ToString();
                if (abstractRecord.FollowUpExamScheduledYear != null) DropDownListFollowUpExamScheduledYear.SelectedValue = abstractRecord.FollowUpExamScheduledYear.ToString();
                if (abstractRecord.FollowUpRecentExamYear != null) DropDownListFollowUpRecentExamYear.SelectedValue = abstractRecord.FollowUpRecentExamYear.ToString();
                if (abstractRecord.YearOfBirth != null) DropDownListYearOfBirth.SelectedValue = abstractRecord.YearOfBirth.ToString();
                if (abstractRecord.MonthOfBirth != null) DropDownListMonthOfBirth.SelectedValue = abstractRecord.MonthOfBirth.ToString();
                //Birthday
                //DropDownListPQRSDOBYear.SelectedValue = DropDownListYearOfBirth.SelectedValue;
                //DropDownListPQRSDOBMonth.SelectedValue = DropDownListMonthOfBirth.SelectedValue;

                if (abstractRecord.YearOfExam != null) DropDownListYearOfExam.SelectedValue = abstractRecord.YearOfExam.ToString(); if (abstractRecord.MonthOfExam != null) DropDownListMonthOfExam.SelectedValue = abstractRecord.MonthOfExam.ToString();
                
                if (abstractRecord.OutcomeBCVAOD != null) DropDownListOutcomeBCVAOD.SelectedValue = abstractRecord.OutcomeBCVAOD.ToString();
                if (abstractRecord.OutcomeBCVAOS != null) DropDownListOutcomeBCVAOS.SelectedValue = abstractRecord.OutcomeBCVAOS.ToString();
                if (abstractRecord.OutcomeOpticNerveExamMonth != null) DropDownListOutcomeOpticNerveExamMonth.SelectedValue = abstractRecord.OutcomeOpticNerveExamMonth.ToString();
                if (abstractRecord.OutcomeOpticNerveExamYear != null) DropDownListOutcomeOpticNerveExamYear.SelectedValue = abstractRecord.OutcomeOpticNerveExamYear.ToString();

                if (abstractRecord.OutcomeVisualFieldODMonth != null) DropDownListOutcomeVisualFieldODMonth.SelectedValue = abstractRecord.OutcomeVisualFieldODMonth.ToString();
                if (abstractRecord.OutcomeVisualFieldOSMonth != null) DropDownListOutcomeVisualFieldOSMonth.SelectedValue = abstractRecord.OutcomeVisualFieldOSMonth.ToString();
                if (abstractRecord.OutcomeVisualFieldOSYear != null) DropDownListOutcomeVisualFieldOSYear.SelectedValue = abstractRecord.OutcomeVisualFieldOSYear.ToString();
                if (abstractRecord.OutcomVisualFieldODYear != null) DropDownListOutcomVisualFieldODYear.SelectedValue = abstractRecord.OutcomVisualFieldODYear.ToString();
                
                if (abstractRecord.VisualFieldPSDOSNA != null) CheckBoxVisualFieldPSDOSNA.Checked = ((bool)abstractRecord.VisualFieldPSDOSNA);


                if (abstractRecord.OutcomeBCVAOD != null) DropDownListOutcomeBCVAOD.SelectedValue = abstractRecord.OutcomeBCVAOD.ToString();
                if (abstractRecord.OutcomeBCVAOS != null) DropDownListOutcomeBCVAOS.SelectedValue = abstractRecord.OutcomeBCVAOS.ToString();
                if (abstractRecord.BCVAOD != null) DropDownListBCVAOD.SelectedValue = abstractRecord.BCVAOD.ToString();
                if (abstractRecord.BCVAOS != null) DropDownListBCVAOS.SelectedValue = abstractRecord.BCVAOS.ToString();



                if (abstractRecord.OutcomeVisualFieldODNA != null) CheckBoxOutcomeVisualFieldODNA.Checked = ((bool)abstractRecord.OutcomeVisualFieldODNA);
                if (abstractRecord.OutcomeVisualFieldOSNA != null) CheckBoxOutcomeVisualFieldOSNA.Checked = ((bool)abstractRecord.OutcomeVisualFieldOSNA);
                if (abstractRecord.OutcomeVisualFieldPSDODNA != null) CheckBoxOutcomeVisualFieldPSDODNA.Checked = ((bool)abstractRecord.OutcomeVisualFieldPSDODNA);
                if (abstractRecord.OutcomeOpticNerveExamNA != null) CheckBoxOutcomeOpticNerveExamNA.Checked = ((bool)abstractRecord.OutcomeOpticNerveExamNA);





                if (abstractRecord.OutcomeVisualFieldPSDOSNA != null) CheckBoxOutcomeVisualFieldPSDOSNA.Checked = ((bool)abstractRecord.OutcomeVisualFieldPSDOSNA);
                if (abstractRecord.VisualFieldPSDODNA != null) CheckBoxVisualFieldPSDODNA.Checked = ((bool)abstractRecord.VisualFieldPSDODNA);
                if (abstractRecord.VisualFieldPSDOSNA != null) CheckBoxVisualFieldPSDODNA.Checked = ((bool)abstractRecord.VisualFieldPSDOSNA);

                if (abstractRecord.TargetIOPMMHG != null) TextBoxTargetIOPMMHG.Text = abstractRecord.TargetIOPMMHG.ToString();
                if (abstractRecord.CentralCornealThicknessOD != null) TextBoxCentralCornealThicknessOD.Text = abstractRecord.CentralCornealThicknessOD.ToString();
                if (abstractRecord.CentralCornealThicknessOS != null) TextBoxCentralCornealThicknessOS.Text = abstractRecord.CentralCornealThicknessOS.ToString();
                if (abstractRecord.IOPODmmHg != null) TextBoxIOPODmmHg.Text = abstractRecord.IOPODmmHg.ToString();
                if (abstractRecord.OutcomeIOPODmmHg != null) TextBoxOutcomeIOPODmmHg.Text = abstractRecord.OutcomeIOPODmmHg.ToString();
                if (abstractRecord.OutcomeIOPOSmmHg != null) TextBoxOutcomeIOPOSmmHg.Text = abstractRecord.OutcomeIOPOSmmHg.ToString();
                if (abstractRecord.OutcomeVisualFieldMeanDeviationValueOD != null) TextBoxOutcomeVisualFieldMeanDeviationValueOD.Text = abstractRecord.OutcomeVisualFieldMeanDeviationValueOD.ToString();
                if (abstractRecord.OutcomeVisualFieldMeanDeviationValueOS != null) TextBoxOutcomeVisualFieldMeanDeviationValueOS.Text = abstractRecord.OutcomeVisualFieldMeanDeviationValueOS.ToString();
                if (abstractRecord.OutcomeVisualFieldPSDValueOD != null) TextBoxOutcomeVisualFieldPSDValueOD.Text = abstractRecord.OutcomeVisualFieldPSDValueOD.ToString();
                if (abstractRecord.OutcomeVisualFieldPSDValueOS != null) TextBoxOutcomeVisualFieldPSDValueOS.Text = abstractRecord.OutcomeVisualFieldPSDValueOS.ToString();
                if (abstractRecord.VisualFieldMeanDeviationValueOD != null) TextBoxVisualFieldMeanDeviationValueOD.Text = abstractRecord.VisualFieldMeanDeviationValueOD.ToString();
                if (abstractRecord.VisualFieldMeanDeviationValueOS != null) TextBoxVisualFieldMeanDeviationValueOS.Text = abstractRecord.VisualFieldMeanDeviationValueOS.ToString();
                if (abstractRecord.VisualFieldPSDValueOD != null) TextBoxVisualFieldPSDValueOD.Text = abstractRecord.VisualFieldPSDValueOD.ToString();




                if (abstractRecord.IOPOSmmHg != null) TextBoxIOPOSmmHg.Text = abstractRecord.IOPOSmmHg.ToString();




                if (abstractRecord.VisualFieldPSDValueOS != null) TextBoxVisualFieldPSDValueOS.Text = abstractRecord.VisualFieldPSDValueOS.ToString();
                if (abstractRecord.RBGlaucomaSeverity != null) RadioButtonListRBGlaucomaSeverity.SelectedValue = abstractRecord.RBGlaucomaSeverity.ToString();
                if (abstractRecord.RBBCVAODNotAbletoPerform != null) RadioButtonListRBBCVAODNotAbletoPerform.SelectedValue = abstractRecord.RBBCVAODNotAbletoPerform.ToString();
                if (abstractRecord.RBBCVAOSNotAbletoPerform != null) RadioButtonListRBBCVAOSNotAbletoPerform.SelectedValue = abstractRecord.RBBCVAOSNotAbletoPerform.ToString();
                if (abstractRecord.RBIOPOSNotAbletoPerform != null) RadioButtonListRBIOPOSNotAbletoPerform.SelectedValue = abstractRecord.RBIOPOSNotAbletoPerform.ToString();
                if (abstractRecord.RBOutcomeIOPODNotAbletoPerform != null) RadioButtonListRBOutcomeIOPODNotAbletoPerform.SelectedValue = abstractRecord.RBOutcomeIOPODNotAbletoPerform.ToString();
                if (abstractRecord.RBOutcomeIOPOSNotAbletoPerform != null) RadioButtonListRBOutcomeIOPOSNotAbletoPerform.SelectedValue = abstractRecord.RBOutcomeIOPOSNotAbletoPerform.ToString();
                if (abstractRecord.RBOutcomeIOPODNotAbletoPerform != null) RadioButtonListRBOutcomeIOPODNotAbletoPerform.SelectedValue = abstractRecord.RBOutcomeIOPODNotAbletoPerform.ToString();
                if (abstractRecord.RBOutcomeIOPOSNotAbletoPerform != null) RadioButtonListRBOutcomeIOPOSNotAbletoPerform.SelectedValue = abstractRecord.RBOutcomeIOPOSNotAbletoPerform.ToString();
                if (abstractRecord.AfferentPupillaryDefect != null) RadioButtonListAfferentPupillaryDefect.SelectedValue = abstractRecord.AfferentPupillaryDefect.ToString();

                if (abstractRecord.RBCentralCornealODNotAbletoPerform != null) RadioButtonListRBCentralCornealNotAbletoPerformOD.SelectedValue = abstractRecord.RBCentralCornealODNotAbletoPerform.ToString();
                if (abstractRecord.RBCentralCornealOSNotAbletoPerform != null) RadioButtonListRBCentralCornealNotAbletoPerformOS.SelectedValue = abstractRecord.RBCentralCornealOSNotAbletoPerform.ToString();

                if (abstractRecord.OutcomeRBBCVAODNotAbletoPerform != null) RadioButtonListOutcomeRBBCVAODNotAbletoPerform.SelectedValue = abstractRecord.OutcomeRBBCVAODNotAbletoPerform.ToString();
                if (abstractRecord.OutcomeRBBCVAOSNotAbletoPerform != null) RadioButtonListOutcomeRBBCVAOSNotAbletoPerform.SelectedValue = abstractRecord.OutcomeRBBCVAOSNotAbletoPerform.ToString();



                if (abstractRecord.RBGonioscopyOD != null) RadioButtonListRBGonioscopyOD.SelectedValue = abstractRecord.RBGonioscopyOD.ToString();
                if (abstractRecord.RBGonioscopyOS != null) RadioButtonListRBGonioscopyOS.SelectedValue = abstractRecord.RBGonioscopyOS.ToString();
                if (abstractRecord.RBHistoryFirstDegreeRelative != null) RadioButtonListRBHistoryFirstDegreeRelative.SelectedValue = abstractRecord.RBHistoryFirstDegreeRelative.ToString();
                if (abstractRecord.RBOpticNerveAppearanceChangeFromBaseline != null) RadioButtonListRBOpticNerveAppearanceChangeFromBaseline.SelectedValue = abstractRecord.RBOpticNerveAppearanceChangeFromBaseline.ToString();
                if (abstractRecord.RBOpticNerveApperanceOD != null) RadioButtonListRBOpticNerveApperanceOD.SelectedValue = abstractRecord.RBOpticNerveApperanceOD.ToString();
                if (abstractRecord.RBOpticNerveApperanceOS != null) RadioButtonListRBOpticNerveApperanceOS.SelectedValue = abstractRecord.RBOpticNerveApperanceOS.ToString();

                if (abstractRecord.EyeMostAdvancedDisease != null) RadioButtonListEyeMostAdvancedDisease.SelectedValue = abstractRecord.EyeMostAdvancedDisease.ToString();

                if (abstractRecord.RBIOPODNotAbletoPerform != null) RadioButtonListRBIOPODNotAbletoPerform.SelectedValue = abstractRecord.RBIOPODNotAbletoPerform.ToString();






                if (abstractRecord.TargetIOPNotAbleToPerform != null) RadioButtonListTargetIOPNotAbleToPerform.SelectedValue = abstractRecord.TargetIOPNotAbleToPerform.ToString();

            }
        }
    }

    public void savedata() {
        using (NetHealthPIMEntities pim = new NetHealthPIMEntities()) {
            int ModuleID = (int)Session[Constants.SESSION_WORKINGMODULEID];
            int cycleID = ctx.ActiveModuleCycleID;
            ParticipantModuleCycle cycle = pim.ParticipantModuleCycle.First(c => c.ParticipantModuleCycleID == cycleID);
            string recordIdentifier = HiddenFieldRecordIdentifier.Value;
            using (TransactionScope transaction = new TransactionScope()) {
                ChartAbstractionPOAG abstractRecord = (from c in pim.ChartAbstractionPOAG
                                                       where c.ParticipantModuleCycle.ParticipantModuleCycleID == cycleID & c.RecordIdentifier == recordIdentifier
                                                       select c).First<ChartAbstractionPOAG>();

                if (RadioButtonOutcomeVisualFieldMeanDeviationSignODPlus.Checked) abstractRecord.OutcomeVisualFieldMeanDeviationSignOD = true;
                if (RadioButtonOutcomeVisualFieldMeanDeviationSignODMinus.Checked) abstractRecord.OutcomeVisualFieldMeanDeviationSignOD = false;
                if (RadioButtonOutcomeVisualFieldMeanDeviationSignOSPlus.Checked) abstractRecord.OutcomeVisualFieldMeanDeviationSignOS = true;
                if (RadioButtonOutcomeVisualFieldMeanDeviationSignOSMinus.Checked) abstractRecord.OutcomeVisualFieldMeanDeviationSignOS = false;
                if (RadioButtonVisualFieldMeanDeviationSignODPlus.Checked) abstractRecord.VisualFieldMeanDeviationSignOD = true;
                if (RadioButtonVisualFieldMeanDeviationSignODMinus.Checked) abstractRecord.VisualFieldMeanDeviationSignOD = false;
                if (RadioButtonVisualFieldMeanDeviationSignODPlus.Checked) abstractRecord.VisualFieldMeanDeviationSignOD = true;
                if (RadioButtonVisualFieldMeanDeviationSignODMinus.Checked) abstractRecord.VisualFieldMeanDeviationSignOD = false;
                if (RadioButtonVisualFieldMeanDeviationSignOSPlus.Checked) abstractRecord.VisualFieldMeanDeviationSignOS = true;
                if (RadioButtonVisualFieldMeanDeviationSignOSMinus.Checked) abstractRecord.VisualFieldMeanDeviationSignOS = false;
                if (RadioButtonManagementPlanFormulatedYes.Checked) abstractRecord.ManagementPlanFormulated = true;
                if (RadioButtonManagementPlanFormulatedNo.Checked) abstractRecord.ManagementPlanFormulated = false;
                if (RadioButtonMedicalHistoryYes.Checked) abstractRecord.MedicalHistory = true;
                if (RadioButtonMedicalHistoryNo.Checked) abstractRecord.MedicalHistory = false;
                if (RadioButtonOcularHistoryYes.Checked) abstractRecord.OcularHistory = true;
                if (RadioButtonOcularHistoryNo.Checked) abstractRecord.OcularHistory = false;

                if (RadioButtonListAutomatedVisualFields.SelectedIndex != -1)
                    abstractRecord.AutomatedVisualFields = Convert.ToInt32(RadioButtonListAutomatedVisualFields.SelectedValue);
                else
                    abstractRecord.AutomatedVisualFields = 0;

                if (RadioButtonFollowUpExamScheduledYes.Checked) abstractRecord.FollowUpExamScheduled = true;
                if (RadioButtonFollowUpExamScheduledNo.Checked) abstractRecord.FollowUpExamScheduled = false;

                if (RadioButtonImpactDailyLivingYes.Checked) abstractRecord.ImpactDailyLiving = true;
                if (RadioButtonImpactDailyLivingNo.Checked) abstractRecord.ImpactDailyLiving = false; ;

                if (RadioButtonListOpticNerveFindingsInterpreted.SelectedIndex != -1) abstractRecord.OpticNerveFindingsInterpreted = Convert.ToInt32(RadioButtonListOpticNerveFindingsInterpreted.SelectedValue);
                else abstractRecord.OpticNerveFindingsInterpreted = 0;




                if (DropDownListBCVAOD.SelectedIndex > 0) abstractRecord.BCVAOD = Convert.ToInt32(DropDownListBCVAOD.SelectedValue);
                else abstractRecord.BCVAOD = 0;
                if (DropDownListBCVAOS.SelectedIndex > 0) abstractRecord.BCVAOS = Convert.ToInt32(DropDownListBCVAOS.SelectedValue);
                else abstractRecord.BCVAOS = 0;
                if (DropDownListFollowUpRecentExamMonth.SelectedIndex > 0) abstractRecord.FollowUpRecentExamMonth = Convert.ToInt32(DropDownListFollowUpRecentExamMonth.SelectedValue);
                else abstractRecord.FollowUpRecentExamMonth = 0;
                if (DropDownListFollowUpExamScheduledMonth.SelectedIndex > 0) abstractRecord.FollowUpExamScheduledMonth = Convert.ToInt32(DropDownListFollowUpExamScheduledMonth.SelectedValue);
                else abstractRecord.FollowUpExamScheduledMonth = 0;
                if (DropDownListFollowUpExamScheduledYear.SelectedIndex > 0) abstractRecord.FollowUpExamScheduledYear = Convert.ToInt32(DropDownListFollowUpExamScheduledYear.SelectedValue);
                else abstractRecord.FollowUpExamScheduledYear = 0;
                if (DropDownListFollowUpRecentExamYear.SelectedIndex > 0) abstractRecord.FollowUpRecentExamYear = Convert.ToInt32(DropDownListFollowUpRecentExamYear.SelectedValue);
                else abstractRecord.FollowUpRecentExamYear = 0;
                if (DropDownListMonthOfBirth.SelectedIndex > 0) abstractRecord.MonthOfBirth = Convert.ToInt32(DropDownListMonthOfBirth.SelectedValue);
                else abstractRecord.MonthOfBirth = 0;
                if (DropDownListMonthOfExam.SelectedIndex > 0) abstractRecord.MonthOfExam = Convert.ToInt32(DropDownListMonthOfExam.SelectedValue);
                else abstractRecord.MonthOfExam = 0;
                if (DropDownListOutcomeBCVAOD.SelectedIndex > 0) abstractRecord.OutcomeBCVAOD = Convert.ToInt32(DropDownListOutcomeBCVAOD.SelectedValue);
                else abstractRecord.OutcomeBCVAOD = 0;
                if (DropDownListOutcomeBCVAOS.SelectedIndex > 0) abstractRecord.OutcomeBCVAOS = Convert.ToInt32(DropDownListOutcomeBCVAOS.SelectedValue);
                else abstractRecord.OutcomeBCVAOS = 0;
                if (DropDownListOutcomeOpticNerveExamMonth.SelectedIndex > 0) abstractRecord.OutcomeOpticNerveExamMonth = Convert.ToInt32(DropDownListOutcomeOpticNerveExamMonth.SelectedValue);
                else abstractRecord.OutcomeOpticNerveExamMonth = 0;
                if (DropDownListOutcomeOpticNerveExamMonth.SelectedIndex > 0) abstractRecord.OutcomeOpticNerveExamYear = Convert.ToInt32(DropDownListOutcomeOpticNerveExamYear.SelectedValue);
                else abstractRecord.OutcomeOpticNerveExamYear = 0;

                if (DropDownListOutcomeVisualFieldODMonth.SelectedIndex > 0) abstractRecord.OutcomeVisualFieldODMonth = Convert.ToInt32(DropDownListOutcomeVisualFieldODMonth.SelectedValue);
                else abstractRecord.OutcomeVisualFieldODMonth = 0;
                if (DropDownListOutcomeVisualFieldOSMonth.SelectedIndex > 0) abstractRecord.OutcomeVisualFieldOSMonth = Convert.ToInt32(DropDownListOutcomeVisualFieldOSMonth.SelectedValue);
                else abstractRecord.OutcomeVisualFieldOSMonth = 0;
                if (DropDownListOutcomeVisualFieldOSYear.SelectedIndex > 0) abstractRecord.OutcomeVisualFieldOSYear = Convert.ToInt32(DropDownListOutcomeVisualFieldOSYear.SelectedValue);
                else abstractRecord.OutcomeVisualFieldOSYear = 0;
                if (DropDownListOutcomVisualFieldODYear.SelectedIndex > 0) abstractRecord.OutcomVisualFieldODYear = Convert.ToInt32(DropDownListOutcomVisualFieldODYear.SelectedValue);
                else abstractRecord.OutcomVisualFieldODYear = 0;
                if (DropDownListYearOfBirth.SelectedIndex > 0) abstractRecord.YearOfBirth = Convert.ToInt32(DropDownListYearOfBirth.SelectedValue);
                else abstractRecord.YearOfBirth = 0;
                if (DropDownListYearOfExam.SelectedIndex > 0) abstractRecord.YearOfExam = Convert.ToInt32(DropDownListYearOfExam.SelectedValue);
                else abstractRecord.YearOfExam = 0;
                abstractRecord.VisualFieldPSDOSNA = CheckBoxVisualFieldPSDOSNA.Checked;
                abstractRecord.OutcomeVisualFieldPSDODNA = CheckBoxOutcomeVisualFieldPSDODNA.Checked;
                abstractRecord.OutcomeVisualFieldPSDOSNA = CheckBoxOutcomeVisualFieldPSDOSNA.Checked;
                abstractRecord.VisualFieldPSDODNA = CheckBoxVisualFieldPSDODNA.Checked;
                try {
                    if (TextBoxCentralCornealThicknessOD.Text != null)
                        abstractRecord.CentralCornealThicknessOD = Convert.ToDouble(TextBoxCentralCornealThicknessOD.Text);
                }
                catch (Exception ex) {
                    abstractRecord.CentralCornealThicknessOD = null;
                    TextBoxCentralCornealThicknessOD.Text = "";
                }
                try {
                    if (TextBoxCentralCornealThicknessOS.Text != null)
                        abstractRecord.CentralCornealThicknessOS = Convert.ToDouble(TextBoxCentralCornealThicknessOS.Text);
                }
                catch (Exception ex) {
                    abstractRecord.CentralCornealThicknessOS = null;
                    TextBoxCentralCornealThicknessOS.Text = "";
                }
                try {
                    if (TextBoxIOPODmmHg.Text != null)
                        abstractRecord.IOPODmmHg = Convert.ToDouble(TextBoxIOPODmmHg.Text);
                }
                catch (Exception ex) {
                    abstractRecord.IOPODmmHg = null;
                    TextBoxIOPODmmHg.Text = "";
                }
                try {
                    if (TextBoxOutcomeIOPODmmHg.Text != null)
                        abstractRecord.OutcomeIOPODmmHg = Convert.ToDouble(TextBoxOutcomeIOPODmmHg.Text);
                }
                catch (Exception ex) {
                    abstractRecord.OutcomeIOPODmmHg = null;
                    TextBoxOutcomeIOPODmmHg.Text = "";
                }


                try {
                    if (TextBoxOutcomeIOPOSmmHg.Text != null)
                        abstractRecord.OutcomeIOPOSmmHg = Convert.ToDouble(TextBoxOutcomeIOPOSmmHg.Text);
                }
                catch (Exception ex) {
                    abstractRecord.OutcomeIOPOSmmHg = null;
                    TextBoxOutcomeIOPOSmmHg.Text = "";
                }




                try {
                    if (TextBoxIOPOSmmHg.Text != null)
                        abstractRecord.IOPOSmmHg = Convert.ToDouble(TextBoxIOPOSmmHg.Text);
                }
                catch (Exception ex) {
                    abstractRecord.IOPOSmmHg = null;
                    TextBoxIOPOSmmHg.Text = "";
                }







                try {
                    if (TextBoxOutcomeVisualFieldMeanDeviationValueOD.Text != null)
                        abstractRecord.OutcomeVisualFieldMeanDeviationValueOD = Convert.ToDouble(TextBoxOutcomeVisualFieldMeanDeviationValueOD.Text);
                }
                catch (Exception ex) {
                    abstractRecord.OutcomeVisualFieldMeanDeviationValueOD = null;
                    TextBoxOutcomeVisualFieldMeanDeviationValueOD.Text = "";
                }
                try {
                    if (TextBoxOutcomeVisualFieldMeanDeviationValueOS.Text != null)
                        abstractRecord.OutcomeVisualFieldMeanDeviationValueOS = Convert.ToDouble(TextBoxOutcomeVisualFieldMeanDeviationValueOS.Text);
                }
                catch (Exception ex) {
                    abstractRecord.OutcomeVisualFieldMeanDeviationValueOS = null;
                    TextBoxOutcomeVisualFieldMeanDeviationValueOS.Text = "";
                }
                try {
                    if (TextBoxOutcomeVisualFieldPSDValueOD.Text != null)
                        abstractRecord.OutcomeVisualFieldPSDValueOD = Convert.ToDouble(TextBoxOutcomeVisualFieldPSDValueOD.Text);
                }
                catch (Exception ex) {
                    abstractRecord.OutcomeVisualFieldPSDValueOD = null;
                    TextBoxOutcomeVisualFieldPSDValueOD.Text = "";
                }
                try {
                    if (TextBoxOutcomeVisualFieldPSDValueOS.Text != null)
                        abstractRecord.OutcomeVisualFieldPSDValueOS = Convert.ToDouble(TextBoxOutcomeVisualFieldPSDValueOS.Text);
                }
                catch (Exception ex) {
                    abstractRecord.OutcomeVisualFieldPSDValueOS = null;
                    TextBoxOutcomeVisualFieldPSDValueOS.Text = "";
                }
                try {
                    if (TextBoxVisualFieldMeanDeviationValueOD.Text != null)
                        abstractRecord.VisualFieldMeanDeviationValueOD = Convert.ToDouble(TextBoxVisualFieldMeanDeviationValueOD.Text);
                }
                catch (Exception ex) {
                    abstractRecord.VisualFieldMeanDeviationValueOD = null;
                    TextBoxVisualFieldMeanDeviationValueOD.Text = "";
                }
                try {
                    if (TextBoxVisualFieldMeanDeviationValueOS.Text != null)
                        abstractRecord.VisualFieldMeanDeviationValueOS = Convert.ToDouble(TextBoxVisualFieldMeanDeviationValueOS.Text);
                }
                catch (Exception ex) {
                    abstractRecord.VisualFieldMeanDeviationValueOS = null;
                    TextBoxVisualFieldMeanDeviationValueOS.Text = "";
                }
                try {
                    if (TextBoxVisualFieldPSDValueOD.Text != null)
                        abstractRecord.VisualFieldPSDValueOD = Convert.ToDouble(TextBoxVisualFieldPSDValueOD.Text);
                }
                catch (Exception ex) {
                    abstractRecord.VisualFieldPSDValueOD = null;
                    TextBoxVisualFieldPSDValueOD.Text = "";
                }
                try {
                    if (TextBoxVisualFieldPSDValueOS.Text != null)
                        abstractRecord.VisualFieldPSDValueOS = Convert.ToDouble(TextBoxVisualFieldPSDValueOS.Text);
                }
                catch (Exception ex) {
                    abstractRecord.VisualFieldPSDValueOS = null;
                    TextBoxVisualFieldPSDValueOS.Text = "";
                }

                try {
                    if (TextBoxTargetIOPMMHG.Text != null)
                        abstractRecord.TargetIOPMMHG = Convert.ToDouble(TextBoxTargetIOPMMHG.Text);
                }
                catch (Exception ex) {
                    abstractRecord.TargetIOPMMHG = null;
                    TextBoxTargetIOPMMHG.Text = "";
                }

                Participant participant = pim.Participant.First(p => p.ParticipantID == ctx.ParticipantID);
                var pqrsselected = pim.ParticipantModule.First(p => p.ParticipantID == participant.ParticipantID);
                Boolean pqrsused = Convert.ToBoolean(pqrsselected.PQRS);
                Int32 visityear = Convert.ToInt32("0" + DropDownListPQRSVisitYear.SelectedValue);
                bool pqrsenabled = Convert.ToBoolean(System.Configuration.ConfigurationManager.AppSettings["PQRSEnabled"]);
                if (pqrsused && visityear == 2015 && pqrsenabled) {

                    //try
                    //{
                    PQRSWebService.PPWebServiceClient client = new PQRSWebService.PPWebServiceClient();
                    client.ClientCredentials.UserName.UserName = "partner:2015";
                    client.ClientCredentials.UserName.Password = "48C1A443-54B0-4D00-A4A2-FE5E46C2F02E";



                    Int32 participantid = client.AddUser(0, "0", participant.ParticipantFirstName, participant.ParticipantLastName, participant.ParticipantEmailAddress, participant.ParticipantEmailAddress, "abo2015", "ABO"); 

                        String visit_date = DropDownListPQRSVisitMonth.SelectedValue.PadLeft(2, '0') + "/01/";
                        visit_date += DropDownListPQRSVisitYear.SelectedValue;
                        Boolean is_medicare = PQRSMedicare.SelectedValue.Equals("1");

                        DataTable values = new DataTable("PQRSPROVisit");
                        values.Columns.Add("MeasureID", typeof(Int32));
                        values.Columns.Add("QuestionType", typeof(String));
                        values.Columns.Add("QuestionID", typeof(Int32));
                        values.Columns.Add("QuestionIDSuffix", typeof(String));
                        values.Columns.Add("Value", typeof(String));
                        values.Columns.Add("CPT", typeof(String));
                        values.Columns.Add("Modifier", typeof(String));
                        values.Columns.Add("SubmissionYear", typeof(Int32));
						

                        String dob = DropDownListMonthOfBirth.SelectedValue.PadLeft(2, '0') + "/01/";
                        dob += DropDownListYearOfBirth.SelectedValue;

                        String ab = CheckBoxPQRSEncounterA.Checked ? "a" : "";
                        ab += CheckBoxPQRSEncounterB.Checked ? ",b" : "";
						
						String PQRSM12Encounter = "0";
						String PQRSM141Encounter = "0";
						//  LP - 12 is a or b for diagnosis, 141 is just a
						if (CheckBoxPQRSEncounterA.Checked | CheckBoxPQRSEncounterB.Checked) PQRSM12Encounter = "1";
						if (CheckBoxPQRSEncounterA.Checked ) PQRSM141Encounter = "1";
                        AddVisitDetail(ref values, 12, 3228, "1");
                        AddVisitDetail(ref values, 12, 3229, PQRSDiagnosis.SelectedValue);
						//AddVisitDetail(ref values, 12, 3230, PQRSEncounterCodes.SelectedValue);
                        AddVisitDetail(ref values, 12, 3230, PQRSM12Encounter);
                        AddVisitDetail(ref values, 12, 3231, PQRSOpticNerveEval.SelectedValue);
                        AddVisitDetail(ref values, 12, 3232, PQRSOpticNerveReason.SelectedValue);

                        
                        AddVisitDetail(ref values, 141, 3540, "1");
                        AddVisitDetail(ref values, 141, 3541, PQRSDiagnosis.SelectedValue);
                        AddVisitDetail(ref values, 141, 3542, PQRSM141Encounter);

                        
                        AddVisitDetail(ref values, 141, 3543, PQRSIOPDocumented.SelectedValue);
                        AddVisitDetail(ref values, 141, 3544, PQRSIOPReduced.SelectedValue);
						AddVisitDetail(ref values, 141, 3545, PQRSGlaucomaPlan.SelectedValue);
						
                        String patient_id = Request.QueryString["ri"].ToString();
                        if (!(cycle.CycleNumber == 1))
                        {
                            patient_id = "C2" + patient_id;
                        }
                        client.AddChart((Int32)participantid, 0, patient_id, visit_date, is_medicare, values);
                        client.Close();
                    //}
                    //catch
                    //{
                    //    ViewState["PQRSBroken"] = true;
                    //}
                }


                if (RadioButtonListRBBCVAODNotAbletoPerform.SelectedIndex != -1) abstractRecord.RBBCVAODNotAbletoPerform = Convert.ToInt32(RadioButtonListRBBCVAODNotAbletoPerform.SelectedValue);
                else abstractRecord.RBBCVAODNotAbletoPerform = 0;



                if (RadioButtonListRBIOPODNotAbletoPerform.SelectedIndex != -1) abstractRecord.RBIOPODNotAbletoPerform = Convert.ToInt32(RadioButtonListRBIOPODNotAbletoPerform.SelectedValue);
                else abstractRecord.RBIOPODNotAbletoPerform = 0;




                if (RadioButtonListRBBCVAOSNotAbletoPerform.SelectedIndex != -1) abstractRecord.RBBCVAOSNotAbletoPerform = Convert.ToInt32(RadioButtonListRBBCVAOSNotAbletoPerform.SelectedValue);
                else abstractRecord.RBBCVAOSNotAbletoPerform = 0;
                if (RadioButtonListRBIOPOSNotAbletoPerform.SelectedIndex != -1) abstractRecord.RBIOPOSNotAbletoPerform = Convert.ToInt32(RadioButtonListRBIOPOSNotAbletoPerform.SelectedValue);
                else abstractRecord.RBIOPOSNotAbletoPerform = 0;

                if (RadioButtonListOutcomeRBBCVAODNotAbletoPerform.SelectedIndex != -1) abstractRecord.OutcomeRBBCVAODNotAbletoPerform = Convert.ToInt32(RadioButtonListOutcomeRBBCVAODNotAbletoPerform.SelectedValue);
                else abstractRecord.OutcomeRBBCVAODNotAbletoPerform = 0;
                if (RadioButtonListOutcomeRBBCVAOSNotAbletoPerform.SelectedIndex != -1) abstractRecord.OutcomeRBBCVAOSNotAbletoPerform = Convert.ToInt32(RadioButtonListOutcomeRBBCVAOSNotAbletoPerform.SelectedValue);
                else abstractRecord.OutcomeRBBCVAOSNotAbletoPerform = 0;

                if (RadioButtonListEyeMostAdvancedDisease.SelectedIndex != -1) abstractRecord.EyeMostAdvancedDisease = Convert.ToInt32(RadioButtonListEyeMostAdvancedDisease.SelectedValue);
                else abstractRecord.EyeMostAdvancedDisease = 0;

                if (RadioButtonListRBOutcomeIOPODNotAbletoPerform.SelectedIndex != -1) abstractRecord.RBOutcomeIOPODNotAbletoPerform = Convert.ToInt32(RadioButtonListRBOutcomeIOPODNotAbletoPerform.SelectedValue);
                else abstractRecord.RBOutcomeIOPODNotAbletoPerform = 0;

                abstractRecord.OutcomeVisualFieldODNA = CheckBoxOutcomeVisualFieldODNA.Checked;


                abstractRecord.OutcomeVisualFieldOSNA = CheckBoxOutcomeVisualFieldOSNA.Checked;


                abstractRecord.OutcomeVisualFieldPSDODNA = CheckBoxOutcomeVisualFieldPSDODNA.Checked;


                abstractRecord.OutcomeOpticNerveExamNA = CheckBoxOutcomeOpticNerveExamNA.Checked;




                if (RadioButtonListTargetIOPNotAbleToPerform.SelectedIndex != -1) abstractRecord.TargetIOPNotAbleToPerform = Convert.ToInt32(RadioButtonListTargetIOPNotAbleToPerform.SelectedValue);
                else abstractRecord.TargetIOPNotAbleToPerform = 0;







                if (RadioButtonListRBOutcomeIOPOSNotAbletoPerform.SelectedIndex != -1) abstractRecord.RBOutcomeIOPOSNotAbletoPerform = Convert.ToInt32(RadioButtonListRBOutcomeIOPOSNotAbletoPerform.SelectedValue);
                else abstractRecord.RBOutcomeIOPOSNotAbletoPerform = 0;
                if (RadioButtonListRBOutcomeIOPODNotAbletoPerform.SelectedIndex != -1) abstractRecord.RBOutcomeIOPODNotAbletoPerform = Convert.ToInt32(RadioButtonListRBOutcomeIOPODNotAbletoPerform.SelectedValue);
                else abstractRecord.RBOutcomeIOPODNotAbletoPerform = 0;
                if (RadioButtonListRBOutcomeIOPOSNotAbletoPerform.SelectedIndex != -1) abstractRecord.RBOutcomeIOPOSNotAbletoPerform = Convert.ToInt32(RadioButtonListRBOutcomeIOPOSNotAbletoPerform.SelectedValue);
                else abstractRecord.RBOutcomeIOPOSNotAbletoPerform = 0;
                if (RadioButtonListAfferentPupillaryDefect.SelectedIndex != -1) abstractRecord.AfferentPupillaryDefect = Convert.ToInt32(RadioButtonListAfferentPupillaryDefect.SelectedValue);
                else abstractRecord.AfferentPupillaryDefect = 0;
                if (RadioButtonListRBGlaucomaSeverity.SelectedIndex != -1) abstractRecord.RBGlaucomaSeverity = Convert.ToInt32(RadioButtonListRBGlaucomaSeverity.SelectedValue);
                else abstractRecord.RBGlaucomaSeverity = 0;
                if (RadioButtonListRBCentralCornealNotAbletoPerformOD.SelectedIndex != -1) abstractRecord.RBCentralCornealODNotAbletoPerform = Convert.ToInt32(RadioButtonListRBCentralCornealNotAbletoPerformOD.SelectedValue);
                else abstractRecord.RBCentralCornealODNotAbletoPerform = 0;
                if (RadioButtonListRBCentralCornealNotAbletoPerformOS.SelectedIndex != -1) abstractRecord.RBCentralCornealOSNotAbletoPerform = Convert.ToInt32(RadioButtonListRBCentralCornealNotAbletoPerformOS.SelectedValue);
                else abstractRecord.RBCentralCornealOSNotAbletoPerform = 0;
                if (RadioButtonListRBGonioscopyOD.SelectedIndex != -1) abstractRecord.RBGonioscopyOD = Convert.ToInt32(RadioButtonListRBGonioscopyOD.SelectedValue);
                else abstractRecord.RBGonioscopyOD = 0;
                if (RadioButtonListRBGonioscopyOS.SelectedIndex != -1) abstractRecord.RBGonioscopyOS = Convert.ToInt32(RadioButtonListRBGonioscopyOS.SelectedValue);
                else abstractRecord.RBGonioscopyOS = 0;
                if (RadioButtonListRBHistoryFirstDegreeRelative.SelectedIndex != -1) abstractRecord.RBHistoryFirstDegreeRelative = Convert.ToInt32(RadioButtonListRBHistoryFirstDegreeRelative.SelectedValue);
                else abstractRecord.RBHistoryFirstDegreeRelative = 0;
                if (RadioButtonListRBOpticNerveAppearanceChangeFromBaseline.SelectedIndex != -1) abstractRecord.RBOpticNerveAppearanceChangeFromBaseline = Convert.ToInt32(RadioButtonListRBOpticNerveAppearanceChangeFromBaseline.SelectedValue);
                else abstractRecord.RBOpticNerveAppearanceChangeFromBaseline = 0;
                if (RadioButtonListRBOpticNerveApperanceOD.SelectedIndex != -1) abstractRecord.RBOpticNerveApperanceOD = Convert.ToInt32(RadioButtonListRBOpticNerveApperanceOD.SelectedValue);
                else abstractRecord.RBOpticNerveApperanceOD = 0;
                if (RadioButtonListRBOpticNerveApperanceOS.SelectedIndex != -1) abstractRecord.RBOpticNerveApperanceOS = Convert.ToInt32(RadioButtonListRBOpticNerveApperanceOS.SelectedValue);
                else abstractRecord.RBOpticNerveApperanceOS = 0;
                abstractRecord.LastUpdateDate = DateTime.Now;
                bool ChartCompleted = isComplete();
                abstractRecord.Complete = ChartCompleted;
                var chartRegistration = (from cr in pim.ChartRegistration
                                         where cr.ParticipantModuleCycleID == cycle.ParticipantModuleCycleID
                                          & cr.ModuleID == ModuleID
                                          & cr.RecordIdentifier == recordIdentifier
                                         select cr).First<NetHealthPIMModel.ChartRegistration>();
                chartRegistration.AbstractCompleted = ChartCompleted;


                if ((DropDownListYearOfBirth.SelectedIndex > 0) && (DropDownListYearOfBirth.SelectedIndex > 0)) {
                    chartRegistration.DOB = abstractRecord.MonthOfBirth + "/" + abstractRecord.YearOfBirth;
                }
                if ((DropDownListMonthOfExam.SelectedIndex > 0) && (DropDownListYearOfExam.SelectedIndex > 0)) {
                    chartRegistration.InitialVisit = abstractRecord.MonthOfExam + "/" + abstractRecord.YearOfExam;
                }
                pim.SaveChanges();

                transaction.Complete();
            }

            CycleManager.UpdateParticipantCompletedModules(cycleID, ModuleID);
        }
    }


    void AddVisitDetail(ref DataTable values, int measure_id, int question_id, string value) 
    { 
        AddVisitDetail(ref values, measure_id, question_id, value, "", "", ""); 
    }

    void AddVisitDetail(ref DataTable values, int measure_id, int question_id, string value, string cpt, string modifier, string suffix) {
        if (!String.IsNullOrEmpty(value)) {
            DataRow detail = values.NewRow();
            detail["MeasureID"] = measure_id;
            detail["QuestionType"] = (measure_id > 0 ? "M" : "G");
            detail["QuestionID"] = question_id;
            detail["QuestionIDSuffix"] = suffix;
            detail["Value"] = value;
            detail["CPT"] = cpt;
            detail["Modifier"] = modifier;
            detail["SubmissionYear"] = Convert.ToInt32(System.Configuration.ConfigurationManager.AppSettings["PQRSYear"]);
            values.Rows.Add(detail);
        }
    }

    protected void ButtonSubmit_Click(object sender, EventArgs e) {
        savedata();
        isComplete();
        if (!isComplete()) {
            isComplete();
            string script = " var answer = confirm('All fields are not complete.  Are you sure you want to submit?'); if (answer==true) window.location = '../PatientChartRegistration.aspx';";
            ScriptManager.RegisterStartupScript(this, GetType(),
                          "ServerControlScript", script, true);
        }
        else {

            Response.Redirect("../PatientChartRegistration.aspx");
        }
    }


    protected bool isComplete() {
        bool ChartCompleted = true;

        LabelRBHistoryFirstDegreeRelative.Visible = false;
        LabelYearOfBirth.Visible = false;
        LabelMonthOfBirth.Visible = false;
        LabelEyeMostAdvancedDisease.Visible = false;
        LabelOutcomeBCVAOD.Visible = false;
        LabelOutcomeBCVAOS.Visible = false;
        LabelOutcomeOpticNerveExamYeaer.Visible = false;
        LabelOutcomeOpticNerveExamMonth.Visible = false;
        LabelAfferentPupillaryDefect.Visible = false;
        LabelCentralCornealThicknessOS.Visible = false;
        LabelCentralCornealThicknessOD.Visible = false;
        LabelTargetIOP.Visible = false;
        LabelRBGonioscopyOD.Visible = false;
        LabelBCVAOS.Visible = false;
        LabelBCVAOD.Visible = false;
        LabelRBGonioscopyOS.Visible = false;
        LabelIOPOD.Visible = false;
        LabelIOPOS.Visible = false;
        LabelRBOpticNerveApperanceOD.Visible = false;
        LabelOutcomeBCVAOS.Visible = false;
        LabelOutcomeBCVAOD.Visible = false;
        LabelRBOpticNerveApperanceOS.Visible = false;
        LabelOutcomeVisualField.Visible = false;
        LabelRBGlaucomaSeverity.Visible = false;
        LabelMedicalHistory.Visible = false;
        LabelOutcomeIOPOS.Visible = false;
        LabelOutcomeIOPOD.Visible = false;
        LabelRBOpticNerveAppearanceChangeFromBaseline.Visible = false;
        //LabelPQRSDOBMonth.Visible = false;
        //LabelPQRSDOBYear.Visible = false;
        PQRSRecentODIOPNoPerformOS.Visible = false;
        //PQRSRecentODIOPNoPerformOD.Visible = false;
        LabelPQRSAffectedEye.Visible = false;
        LabelPQRSMedicare.Visible = false;
        LabelPQRSDiagnosis.Visible = false;

        LabelPQRSEncounter.Visible = false;

        LabelPQRSVisitMonth.Visible = false;

        LabelPQRSVisitYear.Visible = false;

        LabelPQRSOpticNerveEval.Visible = false;


        LabelNerveNo.Visible = false;
        LabelImpactDailyLiving.Visible = false;

        LabelOpticNerveFindingsInterpreted.Visible = false;

        LabelAutomatedVisualFields.Visible = false;

        LabelFollowUpExamScheduled.Visible = false;

        LabelManagementPlanFormulated.Visible = false;

        LabelVisualField.Visible = false;

        LabelOcularHistory.Visible = false;
        NetHealthPIMEntities pim = new NetHealthPIMEntities();
        var moduleinfo = (from c in pim.Module where c.ModuleID == 20 select c).FirstOrDefault();
        string message = "Patient must be  at least " + moduleinfo.RegistrationMinAge + " years old<br />";
        string registrationminage = Validation.validateRegistrationMinAge(Convert.ToInt32(DropDownListYearOfExam.SelectedValue), Convert.ToInt32(DropDownListYearOfBirth.SelectedValue), Convert.ToInt32(moduleinfo.RegistrationMinAge), message);

        if (registrationminage != "") {
            Labelinitialage.Text = message;
            Labelinitialage.Visible = true;
            ChartCompleted = false;
        }

        ///Pqrs Validation
        Participant participant = pim.Participant.First(p => p.ParticipantID == ctx.ParticipantID);
        var pqrsselected = pim.ParticipantModule.First(p => p.ParticipantID == participant.ParticipantID);
        bool pqrsused = Convert.ToBoolean(pqrsselected.PQRS);
        bool pqrsenabled = Convert.ToBoolean(System.Configuration.ConfigurationManager.AppSettings["PQRSEnabled"]);

        //if (pqrsused && pqrsenabled && ViewState["PQRSBroken"]==null)
        //{
        //    //if (DropDownListPQRSDOBMonth.SelectedIndex == 0) {
        //    //    LabelPQRSDOBMonth.Visible = true;
        //    //    ChartCompleted = false;
        //    //}
        //    //if (DropDownListPQRSDOBYear.SelectedIndex == 0) {
        //    //    LabelPQRSDOBYear.Visible = true;
        //    //    ChartCompleted = false;
        //    //}
        //    if (string.IsNullOrEmpty(PQRSMedicare.SelectedValue)) {
        //        LabelPQRSMedicare.Visible = true;
        //        ChartCompleted = false;
        //    }
        //    if (string.IsNullOrEmpty(PQRSDiagnosis.SelectedValue)) {
        //        LabelPQRSDiagnosis.Visible = true;
        //        ChartCompleted = false;
        //    }
        //    if (CheckBoxPQRSEncounterA.Checked == true || CheckBoxPQRSEncounterB.Checked == true) {
        //        if (DropDownListPQRSVisitMonth.SelectedIndex == 0) {
        //            LabelPQRSVisitMonth.Visible = true;
        //            ChartCompleted = false;
        //        }
        //        if (DropDownListPQRSVisitYear.SelectedIndex == 0) {
        //            LabelPQRSVisitYear.Visible = true;
        //            ChartCompleted = false;
        //        }
        //        if (String.IsNullOrEmpty(PQRSOpticNerveEval.SelectedValue)) {
        //            LabelPQRSOpticNerveEval.Visible = true;
        //            ChartCompleted = false;
        //        }
        //        else if (PQRSOpticNerveEval.SelectedValue.Equals("0") && String.IsNullOrEmpty(PQRSOpticNerveReason.SelectedValue)) {
        //            LabelNerveNo.Visible = true;
        //            ChartCompleted = false;
        //        }

        //        if (CheckBoxPQRSEncounterA.Checked == true) {
        //            //if (RadioButtonPQRSAffectedEyeOD.Checked && string.IsNullOrEmpty(PreInterventionOD.SelectedValue)) {
        //            //    PQRSRecentODIOPNoPerformOD.Visible = true;
        //            //    ChartCompleted = false;
        //            //}
        //            //if (RadioButtonPQRSAffectedEyeOS.Checked && string.IsNullOrEmpty(PreInterventionOS.SelectedValue)) {
        //            //    PQRSRecentODIOPNoPerformOS.Visible = true;
        //            //    ChartCompleted = false;
        //            //}
        //        }
        //    }
        //}

        if (RadioButtonListRBHistoryFirstDegreeRelative.SelectedIndex == -1) {
            LabelRBHistoryFirstDegreeRelative.Visible = true;
            ChartCompleted = false;
        }
        if (RadioButtonListEyeMostAdvancedDisease.SelectedIndex == -1) {
            LabelEyeMostAdvancedDisease.Visible = true;
            ChartCompleted = false;
        }
        if (RadioButtonListAfferentPupillaryDefect.SelectedIndex == -1) {
            LabelAfferentPupillaryDefect.Visible = true;
            ChartCompleted = false;
        }
        if (RadioButtonListRBGonioscopyOD.SelectedIndex == -1) {
            LabelRBGonioscopyOD.Visible = true;
            ChartCompleted = false;
        }
        if (RadioButtonListRBGonioscopyOS.SelectedIndex == -1) {
            LabelRBGonioscopyOS.Visible = true;
            ChartCompleted = false;
        }
        if (RadioButtonListRBOpticNerveApperanceOD.SelectedIndex == -1) {
            LabelRBOpticNerveApperanceOD.Visible = true;
            ChartCompleted = false;
        }
        if (RadioButtonListRBOpticNerveApperanceOS.SelectedIndex == -1) {
            LabelRBOpticNerveApperanceOS.Visible = true;
            ChartCompleted = false;
        }
        if (RadioButtonListRBGlaucomaSeverity.SelectedIndex == -1) {
            LabelRBGlaucomaSeverity.Visible = true;

            ChartCompleted = false;
        }
        if (RadioButtonListRBOpticNerveAppearanceChangeFromBaseline.SelectedIndex == -1) {
            LabelRBOpticNerveAppearanceChangeFromBaseline.Visible = true;
            ChartCompleted = false;
        }

        if (RadioButtonImpactDailyLivingNo.Checked == false && RadioButtonImpactDailyLivingYes.Checked == false) {
            LabelImpactDailyLiving.Visible = true;
            ChartCompleted = false;
        }

        if (RadioButtonListAutomatedVisualFields.SelectedIndex == -1) {
            LabelAutomatedVisualFields.Visible = true;
            ChartCompleted = false;
        }
        if (RadioButtonFollowUpExamScheduledNo.Checked == false && RadioButtonFollowUpExamScheduledYes.Checked == false) {
            LabelFollowUpExamScheduled.Visible = true;
            ChartCompleted = false;
        }
        if (RadioButtonManagementPlanFormulatedNo.Checked == false && RadioButtonManagementPlanFormulatedYes.Checked == false) {
            LabelManagementPlanFormulated.Visible = true;
            ChartCompleted = false;
        }

        if (RadioButtonMedicalHistoryYes.Checked == false && RadioButtonMedicalHistoryNo.Checked == false) {
            LabelMedicalHistory.Visible = true;
            ChartCompleted = false;


        }



        if (RadioButtonOcularHistoryNo.Checked == false && RadioButtonOcularHistoryYes.Checked == false) {
            LabelOcularHistory.Visible = true;
            ChartCompleted = false;


        }

        if (DropDownListBCVAOD.SelectedIndex == 0 && RadioButtonListRBBCVAODNotAbletoPerform.SelectedIndex == -1) {

            LabelBCVAOD.Visible = true;
            ChartCompleted = false;

        }
        if (DropDownListBCVAOS.SelectedIndex == 0 && RadioButtonListRBBCVAOSNotAbletoPerform.SelectedIndex == -1) {

            LabelBCVAOS.Visible = true;
            ChartCompleted = false;

        }
        if (TextBoxIOPODmmHg.Text == "" && RadioButtonListRBIOPODNotAbletoPerform.SelectedIndex == -1) {
            LabelIOPOD.Visible = true;
            ChartCompleted = false;

        }
        if (TextBoxIOPOSmmHg.Text == "" && RadioButtonListRBIOPOSNotAbletoPerform.SelectedIndex == -1) {
            LabelIOPOS.Visible = true;
            ChartCompleted = false;

        }
        if (TextBoxCentralCornealThicknessOD.Text == "" && RadioButtonListRBCentralCornealNotAbletoPerformOD.SelectedIndex == -1) {

            LabelCentralCornealThicknessOD.Visible = true;
            ChartCompleted = false;


        }
        if (TextBoxCentralCornealThicknessOS.Text == "" && RadioButtonListRBCentralCornealNotAbletoPerformOS.SelectedIndex == -1) {

            LabelCentralCornealThicknessOS.Visible = true;
            ChartCompleted = false;


        }


        if (TextBoxTargetIOPMMHG.Text == "" && RadioButtonListTargetIOPNotAbleToPerform.SelectedIndex == -1) {

            LabelTargetIOP.Visible = true;
            ChartCompleted = false;
        }
        if (DropDownListOutcomeBCVAOS.SelectedIndex == 0 && RadioButtonListOutcomeRBBCVAOSNotAbletoPerform.SelectedIndex == -1) {

            LabelOutcomeBCVAOS.Visible = true;
            ChartCompleted = false;


        }
        if (DropDownListOutcomeBCVAOD.SelectedIndex == 0 && RadioButtonListOutcomeRBBCVAODNotAbletoPerform.SelectedIndex == -1) {

            LabelOutcomeBCVAOD.Visible = true;
            ChartCompleted = false;


        }
        if (RadioButtonListRBOutcomeIOPODNotAbletoPerform.SelectedIndex == -1 && TextBoxOutcomeIOPODmmHg.Text == "") {
            LabelOutcomeIOPOD.Visible = true;
            ChartCompleted = false;

        }
        if (DropDownListMonthOfBirth.SelectedIndex == 0) {
            LabelMonthOfBirth.Visible = true;
            ChartCompleted = false;


        }
        if (DropDownListYearOfBirth.SelectedIndex == 0) {
            LabelYearOfBirth.Visible = true;
            ChartCompleted = false;


        }
        if (RadioButtonListRBOutcomeIOPOSNotAbletoPerform.SelectedIndex == -1 && TextBoxOutcomeIOPOSmmHg.Text == "") {
            LabelOutcomeIOPOS.Visible = true;
            ChartCompleted = false;

        }


        if (CheckBoxOutcomeOpticNerveExamNA.Checked == false && DropDownListOutcomeOpticNerveExamYear.SelectedIndex == 0) {
            LabelOutcomeOpticNerveExamYeaer.Visible = true;
            ChartCompleted = false;

        }

        return ChartCompleted;
    }
}