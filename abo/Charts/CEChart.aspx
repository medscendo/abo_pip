﻿<%@ Page Title="" Language="C#" MasterPageFile="~/PIMMasterPage.master" AutoEventWireup="true" CodeFile="CEChart.aspx.cs" Inherits="abo_CEChart" %>

<asp:Content ID="Content1" ContentPlaceHolderID="head" Runat="Server">

    <link type="text/css" href="../../common/css/atooltip.css" rel="stylesheet"  media="screen" />
	<script type="text/javascript" src="../../common/js/jquery.min.js"></script>
	<script type="text/javascript" src="../../common/js/jquery.atooltip.js"></script>
    <script type="text/javascript" language="javascript">
        //      Either enables or disables input boxes
        function enabledControl(el, disabled, checked) {
            //alert("Hello world from enabled control");
            try {
                if (disabled) {
                    el.disabled = disabled;
                    el.setAttribute("disabled", true);
                }
                else {
                    el.disabled = disabled;
                    el.removeAttribute("disabled");
                }

                if (checked == true)
                    el.checked = false;
            }
            catch (E) {
            }
            if (el.childNodes && el.childNodes.length > 0) {
                for (var x = 0; x < el.childNodes.length; x++) {
                    enabledControl(el.childNodes[x], disabled, checked);
                }
            }
        }
        //      Either enables or disables dropdown boxes
        function enabledControlDropDown(el, disabled, checked) {
            //alert("Hello world from enabled control");
            try {
                if (disabled) {
                    el.disabled = disabled;
                    el.setAttribute("disabled", true);
                }
                else {
                    el.disabled = disabled;
                    el.removeAttribute("disabled");
                }

                if (checked == true)
                    el.options[0].selected = true;
            }
            catch (E) {
            }
            if (el.childNodes && el.childNodes.length > 0) {
                for (var x = 0; x < el.childNodes.length; x++) {
                    enabledControl(el.childNodes[x], disabled, checked);
                }
            }
        }
        //      Either enables or disables text boxes
        function enabledControlTextField(el, disabled, checked) {
            //alert("Hello world from enabled control");
            try {
                if (disabled) {
                    el.disabled = disabled;
                    el.setAttribute("disabled", true);
                }
                else {
                    el.disabled = disabled;
                    el.removeAttribute("disabled");
                }

                if (checked == true) {
                    el.value = '';
                }
            }
            catch (E) {
            }
            if (el.childNodes && el.childNodes.length > 0) {
                for (var x = 0; x < el.childNodes.length; x++) {
                    enabledControl(el.childNodes[x], disabled, checked);
                }
            }
        }

        // Toggles Qpr2 depending on how the user answers
        function toggleQPr2CB() {
            enabledControl(document.getElementById("<%= CheckBoxRBBCVAdistanceNotAbletoPerform.ClientID %>"), false, true);
        }
        function toggleQPr2DD() {
            enabledControlDropDown(document.getElementById("<%= DropDownListBCVADistance.ClientID %>"), false, true);
        }

        // Toggles QD2 depending on how the user answers
        function toggleQD2TB() {
            enabledControlTextField(document.getElementById("<%= TextBoxCornealPachymetryValue.ClientID %>"), false, true);
        }
        function toggleQD2CB() {
            enabledControl(document.getElementById("<%= CheckBoxCornealPachymetryNP.ClientID %>"), false, true);
        }

        function QS2toggleTB() {
            enabledControlTextField(document.getElementById("<%= TextBoxPachymetry.ClientID %>"), false, true);
        }

        function QS2toggleRB() {
            enabledControl(document.getElementById("<%= RadioButtonPachymetryNA.ClientID %>"), false, true);
        }

        /*function QM8isClicked() {

            if (document.getElementById("<%= RadioButtonReoperationYes.ClientID %>").checked == true) {
        enabledControlTextField(document.getElementById("<%= TextBoxReoperationText.ClientID %>"), false, false);
        document.getElementById("QM8aTxt").style.color = "#333";
        }
        else {
        enabledControlTextField(document.getElementById("<%= TextBoxReoperationText.ClientID %>"), true, true);
        document.getElementById("QM8aTxt").style.color = "gray";
        }
        }*/

    </script>
    <script type="text/javascript">
        function generate(arr1, arr2, istrue) {
            if (istrue == true) {
                for (var i = 0; i < arr1.length; i++) {
                    document.getElementById(arr1[i]).style.color = "gray";
                }
                for (var i = 0; i < arr2.length; i++) {
                    $(arr2[i] + ' :input').attr('disabled', true);
                    $(arr2[i] + ' :input').removeAttr("checked");
                }
            }
            if (istrue == false) {
                for (var i = 0; i < arr1.length; i++) {
                    document.getElementById(arr1[i]).style.color = "#333";
                }
                for (var i = 0; i < arr2.length; i++) {
                    $(arr2[i] + ' :input').removeAttr('disabled');
                }
            }
        }

        $(document).ready(function() {

            // Question 4 Diagnostic Testing, part A **************
            $("#<%= CheckBoxCornealEdemaGlaucoma.ClientID %>").click(function() {
                if ($('#<%= CheckBoxCornealEdemaGlaucoma.ClientID %>').prop("checked") == false) {
                    var myaray = new Array("Q4part1");
                    var myarray2 = new Array('#Q4part1');
                    generate(myaray, myarray2, true);
                }
                else {
                    var myaray = new Array("Q4part1");
                    var myarray2 = new Array('#Q4part1');
                    generate(myaray, myarray2, false);
                }
            });
            if ($('#<%= CheckBoxCornealEdemaGlaucoma.ClientID %>').prop("checked") == false) {
                var myaray = new Array("Q4part1");
                var myarray2 = new Array('#Q4part1');
                generate(myaray, myarray2, true);
            }
            else {
                var myaray = new Array("Q4part1");
                var myarray2 = new Array('#Q4part1');
                generate(myaray, myarray2, false);
            }

            // Question 4 Diagnostic Testing, part B **************
            $("#<%= CheckBoxCornealEdemaPostopEdema.ClientID %>").click(function() {
                if ($('#<%= CheckBoxCornealEdemaPostopEdema.ClientID %>').prop("checked") == false) {
                    var myaray = new Array("Q4part2");
                    var myarray2 = new Array('#Q4part2');
                    generate(myaray, myarray2, true);
                }
                else {
                    var myaray = new Array("Q4part2");
                    var myarray2 = new Array('#Q4part2');
                    generate(myaray, myarray2, false);
                }
            });
            if ($('#<%= CheckBoxCornealEdemaPostopEdema.ClientID %>').prop("checked") == false) {
                var myaray = new Array("Q4part2");
                var myarray2 = new Array('#Q4part2');
                generate(myaray, myarray2, true);
            }
            else {
                var myaray = new Array("Q4part2");
                var myarray2 = new Array('#Q4part2');
                generate(myaray, myarray2, false);
            }

            // Question 4 Diagnostic Testing, part C **************
            $("#<%= CheckBoxCornealEdemacongenital.ClientID %>").click(function() {
                if ($('#<%= CheckBoxCornealEdemacongenital.ClientID %>').prop("checked") == false) {
                    var myaray = new Array("Q4part3");
                    var myarray2 = new Array('#Q4part3');
                    generate(myaray, myarray2, true);
                }
                else {
                    var myaray = new Array("Q4part3");
                    var myarray2 = new Array('#Q4part3');
                    generate(myaray, myarray2, false);
                }
            });
            if ($('#<%= CheckBoxCornealEdemacongenital.ClientID %>').prop("checked") == false) {
                var myaray = new Array("Q4part3");
                var myarray2 = new Array('#Q4part3');
                generate(myaray, myarray2, true);
            }
            else {
                var myaray = new Array("Q4part3");
                var myarray2 = new Array('#Q4part3');
                generate(myaray, myarray2, false);
            }

            // Question 4 Diagnosis/Assessment **************
            $("#<%= CheckBoxLensPseudophakic.ClientID %>").click(function() {
                if ($('#<%= CheckBoxLensPseudophakic.ClientID %>').prop("checked") == false) {
                    var myaray = new Array("clickLensPseudophakictxt");
                    var myarray2 = new Array('#clickLensPseudophakictxt');
                    generate(myaray, myarray2, true);
                }
                else {
                    var myaray = new Array("clickLensPseudophakictxt");
                    var myarray2 = new Array('#clickLensPseudophakictxt');
                    generate(myaray, myarray2, false);
                }
            });
            if ($('#<%= CheckBoxLensPseudophakic.ClientID %>').prop("checked") == false) {
                var myaray = new Array("clickLensPseudophakictxt");
                var myarray2 = new Array('#clickLensPseudophakictxt');
                generate(myaray, myarray2, true);
            }
            else {
                var myaray = new Array("clickLensPseudophakictxt");
                var myarray2 = new Array('#clickLensPseudophakictxt');
                generate(myaray, myarray2, false);
            }

            // Question 8 Management **************
            $("#<%= RadioButtonReoperationYes.ClientID %>").click(function() {
                if ($('#<%= RadioButtonReoperationYes.ClientID %>').prop("checked") == false) {
                    var myaray = new Array("QM8aTxt");
                    var myarray2 = new Array('#QM8aTxt');
                    generate(myaray, myarray2, true);

                    enabledControlTextField(document.getElementById("<%= TextBoxReoperationText.ClientID %>"), true, true);
                }
                else {
                    var myaray = new Array("QM8aTxt");
                    var myarray2 = new Array('#QM8aTxt');
                    generate(myaray, myarray2, false);

                    enabledControlTextField(document.getElementById("<%= TextBoxReoperationText.ClientID %>"), false, false);
                }
            });
            $("#<%= RadioButtonReoperationNo.ClientID %>").click(function() {
                if ($('#<%= RadioButtonReoperationYes.ClientID %>').prop("checked") == false) {
                    var myaray = new Array("QM8aTxt");
                    var myarray2 = new Array('#QM8aTxt');
                    generate(myaray, myarray2, true);

                    enabledControlTextField(document.getElementById("<%= TextBoxReoperationText.ClientID %>"), true, true);
                }
                else {
                    var myaray = new Array("QM8aTxt");
                    var myarray2 = new Array('#QM8aTxt');
                    generate(myaray, myarray2, false);

                    enabledControlTextField(document.getElementById("<%= TextBoxReoperationText.ClientID %>"), false, false);
                }
            });
            if ($('#<%= RadioButtonReoperationYes.ClientID %>').prop("checked") == false) {
                var myaray = new Array("QM8aTxt");
                var myarray2 = new Array('#QM8aTxt');
                generate(myaray, myarray2, true);

                enabledControlTextField(document.getElementById("<%= TextBoxReoperationText.ClientID %>"), true, true);
            }
            else {
                var myaray = new Array("QM8aTxt");
                var myarray2 = new Array('#QM8aTxt');
                generate(myaray, myarray2, false);

                enabledControlTextField(document.getElementById("<%= TextBoxReoperationText.ClientID %>"), false, false);
            }

            // Question 1 Complications **************
            $("#<%= RadioButtonEndoRejectionYes.ClientID %>").click(function() {
                if ($('#<%= RadioButtonEndoRejectionYes.ClientID %>').prop("checked") == false) {
                    var myaray = new Array("QC2txta", "QC2txtb");
                    var myarray2 = new Array('#QC2txtb');
                    generate(myaray, myarray2, true);
                }
                else {
                    var myaray = new Array("QC2txta", "QC2txtb");
                    var myarray2 = new Array('#QC2txtb');
                    generate(myaray, myarray2, false);
                }
            });
            $("#<%= RadioButtonEndoRejectionNo.ClientID %>").click(function() {
                if ($('#<%= RadioButtonEndoRejectionYes.ClientID %>').prop("checked") == false) {
                    var myaray = new Array("QC2txta", "QC2txtb");
                    var myarray2 = new Array('#QC2txtb');
                    generate(myaray, myarray2, true);
                }
                else {
                    var myaray = new Array("QC2txta", "QC2txtb");
                    var myarray2 = new Array('#QC2txtb');
                    generate(myaray, myarray2, false);
                }
            });
            if ($('#<%= RadioButtonEndoRejectionYes.ClientID %>').prop("checked") == false) {
                var myaray = new Array("QC2txta", "QC2txtb");
                var myarray2 = new Array('#QC2txtb');
                generate(myaray, myarray2, true);
            }
            else {
                var myaray = new Array("QC2txta", "QC2txtb");
                var myarray2 = new Array('#QC2txtb');
                generate(myaray, myarray2, false);
            }
        });

    </script>
    <style type="text/css">
        .bginputa{
	        float:right;
	        margin:21px 0 0 1px;
	        background: url(../common/images/orange_button_with_arrow.png) no-repeat;
	        overflow:hidden;
	        height:35px;

        }
        .bginputa a{
	        color: white;
	        text-align:center;
	        height:35px;
	        line-height:28px;
	        padding:0 14px 15px  ;
	        cursor:pointer;
	        float:left;
	        border:none;
	        text-decoration:none;
	        font-family:Arial, Helvetica, sans-serif; 
	        font-size:12px; 
	        font-weight:bold; 
	        letter-spacing: 0px;
        }
        .bginputa a:hover{
	        text-decoration:none;
        }
        .right {
            text-align:right;
        }
    </style>
    
    <script type="text/javascript">

        $(function() {
            $('#QD3').aToolTip({
                clickIt: true,
                tipContent: 'Specular microscopy or confocal microscopy'
            });

            $('#QM7').aToolTip({
                clickIt: true,
                tipContent: 'A corneal epithelial defect present continuously for &gt;3 weeks'
            });

            $('#QC3').aToolTip({
                clickIt: true,
                tipContent: 'Loss of graft clarity due to edema without evidence of a preceding rejection episode'
            });

            $('#QD4a').aToolTip({
                clickIt: true,
                tipContent: 'Postoperative edema: i.e. pseudophakic bullous keratopathy<br/>Anterior segment dysgenesis: i.e. peters anomaly'
            });

            $('#QC5').aToolTip({
                clickIt: true,
                tipContent: 'Initiation or addition of medical therapy or surgical therapy'
            });

            $('#QP2').aToolTip({
                clickIt: true,
                tipContent: '20/800 or count fingers @ 5 ft<br />20/1000 or count fingers @ 4 ft<br />20/1600 or count fingers @ 3ft<br />20/2000 or count fingers @ 2 ft<br />20/4000 or count fingers @ 1 ft<br />20/7777 =CF<br />20/8888 =HM<br />20/9999 =LP<br />20/0000 =NLP'
            });

            $('#QO1').aToolTip({
                clickIt: true,
                tipContent: '20/800 or count fingers @ 5 ft<br />20/1000 or count fingers @ 4 ft<br />20/1600 or count fingers @ 3ft<br />20/2000 or count fingers @ 2 ft<br />20/4000 or count fingers @ 1 ft<br />20/7777 =CF<br />20/8888 =HM<br />20/9999 =LP<br />20/0000 =NLP'
            });
        });
    </script>
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder1" Runat="Server">
    <asp:HiddenField ID="HiddenFieldRecordIdentifier" runat="server"/>
        <!-- Corneal Edema pim -->
        <div class="pim">
            <div class="record-ident clearfix">
                <h3 class="record-first">RECORD IDENTIFIER: <asp:Literal runat="server" ID="LiteralRecordIdentifier"></asp:Literal></h3>
                <h3 class="record-second"><asp:Literal runat="server" ID="LiteralAbstractionNumber"></asp:Literal></h3>
            </div>
            <!-- History -->
            <table>
                <tr>
                    <th colspan="2" width="910px"><p>History</p></th>
                </tr>
                <!-- Question 1 -->
                <tr class="table_body">
                    <td width="70%"><strong>1. Date of birth</strong>
                    <asp:Label ID="LabelMonthOfBirth" runat="server" Visible="false"  ForeColor="Red" Text="<br />Please enter Month"></asp:Label>
                    <asp:Label ID="LabelYearOfBirth" runat="server" Visible="false"  ForeColor="Red"  Text="<br />Please enter Year"></asp:Label></td>
                    <td>
                        <asp:DropDownList ID="DropDownListMonthOfBirth" DataValueField="MonthID" DataTextField="MonthName" runat="server" onchange="DOBValidation();" />
                        <asp:DropDownList ID="DropDownListYearOfBirth" DataValueField="YearID" DataTextField="YearName" runat="server" onchange="DOBValidation();" />
                    </td>
                </tr>
                <!-- Question 1 -->
                <tr class="table_body_bg">
                    <td><strong>2. Date of surgery</strong>
                    <asp:Label ID="LabelMonthofSurgery" runat="server" Visible="false"  ForeColor="Red" Text="<br />Please enter Month"></asp:Label>
                    <asp:Label ID="LabelYearofSurgery" runat="server" Visible="false"  ForeColor="Red"  Text="<br />Please enter Year"></asp:Label></td>
                    <td>
                        <asp:DropDownList ID="DropDownListMonthofSurgery" DataValueField="MonthID" DataTextField="MonthName" runat="server" onchange="firstExamValidation();" />
                        <asp:DropDownList ID="DropDownListYearofSurgery" DataValueField="YearID" DataTextField="YearName" runat="server" onchange="firstExamValidation();" />
                    </td>
                </tr>
                <!-- Question 1 -->
                <tr class="table_body">
                    <td><strong>3. Date of the last visit between 6 and 12 months after surgery</strong>
                    <asp:Label ID="LabelMonthofExam" runat="server" Visible="false"  ForeColor="Red" Text="<br />Please enter Month"></asp:Label>
                    <asp:Label ID="LabelYearofExam" runat="server" Visible="false"  ForeColor="Red"  Text="<br />Please enter Year"></asp:Label></td>
                   
                    <td>
                        <asp:DropDownList ID="DropDownListMonthofExam" DataValueField="MonthID" DataTextField="MonthName" runat="server" onchange="followupValidation();" />
                        <asp:DropDownList ID="DropDownListYearofExam" DataValueField="YearID" DataTextField="YearName" runat="server" onchange="followupValidation();" />
                    </td>
                </tr>
                <!-- Question 1-1/2 -->
                <tr class="table_body_bg">
                    <td>
                        <strong>4. Gender</strong>
                        <asp:Label ID="LabelRBGenderTypeID" runat="server" Visible="false"  ForeColor="Red"  Text="<br />Please complete"></asp:Label>
                    </td>
                    <td>
                        <asp:RadioButtonList id="RadioButtonListRBGenderTypeID" CssClass="aspxList" runat="server">
	                        <asp:ListItem Value="42">&nbsp;Male</asp:ListItem>
	                        <asp:ListItem Value="43">&nbsp;Female</asp:ListItem>
                        </asp:RadioButtonList>
                    </td>
                </tr>
                <!-- Question 2 -->
                <tr class="table_body">
                    <td>
                        <strong>5. Were visual symptoms present?</strong>
                        <asp:Label ID="LabelRBVisualSymptoms" runat="server" Visible="false"  ForeColor="Red"  Text="<br />Please complete "></asp:Label>
                    </td>
                    <td>
                        <asp:RadioButtonList id="RadioButtonListRBVisualSymptoms" RepeatDirection="Vertical" CssClass="aspxList" runat="server">
	                        <asp:ListItem Value="1">&nbsp;Yes</asp:ListItem>
	                        <asp:ListItem Value="2">&nbsp;No</asp:ListItem>
                            <asp:ListItem Value="55">&nbsp;Unilateral</asp:ListItem>
                            <asp:ListItem Value="56">&nbsp;Bilateral</asp:ListItem>
	                        <asp:ListItem Value="3">&nbsp;Not Documented</asp:ListItem>
                        </asp:RadioButtonList>
                    </td>
                </tr>
                <!-- Question 3 -->
                <tr class="table_body_bg">
                    <td>
                        <strong>6. Was ocular discomfort present?</strong>
                        <asp:Label ID="LabelRBOcularDiscomfort" runat="server" Visible="false"  ForeColor="Red"  Text="<br />Please complete "></asp:Label></td>
                    <td>
                        <asp:RadioButtonList id="RadioButtonListRBOcularDiscomfort" RepeatDirection="Horizontal" CssClass="aspxList" runat="server">
                            <asp:ListItem Value="1">&nbsp;Yes</asp:ListItem>
                            <asp:ListItem Value="2">&nbsp;No</asp:ListItem>
                            <asp:ListItem Value="3">&nbsp;Not Documented</asp:ListItem>
                        </asp:RadioButtonList>
                    </td>
                </tr>
                <!-- Question 4 -->
                <tr class="table_body">
                    <td>
                        <strong>7. Did condition interfere with activities of daily living?</strong>
                        <asp:Label ID="LabelRBDailyLiving" runat="server" Visible="false"  ForeColor="Red"  Text="<br />Please enter "></asp:Label></td>
                    <td>
                        <asp:RadioButtonList id="RadioButtonListRBDailyLiving" RepeatDirection="Horizontal" CssClass="aspxList" runat="server">
                            <asp:ListItem Value="1">&nbsp;Yes</asp:ListItem>
                            <asp:ListItem Value="2">&nbsp;No</asp:ListItem>
                            <asp:ListItem Value="3">&nbsp;Not Documented</asp:ListItem>
                        </asp:RadioButtonList>
                    </td>
                </tr>
                <!-- Question 5 -->
                <tr class="table_body_bg">
                    <td>
                        <strong>8. Previous ocular surgery in the affected eye?</strong>
                        <asp:Label ID="LabelRBOcularSurgery" runat="server" Visible="false"  ForeColor="Red"  Text="<br />Please complete "></asp:Label>
                    </td>
                    <td>
                        <asp:RadioButtonList id="RadioButtonListRBOcularSurgery" RepeatDirection="Horizontal" CssClass="aspxList" runat="server">
                            <asp:ListItem Value="1">&nbsp;Yes</asp:ListItem>
                            <asp:ListItem Value="2">&nbsp;No</asp:ListItem>
                            <asp:ListItem Value="3">&nbsp;Not Documented</asp:ListItem>
                        </asp:RadioButtonList>
                    </td>
                </tr>
                <!-- Question 1 -->
                <tr class="table_body">
                    <td width="70%">
                        <strong>9. Is there a history of amantadine use?</strong>
                        <asp:Label ID="LabelRBAmantadine" runat="server" Visible="false"  ForeColor="Red"  Text="<br />Please complete "></asp:Label>
                    </td>
                    <td>
                        <asp:RadioButtonList id="RadioButtonListRBAmantadine" RepeatDirection="Horizontal" CssClass="aspxList" runat="server">
                            <asp:ListItem Value="1">&nbsp;Yes</asp:ListItem>
                            <asp:ListItem Value="2">&nbsp;No</asp:ListItem>
                            <asp:ListItem Value="3">&nbsp;Not Documented</asp:ListItem>
                        </asp:RadioButtonList>
                    </td>
                </tr>
                <!-- Question 2 -->
                <tr class="table_body_bg">
                    <td>
                        <strong>10. Did the patient have a family history of corneal dystrophy?</strong>
                        <asp:Label ID="LabelRBFamilyHistoryCornealDystrophy" runat="server" Visible="false"  ForeColor="Red"  Text="<br />Please complete"></asp:Label>
                    </td>
                    <td>
                        <asp:RadioButtonList id="RadioButtonListRBFamilyHistoryCornealDystrophy" RepeatDirection="Horizontal" CssClass="aspxList" runat="server">
                            <asp:ListItem Value="1">&nbsp;Yes</asp:ListItem>
                            <asp:ListItem Value="2">&nbsp;No</asp:ListItem>
                            <asp:ListItem Value="3">&nbsp;Not Documented</asp:ListItem>
                        </asp:RadioButtonList>
                    </td>
                </tr>
                <!-- Question 3 -->
                <tr class="table_body">
                    <td>
                        <strong>11. Does the patient have a family history of corneal transplantation?</strong>
                        <asp:Label ID="LabelRBFamilyHistoryCornealTransplantation" runat="server" Visible="false"  ForeColor="Red"  Text="<br />Please complete"></asp:Label>
                    </td>
                    <td>
                        <asp:RadioButtonList id="RadioButtonListRBFamilyHistoryCornealTransplantation" RepeatDirection="Horizontal" CssClass="aspxList" runat="server">
                            <asp:ListItem Value="1">&nbsp;Yes</asp:ListItem>
                            <asp:ListItem Value="2">&nbsp;No</asp:ListItem>
                            <asp:ListItem Value="3">&nbsp;Not Documented</asp:ListItem>
                        </asp:RadioButtonList>
                    </td>
                </tr>
            </table>
            <br />
            <!-- Preoperative Diagnosis/Assessment -->
            <table>
                <tr>
                    <th colspan="3" width="910px"><p>Preoperative Diagnosis/Assessment</p></th>
                </tr>
                <!-- Question 1 -->
                <tr class="table_body">
                    <td width="70%">
                        <strong>1. Affected Eye</strong>
                        <asp:Label ID="LabelRBAffectedEye" runat="server" Visible="false"  ForeColor="Red"  Text="<br />Please complete"></asp:Label>
                    </td>
                    <td>
                        <asp:RadioButtonList id="RadioButtonListRBAffectedEye" RepeatDirection="Horizontal" CssClass="aspxList" runat="server">
                            <asp:ListItem Value="49">&nbsp;Right</asp:ListItem>
                            <asp:ListItem Value="50">&nbsp;Left</asp:ListItem>
                        </asp:RadioButtonList>
                    </td>
                </tr>
                <!-- Question 2 -->
                <tr class="table_body_bg">
                    <td>
                        <strong>2. Spectacle or contract lens corrected visual acuity</strong>&nbsp;<a href="#"><img id="QP2" src="../../common/images/tip.gif" alt="Tip" /></a>
                        <asp:Label ID="LabelBCVADistance" runat="server" Visible="false"  ForeColor="Red"  Text="<br />Please complete"></asp:Label>
                    </td>
                    <td>
                        <label>20 / </label><asp:DropDownList ID="DropDownListBCVADistance" DataValueField="examValue" DataTextField="examLabel" runat="server" onchange="toggleQPr2CB();" />
                        <br /><asp:CheckBox runat="server" ID="CheckBoxRBBCVAdistanceNotAbletoPerform" text="&nbsp;Unable to Perform" class="check" onchange="toggleQPr2DD();" />
                    </td>
                </tr>
          
                <!-- Question 1 -->
                <tr class="table_body">
                    <td width="70%">
                        <strong>3. Co-morbidities</strong> (Please check all that apply)
                        <asp:Label ID="LabelCoMoribidity" runat="server" Visible="false"  ForeColor="Red"  Text="<br />Please complete"></asp:Label>
                    </td>
                    <td>
                        <asp:CheckBox runat="server" ID="CheckBoxCoMoribidityFilteringBleb" text="&nbsp;Filtering bleb" class="check"/>
                        <br /><asp:CheckBox runat="server" ID="CheckBoxCoMoribidityGlaucomaTS" text="&nbsp;Glaucoma tube shunt" class="check"/>
                        <br /><asp:CheckBox runat="server" ID="CheckBoxCoMoribidityDeepStromalVS" text="&nbsp;Deep stromal vascularization" class="check"/>
                        <br /><asp:DropDownList ID="DropDownListMoribidityNumberOfQuadrants" runat="server">
                            <asp:ListItem Value="0" Text="Choose"></asp:ListItem>
                            <asp:ListItem Value="1" Text="1"></asp:ListItem>
                            <asp:ListItem Value="2" Text="2"></asp:ListItem>
                            <asp:ListItem Value="3" Text="3"></asp:ListItem>
                            <asp:ListItem Value="4" Text="4"></asp:ListItem>
                             <asp:ListItem Value="176" Text="Not Documented"></asp:ListItem>
                        </asp:DropDownList><label>&nbsp;# of quadrants</label>
                        <br /><asp:CheckBox runat="server" ID="CheckBoxCoMoribidityAnteriorSynechiae" text="&nbsp;Anterior synechiae" class="check"/>
                        <br /><asp:CheckBox runat="server" ID="CheckBoxCoMoribidityVitreous" text="&nbsp;Vitreous in anterior chamber" class="check"/>
                        <br /><asp:CheckBox runat="server" ID="CheckBoxStormalScaring" text="&nbsp;Stromal scarring" class="check"/>
                    </td>
                </tr>
                <!-- Question 2 -->
                <tr class="table_body_bg">
                    <td>
                        <strong>4. Lens</strong>
                        <asp:Label ID="LabelLens" runat="server" Visible="false"  ForeColor="Red"  Text="<br />Please complete"></asp:Label>
                    </td>
                    <td>
                        <asp:CheckBox runat="server" ID="CheckBoxLensAphakic" text="&nbsp;Aphakic" class="check"/>
                        <br /><asp:CheckBox runat="server" ID="CheckBoxLensPhakic" text="&nbsp;Phakic" class="check"/>
                        <br /><asp:CheckBox runat="server" ID="CheckBoxLensPseudophakic" text="&nbsp;Pseudophakic" class="check" />
                        <span id="clickLensPseudophakictxt">
                        <br /><span style="padding-left:25px;"><asp:CheckBox runat="server" ID="CheckBoxLensACIOL" text="&nbsp;AC IOL" class="check"/></span>
                        <br /><span style="padding-left:25px;"><asp:CheckBox runat="server" ID="CheckBoxLensSuclus" text="&nbsp;Suclus PC IOL" class="check"/></span>
                        <br /><span style="padding-left:25px;"><asp:CheckBox runat="server" ID="CheckBoxLensIntracapsular" text="&nbsp;Intracapsular PC IOL" class="check"/></span>
                        <br /><span style="padding-left:25px;"><asp:CheckBox runat="server" ID="CheckBoxLensIris" text="&nbsp;Iris fixated IOL" class="check"/></span>
                        </span>
                    </td>
                </tr>
            </table>
            <br />
            <!-- Diagnostic Testing -->
            <table>
                <!-- Header - Diagnostic Testing -->
                <tr>
                    <th colspan="3" width="910px"><p>Diagnostic Testing</p></th>
                </tr>
                <!-- Question 1 -->
                <tr class="table_body">
                    <td width="70%"><strong>1. Intraocular pressure</strong>
                        <asp:Label ID="LabelRBIntraocularPressure" runat="server" Visible="false"  ForeColor="Red"  Text="<br />Please complete"></asp:Label>
                   </td>
                    <td>
                        <asp:RadioButtonList id="RadioButtonListRBIntraocularPressure" RepeatDirection="Vertical" CssClass="aspxList" runat="server">
	                        <asp:ListItem Value="89">&nbsp;&le;22 mmHg</asp:ListItem>
	                        <asp:ListItem Value="90">&nbsp;&gt;22 mmHg</asp:ListItem>
	                        <asp:ListItem Value="4">&nbsp;Not Performed</asp:ListItem>
                        </asp:RadioButtonList>
                    </td>
                </tr>
                <!-- Question 2 -->
                <tr class="table_body_bg">
                    <td>
                        <strong>2. Corneal Pachymetry</strong>
                        <asp:Label ID="LabelCornealPachymetry" runat="server" Visible="false"  ForeColor="Red"  Text="<br />Please complete"></asp:Label>
                    </td>
                    <td>
                        <asp:TextBox runat="server" ID="TextBoxCornealPachymetryValue" size="5" onkeyup="toggleQD2CB();" /><label>&nbsp;&mu;m</label>
                        <br /><asp:CheckBox runat="server" ID="CheckBoxCornealPachymetryNP" text="&nbsp;Not Performed" class="check" onclick="toggleQD2TB();" />
                    </td>
                </tr>
                <!-- Question 3 -->
                <tr class="table_body">
                    <td>
                        <strong>3. Corneal endothelial imaging</strong>
                        <asp:Label ID="LabelCornealEndoImaging" runat="server" Visible="false"  ForeColor="Red"  Text="<br />Please complete"></asp:Label>
                    </td>
                    <td>
                        <asp:RadioButton runat="server" ID="RadioButtonCornealEndoImagingYes" GroupName="CornealEndoImaging" text="&nbsp;Yes" />
                        <asp:RadioButton runat="server" ID="RadioButtonCornealEndoImagingNo" GroupName="CornealEndoImaging" text="&nbsp;No" />
                    </td>
                </tr>
                <!-- Question 4 -->
                <tr class="table_body_bg">
                    <td>
                        <strong>4. What was the cause of the corneal edema?</strong> (Please check all that apply)&nbsp;<a href="#"><img id="QD4a" src="../../common/images/tip.gif" alt="Tip" /></a>
                        <asp:Label ID="LabelCornealEdema" runat="server" Visible="false"  ForeColor="Red"  Text="<br />Please complete"></asp:Label>
                    </td>
                    <td>
                        <asp:CheckBox runat="server" ID="CheckBoxCornealEdemaFuchs" text="&nbsp;Fuchs endothelial dystrophy" class="check"/>
                        <br /><asp:CheckBox runat="server" ID="CheckBoxCornealEdemaGlaucoma" text="&nbsp;Glaucoma" class="check" />
                        <span id="Q4part1">
                            <br /><asp:CheckBox runat="server" ID="CheckBoxCornealEdemaAngleClosure" text="&nbsp;Angle closure" style="padding-left:25px;" class="check"/>
                            <br /><asp:CheckBox runat="server" ID="CheckBoxCornealEdemaCongenitalGlac" text="&nbsp;Congenital" style="padding-left:25px;" class="check"/>
                            <br /><asp:CheckBox runat="server" ID="CheckBoxCornealEdemaShunt" text="&nbsp;Post-glaucoma shunt valve" style="padding-left:25px;" class="check"/>
                            <br /><asp:CheckBox runat="server" ID="CheckBoxCornealEdemaTrab" text="&nbsp;Post-trabeculectomy" style="padding-left:25px;" class="check"/>
                            <br /><asp:CheckBox runat="server" ID="CheckBoxCornealEdemaOther" text="&nbsp;Other" style="padding-left:25px;" class="check"/>
                        </span>
                        <br /><asp:CheckBox runat="server" ID="CheckBoxCornealEdemaPostopEdema" text="&nbsp;Postoperative edema " class="check" />
                        <span id="Q4part2">
                            <br /><asp:CheckBox runat="server" ID="CheckBoxCornealEdemaRetainedLens" text="&nbsp;Retained nuclear lens fragments" style="padding-left:25px;" class="check"/>
                        </span>
                        <br /><asp:CheckBox runat="server" ID="CheckBoxCornealEdemaPosterior" text="&nbsp;Posterior polymorphous dystrophy" class="check"/>
                        <br /><asp:CheckBox runat="server" ID="CheckBoxCornealEdemaEndotheliitis" text="&nbsp;Endotheliitis" class="check"/>
                        <br /><asp:CheckBox runat="server" ID="CheckBoxCornealEdemaIridocorneal" text="&nbsp;Iridocorneal endothelial syndrome" class="check"/>
                        <br /><asp:CheckBox runat="server" ID="CheckBoxCornealEdemaUveitis" text="&nbsp;Uveitis" class="check"/>
                        <br /><asp:CheckBox runat="server" ID="CheckBoxCornealEdemaSilicone" text="&nbsp;Silicone oil keratopathy" class="check"/>
                        <br /><asp:CheckBox runat="server" ID="CheckBoxCornealEdemaTrauma" text="&nbsp;Trauma" class="check"/>
                        <br /><asp:CheckBox runat="server" ID="CheckBoxCornealEdemacongenital" text="&nbsp;Congenital causes" class="check" />
                        <span id="Q4part3">
                            <br /><asp:CheckBox runat="server" ID="CheckBoxCornealEdemaAnterior" text="&nbsp;Anterior segment dysgenesis" style="padding-left:25px;" class="check"/>
                            <br /><asp:CheckBox runat="server" ID="CheckBoxCornealEdemaForceps" text="&nbsp;Forceps injury at birth" style="padding-left:25px;" class="check"/>
                            <br /><asp:CheckBox runat="server" ID="CheckBoxCornealEdemaCongenitalEndo" text="&nbsp;Congenital endothelial dystrophy" style="padding-left:25px;" class="check"/>
                        </span>
                        <br /><asp:CheckBox runat="server" ID="CheckBoxCornealEdemaOther1" text="&nbsp;Other" class="check"/>
                    </td>
                </tr>
            </table>
            <br />
            <!-- Management -->
            <table>
                <!-- Header - Management -->
                <tr>
                    <th colspan="3" width="910px"><p>Management</p></th>
                </tr>
                <!-- Question 2 -->
                <tr class="table_body">
                    <td width="70%">
                        <strong>1. Surgical Therapy</strong>
                        <asp:Label ID="LabelSurgical" runat="server" Visible="false"  ForeColor="Red"  Text="<br />Please complete"></asp:Label>
                    </td>
                    <td>
                        <asp:CheckBox runat="server" ID="CheckBoxSurgicalDSEK" text="&nbsp;Descemet’s stripping endothelial keratoplasty (DSEK)" class="check"/>
                        <br /><asp:CheckBox runat="server" ID="CheckBoxSurgicalDMEK" text="&nbsp;Descemet’s membrane endothelial keratoplasty (DMEK)" class="check"/>
                        <br /><asp:CheckBox runat="server" ID="CheckBoxSurgicalPK" text="&nbsp;Penetrating keratoplasty" class="check"/>
                        <br /><asp:CheckBox runat="server" ID="CheckBoxSurgicalLAPK" text="&nbsp;Laser-assisted penetrating keratoplasty" class="check"/>
                    </td>
                </tr>
                <!-- Question 3 -->
                <tr class="table_body_bg">
                    <td>
                        <strong>2. If applicable, did the endothelial graft dislocate after surgery?</strong>
                        <asp:Label ID="LabelRBEndothelialGraft" runat="server" Visible="false"  ForeColor="Red"  Text="<br />Please complete"></asp:Label>
                    </td>
                    <td>
                        <asp:RadioButtonList id="RadioButtonListRBEndothelialGraft" RepeatDirection="Horizontal" CssClass="aspxList" runat="server">
	                        <asp:ListItem Value="1">&nbsp;Yes</asp:ListItem>
	                        <asp:ListItem Value="2">&nbsp;No</asp:ListItem>
	                        <asp:ListItem Value="48">&nbsp;Not Applicable</asp:ListItem>
                        </asp:RadioButtonList>
                    </td>
                </tr>
                <!-- Question 4 -->
                <tr class="table_body">
                    <td>
                        <strong>3. Did a suprachoroidal hemorrhage occur?</strong>
                        <asp:Label ID="LabelSuprachoroidalHem" runat="server" Visible="false"  ForeColor="Red"  Text="<br />Please complete"></asp:Label>
                    </td>
                    <td>
                        <asp:RadioButton runat="server" ID="RadioButtonSuprachoroidalHemYes" GroupName="SuprachoroidalHem" text="&nbsp;Yes" />
                        <asp:RadioButton runat="server" ID="RadioButtonSuprachoroidalHemNo" GroupName="SuprachoroidalHem" text="&nbsp;No" />
                    </td>
                </tr>
                <!-- Question 5 -->
                <tr class="table_body_bg">
                    <td>
                        <strong>4. Did postoperative endophthalmitis occur?</strong>
                        <asp:Label ID="LabelPostopEndo" runat="server" Visible="false"  ForeColor="Red"  Text="<br />Please complete  "></asp:Label>
                    </td>
                    <td>
                        <asp:RadioButton runat="server" ID="RadioButtonPostopEndoYes" GroupName="PostopEndo" text="&nbsp;Yes" />
                        <asp:RadioButton runat="server" ID="RadioButtonPostopEndoNo" GroupName="PostopEndo" text="&nbsp;No" />
                    </td>
                </tr>
                <!-- Question 6 -->
                <tr class="table_body">
                    <td>
                        <strong>5. Did a wound leak occur?</strong>
                        <asp:Label ID="LabelWoundLeak" runat="server" Visible="false"  ForeColor="Red"  Text="<br />Please complete"></asp:Label>
                    </td>
                    <td>
                        <asp:RadioButton runat="server" ID="RadioButtonWoundLeakYes" GroupName="WoundLeak" text="&nbsp;Yes" />
                        <asp:RadioButton runat="server" ID="RadioButtonWoundLeakNo" GroupName="WoundLeak" text="&nbsp;No" />
                    </td>
                </tr>
                <!-- Question 7 -->
                <tr class="table_body_bg">
                    <td>
                        <strong>6. Did a persistent epithelial defect develop?</strong>&nbsp;<a href="#"><img id="QM7" src="../../common/images/tip.gif" alt="Tip" /></a>
                        <asp:Label ID="LabelPersistentEpithelialDefect" runat="server" Visible="false"  ForeColor="Red"  Text="<br />Please complete"></asp:Label>
                    </td>
                    <td>
                        <asp:RadioButton runat="server" ID="RadioButtonPersistentEpithelialDefectYes" GroupName="PersistentEpithelialDefect" text="&nbsp;Yes" />
                        <asp:RadioButton runat="server" ID="RadioButtonPersistentEpithelialDefectNo" GroupName="PersistentEpithelialDefect" text="&nbsp;No" />
                    </td>
                </tr>
                <!-- Question 8 -->
                <tr class="table_body">
                    <td>
                        <strong>7. Did primary graft failure occur or was a repeat graft necessary within 6 months of surgery?</strong>
                        <asp:Label ID="LabelPrimaryGraftFailure" runat="server" Visible="false"  ForeColor="Red"  Text="<br />Please complete"></asp:Label>
                    </td>
                    <td>
                        <asp:RadioButton runat="server" ID="RadioButtonPrimaryGraftFailureYes" GroupName="PrimaryGraftFailure" text="&nbsp;Yes" />
                        <asp:RadioButton runat="server" ID="RadioButtonPrimaryGraftFailureNo" GroupName="PrimaryGraftFailure" text="&nbsp;No" />
                    </td>
                </tr>
                <!-- Question 9 -->
                <tr class="table_body_bg">
                    <td>
                        <strong>8. Was reoperation other than repeat graft necessary?</strong>
                        <asp:Label ID="LabelReoperation" runat="server" Visible="false"  ForeColor="Red"  Text="<br />Please complete"></asp:Label>
                    </td>
                    <td>
                        <asp:RadioButton runat="server" ID="RadioButtonReoperationYes" GroupName="Reoperation" text="&nbsp;Yes" />
                        <asp:RadioButton runat="server" ID="RadioButtonReoperationNo" GroupName="Reoperation" text="&nbsp;No" />
                    </td>
                </tr>
                <!-- Question 10 -->
                <tr class="table_body_bg">
                    <td id="QM8aTxt">
                        <strong>8a. If yes, what procedure was performed?</strong>
                        <asp:Label ID="LabelReoperation3" runat="server" Visible="false"  ForeColor="Red"  Text="<br />Please complete"></asp:Label>
                    </td>
                    <td>
                        <asp:TextBox runat="server" ID="TextBoxReoperationText" size="25" />
                    </td>
                </tr>
            </table>
            <br />
            <!-- Outcomes (6-12 Months Post-Op) -->
            <table>
                <tr>
                    <th colspan="3" width="910px"><p>Outcomes (6-12 Months Post-Op)</p></th>
                </tr>
                <td class="table_body" colspan="3">The following should be answered regarding the last visit between 6 and 12 months after surgery.</td>
                <!-- Question 1 -->
                <tr class="table_body">
                    <td width="70%">
                        <strong>1. Spectacle or contact lens corrected visual acuity</strong>&nbsp;<a href="#"><img id="QO1" src="../../common/images/tip.gif" alt="Tip" /></a>
                        <asp:Label ID="LabelOutcomeBCVA" runat="server" Visible="false"  ForeColor="Red"  Text="<br />Please complete"></asp:Label>
                    </td>
                    <td>
                        <label>20 / </label><asp:DropDownList ID="DropDownListOutcomeBCVA" DataValueField="examValue" DataTextField="examLabel" runat="server" />
                    </td>
                </tr>
                <!-- Question 3 -->
                <tr class="table_body_bg">
                    <td>
                        <strong>2. Did this patient have a comorbidity that affected final visual outcome?</strong>
                        <asp:Label ID="LabelComorbidity" runat="server" Visible="false"  ForeColor="Red"  Text="<br />Please complete"></asp:Label>
                    </td>
                    <td>
                        <asp:RadioButton runat="server" ID="RadioButtonComorbidityYes" GroupName="Comorbidity" text="&nbsp;Yes" />
                        <asp:RadioButton runat="server" ID="RadioButtonComorbidityNo" GroupName="Comorbidity" text="&nbsp;No" />
                    </td>
                </tr>
                <!-- Question 4 -->
                <tr class="table_body">
                    <td>
                        <strong>3. Relief of discomfort</strong>
                        <asp:Label ID="LabelRBReliefofDiscomfort" runat="server" Visible="false"  ForeColor="Red"  Text="<br />Please complete"></asp:Label>
                    </td>
                    <td>
                        <asp:RadioButtonList id="RadioButtonListRBReliefofDiscomfort" RepeatDirection="Vertical" CssClass="aspxList" runat="server">
	                        <asp:ListItem Value="1">&nbsp;Yes</asp:ListItem>
	                        <asp:ListItem Value="2">&nbsp;No</asp:ListItem>
	                        <asp:ListItem Value="48">&nbsp;Not Applicable</asp:ListItem>
                            <asp:ListItem Value="3">&nbsp;Not Documented</asp:ListItem>
                        </asp:RadioButtonList>
                    </td>
                </tr>
       
                <tr class="table_body_bg">
                    <td width="70%">
                        <strong>4. Is the corneal graft clear?</strong>
                        <asp:Label ID="LabelCornealGraftClear" runat="server" Visible="false"  ForeColor="Red"  Text="<br />Please complete"></asp:Label>
                    </td>
                    <td>
                        <asp:RadioButton runat="server" ID="RadioButtonCornealGraftClearYes" GroupName="CornealGraftClear" text="&nbsp;Yes" />
                        <asp:RadioButton runat="server" ID="RadioButtonCornealGraftClearNo" GroupName="CornealGraftClear" text="&nbsp;No" />
                    </td>
                </tr>
                <!-- Question 5 -->
                <tr class="table_body">
                    <td>
                        <strong>5. Pachymetry</strong>
                        <asp:Label ID="LabelPachymetry" runat="server" Visible="false"  ForeColor="Red"  Text="<br />Please complete"></asp:Label>
                    </td>
                    <td>
                        <asp:TextBox runat="server" ID="TextBoxPachymetry" size="5" onkeyup="QS2toggleRB();" /><label>&nbsp;&micro;m</label>
                        <br /><asp:CheckBox runat="server" ID="RadioButtonPachymetryNA" text="&nbsp;Not Performed" onclick="QS2toggleTB();" />
                    </td>
                </tr>
            </table>
            <br />
            <!-- Complications -->
            <table>
                <tr>
                    <th colspan="3" width="910px"><p>Complications</p></th>
                </tr>
                <tr>
                    <td class="table_body" colspan="3">If a repeat graft was necessary within 6 months, the following should be answered regarding the outcome 6-12 months after the repeat graft.</td>
                </tr>
                <!-- Question 1 -->
                <tr class="table_body">
                    <td colspan="2">
                        <strong>1. Endothelial rejection</strong>
                        <asp:Label ID="LabelEndoRejection" runat="server" Visible="false"  ForeColor="Red"  Text="<br />Please complete"></asp:Label>
                   </td>
                    <td>
                        <asp:RadioButton runat="server" ID="RadioButtonEndoRejectionYes" GroupName="EndoRejection" text="&nbsp;Yes" />
                        <asp:RadioButton runat="server" ID="RadioButtonEndoRejectionNo" GroupName="EndoRejection" text="&nbsp;No" />
                    </td>
                </tr>
                <!-- Question 2 -->
                <tr class="table_body_bg">
                    <td colspan="2" id="QC2txta">
                        <strong>2. If yes, did graft fail as a result?</strong>
                        <asp:Label ID="LabelRBEndoRejectionGraftFail" runat="server" Visible="false"  ForeColor="Red"  Text="<br />Please complete"></asp:Label>
                    </td>
                    <td id="QC2txtb">
                        <asp:RadioButtonList id="RadioButtonListRBEndoRejectionGraftFail" RepeatDirection="Horizontal" CssClass="aspxList" runat="server">
	                        <asp:ListItem Value="1">&nbsp;Yes</asp:ListItem>
	                        <asp:ListItem Value="2" style="margin-left:-4px;">&nbsp;No</asp:ListItem>
                        </asp:RadioButtonList>
                    </td>
                </tr>
                <!-- Question 3 -->
                <tr class="table_body">
                    <td colspan="2">
                        <strong>3. Late endothelial failure</strong>&nbsp;<a href="#"><img id="QC3" src="../../common/images/tip.gif" alt="Tip" /></a>
                        <asp:Label ID="LabelLateEndoFailure" runat="server" Visible="false"  ForeColor="Red"  Text="<br />Please complete"></asp:Label>
                    </td>
                    <td>
                        <asp:RadioButton runat="server" ID="RadioButtonLateEndoFailureYes" GroupName="LateEndoFailure" text="&nbsp;Yes" />
                        <asp:RadioButton runat="server" ID="RadioButtonLateEndoFailureNo" GroupName="LateEndoFailure" text="&nbsp;No" />
                    </td>
                </tr>
                <!-- Question 4 -->
                <tr class="table_body_bg">
                    <td colspan="2">
                        <strong>4. Other cause of graft failure</strong>
                        <asp:Label ID="LabelOtherFailure" runat="server" Visible="false"  ForeColor="Red"  Text="<br />Please complete"></asp:Label>
                    </td>
                    <td>
                        <asp:RadioButton runat="server" ID="RadioButtonOtherFailureYes" GroupName="OtherFailure" text="&nbsp;Yes" />
                        <asp:RadioButton runat="server" ID="RadioButtonOtherFailureNo" GroupName="OtherFailure" text="&nbsp;No" />
                    </td>
                </tr>
                <!-- Question 5 -->
                <tr class="table_body">
                    <td rowspan="4" width="55%"><strong>5. Other complications</strong>
                        <asp:Label ID="LabelOtherComplGlaucoma" runat="server" Visible="false"  ForeColor="Red"  Text="<br />Please complete Glaucoma"></asp:Label>
                        <asp:Label ID="LabelOtherComplRetinalDetach" runat="server" Visible="false"  ForeColor="Red"  Text="<br />Please complete Retinal Detachment"></asp:Label>
                        <asp:Label ID="LabelOtherCompIK" runat="server" Visible="false"  ForeColor="Red"  Text="<br />Please complete Infectious keratitis"></asp:Label>
                        <asp:Label ID="LabelOtherCompLossEye" runat="server" Visible="false"  ForeColor="Red"  Text="<br />Please complete Loss of Eye"></asp:Label>
                    </td>
                    <td width="15%"><span class="right">Glaucoma&nbsp;<a href="#"><img id="QC5" src="../../common/images/tip.gif" alt="Tip" /></a></span></td>
                    <td>
                        <asp:RadioButton runat="server" ID="RadioButtonOtherComplGlaucomaYes" GroupName="OtherComplGlaucoma" text="&nbsp;Yes" />
                        <asp:RadioButton runat="server" ID="RadioButtonOtherComplGlaucomaNo" GroupName="OtherComplGlaucoma" text="&nbsp;No" />
                    </td>
                </tr>
                <tr class="table_body">
                    <td><span class="right">Retinal detachment</span>
             
                    </td>
                    <td>
                        <asp:RadioButton runat="server" ID="RadioButtonOtherComplRetinalDetachYes" GroupName="OtherComplRetinalDetach" text="&nbsp;Yes" />
                        <asp:RadioButton runat="server" ID="RadioButtonOtherComplRetinalDetachNo" GroupName="OtherComplRetinalDetach" text="&nbsp;No" />
                    </td>
                </tr>
                <tr class="table_body">
                    <td><span class="right">Infectious keratitis</span>
                        </td>
                    <td>
                        <asp:RadioButton runat="server" ID="RadioButtonOtherCompIKYes" GroupName="OtherCompIK" text="&nbsp;Yes" />
                        <asp:RadioButton runat="server" ID="RadioButtonOtherCompIKNo" GroupName="OtherCompIK" text="&nbsp;No" />
                    </td>
                </tr>
                <tr class="table_body">
                    <td><span class="right">Loss of eye</span>
                        </td>
                    <td>
                        <asp:RadioButton runat="server" ID="RadioButtonOtherCompLossEyeYes" GroupName="OtherCompLossEye" text="&nbsp;Yes" />
                        <asp:RadioButton runat="server" ID="RadioButtonOtherCompLossEyeNo" GroupName="OtherCompLossEye" text="&nbsp;No" />
                    </td>
                </tr>
            </table>
            <br />
            <div class="button-box">
                <asp:LinkButton ID="LinkButtonBackToDashboard" runat="server" Text="Back To Chart Registration" PostBackUrl="../PatientChartRegistration.aspx?CycleNumber=1" Visible="false" CssClass="button" />
                <asp:LinkButton ID="ButtonSubmit"  OnClick="ButtonSubmit_Click" runat="server" Text="Submit Chart" CssClass="button" />
            </div>
        </div>
        <!-- CE pim ends -->
</asp:Content>

