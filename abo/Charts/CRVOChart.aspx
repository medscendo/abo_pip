﻿<%@ Page Title="" Language="C#" MasterPageFile="~/PIMMasterPage.master" AutoEventWireup="true" CodeFile="CRVOChart.aspx.cs" Inherits="abo_CRVOChart" %>

<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="Server">    
    <link type="text/css" href="../../common/css/atooltip.css" rel="stylesheet" media="screen" />
    
    <style type="text/css">
        .right {
            text-align: right;
        }
    </style>
    
</asp:Content>
<asp:content id="Content2" contentplaceholderid="ContentPlaceHolder1" runat="Server">
    <asp:HiddenField ID="HiddenFieldRecordIdentifier" runat="server"/>
        <!-- CRVO pim -->
        <div class="pim">
            <div class="record-ident clearfix">
                <h3 class="record-first">RECORD IDENTIFIER: <asp:Literal runat="server" ID="LiteralRecordIdentifier"></asp:Literal></h3>
                <h3 class="record-second"><asp:Literal runat="server" ID="LiteralAbstractionNumber"></asp:Literal></h3>
            </div>
            <!-- History -->
            <table>
                <tr>
                    <th colspan="3" width="910px"><p>History</p></th>
                </tr>
                <!-- Question 1 -->
                <tr class="table_body">
                    <td width="70%">
                        <strong>1. Date of birth</strong>
                        <asp:Label ID='LabelMonthOfBirth' runat="server" Visible="false"  ForeColor="Red" Text="<br />Please complete month" />
                        <asp:Label ID='LabelYearOfBirth' runat="server" Visible="false"  ForeColor="Red" Text="<br />Please complete year" />
                    </td>
                    <td>
                        <asp:DropDownList ID='DropDownListMonthOfBirth' DataValueField='MonthID' DataTextField='MonthName' runat='server' />
                        <asp:DropDownList ID='DropDownListYearOfBirth' DataValueField='YearID' DataTextField='YearName' runat='server' />
                    </td>
                </tr>
                <!-- Question 2 -->
                <tr class="table_body_bg">
                    <td>
                        <strong>2. When was the patient first diagnosed with CRVO?</strong>
                        <asp:Label ID='LabelMonthOfDiagnosis' runat="server" Visible="false"  ForeColor="Red" Text="<br />Please complete month" />
                        <asp:Label ID='LabelYearOfDiagnosis' runat="server" Visible="false"  ForeColor="Red" Text="<br />Please complete year" />
                    </td>
                    <td>
                        <asp:DropDownList ID='DropDownListMonthOfDiagnosis' DataValueField='MonthID' DataTextField='MonthName' runat='server' />
                        <asp:DropDownList ID='DropDownListYearOfDiagnosis' DataValueField='YearID' DataTextField='YearName' runat='server' />
                    </td>
                </tr>
                <!-- Question 3 -->
                <tr class="table_body">
                    <td>
                        <strong>3. Date of exam when you first diagnosed patient with CRVO?</strong>
                        <asp:Label ID='LabelMonthOfExam' runat="server" Visible="false"  ForeColor="Red" Text="<br />Please complete month" />
                        <asp:Label ID='LabelYearOfExam' runat="server" Visible="false"  ForeColor="Red" Text="<br />Please complete year" />
                        <asp:Label ID='Labelinitialvis' runat="server" Visible="false"  
                            ForeColor="Red" Text="<br />" />
                    </td>
                    <td>
                        <asp:DropDownList ID='DropDownListMonthOfExam' DataValueField='MonthID' DataTextField='MonthName' runat='server' />
                        <asp:DropDownList ID='DropDownListYearOfExam' DataValueField='YearID' DataTextField='YearName' runat='server' />
                    </td>
                </tr>
                <!-- Question 4 -->
                <tr class="table_body_bg">
                    <td>
                        <strong>4. Date of one year follow-up exam?</strong>
                        <br />(choose exam date that is closest to one year after the date you first diagnosed the patient with CRVO (date in #3), but not less than one year f/u.)
                        <asp:Label ID='LabelMonthOfFollowUp' runat="server" Visible="false"  ForeColor="Red" Text="<br />Please complete month" />
                        <asp:Label ID='LabelYearOfFollowUp' runat="server" Visible="false"  ForeColor="Red" Text="<br />Please complete year" />
                        <asp:Label ID='Labelinitialfol' runat="server" Visible="false" ForeColor="Red" Text="<br />" />
                    </td>
                    <td>
                        <asp:DropDownList ID='DropDownListMonthOfFollowUp' DataValueField='MonthID' DataTextField='MonthName' runat='server' />
                        <asp:DropDownList ID='DropDownListYearOfFollowUp' DataValueField='YearID' DataTextField='YearName' runat='server' />
                    </td>
                </tr>
                <!-- Question 5 -->
                <tr class="table_body">
                    <td>
                        <strong>5. Gender</strong>
                        <asp:Label ID='LabelRBGender' runat="server" Visible="false"  ForeColor="Red" Text="<br />Please complete" />
                    </td>
                    <td>
                        <asp:RadioButtonList id='RadioButtonListRBGender' RepeatDirection='Horizontal' CssClass='aspxList' runat='server'>
                            <asp:ListItem Value='42'>&nbsp;Male</asp:ListItem>
                            <asp:ListItem Value='43'>&nbsp;Female</asp:ListItem>
                        </asp:RadioButtonList>
                    </td>
                </tr>
                <!-- Question 6 -->
                <tr class="table_body_bg">
                    <td>
                        <strong>6. Are systemic medications recorded?</strong>
                        <asp:Label ID='LabelSystemicMedications' runat="server" Visible="false"  ForeColor="Red" Text="<br />Please complete" />
                    </td>
                    <td>
                        <asp:RadioButton runat='server' ID='RadioButtonSystemicMedicationsYes' GroupName='SystemicMedications' text='&nbsp;Yes' />
                        <asp:RadioButton runat='server' ID='RadioButtonSystemicMedicationsNo' GroupName='SystemicMedications' text='&nbsp;No' />
                    </td>
                </tr>
                <!-- Question 7 -->
                <tr class="table_body">
                    <td>
                        <strong>7. Does the patient have any drug reactions or allergies?</strong>
                        <asp:Label ID='LabelRBDrugReaction' runat="server" Visible="false"  ForeColor="Red" Text="<br />Please complete" />
                    </td>
                    <td>
                        <asp:RadioButtonList id='RadioButtonListRBDrugReaction' RepeatDirection='Horizontal' CssClass='aspxList' runat='server'>
                            <asp:ListItem Value="1">&nbsp;Yes</asp:ListItem>
	                        <asp:ListItem Value="2">&nbsp;No</asp:ListItem>
	                        <asp:ListItem Value="3">&nbsp;Not Recorded</asp:ListItem>
                        </asp:RadioButtonList>
                    </td>
                </tr>
            </table>
            <br />
            <!-- Examination and Diagnostic Procedures -->
            <table>
                <tr>
                    <th colspan="3" width="910px"><p>Examination and Diagnostic Procedures</p></th>
                </tr>
                <!-- Question 1 -->
                <tr class="table_body">
                    <td colspan="2">    
                        <strong>1. Which eye has the CRVO?</strong>
                        <br />(If both eyes have CRVO, designate one eye, preferably the most recently affected eye, as the involved eye and designate the other eye as the fellow eye.)
                        <asp:Label ID='LabelRBExamEyeID' runat="server" Visible="false"  ForeColor="Red" Text="<br />Please complete" />
                    </td>
                    <td>
                        <asp:RadioButtonList id='RadioButtonListRBExamEyeID' RepeatDirection='Horizontal' CssClass='aspxList' runat='server'>
                            <asp:ListItem Value='49'>&nbsp;OD</asp:ListItem>
                            <asp:ListItem Value='50'>&nbsp;OS</asp:ListItem>
                            <asp:ListItem Value='110'>&nbsp;Both</asp:ListItem>
                        </asp:RadioButtonList>
                    </td>
                </tr>
                <!-- Question 2 -->
                <tr class="table_body_bg">
                    <td width="50%" rowspan="2">
                        <strong>2. Visual acuity, best corrected with current script and pinhole</strong>&nbsp;<a href="#"><img id="QE2" src="../../common/images/tip.gif" alt="Tip" /></a>
                        <asp:Label ID='LabelInvolvedBCVA' runat="server" Visible="false"  ForeColor="Red" Text="<br />Please complete eye with CRVO" />
                        <asp:Label ID='LabelFellowBCVA' runat="server" Visible="false"  ForeColor="Red" Text="<br />Please complete fellow eye" />
                    </td>
                    <td width="20%"><span class="right">Eye with CRVO:</span></td>
                    <td>
                        20&nbsp;/&nbsp;<asp:DropDownList ID='DropDownListInvolvedBCVA' DataValueField='examValue' DataTextField='examLabel' runat='server' />
                    </td>
                </tr>
                <tr class="table_body_bg">
                    <td><span class="right">Fellow Eye:</span></td>
                    <td>
                        20&nbsp;/&nbsp;<asp:DropDownList ID='DropDownListFellowBCVA' DataValueField='examValue' DataTextField='examLabel' runat='server' />
                    </td>
                </tr>
                <!-- Question 3 -->
                <tr class="table_body">
                    <td colspan="2">
                        <strong>3. Was an examination for iris neovascularization performed and documented for the involved eye?</strong>
                        <asp:Label ID='LabelExamIrisNeovascularization' runat="server" Visible="false"  ForeColor="Red" Text="<br />Please complete" />
                    </td>
                    <td>
                        <asp:RadioButton runat='server' ID='RadioButtonExamIrisNeovascularizationYes' GroupName='ExamIrisNeovascularization' text='&nbsp;Yes' />
                        <asp:RadioButton runat='server' ID='RadioButtonExamIrisNeovascularizationNo' GroupName='ExamIrisNeovascularization' text='&nbsp;No' />
                    </td>
                </tr>
                <!-- Question 4 -->
                <tr class="table_body_bg">
                    <td colspan="2">
                        <strong>4. Was fluorescein angiography performed?</strong>
                        <asp:Label ID='LabelExamFluorescein' runat="server" Visible="false"  ForeColor="Red" Text="<br />Please complete" />
                    </td>
                    <td>
                        <asp:RadioButton runat='server' ID='RadioButtonExamFluoresceinYes' GroupName='ExamFluorescein ' text='&nbsp;Yes' />
                        <asp:RadioButton runat='server' ID='RadioButtonExamFluoresceinNo' GroupName='ExamFluorescein ' text='&nbsp;No' />
                    </td>
                </tr>
                <!-- Question 5 -->
                <tr class="table_body">
                    <td colspan="2">
                        <strong>5. What was the central macular thickness on optical coherence tomography?</strong>
                        <asp:Label ID='LabelExamCMT' runat="server" Visible="false"  ForeColor="Red" Text="<br />Please complete " />
                        <asp:Label ID='LabelExamCMTNA' runat="server" Visible="false"  ForeColor="Red" Text="<br />Please complete not performed" />
                    </td>
                    <td>
                        <asp:TextBox runat='server' ID='TextBoxExamCMT' size='5' onkeyup="toggleQE5CB();" /> microns
                        <br /><asp:CheckBox runat='server' ID='CheckBoxExamCMTNA' text='&nbsp;Not Performed' onclick="toggleQE5TB();" />
                    </td>
                </tr>
                <!-- Question 6 -->
                <tr class="table_body_bg">
                    <td colspan="2">
                        <strong>6. Did the patient have an RAPD in the affected eye?</strong>
                        <asp:Label ID='LabelRBRAPDMeasured' runat="server" Visible="false"  ForeColor="Red" Text="<br />Please complete" />    
                    </td>
                    <td>
                        <asp:RadioButtonList id='RadioButtonListRBRAPDMeasured' RepeatDirection='Vertical' CssClass='aspxList' runat='server'>
                            <asp:ListItem Value="1">&nbsp;Yes</asp:ListItem>
	                        <asp:ListItem Value="2">&nbsp;No</asp:ListItem>
	                        <asp:ListItem Value="3">&nbsp;Not Recorded</asp:ListItem>
	                        <asp:ListItem Value="338">&nbsp;Unable: Eyes dilated</asp:ListItem>
                        </asp:RadioButtonList>
                    </td>
                </tr>
            </table>
            <br />
            <!-- Assessment and Management -->
            <table>
                <tr>
                    <th colspan="3" width="910px"><p>Assessment and Management</p></th>
                </tr>
                <!-- Question 1 -->
                <tr class="table_body">
                    <td width="70%">
                        <strong>1. Were intravitreal pharmacotherapeutics administered for CME or neovascularization (at this or the following visit)?</strong>
                        <asp:Label ID='LabelRBIntravitrealPharma' runat="server" Visible="false"  ForeColor="Red" Text="<br />Please complete" />
                    </td>
                    <td>
                        <asp:RadioButtonList id='RadioButtonListRBIntravitrealPharma' RepeatDirection='Vertical' CssClass='aspxList' runat='server'>
                            <asp:ListItem Value="1">&nbsp;Yes</asp:ListItem>
	                        <asp:ListItem Value="2">&nbsp;No</asp:ListItem>
	                        <asp:ListItem Value="48">&nbsp;Not applicable (or therapy was given)</asp:ListItem>
                        </asp:RadioButtonList>
                    </td>
                </tr>
                <!-- Question 1a -->
                <tr class="table_body">
                    <td id="QAM1Aa">
                        <strong>1a. If "yes," what treatment was administered?</strong>
                    <asp:Label ID='LabelCheckBoxTherapyBevacizumab' runat="server" Visible="false"  ForeColor="Red" Text="<br />Please complete" />
                    </td>
                    <td id="QAM1Ab">
                        <asp:CheckBox runat='server' ID='CheckBoxTherapyBevacizumab' text='&nbsp;Bevacizumab' />
                        <br /><asp:CheckBox runat='server' ID='CheckBoxTherapyRanibizumab' text='&nbsp;Ranibizumab' />
                        <br /><asp:CheckBox runat='server' ID='CheckBoxTherapyIntravitrealSteroids' text='&nbsp;Intravitreal Steroids' />
                        <br /><asp:CheckBox runat='server' ID='CheckBoxTherapyOther' text='&nbsp;Other&nbsp;' /><asp:TextBox runat='server' ID='TextBoxTherapyOtherText' size='15' />
                        <br /><asp:CheckBox runat='server' ID='CheckBoxTherapyNone' text='&nbsp;Not Recorded' />
                    </td>
                </tr>
                <!-- Question 1b -->
                <tr class="table_body">
                    <td id="QAM2a">
                        <strong>1b. If no therapy for CME was performed at the time of the initial exam (or the following exam), what was the reason?</strong>
                        <asp:Label ID='LabelRBNoTherapyReason' runat="server" Visible="false"  ForeColor="Red" Text="<br />Please complete" />
                    </td>
                    <td id="QAM2b">
                        <asp:RadioButtonList id='RadioButtonListRBNoTherapyReason' RepeatDirection='Vertical' CssClass='aspxList' runat='server'>
                            <asp:ListItem Value='111'>&nbsp;Treatment of CME deemed unlikely to be improve vision or quality of life</asp:ListItem>
                            <asp:ListItem Value='112'>&nbsp;Patient refused treatment</asp:ListItem>
                            <asp:ListItem Value='113'>&nbsp;Risks of therapy outweighed benefits</asp:ListItem>
                        </asp:RadioButtonList>
                    </td>
                </tr>
            </table>
            <br />
            <!-- Follow-Up Intervals -->
            <table>
                <tr>
                    <th colspan="3" width="910px"><p>Follow-Up Intervals</p></th>
                </tr>
                <!-- Question 1 -->
                <tr class="table_body">
                    <td width="70%">
                        <strong>1. When was the patient scheduled for ocular follow-up?</strong>
                        <asp:Label ID='LabelRBPatientFollowupSchedule' runat="server" Visible="false"  ForeColor="Red" Text="<br />Please complete" />
                    </td>
                    <td>
                        <asp:RadioButtonList id='RadioButtonListRBPatientFollowupSchedule' RepeatDirection='Vertical' CssClass='aspxList' runat='server'>
                            <asp:ListItem Value='114'>&nbsp;&#8804;4 weeks</asp:ListItem>
                            <asp:ListItem Value='115'>&nbsp;&gt;4 - 8 weeks</asp:ListItem>
                            <asp:ListItem Value='116'>&nbsp;&gt;8 - 16 weeks</asp:ListItem>
                            <asp:ListItem Value='117'>&nbsp;&gt;16 weeks</asp:ListItem>
                            <asp:ListItem Value='3'>&nbsp;Not recorded</asp:ListItem>
                        </asp:RadioButtonList>
                    </td>
                </tr>
                <!-- Question 2 -->
                <tr class="table_body_bg">
                    <td>
                        <span id="QF2A">
                        <strong>2. If intravitreal steroids were administered, when was the patient scheduled to return for IOP measurement (to you or another eye care provider)?</strong>&nbsp;<a href="#"><img id="QF2" src="../../common/images/tip.gif" alt="Tip" /></a>
                        </span>
                        <asp:Label ID='LabelRBPatientFollowupIOP' runat="server" Visible="false"  ForeColor="Red" Text="<br />Please complete" />
                    </td>
                    <td>
                        <span id="QF2B">
                        <asp:RadioButtonList id='RadioButtonListRBPatientFollowupIOP' RepeatDirection='Vertical' CssClass='aspxList' runat='server'>
                            <asp:ListItem Value='118'>&nbsp;1 to 3 weeks</asp:ListItem>
                            <asp:ListItem Value='119'>&nbsp;&gt;3 to 6 weeks</asp:ListItem>
                            <asp:ListItem Value='120'>&nbsp;&gt;6 to 12 weeks</asp:ListItem>
                            <asp:ListItem Value='121'>&nbsp;Not scheduled for f/u: reason not documented</asp:ListItem>
                            <asp:ListItem Value='122'>&nbsp;Not scheduled for f/u: reason documented (such as pt has functioning shunt)</asp:ListItem>
                        </asp:RadioButtonList>
                        </span>
                    </td>
                </tr>
            </table>
            <br />
            <!-- Outcomes -->
            <table>
                <tr>
                    <th colspan="3" width="910px"><p>Outcomes</p></th>
                </tr>
                <!-- Question 1 -->
                <tr class="table_body">
                    <td width="50%" rowspan="2">
                        <strong>1. Visual acuity, best corrected with current script and pinhole at the one year follow up visit</strong>&nbsp;<a href="#"><img id="QO1" src="../../common/images/tip.gif" alt="Tip" /></a>
                        <asp:Label ID='LabelOutcomeTreatmentEyeBCVA' runat="server" Visible="false"  ForeColor="Red" Text="<br />Please complete eye with CRVO" />
                        <asp:Label ID='LabelOutcomeTreatmentFellowBCVA' runat="server" Visible="false"  ForeColor="Red" Text="<br />Please complete fellow eye" />
                    </td>
                    <td width="20%"><span class="right">Eye with CRVO:</span></td>
                    <td>
                        20&nbsp;/&nbsp;<asp:DropDownList ID='DropDownListOutcomeTreatmentEyeBCVA' DataValueField='examValue' DataTextField='examLabel' runat='server' />
                    </td>
                </tr>
                <tr class="table_body">
                    <td><span class="right">Fellow Eye:</span></td>
                    <td>
                        20&nbsp;/&nbsp;<asp:DropDownList ID='DropDownListOutcomeTreatmentFellowBCVA' DataValueField='examValue' DataTextField='examLabel' runat='server' />
                    </td>
                </tr>
                <!-- Question 2 -->
                <tr class="table_body_bg">
                    <td colspan="2">
                        <strong>2. Was macular edema present on clinical exam or OCT?</strong>
                        <asp:Label ID='LabelRBOutcomeMacularEdema' runat="server" Visible="false"  ForeColor="Red" Text="<br />Please complete" />
                    </td>
                    <td>
                        <asp:RadioButtonList id='RadioButtonListRBOutcomeMacularEdema' RepeatDirection='Vertical' CssClass='aspxList' runat='server'>
                            <asp:ListItem Value='1'>&nbsp;Yes</asp:ListItem>
                            <asp:ListItem Value='2'>&nbsp;No</asp:ListItem>
                            <asp:ListItem Value='3'>&nbsp;Not recorded</asp:ListItem>
                            <asp:ListItem Value='48'>&nbsp;Not applicable</asp:ListItem>
                        </asp:RadioButtonList>
                    </td>
                </tr>
                <!-- Question 3 -->
                <tr class="table_body">
                    <td colspan="2">
                        <strong>3. What was the central macular thickness on the OCT from this date</strong>
                        <br />(or from the date of the most recent OCT)
                        <asp:Label ID='LabelOutcomeCST' runat="server" Visible="false"  ForeColor="Red" Text="<br />Please complete " />
                        <asp:Label ID='LabelOutcomeCSTNA' runat="server" Visible="false"  ForeColor="Red" Text="<br />Please complete " />
                    </td>
                    <td>
                        <asp:TextBox runat='server' ID='TextBoxOutcomeCST' size='5' onkeyup="toggleQO3CB();" /> microns
                        <br /><asp:CheckBox runat='server' ID='CheckBoxOutcomeCSTNA' text='&nbsp;' onclick="toggleQO3TB();" /> No OCT since initial exam
                    </td>
                </tr>
                <!-- Question 4 -->
                <tr class="table_body_bg">
                    <td colspan="2">
                        <strong>4. Were any therapies required over the past year, related to the CRVO or from side effects/complications from treating the CRVO?</strong>
                        <br />(Please check all that are appropriate)
                    </td>
                    <td>
                        <asp:CheckBox runat='server' ID='CheckBoxOutcomeTherapyBevacizumab' text='&nbsp;Bevacizumab' />
                        <br /><asp:CheckBox runat='server' ID='CheckBoxOutcomeTherapyRanibizumab' text='&nbsp;Ranibizumab' />
                        <br /><asp:CheckBox runat='server' ID='CheckBoxOutcomeTherapyPRP' text='&nbsp;PRP' />
                        <br /><asp:CheckBox runat='server' ID='CheckBoxOutcomeTherapyPPVx' text='&nbsp;PPVx' />
                        <br /><asp:CheckBox runat='server' ID='CheckBoxOutcomeTherapyIntravit' text='&nbsp;Intravit. Steroids' />
                        <br /><asp:CheckBox runat='server' ID='CheckBoxOutcomeTherapyCataractSurgery' text='&nbsp;Cataract Surgery ' />
                        <br /><asp:CheckBox runat='server' ID='CheckBoxOutcomeTherapyLaser' text='&nbsp;Laser/surgery to control IOP' />
                        <br /><asp:CheckBox runat='server' ID='CheckBoxOutcomeTherapyOther' text='&nbsp;Other&nbsp;' /><asp:TextBox runat='server' ID='TextBoxOutcomeTherapyOtherText' size='15' />
                    </td>
                </tr>
                <!-- Question 5 -->
                <tr class="table_body">
                    <td colspan="2">
                        <strong>5. If no therapies for CME were performed at the one year follow up visit, what was the reason?</strong>
                        <asp:Label ID='LabelRBNoCMETherapyReason' runat="server" Visible="false"  ForeColor="Red" Text="<br />Please complete" />
                    </td>
                    <td>
                        <asp:RadioButtonList id='RadioButtonListRBNoCMETherapyReason' RepeatDirection='Vertical' CssClass='aspxList' runat='server'>
                            <asp:ListItem Value='123'>&nbsp;CME resolved following initial exam</asp:ListItem>
                            <asp:ListItem Value='124'>&nbsp;Treatment of CME deemed unlikely to be improve vision or quality of life</asp:ListItem>
                            <asp:ListItem Value='112'>&nbsp;Patient refused treatment</asp:ListItem>
                            <asp:ListItem Value='113'>&nbsp;Risks of therapy outweighed benefits</asp:ListItem>
                            <asp:ListItem Value='48'>&nbsp;Not applicable</asp:ListItem>
                        </asp:RadioButtonList>
                    </td>
                </tr>
                <!-- Question 6 -->
                <tr class="table_body_bg">
                    <td rowspan="2">
                        <span id="QO6A">
                        <strong>6. If intravitreal steroids were administered, did the patient develop IOP elevation or cataract during the one year follow-up:</strong>&nbsp;<a href="#"><img id="QO6" src="../../common/images/tip.gif" alt="Tip" /></a>
                        </span>
                        <asp:Label ID='LabelRBIOPElevationDeveloped' runat="server" Visible="false"  ForeColor="Red" Text="<br />Please complete IOP elevation" />
                        <asp:Label ID='LabelRBCataractDeveloped' runat="server" Visible="false"  ForeColor="Red" Text="<br />Please complete cataract" />
                    </td>
                    <td>
                        <span id="QO6B">
                        IOP Elevation
                        <br />(enough to warrant initiation or change in glaucoma mgmt, and felt to be due to steroids rather than other disease, such as NVG)
                        </span>
                    </td>
                    <td>
                        <span id="QO6C">
                        <asp:RadioButtonList id='RadioButtonListRBIOPElevationDeveloped' RepeatDirection='Vertical' CssClass='aspxList' runat='server'>
                            <asp:ListItem Value='1'>&nbsp;Yes</asp:ListItem>
                            <asp:ListItem Value='2'>&nbsp;No</asp:ListItem>
                            <asp:ListItem Value='3'>&nbsp;Not recorded</asp:ListItem>
                        </asp:RadioButtonList>
                        </span> 
                    </td>
                </tr>
                <!-- Question 6b -->
                <tr class="table_body_bg">
                    <td>
                        <span id="QO6D">
                        Cataract
                        <br />(new onset of lens opacity or progression of pre-existing lens opacity)
                        </span>
                    </td>
                    <td>
                        <span id="QO6E">
                        <asp:RadioButtonList id='RadioButtonListRBCataractDeveloped' RepeatDirection='Vertical' CssClass='aspxList' runat='server'>
                            <asp:ListItem Value='1'>&nbsp;Yes</asp:ListItem>
                            <asp:ListItem Value='2'>&nbsp;No</asp:ListItem>
                            <asp:ListItem Value='3'>&nbsp;Not recorded</asp:ListItem>
                            <asp:ListItem Value='48'>&nbsp;Not applicable</asp:ListItem>
                        </asp:RadioButtonList>
                        </span>
                    </td>
                </tr>
            </table>
            <div class="button-box">
                <asp:LinkButton ID="LinkButtonBackToDashboard" runat="server" Text="Back To Chart Registration" PostBackUrl="../PatientChartRegistration.aspx?CycleNumber=1" Visible="false" CssClass="button" />
                <asp:LinkButton ID="ButtonSubmit" OnClick="ButtonSubmit_Click" runat="server" Text="Submit Chart" CssClass="button" />
            </div>
        </div>
        <!-- ION pim ends -->
</asp:content>
<asp:Content runat="server" ID="Content4" ContentPlaceHolderID="javascript">
    <script type="text/javascript" src="../../common/js/jquery.atooltip.js"></script>
    <script type="text/javascript" language="javascript">
        //      Either enables or disables input boxes
        function enabledControl(el, disabled, checked) {
            //alert("Hello world from enabled control");
            try {
                if (disabled) {
                    el.disabled = disabled;
                    el.setAttribute("disabled", true);
                }
                else {
                    el.disabled = disabled;
                    el.removeAttribute("disabled");
                }

                if (checked == true)
                    el.checked = false;
            }
            catch (E) {
            }
            if (el.childNodes && el.childNodes.length > 0) {
                for (var x = 0; x < el.childNodes.length; x++) {
                    enabledControl(el.childNodes[x], disabled, checked);
                }
            }
        }
        //      Either enables or disables dropdown boxes
        function enabledControlDropDown(el, disabled, checked) {
            //alert("Hello world from enabled control");
            try {
                if (disabled) {
                    el.disabled = disabled;
                    el.setAttribute("disabled", true);
                }
                else {
                    el.disabled = disabled;
                    el.removeAttribute("disabled");
                }

                if (checked == true)
                    el.options[0].selected = true;
            }
            catch (E) {
            }
            if (el.childNodes && el.childNodes.length > 0) {
                for (var x = 0; x < el.childNodes.length; x++) {
                    enabledControl(el.childNodes[x], disabled, checked);
                }
            }
        }
        //      Either enables or disables text boxes
        function enabledControlTextField(el, disabled, checked) {
            //alert("Hello world from enabled control");
            try {
                if (disabled) {
                    el.disabled = disabled;
                    el.setAttribute("disabled", true);
                }
                else {
                    el.disabled = disabled;
                    el.removeAttribute("disabled");
                }

                if (checked == true) {
                    el.value = '';
                }
            }
            catch (E) {
            }
            if (el.childNodes && el.childNodes.length > 0) {
                for (var x = 0; x < el.childNodes.length; x++) {
                    enabledControl(el.childNodes[x], disabled, checked);
                }
            }
        }

        function generate(arr1, arr2, istrue) {
            if (istrue == true) {
                for (var i = 0; i < arr1.length; i++) {
                    document.getElementById(arr1[i]).style.color = "gray";
                }
                for (var i = 0; i < arr2.length; i++) {
                    $(arr2[i] + ' :input').attr('disabled', true);
                    $(arr2[i] + ' :input').removeAttr("checked");
                }
            }
            if (istrue == false) {
                for (var i = 0; i < arr1.length; i++) {
                    document.getElementById(arr1[i]).style.color = "#333";
                }
                for (var i = 0; i < arr2.length; i++) {
                    $(arr2[i] + ' :input').removeAttr('disabled');
                }
            }
        }

        function toggleQE5TB() {
            document.getElementById("<%= TextBoxExamCMT.ClientID %>").value = '';
        }
        function toggleQE5CB() {
            document.getElementById("<%= CheckBoxExamCMTNA.ClientID %>").checked = false;
        }

        function toggleQO3TB() {
            document.getElementById("<%= TextBoxOutcomeCST.ClientID %>").value = '';
        }
        function toggleQO3CB() {
            document.getElementById("<%= CheckBoxOutcomeCSTNA.ClientID %>").checked = false;
        }
    </script>
    <script>
        $(document).ready(function () {
            // Question 1a Assessment **************
            $("#<%= CheckBoxTherapyIntravitrealSteroids.ClientID %>").click(function () {
                if ($('#<%= CheckBoxTherapyIntravitrealSteroids.ClientID %>').prop("checked") == true) {
                    var myaray = new Array("QF2A", "QF2B", "QO6A", "QO6B", "QO6C", "QO6D", "QO6E");
                    var myarray2 = new Array('#QF2B', "#QO6C", "#QO6E");
                    generate(myaray, myarray2, false);
                }
                else {
                    var myaray = new Array("QF2A", "QF2B", "QO6A", "QO6B", "QO6C", "QO6D", "QO6E");
                    var myarray2 = new Array('#QF2B', "#QO6C", "#QO6E");
                    generate(myaray, myarray2, true);
                }
            });
            if ($('#<%= CheckBoxTherapyIntravitrealSteroids.ClientID %>').prop("checked") == true) {
                var myaray = new Array("QF2A", "QF2B", "QO6A", "QO6B", "QO6C", "QO6D", "QO6E");
                var myarray2 = new Array('#QF2B', "#QO6C", "#QO6E");
                generate(myaray, myarray2, false);
            }
            else {
                var myaray = new Array("QF2A", "QF2B", "QO6A", "QO6B", "QO6C", "QO6D", "QO6E");
                var myarray2 = new Array('#QF2B', "#QO6C", "#QO6E");
                generate(myaray, myarray2, true);
            }

            // Question 1a Assessment - Other **************
            $("#<%= CheckBoxTherapyOther.ClientID %>").click(function () {
                if ($('#<%= CheckBoxTherapyOther.ClientID %>').prop("checked") == true) {
                    enabledControlTextField(document.getElementById("<%= TextBoxTherapyOtherText.ClientID %>"), false, false);
                }
                else {
                    enabledControlTextField(document.getElementById("<%= TextBoxTherapyOtherText.ClientID %>"), true, true);
                }
            });
            if ($('#<%= CheckBoxTherapyOther.ClientID %>').prop("checked") == true) {
                enabledControlTextField(document.getElementById("<%= TextBoxTherapyOtherText.ClientID %>"), false, false);
            }
            else {
                enabledControlTextField(document.getElementById("<%= TextBoxTherapyOtherText.ClientID %>"), true, true);
            }

            // Question 4 Outcomes - Other **************
            $("#<%= CheckBoxOutcomeTherapyOther.ClientID %>").click(function () {
                if ($('#<%= CheckBoxOutcomeTherapyOther.ClientID %>').prop("checked") == true) {
                    enabledControlTextField(document.getElementById("<%= TextBoxOutcomeTherapyOtherText.ClientID %>"), false, false);
                }
                else {
                    enabledControlTextField(document.getElementById("<%= TextBoxOutcomeTherapyOtherText.ClientID %>"), true, true);
                }
            });
            if ($('#<%= CheckBoxOutcomeTherapyOther.ClientID %>').prop("checked") == true) {
                enabledControlTextField(document.getElementById("<%= TextBoxOutcomeTherapyOtherText.ClientID %>"), false, false);
            }
            else {
                enabledControlTextField(document.getElementById("<%= TextBoxOutcomeTherapyOtherText.ClientID %>"), true, true);
            }

            $("#<%= RadioButtonListRBIntravitrealPharma.ClientID %>").click(function () {
                if ($('#<%= RadioButtonListRBIntravitrealPharma.ClientID %>').find('input:checked').val() == '1') {
                    var myarray = new Array("QAM1Aa", "QAM1Ab", "QAM2a", "QAM2b");
                    var myarray2 = new Array("QAM1Aa", "#QAM1Ab", "QAM2a", "#QAM2b");
                    generate(myarray, myarray2, false);
                }
                else {
                    var myarray = new Array("QAM1Aa", "QAM1Ab", "QAM2a", "QAM2b");
                    var myarray2 = new Array("QAM1Aa", "#QAM1Ab", "QAM2a", "#QAM2b");
                    generate(myarray, myarray2, true);
                    enabledControlTextField(document.getElementById("<%= TextBoxTherapyOtherText.ClientID %>"), true, true);
                }
            });
            if ($('#<%= RadioButtonListRBIntravitrealPharma.ClientID %>').find('input:checked').val() == '1') {
                var myarray = new Array("QAM1Aa", "QAM1Ab", "QAM2a", "QAM2b");
                var myarray2 = new Array("QAM1Aa", "#QAM1Ab", "QAM2a", "#QAM2b");
                generate(myarray, myarray2, false);
            }
            else {
                var myarray = new Array("QAM1Aa", "QAM1Ab", "QAM2a", "QAM2b");
                var myarray2 = new Array("QAM1Aa", "#QAM1Ab", "QAM2a", "#QAM2b");
                generate(myarray, myarray2, true);
                enabledControlTextField(document.getElementById("<%= TextBoxTherapyOtherText.ClientID %>"), true, true);
            }

            $("#<%= RadioButtonListRBIntravitrealPharma.ClientID %>").click(function () {
                if ($('#<%= RadioButtonListRBIntravitrealPharma.ClientID %>').find('input:checked').val() == '2') {
                    var myarray = new Array("QAM2a", "QAM2b");
                    var myarray2 = new Array("QAM2a", "#QAM2b");
                    generate(myarray, myarray2, false);
                }
                else {
                    var myarray = new Array("QAM2a", "QAM2b");
                    var myarray2 = new Array("QAM2a", "#QAM2b");
                    generate(myarray, myarray2, true);
                }
            });
            if ($('#<%= RadioButtonListRBIntravitrealPharma.ClientID %>').find('input:checked').val() == '2') {
                var myarray = new Array("QAM2a", "QAM2b");
                var myarray2 = new Array("QAM2a", "#QAM2b");
                generate(myarray, myarray2, false);
            }
            else {
                var myarray = new Array("QAM2a", "QAM2b");
                var myarray2 = new Array("QAM2a", "#QAM2b");
                generate(myarray, myarray2, true);
            }
        });
    </script>
    <script type="text/javascript">
        $(function () {
            $('#Q').aToolTip({
                clickIt: true,
                tipContent: ''
            });
        });
        $(function () {
            $('#QE2').aToolTip({
                clickIt: true,
                tipContent: '20/800 or count fingers @ 5 ft<br />20/1000 or count fingers @ 4 ft<br />20/1600 or count fingers @ 3ft<br />20/2000 or count fingers @ 2 ft<br />20/4000 or count fingers @ 1 ft<br />20/7777 =CF<br />20/8888 =HM<br />20/9999 =LP<br />20/0000 =NLP'
            });
        });
        $(function () {
            $('#QF2').aToolTip({
                clickIt: true,
                tipContent: 'This question is only available if you clicked the &quot;Intravitreal Steroids&quot; option on to Question #1a under &quot;Assessment and Management&quot;'
            });
        });
        $(function () {
            $('#QO1').aToolTip({
                clickIt: true,
                tipContent: '20/800 or count fingers @ 5 ft<br />20/1000 or count fingers @ 4 ft<br />20/1600 or count fingers @ 3ft<br />20/2000 or count fingers @ 2 ft<br />20/4000 or count fingers @ 1 ft<br />20/7777 =CF<br />20/8888 =HM<br />20/9999 =LP<br />20/0000 =NLP'
            });
        });
        $(function () {
            $('#QO6').aToolTip({
                clickIt: true,
                tipContent: 'This question is only available if you clicked the &quot;Intravitreal Steroids&quot; option on to Question #1a under &quot;Assessment and Management&quot;'
            });
        });
    </script>
</asp:Content>