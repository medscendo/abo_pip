﻿<%@ Page Title="" Language="C#" MasterPageFile="~/PIMMasterPage.master" AutoEventWireup="true" CodeFile="CSMEChart.aspx.cs" Inherits="abo_CSMEChart" %>

<asp:Content ID="Content1" ContentPlaceHolderID="head" Runat="Server">

    <link type="text/css" href="../../common/css/atooltip.css" rel="stylesheet"  media="screen" />
	<script type="text/javascript" src="../../common/js/jquery.atooltip.js"></script>
    
    <style>
        .right {
            text-align:right;
        }
    </style>
    
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder1" Runat="Server">
    <asp:HiddenField ID="HiddenFieldRecordIdentifier" runat="server"/>
    <!--  pim -->
    <div class="pim">
        <div class="record-ident clearfix">
            <h3 class="record-first">RECORD IDENTIFIER: <asp:Literal runat="server" ID="LiteralRecordIdentifier"></asp:Literal></h3>
            <h3 class="record-second"><asp:Literal runat="server" ID="LiteralAbstractionNumber"></asp:Literal></h3>
        </div>
        <!-- PQRS Panel -->
        <asp:Panel runat="server" Visible="true" ID="PQRSPanel" CssClass="clear">
            <table>
                <tr>
                    <th colspan="3" width="910px"><p>Measure #19 - Communication with the Physician Managing On-going Diabetes Care</p></th>
                </tr>
                <!-- Question 1 -->
	            <tr class="table_body">
		            <td>
			            <strong>1. Is this a Physician Fee Schedule (PFS) Medicare Part B Fee-For-Service (FFS) beneficiary?</strong>&nbsp;<a href="#"><img id="TTPQRS19Q1" src="../../common/images/tip.gif" alt="Tip" /></a>
			            <asp:Label ID="LabelPQRSMedicare" runat="server" ForeColor="Red" Visible="false" Text="<br />Please Complete"></asp:Label>
		            </td>
		            <td>
			            <asp:RadioButtonList runat="server" ID="PQRSMedicare" RepeatDirection="Horizontal" RepeatLayout="Flow">
				            <asp:ListItem Value="1" Text=" Yes &nbsp;" />
				            <asp:ListItem Value="0" Text=" No" />
			            </asp:RadioButtonList>
		            </td>
	            </tr>
                <!-- Question 2 -->
	            <tr class="table_body_bg" id="PQRS19Q2">
		            <td>
			            <strong>2. Did this patient have a diagnosis of diabetic retinopathy? (Denominator Criteria)</strong>&nbsp;<a href="#"><img id="TTPQRS19Q2" src="../../common/images/tip.gif" alt="Tip" /></a>
			            <asp:Label ID="LabelPQRSDiagnosis" runat="server" ForeColor="Red" Visible="false" Text="<br />Please Complete"></asp:Label>
		            </td>
		            <td>
			            <asp:RadioButtonList runat="server" ID="PQRSDiagnosis" RepeatDirection="Horizontal" RepeatLayout="Flow">
				            <asp:ListItem Value="1" Text=" Yes &nbsp;" />
				            <asp:ListItem Value="0" Text=" No" />
			            </asp:RadioButtonList>
		            </td>
	            </tr>
                <!-- Question 3 -->
                <tr class="table_body" id="PQRS19Q3">
		            <td>
			            <strong>3. Did the patient have one of the noted encounter codes? (Denominator Criteria)</strong>&nbsp;<a href="#"><img id="TTPQRS19Q3" src="../../common/images/tip.gif" alt="Tip" /></a>
			            <asp:Label ID="LabelPQRSEncounter" runat="server" ForeColor="Red" Visible="false" Text="<br />Please Complete"></asp:Label>
		            </td>
		            <td>
			            <asp:RadioButtonList runat="server" ID="PQRSEncounter" RepeatDirection="Horizontal" RepeatLayout="Flow">
				            <asp:ListItem Value="1" Text=" Yes &nbsp;" />
				            <asp:ListItem Value="0" Text=" No" />
			            </asp:RadioButtonList>
		            </td>
	            </tr>
                <!-- Question 4 -->
                <tr class="table_body_bg" id="PQRS19Q4">
		            <td>
			            <strong>4. Date of patient visit with one of the specified encounter codes</strong>
			            <asp:Label ID="LabelPQRSVisitMonth" runat="server" ForeColor="Red" Visible="false" Text="<br />Please Complete Month"></asp:Label>
			            <asp:Label ID="LabelPQRSVisitYear" runat="server" ForeColor="Red" Visible="false" Text="<br />Please Complete Year"></asp:Label>
		            </td>
		            <td>
			            <asp:DropDownList ID='DropDownListPQRSVisitMonth' DataValueField='MonthID' DataTextField='MonthName' runat='server' />
			            <asp:DropDownList ID='DropDownListPQRSVisitYear' DataValueField='YearID' DataTextField='YearName' runat='server' />
		            </td>
	            </tr>
                <!-- Question 5 -->
                <tr class="table_body" id="PQRS19Q5">
		            <td>
			            <strong>5. Was a dilated macular or fundus exam performed in each eye, AND the level of severity of retinopathy noted within 12 months prior to the Visit Date? (Denominator Criteria)</strong>&nbsp;<a href="#"><img id="TTPQRS19Q5" src="../../common/images/tip.gif" alt="Tip" /></a>
			            <asp:Label ID="LabelPQRSExamPerformed" runat="server" ForeColor="Red" Visible="false" Text="<br />Please Complete"></asp:Label>
		            </td>
		            <td>
			            <asp:RadioButtonList runat="server" ID="PQRSExamPerformed" RepeatDirection="Horizontal" RepeatLayout="Flow">
				            <asp:ListItem Value="1" Text=" Yes &nbsp;" />
				            <asp:ListItem Value="0" Text=" No" />
			            </asp:RadioButtonList>
		            </td>
	            </tr>
                <!-- Question 6 -->
                <tr class="table_body_bg" id="PQRS19Q6">
		            <td>
			            <strong>6. Did the exam include documentation of the presence or absence of macular edema? (Denominator Criteria)</strong>&nbsp;<a href="#"><img id="TTPQRS19Q6" src="../../common/images/tip.gif" alt="Tip" /></a>
			            <asp:Label ID="LabelPQRSDocumentationMacularEdema" runat="server" ForeColor="Red" Visible="false" Text="<br />Please Complete"></asp:Label>
		            </td>
		            <td>
			            <asp:RadioButtonList runat="server" ID="PQRSDocumentationMacularEdema" RepeatDirection="Horizontal" RepeatLayout="Flow">
				            <asp:ListItem Value="1" Text=" Yes &nbsp;" />
				            <asp:ListItem Value="0" Text=" No" />
			            </asp:RadioButtonList>
		            </td>
	            </tr>
                <!-- Question 7 -->
                <tr class="table_body" id="PQRS19Q7">
                    <td width="70%">
                        <strong>7. Was communication with the physician managing on-going diabetes care performed on the visit date or within 12 months of the visit date?</strong>&nbsp;<a href="#"><img id="TTPQRS19Q7" src="../../common/images/tip.gif" alt="Tip" /></a>
                        <asp:Label ID="LabelPQRSCommDiabetesPhysician" runat="server" ForeColor="Red" Visible="false" Text="<br />Please Complete"></asp:Label>
                    </td>
                    <td>
                        <asp:RadioButtonList runat="server" ID="PQRSDiabetesCommunication" RepeatDirection="Horizontal" RepeatLayout="Flow">
                            <asp:ListItem Value="1" Text=" Yes &nbsp;" />
                            <asp:ListItem Value="0" Text=" No" />
                        </asp:RadioButtonList>
                    </td>
                </tr>
                <!-- Question 8 -->
                <tr class="table_body_bg" id="PQRS19Q8">
                    <td>
                        <strong>8. If communication with the physician managing on-going diabetes care was not done, was this due to a documented reason?</strong>&nbsp;<a href="#"><img id="TTPQRS19Q8" src="../../common/images/tip.gif" alt="Tip" /></a>
                        <asp:Label ID="LabelDiabetesCommunicationNo" runat="server" ForeColor="Red" Visible="false" Text="<br />Please Complete"></asp:Label>
                    </td>
                    <td>
                        <asp:RadioButtonList runat="server" ID="PQRSDiabetesCommunicationReason" RepeatDirection="Horizontal" RepeatLayout="Flow">
                            <asp:ListItem Value="1" Text=" Yes &nbsp;" />
                            <asp:ListItem Value="0" Text=" No" />
                        </asp:RadioButtonList>
                    </td>
                </tr>
            </table>
            <br />
            <table>
                <tr>
                    <th colspan="3" width="910px"><p>Measure #117 - Dilated Eye Exam in Diabetic Patient</p></th>
                </tr>
                <!-- Question 1 -->
                <tr class="table_body" id="PQRS117Q1">
                    <td width="70%">
                        <strong>1. Did the patient have one of the noted diagnosis codes?</strong> (See tip for codes) (Denominator Criteria)&nbsp;<a href="#"><img id="TTPQRS117Q1" src="../../common/images/tip.gif" alt="Tip" /></a>
                        <asp:Label ID="LabelPQRSDiagnosis2" runat="server" ForeColor="Red" Visible="false" Text="<br />Please Complete"></asp:Label>

                    </td>
                    <td>
                        <asp:RadioButtonList runat="server" ID="PQRSDiagnosis2" RepeatDirection="Horizontal" RepeatLayout="Flow">
                            <asp:ListItem Value="1" Text=" Yes &nbsp;" />
                            <asp:ListItem Value="0" Text=" No" />
                        </asp:RadioButtonList>
                    </td>
                </tr>
                <!-- Question 2 -->
                <tr class="table_body_bg" id="PQRS117Q2">
                    <td>
                        <strong>2. Did the patient have one of the noted encounter codes?</strong> (See tip for codes) (Denominator Criteria)&nbsp;<a href="#"><img id="TTPQRS117Q2" src="../../common/images/tip.gif" alt="Tip" /></a>
                        <asp:Label ID="LabelPQRSEncounter2" runat="server" ForeColor="Red" Visible="false" Text="<br />Please Complete"></asp:Label>

                    </td>
                    <td>
                        <asp:RadioButtonList runat="server" ID="PQRSEncounter2" RepeatDirection="Horizontal" RepeatLayout="Flow">
                            <asp:ListItem Value="1" Text=" Yes &nbsp;" />
                            <asp:ListItem Value="0" Text=" No" />
                        </asp:RadioButtonList>
                    </td>
                </tr>
                <!-- Question 3 -->
                <tr class="table_body" id="PQRS117Q3">
                    <td>
                        <strong>3. Did the patient have a retinal or dilated eye exam by an eye care professional (optometrist or ophthalmologist) in the measurement period or a negative retinal or dilated eye exam (negative for retinopathy) by an eye care professional (optometrist or ophthalmologist) in the year prior to the measurement period? Possible procedures include the following:
                        <ol style="list-style-type:lower-alpha;">
                            <li>Dilated retinal eye exam with interpretation by an ophthalmologist or optometrist documented and reviewed</li>
                            <li>Seven standard field stereoscopic photos with interpretation by an ophthalmologist or optometrist documented and reviewed</li>
                            <li>Eye imaging validated to match diagnosis from seven standard field stereoscopic photos results documented and reviewed</li>
                            <li>Low risk for retinopathy (no evidence of retinopathy in the prior year).&nbsp;<a href="#"><img id="TTPQRS117Q3" src="../../common/images/tip.gif" alt="Tip" /></a></li>
                        </ol>
                        <asp:Label ID="LabelPQRSExamDone" runat="server" ForeColor="Red" Visible="false" Text="<br />Please Complete"></asp:Label>
                    </td>
                    <td>
                        <asp:RadioButtonList runat="server" ID="DialetedEyeExam" RepeatDirection="Horizontal" RepeatLayout="Flow">
                            <asp:ListItem Value="1" Text=" Yes &nbsp;" />
                            <asp:ListItem Value="0" Text=" No" />
                        </asp:RadioButtonList>
                    </td>
                </tr>
                <!-- Question 4 -->
                <tr class="table_body_bg" id="PQRS117Q4">
                    <td>
                        <strong>4. If none of the above procedures were performed, was this due to the patient having low risk for retinopathy?</strong>&nbsp;<a href="#"><img id="TTPQRS117Q4" src="../../common/images/tip.gif" alt="Tip" /></a>
                        <asp:Label ID="LabelPQRSLowRisk" runat="server" ForeColor="Red" Visible="false" Text="<br />Please Complete"></asp:Label>
                    </td>
                    <td>
                            <asp:RadioButtonList runat="server" ID="PQRSLowRisk" RepeatDirection="Horizontal" RepeatLayout="Flow">
                            <asp:ListItem Value="1" Text=" Yes &nbsp;" />
                            <asp:ListItem Value="0" Text=" No" />
                        </asp:RadioButtonList>
                    </td>
                </tr>
            </table>
            <br />
            <script>
                <%-- Question 1 --%>
                function PQRSMedicareValidation() {
                    if ($('#<%= PQRSMedicare.ClientID %> input:checked').val() == '0') {
                        var myarray = new Array("PQRS19Q2", "PQRS19Q3", "PQRS19Q4", "PQRS19Q5", "PQRS19Q6", "PQRS19Q7", "PQRS19Q8", "PQRS117Q1", "PQRS117Q2", "PQRS117Q3", "PQRS117Q4");
                        var myarray2 = new Array("#PQRS19Q2", "#PQRS19Q3", "#PQRS19Q4", "#PQRS19Q5", "#PQRS19Q6", "#PQRS19Q7", "#PQRS19Q8", "#PQRS117Q1", "#PQRS117Q2", "#PQRS117Q3", "#PQRS117Q4");
                        generate(myarray, myarray2, true);
                    }
                    else {
                        var myarray = new Array("PQRS19Q2", "PQRS117Q1");
                        var myarray2 = new Array("#PQRS19Q2", "#PQRS117Q1");
                        generate(myarray, myarray2, false);
                    }
                }

                <%-- Question 2 --%>
                function PQRSDiagnosisValidation() {
                    if ($('#<%= PQRSDiagnosis.ClientID %> input:checked').val() == '0') {
                        var myarray = new Array("PQRS19Q3", "PQRS19Q4", "PQRS19Q5", "PQRS19Q6", "PQRS19Q7", "PQRS19Q8");
                        var myarray2 = new Array("#PQRS19Q3", "#PQRS19Q4", "#PQRS19Q5", "#PQRS19Q6", "#PQRS19Q7", "#PQRS19Q8");
                        generate(myarray, myarray2, true);
                    }
                    else {
                        var myarray = new Array("PQRS19Q3");
                        var myarray2 = new Array("#PQRS19Q3");
                        generate(myarray, myarray2, false);
                    }
                }

                <%-- Question 3 --%>
                function PQRSEncounterValidation() {
                    if ($('#<%= PQRSEncounter.ClientID %> input:checked').val() == '0') {
                        var myarray = new Array("PQRS19Q4", "PQRS19Q5", "PQRS19Q6", "PQRS19Q7", "PQRS19Q8");
                        var myarray2 = new Array("#PQRS19Q4", "#PQRS19Q5", "#PQRS19Q6", "#PQRS19Q7", "#PQRS19Q8");
                        generate(myarray, myarray2, true);
                    }
                    else {
                        var myarray = new Array("PQRS19Q4", "PQRS19Q5");
                        var myarray2 = new Array("#PQRS19Q4", "#PQRS19Q5");
                        generate(myarray, myarray2, false);
                    }
                }

                <%-- Question 5 --%>
                function PQRSExamPerformedValidation() {
                    if ($('#<%= PQRSExamPerformed.ClientID %> input:checked').val() == '0') {
                        var myarray = new Array("PQRS19Q6", "PQRS19Q7", "PQRS19Q8");
                        var myarray2 = new Array("#PQRS19Q6", "#PQRS19Q7", "#PQRS19Q8");
                        generate(myarray, myarray2, true);
                    }
                    else {
                        var myarray = new Array("PQRS19Q6");
                        var myarray2 = new Array("#PQRS19Q6");
                        generate(myarray, myarray2, false);
                    }
                }

                <%-- Question 6 --%>
                function PQRSDocumentationMacularEdemaValidation() {
                    if ($('#<%= PQRSDocumentationMacularEdema.ClientID %> input:checked').val() == '0') {
                        var myarray = new Array("PQRS19Q7", "PQRS19Q8");
                        var myarray2 = new Array("#PQRS19Q7", "#PQRS19Q8");
                        generate(myarray, myarray2, true);
                    }
                    else {
                        var myarray = new Array("PQRS19Q7", "PQRS19Q8");
                        var myarray2 = new Array("#PQRS19Q7", "#PQRS19Q8");
                        generate(myarray, myarray2, false);
                    }
                }

                <%-- Question 7 --%>
                function PQRSDiabetesCommunicationValidation() {
                    if ($('#<%= PQRSDiabetesCommunication.ClientID %> input:checked').val() == '1') {
                        var myarray = new Array("PQRS19Q8");
                        var myarray2 = new Array("#PQRS19Q8");
                        generate(myarray, myarray2, true);
                    }
                    else {
                        var myarray = new Array("PQRS19Q8");
                        var myarray2 = new Array("#PQRS19Q8");
                        generate(myarray, myarray2, false);
                    }
                }

                <%-- Question 1 --%>
                function PQRSDiagnosis2Validation() {
                    if ($('#<%= PQRSDiagnosis2.ClientID %> input:checked').val() == '0') {
                        var myarray = new Array("PQRS117Q2", "PQRS117Q3", "PQRS117Q4");
                        var myarray2 = new Array("#PQRS117Q2", "#PQRS117Q3", "#PQRS117Q4");
                        generate(myarray, myarray2, true);
                    }
                    else {
                        var myarray = new Array("PQRS117Q2");
                        var myarray2 = new Array("#PQRS117Q2");
                        generate(myarray, myarray2, false);
                    }
                }

                <%-- Question 2 --%>
                function PQRSEncounter2Validation() {
                    if ($('#<%= PQRSEncounter2.ClientID %> input:checked').val() == '0') {
                        var myarray = new Array("PQRS117Q3", "PQRS117Q4");
                        var myarray2 = new Array("#PQRS117Q3", "#PQRS117Q4");
                        generate(myarray, myarray2, true);
                    }
                    else {
                        var myarray = new Array("PQRS117Q3");
                        var myarray2 = new Array("#PQRS117Q3");
                        generate(myarray, myarray2, false);
                    }
                }

                <%-- Question 3 --%>
                function DialetedEyeExamValidation() {
                    if ($('#<%= DialetedEyeExam.ClientID %> input:checked').val() == '1') {
                        var myarray = new Array("PQRS117Q4");
                        var myarray2 = new Array("#PQRS117Q4");
                        generate(myarray, myarray2, true);
                    }
                    else {
                        var myarray = new Array("PQRS117Q4");
                        var myarray2 = new Array("#PQRS117Q4");
                        generate(myarray, myarray2, false);
                    }
                }

                function question_117_1_Click(yesNo) {
                    var Bmyarray = new Array("PQRS117Q3");
                    var Bmyarray2 = new Array("#PQRS117Q3");
                    generate(Bmyarray, Bmyarray2, !yesNo);
                }

                $(document).ready(function () {
                    // Measure #18, question 6, manipulating questions 7 and 8.
                    // Yes

                    var m19_q7Arr = new Array("PQRS19Q7");
                    var m19_q7Arr2 = new Array("#PQRS19Q7");
                    var m19_q8Arr = new Array("PQRS19Q8");
                    var m19_q8Arr2 = new Array("#PQRS19Q8");

                    $("#<%= PQRSMedicare.ClientID %>").click(function () {
                        PQRSMedicareValidation();
                    });
                    PQRSMedicareValidation();

                    $("#<%= PQRSDiagnosis.ClientID %>").click(function () {
                        PQRSDiagnosisValidation();
                    });
                    PQRSDiagnosisValidation();

                    $("#<%= PQRSEncounter.ClientID %>").click(function () {
                        PQRSEncounterValidation();
                    });
                    PQRSEncounterValidation();

                    $("#<%= PQRSExamPerformed.ClientID %>").click(function () {
                        PQRSExamPerformedValidation();
                    });
                    PQRSExamPerformedValidation();

                    $("#<%= PQRSDocumentationMacularEdema.ClientID %>").click(function () {
                        PQRSDocumentationMacularEdemaValidation();
                    });
                    PQRSDocumentationMacularEdemaValidation();

                    $("#<%= PQRSDiabetesCommunication.ClientID %>").click(function () {
                        PQRSDiabetesCommunicationValidation();
                    });
                    PQRSDiabetesCommunicationValidation();

                    $("#<%= PQRSDiagnosis2.ClientID %>").click(function () {
                        PQRSDiagnosis2Validation();
                    });
                    PQRSDiagnosis2Validation();

                    $("#<%= PQRSEncounter2.ClientID %>").click(function () {
                        PQRSEncounter2Validation();
                    });
                    PQRSEncounter2Validation();

                    $("#<%= DialetedEyeExam.ClientID %>").click(function () {
                        DialetedEyeExamValidation();
                    });
                    DialetedEyeExamValidation();

                    /* Check that user is inputting correct year */
                    var rightYearArr = new Array("PQRS19Q5", "PQRS19Q6", "PQRS19Q7", "PQRS19Q8", "PQRS117Q1", "PQRS117Q2", "PQRS117Q3", "PQRS117Q4");
                    var rightYearArr2 = new Array("#PQRS19Q5", "#PQRS19Q6", "#PQRS19Q7", "#PQRS19Q8", "#PQRS117Q1", "#PQRS117Q2", "#PQRS117Q3", "#PQRS117Q4");
                    $("#<%= DropDownListPQRSVisitYear.ClientID %>").change(function () {
                        var year = $("#<%= DropDownListPQRSVisitYear.ClientID %>").val();

                        if (year != 2015 && year != 0) {
                            generate(rightYearArr, rightYearArr2, true);
                            alert("Date of procedure must be in 2015 to be valid for PQRS Reporting.");
                        } else {
                            generate(rightYearArr, rightYearArr2, false);
                        }
                    });
                });

            </script>
            <script>
                $(function () {
                    $('#TTPQRS19Q1').aToolTip({
                        clickIt: true,
                        tipContent: 'Answer "Yes" to this question if the patient is being billed for this visit to Medicare Part B Fee For Service as either their primary or secondary insurance, including Railroad Retirement Board. This does not include Medicare Advantage.'
                    });
                });

                $(function () {
                    $('#TTPQRS19Q2').aToolTip({
                        clickIt: true,
                        tipContent: '<p><strong>ICD-9-CM [for use 1/1/2015-9/30/2015]</strong><strong>:</strong> 362.01, 362.02, 362.03, 362.04, 362.05, 362.06</p><p><strong>ICD-10-CM [for use 10/01/2015-12/31/2015]:</strong> E08.311, E08.319, E08.321, E08.329, E08.331, E08.339, E08.341, E08.349, E08.351, E08.359, E09.311, E09.319, E09.321, E09.329, E09.331, E09.339, E09.341, E09.349, E09.351, E09.359, E10.311, E10.319, E10.321, E10.329, E10.331, E10.339, E10.341, E10.349, E10.351, E10.359, E11.311, E11.319, E11.321, E11.329, E11.331, E11.339, E11.341, E11.349, E11.351, E11.359, E13.311, E13.319, E13.321, E13.329, E13.331, E13.339, E13.341, E13.349, E13.351, E13.359</p>'
                    });
                });

                $(function () {
                    $('#TTPQRS19Q3').aToolTip({
                        clickIt: true,
                        tipContent: 'Diagnosis for diabetic retinopathy (ICD-9-CM): 362.01, 362.02, 362.03, 362.04, 362.05, 362.06'
                    });
                });

                $(function () {
                    $('#TTPQRS19Q5').aToolTip({
                        clickIt: true,
                        tipContent: 'Level of severity of retinopathy is mild nonproliferative, moderate nonproliferative, severe nonproliferative, very severe nonproliferative, proliferative.'
                    });
                });

                $(function () {
                    $('#TTPQRS19Q6').aToolTip({
                        clickIt: true,
                        tipContent: '<p><strong>Definition:<br />Documentation </strong><strong>&ndash; </strong>The medical record must include: documentation of the level of severity of retinopathy (e.g., background diabetic retinopathy, proliferative diabetic retinopathy, non-proliferative diabetic retinopathy) AND documentation of whether macular edema was present or absent.</p>'
                    });
                });

                $(function () {
                    $('#TTPQRS19Q7').aToolTip({
                        clickIt: true,
                        tipContent: '<p><strong>Definitions:<br /> Communication </strong>&ndash; May include documentation in the medical record indicating that the findings of the dilated macular or fundus exam were communicated (e.g., verbally, by letter) with the clinician managing the patient&rsquo;s diabetic care OR a copy of a letter in the medical record to the clinician managing the patient&rsquo;s diabetic care outlining the findings of the dilated macular or fundus exam.<strong><br /> Findings &ndash; </strong>Includes level of severity of retinopathy (e.g., mild nonproliferative, moderate nonproliferative, severe nonproliferative, very severe nonproliferative, proliferative) AND the presence or absence of macular edema.</p>'
                    });
                }); 

                $(function () {
                    $('#TTPQRS19Q8').aToolTip({
                        clickIt: true,
                        tipContent: 'Examples of documented reasons include medical reasons or patient reasons.'
                    });
                });


                $(function () {
                    $('#TTPQRS117Q1').aToolTip({
                        clickIt: true,
                        tipContent: '<p><strong>ICD-9-CM </strong><strong>[</strong><strong>for use 1/1/2015-9/30/2015]</strong><strong>:</strong> 250.00, 250.01, 250.02, 250.03, 250.10, 250.11, 250.12, 250.13, 250.20, 250.21, 250.22, 250.23, 250.30, 250.31, 250.32, 250.33, 250.40, 250.41, 250.42, 250.43, 250.50, 250.51, 250.52, 250.53, 250.60, 250.61, 250.62, 250.63, 250.70, 250.71, 250.72, 250.73, 250.80, 250.81, 250.82, 250.83, 250.90, 250.91, 250.92, 250.93, 357.2, 362.01, 362.02, 362.03, 362.04, 362.05, 362.06, 362.07, 366.41, 648.00, 648.01, 648.02, 648.03, 648.04</p><p><strong>ICD-10-CM </strong><strong>[</strong><strong>for use 10/1/2015-12/31/2015</strong><strong>]:</strong> E10.10, E10.11, E10.21, E10.22, E10.29, E10.311, E10.319, E10.321, E10.329, E10.331, E10.339, E10.341, E10.349, E10.351, E10.359, E10.36, E10.39, E10.40, E10.41, E10.42, E10.43, E10.44, E10.49, E10.51, E10.52, E10.59, E10.610, E10.618, E10.620, E10.621, E10.622, E10.628, E10.630, E10.638, E10.641, E10.649, E10.65, E10.69, E10.8, E10.9, E11.00, E11.01, E11.21, E11.22, E11.29, E11.311, E11.319, E11.321, E11.329, E11.331, E11.339, E11.341, E11.349, E11.351, E11.359, E11.36, E11.39, E11.40, E11.41, E11.42, E11.43, E11.44, E11.49, E11.51, E11.52, E11.59, E11.610, E11.618, E11.620, E11.621, E11.622, E11.628, E11.630, E11.638, E11.641, E11.649, E11.65, E11.69, E11.8, E11.9, O24.011, O24.012, O24.013, O24.019, O24.02, O24.03, O24.111, O24.112, O24.113, O24.119, O24.12, O24.13&nbsp;</p>'
                    });
                });
                $(function () {
                    $('#TTPQRS117Q2').aToolTip({
                        clickIt: true,
                        tipContent: '<p>92002, 92004, 92012, 92014, 99201, 99202, 99203, 99204, 99205, 99211, 99212, 99213, 99214, 99215, 99217, 99218, 99219, 99220, 99221, 99222, 99223, 99231, 99232, 99233, 99238, 99239, 99281, 99282, 99283, 99284, 99285, 99291, 99304, 99305, 99306, 99307, 99308, 99309, 99310, 99315, 99316, 99318, 99324, 99325, 99326, 99327, 99328, 99334, 99335, 99336, 99337, 99341, 99342, 99343, 99344, 99345, 99347, 99348, 99349, 99350, G0402, G0438, G0439</p>'
                    });
                });
                $(function () {
                    $('#TTPQRS117Q3').aToolTip({
                        clickIt: true,
                        tipContent: '<p>For dilated eye exams performed in the previous 12 months, an automated result must be available.&nbsp;</p><p><strong>Definition:</strong></p><p><strong>Automated Result: </strong>Electronic system-based data that includes results generated from test or procedures. For administrative data collection automated/electronic results are necessary in order to show that the exam during the 12 months prior was negative for retinopathy.</p>'
                    });
                });
                $(function () {
                    $('#TTPQRS117Q4').aToolTip({
                        clickIt: true,
                        tipContent: '<p>Low risk for retinopathy is no evidence of retinopathy in the prior year.&nbsp; Low risk can only be selected if the encounter was during the measurement period because it indicates that the patient had &ldquo;no evidence of retinopathy in the prior year&rdquo;. This definition indicates results were negative, therefore an automated result is not required.</p>'
                    });
                });
            </script>
        </asp:Panel>
        <!-- History -->
        <table>
            <tr>
                <th colspan="2" width="910px"><p>History</p></th>
            </tr>
            <!-- Question 1 -->
            <tr class="table_body">
                <td width="70%">
                    <strong>1. Date of birth</strong>
                    <asp:Label ID='LabelMonthOfBirth' runat="server" Visible="false" ForeColor="Red" Text="<br />Please complete month" />
                    <asp:Label ID='LabelYearOfBirth' runat="server" Visible="false" ForeColor="Red" Text="<br />Please complete year" />
                </td>
                <td>
                    <asp:DropDownList ID='DropDownListMonthOfBirth' DataValueField='MonthID' DataTextField='MonthName' runat='server' />
                    <asp:DropDownList ID='DropDownListYearOfBirth' DataValueField='YearID' DataTextField='YearName' runat='server' />
                </td>
            </tr>
            <!-- Question 2 -->
            <tr class="table_body_bg">
                <td>
                    <strong>2. Date of exam when you first diagnosed CSME in this patient</strong>
                    <asp:Label ID='LabelMonthOfExam' runat="server" Visible="false" ForeColor="Red" Text="<br />Please complete month" />
                    <asp:Label ID='LabelYearOfExam' runat="server" Visible="false" ForeColor="Red" Text="<br />Please complete year" />
                    <asp:Label ID='Labelinitialage' runat='server' Visible="false" ForeColor="Red" Text='<br />' />
                    <asp:Label ID='Labelinitialvis' runat='server' Visible="false" ForeColor="Red" Text='<br />Please complete year' />
                </td>
                <td>
                    <asp:DropDownList ID='DropDownListMonthOfExam' DataValueField='MonthID' DataTextField='MonthName' runat='server' />
                    <asp:DropDownList ID='DropDownListYearOfExam' DataValueField='YearID' DataTextField='YearName' runat='server' />
                </td>
            </tr>
            <!-- Question 3 -->
            <tr class="table_body">
                <td>
                    <strong>3. Date of follow-up exam</strong>
                    <br />(choose exam that is closest to 1 year following the date you first diagnosed CSME in this patient (date in #2), but the date should be at least 1 year in f/u)
                    <asp:Label ID='LabelMonthOfFollowUp' runat="server" Visible="false" ForeColor="Red" Text="<br />Please complete month" />
                    <asp:Label ID='LabelYearOfFollowUp' runat="server" Visible="false" ForeColor="Red" Text="<br />Please complete year" />
                    <asp:Label ID='Labelinitialfol' runat='server' Visible="false"  ForeColor="Red" Text="<br />Please complete year" />
                </td>
                <td>
                    <asp:DropDownList ID='DropDownListMonthOfFollowUp' DataValueField='MonthID' DataTextField='MonthName' runat='server' />
                    <asp:DropDownList ID='DropDownListYearOfFollowUp' DataValueField='YearID' DataTextField='YearName' runat='server' />
                </td>
            </tr>
            <!-- Question 4 -->
            <tr class="table_body_bg">
                <td>
                    <strong>4. Gender</strong>
                    <asp:Label ID='LabelRBGender' runat="server" Visible="false" ForeColor="Red" Text="<br />Please Complete" />
                </td>
                <td>
                    <asp:RadioButtonList id='RadioButtonListRBGender' RepeatDirection='Horizontal' CssClass='aspxList' runat='server'>
                        <asp:ListItem Value='42'>&nbsp;Male</asp:ListItem>
                        <asp:ListItem Value='43'>&nbsp;Female</asp:ListItem>
                    </asp:RadioButtonList>
                </td>
            </tr>
            <!-- Question 5 -->
            <tr class="table_body">
                <td>
                    <strong>5. Was a history taken of the patient’s past diabetic control?</strong>&nbsp;<a href="#"><img id="QH5" src="../../common/images/tip.gif" alt="Tip" /></a>
                    <asp:Label ID='LabelRBHistoryDiabetic' runat="server" Visible="false" ForeColor="Red" Text="<br />Please Complete" />
                </td>
                <td>
                    <asp:RadioButtonList id='RadioButtonListRBHistoryDiabetic' RepeatDirection='Horizontal' CssClass='aspxList' runat='server'>
                        <asp:ListItem Value="1">&nbsp;Yes</asp:ListItem>
	                    <asp:ListItem Value="2">&nbsp;No</asp:ListItem>
	                    <asp:ListItem Value="3">&nbsp;Not Recorded</asp:ListItem>
                    </asp:RadioButtonList>
                </td>
            </tr>
            <!-- Question 6 -->
            <tr class="table_body_bg">
                <td>
                    <strong>6. Has the patient had prior cataract surgery in the eye with CSME?</strong>
                    <br />(If both eyes have CSME, designate the Right eye as the eye with CSME.)
                    <asp:Label ID='LabelRBHistoryCataractSurgery' runat="server" Visible="false" ForeColor="Red" Text="<br />Please Complete" />
                </td>
                <td>
                    <asp:RadioButtonList id='RadioButtonListRBHistoryCataractSurgery' RepeatDirection='Horizontal' CssClass='aspxList' runat='server'>
                        <asp:ListItem Value="1">&nbsp;Yes</asp:ListItem>
	                    <asp:ListItem Value="2">&nbsp;No</asp:ListItem>
	                    <asp:ListItem Value="3">&nbsp;Not Recorded</asp:ListItem>
                    </asp:RadioButtonList>
                </td>
            </tr>
        </table>
        <br />
        <!-- Examination and Diagnostic Procedures -->
        <table>
            <tr>
                <th colspan="3" width="910px"><p>Examination and Diagnostic Procedures</p></th>
            </tr>
            <!-- Question 1 -->
            <tr class="table_body">
                <td colspan="2">
                    <strong>1. Which eye has the CSME?</strong>
                    <br />(If both eyes have CSME, designate one eye, preferably the most recently affected eye, as the "Involved" eye, and designate the other eye as the "Fellow" eye.)
                    <asp:Label ID='LabelRBExamEyeID' runat="server" Visible="false" ForeColor="Red" Text="<br />Please Complete" />
                </td>
                <td>
                    <asp:RadioButtonList id='RadioButtonListRBExamEyeID' RepeatDirection='Horizontal' CssClass='aspxList' runat='server'>
                        <asp:ListItem Value='49'>&nbsp;OD</asp:ListItem>
                        <asp:ListItem Value='50'>&nbsp;OS</asp:ListItem>
                        <asp:ListItem Value='110'>&nbsp;Both</asp:ListItem>
                    </asp:RadioButtonList>
                </td>
            </tr>
            <!-- Question 2 -->
            <tr class="table_body_bg">
                <td width="50%" rowspan="2">
                    <strong>2. Visual acuity, best corrected with current script and pinhole</strong>&nbsp;<a href="#"><img id="QE2" src="../../common/images/tip.gif" alt="Tip" /></a>
                    <asp:Label ID='LabelInvolvedBCVA' runat="server" Visible="false" ForeColor="Red" Text="<br />Please complete involved" />
                    <asp:Label ID='LabelFellowBCVA' runat="server" Visible="false" ForeColor="Red" Text="<br />Please complete fellow" />    
                </td>
                <td width="20%"><span class="right">Involved Eye with CSME:</span></td>
                <td>                        
                    20 / <asp:DropDownList ID='DropDownListInvolvedBCVA' DataValueField='examValue' DataTextField='examLabel' runat='server' />
                </td>
            </tr>
            <tr class="table_body_bg">
                <td><span class="right">Fellow Eye:</span></td>
                <td>                        
                    20 / <asp:DropDownList ID='DropDownListFellowBCVA' DataValueField='examValue' DataTextField='examLabel' runat='server' />
                </td>
            </tr>
            <!-- Question 3 -->
            <tr class="table_body">
                <td colspan="2">
                    <strong>3. Was a dilated fundus examination performed in each eye, if possible?</strong>
                    <asp:Label ID='LabelRBDilatedFundusExam' runat="server" Visible="false" ForeColor="Red" Text="<br />Please Complete" />
                </td>
                <td>
                    <asp:RadioButtonList id='RadioButtonListRBDilatedFundusExam' RepeatDirection='Horizontal' CssClass='aspxList' runat='server'>
                        <asp:ListItem Value="1">&nbsp;Yes</asp:ListItem>
	                    <asp:ListItem Value="2">&nbsp;No</asp:ListItem>
	                    <asp:ListItem Value="3">&nbsp;Not Recorded</asp:ListItem>
                    </asp:RadioButtonList>
                </td>
            </tr>
            <!-- Question 4 -->
            <tr class="table_body_bg">
                <td colspan="2">
                    <strong>4. What was the central subfield thickness in the involved eye on OCT?</strong>
                    <asp:Label ID='LabelExamCST' runat="server" Visible="false" ForeColor="Red" Text="<br />Please Complete " />
                    <asp:Label ID='LabelExamCSTNA' runat="server" Visible="false" ForeColor="Red" Text="<br />Please Complete N/A" />
                </td>
                <td>                        
                    <asp:TextBox runat='server' ID='TextBoxExamCST' size='5' onkeyup="toggleQE4CB();" /> microns
                    <br /><asp:CheckBox runat='server' ID='CheckBoxExamCSTNA' text='&nbsp;No OCT obtained' onclick="toggleQE4TB();" />
                </td>
            </tr>
            <!-- Question 5 -->
            <tr class="table_body">
                <td colspan="2">
                    <strong>5. Did the macular edema involve the center of the fovea?</strong>
                    <asp:Label ID='LabelRBExamMacularEdema' runat="server" Visible="false" ForeColor="Red" Text="<br />Please Complete" />
                </td>
                <td>                   
                    <asp:RadioButtonList id='RadioButtonListRBExamMacularEdema' RepeatDirection='Horizontal' CssClass='aspxList' runat='server'>
                        <asp:ListItem Value="1">&nbsp;Yes</asp:ListItem>
	                    <asp:ListItem Value="2">&nbsp;No</asp:ListItem>
	                    <asp:ListItem Value="3">&nbsp;Not Recorded</asp:ListItem>
                    </asp:RadioButtonList>
                </td>
            </tr>
        </table>
        <br />
        <!-- Assessment and Management (current diagnosis) -->
        <table>
            <tr>
                <th colspan="3" width="910px"><p>Assessment and Management (current diagnosis)</p></th>
            </tr>
            <!-- Question 1 -->
            <tr class="table_body">
                <td width="70%">
                    <strong>1. What was the type and level of severity of retinopathy?</strong>
                    <asp:Label ID='LabelRBTypeRetinopathy' runat="server" Visible="false" ForeColor="Red" Text="<br />Please Complete" />
                </td>
                <td>
                    <asp:RadioButtonList id='RadioButtonListRBTypeRetinopathy' RepeatDirection='Vertical' CssClass='aspxList' runat='server'>
                        <asp:ListItem Value='145'>&nbsp;Mild NPDR</asp:ListItem>
                        <asp:ListItem Value='146'>&nbsp;Moderate NDPR</asp:ListItem>
                        <asp:ListItem Value='147'>&nbsp;Severe NPDR</asp:ListItem>
                        <asp:ListItem Value='148'>&nbsp;Very severe NPDR</asp:ListItem>
                        <asp:ListItem Value='149'>&nbsp;PDR: not high risk</asp:ListItem>
                        <asp:ListItem Value='3'>&nbsp;Not documented</asp:ListItem>
                    </asp:RadioButtonList>
                </td>
            </tr>
            <!-- Question 2 -->
            <tr class="table_body_bg">
                <td>
                    <strong>2. Was the patient counseled/educated on the importance of blood sugar, blood pressure control, and blood lipids?</strong>
                    <asp:Label ID='LabelRBPatientCounseled' runat="server" Visible="false" ForeColor="Red" Text="<br />Please Complete" />
                </td>
                <td>
                    <asp:RadioButtonList id='RadioButtonListRBPatientCounseled' RepeatDirection='Horizontal' CssClass='aspxList' runat='server'>
                        <asp:ListItem Value="1">&nbsp;Yes</asp:ListItem>
	                    <asp:ListItem Value="2">&nbsp;No</asp:ListItem>
	                    <asp:ListItem Value="3">&nbsp;Not Recorded</asp:ListItem>
                    </asp:RadioButtonList>
                </td>
            </tr>
               
            <!-- Question 3 -->
            <tr class="table_body">
                <td>
                    <strong>3. What initial management option(s) were recommended?</strong>
                    <br />(you may check more than one answer)
                    <asp:Label ID='LabelRBManagementOption' runat="server" Visible="false" ForeColor="Red" Text="<br />Please Complete" />
                </td>
                <td>
                    <asp:CheckBox ID="CheckBoxFocalGridLaser" Text="Focal/grid laser" runat="server" />
                    <br /><asp:CheckBox ID="CheckBoxBevacizumab" Text="Bevacizumab" runat="server" />
                    <br /><asp:CheckBox ID="CheckBoxIntravitrealSteroids" Text="Intravitreal steroids" runat="server" />
                    <br /><asp:CheckBox ID="CheckBoxPPVx" Text="PPVx" runat="server" />
                    <br /><asp:CheckBox ID="CheckBoxObservation" Text="Observation" runat="server" />
                    <br /><asp:CheckBox ID="CheckBoxObservationOther" Text="Other" runat="server" />
                </td>
            </tr>
            <!-- Question 3a -->
            <tr class="table_body">
                <td id="QAM3aA">
                    <strong>3a. If "Other" please specify</strong>
                </td>
                <td id="QAM3aB">
                    <asp:TextBox runat='server' ID='TextBoxMgntOptionOtherText' size='25' />
                </td>
            </tr>
            <!-- Question 4 -->
            <tr class="table_body_bg">
                <td>
                    <strong>4. Was communication with the physician managing on-going diabetes care performed on this date or within the year?</strong>
                    <asp:Label ID='LabelRBMgntPhysCommunication' runat="server" Visible="false" ForeColor="Red" Text="<br />Please Complete" />
                </td>
                <td>
                    <asp:RadioButtonList id='RadioButtonListRBMgntPhysCommunication' RepeatDirection='Horizontal' CssClass='aspxList' runat='server'>
                        <asp:ListItem Value="1">&nbsp;Yes</asp:ListItem>
	                    <asp:ListItem Value="2">&nbsp;No</asp:ListItem>
	                    <asp:ListItem Value="3">&nbsp;Not Recorded</asp:ListItem>
                    </asp:RadioButtonList>
                </td>
            </tr>
        </table>
        <br />
        <!-- Outcomes -->
        <table>
            <tr>
                <th colspan="3" width="910px"><p>Outcomes</p></th>
            </tr>
            <!-- Question 1 -->
            <tr class="table_body">
                <td width="50%" rowspan="2">
                    <strong>1. Best corrected visual acuity</strong>&nbsp;<a href="#"><img id="QO1" src="../../common/images/tip.gif" alt="Tip" /></a>
                    <asp:Label ID='LabelOutcomeTreatedBCVA' runat="server" Visible="false" ForeColor="Red" Text="<br />Please complete treated" />
                    <asp:Label ID='LabelOutcomeFellowBCVA' runat="server" Visible="false" ForeColor="Red" Text="<br />Please complete fellow" />
                </td>
                <td width="20%"><span class="right">Treated eye:</span></td>
                <td>
                    20&nbsp;/&nbsp;<asp:DropDownList ID='DropDownListOutcomeTreatedBCVA' DataValueField='examValue' DataTextField='examLabel' runat='server' />
                </td>
            </tr>
            <!-- Question 1a -->
            <tr class="table_body">
                <td width="20%"><span class="right">Fellow eye:</span></td>
                <td>
                    20&nbsp;/&nbsp;<asp:DropDownList ID='DropDownListOutcomeFellowBCVA' DataValueField='examValue' DataTextField='examLabel' runat='server' />
                </td>
            </tr>
            <!-- Question 2 -->
            <tr class="table_body_bg">
                <td colspan="2">
                    <strong>2.	Were any therapies required over the past year for management of the CSME?</strong>
                    <br />(Please check all that are appropriate, and indicate the # of times each therapy was given over the year. <em>Do not</em> include the treatment at the initial exam)  
                </td>
                <td>
                    <table class="aspxList">
                        <tr>
                            <td><asp:CheckBox runat='server' ID='CheckBoxTherapyBevacizumab' text='&nbsp;Bevacizumab' /></td>
                            <td># <asp:TextBox runat='server' ID='TextBoxTherapyBevacizumabNbr' size='3' /></td>
                        </tr>
                        <tr>
                            <td><asp:CheckBox runat='server' ID='CheckBoxTherapyRanibizumab' text='&nbsp;Ranibizumab' /></td>
                            <td># <asp:TextBox runat='server' ID='TextBoxTherapyRanibizumabNbr' size='3' /></td>
                        </tr>
                        <tr>
                            <td><asp:CheckBox runat='server' ID='CheckBoxTherapyFocalLaser' text='&nbsp;Focal/grid laser' /></td>
                            <td># <asp:TextBox runat='server' ID='TextBoxTherapyFocalLaserNbr' size='3' /></td>
                        </tr>
                        <tr>
                            <td><asp:CheckBox runat='server' ID='CheckBoxTherapyIntravitrealSteroids' text='&nbsp;Intravit. Steroids' /></td>
                            <td># <asp:TextBox runat='server' ID='TextBoxTherapyIntravitrealSteroidsNbr' size='3' /></td>
                        </tr>
                        <tr>
                            <td><asp:CheckBox runat='server' ID='CheckBoxTherapyVitrectomyLaser' text='&nbsp;Vitrectomy' /></td>
                            <td># <asp:TextBox runat='server' ID='TextBoxTherapyVitrectomyNbr' size='3' /></td>
                        </tr>
                        <tr>
                            <td><asp:CheckBox runat='server' ID='CheckBoxTherapyOther' text='&nbsp;Other' />&nbsp;<asp:TextBox runat='server' ID='TextBoxTherapyOtherText' size='10' /></td>
                            <td># <asp:TextBox runat='server' ID='TextBoxTherapyOtherNbr' size='3' /></td>
                        </tr>
                        <tr>
                            <td><asp:CheckBox runat='server' ID='CheckBoxTherapyNone' text='&nbsp;None' /></td>
                        </tr>
                    </table>
                </td>
            </tr>
                <tr class="table_body">
                <td colspan="2">
                    <strong>3. If Intravitreal corticosteroids were administered over the one year follow-up, did the IOP elevate enough to require initiation of IOP-lowering medications? </strong>
                    <asp:Label ID='LabelIntravitrealCorticosteroids' runat="server" Visible="false" ForeColor="Red" Text="<br />Please complete" />
                </td>
                <td>
                    <asp:RadioButtonList id='RadioButtonListIntravitrealCorticosteroids' 
                        RepeatDirection='Horizontal' CssClass='aspxList' runat='server'>
                        <asp:ListItem Value="1">&nbsp;Yes</asp:ListItem>
	                    <asp:ListItem Value="2">&nbsp;No</asp:ListItem>
	                    <asp:ListItem Value="3">&nbsp;Not Recorded</asp:ListItem>
                    </asp:RadioButtonList>
                &nbsp;<br />
                </td>
            </tr>
            <!-- Question 3 -->
            <tr class="table_body_bg">
                <td colspan="2">
                    <strong>4. What was the central subfield thickness on OCT?</strong>
                    <asp:Label ID='LabelOutcomeCST' runat="server" Visible="false" ForeColor="Red" Text="<br />Please complete" />
                    <asp:Label ID='LabelOutcomeCSTNA' runat="server" Visible="false" ForeColor="Red" Text="<br />Please Complete N/A" />
                </td>
                <td>
                    <asp:TextBox runat='server' ID='TextBoxOutcomeCST' size='5' onkeyup="toggleQO3CB();"  />&nbsp;microns
                    <br /><asp:CheckBox runat='server' ID='CheckBoxOutcomeCSTNA' text='&nbsp;No OCT obtained' onclick="toggleQO3TB();" />
                </td>
            </tr>
            <!-- Question 4 -->
            <tr class="table_body">
                <td colspan="2">
                    <strong>5. What is the status of the CSME, when compared to the initial exam?</strong>
                    <asp:Label ID='LabelRBOutcomeStatusCSME' runat="server" Visible="false" ForeColor="Red" Text="<br />Please Complete" />    
                </td>
                <td>
                    <asp:RadioButtonList id='RadioButtonListRBOutcomeStatusCSME' RepeatDirection='Vertical' CssClass='aspxList' runat='server'>
                        <asp:ListItem Value="329">&nbsp;Resolved</asp:ListItem>
	                    <asp:ListItem Value="331">&nbsp;NCSME</asp:ListItem>
	                    <asp:ListItem Value="332">&nbsp;Improved but CSME persists</asp:ListItem>
	                        <asp:ListItem Value="333">&nbsp;CSME: Same</asp:ListItem>
	                    <asp:ListItem Value="334">&nbsp;CSME: Worse</asp:ListItem>
                    </asp:RadioButtonList>
                </td>
            </tr>
        </table>
        <div class="button-box">
            <asp:LinkButton ID="LinkButtonBackToDashboard" runat="server" Text="Back To Chart Registration" PostBackUrl="../PatientChartRegistration.aspx?CycleNumber=1" Visible="false" CssClass="button" />
            <asp:Button ID="ButtonSubmit" OnClientClick="return ButtonSubmit_PQRS();" OnClick="ButtonSubmit_Click" runat="server" Text="Submit Chart" CssClass="button" />
        </div>
    </div>
    <!-- CSME pim ends -->
    <script>
        //      Either enables or disables input boxes
        function enabledControl(el, disabled, checked) {
            //alert("Hello world from enabled control");
            try {
                if (disabled) {
                    el.disabled = disabled;
                    el.setAttribute("disabled", true);
                }
                else {
                    el.disabled = disabled;
                    el.removeAttribute("disabled");
                }

                if (checked == true)
                    el.checked = false;
            }
            catch (E) {
            }
            if (el.childNodes && el.childNodes.length > 0) {
                for (var x = 0; x < el.childNodes.length; x++) {
                    enabledControl(el.childNodes[x], disabled, checked);
                }
            }
        }
        //      Either enables or disables dropdown boxes
        function enabledControlDropDown(el, disabled, checked) {
            //alert("Hello world from enabled control");
            try {
                if (disabled) {
                    el.disabled = disabled;
                    el.setAttribute("disabled", true);
                }
                else {
                    el.disabled = disabled;
                    el.removeAttribute("disabled");
                }

                if (checked == true)
                    el.options[0].selected = true;
            }
            catch (E) {
            }
            if (el.childNodes && el.childNodes.length > 0) {
                for (var x = 0; x < el.childNodes.length; x++) {
                    enabledControl(el.childNodes[x], disabled, checked);
                }
            }
        }
        //      Either enables or disables text boxes
        function enabledControlTextField(el, disabled, checked) {
            //alert("Hello world from enabled control");
            try {
                if (disabled) {
                    el.disabled = disabled;
                    el.setAttribute("disabled", true);
                }
                else {
                    el.disabled = disabled;
                    el.removeAttribute("disabled");
                }

                if (checked == true) {
                    el.value = '';
                }
            }
            catch (E) {
            }
            if (el.childNodes && el.childNodes.length > 0) {
                for (var x = 0; x < el.childNodes.length; x++) {
                    enabledControl(el.childNodes[x], disabled, checked);
                }
            }
        }

        function generate(arr1, arr2, istrue) {
            if (istrue == true) {
                for (var i = 0; i < arr1.length; i++) {
                    document.getElementById(arr1[i]).style.color = "gray";
                }
                for (var i = 0; i < arr2.length; i++) {
                    $(arr2[i] + ' :input').attr('disabled', true);
                    $(arr2[i] + ' :input').removeAttr("checked");
                }
            }
            if (istrue == false) {
                for (var i = 0; i < arr1.length; i++) {
                    document.getElementById(arr1[i]).style.color = "#333";
                }
                for (var i = 0; i < arr2.length; i++) {
                    $(arr2[i] + ' :input').removeAttr('disabled');
                }
            }
        }

        function toggleQE4TB() {
            document.getElementById("<%= TextBoxExamCST.ClientID %>").value = "";
        }

        function toggleQE4CB() {
            document.getElementById("<%= CheckBoxExamCSTNA.ClientID %>").checked = false;
        }
        function toggleQO3TB() {
            document.getElementById("<%= TextBoxOutcomeCST.ClientID %>").value = "";
        }

        function toggleQO3CB() {
            document.getElementById("<%= CheckBoxOutcomeCSTNA.ClientID %>").checked = false;
        }

        $(document).ready(function () {
            
            // QAM3a, Other
            $("#<%= CheckBoxObservationOther.ClientID %>").click(function () {
                if ($('#<%= CheckBoxObservationOther.ClientID %>').prop('checked') == true) {
                    var myaray = new Array("QAM3aA", "QAM3aB");
                    var myarray2 = new Array('#QAM3aB');
                    generate(myaray, myarray2, false);

                    enabledControlTextField(document.getElementById("<%= TextBoxMgntOptionOtherText.ClientID %>"), false, false);
                }
                else {
                    var myaray = new Array("QAM3aA", "QAM3aB");
                    var myarray2 = new Array('#QAM3aB');
                    generate(myaray, myarray2, true);

                    enabledControlTextField(document.getElementById("<%= TextBoxMgntOptionOtherText.ClientID %>"), true, true);
                }
            });
            if ($('#<%= CheckBoxObservationOther.ClientID %>').prop('checked') == true) {
                var myaray = new Array("QAM3aA", "QAM3aB");
                var myarray2 = new Array('#QAM3aB');
                generate(myaray, myarray2, false);

                enabledControlTextField(document.getElementById("<%= TextBoxMgntOptionOtherText.ClientID %>"), false, false);
            }
            else {
                var myaray = new Array("QAM3aA", "QAM3aB");
                var myarray2 = new Array('#QAM3aB');
                generate(myaray, myarray2, true);

                enabledControlTextField(document.getElementById("<%= TextBoxMgntOptionOtherText.ClientID %>"), true, true);
            }

            // QO2, Other
            $("#<%= CheckBoxTherapyOther.ClientID %>").click(function () {
                if ($('#<%= CheckBoxTherapyOther.ClientID %>').prop('checked') == true) {

                    enabledControlTextField(document.getElementById("<%= TextBoxTherapyOtherText.ClientID %>"), false, false);
                    enabledControlTextField(document.getElementById("<%= TextBoxTherapyOtherNbr.ClientID %>"), false, false);
                }
                else {

                    enabledControlTextField(document.getElementById("<%= TextBoxTherapyOtherText.ClientID %>"), true, true);
                    enabledControlTextField(document.getElementById("<%= TextBoxTherapyOtherNbr.ClientID %>"), true, true);
                }
            });
            if ($('#<%= CheckBoxTherapyOther.ClientID %>').prop('checked') == true) {

                enabledControlTextField(document.getElementById("<%= TextBoxTherapyOtherText.ClientID %>"), false, false);
                enabledControlTextField(document.getElementById("<%= TextBoxTherapyOtherNbr.ClientID %>"), false, false);
            }
            else {

                enabledControlTextField(document.getElementById("<%= TextBoxTherapyOtherText.ClientID %>"), true, true);
                enabledControlTextField(document.getElementById("<%= TextBoxTherapyOtherNbr.ClientID %>"), true, true);
            }        
    
            $("#<%= CheckBoxTherapyOther.ClientID %>").click(function() {
                if ($('#<%= CheckBoxTherapyOther.ClientID %>').prop('checked') == true) {
                    enabledControlTextField(document.getElementById("<%= TextBoxTherapyOtherText.ClientID %>"), false, false);
                    enabledControlTextField(document.getElementById("<%= TextBoxTherapyOtherNbr.ClientID %>"), false, false);
                }
                else {
                    enabledControlTextField(document.getElementById("<%= TextBoxTherapyOtherText.ClientID %>"), true, true);
                    enabledControlTextField(document.getElementById("<%= TextBoxTherapyOtherNbr.ClientID %>"), true, true);
                }
            });
            if ($('#<%= CheckBoxTherapyOther.ClientID %>').prop('checked') == true) {
                enabledControlTextField(document.getElementById("<%= TextBoxTherapyOtherText.ClientID %>"), false, false);
                enabledControlTextField(document.getElementById("<%= TextBoxTherapyOtherNbr.ClientID %>"), false, false);
            }
            else {
                enabledControlTextField(document.getElementById("<%= TextBoxTherapyOtherText.ClientID %>"), true, true);
                enabledControlTextField(document.getElementById("<%= TextBoxTherapyOtherNbr.ClientID %>"), true, true);
            }
        });
    </script>
    <script>
        function ButtonSubmit_PQRS() {
            var pqrs = ("<%= (PQRSEnabled && PQRSUsed) %>" == "True");
            if (pqrs) {
                var year = $("#<%= DropDownListPQRSVisitYear.ClientID %>").val();
                var month = $("#<%= DropDownListPQRSVisitMonth.ClientID %>").val();
                if (year == 0 || month == 0)
                    return confirm("If there is no PQRS visit date selected, all of your PQRS data will not be saved, is that okay?");
                if (year != 2015)
                    return confirm("If the PQRS visit date selected is not 2015, all of your PQRS data will not be saved, is that okay?");
            }
            return true;
        }

        $(function() {
            $('#QH5').aToolTip({
                clickIt: true,
                tipContent: 'such as HgA1c'
            });
        });
        $(function() {
            $('#QE2').aToolTip({
                clickIt: true,
                tipContent: '20/800 or count fingers @ 5 ft<br />20/1000 or count fingers @ 4 ft<br />20/1600 or count fingers @ 3ft<br />20/2000 or count fingers @ 2 ft<br />20/4000 or count fingers @ 1 ft<br />20/7777 =CF<br />20/8888 =HM<br />20/9999 =LP<br />20/0000 =NLP'
            });
        });
        $(function() {
            $('#QO1').aToolTip({
                clickIt: true,
                tipContent: '20/800 or count fingers @ 5 ft<br />20/1000 or count fingers @ 4 ft<br />20/1600 or count fingers @ 3ft<br />20/2000 or count fingers @ 2 ft<br />20/4000 or count fingers @ 1 ft<br />20/7777 =CF<br />20/8888 =HM<br />20/9999 =LP<br />20/0000 =NLP'
            });
        });        
    </script>
</asp:Content>

