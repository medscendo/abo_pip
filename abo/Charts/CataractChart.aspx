﻿<%@ Page Title="" Language="C#" MasterPageFile="~/PIMMasterPage.master" AutoEventWireup="true" CodeFile="CataractChart.aspx.cs" Inherits="abo_CataractChart" %>

<asp:Content ID="Content1" ContentPlaceHolderID="head" Runat="Server">

    <link type="text/css" href="../../common/css/atooltip.css" rel="stylesheet"  media="screen" />
        
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder1" Runat="Server">
    <asp:HiddenField ID="HiddenFieldRecordIdentifier" runat="server" />
        
    <!--  pim -->
    <div class="pim">
        <div class="record-ident clearfix">
            <h3 class="record-first">RECORD IDENTIFIER: <asp:Literal runat="server" ID="LiteralRecordIdentifier"></asp:Literal></h3>
            <h3 class="record-second"><asp:Literal runat="server" ID="LiteralAbstractionNumber"></asp:Literal></h3>
        </div>

        <div class="clear" style="height:70px; clear:both;"></div>
        <!-- PQRS Panel -->
        <asp:Panel runat="server" Visible="true" ID="PQRSPanel" CssClass="clear">
            <style>
                    
                .PQRSOptions {
                    padding-left:40px;
                }

                .tip-dialog-container {
                    display: none;
                }

                .dialog-inner {
                    height: 500px;
                    overflow-y: scroll;
                }

                .tip-dialog-container table {
                    width: 100%;
                    margin-bottom: 20px;
                }

                    .tip-dialog-container table tr {
                        border-bottom: #666;
                    }

                        .tip-dialog-container table tr td {
                            border-bottom: 1px solid #aaa;
                            padding: 5px 3px;
                        }

                            .tip-dialog-container table tr td:first-child {
                                width:40%;
                            }
                .indent {
                    padding-left: 40px;
                }

            </style>
            <table>
                <tr>
                    <th colspan="3" style="width:910px;"><p>PQRS Submission - Cataract Measure Group information</p></th>
                </tr>
                <%-- Question 1 --%>
                <tr class="table_body">
                    <td style="width:70%;">
                        <strong>1. Was patient 18 years (or older) at time of visit?</strong>
                        <asp:Label ID="LabelPQRSAge" runat="server" ForeColor="Red" Visible="false" Text="<br />Please Complete Age" />
                    </td>
                    <td><asp:RadioButtonList runat="server" ID="Age" RepeatDirection="Horizontal" RepeatLayout="Flow">
                            <asp:ListItem Value="1" Text=" Yes &nbsp;" />
                            <asp:ListItem Value="0" Text=" No" />
                        </asp:RadioButtonList></td>
                </tr>
                <%-- Question 2 --%>
                <tr class="table_body_bg" id="PQRSRow2">
                    <td>
                        <strong>2. Date of Procedure</strong>
                        <asp:Label ID="LabelPQRSDOProcedureMonth" runat="server" ForeColor="Red" Visible="false" Text="<br />Please Complete Month"></asp:Label>
                        <asp:Label ID="LabelPQRSDOProcedureYear" runat="server" ForeColor="Red" Visible="false" Text="<br />Please Complete Year"></asp:Label>
                        
                    </td>
                    <td>
                        <asp:DropDownList ID='DropDownListPQRSVisitMonth' DataValueField='MonthID' DataTextField='MonthName' runat='server' />
                        <asp:DropDownList ID='DropDownListPQRSVisitYear' DataValueField='YearID' DataTextField='YearName' runat='server' />
                    </td>
                </tr>
                <%-- Question 3 --%>
                <tr class="table_body" id="PQRSRow3">
                    <td>
                        <strong>3. Is the patient a Medicare FFS patient?</strong>
                        <asp:Label ID="LabelPQRSMedicare" runat="server" ForeColor="Red" Visible="false" Text="<br />Please Complete"></asp:Label>
                        
                    </td>
                    <td>
                        <asp:RadioButtonList runat="server" ID="PQRSMedicare" RepeatDirection="Horizontal" RepeatLayout="Flow">
                            <asp:ListItem Value="1" Text=" Yes &nbsp;" />
                            <asp:ListItem Value="0" Text=" No" />
                        </asp:RadioButtonList>
                    </td>
                </tr>
                <%-- Question 4 --%>
                <tr class="table_body_bg"id="PQRSRow4">
                    <td>
                        <strong>4. Did the patient have one of the noted procedure codes indicating cataract surgery WITHOUT Modifier 55</strong> (postoperative management only) <strong>OR Modifier 56</strong> (preoperative management only)?&nbsp;<a href="#"><img id="PQRSQ4" src="../../common/images/tip.gif" alt="Tip" /></a>
                        <asp:Label ID="LabelPQRSProcedure" runat="server" ForeColor="Red" Visible="false" Text="<br />Please Complete"></asp:Label>
                        
                    </td>
                    <td>
                        <asp:RadioButtonList runat="server" ID="PQRSProcedure" RepeatDirection="Horizontal" RepeatLayout="Flow">
                            <asp:ListItem Value="1" Text=" Yes &nbsp;" />
                            <asp:ListItem Value="0" Text=" No" />
                        </asp:RadioButtonList>
                    </td>
                </tr>
                <%-- Question 5 --%>
                <tr class="table_body"id="PQRSRow5">
                    <td>
                        <!-- (Q130-1) -->
                        <strong>5. Was a list of current medications documented, updated, or reviewed on the date of encounter?</strong> See the tip for specific requirements.&nbsp;<a href="#"><img id="PQRSQ5" src="../../common/images/tip.gif" alt="Tip" /></a>
                        <asp:Label ID="LabelPQRSMedicationsList" runat="server" ForeColor="Red" Visible="false" Text="<br />Please Complete"></asp:Label>
                        
                    </td>
                    <td>
                        <asp:RadioButtonList runat="server" ID="PQRSMedicationsList" RepeatDirection="Horizontal" RepeatLayout="Flow">
                            <asp:ListItem Value="1" Text=" Yes &nbsp;" />
                            <asp:ListItem Value="0" Text=" No" />
                        </asp:RadioButtonList>
                    </td>
                </tr>
                <%-- Question 6 --%>
                <tr class="table_body_bg" id="PQRSRow6">
                    <td>
                        <strong>6. If a list was not documented, was the patient eligible?</strong>
                        <asp:Label ID="LabelPQRSIfNoMedList" runat="server" ForeColor="Red" Visible="false" Text="<br />Please Complete" />
                        
                    </td>
                    <td>
                        <asp:RadioButtonList runat="server" ID="PQRSIfNoMedList" RepeatDirection="Horizontal" RepeatLayout="Flow">
                            <asp:ListItem Value="1" Text=" Yes &nbsp;" />
                            <asp:ListItem Value="0" Text=" No" />
                        </asp:RadioButtonList>
                    </td>
                </tr>
                <%-- Question 7 --%>
                <tr class="table_body"id="PQRSRow7">
                    <td>
                        <strong>7. Did the patient have any of the listed comorbid conditions?</strong>&nbsp;<a href="#"><img id="PQRSQ7" src="../../common/images/tip.gif" alt="Tip" /></a>
                        <asp:Label ID="LabelPQRSConmorbid" runat="server" ForeColor="Red" Visible="false" Text="<br />Please Complete"></asp:Label>
                    </td>
                    <td>
                        <asp:RadioButtonList runat="server" ID="PQRSComorbid" RepeatDirection="Horizontal" RepeatLayout="Flow">
                            <asp:ListItem Value="1" Text=" Yes &nbsp;" />
                            <asp:ListItem Value="0" Text=" No" />
                        </asp:RadioButtonList>
                    </td>
                </tr>
                <%-- Question 8 --%>
                <tr class="table_body_bg"id="PQRSRow8">
                    <td class="indent">
                        <strong>8. Did the patient have a best-corrected visual acuity of 20/40 or better (distance or near) achieved within 90 days following cataract surgery?</strong>
                        <asp:Label ID="LabelPQRSVisualAcuity" runat="server" ForeColor="Red" Visible="false" Text="<br />Please Complete"></asp:Label>
                        
                    </td>
                    <td>
                        <asp:RadioButtonList runat="server" ID="PQRSVisualAcuity" RepeatDirection="Horizontal" RepeatLayout="Flow">
                            <asp:ListItem Value="1" Text=" Yes &nbsp;" />
                            <asp:ListItem Value="0" Text=" No" />
                        </asp:RadioButtonList>
                    </td>
                </tr>
                <%-- Question 9 --%>
                <tr class="table_body" id="PQRSRow9">
                    <td>
                        <strong>9. Does the patient have any of the listed comorbid conditions?</strong>&nbsp;<a href="#"><img id="PQRSQ9" src="../../common/images/tip.gif" alt="Tip" /></a>
                        <asp:Label ID="LabelPQRSConmorbid2" runat="server" ForeColor="Red" Visible="false" Text="<br />Please Complete"></asp:Label>
                        
                    </td>
                    <td>
                        <asp:RadioButtonList runat="server" ID="PQRSComorbid2" RepeatDirection="Horizontal" RepeatLayout="Flow">
                            <asp:ListItem Value="1" Text=" Yes &nbsp;" />
                            <asp:ListItem Value="0" Text=" No" />
                        </asp:RadioButtonList>
                    </td>
                </tr>
                <%-- Question 10 --%>
                <tr class="table_body_bg" id="PQRSRow10">
                    <td>
                        <strong>10. Did the patient have any of a specified list of surgical procedures in the 30 days following cataract surgery which would indicate the occurrence of any of the following major complications: retained nuclear fragments, endophthalmitis, dislocated or wrong power IOL, retinal detachment, or wound dehiscence?&nbsp;<a href="#"><img id="PQRSQ10" src="../../common/images/tip.gif" alt="Tip" /></a></strong>
                        <asp:Label ID="LabelPQRSSurgicalProcedure" runat="server" ForeColor="Red" Visible="false" Text="<br />Please Complete"></asp:Label>
                        
                    </td>
                    <td>
                        <asp:RadioButtonList runat="server" ID="PQRSSurgicalProcedure" RepeatDirection="Horizontal" RepeatLayout="Flow">
                            <asp:ListItem Value="1" Text=" Yes &nbsp;" />
                            <asp:ListItem Value="0" Text=" No" />
                        </asp:RadioButtonList>
                    </td>
                </tr>
                <%-- Question 11 --%>
                <tr class="table_body"id="PQRSRow11">
                    <td>
                        <!-- (Q226-1:) -->
                        <strong>11. Was the patient screened for tobacco use on the Visit Date or within 24 months prior to the Visit Date?</strong>
                        <asp:Label ID="LabelPQRSTabaccoScreened" runat="server" ForeColor="Red" Visible="false" Text="<br />Please Complete"></asp:Label>
                        
                    </td>
                    <td>
                        <asp:RadioButtonList runat="server" ID="PQRSTabaccoScreened" RepeatDirection="Horizontal" RepeatLayout="Flow">
                            <asp:ListItem Value="1" Text=" Yes &nbsp;" />
                            <asp:ListItem Value="0" Text=" No" />
                        </asp:RadioButtonList>
                    </td>
                </tr>
                <%-- Question 12 --%>
                <tr class="table_body_bg" id="PQRSRow12">
                    <td>
                        <!-- (Q226-2) -->
                        <strong>12. If tobacco screening not performed, was this due to a documented medical reason?</strong>&nbsp;<a href="#"><img id="PQRSQ12" src="../../common/images/tip.gif" alt="Tip" /></a>
                        <asp:Label ID="LabelPQRSIfNotTabaccoScreened" runat="server" ForeColor="Red" Visible="false" Text="<br />Please Complete"></asp:Label>
                        
                    </td>
                    <td>
                        <asp:RadioButtonList runat="server" ID="PQRSIfNotTabaccoScreened" RepeatDirection="Horizontal" RepeatLayout="Flow">
                            <asp:ListItem Value="1" Text=" Yes &nbsp;" />
                            <asp:ListItem Value="0" Text=" No" />
                        </asp:RadioButtonList>
                    </td>
                </tr>
                <%-- Question 13 --%>
                <tr class="table_body" id="PQRSRow13">
                    <td>
                        <!-- (Q226-3) -->
                        <strong>13. Was the patient identified as a current tobacco user?</strong>
                        <asp:Label ID="LabelPQRSTabaccoUser" runat="server" ForeColor="Red" Visible="false" Text="<br />Please Complete"></asp:Label>
                        
                    </td>
                    <td>
                        <asp:RadioButtonList runat="server" ID="PQRSTabaccoUser" RepeatDirection="Horizontal" RepeatLayout="Flow">
                            <asp:ListItem Value="1" Text=" Yes &nbsp;" />
                            <asp:ListItem Value="0" Text=" No" />
                        </asp:RadioButtonList>
                    </td>
                </tr>
                <%-- Question 14 --%>
                <tr class="table_body_bg" id="PQRSRow14">
                    <td>
                        <!-- (Q226-4) -->
                        <strong>14. If the patient was identified as a tobacco user, did the patient receive cessation counseling intervention?</strong>&nbsp;<a href="#"><img id="PQRSQ14" src="../../common/images/tip.gif" alt="Tip" /></a>
                        <asp:Label ID="LabelPQRSTabaccoCessation" runat="server" ForeColor="Red" Visible="false" Text="<br />Please Complete"></asp:Label>
                        
                    </td>
                    <td>
                        <asp:RadioButtonList runat="server" ID="PQRSTabaccoCessation" RepeatDirection="Horizontal" RepeatLayout="Flow">
                            <asp:ListItem Value="1" Text=" Yes &nbsp;" />
                            <asp:ListItem Value="0" Text=" No" />
                        </asp:RadioButtonList>
                    </td>
                </tr>
                <%-- Question 15 --%>
                <tr class="table_body" id="PQRSRow15">
                    <td>
                        <strong>15. Did the patient complete a pre-operative and post-operative visual function survey?</strong>&nbsp;<a href="#"><img id="PQRSQ15" src="../../common/images/tip.gif" alt="Tip" /></a>
                        <asp:Label ID="LabelPQRSSurveyDone" runat="server" ForeColor="Red" Visible="false" Text="<br />Please Complete"></asp:Label>
                        <ul class="PQRSOptions">
                            <li>
                                PQRSPRO would be glad to serve as the third party vendor, as indicated by CMS,  for the VF (measure 303) and CAHPS (measure 304) survey components.
			                    We provide automated surveys to be completed easily and anonymously by your patients. If the patient is more 
			                    comfortable with a paper survey they may complete it on paper and we will enter it on line for them.
			                    Contact us at <a href="mailto:PQRSSupport@healthmonix.com">PQRSSupport@healthmonix.com</a> or 610.590.2229 ext 13 for more information.
			                </li>
                        </ul>
                    </td>
                    <td>
                        <asp:RadioButtonList runat="server" ID="PQRS303SurveyDone" RepeatDirection="Horizontal" RepeatLayout="Flow">
                            <asp:ListItem Value="1" Text=" Yes &nbsp;" />
                            <asp:ListItem Value="0" Text=" No" />
                        </asp:RadioButtonList>
                    </td>
                </tr>
                <%-- Question 16 --%>
                <tr class="table_body_bg" id="PQRSRow16">
                    <td>
                        <strong>16. Did the patient have improvement in visual function achieved within 90 days following the cataract surgery, based on completing the pre-operative and post-operative visual function survey?</strong>
                        <asp:Label ID="LabelPQRSVisualImprovement" runat="server" ForeColor="Red" Visible="false" Text="<br />Please Complete" />
                    </td>
                    <td>
                        <asp:RadioButtonList runat="server" ID="PQRSImprovement" RepeatDirection="Horizontal" RepeatLayout="Flow">
                            <asp:ListItem Value="1" Text=" Yes &nbsp;" />
                            <asp:ListItem Value="0" Text=" No" />
                        </asp:RadioButtonList>
                    </td>
                </tr>
                <%-- Question 17 --%>
                <tr class="table_body" id="PQRSRow17">
                    <!-- Q304-1 -->
                    <td>
                        <strong>17. Did the patient complete a Consumer Assessment of Healthcare Providers and Systems Surgical Care Survey?</strong>&nbsp;<a href="#"><img id="PQRSQ17" src="../../common/images/tip.gif" alt="Tip" /></a>
                        <asp:Label ID="LabelPQRSAssessmentComplete" runat="server" ForeColor="Red" Visible="false" Text="<br />Please Complete" />
                        <ul class="PQRSOptions">
                            <li>
                                PQRSPRO would be glad to serve as the third party vendor, as indicated by CMS,  for the measures 303 and 304 survey components.
			                    We provide automated surveys to be completed easily and anonymously by your patients. If the patient is more 
			                    comfortable with a paper survey they may complete it on paper and we will enter it on line for them.
			                    Contact us at <a href="mailto:PQRSSupport@healthmonix.com">PQRSSupport@healthmonix.com</a> or 610.590.2229 ext 13 for more information.
			                </li>
                        </ul>
                    </td>
                    <td>
                        <asp:RadioButtonList runat="server" ID="PQRS304SurveyDone" RepeatDirection="Horizontal" RepeatLayout="Flow">
                            <asp:ListItem Value="1" Text=" Yes &nbsp;" />
                            <asp:ListItem Value="0" Text=" No" />
                        </asp:RadioButtonList>
                    </td>
                </tr>
                <%-- Question 18 --%>
                <tr class="table_body_bg" id="PQRSRow18">
                    <!-- Q304-2 -->
                    <td>
                        <strong>18. Was the patient was satisfied with their care within 90 days following the cataract surgery?</strong>
                        <asp:Label ID="LabelPQRSPatientSatisfied" runat="server" ForeColor="Red" Visible="false" Text="<br />Please Complete"></asp:Label>
                        
                    </td>
                    <td>
                        <asp:RadioButtonList runat="server" ID="PQRSSatisfied" RepeatDirection="Horizontal" RepeatLayout="Flow">
                            <asp:ListItem Value="1" Text=" Yes &nbsp;" />
                            <asp:ListItem Value="0" Text=" No" />
                        </asp:RadioButtonList>
                    </td>
                </tr>
                <%-- Question 19 --%>
                <tr class="table_body"id="PQRSRow19">
                    <!-- Q304-1 -->
                    <td>
                        <strong>19. Was there a <em>preoperative</em> posterior capsule rupture?</strong>
                        <asp:Label ID="LabelPQRSPreopRupture" runat="server" ForeColor="Red" Visible="false" Text="<br />Please Complete"></asp:Label>
                        
                    </td>
                    <td>
                        <asp:RadioButtonList runat="server" ID="PQRSPreopRupture" RepeatDirection="Horizontal" RepeatLayout="Flow">
                            <asp:ListItem Value="1" Text=" Yes &nbsp;" />
                            <asp:ListItem Value="0" Text=" No" />
                        </asp:RadioButtonList>
                    </td>
                </tr>
                <%-- Question 20 --%>
                <tr class="table_body_bg" id="PQRSRow20">
                    <td>
                        <strong>20. Was there an unplanned rupture of the posterior capsule requiring vitrectomy?</strong>&nbsp;<a href="#"><img id="PQRSQ20" src="../../common/images/tip.gif" alt="Tip" /></a>
                        <asp:Label ID="LabelPQRSUnplannedRupture" runat="server" ForeColor="Red" Visible="false" Text="<br />Please Complete"></asp:Label> 
                    </td>
                    <td>
                        <asp:RadioButtonList runat="server" ID="PQRSUnplannedRupture" RepeatDirection="Horizontal" RepeatLayout="Flow">
                            <asp:ListItem Value="1" Text=" Yes &nbsp;" />
                            <asp:ListItem Value="0" Text=" No" />
                        </asp:RadioButtonList>
                    </td>
                </tr>
                <%-- Question 21 --%>
                <tr class="table_body" id="PQRSRow21">
                    <td>
                        <strong>21. Did the patient have any of the listed significant ocular (comorbid) conditions?</strong>
                        <p><a href="#" class="tip-dialog">View codes</a></p>
                        <div class="tip-dialog-container">
                            <div class="dialog-inner">
                                <table>
	                                <tr>
		                                <th>Significant Ocular Condition</th>
		                                <th>Corresponding ICD-9-CM Codes<br /></th>
	                                <tr>
		                                <td>Acute and Subacute lridocyclitis</td>
		                                <td>364.00, 364.01, 364.02, 364.03, 364.04, 364.05</td>
	                                </tr>
	                                <tr>
		                                <td>Amblyopia</td>
		                                <td>368.01, 368.02, 368.03</td>
	                                </tr>
	                                <tr>
		                                <td>Burn Confined to Eye and Adnexa</td>
		                                <td>940,0, 940.1, 940.2,940.3, 940.4, 940.5,940.9</td>
	                                </tr>
	                                <tr>
		                                <td>Cataract Secondary to Ocular Disorders</td>
		                                <td>366.32, 366.33</td>
	                                </tr>
	                                <tr>
		                                <td>Central Corneal Ulcer</td>
		                                <td>370.03</td>
	                                </tr>
	                                <tr>
		                                <td>Certain Types of lridocyclitis</td>
		                                <td>364.21, 364.22, 364.23, 364.24, 364.3</td>
	                                </tr>
	                                <tr>
		                                <td>Choroidal Degenerations</td>
		                                <td>363.43</td>
	                                </tr>
	                                <tr>
		                                <td>Choroidal Detachment</td>
		                                <td>363.72</td>
	                                </tr>
	                                <tr>
		                                <td>Choroidal Hemorrhage and Rupture</td>
		                                <td>363.61, 363.62, 363.63</td>
	                                </tr>
	                                <tr>
		                                <td>Chorioretinal Scars</td>
		                                <td>363.30, 363.31, 363.32, 363.33, 363.35</td>
	                                </tr>
	                                <tr>
		                                <td>Chronic Iridocyclitis</td>
		                                <td>364.10, 364.11</td>
	                                </tr>
	                                <tr>
		                                <td>Cloudy Cornea</td>
		                                <td>371.01, 371.02, 371.03, 371.04</td>
	                                </tr>
	                                <tr>
		                                <td>Corneal Opacity and Other Disorders of Cornea</td>
		                                <td>371.00, 371.03, 371.04</td>
	                                </tr>
	                                <tr>
		                                <td>Corneal Edema</td>
		                                <td>371.20, 371.21, 371.22, 371.23, 371.43, 371.44</td>
	                                </tr>
	                                <tr>
		                                <td>Degeneration of Macula and Posterior Pole</td>
		                                <td>362.50, 362.51, 362.52, 362.53, 362.54, 362.55, 362.56, 362.57</td>
	                                </tr>
	                                <tr>
		                                <td>Degenerative Disorders of Globe</td>
		                                <td>360.20, 360.21, 360.23, 360.24, 360.29</td>
	                                </tr>
	                                <tr>
		                                <td>Diabetic Macular Edema</td>
		                                <td>362.07</td>
	                                </tr>
	                                <tr>
		                                <td>Diabetic Retinopathy</td>
		                                <td>362.01, 362.02, 362.03, 362.04, 362.05, 362.06</td>
	                                </tr>
	                                <tr>
		                                <td>Disorders of Optic Chiasm</td>
		                                <td>377.51, 377.52, 377.53, 377.54</td>
	                                </tr>
	                                <tr>
		                                <td>Disorders of Visual Cortex</td>
		                                <td>377.75</td>
	                                </tr>
	                                <tr>
		                                <td>Disseminated Chorioretinitis and Disseminated Retinochoroiditis</td>
		                                <td>363.10, 363.11, 363.12, 363.13, 363.14, 363.15</td>
	                                </tr>
	                                <tr>
		                                <td>Focal Chorioretinitis and Focal Retinochoroiditis</td>
		                                <td>363.00, 363.01, 363.03, 363.04, 363.05, 363.06, 363.07, 363.08</td>
	                                </tr>
	                                <tr>
		                                <td>Glaucoma</td>
		                                <td>365.10, 365.11, 365.12, 365.13, 365.14, 365.15, 365.20, 365.21, 365.22, 365.23, 365.24, 365.31,365.32, 365.51, 365.52, 365.59, 365.60,365.61, 365.62, 365.63, 365.64, 365.65, 365.81, 365.82, 365.83, 365.89</td>
	                                </tr>
	                                <tr>
		                                <td>Glaucoma Associated with Congenital Anomalies, Dystrophies, and Systemic Syndromes</td>
		                                <td>365.41, 365.42, 365.43, 365.44, 365.60, 365.61, 365.62, 365.63, 365.9, 365.64, 365.65, 365.81, 365.82, 365.83, 365.89,</td>
	                                </tr>
	                                <tr>
		                                <td>Hereditary Choroidal Dystrophies</td>
		                                <td>363.50, 363.51, 363.52, 363.53, 363.54, 363.55, 363.56, 363.57</td>
	                                </tr>
	                                <tr>
		                                <td>Hereditary Corneal Dystrophies</td>
		                                <td>371.50, 371.51, 371.52, 371.53, 371.54, 371.55, 371.56, 371.57, 371.58</td>
	                                </tr>
	                                <tr>
		                                <td>Hereditary Retinal Dystrophies</td>
		                                <td>362.70, 362.71, 362.72, 362.73, 362.74, 362.75, 362.76</td>
	                                </tr>
	                                <tr>
		                                <td>Injury to Optic Nerve and Pathways</td>
		                                <td>950.0, 950.1, 950.2, 950.3, 950.9</td>
	                                </tr>
	                                <tr>
		                                <td>Moderate or Severe Impairment, Better Eye, Profound Impairment Lesser Eye</td>
		                                <td>369.10, 369.17, 369.11, 369.18, 369.12, 369.13, 369.14, 369.15, 369.16,</td>
	                                </tr>
	                                <tr>
		                                <td>Nystagmus and Other Irregular Eye Movements</td>
		                                <td>379.51</td>
	                                </tr>
	                                <tr>
		                                <td>Open Wound of Eyeball</td>
		                                <td>871.0, 871.1, 871.9, 921.3, 871.2, 871.3, 871.4, 871.5, 871.6, 871.7,</td>
	                                </tr>
	                                <tr>
		                                <td>Optic Atrophy</td>
		                                <td>377.10, 377.11, 377.12, 377.13, 377.14, 377.15, 377.16</td>
	                                </tr>
	                                <tr>
		                                <td>Optic Neuritis</td>
		                                <td>377.30, 377.31, 377.32, 377.33, 377.34, 377.39</td>
	                                </tr>
	                                <tr>
		                                <td>Other Background Retinopathy and Retinal Vascular Changes</td>
		                                <td>362.12, 362.16, 362.18</td>
	                                </tr>
	                                <tr>
		                                <td>Other Corneal Deformities</td>
		                                <td>371.70, 371.71, 371.72, 371.73</td>
	                                </tr>
	                                <tr>
		                                <td>Other Disorders of Optic Nerve</td>
		                                <td>377.41</td>
	                                </tr>
	                                <tr>
		                                <td>Other Disorders of Sclera</td>
		                                <td>379.11, 379.12</td>
	                                </tr>
	                                <tr>
		                                <td>Other Endophthalmitis</td>
		                                <td>360.11, 360.12, 360.13, 360.14, 360.19</td>
	                                </tr>
	                                <tr>
		                                <td>Other Proliferative Retinopathy</td>
		                                <td>362.20, 362,27 362.21, 362.22, 362.23, 362.24, 362.25, 362.26,</td>
	                                </tr>
	                                <tr>
		                                <td>Other Retinal Disorders</td>
		                                <td>362.81, 362.82, 362.83, 362.84, 362.85, 362.89</td>
	                                </tr>
	                                <tr>
		                                <td>Other and Unspecified Forms of Chorioretinitis and Retinochoroiditis</td>
		                                <td>363.20, 363.21, 363.22</td>
	                                </tr>
	                                <tr>
		                                <td>Pathologic Myopia</td>
		                                <td>360.20, 360.21</td>
	                                </tr>
	                                <tr>
		                                <td>Prior Penetrating Keratoplasty</td>
		                                <td>371.60, 371.61, 371.62</td>
	                                </tr>
	                                <tr>
		                                <td>Profound Impairment, Both Eyes</td>
		                                <td>369.00, 369.07, 369.01, 369.08 369.02, 369.03, 369.04, 369.05, 369.06,</td>
	                                </tr>
	                                <tr>
		                                <td>Purulent Endophthalmitis</td>
		                                <td>360.00, 360.01, 360.02, 360.03, 360.04</td>
	                                </tr>
	                                <tr>
		                                <td>Retinal Detachment with Retinal Defect</td>
		                                <td>361.00, 361.07 361.01, 361.02, 361.03, 361.04, 361.05, 361.06,</td>
	                                </tr>
	                                <tr>
		                                <td>Retinal Vascular Occlusion</td>
		                                <td>362.31, 362.32, 362.35, 362.36</td>
	                                </tr>
	                                <tr>
		                                <td>Scleritis and Episcleritis</td>
		                                <td>379.04, 379.05, 379.06, 379.07, 379.09</td>
	                                </tr>
	                                <tr>
		                                <td>Separation of Retinal Layers</td>
		                                <td>362.41, 362.42, 362.43</td>
	                                </tr>
	                                <tr>
		                                <td>Uveitis</td>
		                                <td>360.11, 360.12</td>
	                                </tr>
	                                <tr>
		                                <td>Visual Field Defects</td>
		                                <td>368.41</td>
	                                </tr>
                                </table>

                                <table>
	                                <tr>
		                                <th>Significant Ocular Condition</th>
		                                <th>Corresponding ICD-10-CM Codes<br /></th>
	                                <tr>
	                                    <td>
                                            Acute and Subacute Iridocyclitis</td>
                                        <td>
                                            H20.00, H20.011, H20.012, H20.013, H20.019, H20.021, H20.022, H20.023, H20.029, 
                                            H20.031, H20.032, H20.033, H20.039, H20.041, H20.042, H20.043, H20.049, H20.051, 
                                            H20.052, H20.053, H20.059</td>
                                        <tr>
                                            <td>
                                                Amblyopia</td>
                                            <td>
                                                H53.011, H53.012, H53.013, H53.019, H53.021, H53.022, H53.023, H53.029, H53.031, 
                                                H53.032, H53.033, H53.039</td>
                                        </tr>
                                        <tr>
                                            <td>
                                                Burn Confined to Eye and Adnexa</td>
                                            <td>
                                                T26.00XA, T26.01XA, T26.02XA, T26.10XA, T26.11XA, T26.12XA, T26.20XA, T26.21XA, 
                                                T26.22XA, T26.30XA, T26.31XA, T26.32XA, T26.40XA, T26.41XA, T26.42M, T26.50M, 
                                                T26.51XA, T26.52XA, T26.60XA, T26.61XA, T26.62M, T26.70XA, T26.71XA, T26.72XA, 
                                                T26.80XA, T26.81XA, T26.82XA, T26.90XA, T26.91XA, T26.92XA</td>
                                        </tr>
                                        <tr>
                                            <td>
                                                Cataract Secondary to Ocular Disorders</td>
                                            <td>
                                                H26.211, H26.212, H26.213, H26.219, H26.221, H26.222, H26.223, H26.229</td>
                                        </tr>
                                        <tr>
                                            <td>
                                                Central Corneal Ulcer</td>
                                            <td>
                                                H16.011, H16.012, H16.013, H16.019</td>
                                        </tr>
                                        <tr>
                                            <td>
                                                Certain Types of Iridocyclitis</td>
                                            <td>
                                                H20.20, H20.21, H20.22, H20.23, H20.811, H20.812, H20.813, H20.819, H20.821, 
                                                H20.822, H20.823, H20.829, H20.9, H40.40X0</td>
                                        </tr>
                                        <tr>
                                            <td>
                                                Choroidal Degenerations</td>
                                            <td>
                                                H35.33</td>
                                        </tr>
                                        <tr>
                                            <td>
                                                Choroidal Detachment</td>
                                            <td>
                                                H31.411, H31.412, H31.413, H31.419</td>
                                        </tr>
                                        <tr>
                                            <td>
                                                Choroidal Hemorrhage and Rupture</td>
                                            <td>
                                                H31.301, H31.302, H31.303, H31.309, H31.311, H31.312, H31.313, H31.319, H31.321, 
                                                H31.322, H31.323, H31.329</td>
                                        </tr>
                                        <tr>
                                            <td>
                                                Chorioretinal Scars</td>
                                            <td>
                                                H31.001, H31.002, H31.003, H31.009, H31.011, H31.012, H31.013, H31.019, H31.021, 
                                                H31.022, H31.023, H31.029, H31.091, H31.092, H31.093, H31.099</td>
                                        </tr>
                                        <tr>
                                            <td>
                                                Chronic lridocyclitis</td>
                                            <td>
                                                A18.54, H20.10, H20.11, H20.12, H20.13, H20.9</td>
                                        </tr>
                                        <tr>
                                            <td>
                                                Cloudy Cornea</td>
                                            <td>
                                                H17.00, H17.01, H17.02, H17.03, H17.10, H17.11, H17.12, H17.13, H17.811, 
                                                H17.812, H17.813, H17.819, H17.821, H17.822, H17.823, H17.829</td>
                                        </tr>
                                        <tr>
                                            <td>
                                                Corneal Opacity and Other Disorders of Cornea</td>
                                            <td>
                                                H17.00, H17.01, H17.02, H17.03, H17.10, H17.11, H17.12, H17.13, H17.89, H17.9</td>
                                        </tr>
                                        <tr>
                                            <td>
                                                Corneal Edema</td>
                                            <td>
                                                H18.10, H18.11, H18.12, H18.13, H18.20, H18.221, H18.222, H18.223, H18.229, 
                                                H18.231, H18.232, H18.233, H18.239, H18.421, H18.422, H18.423, H18.429, H18.43</td>
                                        </tr>
                                        <tr>
                                            <td>
                                                Degeneration of Macula and Posterior Pole</td>
                                            <td>
                                                H35.30, H35.31, H35.32, H35.341, H35.342, H35.343, H35.349, H35.351, H35.352, 
                                                H35.353, H35.359, H35.361, H35.362, H35.363, H35.369, H35.371, H35.372, H35.373, 
                                                H35.379, H35.381, H35.382, H35.383, H35.389</td>
                                        </tr>
                                        <tr>
                                            <td>
                                                Degenerative Disorders of Globe</td>
                                            <td>
                                                H44.20, H44.21, H44.22, H44.23, H44.311, H44.312, H44.313, H44.319, H44.321, 
                                                H44.322, H44.323, H44.329, H44.391, H44.392, H44.393, H44.399</td>
                                        </tr>
                                        <tr>
                                            <td>
                                                Diabetic Macular Edema</td>
                                            <td>
                                                E08.311, E08.321, E08.331, E08.341, E08.351, E09.311, E09.321, E09.331, E09.341, 
                                                E09.351, E10.311, E10.321, E10.331, E10.341, E10.351, E11.311, E11.321, E11.331, 
                                                E11.341, E11.351, E13.311, E13.321, E13.331, E13.341, E13.351</td>
                                        </tr>
                                        <tr>
                                            <td>
                                                Diabetic Retinopathy</td>
                                            <td>
                                                E08.311, E08.319, E08.321, E08.329, E08.331, E08.339, E08.341, E08.349, E08.351, 
                                                E08.359, E09.311, E09.319, E09.321, E09.329, E09.331, E09.339, E09.341, E09.349, 
                                                E09.351, E09.359, E10.311, E10.319, E10.321, E10.329, E10.331, E10.339, E10.341, 
                                                E10.349, E10.351, E10.359, E11.311, E11.319, E11.321, E11.329, E11.331, E11.339, 
                                                E11.341, E11.349, E11.351, E11.359, E13.311, El 3.319, E13.321, E13.329, 
                                                E13.331, E13.339, E13.341, E13.349, E13.351, E13.359</td>
                                        </tr>
                                        <tr>
                                            <td>
                                                Disorders of Optic Chiasm</td>
                                            <td>
                                                H47.41, H47.42, H47.43, H47.49</td>
                                        </tr>
                                        <tr>
                                            <td>
                                                Disorders of Visual Cortex</td>
                                            <td>
                                                H47.611, H47.612, H47.619</td>
                                        </tr>
                                        <tr>
                                            <td>
                                                Disseminated Chorioretinitis and Disseminated Retinochoroiditis</td>
                                            <td>
                                                A18.53, H30.101, H30.102, H30.103, H30.109, H30.111, H30.112, H30.113, H30.119, 
                                                H30.121, H30.122, H30.123, H30.129, H30.131, H30.132, H30.133, H30.139, H30.141, 
                                                H30.142, H30.143, H30.149</td>
                                        </tr>
                                        <tr>
                                            <td>
                                                Focal Chorioretinitis and Focal Retinochoroiditis</td>
                                            <td>
                                                H30.001, H30.002, H30.003, H30.009, H30.011, H30.012, H30.013, H30.019, H30.021, 
                                                H30.022, H30.023, H30.029, H30.031, H30.032, H30.033, H30.039, H30.041, H30.042, 
                                                H30.043, H30.049</td>
                                        </tr>
                                        <tr>
                                            <td>
                                                Glaucoma</td>
                                            <td>
                                                H40.10X0, H40.10X1, H40.10X2, H40.10X3, H40.10X4, H40.11X0, H40.11X1, H40.11X2, 
                                                H40.11X3, H40.11X4, H40.1210, H40.1211, H40.1212, H40.1213, H40.1214, H40.1220, 
                                                H40.1221, H40.1222, H40.1223, H40.1224, H40.1230, H40.1231, H40.1232, H40.1233, 
                                                H40.1234, H40.1290, H40.1291, H40.1292, H40.1293, H40.1294, H40.1310, H40.1311, 
                                                H40.1312, H40.1313, H40.1314, H40.1320, H40.1321, H40.1322, H40.1323, H40.1324, 
                                                H40.1330, H40.1331, H40.1332, H40.1333, H40.1334, H40.1390, H40.1391, H40.1392, 
                                                H40.1393, H40.1394, H40.1410, H40.1411, H40.1412, H40.1413, H40.1414, H40.1420, 
                                                H40.1421, H40.1422, H40.1423, H40.1424, H40.1430, H40.1431, H40.1432, H40.1433, 
                                                H40.1434, H40.1490, H40.1491, H40.1492, H40.1493, H40.1494, H40.151, H40.152, 
                                                H40.153, H40.159, H40.20X0, H40.20X1, H40.20X2, H40.20X3, H40.20X4, H40.211, 
                                                H40.212, H40.213, H40.219, H40.2210, H40.2211, H40.2212, H40.2213, H40.2214, 
                                                H40.2220, H40.2221, H40.2222, H40.2223, H40.2224, H40.2230, H40.2231, H40.2232, 
                                                H40.2233, H40.2234, H40.2290, H40.2291, H40.2292, H40.2293, H40.2294, H40.231, 
                                                H40.232, H40.233, H40.239, H40.241, H40.242, H40.243, H40.249, H40.30X0, 
                                                H40.30X1, H40.30X2, H40.30X3, H40.30X4, H40.31X0, H40.31X1, H40.31X2, H40.31X3, 
                                                H40.31X4, H40.32X0, H40.32X1, H40.32X2, H40.32X3, H40.32X4, H40.33X0, H40.33X1, 
                                                H40.33X2, H40.33X3, H40.33X4, H40.40X0, H40.40X1, H40.40X2, H40.40X3, H40.40X4, 
                                                H40.41X0, H40.41X1, H40.41X2, H40.41X3, H40.41X4, H40.42X0, H40.42X1, H40.42X2, 
                                                H40.42X3, H40.42X4, H40.43X0, H40.43X1, H40.43X2, H40.43X3, H40.43X4, H40.50X0, 
                                                H40.50X1, H40.50X2, H40.50X3, H40.50X4, H40.51X0, H40.51X1, H40.51X2, H40.51X3, 
                                                H40.51X4, H40.52X0, H40.52X1, H40.52X2, H40.52X3, H40.52X4, H40.53X0, H40.53X1, 
                                                H40.53X2, H40.53X3, H40.53X4, H40.60X0, H40.60X1, H40.60X2, H40.60X3, H40.60X4, 
                                                H40.61X0, H40.61X1, H40.61X2, H40.61X3, H40.61X4, H40.62X0, H40.62X1, H40.62X2, 
                                                H40.62X3, H40.62X4, H40.63X0, H40.63X1, H40.63X2, H40.63X3, H40.63X4, H40.811, 
                                                H40.812, H40.813, H40.819, H40.821, H40.822, H40.823, H40.829, H40.831, H40.832, 
                                                H40.833, H40.839, H40.89, Q15.0</td>
                                        </tr>
                                        <tr>
                                            <td>
                                                Glaucoma Associated with Congenital Anomalies, Dystrophies, and Systemic 
                                                Syndromes</td>
                                            <td>
                                                H40.30X0, H40.30X1, H40.30X2, H40.30X3, H40.30X4, H40.31X0, H40.31X1, H40.31X2, 
                                                H40.31X3, H40.31X4, H40.32X0, H40.32X1, H40.32X2, H40.32X3, H40.32X4, H40.33X0, 
                                                H40.33X1, H40.33X2, H40.33X3, H40.33X4, H40.40X0, H40.40X1, H40.40X2, H40.40X3, 
                                                H40.40X4, H40.41X0, H40.41X1, H40.41X2, H40.41X3, H40.41X4, H40.42X0, H40.42X1, 
                                                H40.42X2, H40.42X3, H40.42X4, H40.43X0, H40.43X1, H40.43X2, H40.43X3, H40.43X4, 
                                                H40.50X0, H40.50X1, H40.50X2, H40.50X3, H40.50X4, H40.51X0, H40.51X1, H40.51X2, 
                                                H40.51X3, H40.51X4, H40.52X0, H40.52X1, H40.52X2, H40.52X3, H40.52X4, H40.53X0, 
                                                H40.53X1, H40.53X2, H40.53X3, H40.53X4, H40.811, H40.812, H40.813, H40.819, 
                                                H40.821, H40.822, H40.823, H40.829, H40.831, H40.832, H40.833, H40.839, H40.89, 
                                                H40.9, H42</td>
                                        </tr>
                                        <tr>
                                            <td>
                                                Hereditary Choroidal Dystrophies</td>
                                            <td>
                                                H31.20, H31.21, H31.22, H31.23, H31.29</td>
                                        </tr>
                                        <tr>
                                            <td>
                                                Hereditary Comeal Dystrophies</td>
                                            <td>
                                                H18.50, H18.51, H18.52, H18.53, H18.54, H18.55, H18.59</td>
                                        </tr>
                                        <tr>
                                            <td>
                                                Hereditary Retinal Dystrophies</td>
                                            <td>
                                                H35.50, H35.51, H35.52, H35.53, H35.54, H36</td>
                                        </tr>
                                        <tr>
                                            <td>
                                                Injury to Optic Nerve and Pathways</td>
                                            <td>
                                                SO4.01 1A, SO4.012A, SO4.019A, SO4.02M, SO4.031A, SO4.032A, SO4.039A, SO4.041A, 
                                                SO4.042A, SO4.049A</td>
                                        </tr>
                                        <tr>
                                            <td>
                                                Moderate or Severe Impairment, Better Eye, Profound Impairment, Lesser Eye</td>
                                            <td>
                                                H54.10, H54.11, H54.12</td>
                                        </tr>
                                        <tr>
                                            <td>
                                                Nystagmus and Other Irregular Eye Movements</td>
                                            <td>
                                                H55.01</td>
                                        </tr>
                                        <tr>
                                            <td>
                                                Open Wound of Eyeball</td>
                                            <td>
                                                S05.10XA, S05.11XA, S05.12XA, S05.20XA, S05.21XA, S05.22M, S05.30XA, S05.31XA, 
                                                S05.32XA, S05.50XA, S05.51XA, S05.52M, S05.60XA, S05.61XA, S05.62XA, S05.70XA, 
                                                S05.71XA, S05.72M, S05.8X1A, S05.8X2A, S05.8X9A, S05.90XA, S05.91XA, S05.92XA</td>
                                        </tr>
                                        <tr>
                                            <td>
                                                Optic Atrophy</td>
                                            <td>
                                                H47.20, H47.211, H47.212, H47.213, H47.219, H47.22, H47.231, H47.232, H47.233, 
                                                H47.239, H47.291, H47.292, H47.293, H47.299</td>
                                        </tr>
                                        <tr>
                                            <td>
                                                Optic Neuritis</td>
                                            <td>
                                                H46.00, H46.01, H46.02, H46.03, H46.10, H46.11, H46.12, H46.13, H46.2, H46.3, 
                                                H46.8, H46.9</td>
                                        </tr>
                                        <tr>
                                            <td>
                                                Other Background Retinopathy and Retinal Vascular Changes</td>
                                            <td>
                                                H35.021, H35.022, H35.023, H35.029, H35.051, H35.052, H35.053, H35.059, H35.061, 
                                                H35.062, H35.063, H35.069</td>
                                        </tr>
                                        <tr>
                                            <td>
                                                Other Corneal Deformities</td>
                                            <td>
                                                H18.70, H18.711, H18.712, H18.713, H18.719, H18.721, H18.722, H18.723, H18.729, 
                                                H18.731, H18.732, H18.733, H18.739, H18.791, H18.792, H18.793, H18.799</td>
                                        </tr>
                                        <tr>
                                            <td>
                                                Other Disorders of Optic Nerve</td>
                                            <td>
                                                H47.011, H47.012, H47.013, H47.019</td>
                                        </tr>
                                        <tr>
                                            <td>
                                                Other Disorders of Sclera</td>
                                            <td>
                                                H15.831, H15.832, H15.833, H15.839, H15.841, H15.842, H15.843, H15.849</td>
                                        </tr>
                                        <tr>
                                            <td>
                                                Other Endophthalmitis</td>
                                            <td>
                                                H16.241, H16.242, H16.243, H16.249, H21.331, H21.332, H21.333, H21.339, H33.121, 
                                                H33.122, H33.123, H33.129, H44.111, H44.112, H44.113, H44.119, H44.121, H44.122, 
                                                H44.123, H44.129, H44.131, H44.132, H44.133, H44.139, H44.19</td>
                                        </tr>
                                        <tr>
                                            <td>
                                                Other Proliferative Retinopathy</td>
                                            <td>
                                                H35.101, H35.102, H35.103, H35.109, H35.111, H35.112, H35.113, H35.119, H35.121, 
                                                H35.122, H35.123, H35.129, H35.131, H35.132, H35.133, H35.139, H35.141, H35.142, 
                                                H35.143, H35.149, H35.151, H35.152, H35.153, H35.159, H35.161, H35.162, H35.163, 
                                                H35.169, H35.171, H35.172, H35.173, H35.179</td>
                                        </tr>
                                        <tr>
                                            <td>
                                                Other Retinal Disorders</td>
                                            <td>
                                                H35.60, H35.61, H35.62, H35.63, H35.81, H35.82, H35.89</td>
                                        </tr>
                                        <tr>
                                            <td>
                                                Other and Unspecified Forms of Chorioretinitis and Retinochoroiditis</td>
                                            <td>
                                                H30.20, H30.21, H30.22, H30.23, H30.811, H30.812, H30.813, H30.819, H30.891, 
                                                H30.892, H30.893, H30.899, H30.90, H30.91, H30.92, H30.93</td>
                                        </tr>
                                        <tr>
                                            <td>
                                                Pathologic Myopia</td>
                                            <td>
                                                H44.20, H44.21, H44.22, H44.23, H44.30</td>
                                        </tr>
                                        <tr>
                                            <td>
                                                Prior Penetrating Keratoplasty</td>
                                            <td>
                                                H18.601, H18.602, H18.603, H18.609, H18.611, H18.612, H18.613, H18.619, H18.621, 
                                                H18.622, H18.623, H18.629</td>
                                        </tr>
                                        <tr>
                                            <td>
                                                Profound Impairment, Both Eyes</td>
                                            <td>
                                                H54.0, H54.10</td>
                                        </tr>
                                        <tr>
                                            <td>
                                                Purulent Endophthalmitis</td>
                                            <td>
                                                H44.001, H44.002, H44.003, H44.009, H44.011, H44.012, H44.013, H44.019, H44.021, 
                                                H44.022, H44.023, H44.029</td>
                                        </tr>
                                        <tr>
                                            <td>
                                                Retinal Detachment with Retinal Defect</td>
                                            <td>
                                                H33.001, H33.002, H33.003, H33.009, H33.011, H33.012, H33.013, H33.019, H33.021, 
                                                H33.022, H33.023, H33.029, H33.031, H33.032, H33.033, H33.039, H33.041, H33.042, 
                                                H33.043, H33.049, H33.051, H33.052, H33.053, H33.059, H33.8</td>
                                        </tr>
                                        <tr>
                                            <td>
                                                Retinal Vascular Occlusion</td>
                                            <td>
                                                H34.10, H34.11, H34.12, H34.13, H34.231, H34.232, H34.233, H34.239, H34.811, 
                                                H34.812, H34.813, H34.819, H34.831, H34.832, H34.833, H34.839</td>
                                        </tr>
                                        <tr>
                                            <td>
                                                Scleritis and Episcleritis</td>
                                            <td>
                                                A18.51, H15.021, H15.022, H15.023, H15.029, H15.031, H15.032, H15.033, H15.039, 
                                                H15.041, H15.042, H15.043, H15.049, H15.051, H15.052, H15.053, H15.059, H15.091, 
                                                H15.092, H15.093, H15.099</td>
                                        </tr>
                                        <tr>
                                            <td>
                                                Separation of Retinal Layers</td>
                                            <td>
                                                H35.711, H35.712, H35.713, H35.719, H35.721, H35.722, H35.723, H35.729, H35.731, 
                                                H35.732, H35.733, H35.739</td>
                                        </tr>
                                        <tr>
                                            <td>
                                                Uveitis</td>
                                            <td>
                                                H44.111, H44.112, H44.113, H44.119, H44.131, H44.132, H44.133, H44.139</td>
                                        </tr>
                                        <tr>
                                            <td>
                                                Visual Field Defects</td>
                                            <td>
                                                H53.411,H53.412, H53.413, H53.419</td>
                                        </tr>
                                </table>
                            </div>
                        </div>
                        <asp:Label ID="LabelPQRSOcularConditions" runat="server" ForeColor="Red" Visible="false" Text="<br />Please Complete"></asp:Label>
                    </td>
                    <td>
                        <asp:RadioButtonList runat="server" ID="PQRSOcularConditions" RepeatDirection="Horizontal" RepeatLayout="Flow">
                            <asp:ListItem Value="1" Text=" Yes &nbsp;" />
                            <asp:ListItem Value="0" Text=" No" />
                        </asp:RadioButtonList>
                    </td>
                </tr>
                <%-- Question 22 --%>
                <tr class="table_body_bg" id="PQRSRow22">
                    <td>
                        <strong>22. Did the patient achieve planned refraction within &plusmn;1 D for the eye that underwent cataract surgery, measured at the one month follow up visit?</strong>&nbsp;<a href="#"><img id="PQRSQ22" src="../../common/images/tip.gif" alt="Tip" /></a>
                        <asp:Label ID="LabelPQRSPlannedRefraction" runat="server" ForeColor="Red" Visible="false" Text="<br />Please Complete"></asp:Label>
                    </td>
                    <td>
                        <asp:RadioButtonList runat="server" ID="PQRSPlannedRefraction" RepeatDirection="Horizontal" RepeatLayout="Flow">
                            <asp:ListItem Value="1" Text=" Yes &nbsp;" />
                            <asp:ListItem Value="0" Text=" No" />
                        </asp:RadioButtonList>
                    </td>
                </tr>
            </table>
            <br />
            <script>

                <%-- Question 1 --%>
                function AgeValidation() {
                    if ($('#<%= Age.ClientID %> input:checked').val() == '0') {
                        var myarray = new Array("PQRSRow2", "PQRSRow3", "PQRSRow4", "PQRSRow5", "PQRSRow6", "PQRSRow7", "PQRSRow8", "PQRSRow9", "PQRSRow10", "PQRSRow11", "PQRSRow12", "PQRSRow13", "PQRSRow14", "PQRSRow15", "PQRSRow16", "PQRSRow17", "PQRSRow18", "PQRSRow19", "PQRSRow20", "PQRSRow21", "PQRSRow22");
                        var myarray2 = new Array("#PQRSRow2", "#PQRSRow3", "#PQRSRow4", "#PQRSRow5", "#PQRSRow6", "#PQRSRow7", "#PQRSRow8", "#PQRSRow9", "#PQRSRow10", "#PQRSRow11", "#PQRSRow12", "#PQRSRow13", "#PQRSRow14", "#PQRSRow15", "#PQRSRow16", "#PQRSRow17", "#PQRSRow18", "#PQRSRow19", "#PQRSRow20", "#PQRSRow21", "#PQRSRow22");
                        generate(myarray, myarray2, true);
                    }
                    else {
                        var myarray = new Array("PQRSRow2", "PQRSRow3", "PQRSRow4");
                        var myarray2 = new Array("#PQRSRow2", "#PQRSRow3", "#PQRSRow4");
                        generate(myarray, myarray2, false);
                    }
                }

                <%-- Question 4 --%>
                function PQRSProcedureValidation() {
                    if ($('#<%= PQRSProcedure.ClientID %> input:checked').val() == '0') {
                        var myarray = new Array("PQRSRow5", "PQRSRow6", "PQRSRow7", "PQRSRow8", "PQRSRow9", "PQRSRow10", "PQRSRow11", "PQRSRow12", "PQRSRow13", "PQRSRow14", "PQRSRow15", "PQRSRow16", "PQRSRow17", "PQRSRow18", "PQRSRow19", "PQRSRow20", "PQRSRow21", "PQRSRow22");
                        var myarray2 = new Array("#PQRSRow5", "#PQRSRow6", "#PQRSRow7", "#PQRSRow8", "#PQRSRow9", "#PQRSRow10", "#PQRSRow11", "#PQRSRow12", "#PQRSRow13", "#PQRSRow14", "#PQRSRow15", "#PQRSRow16", "#PQRSRow17", "#PQRSRow18", "#PQRSRow19", "#PQRSRow20", "#PQRSRow21", "#PQRSRow22");
                        generate(myarray, myarray2, true);
                    }
                    else {
                        var myarray = new Array("PQRSRow5", "PQRSRow6", "PQRSRow7", "PQRSRow8", "PQRSRow9", "PQRSRow10", "PQRSRow11", "PQRSRow12", "PQRSRow13", "PQRSRow14", "PQRSRow15", "PQRSRow16", "PQRSRow17", "PQRSRow18", "PQRSRow19", "PQRSRow20", "PQRSRow21", "PQRSRow22");
                        var myarray2 = new Array("#PQRSRow5", "#PQRSRow6", "#PQRSRow7", "#PQRSRow8", "#PQRSRow9", "#PQRSRow10", "#PQRSRow11", "#PQRSRow12", "#PQRSRow13", "#PQRSRow14", "#PQRSRow15", "#PQRSRow16", "#PQRSRow17", "#PQRSRow18", "#PQRSRow19", "#PQRSRow20", "#PQRSRow21", "#PQRSRow22");
                        generate(myarray, myarray2, false);
                    }
                }

                <%-- Question 5 --%>
                function PQRSMedicationsListValidation() {
                    var myarray = new Array("PQRSRow6");
                    var myarray2 = new Array("#PQRSRow6");

                    if ($('#<%= PQRSMedicationsList.ClientID %> input:checked').val() == '1') {
                        generate(myarray, myarray2, true);
                    }
                    else {
                        generate(myarray, myarray2, false);
                    }
                }

                <%-- Question 7 --%>
                function PQRSComorbidValidation() {
                    var myarray = new Array("PQRSRow8");
                    var myarray2 = new Array("#PQRSRow8");

                    if ($('#<%= PQRSComorbid.ClientID %> input:checked').val() == '1') {
                        generate(myarray, myarray2, true);
                    }
                    else {
                        generate(myarray, myarray2, false);
                    }
                }

                <%-- Question 9 --%>
                function PQRSComorbid2Validation() {
                    var myarray = new Array("PQRSRow10");
                    var myarray2 = new Array("#PQRSRow10");

                    if ($('#<%= PQRSComorbid2.ClientID %> input:checked').val() == '1') {
                        generate(myarray, myarray2, true);
                    }
                    else {
                        generate(myarray, myarray2, false);
                    }
                }

                <%-- Question 15 --%>
                function PQRS303SurveyDoneValidation() {
                    var myarray = new Array("PQRSRow16");
                    var myarray2 = new Array("#PQRSRow16");

                    if ($('#<%= PQRS303SurveyDone.ClientID %> input:checked').val() == '0') {
                        generate(myarray, myarray2, true);
                    }
                    else {
                        generate(myarray, myarray2, false);
                    }
                }

                <%-- Question 17 --%>
                function PQRS304SurveyDoneValidation() {
                    var myarray = new Array("PQRSRow18");
                    var myarray2 = new Array("#PQRSRow18");

                    if ($('#<%= PQRS304SurveyDone.ClientID %> input:checked').val() == '0') {
                        generate(myarray, myarray2, true);
                    }
                    else {
                        generate(myarray, myarray2, false);
                    }
                }

                $(document).ready(function () {

                    /* Check that user is inputting correct year */
                    var rightYearArr = new Array("PQRSRow3", "PQRSRow4", "PQRSRow5", "PQRSRow6", "PQRSRow7", "PQRSRow8", "PQRSRow9", "PQRSRow10", "PQRSRow11", "PQRSRow12", "PQRSRow13", "PQRSRow14", "PQRSRow15", "PQRSRow16", "PQRSRow17", "PQRSRow18", "PQRSRow19", "PQRSRow20", "PQRSRow21", "PQRSRow22");
                    var rightYearArr2 = new Array("#PQRSRow3", "#PQRSRow4", "#PQRSRow5", "#PQRSRow6", "#PQRSRow7", "#PQRSRow8", "#PQRSRow9", "#PQRSRow10", "#PQRSRow11", "#PQRSRow12", "#PQRSRow13", "#PQRSRow14", "#PQRSRow15", "#PQRSRow16", "#PQRSRow17", "#PQRSRow18", "#PQRSRow19", "#PQRSRow20", "#PQRSRow21", "#PQRSRow22");
                    $("#<%= DropDownListPQRSVisitYear.ClientID %>").change(function () {
                        var year = $("#<%= DropDownListPQRSVisitYear.ClientID %>").val();

                        if (year != 2016 && year != 0) {
                            generate(rightYearArr, rightYearArr2, true);
                            alert("Date of procedure must be in 2016 to be valid for PQRS Reporting.");
                        } else {
                            generate(rightYearArr, rightYearArr2, false);
                        }
                    });

                    /* Tobacco Screening Questions */
                    var q12Array1 = new Array("PQRSRow12");
                    var q12Array2 = new Array("#PQRSRow12");

                    var q13Array1 = new Array("PQRSRow13");
                    var q13Array2 = new Array("#PQRSRow13");

                    var q14Array1 = new Array("PQRSRow14");
                    var q14Array2 = new Array("#PQRSRow14");

                    function PQRSTabaccoScreenedValidation() {
                        if ($('#<%= PQRSTabaccoScreened.ClientID %> input:checked').val() == "0") {
                            generate(q12Array1, q12Array2, false);
                            generate(q13Array1, q13Array2, true);
                            generate(q14Array1, q14Array2, true);
                        } else if ($('#<%= PQRSTabaccoScreened.ClientID %> input:checked').val() == "1") {
                            generate(q12Array1, q12Array2, true);
                            generate(q13Array1, q13Array2, false);
                        }
                    }

                    function PQRSTabaccoUserdValidation() {
                        if ($('#<%= PQRSTabaccoUser.ClientID %> input:checked').val() == "0") {
                            generate(q14Array1, q14Array2, true);
                        } else if ($('#<%= PQRSTabaccoUser.ClientID %> input:checked').val() == "1") {
                            generate(q14Array1, q14Array2, false);
                        }
                    }
                    
                    $("#<%= PQRSTabaccoScreened.ClientID %>").click(function () {
                        PQRSTabaccoScreenedValidation();
                    });
                    PQRSTabaccoScreenedValidation();

                    $("#<%= PQRSTabaccoUser.ClientID %>").click(function () {
                        PQRSTabaccoUserdValidation()
                    });
                    PQRSTabaccoUserdValidation();


                    var q15Array1 = new Array("PQRSRow15");
                    var q15Array2 = new Array("#PQRSRow15");

                    var q17Array1 = new Array("PQRSRow17");
                    var q17Array2 = new Array("#PQRSRow17");

                    $("#<%= DropDownListPQRSVisitMonth.ClientID %>").change(function () {
                        if (visitDateValidator()) {
                            generate(q15Array1, q15Array2, false);
                            generate(q17Array1, q17Array2, false);
                        } else {
                            generate(q15Array1, q15Array2, true);
                            generate(q17Array1, q17Array2, true);
                        }
                    });
                    $("#<%= DropDownListPQRSVisitYear.ClientID %>").change(function () {
                        if (visitDateValidator()) {
                            generate(q15Array1, q15Array2, false);
                            generate(q17Array1, q17Array2, false);
                        } else {
                            generate(q15Array1, q15Array2, true);
                            generate(q17Array1, q17Array2, true);
                        }
                    });

                    if (visitDateValidator()) {
                        generate(q15Array1, q15Array2, false);
                        generate(q17Array1, q17Array2, false);
                    } else {
                        generate(q15Array1, q15Array2, true);
                        generate(q17Array1, q17Array2, true);
                    }


                    var q20Array1 = new Array("PQRSRow20");
                    var q20Array2 = new Array("#PQRSRow20");

                    $("#<%= PQRSPreopRupture.ClientID %>").click(function () {
                        if ($('#<%= PQRSPreopRupture.ClientID %> input:checked').val() == "0") {
                            generate(q20Array1, q20Array2, false);
                        } else {
                            generate(q20Array1, q20Array2, true);
                        }
                    });

                    if ($('#<%= PQRSPreopRupture.ClientID %> input:checked').val() == "0") {
                        generate(q20Array1, q20Array2, false);
                    } else {
                        generate(q20Array1, q20Array2, true);
                    }


                    var q22Array1 = new Array("PQRSRow22");
                    var q22Array2 = new Array("#PQRSRow22");

                    $("#<%= PQRSOcularConditions.ClientID %>").click(function () {
                        if ($('#<%= PQRSOcularConditions.ClientID %> input:checked').val() == "0") {
                            generate(q22Array1, q22Array2, false);
                        } else {
                            generate(q22Array1, q22Array2, true);
                        }
                    });

                    if ($('#<%= PQRSOcularConditions.ClientID %> input:checked').val() == "0") {
                        generate(q22Array1, q22Array2, false);
                    } else {
                        generate(q22Array1, q22Array2, true);
                    }


                    $("#<%= PQRS304SurveyDone.ClientID %>").click(function () {
                        PQRS304SurveyDoneValidation();
                    });
                    PQRS304SurveyDoneValidation();

                    $("#<%= PQRS303SurveyDone.ClientID %>").click(function () {
                        PQRS303SurveyDoneValidation();
                    });
                    PQRS303SurveyDoneValidation();

                    $("#<%= PQRSComorbid2.ClientID %>").click(function () {
                        PQRSComorbid2Validation();
                    });
                    PQRSComorbid2Validation();

                    $("#<%= PQRSComorbid.ClientID %>").click(function () {
                        PQRSComorbidValidation();
                    });
                    PQRSComorbidValidation();
                    
                    $("#<%= PQRSMedicationsList.ClientID %>").click(function () {
                        PQRSMedicationsListValidation();
                    });
                    PQRSMedicationsListValidation();

                    $("#<%= PQRSProcedure.ClientID %>").click(function () {
                        PQRSProcedureValidation();
                    });
                    PQRSProcedureValidation();

                    $("#<%= Age.ClientID %>").click(function () {
                        AgeValidation();
                    });
                    AgeValidation();

                });

                function visitDateValidator() {
                    var month = $("#<%= DropDownListPQRSVisitMonth.ClientID %>").val();
                    var year = $("#<%= DropDownListPQRSVisitYear.ClientID %>").val();
                    var isValid = false;

                    if ( (month > 0 && month < 10) && year == 2016 ) {
                        isValid = true;
                    } else {
                        isValid = false;
                    }

                    return isValid;
                }

            </script>
            <script>
                $(function () {

                    $('#PQRSQ4').aToolTip({
                        clickIt: true,
                        tipContent: '66840, 66850, 66852, 66920, 66930, 66940, 66983, 66984'
                    });
                    
                    $('#PQRSQ5').aToolTip({
                        clickIt: true,
                        tipContent: 'This list <strong><em>must</em></strong> include ALL prescriptions, over-the-counters, herbals, and vitamin/mineral/dietary (nutritional) supplements, AND <strong><em>must</em></strong> contain the medications’ name, dosages, frequency and route of administration.  Answer &dlquo;Yes&drquo; if the patient is not currently taking any medications.  Documented medication information may be received from the patient, authorized representative(s), caregiver(s) or other available healthcare resources.<br /><strong>Definition:</strong><br /><strong>Route:</strong> Documentation of the way the medication enters the body (some examples include but are not limited to: oral, sublingual, subcutaneous injections, and/or topical)'
                    });
                    
                    $('#PQRSQ6').aToolTip({
                        clickIt: true,
                        tipContent: '<strong>Definition: </strong><br /><strong>Not Eligible</strong> – A patient is not eligible if the following reason exists:<br />&bull; Patient is in an urgent or emergent medical situation where time is of the essence and to delay treatment would jeopardize the patient’s health status.'
                    });

                    $('#PQRSQ7').aToolTip2({
                        clickIt: true,
                        yOffset: -225,
                        tipContent: '<div style="height: 400px; overflow-y:scroll"><table><tr><td><strong>Significant Ocular Condition</strong></td><td><strong>Corresponding ICD-9-CM Codes<br />[for use 1/1/2016 – 9/30/2016]</strong></td></tr><tr><td>Acute and Subacute Iridocyclitis</td><td>364.00, 364.01, 364.02, 364.03, 364.04, 364.05</td></tr><tr><td>Amblyopia</td><td>368.01, 368.02, 368.03</td></tr><tr><td>Burn Confined to Eye and Adnexa</td><td>940.0, 940.1, 940.2, 940.3, 940.4, 940.5, 940.9</td></tr><tr><td>Cataract Secondary to Ocular Disorders</td><td>366.32, 366.33</td></tr><tr><td>Central Corneal Ulcer</td><td>370.03</td></tr><tr><td>Certain Types of Iridocyclitis</td><td>364.21, 364.22, 364.23, 364.24, 364.3</td></tr><tr><td>Choroidal Degenerations</td><td>363.43</td></tr><tr><td>Choroidal Detachment</td><td>363.72</td></tr><tr><td>Choroidal Hemorrhage and Rupture</td><td>363.61, 363.62, 363.63</td></tr><tr><td>Chorioretinal Scars</td><td>363.30, 363.31, 363.32, 363.33, 363.35</td></tr><tr><td>Chronic Iridocyclitis</td><td>364.10, 364.11</td></tr><tr><td>Cloudy Cornea</td><td>371.01, 371.02, 371.03, 371.04</td></tr><tr><td>Corneal Opacity and Other Disorders of Cornea</td><td>371.00, 371.03, 371.04</td></tr><tr><td>Corneal Edema</td><td>371.20, 371.21, 371.22, 371.23, 371.43, 371.44</td></tr><tr><td>Degeneration of Macula and Posterior Pole</td><td>362.50, 362.51, 362.52, 362.53, 362.54, 362.55, 362.56, 362.57</td></tr><tr><td>Degenerative Disorders of Globe</td><td>360.20, 360.21, 360.23, 360.24, 360.29</td></tr><tr><td>Diabetic Macular Edema</td><td>362.07</td></tr><tr><td>Diabetic Retinopathy</td><td>362.01, 362.02, 362.03, 362.04, 362.05, 362.06</td></tr><tr><td>Disorders of Optic Chiasm</td><td>377.51, 377.52, 377.53, 377.54</td></tr><tr><td>Disorders of Visual Cortex</td><td>377.75</td></tr><tr><td>Disseminated Chorioretinitis and Disseminated Retinochoroiditis</td><td>363.10, 363.11, 363.12, 363.13, 363.14, 363.15</td></tr><tr><td>Focal Chorioretinitis and Focal Retinochoroiditis</td><td>363.00, 363.01, 363.03, 363.04, 363.05, 363.06, 363.07, 363.08</td></tr><tr><td>Glaucoma</td><td>365.10, 365.11, 365.12, 365.13, 365.14, 365.15, 365.20, 365.21, 365.22, 365.23, 365.24, 365.31, 365.32, 365.51, 365.52, 365.59, 365.60, 365.61, 365.62, 365.63, 365.64, 365.65, 365.81, 365.82, 365.83, 365.89</td></tr><tr><td>Glaucoma Associated with Congenital Anomalies, Dystrophies, and Systemic Syndromes</td><td>365.41, 365.42, 365.43, 365.44, 365.60, 365.61, 365.62, 365.63, 365.64, 365.65, 365.81, 365.82, 365.83, 365.89, 365.9</td></tr><tr><td>Hereditary Corneal Dystrophies</td><td>371.50, 371.51, 371.52, 371.53, 371.54, 371.55, 371.56, 371.57, 371.58</td></tr><tr><td>Hereditary Choroidal Dystrophies</td><td>363.50, 363.51, 363.52, 363.53, 363.54, 363.55, 363.56, 363.57</td></tr><tr><td>Hereditary Retinal Dystrophies</td><td>362.70, 362.71, 362.72, 362.73, 362.74, 362.75, 362.76</td></tr><tr><td>Injury to Optic Nerve and Pathways</td><td>950.0, 950.1, 950.2, 950.3, 950.9</td></tr><tr><td>Moderate or Severe Impairment, Better Eye, Profound Impairment Lesser Eye</td><td>369.10, 369.11, 369.12, 369.13, 369.14, 369.15, 369.16, 369.17, 369.18</td></tr><tr><td>Nystagmus and Other Irregular Eye Movements</td><td>379.51</td></tr><tr><td>Open Wound of Eyeball</td><td>871.0, 871.1, 871.2, 871.3, 871.4, 871.5, 871.6, 871.7, 871.9, 921.3</td></tr><tr><td>Optic Atrophy</td><td>377.10, 377.11, 377.12, 377.13, 377.14, 377.15, 377.16</td></tr><tr><td>Optic Neuritis</td><td>377.30, 377.31, 377.32, 377.33, 377.34, 377.39</td></tr><tr><td>Other Background Retinopathy and Retinal Vascular Changes</td><td>362.12, 362.16, 362.18</td></tr><tr><td>Other Corneal Deformities</td><td>371.70, 371.71, 371.72, 371.73</td></tr><tr><td>Other Disorders of Optic Nerve</td><td>377.41</td></tr><tr><td>Other Disorders of Sclera</td><td>379.11, 379.12</td></tr><tr><td>Other Endophthalmitis</td><td>360.11, 360.12, 360.13, 360.14, 360.19</td></tr><tr><td>Other Proliferative Retinopathy</td><td>362.20, 362.21, 362.22, 362.23, 362.24, 362.25, 362.26, 362.27</td></tr><tr><td>Other Retinal Disorders</td><td>362.81, 362.82, 362.83, 362.84, 362.85, 362.89</td></tr><tr><td>Other and Unspecified Forms of Chorioretinitis and Retinochoroiditis</td><td>363.20, 363.21, 363.22</td></tr><tr><td>Pathologic Myopia</td><td>360.20, 360.21</td></tr><tr><td>Prior Penetrating Keratoplasty</td><td>371.60, 371.61, 371.62</td></tr><tr><td>Profound Impairment, Both Eyes</td><td>369.00, 369.01, 369.02, 369.03, 369.04, 369.05, 369.06, 369.07, 369.08</td></tr><tr><td>Purulent Endophthalmitis</td><td>360.00, 360.01, 360.02, 360.03, 360.04</td></tr><tr><td>Retinal Detachment with Retinal Defect</td><td>361.00, 361.01, 361.02, 361.03, 361.04, 361.05, 361.06, 361.07</td></tr><tr><td>Retinal Vascular Occlusion</td><td>362.31, 362.32, 362.35, 362.36</td></tr><tr><td>Scleritis and Episcleritis</td><td>379.04, 379.05, 379.06, 379.07, 379.09</td></tr><tr><td>Separation of Retinal Layers</td><td>362.41, 362.42, 362.43</td></tr><tr><td>Uveitis</td><td>360.11, 360.12</td></tr><tr><td>Visual Field Defects</td><td>368.41</td></tr><tr><td><strong>Significant Ocular Condition</strong></td><td><strong>Corresponding ICD-10-CM Codes<br />[for use 1/1/2016 – 12/31/2016]</strong></td></tr><tr><td>Acute and Subacute Iridocyclitis</td><td>H20.00, H20.011, H20.012, H20.013, H20.019, H20.021, H20.022, H20.023, H20.029, H20.031, H20.032, H20.033, H20.039, H20.041, H20.042, H20.043, H20.049, H20.051, H20.052, H20.053, H20.059</td></tr><tr><td>Amblyopia</td><td>H53.011, H53.012, H53.013, H53.019, H53.021, H53.022, H53.023, H53.029, H53.031, H53.032, H53.033, H53.039</td></tr><tr><td>Burn Confined to Eye and Adnexa</td><td>T26.00XA, T26.01XA, T26.02XA, T26.10XA, T26.11XA, T26.12XA, T26.20XA, T26.21XA, T26.22XA, T26.30XA, T26.31XA, T26.32XA, T26.40XA, T26.41XA, T26.42XA, T26.50XA, T26.51XA, T26.52XA, T26.60XA, T26.61XA, T26.62XA, T26.70XA, T26.71XA, T26.72XA, T26.80XA, T26.81XA, T26.82XA, T26.90XA, T26.91XA, T26.92XA</td></tr><tr><td>Cataract Secondary to Ocular Disorders</td><td>H26.211, H26.212, H26.213, H26.219, H26.221, H26.222, H26.223, H26.229</td></tr><tr><td>Central Corneal Ulcer</td><td>H16.011, H16.012, H16.013, H16.019</td></tr><tr><td>Certain Types of Iridocyclitis</td><td>H20.20, H20.21, H20.22, H20.23, H20.811, H20.812, H20.813, H20.819, H20.821, H20.822, H20.823, H20.829, H20.9, H40.40X0</td></tr><tr><td>Choroidal Degenerations</td><td>H35.33</td></tr><tr><td>Choroidal Detachment</td><td>H31.411, H31.412, H31.413, H31.419</td></tr><tr><td>Choroidal Hemorrhage and Rupture</td><td>H31.301, H31.302, H31.303, H31.309, H31.311, H31.312, H31.313, H31.319, H31.321, H31.322, H31.323, H31.329</td></tr><tr><td>Chorioretinal Scars</td><td>H31.001, H31.002, H31.003, H31.009, H31.011, H31.012, H31.013, H31.019, H31.021, H31.022, H31.023, H31.029, H31.091, H31.092, H31.093, H31.099</td></tr><tr><td>Chronic Iridocyclitis</td><td>A18.54, H20.10, H20.11, H20.12, H20.13, H20.9</td></tr><tr><td>Cloudy Cornea</td><td>H17.00, H17.01, H17.02, H17.03, H17.10, H17.11, H17.12, H17.13, H17.811, H17.812, H17.813, H17.819, H17.821, H17.822, H17.823, H17.829</td></tr><tr><td>Corneal Opacity and Other Disorders of Cornea</td><td>H17.00, H17.01, H17.02, H17.03, H17.10, H17.11, H17.12, H17.13, H17.89, H17.9</td></tr><tr><td>Corneal Edema</td><td>H18.10, H18.11, H18.12, H18.13, H18.20, H18.221, H18.222, H18.223, H18.229, H18.231, H18.232, H18.233, H18.239, H18.421, H18.422, H18.423, H18.429, H18.43</td></tr><tr><td>Degeneration of Macula and Posterior Pole</td><td>H35.30, H35.31, H35.32, H35.341, H35.342, H35.343, H35.349, H35.351, H35.352, H35.353, H35.359, H35.361, H35.362, H35.363, H35.369, H35.371, H35.372, H35.373, H35.379, H35.381, H35.382, H35.383, H35.389</td></tr><tr><td>Degenerative Disorders of Globe</td><td>H44.20, H44.21, H44.22, H44.23, H44.311, H44.312, H44.313, H44.319, H44.321, H44.322, H44.323, H44.329, H44.391, H44.392, H44.393, H44.399</td></tr><tr><td>Diabetic Macular Edema</td><td>E08.311, E08.321, E08.331, E08.341, E08.351, E09.311, E09.321, E09.331, E09.341, E09.351, E10.311, E10.321, E10.331, E10.341, E10.351, E11.311, E11.321, E11.331, E11.341, E11.351, E13.311, E13.321, E13.331, E13.341, E13.351</td></tr><tr><td>Diabetic Retinopathy</td><td>E08.311, E08.319, E08.321, E08.329, E08.331, E08.339, E08.341, E08.349, E08.351, E08.359, E09.311, E09.319, E09.321, E09.329, E09.331, E09.339, E09.341, E09.349, E09.351, E09.359, E10.311, E10.319, E10.321, E10.329, E10.331, E10.339, E10.341, E10.349, E10.351, E10.359, E11.311, E11.319, E11.321, E11.329, E11.331, E11.339, E11.341, E11.349, E11.351, E11.359, E13.311, E13.319, E13.321, E13.329, E13.331, E13.339, E13.341, E13.349, E13.351, E13.359</td></tr><tr><td>Disorders of Optic Chiasm</td><td>H47.41, H47.42, H47.43, H47.49</td></tr><tr><td>Disorders of Visual Cortex</td><td>H47.611, H47.612, H47.619</td></tr><tr><td>Disseminated Chorioretinitis and Disseminated Retinochoroiditis</td><td>A18.53, H30.101, H30.102, H30.103, H30.109, H30.111, H30.112, H30.113, H30.119, H30.121, H30.122, H30.123, H30.129, H30.131, H30.132, H30.133, H30.139, H30.141, H30.142, H30.143, H30.149</td></tr><tr><td>Focal Chorioretinitis and Focal Retinochoroiditis</td><td>H30.001, H30.002, H30.003, H30.009, H30.011, H30.012, H30.013, H30.019, H30.021, H30.022, H30.023, H30.029, H30.031, H30.032, H30.033, H30.039, H30.041, H30.042, H30.043, H30.049</td></tr><tr><td>Glaucoma</td><td>H40.10X0, H40.10X1, H40.10X2, H40.10X3, H40.10X4, H40.11X0, H40.11X1, H40.11X2, H40.11X3, H40.11X4, H40.1210, H40.1211, H40.1212, H40.1213, H40.1214, H40.1220, H40.1221, H40.1222, H40.1223, H40.1224, H40.1230, H40.1231, H40.1232, H40.1233, H40.1234, H40.1290, H40.1291, H40.1292, H40.1293, H40.1294, H40.1310, H40.1311, H40.1312, H40.1313, H40.1314, H40.1320, H40.1321, H40.1322, H40.1323, H40.1324, H40.1330, H40.1331, H40.1332, H40.1333, H40.1334, H40.1390, H40.1391, H40.1392, H40.1393, H40.1394, H40.1410, H40.1411, H40.1412, H40.1413, H40.1414, H40.1420, H40.1421, H40.1422, H40.1423, H40.1424, H40.1430, H40.1431, H40.1432, H40.1433, H40.1434, H40.1490, H40.1491, H40.1492, H40.1493, H40.1494, H40.151, H40.152, H40.153, H40.159, H40.20X0, H40.20X1, H40.20X2, H40.20X3, H40.20X4, H40.211, H40.212, H40.213, H40.219, H40.2210, H40.2211, H40.2212, H40.2213, H40.2214, H40.2220, H40.2221, H40.2222, H40.2223, H40.2224, H40.2230, H40.2231, H40.2232, H40.2233, H40.2234, H40.2290, H40.2291, H40.2292, H40.2293, H40.2294, H40.231, H40.232, H40.233, H40.239, H40.241, H40.242, H40.243, H40.249, H40.30X0, H40.30X1, H40.30X2, H40.30X3, H40.30X4, H40.31X0, H40.31X1, H40.31X2, H40.31X3, H40.31X4, H40.32X0, H40.32X1, H40.32X2, H40.32X3, H40.32X4, H40.33X0, H40.33X1, H40.33X2, H40.33X3, H40.33X4, H40.40X0, H40.40X1, H40.40X2, H40.40X3, H40.40X4, H40.41X0, H40.41X1, H40.41X2, H40.41X3, H40.41X4, H40.42X0, H40.42X1, H40.42X2, H40.42X3, H40.42X4, H40.43X0, H40.43X1, H40.43X2, H40.43X3, H40.43X4, H40.50X0, H40.50X1, H40.50X2, H40.50X3, H40.50X4, H40.51X0, H40.51X1, H40.51X2, H40.51X3, H40.51X4, H40.52X0, H40.52X1, H40.52X2, H40.52X3, H40.52X4, H40.53X0, H40.53X1, H40.53X2, H40.53X3, H40.53X4, H40.60X0, H40.60X1, H40.60X2, H40.60X3, H40.60X4, H40.61X0, H40.61X1, H40.61X2, H40.61X3, H40.61X4, H40.62X0, H40.62X1, H40.62X2, H40.62X3, H40.62X4, H40.63X0, H40.63X1, H40.63X2, H40.63X3, H40.63X4, H40.811, H40.812, H40.813, H40.819, H40.821, H40.822, H40.823, H40.829, H40.831, H40.832, H40.833, H40.839, H40.89, Q15.0</td></tr><tr><td>Glaucoma Associated with Congenital Anomalies, Dystrophies, and Systemic Syndromes</td><td>H40.30X0, H40.30X1, H40.30X2, H40.30X3, H40.30X4, H40.31X0, H40.31X1, H40.31X2, H40.31X3, H40.31X4, H40.32X0, H40.32X1, H40.32X2, H40.32X3, H40.32X4, H40.33X0, H40.33X1, H40.33X2, H40.33X3, H40.33X4, H40.40X0, H40.40X1, H40.40X2, H40.40X3, H40.40X4, H40.41X0, H40.41X1, H40.41X2, H40.41X3, H40.41X4, H40.42X0, H40.42X1, H40.42X2, H40.42X3, H40.42X4, H40.43X0, H40.43X1, H40.43X2, H40.43X3, H40.43X4, H40.50X0, H40.50X1, H40.50X2, H40.50X3, H40.50X4, H40.51X0, H40.51X1, H40.51X2, H40.51X3, H40.51X4, H40.52X0, H40.52X1, H40.52X2, H40.52X3, H40.52X4, H40.53X0, H40.53X1, H40.53X2, H40.53X3, H40.53X4, H40.811, H40.812, H40.813, H40.819, H40.821, H40.822, H40.823, H40.829, H40.831, H40.832, H40.833, H40.839, H40.89, H40.9, H42</td></tr><tr><td>Hereditary Corneal Dystrophies</td><td>H18.50, H18.51, H18.52, H18.53, H18.54, H18.55, H18.59</td></tr><tr><td>Hereditary Choroidal Dystrophies</td><td>H31.20, H31.21, H31.22, H31.23, H31.29</td></tr><tr><td>Hereditary Retinal Dystrophies</td><td>H35.50, H35.51, H35.52, H35.53, H35.54, H36</td></tr><tr><td>Injury to Optic Nerve and Pathways</td><td>S04.011A, S04.012A, S04.019A, S04.02XA, S04.031A, S04.032A, S04.039A, S04.041A, S04.042A, S04.049A</td></tr><tr><td>Moderate or Severe Impairment, Better Eye, Profound Impairment Lesser Eye</td><td>H54.10, H54.11, H54.12</td></tr><tr><td>Nystagmus and Other Irregular Eye Movements</td><td>H55.01</td></tr><tr><td>Open Wound of Eyeball</td><td>S05.10XA, S05.11XA, S05.12XA, S05.20XA, S05.21XA, S05.22XA, S05.30XA, S05.31XA, S05.32XA, S05.50XA, S05.51XA, S05.52XA, S05.60XA, S05.61XA, S05.62XA, S05.70XA, S05.71XA, S05.72XA, S05.8X1A, S05.8X2A, S05.8X9A, S05.90XA, S05.91XA, S05.92XA</td></tr><tr><td>Optic Atrophy</td><td>H47.20, H47.211, H47.212, H47.213, H47.219, H47.22, H47.231, H47.232, H47.233, H47.239, H47.291, H47.292, H47.293, H47.299</td></tr><tr><td>Optic Neuritis</td><td>H46.00, H46.01, H46.02, H46.03, H46.10, H46.11, H46.12, H46.13, H46.2, H46.3, H46.8, H46.9</td></tr><tr><td>Other Background Retinopathy and Retinal Vascular Changes</td><td>H35.021, H35.022, H35.023, H35.029, H35.051, H35.052, H35.053, H35.059, H35.061, H35.062, H35.063, H35.069</td></tr><tr><td>Other Corneal Deformities</td><td>H18.70, H18.711, H18.712, H18.713, H18.719, H18.721, H18.722, H18.723, H18.729, H18.731, H18.732, H18.733, H18.739, H18.791, H18.792, H18.793, H18.799</td></tr><tr><td>Other Disorders of Optic Nerve</td><td>H47.011, H47.012, H47.013, H47.019</td></tr><tr><td>Other Disorders of Sclera</td><td>H15.831, H15.832, H15.833, H15.839, H15.841, H15.842, H15.843, H15.849</td></tr><tr><td>Other Endophthalmitis</td><td>H16.241, H16.242, H16.243, H16.249, H21.331, H21.332, H21.333, H21.339, H33.121, H33.122, H33.123, H33.129, H44.111, H44.112, H44.113, H44.119, H44.121, H44.122, H44.123, H44.129, H44.131, H44.132, H44.133, H44.139, H44.19</td></tr><tr><td>Other Proliferative Retinopathy</td><td>H35.101, H35.102, H35.103, H35.109, H35.111, H35.112, H35.113, H35.119, H35.121, H35.122, H35.123, H35.129, H35.131, H35.132, H35.133, H35.139, H35.141, H35.142, H35.143, H35.149, H35.151, H35.152, H35.153, H35.159, H35.161, H35.162, H35.163, H35.169, H35.171, H35.172, H35.173, H35.179</td></tr><tr><td>Other Retinal Disorders</td><td>H35.60, H35.61, H35.62, H35.63, H35.81, H35.89, H35.82</td></tr><tr><td>Other and Unspecified Forms of Chorioretinitis and Retinochoroiditis</td><td>H30.20, H30.21, H30.22, H30.23, H30.811, H30.812, H30.813, H30.819, H30.891, H30.892, H30.893, H30.899, H30.90, H30.91, H30.92, H30.93</td></tr><tr><td>Pathologic Myopia</td><td>H44.20, H44.21, H44.22, H44.23, H44.30</td></tr><tr><td>Prior Penetrating Keratoplasty</td><td>H18.601, H18.602, H18.603, H18.609, H18.611, H18.612, H18.613, H18.619, H18.621, H18.622, H18.623, H18.629</td></tr><tr><td>Profound Impairment, Both Eyes</td><td>H54.0, H54.10</td></tr><tr><td>Purulent Endophthalmitis</td><td>H44.001, H44.002, H44.003, H44.009, H44.011, H44.012, H44.013, H44.019, H44.021, H44.022, H44.023, H44.029,</td></tr><tr><td>Retinal Detachment with Retinal Defect</td><td>H33.001, H33.002, H33.003, H33.009, H33.011, H33.012, H33.013, H33.019, H33.021, H33.022, H33.023, H33.029, H33.031, H33.032, H33.033, H33.039, H33.041, H33.042, H33.043, H33.049, H33.051, H33.052, H33.053, H33.059, H33.8</td></tr><tr><td>Retinal Vascular Occlusion</td><td>H34.10, H34.11, H34.12, H34.13, H34.231, H34.232, H34.233, H34.239, H34.811, H34.812, H34.813, H34.819, H34.831, H34.832, H34.833, H34.839</td></tr><tr><td>Scleritis and Episcleritis</td><td>A18.51, H15.021, H15.022, H15.023, H15.029, H15.031, H15.032, H15.033, H15.039, H15.041, H15.042, H15.043, H15.049, H15.051, H15.052, H15.053, H15.059, H15.091, H15.092, H15.093, H15.099</td></tr><tr><td>Separation of Retinal Layers</td><td>H35.711, H35.712, H35.713, H35.719, H35.721, H35.722, H35.723, H35.729, H35.731, H35.732, H35.733, H35.739</td></tr><tr><td>Uveitis</td><td>H44.111, H44.112, H44.113, H44.119, H44.131, H44.132, H44.133, H44.139</td></tr><tr><td>Visual Field Defects</td><td>H53.411, H53.412, H53.413, H53.419</td></tr></table></div>'
                    });

                    $('#PQRSQ9').aToolTip2({
                        clickIt: true,
                        yOffset: -225,
                        tipContent: '<div style="height: 400px; overflow-y:scroll"><table><tr><td><strong>Significant Ocular Condition</strong></td><td><strong>Corresponding ICD-9-CM Codes<br />[for use 1/1/2016 – 9/30/2016]</strong></td></tr><tr><td>Acute and Subacute Iridocyclitis</td><td>364.00, 364.01, 364.02, 364.03, 364.04, 364.05</td></tr><tr><td>Amblyopia</td><td>368.01, 368.02, 368.03</td></tr><tr><td>Burn Confined to Eye and Adnexa</td><td>940.0, 940.1, 940.2, 940.3, 940.4, 940.5, 940.9</td></tr><tr><td>Cataract Secondary to Ocular Disorders</td><td>366.32, 366.33</td></tr><tr><td>Central Corneal Ulcer</td><td>370.03</td></tr><tr><td>Certain Types of Iridocyclitis</td><td>364.21, 364.22, 364.23, 364.24, 364.3</td></tr><tr><td>Choroidal Degenerations</td><td>363.43</td></tr><tr><td>Choroidal Detachment</td><td>363.72</td></tr><tr><td>Choroidal Hemorrhage and Rupture</td><td>363.61, 363.62, 363.63</td></tr><tr><td>Chorioretinal Scars</td><td>363.30, 363.31, 363.32, 363.33, 363.35</td></tr><tr><td>Chronic Iridocyclitis</td><td>364.10, 364.11</td></tr><tr><td>Cloudy Cornea</td><td>371.01, 371.02, 371.03, 371.04</td></tr><tr><td>Corneal Opacity and Other Disorders of Cornea</td><td>371.00, 371.03, 371.04</td></tr><tr><td>Corneal Edema</td><td>371.20, 371.21, 371.22, 371.23, 371.43, 371.44</td></tr><tr><td>Degeneration of Macula and Posterior Pole</td><td>362.50, 362.51, 362.52, 362.53, 362.54, 362.55, 362.56, 362.57</td></tr><tr><td>Degenerative Disorders of Globe</td><td>360.20, 360.21, 360.23, 360.24, 360.29</td></tr><tr><td>Diabetic Macular Edema</td><td>362.07</td></tr><tr><td>Diabetic Retinopathy</td><td>362.01, 362.02, 362.03, 362.04, 362.05, 362.06</td></tr><tr><td>Disorders of Optic Chiasm</td><td>377.51, 377.52, 377.53, 377.54</td></tr><tr><td>Disorders of Visual Cortex</td><td>377.75</td></tr><tr><td>Disseminated Chorioretinitis and Disseminated Retinochoroiditis</td><td>363.10, 363.11, 363.12, 363.13, 363.14, 363.15</td></tr><tr><td>Focal Chorioretinitis and Focal Retinochoroiditis</td><td>363.00, 363.01, 363.03, 363.04, 363.05, 363.06, 363.07, 363.08</td></tr><tr><td>Glaucoma</td><td>365.10, 365.11, 365.12, 365.13, 365.14, 365.15, 365.20, 365.21, 365.22, 365.23, 365.24, 365.31, 365.32, 365.51, 365.52, 365.59, 365.60, 365.61, 365.62, 365.63, 365.64, 365.65, 365.81, 365.82, 365.83, 365.89</td></tr><tr><td>Glaucoma Associated with Congenital Anomalies, Dystrophies, and Systemic Syndromes</td><td>365.41, 365.42, 365.43, 365.44, 365.60, 365.61, 365.62, 365.63, 365.64, 365.65, 365.81, 365.82, 365.83, 365.89, 365.9</td></tr><tr><td>Hereditary Corneal Dystrophies</td><td>371.50, 371.51, 371.52, 371.53, 371.54, 371.55, 371.56, 371.57, 371.58</td></tr><tr><td>Hereditary Choroidal Dystrophies</td><td>363.50, 363.51, 363.52, 363.53, 363.54, 363.55, 363.56, 363.57</td></tr><tr><td>Hereditary Retinal Dystrophies</td><td>362.70, 362.71, 362.72, 362.73, 362.74, 362.75, 362.76</td></tr><tr><td>Injury to Optic Nerve and Pathways</td><td>950.0, 950.1, 950.2, 950.3, 950.9</td></tr><tr><td>Moderate or Severe Impairment, Better Eye, Profound Impairment Lesser Eye</td><td>369.10, 369.11, 369.12, 369.13, 369.14, 369.15, 369.16, 369.17, 369.18</td></tr><tr><td>Nystagmus and Other Irregular Eye Movements</td><td>379.51</td></tr><tr><td>Open Wound of Eyeball</td><td>871.0, 871.1, 871.2, 871.3, 871.4, 871.5, 871.6, 871.7, 871.9, 921.3</td></tr><tr><td>Optic Atrophy</td><td>377.10, 377.11, 377.12, 377.13, 377.14, 377.15, 377.16</td></tr><tr><td>Optic Neuritis</td><td>377.30, 377.31, 377.32, 377.33, 377.34, 377.39</td></tr><tr><td>Other Background Retinopathy and Retinal Vascular Changes</td><td>362.12, 362.16, 362.18</td></tr><tr><td>Other Corneal Deformities</td><td>371.70, 371.71, 371.72, 371.73</td></tr><tr><td>Other Disorders of Optic Nerve</td><td>377.41</td></tr><tr><td>Other Disorders of Sclera</td><td>379.11, 379.12</td></tr><tr><td>Other Endophthalmitis</td><td>360.11, 360.12, 360.13, 360.14, 360.19</td></tr><tr><td>Other Proliferative Retinopathy</td><td>362.20, 362.21, 362.22, 362.23, 362.24, 362.25, 362.26, 362.27</td></tr><tr><td>Other Retinal Disorders</td><td>362.81, 362.82, 362.83, 362.84, 362.85, 362.89</td></tr><tr><td>Other and Unspecified Forms of Chorioretinitis and Retinochoroiditis</td><td>363.20, 363.21, 363.22</td></tr><tr><td>Pathologic Myopia</td><td>360.20, 360.21</td></tr><tr><td>Prior Penetrating Keratoplasty</td><td>371.60, 371.61, 371.62</td></tr><tr><td>Profound Impairment, Both Eyes</td><td>369.00, 369.01, 369.02, 369.03, 369.04, 369.05, 369.06, 369.07, 369.08</td></tr><tr><td>Purulent Endophthalmitis</td><td>360.00, 360.01, 360.02, 360.03, 360.04</td></tr><tr><td>Retinal Detachment with Retinal Defect</td><td>361.00, 361.01, 361.02, 361.03, 361.04, 361.05, 361.06, 361.07</td></tr><tr><td>Retinal Vascular Occlusion</td><td>362.31, 362.32, 362.35, 362.36</td></tr><tr><td>Scleritis and Episcleritis</td><td>379.04, 379.05, 379.06, 379.07, 379.09</td></tr><tr><td>Separation of Retinal Layers</td><td>362.41, 362.42, 362.43</td></tr><tr><td>Uveitis</td><td>360.11, 360.12</td></tr><tr><td>Visual Field Defects</td><td>368.41</td></tr><tr><td><strong>Significant Ocular Condition</strong></td><td><strong>Corresponding ICD-10-CM Codes<br />[for use 10/1/2016 – 12/31/2016]</strong></td></tr><tr><td>Acute and Subacute Iridocyclitis</td><td>H20.00, H20.011, H20.012, H20.013, H20.019, H20.021, H20.022, H20.023, H20.029, H20.031, H20.032, H20.033, H20.039, H20.041, H20.042, H20.043, H20.049, H20.051, H20.052, H20.053, H20.059</td></tr><tr><td>Amblyopia</td><td>H53.011, H53.012, H53.013, H53.019, H53.021, H53.022, H53.023, H53.029, H53.031, H53.032, H53.033, H53.039</td></tr><tr><td>Burn Confined to Eye and Adnexa</td><td>T26.00XA, T26.01XA, T26.02XA, T26.10XA, T26.11XA, T26.12XA, T26.20XA, T26.21XA, T26.22XA, T26.30XA, T26.31XA, T26.32XA, T26.40XA, T26.41XA, T26.42XA, T26.50XA, T26.51XA, T26.52XA, T26.60XA, T26.61XA, T26.62XA, T26.70XA, T26.71XA, T26.72XA, T26.80XA, T26.81XA, T26.82XA, T26.90XA, T26.91XA, T26.92XA</td></tr><tr><td>Cataract Secondary to Ocular Disorders</td><td>H26.211, H26.212, H26.213, H26.219, H26.221, H26.222, H26.223, H26.229</td></tr><tr><td>Central Corneal Ulcer</td><td>H16.011, H16.012, H16.013, H16.019</td></tr><tr><td>Certain Types of Iridocyclitis</td><td>H20.20, H20.21, H20.22, H20.23, H20.811, H20.812, H20.813, H20.819, H20.821, H20.822, H20.823, H20.829, H20.9, H40.40X0</td></tr><tr><td>Choroidal Degenerations</td><td>H35.33</td></tr><tr><td>Choroidal Detachment</td><td>H31.411, H31.412, H31.413, H31.419</td></tr><tr><td>Choroidal Hemorrhage and Rupture</td><td>H31.301, H31.302, H31.303, H31.309, H31.311, H31.312, H31.313, H31.319, H31.321, H31.322, H31.323, H31.329</td></tr><tr><td>Chorioretinal Scars</td><td>H31.001, H31.002, H31.003, H31.009, H31.011, H31.012, H31.013, H31.019, H31.021, H31.022, H31.023, H31.029, H31.091, H31.092, H31.093, H31.099</td></tr><tr><td>Chronic Iridocyclitis</td><td>A18.54, H20.10, H20.11, H20.12, H20.13, H20.9</td></tr><tr><td>Cloudy Cornea</td><td>H17.00, H17.01, H17.02, H17.03, H17.10, H17.11, H17.12, H17.13, H17.811, H17.812, H17.813, H17.819, H17.821, H17.822, H17.823, H17.829</td></tr><tr><td>Corneal Opacity and Other Disorders of Cornea</td><td>H17.00, H17.01, H17.02, H17.03, H17.10, H17.11, H17.12, H17.13, H17.89, H17.9</td></tr><tr><td>Corneal Edema</td><td>H18.10, H18.11, H18.12, H18.13, H18.20, H18.221, H18.222, H18.223, H18.229, H18.231, H18.232, H18.233, H18.239, H18.421, H18.422, H18.423, H18.429, H18.43</td></tr><tr><td>Degeneration of Macula and Posterior Pole</td><td>H35.30, H35.31, H35.32, H35.341, H35.342, H35.343, H35.349, H35.351, H35.352, H35.353, H35.359, H35.361, H35.362, H35.363, H35.369, H35.371, H35.372, H35.373, H35.379, H35.381, H35.382, H35.383, H35.389</td></tr><tr><td>Degenerative Disorders of Globe</td><td>H44.20, H44.21, H44.22, H44.23, H44.311, H44.312, H44.313, H44.319, H44.321, H44.322, H44.323, H44.329, H44.391, H44.392, H44.393, H44.399</td></tr><tr><td>Diabetic Macular Edema</td><td>E08.311, E08.321, E08.331, E08.341, E08.351, E09.311, E09.321, E09.331, E09.341, E09.351, E10.311, E10.321, E10.331, E10.341, E10.351, E11.311, E11.321, E11.331, E11.341, E11.351, E13.311, E13.321, E13.331, E13.341, E13.351</td></tr><tr><td>Diabetic Retinopathy</td><td>E08.311, E08.319, E08.321, E08.329, E08.331, E08.339, E08.341, E08.349, E08.351, E08.359, E09.311, E09.319, E09.321, E09.329, E09.331, E09.339, E09.341, E09.349, E09.351, E09.359, E10.311, E10.319, E10.321, E10.329, E10.331, E10.339, E10.341, E10.349, E10.351, E10.359, E11.311, E11.319, E11.321, E11.329, E11.331, E11.339, E11.341, E11.349, E11.351, E11.359, E13.311, E13.319, E13.321, E13.329, E13.331, E13.339, E13.341, E13.349, E13.351, E13.359</td></tr><tr><td>Disorders of Optic Chiasm</td><td>H47.41, H47.42, H47.43, H47.49</td></tr><tr><td>Disorders of Visual Cortex</td><td>H47.611, H47.612, H47.619</td></tr><tr><td>Disseminated Chorioretinitis and Disseminated Retinochoroiditis</td><td>A18.53, H30.101, H30.102, H30.103, H30.109, H30.111, H30.112, H30.113, H30.119, H30.121, H30.122, H30.123, H30.129, H30.131, H30.132, H30.133, H30.139, H30.141, H30.142, H30.143, H30.149</td></tr><tr><td>Focal Chorioretinitis and Focal Retinochoroiditis</td><td>H30.001, H30.002, H30.003, H30.009, H30.011, H30.012, H30.013, H30.019, H30.021, H30.022, H30.023, H30.029, H30.031, H30.032, H30.033, H30.039, H30.041, H30.042, H30.043, H30.049</td></tr><tr><td>Glaucoma</td><td>H40.10X0, H40.10X1, H40.10X2, H40.10X3, H40.10X4, H40.11X0, H40.11X1, H40.11X2, H40.11X3, H40.11X4, H40.1210, H40.1211, H40.1212, H40.1213, H40.1214, H40.1220, H40.1221, H40.1222, H40.1223, H40.1224, H40.1230, H40.1231, H40.1232, H40.1233, H40.1234, H40.1290, H40.1291, H40.1292, H40.1293, H40.1294, H40.1310, H40.1311, H40.1312, H40.1313, H40.1314, H40.1320, H40.1321, H40.1322, H40.1323, H40.1324, H40.1330, H40.1331, H40.1332, H40.1333, H40.1334, H40.1390, H40.1391, H40.1392, H40.1393, H40.1394, H40.1410, H40.1411, H40.1412, H40.1413, H40.1414, H40.1420, H40.1421, H40.1422, H40.1423, H40.1424, H40.1430, H40.1431, H40.1432, H40.1433, H40.1434, H40.1490, H40.1491, H40.1492, H40.1493, H40.1494, H40.151, H40.152, H40.153, H40.159, H40.20X0, H40.20X1, H40.20X2, H40.20X3, H40.20X4, H40.211, H40.212, H40.213, H40.219, H40.2210, H40.2211, H40.2212, H40.2213, H40.2214, H40.2220, H40.2221, H40.2222, H40.2223, H40.2224, H40.2230, H40.2231, H40.2232, H40.2233, H40.2234, H40.2290, H40.2291, H40.2292, H40.2293, H40.2294, H40.231, H40.232, H40.233, H40.239, H40.241, H40.242, H40.243, H40.249, H40.30X0, H40.30X1, H40.30X2, H40.30X3, H40.30X4, H40.31X0, H40.31X1, H40.31X2, H40.31X3, H40.31X4, H40.32X0, H40.32X1, H40.32X2, H40.32X3, H40.32X4, H40.33X0, H40.33X1, H40.33X2, H40.33X3, H40.33X4, H40.40X0, H40.40X1, H40.40X2, H40.40X3, H40.40X4, H40.41X0, H40.41X1, H40.41X2, H40.41X3, H40.41X4, H40.42X0, H40.42X1, H40.42X2, H40.42X3, H40.42X4, H40.43X0, H40.43X1, H40.43X2, H40.43X3, H40.43X4, H40.50X0, H40.50X1, H40.50X2, H40.50X3, H40.50X4, H40.51X0, H40.51X1, H40.51X2, H40.51X3, H40.51X4, H40.52X0, H40.52X1, H40.52X2, H40.52X3, H40.52X4, H40.53X0, H40.53X1, H40.53X2, H40.53X3, H40.53X4, H40.60X0, H40.60X1, H40.60X2, H40.60X3, H40.60X4, H40.61X0, H40.61X1, H40.61X2, H40.61X3, H40.61X4, H40.62X0, H40.62X1, H40.62X2, H40.62X3, H40.62X4, H40.63X0, H40.63X1, H40.63X2, H40.63X3, H40.63X4, H40.811, H40.812, H40.813, H40.819, H40.821, H40.822, H40.823, H40.829, H40.831, H40.832, H40.833, H40.839, H40.89, Q15.0</td></tr><tr><td>Glaucoma Associated with Congenital Anomalies, Dystrophies, and Systemic Syndromes</td><td>H40.30X0, H40.30X1, H40.30X2, H40.30X3, H40.30X4, H40.31X0, H40.31X1, H40.31X2, H40.31X3, H40.31X4, H40.32X0, H40.32X1, H40.32X2, H40.32X3, H40.32X4, H40.33X0, H40.33X1, H40.33X2, H40.33X3, H40.33X4, H40.40X0, H40.40X1, H40.40X2, H40.40X3, H40.40X4, H40.41X0, H40.41X1, H40.41X2, H40.41X3, H40.41X4, H40.42X0, H40.42X1, H40.42X2, H40.42X3, H40.42X4, H40.43X0, H40.43X1, H40.43X2, H40.43X3, H40.43X4, H40.50X0, H40.50X1, H40.50X2, H40.50X3, H40.50X4, H40.51X0, H40.51X1, H40.51X2, H40.51X3, H40.51X4, H40.52X0, H40.52X1, H40.52X2, H40.52X3, H40.52X4, H40.53X0, H40.53X1, H40.53X2, H40.53X3, H40.53X4, H40.811, H40.812, H40.813, H40.819, H40.821, H40.822, H40.823, H40.829, H40.831, H40.832, H40.833, H40.839, H40.89, H40.9, H42</td></tr><tr><td>Hereditary Corneal Dystrophies</td><td>H18.50, H18.51, H18.52, H18.53, H18.54, H18.55, H18.59</td></tr><tr><td>Hereditary Choroidal Dystrophies</td><td>H31.20, H31.21, H31.22, H31.23, H31.29</td></tr><tr><td>Hereditary Retinal Dystrophies</td><td>H35.50, H35.51, H35.52, H35.53, H35.54, H36</td></tr><tr><td>Injury to Optic Nerve and Pathways</td><td>S04.011A, S04.012A, S04.019A, S04.02XA, S04.031A, S04.032A, S04.039A, S04.041A, S04.042A, S04.049A</td></tr><tr><td>Moderate or Severe Impairment, Better Eye, Profound Impairment Lesser Eye</td><td>H54.10, H54.11, H54.12</td></tr><tr><td>Nystagmus and Other Irregular Eye Movements</td><td>H55.01</td></tr><tr><td>Open Wound of Eyeball</td><td>S05.10XA, S05.11XA, S05.12XA, S05.20XA, S05.21XA, S05.22XA, S05.30XA, S05.31XA, S05.32XA, S05.50XA, S05.51XA, S05.52XA, S05.60XA, S05.61XA, S05.62XA, S05.70XA, S05.71XA, S05.72XA, S05.8X1A, S05.8X2A, S05.8X9A, S05.90XA, S05.91XA, S05.92XA</td></tr><tr><td>Optic Atrophy</td><td>H47.20, H47.211, H47.212, H47.213, H47.219, H47.22, H47.231, H47.232, H47.233, H47.239, H47.291, H47.292, H47.293, H47.299</td></tr><tr><td>Optic Neuritis</td><td>H46.00, H46.01, H46.02, H46.03, H46.10, H46.11, H46.12, H46.13, H46.2, H46.3, H46.8, H46.9</td></tr><tr><td>Other Background Retinopathy and Retinal Vascular Changes</td><td>H35.021, H35.022, H35.023, H35.029, H35.051, H35.052, H35.053, H35.059, H35.061, H35.062, H35.063, H35.069</td></tr><tr><td>Other Corneal Deformities</td><td>H18.70, H18.711, H18.712, H18.713, H18.719, H18.721, H18.722, H18.723, H18.729, H18.731, H18.732, H18.733, H18.739, H18.791, H18.792, H18.793, H18.799</td></tr><tr><td>Other Disorders of Optic Nerve</td><td>H47.011, H47.012, H47.013, H47.019</td></tr><tr><td>Other Disorders of Sclera</td><td>H15.831, H15.832, H15.833, H15.839, H15.841, H15.842, H15.843, H15.849</td></tr><tr><td>Other Endophthalmitis</td><td>H16.241, H16.242, H16.243, H16.249, H21.331, H21.332, H21.333, H21.339, H33.121, H33.122, H33.123, H33.129, H44.111, H44.112, H44.113, H44.119, H44.121, H44.122, H44.123, H44.129, H44.131, H44.132, H44.133, H44.139, H44.19</td></tr><tr><td>Other Proliferative Retinopathy</td><td>H35.101, H35.102, H35.103, H35.109, H35.111, H35.112, H35.113, H35.119, H35.121, H35.122, H35.123, H35.129, H35.131, H35.132, H35.133, H35.139, H35.141, H35.142, H35.143, H35.149, H35.151, H35.152, H35.153, H35.159, H35.161, H35.162, H35.163, H35.169, H35.171, H35.172, H35.173, H35.179</td></tr><tr><td>Other Retinal Disorders</td><td>H35.60, H35.61, H35.62, H35.63, H35.81, H35.89, H35.82</td></tr><tr><td>Other and Unspecified Forms of Chorioretinitis and Retinochoroiditis</td><td>H30.20, H30.21, H30.22, H30.23, H30.811, H30.812, H30.813, H30.819, H30.891, H30.892, H30.893, H30.899, H30.90, H30.91, H30.92, H30.93</td></tr><tr><td>Pathologic Myopia</td><td>H44.20, H44.21, H44.22, H44.23, H44.30</td></tr><tr><td>Prior Penetrating Keratoplasty</td><td>H18.601, H18.602, H18.603, H18.609, H18.611, H18.612, H18.613, H18.619, H18.621, H18.622, H18.623, H18.629</td></tr><tr><td>Profound Impairment, Both Eyes</td><td>H54.0, H54.10</td></tr><tr><td>Purulent Endophthalmitis</td><td>H44.001, H44.002, H44.003, H44.009, H44.011, H44.012, H44.013, H44.019, H44.021, H44.022, H44.023, H44.029,</td></tr><tr><td>Retinal Detachment with Retinal Defect</td><td>H33.001, H33.002, H33.003, H33.009, H33.011, H33.012, H33.013, H33.019, H33.021, H33.022, H33.023, H33.029, H33.031, H33.032, H33.033, H33.039, H33.041, H33.042, H33.043, H33.049, H33.051, H33.052, H33.053, H33.059, H33.8</td></tr><tr><td>Retinal Vascular Occlusion</td><td>H34.10, H34.11, H34.12, H34.13, H34.231, H34.232, H34.233, H34.239, H34.811, H34.812, H34.813, H34.819, H34.831, H34.832, H34.833, H34.839</td></tr><tr><td>Scleritis and Episcleritis</td><td>A18.51, H15.021, H15.022, H15.023, H15.029, H15.031, H15.032, H15.033, H15.039, H15.041, H15.042, H15.043, H15.049, H15.051, H15.052, H15.053, H15.059, H15.091, H15.092, H15.093, H15.099</td></tr><tr><td>Separation of Retinal Layers</td><td>H35.711, H35.712, H35.713, H35.719, H35.721, H35.722, H35.723, H35.729, H35.731, H35.732, H35.733, H35.739</td></tr><tr><td>Uveitis</td><td>H44.111, H44.112, H44.113, H44.119, H44.131, H44.132, H44.133, H44.139</td></tr><tr><td>Visual Field Defects</td><td>H53.411, H53.412, H53.413, H53.419</td></tr></table></div>'
                    });

                    $('#PQRSQ10').aToolTip({
                        clickIt: true,
                        tipContent: 'Codes for major complications: 65235, 65800, 65810, 65815, 65860, 65880, 65900, 65920, 65930, 66030, 66250, 66820, 66825, 66830, 66852, 66986, 67005, 67010, 67015, 67025, 67028, 67030, 67031, 67036, 67039, 67041, 67042, 67043, 67101, 67105, 67107, 67108, 67110, 67112, 67141, 67145, 67250, 67255<br />Note: Measure 192 is a reverse performance measure so the lower the performance rate the better.'
                    });
                    
                    $('#PQRSQ12').aToolTip({
                        clickIt: true,
                        tipContent: 'The medical reason must be documented in the medical record. An example of a medical reason is limited life expectancy.'
                    });

                    $('#PQRSQ13').aToolTip({
                        clickIt: true,
                        tipContent: '<strong>Tobacco Use</strong>:<br /><strong>Cessation Counseling Intervention</strong> – Includes use of any type of tobacco.'
                    });

                    $('#PQRSQ14').aToolTip({
                        clickIt: true,
                        tipContent: '<strong>Definition</strong>:<br /><strong>Cessation Counseling Intervention</strong> – Includes brief counseling (3 minutes or less), and/or pharmacotherapy.'
                    });

                    $('#PQRSQ15').aToolTip({
                        clickIt: true,
                        tipContent: 'PQRSPRO would be glad to serve as the third party vendor, as indicated by CMS, for the measures 303 and 304 survey components. We provide automated surveys to be completed easily and anonymously by your patients. If the patient is more comfortable with a paper survey they may complete it on paper and we will enter it on line for them. Contact us at <a href="mailto:PQRSSupport@healthmonix.com">PQRSSupport@healthmonix.com</a> or 610.590.2229 ext 13 for more information.'
                    });

                    $('#PQRSQ17').aToolTip({
                        clickIt: true,
                        tipContent: 'PQRSPRO would be glad to serve as the third party vendor, as indicated by CMS, for the measures 303 and 304 survey components. We provide automated surveys to be completed easily and anonymously by your patients. If the patient is more comfortable with a paper survey they may complete it on paper and we will enter it on line for them. Contact us at <a href="mailto:PQRSSupport@healthmonix.com">PQRSSupport@healthmonix.com</a> or 610.590.2229 ext 13 for more information.'
                    });

                    $('#PQRSQ20').aToolTip({
                        clickIt: true,
                        tipContent: 'For patients who receive the surgical procedures specified in the denominator coding in the sample, it should be reported whether or not the patient had a rupture of the posterior capsule during anterior segment surgery requiring vitrectomy.<br /><br />A lower calculated performance rate for this measure indicates better clinical care or control.'
                    });

                    $('#PQRSQ22').aToolTip({
                        clickIt: true,
                        tipContent: 'Include only procedures performed through September 30 of the reporting period. This will allow the post-operative period to occur within the reporting year.'
                    });

                });

                $('.tip-dialog').click(function (e) {
                    e.preventDefault();
                    $('.tip-dialog-container').dialog({
                        width: 800
                    });
                });

            </script>
        </asp:Panel>
        <!-- History -->
        <table>
            <tr>
                <th colspan="3" width="910px"><p>History</p></th>
            </tr>
            <!-- Question 1 -->
            <tr class="table_body">
                <td width="70%" colspan="2">
                    <strong>1. Date of birth</strong>
                    <asp:Label Visible="false" ID="LabelMonthOfBirth" runat="server" ForeColor="Red" Text="<br />Please Complete Month" />
                    <asp:Label Visible="false" ID='LabelYearOfBirth' ForeColor="Red"  runat='server' Text='<br />Please Complete Year' />
                </td>
                <td>
                    <asp:DropDownList ID="DropDownListMonthOfBirth" runat="server" DataValueField="MonthID" DataTextField="MonthName" />
                    <asp:DropDownList ID='DropDownListYearOfBirth' DataValueField='YearID' DataTextField='YearName' runat='server' />
                </td>
            </tr>
            <!-- Question 2 -->
            <tr class="table_body_bg">
                <td colspan="2"><strong>2. Gender</strong>
                    <asp:Label Visible="false" ID="LabelRBGender" runat="server" ForeColor="Red" 
                        Text="&lt;br /&gt;Please Complete"  />
                </td>
                <td>
                    <asp:RadioButtonList ID="RadioButtonListRBGender" runat="server" 
                        CssClass="aspxList" RepeatDirection="Horizontal">
                        <asp:ListItem Value="42">&nbsp;Male</asp:ListItem>
                        <asp:ListItem Value="43">&nbsp;Female</asp:ListItem>
                    </asp:RadioButtonList>
                </td>
            </tr>
            <!-- Visual Complaint header -->
            <tr class="table_body">
                <td>
                    <strong>3. Visual complaint</strong>
                </td>
                <td>
                    Do symptoms impact activities of daily living?
                    <asp:Label Visible="false" ID="LabelRBSymptomsDailyLiving" runat="server" ForeColor="Red" Text="<br>Please Complete"></asp:Label>    
                </td>
                <td>
                    <asp:RadioButtonList id="RadioButtonListRBSymptomsDailyLiving" RepeatDirection="Horizontal" CssClass="aspxList" runat="server">
                        <asp:ListItem Value="1">&nbsp;Yes</asp:ListItem>
	                    <asp:ListItem Value="2">&nbsp;No</asp:ListItem>
	                    <asp:ListItem Value="3">&nbsp;Not Recorded</asp:ListItem>
                    </asp:RadioButtonList>
                </td>
            </tr>
            <!-- Question 4 -->
            <!-- Medication History header -->
            <tr  class="table_body_bg">
                <td rowspan="2"><strong>4. Medication history</strong></td>
                
                <td>Alpha-1a agonists (i.e. tamsulosin)
                <asp:Label Visible="false" ID="LabelRBHistoryAlpha1aAgonists" runat="server" ForeColor="Red" Text="<br>Please Complete"></asp:Label>
                    
                </td>
                <td><asp:RadioButtonList id="RadioButtonListRBHistoryAlpha1aAgonists" 
                        RepeatDirection="Horizontal" CssClass="aspxList" runat="server">
                        <asp:ListItem Value="1">&nbsp;Yes</asp:ListItem>
	                    <asp:ListItem Value="2">&nbsp;No</asp:ListItem>
	                    <asp:ListItem Value="3">&nbsp;Not Recorded</asp:ListItem>
                    </asp:RadioButtonList></td>
            </tr>
            <!-- Question 6 -->
            <tr class="table_body_bg">
                <td>Anticoagulation
                <asp:Label Visible="false" ID="LabelRBHistoryAnticoag" runat="server" ForeColor="Red" Text="<br />Please Complete"></asp:Label></td>
                <td><asp:RadioButtonList id="RadioButtonListRBHistoryAnticoag" 
                        RepeatDirection="Horizontal" CssClass="aspxList" runat="server">
                        <asp:ListItem Value="1">&nbsp;Yes</asp:ListItem>
	                    <asp:ListItem Value="2">&nbsp;No</asp:ListItem>
	                    <asp:ListItem Value="3">&nbsp;Not Recorded</asp:ListItem>
                    </asp:RadioButtonList></td>
            </tr>
        </table>
        <br />
        <br />
        <!-- Examination -->
        <table>
            <tr>
                <th colspan="2" width="910px"><p>Preoperative Examination</p></th>
            </tr>
            <!-- Question 1 -->
            <tr class="table_body">
                <td width="70%"><strong>1. Corrected visual acuity (with glasses or most recent refraction)</strong>&nbsp;<a href="#"><img id="QE1" src="../../common/images/tip.gif" alt="Tip" /></a>
                <asp:Label Visible="false" ID="LabelBCVA" runat="server" ForeColor="Red" Text="<br />Please Complete"></asp:Label></td>
                    
                <td>  20 / <asp:DropDownList ID='DropDownListBCVA' DataValueField='examValue' DataTextField='examLabel' runat='server' /></td>
            </tr>
            <!-- Question 2 -->
            <tr class="table_body_bg">
                <td style="padding-left:40px;"><strong>2. Refraction performed within last 12 months (or results of recent refraction documented?)</strong> <br />
                <asp:Label Visible="false" ID="LabelRBExamRefraction" runat="server" ForeColor="Red" Text="Please Complete"></asp:Label></td>
                <td><asp:RadioButtonList id="RadioButtonListRBExamRefraction" 
                        RepeatDirection="Horizontal" CssClass="aspxList" runat="server">
                        <asp:ListItem Value="1">&nbsp;Yes</asp:ListItem>
	                    <asp:ListItem Value="2">&nbsp;No</asp:ListItem>
	                    <asp:ListItem Value="3">&nbsp;Not Recorded</asp:ListItem>
                    </asp:RadioButtonList>    </td>
            </tr>
            <!-- Question 3 -->
            <tr class="table_body">
                <td style="padding-left:40px;"><strong>3.  Glare testing</strong>
                <br />
                <asp:Label Visible="false" ID="LabelExamGlareTest" runat="server" ForeColor="Red" Text="Please Complete"></asp:Label>
                </td>
                <td> <asp:RadioButton runat='server' ID='RadioButtonExamGlareTestYes' GroupName='ExamGlareTest ' text='&nbsp;Yes' />
<asp:RadioButton runat='server' ID='RadioButtonExamGlareTestNo' GroupName='ExamGlareTest ' text='&nbsp;No' />
</td>
            </tr>
            <!-- Question 4 -->
            <tr class="table_body_bg">
                <td style="padding-left:40px;"><strong>4. Potential acuity testing</strong><br />
                <asp:Label Visible="false" ID="LabelExamPotentialAcuityTest" runat="server" ForeColor="Red" Text="Please Complete"></asp:Label>
                </td>
                <td> <asp:RadioButton runat='server' ID='RadioButtonExamPotentialAcuityTestYes' GroupName='ExamPotentialAcuityTest ' text='&nbsp;Yes' />
<asp:RadioButton runat='server' ID='RadioButtonExamPotentialAcuityTestNo' GroupName='ExamPotentialAcuityTest ' text='&nbsp;No' />
</td>
            </tr>
            <!-- Question 5 -->
            <tr class="table_body">
                <td><strong>5. Slit lamp exam</strong><br />
                <asp:Label Visible="false" ID="LabelExamSlitLamp" runat="server" ForeColor="Red" Text="Please Complete"></asp:Label></td>
                <td> <asp:RadioButton runat='server' ID='RadioButtonExamSlitLampYes' GroupName='ExamSlitLamp ' text='&nbsp;Yes' />
<asp:RadioButton runat='server' ID='RadioButtonExamSlitLampNo' GroupName='ExamSlitLamp ' text='&nbsp;No' />
</td>
            </tr>
            <!-- Question 6 -->
            <tr class="table_body_bg">
                <td style="padding-left:40px;"><strong>6. Dilated pupil size</strong> <br /><asp:Label Visible="false" ID="LabelRBExamDilatedPupil" runat="server" ForeColor="Red" Text="Please Complete"></asp:Label></td>
                <td><asp:RadioButtonList id="RadioButtonListRBExamDilatedPupil" 
                        RepeatDirection="Vertical" CssClass="aspxList" runat="server">
                        <asp:ListItem Value="243">&nbsp;Adequate</asp:ListItem>
	                    <asp:ListItem Value="244">&nbsp;Small</asp:ListItem>
	                    <asp:ListItem Value="3">&nbsp;Not Recorded</asp:ListItem>
                    </asp:RadioButtonList></td>
            </tr>
            <!-- Question 7 -->
            <tr class="table_body">
                <td style="padding-left:40px;"><strong>7. Corneal endothelium</strong><br /><asp:Label Visible="false" ID="LabelRBExamCornealEndo" runat="server" ForeColor="Red" Text="Please Complete"></asp:Label></td>
                <td><asp:RadioButtonList id='RadioButtonListRBExamCornealEndo' RepeatDirection='Vertical' CssClass='aspxList' runat='server'>
<asp:ListItem Value="104">&nbsp;Normal</asp:ListItem>
	                    <asp:ListItem Value="245">&nbsp;Guttae</asp:ListItem>
	                    <asp:ListItem Value="246">&nbsp;Other Abnormality</asp:ListItem>
	                    <asp:ListItem Value="3">&nbsp;Not Recorded</asp:ListItem>  
</asp:RadioButtonList>
</td>
            </tr>
            <!-- Question 8 -->
            <tr class="table_body_bg">
                <td style="padding-left:40px;"><strong>8. Presence of pseudoexfoliation material</strong><br /><asp:Label Visible="false" ID="LabelExamPseudoexfoliative" runat="server" ForeColor="Red" Text="Please Complete"></asp:Label></td>
                <td><asp:RadioButton runat='server' ID='RadioButtonExamPseudoexfoliativeYes' GroupName='ExamPseudoexfoliative ' text='&nbsp;Yes' />
<asp:RadioButton runat='server' ID='RadioButtonExamPseudoexfoliativeNo' GroupName='ExamPseudoexfoliative ' text='&nbsp;No' />
</td>
            </tr>
            <!-- Question 9 -->
            <tr class="table_body">
                <td style="padding-left:40px;"><strong>9. Cataract grading</strong><br /><asp:Label Visible="false" ID="LabelExamCataractGrading" runat="server" ForeColor="Red" Text="Please Complete"></asp:Label></td>
                <td> <asp:RadioButton runat='server' ID='RadioButtonExamCataractGradingYes' GroupName='ExamCataractGrading ' text='&nbsp;Documented' />
<asp:RadioButton runat='server' ID='RadioButtonExamCataractGradingNo' GroupName='ExamCataractGrading ' text='&nbsp;Not Documented' />
</td>
            </tr>
            <!-- Question 10 -->
            <tr class="table_body_bg">
                <td style="padding-left:40px;"><strong>10. Dilated fundus exam</strong><br /><asp:Label Visible="false" ID="LabelExamDilatedFundus" runat="server" ForeColor="Red" Text="Please Complete"></asp:Label></td>
                <td> <asp:RadioButton runat='server' ID='RadioButtonExamDilatedFundusYes' GroupName='ExamDilatedFundus ' text='&nbsp;Done' />
<asp:RadioButton runat='server' ID='RadioButtonExamDilatedFundusNo' GroupName='ExamDilatedFundus ' text='&nbsp;Not Done' />
</td>
            </tr>
            <!-- Question 11 -->
            <tr class="table_body">
                <td style="padding-left:40px;"><strong>11. Macula</strong><br /><asp:Label Visible="false" ID="LabelRBExamMacula" runat="server" ForeColor="Red" Text="Please Complete"></asp:Label></td>
                <td><asp:RadioButtonList id='RadioButtonListRBExamMacula' RepeatDirection='Vertical' CssClass='aspxList' runat='server'>
<asp:ListItem Value="104">&nbsp;Normal</asp:ListItem>
	                    <asp:ListItem Value="105">&nbsp;Abnormal</asp:ListItem>
	                    <asp:ListItem Value="3">&nbsp;Not Recorded</asp:ListItem> 
</asp:RadioButtonList>
</td>
            </tr>
        </table>
        <br />
        <!-- Pre-Operative Management -->
        <table>
            <tr>
                <th colspan="2" width="910px"><p>Pre-Operative Management</p></th>
            </tr>
            <!-- Question 1 -->
            <tr class="table_body">
                <td width="70%" style="vertical-align: top; padding-top: 30px;"><strong>1. Indication for surgery</strong></td>
                <td>
                    <asp:CheckBox runat='server' ID='CheckBoxPreopIndVisuallySigCataract' text='&nbsp;Visually significant cataract' />
                    <br /><asp:CheckBox runat='server' ID='CheckBoxPreopIndAnisometropia' text='&nbsp;Clinically significant anisometropia in the presence of cataract' />
                    <br /><asp:CheckBox runat='server' ID='CheckBoxPreopIndLensOpacity' text='&nbsp;Lens opacity interfering with diagnosis or management of  posterior segment conditions' />
                    <br /><asp:CheckBox runat='server' ID='CheckBoxPreopIndLensCausingInflam' text='&nbsp;Lens causing inflammation' />
                    <br /><asp:CheckBox runat='server' ID='CheckBoxPreopIndNarrowAngle' text='&nbsp;Lens inducing narrow angle or angle closure' />
                    <br /><asp:CheckBox runat='server' ID='CheckBoxPreopIndOther' text='&nbsp;Other' />
                    <br /><asp:CheckBox runat='server' ID='CheckBoxPreopIndNotDoc' text='&nbsp;Not documented' />
                        
                </td>
            </tr>
            <!-- Question 2 -->
            <tr class="table_body_bg">
                <td><strong>2. Were risks, benefits and alternatives to surgery discussed?</strong><br /><asp:Label Visible="false" ID="LabelRBPreopRisksDiscussed" runat="server" ForeColor="Red" Text="Please Complete"></asp:Label></td>
                <td><asp:RadioButtonList id='RadioButtonListRBPreopRisksDiscussed' RepeatDirection='Horizontal' CssClass='aspxList' runat='server'>
                        <asp:ListItem Value="1">&nbsp;Yes</asp:ListItem>
	                    <asp:ListItem Value="2">&nbsp;No</asp:ListItem>
	                    <asp:ListItem Value="3">&nbsp;Not Recorded</asp:ListItem>
                    </asp:RadioButtonList>
                            </td>
                    </tr>
                    <!-- Question 3 -->
                    <tr class="table_body">
                        <td><strong>3. Have the refractive goals and/or options for intraocular lenses and options been explained to the patient?              </strong><br /><asp:Label Visible="false" ID="LabelRBPreopGoalsExplained" runat="server" ForeColor="Red" Text="Please Complete"></asp:Label></td>
                        <td>
                            <asp:RadioButtonList id='RadioButtonListRBPreopGoalsExplained' RepeatDirection='Horizontal' CssClass='aspxList' runat='server'>
                                <asp:ListItem Value="1">&nbsp;Yes</asp:ListItem>
	                            <asp:ListItem Value="2">&nbsp;No</asp:ListItem>
	                            <asp:ListItem Value="3">&nbsp;Not Recorded</asp:ListItem>
                            </asp:RadioButtonList>
                        </td>
            </tr>
            <!-- Question 4 -->
            <tr class="table_body_bg">
                <td><strong>4. Was signed informed consent obtained?</strong><br /><asp:Label Visible="false" ID="LabelRBPreopSignedConsent" runat="server" ForeColor="Red" Text="Please Complete"></asp:Label></td>
                <td><asp:RadioButtonList id='RadioButtonListRBPreopSignedConsent' RepeatDirection='Horizontal' CssClass='aspxList' runat='server'>
<asp:ListItem Value="1">&nbsp;Yes</asp:ListItem>
	                    <asp:ListItem Value="2">&nbsp;No</asp:ListItem>
	                    <asp:ListItem Value="3">&nbsp;Not Recorded</asp:ListItem>
</asp:RadioButtonList>
</td>
            </tr>
            <!-- Question 5 -->
            <tr class="table_body">
                <td><strong>5. Method of axial length measurement</strong></td>
                <td>
                <asp:CheckBox runat='server' ID='CheckBoxPreopMeasureContactUltrasound' text='&nbsp;Contact Ultrasound ' /><br />
                <asp:CheckBox runat='server' ID='CheckBoxPreopMeasureImmersionUltrasound' text='&nbsp;Immersion Ultrasound' /><br />
                <asp:CheckBox runat='server' ID='CheckBoxPreopMeasureOpticalBiometry' text='&nbsp;Optical Biometry (i.e. IOLMaster, Lenstar) ' /><br />
                <asp:CheckBox runat='server' ID='CheckBoxPreopMeasureOther' text='&nbsp;Other' /><br />
</td>
            </tr>
            <!-- Question 5a -->
            <tr class="table_body">
                <td><strong><span id="QPO5aA">5a. If other, please specify:</span></strong></td>
                <td>
                    <asp:TextBox runat='server' ID='TextBoxPreopMeasureOtherText' size='' width="240px"  />
                </td>
            </tr>
            <!-- Question 6 -->
            <tr class="table_body_bg">
                <td><strong>6. Method of keratometry</strong></td>
                <td>
                    <asp:CheckBox runat='server' ID='CheckBoxPreopKerIOLMaster' text='&nbsp;Manual Keratometry' /><br />
                    <asp:CheckBox runat='server' ID='CheckBoxPreopKerCornealTop' text='&nbsp;Corneal Topography' /><br />
                    <asp:CheckBox runat='server' ID='CheckBoxPreopKerHandheldKer' text='&nbsp;Optical Biometry (i.e. IOLMaster, Lenstar) ' /><br />
                    <asp:CheckBox runat='server' ID='CheckBoxPreopKerManualKer' text='&nbsp;Other automated keratometer (i.e. hand held device)' /><br />
                    <asp:CheckBox runat='server' ID='CheckBoxPreopKerOther' text='&nbsp;Other' /><br />
                </td>
            </tr>
            <!-- Question 6a -->
            <tr class="table_body_bg">
                <td>
                    <strong><span id="QPO6aA">6a. If other, please specify:</span></strong>
                </td>
                    
                <td>
                    <asp:TextBox runat='server' ID='TextBoxPreopKerOtherText' size='' width="240px"  />
                </td>
            </tr>
            <!-- Question 7 -->
            <tr class="table_body">
                <td><strong>7. What formula was used for IOL power calculation?</strong></td>
                <td>
                    <asp:CheckBox runat='server' ID='CheckBoxPreopFormulaHaigis' text='&nbsp;Haigis' /><br />
                    <asp:CheckBox runat='server' ID='CheckBoxPreopFormulaHofferQ' text='&nbsp;Hoffer Q' /><br />
                    <asp:CheckBox runat='server' ID='CheckBoxPreopFormulaHolladay' text='&nbsp;Holladay' /><br />
                    <asp:CheckBox runat='server' ID='CheckBoxPreopFormulaHolladayII' text='&nbsp;Holladay II' /><br />
                    <asp:CheckBox runat='server' ID='CheckBoxPreopFormulaSRKT' text='&nbsp;SRK-T' /><br />
                    <asp:CheckBox runat='server' ID='CheckBoxPreopFormulaOther' text='&nbsp;Other ' /></td>
            </tr>
            <!-- Question 7a -->
            <tr class="table_body">
                <td><strong><span id="QPrO7aA">7a. If other, please specify:</span></strong></td>
                <td>
                    <asp:textBox runat='server' ID='TextBoxPreopFormulaOtherText' width="240px"  />
                </td>
            </tr>
        </table>
        <br />
        <!-- Management and Post-Operative Care -->
        <table>
            <tr>
                <th colspan="2" width="910px"><p>Management and Post-Operative Care</p></th>
            </tr>
            <!-- Question 1 -->
            <tr class="table_body">
                <td width="70%"><strong>1. Date of surgery</strong><br /><asp:Label Visible="false" ID="LabelMonthOfSurgery" runat="server" ForeColor="Red" Text="Please Complete Month"></asp:Label><br />
                <asp:Label Visible="false" ID="LabelYearOfSurgery" runat="server" ForeColor="Red" Text="Please Complete Year"></asp:Label><br />
                <asp:Label Visible="false" ID="Labelinitialage" runat="server" ForeColor="Red" 
                        Text=""></asp:Label>
                    <br />
                </td>
                <td><asp:DropDownList ID='DropDownListMonthOfSurgery' DataValueField="MonthID" DataTextField="MonthName"  runat='server' />
                    <asp:DropDownList ID='DropDownListYearOfSurgery' DataValueField='YearID' DataTextField='YearName'  runat='server' />
                </td>
            </tr>
            <!-- Question 2 -->
            <tr class="table_body_bg">
                <td>
                    <strong>2. Which surgical intervention was used?</strong>
                    <asp:Label Visible="false" ID="LabelRBMgntSurgery" runat="server" ForeColor="Red" Text="<br />Please Complete" />
                </td>
                <td>
                    <asp:RadioButtonList id='RadioButtonListRBMgntSurgery' RepeatDirection='Vertical' CssClass='aspxList' runat='server'>
                        <asp:ListItem Value='247'>&nbsp;Large incision extracapsular extraction </asp:ListItem>
                        <asp:ListItem Value='248'>&nbsp;Manual small incision extracapsular extraction (i.e. SICS)</asp:ListItem>
                        <asp:ListItem Value='249'>&nbsp;Phacoemulsification</asp:ListItem>
                    </asp:RadioButtonList>
                </td>
            </tr>
            <!-- Question 3 -->
            <tr class="table_body">
                <td><strong>3. What type of anesthesia was utilized?</strong></td>
                <td>
                    <asp:CheckBox ID="CheckBoxAnesthesiaGeneralAnesthesia" Text="General anesthesia" runat="server" /><br />
                    <asp:CheckBox ID="CheckBoxAnesthesiaPeribularInjection" Text="Peribular injection" runat="server" /><br />
                    <asp:CheckBox ID="CheckBoxAnesthesiaRetrobulbarInjection" Text="Retrobulbar injection" runat="server" /><br />
                    <asp:CheckBox ID="CheckBoxAnesthesiaTopical" Text="Topical" runat="server" /><br />
                    <asp:CheckBox ID="CheckBoxAnesthesiaTopicalWithIntracameral"  Text="Topical with intracameral" runat="server" /><br />
                    <asp:CheckBox ID="CheckBoxAnesthesiaSubTenon" Text="Sub tenon" runat="server" /><br />
                    <asp:CheckBox ID="CheckBoxAnesthesiaBoxOther" Text="Other" runat="server" /><br />
                </td>
            </tr>
            <!-- Question 3a -->
            <tr class="table_body">
                <td>
                    <strong><span id="QPO4aA">3a. If other, please specify:</span></strong>
                    <asp:Label Visible="false" ID="LabelRBMgntAnesthesia" runat="server" ForeColor="Red" Text="<br />Please Complete"></asp:Label>
                </td>
                <td>
                    <asp:TextBox runat='server' ID='TextBoxRBMgntAnesthesiaOtherText' width="240px"  />
                </td>
            </tr>
            <!-- Question 4 -->
            <tr class="table_body_bg">
                <td><strong>4. What refractive target was selected to the nearest 0.25 diopters?</strong>
                <asp:Label Visible="false" ID="LabelMgntTargetRefractionSign" runat="server" ForeColor="Red" Text="<br />Please Complete"></asp:Label></td>
                <td> 
                    <asp:RadioButton runat='server' ID='RadioButtonMgntTargetRefractionSignYes' GroupName='MgntTargetRefractionSign ' text='&nbsp;+' />
                    <asp:RadioButton runat='server' ID='RadioButtonMgntTargetRefractionSignNo' GroupName='MgntTargetRefractionSign ' text='&nbsp;-' />
                    &nbsp;<asp:TextBox ID="TextBoxMgntTargetRefraction" class="auto" runat="server" width="150" />&nbsp;d
            </td>
            </tr>
            <!-- Question 6 -->
            <tr class="table_body">
                <td><strong>5. IOL Placement</strong><asp:Label Visible="false" ID="LabelRBMgntIOLPlacement" runat="server" ForeColor="Red" Text="<br />Please Complete"></asp:Label></td>
                <td><asp:RadioButtonList id='RadioButtonListRBMgntIOLPlacement' RepeatDirection='Vertical' CssClass='aspxList' runat='server'>
                        <asp:ListItem Value='256'>&nbsp;Anterior chamber</asp:ListItem>
                        <asp:ListItem Value='257'>&nbsp;Iris fixated</asp:ListItem>
                        <asp:ListItem Value='258'>&nbsp;Posterior capsule</asp:ListItem>
                        <asp:ListItem Value='259'>&nbsp;Sulcus</asp:ListItem>
                        <asp:ListItem Value='260'>&nbsp;Sulcus with suture fixation</asp:ListItem>
                    </asp:RadioButtonList>
                </td>
            </tr>
            <!-- Question 7 -->
            <tr class="table_body_bg">
                <td><strong>6. Which intraoperative complications were encountered?</strong></td>
                <td>
                    <asp:CheckBox runat='server' ID='CheckBoxComplicationNone' text='&nbsp;None' /> <br />
                    <asp:CheckBox runat='server' ID='CheckBoxComplicationIrisProlapse' text='&nbsp;Iris prolapse or injury' /> <br />
                    <asp:CheckBox runat='server' ID='CheckBoxComplicationLossLensFragments' text='&nbsp;Loss of lens fragments into vitreous' /> <br />
                    <asp:CheckBox runat='server' ID='CheckBoxComplicationPosteriorRupture' text='&nbsp;Posterior capsular rupture ' /> <br />
                    <asp:CheckBox runat='server' ID='CheckBoxComplicationSupraHemorrhage' text='&nbsp;Suprachoroidal ' /> <br />
                    <asp:CheckBox runat='server' ID='CheckBoxComplicationVitreousLoss' text='&nbsp;Vitreous loss' /> <br />
                    <asp:CheckBox runat='server' ID='CheckBoxComplicationZonular' text='&nbsp;Zonular dehiscence' /> <br />
                    <asp:CheckBox runat='server' ID='CheckBoxComplicationOther' text='&nbsp;Other' /> <br />
                </td>
            </tr>
            <!-- Question 7a -->
            <tr class="table_body_bg">
                <td><strong><span id="QPO7aA">6a. If other, please specify:</span></strong></td>
                <td>
                    <asp:TextBox runat='server' ID='TextBoxComplicationOtherText' Enabled="false"  size='' width="240px" />
                </td>
            </tr>
            <!-- Question 8 -->
            <tr class="table_body">
                <td><strong>7. Which adjunctive procedures were necessary:</strong></td>
                <td>
                    <asp:CheckBox runat='server' ID='CheckBoxAdjProcNone' text='&nbsp;None' /> <br />
                    <asp:CheckBox runat='server' ID='CheckBoxAdjProcAnteriorVitrectomy' text='&nbsp;Anterior vitrectomy' /> <br />
                    <asp:CheckBox runat='server' ID='CheckBoxAdjProcCapsularTension' text='&nbsp;Capsular tension ring' /> <br />
                    <asp:CheckBox runat='server' ID='CheckBoxAdjProcIrisRetractorExp' text='&nbsp;Iris retractors or expanders' /> <br />
                    <asp:CheckBox runat='server' ID='CheckBoxAdjProcOther' text='&nbsp;Other' /> <br />
                </td>
            </tr>
            <!-- Question 8a -->
            <tr class="table_body">
                <td><strong><span id="QPO8aA">7a. If other, please specify:</span></strong></td>
                <td><asp:TextBox runat='server' ID='TextBoxAdjProcOtherText' Enabled="false"  width="240px"  size='' />

                    </td>
            </tr>
            <!-- Question 9 -->
            <tr class="table_body_bg">
                <td>
                    <strong>8. Were intracameral or subconjunctival antibiotics given at the time of surgery?</strong>
                    <asp:Label Visible="false" ID="LabelIntracameralAntibiotics" runat="server" ForeColor="Red" Text="<br />Please Complete"></asp:Label>
                </td>
                <td> <asp:RadioButton runat='server' ID='RadioButtonIntracameralAntibioticsYes' GroupName='IntracameralAntibiotics ' text='&nbsp;Yes' />
<asp:RadioButton runat='server' ID='RadioButtonIntracameralAntibioticsNo' GroupName='IntracameralAntibiotics ' text='&nbsp;No' />
</td>
            </tr>
        </table>
        <br />
        <!-- NOTE: THE NEXT FIVE QUESTIONS PERTAIN SPECIFICALLY TO  POST-OPERATIVE CARE -->
        <table>
            <tr>
                <th colspan="2" width="910px"><p>Post-Operative Care</p></th></tr><!-- Question 1 --><tr class="table_body">
                <td width="70%"><strong>1. Was the patient seen within 48 hours of surgery?<br />
                    </strong><asp:Label Visible="false" ID="LabelPostop48Hours" runat="server" ForeColor="Red" Text="Please Complete"></asp:Label></td><td> <asp:RadioButton runat='server' ID='RadioButtonPostop48HoursYes' GroupName='Postop48Hours ' text='&nbsp;Yes' />
<asp:RadioButton runat='server' ID='RadioButtonPostop48HoursNo' GroupName='Postop48Hours ' text='&nbsp;No' />
</td>
            </tr>
            <!-- Question 2 -->
            <tr class="table_body_bg">
                <td><strong>2. Was topical antibiotic medication prescribed?</strong><br />
                <asp:Label Visible="false" ID="LabelPostopAntibioticDrops" runat="server" ForeColor="Red" Text="Please Complete"></asp:Label>
                </td>
                <td> <asp:RadioButton runat='server' ID='RadioButtonPostopAntibioticDropsYes' GroupName='PostopAntibioticDrops' text='&nbsp;Yes' />
                    <asp:RadioButton runat='server' ID='RadioButtonPostopAntibioticDropsNo' GroupName='PostopAntibioticDrops' text='&nbsp;No' />
                </td>
            </tr>
                 
            <!-- Question 2a -->
            <tr class="table_body_bg">
                <td>
                    <span id="QP2aA"><strong>2a. If Yes, was this medication prescribed to be given postoperatively on the same day as surgery?</strong></span>
                    <asp:Label Visible="false" ID="LabelRBPostopAntibioticDropsSameDay" runat="server" ForeColor="Red" Text="<br />Please Complete"></asp:Label>
                </td>
                <td>
                    <span id="QP2aB">
                    <asp:RadioButtonList id='RadioButtonListRBPostopAntibioticDropsSameDay' RepeatDirection='Horizontal' CssClass='aspxList' runat='server'>
                        <asp:ListItem Value="1">&nbsp;Yes</asp:ListItem>
	                    <asp:ListItem Value="2">&nbsp;No</asp:ListItem>
	                    <asp:ListItem Value="3">&nbsp;Not Recorded</asp:ListItem>
                    </asp:RadioButtonList>
                    </span>
                </td>
            </tr>
            <!-- Question 3 -->
            <tr class="table_body">
                <td><strong>3. Was topical anti-iflammatory medication prescribed?</strong><br /> <asp:Label Visible="false" ID="LabelPostopAntiImflamDrops" runat="server" ForeColor="Red" Text="Please Complete"></asp:Label></td>
                <td> <asp:RadioButton runat='server' ID='RadioButtonPostopAntiImflamDropsYes' GroupName='PostopAntiImflamDrops ' text='&nbsp;Yes' />
<asp:RadioButton runat='server' ID='RadioButtonPostopAntiImflamDropsNo' GroupName='PostopAntiImflamDrops ' text='&nbsp;No' />
</td>
            </tr>
            <!-- Question 4 -->
            <tr class="table_body_bg">
                <td><strong>4. Which immediate (<48 hours) post-operative complications were encountered?</strong></td>
                <td>
                    <asp:CheckBox runat='server' ID='CheckBoxPostopComplicationSTNone' text='&nbsp;None' /><br />
                    <asp:CheckBox runat='server' ID='CheckBoxPostopComplicationSTElevatedPressure' text='&nbsp;Elevated intraocular pressure (&gt;30mm HG)' /><br />
                    <asp:CheckBox runat='server' ID='CheckBoxPostopComplicationSTHyphema' text='&nbsp;Hyphema' /><br />
                    <asp:CheckBox runat='server' ID='CheckBoxPostopComplicationSTWoundLeak' text='&nbsp;Wound Leak' /><br />
                    <asp:CheckBox runat='server' ID='CheckBoxPostopComplicationSTOther' text='&nbsp;Other' /><br />
                </td>
            </tr>
            <!-- Question 4a -->
            <tr class="table_body_bg">
                <td><strong><span id="QP4aA">4a. If &quot;Other&quot;, please specify:</span></strong></td>
                <td>
                    <asp:TextBox runat='server' ID='TextBoxPostopComplicationSTOther' width="240px" /><br />
                </td>
            </tr>
            <!-- Question 5 -->
            <tr class="table_body">
                <td><strong>5. Were verbal and/or written instructions given on the symptoms that should lead the patient to contact the ophthalmologist immediately?</strong><br /> <asp:Label Visible="false" ID="LabelRBPostopInstructions" runat="server" ForeColor="Red" Text="Please Complete"></asp:Label></td>
                <td><asp:RadioButtonList id='RadioButtonListRBPostopInstructions' RepeatDirection='Horizontal' CssClass='aspxList' runat='server'>
<asp:ListItem Value="1">&nbsp;Yes</asp:ListItem>
	                    <asp:ListItem Value="2">&nbsp;No</asp:ListItem>
	                    <asp:ListItem Value="3">&nbsp;Not Recorded</asp:ListItem>
</asp:RadioButtonList>
</td>
            </tr>
        </table>
        <br />
        <!-- Outcomes -->
        <table>
            <tr>
                <th colspan="2" width="910px"><p>Outcomes</p></th></tr><!-- Question 1 --><tr class="table_body">
                <td width="70%" style="vertical-align: top; padding-top: 30px;"><strong>1. Were any major post-operative complications encountered?</strong></td>
                <td>
                    <asp:CheckBox runat='server' ID='CheckBoxPostopComplicationNone' text='&nbsp;None' /><br />
                    <asp:CheckBox runat='server' ID='CheckBoxPostopComplicationCystoidEdema' text='&nbsp;Cystoid macular edema' /><br />
                    <asp:CheckBox runat='server' ID='CheckBoxPostopComplicationDislocatedIOL' text='&nbsp;Dislocated IOL' /><br />
                    <asp:CheckBox runat='server' ID='CheckBoxPostopComplicationEndophthalmitis' text='&nbsp;Endophthalmitis' /><br />
                    <asp:CheckBox runat='server' ID='CheckBoxPostopComplicationPseudophakic' text='&nbsp;Pseudophakic bullous keratopathy' /><br />
                    <asp:CheckBox runat='server' ID='CheckBoxPostopComplicationLensFragAnterior' text='&nbsp;Retained lens fragments (anterior chamber)' /><br />
                    <asp:CheckBox runat='server' ID='CheckBoxPostopComplicationLensFragVitreous' text='&nbsp;Retained lens fragments (vitreous cavity)' /><br />
                    <asp:CheckBox runat='server' ID='CheckBoxPostopComplicationRetinalDetach' text='&nbsp;Retinal detachment' /><br />
                    <asp:CheckBox runat='server' ID='CheckBoxPostopComplicationTASS' text='&nbsp;Toxic anterior segment syndrome (TASS)' /><br />
                    <asp:CheckBox runat='server' ID='CheckBoxPostopComplicationWoundDehiscence' text='&nbsp;Wound dehiscence' /><br />
                    <asp:CheckBox runat='server' ID='CheckBoxPostopComplicationWrongPowerIOL' text='&nbsp;Wrong power IOL' /><br />
                    <asp:CheckBox runat='server' ID='CheckBoxPostopComplicationOther' text='&nbsp;Other' />
                </td>
            </tr>
            <!-- Question 1a -->
            <tr class="table_body">
                <td>
                    <strong><span id="QO1aA">1a. If other, please specify:</span></strong>
                </td>
                <td>
                    <asp:TextBox runat='server' ID='TextBoxPostopComplicationOtherText'  size='' width="240px" />
                </td>
            </tr>
            <!-- Question 2 -->
            <tr class="table_body_bg">
                <td><strong>2. Was further surgery necessary within 90 days due to a complication of the cataract surgery?</strong><br /><asp:Label  Visible="false" ID="LabelPostopFurtherSurgery" runat="server" ForeColor="Red" Text="Please Complete"></asp:Label></td>
                <td>  
                    <asp:RadioButton runat='server' ID='RadioButtonPostopFurtherSurgeryYes' GroupName='PostopFurtherSurgery ' text='&nbsp;Yes' />
                    <asp:RadioButton runat='server' ID='RadioButtonPostopFurtherSurgeryNo' GroupName='PostopFurtherSurgery ' text='&nbsp;No' />
                </td>
            </tr>
            <!-- Question 2a -->
            <tr class="table_body_bg">
                <td><strong><span id="QO2aA">2a. If yes, specify reason and procedure below:</span></strong></td>
                <td><asp:TextBox runat='server' ID='TextBoxPostopFurtherSurgeryText' size='' 
                        width="240px" />
</td>
            </tr>
            <!-- Question 3 -->
            <tr class="table_body">
                <td><strong>3. Best corrected visual acuity within 90 days</strong>&nbsp;<a href="#"><img id="QO3" src="../../common/images/tip.gif" alt="Tip" /></a>
                <asp:Label Visible="false" ID="LabelPostopBCVA" runat="server" ForeColor="Red" Text="<br />Please Complete"></asp:Label>
                </td>
                <td> 20 / <asp:DropDownList ID='DropDownListPostopBCVA' DataValueField='examValue' DataTextField='examLabel' runat='server' /></td>
            </tr>
            <!-- Question 4 -->
            <tr class="table_body_bg">
                <td><strong>4. Refracted spherical equivalent (to the nearest 0.25 diopters)</strong>
                <asp:Label Visible="false" ID="LabelPostopSphericalEquivalentSign" runat="server" ForeColor="Red" Text="<br />Please Complete"></asp:Label>
                    
                </td>
                <td> 
                    <asp:RadioButton runat='server' ID='RadioButtonPostopSphericalEquivalentSignYes' GroupName='PostopSphericalEquivalentSign ' text='&nbsp;+' />
                        <asp:RadioButton runat='server' ID='RadioButtonPostopSphericalEquivalentSignNo' GroupName='PostopSphericalEquivalentSign ' text='&nbsp;-' />&nbsp;<asp:TextBox runat='server' ID='TextBoxPostopSphericalEquivalent' class="auto" width="150"  />d
                    </td>
            </tr>
            <!-- Question 5 -->
            <tr class="table_body">
                <td><strong>5. Did the patient have a pre-existing co-morbid condition that affected the postoperative visual acuity?
                </strong><asp:Label Visible="false" ID="LabelPostopVACoMorbid" runat="server" ForeColor="Red" Text="<br />Please Complete"></asp:Label></td>
                <td> 
                        <asp:RadioButton runat='server' ID='RadioButtonPostopVACoMorbidYes' GroupName='PostopVACoMorbid ' text='&nbsp;Yes' />
                        <asp:RadioButton runat='server' ID='RadioButtonPostopVACoMorbidNo' GroupName='PostopVACoMorbid ' text='&nbsp;No' />

</td>
            </tr>
            <!-- Question 6 -->
            <tr class="table_body_bg">
                <td>
                    <strong><span id="QO6A">6. If above acuity is worse than 20/40, what is the reason?</span></strong>
                </td>
                <td>
                    <span id="QO6B">
                    <asp:CheckBox runat='server' ID='CheckBoxPostopVAReasonCornealDisease' text='&nbsp;Corneal disease' /><br />
                    <asp:CheckBox runat='server' ID='CheckBoxPostopVAReasonOpticNerve' text='&nbsp;Optic nerve disease' /><br />
                    <asp:CheckBox runat='server' ID='CheckBoxPostopVAReasonRetinalDisease' text='&nbsp;Retinal disease' /><br />
                    <asp:CheckBox runat='server' ID='CheckBoxPostopVAReasonND' text='&nbsp;Not documented' /><br />
                    <asp:CheckBox runat='server' ID='CheckBoxPostopVAReasonUndetermined' text='&nbsp;Unable to determine' /><br />
                    <asp:CheckBox runat='server' ID='CheckBoxPostopVAReasonOther' text='&nbsp;Other' /><br />
                    </span>
                </td>
            </tr>
            <!-- Question 6a -->
            <tr class="table_body_bg">
                <td>
                    <strong><span id="QO6aA">6a. If other, please specify:</span></strong>
                </td>
                <td>
                    <asp:TextBox runat='server' ID='TextBoxPostopVAReasonOtherText'  size='' width="240px"  />
                </td>
            </tr>
        </table>
        <br />
        <div class="bginput"><asp:LinkButton ID="LinkButtonBackToDashboard" runat="server" Text="Back To Chart Registration" PostBackUrl="../PatientChartRegistration.aspx?CycleNumber=1" Visible="false"/></div>
        <asp:Button ID="ButtonSubmit" OnClientClick="return ButtonSubmit_PQRS();" OnClick="ButtonSubmit_Click" runat="server" Text="Submit Chart" CssClass="button" />
    </div>
    <!-- ION pim ends -->

    <script type="text/javascript" src="../../common/js/jquery.metadata.js"></script> <!--when changing defaults-->
    <script type="text/javascript" src="../../common/js/autoNumeric-1.7.5.js"></script>
	<script type="text/javascript" src="../../common/js/jquery.atooltip.js"></script>
    <script type="text/javascript" src="../../common/js/jquery.atooltip2.js"></script>
    <script type="text/javascript">
        //      Either enables or disables input boxes
        function enabledControl(el, disabled, checked) {
            //alert("Hello world from enabled control");
            try {
                if (disabled) {
                    el.disabled = disabled;
                    el.setAttribute("disabled", true);
                }
                else {
                    el.disabled = disabled;
                    el.removeAttribute("disabled");
                }

                if (checked == true)
                    el.checked = false;
            }
            catch (E) {
            }
            if (el.childNodes && el.childNodes.length > 0) {
                for (var x = 0; x < el.childNodes.length; x++) {
                    enabledControl(el.childNodes[x], disabled, checked);
                }
            }
        }
        //      Either enables or disables dropdown boxes
        function enabledControlDropDown(el, disabled, checked) {
            //alert("Hello world from enabled control");
            try {
                if (disabled) {
                    el.disabled = disabled;
                    el.setAttribute("disabled", true);
                }
                else {
                    el.disabled = disabled;
                    el.removeAttribute("disabled");
                }

                if (checked == true)
                    el.options[0].selected = true;
            }
            catch (E) {
            }
            if (el.childNodes && el.childNodes.length > 0) {
                for (var x = 0; x < el.childNodes.length; x++) {
                    enabledControl(el.childNodes[x], disabled, checked);
                }
            }
        }
        //      Either enables or disables text boxes
        function enabledControlTextField(el, disabled, checked) {
            //alert("Hello world from enabled control");
            try {
                if (disabled) {
                    el.disabled = disabled;
                    el.setAttribute("disabled", true);
                }
                else {
                    el.disabled = disabled;
                    el.removeAttribute("disabled");
                }

                if (checked == true) {
                    el.value = '';
                }
            }
            catch (E) {
            }
            if (el.childNodes && el.childNodes.length > 0) {
                for (var x = 0; x < el.childNodes.length; x++) {
                    enabledControl(el.childNodes[x], disabled, checked);
                }
            }
        }

        function generate(arr1, arr2, istrue) {
            if (istrue == true) {
                for (var i = 0; i < arr1.length; i++) {
                    document.getElementById(arr1[i]).style.color = "gray";
                }
                for (var i = 0; i < arr2.length; i++) {
                    $(arr2[i] + ' :input').attr('disabled', true);
                    $(arr2[i] + ' :input').removeAttr("checked");
                }
            }
            if (istrue == false) {
                for (var i = 0; i < arr1.length; i++) {
                    document.getElementById(arr1[i]).style.color = "#333";
                }
                for (var i = 0; i < arr2.length; i++) {
                    $(arr2[i] + ' :input').removeAttr('disabled');
                }
            }
        }
    </script>

    <script  type="text/javascript" language="javascript">
        $(document).ready(function () {

            // Question 5, other - Pre-Operative Management
            $("#<%= CheckBoxPreopMeasureOther.ClientID %>").click(function () {
                if ($('#<%= CheckBoxPreopMeasureOther.ClientID %>').prop('checked') == true) {
                    var myaray = new Array("QPO5aA");
                    var myarray2 = new Array('#QPO5aA');
                    generate(myaray, myarray2, false);

                    enabledControlTextField(document.getElementById("<%= TextBoxPreopMeasureOtherText.ClientID %>"), false, false);
                }
                else {
                    var myaray = new Array("QPO5aA");
                    var myarray2 = new Array('#QPO5aA');
                    generate(myaray, myarray2, true);

                    enabledControlTextField(document.getElementById("<%= TextBoxPreopMeasureOtherText.ClientID %>"), true, true);
                }
            });
            if ($('#<%= CheckBoxPreopMeasureOther.ClientID %>').prop('checked') == true) {
                var myaray = new Array("QPO5aA");
                var myarray2 = new Array('#QPO5aA');
                generate(myaray, myarray2, false);

                enabledControlTextField(document.getElementById("<%= TextBoxPreopMeasureOtherText.ClientID %>"), false, false);
            }
            else {
                var myaray = new Array("QPO5aA");
                var myarray2 = new Array('#QPO5aA');
                generate(myaray, myarray2, true);

                enabledControlTextField(document.getElementById("<%= TextBoxPreopMeasureOtherText.ClientID %>"), true, true);
            }


            // Question 6, other - Pre-Operative Management
            $("#<%= CheckBoxPreopKerOther.ClientID %>").click(function () {
                if ($('#<%= CheckBoxPreopKerOther.ClientID %>').prop('checked') == true) {
                    var myaray = new Array("QPO6aA");
                    var myarray2 = new Array('#QPO6aA');
                    generate(myaray, myarray2, false);

                    enabledControlTextField(document.getElementById("<%= TextBoxRBMgntAnesthesiaOtherText.ClientID %>"), false, false);
                }
                else {
                    var myaray = new Array("QPO6aA");
                    var myarray2 = new Array('#QPO6aA');
                    generate(myaray, myarray2, true);

                    enabledControlTextField(document.getElementById("<%= TextBoxRBMgntAnesthesiaOtherText.ClientID %>"), true, true);
                }
            });
            if ($('#<%= CheckBoxPreopKerOther.ClientID %>').prop('checked') == true) {
                var myaray = new Array("QPO6aA");
                var myarray2 = new Array('#QPO6aA');
                generate(myaray, myarray2, false);

                enabledControlTextField(document.getElementById("<%= TextBoxRBMgntAnesthesiaOtherText.ClientID %>"), false, false);
            }
            else {
                var myaray = new Array("QPO6aA");
                var myarray2 = new Array('#QPO6aA');
                generate(myaray, myarray2, true);

                enabledControlTextField(document.getElementById("<%= TextBoxRBMgntAnesthesiaOtherText.ClientID %>"), true, true);
            }


            // Question 7, other - Pre-Operative Management
            $("#<%= CheckBoxPreopFormulaOther.ClientID %>").click(function () {
                if ($('#<%= CheckBoxPreopFormulaOther.ClientID %>').prop('checked') == true) {
                    var myaray = new Array("QPrO7aA");
                    var myarray2 = new Array('#QPrO7aA');
                    generate(myaray, myarray2, false);

                    enabledControlTextField(document.getElementById("<%= TextBoxPreopFormulaOtherText.ClientID %>"), false, false);
                }
                else {
                    var myaray = new Array("QPrO7aA");
                    var myarray2 = new Array('#QPrO7aA');
                    generate(myaray, myarray2, true);

                    enabledControlTextField(document.getElementById("<%= TextBoxPreopFormulaOtherText.ClientID %>"), true, true);
                }
            });
            if ($('#<%= CheckBoxPreopFormulaOther.ClientID %>').prop('checked') == true) {
                var myaray = new Array("QPrO7aA");
                var myarray2 = new Array('#QPrO7aA');
                generate(myaray, myarray2, false);

                enabledControlTextField(document.getElementById("<%= TextBoxPreopFormulaOtherText.ClientID %>"), false, false);
            }
            else {
                var myaray = new Array("QPrO7aA");
                var myarray2 = new Array('#QPrO7aA');
                generate(myaray, myarray2, true);

                enabledControlTextField(document.getElementById("<%= TextBoxPreopFormulaOtherText.ClientID %>"), true, true);
            }


            // Question 4a, other - Management and Post-Operative care




            // Question 7a, other - Management and Post-Operative care
            $("#<%= CheckBoxComplicationOther.ClientID %>").click(function () {
                if ($('#<%= CheckBoxComplicationOther.ClientID %>').prop('checked') == true) {
                    var myaray = new Array("QPO7aA");
                    var myarray2 = new Array('#QPO7aA');
                    generate(myaray, myarray2, false);

                    enabledControlTextField(document.getElementById("<%= TextBoxComplicationOtherText.ClientID %>"), false, false);
                }
                else {
                    var myaray = new Array("QPO7aA");
                    var myarray2 = new Array('#QPO7aA');
                    generate(myaray, myarray2, true);

                    enabledControlTextField(document.getElementById("<%= TextBoxComplicationOtherText.ClientID %>"), true, true);
                }
            });
            if ($('#<%= CheckBoxComplicationOther.ClientID %>').prop('checked') == true) {
                var myaray = new Array("QPO7aA");
                var myarray2 = new Array('#QPO7aA');
                generate(myaray, myarray2, false);

                enabledControlTextField(document.getElementById("<%= TextBoxComplicationOtherText.ClientID %>"), false, false);
            }
            else {
                var myaray = new Array("QPO7aA");
                var myarray2 = new Array('#QPO7aA');
                generate(myaray, myarray2, true);

                enabledControlTextField(document.getElementById("<%= TextBoxComplicationOtherText.ClientID %>"), true, true);
            }


            // Question 8a, other - Management and Post-Operative care
            $("#<%= CheckBoxAdjProcOther.ClientID %>").click(function () {
                if ($('#<%= CheckBoxAdjProcOther.ClientID %>').prop('checked') == true) {
                    var myaray = new Array("QPO8aA");
                    var myarray2 = new Array('#QPO8aA');
                    generate(myaray, myarray2, false);

                    enabledControlTextField(document.getElementById("<%= TextBoxAdjProcOtherText.ClientID %>"), false, false);
                }
                else {
                    var myaray = new Array("QPO8aA");
                    var myarray2 = new Array('#QPO8aA');
                    generate(myaray, myarray2, true);

                    enabledControlTextField(document.getElementById("<%= TextBoxAdjProcOtherText.ClientID %>"), true, true);
                }
            });
            if ($('#<%= CheckBoxAdjProcOther.ClientID %>').prop('checked') == true) {
                var myaray = new Array("QPO8aA");
                var myarray2 = new Array('#QPO8aA');
                generate(myaray, myarray2, false);

                enabledControlTextField(document.getElementById("<%= TextBoxAdjProcOtherText.ClientID %>"), false, false);
            }
            else {
                var myaray = new Array("QPO8aA");
                var myarray2 = new Array('#QPO8aA');
                generate(myaray, myarray2, true);

                enabledControlTextField(document.getElementById("<%= TextBoxAdjProcOtherText.ClientID %>"), true, true);
            }


            // Question 2 - Post-Operative care
            $("#<%= RadioButtonPostopAntibioticDropsYes.ClientID %>").click(function () {
                if ($('#<%= RadioButtonPostopAntibioticDropsYes.ClientID %>').prop('checked') == true) {
                    var myaray = new Array("QP2aA", "QP2aB");
                    var myarray2 = new Array('#QP2aB');
                    generate(myaray, myarray2, false);
                }
                else {
                    var myaray = new Array("QP2aA", "QP2aB");
                    var myarray2 = new Array('#QP2aB');
                    generate(myaray, myarray2, true);
                }
            });
            $("#<%= RadioButtonPostopAntibioticDropsNo.ClientID %>").click(function () {
                if ($('#<%= RadioButtonPostopAntibioticDropsYes.ClientID %>').prop('checked') == true) {
                    var myaray = new Array("QP2aA", "QP2aB");
                    var myarray2 = new Array('#QP2aB');
                    generate(myaray, myarray2, false);
                }
                else {
                    var myaray = new Array("QP2aA", "QP2aB");
                    var myarray2 = new Array('#QP2aB');
                    generate(myaray, myarray2, true);
                }
            });
            if ($('#<%= RadioButtonPostopAntibioticDropsYes.ClientID %>').prop('checked') == true) {
                var myaray = new Array("QP2aA", "QP2aB");
                var myarray2 = new Array('#QP2aB');
                generate(myaray, myarray2, false);
            }
            else {
                var myaray = new Array("QP2aA", "QP2aB");
                var myarray2 = new Array('#QP2aB');
                generate(myaray, myarray2, true);
            }


            // Question 4, other - Post-Operative care
            $("#<%= CheckBoxPostopComplicationSTOther.ClientID %>").click(function () {
                if ($('#<%= CheckBoxPostopComplicationSTOther.ClientID %>').prop('checked') == true) {
                    var myaray = new Array("QP4aA");
                    var myarray2 = new Array('#QP4aA');
                    generate(myaray, myarray2, false);

                    enabledControlTextField(document.getElementById("<%= TextBoxPostopComplicationSTOther.ClientID %>"), false, false);
                }
                else {
                    var myaray = new Array("QP4aA");
                    var myarray2 = new Array('#QP4aA');
                    generate(myaray, myarray2, true);

                    enabledControlTextField(document.getElementById("<%= TextBoxPostopComplicationSTOther.ClientID %>"), true, true);
                }
            });
            if ($('#<%= CheckBoxPostopComplicationSTOther.ClientID %>').prop('checked') == true) {
                var myaray = new Array("QP4aA");
                var myarray2 = new Array('#QP4aA');
                generate(myaray, myarray2, false);

                enabledControlTextField(document.getElementById("<%= TextBoxPostopComplicationSTOther.ClientID %>"), false, false);
            }
            else {
                var myaray = new Array("QP4aA");
                var myarray2 = new Array('#QP4aA');
                generate(myaray, myarray2, true);

                enabledControlTextField(document.getElementById("<%= TextBoxPostopComplicationSTOther.ClientID %>"), true, true);
            }


            // Question 1, other - Outcomes
            $("#<%= CheckBoxPostopComplicationOther.ClientID %>").click(function () {
                if ($('#<%= CheckBoxPostopComplicationOther.ClientID %>').prop('checked') == true) {
                    var myaray = new Array("QO1aA");
                    var myarray2 = new Array('#QO1aA');
                    generate(myaray, myarray2, false);

                    enabledControlTextField(document.getElementById("<%= TextBoxPostopComplicationOtherText.ClientID %>"), false, false);
                }
                else {
                    var myaray = new Array("QO1aA");
                    var myarray2 = new Array('#QO1aA');
                    generate(myaray, myarray2, true);

                    enabledControlTextField(document.getElementById("<%= TextBoxPostopComplicationOtherText.ClientID %>"), true, true);
                }
            });
            if ($('#<%= CheckBoxPostopComplicationOther.ClientID %>').prop('checked') == true) {
                var myaray = new Array("QO1aA");
                var myarray2 = new Array('#QO1aA');
                generate(myaray, myarray2, false);

                enabledControlTextField(document.getElementById("<%= TextBoxPostopComplicationOtherText.ClientID %>"), false, false);
            }
            else {
                var myaray = new Array("QO1aA");
                var myarray2 = new Array('#QO1aA');
                generate(myaray, myarray2, true);

                enabledControlTextField(document.getElementById("<%= TextBoxPostopComplicationOtherText.ClientID %>"), true, true);
            }


            // Question 2, other - Outcomes
            $("#<%= RadioButtonPostopFurtherSurgeryYes.ClientID %>").click(function () {
                if ($('#<%= RadioButtonPostopFurtherSurgeryYes.ClientID %>').prop('checked') == true) {
                    var myaray = new Array("QO2aA");
                    var myarray2 = new Array('#QO2aA');
                    generate(myaray, myarray2, false);

                    enabledControlTextField(document.getElementById("<%= TextBoxPostopFurtherSurgeryText.ClientID %>"), false, false);
                }
                else {
                    var myaray = new Array("QO2aA");
                    var myarray2 = new Array('#QO2aA');
                    generate(myaray, myarray2, true);

                    enabledControlTextField(document.getElementById("<%= TextBoxPostopFurtherSurgeryText.ClientID %>"), true, true);
                }
            });
            $("#<%= RadioButtonPostopFurtherSurgeryNo.ClientID %>").click(function () {
                if ($('#<%= RadioButtonPostopFurtherSurgeryYes.ClientID %>').prop('checked') == true) {
                    var myaray = new Array("QO2aA");
                    var myarray2 = new Array('#QO2aA');
                    generate(myaray, myarray2, false);

                    enabledControlTextField(document.getElementById("<%= TextBoxPostopFurtherSurgeryText.ClientID %>"), false, false);
                }
                else {
                    var myaray = new Array("QO2aA");
                    var myarray2 = new Array('#QO2aA');
                    generate(myaray, myarray2, true);

                    enabledControlTextField(document.getElementById("<%= TextBoxPostopFurtherSurgeryText.ClientID %>"), true, true);
                }
            });
            if ($('#<%= RadioButtonPostopFurtherSurgeryYes.ClientID %>').prop('checked') == true) {
                var myaray = new Array("QO2aA");
                var myarray2 = new Array('#QO2aA');
                generate(myaray, myarray2, false);

                enabledControlTextField(document.getElementById("<%= TextBoxPostopFurtherSurgeryText.ClientID %>"), false, false);
            }
            else {
                var myaray = new Array("QO2aA");
                var myarray2 = new Array('#QO2aA');
                generate(myaray, myarray2, true);

                enabledControlTextField(document.getElementById("<%= TextBoxPostopFurtherSurgeryText.ClientID %>"), true, true);
            }


            // Question 6 and 6a, dropdown logic - Outcomes
            $("#<%= DropDownListPostopBCVA.ClientID %>").change(function () {
                var sel = $('#<%= DropDownListPostopBCVA.ClientID %> option:selected').text();
                if (sel > 40) {
                    var myaray = new Array("QO6A", "QO6B");
                    var myarray2 = new Array('#QO6B');
                    generate(myaray, myarray2, false);
                }
                else {
                    var myaray = new Array("QO6A", "QO6B", "QO6aA");
                    var myarray2 = new Array('#QO6B');
                    generate(myaray, myarray2, true);

                    enabledControlTextField(document.getElementById("<%= TextBoxPostopVAReasonOtherText.ClientID %>"), true, true);
                }
            });
            var sel = $('#<%= DropDownListPostopBCVA.ClientID %> option:selected').text();
            if (sel > 40) {
                var myaray = new Array("QO6A", "QO6B");
                var myarray2 = new Array('#QO6B');
                generate(myaray, myarray2, false);
            }
            else {
                var myaray = new Array("QO6A", "QO6B", "QO6aA");
                var myarray2 = new Array('#QO6B');
                generate(myaray, myarray2, true);

                enabledControlTextField(document.getElementById("<%= TextBoxPostopVAReasonOtherText.ClientID %>"), true, true);
            }


            // Question 6a, other - Outcomes
            $("#<%= CheckBoxPostopVAReasonOther.ClientID %>").click(function () {
                if ($('#<%= CheckBoxPostopVAReasonOther.ClientID %>').prop('checked') == true) {
                    var myaray = new Array("QO6aA");
                    var myarray2 = new Array('#QO6aA');
                    generate(myaray, myarray2, false);

                    enabledControlTextField(document.getElementById("<%= TextBoxPostopVAReasonOtherText.ClientID %>"), false, false);
                }
                else {
                    var myaray = new Array("QO6aA");
                    var myarray2 = new Array('#QO6aA');
                    generate(myaray, myarray2, true);

                    enabledControlTextField(document.getElementById("<%= TextBoxPostopVAReasonOtherText.ClientID %>"), true, true);
                }
            });
            if ($('#<%= CheckBoxPostopVAReasonOther.ClientID %>').prop('checked') == true) {
                var myaray = new Array("QO6aA");
                var myarray2 = new Array('#QO6aA');
                generate(myaray, myarray2, false);

                enabledControlTextField(document.getElementById("<%= TextBoxPostopVAReasonOtherText.ClientID %>"), false, false);
            }
            else {
                var myaray = new Array("QO6aA");
                var myarray2 = new Array('#QO6aA');
                generate(myaray, myarray2, true);

                enabledControlTextField(document.getElementById("<%= TextBoxPostopVAReasonOtherText.ClientID %>"), true, true);
            }

            // Question 3a other, Management and Post-Operative Care
            $("#<%= CheckBoxAnesthesiaBoxOther.ClientID %>").click(function () {
                if ($('#<%= CheckBoxAnesthesiaBoxOther.ClientID %>').prop('checked') == true) {
                    var myaray = new Array("QPO4aA");
                    var myarray2 = new Array('#QPO4aA');
                    generate(myaray, myarray2, false);

                    enabledControlTextField(document.getElementById("<%= TextBoxRBMgntAnesthesiaOtherText.ClientID %>"), false, false);
                }
                else {
                    var myaray = new Array("QPO4aA");
                    var myarray2 = new Array('#QPO4aA');
                    generate(myaray, myarray2, true);

                    enabledControlTextField(document.getElementById("<%= TextBoxRBMgntAnesthesiaOtherText.ClientID %>"), true, true);
                }
            });
            if ($('#<%= CheckBoxAnesthesiaBoxOther.ClientID %>').prop('checked') == true) {
                var myaray = new Array("QPO4aA");
                var myarray2 = new Array('#QPO4aA');
                generate(myaray, myarray2, false);

                enabledControlTextField(document.getElementById("<%= TextBoxRBMgntAnesthesiaOtherText.ClientID %>"), false, false);
            }
            else {
                var myaray = new Array("QPO4aA");
                var myarray2 = new Array('#QPO4aA');
                generate(myaray, myarray2, true);

                enabledControlTextField(document.getElementById("<%= TextBoxRBMgntAnesthesiaOtherText.ClientID %>"), true, true);
            }
        });
    </script>
    <script type="text/javascript" language="javascript">
        $(document).ready(function () {
            /** instruct the metadata plugin where to look the metadata
            * jQuery.metadata.setType( type, name );
            * please read the metadata instructions for additional information
            * http://plugins.jquery.com/project/metadata
            */
            $.metadata.setType('attr', 'meta');

            /** To call autoNumeric
            * $(selector).autoNumeric({options}); 
            * The below example uses the input & class selector
            */
            $('input.auto').autoNumeric();
            /* scripts for metadata code generator  */

            /* rountine that prevents  numeric characters from being entered the the altDec field  */
            $('#altDecb').keypress(function (e) {
                var cc = String.fromCharCode(e.which);
                if (e.which != 32 && cc >= 0 && cc <= 9) {
                    e.preventDefault();
                }
            });

            /* rountine that prevents  apostrophe, comma, more than one period (full stop) or numeric characters from being entered the the aSign field  */
            $('#aSignb').keypress(function (e) {
                var cc = String.fromCharCode(e.which);
                if ((e.which != 32 && cc >= 0 && cc <= 9) || cc == "," || cc == "'" || cc == "." && this.value.lastIndexOf('.') != -1) {
                    e.preventDefault();
                }
            });

            $("input.md").bind('click keyup blur', function () {
                var metaCode = '', aSep = '', dGroup = '', aDec = '', altDec = '', aSign = '', pSign = '', vMin = '', vMax = '', mDec = '', mRound = '', aPad = '', wEmpty = '', aForm = '';
                if ($("input:radio[name=aSep]:checked").attr('id') == 'aSepc') {
                    $('input:radio[name=aDec]:nth(0)').removeAttr("disabled");
                    $('input:radio[name=aDec]:nth(0)').attr('checked', true);
                    $('input:radio[name=aDec]:nth(1)').attr("disabled", true);
                }
                if ($("input:radio[name=aSep]:checked").attr('id') == 'aSepp') {
                    $('input:radio[name=aDec]:nth(1)').removeAttr("disabled");
                    $('input:radio[name=aDec]:nth(1)').attr('checked', true);
                    $('input:radio[name=aDec]:nth(0)').attr("disabled", true);
                }
                if ($("input:radio[name=aSep]:checked").attr('id') != 'aSepc' || $("input:radio[name=aSep]:checked").attr('id') != 'aSepp') {
                    $('input:radio[name=aDec]:nth(0)').removeAttr("disabled");
                    $('input:radio[name=aDec]:nth(1)').removeAttr("disabled");
                }
                aSep = $("input:radio[name=aSep]:checked").val();
                dGroup = $("input:radio[name=dGroup]:checked").val();
                aDec = $("input:radio[name=aDec]:checked").val();

                if ($("input:radio[name=altDec]:checked").attr('id') == 'altDecd') {
                    $('#altDecb').val('');
                    $('#altDecb').attr("disabled", true);
                }
                if ($("input:radio[name=altDec]:checked").attr('id') == 'altDeca') {
                    $('#altDecb').removeAttr("disabled");
                    altDec = $('#altDecb').val();
                }

                if ($("input:radio[name=aSign]:checked").attr('id') == 'aSignd') {
                    $('#aSignb').val('');
                    $('#aSignb').attr("disabled", true);
                }
                if ($("input:radio[name=aSign]:checked").attr('id') == 'aSigna') {
                    $('#aSignb').removeAttr("disabled");
                    aSign = $('#aSignb').val();
                }

                pSign = $("input:radio[name=pSign]:checked").val();
                if ($("input:radio[name=vMin]:checked").attr('id') == 'vMind') {
                    $('#vMinb').val('');
                    $('#vMinb').attr("disabled", true);
                }
                if ($("input:radio[name=vMin]:checked").attr('id') == 'vMina') {
                    $('#vMinb').removeAttr("disabled");
                    vMin = $('#vMinb').val();
                }
                if ($("input:radio[name=vMax]:checked").attr('id') == 'vMaxd') {
                    $('#vMaxb').val('');
                    $('#vMaxb').attr("disabled", true);
                }
                if ($("input:radio[name=vMax]:checked").attr('id') == 'vMaxa') {
                    $('#vMaxb').removeAttr("disabled");
                    vMax = $('#vMaxb').val();
                }
                if ($("input:radio[name=mDec]:checked").attr('id') == 'mDecd') {
                    $('#mDecbb').val('');
                    $('#mDecbb').attr("disabled", true);
                }
                if ($("input:radio[name=mDec]:checked").attr('id') == 'mDeca') {
                    $('#mDecbb').removeAttr("disabled");
                    mDec = $('#mDecbb').val();
                }
                mRound = $("input:radio[name=mRound]:checked").val();
                aPad = $("input:radio[name=aPad]:checked").val();
                wEmpty = $("input:radio[name=wEmpty]:checked").val();
                if (aSep != '') {
                    metaCode = aSep;
                }
                if (dGroup != '') {
                    if (metaCode != '') {
                        metaCode = metaCode + ", " + dGroup;
                    }
                    else {
                        metaCode = dGroup;
                    }
                }
                if (aDec != '') {
                    if (metaCode != '') {
                        metaCode = metaCode + ", " + aDec;
                    }
                    else {
                        metaCode = aDec;
                    }
                }
                if (altDec != '') {
                    if (metaCode != '') {
                        metaCode = metaCode + ", altDec: '" + altDec + "'";
                    }
                    else {
                        metaCode = "altDec: '" + altDec + "'";
                    }
                }
                if (aSign != '') {
                    if (metaCode != '') {
                        metaCode = metaCode + ", aSign: '" + aSign + "'";
                    }
                    else {
                        metaCode = "aSign: '" + aSign + "'";
                    }
                }
                if (pSign != '') {
                    if (metaCode != '') {
                        metaCode = metaCode + ", " + pSign;
                    }
                    else {
                        metaCode = pSign;
                    }
                }
                if (vMin != '') {
                    if (metaCode != '') {
                        metaCode = metaCode + ", vMin: '" + vMin + "'";
                    }
                    else {
                        metaCode = "vMin: '" + vMin + "'";
                    }
                }
                if (vMax != '') {
                    if (metaCode != '') {
                        metaCode = metaCode + ", vMax: '" + vMax + "'";
                    }
                    else {
                        metaCode = "vMax: '" + vMax + "'";
                    }
                }
                if (mDec != '') {
                    if (metaCode != '') {
                        metaCode = metaCode + ", mDec: '" + $('#mDecbb').val() + "'";
                    }
                    else {
                        metaCode = "mDec: '" + $('#mDecbb').val() + "'";
                    }
                }

                if (mRound != '') {
                    if (metaCode != '') {
                        metaCode = metaCode + ", " + mRound;
                    }
                    else {
                        metaCode = mRound;
                    }
                }
                if (aPad != '') {
                    if (metaCode != '') {
                        metaCode = metaCode + ", " + aPad;
                    }
                    else {
                        metaCode = aPad;
                    }
                }
                if (wEmpty != '') {
                    if (metaCode != '') {
                        metaCode = metaCode + ", " + wEmpty;
                    }
                    else {
                        metaCode = wEmpty;
                    }
                }
                $('#metaCode').text('');
                if (metaCode != '') {
                    $('#metaCode').text('meta="{' + metaCode + '}"');
                }
            });

            /* clears the metadata code  */
            $('#rd').click(function () {
                $('#metaCode').text('');
            });
            /* ends scripts for metadata code generator  */

            /* script  for defaults demo  */
            $('#d_noMeta').blur(function () {
                var convertInput = '';
                convertInput = $(this).autoNumericGet();
                $('#d_Get').val(convertInput);
                $('#d_Set').autoNumericSet(convertInput);
            });
            /* end script  for defaults demo  */

            /* script  for various samples demo  */
            $('input[name$="sample"]').blur(function () {
                var convertInput = '';
                var row = 'row_' + this.id.charAt(4);
                convertInput = $(this).autoNumericGet();
                $('#' + row + 'b').val(convertInput);
                $('#' + row + 'c').autoNumericSet(convertInput);
            });
            /* end script  for various samples demo  */

            /* script  for rounding methods  */
            $('#roundValue').blur(function () {
                if (this.value != '') {
                    convertInput = $('#roundValue').autoNumericGet();
                    var i = 1;
                    for (i = 1; i <= 9; i++) {
                        $('#roundMethod' + i).autoNumericSet(convertInput);
                    }
                }
            });

            $('#roundDecimal').change(function () { /* changes decimal places */
                convertInput = $('#roundValue').autoNumericGet();
                if (convertInput > 0) {
                    var i = 1;
                    for (i = 1; i <= 9; i++) {
                        $('#roundMethod' + i).autoNumericSet(convertInput);
                    }
                }
            });
            /* end script  for rounding methods  */

            /* script for dynamically loaded values  demo*/
            $.getJSON("test_JSON.php", function (data) {
                var valueFormatted = '';
                $.each(data, function (key, value) { // loops through JSON keys and returns value	
                    $('#' + key).autoNumericSet(value);
                });
            });
            /* end script for dynamically loaded values demo*/

            /* script for callback demo*/
            $.autoNumeric.get_mDec = function () { /* get_mDec function attached to autoNumeric() */
                var set_mDec = $('#get_metricUnit').val();
                if (set_mDec == ' km') {
                    set_mDec = 3;
                } else {
                    set_mDec = 0;
                }
                return set_mDec; /* set mDec decimal places */
            }

            var get_vMax = function () { /* set the maximum value allowed based on the metric unit */
                var set_vMax = $('#get_metricUnit').val();
                if (set_vMax == ' km') {
                    set_vMax = '99999.999';
                } else {
                    set_vMax = '99999999';
                }
                return set_vMax;
            }

            $('#length').autoNumeric({ vMax: get_vMax }); /* calls autoNumeric and passes function get_vMax */

            $('#get_metricUnit').change(function () {
                var set_value = $('#length').autoNumericGet();
                if (this.value == ' km') {
                    set_value = set_value / 1000;
                } else {
                    set_value = set_value * 1000;
                }
                $('#length').autoNumericSet(set_value);
            });
            /* end script for callback demo*/

            $('<%= TextBoxPostopSphericalEquivalent.ClientID %>.auto').autoNumeric();
            $('<%= TextBoxMgntTargetRefraction.ClientID %>.auto').autoNumeric();
        });
    </script>

    <script type="text/javascript">
        function ButtonSubmit_PQRS() {
            var pqrs = ("<%= (PQRSEnabled && PQRSUsed) %>" == "True");
            if (pqrs) {
                var year = $("#<%= DropDownListPQRSVisitYear.ClientID %>").val();
                var month = $("#<%= DropDownListPQRSVisitMonth.ClientID %>").val();
                if (year == 0 || month == 0)
                    return confirm("If there is no PQRS visit date selected, all of your PQRS data will not be saved, is that okay?");
                if (year != 2016)
                    return confirm("If the PQRS visit date selected is not 2016, all of your PQRS data will not be saved, is that okay?");
            }
            return true;
        }

        $(function () {
            $('#QE1').aToolTip({
                clickIt: true,
                tipContent: '20/800 or count fingers @ 5 ft<br />20/1000 or count fingers @ 4 ft<br />20/1600 or count fingers @ 3ft<br />20/2000 or count fingers @ 2 ft<br />20/4000 or count fingers @ 1 ft<br />20/7777 =CF<br />20/8888 =HM<br />20/9999 =LP<br />20/0000 =NLP'
            });

            $('#QO3').aToolTip({
                clickIt: true,
                tipContent: '20/800 or count fingers @ 5 ft<br />20/1000 or count fingers @ 4 ft<br />20/1600 or count fingers @ 3ft<br />20/2000 or count fingers @ 2 ft<br />20/4000 or count fingers @ 1 ft<br />20/7777 =CF<br />20/8888 =HM<br />20/9999 =LP<br />20/0000 =NLP'
            });
        });
    </script>
</asp:Content>
