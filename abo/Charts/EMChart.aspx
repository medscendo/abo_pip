﻿<%@ Page Title="" Language="C#" MasterPageFile="~/PIMMasterPage.master" AutoEventWireup="true" CodeFile="EMChart.aspx.cs" Inherits="abo_EMChart" %>

<asp:Content ID="Content1" ContentPlaceHolderID="head" Runat="Server">

    <style type="text/css">
        .bginputa{
	        float:right;
	        margin:21px 0 0 1px;
	        background: url(../common/images/orange_button_with_arrow.png) no-repeat;
	        overflow:hidden;
	        height:35px;

        }
        .bginputa a{
	        color: white;
	        text-align:center;
	        height:35px;
	        line-height:28px;
	        padding:0 14px 15px  ;
	        cursor:pointer;
	        float:left;
	        border:none;
	        text-decoration:none;
	        font-family:Arial, Helvetica, sans-serif; 
	        font-size:12px; 
	        font-weight:bold; 
	        letter-spacing: 0px;
        }
        .bginputa a:hover{
	        text-decoration:none;
        }
        .right {
            text-align:right;
        }
    </style>

    <link type="text/css" href="../../common/css/atooltip.css" rel="stylesheet"  media="screen" />

</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder1" Runat="Server">
    <asp:HiddenField ID="HiddenFieldRecordIdentifier" runat="server"/>
        <!--  pim -->
        <div class="pim">
            <div class="record-ident clearfix">
                <h3 class="record-first">RECORD IDENTIFIER: <asp:Literal runat="server" ID="LiteralRecordIdentifier"></asp:Literal></h3>
                <h3 class="record-second"><asp:Literal runat="server" ID="LiteralAbstractionNumber"></asp:Literal></h3>
            </div>
            <!-- Name -->
            <table>
                <tr>
                    <th colspan="2" width="910px"><p>History</p></th>
                </tr>
                <!-- Question 1 -->
                <tr class="table_body">
                    <td width="70%">
                        <strong>1. Date of birth:</strong>
                         <br />
                    <asp:Label ID="LabelMonthOfBirth" runat="server" Visible="false"  ForeColor="Red" Text="Please enter Month  "></asp:Label><br />
                    <asp:Label ID="LabelYearOfBirth" runat="server" Visible="false"  ForeColor="Red"  Text="Please enter Year "></asp:Label>
                    </td>
                    <td>
                        <asp:DropDownList ID="DropDownListMonthOfBirth" DataValueField="MonthID" DataTextField="MonthName" runat="server" />
                        <asp:DropDownList ID="DropDownListYearOfBirth" DataValueField="YearID" DataTextField="YearName" runat="server" />
                    </td>
                </tr>
                <!-- Question 2 -->
                <tr class="table_body_bg">
                    <td>
                        <strong>2. Gender</strong>
                        <br />
                         <asp:Label ID="LabelRBGender" runat="server" Visible="false"  ForeColor="Red" Text="Please complete"></asp:Label>
                    </td>
                    <td>
                        <asp:RadioButtonList id="RadioButtonListRBGender" RepeatDirection="Horizontal" CssClass="aspxList" runat="server">
                            <asp:ListItem Value="42">&nbsp;Male</asp:ListItem>
                            <asp:ListItem Value="43">&nbsp;Female</asp:ListItem>
                        </asp:RadioButtonList>

                    </td>
                </tr>
                <!-- Question 3 -->
                <tr class="table_body">
                    <td>
                        <strong>3. Is there a history of prior cutaneous malignancies?</strong>&nbsp;<a href="#"><img id="QH3" src="../../common/images/tip.gif" alt="Tip" /></a>
                        <br />
                         <asp:Label ID="LabelRBHistoryPriorCutMalignancy" runat="server" Visible="false"  ForeColor="Red" Text="Please complete"></asp:Label>

                    </td>
                    <td>
                        <asp:RadioButtonList id="RadioButtonListRBHistoryPriorCutMalignancy" RepeatDirection="Horizontal" CssClass="aspxList" runat="server">
                            <asp:ListItem Value="1">&nbsp;Yes</asp:ListItem>
	                        <asp:ListItem Value="2">&nbsp;No</asp:ListItem>
	                        <asp:ListItem Value="3">&nbsp;Not recorded</asp:ListItem>
                        </asp:RadioButtonList>

                    </td>
                </tr>
                <!-- Question 4 -->
                <tr class="table_body_bg">
                    <td>
                        <strong>4. Is there a history or evidence of excessive sun exposure?</strong>&nbsp;<a href="#"><img id="QH4" src="../../common/images/tip.gif" alt="Tip" /></a>
                        <br />
                         <asp:Label ID="LabelRBHistoryExcessiveSunExposure" runat="server" Visible="false"  ForeColor="Red" Text="Please complete"></asp:Label>

                    </td>
                    <td>
                        <asp:RadioButtonList id="RadioButtonListRBHistoryExcessiveSunExposure" RepeatDirection="Horizontal" CssClass="aspxList" runat="server">
                            <asp:ListItem Value="1">&nbsp;Yes</asp:ListItem>
	                        <asp:ListItem Value="2">&nbsp;No</asp:ListItem>
	                        <asp:ListItem Value="3">&nbsp;Not recorded</asp:ListItem>
                        </asp:RadioButtonList>

                    </td>
                </tr>
                <!-- Question 5 -->
                <tr class="table_body">
                    <td>
                        <strong>5. Is there a history of systemic malignancy, immunosuppressing disorders or drugs?</strong>&nbsp;<a href="#"><img id="QH5" src="../../common/images/tip.gif" alt="Tip" /></a>
                        <br />
                         <asp:Label ID="LabelRBHistorySystemicMalignancy" runat="server" Visible="false"  ForeColor="Red" Text="Please complete"></asp:Label>

                    </td>
                    <td>
                        <asp:RadioButtonList id="RadioButtonListRBHistorySystemicMalignancy" RepeatDirection="Horizontal" CssClass="aspxList" runat="server">
                            <asp:ListItem Value="1">&nbsp;Yes</asp:ListItem>
	                        <asp:ListItem Value="2">&nbsp;No</asp:ListItem>
	                        <asp:ListItem Value="3">&nbsp;Not recorded</asp:ListItem>
                        </asp:RadioButtonList>
                    </td>
                </tr>
                <!-- Question 6 -->
                <tr class="table_body_bg">
                    <td>
                        <strong>6. Is the patient an active smoker?</strong>&nbsp;<a href="#"><img id="QH6" src="../../common/images/tip.gif" alt="Tip" /></a>
                        <br />
                         <asp:Label ID="LabelRBHistorySmoking" runat="server" Visible="false"  ForeColor="Red" Text="Please complete"></asp:Label>

                    </td>
                    <td>
                        <asp:RadioButtonList id="RadioButtonListRBHistorySmoking" RepeatDirection="Horizontal" CssClass="aspxList" runat="server">
                            <asp:ListItem Value="1">&nbsp;Yes</asp:ListItem>
	                        <asp:ListItem Value="2">&nbsp;No</asp:ListItem>
	                        <asp:ListItem Value="3">&nbsp;Not recorded</asp:ListItem>
                        </asp:RadioButtonList>
                    </td>
                </tr>
                <!-- Question 7 -->
                <tr class="table_body">
                    <td>
                        <strong>7. Has the patient received radiation treatment to the head?</strong>&nbsp;<a href="#"><img id="QH7" src="../../common/images/tip.gif" alt="Tip" /></a>
                        <br />
                         <asp:Label ID="LabelRBHistoryRadiation" runat="server" Visible="false"  ForeColor="Red" Text="Please complete"></asp:Label>

                    </td>
                    <td>
                        <asp:RadioButtonList id="RadioButtonListRBHistoryRadiation" RepeatDirection="Horizontal" CssClass="aspxList" runat="server">
                            <asp:ListItem Value="1">&nbsp;Yes</asp:ListItem>
	                        <asp:ListItem Value="2">&nbsp;No</asp:ListItem>
	                        <asp:ListItem Value="3">&nbsp;Not recorded</asp:ListItem>
                        </asp:RadioButtonList>
                    </td>
                </tr>
            </table>
            <br />
            <!-- Examination -->
            <table>
                <tr>
                    <th colspan="3" width="910px"><p>Examination</p></th>
                </tr>
                <!-- Question 1 -->
                <tr class="table_body">
                    <td width="50%" rowspan="4">
                        <strong>1. Do chart notes document:</strong>
                        <asp:Label ID="LabelExamLesionLocation" runat="server" Visible="false"  ForeColor="Red" Text="<br />Please complete Location"></asp:Label>
                        <asp:Label ID="LabelExamLesionSize" runat="server" Visible="false"  ForeColor="Red" Text="<br />Please complete Size"></asp:Label>
                        <asp:Label ID="LabelRBExamInvolvementLacrimal" runat="server" Visible="false"  ForeColor="Red" Text="<br />Please complete Lacrimal outflow system"></asp:Label>
                    </td>
                    <td width="20%">
                        Location of the lesion?
                    </td>
                    <td>
                        <asp:RadioButton runat="server" ID="RadioButtonExamLesionLocationYes" GroupName="ExamLesionLocation" text="&nbsp;Yes" />&nbsp;&nbsp;
                        <asp:RadioButton runat="server" ID="RadioButtonExamLesionLocationNo" GroupName="ExamLesionLocation" text="&nbsp;No" />
                    </td>
                </tr>
                <tr class="table_body">
                    <td>
                        Size of lesion?
                    </td>
                    <td>
                        <asp:RadioButton runat="server" ID="RadioButtonExamLesionSizeYes" GroupName="ExamLesionSize " text="&nbsp;Yes" />&nbsp;&nbsp;
                        <asp:RadioButton runat="server" ID="RadioButtonExamLesionSizeNo" GroupName="ExamLesionSize " text="&nbsp;No" />
                    </td>
                </tr>
                <tr class="table_body">
                    <td>
                        Involvement of lacrimal outflow system?
                    </td>
                    <td>
                        <asp:RadioButtonList id="RadioButtonListRBExamInvolvementLacrimal" RepeatDirection="Horizontal" CssClass="aspxList" runat="server">
                            <asp:ListItem Value="1">&nbsp;Yes</asp:ListItem>
	                        <asp:ListItem Value="2">&nbsp;No</asp:ListItem>
	                        <asp:ListItem Value="48">&nbsp;N/A</asp:ListItem>
                        </asp:RadioButtonList>
                    </td>
                </tr>
            </table>
            <br />
            <!-- Treatment -->
            <table>
                <tr>
                    <th colspan="2" width="910px"><p>Treatment</p></th>
                </tr>
                <!-- Question 1 -->
                <tr class="table_body">
                    <td>
                        <strong>1. Was a biopsy performed prior to excision by yourself or another provider?</strong>
                        <br />
                         <asp:Label ID="LabelExamBiopsy" runat="server" Visible="false"  ForeColor="Red" Text="Please complete Biopsy"></asp:Label>
                    </td>
                    <td>
                        <asp:RadioButton runat="server" ID="RadioButtonExamBiopsyYes" GroupName="ExamBiopsy" text="&nbsp;Yes" />&nbsp;&nbsp;
                        <asp:RadioButton runat="server" ID="RadioButtonExamBiopsyNo" GroupName="ExamBiopsy" text="&nbsp;No" />
                    </td>
                </tr>
                <!-- Question 2 -->
                <tr class="table_body_bg">
                    <td width="70%">
                        <strong>2. How was the lesion treated?</strong>&nbsp;<a href="#"><img id="QT1" src="../../common/images/tip.gif" alt="Tip" /></a>
                        <asp:Label ID="LabelLesion" runat="server" Visible="false"  ForeColor="Red" Text="<br />Please complete"></asp:Label>
                    </td>
                    <td>
                      <asp:RadioButtonList ID="RadioButtonListLesionTreated" RepeatDirection="Vertical" CssClass="aspxList" runat="server">
                          <asp:ListItem Value="335" Text="&nbsp;Primary excision for diagnosis and/or definitive treatment (permanent sections)"></asp:ListItem>
                          <asp:ListItem Value="336" Text="&nbsp;Removal of known malignancy with MOHS margin control, defect reconstruction"></asp:ListItem>
                          <asp:ListItem Value="337" Text="&nbsp;Removal of known malignancy with frozen section margins, defect reconstruction"></asp:ListItem>
                          <asp:ListItem Value="14" Text="&nbsp;Other"></asp:ListItem>
                      </asp:RadioButtonList>
                    </td>
                </tr>
                <!-- Question 2a -->
                <tr class="table_body_bg">
                    <td id="QT2aA">
                        <strong>2a. If you selected other, please specify:</strong>
                        <asp:Label ID="LabelLesionTreatmentOtherText" runat="server" Visible="false"  ForeColor="Red" Text="<br />Please complete"></asp:Label>
                    </td>
                    <td id="QT2aB">
                        <asp:TextBox runat="server" ID="TextBoxLesionTreatmentOtherText" size="25" />
                    </td>
                </tr>
                <!-- Question 3 -->
                <tr class="table_body">
                    <td><strong>3. The malignancy involved which of these anatomic regions:</strong> (Check all that apply)
                          <br />
                         <asp:Label ID="LabelLesionRegion" runat="server" Visible="false"  ForeColor="Red" Text="Please complete"></asp:Label>
                   </td>
                    <td>
                        <asp:CheckBox runat="server" ID="CheckBoxLesionRegionUpperLid" size="" text="&nbsp;Upper lid" />
                        <br /><asp:CheckBox runat="server" ID="CheckBoxLesionRegionLowerLid" size="" text="&nbsp;Lower lid" />
                        <br /><asp:CheckBox runat="server" ID="CheckBoxLesionRegionMedialCanthus" size="" text="&nbsp;Medial canthus" />
                        <br /><asp:CheckBox runat="server" ID="CheckBoxLesionRegionLateralCanthus" size="" text="&nbsp;Lateral canthus" />
                        <br /><asp:CheckBox runat="server" ID="CheckBoxLesionRegionEyelidMargin" size="" text="&nbsp;Eyelid margin" />
                        <br /><asp:CheckBox runat="server" ID="CheckBoxLesionRegionGreater" size="" text="&nbsp;Greater than &frac14; eyelid margin" />
                        <br /><asp:CheckBox runat="server" ID="CheckBoxLesionRegionLacrimalOutflow" size="" text="&nbsp;Lacrimal outflow system" />
                    </td>
                </tr>
                <!-- Question 4 -->
                <tr class="table_body_bg">
                    <td>
                        <strong>4. How was the defect repaired:</strong> (Check all that apply)      
                        <br />
                         <asp:Label ID="LabelRepair" runat="server" Visible="false"  ForeColor="Red" Text="Please complete"></asp:Label>
                   
                    </td>
                    <td>
                        <asp:CheckBox runat="server" ID="CheckBoxRepairHealing" size="" text="&nbsp;Healing by secondary intention/granulation" />
                        <br /><asp:CheckBox runat="server" ID="CheckBoxRepairSimple" size="" text="&nbsp;Simple direct closure" />
                        <br /><asp:CheckBox runat="server" ID="CheckBoxRepairSkinFlaps" size="" text="&nbsp;Skin flaps (advancement, rotational, transposition)" />
                        <br /><asp:CheckBox runat="server" ID="CheckBoxRepairRegionalFlaps" size="" text="&nbsp;Regional flaps (mid-forehead, cheek, etc)" />
                        <br /><asp:CheckBox runat="server" ID="CheckBoxRepairTarsoFlap" size="" text="&nbsp;Tarsoconjunctival flap" />
                        <br /><asp:CheckBox runat="server" ID="CheckBoxRepairFullGraft" size="" text="&nbsp;Full-thickness skin graft" />
                        <br /><asp:CheckBox runat="server" ID="CheckBoxRepairSplitGraft" size="" text="&nbsp;Split-thickness skin graft" />
                        <br /><asp:CheckBox runat="server" ID="CheckBoxRepairReconstruct" size="" text="&nbsp;Reconstruction of lacrimal outflow system" />
                        <br /><asp:CheckBox runat="server" ID="CheckBoxRepairOther" size="" text="&nbsp;Other" />
                    </td>
                </tr>
            </table>
            <br />
            <!-- FOLLOW-UP (PROCEDURE #1) -->
            <table>
                <tr>
                    <th colspan="2" width="910px"><p>Follow-Up (Procedure #1)</p></th>
                </tr>
                <!-- Question 1 -->
                <tr class="table_body">
                    <td width="70%">
                        <strong>1. The pathology report identified the tumor as most consistent with:</strong>
                            <br />
                            <asp:Label ID="LabelRBTumorConsistent" runat="server" Visible="false"  ForeColor="Red" Text="Please complete"></asp:Label>

                    </td>
                    <td>
                        <asp:RadioButtonList id="RadioButtonListRBTumorConsistent" RepeatDirection="Vertical" CssClass="aspxList" runat="server">
                            <asp:ListItem Value="84">&nbsp;Basal cell carcinoma</asp:ListItem>
                            <asp:ListItem Value="85">&nbsp;Squamous cell carcinoma</asp:ListItem>
                            <asp:ListItem Value="86">&nbsp;Melanoma or variant (e.g. lentigo maligna)</asp:ListItem>
                            <asp:ListItem Value="87">&nbsp;Sebaceous carcinoma</asp:ListItem>
                            <asp:ListItem Value="14">&nbsp;Other</asp:ListItem>
                        </asp:RadioButtonList>
                    </td>
                </tr>
                <!-- Question 1a -->
                <tr class="table_body">
                    <td><strong>1a. If you selected other, please specify:</strong>
                        <br />
                            <asp:Label ID="LabelTumorConsistentOtherText" runat="server" Visible="false"  ForeColor="Red" Text="Please complete"></asp:Label>
                   </td>
                    <td>
                        <asp:TextBox runat="server" ID="TextBoxTumorConsistentOtherText" size="25" />
                    </td>                
                </tr>
                <!-- Question 2 -->
                <tr class="table_body_bg">
                    <td>
                        <strong>2. Does the pathology report indicate the final margins are positive for tumor?</strong>&nbsp;<a href="#"><img id="QF2" src="../../common/images/tip.gif" alt="Tip" /></a>
                        <asp:Label ID="LabelFinalMarginsPositive" runat="server" Visible="false"  ForeColor="Red" Text="<br />Please complete"></asp:Label>
                    </td>
                    <td>
                        <asp:RadioButtonList ID="RadioButtonFinalMarginsPositive" RepeatDirection="Vertical" CssClass="aspxList" runat="server">
                            <asp:ListItem Value="1">&nbsp;Yes</asp:ListItem>
	                        <asp:ListItem Value="2">&nbsp;No</asp:ListItem>
	                        <asp:ListItem Value="351">&nbsp;Margin status not reported</asp:ListItem>
                        </asp:RadioButtonList>
                    </td>
                </tr>
                <!-- Question 3 -->
                <tr class="table_body">
                    <td>
                        <strong><span id="QF3A">3. If margins were found to be positive, which of the following actions were taken?</span></strong>&nbsp;<a href="#"><img id="QF3" src="../../common/images/tip.gif" alt="Tip" /></a>
                        <asp:Label ID="LabelAction" runat="server" Visible="false"  ForeColor="Red" Text="<br />Please complete"></asp:Label>
                    </td>
                    <td>
                        <span id="QF3B">
                        <asp:CheckBox runat="server" ID="CheckBoxActionReferral" size="" text="&nbsp;Patient was referred to &quot;oncologist&quot; or other physician for general evaluation (or systemic tumor work up) prior to deciding on local tumor management" />
                        <br /><asp:CheckBox runat="server" ID="CheckBoxActionAdditionalProc" size="" text="&nbsp;An additional procedure was planned to remove residual tumor and reconstruct" />
                        <br /><asp:CheckBox runat="server" ID="CheckBoxActionCryotherapy" size="" text="&nbsp;Cryotherapy was recommended to treat residual tumor" />
                        <br /><asp:CheckBox runat="server" ID="CheckBoxActionFollowPatient" size="" text="&nbsp;Decision was made to follow patient with residual tumor" />
                        <br /><asp:CheckBox runat="server" ID="CheckBoxActionRadiationTherapy" size="" text="&nbsp;Radiation therapy" />
                        <br /><asp:CheckBox runat="server" ID="CheckBoxActionOther" size="" text="&nbsp;Other" />
                        </span>
                    </td>
                </tr>
                <!-- Question 3a -->
                <tr class="table_body">
                    <td>
                        <strong>3a. If you selected other, please specify:</strong>
                        <br />
                        <asp:Label ID="LabelActionOtherText" runat="server" Visible="false"  ForeColor="Red" Text="Please complete"></asp:Label>
                  
                    </td>
                    <td>
                        <asp:TextBox runat="server" ID="TextBoxActionOtherText" size="25" />

                    </td>
                </tr>
            </table>
            <br />
            <!-- Outcomes -->
            <table>
                <tr>
                    <th colspan="2" width="910px"><p>Outcomes</p></th>
                </tr>
                <!-- Question 1 -->
                <tr class="table_body">
                    <td width="70%">
                        <strong>1. How long was the patient followed after the primary resection-reconstruction?</strong>
                        <br />
                        <asp:Label ID="LabelFollowUpPerformed" runat="server" Visible="false"  ForeColor="Red" Text="Please complete"></asp:Label>
                    </td>
                    <td>
                        #&nbsp;<asp:TextBox runat="server" ID="TextBoxFollowedMonths" size="5" />&nbsp;months&nbsp;&nbsp;#&nbsp;
                        <asp:TextBox runat="server" ID="TextBoxFollowedYears" size="5" />&nbsp;days
                    </td>
                </tr>
                <!-- Question 2 -->
                <tr class="table_body_bg">
                    <td>
                        <strong>2. The post-operative surgical result left a satisfactory functioning eyelid?</strong>
                        <br />
                        <asp:Label ID="LabelSatisfactoryEyelid" runat="server" Visible="false"  ForeColor="Red" Text="Please complete"></asp:Label>
                  
                    </td>
                    <td>
                        <asp:RadioButton runat="server" ID="RadioButtonSatisfactoryEyelidYes" GroupName="SatisfactoryEyelid" text="&nbsp;Yes" />&nbsp;&nbsp;
                        <asp:RadioButton runat="server" ID="RadioButtonSatisfactoryEyelidNo" GroupName="SatisfactoryEyelid" text="&nbsp;No" />
                    </td>
                </tr>
                <!-- Question 3 -->
                <tr class="table_body">
                    <td>
                        <strong>3. Complications identified included (check all that apply)</strong>
                        <br />
                        <asp:Label ID="LabelComplication" runat="server" Visible="false"  ForeColor="Red" Text="Please complete"></asp:Label>
                  
                    </td>
                    <td>
                        <asp:CheckBox runat="server" ID="CheckBoxComplicationHemorrhage" text="&nbsp;Hemorrhage" />
                        <br /><asp:CheckBox runat="server" ID="CheckBoxComplicationInfection" text="&nbsp;Infection" />
                        <br /><asp:CheckBox runat="server" ID="CheckBoxComplicationLidRetraction" text="&nbsp;Lid retraction" />
                        <br /><asp:CheckBox runat="server" ID="CheckBoxComplicationEntropion" text="&nbsp;Entropion" />
                        <br /><asp:CheckBox runat="server" ID="CheckBoxComplicationEctropion" text="&nbsp;Ectropion" />
                        <br /><asp:CheckBox runat="server" ID="CheckBoxComplicationCorneal" text="&nbsp;Corneal exposure/lagophthalmos" />
                        <br /><asp:CheckBox runat="server" ID="CheckBoxComplicationOther" text="&nbsp;Other" />
                    </td>
                </tr>
                 <!-- Question 3a -->
                <tr class="table_body">
                    <td>
                        <strong>3a. If you selected other, please specify:</strong>
                        <br />
                        <asp:Label ID="LabelComplicationOtherText" runat="server" Visible="false"  ForeColor="Red" Text="Please complete"></asp:Label>
                  
                    </td>
                    <td>
                        <asp:TextBox runat="server" ID="TextBoxComplicationOtherText" size="25" />
                    </td>
                </tr>
               <!-- Question 4 -->
                <tr class="table_body_bg">
                    <td>
                        <strong>4. Patient was counseled regarding need for sun protection i.e. sunscreen, hat and sunglasses?</strong>
                        <br />
                        <asp:Label ID="LabelPatientCounsel" runat="server" Visible="false"  ForeColor="Red" Text="Please complete"></asp:Label>
                  
                    </td>
                    <td>
                           <asp:RadioButton runat="server" ID="RadioButtonPatientCounselYes" GroupName="PatientCounsel" text="&nbsp;Yes" />&nbsp;&nbsp;
                           <asp:RadioButton runat="server" ID="RadioButtonPatientCounselNo" GroupName="PatientCounsel" text="&nbsp;No" />
                    </td>
                </tr>
                <!-- Question 5 -->
                <tr class="table_body">
                    <td>
                        <strong>5. Recurrence of malignant tumor was noted in the operative site during the post-operative period?</strong>
                        <br />
                        <asp:Label ID="LabelRecurrenceNoted" runat="server" Visible="false"  ForeColor="Red" Text="Please complete"></asp:Label>
                  
                    </td>
                    <td>
                        <asp:RadioButton runat="server" ID="RadioButtonRecurrenceNotedYes" GroupName="RecurrenceNoted" text="&nbsp;Yes" />&nbsp;&nbsp;
                        <asp:RadioButton runat="server" ID="RadioButtonRecurrenceNotedNo" GroupName="RecurrenceNoted" text="&nbsp;No" />
                     </td>
                </tr>
                <!-- Question 6 -->
                <tr class="table_body_bg">
                    <td>
                        <strong>6. Follow-up was arranged for tumor surveillance with (check all that apply)</strong>
                        <br />
                        <asp:Label ID="LabelFollowUp" runat="server" Visible="false"  ForeColor="Red" Text="Please complete"></asp:Label>
                  
                    </td>
                    <td>
                    <asp:CheckBox runat="server" ID="CheckBoxFollowUpSurgeon" text="&nbsp;Myself" />
                        <br /><asp:CheckBox runat="server" ID="CheckBoxFollowUpDermatologist" text="&nbsp;Dermatologist" />
                        
                        <br /><asp:CheckBox runat="server" ID="CheckBoxFollowUpOncologist" text="&nbsp;Oncologist" />
                        <br /><asp:CheckBox runat="server" ID="CheckBoxFollowUpOtherOphthal" text="&nbsp;Other Ophthalmologist" />
                        <br /><asp:CheckBox runat="server" ID="CheckBoxFollowUpOther" text="&nbsp;Other" />
                        <br /><asp:CheckBox runat="server" ID="CheckBoxFollowUpNone" text="&nbsp;None or lost to follow-up" />
                    </td>
                </tr>
                <!-- Question 6a -->
                <tr class="table_body_bg">
                    <td>
                        <strong>6a. If you selected other, please specify:</strong>
                     </td>
                    <td>
                        <asp:TextBox runat="server" ID="TextBoxFollowUpOtherText" size="25" />
                    </td>
                </tr>
                 <!-- Question 7 -->
               <tr class="table_body">
                    <td>
                         <strong>7. For patients who will be followed in your office, do you have a system in place to identify patients who have missed follow-up visits?</strong>&nbsp;<a href="#"><img id="QO7" src="../../common/images/tip.gif" alt="Tip" /></a>
                         <br />
                        <asp:Label ID="LabelRBFollowUpMissedVisit" runat="server" Visible="false"  ForeColor="Red" Text="Please complete"></asp:Label>
                  
                    </td>
                    <td>
                        <asp:RadioButtonList id="RadioButtonListRBFollowUpMissedVisit" RepeatDirection="Horizontal" CssClass="aspxList" runat="server">
                            <asp:ListItem Value="1">&nbsp;Yes</asp:ListItem>
                            <asp:ListItem Value="2">&nbsp;No</asp:ListItem>
                            <asp:ListItem Value="48">&nbsp;N/A</asp:ListItem>
                        </asp:RadioButtonList>
                    </td>
                </tr>
            </table>
            <div class="button-box">
                <asp:LinkButton ID="LinkButtonBackToDashboard" runat="server" Text="Back To Chart Registration" PostBackUrl="../PatientChartRegistration.aspx?CycleNumber=1" Visible="false" CssClass="button" />
                <asp:LinkButton ID="ButtonSubmit" OnClick="ButtonSubmit_Click" runat="server" Text="Submit Chart" CssClass="button" />
            </div>
        </div>
        <!-- ION pim ends -->
</asp:Content>

<asp:Content runat="server" ID="Content4" ContentPlaceHolderID="javascript">
    <script type="text/javascript" src="../../common/js/jquery.atooltip.js"></script>
    <script type="text/javascript" language="javascript">
        //      Either enables or disables input boxes
        function enabledControl(el, disabled, checked) {
            //alert("Hello world from enabled control");
            try {
                if (disabled) {
                    el.disabled = disabled;
                    el.setAttribute("disabled", true);
                }
                else {
                    el.disabled = disabled;
                    el.removeAttribute("disabled");
                }

                if (checked == true)
                    el.checked = false;
            }
            catch (E) {
            }
            if (el.childNodes && el.childNodes.length > 0) {
                for (var x = 0; x < el.childNodes.length; x++) {
                    enabledControl(el.childNodes[x], disabled, checked);
                }
            }
        }
        //      Either enables or disables dropdown boxes
        function enabledControlDropDown(el, disabled, checked) {
            //alert("Hello world from enabled control");
            try {
                if (disabled) {
                    el.disabled = disabled;
                    el.setAttribute("disabled", true);
                }
                else {
                    el.disabled = disabled;
                    el.removeAttribute("disabled");
                }

                if (checked == true)
                    el.options[0].selected = true;
            }
            catch (E) {
            }
            if (el.childNodes && el.childNodes.length > 0) {
                for (var x = 0; x < el.childNodes.length; x++) {
                    enabledControl(el.childNodes[x], disabled, checked);
                }
            }
        }
        //      Either enables or disables text boxes
        function enabledControlTextField(el, disabled, checked) {
            //alert("Hello world from enabled control");
            try {
                if (disabled) {
                    el.disabled = disabled;
                    el.setAttribute("disabled", true);
                }
                else {
                    el.disabled = disabled;
                    el.removeAttribute("disabled");
                }

                if (checked == true) {
                    el.value = '';
                }
            }
            catch (E) {
            }
            if (el.childNodes && el.childNodes.length > 0) {
                for (var x = 0; x < el.childNodes.length; x++) {
                    enabledControl(el.childNodes[x], disabled, checked);
                }
            }
        }

        function generate(arr1, arr2, istrue) {
            if (istrue == true) {
                for (var i = 0; i < arr1.length; i++) {
                    document.getElementById(arr1[i]).style.color = "gray";
                }
                for (var i = 0; i < arr2.length; i++) {
                    $(arr2[i] + ' :input').attr('disabled', true);
                    $(arr2[i] + ' :input').removeAttr("checked");
                }
            }
            if (istrue == false) {
                for (var i = 0; i < arr1.length; i++) {
                    document.getElementById(arr1[i]).style.color = "#333";
                }
                for (var i = 0; i < arr2.length; i++) {
                    $(arr2[i] + ' :input').removeAttr('disabled');
                }
            }
        }

    </script>
    
    
    <script  type="text/javascript" language="javascript">
        $(document).ready(function() {

            $("#<%= RadioButtonListRBTumorConsistent.ClientID %>").click(function() {
                if ($('#<%= RadioButtonListRBTumorConsistent.ClientID %>').find('input:checked').val() == ('14')) {
                    enabledControlTextField(document.getElementById("<%= TextBoxTumorConsistentOtherText.ClientID %>"), false, false);
                }
                else {
                    enabledControlTextField(document.getElementById("<%= TextBoxTumorConsistentOtherText.ClientID %>"), true, true);
                }
            });

            $("#<%= CheckBoxActionOther.ClientID %>").click(function() {
                if ($('#<%= CheckBoxActionOther.ClientID %>').prop("checked") == true) {
                    enabledControlTextField(document.getElementById("<%= TextBoxActionOtherText.ClientID %>"), false, false);
                }
                else {
                    enabledControlTextField(document.getElementById("<%= TextBoxActionOtherText.ClientID %>"), true, true);
                }
            });

            $("#<%= CheckBoxComplicationOther.ClientID %>").click(function() {
                if ($('#<%= CheckBoxComplicationOther.ClientID %>').prop("checked") == true) {
                    enabledControlTextField(document.getElementById("<%= TextBoxComplicationOtherText.ClientID %>"), false, false);
                }
                else {
                    enabledControlTextField(document.getElementById("<%= TextBoxComplicationOtherText.ClientID %>"), true, true);
                }
            });

            $("#<%= CheckBoxFollowUpOther.ClientID %>").click(function() {
                if ($('#<%= CheckBoxFollowUpOther.ClientID %>').prop("checked") == true) {
                    enabledControlTextField(document.getElementById("<%= TextBoxFollowUpOtherText.ClientID %>"), false, false);
                }
                else {
                    enabledControlTextField(document.getElementById("<%= TextBoxFollowUpOtherText.ClientID %>"), true, true);
                }
            });

            if ($('#<%= RadioButtonListRBTumorConsistent.ClientID %>').find('input:checked').val() == ('14')) {
                enabledControlTextField(document.getElementById("<%= TextBoxTumorConsistentOtherText.ClientID %>"), false, false);
            }
            else {
                enabledControlTextField(document.getElementById("<%= TextBoxTumorConsistentOtherText.ClientID %>"), true, true);
            }

            if ($('#<%= CheckBoxActionOther.ClientID %>').prop("checked") == true) {
                enabledControlTextField(document.getElementById("<%= TextBoxActionOtherText.ClientID %>"), false, false);
            }
            else {
                enabledControlTextField(document.getElementById("<%= TextBoxActionOtherText.ClientID %>"), true, true);
            }

            if ($('#<%= CheckBoxComplicationOther.ClientID %>').prop("checked") == true) {
                enabledControlTextField(document.getElementById("<%= TextBoxComplicationOtherText.ClientID %>"), false, false);
            }
            else {
                enabledControlTextField(document.getElementById("<%= TextBoxComplicationOtherText.ClientID %>"), true, true);
            }

            if ($('#<%= CheckBoxFollowUpOther.ClientID %>').prop("checked") == true) {
                enabledControlTextField(document.getElementById("<%= TextBoxFollowUpOtherText.ClientID %>"), false, false);
            }
            else {
                enabledControlTextField(document.getElementById("<%= TextBoxFollowUpOtherText.ClientID %>"), true, true);
            }


        // Treatment - Question 2a: Other
        $("#<%= RadioButtonListLesionTreated.ClientID %>").click(function() {
            if ($('#<%= RadioButtonListLesionTreated.ClientID %>').find('input:checked').val() == ('14')) {
                var myaray = new Array("QT2aA", "QT2aB");
                var myarray2 = new Array('#QT2aB');
                generate(myaray, myarray2, false);

                enabledControlTextField(document.getElementById("<%= TextBoxLesionTreatmentOtherText.ClientID %>"), false, false);
            }
            else {
                var myaray = new Array("QT2aA", "QT2aB");
                var myarray2 = new Array('#QT2aB');
                generate(myaray, myarray2, true);

                enabledControlTextField(document.getElementById("<%= TextBoxLesionTreatmentOtherText.ClientID %>"), true, true);
            }
        });
        
        // LabelLesionTreatmentOtherText
        if ($('#<%= RadioButtonListLesionTreated.ClientID %>').find('input:checked').val() == ('14')) {
            var myaray = new Array("QT2aA", "QT2aB");
            var myarray2 = new Array('#QT2aB');
            generate(myaray, myarray2, false);

            enabledControlTextField(document.getElementById("<%= TextBoxLesionTreatmentOtherText.ClientID %>"), false, false);
        }
        else {
            var myaray = new Array("QT2aA", "QT2aB");
            var myarray2 = new Array('#QT2aB');
            generate(myaray, myarray2, true);

            enabledControlTextField(document.getElementById("<%= TextBoxLesionTreatmentOtherText.ClientID %>"), true, true);
        }

    });
    </script>

    <script type="text/javascript">
        $(function() {
            $('#QH3').aToolTip({
                clickIt: true,
                tipContent: 'Patients with one cutaneous malignancy are likely to develop others'
            });
        });
        $(function() {
            $('#QH4').aToolTip({
                clickIt: true,
                tipContent: 'Solar exposure increases risk of cutaneous malignancies'
            });
        });
        $(function() {
            $('#QH5').aToolTip({
                clickIt: true,
                tipContent: 'Patients with diseases such as AIDS, lymphoma, or on immunosuppressive drugs such as chemotherapeutic agents or prednisone, have an increased incidence of cutaneous malignancies'
            });
        });
        $(function() {
            $('#QH6').aToolTip({
                clickIt: true,
                tipContent: 'Smoking has been associated with increased risk of malignancies'
            });
        });
        $(function() {
            $('#QH7').aToolTip({
                clickIt: true,
                tipContent: 'Radiation exposure has been associated with increased risk of malignancies'
            });
        });
        $(function() {
            $('#QT1').aToolTip({
                clickIt: true,
                tipContent: 'All lesions suspicious for malignancy should be submitted to pathology for evaluation'
            });
        });
        $(function() {
            $('#QF2').aToolTip({
                clickIt: true,
                tipContent: 'Complete removal by a Mohs surgeon are considered to have negative margins'
            });
        });
        $(function() {
            $('#QF3').aToolTip({
                clickIt: true,
                tipContent: 'For certain tumors it may be appropriate to determine if the patient has systemic manifestations of disease prior to deciding on local management<br/>If a biopsy reports tumor at the margins, it is generally accepted that a plan needs to be developed to address the residual tumor; it may be appropriate to follow some lesions expectantly.'
            });
        });
        $(function() {
            $('#QO7').aToolTip({
                clickIt: true,
                tipContent: 'It is expected that patients with cutaneous malignancies will have follow-up monitoring arrangements and that physicians have in place systems to identify patients who do not follow through with this monitoring.'
            });
        });
    </script>
</asp:Content>