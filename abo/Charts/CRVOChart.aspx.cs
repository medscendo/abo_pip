﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Transactions;
using NetHealthPIMModel;

public partial class abo_CRVOChart : BasePage
{
    protected void Page_Load(object sender, EventArgs e)
    {
    
   if (!IsPostBack)
        {
            InitializePage();
        }

    }
    protected void InitializePage()
    {
        ((PIMMasterPage)Page.Master).SetTitle("Chart");

        using (NetHealthPIMEntities pim = new NetHealthPIMEntities())
        {
            int ModuleID = (int)Session[Constants.SESSION_WORKINGMODULEID];
            Module module = pim.Module.First(c => c.ModuleID == ModuleID);
            ((PIMMasterPage)Page.Master).SetHeader(module.ModuleName + " Chart Abstraction");

            ParticipantModuleCycle prev = (ParticipantModuleCycle)Session[Constants.SESSION_PREVIOUSCYCLE];
            String CycleNumber = Request.QueryString["CycleNumber"];

            int cycleID = ctx.ActiveModuleCycleID;
            if (CycleNumber == "1")
            {
                cycleID = prev.ParticipantModuleCycleID;
                LinkButtonBackToDashboard.Visible = true;
                ButtonSubmit.Visible = false;
            }

            ParticipantModuleCycle cycle = pim.ParticipantModuleCycle.First(c => c.ParticipantModuleCycleID == cycleID);
            String numberOfRecord = Request.QueryString["nr"];
            String totalRecords = Request.QueryString["t"];
            if (cycle.CycleNumber == 1)
            {
                ((PIMMasterPage)Page.Master).SetStatus(2);
                LiteralAbstractionNumber.Text = "Initial Abstraction: Chart " + numberOfRecord + " of " + totalRecords;
            }
            else
            {
                ((PIMMasterPage)Page.Master).SetStatus(3);
                LiteralAbstractionNumber.Text = "Second Abstraction: Chart " + numberOfRecord + " of " + totalRecords;
            }
            DropDownListMonthOfBirth.DataSource = CycleManager.getMonthList();
            DropDownListYearOfBirth.DataSource = CycleManager.getDOBYearList(0, 100);
            DropDownListMonthOfExam.DataSource = CycleManager.getMonthList();
            DropDownListYearOfExam.DataSource = CycleManager.getDOBYearList(0, 100);
             DropDownListMonthOfFollowUp.DataSource = CycleManager.getMonthList();
            DropDownListYearOfFollowUp.DataSource = CycleManager.getDOBYearList(0, 100);
            DropDownListMonthOfDiagnosis.DataSource = CycleManager.getMonthList();
            DropDownListYearOfDiagnosis.DataSource = CycleManager.getDOBYearList(0, 100);
            DropDownListInvolvedBCVA.DataSource = CycleManager.getExamValues();
            DropDownListFellowBCVA.DataSource = CycleManager.getExamValues();
            DropDownListOutcomeTreatmentEyeBCVA.DataSource = CycleManager.getExamValues();
            DropDownListOutcomeTreatmentFellowBCVA.DataSource = CycleManager.getExamValues();
            DataBind();
            DropDownListYearOfBirth.Items.Insert(0, new ListItem("Year", "0"));
            DropDownListYearOfExam.Items.Insert(0, new ListItem("Year", "0"));
            DropDownListYearOfFollowUp.Items.Insert(0, new ListItem("Year", "0"));
            DropDownListYearOfDiagnosis.Items.Insert(0, new ListItem("Year", "0"));
            string recordIdentifier = Request.QueryString["ri"];
            HiddenFieldRecordIdentifier.Value = recordIdentifier;
            LiteralRecordIdentifier.Text = recordIdentifier;
            var count = pim.ChartAbstractionCRVO.Where(ps => ps.ParticipantModuleCycle.ParticipantModuleCycleID == cycleID & ps.RecordIdentifier == recordIdentifier).Count();
            if (count == 0)
            {
                using (TransactionScope transaction = new TransactionScope())
                {
                    //Module module = pim.Module.First(m => m.ModuleID == Constants.ABO_AMBLYOPIA_MODULEID);

                    NetHealthPIMModel.ChartRegistration chartRegistration = (from cr in pim.ChartRegistration
                                                           where cr.ParticipantModuleCycleID == cycleID
                                                           & cr.RecordIdentifier == recordIdentifier
                                                           & cr.ModuleID == 58//// NEEDS TO BE CHANGED
                                                           select cr).First<NetHealthPIMModel.ChartRegistration>();

                    DateTime initialVisit = Convert.ToDateTime(chartRegistration.InitialVisit);
                    DateTime DOB = Convert.ToDateTime(chartRegistration.DOB);

                    ChartAbstractionCRVO abstractRecord = new ChartAbstractionCRVO();
                    abstractRecord.ParticipantModuleCycle = cycle;
                    abstractRecord.RecordIdentifier = recordIdentifier;
                    abstractRecord.MonthOfExam = initialVisit.Month;
                    abstractRecord.YearOfExam = initialVisit.Year;
                    abstractRecord.MonthOfBirth = DOB.Month;
                    abstractRecord.YearOfBirth = DOB.Year;
                    DropDownListYearOfBirth.SelectedValue = abstractRecord.YearOfBirth.ToString();
                    DropDownListMonthOfBirth.SelectedValue = abstractRecord.MonthOfBirth.ToString();
                    DropDownListMonthOfExam.SelectedValue = abstractRecord.MonthOfExam.ToString();
                    DropDownListYearOfExam.SelectedValue = abstractRecord.YearOfExam.ToString();
                    abstractRecord.CreatedOn = DateTime.Now;
                    abstractRecord.LastUpdateDate = DateTime.Now;
                    pim.SaveChanges();

                    transaction.Complete();
                }
            }
            else
            {
                ChartAbstractionCRVO abstractRecord = (from c in pim.ChartAbstractionCRVO
                                                                       where c.ParticipantModuleCycle.ParticipantModuleCycleID == cycleID & c.RecordIdentifier == recordIdentifier
                                                       select c).First<ChartAbstractionCRVO>();

                if (abstractRecord.OutcomeTherapyIntravit != null) CheckBoxOutcomeTherapyIntravit.Checked = ((bool)abstractRecord.OutcomeTherapyIntravit);
                if (abstractRecord.ExamCMTNA != null) CheckBoxExamCMTNA.Checked = ((bool)abstractRecord.ExamCMTNA);
                if (abstractRecord.TherapyBevacizumab != null) CheckBoxTherapyBevacizumab.Checked = ((bool)abstractRecord.TherapyBevacizumab);
                if (abstractRecord.TherapyRanibizumab != null) CheckBoxTherapyRanibizumab.Checked = ((bool)abstractRecord.TherapyRanibizumab);
                if (abstractRecord.TherapyIntravitrealSteroids != null) CheckBoxTherapyIntravitrealSteroids.Checked = ((bool)abstractRecord.TherapyIntravitrealSteroids);
                if (abstractRecord.TherapyOther != null) CheckBoxTherapyOther.Checked = ((bool)abstractRecord.TherapyOther);
                if (abstractRecord.TherapyNone != null) CheckBoxTherapyNone.Checked = ((bool)abstractRecord.TherapyNone);
                if (abstractRecord.OutcomeCSTNA != null) CheckBoxOutcomeCSTNA.Checked = ((bool)abstractRecord.OutcomeCSTNA);
                if (abstractRecord.OutcomeTherapyBevacizumab != null) CheckBoxOutcomeTherapyBevacizumab.Checked = ((bool)abstractRecord.OutcomeTherapyBevacizumab);
                if (abstractRecord.OutcomeTherapyRanibizumab != null) CheckBoxOutcomeTherapyRanibizumab.Checked = ((bool)abstractRecord.OutcomeTherapyRanibizumab);
                if (abstractRecord.OutcomeTherapyPRP != null) CheckBoxOutcomeTherapyPRP.Checked = ((bool)abstractRecord.OutcomeTherapyPRP);
                if (abstractRecord.OutcomeTherapyPPVx != null) CheckBoxOutcomeTherapyPPVx.Checked = ((bool)abstractRecord.OutcomeTherapyPPVx);
                if (abstractRecord.OutcomeTherapyCataractSurgery != null) CheckBoxOutcomeTherapyCataractSurgery.Checked = ((bool)abstractRecord.OutcomeTherapyCataractSurgery);
                if (abstractRecord.OutcomeTherapyLaser != null) CheckBoxOutcomeTherapyLaser.Checked = ((bool)abstractRecord.OutcomeTherapyLaser);
                if (abstractRecord.OutcomeTherapyOther != null) CheckBoxOutcomeTherapyOther.Checked = ((bool)abstractRecord.OutcomeTherapyOther);



                if (abstractRecord.RBGender != null) RadioButtonListRBGender.SelectedValue = abstractRecord.RBGender.ToString();
                if (abstractRecord.RBDrugReaction != null) RadioButtonListRBDrugReaction.SelectedValue = abstractRecord.RBDrugReaction.ToString();
                if (abstractRecord.RBExamEyeID != null) RadioButtonListRBExamEyeID.SelectedValue = abstractRecord.RBExamEyeID.ToString();
                if (abstractRecord.RBRAPDMeasured != null) RadioButtonListRBRAPDMeasured.SelectedValue = abstractRecord.RBRAPDMeasured.ToString();
                if (abstractRecord.RBIntravitrealPharma != null) RadioButtonListRBIntravitrealPharma.SelectedValue = abstractRecord.RBIntravitrealPharma.ToString();
                if (abstractRecord.RBNoTherapyReason != null) RadioButtonListRBNoTherapyReason.SelectedValue = abstractRecord.RBNoTherapyReason.ToString();
                if (abstractRecord.RBPatientFollowupSchedule != null) RadioButtonListRBPatientFollowupSchedule.SelectedValue = abstractRecord.RBPatientFollowupSchedule.ToString();
                if (abstractRecord.RBPatientFollowupIOP != null) RadioButtonListRBPatientFollowupIOP.SelectedValue = abstractRecord.RBPatientFollowupIOP.ToString();
                if (abstractRecord.RBOutcomeMacularEdema != null) RadioButtonListRBOutcomeMacularEdema.SelectedValue = abstractRecord.RBOutcomeMacularEdema.ToString();
                if (abstractRecord.RBNoCMETherapyReason != null) RadioButtonListRBNoCMETherapyReason.SelectedValue = abstractRecord.RBNoCMETherapyReason.ToString();
                if (abstractRecord.RBIOPElevationDeveloped != null) RadioButtonListRBIOPElevationDeveloped.SelectedValue = abstractRecord.RBIOPElevationDeveloped.ToString();
                if (abstractRecord.RBCataractDeveloped != null) RadioButtonListRBCataractDeveloped.SelectedValue = abstractRecord.RBCataractDeveloped.ToString();



                if (abstractRecord.MonthOfBirth != null) DropDownListMonthOfBirth.SelectedValue = abstractRecord.MonthOfBirth.ToString();
                if (abstractRecord.YearOfBirth != null) DropDownListYearOfBirth.SelectedValue = abstractRecord.YearOfBirth.ToString();
                if (abstractRecord.MonthOfExam != null) DropDownListMonthOfExam.SelectedValue = abstractRecord.MonthOfExam.ToString();
                if (abstractRecord.YearOfExam != null) DropDownListYearOfExam.SelectedValue = abstractRecord.YearOfExam.ToString();
                if (abstractRecord.MonthOfFollowUp != null) DropDownListMonthOfFollowUp.SelectedValue = abstractRecord.MonthOfFollowUp.ToString();
                if (abstractRecord.YearOfFollowUp != null) DropDownListYearOfFollowUp.SelectedValue = abstractRecord.YearOfFollowUp.ToString();
                if (abstractRecord.MonthOfDiagnosis != null) DropDownListMonthOfDiagnosis.SelectedValue = abstractRecord.MonthOfDiagnosis.ToString();
                if (abstractRecord.YearOfDiagnosis != null) DropDownListYearOfDiagnosis.SelectedValue = abstractRecord.YearOfDiagnosis.ToString();






                if (abstractRecord.InvolvedBCVA != null) DropDownListInvolvedBCVA.SelectedValue = abstractRecord.InvolvedBCVA.ToString();
                if (abstractRecord.FellowBCVA != null) DropDownListFellowBCVA.SelectedValue = abstractRecord.FellowBCVA.ToString();
                if (abstractRecord.OutcomeTreatmentEyeBCVA != null) DropDownListOutcomeTreatmentEyeBCVA.SelectedValue = abstractRecord.OutcomeTreatmentEyeBCVA.ToString();
                if (abstractRecord.OutcomeTreatmentFellowBCVA != null) DropDownListOutcomeTreatmentFellowBCVA.SelectedValue = abstractRecord.OutcomeTreatmentFellowBCVA.ToString();



                if (abstractRecord.SystemicMedications != null)
                {
                    RadioButtonSystemicMedicationsYes.Checked = ((bool)abstractRecord.SystemicMedications);
                    RadioButtonSystemicMedicationsNo.Checked = !((bool)abstractRecord.SystemicMedications);
                }
                if (abstractRecord.ExamIrisNeovascularization != null)
                {
                    RadioButtonExamIrisNeovascularizationYes.Checked = ((bool)abstractRecord.ExamIrisNeovascularization);
                    RadioButtonExamIrisNeovascularizationNo.Checked = !((bool)abstractRecord.ExamIrisNeovascularization);
                }
                if (abstractRecord.ExamFluorescein != null)
                {
                    RadioButtonExamFluoresceinYes.Checked = ((bool)abstractRecord.ExamFluorescein);
                    RadioButtonExamFluoresceinNo.Checked = !((bool)abstractRecord.ExamFluorescein);
                }



                if (abstractRecord.ExamCMT != null) TextBoxExamCMT.Text = abstractRecord.ExamCMT.ToString();
                if (abstractRecord.TherapyOtherText != null) TextBoxTherapyOtherText.Text = abstractRecord.TherapyOtherText.ToString();
                if (abstractRecord.OutcomeCST != null) TextBoxOutcomeCST.Text = abstractRecord.OutcomeCST.ToString();
                if (abstractRecord.OutcomeTherapyOtherText != null) TextBoxOutcomeTherapyOtherText.Text = abstractRecord.OutcomeTherapyOtherText.ToString();




            }
        }
    }


    public void savedata()
    {
        using (NetHealthPIMEntities pim = new NetHealthPIMEntities())
        {
            int ModuleID = (int)Session[Constants.SESSION_WORKINGMODULEID];
            int cycleID = ctx.ActiveModuleCycleID;
            ParticipantModuleCycle cycle = pim.ParticipantModuleCycle.First(c => c.ParticipantModuleCycleID == cycleID);
            string recordIdentifier = HiddenFieldRecordIdentifier.Value;
            using (TransactionScope transaction = new TransactionScope())
            {
                ChartAbstractionCRVO abstractRecord = (from c in pim.ChartAbstractionCRVO
                                                         where c.ParticipantModuleCycle.ParticipantModuleCycleID == cycleID & c.RecordIdentifier == recordIdentifier
                                                       select c).First<ChartAbstractionCRVO>();

               
                abstractRecord.ExamCMTNA = CheckBoxExamCMTNA.Checked;
                abstractRecord.TherapyBevacizumab = CheckBoxTherapyBevacizumab.Checked;
                abstractRecord.TherapyRanibizumab = CheckBoxTherapyRanibizumab.Checked;
                abstractRecord.TherapyIntravitrealSteroids = CheckBoxTherapyIntravitrealSteroids.Checked;
                abstractRecord.TherapyOther = CheckBoxTherapyOther.Checked;
                abstractRecord.TherapyNone = CheckBoxTherapyNone.Checked;
                abstractRecord.OutcomeCSTNA = CheckBoxOutcomeCSTNA.Checked;
                abstractRecord.OutcomeTherapyBevacizumab = CheckBoxOutcomeTherapyBevacizumab.Checked;
                abstractRecord.OutcomeTherapyRanibizumab = CheckBoxOutcomeTherapyRanibizumab.Checked;
                abstractRecord.OutcomeTherapyPRP = CheckBoxOutcomeTherapyPRP.Checked;
                abstractRecord.OutcomeTherapyPPVx = CheckBoxOutcomeTherapyPPVx.Checked;
                abstractRecord.OutcomeTherapyCataractSurgery = CheckBoxOutcomeTherapyCataractSurgery.Checked;
                abstractRecord.OutcomeTherapyLaser = CheckBoxOutcomeTherapyLaser.Checked;
                abstractRecord.OutcomeTherapyOther = CheckBoxOutcomeTherapyOther.Checked;
                abstractRecord.OutcomeTherapyIntravit = CheckBoxOutcomeTherapyIntravit.Checked;


                if (RadioButtonListRBGender.SelectedIndex != -1) abstractRecord.RBGender = Convert.ToInt32(RadioButtonListRBGender.SelectedValue);
                else abstractRecord.RBGender = 0;
                if (RadioButtonListRBDrugReaction.SelectedIndex != -1) abstractRecord.RBDrugReaction = Convert.ToInt32(RadioButtonListRBDrugReaction.SelectedValue);
                else abstractRecord.RBDrugReaction = 0;
                if (RadioButtonListRBExamEyeID.SelectedIndex != -1) abstractRecord.RBExamEyeID = Convert.ToInt32(RadioButtonListRBExamEyeID.SelectedValue);
                else abstractRecord.RBExamEyeID = 0;
                if (RadioButtonListRBRAPDMeasured.SelectedIndex != -1) abstractRecord.RBRAPDMeasured = Convert.ToInt32(RadioButtonListRBRAPDMeasured.SelectedValue);
                else abstractRecord.RBRAPDMeasured = 0;
                if (RadioButtonListRBIntravitrealPharma.SelectedIndex != -1) abstractRecord.RBIntravitrealPharma = Convert.ToInt32(RadioButtonListRBIntravitrealPharma.SelectedValue);
                else abstractRecord.RBIntravitrealPharma = 0;
                if (RadioButtonListRBNoTherapyReason.SelectedIndex != -1) abstractRecord.RBNoTherapyReason = Convert.ToInt32(RadioButtonListRBNoTherapyReason.SelectedValue);
                else abstractRecord.RBNoTherapyReason = 0;
                if (RadioButtonListRBPatientFollowupSchedule.SelectedIndex != -1) abstractRecord.RBPatientFollowupSchedule = Convert.ToInt32(RadioButtonListRBPatientFollowupSchedule.SelectedValue);
                else abstractRecord.RBPatientFollowupSchedule = 0;
                if (RadioButtonListRBPatientFollowupIOP.SelectedIndex != -1) abstractRecord.RBPatientFollowupIOP = Convert.ToInt32(RadioButtonListRBPatientFollowupIOP.SelectedValue);
                else abstractRecord.RBPatientFollowupIOP = 0;
                if (RadioButtonListRBOutcomeMacularEdema.SelectedIndex != -1) abstractRecord.RBOutcomeMacularEdema = Convert.ToInt32(RadioButtonListRBOutcomeMacularEdema.SelectedValue);
                else abstractRecord.RBOutcomeMacularEdema = 0;
                if (RadioButtonListRBNoCMETherapyReason.SelectedIndex != -1) abstractRecord.RBNoCMETherapyReason = Convert.ToInt32(RadioButtonListRBNoCMETherapyReason.SelectedValue);
                else abstractRecord.RBNoCMETherapyReason = 0;
                if (RadioButtonListRBIOPElevationDeveloped.SelectedIndex != -1) abstractRecord.RBIOPElevationDeveloped = Convert.ToInt32(RadioButtonListRBIOPElevationDeveloped.SelectedValue);
                else abstractRecord.RBIOPElevationDeveloped = 0;
                if (RadioButtonListRBCataractDeveloped.SelectedIndex != -1) abstractRecord.RBCataractDeveloped = Convert.ToInt32(RadioButtonListRBCataractDeveloped.SelectedValue);
                else abstractRecord.RBCataractDeveloped = 0;



                if (DropDownListMonthOfBirth.SelectedIndex > 0) abstractRecord.MonthOfBirth = Convert.ToInt32(DropDownListMonthOfBirth.SelectedValue);
                else abstractRecord.MonthOfBirth = 0;
                if (DropDownListYearOfBirth.SelectedIndex > 0) abstractRecord.YearOfBirth = Convert.ToInt32(DropDownListYearOfBirth.SelectedValue);
                else abstractRecord.YearOfBirth = 0;
                if (DropDownListMonthOfExam.SelectedIndex > 0) abstractRecord.MonthOfExam = Convert.ToInt32(DropDownListMonthOfExam.SelectedValue);
                else abstractRecord.MonthOfExam = 0;
                if (DropDownListYearOfExam.SelectedIndex > 0) abstractRecord.YearOfExam = Convert.ToInt32(DropDownListYearOfExam.SelectedValue);
                else abstractRecord.YearOfExam = 0;
                if (DropDownListMonthOfFollowUp.SelectedIndex > 0) abstractRecord.MonthOfFollowUp = Convert.ToInt32(DropDownListMonthOfFollowUp.SelectedValue);
                else abstractRecord.MonthOfFollowUp = 0;
                if (DropDownListYearOfFollowUp.SelectedIndex > 0) abstractRecord.YearOfFollowUp = Convert.ToInt32(DropDownListYearOfFollowUp.SelectedValue);
                else abstractRecord.YearOfFollowUp = 0;
                if (DropDownListMonthOfDiagnosis.SelectedIndex > 0) abstractRecord.MonthOfDiagnosis = Convert.ToInt32(DropDownListMonthOfDiagnosis.SelectedValue);
                else abstractRecord.MonthOfDiagnosis = 0;
                if (DropDownListYearOfDiagnosis.SelectedIndex > 0) abstractRecord.YearOfDiagnosis = Convert.ToInt32(DropDownListYearOfDiagnosis.SelectedValue);
                else abstractRecord.YearOfDiagnosis = 0;
                if (DropDownListInvolvedBCVA.SelectedIndex > 0) abstractRecord.InvolvedBCVA = Convert.ToInt32(DropDownListInvolvedBCVA.SelectedValue);
                else abstractRecord.InvolvedBCVA = 0;
                if (DropDownListFellowBCVA.SelectedIndex > 0) abstractRecord.FellowBCVA = Convert.ToInt32(DropDownListFellowBCVA.SelectedValue);
                else abstractRecord.FellowBCVA = 0;
                if (DropDownListOutcomeTreatmentEyeBCVA.SelectedIndex > 0) abstractRecord.OutcomeTreatmentEyeBCVA = Convert.ToInt32(DropDownListOutcomeTreatmentEyeBCVA.SelectedValue);
                else abstractRecord.OutcomeTreatmentEyeBCVA = 0;
                if (DropDownListOutcomeTreatmentFellowBCVA.SelectedIndex > 0) abstractRecord.OutcomeTreatmentFellowBCVA = Convert.ToInt32(DropDownListOutcomeTreatmentFellowBCVA.SelectedValue);
                else abstractRecord.OutcomeTreatmentFellowBCVA = 0;



                if (RadioButtonSystemicMedicationsYes.Checked) abstractRecord.SystemicMedications = true;
                if (RadioButtonSystemicMedicationsNo.Checked) abstractRecord.SystemicMedications = false;
                if (RadioButtonExamIrisNeovascularizationYes.Checked) abstractRecord.ExamIrisNeovascularization = true;
                if (RadioButtonExamIrisNeovascularizationNo.Checked) abstractRecord.ExamIrisNeovascularization = false;
                if (RadioButtonExamFluoresceinYes.Checked) abstractRecord.ExamFluorescein = true;
                if (RadioButtonExamFluoresceinNo.Checked) abstractRecord.ExamFluorescein = false;



                try
                {
                if (TextBoxExamCMT.Text != null)
                abstractRecord.ExamCMT = Convert.ToDouble(TextBoxExamCMT.Text);
                }
                catch (Exception ex)
                {
                abstractRecord.ExamCMT = null;
                TextBoxExamCMT.Text = "";
                }
                try
                {
                if (TextBoxTherapyOtherText.Text != null)
                abstractRecord.TherapyOtherText = TextBoxTherapyOtherText.Text;
                }
                catch (Exception ex)
                {
                abstractRecord.TherapyOtherText = null;
                TextBoxTherapyOtherText.Text = "";
                }
                try
                {
                if (TextBoxOutcomeCST.Text != null)
                abstractRecord.OutcomeCST = Convert.ToDouble(TextBoxOutcomeCST.Text);
                }
                catch (Exception ex)
                {
                abstractRecord.OutcomeCST = null;
                TextBoxOutcomeCST.Text = "";
                }
                try
                {
                if (TextBoxOutcomeTherapyOtherText.Text != null)
                abstractRecord.OutcomeTherapyOtherText = TextBoxOutcomeTherapyOtherText.Text;
                }
                catch (Exception ex)
                {
                abstractRecord.OutcomeTherapyOtherText = null;
                TextBoxOutcomeTherapyOtherText.Text = "";
                }
                abstractRecord.LastUpdateDate = DateTime.Now;
                bool ChartCompleted = isComplete();
                abstractRecord.Complete = ChartCompleted;
                var chartRegistration = (from cr in pim.ChartRegistration
                                         where cr.ParticipantModuleCycleID == cycle.ParticipantModuleCycleID
                                          & cr.ModuleID == ModuleID
                                          & cr.RecordIdentifier == recordIdentifier
                                         select cr).First<NetHealthPIMModel.ChartRegistration>();
                chartRegistration.AbstractCompleted = ChartCompleted;
                if ((DropDownListYearOfBirth.SelectedIndex > 0) && (DropDownListYearOfBirth.SelectedIndex > 0))
                {
                    chartRegistration.DOB = abstractRecord.MonthOfBirth + "/" + abstractRecord.YearOfBirth;
                }
                if ((DropDownListMonthOfExam.SelectedIndex > 0) && (DropDownListYearOfExam.SelectedIndex > 0))
                {
                    chartRegistration.InitialVisit = abstractRecord.MonthOfExam + "/" + abstractRecord.YearOfExam;
                }
                pim.SaveChanges();

                transaction.Complete();




            }

            CycleManager.UpdateParticipantCompletedModules(cycleID, ModuleID);
        }


    }

    protected void ButtonSubmit_Click(object sender, EventArgs e)
    {
        savedata();
        isComplete();
        if (!isComplete())
        {
            isComplete();
            string script = " var answer = confirm('Chart data is not complete.\\nClick OK to save entries and exit chart.\\nClick CANCEL to save entries and review the chart. '); if (answer==true) window.location = '../PatientChartRegistration.aspx';";
            ScriptManager.RegisterStartupScript(this, GetType(),
                          "ServerControlScript", script, true);
        }
        else
        {

            Response.Redirect("../PatientChartRegistration.aspx");
        }
    }
    protected bool isComplete()
    {

        bool ChartCompleted = true;

        LabelRBGender.Visible = false;

        LabelRBDrugReaction.Visible = false;

        LabelRBExamEyeID.Visible = false;

        LabelRBRAPDMeasured.Visible = false;

        LabelRBIntravitrealPharma.Visible = false;

        LabelRBNoTherapyReason.Visible = false;

        LabelRBPatientFollowupSchedule.Visible = false;

        LabelRBPatientFollowupIOP.Visible = false;

        LabelRBOutcomeMacularEdema.Visible = false;

        LabelRBNoCMETherapyReason.Visible = false;

        LabelRBIOPElevationDeveloped.Visible = false;

        LabelRBCataractDeveloped.Visible = false;




        LabelMonthOfBirth.Visible = false;

        LabelYearOfBirth.Visible = false;

        LabelMonthOfExam.Visible = false;


        LabelYearOfExam.Visible = false;


        LabelMonthOfFollowUp.Visible = false;


        LabelYearOfFollowUp.Visible = false;

        LabelMonthOfDiagnosis.Visible = false;

        LabelYearOfDiagnosis.Visible = false;

        LabelInvolvedBCVA.Visible = false;

        LabelFellowBCVA.Visible = false;

        LabelExamCMT.Visible = false;

        LabelOutcomeTreatmentEyeBCVA.Visible = false;

        LabelOutcomeTreatmentFellowBCVA.Visible = false;

        LabelOutcomeCST.Visible = false;

        LabelCheckBoxTherapyBevacizumab.Visible = false;
        LabelRBNoTherapyReason.Visible = false;


        LabelSystemicMedications.Visible = false;

        LabelExamIrisNeovascularization.Visible = false;

        LabelExamFluorescein.Visible = false;
        Labelinitialfol.Visible = false;

      
        Labelinitialvis.Visible = false;
        NetHealthPIMEntities pim = new NetHealthPIMEntities();
        var moduleinfo = (from c in pim.Module where c.ModuleID == 58 select new { c.RegistrationFollowUpVisitMonths, c.RegistrationInitialVisitMonths, c.RegistrationMinAge }).FirstOrDefault();
        string message = "Patient must be  at least " + moduleinfo.RegistrationMinAge + " years old<br />";
        string message1 = "Initial visit must be within " + moduleinfo.RegistrationInitialVisitMonths + " months<br />";
        string message2 = "Follow up visit must be at least " + moduleinfo.RegistrationFollowUpVisitMonths + " months after initial visit<br />";
        if (DropDownListYearOfExam.SelectedIndex != 0 && DropDownListMonthOfExam.SelectedIndex != 0 && DropDownListYearOfFollowUp.SelectedIndex != 0 && DropDownListMonthOfFollowUp.SelectedIndex != 0)
        {
            DateTime monthofsurgery = new DateTime(Convert.ToInt32(DropDownListYearOfExam.SelectedValue), Convert.ToInt32(DropDownListMonthOfExam.SelectedValue), 01);
            DateTime folowup = new DateTime(Convert.ToInt32(DropDownListYearOfFollowUp.SelectedValue), Convert.ToInt32(DropDownListMonthOfFollowUp.SelectedValue), 01);
            string registrationminage2 = Validation.validateRegistrationFolowUpMonth(monthofsurgery, folowup, Convert.ToInt32(moduleinfo.RegistrationFollowUpVisitMonths), message2);
            string registrationminage = Validation.validateRegistrationMinAge(Convert.ToInt32(DropDownListYearOfExam.SelectedValue), Convert.ToInt32(DropDownListYearOfBirth.SelectedValue), Convert.ToInt32(moduleinfo.RegistrationMinAge), message);
            string registrationminage1 = Validation.validateRegistrationInitialVisitMonth(DateTime.Now, monthofsurgery, Convert.ToInt32(moduleinfo.RegistrationInitialVisitMonths), message1);



            if (registrationminage1 != "")
            {
                Labelinitialvis.Text = message1;
                Labelinitialvis.Visible = true;
                ChartCompleted = false;
            }
            if (registrationminage2 != "")
            {
                Labelinitialfol.Text = message2;
                Labelinitialfol.Visible = true;
                ChartCompleted = false;
            }

        }
        if (RadioButtonListRBGender.SelectedIndex == -1)
        {
            LabelRBGender.Visible = true;
            ChartCompleted = false;
        }
        if (RadioButtonListRBDrugReaction.SelectedIndex == -1)
        {
            LabelRBDrugReaction.Visible = true;
            ChartCompleted = false;
        }
        if (RadioButtonListRBExamEyeID.SelectedIndex == -1)
        {
            LabelRBExamEyeID.Visible = true;
            ChartCompleted = false;
        }
        if (RadioButtonListRBRAPDMeasured.SelectedIndex == -1)
        {
            LabelRBRAPDMeasured.Visible = true;
            ChartCompleted = false;
        }
        if (RadioButtonListRBIntravitrealPharma.SelectedIndex == -1)
        {
            LabelRBIntravitrealPharma.Visible = true;
            ChartCompleted = false;
        }
        if (RadioButtonListRBIntravitrealPharma.SelectedValue == "1")
        {
            if (!CheckBoxTherapyBevacizumab.Checked && !CheckBoxTherapyRanibizumab.Checked && !CheckBoxTherapyIntravitrealSteroids.Checked && !CheckBoxTherapyOther.Checked && !CheckBoxTherapyNone.Checked)
            {
                LabelCheckBoxTherapyBevacizumab.Visible = true;
                ChartCompleted = false;
            } 
                    
        }
        if (RadioButtonListRBIntravitrealPharma.SelectedValue == "2")
        {
            if (RadioButtonListRBNoTherapyReason.SelectedIndex == -1)
            {
                LabelRBNoTherapyReason.Visible = true;
                ChartCompleted = false;
            }

        }





     
        if (RadioButtonListRBPatientFollowupSchedule.SelectedIndex == -1)
        {
            LabelRBPatientFollowupSchedule.Visible = true;
            ChartCompleted = false;
        }
    
        if (RadioButtonListRBOutcomeMacularEdema.SelectedIndex == -1)
        {
            LabelRBOutcomeMacularEdema.Visible = true;
            ChartCompleted = false;
        }
        if (RadioButtonListRBNoCMETherapyReason.SelectedIndex == -1)
        {
            LabelRBNoCMETherapyReason.Visible = true;
            ChartCompleted = false;
        }
     
    



        if (DropDownListMonthOfBirth.SelectedIndex == 0)
        {
            LabelMonthOfBirth.Visible = true;
            ChartCompleted = false;
        }
        if (DropDownListYearOfBirth.SelectedIndex == 0)
        {
            LabelYearOfBirth.Visible = true;
            ChartCompleted = false;
        }
        if (DropDownListMonthOfExam.SelectedIndex == 0)
        {
            LabelMonthOfExam.Visible = true;
            ChartCompleted = false;
        }
        if (DropDownListYearOfExam.SelectedIndex == 0)
        {
            LabelYearOfExam.Visible = true;
            ChartCompleted = false;
        }
        if (DropDownListMonthOfFollowUp.SelectedIndex == 0)
        {
            LabelMonthOfFollowUp.Visible = true;
            ChartCompleted = false;
        }
        if (DropDownListYearOfFollowUp.SelectedIndex == 0)
        {
            LabelYearOfFollowUp.Visible = true;
            ChartCompleted = false;
        }
        if (DropDownListMonthOfDiagnosis.SelectedIndex == 0)
        {
            LabelMonthOfDiagnosis.Visible = true;
            ChartCompleted = false;
        }
        if (DropDownListYearOfDiagnosis.SelectedIndex == 0)
        {
            LabelYearOfDiagnosis.Visible = true;
            ChartCompleted = false;
        }
        if (DropDownListInvolvedBCVA.SelectedIndex == 0)
        {
            LabelInvolvedBCVA.Visible = true;
            ChartCompleted = false;
        }
        if (DropDownListFellowBCVA.SelectedIndex == 0)
        {
            LabelFellowBCVA.Visible = true;
            ChartCompleted = false;
        }
        if (string.IsNullOrEmpty(TextBoxExamCMT.Text) && CheckBoxExamCMTNA.Checked==false)
        {
            LabelExamCMT.Visible = true;
            ChartCompleted = false;
        }
        if (DropDownListOutcomeTreatmentEyeBCVA.SelectedIndex == 0)
        {
            LabelOutcomeTreatmentEyeBCVA.Visible = true;
            ChartCompleted = false;
        }
        if (DropDownListOutcomeTreatmentFellowBCVA.SelectedIndex == 0)
        {
            LabelOutcomeTreatmentFellowBCVA.Visible = true;
            ChartCompleted = false;
        }
        if (string.IsNullOrEmpty(TextBoxOutcomeCST.Text) && CheckBoxOutcomeCSTNA.Checked==false)
        {
            LabelOutcomeCST.Visible = true;
            ChartCompleted = false;
        }



        if (RadioButtonSystemicMedicationsNo.Checked == false && RadioButtonSystemicMedicationsYes.Checked == false)
        {
            LabelSystemicMedications.Visible = true;
            ChartCompleted = false;
        }
        if (RadioButtonExamIrisNeovascularizationNo.Checked == false && RadioButtonExamIrisNeovascularizationYes.Checked == false)
        {
            LabelExamIrisNeovascularization.Visible = true;
            ChartCompleted = false;
        }
        if (RadioButtonExamFluoresceinNo.Checked == false && RadioButtonExamFluoresceinYes.Checked == false)
        {
            LabelExamFluorescein.Visible = true;
            ChartCompleted = false;
        }



        return ChartCompleted;
    }


}

