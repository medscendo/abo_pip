﻿<%@ Page Title="" Language="C#" MasterPageFile="~/PIMMasterPage.master" AutoEventWireup="true" CodeFile="PACGChart.aspx.cs" Inherits="abo_PACGChart" %>

<asp:Content ID="Content1" ContentPlaceHolderID="head" Runat="Server">

    <link type="text/css" href="../../common/css/atooltip.css" rel="stylesheet"  media="screen" />

    <style type="text/css">
        .bginputa{
	        float:right;
	        margin:21px 0 0 1px;
	        background: url(../common/images/orange_button_with_arrow.png) no-repeat;
	        overflow:hidden;
	        height:35px;

        }
        .bginputa a{
	        color: white;
	        text-align:center;
	        height:35px;
	        line-height:28px;
	        padding:0 14px 15px  ;
	        cursor:pointer;
	        float:left;
	        border:none;
	        text-decoration:none;
	        font-family:Arial, Helvetica, sans-serif; 
	        font-size:12px; 
	        font-weight:bold; 
	        letter-spacing: 0px;
        }
        .bginputa a:hover{
	        text-decoration:none;
        }
        .right {
            text-align:right;
        }
    </style>
    
    
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder1" Runat="Server">
    <asp:HiddenField ID="HiddenFieldRecordIdentifier" runat="server"/>
        <!--  pim -->
        <div class="pim">
            <div class="record-ident clearfix">
                <h3 class="record-first">RECORD IDENTIFIER: <asp:Literal runat="server" ID="LiteralRecordIdentifier"></asp:Literal></h3>
                <h3 class="record-second"><asp:Literal runat="server" ID="LiteralAbstractionNumber"></asp:Literal></h3>
            </div>
            <!-- History -->
            <table>
                <tr>
                    <th colspan="2" width="910px"><p>History</p></th>
                </tr>
                <!-- Question 1 -->
                <tr class="table_body">
                    <td width="70%"><strong>1. Date of birth</strong>
                        <br />
                        <asp:Label ID="LabelMonthOfBirth" runat="server" Visible="false"  ForeColor="Red" Text="Please enter Month  "></asp:Label><br />
                        <asp:Label ID="LabelYearOfBirth" runat="server" Visible="false"  ForeColor="Red"  Text="Please enter Year "></asp:Label>
                    </td>
                    <td>
                        <asp:DropDownList ID="DropDownListMonthOfBirth" DataValueField="MonthID" DataTextField="MonthName" runat="server" onchange="DOBValidation();" />
                        <asp:DropDownList ID="DropDownListYearOfBirth" DataValueField="YearID" DataTextField="YearName" runat="server" onchange="DOBValidation();" />
                    </td>
                </tr>
                <!-- Question 2 -->
                <tr class="table_body_bg">
                    <td><strong>2. Date of first exam</strong>
                    <br />
                        <asp:Label ID="LabelMonthofExam" runat="server" Visible="false"  ForeColor="Red" Text="Please enter Month  "></asp:Label><br />
                        <asp:Label ID="LabelMonthofYear" runat="server" Visible="false"  ForeColor="Red"  Text="Please enter Year "></asp:Label><br />
                          <asp:Label ID="Labelinitialage" runat="server" Visible="false"  ForeColor="Red"  Text="Please enter Year "></asp:Label>
                    </td>
                    <td>
                        <asp:DropDownList ID="DropDownListMonthofExam" DataValueField="MonthID" DataTextField="MonthName" runat="server" onchange="firstExamValidation();" />
                        <asp:DropDownList ID="DropDownListMonthofYear" DataValueField="YearID" DataTextField="YearName" runat="server" onchange="firstExamValidation();" />
                    </td>
                </tr>
                <!-- Question 3 -->
                <tr class="table_body">
                    <td><strong>3. Date of most recent exam</strong>
                    <br />
                        <asp:Label ID="LabelOutcomeExamMonth" runat="server" Visible="false"  ForeColor="Red" Text="Please enter Month  "></asp:Label><br />
                        <asp:Label ID="LabelOutcomeExamYear" runat="server" Visible="false"  ForeColor="Red"  Text="Please enter Year "></asp:Label>
                   </td>
                    <td>
                        <asp:DropDownList ID="DropDownListOutcomeExamMonth" DataValueField="MonthID" DataTextField="MonthName" runat="server" onchange="recentExam();" />
                        <asp:DropDownList ID="DropDownListOutcomeExamYear" DataValueField="YearID" DataTextField="YearName" runat="server" onchange="recentExam();" />
                    </td>
                </tr>
                <!-- Question 4 -->
                <tr class="table_body_bg">
                    <td><strong>4. Gender</strong><br />
                    <asp:Label ID="LabelRBGender" runat="server" Visible="false"  ForeColor="Red" Text="Please complete"></asp:Label>

                    </td>
                    <td>
                        <asp:RadioButtonList id="RadioButtonListRBGender" CssClass="aspxList" runat="server">
	                        <asp:ListItem Value="42">&nbsp;Male</asp:ListItem>
	                        <asp:ListItem Value="43">&nbsp;Female</asp:ListItem>
                        </asp:RadioButtonList>
                    </td>
                </tr>
            
                <!-- Question 1 -->
                <tr class="table_body">
                    <td width="70%"><strong>5. Are current ocular medications listed?</strong>
                         <br />
                        <asp:Label ID="LabelOcularMedications" runat="server" Visible="false"  ForeColor="Red" Text="Please complete"></asp:Label></td>
                    <td>
                        <asp:RadioButtonList id="RadioButtonListOcularMedications" RepeatDirection="Horizontal" CssClass="aspxList" runat="server">
	                        <asp:ListItem Value="1">&nbsp;Yes</asp:ListItem>
	                        <asp:ListItem Value="2">&nbsp;No</asp:ListItem>
	                        <asp:ListItem Value="3">&nbsp;Not Documented</asp:ListItem>
                        </asp:RadioButtonList>
                    </td>
                </tr>
                <!-- Question 2 -->
                <tr class="table_body_bg">
                    <td><strong>6. Are current systemic medications listed?</strong>
                         <br />
                        <asp:Label ID="LabelSystemicMedications" runat="server" Visible="false"  ForeColor="Red" Text="Please complete"></asp:Label></td>
                   </td>
                    <td>
                         <asp:RadioButtonList id="RadioButtonListSystemicMedications" RepeatDirection="Horizontal" CssClass="aspxList" runat="server">
	                        <asp:ListItem Value="1">&nbsp;Yes</asp:ListItem>
	                        <asp:ListItem Value="2">&nbsp;No</asp:ListItem>
	                        <asp:ListItem Value="3">&nbsp;Not Documented</asp:ListItem>
                        </asp:RadioButtonList>
                    </td>
                </tr>
                <!-- Question 3 -->
                <tr class="table_body">
                    <td><strong>7. Was an ocular history performed?</strong>
                         <br />
                        <asp:Label ID="LabelOcularHistory" runat="server" Visible="false"  ForeColor="Red" Text="Please complete"></asp:Label></td>
                   </td>
                    <td>
                        <asp:RadioButton runat="server" ID="RadioButtonOcularHistoryYes" GroupName="OcularHistory" text="&nbsp;Yes" />&nbsp;&nbsp;
                        <asp:RadioButton runat="server" ID="RadioButtonOcularHistoryNo" GroupName="OcularHistory" text="&nbsp;No" />
                    </td>
                </tr>
            </table>
            <br />
            <!-- Examination - First Visit With Angle Closure Glaucoma -->
            <table>
                <tr>
                    <th colspan="3" width="910px"><p>Initial Examination</p></th>
                </tr>
                <!-- Question 1 -->
                <tr class="table_body">
                    <td width="50%" rowspan="2"><strong>1. Best-corrected visual acuity on initial examination</strong>&nbsp;<a href="#"><img id="QE1" src="../../common/images/tip.gif" alt="Tip" /></a>
                         <br />
                        <asp:Label ID="LabelBCVAOD" runat="server" Visible="false"  ForeColor="Red" Text="Please complete"></asp:Label></td>
                   </td>
                    <td width="20%"><span class="right">OD</span></td>
                    <td>
                        <label>20/</label>
                        <asp:DropDownList ID="DropDownListBCVAOD" DataValueField="examValue" DataTextField="examLabel" runat="server" onchange="QE1ODtoggleRB();" />
                        <asp:RadioButtonList id="RadioButtonListRBBCVAODNotAbletoPerform" RepeatDirection="Vertical" CssClass="aspxList" runat="server" onclick="QE1ODtoggleDD();">
                            <asp:ListItem Value="4">&nbsp;Unable to Perform</asp:ListItem>
                            <asp:ListItem Value="3">&nbsp;Not Recorded</asp:ListItem>
                        </asp:RadioButtonList>
                    </td>
                </tr>
                <tr class="table_body">
                    <td><span class="right">OS</span></td>
                    <td>
                        <label>20/</label>
                        <asp:DropDownList ID="DropDownListBCVAOS" DataValueField="examValue" DataTextField="examLabel" onchange="QE1OStoggleRB();" runat="server" />
                        <asp:RadioButtonList id="RadioButtonListRBBCVAOSNotAbletoPerform" RepeatDirection="Vertical" CssClass="aspxList" onclick="QE1OStoggleDD();" runat="server">
                            <asp:ListItem Value="4">&nbsp;Unable to Perform</asp:ListItem>
                            <asp:ListItem Value="3">&nbsp;Not Recorded</asp:ListItem>
                        </asp:RadioButtonList>
                    </td>
                </tr>
                <!-- Question 2 -->
                <tr class="table_body_bg">
                    <td rowspan="2"><strong>2. Initial IOP</strong>
                         <br />
                        <asp:Label ID="LabelIOP" runat="server" Visible="false"  ForeColor="Red" Text="Please complete"></asp:Label></td>
                   </td>
                    <td><span class="right">OD</span></td>
                    <td>
                        <asp:TextBox runat="server" ID="TextBoxIOPODmmHg" size="5" onkeyup="QE2ODtoggleRB();" /><label>&nbsp;mmHg</label>
                        <asp:RadioButtonList id="RadioButtonListRBIOPODNotAbletoPerform" RepeatDirection="Vertical" CssClass="aspxList" runat="server" onclick="QE2ODtoggleTB();">
	                        <asp:ListItem Value="4">&nbsp;Unable to Perform</asp:ListItem>
                            <asp:ListItem Value="3">&nbsp;Not Recorded</asp:ListItem>
                        </asp:RadioButtonList>
                    </td>
                </tr>
                <tr class="table_body_bg">
                    <td><span class="right">OS</span></td>
                    <td>
                        <asp:TextBox runat="server" ID="TextBoxIOPOSmmHg" size="5" onkeyup="QE2OStoggleRB();" /><label>&nbsp;mmHg</label>
                        <asp:RadioButtonList id="RadioButtonListRBIOPOSNotAbletoPerform" RepeatDirection="Vertical" CssClass="aspxList" runat="server" onclick="QE2OStoggleTB();" >
                            <asp:ListItem Value="4">&nbsp;Unable to Perform</asp:ListItem>
                            <asp:ListItem Value="3">&nbsp;Not Recorded</asp:ListItem>
                        </asp:RadioButtonList>
                    </td>
                </tr>
                <!-- Question 3 -->
                <tr class="table_body">
                    <td rowspan="2"><strong>3. Was gonioscopy performed?</strong>
                         <br />
                        <asp:Label ID="LabelRBGonioscopy" runat="server" Visible="false"  ForeColor="Red" Text="Please complete"></asp:Label>
                    </td>
                    <td><span class="right">OD</span></td>
                    <td>
                        <asp:RadioButtonList id="RadioButtonListRBGonioscopyOD" RepeatDirection="Horizontal" CssClass="aspxList" runat="server">
	                        <asp:ListItem Value="1">&nbsp;Yes</asp:ListItem>
	                        <asp:ListItem Value="2">&nbsp;No</asp:ListItem>
	                        <asp:ListItem Value="4">&nbsp;Unable to Perform</asp:ListItem>
                        </asp:RadioButtonList>
                    </td>
                </tr>
                <tr class="table_body">
                    <td><span class="right">OS</span></td>
                    <td>
                        <asp:RadioButtonList id="RadioButtonListRBGonioscopyOS" RepeatDirection="Horizontal" CssClass="aspxList" runat="server">
	                        <asp:ListItem Value="1">&nbsp;Yes</asp:ListItem>
	                        <asp:ListItem Value="2">&nbsp;No</asp:ListItem>
	                        <asp:ListItem Value="4">&nbsp;Unable to Perform</asp:ListItem>
                        </asp:RadioButtonList>
                    </td>
                </tr>
                <!-- Question 4 -->
                <tr class="table_body_bg">
                    <td rowspan="2"><strong>4. Was slit lamp examination performed?</strong>
                         <br />
                        <asp:Label ID="LabelRBSlitLamp" runat="server" Visible="false"  ForeColor="Red" Text="Please complete"></asp:Label></td>
                   </td>
                    <td><span class="right">OD</span></td>
                    <td>
                        <asp:RadioButtonList id="RadioButtonListRBSlitLampOD" RepeatDirection="Horizontal" CssClass="aspxList" runat="server">
	                        <asp:ListItem Value="1">&nbsp;Yes</asp:ListItem>
	                        <asp:ListItem Value="2">&nbsp;No</asp:ListItem>
	                        <asp:ListItem Value="4">&nbsp;Unable to Perform</asp:ListItem>
                        </asp:RadioButtonList>
                    </td>
                </tr>
                <tr class="table_body_bg">
                    <td><span class="right">OS</span></td>
                    <td>
                        <asp:RadioButtonList id="RadioButtonListRBSlitLampOS" RepeatDirection="Horizontal" CssClass="aspxList" runat="server">
	                        <asp:ListItem Value="1">&nbsp;Yes</asp:ListItem>
	                        <asp:ListItem Value="2">&nbsp;No</asp:ListItem>
	                        <asp:ListItem Value="4">&nbsp;Unable to Perform</asp:ListItem>
                        </asp:RadioButtonList>
                    </td>
                </tr>
                <!-- Question 5 -->
                <tr class="table_body">
                    <td colspan="2"><strong>5. Was the optic nerve appearance documented?</strong>&nbsp;<a href="#"><img id="QE5" src="../../common/images/tip.gif" alt="Tip" /></a>
                         <br />
                        <asp:Label ID="LabelRBOpticNerveApperanceBothEyes" runat="server" Visible="false"  ForeColor="Red" Text="Please complete"></asp:Label></td>
                   </td>
                    <td>
                        <asp:RadioButtonList id="RadioButtonListRBOpticNerveApperanceBothEyes" RepeatDirection="Horizontal" CssClass="aspxList" runat="server">
	                        <asp:ListItem Value="1">&nbsp;Yes</asp:ListItem>
	                        <asp:ListItem Value="2">&nbsp;No</asp:ListItem>
	                        <asp:ListItem Value="4">&nbsp;Unable to Perform</asp:ListItem>
                        </asp:RadioButtonList>
                    </td>
                </tr>
            </table>
            <br />
            <!-- Assessment and Management -->
            <table>
                <tr>
                    <th colspan="3" width="910px"><p>Assessment and Management</p></th>
                </tr>
                <!-- Question 1 -->
                <tr class="table_body">
                    <td width="50%" rowspan="2"><strong>1. Type of angle closure at initial exam?</strong>
                         <br />
                        <asp:Label ID="LabelRBAngleClosureType" runat="server" Visible="false"  ForeColor="Red" Text="Please complete"></asp:Label></td>
                   </td>
                    <td width="20%"><span class="right">OD</span></td>
                    <td>
                        <asp:RadioButtonList id="RadioButtonListRBAngleClosureTypeOD" RepeatDirection="Vertical" CssClass="aspxList" runat="server">
	                        <asp:ListItem Value="44">&nbsp;Narrow angle with or without appositional closure</asp:ListItem>
	                        <asp:ListItem Value="45">&nbsp;Chronic primary angle-closure without glaucoma</asp:ListItem>
	                        <asp:ListItem Value="46">&nbsp;Narrow angle with co-existing glaucoma</asp:ListItem>
                            <asp:ListItem Value="47">&nbsp;Chronic primary angle-closure with glaucoma</asp:ListItem>
	                        <asp:ListItem Value="3">&nbsp;Not Recorded</asp:ListItem>
                        </asp:RadioButtonList>
                    </td>
                </tr>
                <tr class="table_body">
                    <td><span class="right">OS</span></td>
                    <td>
                        <asp:RadioButtonList id="RadioButtonListRBAngleClosureTypeOS" RepeatDirection="Vertical" CssClass="aspxList" runat="server">
	                        <asp:ListItem Value="44">&nbsp;Narrow angle with or without appositional closure</asp:ListItem>
	                        <asp:ListItem Value="45">&nbsp;Chronic primary angle-closure without glaucoma</asp:ListItem>
	                        <asp:ListItem Value="46">&nbsp;Narrow angle with co-existing glaucoma</asp:ListItem>
                            <asp:ListItem Value="47">&nbsp;Chronic primary angle-closure with glaucoma</asp:ListItem>
	                        <asp:ListItem Value="3">&nbsp;Not Recorded</asp:ListItem>
                        </asp:RadioButtonList>
                    </td>
                </tr>
                <!-- Question 2 -->
                <tr class="table_body_bg">
                    <td colspan="2"><strong>2. Was a management plan formulated?</strong>
                         <br />
                        <asp:Label ID="LabelManagementPlanFormulated" runat="server" Visible="false"  ForeColor="Red" Text="Please complete"></asp:Label></td>
                   </td>
                    <td>
                        <asp:RadioButton runat="server" ID="RadioButtonManagementPlanFormulatedYes" GroupName="ManagementPlanFormulated" text="&nbsp;Yes" />&nbsp;&nbsp;
                        <asp:RadioButton runat="server" ID="RadioButtonManagementPlanFormulatedNo" GroupName="ManagementPlanFormulated" text="&nbsp;No" />
                    </td>
                </tr>
                <!-- Question 3 -->
                <tr class="table_body">
                    <td colspan="2"><strong>3. Was a follow-up examination scheduled?</strong>
                         <br />
                        <asp:Label ID="LabelFollowUpExamScheduled" runat="server" Visible="false"  ForeColor="Red" Text="Please complete"></asp:Label></td>
                   </td>
                    <td>
                        <asp:RadioButton runat="server" ID="RadioButtonFollowUpExamScheduledYes" GroupName="FollowUpExamScheduled" text="&nbsp;Yes" onclick="QA3isYes();" />&nbsp;&nbsp;
                        <asp:RadioButton runat="server" ID="RadioButtonFollowUpExamScheduledNo" GroupName="FollowUpExamScheduled" text="&nbsp;No" onclick="QA3isYes();" />
                    </td>
                </tr>
                <!-- Question 4 -->
                <tr class="table_body">
                    <td colspan="2"><span id="QA4txta"><strong>3a. When was the next exam scheduled?</strong></span>
                         <br />
                        <asp:Label ID="LabelFollowUpExamDate" runat="server" Visible="false"  ForeColor="Red" Text="Please complete"></asp:Label></td>
                   </td>
                    <td>
                        <span id="QA4txtb">
                        <asp:DropDownList ID="DropDownListFollowUpExamDateAmount" DataValueField="MonthID" DataTextField="MonthName" runat="server" />
                        <asp:DropDownList ID="DropDownListFollowUpExamDateType" DataValueField="YearID" DataTextField="YearName" runat="server" />
                        </span>
                    </td>
                </tr>
            </table>
            <br />
            <!-- Follow-up (From First to Last Visit) -->
            <table>
                <tr>
                    <th colspan="3" width="910px"><p>Follow-up (From First to Last Visit)</p></th>
                </tr>
                <!-- Question 1 -->
                <tr class="table_body">
                    <td width="70%"><strong>1. Was an iridotomy/iridectomy performed?</strong>
                         <br />
                        <asp:Label ID="LabelIridotomy" runat="server" Visible="false"  ForeColor="Red" Text="Please complete"></asp:Label></td>
                    <td>
                        <asp:RadioButton runat="server" ID="RadioButtonIridotomyYes" GroupName="Iridotomy" text="&nbsp;Yes" />&nbsp;&nbsp;
                        <asp:RadioButton runat="server" ID="RadioButtonIridotomyNo" GroupName="Iridotomy" text="&nbsp;No" />
                    </td>
                </tr>
                <!-- Question 2 -->
                <tr class="table_body_bg">
                    <td><strong>2. Was cataract surgery performed?</strong>
                         <br />
                        <asp:Label ID="LabelCataractSurgery" runat="server" Visible="false"  ForeColor="Red" Text="Please complete"></asp:Label></td>
                    <td>
                        <asp:RadioButton runat="server" ID="RadioButtonCataractSurgeryYes" GroupName="CataractSurgery" text="&nbsp;Yes" />&nbsp;&nbsp;
                        <asp:RadioButton runat="server" ID="RadioButtonCataractSurgeryNo" GroupName="CataractSurgery" text="&nbsp;No" />
                    </td>
                </tr>
                <!-- Question 3 -->
                <tr class="table_body">
                    <td><strong>3. Was goniosynechialysis performed?</strong>
                         <br />
                        <asp:Label ID="LabelGoniosynechialysis" runat="server" Visible="false"  ForeColor="Red" Text="Please complete"></asp:Label></td>
                    <td>
                        <asp:RadioButton runat="server" ID="RadioButtonFollowUpGoniosynechialysisYes" GroupName="FollowUpGoniosynechialysis" text="&nbsp;Yes" />&nbsp;&nbsp;
                        <asp:RadioButton runat="server" ID="RadioButtonFollowUpGoniosynechialysisNo" GroupName="FollowUpGoniosynechialysis" text="&nbsp;No" />
                    </td>
                </tr>
            </table>
            <br />
            <!-- Outcomes -->
            <table>
                <tr>
                    <th colspan="3" width="910px"><p>Outcomes</p></th>
                </tr>
                <!-- Question 1 -->
                <tr class="table_body">
                    <td rowspan="2" width="50%"><strong>1. Best-corrected visual acuity at most recent examination</strong>&nbsp;<a href="#"><img id="QO1" src="../../common/images/tip.gif" alt="Tip" /></a>
                         <br />
                        <asp:Label ID="LabelOutcomeBCVA" runat="server" Visible="false"  ForeColor="Red" Text="Please complete"></asp:Label></td>
                    <td width="20%"><span class="right">OD</span></td>
                    <td>
                        <label>20 / </label>
                        <asp:DropDownList ID="DropDownListOutcomeBCVAOD" DataValueField="examValue" DataTextField="examLabel" onchange="QO1ODtoggleRB();" runat="server" />
                        <asp:RadioButtonList id="RadioButtonListOutcomeRBBCVAODNotAbletoPerform" RepeatDirection="Vertical" CssClass="aspxList" onclick="QO1ODtoggleDD();" runat="server">
                            <asp:ListItem Value="4">&nbsp;Unable to Perform</asp:ListItem>
                            <asp:ListItem Value="3">&nbsp;Not Recorded</asp:ListItem>
                        </asp:RadioButtonList>
                    </td>
                </tr>
                <tr class="table_body">
                    <td><span class="right">OS</span></td>
                    <td>
                        <label>20 / </label>
                        <asp:DropDownList ID="DropDownListOutcomeBCVAOS" DataValueField="examValue" DataTextField="examLabel" onchange="QO1OStoggleRB();" runat="server" />
                        <asp:RadioButtonList id="RadioButtonListOutcomeRBBCVAOSNotAbletoPerform" RepeatDirection="Vertical" CssClass="aspxList" onclick="QO1OStoggleDD();" runat="server">
                            <asp:ListItem Value="4">&nbsp;Unable to Perform</asp:ListItem>
                            <asp:ListItem Value="3">&nbsp;Not Recorded</asp:ListItem>
                        </asp:RadioButtonList>
                    </td>
                </tr>
                <!-- Question 2 -->
                <tr class="table_body_bg">
                    <td rowspan="2"><strong>2. What is the IOP at most recent visit?</strong>
                         <br />
                        <asp:Label ID="LabelGender" runat="server" Visible="false"  ForeColor="Red" Text="Please complete"></asp:Label></td>
                   </td>
                    <td><span class="right">OD</span></td>
                    <td>
                        <asp:TextBox runat="server" ID="TextBoxOutcomeIOPODmmHg" onkeyup="QO2ODtoggleRB();" size="5" />
                        <asp:RadioButtonList id="RadioButtonListRBOutcomeIOPODNotAbletoPerform" RepeatDirection="Vertical" CssClass="aspxList" onclick="QO2ODtoggleTB();" runat="server">
                            <asp:ListItem Value="4">&nbsp;Unable to Perform</asp:ListItem>
                            <asp:ListItem Value="3">&nbsp;Not Recorded</asp:ListItem>
                        </asp:RadioButtonList>
                    </td>
                </tr>
                <tr class="table_body_bg">
                    <td><span class="right">OS</span></td>
                    <td>
                        <asp:TextBox runat="server" ID="TextBoxOutcomeIOPOSmmHg" size="5" onkeyup="QO2OStoggleRB();" />
                        <asp:RadioButtonList id="RadioButtonListRBOutcomeIOPOSNotAbletoPerform" RepeatDirection="Vertical" CssClass="aspxList" onclick="QO2OStoggleTB();" runat="server">
                            <asp:ListItem Value="4">&nbsp;Unable to Perform</asp:ListItem>
                            <asp:ListItem Value="3">&nbsp;Not Recorded</asp:ListItem>
                        </asp:RadioButtonList>
                    </td>
                </tr>
                <!-- Question 3 -->
                <tr class="table_body">
                    <td rowspan="2"><strong>3. On the most recent visit, is the iridotomy/iridectomy patent?</strong>
                         <br />
                        <asp:Label ID="LabelRBOutcomeIridotomy" runat="server" Visible="false"  ForeColor="Red" Text="Please complete"></asp:Label></td>
                   </td>
                    <td><span class="right">OD</span></td>
                    <td>
                        <asp:RadioButtonList id="RadioButtonListRBOutcomeIridotomyOD" RepeatDirection="Vertical" CssClass="aspxList" runat="server">
                            <asp:ListItem Value="1">&nbsp;Yes</asp:ListItem>
                            <asp:ListItem Value="2">&nbsp;No</asp:ListItem>
                            <asp:ListItem Value="4">&nbsp;Unable to Perform</asp:ListItem>
                            <asp:ListItem Value="3">&nbsp;Not Recorded</asp:ListItem>
                            <asp:ListItem Value="48">&nbsp;Not Applicable</asp:ListItem>
                        </asp:RadioButtonList>
                    </td>
                </tr>
                <tr class="table_body">
                    <td><span class="right">OS</span></td>
                    <td>
                        <asp:RadioButtonList id="RadioButtonListRBOutcomeIridotomyOS" RepeatDirection="Vertical" CssClass="aspxList" runat="server">
                            <asp:ListItem Value="1">&nbsp;Yes</asp:ListItem>
                            <asp:ListItem Value="2">&nbsp;No</asp:ListItem>
                            <asp:ListItem Value="4">&nbsp;Unable to Perform</asp:ListItem>
                            <asp:ListItem Value="3">&nbsp;Not Recorded</asp:ListItem>
                            <asp:ListItem Value="48">&nbsp;Not Applicable</asp:ListItem>
                        </asp:RadioButtonList>
                    </td>
                </tr>
                <!-- Question 4 -->
                <tr class="table_body_bg">
                    <td rowspan="2"><strong>4. If the patient did not have surgery, is there an increasing cataract?</strong>
                        <br />(If there was no surgery, select &quot;Not Applicable&quot;)
                         <br />
                        <asp:Label ID="LabelRBOutcomeIncreasingCataract" runat="server" Visible="false"  ForeColor="Red" Text="Please complete"></asp:Label></td>
                   </td>
                    <td><span class="right">OD</span></td>
                    <td>
                        <asp:RadioButtonList id="RadioButtonListRBOutcomeIncreasingCataractOD" RepeatDirection="Vertical" CssClass="aspxList" runat="server">
                            <asp:ListItem Value="1">&nbsp;Yes</asp:ListItem>
                            <asp:ListItem Value="2">&nbsp;No</asp:ListItem>
                            <asp:ListItem Value="3">&nbsp;Not Recorded</asp:ListItem>
                            <asp:ListItem Value="48">&nbsp;Not Applicable</asp:ListItem>
                        </asp:RadioButtonList>
                    </td>
                </tr>
                <tr class="table_body_bg">
                    <td><span class="right">OS</span></td>
                    <td>
                        <asp:RadioButtonList id="RadioButtonListRBOutcomeIncreasingCataractOS" RepeatDirection="Vertical" CssClass="aspxList" runat="server">
                            <asp:ListItem Value="1">&nbsp;Yes</asp:ListItem>
                            <asp:ListItem Value="2">&nbsp;No</asp:ListItem>
                            <asp:ListItem Value="3">&nbsp;Not Recorded</asp:ListItem>
                            <asp:ListItem Value="48">&nbsp;Not Applicable</asp:ListItem>
                        </asp:RadioButtonList>
                    </td>
                </tr>
                <!-- Question 5 -->
                <tr class="table_body">
                    <td rowspan="2"><strong>5. Date of most recent optic nerve head exam?</strong>
                         <br />
                        <asp:Label ID="LabelOutcomeOpticNerveExamODMonth" runat="server" Visible="false"  ForeColor="Red" Text="Please complete Month OD"></asp:Label><br />
                        <asp:Label ID="LabelOutcomeOpticNerveExamODYear" runat="server" Visible="false"  ForeColor="Red" Text="Please complete Year OD"></asp:Label><br />
                        <asp:Label ID="LabelOutcomeOpticNerveExamOSMonth" runat="server" Visible="false"  ForeColor="Red" Text="Please complete Month OS"></asp:Label><br />
                        <asp:Label ID="LabelOutcomeOpticNerveExamOSYear" runat="server" Visible="false"  ForeColor="Red" Text="Please complete Year OS"></asp:Label><br />
                        
                        </td>
                   </td>
                    <td><span class="right">OD</span></td>
                    <td>
                        <asp:DropDownList ID="DropDownListOutcomeOpticNerveExamODMonth" DataValueField="MonthID" DataTextField="MonthName" runat="server" onchange="recentOpticNerveExamOD();" />
                        <asp:DropDownList ID="DropDownListOutcomeOpticNerveExamODYear" DataValueField="YearID" DataTextField="YearName" runat="server" onchange="recentOpticNerveExamOD();" />    
                    </td>
                </tr>
                <tr class="table_body">
                    <td><span class="right">OS</span></td>
                    <td>
                        <asp:DropDownList ID="DropDownListOutcomeOpticNerveExamOSMonth" DataValueField="MonthID" DataTextField="MonthName" runat="server" onchange="recentOpticNerveExamOS()" />
                        <asp:DropDownList ID="DropDownListOutcomeOpticNerveExamOSYear" DataValueField="YearID" DataTextField="YearName" runat="server" onchange="recentOpticNerveExamOS()" />
                    </td>
                </tr>
                <!-- Question 6 -->
                <tr class="table_body_bg">
                    <td rowspan="2"><strong>6. Has the appearance changed from baseline?</strong>
                         <br />
                        <asp:Label ID="LabelRBOpticNerveAppearanceChangeFromBaseline" runat="server" Visible="false"  ForeColor="Red" Text="Please complete"></asp:Label></td>
                   </td>
                    <td><span class="right">OD</span></td>
                    <td>
                        <asp:RadioButtonList id="RadioButtonListRBOpticNerveAppearanceChangeFromBaselineOD" RepeatDirection="Vertical" CssClass="aspxList" runat="server">
	                        <asp:ListItem Value="40">&nbsp;Better</asp:ListItem>
	                        <asp:ListItem Value="41">&nbsp;Worse</asp:ListItem>
                            <asp:ListItem Value="51">&nbsp;No Change</asp:ListItem>
	                        <asp:ListItem Value="4">&nbsp;Not Recorded</asp:ListItem>
                        </asp:RadioButtonList>
                    </td>
                </tr>
                <tr class="table_body_bg">
                    <td><span class="right">OS</span></td>
                    <td>
                        <asp:RadioButtonList id="RadioButtonListRBOpticNerveAppearanceChangeFromBaselineOS" RepeatDirection="Vertical" CssClass="aspxList" runat="server">
	                        <asp:ListItem Value="40">&nbsp;Better</asp:ListItem>
	                        <asp:ListItem Value="41">&nbsp;Worse</asp:ListItem>
                            <asp:ListItem Value="51">&nbsp;No Change</asp:ListItem>
	                        <asp:ListItem Value="4">&nbsp;Not Recorded</asp:ListItem>
                        </asp:RadioButtonList>
                    </td>
                </tr>
            </table>
            <div class="button-box">
                <asp:LinkButton ID="LinkButtonBackToDashboard" runat="server" Text="Back To Chart Registration" PostBackUrl="../PatientChartRegistration.aspx?CycleNumber=1" Visible="false" CssClass="button" />
                <asp:LinkButton ID="ButtonSubmit" runat="server" OnClick="ButtonSubmit_Click" Text="Submit Chart" CssClass="button" />
            </div>
        </div>
        <!-- PACG pim ends -->
    </div>
</asp:Content>

<asp:Content runat="server" ID="Content4" ContentPlaceHolderID="javascript">

    	<script type="text/javascript" src="../../common/js/jquery.min.js"></script>
	<script type="text/javascript" src="../../common/js/jquery.atooltip.js"></script>
    <script type="text/javascript" language="javascript">
        //      Either enables or disables input boxes
        function enabledControl(el, disabled, checked) {
            //alert("Hello world from enabled control");
            try {
                if (disabled) {
                    el.disabled = disabled;
                    el.setAttribute("disabled", true);
                }
                else {
                    el.disabled = disabled;
                    el.removeAttribute("disabled");
                }

                if (checked == true)
                    el.checked = false;
            }
            catch (E) {
            }
            if (el.childNodes && el.childNodes.length > 0) {
                for (var x = 0; x < el.childNodes.length; x++) {
                    enabledControl(el.childNodes[x], disabled, checked);
                }
            }
        }
        //      Either enables or disables dropdown boxes
        function enabledControlDropDown(el, disabled, checked) {
            //alert("Hello world from enabled control");
            try {
                if (disabled) {
                    el.disabled = disabled;
                    el.setAttribute("disabled", true);
                }
                else {
                    el.disabled = disabled;
                    el.removeAttribute("disabled");
                }

                if (checked == true)
                    el.options[0].selected = true;
            }
            catch (E) {
            }
            if (el.childNodes && el.childNodes.length > 0) {
                for (var x = 0; x < el.childNodes.length; x++) {
                    enabledControl(el.childNodes[x], disabled, checked);
                }
            }
        }
        //      Either enables or disables text boxes
        function enabledControlTextField(el, disabled, checked) {
            //alert("Hello world from enabled control");
            try {
                if (disabled) {
                    el.disabled = disabled;
                    el.setAttribute("disabled", true);
                }
                else {
                    el.disabled = disabled;
                    el.removeAttribute("disabled");
                }

                if (checked == true) {
                    el.value = '';
                }
            }
            catch (E) {
            }
            if (el.childNodes && el.childNodes.length > 0) {
                for (var x = 0; x < el.childNodes.length; x++) {
                    enabledControl(el.childNodes[x], disabled, checked);
                }
            }
        }

        function DOBValidation() {
            var month = document.getElementById("<%= DropDownListMonthOfBirth.ClientID %>")
            var year = document.getElementById("<%= DropDownListYearOfBirth.ClientID %>")
            if ((month.value != 0 && year.value != 0) && (validateDate(month) != true)) {
                enabledControlDropDown(month, false, true);
                enabledControlDropDown(year, false, true);
            }
        }

        function firstExamValidation() {
            var month = document.getElementById("<%= DropDownListMonthofExam.ClientID %>")
            var year = document.getElementById("<%= DropDownListMonthofYear.ClientID %>")
            if ((month.value != 0 && year.value != 0) && (validateDate(month) != true)) {
                enabledControlDropDown(month, false, true);
                enabledControlDropDown(year, false, true);
            }
        }

        function recentExam() {
            var month = document.getElementById("<%= DropDownListOutcomeExamMonth.ClientID %>")
            var year = document.getElementById("<%= DropDownListOutcomeExamYear.ClientID %>")
            var month2 = document.getElementById("<%= DropDownListMonthofExam.ClientID %>")
            var year2 = document.getElementById("<%= DropDownListMonthofYear.ClientID %>")
            if ((month.value != 0 && year.value != 0) && (validateDate(month) != true)) {
                enabledControlDropDown(month, false, true);
                enabledControlDropDown(year, false, true);
                alert("The follow-up exam must have occured 6 months or later than the inition exam.");
            }
            else if (validateTimeFromAppointment(month2, year2, month, year) == false) {
                enabledControlDropDown(month, false, true);
                enabledControlDropDown(year, false, true);
            }
        }

        function recentOpticNerveExamOD() {
            var month = document.getElementById("<%= DropDownListOutcomeOpticNerveExamODMonth.ClientID %>")
            var year = document.getElementById("<%= DropDownListOutcomeOpticNerveExamODYear.ClientID %>")
            if ((month.value != 0 && year.value != 0) && (validateDate(month) != true)) {
                enabledControlDropDown(month, false, true);
                enabledControlDropDown(year, false, true);
            }
        }

        function recentOpticNerveExamOS() {
            var month = document.getElementById("<%= DropDownListOutcomeOpticNerveExamOSMonth.ClientID %>")
            var year = document.getElementById("<%= DropDownListOutcomeOpticNerveExamOSYear.ClientID %>")
            if ((month.value != 0 && year.value != 0) && (validateDate(month) != true)) {
                enabledControlDropDown(month, false, true);
                enabledControlDropDown(year, false, true);
            }
        }

        // Checks that the date has happened
        function validateDate(month, year) {
            var today = new Date();
            var monthVar = month - 1;
            var checkYear = today.getFullYear() - year;
            var checkMonth = today.getMonth() - monthVar;
            if (checkMonth < 0) {
                checkYear--;
            }
            if (checkYear < 0) {
                alert("Invalid date, you must pick a date that has already occurred.");
                return false;
            }
            else {
                return true;
            }
        }

        function validateTimeFromAppointment(month1, year1, month2, year2) {
            var firstExamMon = month1.value;
            var firstExamYear = year1.value;
            var secondExamMon = month2.value;
            var secondExamYear = year2.value;

            if ((firstExamMon != 0) && (firstExamYear != 0) && (secondExamMon != 0) && (secondExamYear != 0)) {
                var age = howManyMonths(firstExamMon, firstExamYear, secondExamMon, secondExamYear);
                if (age >= 6) {
                    //alert(age);
                    return true;
                }
                else {
                    //alert(age);
                    return false;
                }
            }
        }

        // Returns how many months are between the dates
        function howManyMonths(firstMon, firstYear, secMon, secYear) {
            // Input variables
            var DOBMonth = firstMon;
            var DOBYear = firstYear;
            var ExamMonth = secMon;
            var ExamYear = secYear;
            //alert("DOBMonth: " + DOBMonth + "; DOBYear: " + DOBYear + "; ExamMonth: " + ExamMonth + "; ExamYear: " + ExamYear);
            // Calculation variables
            var yearsInMonths = 0;
            var years = ExamYear - DOBYear;
            var months = ExamMonth - DOBMonth;
            // Variable to be returned
            if (years > 0) {
                yearsInMonths = years * 12;
            }
            var totalMonths = yearsInMonths + months;
            return totalMonths;
        }

        // Question 1 OD and OS toggles
        function QE1ODtoggleDD() {
            enabledControlDropDown(document.getElementById("<%= DropDownListBCVAOD.ClientID %>"), false, true);
        }

        function QE1ODtoggleRB() {
            enabledControl(document.getElementById("<%= RadioButtonListRBBCVAODNotAbletoPerform.ClientID %>"), false, true);
        }

        function QE1OStoggleDD() {
            enabledControlDropDown(document.getElementById("<%= DropDownListBCVAOS.ClientID %>"), false, true);
        }

        function QE1OStoggleRB() {
            enabledControl(document.getElementById("<%= RadioButtonListRBBCVAOSNotAbletoPerform.ClientID %>"), false, true);
        }

        // Question 2 OD and OS toggles
        function QE2ODtoggleTB() {
            enabledControlTextField(document.getElementById("<%= TextBoxIOPODmmHg.ClientID %>"), false, true);
        }

        function QE2ODtoggleRB() {
            enabledControl(document.getElementById("<%= RadioButtonListRBIOPODNotAbletoPerform.ClientID %>"), false, true);
        }

        function QE2OStoggleTB() {
            enabledControlTextField(document.getElementById("<%= TextBoxIOPOSmmHg.ClientID %>"), false, true);
        }

        function QE2OStoggleRB() {
            enabledControl(document.getElementById("<%= RadioButtonListRBIOPOSNotAbletoPerform.ClientID %>"), false, true);
        }

        // Question 3 Yes/No toggle for question 4
        function QA3isYes() {
            if (document.getElementById("<%= RadioButtonFollowUpExamScheduledYes.ClientID %>").checked != true) {
                enabledControlDropDown(document.getElementById("<%= DropDownListFollowUpExamDateAmount.ClientID %>"), true, true);
                enabledControlDropDown(document.getElementById("<%= DropDownListFollowUpExamDateType.ClientID %>"), true, true);
                document.getElementById("QA4txta").style.color = "gray";
                document.getElementById("QA4txtb").style.color = "gray";
            }
            else {
                enabledControlDropDown(document.getElementById("<%= DropDownListFollowUpExamDateAmount.ClientID %>"), false, false);
                enabledControlDropDown(document.getElementById("<%= DropDownListFollowUpExamDateType.ClientID %>"), false, false);
                document.getElementById("QA4txta").style.color = "#333";
                document.getElementById("QA4txtb").style.color = "#333";
            }
        }

        // Toggle function for outcomes Q1
        function QO1ODtoggleDD() {
            enabledControlDropDown(document.getElementById("<%= DropDownListOutcomeBCVAOD.ClientID %>"), false, true);
        }

        function QO1ODtoggleRB() {
            enabledControl(document.getElementById("<%= RadioButtonListOutcomeRBBCVAODNotAbletoPerform.ClientID %>"), false, true);
        }

        function QO1OStoggleDD() {
            enabledControlDropDown(document.getElementById("<%= DropDownListOutcomeBCVAOS.ClientID %>"), false, true);
        }

        function QO1OStoggleRB() {
            enabledControl(document.getElementById("<%= RadioButtonListOutcomeRBBCVAOSNotAbletoPerform.ClientID %>"), false, true);
        }

        // Toggle function for outcomes Q2
        function QO2ODtoggleTB() {
            enabledControlTextField(document.getElementById("<%= TextBoxOutcomeIOPODmmHg.ClientID %>"), false, true);
        }

        function QO2ODtoggleRB() {
            enabledControl(document.getElementById("<%= RadioButtonListRBOutcomeIOPODNotAbletoPerform.ClientID %>"), false, true);
        }

        function QO2OStoggleTB() {
            enabledControlTextField(document.getElementById("<%= TextBoxOutcomeIOPOSmmHg.ClientID %>"), false, true);
        }

        function QO2OStoggleRB() {
            enabledControl(document.getElementById("<%= RadioButtonListRBOutcomeIOPOSNotAbletoPerform.ClientID %>"), false, true);
        }

        window.onload = function onLoadForm() {
            QA3isYes();
        }

    </script>

    <script type="text/javascript">
        $(function () {
            $('#QE1').aToolTip({
                clickIt: true,
                tipContent: '20/800 or count fingers @ 5 ft<br />20/1000 or count fingers @ 4 ft<br />20/1600 or count fingers @ 3ft<br />20/2000 or count fingers @ 2 ft<br />20/4000 or count fingers @ 1 ft<br />20/7777 =CF<br />20/8888 =HM<br />20/9999 =LP<br />20/0000 =NLP'
            });
        });
        $(function () {
            $('#QE5').aToolTip({
                clickIt: true,
                tipContent: 'In examining the optic nerve, particular attention should be paid to the neuroretinal rim, including generalized or focal thinning, notching, and disc hemorrhage.  The presence and extent of peripapillary atrophy should also be noted, as it is a risk factor for glaucoma progression.'
            });
        });
        $(function () {
            $('#QO1').aToolTip({
                clickIt: true,
                tipContent: '20/800 or count fingers @ 5 ft<br />20/1000 or count fingers @ 4 ft<br />20/1600 or count fingers @ 3ft<br />20/2000 or count fingers @ 2 ft<br />20/4000 or count fingers @ 1 ft<br />20/7777 =CF<br />20/8888 =HM<br />20/9999 =LP<br />20/0000 =NLP'
            });
        });

    </script>

</asp:Content>