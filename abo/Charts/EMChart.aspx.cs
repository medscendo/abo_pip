﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Transactions;
using NetHealthPIMModel;

public partial class abo_EMChart : BasePage
{
    protected void Page_Load(object sender, EventArgs e)
   {
        if (!IsPostBack)
        {
            InitializePage();
        }


    }
    protected void InitializePage()
    {
        ((PIMMasterPage)Page.Master).SetTitle("Chart");

        using (NetHealthPIMEntities pim = new NetHealthPIMEntities())
        {
            int ModuleID = (int)Session[Constants.SESSION_WORKINGMODULEID];
            Module module = pim.Module.First(c => c.ModuleID == ModuleID);
            ((PIMMasterPage)Page.Master).SetHeader(module.ModuleName + " Chart Abstraction");

            ParticipantModuleCycle prev = (ParticipantModuleCycle)Session[Constants.SESSION_PREVIOUSCYCLE];
            String CycleNumber = Request.QueryString["CycleNumber"];

            int cycleID = ctx.ActiveModuleCycleID;
            if (CycleNumber == "1")
            {
                cycleID = prev.ParticipantModuleCycleID;
                LinkButtonBackToDashboard.Visible = true;
                ButtonSubmit.Visible = false;
            }

            ParticipantModuleCycle cycle = pim.ParticipantModuleCycle.First(c => c.ParticipantModuleCycleID == cycleID);
            String numberOfRecord = Request.QueryString["nr"];
            String totalRecords = Request.QueryString["t"];
            if (cycle.CycleNumber == 1)
            {
                ((PIMMasterPage)Page.Master).SetStatus(2);
                LiteralAbstractionNumber.Text = "Initial Abstraction: Chart " + numberOfRecord + " of " + totalRecords;
            }
            else
            {
                ((PIMMasterPage)Page.Master).SetStatus(3);
                LiteralAbstractionNumber.Text = "Second Abstraction: Chart " + numberOfRecord + " of " + totalRecords;
            }
            DropDownListMonthOfBirth.DataSource = CycleManager.getMonthList();
            DropDownListYearOfBirth.DataSource = CycleManager.getDOBYearList(0, 100);
          




            DataBind();
            DropDownListYearOfBirth.Items.Insert(0, new ListItem("Year", "0"));

            string recordIdentifier = Request.QueryString["ri"];
            HiddenFieldRecordIdentifier.Value = recordIdentifier;
            LiteralRecordIdentifier.Text = recordIdentifier;
            var count = pim.ChartAbstractionEyelid.Where(ps => ps.ParticipantModuleCycle.ParticipantModuleCycleID == cycleID & ps.RecordIdentifier == recordIdentifier).Count();
            if (count == 0)
            {
                using (TransactionScope transaction = new TransactionScope())
                {
                    //Module module = pim.Module.First(m => m.ModuleID == Constants.ABO_AMBLYOPIA_MODULEID);

                    NetHealthPIMModel.ChartRegistration chartRegistration = (from cr in pim.ChartRegistration
                                                           where cr.ParticipantModuleCycleID == cycleID
                                                           & cr.RecordIdentifier == recordIdentifier
                                                           & cr.ModuleID == 35
                                                           select cr).First<NetHealthPIMModel.ChartRegistration>();

                    DateTime initialVisit = Convert.ToDateTime(chartRegistration.InitialVisit);
                    DateTime DOB = Convert.ToDateTime(chartRegistration.DOB);

                    ChartAbstractionEyelid abstractRecord = new ChartAbstractionEyelid();
                    abstractRecord.ParticipantModuleCycle = cycle;
                    abstractRecord.RecordIdentifier = recordIdentifier;
                    abstractRecord.MonthOfBirth = DOB.Month;
                    abstractRecord.YearOfBirth = DOB.Year;
                    DropDownListYearOfBirth.SelectedValue = abstractRecord.YearOfBirth.ToString();
                    DropDownListMonthOfBirth.SelectedValue = abstractRecord.MonthOfBirth.ToString();
                  
                    abstractRecord.CreatedOn = DateTime.Now;
                    abstractRecord.LastUpdateDate = DateTime.Now;
                    pim.SaveChanges();

                    transaction.Complete();
                }
            }
            else
            {

                ChartAbstractionEyelid abstractRecord = (from c in pim.ChartAbstractionEyelid
                                                     where c.ParticipantModuleCycle.ParticipantModuleCycleID == cycleID & c.RecordIdentifier == recordIdentifier
                                                         select c).First<ChartAbstractionEyelid>();


                if (abstractRecord.LesionRegionUpperLid != null) CheckBoxLesionRegionUpperLid.Checked = ((bool)abstractRecord.LesionRegionUpperLid);
                if (abstractRecord.LesionRegionLowerLid != null) CheckBoxLesionRegionLowerLid.Checked = ((bool)abstractRecord.LesionRegionLowerLid);
                if (abstractRecord.LesionRegionMedialCanthus != null) CheckBoxLesionRegionMedialCanthus.Checked = ((bool)abstractRecord.LesionRegionMedialCanthus);
                if (abstractRecord.LesionRegionLateralCanthus != null) CheckBoxLesionRegionLateralCanthus.Checked = ((bool)abstractRecord.LesionRegionLateralCanthus);
                if (abstractRecord.LesionRegionEyelidMargin != null) CheckBoxLesionRegionEyelidMargin.Checked = ((bool)abstractRecord.LesionRegionEyelidMargin);
                if (abstractRecord.LesionRegionGreater != null) CheckBoxLesionRegionGreater.Checked = ((bool)abstractRecord.LesionRegionGreater);
                if (abstractRecord.LesionRegionLacrimalOutflow != null) CheckBoxLesionRegionLacrimalOutflow.Checked = ((bool)abstractRecord.LesionRegionLacrimalOutflow);
                if (abstractRecord.RepairHealing != null) CheckBoxRepairHealing.Checked = ((bool)abstractRecord.RepairHealing);
                if (abstractRecord.RepairSimple != null) CheckBoxRepairSimple.Checked = ((bool)abstractRecord.RepairSimple);
                if (abstractRecord.RepairSkinFlaps != null) CheckBoxRepairSkinFlaps.Checked = ((bool)abstractRecord.RepairSkinFlaps);
                if (abstractRecord.RepairRegionalFlaps != null) CheckBoxRepairRegionalFlaps.Checked = ((bool)abstractRecord.RepairRegionalFlaps);
                if (abstractRecord.RepairTarsoFlap != null) CheckBoxRepairTarsoFlap.Checked = ((bool)abstractRecord.RepairTarsoFlap);
                if (abstractRecord.RepairFullGraft != null) CheckBoxRepairFullGraft.Checked = ((bool)abstractRecord.RepairFullGraft);
                if (abstractRecord.RepairSplitGraft != null) CheckBoxRepairSplitGraft.Checked = ((bool)abstractRecord.RepairSplitGraft);
                if (abstractRecord.RepairReconstruct != null) CheckBoxRepairReconstruct.Checked = ((bool)abstractRecord.RepairReconstruct);
                if (abstractRecord.ActionReferral != null) CheckBoxActionReferral.Checked = ((bool)abstractRecord.ActionReferral);
                if (abstractRecord.ActionAdditionalProc != null) CheckBoxActionAdditionalProc.Checked = ((bool)abstractRecord.ActionAdditionalProc);
                if (abstractRecord.ActionCryotherapy != null) CheckBoxActionCryotherapy.Checked = ((bool)abstractRecord.ActionCryotherapy);
                if (abstractRecord.ActionFollowPatient != null) CheckBoxActionFollowPatient.Checked = ((bool)abstractRecord.ActionFollowPatient);
                if (abstractRecord.ActionRadiationTherapy != null) CheckBoxActionRadiationTherapy.Checked = ((bool)abstractRecord.ActionRadiationTherapy);
                if (abstractRecord.ActionOther != null) CheckBoxActionOther.Checked = ((bool)abstractRecord.ActionOther);
                if (abstractRecord.ComplicationHemorrhage != null) CheckBoxComplicationHemorrhage.Checked = ((bool)abstractRecord.ComplicationHemorrhage);
                if (abstractRecord.ComplicationInfection != null) CheckBoxComplicationInfection.Checked = ((bool)abstractRecord.ComplicationInfection);
                if (abstractRecord.ComplicationLidRetraction != null) CheckBoxComplicationLidRetraction.Checked = ((bool)abstractRecord.ComplicationLidRetraction);
                if (abstractRecord.ComplicationEntropion != null) CheckBoxComplicationEntropion.Checked = ((bool)abstractRecord.ComplicationEntropion);
                if (abstractRecord.ComplicationEctropion != null) CheckBoxComplicationEctropion.Checked = ((bool)abstractRecord.ComplicationEctropion);
                if (abstractRecord.ComplicationCorneal != null) CheckBoxComplicationCorneal.Checked = ((bool)abstractRecord.ComplicationCorneal);
                if (abstractRecord.ComplicationOther != null) CheckBoxComplicationOther.Checked = ((bool)abstractRecord.ComplicationOther);
                if (abstractRecord.FollowUpDermatologist != null) CheckBoxFollowUpDermatologist.Checked = ((bool)abstractRecord.FollowUpDermatologist);
                if (abstractRecord.FollowUpSurgeon != null) CheckBoxFollowUpSurgeon.Checked = ((bool)abstractRecord.FollowUpSurgeon);
                if (abstractRecord.FollowUpOncologist != null) CheckBoxFollowUpOncologist.Checked = ((bool)abstractRecord.FollowUpOncologist);
                if (abstractRecord.FollowUpOtherOphthal != null) CheckBoxFollowUpOtherOphthal.Checked = ((bool)abstractRecord.FollowUpOtherOphthal);
                if (abstractRecord.FollowUpOther != null) CheckBoxFollowUpOther.Checked = ((bool)abstractRecord.FollowUpOther);
                if (abstractRecord.FollowUpNone != null) CheckBoxFollowUpNone.Checked = ((bool)abstractRecord.FollowUpNone);
                if (abstractRecord.RepairOther != null) CheckBoxRepairOther.Checked = ((bool)abstractRecord.RepairOther);


                if (abstractRecord.RBGender != null) RadioButtonListRBGender.SelectedValue = abstractRecord.RBGender.ToString();
                if (abstractRecord.RBHistoryPriorCutMalignancy != null) RadioButtonListRBHistoryPriorCutMalignancy.SelectedValue = abstractRecord.RBHistoryPriorCutMalignancy.ToString();
                if (abstractRecord.RBHistoryExcessiveSunExposure != null) RadioButtonListRBHistoryExcessiveSunExposure.SelectedValue = abstractRecord.RBHistoryExcessiveSunExposure.ToString();
                if (abstractRecord.RBHistorySystemicMalignancy != null) RadioButtonListRBHistorySystemicMalignancy.SelectedValue = abstractRecord.RBHistorySystemicMalignancy.ToString();
                if (abstractRecord.RBHistorySmoking != null) RadioButtonListRBHistorySmoking.SelectedValue = abstractRecord.RBHistorySmoking.ToString();
                if (abstractRecord.RBHistoryRadiation != null) RadioButtonListRBHistoryRadiation.SelectedValue = abstractRecord.RBHistoryRadiation.ToString();
                if (abstractRecord.RBExamInvolvementLacrimal != null) RadioButtonListRBExamInvolvementLacrimal.SelectedValue = abstractRecord.RBExamInvolvementLacrimal.ToString();
                if (abstractRecord.RBTumorConsistent != null) RadioButtonListRBTumorConsistent.SelectedValue = abstractRecord.RBTumorConsistent.ToString();
                if (abstractRecord.FinalMarginsPositive != null) RadioButtonFinalMarginsPositive.SelectedValue = abstractRecord.FinalMarginsPositive.ToString();
                if (abstractRecord.RBFollowUpMissedVisit != null) RadioButtonListRBFollowUpMissedVisit.SelectedValue = abstractRecord.RBFollowUpMissedVisit.ToString();

                
                if (abstractRecord.RadioButtonListLesionTreated != null) RadioButtonListLesionTreated.SelectedValue = abstractRecord.RadioButtonListLesionTreated.ToString();


                if (abstractRecord.MonthOfBirth != null) DropDownListMonthOfBirth.SelectedValue = abstractRecord.MonthOfBirth.ToString();
                if (abstractRecord.YearOfBirth != null) DropDownListYearOfBirth.SelectedValue = abstractRecord.YearOfBirth.ToString();



                if (abstractRecord.ExamLesionLocation != null)
                {
                    RadioButtonExamLesionLocationYes.Checked = ((bool)abstractRecord.ExamLesionLocation);
                    RadioButtonExamLesionLocationNo.Checked = !((bool)abstractRecord.ExamLesionLocation);
                }
                if (abstractRecord.ExamLesionSize != null)
                {
                    RadioButtonExamLesionSizeYes.Checked = ((bool)abstractRecord.ExamLesionSize);
                    RadioButtonExamLesionSizeNo.Checked = !((bool)abstractRecord.ExamLesionSize);
                }
                if (abstractRecord.ExamBiopsy != null)
                {
                    RadioButtonExamBiopsyYes.Checked = ((bool)abstractRecord.ExamBiopsy);
                    RadioButtonExamBiopsyNo.Checked = !((bool)abstractRecord.ExamBiopsy);
                }
              
               
                if (abstractRecord.SatisfactoryEyelid != null)
                {
                    RadioButtonSatisfactoryEyelidYes.Checked = ((bool)abstractRecord.SatisfactoryEyelid);
                    RadioButtonSatisfactoryEyelidNo.Checked = !((bool)abstractRecord.SatisfactoryEyelid);
                }
                if (abstractRecord.PatientCounsel != null)
                {
                    RadioButtonPatientCounselYes.Checked = ((bool)abstractRecord.PatientCounsel);
                    RadioButtonPatientCounselNo.Checked = !((bool)abstractRecord.PatientCounsel);
                }
                if (abstractRecord.RecurrenceNoted != null)
                {
                    RadioButtonRecurrenceNotedYes.Checked = ((bool)abstractRecord.RecurrenceNoted);
                    RadioButtonRecurrenceNotedNo.Checked = !((bool)abstractRecord.RecurrenceNoted);
                }



                if (abstractRecord.LesionTreatmentOtherText != null) TextBoxLesionTreatmentOtherText.Text = abstractRecord.LesionTreatmentOtherText.ToString();
                if (abstractRecord.TumorConsistentOtherText != null) TextBoxTumorConsistentOtherText.Text = abstractRecord.TumorConsistentOtherText.ToString();
                if (abstractRecord.ActionOtherText != null) TextBoxActionOtherText.Text = abstractRecord.ActionOtherText.ToString();
                if (abstractRecord.ComplicationOtherText != null) TextBoxComplicationOtherText.Text = abstractRecord.ComplicationOtherText.ToString();
                if (abstractRecord.FollowUpOtherText != null) TextBoxFollowUpOtherText.Text = abstractRecord.FollowUpOtherText.ToString();
                if (abstractRecord.FollowedMonths != null) TextBoxFollowedMonths.Text = abstractRecord.FollowedMonths.ToString();

                if (abstractRecord.FollowedYears != null) TextBoxFollowedYears.Text = abstractRecord.FollowedYears.ToString();
             



            }
        }
    }

    public void savedata()
    {
        using (NetHealthPIMEntities pim = new NetHealthPIMEntities())
        {
            int ModuleID = (int)Session[Constants.SESSION_WORKINGMODULEID];
            int cycleID = ctx.ActiveModuleCycleID;
            ParticipantModuleCycle cycle = pim.ParticipantModuleCycle.First(c => c.ParticipantModuleCycleID == cycleID);
            string recordIdentifier = HiddenFieldRecordIdentifier.Value;
            using (TransactionScope transaction = new TransactionScope())
            {
                ChartAbstractionEyelid abstractRecord = (from c in pim.ChartAbstractionEyelid
                                                     where c.ParticipantModuleCycle.ParticipantModuleCycleID == cycleID & c.RecordIdentifier == recordIdentifier
                                                         select c).First<ChartAbstractionEyelid>();
           
            
                                   
                                    abstractRecord.LesionRegionUpperLid = CheckBoxLesionRegionUpperLid.Checked;
                                    abstractRecord.LesionRegionLowerLid = CheckBoxLesionRegionLowerLid.Checked;
                                    abstractRecord.LesionRegionMedialCanthus = CheckBoxLesionRegionMedialCanthus.Checked;
                                    abstractRecord.LesionRegionLateralCanthus = CheckBoxLesionRegionLateralCanthus.Checked;
                                    abstractRecord.LesionRegionEyelidMargin = CheckBoxLesionRegionEyelidMargin.Checked;
                                    abstractRecord.LesionRegionGreater = CheckBoxLesionRegionGreater.Checked;
                                    abstractRecord.LesionRegionLacrimalOutflow = CheckBoxLesionRegionLacrimalOutflow.Checked;
                                    abstractRecord.RepairHealing = CheckBoxRepairHealing.Checked;
                                    abstractRecord.RepairSimple = CheckBoxRepairSimple.Checked;
                                    abstractRecord.RepairSkinFlaps = CheckBoxRepairSkinFlaps.Checked;
                                    abstractRecord.RepairRegionalFlaps = CheckBoxRepairRegionalFlaps.Checked;
                                    abstractRecord.RepairTarsoFlap = CheckBoxRepairTarsoFlap.Checked;
                                    abstractRecord.RepairFullGraft = CheckBoxRepairFullGraft.Checked;
                                    abstractRecord.RepairSplitGraft = CheckBoxRepairSplitGraft.Checked;
                                    abstractRecord.RepairReconstruct = CheckBoxRepairReconstruct.Checked;
                                    abstractRecord.ActionReferral = CheckBoxActionReferral.Checked;
                                    abstractRecord.ActionAdditionalProc = CheckBoxActionAdditionalProc.Checked;
                                    abstractRecord.ActionCryotherapy = CheckBoxActionCryotherapy.Checked;
                                    abstractRecord.ActionFollowPatient = CheckBoxActionFollowPatient.Checked;
                                    abstractRecord.ActionRadiationTherapy = CheckBoxActionRadiationTherapy.Checked;
                                    abstractRecord.ActionOther = CheckBoxActionOther.Checked;
                                    abstractRecord.ComplicationHemorrhage = CheckBoxComplicationHemorrhage.Checked;
                                    abstractRecord.ComplicationInfection = CheckBoxComplicationInfection.Checked;
                                    abstractRecord.ComplicationLidRetraction = CheckBoxComplicationLidRetraction.Checked;
                                    abstractRecord.ComplicationEntropion = CheckBoxComplicationEntropion.Checked;
                                    abstractRecord.ComplicationEctropion = CheckBoxComplicationEctropion.Checked;
                                    abstractRecord.ComplicationCorneal = CheckBoxComplicationCorneal.Checked;
                                    abstractRecord.ComplicationOther = CheckBoxComplicationOther.Checked;
                                    abstractRecord.FollowUpDermatologist = CheckBoxFollowUpDermatologist.Checked;
                                    abstractRecord.FollowUpSurgeon = CheckBoxFollowUpSurgeon.Checked;
                                    abstractRecord.FollowUpOncologist = CheckBoxFollowUpOncologist.Checked;
                                    abstractRecord.FollowUpOtherOphthal = CheckBoxFollowUpOtherOphthal.Checked;
                                    abstractRecord.FollowUpOther = CheckBoxFollowUpOther.Checked;
                                    abstractRecord.FollowUpNone = CheckBoxFollowUpNone.Checked;
                                    abstractRecord.RepairOther = CheckBoxRepairOther.Checked;


                                    if (RadioButtonListRBGender.SelectedIndex != -1) abstractRecord.RBGender = Convert.ToInt32(RadioButtonListRBGender.SelectedValue);
                                    else abstractRecord.RBGender = 0;
                                  


                                    if (RadioButtonListRBHistoryPriorCutMalignancy.SelectedIndex != -1) abstractRecord.RBHistoryPriorCutMalignancy = Convert.ToInt32(RadioButtonListRBHistoryPriorCutMalignancy.SelectedValue);
                                    else abstractRecord.RBHistoryPriorCutMalignancy = 0;
                                    if (RadioButtonListRBHistoryExcessiveSunExposure.SelectedIndex != -1) abstractRecord.RBHistoryExcessiveSunExposure = Convert.ToInt32(RadioButtonListRBHistoryExcessiveSunExposure.SelectedValue);
                                    else abstractRecord.RBHistoryExcessiveSunExposure = 0;
                                    if (RadioButtonListRBHistorySystemicMalignancy.SelectedIndex != -1) abstractRecord.RBHistorySystemicMalignancy = Convert.ToInt32(RadioButtonListRBHistorySystemicMalignancy.SelectedValue);
                                    else abstractRecord.RBHistorySystemicMalignancy = 0;
                                    if (RadioButtonListRBHistorySmoking.SelectedIndex != -1) abstractRecord.RBHistorySmoking = Convert.ToInt32(RadioButtonListRBHistorySmoking.SelectedValue);
                                    else abstractRecord.RBHistorySmoking = 0;
                                    if (RadioButtonListRBHistoryRadiation.SelectedIndex != -1) abstractRecord.RBHistoryRadiation = Convert.ToInt32(RadioButtonListRBHistoryRadiation.SelectedValue);
                                    else abstractRecord.RBHistoryRadiation = 0;
                                    if (RadioButtonListRBExamInvolvementLacrimal.SelectedIndex != -1) abstractRecord.RBExamInvolvementLacrimal = Convert.ToInt32(RadioButtonListRBExamInvolvementLacrimal.SelectedValue);
                                    else abstractRecord.RBExamInvolvementLacrimal = 0;
                                    if (RadioButtonListRBTumorConsistent.SelectedIndex != -1) abstractRecord.RBTumorConsistent = Convert.ToInt32(RadioButtonListRBTumorConsistent.SelectedValue);
                                    else abstractRecord.RBTumorConsistent = 0;
                                    if (RadioButtonListRBFollowUpMissedVisit.SelectedIndex != -1) abstractRecord.RBFollowUpMissedVisit = Convert.ToInt32(RadioButtonListRBFollowUpMissedVisit.SelectedValue);
                                    else abstractRecord.RBFollowUpMissedVisit = 0;
                                    if (RadioButtonFinalMarginsPositive.SelectedIndex != -1) abstractRecord.FinalMarginsPositive = Convert.ToInt32(RadioButtonFinalMarginsPositive.SelectedValue);
                                    else abstractRecord.FinalMarginsPositive = 0;

                                    if (RadioButtonListLesionTreated.SelectedIndex != -1) abstractRecord.RadioButtonListLesionTreated = Convert.ToInt32(RadioButtonListLesionTreated.SelectedValue);
                                    else abstractRecord.RadioButtonListLesionTreated = 0;

                                    if (DropDownListMonthOfBirth.SelectedIndex > 0) abstractRecord.MonthOfBirth = Convert.ToInt32(DropDownListMonthOfBirth.SelectedValue);
                                    else abstractRecord.MonthOfBirth = 0;
                                    if (DropDownListYearOfBirth.SelectedIndex > 0) abstractRecord.YearOfBirth = Convert.ToInt32(DropDownListYearOfBirth.SelectedValue);
                                    else abstractRecord.YearOfBirth = 0;



                                    if (RadioButtonExamLesionLocationYes.Checked) abstractRecord.ExamLesionLocation = true;
                                    if (RadioButtonExamLesionLocationNo.Checked) abstractRecord.ExamLesionLocation = false;
                                    if (RadioButtonExamLesionSizeYes.Checked) abstractRecord.ExamLesionSize = true;
                                    if (RadioButtonExamLesionSizeNo.Checked) abstractRecord.ExamLesionSize = false;
                                    if (RadioButtonExamBiopsyYes.Checked) abstractRecord.ExamBiopsy = true;
                                    if (RadioButtonExamBiopsyNo.Checked) abstractRecord.ExamBiopsy = false;
                                  
                                 
                                    if (RadioButtonSatisfactoryEyelidYes.Checked) abstractRecord.SatisfactoryEyelid = true;
                                    if (RadioButtonSatisfactoryEyelidNo.Checked) abstractRecord.SatisfactoryEyelid = false;
                                    if (RadioButtonPatientCounselYes.Checked) abstractRecord.PatientCounsel = true;
                                    if (RadioButtonPatientCounselNo.Checked) abstractRecord.PatientCounsel = false;
                                    if (RadioButtonRecurrenceNotedYes.Checked) abstractRecord.RecurrenceNoted = true;
                                    if (RadioButtonRecurrenceNotedNo.Checked) abstractRecord.RecurrenceNoted = false;


                                    try
                                    {
                                        if (TextBoxFollowedMonths.Text != null)
                                            abstractRecord.FollowedMonths = Convert.ToInt32(TextBoxFollowedMonths.Text);
                                    }
                                    catch (Exception ex)
                                    {
                                        abstractRecord.FollowedMonths = null;
                                        TextBoxFollowedMonths.Text = "";
                                    }


                                    try
                                    {
                                        if (TextBoxFollowedYears.Text != null)
                                            abstractRecord.FollowedYears = Convert.ToInt32(TextBoxFollowedYears.Text);
                                    }
                                    catch (Exception ex)
                                    {
                                        abstractRecord.FollowedYears = null;
                                        TextBoxFollowedYears.Text = "";
                                    }








                                    try
                                    {
                                    if (TextBoxLesionTreatmentOtherText.Text != null)
                                    abstractRecord.LesionTreatmentOtherText = TextBoxLesionTreatmentOtherText.Text;
                                    }
                                    catch (Exception ex)
                                    {
                                    abstractRecord.LesionTreatmentOtherText = null;
                                    TextBoxLesionTreatmentOtherText.Text = "";
                                    }
                                    try
                                    {
                                    if (TextBoxTumorConsistentOtherText.Text != null)
                                    abstractRecord.TumorConsistentOtherText = TextBoxTumorConsistentOtherText.Text;
                                    }
                                    catch (Exception ex)
                                    {
                                    abstractRecord.TumorConsistentOtherText = null;
                                    TextBoxTumorConsistentOtherText.Text = "";
                                    }
                                    try
                                    {
                                    if (TextBoxActionOtherText.Text != null)
                                    abstractRecord.ActionOtherText = TextBoxActionOtherText.Text;
                                    }
                                    catch (Exception ex)
                                    {
                                    abstractRecord.ActionOtherText = null;
                                    TextBoxActionOtherText.Text = "";
                                    }
                                    try
                                    {
                                    if (TextBoxComplicationOtherText.Text != null)
                                    abstractRecord.ComplicationOtherText = TextBoxComplicationOtherText.Text;
                                    }
                                    catch (Exception ex)
                                    {
                                    abstractRecord.ComplicationOtherText = null;
                                    TextBoxComplicationOtherText.Text = "";
                                    }
                                    try
                                    {
                                    if (TextBoxFollowUpOtherText.Text != null)
                                    abstractRecord.FollowUpOtherText = TextBoxFollowUpOtherText.Text;
                                    }
                                    catch (Exception ex)
                                    {
                                    abstractRecord.FollowUpOtherText = null;
                                    TextBoxFollowUpOtherText.Text = "";
                                    }
                                    abstractRecord.LastUpdateDate = DateTime.Now;
                                    bool ChartCompleted = isComplete();
                                    abstractRecord.Complete = ChartCompleted;
                                    var chartRegistration = (from cr in pim.ChartRegistration
                                                             where cr.ParticipantModuleCycleID == cycle.ParticipantModuleCycleID
                                                              & cr.ModuleID == ModuleID
                                                              & cr.RecordIdentifier == recordIdentifier
                                                             select cr).First<NetHealthPIMModel.ChartRegistration>();
                                    chartRegistration.AbstractCompleted = ChartCompleted;

                                    if ((DropDownListYearOfBirth.SelectedIndex > 0) && (DropDownListYearOfBirth.SelectedIndex > 0))
                                    {
                                        chartRegistration.DOB = abstractRecord.MonthOfBirth + "/" + abstractRecord.YearOfBirth;
                                    }
                                   
                                    pim.SaveChanges();

                                    transaction.Complete();




            }

            CycleManager.UpdateParticipantCompletedModules(cycleID, ModuleID);
        }


    }


    protected void ButtonSubmit_Click(object sender, EventArgs e)
    {
        savedata();
        isComplete();
        if (!isComplete())
        {
            isComplete();
            string script = " var answer = confirm('Chart data is not complete.\\nClick OK to save entries and exit chart.\\nClick CANCEL to save entries and review the chart. '); if (answer==true) window.location = '../PatientChartRegistration.aspx';";
            ScriptManager.RegisterStartupScript(this, GetType(),
                          "ServerControlScript", script, true);
        }
        else
        {

            Response.Redirect("../PatientChartRegistration.aspx");
        }
    }



    protected bool isComplete()
    {

        bool ChartCompleted = true;

        LabelLesion.Visible = false;
        LabelRBGender.Visible = false;

        LabelRBHistoryPriorCutMalignancy.Visible = false;

        LabelRBHistoryExcessiveSunExposure.Visible = false;

        LabelRBHistorySystemicMalignancy.Visible = false;

        LabelRBHistorySmoking.Visible = false;

        LabelRBHistoryRadiation.Visible = false;

        LabelRBExamInvolvementLacrimal.Visible = false;

        LabelRBTumorConsistent.Visible = false;

        LabelRBFollowUpMissedVisit.Visible = false;




        LabelMonthOfBirth.Visible = false;

        LabelYearOfBirth.Visible = false;




        LabelExamLesionLocation.Visible = false;

        LabelExamLesionSize.Visible = false;

        LabelExamBiopsy.Visible = false;

        LabelFinalMarginsPositive.Visible = false;

        LabelFollowUpPerformed.Visible = false;

        LabelSatisfactoryEyelid.Visible = false;

        LabelPatientCounsel.Visible = false;

        LabelRecurrenceNoted.Visible = false;







        if (RadioButtonListLesionTreated.SelectedIndex == -1)
        {
            LabelLesion.Visible = true;
            ChartCompleted = false;
        }


        if (RadioButtonListRBGender.SelectedIndex == -1)
        {
            LabelRBGender.Visible = true;
            ChartCompleted = false;
        }
        if (RadioButtonListRBHistoryPriorCutMalignancy.SelectedIndex == -1)
        {
            LabelRBHistoryPriorCutMalignancy.Visible = true;
            ChartCompleted = false;
        }
        if (RadioButtonListRBHistoryExcessiveSunExposure.SelectedIndex == -1)
        {
            LabelRBHistoryExcessiveSunExposure.Visible = true;
            ChartCompleted = false;
        }
        if (RadioButtonListRBHistorySystemicMalignancy.SelectedIndex == -1)
        {
            LabelRBHistorySystemicMalignancy.Visible = true;
            ChartCompleted = false;
        }
        if (RadioButtonListRBHistorySmoking.SelectedIndex == -1)
        {
            LabelRBHistorySmoking.Visible = true;
            ChartCompleted = false;
        }
        if (RadioButtonListRBHistoryRadiation.SelectedIndex == -1)
        {
            LabelRBHistoryRadiation.Visible = true;
            ChartCompleted = false;
        }
        if (RadioButtonListRBExamInvolvementLacrimal.SelectedIndex == -1)
        {
            LabelRBExamInvolvementLacrimal.Visible = true;
            ChartCompleted = false;
        }
        if (RadioButtonListRBTumorConsistent.SelectedIndex == -1)
        {
            LabelRBTumorConsistent.Visible = true;
            ChartCompleted = false;
        }
        if (RadioButtonListRBFollowUpMissedVisit.SelectedIndex == -1)
        {
            LabelRBFollowUpMissedVisit.Visible = true;
            ChartCompleted = false;
        }



        if (DropDownListMonthOfBirth.SelectedIndex == 0)
        {
            LabelMonthOfBirth.Visible = true;
            ChartCompleted = false;
        }
        if (DropDownListYearOfBirth.SelectedIndex == 0)
        {
            LabelYearOfBirth.Visible = true;
            ChartCompleted = false;
        }



        if (RadioButtonExamLesionLocationNo.Checked == false && RadioButtonExamLesionLocationYes.Checked == false)
        {
            LabelExamLesionLocation.Visible = true;
            ChartCompleted = false;
        }
        if (RadioButtonExamLesionSizeNo.Checked == false && RadioButtonExamLesionSizeYes.Checked == false)
        {
            LabelExamLesionSize.Visible = true;
            ChartCompleted = false;
        }
        if (RadioButtonExamBiopsyNo.Checked == false && RadioButtonExamBiopsyYes.Checked == false)
        {
            LabelExamBiopsy.Visible = true;
            ChartCompleted = false;
        }
     
  
        if (RadioButtonSatisfactoryEyelidNo.Checked == false && RadioButtonSatisfactoryEyelidYes.Checked == false)
        {
            LabelSatisfactoryEyelid.Visible = true;
            ChartCompleted = false;
        }
        if (RadioButtonPatientCounselNo.Checked == false && RadioButtonPatientCounselYes.Checked == false)
        {
            LabelPatientCounsel.Visible = true;
            ChartCompleted = false;
        }
        if (RadioButtonRecurrenceNotedNo.Checked == false && RadioButtonRecurrenceNotedYes.Checked == false)
        {
            LabelRecurrenceNoted.Visible = true;
            ChartCompleted = false;
        }

      





        return ChartCompleted;
    }
            
            
            
            
            
            
            
 }

