﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Transactions;
using NetHealthPIMModel;

public partial class abo_PGRBChart : BasePage
{
    protected void Page_Load(object sender, EventArgs e)
    {
        if (!IsPostBack)
        {
            InitializePage();
        }


    }
    protected void InitializePage()
    {
        ((PIMMasterPage)Page.Master).SetTitle("Chart");

        using (NetHealthPIMEntities pim = new NetHealthPIMEntities())
        {
            int ModuleID = (int)Session[Constants.SESSION_WORKINGMODULEID];
            Module module = pim.Module.First(c => c.ModuleID == ModuleID);
            ((PIMMasterPage)Page.Master).SetHeader(module.ModuleName + " Chart Abstraction");

            ParticipantModuleCycle prev = (ParticipantModuleCycle)Session[Constants.SESSION_PREVIOUSCYCLE];
            String CycleNumber = Request.QueryString["CycleNumber"];

            int cycleID = ctx.ActiveModuleCycleID;
            if (CycleNumber == "1")
            {
                cycleID = prev.ParticipantModuleCycleID;
                LinkButtonBackToDashboard.Visible = true;
                ButtonSubmit.Visible = false;
            }

            ParticipantModuleCycle cycle = pim.ParticipantModuleCycle.First(c => c.ParticipantModuleCycleID == cycleID);
            String numberOfRecord = Request.QueryString["nr"];
            String totalRecords = Request.QueryString["t"];
            if (cycle.CycleNumber == 1)
            {
                ((PIMMasterPage)Page.Master).SetStatus(2);
                LiteralAbstractionNumber.Text = "Initial Abstraction: Chart " + numberOfRecord + " of " + totalRecords;
            }
            else
            {
                ((PIMMasterPage)Page.Master).SetStatus(3);
                LiteralAbstractionNumber.Text = "Second Abstraction: Chart " + numberOfRecord + " of " + totalRecords;
            }
            string recordIdentifier = Request.QueryString["ri"];
            HiddenFieldRecordIdentifier.Value = recordIdentifier;
            LiteralRecordIdentifier.Text = recordIdentifier;
            var count = pim.ChartAbstractionPathologyBiopsy.Where(ps => ps.ParticipantModuleCycle.ParticipantModuleCycleID == cycleID & ps.RecordIdentifier == recordIdentifier).Count();
            if (count == 0)
            {
                using (TransactionScope transaction = new TransactionScope())
                {
                    //Module module = pim.Module.First(m => m.ModuleID == Constants.ABO_AMBLYOPIA_MODULEID);

                    NetHealthPIMModel.ChartRegistration chartRegistration = (from cr in pim.ChartRegistration
                                                           where cr.ParticipantModuleCycleID == cycleID
                                                           & cr.RecordIdentifier == recordIdentifier
                                                           & cr.ModuleID == 66
                                                           select cr).First<NetHealthPIMModel.ChartRegistration>();

                    DateTime initialVisit = Convert.ToDateTime(chartRegistration.InitialVisit);
                    DateTime DOB = Convert.ToDateTime(chartRegistration.DOB);
                    ChartAbstractionPathologyBiopsy abstractRecord = new ChartAbstractionPathologyBiopsy();
                    abstractRecord.ParticipantModuleCycle = cycle;
                    abstractRecord.RecordIdentifier = recordIdentifier;
                    abstractRecord.CreatedOn = DateTime.Now;
                    abstractRecord.LastUpdateDate = DateTime.Now;
                    pim.SaveChanges();
                    transaction.Complete();
                }
            }
            else
            {
                ChartAbstractionPathologyBiopsy abstractRecord = (from c in pim.ChartAbstractionPathologyBiopsy
                                                                  where c.ParticipantModuleCycle.ParticipantModuleCycleID == cycleID & c.RecordIdentifier == recordIdentifier
                                                              select c).First<ChartAbstractionPathologyBiopsy>();

                if (abstractRecord.BiopsySpecimenMeasured != null)
                {
                    RadioButtonBiopsySpecimenMeasuredYes.Checked = ((bool)abstractRecord.BiopsySpecimenMeasured);
                    RadioButtonBiopsySpecimenMeasuredNo.Checked = !((bool)abstractRecord.BiopsySpecimenMeasured);
                }
                if (abstractRecord.FormDocQualityDailyBasis != null)
                {
                    RadioFormDocQualityDailyBasisYes.Checked = ((bool)abstractRecord.FormDocQualityDailyBasis);
                    RadioFormDocQualityDailyBasisNo.Checked = !((bool)abstractRecord.FormDocQualityDailyBasis);
                }

                if (abstractRecord.ReportTurnaroundDays != null) TextBoxReportTurnaroundDays.Text = abstractRecord.ReportTurnaroundDays.ToString();

            }
        }
    }

    public void savedata()
    {
        using (NetHealthPIMEntities pim = new NetHealthPIMEntities())
        {
            int ModuleID = (int)Session[Constants.SESSION_WORKINGMODULEID];
            int cycleID = ctx.ActiveModuleCycleID;
            ParticipantModuleCycle cycle = pim.ParticipantModuleCycle.First(c => c.ParticipantModuleCycleID == cycleID);
            string recordIdentifier = HiddenFieldRecordIdentifier.Value;
            using (TransactionScope transaction = new TransactionScope())
            {
                ChartAbstractionPathologyBiopsy abstractRecord = (from c in pim.ChartAbstractionPathologyBiopsy
                                                                  where c.ParticipantModuleCycle.ParticipantModuleCycleID == cycleID & c.RecordIdentifier == recordIdentifier
                                                                  select c).First<ChartAbstractionPathologyBiopsy>();







                if (RadioButtonBiopsySpecimenMeasuredYes.Checked) abstractRecord.BiopsySpecimenMeasured = true;
                if (RadioButtonBiopsySpecimenMeasuredNo.Checked) abstractRecord.BiopsySpecimenMeasured = false;


                if (RadioFormDocQualityDailyBasisYes.Checked) abstractRecord.FormDocQualityDailyBasis = true;
                if (RadioFormDocQualityDailyBasisNo.Checked) abstractRecord.FormDocQualityDailyBasis = false;

                try
                {
                    if (TextBoxReportTurnaroundDays.Text != null)
                        abstractRecord.ReportTurnaroundDays = Convert.ToInt32(TextBoxReportTurnaroundDays.Text);
                }
                catch (Exception ex)
                {
                    abstractRecord.ReportTurnaroundDays = null;
                    TextBoxReportTurnaroundDays.Text = "";
                }

                abstractRecord.LastUpdateDate = DateTime.Now;
                bool ChartCompleted = isComplete();
                abstractRecord.Complete = ChartCompleted;
                var chartRegistration = (from cr in pim.ChartRegistration
                                         where cr.ParticipantModuleCycleID == cycle.ParticipantModuleCycleID
                                          & cr.ModuleID == ModuleID
                                          & cr.RecordIdentifier == recordIdentifier
                                         select cr).First<NetHealthPIMModel.ChartRegistration>();
                chartRegistration.AbstractCompleted = ChartCompleted;
                pim.SaveChanges();
                transaction.Complete();




            }

            CycleManager.UpdateParticipantCompletedModules(cycleID, ModuleID);
        }


    }

    protected void ButtonSubmit_Click(object sender, EventArgs e)
    {
        savedata();
        isComplete();
        if (!isComplete())
        {
            isComplete();
            string script = " var answer = confirm('Chart data is not complete.\\nClick OK to save entries and exit chart.\\nClick CANCEL to save entries and review the chart. '); if (answer==true) window.location = '../PatientChartRegistration.aspx';";
            ScriptManager.RegisterStartupScript(this, GetType(),
                          "ServerControlScript", script, true);
        }
        else
        {

            Response.Redirect("../PatientChartRegistration.aspx");
        }
    }


    protected bool isComplete()
    {

        bool ChartCompleted = true;


        LabelReportTurnaroundDays.Visible = false;


        LabelBiopsySpecimenMeasured.Visible = false;

        LabelCapsGlobeOpened.Visible = false;




        if (RadioButtonBiopsySpecimenMeasuredYes.Checked == false && RadioButtonBiopsySpecimenMeasuredNo.Checked == false)
        {
            LabelBiopsySpecimenMeasured.Visible = true;
            ChartCompleted = false;
        }
        if (RadioFormDocQualityDailyBasisYes.Checked == false && RadioFormDocQualityDailyBasisNo.Checked == false)
        {
            LabelCapsGlobeOpened.Visible = true;
            ChartCompleted = false;
        }
        if (TextBoxReportTurnaroundDays.Text == "")
        {
            LabelReportTurnaroundDays.Visible = true;
            ChartCompleted = false;
        }


        return ChartCompleted;

    }
}

