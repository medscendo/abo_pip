﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Transactions;
using NetHealthPIMModel;

public partial class abo_IONChart : BasePage
{
    protected void Page_Load(object sender, EventArgs e)
    {
        if (!IsPostBack)
        {
            InitializePage();
        }

    }
    protected void InitializePage()
    {
        ((PIMMasterPage)Page.Master).SetTitle("Chart");

        using (NetHealthPIMEntities pim = new NetHealthPIMEntities())
        {
            int ModuleID = (int)Session[Constants.SESSION_WORKINGMODULEID];
            Module module = pim.Module.First(c => c.ModuleID == ModuleID);
            ((PIMMasterPage)Page.Master).SetHeader(module.ModuleName + " Chart Abstraction");

            ParticipantModuleCycle prev = (ParticipantModuleCycle)Session[Constants.SESSION_PREVIOUSCYCLE];
            String CycleNumber = Request.QueryString["CycleNumber"];

            int cycleID = ctx.ActiveModuleCycleID;
            if (CycleNumber == "1")
            {
                cycleID = prev.ParticipantModuleCycleID;
                LinkButtonBackToDashboard.Visible = true;
                ButtonSubmit.Visible = false;
            }

            ParticipantModuleCycle cycle = pim.ParticipantModuleCycle.First(c => c.ParticipantModuleCycleID == cycleID);
            String numberOfRecord = Request.QueryString["nr"];
            String totalRecords = Request.QueryString["t"];
            if (cycle.CycleNumber == 1)
            {
                ((PIMMasterPage)Page.Master).SetStatus(2);
                LiteralAbstractionNumber.Text = "Initial Abstraction: Chart " + numberOfRecord + " of " + totalRecords;
            }
            else
            {
                ((PIMMasterPage)Page.Master).SetStatus(3);
                LiteralAbstractionNumber.Text = "Second Abstraction: Chart " + numberOfRecord + " of " + totalRecords;
            }
            DropDownListMonthOfBirth.DataSource = CycleManager.getMonthList();
            DropDownListYearOfBirth.DataSource = CycleManager.getDOBYearList(0, 100);
            DropDownListMonthOfFirstExam.DataSource = CycleManager.getMonthList();
            DropDownListYearOfFirstExam.DataSource = CycleManager.getDOBYearList(0, 100);
            DropDownExamBestCorrectedVisualOS.DataSource = CycleManager.getExamValues();
            DropDownExamBestCorrectedVisualOD.DataSource = CycleManager.getExamValues();
            DropdownListOutcomeBestCorrectedVisualOD.DataSource = CycleManager.getExamValues();
            DropdownListOutcomeBestCorrectedVisualOS.DataSource = CycleManager.getExamValues();
            DataBind();
            DropDownListYearOfBirth.Items.Insert(0, new ListItem("Year", "0"));
            DropDownListYearOfFirstExam.Items.Insert(0, new ListItem("Year", "0"));
            string recordIdentifier = Request.QueryString["ri"];
            HiddenFieldRecordIdentifier.Value = recordIdentifier;
            LiteralRecordIdentifier.Text = recordIdentifier;
            CycleManager.UpdateChartDates(cycleID, recordIdentifier, Constants.ABO_ION_MODULEID, false);
            var count = pim.ChartAbstractionInitialOpticNeuritis.Where(ps => ps.ParticipantModuleCycle.ParticipantModuleCycleID == cycleID & ps.RecordIdentifier == recordIdentifier).Count();
            if (count == 0)
            {
                using (TransactionScope transaction = new TransactionScope())
                {
                    //Module module = pim.Module.First(m => m.ModuleID == Constants.ABO_AMBLYOPIA_MODULEID);

                    NetHealthPIMModel.ChartRegistration chartRegistration = (from cr in pim.ChartRegistration
                                                           where cr.ParticipantModuleCycleID == cycleID
                                                           & cr.RecordIdentifier == recordIdentifier
                                                           & cr.ModuleID == Constants.ABO_ION_MODULEID //// NEEDS TO BE CHANGED
                                                           select cr).First<NetHealthPIMModel.ChartRegistration>();

                    DateTime initialVisit = Convert.ToDateTime(chartRegistration.InitialVisit);
                    DateTime DOB = Convert.ToDateTime(chartRegistration.DOB);

                    ChartAbstractionInitialOpticNeuritis abstractRecord = new ChartAbstractionInitialOpticNeuritis();
                    abstractRecord.ParticipantModuleCycle = cycle;
                    abstractRecord.RecordIdentifier = recordIdentifier;
                    abstractRecord.MonthOfFirstExam = initialVisit.Month;
                    abstractRecord.YearOfFirstExam = initialVisit.Year;
                    abstractRecord.MonthOfBirth = DOB.Month;
                    abstractRecord.YearOfBirth = DOB.Year;
                    DropDownListYearOfBirth.SelectedValue = abstractRecord.YearOfBirth.ToString();
                    DropDownListMonthOfBirth.SelectedValue = abstractRecord.MonthOfBirth.ToString();
                    DropDownListMonthOfFirstExam.SelectedValue = abstractRecord.MonthOfFirstExam.ToString();
                    DropDownListYearOfFirstExam.SelectedValue = abstractRecord.YearOfFirstExam.ToString();
                    abstractRecord.CreatedOn = DateTime.Now;
                    abstractRecord.LastUpdateDate = DateTime.Now;
                    pim.SaveChanges();

                    transaction.Complete();
                }
            }
            else
            {
                ChartAbstractionInitialOpticNeuritis abstractRecord = (from c in pim.ChartAbstractionInitialOpticNeuritis
                                                                       where c.ParticipantModuleCycle.ParticipantModuleCycleID == cycleID & c.RecordIdentifier == recordIdentifier
                                                                       select c).First<ChartAbstractionInitialOpticNeuritis>();
                if (abstractRecord.YearOfBirth != null) DropDownListYearOfBirth.SelectedValue = abstractRecord.YearOfBirth.ToString();
                if (abstractRecord.MonthOfBirth != null) DropDownListMonthOfBirth.SelectedValue = abstractRecord.MonthOfBirth.ToString();
                if (abstractRecord.MonthOfFirstExam != null) DropDownListMonthOfFirstExam.SelectedValue = abstractRecord.MonthOfFirstExam.ToString();
                if (abstractRecord.YearOfFirstExam!= null) DropDownListYearOfFirstExam.SelectedValue = abstractRecord.YearOfFirstExam.ToString();
                if (abstractRecord.RBExamAbnormalityColorVision != null) RadioButtonListRBExamAbnormalityColorVision.SelectedValue = abstractRecord.RBExamAbnormalityColorVision.ToString();
                if (abstractRecord.RBExamBaselinePerimetryDefect != null) RadioButtonListRBExamBaselinePerimetryDefect.SelectedValue = abstractRecord.RBExamBaselinePerimetryDefect.ToString();
                if (abstractRecord.RBExamOcularMotility != null) RadioButtonListRBExamOcularMotility.SelectedValue = abstractRecord.RBExamOcularMotility.ToString();
                if (abstractRecord.RBExamOcularMotilityType != null) RadioButtonListRBExamOcularMotilityType.SelectedValue = abstractRecord.RBExamOcularMotilityType.ToString();
                if (abstractRecord.RBExamOpticDisc != null) RadioButtonListRBExamOpticDisc.SelectedValue = abstractRecord.RBExamOpticDisc.ToString();
                if (abstractRecord.RBExamOpticDiscAppearance != null) RadioButtonListRBExamOpticDiscAppearance.SelectedValue = abstractRecord.RBExamOpticDiscAppearance.ToString();
                if (abstractRecord.RBExamRelativeAfferentPupillaryDefect != null) RadioButtonListRBExamRelativeAfferentPupillaryDefect.SelectedValue = abstractRecord.RBExamRelativeAfferentPupillaryDefect.ToString();
                if (abstractRecord.RBExamUveitis != null) RadioButtonListRBExamUveitis.SelectedValue = abstractRecord.RBExamUveitis.ToString();
                if (abstractRecord.RBGenderTypeID != null) RadioButtonListRBGenderTypeID.SelectedValue = abstractRecord.RBGenderTypeID.ToString();
                if (abstractRecord.RBHistoryAutoimmuneDisease != null) RadioButtonListRBHistoryAutoimmuneDisease.SelectedValue = abstractRecord.RBHistoryAutoimmuneDisease.ToString();
                if (abstractRecord.RBHistoryPainEyeMovement != null) RadioButtonListRBHistoryPainEyeMovement.SelectedValue = abstractRecord.RBHistoryPainEyeMovement.ToString();
                if (abstractRecord.RBManageMRI != null) RadioButtonListRBManageMRI.SelectedValue = abstractRecord.RBManageMRI.ToString();
                if (abstractRecord.RBManageRelationship != null) RadioButtonListRBManageRelationship.SelectedValue = abstractRecord.RBManageRelationship.ToString();
                if (abstractRecord.RBManageSystemicCorticosteroid != null) RadioButtonListRBManageSystemicCorticosteroid.SelectedValue = abstractRecord.RBManageSystemicCorticosteroid.ToString();
                
                if (abstractRecord.RBManageSystemicCorticosteroidSE != null) RadioButtonListRBManageSystemicCorticosteroidSE.SelectedValue = abstractRecord.RBManageSystemicCorticosteroidSE.ToString();
                if (abstractRecord.RBManageWhiteMatterLesion != null) RadioButtonListRBManageWhiteMatterLesion.SelectedValue = abstractRecord.RBManageWhiteMatterLesion.ToString();
                if (abstractRecord.RBOutcomeFinalEtiology != null) RadioButtonListRBOutcomeFinalEtiology.SelectedValue = abstractRecord.RBOutcomeFinalEtiology.ToString();
                if (abstractRecord.RBOutcomeFollowUpBCVARecorded != null) RadioButtonListRBOutcomeFollowUpBCVARecorded.SelectedValue = abstractRecord.RBOutcomeFollowUpBCVARecorded.ToString();
                if (abstractRecord.RBOutcomePerimetryDefect != null) RadioButtonListRBOutcomePerimetryDefect.SelectedValue = abstractRecord.RBOutcomePerimetryDefect.ToString();
                if (abstractRecord.RBOutcomeWhiteMatterLesionReferral != null) RadioButtonListRBOutcomeWhiteMatterLesionReferral.SelectedValue = abstractRecord.RBOutcomeWhiteMatterLesionReferral.ToString();
                if (abstractRecord.RBSymptomsGeneralNeurologic != null) RadioButtonListRBSymptomsGeneralNeurologic.SelectedValue = abstractRecord.RBSymptomsGeneralNeurologic.ToString();
                if (abstractRecord.RBCorticosteroidTherapy != null) RadioButtonListCorticosteroidTherapy.SelectedValue = abstractRecord.RBCorticosteroidTherapy.ToString();
                if (abstractRecord.OutcomePerimetry != null) RadioButtonListOutcomePerimetry.SelectedValue = abstractRecord.OutcomePerimetry.ToString();
                if (abstractRecord.RadioButtonListRBHistorySystemic != null) RadioButtonListRBHistorySystemic.SelectedValue = abstractRecord.RadioButtonListRBHistorySystemic.ToString();
                if (abstractRecord.SystemicCSText != null) TextBoxSystemicCSText.Text = abstractRecord.SystemicCSText;
                
                
                if (abstractRecord.ExamBaselinePerimetry != null)
                {
                    RadioButtonExamBaselinePerimetryYes.Checked = ((bool)abstractRecord.ExamBaselinePerimetry);
                    RadioButtonExamBaselinePerimetryNo.Checked = !((bool)abstractRecord.ExamBaselinePerimetry);
                }

              if (abstractRecord.CheckBoxHistoryAISystemiclupus != null) CheckBoxHistoryAISystemiclupus.Checked = ((bool)abstractRecord.CheckBoxHistoryAISystemiclupus);
              if (abstractRecord.CheckBoxHistoryAISarcoidosis != null) CheckBoxHistoryAISarcoidosis.Checked = ((bool)abstractRecord.CheckBoxHistoryAISarcoidosis);
              if (abstractRecord.CheckBoxHistoryAIOther != null) CheckBoxHistoryAIOther.Checked = ((bool)abstractRecord.CheckBoxHistoryAIOther);
              
              if (abstractRecord.HisotryAIOther != null) CheckBoxHistoryAIOther.Checked = ((bool)abstractRecord.HisotryAIOther);
              if (abstractRecord.HistoryAIBartonella != null) CheckBoxHistorySIBartonella.Checked = ((bool)abstractRecord.HistoryAIBartonella);
              if (abstractRecord.CheckBoxDropdownListOutcomeBestCorrectedVisualOSNA != null) CheckBoxDropdownListOutcomeBestCorrectedVisualOSNA.Checked = ((bool)abstractRecord.CheckBoxDropdownListOutcomeBestCorrectedVisualOSNA);
              if (abstractRecord.CheckBoxDropdownListOutcomeBestCorrectedVisualODNA != null) CheckBoxDropdownListOutcomeBestCorrectedVisualODNA.Checked = ((bool)abstractRecord.CheckBoxDropdownListOutcomeBestCorrectedVisualODNA);

              if (abstractRecord.HistoryAISyphillis != null) CheckBoxHistorySISyphillis.Checked = ((bool)abstractRecord.HistoryAISyphillis);
              if (abstractRecord.HistoryAIViral != null) CheckBoxHistorySIViral.Checked = ((bool)abstractRecord.HistoryAIViral);
              if (abstractRecord.HistoryVisualComplaintBlurredVision != null) CheckBoxHistoryVisualComplaintBlurredVision.Checked = ((bool)abstractRecord.HistoryVisualComplaintBlurredVision);
              if (abstractRecord.HistoryVisualComplaintColorVision != null) CheckBoxHistoryVisualComplaintColorVision.Checked = ((bool)abstractRecord.HistoryVisualComplaintColorVision);
              if (abstractRecord.HistoryVisualComplaintDimming != null) CheckBoxHistoryVisualComplaintDimming.Checked = ((bool)abstractRecord.HistoryVisualComplaintDimming);
              if (abstractRecord.HistoryVisualComplaintNotRecorded != null) CheckBoxHistoryVisualComplaintNotRecorded.Checked = ((bool)abstractRecord.HistoryVisualComplaintNotRecorded);
              if (abstractRecord.HistoryVisualComplaintOther != null) CheckBoxHistoryVisualComplaintOther.Checked = ((bool)abstractRecord.HistoryVisualComplaintOther);
              if (abstractRecord.HistoryVisualComplaintVisualFieldDefect != null) CheckBoxHistoryVisualComplaintVisualFieldDefect.Checked = ((bool)abstractRecord.HistoryVisualComplaintVisualFieldDefect);
                if (abstractRecord.OutcomeFollowUpExam != null)
                {
                    RadioButtonOutcomeFollowUpExamYes.Checked = ((bool)abstractRecord.OutcomeFollowUpExam);
                    RadioButtonOutcomeFollowUpExamNo.Checked = !((bool)abstractRecord.OutcomeFollowUpExam);
                }


                if (abstractRecord.CheckBoxHistorySIOther != null) CheckBoxHistorySIOther.Checked = ((bool)abstractRecord.CheckBoxHistorySIOther);
                if (abstractRecord.SymptomsAtaxia != null) CheckBoxSymptomsAtaxia.Checked = ((bool)abstractRecord.SymptomsAtaxia);
                if (abstractRecord.SymptomsDiplopia != null) CheckBoxSymptomsDiplopia.Checked = ((bool)abstractRecord.SymptomsDiplopia);
                if (abstractRecord.SymptomsOther != null) CheckBoxSymptomsOther.Checked = ((bool)abstractRecord.SymptomsOther);
                if (abstractRecord.SymptomsParesthesia != null) CheckBoxSymptomsParesthesia.Checked = ((bool)abstractRecord.SymptomsParesthesia);
                if (abstractRecord.SymptomsWeakness != null) CheckBoxSymptomsWeakness.Checked = ((bool)abstractRecord.SymptomsWeakness);

                if (abstractRecord.ExamBestCorrectedVisualOD != null) DropDownExamBestCorrectedVisualOS.SelectedValue = abstractRecord.ExamBestCorrectedVisualOD.ToString();
                if (abstractRecord.ExamBestCorrectedVisualOS != null) DropDownExamBestCorrectedVisualOD.SelectedValue = abstractRecord.ExamBestCorrectedVisualOS.ToString();

                if (abstractRecord.ExamMeanDefectDB != null) TextBoxExamMeanDefectDB.Text = abstractRecord.ExamMeanDefectDB.ToString();
                if (abstractRecord.OutcomeBestCorrectedVisualOD != null) DropdownListOutcomeBestCorrectedVisualOD.SelectedValue = abstractRecord.OutcomeBestCorrectedVisualOD.ToString();
                if (abstractRecord.OutcomeBestCorrectedVisualOS != null) DropdownListOutcomeBestCorrectedVisualOS.SelectedValue = abstractRecord.OutcomeBestCorrectedVisualOS.ToString();
                if (abstractRecord.OutcomeMeanDefectDB != null) TextBoxOutcomeMeanDefectDB.Text = abstractRecord.OutcomeMeanDefectDB.ToString();


            }

        }
    }
    public void savedata()
    {
        using (NetHealthPIMEntities pim = new NetHealthPIMEntities())
        {
            int ModuleID = (int)Session[Constants.SESSION_WORKINGMODULEID];
            int cycleID = ctx.ActiveModuleCycleID;
            ParticipantModuleCycle cycle = pim.ParticipantModuleCycle.First(c => c.ParticipantModuleCycleID == cycleID);
            string recordIdentifier = HiddenFieldRecordIdentifier.Value;
            using (TransactionScope transaction = new TransactionScope())
            {
                ChartAbstractionInitialOpticNeuritis abstractRecord = (from c in pim.ChartAbstractionInitialOpticNeuritis
                                                                  where c.ParticipantModuleCycle.ParticipantModuleCycleID == cycleID & c.RecordIdentifier == recordIdentifier
                                                                       select c).First<ChartAbstractionInitialOpticNeuritis>();

                if (DropDownListMonthOfBirth.SelectedIndex > 0) abstractRecord.MonthOfBirth = Convert.ToInt32(DropDownListMonthOfBirth.SelectedValue);
                else abstractRecord.MonthOfBirth = 0;
                if (DropDownListYearOfBirth.SelectedIndex > 0) abstractRecord.YearOfBirth = Convert.ToInt32(DropDownListYearOfBirth.SelectedValue);
                else abstractRecord.YearOfBirth = 0;
                if (DropDownListMonthOfFirstExam.SelectedIndex > 0) abstractRecord.MonthOfFirstExam = Convert.ToInt32(DropDownListMonthOfFirstExam.SelectedValue);
                else abstractRecord.MonthOfFirstExam = 0;
                if (DropDownListYearOfFirstExam.SelectedIndex > 0) abstractRecord.YearOfFirstExam = Convert.ToInt32(DropDownListYearOfFirstExam.SelectedValue);
                else abstractRecord.YearOfFirstExam = 0;
                if (DropDownExamBestCorrectedVisualOS.SelectedIndex > 0) abstractRecord.ExamBestCorrectedVisualOS = Convert.ToInt32(DropDownExamBestCorrectedVisualOS.SelectedValue);
                else abstractRecord.ExamBestCorrectedVisualOS = 0;
                if (DropDownExamBestCorrectedVisualOD.SelectedIndex > 0) abstractRecord.ExamBestCorrectedVisualOD = Convert.ToInt32(DropDownExamBestCorrectedVisualOD.SelectedValue);
                else abstractRecord.ExamBestCorrectedVisualOD = 0;
                if (DropdownListOutcomeBestCorrectedVisualOS.SelectedIndex > 0) abstractRecord.OutcomeBestCorrectedVisualOS = Convert.ToInt32(DropdownListOutcomeBestCorrectedVisualOS.SelectedValue);
                else abstractRecord.OutcomeBestCorrectedVisualOS = 0;
                if (DropdownListOutcomeBestCorrectedVisualOD.SelectedIndex > 0) abstractRecord.OutcomeBestCorrectedVisualOD = Convert.ToInt32(DropdownListOutcomeBestCorrectedVisualOD.SelectedValue);
                else abstractRecord.OutcomeBestCorrectedVisualOD = 0;
                
                if (RadioButtonListRBExamAbnormalityColorVision.SelectedIndex != -1) abstractRecord.RBExamAbnormalityColorVision = Convert.ToInt32(RadioButtonListRBExamAbnormalityColorVision.SelectedValue);
                else abstractRecord.RBExamAbnormalityColorVision = 0;
                if (RadioButtonListRBExamBaselinePerimetryDefect.SelectedIndex != -1) abstractRecord.RBExamBaselinePerimetryDefect = Convert.ToInt32(RadioButtonListRBExamBaselinePerimetryDefect.SelectedValue);
                else abstractRecord.RBExamBaselinePerimetryDefect = 0;
                if (RadioButtonListRBExamOcularMotility.SelectedIndex != -1) abstractRecord.RBExamOcularMotility = Convert.ToInt32(RadioButtonListRBExamOcularMotility.SelectedValue);
                else abstractRecord.RBExamOcularMotility = 0;
                if (RadioButtonListRBExamOcularMotilityType.SelectedIndex != -1) abstractRecord.RBExamOcularMotilityType = Convert.ToInt32(RadioButtonListRBExamOcularMotilityType.SelectedValue);
                else abstractRecord.RBExamOcularMotilityType = 0;
                if (RadioButtonListRBExamOpticDisc.SelectedIndex != -1) abstractRecord.RBExamOpticDisc = Convert.ToInt32(RadioButtonListRBExamOpticDisc.SelectedValue);
                else abstractRecord.RBExamOpticDisc = 0;
                if (RadioButtonListRBExamOpticDiscAppearance.SelectedIndex != -1) abstractRecord.RBExamOpticDiscAppearance = Convert.ToInt32(RadioButtonListRBExamOpticDiscAppearance.SelectedValue);
                else abstractRecord.RBExamOpticDiscAppearance = 0;
                if (RadioButtonListRBExamRelativeAfferentPupillaryDefect.SelectedIndex != -1) abstractRecord.RBExamRelativeAfferentPupillaryDefect = Convert.ToInt32(RadioButtonListRBExamRelativeAfferentPupillaryDefect.SelectedValue);
                else abstractRecord.RBExamRelativeAfferentPupillaryDefect = 0;
                if (RadioButtonListRBExamUveitis.SelectedIndex != -1) abstractRecord.RBExamUveitis = Convert.ToInt32(RadioButtonListRBExamUveitis.SelectedValue);
                else abstractRecord.RBExamUveitis = 0;
                if (RadioButtonListRBGenderTypeID.SelectedIndex != -1) abstractRecord.RBGenderTypeID = Convert.ToInt32(RadioButtonListRBGenderTypeID.SelectedValue);
                else abstractRecord.RBGenderTypeID = 0;
                if (RadioButtonListRBHistoryAutoimmuneDisease.SelectedIndex != -1) abstractRecord.RBHistoryAutoimmuneDisease = Convert.ToInt32(RadioButtonListRBHistoryAutoimmuneDisease.SelectedValue);
                else abstractRecord.RBHistoryAutoimmuneDisease = 0;
                if (RadioButtonListRBHistoryPainEyeMovement.SelectedIndex != -1) abstractRecord.RBHistoryPainEyeMovement = Convert.ToInt32(RadioButtonListRBHistoryPainEyeMovement.SelectedValue);
                else abstractRecord.RBHistoryPainEyeMovement = 0;
                if (RadioButtonListRBManageMRI.SelectedIndex != -1) abstractRecord.RBManageMRI = Convert.ToInt32(RadioButtonListRBManageMRI.SelectedValue);
                else abstractRecord.RBManageMRI = 0;
                if (RadioButtonListRBManageRelationship.SelectedIndex != -1) abstractRecord.RBManageRelationship = Convert.ToInt32(RadioButtonListRBManageRelationship.SelectedValue);
                else abstractRecord.RBManageRelationship = 0;
                if (RadioButtonListRBManageSystemicCorticosteroid.SelectedIndex != -1) abstractRecord.RBManageSystemicCorticosteroid = Convert.ToInt32(RadioButtonListRBManageSystemicCorticosteroid.SelectedValue);
                else abstractRecord.RBManageSystemicCorticosteroid = 0;
                if (RadioButtonListOutcomePerimetry.SelectedIndex != -1) abstractRecord.OutcomePerimetry = Convert.ToInt32(RadioButtonListOutcomePerimetry.SelectedValue);
                else abstractRecord.OutcomePerimetry = 0;
                if (RadioButtonListCorticosteroidTherapy.SelectedIndex != -1) abstractRecord.RBCorticosteroidTherapy = Convert.ToInt32(RadioButtonListCorticosteroidTherapy.SelectedValue);
                else abstractRecord.RBCorticosteroidTherapy = 0;


                if (RadioButtonListRBHistorySystemic.SelectedIndex != -1) abstractRecord.RadioButtonListRBHistorySystemic = Convert.ToInt32(RadioButtonListRBHistorySystemic.SelectedValue);
                else abstractRecord.RadioButtonListRBHistorySystemic = 0;



                if (RadioButtonListRBManageSystemicCorticosteroidSE.SelectedIndex != -1) abstractRecord.RBManageSystemicCorticosteroidSE = Convert.ToInt32(RadioButtonListRBManageSystemicCorticosteroidSE.SelectedValue);
                else abstractRecord.RBManageSystemicCorticosteroidSE = 0;
                if (RadioButtonListRBManageWhiteMatterLesion.SelectedIndex != -1) abstractRecord.RBManageWhiteMatterLesion = Convert.ToInt32(RadioButtonListRBManageWhiteMatterLesion.SelectedValue);
                else abstractRecord.RBManageWhiteMatterLesion = 0;
                if (RadioButtonListRBOutcomeFinalEtiology.SelectedIndex != -1) abstractRecord.RBOutcomeFinalEtiology = Convert.ToInt32(RadioButtonListRBOutcomeFinalEtiology.SelectedValue);
                else abstractRecord.RBOutcomeFinalEtiology = 0;
                if (RadioButtonListRBOutcomeFollowUpBCVARecorded.SelectedIndex != -1) abstractRecord.RBOutcomeFollowUpBCVARecorded = Convert.ToInt32(RadioButtonListRBOutcomeFollowUpBCVARecorded.SelectedValue);
                else abstractRecord.RBOutcomeFollowUpBCVARecorded = 0;
                if (RadioButtonListRBOutcomePerimetryDefect.SelectedIndex != -1) abstractRecord.RBOutcomePerimetryDefect = Convert.ToInt32(RadioButtonListRBOutcomePerimetryDefect.SelectedValue);
                else abstractRecord.RBOutcomePerimetryDefect = 0;
                if (RadioButtonListRBOutcomeWhiteMatterLesionReferral.SelectedIndex != -1) abstractRecord.RBOutcomeWhiteMatterLesionReferral = Convert.ToInt32(RadioButtonListRBOutcomeWhiteMatterLesionReferral.SelectedValue);
                else abstractRecord.RBOutcomeWhiteMatterLesionReferral = 0;
                if (RadioButtonListRBSymptomsGeneralNeurologic.SelectedIndex != -1) abstractRecord.RBSymptomsGeneralNeurologic = Convert.ToInt32(RadioButtonListRBSymptomsGeneralNeurologic.SelectedValue);
                else abstractRecord.RBSymptomsGeneralNeurologic = 0;
                
                abstractRecord.HisotryAIOther = CheckBoxHistoryAIOther.Checked;
                abstractRecord.HistoryAIBartonella = CheckBoxHistorySIBartonella.Checked;

                abstractRecord.HistoryAISyphillis = CheckBoxHistorySISyphillis.Checked;
                abstractRecord.HistoryAIViral = CheckBoxHistorySIViral.Checked;
                abstractRecord.HistoryVisualComplaintBlurredVision = CheckBoxHistoryVisualComplaintBlurredVision.Checked;
                abstractRecord.HistoryVisualComplaintColorVision = CheckBoxHistoryVisualComplaintColorVision.Checked;
                abstractRecord.HistoryVisualComplaintDimming = CheckBoxHistoryVisualComplaintDimming.Checked;
                abstractRecord.HistoryVisualComplaintNotRecorded = CheckBoxHistoryVisualComplaintNotRecorded.Checked;
                abstractRecord.HistoryVisualComplaintOther = CheckBoxHistoryVisualComplaintOther.Checked;
                abstractRecord.HistoryVisualComplaintVisualFieldDefect = CheckBoxHistoryVisualComplaintVisualFieldDefect.Checked;
                abstractRecord.CheckBoxDropdownListOutcomeBestCorrectedVisualODNA = CheckBoxDropdownListOutcomeBestCorrectedVisualODNA.Checked;
                abstractRecord.CheckBoxDropdownListOutcomeBestCorrectedVisualOSNA = CheckBoxDropdownListOutcomeBestCorrectedVisualOSNA.Checked; 
                abstractRecord.SymptomsAtaxia = CheckBoxSymptomsAtaxia.Checked;
                abstractRecord.SymptomsDiplopia = CheckBoxSymptomsDiplopia.Checked;
                abstractRecord.SymptomsOther = CheckBoxSymptomsOther.Checked;
                abstractRecord.SymptomsParesthesia = CheckBoxSymptomsParesthesia.Checked;
                abstractRecord.SymptomsWeakness = CheckBoxSymptomsWeakness.Checked;
               abstractRecord.CheckBoxHistoryAISystemiclupus = CheckBoxHistoryAISystemiclupus.Checked;
               abstractRecord.CheckBoxHistoryAISarcoidosis =  CheckBoxHistoryAISarcoidosis.Checked;
               abstractRecord.CheckBoxHistoryAIOther = CheckBoxHistoryAIOther.Checked;

               abstractRecord.CheckBoxHistorySIOther = CheckBoxHistorySIOther.Checked;




                if (RadioButtonOutcomeFollowUpExamYes.Checked) abstractRecord.OutcomeFollowUpExam = true;
                if (RadioButtonOutcomeFollowUpExamNo.Checked) abstractRecord.OutcomeFollowUpExam = false;
                if (RadioButtonExamBaselinePerimetryYes.Checked) abstractRecord.ExamBaselinePerimetry = true;
                if (RadioButtonExamBaselinePerimetryNo.Checked) abstractRecord.ExamBaselinePerimetry = false;
                try
                {
                    if (TextBoxExamMeanDefectDB.Text != null)
                        abstractRecord.ExamMeanDefectDB = Convert.ToInt32(TextBoxExamMeanDefectDB.Text);
                }
                catch (Exception ex)
                {
                    abstractRecord.ExamMeanDefectDB = null;
                    TextBoxExamMeanDefectDB.Text = "";
                }
                try
                {
                    if (TextBoxOutcomeMeanDefectDB.Text != null)
                        abstractRecord.OutcomeMeanDefectDB = Convert.ToInt32(TextBoxOutcomeMeanDefectDB.Text);
                }
                catch (Exception ex)
                {
                    abstractRecord.OutcomeMeanDefectDB = null;
                    TextBoxOutcomeMeanDefectDB.Text = "";
                }
                try
                {
                    if (TextBoxSystemicCSText.Text != null)
                        abstractRecord.SystemicCSText = TextBoxSystemicCSText.Text;
                }
                catch (Exception ex)
                {
                    abstractRecord.SystemicCSText = null;
                    TextBoxSystemicCSText.Text = "";
                }

                abstractRecord.LastUpdateDate = DateTime.Now;
                bool ChartCompleted = isComplete();
                abstractRecord.Complete = ChartCompleted;
                var chartRegistration = (from cr in pim.ChartRegistration
                                         where cr.ParticipantModuleCycleID == cycle.ParticipantModuleCycleID
                                          & cr.ModuleID == ModuleID
                                          & cr.RecordIdentifier == recordIdentifier
                                         select cr).First<NetHealthPIMModel.ChartRegistration>();
                chartRegistration.AbstractCompleted = ChartCompleted;


                if ((DropDownListYearOfBirth.SelectedIndex > 0) && (DropDownListYearOfBirth.SelectedIndex > 0))
                {
                    chartRegistration.DOB = abstractRecord.MonthOfBirth + "/" + abstractRecord.YearOfBirth;
                }

                pim.SaveChanges();

                transaction.Complete();




            }

            CycleManager.UpdateParticipantCompletedModules(cycleID, ModuleID);
        }


    }
protected void ButtonSubmit_Click(object sender, EventArgs e)
{

    savedata();
    isComplete(); 
    if (!isComplete())
    {
        isComplete();
            string script = " var answer = confirm('Chart data is not complete.\\nClick OK to save entries and exit chart.\\nClick CANCEL to save entries and review the chart. '); if (answer==true) window.location = '../PatientChartRegistration.aspx';";
            ScriptManager.RegisterStartupScript(this, GetType(),
                          "ServerControlScript", script, true);
    }
    else
    {

        Response.Redirect("../PatientChartRegistration.aspx");
    }

}

    protected bool isComplete()
    {
        bool ChartCompleted = true;
        LabelMonthOfBirth.Visible= false;
            LabelYearOfBirth.Visible= false;
 LabelMonthOfFirstExam.Visible= false;
 LabelYearOfFirstExam.Visible = false;
  LabelHistoryVisualComplaint.Visible = false;
 LabelRBHistoryPainEyeMovement.Visible= false;
LabelRBSymptomsGeneralNeurologic.Visible = false;
  LabelSymptoms.Visible = false;
LabelRBHistoryAutoimmuneDisease.Visible = false;
LabelHistoryAI.Visible = false;
 LabelExamBestCorrectedVisualOD.Visible= false;
LabelExamBestCorrectedVisualOS.Visible= false;
LabelRBExamAbnormalityColorVision.Visible= false;
LabelRBExamRelativeAfferentPupillaryDefect.Visible= false;
 LabelRBExamOcularMotility.Visible= false;
   LabelRBExamOcularMotilityType.Visible= false;
    LabelRBExamUveitis.Visible= false;
  LabelRBExamOpticDisc.Visible = false;
 LabelRBExamOpticDiscAppearance.Visible = false;
LabelExamBaselinePerimetry.Visible = false;
            LabelRBExamBaselinePerimetryDefect.Visible = false;
 LabelExamMeanDefect.Visible= false;
LabelRBManageRelationship.Visible= false;
LabelRBManageMRI.Visible= false;
            LabelRBManageWhiteMatterLesion.Visible= false;
LabelRBManageSystemicCorticosteroid.Visible= false;
//LabelRBManageSystemicCorticosteroidDosage.Visible= false;
 LabelRBManageSystemicCorticosteroidSE.Visible= false;
  LabelOutcomeFollowUpExam.Visible= false;
 LabelRBOutcomeFollowUpBCVARecorded.Visible= false;
 LabelOutcomeBestCorrectedVisualOD.Visible = false;
            LabelExamBestCorrectedVisualOS.Visible= false;
            LabelOutcomePerimetry.Visible= false;
         LabelRBOutcomePerimetryDefect.Visible= false;
          LabelOutcomeMeanDefect.Visible= false;
            LabelRBOutcomeWhiteMatterLesionReferral.Visible= false;
            LabelRBOutcomeFinalEtiology.Visible= false;
            LabelRBHistorySystemic.Visible = false;
            LabelRBGenderTypeID.Visible = false;
            Labelinitialage.Visible = false;

            NetHealthPIMEntities pim = new NetHealthPIMEntities();
            var moduleinfo = (from c in pim.Module where c.ModuleID == 28 select c).FirstOrDefault();
            string message = "Patient must be  at least " + moduleinfo.RegistrationMinAge + " years old<br />";
            string registrationminage = Validation.validateRegistrationMinAge(Convert.ToInt32(DropDownListYearOfFirstExam.SelectedValue), Convert.ToInt32(DropDownListYearOfBirth.SelectedValue), Convert.ToInt32(moduleinfo.RegistrationMinAge), message);

            if (registrationminage != "")
            {
                Labelinitialage.Text = message;
                Labelinitialage.Visible = true;
                ChartCompleted = false;
            }
     
        if (DropDownListMonthOfBirth.SelectedIndex == 0) 
        { 
            LabelMonthOfBirth.Visible=true;
            ChartCompleted = false;
        }
       if (DropDownListYearOfBirth.SelectedIndex == 0) 
           
       {
       
            LabelYearOfBirth.Visible=true;
            ChartCompleted = false;
       
       
       }
           
     if (DropDownListMonthOfFirstExam.SelectedIndex == 0)
     { 
         
            LabelMonthOfFirstExam.Visible=true;
            ChartCompleted = false;
       
     }
     if (RadioButtonListRBGenderTypeID.SelectedIndex == -1)
     {

         LabelRBGenderTypeID.Visible = true;
         ChartCompleted = false;

     } 

     if (DropDownListYearOfFirstExam.SelectedIndex == 0)
     {

         LabelYearOfFirstExam.Visible = true;
         ChartCompleted = false;
     
     } 
      
        if (RadioButtonListRBHistoryPainEyeMovement.SelectedIndex == -1) 
        {
            LabelRBHistoryPainEyeMovement.Visible=true;
            ChartCompleted = false;

        }
        if(RadioButtonListRBSymptomsGeneralNeurologic.SelectedIndex==-1)
        {
            LabelRBSymptomsGeneralNeurologic.Visible = true;
            ChartCompleted = false;
        
        }


        if (RadioButtonListRBHistorySystemic.SelectedIndex == -1)
        {
            LabelRBHistorySystemic.Visible = true;
            ChartCompleted = false;

        }






        if (RadioButtonListRBHistoryAutoimmuneDisease.SelectedIndex == -1)
          {
              LabelRBHistoryAutoimmuneDisease.Visible = true;
              ChartCompleted = false;

          }
      
        if (DropDownExamBestCorrectedVisualOD.SelectedIndex == 0) 
        {
            LabelExamBestCorrectedVisualOD.Visible=true;
            ChartCompleted = false;

        }
        if(DropDownExamBestCorrectedVisualOS.SelectedIndex == 0) 
        {  
            LabelExamBestCorrectedVisualOS.Visible=true;
            ChartCompleted = false;
            
        }
        if(RadioButtonListRBExamAbnormalityColorVision.SelectedIndex==-1) 
        {
              LabelRBExamAbnormalityColorVision.Visible=true;
            ChartCompleted = false;
            
            
        }  
        if(RadioButtonListRBExamRelativeAfferentPupillaryDefect.SelectedIndex==-1) 
        {
            
             LabelRBExamRelativeAfferentPupillaryDefect.Visible=true;
            ChartCompleted = false;
            
        }
        if(RadioButtonListRBExamOcularMotility.SelectedIndex==-1) 
        {  
            
             LabelRBExamOcularMotility.Visible=true;
            ChartCompleted = false;
        }
            
       
         if(RadioButtonListRBExamUveitis.SelectedIndex==-1) 
         { 
            LabelRBExamUveitis.Visible=true;
            ChartCompleted = false;
             
             
         }
         if (RadioButtonListRBExamOpticDisc.SelectedIndex == -1)
         {
             LabelRBExamOpticDisc.Visible = true;
             ChartCompleted = false;


         }
        
        
        

        if (RadioButtonExamBaselinePerimetryYes.Checked == false && RadioButtonExamBaselinePerimetryNo.Checked == false)
        {
            LabelExamBaselinePerimetry.Visible = true;
            ChartCompleted = false;
        
        }
       
        if(RadioButtonExamBaselinePerimetryYes.Checked == true &&  TextBoxExamMeanDefectDB.Text=="" )
        {
        
                LabelExamMeanDefect.Visible=true;
                ChartCompleted = false;
        
        }
        if (RadioButtonListRBManageRelationship.SelectedIndex == -1 )
        {
            LabelRBManageRelationship.Visible=true;
            ChartCompleted = false;

        }
        if( RadioButtonListRBManageMRI.SelectedIndex == -1)
        {       LabelRBManageMRI.Visible=true;
               ChartCompleted = false;
        }
        
        if (RadioButtonListRBManageSystemicCorticosteroid.SelectedIndex == -1)
        {
            LabelRBManageSystemicCorticosteroid.Visible=true;
            ChartCompleted = false;

        }
      
            
        
        if (RadioButtonOutcomeFollowUpExamYes.Checked == false && RadioButtonOutcomeFollowUpExamNo.Checked==false)
        {
            LabelOutcomeFollowUpExam.Visible=true;
            ChartCompleted =false;

        }
        if (RadioButtonListRBOutcomeFollowUpBCVARecorded.SelectedIndex==-1)
        {
            LabelRBOutcomeFollowUpBCVARecorded.Visible=true;
            ChartCompleted = false;

        }
       
     
        
   
        if (RadioButtonListRBManageWhiteMatterLesion.SelectedValue == "1" && RadioButtonListRBOutcomeWhiteMatterLesionReferral.SelectedIndex == -1)
        {
            LabelRBOutcomeWhiteMatterLesionReferral.Visible=true;
            ChartCompleted = false;
        }
        if (RadioButtonListRBOutcomeFinalEtiology.SelectedIndex == -1)
        {
            LabelRBOutcomeFinalEtiology.Visible=true;
            ChartCompleted = false;
        
        }

        return ChartCompleted;
    }









}
