﻿<%@ Page Title="" Language="C#" MasterPageFile="~/PIMMasterPage.master" AutoEventWireup="true" CodeFile="CMChart.aspx.cs" Inherits="abo_CMChart" %>

<asp:Content ID="Content1" ContentPlaceHolderID="head" Runat="Server">

    <link type="text/css" href="../../common/css/atooltip.css" rel="stylesheet"  media="screen" />
	<script type="text/javascript" src="../../common/js/jquery.atooltip.js"></script>
    <script type="text/javascript" language="javascript">
        //      Either enables or disables input boxes
        function enabledControl(el, disabled, checked) {
            //alert("Hello world from enabled control");
            try {
                if (disabled) {
                    el.disabled = disabled;
                    el.setAttribute("disabled", true);
                }
                else {
                    el.disabled = disabled;
                    el.removeAttribute("disabled");
                }

                if (checked == true)
                    el.checked = false;
            }
            catch (E) {
            }
            if (el.childNodes && el.childNodes.length > 0) {
                for (var x = 0; x < el.childNodes.length; x++) {
                    enabledControl(el.childNodes[x], disabled, checked);
                }
            }
        }
        //      Either enables or disables dropdown boxes
        function enabledControlDropDown(el, disabled, checked) {
            //alert("Hello world from enabled control");
            try {
                if (disabled) {
                    el.disabled = disabled;
                    el.setAttribute("disabled", true);
                }
                else {
                    el.disabled = disabled;
                    el.removeAttribute("disabled");
                }

                if (checked == true)
                    el.options[0].selected = true;
            }
            catch (E) {
            }
            if (el.childNodes && el.childNodes.length > 0) {
                for (var x = 0; x < el.childNodes.length; x++) {
                    enabledControl(el.childNodes[x], disabled, checked);
                }
            }
        }
        //      Either enables or disables text boxes
        function enabledControlTextField(el, disabled, checked) {
            //alert("Hello world from enabled control");
            try {
                if (disabled) {
                    el.disabled = disabled;
                    el.setAttribute("disabled", true);
                }
                else {
                    el.disabled = disabled;
                    el.removeAttribute("disabled");
                }

                if (checked == true) {
                    el.value = '';
                }
            }
            catch (E) {
            }
            if (el.childNodes && el.childNodes.length > 0) {
                for (var x = 0; x < el.childNodes.length; x++) {
                    enabledControl(el.childNodes[x], disabled, checked);
                }
            }
        }

        function generate(arr1, arr2, istrue) {
            if (istrue == true) {
                for (var i = 0; i < arr1.length; i++) {
                    document.getElementById(arr1[i]).style.color = "gray";
                }
                for (var i = 0; i < arr2.length; i++) {
                    $(arr2[i] + ' :input').attr('disabled', true);
                    $(arr2[i] + ' :input').removeAttr("checked");
                }
            }
            if (istrue == false) {
                for (var i = 0; i < arr1.length; i++) {
                    document.getElementById(arr1[i]).style.color = "#333";
                }
                for (var i = 0; i < arr2.length; i++) {
                    $(arr2[i] + ' :input').removeAttr('disabled');
                }
            }
        }

     
  
    </script>

    <script  type="text/javascript" language="javascript">
        $(document).ready(function() {
            $("#<%= RadioButtonListRBSurgeryRadiationForm.ClientID %>").click(function() {
                if ($('#<%= RadioButtonListRBSurgeryRadiationForm.ClientID %>').find('input:checked').val() == ('14')) {
                    var myaray = new Array("QS4aA", "QS4aB");
                    var myarray2 = new Array('#QS4aB');
                    generate(myaray, myarray2, false);

                    enabledControlTextField(document.getElementById("<%= TextBoxSurgeryRadiationOther.ClientID %>"), false, false);
                }
                else {
                    var myaray = new Array("QS4aA", "QS4aB");
                    var myarray2 = new Array('#QS4aB');
                    generate(myaray, myarray2, true);

                    enabledControlTextField(document.getElementById("<%= TextBoxSurgeryRadiationOther.ClientID %>"), true, true);
                }
            });
            if ($('#<%= RadioButtonListRBSurgeryRadiationForm.ClientID %>').find('input:checked').val() == ('14')) {
                var myaray = new Array("QS4aA", "QS4aB");
                var myarray2 = new Array('#QS4aB');
                generate(myaray, myarray2, false);

                enabledControlTextField(document.getElementById("<%= TextBoxSurgeryRadiationOther.ClientID %>"), false, false);
            }
            else {
                var myaray = new Array("QS4aA", "QS4aB");
                var myarray2 = new Array('#QS4aB');
                generate(myaray, myarray2, true);

                enabledControlTextField(document.getElementById("<%= TextBoxSurgeryRadiationOther.ClientID %>"), true, true);
            }

            // #1 Toggle, Examination
            $("#<%= RadioButtonListExamTumorThicknessNo.ClientID %>").click(function() {
                enabledControlTextField(document.getElementById("<%= TextBoxExamTumorThickness.ClientID %>"), false, true);
            });
            $("#<%= TextBoxExamTumorThickness.ClientID %>").keyup(function() {
                enabledControl(document.getElementById("<%= RadioButtonListExamTumorThicknessNo.ClientID %>"), false, true);
            });

            // #2 Toggle, Examination
            $("#<%= RadioButtonListExamBasalDiameterNO.ClientID %>").click(function() {
                enabledControlTextField(document.getElementById("<%= TextBoxExamBasalDiameter.ClientID %>"), false, true);
            });
            $("#<%= TextBoxExamBasalDiameter.ClientID %>").keyup(function() {
                enabledControl(document.getElementById("<%= RadioButtonListExamBasalDiameterNO.ClientID %>"), false, true);
            });

            // #4a Toggle, Surgery
            $("#<%= RadioButtonListSurgeryRadiationOtherNO.ClientID %>").click(function() {
                enabledControlTextField(document.getElementById("<%= TextBoxSurgeryRadiationOther.ClientID %>"), false, true);
            });
            $("#<%= TextBoxSurgeryRadiationOther.ClientID %>").keyup(function() {
                enabledControl(document.getElementById("<%= RadioButtonListSurgeryRadiationOtherNO.ClientID %>"), false, true);
            });
        });
    </script>
    <style type="text/css">
        .bginputa{
	        float:right;
	        margin:21px 0 0 1px;
	        background: url(../common/images/orange_button_with_arrow.png) no-repeat;
	        overflow:hidden;
	        height:35px;

        }
        .bginputa a{
	        color: white;
	        text-align:center;
	        height:35px;
	        line-height:28px;
	        padding:0 14px 15px  ;
	        cursor:pointer;
	        float:left;
	        border:none;
	        text-decoration:none;
	        font-family:Arial, Helvetica, sans-serif; 
	        font-size:12px; 
	        font-weight:bold; 
	        letter-spacing: 0px;
        }
        .bginputa a:hover{
	        text-decoration:none;
        }
        .right {
            text-align:right;
        }
    </style>
    <script type="text/javascript">
        $(function() {
            $('#Q').aToolTip({
                clickIt: true,
                tipContent: ''
            });
        });
    </script>
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder1" Runat="Server">
    <asp:HiddenField ID="HiddenFieldRecordIdentifier" runat="server"/>
        <!--  pim -->
        <div class="pim">
            <div class="record-ident clearfix">
                <h3 class="record-first">RECORD IDENTIFIER: <asp:Literal runat="server" ID="LiteralRecordIdentifier"></asp:Literal></h3>
                <h3 class="record-second"><asp:Literal runat="server" ID="LiteralAbstractionNumber"></asp:Literal></h3>
            </div>
            <!-- History -->
            <table>
                <tr>
                    <th colspan="2" width="910px"><p>History</p></th>
                </tr>
                <!-- Question 1 -->
                <tr class="table_body">
                    <td width="70%">
                        <strong>1. Date of birth</strong>
                        <br /><asp:Label ID="LabelMonthOfBirth"  runat="server" Visible="false"  ForeColor="Red" Text="Please complete Month" />
                        <br /><asp:Label ID="LabelYearOfBirth"  runat="server" Visible="false"  ForeColor="Red" Text="Please complete Year" />
                    </td>
                    <td>
                        <asp:DropDownList ID='DropDownListMonthOfBirth' DataValueField='MonthID' DataTextField='MonthName' runat='server' />
                        <asp:DropDownList ID='DropDownListYearOfBirth' DataValueField='YearID' DataTextField='YearName' runat='server' />
                    </td>
                </tr>
                <!-- Question 2 -->
                <tr class="table_body_bg">
                    <td>
                        <strong>2. Date of treatment</strong>
                        <br /><asp:Label ID="LabelMonthOfTreatment"  runat="server" Visible="false"  ForeColor="Red" Text="Please complete month" />
                        <br /><asp:Label ID="LabelYearOfTreatment"  runat="server" Visible="false"  ForeColor="Red" Text="Please complete year" />
                    </td>
                    <td>
                        <asp:DropDownList ID='DropDownListMonthOfTreatment' DataValueField='MonthID' DataTextField='MonthName' runat='server' />
                        <asp:DropDownList ID='DropDownListYearOfTreatment' DataValueField='YearID' DataTextField='YearName' runat='server' />
                    </td>
                </tr>
               
                <!-- Question 3 -->
                <tr class="table_body">
                    <td>
                        <strong>3. Gender</strong>
                        <br /><asp:Label ID="LabelRBGender"  runat="server" Visible="false"  ForeColor="Red" Text="Please complete" />                       
                    </td>
                    <td>
                        <asp:RadioButtonList id='RadioButtonListRBGender' RepeatDirection='Horizontal' CssClass='aspxList' runat='server'>
                            <asp:ListItem Value='42'>&nbsp;Male</asp:ListItem>
                            <asp:ListItem Value='43'>&nbsp;Female</asp:ListItem>
                        </asp:RadioButtonList>
                    </td>
                </tr>               
            </table>
            <br />
            <!-- Examination -->
            <table>
                <tr>
                    <th colspan="3" width="910px"><p>Examination</p></th>
                </tr>
                <!-- Question 1 -->
                <tr class="table_body">
                    <td width="70%">
                        <strong>1. What was the pretreatment thickness of the tumor in mm?</strong>
                        <br /><asp:Label ID="LabelExamTumorThickness"  runat="server" Visible="false"  ForeColor="Red" Text="Please complete" />
                    </td>
                    <td>
                        <asp:TextBox runat="server" ID="TextBoxExamTumorThickness" size="5" />&nbsp;mm
                        <asp:CheckBoxList ID="RadioButtonListExamTumorThicknessNo" CssClass='aspxList' runat="server">
                            <asp:ListItem Value="3">Not Documented</asp:ListItem>
                        </asp:CheckBoxList>
                    </td>
                </tr>
                <!-- Question 2 -->
              
                <tr class="table_body_bg">
                    <td>
                        <strong>2. What was the pretreatment maximal basal diameter of the tumor in mm?</strong>
                        <asp:Label ID="LabelExamBasalDiameter"  runat="server" Visible="false"  ForeColor="Red" Text="<br />Please complete" />
                    </td>
                    <td>
                        <asp:TextBox runat="server" ID="TextBoxExamBasalDiameter" size="5" />&nbsp;mm
                        <asp:CheckBoxList ID="RadioButtonListExamBasalDiameterNO" CssClass='aspxList' runat="server">
                            <asp:ListItem Value="3">Not Documented</asp:ListItem>
                        </asp:CheckBoxList>
                    </td>
                </tr>
                <!-- Question 3 -->
                <tr class="table_body">
                    <td>
                        <strong>3. Is there documentation of preoperative assessment for liver metastasis?</strong>
                        <asp:Label ID="LabelExamPreopLiver"  runat="server" Visible="false"  ForeColor="Red" Text="<br />Please complete" />
                    </td>
                    <td>
                        <asp:RadioButton runat='server' ID='RadioButtonExamPreopLiverYes' GroupName='ExamPreopLiver' text='&nbsp;Yes' />&nbsp;&nbsp;
                        <asp:RadioButton runat='server' ID='RadioButtonExamPreopLiverNo' GroupName='ExamPreopLiver' text='&nbsp;No' />
                    </td>
                </tr>
            </table>
            <br />
            <!-- Surgery -->
            <table>
                <tr>
                    <th colspan="2" width="910px"><p>Surgery</p></th>
                </tr>
                <!-- Question 1 -->
                <tr class="table_body">
                    <td width="70%">
                        <strong>1. Was a biopsy performed?</strong>
                        <asp:Label ID="LabelSurgeryBiopsy"  runat="server" Visible="false"  ForeColor="Red" Text="<br />Please complete" />
                    </td>
                    <td>
                         <asp:RadioButton runat='server' ID='RadioButtonSurgeryBiopsyYes' GroupName='SurgeryBiopsy' text='&nbsp;Yes' />&nbsp;&nbsp;
                         <asp:RadioButton runat='server' ID='RadioButtonSurgeryBiopsyNo' GroupName='SurgeryBiopsy' text='&nbsp;No' />
                    </td>
                </tr>
                <!-- Question 2 -->
                <tr class="table_body_bg">
                    <td>
                        <strong>2. What was the apical radiation dose?</strong>
                        <asp:Label ID="LabelSurgeryRadiationDose" runat="server" Visible="false"  ForeColor="Red" Text="<br />Please complete" />
                    </td>
                    <td>
                        <asp:TextBox ID="TextBoxSurgeryRadiationDose" runat="server" Width="39px"></asp:TextBox>&nbsp;Gy
                    </td>
                </tr>
                <!-- Question 3 -->
                <tr class="table_body">
                    <td>
                        <strong>3. Was a rectus muscle detached?</strong>
                        <asp:Label ID="LabelSurgeryMuscleDetached" runat="server" Visible="false"  ForeColor="Red" Text="<br />Please complete" />
                    </td>
                    <td>
                        <asp:RadioButton runat='server' ID='RadioButtonSurgeryMuscleDetachedYes' GroupName='SurgeryMuscleDetached' text='&nbsp;Yes' />&nbsp;&nbsp;
                        <asp:RadioButton runat='server' ID='RadioButtonSurgeryMuscleDetachedNo' GroupName='SurgeryMuscleDetached' text='&nbsp;No' />
                    </td>
                </tr>
                <!-- Question 4 -->
                <tr class="table_body_bg">
                    <td>
                        <strong>4. What form of radiation was used?</strong>
                        <asp:Label ID="LabelRBSurgeryRadiationForm"  runat="server" Visible="false"  ForeColor="Red" Text="<br />Please complete" />
                    </td>
                    <td>
                        <asp:RadioButtonList id='RadioButtonListRBSurgeryRadiationForm' RepeatDirection='Vertical' CssClass='aspxList' runat='server'>
                            <asp:ListItem Value='101'>&nbsp;125-l</asp:ListItem>
                            <asp:ListItem Value='102'>&nbsp;Helium Ion</asp:ListItem>
                            <asp:ListItem Value='103'>&nbsp;Proton</asp:ListItem>
                            <asp:ListItem Value='14'>&nbsp;Other</asp:ListItem>
                        </asp:RadioButtonList>
                    </td>
                </tr>
                <!-- Question 4a -->
                <tr class="table_body_bg">
                    <td id="QS4aA">
                        <strong>4a. If other, please specify:</strong>
                        <asp:Label ID="LabeSurgeryRadiationOther"  runat="server" Visible="false"  ForeColor="Red" Text="<br />Please complete" />
                    </td>
                    <td id="QS4aB">
                        <asp:TextBox runat="server" ID="TextBoxSurgeryRadiationOther" size="25" />
                        <asp:CheckBoxList ID="RadioButtonListSurgeryRadiationOtherNO" CssClass='aspxList' runat="server">
                            <asp:ListItem Value="3">Not Documented</asp:ListItem>
                        </asp:CheckBoxList>
                    </td>
                </tr>
            </table>
            <br />
            <!-- Communication -->
            <table>
                <tr>
                    <th colspan="3" width="910px"><p>Communication</p></th>
                </tr>
                <!-- Question 1 -->
                <tr class="table_body">
                    <td width="70%">
                        <strong>1. Was there documentation of informed consent for surgery?</strong>
                        <asp:Label ID="LabelDocInformedConsent"  runat="server" Visible="false"  ForeColor="Red" Text="<br />Please complete" />
                    </td>
                    <td>
                        <asp:RadioButton runat='server' ID='RadioButtonDocInformedConsentYes' GroupName='DocInformedConsent' text='&nbsp;Yes' />&nbsp;&nbsp;
                        <asp:RadioButton runat='server' ID='RadioButtonDocInformedConsentNo' GroupName='DocInformedConsent' text='&nbsp;No' />
                    </td>
                </tr>
                <!-- Question 2 -->
                <tr class="table_body_bg">
                    <td>
                        <strong>2. Was the patient counseled regarding molecular testing for prognosis?</strong>
                        <asp:Label ID="LabelDocMolecularTesting"  runat="server" Visible="false"  ForeColor="Red" Text="<br />Please complete" />
                    </td>
                    <td>
                        <asp:RadioButton runat='server' ID='RadioButtonDocMolecularTestingYes' GroupName='DocMolecularTesting' text='&nbsp;Yes' />&nbsp;&nbsp;
                        <asp:RadioButton runat='server' ID='RadioButtonDocMolecularTestingNo' GroupName='DocMolecularTesting' text='&nbsp;No' />
                    </td>
                </tr>
                <!-- Question 3 -->
                <tr class="table_body">
                    <td>
                        <strong>3. Was there documented communication with the patient’s primary care physician or oncologist within 1 month of initial diagnosis?</strong>
                        <asp:Label ID="LabelDocPCP"  runat="server" Visible="false"  ForeColor="Red" Text="<br />Please complete" />
                    </td>
                    <td>
                        <asp:RadioButton runat='server' ID='RadioButtonDocPCPYes' GroupName='DocPCP' text='&nbsp;Yes' />&nbsp;&nbsp;&nbsp;&nbsp;
                        <asp:RadioButton runat='server' ID='RadioButtonDocPCPNo' GroupName='DocPCP' text='&nbsp;No' />
                    </td>
                </tr>
            </table>
            <br />
            <!-- Outcomes -->
            <table>
                <tr>
                    <th colspan="3" width="910px"><p>Outcomes</p></th>
                </tr>
                <!-- Question 1 -->
                <tr class="table_body">
                    <td width="70%">
                        <strong>1. At any time during the 5 year follow-up did the tumor increase in size (more than 1 mm in thickness; more than 2 mm in basal diameter)?</strong>
                        <asp:Label ID="LabelOutcomeSizeIncrease"  runat="server" Visible="false"  ForeColor="Red" Text="<br />Please complete" />
                    </td>
                    <td>
                        <asp:RadioButton runat='server' ID='RadioButtonOutcomeSizeIncreaseYes' GroupName='OutcomeSizeIncrease' text='&nbsp;Yes' />&nbsp;&nbsp;
                        <asp:RadioButton runat='server' ID='RadioButtonOutcomeSizeIncreaseNo' GroupName='OutcomeSizeIncrease' text='&nbsp;No' />
                    </td>
                </tr>
                <!-- Question 2 -->
                <tr class="table_body_bg">
                    <td>
                        <strong>2. At any time in the 5 year follow up period did the patient develop anterior segment neovascularization?</strong>
                        <asp:Label ID="LabelOutcomeNeovascularization"  runat="server" Visible="false"  ForeColor="Red" Text="<br />Please complete" />
                    </td>
                    <td>
                        <asp:RadioButton runat='server' ID='RadioButtonOutcomeNeovascularizationYes' GroupName='OutcomeNeovascularization' text='&nbsp;Yes' />&nbsp;&nbsp;
                        <asp:RadioButton runat='server' ID='RadioButtonOutcomeNeovascularizationNo' GroupName='OutcomeNeovascularization' text='&nbsp;No' />
                    </td>
                </tr>
                <!-- Question 3 -->
                <tr class="table_body">
                    <td>
                        <strong>3. Did the patient develop diplopia that persisted beyond the 6 week postoperative period?</strong>
                        <asp:Label ID="LabelOutcomeDiplopia"  runat="server" Visible="false"  ForeColor="Red" Text="<br />Please complete" />
                    </td>
                    <td>
                        <asp:RadioButton runat='server' ID='RadioButtonOutcomeDiplopiaYes' GroupName='OutcomeDiplopia' text='&nbsp;Yes' />&nbsp;&nbsp;
                        <asp:RadioButton runat='server' ID='RadioButtonOutcomeDiplopiaNo' GroupName='OutcomeDiplopia' text='&nbsp;No' />
                    </td>
                </tr>
                <!-- Question 4 -->
                <tr class="table_body_bg">
                    <td>
                        <strong>4. At any time during the 5 year follow up was the treated globe enucleated?</strong>
                        <asp:Label ID="LabelOutcomeEnucleated"  runat="server" Visible="false"  ForeColor="Red" Text="<br />Please complete" />
                    </td>
                    <td>
                        <asp:RadioButton runat='server' ID='RadioButtonOutcomeEnucleatedYes' GroupName='OutcomeEnucleated' text='&nbsp;Yes' />&nbsp;&nbsp;
                        <asp:RadioButton runat='server' ID='RadioButtonOutcomeEnucleatedNo' GroupName='OutcomeEnucleated' text='&nbsp;No' />
                    </td>
                </tr>
                <!-- Question 5 -->
                <tr class="table_body">
                    <td>
                        <strong>5. At any time during the 5 year follow up, did the patient develop metastasis, or die from melanoma related causes?</strong>
                        <asp:Label ID="LabelOutcomeMetastasis"  runat="server" Visible="false"  ForeColor="Red" Text="<br />Please complete" />
                    </td>
                    <td>
                        <asp:RadioButton runat='server' ID='RadioButtonOutcomeMetastasisYes' GroupName='OutcomeMetastasis' text='&nbsp;Yes' />&nbsp;&nbsp;
                        <asp:RadioButton runat='server' ID='RadioButtonOutcomeMetastasisNo' GroupName='OutcomeMetastasis' text='&nbsp;No' />
                    </td>
                </tr>
            </table>
            <div class="button-box">
                <asp:LinkButton ID="LinkButtonBackToDashboard" runat="server" Text="Back To Chart Registration" PostBackUrl="../PatientChartRegistration.aspx?CycleNumber=1" Visible="false" CssClass="button" />
                <asp:LinkButton ID="ButtonSubmit" OnClick="ButtonSubmit_Click" runat="server" Text="Submit Chart" CssClass="button" />
            </div>
        </div>
        <!-- ION pim ends -->
</asp:Content>

