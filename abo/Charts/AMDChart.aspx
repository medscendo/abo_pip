﻿<%@ Page Title="" Language="C#" MasterPageFile="~/PIMMasterPage.master" AutoEventWireup="true" CodeFile="AMDChart.aspx.cs" Inherits="abo_AMDChart" %>

<asp:Content ID="Content1" ContentPlaceHolderID="head" Runat="Server">

    <link type="text/css" href="../../common/css/atooltip.css" rel="stylesheet"  media="screen" />
	<script type="text/javascript" src="../../common/js/jquery.atooltip.js"></script>
    
    <style>

        .right {
            text-align:right;
        }

    </style>
    
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder1" Runat="Server">
    <asp:HiddenField ID="HiddenFieldRecordIdentifier" runat="server"/>
        <!-- Exudative Age-Related Macular Degeneration pim -->
        <div class="pim">
            <div class="record-ident clearfix">
                <h3 class="record-first">RECORD IDENTIFIER: <asp:Literal runat="server" ID="LiteralRecordIdentifier"></asp:Literal></h3>
                <h3 class="record-second"><asp:Literal runat="server" ID="LiteralAbstractionNumber"></asp:Literal></h3>
            </div>
            <!-- PQRS Panel -->
            <asp:Panel runat="server" Visible="true" ID="PQRSPanel" CssClass="clear">
                <table>
                    <tr>
                        <th colspan="3" width="910px"><p>Measure #14 - Age-Related Macular Degeneration (AMD): Dilated Macular Examination</p></th>
                    </tr>
                    <!-- Question 1 -->
                    <tr class="table_body">
                        <td width="70%">
                            <strong>1. Date of patient visit with one of the specified encounter codes</strong>
                            <asp:Label ID="LabelPQRSVisitMonth" runat="server" ForeColor="Red" Visible="false" Text="<br />Please Complete Month"></asp:Label>
                            <asp:Label ID="LabelPQRSVisitYear" runat="server" ForeColor="Red" Visible="false" Text="<br />Please Complete Year"></asp:Label>
                        
                        </td>
                        <td>
                            <asp:DropDownList ID='DropDownListPQRSVisitMonth' DataValueField='MonthID' DataTextField='MonthName' runat='server' />
                            <asp:DropDownList ID='DropDownListPQRSVisitYear' DataValueField='YearID' DataTextField='YearName' runat='server' />
                        </td>
                    </tr>
                    <!-- Question 2 -->
                    <tr class="table_body_bg" id="PQRS14Q2">
                        <td>
                            <strong>2. Is the patient a Medicare FFS patient?</strong>&nbsp;<a href="#"><img id="PQRSQ2" src="../../common/images/tip.gif" alt="Tip" /></a>
                            <asp:Label ID="LabelPQRSMedicare" runat="server" ForeColor="Red" Visible="false" Text="<br />Please Complete"></asp:Label>
                        
                        </td>
                        <td>
                            <asp:RadioButtonList runat="server" ID="PQRSMedicare" RepeatDirection="Horizontal" RepeatLayout="Flow">
                                <asp:ListItem Value="1" Text=" Yes &nbsp;" />
                                <asp:ListItem Value="0" Text=" No" />
                            </asp:RadioButtonList>
                        </td>
                    </tr>
                    <!-- Question 3 -->
                    <tr class="table_body" id="PQRS14Q3">
                        <td>
                            <strong>3. Does the patient have one of the noted diagnosis codes? (Denominator Criteria)</strong>&nbsp;<a href="#"><img id="PQRSQ3" src="../../common/images/tip.gif" alt="Tip" /></a>
                            <asp:Label ID="LabelPQRSDiagnosis" runat="server" ForeColor="Red" Visible="false" Text="<br />Please Complete"></asp:Label>
                        </td>
                        <td>
                            <asp:RadioButtonList runat="server" ID="PQRSDiagnosis" RepeatDirection="Horizontal" RepeatLayout="Flow">
                                <asp:ListItem Value="1" Text=" Yes &nbsp;" />
                                <asp:ListItem Value="0" Text=" No" />
                            </asp:RadioButtonList>
                        </td>
                    </tr>
                    <!-- Question 4 -->
                    <tr class="table_body_bg" id="PQRS14Q4">
                        <td>
                            <strong>4. Does the patient have one of the noted encounter codes (CPT)? (Denominator Criteria)</strong>
                            <asp:Label ID="LabelPQRSEncounter" runat="server" ForeColor="Red" Visible="false" Text="<br />Please Complete"></asp:Label>
                        </td>
                        <td>
                            <asp:RadioButtonList runat="server" ID="PQRSEncounter" RepeatDirection="Horizontal" RepeatLayout="Flow">
				                <asp:ListItem Value="1" Text=" Yes &nbsp;" />
				                <asp:ListItem Value="0" Text=" No" />
			                </asp:RadioButtonList>
                        </td>
                    </tr>
                    <!-- Question 5 -->
                    <tr class="table_body" id="PQRS14Q5">
                        <td>
                            <strong>5.	Was a dilated macular examination performed?</strong>&nbsp;<a href="#"><img id="PQRSQ5" src="../../common/images/tip.gif" alt="Tip" /></a>
                            <asp:Label ID="LabelPQRSMacularExamination" runat="server" ForeColor="Red" Visible="false" Text="<br />Please Complete"></asp:Label>
                        </td>
                        <td>
                            <asp:RadioButtonList runat="server" ID="PQRSMacularExamination" RepeatDirection="Horizontal" RepeatLayout="Flow">
                                <asp:ListItem Value="1" Text=" Yes &nbsp;" />
                                <asp:ListItem Value="0" Text=" No" />
                            </asp:RadioButtonList>
                        </td>
                    </tr>
                    <!-- Question 6 -->
                    <tr class="table_body_bg" id="PQRS14Q6">
                        <td>
                            <strong>6. Is there documentation of the presence or absence of macular thickening or hemorrhage</strong>&nbsp;<a href="#"><img id="PQRSQ6" src="../../common/images/tip.gif" alt="Tip" /></a>
                            <asp:Label ID="LabelPQRSMacularThickening" runat="server" ForeColor="Red" Visible="false" Text="<br />Please Complete"></asp:Label>
                        
                        </td>
                        <td>
                            <asp:RadioButtonList runat="server" ID="PQRSMacularThickening" RepeatDirection="Horizontal" RepeatLayout="Flow">
                                <asp:ListItem Value="1" Text=" Yes &nbsp;" />
                                <asp:ListItem Value="0" Text=" No" />
                            </asp:RadioButtonList>
                        </td>
                    </tr>
                    <!-- Question 7 -->
					<tr class="table_body" id="PQRS14Q7">
                        <td>
                            <strong>7. Was the level of macular degeneration severity documented?</strong>&nbsp;<a href="#"><img id="PQRSQ7" src="../../common/images/tip.gif" alt="Tip" /></a>
                            <asp:Label ID="LabelPQRSMacularDocumented" runat="server" ForeColor="Red" Visible="false" Text="<br />Please Complete"></asp:Label>
                        
                        </td>
                        <td>
                            <asp:RadioButtonList runat="server" ID="PQRSMacularDocumented" RepeatDirection="Horizontal" RepeatLayout="Flow">
                                <asp:ListItem Value="1" Text=" Yes &nbsp;" />
                                <asp:ListItem Value="0" Text=" No" />
                            </asp:RadioButtonList>
                        </td>
                    </tr>
                    <!-- Question 8 -->
                    <tr class="table_body_bg" id="PQRS14Q8">
                        <td>
                            <strong>8.	If <em>either</em> the dilated macular exam, the presence or absence of macular thickening or hemorrhage, <em>or</em> the level of macular degeneration severity&nbsp; was not documented, was this due to a documented reason?</strong>&nbsp;<a href="#"><img id="PQRSQ8" src="../../common/images/tip.gif" alt="Tip" /></a>
                            <asp:Label ID="LabelPQRSMacularSeverity" runat="server" ForeColor="Red" Visible="false" Text="<br />Please Complete"></asp:Label>
                        
                        </td>
                        <td>
                            <asp:RadioButtonList runat="server" ID="PQRSMacularSeverityReason" RepeatDirection="Horizontal" RepeatLayout="Flow">
                                <asp:ListItem Value="1" Text=" Yes &nbsp;" />
                                <asp:ListItem Value="0" Text=" No" />
                            </asp:RadioButtonList>
                        </td>
                    </tr>
                      
                </table>
                <br />
                <table>
                    <tr>
                        <th colspan="3" width="910px"><p>Measure #140 Age-Related Macular Degeneration (AMD): Counseling on Antioxidant Supplement&nbsp;<a href="#"><img id="PQRSHeader2" src="../../common/images/tip.gif" alt="Tip" /></a></p></th>
                    </tr>
                    <!-- Question 1 -->
                    <tr class="table_body" id="PQRS140Q1">
                        <td style="width:70%;">
                            <strong>1. Was the patient and/or their caregiver(s) counseled within 12 months on the benefits and/or risks of the AREDS formulation for preventing progression of AMD?</strong>&nbsp;<a href="#"><img id="PQRSM114Q1" src="../../common/images/tip.gif" alt="Tip" /></a>
                        </td>
                        <td>
                            <asp:RadioButtonList runat="server" ID="PQRSCounseling" RepeatDirection="Horizontal" RepeatLayout="Flow">
                                <asp:ListItem Value="1" Text=" Yes &nbsp;" />
                                <asp:ListItem Value="0" Text=" No" />
                            </asp:RadioButtonList>
                        </td>
                    </tr>
                </table>
                <br />
                
                <script>
                    <%--Validation for PQRS question 2--%>
                    function PQRSMedicareValidation() {
                        var myarray = new Array("PQRS14Q3", "PQRS14Q4", "PQRS14Q5", "PQRS14Q6", "PQRS14Q7", "PQRS14Q8", "PQRS140Q1");
                        var myarray2 = new Array("#PQRS14Q3", "#PQRS14Q4", "#PQRS14Q5", "#PQRS14Q6", "#PQRS14Q7", "#PQRS14Q8", "#PQRS140Q1");
                        var myarray3 = new Array("PQRS14Q3");
                        var myarray4 = new Array("#PQRS14Q3");

                        if ($('#<%= PQRSMedicare.ClientID %> input:checked').val() == '1') {
                            generate(myarray3, myarray4, false);
                        }
                        else {
                            generate(myarray, myarray2, true);
                        }
                    }

                    function question3() {
                        var myarray = new Array("PQRS14Q4", "PQRS14Q5", "PQRS14Q6", "PQRS14Q7", "PQRS14Q8", "PQRS140Q1");
                        var myarray2 = new Array("#PQRS14Q4", "#PQRS14Q5", "#PQRS14Q6", "#PQRS14Q7", "#PQRS14Q8", "#PQRS140Q1");
                        var myarray3 = new Array("PQRS14Q4");
                        var myarray4 = new Array("#PQRS14Q4");

                        if ($('#<%= PQRSDiagnosis.ClientID %> input:checked').val() == '1') {
                            generate(myarray3, myarray4, false);
                        }
                        else {
                            generate(myarray, myarray2, true);
                        }
                    }

                    <%--Validation for PQRS question 4--%>
                    function PQRSEncounterValidation() {
                        var myarray = new Array("PQRS14Q5", "PQRS14Q6", "PQRS14Q7", "PQRS14Q8", "PQRS140Q1");
                        var myarray2 = new Array("#PQRS14Q5", "#PQRS14Q6", "#PQRS14Q7", "#PQRS14Q8", "#PQRS140Q1");

                        if ($('#<%= PQRSEncounter.ClientID %> input:checked').val() == '1') {
                            generate(myarray, myarray2, false);
                        }
                        else {
                            generate(myarray, myarray2, true);
                        }
                    }

                    <%--Validation for PQRS question 5--%>
                    function PQRSMacularExaminationValidation() {
                        var myarray = new Array("PQRS14Q6", "PQRS14Q7");
                        var myarray2 = new Array("#PQRS14Q6", "#PQRS14Q7");

                        if ($('#<%= PQRSMacularExamination.ClientID %> input:checked').val() == '1') {
                            generate(myarray, myarray2, false);
                        }
                        else {
                            generate(myarray, myarray2, true);
                        }
                    }

                    <%--Validation for PQRS question 6--%>
                    function PQRSMacularThickeningValidation() {
                        var myarray = new Array("PQRS14Q7");
                        var myarray2 = new Array("#PQRS14Q7");

                        if ($('#<%= PQRSMacularThickening.ClientID %> input:checked').val() == '1') {
                            generate(myarray, myarray2, false);
                        }
                        else {
                            generate(myarray, myarray2, true);
                        }
                    }

                    function question8ifNo() {
                        var myarray = new Array("PQRS14Q8");
                        var myarray2 = new Array("#PQRS14Q8");

                        if ($("#<%= PQRSMacularExamination.ClientID %> input:checked").val() == "0" || $("#<%= PQRSMacularThickening.ClientID %> input:checked").val() == "0" || $("#<%= PQRSMacularDocumented.ClientID %> input:checked").val() == "0") {
                            generate(myarray, myarray2, false);
                        }
                        else {
                            generate(myarray, myarray2, true);
                        }
                    }

                    $(document).ready(function () {

                        /* Question 7 */
                        $("#<%= PQRSMacularDocumented.ClientID %>").click(function () {
                            question8ifNo();
                        });

                        /* Question 6 */
                        $("#<%= PQRSMacularThickening.ClientID %>").click(function () {
                            question8ifNo();
                            PQRSMacularThickeningValidation();
                        });

                        

                        /********** Question 5-8 block **********/
                        PQRSMacularExaminationValidation();
                        PQRSMacularThickeningValidation();
                        question8ifNo();

                        /* Question 5 */
                        $("#<%= PQRSMacularExamination.ClientID %>").click(function () {
                            PQRSMacularExaminationValidation();
                            question8ifNo();
                        });
                        
                        <%-- Validation for when the user clicks no for question 4 (Do they have an encounter code? --%>
                        $("#<%= PQRSEncounter.ClientID %>").click(function () {
                            PQRSEncounterValidation();
                        });
                        PQRSEncounterValidation();
                        
                        /* Question 3 */
                        $("#<%= PQRSDiagnosis.ClientID %>").click(function () {
                            question3();
                        });

                        question3();

                        <%-- Validation for when the user clicks no for question 2 (is medicare? --%>
                        $("#<%= PQRSMedicare.ClientID %>").click(function () {
                            PQRSMedicareValidation();
                        });
                        PQRSMedicareValidation();
                        
                        /* Check that user is inputting correct year */
                        var rightYearArr = new Array("PQRS14Q2", "PQRS14Q3", "PQRS14Q4", "PQRS14Q5", "PQRS14Q6", "PQRS14Q7", "PQRS14Q8", "PQRS140Q1");
                        var rightYearArr2 = new Array("#PQRS14Q2", "#PQRS14Q3", "#PQRS14Q4", "#PQRS14Q5", "#PQRS14Q6", "#PQRS14Q7", "#PQRS14Q8", "#PQRS140Q1");
                        $("#<%= DropDownListPQRSVisitYear.ClientID %>").change(function () {
                            var year = $("#<%= DropDownListPQRSVisitYear.ClientID %>").val();

                            if (year != 2015 && year != 0) {
                                generate(rightYearArr, rightYearArr2, true);
                                alert("Date of procedure must be in 2015 to be valid for PQRS Reporting.");
                            } else {
                                generate(rightYearArr, rightYearArr2, false);
                            }
                        });
                    });

                </script>
                <script>
                    $(function () {
                        $('#PQRSQ2').aToolTip({
                            clickIt: true,
                            tipContent: 'Medicare Part B can be the Primary, Secondary or Tertiary insurance'
                        });
                    });

                    $(function () {
                        $('#PQRSQ3').aToolTip({
                            clickIt: true,
                            tipContent: '<p><strong>ICD-9-CM [for use 1/1/2015-9/30/2015]:</strong> 365.10, 365.11, 365.12, 365.15</p><p><strong>ICD-10-CM [for use 10/1/2015-12/31/2015]:</strong> H40.10X0, H40.10X1, H40.10X2, H40.10X3, H40.10X4, H40.11X0, H40.11X1, H40.11X2, H40.11X3, H40.11X4, H40.1210, H40.1211, H40.1212, H40.1213, H40.1214, H40.1220, H40.1221, H40.1222, H40.1223, H40.1224, H40.1230, H40.1231, H40.1232, H40.1233, H40.1234, H40.1290, H40.1291, H40.1292, H40.1293, H40.1294, H40.151, H40.152, H40.153, H40.159</p>'
                        });
                    });

                    $(function () {
                        $('#PQRSQ5').aToolTip({
                            clickIt: true,
                            tipContent: '<p><strong>Definition:</strong></p><p><strong>Macular Thickening:</strong> Acceptable synonyms for &ldquo;macular thickening&rdquo; include: intraretinal thickening, serous detachment of the retina, pigment epithelial detachment, or macular edema.</p>'
                        });
                    });

                    $(function () {
                        $('#PQRSQ6').aToolTip({
                            clickIt: true,
                            tipContent: '<p><strong>Definition:</strong></p><p><strong>Macular Thickening:</strong> Acceptable synonyms for &ldquo;macular thickening&rdquo; include: intraretinal thickening, serous detachment of the retina, pigment epithelial detachment, or macular edema.</p>'
                        });
                    });

                    $(function () {
                        $('#PQRSQ7').aToolTip({
                            clickIt: true,
                            tipContent: '<p><strong>Definition:</strong></p><p><strong>Severity of Macular Degeneration: </strong>Early, intermediate, or advanced.</p>'
                        });
                    });

                    $(function () {
                        $('#PQRSQ8').aToolTip({
                            clickIt: true,
                            tipContent: 'The medical or patient reason(s) must be documented in the medical record.'
                        });
                    });

                    $(function () {
                        $('#PQRSHeader2').aToolTip({
                            clickIt: true,
                            tipContent: 'Documentation in the medical record should include a discussion of risk or benefits of the AREDS formulation. Counseling can be discussed with all patients with AMD, even those who do not meet the criteria for the AREDS formulation, patients who are smokers (beta-carotene can increase the risk for cancer in these patients) or other reasons why the patient would not meet criteria for AREDS formulation as outlined in the AREDS. The ophthalmologist or optometrist can explain why these supplements are not appropriate for their particular situation. Also, given the purported risks associated with antioxidant use, patients would be informed of the risks and benefits and make their choice based on valuation of vision loss vs. other risks. As such, the measure seeks to educate patients about overuse as well as appropriate use.'
                        });
                    });

                    $(function () {
                        $('#PQRSM114Q1').aToolTip({
                            clickIt: true,
                            tipContent: 'Note: If patient is already receiving AREDS formulation, the assumption is that counseling about AREDS has already been performed'
                        });
                    });
                </script>
            </asp:Panel>
            <!-- History -->
            <table>
                <tr>
                    <th colspan="3" width="910px"><p>History</p></th>
                </tr>
                <!-- Question 1 -->
                <tr class="table_body">
                    <td width="70%">
                        <strong>1. Date of birth</strong>
                        <asp:Label ID='LabelMonthOfBirth' runat='server' Visible="false"  ForeColor="Red" Text="<br />Please complete month" />
                        <asp:Label ID='LabelYearOfBirth' runat='server' Visible="false"  ForeColor="Red" Text='<br />Please complete year' />
                    </td>
                    <td>
                        <asp:DropDownList ID='DropDownListMonthOfBirth' DataValueField='MonthID' DataTextField='MonthName' runat='server' />
                        <asp:DropDownList ID='DropDownListYearOfBirth' DataValueField='YearID' DataTextField='YearName' runat='server' />
                    </td>
                </tr>
                <!-- Question 2 -->
                <tr class="table_body_bg">
                    <td>
                        <strong>2. Date of exam</strong>
                        <asp:Label ID='LabelMonthOfExam' runat='server' Visible="false"  ForeColor="Red" Text='<br />Please complete month' />
                        <asp:Label ID='LabelYearOfExam' runat='server' Visible="false"  ForeColor="Red" Text='<br />Please complete year' />
                        <asp:Label ID='Labelinitialage' runat='server' Visible="false"  
                            ForeColor="Red" Text='<br />' />
                        <asp:Label ID='Labelinitialvis' runat='server' Visible="false"  
                            ForeColor="Red" Text='<br />Please complete year' /><br />
                              <asp:Label ID='Labelexam' runat='server' Visible="false"  
                            ForeColor="Red" Text='<br />Date cannot be after todays date' />
                    </td>
                    <td>
                        <asp:DropDownList ID='DropDownListMonthOfExam' DataValueField='MonthID' DataTextField='MonthName' runat='server' />
                        <asp:DropDownList ID='DropDownListYearOfExam' DataValueField='YearID' DataTextField='YearName' runat='server' />
                    </td>
                </tr>
                <!-- Question 3 -->
                <tr class="table_body">
                    <td>
                        <strong>3. Date of follow-up exam</strong> (choose exam that is closest to the 
                        one year follow-up date, but not less than one year in follow-up)
                        <asp:Label ID='LabelMonthOfFollowUp' runat='server' Visible="false"  ForeColor="Red" Text='<br />Please complete month' />
                        
                        <asp:Label ID='LabelYearOfFollowUp' runat='server' Visible="false"  ForeColor="Red" Text='<br />Please complete year' />
                        <asp:Label ID='Labelinitialfol' runat='server' Visible="false"  
                            ForeColor="Red" Text='<br />' />
                               <asp:Label ID='Labelfollowexam' runat='server' Visible="false"  
                            ForeColor="Red" Text='Date cannot be after todays date<br />' />
                    </td>
                    <td>
                        <asp:DropDownList ID='DropDownListMonthOfFollowUp' DataValueField='MonthID' DataTextField='MonthName' runat='server' />
                        <asp:DropDownList ID='DropDownListYearOfFollowUp' DataValueField='YearID' DataTextField='YearName' runat='server' />
                    </td>
                </tr>
                <!-- Question 4 -->
                <tr class="table_body_bg">
                    <td>
                        <strong>4. Gender</strong>
                        <asp:Label ID='LabelRBGender' runat='server' Visible="false"  ForeColor="Red" Text='<br />Please Complete' />
                    </td>
                    <td>
                        <asp:RadioButtonList id='RadioButtonListRBGender' RepeatDirection='Horizontal' CssClass='aspxList' runat='server'>
                            <asp:ListItem Value='42'>&nbsp;Male</asp:ListItem>
                            <asp:ListItem Value='43'>&nbsp;Female</asp:ListItem>
                        </asp:RadioButtonList>
                    </td>
                </tr>
                <!-- Question 5 -->
                <tr class="table_body">
                    <td>
                        <strong>5. Which eye has exudative AMD?</strong>&nbsp;<a href="#"><img id="QH5" src="../../common/images/tip.gif" alt="Tip" /></a>
                        <asp:Label ID='LabelRBEyeWithExudativeAMD' runat='server'  Visible="false"  ForeColor="Red" Text='<br />Please complete'></asp:Label>    
                    </td>
                    <td>
                        <asp:RadioButtonList id='RadioButtonListRBEyeWithExudativeAMD' RepeatDirection='Horizontal' CssClass='aspxList' runat='server'>
                            <asp:ListItem Value='49'>&nbsp;OD</asp:ListItem>
                            <asp:ListItem Value='50'>&nbsp;OS</asp:ListItem>
                        </asp:RadioButtonList>
                    </td>
                </tr>
                <!-- Question 6 -->
                <tr class="table_body_bg">
                    <td>
                        <strong>6. What is the patient&rsquo;s smoking status?</strong>
                        <asp:Label ID='LabelRBSmokingStatus' runat='server'  Visible="false"  ForeColor="Red" Text='<br />Please complete'></asp:Label>
                    </td>
                    <td>
                        <asp:RadioButtonList id='RadioButtonListRBSmokingStatus' RepeatDirection='Vertical' CssClass='aspxList' runat='server'>
                            <asp:ListItem Value='91'>&nbsp;Currently smokes</asp:ListItem>
                            <asp:ListItem Value='135'>&nbsp;Quit &lt 3 years ago</asp:ListItem>
                            <asp:ListItem Value='136'>&nbsp;Quit &ge;3 years ago</asp:ListItem>
                            <asp:ListItem Value='93'>&nbsp;Never smoked</asp:ListItem>
                            <asp:ListItem Value='3'>&nbsp;Not recorded</asp:ListItem>
                        </asp:RadioButtonList>
                    </td>
                </tr>
                <!-- Question 7 -->
                <tr class="table_body">
                    <td>
                        <strong>7. Are patient&rsquo;s drug reactions and allergies (or the lack of) documented?</strong>
                        <asp:Label ID='LabelDrugReactions' runat='server'  Visible="false"  ForeColor="Red" Text='<br />Please complete'></asp:Label>
                    </td>
                    <td>
                        <asp:RadioButton runat='server' ID='RadioButtonDrugReactionsYes' GroupName='DrugReactions ' text='&nbsp;Yes' />&nbsp;&nbsp;
                        <asp:RadioButton runat='server' ID='RadioButtonDrugReactionsNo' GroupName='DrugReactions ' text='&nbsp;No' />
                    </td>
                </tr>
            </table>
            <br />
            <!-- Examination and Diagnostic Procedures -->
            <table>
                <tr>
                    <th colspan="3" width="910px"><p>Examination and Diagnostic Procedures</p></th>
                </tr>
                <!-- Question 1 -->
                <tr class="table_body">
                    <td width="50%" rowspan="2">
                        <strong>1. Best corrected visual acuity</strong>&nbsp;<a href="#"><img id="QE1" src="../../common/images/tip.gif" alt="Tip" /></a><br />
                        <asp:Label ID='LabelBCVAInvolvedEye' runat='server'  Visible="false"  ForeColor="Red" Text='Please complete involved eye'></asp:Label><br />
                        <asp:Label ID='LabelBCVAFellowEye' runat='server'  Visible="false"  ForeColor="Red" Text='Please complete fellow eye'></asp:Label>
                    </td>
                    <td width="20%"><span class="right">Involved eye:</span></td>
                    <td>20 / <asp:DropDownList ID="DropDownListBCVAInvolvedEye" DataValueField="examValue" DataTextField="examLabel" runat="server" onchange="toggleQO1ODCB();" /></td>
                </tr>
                <tr class="table_body">
                    <td><span class="right">Fellow eye:</span></td>
                    <td>20 / <asp:DropDownList ID="DropDownListBCVAFellowEye" DataValueField="examValue" DataTextField="examLabel" runat="server" onchange="toggleQO1ODCB();" /></td>
                </tr>
                <!-- Question 2 -->
                <tr class="table_body_bg">
                    <td colspan="2">
                        <strong>2. What is the status of AMD in the fellow eye?</strong>
                        <asp:Label ID='LabelRBExamAMDFellowEye' runat='server'  Visible="false"  ForeColor="Red" Text='<br />Please complete'></asp:Label>
                    </td>
                    <td>
                        <asp:RadioButtonList id='RadioButtonListRBExamAMDFellowEye' RepeatDirection='Vertical' CssClass='aspxList' runat='server'>
                            <asp:ListItem Value='137'>&nbsp;No or early AMD</asp:ListItem>
                            <asp:ListItem Value='138'>&nbsp;Intermediate AMD</asp:ListItem>
                            <asp:ListItem Value='139'>&nbsp;Advanced AMD</asp:ListItem>
                            <asp:ListItem Value='3'>&nbsp;Not recorded</asp:ListItem>
                        </asp:RadioButtonList>
                    </td>
                </tr>
                <!-- Question 3 -->
                <tr class="table_body">
                    <td colspan="2">
                        <strong>3. Was fluorescein angiography performed?</strong>
                        <asp:Label ID='LabelExamFluorescein' runat='server'  Visible="false"  ForeColor="Red" Text='<br />Please complete'></asp:Label>
                    </td>
                    <td>
                        <asp:RadioButton runat='server' ID='RadioButtonExamFluoresceinYes' GroupName='ExamFluorescein ' text='&nbsp;Yes' />&nbsp;&nbsp;
                        <asp:RadioButton runat='server' ID='RadioButtonExamFluoresceinNo' GroupName='ExamFluorescein ' text='&nbsp;No' />
                    </td>
                </tr>
                <!-- Question 4 -->
                <tr class="table_body_bg">
                    <td colspan="2">
                        <strong>4. Was OCT performed?</strong>
                        <asp:Label ID='LabelExamOCT' runat='server'  Visible="false"  ForeColor="Red" Text='<br />Please complete'></asp:Label>
                    </td>
                    <td>
                        <asp:RadioButton runat='server' ID='RadioButtonExamOCTYes' GroupName='ExamOCT ' text='&nbsp;Yes' />&nbsp;&nbsp;
                        <asp:RadioButton runat='server' ID='RadioButtonExamOCTNo' GroupName='ExamOCT ' text='&nbsp;No' />
                    </td>
                </tr>
                <!-- Question 4a -->
               
                <!-- Question 5 -->
                <tr class="table_body">
                    <td colspan="2">
                        <strong>5. Was ICG angiography performed?</strong>
                        <asp:Label ID='LabelExamICG' runat='server'  Visible="false"  ForeColor="Red" Text='<br />Please complete'></asp:Label>
                    </td>
                    <td>
                        <asp:RadioButton runat='server' ID='RadioButtonExamICGYes' GroupName='ExamICG ' text='&nbsp;Yes' />
                        <asp:RadioButton runat='server' ID='RadioButtonExamICGNo' GroupName='ExamICG ' text='&nbsp;No' />
                    </td>
                </tr>
            </table>
            <br />
            <!-- Assessment and Management (Current Diagnosis) -->
            <table>
                <tr>
                    <th colspan="2" width="910px"><p>Assessment and Management (Current Diagnosis)</p></th>
                </tr>
                <!-- Question 1 -->
                <tr class="table_body">
                    <td width="70%">
                        <strong>1. What was the management for the eye at this visit (the therapy may have been performed on a subsequent day)?</strong>
                        <br />(Mark more than one choice if appropriate)    
                    </td>
                    <td>
                        <asp:CheckBox runat='server' ID='CheckBoxMgntBevacizumab' text='&nbsp;Bevacizumab' />
                        <br /><asp:CheckBox runat='server' ID='CheckBoxMgntRanibizumab' text='&nbsp;Ranibizumab' />
                        <br /><asp:CheckBox runat='server' ID='CheckBoxMgntAflibercept' text='&nbsp;Aflibercept' />
                        <br /><asp:CheckBox runat='server' ID='CheckBoxMgntIntravitrealSteroids' text='&nbsp;Intravitreal steroids' />
                        <br /><asp:CheckBox runat='server' ID='CheckBoxMgntFocalLaser' text='&nbsp;Focal laser' />
                        <br /><asp:CheckBox runat='server' ID='CheckBoxMgntObservation' text='&nbsp;Observation' />
                        <br /><asp:CheckBox runat='server' ID='CheckBoxMgntPDT' text='&nbsp;PDT' />
                        <br /><asp:CheckBox runat='server' ID='CheckBoxMgntOther' text='&nbsp;Other' />
                        <br /><asp:CheckBox runat='server' ID='CheckBoxMgntNR' text='&nbsp;Not recorded' />
                    </td>
                </tr>
                <!-- Question 1a -->
                <tr class="table_body">
                    <td width="70%">
                        <span id="QA1aA"><strong>1a. If observation was the initial management, why was this management chosen?</strong></span>
                    </td>
                    <td>
                        <span id="QA1aB">
                        <asp:CheckBox runat='server' ID='CheckBoxObservationReasonPatRef' text='&nbsp;Patient refused treatment' />
                        <br /><asp:CheckBox runat='server' ID='CheckBoxObservationReasonRisksOutweigh' text='&nbsp;Risks of treatment outweighed the benefits' />
                        <br /><asp:CheckBox runat='server' ID='CheckBoxObservationReasonUnlikelyToImprove' text='&nbsp;Treatment was deemed to be unlikely to improve visual acuity or quality of life' />
                        </span>
                    </td>
                </tr>
                <!-- Question 2 -->
                <tr class="table_body_bg">
                    <td>
                        <strong>2. Treatment plan for exudative AMD involved evaluation/treatment at which of the following intervals?</strong>
                        <br />(select one)
                        <asp:Label ID='LabelRBInterval' runat='server'  Visible="false"  ForeColor="Red" Text='<br />Please complete'></asp:Label>
                    </td>
                    <td>
                        <asp:RadioButtonList id='RadioButtonListRBInterval' RepeatDirection='Vertical' CssClass='aspxList' runat='server'>
                            <asp:ListItem Value='140'>&nbsp;Monthly</asp:ListItem>
                            <asp:ListItem Value='141'>&nbsp;Monthly until dry, then followed monthly with  OCT with treatment prn</asp:ListItem>
                            <asp:ListItem Value='142'>&nbsp;Monthly until dry, then prn (without monthly OCT)</asp:ListItem>
                            <asp:ListItem Value='143'>&nbsp;Treat and extend</asp:ListItem>
                            <asp:ListItem Value='3'>&nbsp;Not recorded</asp:ListItem>
                            <asp:ListItem Value='14'>&nbsp;Other</asp:ListItem>
                        </asp:RadioButtonList>
                    </td>
                </tr>
                <!-- Question 3 -->
                <tr class="table_body">
                    <td>
                        <strong>3. Was the patient counseled on the use of antioxidant and zinc use, as appropriate for smoking status</strong>
                        <br />(or had they been discussed with the patient within the past 12 months)?
                        <asp:Label ID='LabelRBPatientCounseledAntioxidant' runat='server'  Visible="false"  ForeColor="Red" Text='<br />Please complete'></asp:Label>
                    </td>
                    <td>
                        <asp:RadioButtonList id='RadioButtonListRBPatientCounseledAntioxidant' RepeatDirection='Vertical' CssClass='aspxList' runat='server'>
                            <asp:ListItem Value='1'>&nbsp;Yes</asp:ListItem>
                            <asp:ListItem Value='144'>&nbsp;No: Not indicated per AREDS</asp:ListItem>
                            <asp:ListItem Value='14'>&nbsp;No: Other</asp:ListItem>
                            <asp:ListItem Value='3'>&nbsp;Not recorded</asp:ListItem>
                        </asp:RadioButtonList>
                    </td>
                </tr>
            </table>
            <br />
            <!-- Outcomes -->
            <table>
                <tr>
                    <th colspan="3" width="910px"><p>Outcomes</p></th>
                </tr>
                <!-- Question 1 -->
                <tr class="table_body">
                    <td width="50%" rowspan="2">
                        <strong>1. Best corrected visual acuity</strong>&nbsp;<a href="#"><img id="QO1" src="../../common/images/tip.gif" alt="Tip" /></a>
                        <asp:Label ID='LabelOutcomeTreatedBCVA' runat='server'  Visible="false"  ForeColor="Red" Text='<br />Please complete'></asp:Label>
                        <asp:Label ID='LabelOutcomeFellowBCVA' runat='server'  Visible="false"  ForeColor="Red" Text='<br />Please complete'></asp:Label>
                    </td>
                    <td width="20%"><span class="right">Treated eye:</span></td>
                    <td>
                        20 / <asp:DropDownList ID='DropDownListOutcomeTreatedBCVA' DataValueField='examValue' DataTextField='examLabel' runat='server' />                        
                    </td>
                </tr>
                <tr class="table_body">
                    <td><span class="right">Fellow eye:</span></td>
                    <td>
                        20 / <asp:DropDownList ID='DropDownListOutcomeFellowBCVA' DataValueField='examValue' DataTextField='examLabel' runat='server' />
                    </td>
                </tr>
                <!-- Question 2 -->
                <tr class="table_body_bg">
                    <td colspan="2">
                        <strong>2.	Were any therapies required over the past year for management of the exudative AMD?</strong>
                        <br />(Please check all that are appropriate, and indicate the number of times each therapy was given over the year. <em>Do not</em> include the treatment at the initial exam)
                    </td>
                    <td>
                        <table class="aspxList">
                            <tr>
                                <td><asp:CheckBox runat='server' ID='CheckBoxTherapyBevacizumab' text='&nbsp;Bevacizumab' /></td>
                                <td>#&nbsp;<asp:TextBox runat='server' ID='TextBoxTherapyBevacizumabNbr' size='5' Width="20px" /></td>
                            </tr>
                            <tr>
                                <td><asp:CheckBox runat='server' ID='CheckBoxTherapyRanibizumab' text='&nbsp;Ranibizumab' /></td>
                                <td>#&nbsp;<asp:TextBox runat='server' ID='TextBoxTherapyRanibizumabNbr' size='5' Width="20px" /></td>
                            </tr>
                            <tr>
                                <td><asp:CheckBox runat='server' ID='CheckBoxTherapyAflibercept' text='&nbsp;Aflibercept' /></td>
                                <td>#&nbsp;<asp:TextBox runat='server' ID='TextBoxTherapyAfliberceptNbr' size='5' Width="20px" /></td>
                            </tr>
                            <tr>
                                <td><asp:CheckBox runat='server' ID='CheckBoxTherapyPDT' text='&nbsp;PDT' /></td>
                                <td>#&nbsp;<asp:TextBox runat='server' ID='TextBoxTherapyPDTNbr' size='5' Width="20px" /></td>
                            </tr>
                            <tr>
                                <td><asp:CheckBox runat='server' ID='CheckBoxTherapyIntravitrealSteroids' text='&nbsp;Intravit. steroids' /></td>
                                <td>#&nbsp;<asp:TextBox runat='server' ID='TextBoxTherapyIntravitrealSteroidsNbr' size='5' Width="20px" /></td>
                            </tr>
                            <tr>
                                <td><asp:CheckBox runat='server' ID='CheckBoxTherapyMPSLaser' text='&nbsp;MPS style focal laser' /></td>
                                <td>#&nbsp;<asp:TextBox runat='server' ID='TextBoxTherapyMPSLaserNbr' size='5' Width="20px" /></td>
                            </tr>
                            <tr>
                                <td><asp:CheckBox runat='server' ID='CheckBoxTherapyICGLaser' text='&nbsp;ICG-guided focal laser' /></td>
                                <td>#&nbsp;<asp:TextBox runat='server' ID='TextBoxTherapyICGLaserNbr' size='5' Width="20px" /></td>
                            </tr>
                            <tr>
                                <td>
                                    <asp:CheckBox runat='server' ID='CheckBoxTherapyOther' text='&nbsp;Other&nbsp;' />
                                    <asp:TextBox runat='server' ID='TextBoxTherapyOtherText' size='15' />            
                                </td>
                                <td>#&nbsp;<asp:TextBox runat='server' ID='TextBoxTherapyOtherNbr' size='5' Width="20px" /></td>
                            </tr>
                        </table>
                    </td>
                </tr>
                <!-- Question 3 -->
                <tr class="table_body">
                    <td colspan="2"><strong>3. Status of neovascular process in treated eye:</strong>&nbsp;<a href="#"><img id="QO3" src="../../common/images/tip.gif" alt="Tip" /></a></td>
                    <td>
                        <asp:CheckBox runat='server' ID='CheckBoxOutcomeTreatedEyeCME' text='&nbsp;CME' />
                        <br /><asp:CheckBox runat='server' ID='CheckBoxOutcomeTreatedEyeDryScar' text='&nbsp;Dry scar' />
                        <br /><asp:CheckBox runat='server' ID='CheckBoxOutcomeTreatedEyeHemorrhage' text='&nbsp;Hemorrhage' />
                        <br /><asp:CheckBox runat='server' ID='CheckBoxOutcomeTreatedEyeLipid' text='&nbsp;Lipid' />
                        <br /><asp:CheckBox runat='server' ID='CheckBoxOutcomeTreatedEyeSRF' text='&nbsp;SRF' />
                        <br /><asp:CheckBox runat='server' ID='CheckBoxOutcomeTreatedEyeSubRPE' text='&nbsp;Sub-RPE fluid' />
                        <br /><asp:CheckBox runat='server' ID='CheckBoxOutcomeTreatedEyeNR' text='&nbsp;Not recorded' />
                        <br /><asp:CheckBox runat='server' ID='CheckBoxOutcomeTreatedEyeOther' text='&nbsp;Other' />
                    </td>
                </tr>
                <!-- Question 3a -->
                <tr class="table_body">
                    <td colspan="2"><strong>3a. If&nbsp; &quot;Other,&quot;&nbsp; please specify:</strong></td>
                    <td>
                        <asp:TextBox runat='server' ID='TextBoxOutcomeTreatedEyeOtherText' size='25' />
                    </td>
                </tr>
            </table>
            <div class="button-box">
                <asp:LinkButton ID="LinkButtonBackToDashboard" runat="server" Text="Back To Chart Registration" PostBackUrl="../PatientChartRegistration.aspx?CycleNumber=1" Visible="false" CssClass="button" />
                <asp:Button ID="ButtonSubmit" OnClientClick="return ButtonSubmit_PQRS();" OnClick="ButtonSubmit_Click" runat="server" Text="Submit Chart" CssClass="button" />
            </div>
        </div>
        <!-- ION pim ends -->
    <script>
        //      Either enables or disables input boxes
        function enabledControl(el, disabled, checked) {
            //alert("Hello world from enabled control");
            try {
                if (disabled) {
                    el.disabled = disabled;
                    el.setAttribute("disabled", true);
                }
                else {
                    el.disabled = disabled;
                    el.removeAttribute("disabled");
                }

                if (checked == true)
                    el.checked = false;
            }
            catch (E) {
            }
            if (el.childNodes && el.childNodes.length > 0) {
                for (var x = 0; x < el.childNodes.length; x++) {
                    enabledControl(el.childNodes[x], disabled, checked);
                }
            }
        }
        //      Either enables or disables dropdown boxes
        function enabledControlDropDown(el, disabled, checked) {
            //alert("Hello world from enabled control");
            try {
                if (disabled) {
                    el.disabled = disabled;
                    el.setAttribute("disabled", true);
                }
                else {
                    el.disabled = disabled;
                    el.removeAttribute("disabled");
                }

                if (checked == true)
                    el.options[0].selected = true;
            }
            catch (E) {
            }
            if (el.childNodes && el.childNodes.length > 0) {
                for (var x = 0; x < el.childNodes.length; x++) {
                    enabledControl(el.childNodes[x], disabled, checked);
                }
            }
        }
        //      Either enables or disables text boxes
        function enabledControlTextField(el, disabled, checked) {
            //alert("Hello world from enabled control");
            try {
                if (disabled) {
                    el.disabled = disabled;
                    el.setAttribute("disabled", true);
                }
                else {
                    el.disabled = disabled;
                    el.removeAttribute("disabled");
                }

                if (checked == true) {
                    el.value = '';
                }
            }
            catch (E) {
            }
            if (el.childNodes && el.childNodes.length > 0) {
                for (var x = 0; x < el.childNodes.length; x++) {
                    enabledControl(el.childNodes[x], disabled, checked);
                }
            }
        }

        function generate(arr1, arr2, istrue) {
            if (istrue == true) {
                for (var i = 0; i < arr1.length; i++) {
                    document.getElementById(arr1[i]).style.color = "gray";
                }
                for (var i = 0; i < arr2.length; i++) {
                    $(arr2[i] + ' :input').attr('disabled', true);
                    $(arr2[i] + ' :input').removeAttr("checked");
                }
            }
            if (istrue == false) {
                for (var i = 0; i < arr1.length; i++) {
                    document.getElementById(arr1[i]).style.color = "#333";
                }
                for (var i = 0; i < arr2.length; i++) {
                    $(arr2[i] + ' :input').removeAttr('disabled');
                }
            }
        }

    </script>
    <script>
        $(document).ready(function () {
            $("#<%= CheckBoxTherapyOther.ClientID %>").click(function () {
                if ($('#<%= CheckBoxTherapyOther.ClientID %>').prop('checked') == true) {
                    enabledControlTextField(document.getElementById("<%= TextBoxTherapyOtherText.ClientID %>"), false, false);
                }
                else {
                    enabledControlTextField(document.getElementById("<%= TextBoxTherapyOtherText.ClientID %>"), true, true);
                }
            });

            if ($('#<%= CheckBoxTherapyOther.ClientID %>').prop('checked') == true) {
                enabledControlTextField(document.getElementById("<%= TextBoxTherapyOtherText.ClientID %>"), false, false);
            }
            else {
                enabledControlTextField(document.getElementById("<%= TextBoxTherapyOtherText.ClientID %>"), true, true);
            }

            $("#<%= CheckBoxOutcomeTreatedEyeOther.ClientID %>").click(function () {
                if ($('#<%= CheckBoxOutcomeTreatedEyeOther.ClientID %>').prop('checked') == true) {
                    enabledControlTextField(document.getElementById("<%= TextBoxOutcomeTreatedEyeOtherText.ClientID %>"), false, false);
                }
                else {
                    enabledControlTextField(document.getElementById("<%= TextBoxOutcomeTreatedEyeOtherText.ClientID %>"), true, true);
                }
            });

            if ($('#<%= CheckBoxOutcomeTreatedEyeOther.ClientID %>').prop('checked') == true) {
                enabledControlTextField(document.getElementById("<%= TextBoxOutcomeTreatedEyeOtherText.ClientID %>"), false, false);
            }
            else {
                enabledControlTextField(document.getElementById("<%= TextBoxOutcomeTreatedEyeOtherText.ClientID %>"), true, true);
            }

            $("#<%= CheckBoxMgntObservation.ClientID %>").click(function () {
                if ($('#<%= CheckBoxMgntObservation.ClientID %>').prop('checked') == true) {
                    var myaray = new Array("QA1aA", "QA1aB");
                    var myarray2 = new Array('#QA1aB');
                    generate(myaray, myarray2, false);
                }
                else {
                    var myaray = new Array("QA1aA", "QA1aB");
                    var myarray2 = new Array('#QA1aB');
                    generate(myaray, myarray2, true);
                }
            });
            if ($('#<%= CheckBoxMgntObservation.ClientID %>').prop('checked') == true) {
                var myaray = new Array("QA1aA", "QA1aB");
                var myarray2 = new Array('#QA1aB');
                generate(myaray, myarray2, false);
            }
            else {
                var myaray = new Array("QA1aA", "QA1aB");
                var myarray2 = new Array('#QA1aB');
                generate(myaray, myarray2, true);
            }

        });
    </script>
    <script>
        function ButtonSubmit_PQRS() {
            var pqrs = ("<%= (PQRSEnabled && PQRSUsed) %>" == "True");
            if (pqrs) {
                var year = $("#<%= DropDownListPQRSVisitYear.ClientID %>").val();
                var month = $("#<%= DropDownListPQRSVisitMonth.ClientID %>").val();
                if (year == 0 || month == 0)
                    return confirm("If there is no PQRS visit date selected, all of your PQRS data will not be saved, is that okay?");
                if (year != 2015)
                    return confirm("If the PQRS visit date selected is not 2015, all of your PQRS data will not be saved, is that okay?");
            }
            return true;
        }

        $(function () {
            $('#QH5').aToolTip({
                clickIt: true,
                tipContent: 'If both eyes have exudative AMD, designate one for this Improvement in Medical Practice Activity,<br />preferably the one with the more recent onset of symptoms.<br />The other eye will be called the fellow eye.'
            });
        });

        $(function () {
            $('#QE1').aToolTip({
                clickIt: true,
                tipContent: '20/800 or count fingers @ 5 ft<br />20/1000 or count fingers @ 4 ft<br />20/1600 or count fingers @ 3ft<br />20/2000 or count fingers @ 2 ft<br />20/4000 or count fingers @ 1 ft<br />20/7777 =CF<br />20/8888 =HM<br />20/9999 =LP<br />20/0000 =NLP'
            });
        });

        $(function () {
            $('#QO1').aToolTip({
                clickIt: true,
                tipContent: '20/800 or count fingers @ 5 ft<br />20/1000 or count fingers @ 4 ft<br />20/1600 or count fingers @ 3ft<br />20/2000 or count fingers @ 2 ft<br />20/4000 or count fingers @ 1 ft<br />20/7777 =CF<br />20/8888 =HM<br />20/9999 =LP<br />20/0000 =NLP'
            });
        });

        $(function () {
            $('#QO3').aToolTip({
                clickIt: true,
                tipContent: 'Based on clinical exam, and any additional testing.  Mark all that apply'
            });
        });
    </script>
    
</asp:Content>