﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Transactions;
using NetHealthPIMModel;

public partial class abo_TOChart : BasePage
{
    protected void Page_Load(object sender, EventArgs e)
    
   {
        if (!IsPostBack)
        {
            InitializePage();
        }


    }
    protected void InitializePage()
    {
        ((PIMMasterPage)Page.Master).SetTitle("Chart");

        using (NetHealthPIMEntities pim = new NetHealthPIMEntities())
        {
            int ModuleID = (int)Session[Constants.SESSION_WORKINGMODULEID];
            Module module = pim.Module.First(c => c.ModuleID == ModuleID);
            ((PIMMasterPage)Page.Master).SetHeader(module.ModuleName + " Chart Abstraction");

            ParticipantModuleCycle prev = (ParticipantModuleCycle)Session[Constants.SESSION_PREVIOUSCYCLE];
            String CycleNumber = Request.QueryString["CycleNumber"];

            int cycleID = ctx.ActiveModuleCycleID;
            if (CycleNumber == "1")
            {
                cycleID = prev.ParticipantModuleCycleID;
                LinkButtonBackToDashboard.Visible = true;
                ButtonSubmit.Visible = false;
            }

            ParticipantModuleCycle cycle = pim.ParticipantModuleCycle.First(c => c.ParticipantModuleCycleID == cycleID);
            String numberOfRecord = Request.QueryString["nr"];
            String totalRecords = Request.QueryString["t"];
            if (cycle.CycleNumber == 1)
            {
                ((PIMMasterPage)Page.Master).SetStatus(2);
                LiteralAbstractionNumber.Text = "Initial Abstraction: Chart " + numberOfRecord + " of " + totalRecords;
            }
            else
            {
                ((PIMMasterPage)Page.Master).SetStatus(3);
                LiteralAbstractionNumber.Text = "Second Abstraction: Chart " + numberOfRecord + " of " + totalRecords;
            }
            DropDownListMonthOfBirth.DataSource = CycleManager.getMonthList();
            DropDownListYearOfBirth.DataSource = CycleManager.getDOBYearList(0, 100);





            DataBind();
            DropDownListYearOfBirth.Items.Insert(0, new ListItem("Year", "0"));

            string recordIdentifier = Request.QueryString["ri"];
            HiddenFieldRecordIdentifier.Value = recordIdentifier;
            LiteralRecordIdentifier.Text = recordIdentifier;
            var count = pim.ChartAbstractionThyroid.Where(ps => ps.ParticipantModuleCycle.ParticipantModuleCycleID == cycleID & ps.RecordIdentifier == recordIdentifier).Count();
            if (count == 0)
            {
                using (TransactionScope transaction = new TransactionScope())
                {
                    //Module module = pim.Module.First(m => m.ModuleID == Constants.ABO_AMBLYOPIA_MODULEID);

                    NetHealthPIMModel.ChartRegistration chartRegistration = (from cr in pim.ChartRegistration
                                                           where cr.ParticipantModuleCycleID == cycleID
                                                           & cr.RecordIdentifier == recordIdentifier
                                                           & cr.ModuleID == 37
                                                                             select cr).First<NetHealthPIMModel.ChartRegistration>();

                    DateTime initialVisit = Convert.ToDateTime(chartRegistration.InitialVisit);
                    DateTime DOB = Convert.ToDateTime(chartRegistration.DOB);


                    ChartAbstractionThyroid abstractRecord = new ChartAbstractionThyroid();
                    abstractRecord.ParticipantModuleCycle = cycle;
                    abstractRecord.RecordIdentifier = recordIdentifier;
                    abstractRecord.MonthOfBirth = DOB.Month;
                    abstractRecord.YearOfBirth = DOB.Year;
                    DropDownListYearOfBirth.SelectedValue = abstractRecord.YearOfBirth.ToString();
                    DropDownListMonthOfBirth.SelectedValue = abstractRecord.MonthOfBirth.ToString();

                    abstractRecord.CreatedOn = DateTime.Now;
                    abstractRecord.LastUpdateDate = DateTime.Now;
                    pim.SaveChanges();

                    transaction.Complete();
                }
            }
            else
            {

                ChartAbstractionThyroid abstractRecord = (from c in pim.ChartAbstractionThyroid
                                                          where c.ParticipantModuleCycle.ParticipantModuleCycleID == cycleID & c.RecordIdentifier == recordIdentifier
                                                          select c).First<ChartAbstractionThyroid>();




                if (abstractRecord.HistorySysHyperThyroid != null) CheckBoxHistorySysHyperThyroid.Checked = ((bool)abstractRecord.HistorySysHyperThyroid);
                if (abstractRecord.HistorySysHypoThyroid != null) CheckBoxHistorySysHypoThyroid.Checked = ((bool)abstractRecord.HistorySysHypoThyroid);
                if (abstractRecord.HistoryHashimoto != null) CheckBoxHistoryHashimoto.Checked = ((bool)abstractRecord.HistoryHashimoto);
                if (abstractRecord.HistoryEuthyroid != null) CheckBoxHistoryEuthyroid.Checked = ((bool)abstractRecord.HistoryEuthyroid);
                if (abstractRecord.HistoryND != null) CheckBoxHistoryND.Checked = ((bool)abstractRecord.HistoryND);


                if (abstractRecord.RBExophthalmometry != null) RadioButtonListRBExophthalmometry.SelectedValue = abstractRecord.RBExophthalmometry.ToString();
                if (abstractRecord.RBGender != null) RadioButtonListRBGender.SelectedValue = abstractRecord.RBGender.ToString();
                if (abstractRecord.RBSmoking != null) RadioButtonListRBSmoking.SelectedValue = abstractRecord.RBSmoking.ToString();
                if (abstractRecord.RBAfferentPapillaryDefect != null) RadioButtonListRBAfferentPapillaryDefect.SelectedValue = abstractRecord.RBAfferentPapillaryDefect.ToString();
                if (abstractRecord.RBMotilityRestricted != null) RadioButtonListRBMotilityRestricted.SelectedValue = abstractRecord.RBMotilityRestricted.ToString();
                if (abstractRecord.RBUpperLidRetraction != null) RadioButtonListRBUpperLidRetraction.SelectedValue = abstractRecord.RBUpperLidRetraction.ToString();
                if (abstractRecord.BCVA != null) RadioButtonListBCVA.SelectedValue = abstractRecord.BCVA.ToString();
                if (abstractRecord.RBKeratopathy != null) RadioButtonListRBKeratopathy.SelectedValue = abstractRecord.RBKeratopathy.ToString();
                if (abstractRecord.RBIOPElevated != null) RadioButtonListRBIOPElevated.SelectedValue = abstractRecord.RBIOPElevated.ToString();
                if (abstractRecord.RBOpticNerveAbnormalities != null) RadioButtonListRBOpticNerveAbnormalities.SelectedValue = abstractRecord.RBOpticNerveAbnormalities.ToString();
                if (abstractRecord.RBSmokingDiscussion != null) RadioButtonListRBSmokingDiscussion.SelectedValue = abstractRecord.RBSmokingDiscussion.ToString();
                if (abstractRecord.RBThyroidFunctionTest != null) RadioButtonListRBThyroidFunctionTest.SelectedValue = abstractRecord.RBThyroidFunctionTest.ToString();
                if (abstractRecord.RBOcularLubrication != null) RadioButtonListRBOcularLubrication.SelectedValue = abstractRecord.RBOcularLubrication.ToString();
                if (abstractRecord.RBDurationStability != null) RadioButtonListRBDurationStability.SelectedValue = abstractRecord.RBDurationStability.ToString();
                if (abstractRecord.RBStopSmoking != null) RadioButtonListRBStopSmoking.SelectedValue = abstractRecord.RBStopSmoking.ToString();
                if (abstractRecord.RBOcularComfortImproved != null) RadioButtonListRBOcularComfortImproved.SelectedValue = abstractRecord.RBOcularComfortImproved.ToString();
                if (abstractRecord.RBDiplopiaImproved != null) RadioButtonListRBDiplopiaImproved.SelectedValue = abstractRecord.RBDiplopiaImproved.ToString();
                if (abstractRecord.RBQualityVision != null) RadioButtonListRBQualityVision.SelectedValue = abstractRecord.RBQualityVision.ToString();



                if (abstractRecord.MonthOfBirth != null) DropDownListMonthOfBirth.SelectedValue = abstractRecord.MonthOfBirth.ToString();
                if (abstractRecord.YearOfBirth != null) DropDownListYearOfBirth.SelectedValue = abstractRecord.YearOfBirth.ToString();

                if (abstractRecord.ChemosisInjection != null)
                {
                    RadioButtonChemosisInjectionYes.Checked = ((bool)abstractRecord.ChemosisInjection);
                    RadioButtonChemosisInjectionNo.Checked = !((bool)abstractRecord.ChemosisInjection);
                }

                if (abstractRecord.SymptomDryness != null)
                {
                    RadioButtonRadioButtonSymptomDryness.SelectedValue = abstractRecord.SymptomDryness.ToString();
                }
                if (abstractRecord.SymptomDoubleVision != null)
                {
                    RadioButtonListSymptomDoubleVision.SelectedValue = abstractRecord.SymptomDoubleVision.ToString();
                }
           
                if (abstractRecord.SymptomStability != null)
                {
                    RadioButtonSymptomStabilityYes.Checked = ((bool)abstractRecord.SymptomStability);
                    RadioButtonSymptomStabilityNo.Checked = !((bool)abstractRecord.SymptomStability);
                }
                if (abstractRecord.HistoryMostRecent != null)
                {
                    RadioButtonHistoryMostRecentYes.Checked = ((bool)abstractRecord.HistoryMostRecent);
                    RadioButtonHistoryMostRecentNo.Checked = !((bool)abstractRecord.HistoryMostRecent);
                }
                //if (abstractRecord.BCVA != null)
                //{
                //    RadioButtonBCVAYes.Checked = ((bool)abstractRecord.BCVA);
                //    RadioButtonBCVANo.Checked = !((bool)abstractRecord.BCVA);
                //}
                if (abstractRecord.ColorVisionVisualFields != null)
                {
                    RadioButtonColorVisionVisualFieldsYes.Checked = ((bool)abstractRecord.ColorVisionVisualFields);
                    RadioButtonColorVisionVisualFieldsNo.Checked = !((bool)abstractRecord.ColorVisionVisualFields);
                }



                if (abstractRecord.LidSwellingPresent != null)
                {
                    RadioButtonLidSwellingPresentYes.Checked = ((bool)abstractRecord.LidSwellingPresent);
                    RadioButtonLidSwellingPresentNo.Checked = !((bool)abstractRecord.LidSwellingPresent);
                }
               




                if (abstractRecord.TreatmentSteroids != null)
                {
                    RadioButtonTreatmentSteroidsYes.Checked = ((bool)abstractRecord.TreatmentSteroids);
                    RadioButtonTreatmentSteroidsNo.Checked = !((bool)abstractRecord.TreatmentSteroids);
                }
                if (abstractRecord.OrbitalRadiation != null)
                {
                    RadioButtonOrbitalRadiationYes.Checked = ((bool)abstractRecord.OrbitalRadiation);
                    RadioButtonOrbitalRadiationNo.Checked = !((bool)abstractRecord.OrbitalRadiation);
                }
                if (abstractRecord.SurgicalDecompression != null)
                {
                    RadioButtonSurgicalDecompressionYes.Checked = ((bool)abstractRecord.SurgicalDecompression);
                    RadioButtonSurgicalDecompressionNo.Checked = !((bool)abstractRecord.SurgicalDecompression);
                }
                if (abstractRecord.StrabismusSurgery != null)
                {
                    RadioButtonStrabismusSurgeryYes.Checked = ((bool)abstractRecord.StrabismusSurgery);
                    RadioButtonStrabismusSurgeryNo.Checked = !((bool)abstractRecord.StrabismusSurgery);
                }
                if (abstractRecord.EyelidRetractionRepair != null)
                {
                    RadioButtonEyelidRetractionRepairYes.Checked = ((bool)abstractRecord.EyelidRetractionRepair);
                    RadioButtonEyelidRetractionRepairNo.Checked = !((bool)abstractRecord.EyelidRetractionRepair);
                }
                if (abstractRecord.ImprovedQualityLife != null)
                {
                    RadioButtonImprovedQualityLifeYes.Checked = ((bool)abstractRecord.ImprovedQualityLife);
                    RadioButtonImprovedQualityLifeNo.Checked = !((bool)abstractRecord.ImprovedQualityLife);
                }




            }




            }



            }

        public void savedata()
    {
        using (NetHealthPIMEntities pim = new NetHealthPIMEntities())
        {
            int ModuleID = (int)Session[Constants.SESSION_WORKINGMODULEID];
            int cycleID = ctx.ActiveModuleCycleID;
            ParticipantModuleCycle cycle = pim.ParticipantModuleCycle.First(c => c.ParticipantModuleCycleID == cycleID);
            string recordIdentifier = HiddenFieldRecordIdentifier.Value;
            using (TransactionScope transaction = new TransactionScope())
            {
                ChartAbstractionThyroid abstractRecord = (from c in pim.ChartAbstractionThyroid 
                                                         where c.ParticipantModuleCycle.ParticipantModuleCycleID == cycleID & c.RecordIdentifier == recordIdentifier
                                                          select c).First<ChartAbstractionThyroid>();



                abstractRecord.HistorySysHyperThyroid = CheckBoxHistorySysHyperThyroid.Checked;
                abstractRecord.HistorySysHypoThyroid = CheckBoxHistorySysHypoThyroid.Checked;
                abstractRecord.HistoryHashimoto = CheckBoxHistoryHashimoto.Checked;
                abstractRecord.HistoryEuthyroid = CheckBoxHistoryEuthyroid.Checked;
                abstractRecord.HistoryND = CheckBoxHistoryND.Checked;



                if (RadioButtonListRBGender.SelectedIndex != -1) abstractRecord.RBGender = Convert.ToInt32(RadioButtonListRBGender.SelectedValue);
                else abstractRecord.RBGender = 0;
                if (RadioButtonListRBSmoking.SelectedIndex != -1) abstractRecord.RBSmoking = Convert.ToInt32(RadioButtonListRBSmoking.SelectedValue);
                else abstractRecord.RBSmoking = 0;
                if (RadioButtonListRBAfferentPapillaryDefect.SelectedIndex != -1) abstractRecord.RBAfferentPapillaryDefect = Convert.ToInt32(RadioButtonListRBAfferentPapillaryDefect.SelectedValue);
                else abstractRecord.RBAfferentPapillaryDefect = 0;
                if (RadioButtonListRBMotilityRestricted.SelectedIndex != -1) abstractRecord.RBMotilityRestricted = Convert.ToInt32(RadioButtonListRBMotilityRestricted.SelectedValue);
                else abstractRecord.RBMotilityRestricted = 0;
                if (RadioButtonListRBUpperLidRetraction.SelectedIndex != -1) abstractRecord.RBUpperLidRetraction = Convert.ToInt32(RadioButtonListRBUpperLidRetraction.SelectedValue);
                else abstractRecord.RBUpperLidRetraction = 0;
                if (RadioButtonListBCVA.SelectedIndex != -1) abstractRecord.BCVA = Convert.ToInt32(RadioButtonListBCVA.SelectedValue);
                else abstractRecord.BCVA = 0;
                if (RadioButtonListRBKeratopathy.SelectedIndex != -1) abstractRecord.RBKeratopathy = Convert.ToInt32(RadioButtonListRBKeratopathy.SelectedValue);
                else abstractRecord.RBKeratopathy = 0;
                if (RadioButtonListRBIOPElevated.SelectedIndex != -1) abstractRecord.RBIOPElevated = Convert.ToInt32(RadioButtonListRBIOPElevated.SelectedValue);
                else abstractRecord.RBIOPElevated = 0;
                if (RadioButtonListRBOpticNerveAbnormalities.SelectedIndex != -1) abstractRecord.RBOpticNerveAbnormalities = Convert.ToInt32(RadioButtonListRBOpticNerveAbnormalities.SelectedValue);
                else abstractRecord.RBOpticNerveAbnormalities = 0;
                if (RadioButtonListRBSmokingDiscussion.SelectedIndex != -1) abstractRecord.RBSmokingDiscussion = Convert.ToInt32(RadioButtonListRBSmokingDiscussion.SelectedValue);
                else abstractRecord.RBSmokingDiscussion = 0;
                if (RadioButtonListRBThyroidFunctionTest.SelectedIndex != -1) abstractRecord.RBThyroidFunctionTest = Convert.ToInt32(RadioButtonListRBThyroidFunctionTest.SelectedValue);
                else abstractRecord.RBThyroidFunctionTest = 0;
                if (RadioButtonListRBOcularLubrication.SelectedIndex != -1) abstractRecord.RBOcularLubrication = Convert.ToInt32(RadioButtonListRBOcularLubrication.SelectedValue);
                else abstractRecord.RBOcularLubrication = 0;
                if (RadioButtonListRBDurationStability.SelectedIndex != -1) abstractRecord.RBDurationStability = Convert.ToInt32(RadioButtonListRBDurationStability.SelectedValue);
                else abstractRecord.RBDurationStability = 0;
                if (RadioButtonListRBStopSmoking.SelectedIndex != -1) abstractRecord.RBStopSmoking = Convert.ToInt32(RadioButtonListRBStopSmoking.SelectedValue);
                else abstractRecord.RBStopSmoking = 0;
                if (RadioButtonListRBOcularComfortImproved.SelectedIndex != -1) abstractRecord.RBOcularComfortImproved = Convert.ToInt32(RadioButtonListRBOcularComfortImproved.SelectedValue);
                else abstractRecord.RBOcularComfortImproved = 0;
                if (RadioButtonListRBDiplopiaImproved.SelectedIndex != -1) abstractRecord.RBDiplopiaImproved = Convert.ToInt32(RadioButtonListRBDiplopiaImproved.SelectedValue);
                else abstractRecord.RBDiplopiaImproved = 0;
                if (RadioButtonListRBQualityVision.SelectedIndex != -1) abstractRecord.RBQualityVision = Convert.ToInt32(RadioButtonListRBQualityVision.SelectedValue);
                else abstractRecord.RBQualityVision = 0;

                if (RadioButtonListRBExophthalmometry.SelectedIndex != -1) abstractRecord.RBExophthalmometry = Convert.ToInt32(RadioButtonListRBExophthalmometry.SelectedValue);
                else abstractRecord.RBExophthalmometry = 0;

                if (DropDownListMonthOfBirth.SelectedIndex > 0) abstractRecord.MonthOfBirth = Convert.ToInt32(DropDownListMonthOfBirth.SelectedValue);
                else abstractRecord.MonthOfBirth = 0;
                if (DropDownListYearOfBirth.SelectedIndex > 0) abstractRecord.YearOfBirth = Convert.ToInt32(DropDownListYearOfBirth.SelectedValue);
                else abstractRecord.YearOfBirth = 0;

                if (RadioButtonLidSwellingPresentYes.Checked) abstractRecord.LidSwellingPresent = true;
                if (RadioButtonLidSwellingPresentNo.Checked) abstractRecord.LidSwellingPresent = false;
                
                if (RadioButtonRadioButtonSymptomDryness.SelectedIndex != -1) abstractRecord.SymptomDryness = Convert.ToInt32(RadioButtonRadioButtonSymptomDryness.SelectedValue);
                else abstractRecord.SymptomDryness = 0;
                if (RadioButtonListSymptomDoubleVision.SelectedIndex != -1) abstractRecord.SymptomDoubleVision = Convert.ToInt32(RadioButtonListSymptomDoubleVision.SelectedValue);
                else abstractRecord.SymptomDoubleVision = 0;
              
                
                if (RadioButtonSymptomStabilityYes.Checked) abstractRecord.SymptomStability = true;
                if (RadioButtonSymptomStabilityNo.Checked) abstractRecord.SymptomStability = false;
                if (RadioButtonHistoryMostRecentYes.Checked) abstractRecord.HistoryMostRecent = true;
                if (RadioButtonHistoryMostRecentNo.Checked) abstractRecord.HistoryMostRecent = false;
                if (RadioButtonChemosisInjectionYes.Checked) abstractRecord.ChemosisInjection = true;
                if (RadioButtonChemosisInjectionNo.Checked) abstractRecord.ChemosisInjection = false;
                if (RadioButtonColorVisionVisualFieldsYes.Checked) abstractRecord.ColorVisionVisualFields = true;
                if (RadioButtonColorVisionVisualFieldsNo.Checked) abstractRecord.ColorVisionVisualFields = false;
               
                if (RadioButtonTreatmentSteroidsYes.Checked) abstractRecord.TreatmentSteroids = true;
                if (RadioButtonTreatmentSteroidsNo.Checked) abstractRecord.TreatmentSteroids = false;
                if (RadioButtonOrbitalRadiationYes.Checked) abstractRecord.OrbitalRadiation = true;
                if (RadioButtonOrbitalRadiationNo.Checked) abstractRecord.OrbitalRadiation = false;
                if (RadioButtonSurgicalDecompressionYes.Checked) abstractRecord.SurgicalDecompression = true;
                if (RadioButtonSurgicalDecompressionNo.Checked) abstractRecord.SurgicalDecompression = false;
                if (RadioButtonStrabismusSurgeryYes.Checked) abstractRecord.StrabismusSurgery = true;
                if (RadioButtonStrabismusSurgeryNo.Checked) abstractRecord.StrabismusSurgery = false;
                if (RadioButtonEyelidRetractionRepairYes.Checked) abstractRecord.EyelidRetractionRepair = true;
                if (RadioButtonEyelidRetractionRepairNo.Checked) abstractRecord.EyelidRetractionRepair = false;
                if (RadioButtonImprovedQualityLifeYes.Checked) abstractRecord.ImprovedQualityLife = true;
                if (RadioButtonImprovedQualityLifeNo.Checked) abstractRecord.ImprovedQualityLife = false;

                abstractRecord.LastUpdateDate = DateTime.Now;
                bool ChartCompleted = isComplete();
                abstractRecord.Complete = ChartCompleted;
                var chartRegistration = (from cr in pim.ChartRegistration
                                         where cr.ParticipantModuleCycleID == cycle.ParticipantModuleCycleID
                                          & cr.ModuleID == ModuleID
                                          & cr.RecordIdentifier == recordIdentifier
                                         select cr).First<NetHealthPIMModel.ChartRegistration>();
                chartRegistration.AbstractCompleted = ChartCompleted;

                if ((DropDownListYearOfBirth.SelectedIndex > 0) && (DropDownListYearOfBirth.SelectedIndex > 0))
                {
                    chartRegistration.DOB = abstractRecord.MonthOfBirth + "/" + abstractRecord.YearOfBirth;
                }
              
                pim.SaveChanges();

                transaction.Complete();




            }

            CycleManager.UpdateParticipantCompletedModules(cycleID, ModuleID);
        }


    }
        protected void ButtonSubmit_Click(object sender, EventArgs e)
        {
            savedata();
            isComplete();
            if (!isComplete())
            {
                isComplete();
                string script = " var answer = confirm('Chart data is not complete.\\nClick OK to save entries and exit chart.\\nClick CANCEL to save entries and review the chart. '); if (answer==true) window.location = '../PatientChartRegistration.aspx';";
                ScriptManager.RegisterStartupScript(this, GetType(),
                              "ServerControlScript", script, true);
            }
            else
            {

                Response.Redirect("../PatientChartRegistration.aspx");
            }
        }




        protected bool isComplete()
        {

            bool ChartCompleted = true;

            LabelRBGender.Visible = false;

            LabelRBSmoking.Visible = false;

            LabelRBAfferentPapillaryDefect.Visible = false;

            LabelRBMotilityRestricted.Visible = false;

            LabelRBUpperLidRetraction.Visible = false;

            LabelChemosisInjection.Visible = false;

            LabelRBKeratopathy.Visible = false;

            LabelRBIOPElevated.Visible = false;

            LabelRBOpticNerveAbnormalities.Visible = false;

            LabelRBSmokingDiscussion.Visible = false;

            LabelRBThyroidFunctionTest.Visible = false;

            LabelRBOcularLubrication.Visible = false;

            LabelRBDurationStability.Visible = false;

            LabelRBStopSmoking.Visible = false;

            LabelRBOcularComfortImproved.Visible = false;

            LabelRBDiplopiaImproved.Visible = false;

            LabelRBQualityVision.Visible = false;



            LabelMonthOfBirth.Visible = false;

            LabelYearOfBirth.Visible = false;



            LabelSymptomDryness.Visible = false;


            LabelSymptomDoubleVision.Visible = false;

           

            LabelSymptomStability.Visible = false;

            LabelHistoryMostRecent.Visible = false;

            LabelBCVA.Visible = false;

            LabelColorVisionVisualFields.Visible = false;

            LabelExophthalmometry.Visible = false;

            LabelTreatmentSteroids.Visible = false;

            LabelOrbitalRadiation.Visible = false;

            LabelSurgicalDecompression.Visible = false;

            LabelStrabismusSurgery.Visible = false;

            LabelEyelidRetractionRepair.Visible = false;

            LabelImprovedQualityLife.Visible = false;
            if (RadioButtonListRBExophthalmometry.SelectedIndex == -1)
            {
                LabelExophthalmometry.Visible = true;
                ChartCompleted = false;
            
            }
            if (RadioButtonListRBGender.SelectedIndex == -1)
            {
                LabelRBGender.Visible = true;
                ChartCompleted = false;
            }
            if (RadioButtonListRBSmoking.SelectedIndex == -1)
            {
                LabelRBSmoking.Visible = true;
                ChartCompleted = false;
            }
            if (RadioButtonListRBAfferentPapillaryDefect.SelectedIndex == -1)
            {
                LabelRBAfferentPapillaryDefect.Visible = true;
                ChartCompleted = false;
            }
            if (RadioButtonListRBMotilityRestricted.SelectedIndex == -1)
            {
                LabelRBMotilityRestricted.Visible = true;
                ChartCompleted = false;
            }
            if (RadioButtonListRBUpperLidRetraction.SelectedIndex == -1)
            {
                LabelRBUpperLidRetraction.Visible = true;
                ChartCompleted = false;
            }
            if (RadioButtonListBCVA.SelectedIndex == -1)
            {
                LabelChemosisInjection.Visible = true;
                ChartCompleted = false;
            }
            if (RadioButtonListRBKeratopathy.SelectedIndex == -1)
            {
                LabelRBKeratopathy.Visible = true;
                ChartCompleted = false;
            }
            if (RadioButtonListRBIOPElevated.SelectedIndex == -1)
            {
                LabelRBIOPElevated.Visible = true;
                ChartCompleted = false;
            }
            if (RadioButtonListRBOpticNerveAbnormalities.SelectedIndex == -1)
            {
                LabelRBOpticNerveAbnormalities.Visible = true;
                ChartCompleted = false;
            }
            if (RadioButtonListRBSmokingDiscussion.SelectedIndex == -1)
            {
                LabelRBSmokingDiscussion.Visible = true;
                ChartCompleted = false;
            }
            if (RadioButtonListRBThyroidFunctionTest.SelectedIndex == -1)
            {
                LabelRBThyroidFunctionTest.Visible = true;
                ChartCompleted = false;
            }
            if (RadioButtonListRBOcularLubrication.SelectedIndex == -1)
            {
                LabelRBOcularLubrication.Visible = true;
                ChartCompleted = false;
            }
            if (RadioButtonListRBDurationStability.SelectedIndex == -1)
            {
                LabelRBDurationStability.Visible = true;
                ChartCompleted = false;
            }
            if (RadioButtonListRBStopSmoking.SelectedIndex == -1)
            {
                LabelRBStopSmoking.Visible = true;
                ChartCompleted = false;
            }
            if (RadioButtonListRBOcularComfortImproved.SelectedIndex == -1)
            {
                LabelRBOcularComfortImproved.Visible = true;
                ChartCompleted = false;
            }
            if (RadioButtonListRBDiplopiaImproved.SelectedIndex == -1)
            {
                LabelRBDiplopiaImproved.Visible = true;
                ChartCompleted = false;
            }
            if (RadioButtonListRBQualityVision.SelectedIndex == -1)
            {
                LabelRBQualityVision.Visible = true;
                ChartCompleted = false;
            }



            if (DropDownListMonthOfBirth.SelectedIndex == 0)
            {
                LabelMonthOfBirth.Visible = true;
                ChartCompleted = false;
            }
            if (DropDownListYearOfBirth.SelectedIndex == 0)
            {
                LabelYearOfBirth.Visible = true;
                ChartCompleted = false;
            }



            if (RadioButtonRadioButtonSymptomDryness.SelectedIndex==-1)
            {
                LabelSymptomDryness.Visible = true;
                ChartCompleted = false;
            }
            if (RadioButtonListSymptomDoubleVision.SelectedIndex==-1)
            {
                LabelSymptomDoubleVision.Visible = true;
                ChartCompleted = false;
            }
       
            if (RadioButtonSymptomStabilityNo.Checked == false && RadioButtonSymptomStabilityYes.Checked == false)
            {
                LabelSymptomStability.Visible = true;
                ChartCompleted = false;
            }
            if (RadioButtonHistoryMostRecentNo.Checked == false && RadioButtonHistoryMostRecentYes.Checked == false)
            {
                LabelHistoryMostRecent.Visible = true;
                ChartCompleted = false;
            }
            if (RadioButtonChemosisInjectionNo.Checked == false && RadioButtonChemosisInjectionYes.Checked == false)
            {
                LabelBCVA.Visible = true;
                ChartCompleted = false;
            }
            if (RadioButtonColorVisionVisualFieldsNo.Checked == false && RadioButtonColorVisionVisualFieldsYes.Checked == false)
            {
                LabelColorVisionVisualFields.Visible = true;
                ChartCompleted = false;
            }
          
            if (RadioButtonTreatmentSteroidsNo.Checked == false && RadioButtonTreatmentSteroidsYes.Checked == false)
            {
                LabelTreatmentSteroids.Visible = true;
                ChartCompleted = false;
            }
            if (RadioButtonOrbitalRadiationNo.Checked == false && RadioButtonOrbitalRadiationYes.Checked == false)
            {
                LabelOrbitalRadiation.Visible = true;
                ChartCompleted = false;
            }
            if (RadioButtonSurgicalDecompressionNo.Checked == false && RadioButtonSurgicalDecompressionYes.Checked == false)
            {
                LabelSurgicalDecompression.Visible = true;
                ChartCompleted = false;
            }
            if (RadioButtonStrabismusSurgeryNo.Checked == false && RadioButtonStrabismusSurgeryYes.Checked == false)
            {
                LabelStrabismusSurgery.Visible = true;
                ChartCompleted = false;
            }
            if (RadioButtonEyelidRetractionRepairNo.Checked == false && RadioButtonEyelidRetractionRepairYes.Checked == false)
            {
                LabelEyelidRetractionRepair.Visible = true;
                ChartCompleted = false;
            }
            if (RadioButtonImprovedQualityLifeNo.Checked == false && RadioButtonImprovedQualityLifeYes.Checked == false)
            {
                LabelImprovedQualityLife.Visible = true;
                ChartCompleted = false;
            }


            return ChartCompleted;



        }


}

