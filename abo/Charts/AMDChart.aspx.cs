﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Transactions;
using NetHealthPIMModel;

public partial class abo_AMDChart : BasePage
{

    public bool PQRSEnabled { get; set; }

    public bool PQRSUsed { get; set; }

    protected void Page_Load(object sender, EventArgs e) {
        NetHealthPIMEntities pim1 = new NetHealthPIMEntities();
        var pqrsselected = pim1.ParticipantModule.First(p => p.ParticipantID == ctx.ParticipantID);
        PQRSPanel.Visible = Convert.ToBoolean(pqrsselected.PQRS);

        if (!IsPostBack) {
            InitializePage();
            PQRSUsed = Convert.ToBoolean(pqrsselected.PQRS);
            PQRSEnabled = Convert.ToBoolean(System.Configuration.ConfigurationManager.AppSettings["PQRSEnabled"]);
        }
    }

    protected void InitializePage()
    {
        ((PIMMasterPage)Page.Master).SetTitle("Chart");

        using (NetHealthPIMEntities pim = new NetHealthPIMEntities())
        {
            int ModuleID = (int)Session[Constants.SESSION_WORKINGMODULEID];
            Module module = pim.Module.First(c => c.ModuleID == ModuleID);
            ((PIMMasterPage)Page.Master).SetHeader(module.ModuleName + " Chart Abstraction");

            ParticipantModuleCycle prev = (ParticipantModuleCycle)Session[Constants.SESSION_PREVIOUSCYCLE];
            String CycleNumber = Request.QueryString["CycleNumber"];

            int cycleID = ctx.ActiveModuleCycleID;
            if (CycleNumber == "1")
            {
                cycleID = prev.ParticipantModuleCycleID;
                LinkButtonBackToDashboard.Visible = true;
                ButtonSubmit.Visible = false;
            }

            ParticipantModuleCycle cycle = pim.ParticipantModuleCycle.First(c => c.ParticipantModuleCycleID == cycleID);
            String numberOfRecord = Request.QueryString["nr"];
            String totalRecords = Request.QueryString["t"];
            if (cycle.CycleNumber == 1)
            {
                ((PIMMasterPage)Page.Master).SetStatus(2);
                LiteralAbstractionNumber.Text = "Initial Abstraction: Chart " + numberOfRecord + " of " + totalRecords;
            }
            else
            {
                ((PIMMasterPage)Page.Master).SetStatus(3);
                LiteralAbstractionNumber.Text = "Second Abstraction: Chart " + numberOfRecord + " of " + totalRecords;
            }
            DropDownListMonthOfBirth.DataSource = CycleManager.getMonthList();
            DropDownListYearOfBirth.DataSource = CycleManager.getDOBYearList(0, 100);
                  DropDownListMonthOfExam.DataSource= CycleManager.getMonthList();
                  DropDownListYearOfExam.DataSource = CycleManager.getDOBYearList(0, 100);
                  DropDownListMonthOfFollowUp.DataSource= CycleManager.getMonthList();
                  DropDownListYearOfFollowUp.DataSource = CycleManager.getDOBYearList(0, 100);

                  DropDownListBCVAInvolvedEye.DataSource = CycleManager.getExamValues();
                  DropDownListBCVAFellowEye.DataSource = CycleManager.getExamValues();
                  DropDownListOutcomeTreatedBCVA.DataSource = CycleManager.getExamValues();
                  DropDownListOutcomeFellowBCVA.DataSource = CycleManager.getExamValues();
                  //DropDownListPQRSDOBMonth.DataSource = CycleManager.getMonthList();
                  //DropDownListPQRSDOBYear.DataSource = CycleManager.getDOBYearList(0, 100);
                  DropDownListPQRSVisitMonth.DataSource = CycleManager.getMonthList();
                  DropDownListPQRSVisitYear.DataSource = CycleManager.getDOBYearList(0, 100);

            DataBind();
            DropDownListPQRSVisitYear.Items.Insert(0, new ListItem("Year", "0"));
            DropDownListYearOfBirth.Items.Insert(0, new ListItem("Year", "0"));
            DropDownListYearOfExam.Items.Insert(0, new ListItem("Year", "0"));
            DropDownListYearOfFollowUp.Items.Insert(0, new ListItem("Year", "0"));
            //DropDownListPQRSDOBYear.Items.Insert(0, new ListItem("Year", "0"));
            //DropDownListPQRSVisitYear.Items.Insert(0, new ListItem("Year", "0"));
            string recordIdentifier = Request.QueryString["ri"];
            HiddenFieldRecordIdentifier.Value = recordIdentifier;
            LiteralRecordIdentifier.Text = recordIdentifier;
            var count = pim.ChartAbstractionExudativeAMD.Where(ps => ps.ParticipantModuleCycle.ParticipantModuleCycleID == cycleID & ps.RecordIdentifier == recordIdentifier).Count();
            if (count == 0)
            {
                using (TransactionScope transaction = new TransactionScope())
                {
                    //Module module = pim.Module.First(m => m.ModuleID == Constants.ABO_AMBLYOPIA_MODULEID);

                    NetHealthPIMModel.ChartRegistration chartRegistration = (from cr in pim.ChartRegistration
                                                           where cr.ParticipantModuleCycleID == cycleID
                                                           & cr.RecordIdentifier == recordIdentifier
                                                           & cr.ModuleID == 53
                                                           select cr).First<NetHealthPIMModel.ChartRegistration>();
                   
                    DateTime initialVisit = Convert.ToDateTime(chartRegistration.InitialVisit);
                    DateTime DOB = Convert.ToDateTime(chartRegistration.DOB);
                    ChartAbstractionExudativeAMD abstractRecord = new ChartAbstractionExudativeAMD();
                    abstractRecord.ParticipantModuleCycle = cycle;
                    abstractRecord.RecordIdentifier = recordIdentifier;
                    abstractRecord.MonthOfBirth = DOB.Month;
                    abstractRecord.YearOfBirth = DOB.Year;
                    abstractRecord.MonthOfExam= initialVisit.Month;
                    abstractRecord.YearOfExam = initialVisit.Year;
                    DropDownListYearOfBirth.SelectedValue = abstractRecord.YearOfBirth.ToString();
                    DropDownListMonthOfBirth.SelectedValue = abstractRecord.MonthOfBirth.ToString();
                    DropDownListMonthOfExam.SelectedValue = abstractRecord.MonthOfExam.ToString();
                    //DropDownListPQRSDOBYear.SelectedValue = DropDownListYearOfBirth.SelectedValue;
                    //DropDownListPQRSDOBMonth.SelectedValue = DropDownListMonthOfBirth.SelectedValue;
                    DropDownListYearOfExam.DataSource = CycleManager.getDOBYearList(0, 100);
                    DropDownListYearOfExam.SelectedValue = abstractRecord.YearOfExam.ToString();
                    
                    abstractRecord.CreatedOn = DateTime.Now;
                    abstractRecord.LastUpdateDate = DateTime.Now;
                    pim.SaveChanges();

                    transaction.Complete();
                }
            }
            else
            {
                ChartAbstractionExudativeAMD abstractRecord = (from c in pim.ChartAbstractionExudativeAMD
                                                            where c.ParticipantModuleCycle.ParticipantModuleCycleID == cycleID & c.RecordIdentifier == recordIdentifier
                                                               select c).First<ChartAbstractionExudativeAMD>();
                  
                Participant participant = pim.Participant.First(p => p.ParticipantID == ctx.ParticipantID);
                var pqrsselected = pim.ParticipantModule.First(p => p.ParticipantID == participant.ParticipantID);
                bool pqrsused = Convert.ToBoolean(pqrsselected.PQRS);
                bool pqrsenabled = Convert.ToBoolean(System.Configuration.ConfigurationManager.AppSettings["PQRSEnabled"]);
                if (pqrsused && pqrsenabled) {
                    PQRSPanel.Visible = true;
                    //try
                    //{
                    PQRSWebService.PPWebServiceClient client = new PQRSWebService.PPWebServiceClient();
                    client.ClientCredentials.UserName.UserName = "partner:2015";
                    client.ClientCredentials.UserName.Password = "48C1A443-54B0-4D00-A4A2-FE5E46C2F02E";



                    Int32 participantid = client.AddUser(0, "0", participant.ParticipantFirstName, participant.ParticipantLastName, participant.ParticipantEmailAddress, participant.ParticipantEmailAddress, "abo2015", "ABO"); 



                        String patient_id = Request.QueryString["ri"].ToString();
                        if (!(cycle.CycleNumber == 1))
                        {
                            patient_id = "C2" + patient_id;
                        }


                        DataTable values = client.GetPatientVisit(participantid, patient_id, 2015);
                        if (values != null && values.Rows.Count>0)
                        {
                            DataRow visit = values.Rows[0];
                            DropDownListPQRSVisitMonth.SelectedValue = visit["VisitDate"].ToString().Substring(0, 2).TrimStart('0');
                            DropDownListPQRSVisitYear.SelectedValue = visit["VisitDate"].ToString().Substring(6);
                            PQRSMedicare.SelectedValue = (Boolean)visit["IsMedicarePartB"] ? "1" : "0";

                            ///  try {
                            String question_id = "";
                            //question_id = "G1"; if (visit[question_id] != null) RadioButtonListRBGender.SelectedValue = visit[question_id].ToString().Replace("M", "42").Replace("F", "43");

                            
                            try {question_id = "Q3234"; //Diagnosis 14.2, 140.2
                            if (visit[question_id] != null) PQRSDiagnosis.SelectedValue = visit[question_id].ToString();
							} catch {}
							
                            try {question_id = "Q3235"; //Q1863
                            if (visit[question_id] != null)
                            PQRSEncounter.SelectedValue = visit[question_id].ToString();
							} catch {}
                            try {question_id = "Q3236"; if (visit[question_id] != null) PQRSMacularExamination.SelectedValue = visit[question_id].ToString();
							} catch {}
                            try {question_id = "Q3237"; if (visit[question_id] != null) PQRSMacularThickening.SelectedValue = visit[question_id].ToString();
							} catch {}

                            try {question_id = "Q3238"; if (visit[question_id] != null) PQRSMacularDocumented.SelectedValue = visit[question_id].ToString();
							} catch {}
							try {question_id = "Q3239"; if (visit[question_id] != null) PQRSMacularSeverityReason.SelectedValue = visit[question_id].ToString();
							}  catch { }
                            try {question_id = "Q3539"; if (visit[question_id] != null) PQRSCounseling.SelectedValue = visit[question_id].ToString();
							}  catch { }
                            // }
                            // catch { }
                            /// }
                        }
                    //}
                    //catch
                    //{
                    //    ViewState["PQRSBroken"] = true;
                    //}
                }
            
                if (abstractRecord.MgntBevacizumab != null) CheckBoxMgntBevacizumab.Checked = ((bool)abstractRecord.MgntBevacizumab);
                if (abstractRecord.MgntRanibizumab != null) CheckBoxMgntRanibizumab.Checked = ((bool)abstractRecord.MgntRanibizumab);
                if (abstractRecord.MgntAflibercept != null) CheckBoxMgntAflibercept.Checked = ((bool)abstractRecord.MgntAflibercept);
                if (abstractRecord.MgntIntravitrealSteroids != null) CheckBoxMgntIntravitrealSteroids.Checked = ((bool)abstractRecord.MgntIntravitrealSteroids);
                if (abstractRecord.MgntFocalLaser != null) CheckBoxMgntFocalLaser.Checked = ((bool)abstractRecord.MgntFocalLaser);
                if (abstractRecord.MgntObservation != null) CheckBoxMgntObservation.Checked = ((bool)abstractRecord.MgntObservation);
                if (abstractRecord.MgntPDT != null) CheckBoxMgntPDT.Checked = ((bool)abstractRecord.MgntPDT);
                if (abstractRecord.MgntOther != null) CheckBoxMgntOther.Checked = ((bool)abstractRecord.MgntOther);
                if (abstractRecord.MgntNR != null) CheckBoxMgntNR.Checked = ((bool)abstractRecord.MgntNR);
                if (abstractRecord.ObservationReasonPatRef != null) CheckBoxObservationReasonPatRef.Checked = ((bool)abstractRecord.ObservationReasonPatRef);
                if (abstractRecord.ObservationReasonRisksOutweigh != null) CheckBoxObservationReasonRisksOutweigh.Checked = ((bool)abstractRecord.ObservationReasonRisksOutweigh);
                if (abstractRecord.ObservationReasonUnlikelyToImprove != null) CheckBoxObservationReasonUnlikelyToImprove.Checked = ((bool)abstractRecord.ObservationReasonUnlikelyToImprove);
                if (abstractRecord.TherapyBevacizumab != null) CheckBoxTherapyBevacizumab.Checked = ((bool)abstractRecord.TherapyBevacizumab);
                if (abstractRecord.TherapyRanibizumab != null) CheckBoxTherapyRanibizumab.Checked = ((bool)abstractRecord.TherapyRanibizumab);
                if (abstractRecord.TherapyAflibercept != null) CheckBoxTherapyAflibercept.Checked = ((bool)abstractRecord.TherapyAflibercept);
                if (abstractRecord.TherapyPDT != null) CheckBoxTherapyPDT.Checked = ((bool)abstractRecord.TherapyPDT);
                if (abstractRecord.TherapyIntravitrealSteroids != null) CheckBoxTherapyIntravitrealSteroids.Checked = ((bool)abstractRecord.TherapyIntravitrealSteroids);
                if (abstractRecord.TherapyMPSLaser != null) CheckBoxTherapyMPSLaser.Checked = ((bool)abstractRecord.TherapyMPSLaser);
                if (abstractRecord.TherapyICGLaser != null) CheckBoxTherapyICGLaser.Checked = ((bool)abstractRecord.TherapyICGLaser);
                if (abstractRecord.TherapyOther != null) CheckBoxTherapyOther.Checked = ((bool)abstractRecord.TherapyOther);
                if (abstractRecord.OutcomeTreatedEyeCME != null) CheckBoxOutcomeTreatedEyeCME.Checked = ((bool)abstractRecord.OutcomeTreatedEyeCME);
                if (abstractRecord.OutcomeTreatedEyeDryScar != null) CheckBoxOutcomeTreatedEyeDryScar.Checked = ((bool)abstractRecord.OutcomeTreatedEyeDryScar);
                if (abstractRecord.OutcomeTreatedEyeHemorrhage != null) CheckBoxOutcomeTreatedEyeHemorrhage.Checked = ((bool)abstractRecord.OutcomeTreatedEyeHemorrhage);
                if (abstractRecord.OutcomeTreatedEyeLipid != null) CheckBoxOutcomeTreatedEyeLipid.Checked = ((bool)abstractRecord.OutcomeTreatedEyeLipid);
                if (abstractRecord.OutcomeTreatedEyeSRF != null) CheckBoxOutcomeTreatedEyeSRF.Checked = ((bool)abstractRecord.OutcomeTreatedEyeSRF);
                if (abstractRecord.OutcomeTreatedEyeSubRPE != null) CheckBoxOutcomeTreatedEyeSubRPE.Checked = ((bool)abstractRecord.OutcomeTreatedEyeSubRPE);
                if (abstractRecord.OutcomeTreatedEyeNR != null) CheckBoxOutcomeTreatedEyeNR.Checked = ((bool)abstractRecord.OutcomeTreatedEyeNR);
                if (abstractRecord.OutcomeTreatedEyeOther != null) CheckBoxOutcomeTreatedEyeOther.Checked = ((bool)abstractRecord.OutcomeTreatedEyeOther);



                if (abstractRecord.RBGender != null) RadioButtonListRBGender.SelectedValue = abstractRecord.RBGender.ToString();
                if (abstractRecord.RBEyeWithExudativeAMD != null) RadioButtonListRBEyeWithExudativeAMD.SelectedValue = abstractRecord.RBEyeWithExudativeAMD.ToString();
                if (abstractRecord.RBSmokingStatus != null) RadioButtonListRBSmokingStatus.SelectedValue = abstractRecord.RBSmokingStatus.ToString();
                if (abstractRecord.RBExamAMDFellowEye != null) RadioButtonListRBExamAMDFellowEye.SelectedValue = abstractRecord.RBExamAMDFellowEye.ToString();
                if (abstractRecord.RBInterval != null) RadioButtonListRBInterval.SelectedValue = abstractRecord.RBInterval.ToString();
                
                if (abstractRecord.RBPatientCounseledAntioxidant != null) RadioButtonListRBPatientCounseledAntioxidant.SelectedValue = abstractRecord.RBPatientCounseledAntioxidant.ToString();


                //Birthday
                if (abstractRecord.YearOfBirth != null) DropDownListYearOfBirth.SelectedValue = abstractRecord.YearOfBirth.ToString();
                if (abstractRecord.MonthOfBirth != null) DropDownListMonthOfBirth.SelectedValue = abstractRecord.MonthOfBirth.ToString();
                //DropDownListPQRSDOBYear.SelectedValue = DropDownListYearOfBirth.SelectedValue;
                //DropDownListPQRSDOBMonth.SelectedValue = DropDownListMonthOfBirth.SelectedValue;

                if (abstractRecord.MonthOfExam != null) DropDownListMonthOfExam.SelectedValue = abstractRecord.MonthOfExam.ToString();
                if (abstractRecord.YearOfExam != null) DropDownListYearOfExam.SelectedValue = abstractRecord.YearOfExam.ToString();
                if (abstractRecord.MonthOfFollowUp != null) DropDownListMonthOfFollowUp.SelectedValue = abstractRecord.MonthOfFollowUp.ToString();
                if (abstractRecord.YearOfFollowUp != null) DropDownListYearOfFollowUp.SelectedValue = abstractRecord.YearOfFollowUp.ToString();
                if (abstractRecord.OutcomeTreatedBCVA != null) DropDownListOutcomeTreatedBCVA.SelectedValue = abstractRecord.OutcomeTreatedBCVA.ToString();
                if (abstractRecord.OutcomeFellowBCVA != null) DropDownListOutcomeFellowBCVA.SelectedValue = abstractRecord.OutcomeFellowBCVA.ToString();



                if (abstractRecord.DrugReactions != null)
                {
                    RadioButtonDrugReactionsYes.Checked = ((bool)abstractRecord.DrugReactions);
                    RadioButtonDrugReactionsNo.Checked = !((bool)abstractRecord.DrugReactions);
                }
                if (abstractRecord.ExamFluorescein != null)
                {
                    RadioButtonExamFluoresceinYes.Checked = ((bool)abstractRecord.ExamFluorescein);
                    RadioButtonExamFluoresceinNo.Checked = !((bool)abstractRecord.ExamFluorescein);
                }
                if (abstractRecord.ExamOCT != null)
                {
                    RadioButtonExamOCTYes.Checked = ((bool)abstractRecord.ExamOCT);
                    RadioButtonExamOCTNo.Checked = !((bool)abstractRecord.ExamOCT);
                }
                if (abstractRecord.ExamICG != null)
                {
                    RadioButtonExamICGYes.Checked = ((bool)abstractRecord.ExamICG);
                    RadioButtonExamICGNo.Checked = !((bool)abstractRecord.ExamICG);
                }


                if (abstractRecord.BCVAInvolvedEye != null) DropDownListBCVAInvolvedEye.SelectedValue = abstractRecord.BCVAInvolvedEye.ToString();
                if (abstractRecord.BCVAFellowEye != null) DropDownListBCVAFellowEye.SelectedValue = abstractRecord.BCVAFellowEye.ToString();
                
                if (abstractRecord.TherapyBevacizumabNbr != null) TextBoxTherapyBevacizumabNbr.Text = abstractRecord.TherapyBevacizumabNbr.ToString();
                if (abstractRecord.TherapyRanibizumabNbr != null) TextBoxTherapyRanibizumabNbr.Text = abstractRecord.TherapyRanibizumabNbr.ToString();
                if (abstractRecord.TherapyAfliberceptNbr != null) TextBoxTherapyAfliberceptNbr.Text = abstractRecord.TherapyAfliberceptNbr.ToString();
                if (abstractRecord.TherapyPDTNbr != null) TextBoxTherapyPDTNbr.Text = abstractRecord.TherapyPDTNbr.ToString();
                if (abstractRecord.TherapyIntravitrealSteroidsNbr != null) TextBoxTherapyIntravitrealSteroidsNbr.Text = abstractRecord.TherapyIntravitrealSteroidsNbr.ToString();
                if (abstractRecord.TherapyMPSLaserNbr != null) TextBoxTherapyMPSLaserNbr.Text = abstractRecord.TherapyMPSLaserNbr.ToString();
                if (abstractRecord.TherapyICGLaserNbr != null) TextBoxTherapyICGLaserNbr.Text = abstractRecord.TherapyICGLaserNbr.ToString();
                if (abstractRecord.TherapyOtherNbr != null) TextBoxTherapyOtherNbr.Text = abstractRecord.TherapyOtherNbr.ToString();
                if (abstractRecord.TherapyOtherText != null) TextBoxTherapyOtherText.Text = abstractRecord.TherapyOtherText.ToString();
                if (abstractRecord.OutcomeTreatedEyeOtherText != null) TextBoxOutcomeTreatedEyeOtherText.Text = abstractRecord.OutcomeTreatedEyeOtherText.ToString();
            }
        }
    }

    public void savedata() {
        using (NetHealthPIMEntities pim = new NetHealthPIMEntities()) {
            int ModuleID = (int)Session[Constants.SESSION_WORKINGMODULEID];
            int cycleID = ctx.ActiveModuleCycleID;
            ParticipantModuleCycle cycle = pim.ParticipantModuleCycle.First(c => c.ParticipantModuleCycleID == cycleID);
            string recordIdentifier = HiddenFieldRecordIdentifier.Value;
            using (TransactionScope transaction = new TransactionScope()) {
                ChartAbstractionExudativeAMD abstractRecord = (from c in pim.ChartAbstractionExudativeAMD
                                                               where c.ParticipantModuleCycle.ParticipantModuleCycleID == cycleID & c.RecordIdentifier == recordIdentifier
                                                               select c).First<ChartAbstractionExudativeAMD>();

                abstractRecord.MgntBevacizumab = CheckBoxMgntBevacizumab.Checked;
                abstractRecord.MgntRanibizumab = CheckBoxMgntRanibizumab.Checked;
                abstractRecord.MgntAflibercept = CheckBoxMgntAflibercept.Checked;
                abstractRecord.MgntIntravitrealSteroids = CheckBoxMgntIntravitrealSteroids.Checked;
                abstractRecord.MgntFocalLaser = CheckBoxMgntFocalLaser.Checked;
                abstractRecord.MgntObservation = CheckBoxMgntObservation.Checked;
                abstractRecord.MgntPDT = CheckBoxMgntPDT.Checked;
                abstractRecord.MgntOther = CheckBoxMgntOther.Checked;
                abstractRecord.MgntNR = CheckBoxMgntNR.Checked;
                abstractRecord.ObservationReasonPatRef = CheckBoxObservationReasonPatRef.Checked;
                abstractRecord.ObservationReasonRisksOutweigh = CheckBoxObservationReasonRisksOutweigh.Checked;
                abstractRecord.ObservationReasonUnlikelyToImprove = CheckBoxObservationReasonUnlikelyToImprove.Checked;
                abstractRecord.TherapyBevacizumab = CheckBoxTherapyBevacizumab.Checked;
                abstractRecord.TherapyRanibizumab = CheckBoxTherapyRanibizumab.Checked;
                abstractRecord.TherapyAflibercept = CheckBoxTherapyAflibercept.Checked;
                abstractRecord.TherapyPDT = CheckBoxTherapyPDT.Checked;
                abstractRecord.TherapyIntravitrealSteroids = CheckBoxTherapyIntravitrealSteroids.Checked;
                abstractRecord.TherapyMPSLaser = CheckBoxTherapyMPSLaser.Checked;
                abstractRecord.TherapyICGLaser = CheckBoxTherapyICGLaser.Checked;
                abstractRecord.TherapyOther = CheckBoxTherapyOther.Checked;
                abstractRecord.OutcomeTreatedEyeCME = CheckBoxOutcomeTreatedEyeCME.Checked;
                abstractRecord.OutcomeTreatedEyeDryScar = CheckBoxOutcomeTreatedEyeDryScar.Checked;
                abstractRecord.OutcomeTreatedEyeHemorrhage = CheckBoxOutcomeTreatedEyeHemorrhage.Checked;
                abstractRecord.OutcomeTreatedEyeLipid = CheckBoxOutcomeTreatedEyeLipid.Checked;
                abstractRecord.OutcomeTreatedEyeSRF = CheckBoxOutcomeTreatedEyeSRF.Checked;
                abstractRecord.OutcomeTreatedEyeSubRPE = CheckBoxOutcomeTreatedEyeSubRPE.Checked;
                abstractRecord.OutcomeTreatedEyeNR = CheckBoxOutcomeTreatedEyeNR.Checked;
                abstractRecord.OutcomeTreatedEyeOther = CheckBoxOutcomeTreatedEyeOther.Checked;



                if (RadioButtonListRBGender.SelectedIndex != -1) abstractRecord.RBGender = Convert.ToInt32(RadioButtonListRBGender.SelectedValue);
                else abstractRecord.RBGender = 0;
                if (RadioButtonListRBEyeWithExudativeAMD.SelectedIndex != -1) abstractRecord.RBEyeWithExudativeAMD = Convert.ToInt32(RadioButtonListRBEyeWithExudativeAMD.SelectedValue);
                else abstractRecord.RBEyeWithExudativeAMD = 0;
                if (RadioButtonListRBSmokingStatus.SelectedIndex != -1) abstractRecord.RBSmokingStatus = Convert.ToInt32(RadioButtonListRBSmokingStatus.SelectedValue);
                else abstractRecord.RBSmokingStatus = 0;
                if (RadioButtonListRBExamAMDFellowEye.SelectedIndex != -1) abstractRecord.RBExamAMDFellowEye = Convert.ToInt32(RadioButtonListRBExamAMDFellowEye.SelectedValue);
                else abstractRecord.RBExamAMDFellowEye = 0;
                if (RadioButtonListRBInterval.SelectedIndex != -1) abstractRecord.RBInterval = Convert.ToInt32(RadioButtonListRBInterval.SelectedValue);
                else abstractRecord.RBInterval = 0;

                if (RadioButtonListRBPatientCounseledAntioxidant.SelectedIndex != -1) abstractRecord.RBPatientCounseledAntioxidant = Convert.ToInt32(RadioButtonListRBPatientCounseledAntioxidant.SelectedValue);
                else abstractRecord.RBPatientCounseledAntioxidant = 0;


                if (DropDownListBCVAInvolvedEye.SelectedIndex > 0) abstractRecord.BCVAInvolvedEye = Convert.ToInt32(DropDownListBCVAInvolvedEye.SelectedValue);
                else abstractRecord.BCVAInvolvedEye = 0;
                if (DropDownListBCVAFellowEye.SelectedIndex > 0) abstractRecord.BCVAFellowEye = Convert.ToInt32(DropDownListBCVAFellowEye.SelectedValue);
                else abstractRecord.BCVAFellowEye = 0;

                if (DropDownListMonthOfBirth.SelectedIndex > 0) abstractRecord.MonthOfBirth = Convert.ToInt32(DropDownListMonthOfBirth.SelectedValue);
                else abstractRecord.MonthOfBirth = 0;
                if (DropDownListYearOfBirth.SelectedIndex > 0) abstractRecord.YearOfBirth = Convert.ToInt32(DropDownListYearOfBirth.SelectedValue);
                else abstractRecord.YearOfBirth = 0;
                if (DropDownListMonthOfExam.SelectedIndex > 0) abstractRecord.MonthOfExam = Convert.ToInt32(DropDownListMonthOfExam.SelectedValue);
                else abstractRecord.MonthOfExam = 0;
                if (DropDownListYearOfExam.SelectedIndex > 0) abstractRecord.YearOfExam = Convert.ToInt32(DropDownListYearOfExam.SelectedValue);
                else abstractRecord.YearOfExam = 0;
                if (DropDownListMonthOfFollowUp.SelectedIndex > 0) abstractRecord.MonthOfFollowUp = Convert.ToInt32(DropDownListMonthOfFollowUp.SelectedValue);
                else abstractRecord.MonthOfFollowUp = 0;
                if (DropDownListYearOfFollowUp.SelectedIndex > 0) abstractRecord.YearOfFollowUp = Convert.ToInt32(DropDownListYearOfFollowUp.SelectedValue);
                else abstractRecord.YearOfFollowUp = 0;
                if (DropDownListOutcomeTreatedBCVA.SelectedIndex > 0) abstractRecord.OutcomeTreatedBCVA = Convert.ToInt32(DropDownListOutcomeTreatedBCVA.SelectedValue);
                else abstractRecord.OutcomeTreatedBCVA = 0;
                if (DropDownListOutcomeFellowBCVA.SelectedIndex > 0) abstractRecord.OutcomeFellowBCVA = Convert.ToInt32(DropDownListOutcomeFellowBCVA.SelectedValue);
                else abstractRecord.OutcomeFellowBCVA = 0;



                if (RadioButtonDrugReactionsYes.Checked) abstractRecord.DrugReactions = true;
                if (RadioButtonDrugReactionsNo.Checked) abstractRecord.DrugReactions = false;
                if (RadioButtonExamFluoresceinYes.Checked) abstractRecord.ExamFluorescein = true;
                if (RadioButtonExamFluoresceinNo.Checked) abstractRecord.ExamFluorescein = false;
                if (RadioButtonExamOCTYes.Checked) abstractRecord.ExamOCT = true;
                if (RadioButtonExamOCTNo.Checked) abstractRecord.ExamOCT = false;
                if (RadioButtonExamICGYes.Checked) abstractRecord.ExamICG = true;
                if (RadioButtonExamICGNo.Checked) abstractRecord.ExamICG = false;





                //try
                //{
                //if (TextBoxIntervalOtherText.Text != null)
                //abstractRecord.IntervalOtherText = TextBoxIntervalOtherText.Text;
                //}
                //catch (Exception ex)
                //{
                //abstractRecord.IntervalOtherText = null;
                //TextBoxIntervalOtherText.Text = "";
                //}
                try {
                    if (TextBoxTherapyBevacizumabNbr.Text != null)
                        abstractRecord.TherapyBevacizumabNbr = Convert.ToInt32(TextBoxTherapyBevacizumabNbr.Text);
                }
                catch (Exception ex) {
                    abstractRecord.TherapyBevacizumabNbr = null;
                    TextBoxTherapyBevacizumabNbr.Text = "";
                }
                try {
                    if (TextBoxTherapyRanibizumabNbr.Text != null)
                        abstractRecord.TherapyRanibizumabNbr = Convert.ToInt32(TextBoxTherapyRanibizumabNbr.Text);
                }
                catch (Exception ex) {
                    abstractRecord.TherapyRanibizumabNbr = null;
                    TextBoxTherapyRanibizumabNbr.Text = "";
                }
                try {
                    if (TextBoxTherapyAfliberceptNbr.Text != null)
                        abstractRecord.TherapyAfliberceptNbr = Convert.ToInt32(TextBoxTherapyAfliberceptNbr.Text);
                }
                catch (Exception ex) {
                    abstractRecord.TherapyAfliberceptNbr = null;
                    TextBoxTherapyAfliberceptNbr.Text = "";
                }
                try {
                    if (TextBoxTherapyPDTNbr.Text != null)
                        abstractRecord.TherapyPDTNbr = Convert.ToInt32(TextBoxTherapyPDTNbr.Text);
                }
                catch (Exception ex) {
                    abstractRecord.TherapyPDTNbr = null;
                    TextBoxTherapyPDTNbr.Text = "";
                }
                try {
                    if (TextBoxTherapyIntravitrealSteroidsNbr.Text != null)
                        abstractRecord.TherapyIntravitrealSteroidsNbr = Convert.ToInt32(TextBoxTherapyIntravitrealSteroidsNbr.Text);
                }
                catch (Exception ex) {
                    abstractRecord.TherapyIntravitrealSteroidsNbr = null;
                    TextBoxTherapyIntravitrealSteroidsNbr.Text = "";
                }
                try {
                    if (TextBoxTherapyMPSLaserNbr.Text != null)
                        abstractRecord.TherapyMPSLaserNbr = Convert.ToInt32(TextBoxTherapyMPSLaserNbr.Text);
                }
                catch (Exception ex) {
                    abstractRecord.TherapyMPSLaserNbr = null;
                    TextBoxTherapyMPSLaserNbr.Text = "";
                }
                try {
                    if (TextBoxTherapyICGLaserNbr.Text != null)
                        abstractRecord.TherapyICGLaserNbr = Convert.ToInt32(TextBoxTherapyICGLaserNbr.Text);
                }
                catch (Exception ex) {
                    abstractRecord.TherapyICGLaserNbr = null;
                    TextBoxTherapyICGLaserNbr.Text = "";
                }
                try {
                    if (TextBoxTherapyOtherNbr.Text != null)
                        abstractRecord.TherapyOtherNbr = Convert.ToInt32(TextBoxTherapyOtherNbr.Text);
                }
                catch (Exception ex) {
                    abstractRecord.TherapyOtherNbr = null;
                    TextBoxTherapyOtherNbr.Text = "";
                }
                try {
                    if (TextBoxTherapyOtherText.Text != null)
                        abstractRecord.TherapyOtherText = TextBoxTherapyOtherText.Text;
                }
                catch (Exception ex) {
                    abstractRecord.TherapyOtherText = null;
                    TextBoxTherapyOtherText.Text = "";
                }
                try {
                    if (TextBoxOutcomeTreatedEyeOtherText.Text != null)
                        abstractRecord.OutcomeTreatedEyeOtherText = TextBoxOutcomeTreatedEyeOtherText.Text;
                }
                catch (Exception ex) {
                    abstractRecord.OutcomeTreatedEyeOtherText = null;
                    TextBoxOutcomeTreatedEyeOtherText.Text = "";
                }

                Participant participant = pim.Participant.First(p => p.ParticipantID == ctx.ParticipantID);
                var pqrsselected = pim.ParticipantModule.First(p => p.ParticipantID == participant.ParticipantID);
                Boolean pqrsused = Convert.ToBoolean(pqrsselected.PQRS);
                Int32 visityear = Convert.ToInt32("0" + DropDownListPQRSVisitYear.SelectedValue);
                bool pqrsenabled = Convert.ToBoolean(System.Configuration.ConfigurationManager.AppSettings["PQRSEnabled"]);
                if (pqrsused && visityear == 2015 && pqrsenabled) {
                    try
                    {
                        PQRSWebService.PPWebServiceClient client = new PQRSWebService.PPWebServiceClient();
                        client.ClientCredentials.UserName.UserName = "partner:2015";
                        client.ClientCredentials.UserName.Password = "48C1A443-54B0-4D00-A4A2-FE5E46C2F02E";



                        Int32 participantid = client.AddUser(0, "0", participant.ParticipantFirstName, participant.ParticipantLastName, participant.ParticipantEmailAddress, participant.ParticipantEmailAddress, "abo2015", "ABO"); 

                        String visit_date = DropDownListPQRSVisitMonth.SelectedValue.PadLeft(2, '0') + "/01/";
                        visit_date += DropDownListPQRSVisitYear.SelectedValue;
                        Boolean is_medicare = PQRSMedicare.SelectedValue.Equals("1");

                        DataTable values = new DataTable("PQRSPROVisit");
                        values.Columns.Add("MeasureID", typeof(Int32));
                        values.Columns.Add("QuestionType", typeof(String));
                        values.Columns.Add("QuestionID", typeof(Int32));
                        values.Columns.Add("QuestionIDSuffix", typeof(String));
                        values.Columns.Add("Value", typeof(String));
                        values.Columns.Add("CPT", typeof(String));
                        values.Columns.Add("Modifier", typeof(String));
                        values.Columns.Add("SubmissionYear", typeof(Int32));
                      
						//age			
                        AddVisitDetail(ref values, 14, 3233, "1");
						AddVisitDetail(ref values, 14, 3233, "1");
						//diagnosis
                        AddVisitDetail(ref values, 14, 3234, PQRSDiagnosis.SelectedValue);
                        AddVisitDetail(ref values, 14, 3235, PQRSEncounter.SelectedValue);
                        AddVisitDetail(ref values, 14, 3236, PQRSMacularExamination.SelectedValue);
                        AddVisitDetail(ref values, 14, 3237, PQRSMacularThickening.SelectedValue);
                        AddVisitDetail(ref values, 14, 3238, PQRSMacularDocumented.SelectedValue);
                        AddVisitDetail(ref values, 14, 3239, PQRSMacularSeverityReason.SelectedValue);

                        AddVisitDetail(ref values, 140, 3536, "1");
						AddVisitDetail(ref values, 140, 3540, "1");
                        AddVisitDetail(ref values, 140, 3537, PQRSDiagnosis.SelectedValue);
                        AddVisitDetail(ref values, 140, 3538, PQRSEncounter.SelectedValue);
                        AddVisitDetail(ref values, 140, 3539, PQRSCounseling.SelectedValue);
                        String patient_id = Request.QueryString["ri"].ToString();
                        if (!(cycle.CycleNumber == 1))
                        {
                            patient_id = "C2" + patient_id;
                        }

                        client.AddChart((Int32)participantid, 0, patient_id, visit_date, is_medicare, values);
                        client.Close();
                    }
                    catch
                    {
                        ViewState["PQRSBroken"] = true;
                    }
                }

                abstractRecord.LastUpdateDate = DateTime.Now;
                bool ChartCompleted = isComplete();
                abstractRecord.Complete = ChartCompleted;
                var chartRegistration = (from cr in pim.ChartRegistration
                                         where cr.ParticipantModuleCycleID == cycle.ParticipantModuleCycleID
                                          & cr.ModuleID == ModuleID
                                          & cr.RecordIdentifier == recordIdentifier
                                         select cr).First<NetHealthPIMModel.ChartRegistration>();
                chartRegistration.AbstractCompleted = ChartCompleted;
                if ((DropDownListYearOfBirth.SelectedIndex > 0) && (DropDownListYearOfBirth.SelectedIndex > 0)) {
                    chartRegistration.DOB = abstractRecord.MonthOfBirth + "/" + abstractRecord.YearOfBirth;
                }
                if ((DropDownListMonthOfExam.SelectedIndex > 0) && (DropDownListYearOfExam.SelectedIndex > 0)) {
                    chartRegistration.InitialVisit = abstractRecord.MonthOfExam + "/" + abstractRecord.YearOfExam;
                }

                pim.SaveChanges();

                transaction.Complete();
            }
            CycleManager.UpdateParticipantCompletedModules(cycleID, ModuleID);
        }
    }

    void AddVisitDetail(ref DataTable values, int measure_id, int question_id, string value) { AddVisitDetail(ref values, measure_id, question_id, value, "", "", ""); }
    void AddVisitDetail(ref DataTable values, int measure_id, int question_id, string value, string cpt, string modifier, string suffix) {
        if (!String.IsNullOrEmpty(value)) {
            DataRow detail = values.NewRow();
            detail["MeasureID"] = measure_id;
            detail["QuestionType"] = (measure_id > 0 ? "M" : "G");
            detail["QuestionID"] = question_id;
            detail["QuestionIDSuffix"] = suffix;
            detail["Value"] = value;
            detail["CPT"] = cpt;
            detail["Modifier"] = modifier;
            detail["SubmissionYear"] = Convert.ToInt32(System.Configuration.ConfigurationManager.AppSettings["PQRSYear"]);
            values.Rows.Add(detail);
        }
    }

    protected void ButtonSubmit_Click(object sender, EventArgs e) {
        savedata();
        if (!isComplete()) {
            string script = " var answer = confirm('All fields are not complete.  Are you sure you want to submit? " + ERR + "'); if (answer==true) window.location = '../PatientChartRegistration.aspx';";
            ScriptManager.RegisterStartupScript(this, GetType(), "ServerControlScript", script, true);
        }
        else
            Response.Redirect("../PatientChartRegistration.aspx", true);
    }

    string ERR = "";
    protected bool isComplete()
    {

        bool ChartCompleted = true;
            
            LabelRBGender.Visible = false;
            
            LabelRBEyeWithExudativeAMD.Visible = false;
           
            LabelRBSmokingStatus.Visible = false;
          
            LabelRBExamAMDFellowEye.Visible = false;
           
            LabelRBInterval.Visible = false;
           
           
            LabelRBPatientCounseledAntioxidant.Visible = false;
            
            LabelMonthOfBirth.Visible = false;
           
            LabelYearOfBirth.Visible = false;
           
            LabelMonthOfExam.Visible = false;
           
            LabelYearOfExam.Visible = false;
           
            LabelMonthOfFollowUp.Visible = false;
           
            LabelYearOfFollowUp.Visible = false;
           
            LabelBCVAInvolvedEye.Visible = false;
            
            LabelBCVAFellowEye.Visible = false;
           
            LabelOutcomeTreatedBCVA .Visible = false;
            
            LabelOutcomeFellowBCVA.Visible = false;
           
            LabelDrugReactions.Visible = false;
            
            LabelExamFluorescein.Visible = false;
           
            LabelExamOCT.Visible = false;
            
            LabelExamICG.Visible = false;

            //LabelPQRSDOBMonth.Visible = false;

            //LabelPQRSDOBYear.Visible = false;



            LabelPQRSVisitMonth.Visible = false;

            LabelPQRSVisitYear.Visible = false;



            LabelPQRSMedicare.Visible = false;

            LabelPQRSDiagnosis.Visible = false;

            LabelPQRSEncounter.Visible = false;

            LabelPQRSMacularExamination.Visible = false;

            LabelPQRSMacularThickening.Visible = false;

            LabelPQRSMacularSeverity.Visible = false;
            //LabelButtonPQRSDocumented.Visible = false;
            Labelinitialfol.Visible = false;

            Labelinitialage.Visible = false;
            Labelinitialvis.Visible = false;
            NetHealthPIMEntities pim = new NetHealthPIMEntities();
            var moduleinfo = (from c in pim.Module where c.ModuleID == 53 select new { c.RegistrationFollowUpVisitMonths, c.RegistrationInitialVisitMonths, c.RegistrationMinAge }).FirstOrDefault();
            string message = "Patient must be  at least " + moduleinfo.RegistrationMinAge + " years old<br />";
            string message1 = "Initial visit must be within " + moduleinfo.RegistrationInitialVisitMonths + " months<br />";
            string message2 = "Follow up visit must be at least " + moduleinfo.RegistrationFollowUpVisitMonths + " months after initial visit<br />";
            if (DropDownListYearOfExam.SelectedIndex != 0 && DropDownListMonthOfExam.SelectedIndex != 0 && DropDownListYearOfFollowUp.SelectedIndex != 0 && DropDownListMonthOfFollowUp.SelectedIndex != 0)
            {
                DateTime monthofsurgery = new DateTime(Convert.ToInt32(DropDownListYearOfExam.SelectedValue), Convert.ToInt32(DropDownListMonthOfExam.SelectedValue), 01);
                DateTime folowup = new DateTime(Convert.ToInt32(DropDownListYearOfFollowUp.SelectedValue), Convert.ToInt32(DropDownListMonthOfFollowUp.SelectedValue), 01);
                string registrationminage2 = Validation.validateRegistrationFolowUpMonth(monthofsurgery, folowup, Convert.ToInt32(moduleinfo.RegistrationFollowUpVisitMonths), message2);
                string registrationminage = Validation.validateRegistrationMinAge(Convert.ToInt32(DropDownListYearOfExam.SelectedValue), Convert.ToInt32(DropDownListYearOfBirth.SelectedValue), Convert.ToInt32(moduleinfo.RegistrationMinAge), message);
                string registrationminage1 = Validation.validateRegistrationInitialVisitMonth(DateTime.Now, monthofsurgery, Convert.ToInt32(moduleinfo.RegistrationInitialVisitMonths), message1);


                if (registrationminage != "")
                {
                    //ERR += "RegistrationImage=BLANK;";
                    Labelinitialage.Text = message;
                    Labelinitialage.Visible = true;
                    ChartCompleted = false;
                }
                if (registrationminage1 != "")
                {
                    //ERR += "RegistrationImage1=BLANK;";
                    Labelinitialvis.Text = message1;
                    Labelinitialvis.Visible = true;
                    ChartCompleted = false;
                }
                //if (registrationminage2 != "")
                //{
                //    ERR += "RegistrationImage2=BLANK;";
                //    Labelinitialfol.Text = message2;
                //    Labelinitialfol.Visible = true;
                //    ChartCompleted = false;
                //}
            }
            DateTime dateofbirth = DateTime.Now;
            DateTime dateofexam = DateTime.Now;
            DateTime dateoffollowexam = DateTime.Now;
            if (DropDownListMonthOfBirth.SelectedIndex > 0 && DropDownListYearOfBirth.SelectedIndex > 0)
            {
                 dateofbirth = Convert.ToDateTime(DropDownListMonthOfBirth.SelectedValue + "/01/" + DropDownListYearOfBirth.SelectedValue);
            }
            if (DropDownListMonthOfExam.SelectedIndex > 0 && DropDownListYearOfExam.SelectedIndex > 0)
            {
                 dateofexam = Convert.ToDateTime(DropDownListMonthOfExam.SelectedValue + "/01/" + DropDownListYearOfExam.SelectedValue);
            }
            if (DropDownListMonthOfFollowUp.SelectedIndex > 0 && DropDownListYearOfFollowUp.SelectedIndex > 0)
            {
                 dateoffollowexam = Convert.ToDateTime(DropDownListMonthOfFollowUp.SelectedValue + "/01/" + DropDownListYearOfFollowUp.SelectedValue);
            }
            Participant participant = pim.Participant.First(p => p.ParticipantID == ctx.ParticipantID);
                  var pqrsselected = pim.ParticipantModule.First(p => p.ParticipantID == participant.ParticipantID);
                  bool pqrsused = Convert.ToBoolean(pqrsselected.PQRS);
                  bool pqrsenabled = Convert.ToBoolean(System.Configuration.ConfigurationManager.AppSettings["PQRSEnabled"]);
                  //if (pqrsused && pqrsenabled && ViewState["PQRSBroken"]==null)
                  //{
                     

                  //    if (DropDownListPQRSVisitMonth.SelectedIndex == 0) {
                  //        //ERR += "DropDownPQRSVisitMonth=BLANK;";
                  //        LabelPQRSVisitMonth.Visible = true;
                  //        ChartCompleted = false;
                  //    }
                  //    if (DropDownListPQRSVisitYear.SelectedIndex == 0) {
                  //        //ERR += "DropDownPQRSVisitYear=BLANK;";
                  //        LabelPQRSVisitYear.Visible = true;
                  //        ChartCompleted = false;
                  //    }


                  //    if (String.IsNullOrEmpty(PQRSMedicare.SelectedValue)) {
                  //        LabelPQRSMedicare.Visible = true;
                  //        ChartCompleted = false;
                  //    }
                  //    if (String.IsNullOrEmpty(PQRSDiagnosis.SelectedValue)) {
                  //        LabelPQRSDiagnosis.Visible = true;
                  //        ChartCompleted = false;
                  //    }
                  //    else if (PQRSDiagnosis.SelectedValue.Equals("1")) {
                  //        if (String.IsNullOrEmpty(PQRSMacularExamination.SelectedValue)) {
                  //            LabelPQRSMacularExamination.Visible = true;
                  //            ChartCompleted = false;
                  //        }
                  //        else if (PQRSMacularExamination.SelectedValue.Equals("1")) {
                  //            if (String.IsNullOrEmpty(PQRSMacularThickening.SelectedValue)) {
                  //                LabelPQRSMacularThickening.Visible = true;
                  //                ChartCompleted = false;
                  //            }
                  //            else if (PQRSMacularThickening.SelectedValue.Equals("1")) {
                  //                if (String.IsNullOrEmpty(PQRSMacularDocumented.SelectedValue)) {
                  //                    LabelPQRSMacularDocumented.Visible = true;
                  //                    ChartCompleted = false;
                  //                }
                  //                else if (PQRSMacularDocumented.SelectedValue.Equals("0")) {
                  //                    if (String.IsNullOrEmpty(PQRSMacularSeverityReason.SelectedValue)) {
                  //                        //LabelButtonPQRSMacular.Visible = true;
                  //                        ChartCompleted = false;
                  //                    }
                  //                }
                  //            }
                  //        }
                  //    }
                  //}
           
           if(RadioButtonListRBGender.SelectedIndex == -1)
            {
               //ERR += "RadioButtonListRBGender=BLANK;";
               LabelRBGender.Visible = true;
            ChartCompleted = false;
            }
            if(RadioButtonListRBEyeWithExudativeAMD.SelectedIndex == -1)
            {
            LabelRBEyeWithExudativeAMD.Visible = true;
            ChartCompleted = false;
            }
            if(RadioButtonListRBSmokingStatus.SelectedIndex == -1)
            {
            LabelRBSmokingStatus.Visible = true;
            ChartCompleted = false;
            }
            if(RadioButtonListRBExamAMDFellowEye.SelectedIndex == -1)
            {
            LabelRBExamAMDFellowEye.Visible = true;
            ChartCompleted = false;
            }
            if(RadioButtonListRBInterval.SelectedIndex == -1)
            {
            LabelRBInterval.Visible = true;
            ChartCompleted = false;
            }
           
            if(RadioButtonListRBPatientCounseledAntioxidant.SelectedIndex == -1)
            {
            LabelRBPatientCounseledAntioxidant.Visible = true;
            ChartCompleted = false;
            }


           int result = DateTime.Compare(dateofbirth, DateTime.Now);
            if(DropDownListMonthOfBirth.SelectedIndex == 0)
            {
                //ERR += "DropDownListMonthOfBirth=BLANK;";
                LabelMonthOfBirth.Visible = true;
            ChartCompleted = false;
            }
            if(DropDownListYearOfBirth.SelectedIndex == 0 && result > 0)
            {
                //ERR += "DropDownListYearOfBirth=BLANK;";
                LabelYearOfBirth.Visible = true;
            ChartCompleted = false;
            }

            if(DropDownListMonthOfExam.SelectedIndex == 0)
            {
                //ERR += "DropDownListMonthOfExam=BLANK;";
                LabelMonthOfExam.Visible = true;
            ChartCompleted = false;
            }
            int result2 = DateTime.Compare(dateofexam, DateTime.Now);
            if(DropDownListYearOfExam.SelectedIndex == 0  && result2 > 0)
            {
            LabelYearOfExam.Visible = true;
            ChartCompleted = false;
            }
            if(DropDownListMonthOfFollowUp.SelectedIndex == 0)
            {
            LabelMonthOfFollowUp.Visible = true;
            ChartCompleted = false;
            }
            int result3 = DateTime.Compare(dateoffollowexam, DateTime.Now);
            if(DropDownListYearOfFollowUp.SelectedIndex == 0 && result3 > 0)
            {
                //ERR += "DropDownListYearOfExam=BLANK;";
                LabelYearOfFollowUp.Visible = true;
            ChartCompleted = false;
            }
            if( DropDownListBCVAInvolvedEye.SelectedIndex==0)
            {
            LabelBCVAInvolvedEye.Visible = true;
            ChartCompleted = false;
            }
            if( DropDownListBCVAFellowEye.SelectedIndex==0)
            {
            LabelBCVAFellowEye.Visible = true;
            ChartCompleted = false;
            }
            if(DropDownListOutcomeTreatedBCVA .SelectedIndex == 0)
            {
            LabelOutcomeTreatedBCVA .Visible = true;
            ChartCompleted = false;
            }
            if(DropDownListOutcomeFellowBCVA.SelectedIndex == 0)
            {
            LabelOutcomeFellowBCVA.Visible = true;
            ChartCompleted = false;
            }



            if(RadioButtonDrugReactionsNo.Checked == false && RadioButtonDrugReactionsYes.Checked == false) 
            {
            LabelDrugReactions.Visible = true;
            ChartCompleted = false;
            }
            if(RadioButtonExamFluoresceinNo.Checked == false && RadioButtonExamFluoresceinYes.Checked == false) 
            {
            LabelExamFluorescein.Visible = true;
            ChartCompleted = false;
            }
            if(RadioButtonExamOCTNo.Checked == false && RadioButtonExamOCTYes.Checked == false) 
            {
            LabelExamOCT.Visible = true;
            ChartCompleted = false;
            }
            if(RadioButtonExamICGNo.Checked == false && RadioButtonExamICGYes.Checked == false) 
            {
            LabelExamICG.Visible = true;
            ChartCompleted = false;
            }

           return ChartCompleted;
       }
}