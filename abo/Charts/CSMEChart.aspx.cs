﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Transactions;
using NetHealthPIMModel;

public partial class abo_CSMEChart : BasePage
{
    public bool PQRSEnabled { get; set; }

    public bool PQRSUsed { get; set; }

    protected void Page_Load(object sender, EventArgs e)
    {
        NetHealthPIMEntities pim1 = new NetHealthPIMEntities();
        var pqrsselected = pim1.ParticipantModule.First(p => p.ParticipantID == ctx.ParticipantID);
        bool selectedornot = Convert.ToBoolean(pqrsselected.PQRS);
        if (!selectedornot)
        {
            PQRSPanel.Visible = false;
        }
        if (!IsPostBack)
        {
            InitializePage();
            PQRSUsed = Convert.ToBoolean(pqrsselected.PQRS);
            PQRSEnabled = Convert.ToBoolean(System.Configuration.ConfigurationManager.AppSettings["PQRSEnabled"]);
        }
    }
    protected void InitializePage()
    {
        ((PIMMasterPage)Page.Master).SetTitle("Chart");

        using (NetHealthPIMEntities pim = new NetHealthPIMEntities())
        {
            int ModuleID = (int)Session[Constants.SESSION_WORKINGMODULEID];
            Module module = pim.Module.First(c => c.ModuleID == ModuleID);
            ((PIMMasterPage)Page.Master).SetHeader(module.ModuleName + " Chart Abstraction");

            ParticipantModuleCycle prev = (ParticipantModuleCycle)Session[Constants.SESSION_PREVIOUSCYCLE];
            String CycleNumber = Request.QueryString["CycleNumber"];

            int cycleID = ctx.ActiveModuleCycleID;
            if (CycleNumber == "1")
            {
                cycleID = prev.ParticipantModuleCycleID;
                LinkButtonBackToDashboard.Visible = true;
                ButtonSubmit.Visible = false;
            }

            ParticipantModuleCycle cycle = pim.ParticipantModuleCycle.First(c => c.ParticipantModuleCycleID == cycleID);
            String numberOfRecord = Request.QueryString["nr"];
            String totalRecords = Request.QueryString["t"];
            if (cycle.CycleNumber == 1)
            {
                ((PIMMasterPage)Page.Master).SetStatus(2);
                LiteralAbstractionNumber.Text = "Initial Abstraction: Chart " + numberOfRecord + " of " + totalRecords;
            }
            else
            {
                ((PIMMasterPage)Page.Master).SetStatus(3);
                LiteralAbstractionNumber.Text = "Second Abstraction: Chart " + numberOfRecord + " of " + totalRecords;
            }
            DropDownListMonthOfBirth.DataSource = CycleManager.getMonthList();
            DropDownListYearOfBirth.DataSource = CycleManager.getDOBYearList(0, 100);
            DropDownListMonthOfExam.DataSource = CycleManager.getMonthList();
            DropDownListYearOfExam.DataSource = CycleManager.getDOBYearList(0, 100);
            DropDownListMonthOfFollowUp.DataSource = CycleManager.getMonthList();
            DropDownListYearOfFollowUp.DataSource = CycleManager.getDOBYearList(0, 100);
            DropDownListInvolvedBCVA.DataSource = CycleManager.getExamValues();
            DropDownListFellowBCVA.DataSource = CycleManager.getExamValues();
            DropDownListOutcomeTreatedBCVA.DataSource = CycleManager.getExamValues();
            DropDownListOutcomeFellowBCVA.DataSource = CycleManager.getExamValues();
            //DropDownListPQRSDOBMonth.DataSource = CycleManager.getMonthList();
            //DropDownListPQRSDOBYear.DataSource = CycleManager.getDOBYearList(0, 100);
            DropDownListPQRSVisitMonth.DataSource = CycleManager.getMonthList();
            DropDownListPQRSVisitYear.DataSource = CycleManager.getDOBYearList(0, 100);
            DataBind();
            DropDownListPQRSVisitYear.Items.Insert(0, new ListItem("Year", "0"));
            DropDownListYearOfBirth.Items.Insert(0, new ListItem("Year", "0"));
            DropDownListYearOfExam.Items.Insert(0, new ListItem("Year", "0"));
            DropDownListYearOfFollowUp.Items.Insert(0, new ListItem("Year", "0"));
            //DropDownListPQRSDOBYear.Items.Insert(0, new ListItem("Year", "0"));
            //DropDownListPQRSVisitYear.Items.Insert(0, new ListItem("Year", "0"));
            string recordIdentifier = Request.QueryString["ri"];
            HiddenFieldRecordIdentifier.Value = recordIdentifier;
            LiteralRecordIdentifier.Text = recordIdentifier;
            var count = pim.ChartAbstractionRetinaCSME.Where(ps => ps.ParticipantModuleCycle.ParticipantModuleCycleID == cycleID & ps.RecordIdentifier == recordIdentifier).Count();
            if (count == 0)
            {
                using (TransactionScope transaction = new TransactionScope())
                {
                    //Module module = pim.Module.First(m => m.ModuleID == Constants.ABO_AMBLYOPIA_MODULEID);

                    NetHealthPIMModel.ChartRegistration chartRegistration = (from cr in pim.ChartRegistration
                                                           where cr.ParticipantModuleCycleID == cycleID
                                                           & cr.RecordIdentifier == recordIdentifier
                                                           & cr.ModuleID == 56///// NEEDS TO BE CHANGED
                                                           select cr).First<NetHealthPIMModel.ChartRegistration>();

                    DateTime initialVisit = Convert.ToDateTime(chartRegistration.InitialVisit);
                    DateTime DOB = Convert.ToDateTime(chartRegistration.DOB);

                    ChartAbstractionRetinaCSME abstractRecord = new ChartAbstractionRetinaCSME();
                    abstractRecord.ParticipantModuleCycle = cycle;
                    abstractRecord.RecordIdentifier = recordIdentifier;
                    abstractRecord.MonthOfBirth = DOB.Month;
                    abstractRecord.YearOfBirth = DOB.Year;

                    //Birthday
                    DropDownListMonthOfBirth.SelectedValue = abstractRecord.MonthOfBirth.ToString();
                    DropDownListYearOfBirth.SelectedValue = abstractRecord.YearOfBirth.ToString();
                    //DropDownListPQRSDOBMonth.SelectedValue = DropDownListMonthOfBirth.SelectedValue;
                    //DropDownListPQRSDOBYear.SelectedValue = DropDownListYearOfBirth.SelectedValue;
                    
                    abstractRecord.CreatedOn = DateTime.Now;
                    abstractRecord.LastUpdateDate = DateTime.Now;
                    pim.SaveChanges();

                    transaction.Complete();
                }
            }
            else
            {

                ChartAbstractionRetinaCSME abstractRecord = (from c in pim.ChartAbstractionRetinaCSME
                                                     where c.ParticipantModuleCycle.ParticipantModuleCycleID == cycleID & c.RecordIdentifier == recordIdentifier
                                                             select c).First<ChartAbstractionRetinaCSME>();

                Participant participant = pim.Participant.First(p => p.ParticipantID == ctx.ParticipantID);
                var pqrsselected = pim.ParticipantModule.First(p => p.ParticipantID == participant.ParticipantID);
                bool pqrsused = Convert.ToBoolean(pqrsselected.PQRS);
                bool pqrsenabled = Convert.ToBoolean(System.Configuration.ConfigurationManager.AppSettings["PQRSEnabled"]);
                if (pqrsused && pqrsenabled) {
                    PQRSPanel.Visible = true;
                    //try
                    //{
                    PQRSWebService.PPWebServiceClient client = new PQRSWebService.PPWebServiceClient();
                    client.ClientCredentials.UserName.UserName = "partner:2015";
                    client.ClientCredentials.UserName.Password = "48C1A443-54B0-4D00-A4A2-FE5E46C2F02E";



                    Int32 participantid = client.AddUser(0, "0", participant.ParticipantFirstName, participant.ParticipantLastName, participant.ParticipantEmailAddress, participant.ParticipantEmailAddress, "abo2015", "ABO"); 

                        String patient_id = Request.QueryString["ri"].ToString();
                        if (!(cycle.CycleNumber == 1))
                        {
                            patient_id = "C2" + patient_id;
                        }


                        DataTable values = client.GetPatientVisit(participantid, patient_id, 2015);
                        if (values != null && values.Rows.Count>0)
                        {
                            DataRow visit = values.Rows[0];
                            DropDownListPQRSVisitMonth.SelectedValue = visit["VisitDate"].ToString().Substring(0, 2).TrimStart('0');
                            DropDownListPQRSVisitYear.SelectedValue = visit["VisitDate"].ToString().Substring(6);
                            PQRSMedicare.SelectedValue = (Boolean)visit["IsMedicarePartB"] ? "1" : "0";

                            //  try {
                            String question_id = "";
                            //question_id = "Q2447"; //Q2489
                            //if (visit[question_id] != null)
                            //{ //Age 18.1, 19.1, 117.1
                            //    DropDownListPQRSDOBMonth.SelectedValue = visit[question_id].ToString().Substring(0, 2).TrimStart('0');
                            //    DropDownListPQRSDOBYear.SelectedValue = visit[question_id].ToString().Substring(6);
                            //}
                            try {question_id = "Q3241"; //Diagnosis 18.2, 19.2
                            if (visit[question_id] != null) PQRSDiagnosis.SelectedValue = visit[question_id].ToString();
							} catch {}
                             try {question_id = "Q3242"; //Diagnosis 18.3, 19.3
                            if (visit[question_id] != null) PQRSEncounter.SelectedValue = visit[question_id].ToString();
							} catch {}
                             try {question_id = "Q3243"; //YesNo 18.4, 19.4
                            if (visit[question_id] != null) PQRSExamPerformed.SelectedValue = visit[question_id].ToString();
							} catch {}
							//   19 - 5
							 try {question_id = "Q3244"; //Did the exam include documentation of the presence or absence of macular edema? (Denominator Criteria)
                            if (visit[question_id] != null) PQRSDocumentationMacularEdema.SelectedValue = visit[question_id].ToString();
							} catch {}
							//   19 - 6 - Was communication with the physician managing ongoing diabetes care performed on the Visit Date or within 12 months prior to Visit Date?
							 try {question_id = "Q3245";
                            if (visit[question_id] != null) PQRSDiabetesCommunication.SelectedValue = visit[question_id].ToString();
							} catch {}
							//   19 - 7 - If communication with the physician managing on-going diabetes care was not done, was this due to a documented reason?
							try {question_id = "Q3246";
                            if (visit[question_id] != null) PQRSDiabetesCommunicationReason.SelectedValue = visit[question_id].ToString();
							} catch {}

                             try {question_id = "Q3465"; //Diagnosis 117.2
                            if (visit[question_id] != null) PQRSDiagnosis2.SelectedValue = visit[question_id].ToString();
							} catch {}
                             try {question_id = "Q3466"; //Diagnosis 117.3
                            if (visit[question_id] != null) PQRSEncounter2.SelectedValue = visit[question_id].ToString();
							} catch {}
                             try {question_id = "Q3467"; //Diagnosis 117.4
                            if (visit[question_id] != null) DialetedEyeExam.SelectedValue = visit[question_id].ToString();
							} catch {}
							//  117- 5  If none of the above procedures were performed, was this due to the patient having low risk for retinopathy?
                             try {question_id = "Q4586"; // 117.5
                            if (visit[question_id] != null) PQRSLowRisk.SelectedValue = visit[question_id].ToString();
							} catch {}
							
							// }
                            // catch { }
                        }
                    //}
                    //catch
                    //{
                    //    ViewState["PQRSBroken"] = true;
                    //}
                }
                if (abstractRecord.ExamCSTNA != null) CheckBoxExamCSTNA.Checked = ((bool)abstractRecord.ExamCSTNA);
                if (abstractRecord.TherapyBevacizumab != null) CheckBoxTherapyBevacizumab.Checked = ((bool)abstractRecord.TherapyBevacizumab);
                if (abstractRecord.TherapyRanibizumab != null) CheckBoxTherapyRanibizumab.Checked = ((bool)abstractRecord.TherapyRanibizumab);
                if (abstractRecord.TherapyFocalLaser != null) CheckBoxTherapyFocalLaser.Checked = ((bool)abstractRecord.TherapyFocalLaser);
                if (abstractRecord.TherapyIntravitrealSteroids != null) CheckBoxTherapyIntravitrealSteroids.Checked = ((bool)abstractRecord.TherapyIntravitrealSteroids);
                if (abstractRecord.TherapyVitrectomyLaser != null) CheckBoxTherapyVitrectomyLaser.Checked = ((bool)abstractRecord.TherapyVitrectomyLaser);
                if (abstractRecord.TherapyOther != null) CheckBoxTherapyOther.Checked = ((bool)abstractRecord.TherapyOther);
                if (abstractRecord.TherapyNone != null) CheckBoxTherapyNone.Checked = ((bool)abstractRecord.TherapyNone);
                if (abstractRecord.OutcomeCSTNA != null) CheckBoxOutcomeCSTNA.Checked = ((bool)abstractRecord.OutcomeCSTNA);
                if (abstractRecord.FocalGridLaser != null) CheckBoxFocalGridLaser.Checked = ((bool)abstractRecord.FocalGridLaser);

                if (abstractRecord.FocalGridLaser != null) CheckBoxTherapyNone.Checked = ((bool)abstractRecord.FocalGridLaser);
                if (abstractRecord.Bevacizumab != null) CheckBoxBevacizumab.Checked = ((bool)abstractRecord.Bevacizumab);
                if (abstractRecord.IntravitrealSteroids != null) CheckBoxIntravitrealSteroids.Checked = ((bool)abstractRecord.IntravitrealSteroids);
                if (abstractRecord.PPVx != null) CheckBoxPPVx.Checked = ((bool)abstractRecord.PPVx);
                if (abstractRecord.Observation != null) CheckBoxObservation.Checked = ((bool)abstractRecord.Observation);
                if (abstractRecord.ObservationOther != null) CheckBoxObservationOther.Checked = ((bool)abstractRecord.ObservationOther);



                if (abstractRecord.RBGender != null) RadioButtonListRBGender.SelectedValue = abstractRecord.RBGender.ToString();
                if (abstractRecord.RBHistoryDiabetic != null) RadioButtonListRBHistoryDiabetic.SelectedValue = abstractRecord.RBHistoryDiabetic.ToString();
                if (abstractRecord.RBHistoryCataractSurgery != null) RadioButtonListRBHistoryCataractSurgery.SelectedValue = abstractRecord.RBHistoryCataractSurgery.ToString();
                if (abstractRecord.RBExamEyeID != null) RadioButtonListRBExamEyeID.SelectedValue = abstractRecord.RBExamEyeID.ToString();
                if (abstractRecord.RBDilatedFundusExam != null) RadioButtonListRBDilatedFundusExam.SelectedValue = abstractRecord.RBDilatedFundusExam.ToString();
                if (abstractRecord.RBExamMacularEdema != null) RadioButtonListRBExamMacularEdema.SelectedValue = abstractRecord.RBExamMacularEdema.ToString();
                if (abstractRecord.RBTypeRetinopathy != null) RadioButtonListRBTypeRetinopathy.SelectedValue = abstractRecord.RBTypeRetinopathy.ToString();
                if (abstractRecord.RBPatientCounseled != null) RadioButtonListRBPatientCounseled.SelectedValue = abstractRecord.RBPatientCounseled.ToString();
                
                if (abstractRecord.RBMgntPhysCommunication != null) RadioButtonListRBMgntPhysCommunication.SelectedValue = abstractRecord.RBMgntPhysCommunication.ToString();
                if (abstractRecord.RBOutcomeStatusCSME != null) RadioButtonListRBOutcomeStatusCSME.SelectedValue = abstractRecord.RBOutcomeStatusCSME.ToString();
                if (abstractRecord.IntravitrealCorticosteroids != null) RadioButtonListIntravitrealCorticosteroids.SelectedValue = abstractRecord.IntravitrealCorticosteroids.ToString();

                //Birthday
                if (abstractRecord.MonthOfBirth != null) DropDownListMonthOfBirth.SelectedValue = abstractRecord.MonthOfBirth.ToString();
                if (abstractRecord.YearOfBirth != null) DropDownListYearOfBirth.SelectedValue = abstractRecord.YearOfBirth.ToString();
                //DropDownListPQRSDOBMonth.SelectedValue = DropDownListMonthOfBirth.SelectedValue;
                //DropDownListPQRSDOBYear.SelectedValue = DropDownListYearOfBirth.SelectedValue;

                if (abstractRecord.MonthOfExam != null) DropDownListMonthOfExam.SelectedValue = abstractRecord.MonthOfExam.ToString();
                if (abstractRecord.YearOfExam != null) DropDownListYearOfExam.SelectedValue = abstractRecord.YearOfExam.ToString();
                if (abstractRecord.MonthOfFollowUp != null) DropDownListMonthOfFollowUp.SelectedValue = abstractRecord.MonthOfFollowUp.ToString();
                if (abstractRecord.YearOfFollowUp != null) DropDownListYearOfFollowUp.SelectedValue = abstractRecord.YearOfFollowUp.ToString();
                if (abstractRecord.InvolvedBCVA != null) DropDownListInvolvedBCVA.SelectedValue = abstractRecord.InvolvedBCVA.ToString();
                if (abstractRecord.FellowBCVA != null) DropDownListFellowBCVA.SelectedValue = abstractRecord.FellowBCVA.ToString();
                if (abstractRecord.OutcomeTreatedBCVA != null) DropDownListOutcomeTreatedBCVA.SelectedValue = abstractRecord.OutcomeTreatedBCVA.ToString();
                if (abstractRecord.OutcomeFellowBCVA != null) DropDownListOutcomeFellowBCVA.SelectedValue = abstractRecord.OutcomeFellowBCVA.ToString();

               




                if (abstractRecord.ExamCST != null) TextBoxExamCST.Text = abstractRecord.ExamCST.ToString();
                if (abstractRecord.MgntOptionOtherText != null) TextBoxMgntOptionOtherText.Text = abstractRecord.MgntOptionOtherText.ToString();
                if (abstractRecord.TherapyBevacizumabNbr != null) TextBoxTherapyBevacizumabNbr.Text = abstractRecord.TherapyBevacizumabNbr.ToString();
                if (abstractRecord.TherapyRanibizumabNbr != null) TextBoxTherapyRanibizumabNbr.Text = abstractRecord.TherapyRanibizumabNbr.ToString();
                if (abstractRecord.TherapyFocalLaserNbr != null) TextBoxTherapyFocalLaserNbr.Text = abstractRecord.TherapyFocalLaserNbr.ToString();
                if (abstractRecord.TherapyIntravitrealSteroidsNbr != null) TextBoxTherapyIntravitrealSteroidsNbr.Text = abstractRecord.TherapyIntravitrealSteroidsNbr.ToString();
                if (abstractRecord.TherapyVitrectomyNbr != null) TextBoxTherapyVitrectomyNbr.Text = abstractRecord.TherapyVitrectomyNbr.ToString();
                if (abstractRecord.TherapyOtherNbr != null) TextBoxTherapyOtherNbr.Text = abstractRecord.TherapyOtherNbr.ToString();
                if (abstractRecord.TherapyOtherText != null) TextBoxTherapyOtherText.Text = abstractRecord.TherapyOtherText.ToString();
                if (abstractRecord.OutcomeCST != null) TextBoxOutcomeCST.Text = abstractRecord.OutcomeCST.ToString();





            }
        }
    }


    public void savedata() {
        using (NetHealthPIMEntities pim = new NetHealthPIMEntities()) {
            int ModuleID = (int)Session[Constants.SESSION_WORKINGMODULEID];
            int cycleID = ctx.ActiveModuleCycleID;
            ParticipantModuleCycle cycle = pim.ParticipantModuleCycle.First(c => c.ParticipantModuleCycleID == cycleID);
            string recordIdentifier = HiddenFieldRecordIdentifier.Value;
            using (TransactionScope transaction = new TransactionScope()) {
                ChartAbstractionRetinaCSME abstractRecord = (from c in pim.ChartAbstractionRetinaCSME
                                                             where c.ParticipantModuleCycle.ParticipantModuleCycleID == cycleID & c.RecordIdentifier == recordIdentifier
                                                             select c).First<ChartAbstractionRetinaCSME>();

                abstractRecord.ExamCSTNA = CheckBoxExamCSTNA.Checked;
                abstractRecord.TherapyBevacizumab = CheckBoxTherapyBevacizumab.Checked;
                abstractRecord.TherapyRanibizumab = CheckBoxTherapyRanibizumab.Checked;
                abstractRecord.TherapyFocalLaser = CheckBoxTherapyFocalLaser.Checked;
                abstractRecord.TherapyIntravitrealSteroids = CheckBoxTherapyIntravitrealSteroids.Checked;
                abstractRecord.TherapyVitrectomyLaser = CheckBoxTherapyVitrectomyLaser.Checked;
                abstractRecord.TherapyOther = CheckBoxTherapyOther.Checked;
                abstractRecord.TherapyNone = CheckBoxTherapyNone.Checked;
                abstractRecord.OutcomeCSTNA = CheckBoxOutcomeCSTNA.Checked;
                abstractRecord.FocalGridLaser = CheckBoxFocalGridLaser.Checked;
                abstractRecord.TherapyNone = CheckBoxTherapyNone.Checked;
                abstractRecord.Bevacizumab = CheckBoxBevacizumab.Checked;
                abstractRecord.IntravitrealSteroids = CheckBoxIntravitrealSteroids.Checked;
                abstractRecord.PPVx = CheckBoxPPVx.Checked;
                abstractRecord.Observation = CheckBoxObservation.Checked;
                abstractRecord.ObservationOther = CheckBoxObservationOther.Checked;

                if (RadioButtonListRBGender.SelectedIndex != -1) abstractRecord.RBGender = Convert.ToInt32(RadioButtonListRBGender.SelectedValue);
                else abstractRecord.RBGender = 0;
                if (RadioButtonListRBHistoryDiabetic.SelectedIndex != -1) abstractRecord.RBHistoryDiabetic = Convert.ToInt32(RadioButtonListRBHistoryDiabetic.SelectedValue);
                else abstractRecord.RBHistoryDiabetic = 0;
                if (RadioButtonListRBHistoryCataractSurgery.SelectedIndex != -1) abstractRecord.RBHistoryCataractSurgery = Convert.ToInt32(RadioButtonListRBHistoryCataractSurgery.SelectedValue);
                else abstractRecord.RBHistoryCataractSurgery = 0;
                if (RadioButtonListRBExamEyeID.SelectedIndex != -1) abstractRecord.RBExamEyeID = Convert.ToInt32(RadioButtonListRBExamEyeID.SelectedValue);
                else abstractRecord.RBExamEyeID = 0;
                if (RadioButtonListRBDilatedFundusExam.SelectedIndex != -1) abstractRecord.RBDilatedFundusExam = Convert.ToInt32(RadioButtonListRBDilatedFundusExam.SelectedValue);
                else abstractRecord.RBDilatedFundusExam = 0;
                if (RadioButtonListRBExamMacularEdema.SelectedIndex != -1) abstractRecord.RBExamMacularEdema = Convert.ToInt32(RadioButtonListRBExamMacularEdema.SelectedValue);
                else abstractRecord.RBExamMacularEdema = 0;
                if (RadioButtonListRBTypeRetinopathy.SelectedIndex != -1) abstractRecord.RBTypeRetinopathy = Convert.ToInt32(RadioButtonListRBTypeRetinopathy.SelectedValue);
                else abstractRecord.RBTypeRetinopathy = 0;
                if (RadioButtonListRBPatientCounseled.SelectedIndex != -1) abstractRecord.RBPatientCounseled = Convert.ToInt32(RadioButtonListRBPatientCounseled.SelectedValue);
                else abstractRecord.RBPatientCounseled = 0;

                if (RadioButtonListRBMgntPhysCommunication.SelectedIndex != -1) abstractRecord.RBMgntPhysCommunication = Convert.ToInt32(RadioButtonListRBMgntPhysCommunication.SelectedValue);
                else abstractRecord.RBMgntPhysCommunication = 0;
                if (RadioButtonListRBOutcomeStatusCSME.SelectedIndex != -1) abstractRecord.RBOutcomeStatusCSME = Convert.ToInt32(RadioButtonListRBOutcomeStatusCSME.SelectedValue);
                else abstractRecord.RBOutcomeStatusCSME = 0;
                if (RadioButtonListIntravitrealCorticosteroids.SelectedIndex != -1) abstractRecord.IntravitrealCorticosteroids = Convert.ToInt32(RadioButtonListIntravitrealCorticosteroids.SelectedValue);
                else abstractRecord.IntravitrealCorticosteroids = 0;


                if (DropDownListMonthOfBirth.SelectedIndex > 0) abstractRecord.MonthOfBirth = Convert.ToInt32(DropDownListMonthOfBirth.SelectedValue);
                else abstractRecord.MonthOfBirth = 0;
                if (DropDownListYearOfBirth.SelectedIndex > 0) abstractRecord.YearOfBirth = Convert.ToInt32(DropDownListYearOfBirth.SelectedValue);
                else abstractRecord.YearOfBirth = 0;
                if (DropDownListMonthOfExam.SelectedIndex > 0) abstractRecord.MonthOfExam = Convert.ToInt32(DropDownListMonthOfExam.SelectedValue);
                else abstractRecord.MonthOfExam = 0;
                if (DropDownListYearOfExam.SelectedIndex > 0) abstractRecord.YearOfExam = Convert.ToInt32(DropDownListYearOfExam.SelectedValue);
                else abstractRecord.YearOfExam = 0;
                if (DropDownListMonthOfFollowUp.SelectedIndex > 0) abstractRecord.MonthOfFollowUp = Convert.ToInt32(DropDownListMonthOfFollowUp.SelectedValue);
                else abstractRecord.MonthOfFollowUp = 0;
                if (DropDownListYearOfFollowUp.SelectedIndex > 0) abstractRecord.YearOfFollowUp = Convert.ToInt32(DropDownListYearOfFollowUp.SelectedValue);
                else abstractRecord.YearOfFollowUp = 0;
                if (DropDownListInvolvedBCVA.SelectedIndex > 0) abstractRecord.InvolvedBCVA = Convert.ToInt32(DropDownListInvolvedBCVA.SelectedValue);
                else abstractRecord.InvolvedBCVA = 0;
                if (DropDownListFellowBCVA.SelectedIndex > 0) abstractRecord.FellowBCVA = Convert.ToInt32(DropDownListFellowBCVA.SelectedValue);
                else abstractRecord.FellowBCVA = 0;
                if (DropDownListOutcomeTreatedBCVA.SelectedIndex > 0) abstractRecord.OutcomeTreatedBCVA = Convert.ToInt32(DropDownListOutcomeTreatedBCVA.SelectedValue);
                else abstractRecord.OutcomeTreatedBCVA = 0;
                if (DropDownListOutcomeFellowBCVA.SelectedIndex > 0) abstractRecord.OutcomeFellowBCVA = Convert.ToInt32(DropDownListOutcomeFellowBCVA.SelectedValue);
                else abstractRecord.OutcomeFellowBCVA = 0;






                try {
                    if (TextBoxExamCST.Text != null)
                        abstractRecord.ExamCST = Convert.ToDouble(TextBoxExamCST.Text);
                }
                catch (Exception ex) {
                    abstractRecord.ExamCST = null;
                    TextBoxExamCST.Text = "";
                }
                try {
                    if (TextBoxMgntOptionOtherText.Text != null)
                        abstractRecord.MgntOptionOtherText = TextBoxMgntOptionOtherText.Text;
                }
                catch (Exception ex) {
                    abstractRecord.MgntOptionOtherText = null;
                    TextBoxMgntOptionOtherText.Text = "";
                }
                try {
                    if (TextBoxTherapyBevacizumabNbr.Text != null)
                        abstractRecord.TherapyBevacizumabNbr = Convert.ToInt32(TextBoxTherapyBevacizumabNbr.Text);
                }
                catch (Exception ex) {
                    abstractRecord.TherapyBevacizumabNbr = null;
                    TextBoxTherapyBevacizumabNbr.Text = "";
                }
                try {
                    if (TextBoxTherapyRanibizumabNbr.Text != null)
                        abstractRecord.TherapyRanibizumabNbr = Convert.ToInt32(TextBoxTherapyRanibizumabNbr.Text);
                }
                catch (Exception ex) {
                    abstractRecord.TherapyRanibizumabNbr = null;
                    TextBoxTherapyRanibizumabNbr.Text = "";
                }
                try {
                    if (TextBoxTherapyFocalLaserNbr.Text != null)
                        abstractRecord.TherapyFocalLaserNbr = Convert.ToInt32(TextBoxTherapyFocalLaserNbr.Text);
                }
                catch (Exception ex) {
                    abstractRecord.TherapyFocalLaserNbr = null;
                    TextBoxTherapyFocalLaserNbr.Text = "";
                }
                try {
                    if (TextBoxTherapyIntravitrealSteroidsNbr.Text != null)
                        abstractRecord.TherapyIntravitrealSteroidsNbr = Convert.ToInt32(TextBoxTherapyIntravitrealSteroidsNbr.Text);
                }
                catch (Exception ex) {
                    abstractRecord.TherapyIntravitrealSteroidsNbr = null;
                    TextBoxTherapyIntravitrealSteroidsNbr.Text = "";
                }
                try {
                    if (TextBoxTherapyVitrectomyNbr.Text != null)
                        abstractRecord.TherapyVitrectomyNbr = Convert.ToInt32(TextBoxTherapyVitrectomyNbr.Text);
                }
                catch (Exception ex) {
                    abstractRecord.TherapyVitrectomyNbr = null;
                    TextBoxTherapyVitrectomyNbr.Text = "";
                }
                try {
                    if (TextBoxTherapyOtherNbr.Text != null)
                        abstractRecord.TherapyOtherNbr = Convert.ToInt32(TextBoxTherapyOtherNbr.Text);
                }
                catch (Exception ex) {
                    abstractRecord.TherapyOtherNbr = null;
                    TextBoxTherapyOtherNbr.Text = "";
                }
                try {
                    if (TextBoxTherapyOtherText.Text != null)
                        abstractRecord.TherapyOtherText = TextBoxTherapyOtherText.Text;
                }
                catch (Exception ex) {
                    abstractRecord.TherapyOtherText = null;
                    TextBoxTherapyOtherText.Text = "";
                }
                try {
                    if (TextBoxOutcomeCST.Text != null)
                        abstractRecord.OutcomeCST = Convert.ToDouble(TextBoxOutcomeCST.Text);
                }
                catch (Exception ex) {
                    abstractRecord.OutcomeCST = null;
                    TextBoxOutcomeCST.Text = "";
                }

                Participant participant = pim.Participant.First(p => p.ParticipantID == ctx.ParticipantID);
                var pqrsselected = pim.ParticipantModule.First(p => p.ParticipantID == participant.ParticipantID);
                Boolean pqrsused = Convert.ToBoolean(pqrsselected.PQRS);
                Int32 visityear = Convert.ToInt32("0" + DropDownListPQRSVisitYear.SelectedValue);
                bool pqrsenabled = Convert.ToBoolean(System.Configuration.ConfigurationManager.AppSettings["PQRSEnabled"]);
                if (pqrsused && visityear == 2015 && pqrsenabled) {

                    try
                    {

                        PQRSWebService.PPWebServiceClient client = new PQRSWebService.PPWebServiceClient();
                        client.ClientCredentials.UserName.UserName = "partner:2015";
                        client.ClientCredentials.UserName.Password = "48C1A443-54B0-4D00-A4A2-FE5E46C2F02E";



                        Int32 participantid = client.AddUser(0, "0", participant.ParticipantFirstName, participant.ParticipantLastName, participant.ParticipantEmailAddress, participant.ParticipantEmailAddress, "abo2015", "ABO"); 

                        String visit_date = DropDownListPQRSVisitMonth.SelectedValue.PadLeft(2, '0') + "/01/";
                        visit_date += DropDownListPQRSVisitYear.SelectedValue;
                        Boolean is_medicare = PQRSMedicare.SelectedValue.Equals("1");

                        DataTable values = new DataTable("PQRSPROVisit");
                        values.Columns.Add("MeasureID", typeof(Int32));
                        values.Columns.Add("QuestionType", typeof(String));
                        values.Columns.Add("QuestionID", typeof(Int32));
                        values.Columns.Add("QuestionIDSuffix", typeof(String));
                        values.Columns.Add("Value", typeof(String));
                        values.Columns.Add("CPT", typeof(String));
                        values.Columns.Add("Modifier", typeof(String));
                        values.Columns.Add("SubmissionYear", typeof(Int32));
                        


                        //AddVisitDetail(ref values, 19, 2448, dob);
						AddVisitDetail(ref values, 19, 3240, "1");
                        AddVisitDetail(ref values, 19, 3241, PQRSDiagnosis.SelectedValue);
                        AddVisitDetail(ref values, 19, 3242, PQRSEncounter.SelectedValue);
                        AddVisitDetail(ref values, 19, 3243, PQRSExamPerformed.SelectedValue);
                        AddVisitDetail(ref values, 19, 3244, PQRSDocumentationMacularEdema.SelectedValue);
                        AddVisitDetail(ref values, 19, 3245, PQRSDiabetesCommunication.SelectedValue);
                        AddVisitDetail(ref values, 19, 3246, PQRSDiabetesCommunicationReason.SelectedValue);

                        //AddVisitDetail(ref values, 117, 2519, dob);
						
						//   3464 = 117.1   is patient aged 18 through 75
						String dob = DropDownListMonthOfBirth.SelectedValue.PadLeft(2, '0') + "/01/";
                        dob += DropDownListYearOfBirth.SelectedValue;
						//Date dob_date = Convert.ToDate(dob);
						Int32 Age = Convert.ToInt32((Convert.ToDateTime(visit_date) - Convert.ToDateTime(dob)).TotalDays / 365);
						if (Age > 75) {
							AddVisitDetail(ref values, 117, 3464, "0");
						}
						else
						{
							AddVisitDetail(ref values, 117, 3464, "1");
						}
                        AddVisitDetail(ref values, 117, 3465, PQRSDiagnosis2.SelectedValue);
                        AddVisitDetail(ref values, 117, 3466, PQRSEncounter2.SelectedValue);
                        AddVisitDetail(ref values, 117, 3467, DialetedEyeExam.SelectedValue);
						AddVisitDetail(ref values, 117, 4586, PQRSLowRisk.SelectedValue);

                        String patient_id = Request.QueryString["ri"].ToString();
                        if (!(cycle.CycleNumber == 1))
                        {
                            patient_id = "C2" + patient_id;
                        }

                        client.AddChart((Int32)participantid, 0, patient_id, visit_date, is_medicare, values);
                        client.Close();
                    }
                    catch
                    {
                        ViewState["PQRSBroken"] = true;
                    }
                }

                abstractRecord.LastUpdateDate = DateTime.Now;
                bool ChartCompleted = isComplete();
                abstractRecord.Complete = ChartCompleted;
                var chartRegistration = (from cr in pim.ChartRegistration
                                         where cr.ParticipantModuleCycleID == cycle.ParticipantModuleCycleID
                                          & cr.ModuleID == ModuleID
                                          & cr.RecordIdentifier == recordIdentifier
                                         select cr).First<NetHealthPIMModel.ChartRegistration>();
                chartRegistration.AbstractCompleted = ChartCompleted;


                if ((DropDownListYearOfBirth.SelectedIndex > 0) && (DropDownListYearOfBirth.SelectedIndex > 0)) {
                    chartRegistration.DOB = abstractRecord.MonthOfBirth + "/" + abstractRecord.YearOfBirth;
                }
                if ((DropDownListMonthOfExam.SelectedIndex > 0) && (DropDownListYearOfExam.SelectedIndex > 0)) {
                    chartRegistration.InitialVisit = abstractRecord.MonthOfExam + "/" + abstractRecord.YearOfExam;
                }
                pim.SaveChanges();

                transaction.Complete();




            }

            CycleManager.UpdateParticipantCompletedModules(cycleID, ModuleID);
        }
    }

    void AddVisitDetail(ref DataTable values, int measure_id, int question_id, string value) { AddVisitDetail(ref values, measure_id, question_id, value, "", "", ""); }
    void AddVisitDetail(ref DataTable values, int measure_id, int question_id, string value, string cpt, string modifier, string suffix) {
        if (!String.IsNullOrEmpty(value)) {
            DataRow detail = values.NewRow();
            detail["MeasureID"] = measure_id;
            detail["QuestionType"] = (measure_id > 0 ? "M" : "G");
            detail["QuestionID"] = question_id;
            detail["QuestionIDSuffix"] = suffix;
            detail["Value"] = value;
            detail["CPT"] = cpt;
            detail["Modifier"] = modifier;
            detail["SubmissionYear"] = Convert.ToInt32(System.Configuration.ConfigurationManager.AppSettings["PQRSYear"]);
            values.Rows.Add(detail);
        }
    }

    protected void ButtonSubmit_Click(object sender, EventArgs e)
    {
        savedata();
        isComplete();
        if (!isComplete())
        {
            isComplete();
            string script = " var answer = confirm('All fields are not complete.  Are you sure you want to submit?'); if (answer==true) window.location = '../PatientChartRegistration.aspx';";
            ScriptManager.RegisterStartupScript(this, GetType(),
                          "ServerControlScript", script, true);
        }
        else
        {

            Response.Redirect("../PatientChartRegistration.aspx");
        }
    }

    protected bool isComplete() {

        bool ChartCompleted = true;

        LabelRBGender.Visible = false;

        LabelRBHistoryDiabetic.Visible = false;

        LabelRBHistoryCataractSurgery.Visible = false;

        LabelRBExamEyeID.Visible = false;

        LabelRBDilatedFundusExam.Visible = false;

        LabelRBExamMacularEdema.Visible = false;

        LabelRBTypeRetinopathy.Visible = false;

        LabelRBPatientCounseled.Visible = false;

        LabelRBManagementOption.Visible = false;

        LabelRBMgntPhysCommunication.Visible = false;

        LabelRBOutcomeStatusCSME.Visible = false;
        //LabelPQRSMacularNo.Visible = false;
        LabelPQRSCommDiabetesPhysician.Visible = false;
        LabelDiabetesCommunicationNo.Visible = false;
        LabelPQRSDiagnosis2.Visible = false;
        LabelPQRSEncounter2.Visible = false;
        LabelPQRSExamDone.Visible = false;



        LabelMonthOfBirth.Visible = false;

        LabelYearOfBirth.Visible = false;

        LabelMonthOfExam.Visible = false;

        LabelYearOfExam.Visible = false;

        LabelMonthOfFollowUp.Visible = false;

        LabelYearOfFollowUp.Visible = false;

        LabelInvolvedBCVA.Visible = false;

        LabelFellowBCVA.Visible = false;

        //LabelPQRSDOBMonth.Visible = false;
        //LabelPQRSDOBYear.Visible = false;

        LabelPQRSMedicare.Visible = false;
        LabelPQRSDiagnosis.Visible = false;

        LabelPQRSEncounter.Visible = false;

        LabelPQRSVisitMonth.Visible = false;

        LabelPQRSVisitYear.Visible = false;

        LabelPQRSExamPerformed.Visible = false;


        //LabelPQRSEdemaPresence.Visible = false;
        LabelExamCST.Visible = false;

        LabelOutcomeTreatedBCVA.Visible = false;

        LabelOutcomeFellowBCVA.Visible = false;

        LabelOutcomeCST.Visible = false;


        Labelinitialfol.Visible = false;

        Labelinitialage.Visible = false;
        Labelinitialvis.Visible = false;
        NetHealthPIMEntities pim = new NetHealthPIMEntities();
        var moduleinfo = (from c in pim.Module where c.ModuleID == 56 select new { c.RegistrationFollowUpVisitMonths, c.RegistrationInitialVisitMonths, c.RegistrationMinAge }).FirstOrDefault();
        string message = "Patient must be  at least " + moduleinfo.RegistrationMinAge + " years old<br />";
        string message1 = "Initial visit must be within " + moduleinfo.RegistrationInitialVisitMonths + " months<br />";
        string message2 = "Follow up visit must be at least " + moduleinfo.RegistrationFollowUpVisitMonths + " months after initial visit<br />";
        if (DropDownListYearOfExam.SelectedIndex != 0 && DropDownListMonthOfExam.SelectedIndex != 0 && DropDownListYearOfFollowUp.SelectedIndex != 0 && DropDownListMonthOfFollowUp.SelectedIndex != 0) {
            DateTime monthofsurgery = new DateTime(Convert.ToInt32(DropDownListYearOfExam.SelectedValue), Convert.ToInt32(DropDownListMonthOfExam.SelectedValue), 01);
            DateTime folowup = new DateTime(Convert.ToInt32(DropDownListYearOfFollowUp.SelectedValue), Convert.ToInt32(DropDownListMonthOfFollowUp.SelectedValue), 01);
            string registrationminage2 = Validation.validateRegistrationFolowUpMonth(monthofsurgery, folowup, Convert.ToInt32(moduleinfo.RegistrationFollowUpVisitMonths), message2);
            string registrationminage = Validation.validateRegistrationMinAge(Convert.ToInt32(DropDownListYearOfExam.SelectedValue), Convert.ToInt32(DropDownListYearOfBirth.SelectedValue), Convert.ToInt32(moduleinfo.RegistrationMinAge), message);
            string registrationminage1 = Validation.validateRegistrationInitialVisitMonth(DateTime.Now, monthofsurgery, Convert.ToInt32(moduleinfo.RegistrationInitialVisitMonths), message1);


            if (registrationminage != "") {
                Labelinitialage.Text = message;
                Labelinitialage.Visible = true;
                ChartCompleted = false;
            }
            if (registrationminage1 != "") {
                Labelinitialvis.Text = message1;
                Labelinitialvis.Visible = true;
                ChartCompleted = false;
            }
            if (registrationminage2 != "") {
                Labelinitialfol.Text = message2;
                Labelinitialfol.Visible = true;
                ChartCompleted = false;
            }
        }

        ///Pqrs Validation
        Participant participant = pim.Participant.First(p => p.ParticipantID == ctx.ParticipantID);
        var pqrsselected = pim.ParticipantModule.First(p => p.ParticipantID == participant.ParticipantID);
        bool pqrsused = Convert.ToBoolean(pqrsselected.PQRS);
        bool pqrsenabled = Convert.ToBoolean(System.Configuration.ConfigurationManager.AppSettings["PQRSEnabled"]);
       /*  if (pqrsused && pqrsenabled && ViewState["PQRSBroken"]==null)
        {
            //if (DropDownListPQRSDOBMonth.SelectedIndex == 0) {
            //    LabelPQRSDOBMonth.Visible = true;
            //    ChartCompleted = false;
            //}
            //if (DropDownListPQRSDOBYear.SelectedIndex == 0) {
            //    LabelPQRSDOBYear.Visible = true;
            //    ChartCompleted = false;
            //}

            if (String.IsNullOrEmpty(PQRSMedicare.SelectedValue)) {
                LabelPQRSMedicare.Visible = true;
                ChartCompleted = false;
            }
            if (String.IsNullOrEmpty(PQRSDiagnosis.SelectedValue)) {
                LabelPQRSDiagnosis.Visible = true;
                ChartCompleted = false;
            }
            if (!PQRSMedicare.SelectedValue.Equals("1") && !PQRSDiagnosis.SelectedValue.Equals("0"))
            {
                if (String.IsNullOrEmpty(PQRSEncounter.SelectedValue))
                {
                    LabelPQRSEncounter.Visible = true;
                    ChartCompleted = false;
                }

                if (DropDownListPQRSVisitMonth.SelectedIndex == 0)
                {
                    LabelPQRSVisitMonth.Visible = true;
                    ChartCompleted = false;
                }
                if (DropDownListPQRSVisitYear.SelectedIndex == 0)
                {
                    LabelPQRSVisitYear.Visible = true;
                    ChartCompleted = false;
                }
                if (String.IsNullOrEmpty(PQRSExamPerformed.SelectedValue))
                {
                    LabelPQRSExamPerformed.Visible = true;
                    ChartCompleted = false;
                }

                if (PQRSExamPerformed.SelectedValue.Equals("1"))
                {
                    //if (String.IsNullOrEmpty(PQRSEdemaPresence.SelectedValue)) {
                    //    LabelPQRSEdemaPresence.Visible = true;
                    //    ChartCompleted = false;
                    //}
                    //else if (PQRSEdemaPresence.SelectedValue.Equals("0") && String.IsNullOrEmpty(PQRSEdemaPresenceReason.SelectedValue)) {
                    //    LabelPQRSMacularNo.Visible = true;
                    //    ChartCompleted = false;
                    //}
                }

                if (PQRSEncounter.SelectedValue.Equals("1") && PQRSExamPerformed.SelectedValue.Equals("1"))
                {
                    if (String.IsNullOrEmpty(PQRSDiabetesCommunication.SelectedValue))
                    {
                        LabelPQRSCommDiabetesPhysician.Visible = true;
                        ChartCompleted = false;
                    }
                    else if (PQRSDiabetesCommunication.SelectedValue.Equals("0") && String.IsNullOrEmpty(PQRSDiabetesCommunicationReason.SelectedValue))
                    {
                        LabelDiabetesCommunicationNo.Visible = true;
                        ChartCompleted = false;
                    }
                }
            } */





           /*  if (string.IsNullOrEmpty(PQRSDiagnosis2.SelectedValue)) {
                LabelPQRSDiagnosis2.Visible = true;
                ChartCompleted = false;
            }
            if (PQRSDiagnosis2.SelectedValue.Equals("1"))
            {
                if (string.IsNullOrEmpty(PQRSEncounter2.SelectedValue))
                {
                    LabelPQRSEncounter2.Visible = true;
                    ChartCompleted = false;
                }
            }
            if (PQRSDiagnosis2.SelectedValue.Equals("1") && string.IsNullOrEmpty(DialetedEyeExam.SelectedValue)) {
                LabelPQRSExamDone.Visible = true;
                ChartCompleted = false;
            } */
      //  }

        if (RadioButtonListRBGender.SelectedIndex == -1) {
            LabelRBGender.Visible = true;
            ChartCompleted = false;
        }
        if (RadioButtonListRBHistoryDiabetic.SelectedIndex == -1) {
            LabelRBHistoryDiabetic.Visible = true;
            ChartCompleted = false;
        }
        if (RadioButtonListRBHistoryCataractSurgery.SelectedIndex == -1) {
            LabelRBHistoryCataractSurgery.Visible = true;
            ChartCompleted = false;
        }
        if (RadioButtonListRBExamEyeID.SelectedIndex == -1) {
            LabelRBExamEyeID.Visible = true;
            ChartCompleted = false;
        }
        if (RadioButtonListRBDilatedFundusExam.SelectedIndex == -1) {
            LabelRBDilatedFundusExam.Visible = true;
            ChartCompleted = false;
        }
        if (RadioButtonListRBExamMacularEdema.SelectedIndex == -1) {
            LabelRBExamMacularEdema.Visible = true;
            ChartCompleted = false;
        }
        if (RadioButtonListRBTypeRetinopathy.SelectedIndex == -1) {
            LabelRBTypeRetinopathy.Visible = true;
            ChartCompleted = false;
        }
        if (RadioButtonListRBPatientCounseled.SelectedIndex == -1) {
            LabelRBPatientCounseled.Visible = true;
            ChartCompleted = false;
        }

        if (RadioButtonListRBMgntPhysCommunication.SelectedIndex == -1) {
            LabelRBMgntPhysCommunication.Visible = true;
            ChartCompleted = false;
        }
        if (RadioButtonListRBOutcomeStatusCSME.SelectedIndex == -1) {
            LabelRBOutcomeStatusCSME.Visible = true;
            ChartCompleted = false;
        }



        if (DropDownListMonthOfBirth.SelectedIndex == 0) {
            LabelMonthOfBirth.Visible = true;
            ChartCompleted = false;
        }
        if (DropDownListYearOfBirth.SelectedIndex == 0) {
            LabelYearOfBirth.Visible = true;
            ChartCompleted = false;
        }
        if (DropDownListMonthOfExam.SelectedIndex == 0) {
            LabelMonthOfExam.Visible = true;
            ChartCompleted = false;
        }
        if (DropDownListYearOfExam.SelectedIndex == 0) {
            LabelYearOfExam.Visible = true;
            ChartCompleted = false;
        }
        if (DropDownListMonthOfFollowUp.SelectedIndex == 0) {
            LabelMonthOfFollowUp.Visible = true;
            ChartCompleted = false;
        }
        if (DropDownListYearOfFollowUp.SelectedIndex == 0) {
            LabelYearOfFollowUp.Visible = true;
            ChartCompleted = false;
        }
        if (DropDownListInvolvedBCVA.SelectedIndex == 0) {
            LabelInvolvedBCVA.Visible = true;
            ChartCompleted = false;
        }
        if (DropDownListFellowBCVA.SelectedIndex == 0) {
            LabelFellowBCVA.Visible = true;
            ChartCompleted = false;
        }
        if (string.IsNullOrEmpty(TextBoxExamCST.Text) && CheckBoxExamCSTNA.Checked == false) {
            LabelExamCST.Visible = true;
            ChartCompleted = false;
        }
        if (DropDownListOutcomeTreatedBCVA.SelectedIndex == 0) {
            LabelOutcomeTreatedBCVA.Visible = true;
            ChartCompleted = false;
        }
        if (DropDownListOutcomeFellowBCVA.SelectedIndex == 0) {
            LabelOutcomeFellowBCVA.Visible = true;
            ChartCompleted = false;
        }
        if (string.IsNullOrEmpty(TextBoxOutcomeCST.Text) && CheckBoxOutcomeCSTNA.Checked == false) {
            LabelOutcomeCST.Visible = true;
            ChartCompleted = false;
        }







        return ChartCompleted;
    }
}