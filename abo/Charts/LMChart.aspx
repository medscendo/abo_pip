﻿<%@ Page Title="" Language="C#" MasterPageFile="~/PIMMasterPage.master" AutoEventWireup="true" CodeFile="LMChart.aspx.cs" Inherits="abo_LMChart" %>

<asp:Content ID="Content1" ContentPlaceHolderID="head" Runat="Server">

    <link type="text/css" href="../../common/css/atooltip.css" rel="stylesheet"  media="screen" />

    <style type="text/css">
        .bginputa{
	        float:right;
	        margin:21px 0 0 1px;
	        background: url(../common/images/orange_button_with_arrow.png) no-repeat;
	        overflow:hidden;
	        height:35px;

        }
        .bginputa a{
	        color: white;
	        text-align:center;
	        height:35px;
	        line-height:28px;
	        padding:0 14px 15px  ;
	        cursor:pointer;
	        float:left;
	        border:none;
	        text-decoration:none;
	        font-family:Arial, Helvetica, sans-serif; 
	        font-size:12px; 
	        font-weight:bold; 
	        letter-spacing: 0px;
        }
        .bginputa a:hover{
	        text-decoration:none;
        }
        .right {
            text-align:right;
        }
    </style>
    
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder1" Runat="Server">
    <asp:HiddenField ID="HiddenFieldRecordIdentifier" runat="server"/>
        <!--  pim -->
        <div class="pim">
            <div class="record-ident clearfix">
                <h3 class="record-first">RECORD IDENTIFIER: <asp:Literal runat="server" ID="LiteralRecordIdentifier"></asp:Literal></h3>
                <h3 class="record-second"><asp:Literal runat="server" ID="LiteralAbstractionNumber"></asp:Literal></h3>
            </div>
            <br />
            <!-- History -->
            <table>
                <tr>
                    <th colspan="3" width="910px"><p>History</p></th>
                </tr>
                <!-- Question 1 -->
                <tr class="table_body">
                    <td colspan="2">
                        <strong>1. Date of birth</strong>
                        <br /><asp:Label ID="LabelMonthOfBirth" runat="server" Visible="false"  ForeColor="Red" Text="Please complete month" />
                        <br /><asp:Label ID="LabelYearOfBirth" runat="server" Visible="false"  ForeColor="Red" Text="Please complete year" />    
                    </td>
                    <td>
                        <asp:DropDownList ID='DropDownListMonthOfBirth' DataValueField='MonthID' DataTextField='MonthName' runat='server' />
                        <asp:DropDownList ID='DropDownListYearOfBirth' DataValueField='YearID' DataTextField='YearName' runat='server' />
                    </td>
                </tr>
                <!-- Question 2 -->
                <tr class="table_body_bg">
                    <td colspan="2">
                        <strong>2. Date of preoperative examination</strong>
                        <br /><asp:Label ID="LabelMonthOfPreopExam" runat="server" Visible="false"  ForeColor="Red" Text="Please complete" />
                        <br /><asp:Label ID="LabelYearOfPreopExam" runat="server" Visible="false"  ForeColor="Red" Text="Please complete" />
                        <br />
                        <asp:Label ID="Labelinitialage" runat="server" Visible="false"  
                            ForeColor="Red" Text="" />
                    </td>
                    <td>
                        <asp:DropDownList ID='DropDownListMonthOfPreopExam' DataValueField='MonthID' DataTextField='MonthName' runat='server' />
                        <asp:DropDownList ID='DropDownListYearOfPreopExam' DataValueField='YearID' DataTextField='YearName' runat='server' />
                    </td>
                </tr>
                <!-- Question 3 -->
                <tr class="table_body">
                    <td colspan="2">
                        <strong>3. Gender</strong>
                        <br /><asp:Label ID="LabelRBGender" runat="server" Visible="false"  ForeColor="Red" Text="Please complete" />    
                    </td>
                    <td>
                        <asp:RadioButtonList id='RadioButtonListRBGender' RepeatDirection='Horizontal' CssClass='aspxList' runat='server'>
                            <asp:ListItem Value='42'>&nbsp;Male</asp:ListItem>
                            <asp:ListItem Value='43'>&nbsp;Female</asp:ListItem>
                        </asp:RadioButtonList>
                    </td>
                </tr>
                <!-- Question 4 -->
                <tr class="table_body_bg">
                    <td colspan="2">
                        <strong>4. Occupational/visual requirements documented</strong>
                        <br /><asp:Label ID="LabelVisualRequirementsDoc" runat="server" Visible="false"  ForeColor="Red" Text="Please complete" />
                    </td>
                    <td>
                        <asp:RadioButton runat='server' ID='RadioButtonVisualRequirementsDocYes' GroupName='VisualRequirementsDoc ' text='&nbsp;Yes' />
                        <asp:RadioButton runat='server' ID='RadioButtonVisualRequirementsDocNo' GroupName='VisualRequirementsDoc ' text='&nbsp;No' />
                        
                    </td>                    
                </tr>
                <!-- Question 5 -->
                <tr class="table_body">
                    <td rowspan="6" width="50%">
                        <strong>5. Past ocular history</strong>
                        <br /><asp:Label ID="LabelHistoryDryEye" runat="server" Visible="false"  ForeColor="Red" Text="Please complete dry eye syndrome" />
                        <br /><asp:Label ID="LabelHistoryKeratoconus" runat="server" Visible="false"  ForeColor="Red" Text="Please complete keratoconus/pellucid marginal" />
                        <br /><asp:Label ID="LabelHistoryInfection" runat="server" Visible="false"  ForeColor="Red" Text="Please complete infection" />
                        <br /><asp:Label ID="LabelHistoryGlaucoma" runat="server" Visible="false"  ForeColor="Red" Text="Please complete glaucoma" />
                        <br /><asp:Label ID="LabelHistoryStrabismus" runat="server" Visible="false"  ForeColor="Red" Text="Please complete strabismus/amblyopia" />
                        <br /><asp:Label ID="LabelHistoryOther" runat="server" Visible="false"  ForeColor="Red" Text="Please complete other" />
                    </td>
                    <td width="20%"><span class="right">Dry Eye Syndrome</span></td>
                    <td>
                        <asp:RadioButton runat='server' ID='RadioButtonHistoryDryEyeYes' GroupName='HistoryDryEye ' text='&nbsp;Yes' />
                        <asp:RadioButton runat='server' ID='RadioButtonHistoryDryEyeNo' GroupName='HistoryDryEye ' text='&nbsp;No' />  
                    </td>
                </tr>
                <tr class="table_body">
                    <td><span class="right">Keratoconus/Pellucid marginal</span></td>
                    <td>
                        <asp:RadioButton runat='server' ID='RadioButtonHistoryKeratoconusYes' GroupName='HistoryKeratoconus ' text='&nbsp;Yes' />
                        <asp:RadioButton runat='server' ID='RadioButtonHistoryKeratoconusNo' GroupName='HistoryKeratoconus ' text='&nbsp;No' />
                    </td>
                </tr>
                <tr class="table_body">
                    <td><span class="right">Infection (i.e. Herpes Simplex Virus)</span></td>
                    <td>
                        <asp:RadioButton runat='server' ID='RadioButtonHistoryInfectionYes' GroupName='HistoryInfection ' text='&nbsp;Yes' />
                        <asp:RadioButton runat='server' ID='RadioButtonHistoryInfectionNo' GroupName='HistoryInfection ' text='&nbsp;No' />
                    </td>
                </tr>
                <tr class="table_body">
                    <td><span class="right">Glaucoma</span></td>
                    <td>
                        <asp:RadioButton runat='server' ID='RadioButtonHistoryGlaucomaYes' GroupName='HistoryGlaucoma ' text='&nbsp;Yes' />
                        <asp:RadioButton runat='server' ID='RadioButtonHistoryGlaucomaNo' GroupName='HistoryGlaucoma ' text='&nbsp;No' />
                    </td>
                </tr>
                <tr class="table_body">
                    <td><span class="right">Strabismus/Amblyopia</span></td>
                    <td>
                        <asp:RadioButton runat='server' ID='RadioButtonHistoryStrabismusYes' GroupName='HistoryStrabismus ' text='&nbsp;Yes' />
                        <asp:RadioButton runat='server' ID='RadioButtonHistoryStrabismusNo' GroupName='HistoryStrabismus ' text='&nbsp;No' />
                    </td>
                </tr>
                <tr class="table_body">
                    <td><span class="right">Other</span></td>
                    <td>
                        <asp:RadioButton runat='server' ID='RadioButtonHistoryOtherYes' GroupName='HistoryOther ' text='&nbsp;Yes' />
                        <asp:RadioButton runat='server' ID='RadioButtonHistoryOtherNo' GroupName='HistoryOther ' text='&nbsp;No' />
                    </td>
                </tr>
                <tr class="table_body">
                    <td colspan="2">
                        <strong>5a. If "Other," please specify:</strong>
                    </td>
                    <td>
                        <asp:TextBox runat="server" ID="TextBoxHistoryOtherText" size="25" />
                    </td>
                </tr>
                <!-- Question 6 -->
                <tr class="table_body_bg">
                    <td colspan="2">
                        <strong>6. Previous eye surgery</strong>
                        <br /><asp:Label ID="LabelRBHistoryEyeSurgery" runat="server" Visible="false"  ForeColor="Red" Text="Please complete" />
                    </td>
                    <td>
                        <asp:RadioButtonList id='RadioButtonListRBHistoryEyeSurgery' RepeatDirection='Horizontal' CssClass='aspxList' runat='server'>
                            <asp:ListItem Value='1'>&nbsp;Yes</asp:ListItem>
                            <asp:ListItem Value='2'>&nbsp;No</asp:ListItem>
                            <asp:ListItem Value='3'>&nbsp;Not Documented</asp:ListItem>
                        </asp:RadioButtonList>
                    </td>
                </tr>
            </table>
            <br />
            <!-- Examination -->
            <table>
                <tr>
                    <th colspan="3" width="910px"><p>Examination</p></th>
                </tr>
                <!-- Question 1 -->
                <tr class="table_body">
                    <td width="50%" rowspan="3"><strong>1. Previous refractive error spectacle correction</strong>&nbsp;<a href="#"><img id="QE1" src="../../common/images/tip.gif" alt="Tip" /></a></td>
                    <td width="20%"><span class="right">OD</span></td>
                    <td>
                        <table class="aspxList">
                            <tr>
                                <td>    
                                    <span class="right" id="Q6txtc">
                                    <asp:RadioButton ID="RadioButtonRefractiveCorrectionOD1SignYes" runat="server" GroupName="RefractiveCorrectionOD1Sign" Text="&nbsp;+" onclick="toggleQE1B();" />&nbsp;
                                    <asp:RadioButton ID="RadioButtonRefractiveCorrectionOD1SignNo" runat="server" GroupName="RefractiveCorrectionOD1Sign" Text="&nbsp;-" onclick="toggleQE1B();" />
                                    </span>
                                </td>
                                <td>
                                    <span id="QI6txtd">
                                    <asp:TextBox runat="server" meta="{vMin: '-30.00', vMax: '30.00'}" class="auto" ID="TextBoxRefractiveCorrectionOD1" size="3" onkeyup="toggleQE1B();" />&nbsp;sph
                                    </span>
                                </td>
                            </tr>
                            <tr>
                                <td><span class="right" id="QI6txte">+ </span></td>
                                <td><span id="QI6txtf"><asp:TextBox runat="server" meta="{vMin: '-99.99', vMax: '99.99'}" class="auto" ID="TextBoxRefractiveCorrectionOD2" size="3" onkeyup="toggleQE1B();" />&nbsp;cyl</span></td>
                            </tr>
                            <tr>
                                <td></td>
                                <td><span id="QI6txtg"><asp:TextBox runat="server" meta="{vMin: '0.00', vMax: '180.00'}" class="auto" ID="TextBoxRefractiveCorrectionOD3" size="3" onkeyup="toggleQE1B();" />&nbsp;axis</span></td>
                            </tr>
                        </table>
                    </td>
                </tr>
                <tr class="table_body">
                    <td><span class="right">OS</span></td>
                    <td>
                        <table class="aspxList">
                            <tr>
                                <td>    
                                    <span class="right" id="QI6txth">
                                    <asp:RadioButton ID="RadioButtonRefractiveCorrectionOS1SignYes" runat="server" GroupName="RefractiveCorrectionOS1" Text="&nbsp;+" onclick="toggleQE1B();" />&nbsp;
                                    <asp:RadioButton ID="RadioButtonRefractiveCorrectionOS1SignNo" runat="server" GroupName="RefractiveCorrectionOS1" Text="&nbsp;-" onclick="toggleQE1B();" />
                                    </span>
                                </td>
                                <td>
                                    <span id="QI6txti"><asp:TextBox runat="server" meta="{vMin: '-30.00', vMax: '30.00'}" class="auto" ID="TextBoxRefractiveCorrectionOS1" size="3" onkeyup="toggleQE1B();" />&nbsp;sph</span>
                                </td>
                            </tr>
                            <tr>
                                <td><span class="right" id="QI6txtj">+ </span></td>
                                <td><span id="QI6txtk"><asp:TextBox runat="server" meta="{vMin: '-99.99', vMax: '99.99'}" class="auto" ID="TextBoxRefractiveCorrectionOS2" size="3" onkeyup="toggleQE1B();" />&nbsp;cyl</span></td>
                            </tr>
                            <tr>
                                <td></td>
                                <td><span id="QI6txtl"><asp:TextBox runat="server" meta="{vMin: '0.00', vMax: '180.00'}" class="auto" ID="TextBoxRefractiveCorrectionOS3" size="3" onkeyup="toggleQE1B();" />&nbsp;axis</span></td>
                            </tr>
                        </table>
                    </td>
                </tr>
                <tr class="table_body">
                    <td class="aspxList"></td>
                    <td><asp:CheckBox runat='server' ID='CheckBoxRefractiveCorrectionNA' text='&nbsp;Not Done' onclick="toggleQE1A();" /></td>
                </tr>
                <!-- Question 2 -->
                <tr class="table_body_bg">
                    <td rowspan="2"><strong>2. Manifest refraction</strong>&nbsp;<a href="#"><img id="QE2" src="../../common/images/tip.gif" alt="Tip" /></a></td>
                    <td><span class="right">OD</span></td>
                    <td>
                        <table class="aspxList">
                            <tr>
                                <td>    
                                    <span class="right" id="Span1">
                                    <asp:RadioButton ID="RadioButtonManifestRefractionOD1SignYes" runat="server" GroupName="ManifestRefractionOD1" Text="&nbsp;+" onclick="toggleQE2ODA();" />&nbsp;
                                    <asp:RadioButton ID="RadioButtonManifestRefractionOD1SignNo" runat="server" GroupName="ManifestRefractionOD1" Text="&nbsp;-" onclick="toggleQE2ODA();" />
                                    </span>
                                </td>
                                <td>
                                    <span id="Span2"><asp:TextBox runat="server" meta="{vMin: '-30.00', vMax: '30.00'}" class="auto" ID="TextBoxManifestRefractionOD1" size="3" onkeyup="toggleQE2ODA();" />&nbsp;sph</span>
                                </td>
                            </tr>
                            <tr>
                                <td><span class="right" id="Span3">+ </span></td>
                                <td><span id="Span4"><asp:TextBox runat="server" meta="{vMin: '-99.99', vMax: '99.99'}" class="auto" ID="TextBoxManifestRefractionOD2" size="3" onkeyup="toggleQE2ODA();" />&nbsp;cyl</span></td>
                            </tr>
                            <tr>
                                <td></td>
                                <td><span id="Span5"><asp:TextBox runat="server" meta="{vMin: '0.00', vMax: '180.00'}" class="auto" ID="TextBoxManifestRefractionOD3" size="3" onkeyup="toggleQE2ODA();" />&nbsp;axis</span></td>
                            </tr>
                        </table>
                        <asp:CheckBox runat='server' ID='CheckBoxManifestRefractionODNA' text='&nbsp;Not Done' onclick="toggleQE2ODB();" />
                    </td>
                </tr>
                <tr class="table_body_bg">
                    <td><span class="right">OS</span></td>
                    <td>
                        <table class="aspxList">
                            <tr>
                                <td>    
                                    <span class="right" id="Span6">
                                    <asp:RadioButton ID="RadioButtonManifestRefractionOS1SignYes" runat="server" GroupName="ManifestRefractionOS1Sign" Text="&nbsp;+" onclick="toggleQE2OSA();" />&nbsp;
                                    <asp:RadioButton ID="RadioButtonManifestRefractionOS1SignNo" runat="server" GroupName="ManifestRefractionOS1Sign" Text="&nbsp;-" onclick="toggleQE2OSA();" />
                                    </span>
                                </td>
                                <td>
                                    <span id="Span7"><asp:TextBox runat="server" meta="{vMin: '-30.00', vMax: '30.00'}" class="auto" ID="TextBoxManifestRefractionOS1" size="3" onkeyup="toggleQE2OSA();" />&nbsp;sph</span>
                                </td>
                            </tr>
                            <tr>
                                <td><span class="right" id="Span8">+ </span></td>
                                <td><span id="Span9"><asp:TextBox runat="server" meta="{vMin: '-99.99', vMax: '99.99'}" class="auto" ID="TextBoxManifestRefractionOS2" size="3" onkeyup="toggleQE2OSA();" />&nbsp;cyl</span></td>
                            </tr>
                            <tr>
                                <td></td>
                                <td><span id="Span10"><asp:TextBox runat="server" meta="{vMin: '0.00', vMax: '180.00'}" class="auto" ID="TextBoxManifestRefractionOS3" size="3" onkeyup="toggleQE2OSA();" />&nbsp;axis</span></td>
                            </tr>
                        </table>
                        <asp:CheckBox runat='server' ID='CheckBoxManifestRefractionOSNA' text='&nbsp;Not Done' onclick="toggleQE2OSB();" />
                    </td>
                </tr>
                <!-- Question 3 -->
                <tr class="table_body">
                    <td colspan="2">
                        <strong>3. Add required for near</strong>
                        <br /><asp:Label ID="LabelRBRequiredAdd" runat="server" Visible="false"  ForeColor="Red" Text="Please complete" />    
                    </td>
                    <td>
                        <asp:RadioButtonList id='RadioButtonListRBRequiredAdd' RepeatDirection='Horizontal' CssClass='aspxList' runat='server'>
                            <asp:ListItem Value="1">&nbsp;Yes</asp:ListItem>
	                        <asp:ListItem Value="2">&nbsp;No</asp:ListItem>
	                        <asp:ListItem Value="3">&nbsp;Not Recorded</asp:ListItem>
                        </asp:RadioButtonList> 
                    </td>
                </tr>
                <!-- Question 4 -->
                <tr class="table_body_bg">
                    <td rowspan="3">
                        <strong>4. Distance visual acuity with manifest refraction</strong>&nbsp;<a href="#"><img id="QE4" src="../../common/images/tip.gif" alt="Tip" /></a>
                        <asp:Label ID="LabelDistVAManifestRefractionOD" runat="server" Visible="false"  ForeColor="Red" Text="<br />Please complete OD" />    
                        <asp:Label ID="LabelDistVAManifestRefractionOS" runat="server" Visible="false"  ForeColor="Red" Text="<br />Please complete OS" />    
                        <asp:Label ID="LabelDistVAManifestRefractionNA" runat="server" Visible="false"  ForeColor="Red" Text="<br />Please complete" />    
                    </td>
                    <td><span class="right">OD</span></td>
                    <td>
                        20 / <asp:DropDownList ID='DropDownListDistVAManifestRefractionOD' DataValueField="examValue" DataTextField="examLabel" runat='server' onchange="toggleQE4CB();" />
                    </td>
                </tr>
                <tr class="table_body_bg">
                    <td><span class="right">OS</span></td>
                    <td>
                        20 / <asp:DropDownList ID='DropDownListDistVAManifestRefractionOS' DataValueField="examValue" DataTextField="examLabel" runat='server' onchange="toggleQE4CB();" />
                    </td>
                </tr>
                <tr class="table_body_bg">
                    <td></td>
                    <td><asp:CheckBox runat='server' ID='CheckBoxDistVAManifestRefractionNA' text='&nbsp;Not Done' onclick="toggleQE4DD();" /></td>
                </tr>
                <!-- Question 5 -->
                <tr class="table_body">
                    <td rowspan="3">
                        <strong>5. Near visual acuity with manifest refraction</strong>&nbsp;<a href="#"><img id="QE5" src="../../common/images/tip.gif" alt="Tip" /></a>
                        <asp:Label ID="LabelNearVAManifestRefractionOD" runat="server" Visible="false"  ForeColor="Red" Text="<br />Please complete OD" />    
                        <asp:Label ID="LabelNearVAManifestRefractionOS" runat="server" Visible="false"  ForeColor="Red" Text="<br />Please complete OS" />    
                        <asp:Label ID="LabelNearVAManifestRefractionNA" runat="server" Visible="false"  ForeColor="Red" Text="<br />Please complete" />
                    </td>
                    <td><span class="right">OD</span></td>
                    <td>
                        20 / <asp:DropDownList ID='DropDownListNearVAManifestRefractionOD' DataValueField="examValue" DataTextField="examLabel" runat='server' onchange="toggleQE5CB();" />
                    </td>
                </tr>
                <tr class="table_body">
                    <td><span class="right">OS</span></td>
                    <td>20 / <asp:DropDownList ID='DropDownListNearVAManifestRefractionOS' DataValueField="examValue" DataTextField="examLabel" runat='server' onchange="toggleQE5CB();" /></td>
                </tr>
                <tr class="table_body">
                    <td></td>
                    <td><asp:CheckBox runat='server' ID='CheckBoxNearVAManifestRefractionNA' text='&nbsp;Not Done' onclick="toggleQE5DD();" /></td>
                </tr>
                <!-- Question 6 -->
                <tr class="table_body_bg">
                    <td rowspan="2"><strong>6. Cycloplegic refraction</strong>&nbsp;<a href="#"><img id="QE6" src="../../common/images/tip.gif" alt="Tip" /></a></td>
                    <td><span class="right">OD</span></td>
                    <td>
                        <table class="aspxList">
                            <tr>
                                <td>    
                                    <span class="right" id="Span11">
                                    <asp:RadioButton ID="RadioButtonCycloplegicRefractionOD1SignYes" runat="server" GroupName="CycloplegicRefractionOD1" Text="&nbsp;+" onclick="toggleQE6ODA();" />&nbsp;
                                    <asp:RadioButton ID="RadioButtonCycloplegicRefractionOD1SignNo" runat="server" GroupName="CycloplegicRefractionOD1" Text="&nbsp;-" onclick="toggleQE6ODA();" />
                                    </span>
                                </td>
                                <td>
                                    <span id="Span12"><asp:TextBox runat="server" meta="{vMin: '-30.00', vMax: '30.00'}" class="auto" ID="TextBoxCycloplegicRefractionOD1" size="3" onkeyup="toggleQE6ODA();" />&nbsp;sph</span>
                                </td>
                            </tr>
                            <tr>
                                <td><span class="right" id="Span13">+ </span></td>
                                <td><span id="Span14"><asp:TextBox runat="server" meta="{vMin: '0.00', vMax: '99.99'}" class="auto" ID="TextBoxCycloplegicRefractionOD2" size="3" onkeyup="toggleQE6ODA();" />&nbsp;cyl</span></td>
                            </tr>
                            <tr>
                                <td></td>
                                <td><span id="Span15"><asp:TextBox runat="server" meta="{vMin: '0.00', vMax: '180.00'}" class="auto" ID="TextBoxCycloplegicRefractionOD3" size="3" onkeyup="toggleQE6ODA();" />&nbsp;axis</span></td>
                            </tr>
                        </table>
                        <asp:CheckBox runat='server' ID='CheckBoxCycloplegicRefractionODNA' text='&nbsp;Not Done' onclick="toggleQE6ODB();" />
                    </td>
                </tr>
                <tr class="table_body_bg">
                    <td><span class="right">OS</span></td>
                    <td>
                        <table class="aspxList">
                            <tr>
                                <td>    
                                    <span class="right" id="Span16">
                                    <asp:RadioButton ID="RadioButtonCycloplegicRefractionOS1SignYes" runat="server" GroupName="CycloplegicRefractionOSsph" Text="&nbsp;+" onclick="toggleQE6OSA();" />&nbsp;
                                    <asp:RadioButton ID="RadioButtonCycloplegicRefractionOS1SignNo" runat="server" GroupName="CycloplegicRefractionOSsph" Text="&nbsp;-" onclick="toggleQE6OSA();" />
                                    </span>
                                </td>
                                <td>
                                    <span id="Span17"><asp:TextBox runat="server" meta="{vMin: '-30.00', vMax: '30.00'}" class="auto" ID="TextBoxCycloplegicRefractionOS1" size="3" onkeyup="toggleQE6OSA();" />&nbsp;sph</span>
                                </td>
                            </tr>
                            <tr>
                                <td><span class="right" id="Span18">+ </span></td>
                                <td><span id="Span19"><asp:TextBox runat="server" meta="{vMin: '0.00', vMax: '99.99'}" class="auto" ID="TextBoxCycloplegicRefractionOS2" size="3" onkeyup="toggleQE6OSA();" />&nbsp;cyl</span></td>
                            </tr>
                            <tr>
                                <td></td>
                                <td><span id="Span20"><asp:TextBox runat="server" meta="{vMin: '0.00', vMax: '180.00'}" class="auto" ID="TextBoxCycloplegicRefractionOS3" size="3" onkeyup="toggleQE6OSA();" />&nbsp;axis</span></td> 
                            </tr>
                        </table>
                        <asp:CheckBox runat='server' ID='CheckBoxCycloplegicRefractionOSNA' text='&nbsp;Not Done' onclick="toggleQE6OSB();" />
                    </td>
                </tr>
                <!-- Question 7 -->
                <tr class="table_body">
                    <td rowspan="3"><strong>7. Distance visual acuity with cycloplegic refraction</strong>&nbsp;<a href="#"><img id="QE7" src="../../common/images/tip.gif" alt="Tip" /></a></td>
                    <td><span class="right">OD</span></td>
                    <td>
                        
                        20 / <asp:DropDownList ID='DropDownListDistanceVACycloplegicOD' DataValueField="examValue" DataTextField="examLabel" runat='server' onclick="toggleQE7CB();"  ></asp:DropDownList>
                    </td>    
                </tr>
                <tr class="table_body">
                    <td><span class="right">OS</span>
                    <td>
                    
                20 / <asp:DropDownList ID='DropDownListDistanceVACycloplegicOS' DataValueField="examValue" DataTextField="examLabel" runat='server' onclick="toggleQE7CB();"  ></asp:DropDownList>
                    
                </tr>
                <tr class="table_body">
                    <td></td>
                    <td><asp:CheckBox runat='server' ID='CheckBoxDistanceVACycloplegicNA' text='&nbsp;Not Done' onclick="toggleQE7DD();" /></td>
                </tr>
                <!-- Question 8 -->
                <tr class="table_body_bg">
                    <td colspan="2">
                        <strong>8. Ocular dominance</strong>
                        <br /><asp:Label ID="LabelRBOcularDominance" runat="server" Visible="false"  ForeColor="Red" Text="Please complete" />    
                    </td>
                    <td>
                        <asp:RadioButtonList id='RadioButtonListRBOcularDominance' RepeatDirection='Vertical' CssClass='aspxList' runat='server'>
                            <asp:ListItem Value='49'>&nbsp;Right</asp:ListItem>
                            <asp:ListItem Value='50'>&nbsp;Left</asp:ListItem>
                            <asp:ListItem Value='3'>&nbsp;Not Documented</asp:ListItem>
                        </asp:RadioButtonList>
                    </td>
                </tr>
                <!-- Question 9 -->
                <tr class="table_body">
                    <td colspan="2">
                        <strong>9. Monovision simulation performed</strong>
                        <br /><asp:Label ID="LabelMonovisionSimulation" runat="server" Visible="false"  ForeColor="Red" Text="Please complete" />
                    </td>
                    <td>
                        <asp:RadioButton runat="server" ID="RadioButtonMonovisionSimulationYes" GroupName="MonovisionSimulation" text="&nbsp;Yes" />&nbsp;&nbsp;
                        <asp:RadioButton runat="server" ID="RadioButtonMonovisionSimulationNo" GroupName="MonovisionSimulation" text="&nbsp;No" />
                    </td>
                </tr>
                <!-- Question 10 -->
                <tr class="table_body_bg">
                    <td colspan="2">
                        <strong>10. Eyelids and lid function</strong>
                        <br /><asp:Label ID="LabelRBEyelidFunction" runat="server" Visible="false"  ForeColor="Red" Text="Please complete" />
                    </td>
                    <td>
                        <asp:RadioButtonList id='RadioButtonListRBEyelidFunction' RepeatDirection='Vertical' CssClass='aspxList' runat='server'>
                            <asp:ListItem Value='104'>&nbsp;Normal</asp:ListItem>
                            <asp:ListItem Value='105'>&nbsp;Abnormal</asp:ListItem>
                            <asp:ListItem Value='3'>&nbsp;Not Documented</asp:ListItem>
                        </asp:RadioButtonList>
                    </td>
                </tr>
                <!-- Question 11 -->
                <tr class="table_body">
                    <td rowspan="5">
                        <strong>11. Pupil size measurement (light/dark)</strong>
                    </td>
                    <td><span class="right">Right:</span></td>
                    <td> 
                        <asp:DropDownList ID="DropDownListPupilSizeODLight" runat="server" >
                            <asp:ListItem Value="0" Text="Choose"></asp:ListItem>
                            <asp:ListItem Value="1" Text="1"></asp:ListItem>
                            <asp:ListItem Value="2" Text="2"></asp:ListItem>
                            <asp:ListItem Value="3" Text="3"></asp:ListItem>
                            <asp:ListItem Value="4" Text="4"></asp:ListItem>
                            <asp:ListItem Value="5" Text="5"></asp:ListItem>
                            <asp:ListItem Value="6" Text="6"></asp:ListItem>
                            <asp:ListItem Value="7" Text="7"></asp:ListItem>
                            <asp:ListItem Value="8" Text="8"></asp:ListItem>
                            <asp:ListItem Value="9" Text="9"></asp:ListItem>
                            <asp:ListItem Value="10" Text="10"></asp:ListItem>                                        
                        </asp:DropDownList><label>&nbsp;mm in light</label>
                    </td>
                </tr>
                <tr class="table_body">
                    <td><span class="right">Left:</span></td>
                    <td>
                        <asp:DropDownList ID="DropDownListPupilSizeOSLight" runat="server" >
                        <asp:ListItem Value="0" Text="Choose"></asp:ListItem>
                        <asp:ListItem Value="1" Text="1"></asp:ListItem>
                        <asp:ListItem Value="2" Text="2"></asp:ListItem>
                        <asp:ListItem Value="3" Text="3"></asp:ListItem>
                        <asp:ListItem Value="4" Text="4"></asp:ListItem>
                        <asp:ListItem Value="5" Text="5"></asp:ListItem>
                        <asp:ListItem Value="6" Text="6"></asp:ListItem>
                        <asp:ListItem Value="7" Text="7"></asp:ListItem>
                        <asp:ListItem Value="8" Text="8"></asp:ListItem>
                        <asp:ListItem Value="9" Text="9"></asp:ListItem>
                        <asp:ListItem Value="10" Text="10"></asp:ListItem>               
                        </asp:DropDownList><label>&nbsp;mm in light</label>
                    </td>
                </tr>
                <tr class="table_body">
                    <td><span class="right">Right:</span></td>
                    <td>
                        <asp:DropDownList ID="DropDownListPupilSizeODDark" runat="server" >
                            <asp:ListItem Value="0" Text="Choose"></asp:ListItem>
                            <asp:ListItem Value="1" Text="1"></asp:ListItem>
                            <asp:ListItem Value="2" Text="2"></asp:ListItem>
                            <asp:ListItem Value="3" Text="3"></asp:ListItem>
                            <asp:ListItem Value="4" Text="4"></asp:ListItem>
                            <asp:ListItem Value="5" Text="5"></asp:ListItem>
                            <asp:ListItem Value="6" Text="6"></asp:ListItem>
                            <asp:ListItem Value="7" Text="7"></asp:ListItem>
                            <asp:ListItem Value="8" Text="8"></asp:ListItem>
                            <asp:ListItem Value="9" Text="9"></asp:ListItem>
                            <asp:ListItem Value="10" Text="10"></asp:ListItem>                 
                        </asp:DropDownList><label>&nbsp;mm in dark</label>
                    </td>
                </tr>
                <tr class="table_body">
                    <td><span class="right">Left:</span></td>
                    <td>
                        <asp:DropDownList ID="DropDownListPupilSizeOSDark" runat="server" >
                            <asp:ListItem Value="0" Text="Choose"></asp:ListItem>
                            <asp:ListItem Value="1" Text="1"></asp:ListItem>
                            <asp:ListItem Value="2" Text="2"></asp:ListItem>
                            <asp:ListItem Value="3" Text="3"></asp:ListItem>
                            <asp:ListItem Value="4" Text="4"></asp:ListItem>
                            <asp:ListItem Value="5" Text="5"></asp:ListItem>
                            <asp:ListItem Value="6" Text="6"></asp:ListItem>
                            <asp:ListItem Value="7" Text="7"></asp:ListItem>
                            <asp:ListItem Value="8" Text="8"></asp:ListItem>
                            <asp:ListItem Value="9" Text="9"></asp:ListItem>
                            <asp:ListItem Value="10" Text="10"></asp:ListItem>            
                        </asp:DropDownList><label>&nbsp;mm in dark</label>
                    </td>
                </tr>
                <tr class="table_body">
                    <td><span class="right"></span></td>
                    <td><asp:CheckBox runat='server' ID='CheckBoxPupilSizeNA' text='&nbsp;Not Measured' onclick="toggleQE11A();" /></td>
                </tr>
                <!-- Question 12 -->
                <tr class="table_body_bg">
                    <td rowspan="2">
                        <strong>12. Intraocular pressure</strong> 
                    </td>
                    <td><span class="right">Right</span></td>
                    <td>
                        <asp:TextBox runat='server' ID='TextBoxIOPOD' size='5' onkeyup="toggleQE12ODCB();" /> mmHG
                        <br /><asp:CheckBox runat='server' ID='CheckBoxIOPODNA' text='&nbsp;Not Performed' onclick="toggleQE12ODTB();" />
                    </td>
                </tr>
                <tr class="table_body_bg">
                    <td><span class="right">Left</span></td>
                    <td>
                        <asp:TextBox runat='server' ID='TextBoxIOPOS' size='5' onkeyup="toggleQE12OSCB();" /> mmHG
                        <br /><asp:CheckBox runat='server' ID='CheckBoxIOPOSNA' text='&nbsp;Not Performed' onclick="toggleQE12OSTB();" />
                    </td>
                </tr>
                <!-- Question 13 -->
                <tr class="table_body">
                    <td rowspan="3"><strong>13. Keratometry (flat K / steep K / axis of steep K)</strong></td>
                    <td><span class="right">Right</span></td>
                    <td>
                        <asp:TextBox runat='server' meta="{vMin: '-99.99', vMax: '99.99'}" class="auto" ID='TextBoxKeratometryFlatOD' size='5' onkeyup="toggleQE13CB();" /> / 
                        <asp:TextBox runat='server' meta="{vMin: '00.00', vMax: '99.99'}" class="auto" ID='TextBoxKeratometrySteepOD' size='5' onkeyup="toggleQE13CB();" /> / 
                        <asp:TextBox runat='server' meta="{vMin: '0', vMax: '180'}" class="auto" ID='TextBoxKeratometryAxisOD' size='5' onkeyup="toggleQE13CB();" />
                    </td>
                </tr>
                <tr class="table_body">
                    <td><span class="right">Left</span></td>
                    <td>
                        <asp:TextBox runat='server' meta="{vMin: '-99.99', vMax: '99.99'}" class="auto" ID='TextBoxKeratometryFlatOS' size='5' onkeyup="toggleQE13CB();" /> / 
                        <asp:TextBox runat='server' meta="{vMin: '00.00', vMax: '99.99'}" class="auto" ID='TextBoxKeratometrySteepOS' size='5' onkeyup="toggleQE13CB();" /> / 
                        <asp:TextBox runat='server' meta="{vMin: '0', vMax: '180'}" class="auto" ID='TextBoxKeratometryAxisOS' size='5' onkeyup="toggleQE13CB();" />
                    </td>
                </tr>
                <tr class="table_body">
                    <td></td>
                    <td>
                        <asp:CheckBox runat='server' ID='CheckBoxKeratometryNA' text='&nbsp;Not Done' onclick="toggleQE13DD();" />
                    </td>
                </tr>
                <!-- Question 14 -->
                <tr class="table_body_bg">
                    <td rowspan="2"><strong>14. Corneal thickness</strong></td>
                    <td><span class="right">Right</span></td>
                    <td>
                        <asp:TextBox runat='server' ID='TextBoxCornealThicknessOD' size='5' onkeyup="toggleQE14ODCB();" />&nbsp;&mu;
                        <br /><asp:CheckBox runat='server' ID='CheckBoxCornealThicknessODNA' text='&nbsp;Not Performed' onclick="toggleQE14ODTB();" />
                    </td>
                </tr>
                <tr class="table_body_bg">
                    <td><span class="right">Left</span></td>
                    <td>
                        <asp:TextBox runat='server' ID='TextBoxCornealThicknessOS' size='5' onkeyup="toggleQE14OSCB();" />&nbsp;&mu;
                        <br /><asp:CheckBox runat='server' ID='CheckBoxCornealThicknessOSNA' text='&nbsp;Not Performed' onclick="toggleQE14OSTB();" />
                    </td>
                </tr>
                <!-- Question 15 -->
                <tr class="table_body">
                    <td colspan="2">
                        <strong>15. Results of corneal topography documented?</strong>
                        <br /><asp:Label ID="LabelCornealTopDoc" runat="server" Visible="false"  ForeColor="Red" Text="Please complete" />
                    </td>
                    <td>
                         <asp:RadioButton runat='server' ID='RadioButtonCornealTopDocYes' GroupName='CornealTopDoc' text='&nbsp;Yes' />&nbsp;&nbsp;
                         <asp:RadioButton runat='server' ID='RadioButtonCornealTopDocNo' GroupName='CornealTopDoc' text='&nbsp;No' />
                    </td>
                </tr>
                <!-- Question 16 -->
                <tr class="table_body_bg">
                    <td colspan="2"><strong>16. Additional testing performed</strong></td>
                    <td>
                        <asp:CheckBox runat='server' ID='CheckBoxTestWaveFrontMeasure' text='&nbsp;Wavefront measurement interpretation' />
                        <br /><asp:CheckBox runat='server' ID='CheckBoxTestSchirmerTear' text='&nbsp;Schirmer tear secretion test' />
                        <br /><asp:CheckBox runat='server' ID='CheckBoxTestVitalDye' text='&nbsp;Vital dye staining (Lissamine green, Fluorescein and/or Rose Bengal)' />
                    </td>
                </tr>
                <!-- Question 17 -->
                <tr class="table_body">
                    <td colspan="2"><strong>17. Slit lamp exam findings</strong></td>
                    <td>
                        <asp:CheckBox runat='server' ID='CheckBoxSlitLampBlepharitis' text='&nbsp;Blepharitis' />
                        <br /><asp:CheckBox runat='server' ID='CheckBoxSlitLampAnterior' text='&nbsp;Epithelial basement membrane dystrophy' />
                        <br /><asp:CheckBox runat='server' ID='CheckBoxSlitLampStromal' text='&nbsp;Stromal edema' />
                        <br /><asp:CheckBox runat='server' ID='CheckBoxSlitLampGuttata' text='&nbsp;Guttata' />
                        <br /><asp:CheckBox runat='server' ID='CheckBoxSlitLampCornealScar' text='&nbsp;Corneal Scar or Haze' />
                        <br /><asp:CheckBox runat='server' ID='CheckBoxSlitLampVogts' text='&nbsp;Vogt’s striae or other findings or keratoconus' />
                        <br /><asp:CheckBox runat='server' ID='CheckBoxSlitLampNone' text='&nbsp;None of the Above' />
                        <br /><asp:CheckBox runat='server' ID='CheckBoxSlitLampNA' text='&nbsp;Not Documented' />
                    </td>
                </tr>
            </table>
            <br />
            <!-- Assessment and Management -->
            <table>
                <tr>
                    <th colspan="3" width="910px"><p>Assessment and Management</p></th>
                </tr>
                <!-- Question 1 -->
   
                <tr class="table_body_bg">
                    <td colspan="2">
                        <strong>1. Were the refractive goals and/or options for laser vision correction explained to the patient?</strong>
                        <br /><asp:Label ID="LabelRBGoalsExplained" runat="server" Visible="false"  ForeColor="Red" Text="Please complete" />
                    </td>
                    <td>
                        <asp:RadioButtonList id='RadioButtonListRBGoalsExplained' RepeatDirection='Horizontal' CssClass='aspxList' runat='server'>
                            <asp:ListItem Value="1">&nbsp;Yes</asp:ListItem>
	                        <asp:ListItem Value="2">&nbsp;No</asp:ListItem>
	                        <asp:ListItem Value="3">&nbsp;Not Recorded</asp:ListItem>
                        </asp:RadioButtonList>
                    </td>
                </tr>
                
                <!-- Question 2 -->
                <tr class="table_body">
                    <td width="50%" rowspan="2">
                        <strong>2. What is your intended post-operative refraction (spherical equivalent)</strong>
                        <br /><asp:Label ID="LabelRBIntendedPostopRefractionOD" runat="server" Visible="false"  ForeColor="Red" Text="Please complete right" />
                        <br /><asp:Label ID="LabelRBIntendedPostopRefractionOS" runat="server" Visible="false"  ForeColor="Red" Text="Please complete left" />
                        
                    </td>
                    <td width="20%"><span class="right">Right</span></td>
                    <td>
                        <asp:TextBox runat='server' ID='TextBoxRBIntendedPostopRefractionOD' size='5' />
                    </td>
                </tr>
                <tr class="table_body">
                    <td width="20%"><span class="right">Left</span></td>
                    <td>
                        <asp:TextBox runat='server' ID='TextBoxRBIntendedPostopRefractionOS' size='5' />
                    </td>
                </tr>
	    <!-- Question 3 -->
                <tr class="table_body_bg">
                    <td colspan="2">
                        <strong>3. What eye was treated?</strong>
                        <br /><asp:Label ID="LabelAssessmentEye" runat="server" Visible="false"  ForeColor="Red" Text="Please complete" />
                    </td>
                    <td>
                        <asp:RadioButtonList id='RadioButtonListAssessmentEye'  CssClass='aspxList' runat='server'>
                          <asp:ListItem Value="108">&nbsp;Right eye</asp:ListItem>
	                        <asp:ListItem Value="109">&nbsp;Left eye</asp:ListItem>
	                        <asp:ListItem Value="110">&nbsp;Both eyes</asp:ListItem>
                        </asp:RadioButtonList>
                    </td>
                </tr>
	    <!-- Question 4 -->
                <tr class="table_body">
                    <td colspan="2">
                        <strong>4. Has patient been out of contact lenses for at least 2 weeks prior to surgery?</strong>
                        <br /><asp:Label ID="LabelRBOutOfLenses" runat="server" Visible="false"  ForeColor="Red" Text="Please complete" />    
                    </td>
                    <td>
                        <asp:RadioButtonList id='RadioButtonListRBOutOfLenses' RepeatDirection='Horizontal'  CssClass='aspxList' runat='server'>
                                 <asp:ListItem Value="1">&nbsp;Yes</asp:ListItem>
	                        <asp:ListItem Value="2">&nbsp;No</asp:ListItem>
	                        <asp:ListItem Value="3">&nbsp;Not Recorded</asp:ListItem>
                        </asp:RadioButtonList>
                    </td>
                </tr>
            </table>
		
            <br />
            <!-- Intraoperative Care -->
            <table>
                <tr>
                    <th colspan="2" width="910px"><p>Intraoperative Care</p></th>
                </tr>
                <!-- Question 1 -->
                <tr class="table_body">
                    <td width="70%">
                        <strong>1. Were the eyelids scrubbed with 5% povidone iodine prior to surgery?</strong>
                        <br /><asp:Label ID="LabelRBEyelidsScrubbed" runat="server" Visible="false"  ForeColor="Red" Text="Please complete" />    
                    </td>
                    <td>
                        <asp:RadioButtonList id='RadioButtonListRBEyelidsScrubbed' RepeatDirection='Horizontal' CssClass='aspxList' runat='server'>
                            <asp:ListItem Value="1">&nbsp;Yes</asp:ListItem>
	                        <asp:ListItem Value="2">&nbsp;No</asp:ListItem>
	                        <asp:ListItem Value="3">&nbsp;Not Recorded</asp:ListItem>
                        </asp:RadioButtonList>
                    </td>
                </tr>
                <!-- Question 2 -->
                <tr class="table_body_bg">
                    <td>
                        <strong>2. Were sterile surgical gloves and patient drapes used in surgery?</strong>
                        <br /><asp:Label ID="LabelRBSterileSurgicalGloves" runat=server Visible="false"  ForeColor="Red" Text="Please complete" />    
                    </td>
                    <td>
                        <asp:RadioButtonList id='RadioButtonListRBSterileSurgicalGloves' RepeatDirection='Horizontal' CssClass='aspxList' runat='server'>
                            <asp:ListItem Value="1">&nbsp;Yes</asp:ListItem>
	                        <asp:ListItem Value="2">&nbsp;No</asp:ListItem>
	                        <asp:ListItem Value="3">&nbsp;Not Recorded</asp:ListItem>
                        </asp:RadioButtonList>
                    </td>
                </tr>
                <!-- Question 3 -->
                <tr class="table_body">
                    <td>
                        <strong>3. Which surgical intervention was used to create the LASIK flap?</strong>
                        <br /><asp:Label ID="LabelRBSurgicalIntervention" runat=server Visible="false"  ForeColor="Red" Text="Please complete" />
                    </td>
                    <td>
                        <asp:RadioButtonList id='RadioButtonListRBSurgicalIntervention' RepeatDirection='Vertical' CssClass='aspxList' runat='server'>
                            <asp:ListItem Value='106'>&nbsp;Microkeratome</asp:ListItem>
                            <asp:ListItem Value='107'>&nbsp;Femtosecond laser</asp:ListItem>
                            <asp:ListItem Value='14'>&nbsp;Other</asp:ListItem>
                        </asp:RadioButtonList>
                    </td>
                </tr>
                <!-- Question 4 -->
                <tr class="table_body_bg">
                    <td>
                        <strong>4. Were intraoperative complications encountered?</strong>
                        <br /><asp:Label ID="LabelRBIntraopComplications" runat=server Visible="false"  ForeColor="Red" Text="Please complete" />    
                    </td>
                    <td>
                        <asp:RadioButtonList id='RadioButtonListRBIntraopComplications' RepeatDirection='Horizontal' CssClass='aspxList' runat='server'>
                            <asp:ListItem Value="1">&nbsp;Yes</asp:ListItem>
	                        <asp:ListItem Value="2">&nbsp;No</asp:ListItem>
	                        <asp:ListItem Value="3">&nbsp;Not Recorded</asp:ListItem>
                        </asp:RadioButtonList>  
                    </td>
                </tr>
                <!-- Question 5 -->
                <tr class="table_body">
                    <td><strong>5. If yes, which intraoperative complications were encountered?</strong></td>
                    <td>
                        <asp:CheckBox runat='server' ID='CheckBoxIntraopFlap' text='&nbsp;Flap striae' />
                        <br /><asp:CheckBox runat='server' ID='CheckBoxIntraopIncompleteFlap' text='&nbsp;Incomplete flap creation' />
                        <br /><asp:CheckBox runat='server' ID='CheckBoxIntraopIncompleteSide' text='&nbsp;Incomplete side cut incision' />
                        <br /><asp:CheckBox runat='server' ID='CheckBoxIntraopFlapTear' text='&nbsp;Flap tear upon lifting' />
                        <br /><asp:CheckBox runat='server' ID='CheckBoxIntraopFree' text='&nbsp;Free anterior cap' />
                        <br /><asp:CheckBox runat='server' ID='CheckBoxIntraopGlobePerforation' text='&nbsp;Globe perforation' />
                        <br /><asp:CheckBox runat='server' ID='CheckBoxIntraopButtonhole' text='&nbsp;Buttonhole flap' />
                        <br /><asp:CheckBox runat='server' ID='CheckBoxIntraopOther' text='&nbsp;Other' />
                    </td>
                </tr>
                <!-- Question 5a -->
                <tr class="table_body">
                    <td><strong>5a. If "Other," please specify:</strong></td>
                    <td>
                        <asp:TextBox runat='server' ID='TextBoxIntraopOtherText' size='25' />
                    </td>
                </tr>
                <!-- Question 6 -->
                <tr class="table_body_bg">
                    <td><strong>6. Which adjunctive procedures were necessary?</strong></td>
                    <td>
                        <asp:CheckBox runat='server' ID='CheckBoxAdjunctiveFlapLift' text='&nbsp;Flap lift and repositioning' />
                        <br /><asp:CheckBox runat='server' ID='CheckBoxAdjunctiveFlapSuturing' text='&nbsp;Flap suturing' />
                        <br /><asp:CheckBox runat='server' ID='CheckBoxAdjunctiveLaserLamellar' text='&nbsp;Additional femtosecond laser lamellar bed creation' />
                        <br /><asp:CheckBox runat='server' ID='CheckBoxAdjunctiveLaserSideCut' text='&nbsp;Additional femtosecond laser side cut creation' />
                        <br /><asp:CheckBox runat='server' ID='CheckBoxAdjunctiveOther' text='&nbsp;Other' />
                        <br /><asp:CheckBox runat='server' ID='CheckBoxAdjunctiveNont' text='&nbsp;None' />
                    </td>
                </tr>
                <!-- Question 6a -->
                <tr class="table_body_bg">
                    <td><strong>6a. If "Other," please specify:</strong></td>
                    <td>
                        <asp:TextBox runat='server' ID='TextBoxAdjunctiveOtherText' size='25' />
                    </td>
                </tr>
            </table>

            <br />
            <!-- Post-Operative Care -->
            <table>
                <tr>
                    <th colspan="3" width="910px"><p>Post-Operative Care</p></th>
                </tr>
                <!-- Question 1 -->
                <tr class="table_body">
                    <td width="70%">
                        <strong>1. Was the patient seen within 48 hours of surgery?</strong>
                        <br /><asp:Label ID="LabelRBPostop48Hours" runat=server Visible="false"  ForeColor="Red" Text="Please complete" />    
                    </td>
                    <td>
                        <asp:RadioButtonList id='RadioButtonListRBPostop48Hours' RepeatDirection='Horizontal' CssClass='aspxList' runat='server'>
                            <asp:ListItem Value="1">&nbsp;Yes</asp:ListItem>
	                        <asp:ListItem Value="2">&nbsp;No</asp:ListItem>
	                        <asp:ListItem Value="3">&nbsp;Not Recorded</asp:ListItem>
                        </asp:RadioButtonList>
                    </td>
                </tr>
                <!-- Question 2 -->
                <tr class="table_body_bg">
                    <td>
                        <strong>2. Were antibiotic drops prescribed?</strong>
                        <br /><asp:Label ID="LabelRBPostopAntibiotic" runat=server Visible="false"  ForeColor="Red" Text="Please complete" />
                    </td>
                    <td>
                        <asp:RadioButtonList id='RadioButtonListRBPostopAntibiotic' RepeatDirection='Horizontal' CssClass='aspxList' runat='server'>
                            <asp:ListItem Value="1">&nbsp;Yes</asp:ListItem>
	                        <asp:ListItem Value="2">&nbsp;No</asp:ListItem>
	                        <asp:ListItem Value="3">&nbsp;Not Recorded</asp:ListItem>
                        </asp:RadioButtonList>
                    </td>
                </tr>
                <!-- Question 3 -->
                <tr class="table_body">
                    <td>
                        <strong>3. Were anti-inflammatory drops prescribed?</strong>
                        <br /><asp:Label ID="LabelRBPostopAntiInflam" runat=server Visible="false"  ForeColor="Red" Text="Please complete" />
                    </td>
                    <td>
                        <asp:RadioButtonList id='RadioButtonListRBPostopAntiInflam' RepeatDirection='Horizontal' CssClass='aspxList' runat='server'>
                            <asp:ListItem Value="1">&nbsp;Yes</asp:ListItem>
	                        <asp:ListItem Value="2">&nbsp;No</asp:ListItem>
	                        <asp:ListItem Value="3">&nbsp;Not Recorded</asp:ListItem>
                        </asp:RadioButtonList>
                    </td>
                </tr>
                <!-- Question 4 -->
                <tr class="table_body_bg">
                    <td><strong>4. Did any of the following post-operative complications occur?</strong></td>
                    <td>
                        <asp:CheckBox runat='server' ID='CheckBoxPostopInfection' text='&nbsp;Infection' />
                        <br /><asp:CheckBox runat='server' ID='CheckBoxPostopCorneal' text='&nbsp;Corneal epithelial defect' />
                        <br /><asp:CheckBox runat='server' ID='CheckBoxPostopFlap' text='&nbsp;Flap striae' />
                        <br /><asp:CheckBox runat='server' ID='CheckBoxPostopDiffuse' text='&nbsp;Diffuse lamellar keratitis' />
                        <br /><asp:CheckBox runat='server' ID='CheckBoxPostopElevated' text='&nbsp;Elevated intraocular pressure' />
                        <br /><asp:CheckBox runat='server' ID='CheckBoxPostopOther' text='&nbsp;Other' />
                        <br /><asp:CheckBox runat='server' ID='CheckBoxPostopNone' text='&nbsp;None' />
                    </td>
                </tr>
            </table>
            <br />
            <!-- Outcome at Three Months After Surgery -->
            <table>
                <tr>
                    <th colspan="3" width="910px"><p>Outcome at Three Months After Surgery</p></th>
                </tr>
                <!-- Question 1 -->
                <tr class="table_body">
                    <td width="50%" rowspan="2"><strong>1. UNCORRECTED visual acuity at distance after surgery</strong>&nbsp;<a href="#"><img id="QO1" src="../../common/images/tip.gif" alt="Tip" /></a></td>
                    <td width="20%"><span class="right">OD</span></td>
                    <td>
                        20 / <asp:DropDownList ID="DropDownListOutcomeVAOD" DataValueField="examValue" DataTextField="examLabel" runat="server" onchange="toggleQO1ODCB();" />
                        <br /><asp:CheckBox runat='server' ID='CheckBoxOutcomeVAODNA' text='&nbsp;Not Done' onclick="toggleQO1ODTB();" />
                    </td>
                </tr>
                <tr class="table_body">
                    <td><span class="right">OS</span></td>
                    <td>
                        20 / <asp:DropDownList ID="DropDownListOutcomeVAOS" DataValueField="examValue" DataTextField="examLabel" runat="server" onchange="toggleQO1OSCB();" />
                        <br /><asp:CheckBox runat='server' ID='CheckBoxOutcomeVAOSNA' text='&nbsp;Not Done' onclick="toggleQO1OSTB();" />
                    </td>
                </tr>
                <!-- Question 2 -->
                <tr class="table_body_bg">
                    <td rowspan="2"><strong>2. Manifest refraction</strong>&nbsp;<a href="#"><img id="QO2" src="../../common/images/tip.gif" alt="Tip" /></a></td>
                    <td><span class="right">OD</span></td>
                    <td>
                        <table class="aspxList">
                            <tr>
                                <td>    
                                    <span class="right" id="Span21">
                                    <asp:RadioButton ID="RadioButtonOutcomeManifestRefractionOD1SignYes" runat="server" GroupName="OutcomeManifestRefractionOD1" Text="&nbsp;+" onclick="toggleQO2ODB();" />&nbsp;
                                    <asp:RadioButton ID="RadioButtonOutcomeManifestRefractionOD1SignNo" runat="server" GroupName="OutcomeManifestRefractionOD1" Text="&nbsp;-" onclick="toggleQO2ODB();" />
                                    </span>
                                </td>
                                <td>
                                    <span id="Span22"><asp:TextBox runat="server" meta="{vMin: '-30.00', vMax: '30.00'}" class="auto" ID="TextBoxOutcomeManifestRefractionOD1" size="3" onkeyup="toggleQO2ODB();" />&nbsp;sph</span>
                                </td>
                            </tr>
                            <tr>
                                <td><span class="right" id="Span23">+ </span></td>
                                <td><span id="Span24"><asp:TextBox runat="server" meta="{vMin: '0.00', vMax: '99.99'}" class="auto" ID="TextBoxOutcomeManifestRefractionOD2" size="3" onkeyup="toggleQO2ODB();" />&nbsp;cyl</span></td>
                            </tr>
                            <tr>
                                <td></td>
                                <td><span id="Span25"><asp:TextBox runat="server" meta="{vMin: '0.00', vMax: '180.00'}" class="auto" ID="TextBoxOutcomeManifestRefractionOD3" size="3" onkeyup="toggleQO2ODB();" />&nbsp;axis</span></td>
                            </tr>
                        </table>
                        <br /><asp:CheckBox runat='server' ID='CheckBoxOutcomeManifestRefractionODNA' text='&nbsp;Not Done' onclick="toggleQO2ODA();" />
                    </td>
                </tr>
                <tr class="table_body_bg">
                    <td><span class="right">OS</span></td>
                    <td>
                        <table class="aspxList">
                            <tr>
                                <td>    
                                    <span class="right" id="Span26">
                                    <asp:RadioButton ID="RadioButtonOutcomeManifestRefractionOS1SignYes" runat="server" GroupName="OutcomeManifestRefractionOS1" Text="&nbsp;+" onclick="toggleQO2OSB();" />&nbsp;
                                    <asp:RadioButton ID="RadioButtonOutcomeManifestRefractionOS1SignNo" runat="server" GroupName="OutcomeManifestRefractionOS1" Text="&nbsp;-" onclick="toggleQO2OSB();" />
                                    </span>
                                </td>
                                <td>
                                    <span id="Span27"><asp:TextBox runat="server" meta="{vMin: '-30.00', vMax: '30.00'}" class="auto" ID="TextBoxOutcomeManifestRefractionOS1" size="3" onkeyup="toggleQO2OSB();" />&nbsp;sph</span>
                                </td>
                            </tr>
                            <tr>
                                <td><span class="right" id="Span28">+ </span></td>
                                <td><span id="Span29"><asp:TextBox runat="server" meta="{vMin: '0.00', vMax: '99.99'}" class="auto" ID="TextBoxOutcomeManifestRefractionOS2" size="3" onkeyup="toggleQO2OSB();" />&nbsp;cyl</span></td>
                            </tr>
                            <tr>
                                <td></td>
                                <td><span id="Span30"><asp:TextBox runat="server" meta="{vMin: '0.00', vMax: '180.00'}" class="auto" ID="TextBoxOutcomeManifestRefractionOS3" size="3" onkeyup="toggleQO2OSB();" />&nbsp;axis</span></td>
                            </tr>
                        </table>
                        <asp:CheckBox runat='server' ID='CheckBoxOutcomeManifestRefractionOSNA' text='&nbsp;Not Done' onclick="toggleQO2OSA();" />
                    </td>
                </tr>
                <!-- Question 3 -->
                <tr class="table_body">
                    <td colspan="2" id="QO3M3a">
                        <strong>3. If UNCORRECTED visual acuity was worse than 20/40, list the reason</strong>
                        
                    </td>
                    <td id="QO3M3b">
                        <asp:CheckBox runat='server' ID='CheckBoxOutcomeVABadNA' text='&nbsp;N/A' />
                        <br /><asp:CheckBox runat='server' ID='CheckBoxOutcomeVABadResidual' text='&nbsp;Residual refractive error' />
                        <br /><asp:CheckBox runat='server' ID='CheckBoxOutcomeVADryEye' text='&nbsp;Dry eye syndrome' />
                        <br /><asp:CheckBox runat='server' ID='CheckBoxOutcomeVAFlap' text='&nbsp;Flap complication' />
                        <br /><asp:CheckBox runat='server' ID='CheckBoxOutcomeVADiffuse' text='&nbsp;Diffuse lamellar keratitis' />
                        <br /><asp:CheckBox runat='server' ID='CheckBoxOutcomeVAPlanned' text='&nbsp;Planned monovision' />
                        <br /><asp:CheckBox runat='server' ID='CheckBoxOutcomeVAUnknown' text='&nbsp;Unknown' />
                        <br /><asp:CheckBox runat='server' ID='CheckBoxOutcomeVAOther' text='&nbsp;Other' /> 
                        <br /><asp:CheckBox runat='server' ID='CheckBoxOutcomeVANA' text='&nbsp;Not Documented' />
                    </td>
                </tr>
                <tr class="table_body">
                    <td colspan="2"><strong>3a. If "Other," please specify:</strong></td>
                    <td><asp:TextBox runat='server' ID='TextBoxOutcomeVAOtherText' size='25' /></td>
                </tr>
                <!-- Question 4 -->
                <tr class="table_body_bg">
                    <td rowspan="2">
                        <strong>4. Was the final refractive target within &plusmn;0.50 diopter of predicted after surgery?</strong>
                        <br /><asp:Label ID="LabelRBOutcomeTargetOD" runat="server" Visible="false"  ForeColor="Red" Text="Please complete" />
                        <br /><asp:Label ID="LabelRBOutcomeTargetOS" runat="server" Visible="false"  ForeColor="Red" Text="Please complete" />

                    </td>
                    <td><span class="right">OD</span></td>
                    <td>
                        <asp:RadioButtonList id='RadioButtonListRBOutcomeTargetOD' RepeatDirection='Horizontal' CssClass='aspxList' runat='server'>
                            <asp:ListItem Value="1">&nbsp;Yes</asp:ListItem>
	                        <asp:ListItem Value="2">&nbsp;No</asp:ListItem>
	                        <asp:ListItem Value="3">&nbsp;Not Recorded</asp:ListItem>
                        </asp:RadioButtonList>
                    </td>
                </tr>
                <tr class="table_body_bg">
                    <td><span class="right">OS</span></td>
                    <td>
                        <asp:RadioButtonList id='RadioButtonListRBOutcomeTargetOS' RepeatDirection='Horizontal' CssClass='aspxList' runat='server'>
                            <asp:ListItem Value="1">&nbsp;Yes</asp:ListItem>
	                        <asp:ListItem Value="2">&nbsp;No</asp:ListItem>
	                        <asp:ListItem Value="3">&nbsp;Not Recorded</asp:ListItem>
                        </asp:RadioButtonList>
                    </td>
                </tr>
                <!-- Question 5 -->
                <tr class="table_body">
                    <td colspan="2"><strong>5. Were any post-operative complications encountered? If yes, please check all that apply:</strong></td>
                    <td>
                        <asp:CheckBox runat='server' ID='CheckBoxOutcomeComplicationNone' text='&nbsp;None' />
                        <br /><asp:CheckBox runat='server' ID='CheckBoxOutcomeComplicationMicro' text='&nbsp;Microbial keratitis' />
                        <br /><asp:CheckBox runat='server' ID='CheckBoxOutcomeComplicationMacro' text='&nbsp;Macrostriae of LASIK flap' />
                        <br /><asp:CheckBox runat='server' ID='CheckBoxOutcomeComplicationEpith' text='&nbsp;Epithelial ingrowth' />
                        <br /><asp:CheckBox runat='server' ID='CheckBoxOutcomeComplicationDiffuse' text='&nbsp;Diffuse lamellar keratitis' />
                        <br /><asp:CheckBox runat='server' ID='CheckBoxOutcomeComplicationRetinal' text='&nbsp;Retinal detachment' />
                        <br /><asp:CheckBox runat='server' ID='CheckBoxOutcomeComplicationIrregular' text='&nbsp;Irregular astigmatism' />
                        <br /><asp:CheckBox runat='server' ID='CheckBoxOutcomeComplicationOther' text='&nbsp;Other' />
                        <br /><asp:CheckBox runat='server' ID='CheckBoxOutcomeComplicationNA' text='&nbsp;Not Documented' />
                    </td>
                </tr>
                <!-- Question 5a -->
                <tr class="table_body">
                    <td colspan="2"><strong>5a. If "Other," please specify:</strong></td>
                    <td><asp:TextBox runat='server' ID='TextBoxOutcomeComplicationOtherText' size='25' /></td>
                </tr>
                <!-- Question 6 -->
                <tr class="table_body_bg">
                    <td colspan="2">
                        <strong>6. Was enhancement surgery necessary after surgery?</strong>
                        <br /><asp:Label ID="LabelRBOutcomeEnhancementSurgery" runat=server Visible="false"  ForeColor="Red" Text="Please complete" />    
                    </td>
                    <td>
                        <asp:RadioButtonList id='RadioButtonListRBOutcomeEnhancementSurgery' RepeatDirection='Vertical' CssClass='aspxList' runat='server'>
                            <asp:ListItem Value='108'>&nbsp;Right eye</asp:ListItem>
                            <asp:ListItem Value='109'>&nbsp;Left eye</asp:ListItem>
                            <asp:ListItem Value='110'>&nbsp;Both eyes</asp:ListItem>
                            <asp:ListItem Value='2'>&nbsp;No</asp:ListItem>
                            <asp:ListItem Value='3'>&nbsp;Not documented</asp:ListItem>
                        </asp:RadioButtonList>
                    </td>
                </tr>
                <!-- Question 7 -->
                <tr class="table_body">
                    <td rowspan="3" id="Q7O3Ma">
                        <strong>7. Date enhancement surgery was performed:</strong>
                        <br /><asp:Label ID="LabelOutcomeEnhancementSurgeryODMonth" runat=server Visible="false"  ForeColor="Red" Text="Please complete OD - Month" />
                        <br /><asp:Label ID="LabelOutcomeEnhancementSurgeryODYear" runat=server Visible="false"  ForeColor="Red" Text="Please complete OD - Year" />
                        <br /><asp:Label ID="LabelOutcomeEnhancementSurgeryOSMonth" runat=server Visible="false"  ForeColor="Red" Text="Please complete OS - Month" />
                        <br /><asp:Label ID="LabelOutcomeEnhancementSurgeryOSYear" runat=server Visible="false"  ForeColor="Red" Text="Please complete OS - Year" />    
                    </td>
                    <td id="Q7O3Mb"><span class="right">OD</span></td>
                    <td id="Q7O3Mc">
                        <asp:DropDownList ID='DropDownListOutcomeEnhancementSurgeryODMonth' DataValueField='MonthID' DataTextField='MonthName' runat='server' onchange="toggleQO7CB();" />
                        <asp:DropDownList ID='DropDownListOutcomeEnhancementSurgeryODYear' DataValueField='YearID' DataTextField='YearName' runat='server' onchange="toggleQO7CB();" />
                    </td>
                </tr>
                <tr class="table_body">
                    <td id="Q7O3Md"><span class="right">OS</span></td>
                    <td id="Q7O3Me">
                        <asp:DropDownList ID='DropDownListOutcomeEnhancementSurgeryOSMonth' DataValueField='MonthID' DataTextField='MonthName' runat='server' onchange="toggleQO7CB();" />
                        <asp:DropDownList ID='DropDownListOutcomeEnhancementSurgeryOSYear' DataValueField='YearID' DataTextField='YearName' runat='server' onchange="toggleQO7CB();" />
                    </td>
                </tr>
                <tr class="table_body">
                    <td></td>
                    <td id="Q7O3Mf">
                        <asp:CheckBox runat='server' ID='CheckBoxOutcomeEnhancementSurgeryNA' text='&nbsp;Not Documented' onchange="toggleQO7DD();" />
                    </td>
                </tr>
                <!-- Question 8 -->
                <tr class="table_body">
                    <td colspan="2">
                        <strong>8. Did best corrected visual acuity decrease by more than one line in 
                        treated eye?</strong>
                        <br /><asp:Label ID="LabelOutcomeBCVADecrease" runat="server" Visible="false"  ForeColor="Red" Text="Please complete" />    
                    </td>
                    <td>
                        <asp:RadioButton runat='server' ID='RadioButtonOutcomeBCVADecreaseYes' GroupName='OutcomeBCVADecrease' text='&nbsp;Yes' />
                        <asp:RadioButton runat='server' ID='RadioButtonOutcomeBCVADecreaseNo' GroupName='OutcomeBCVADecrease' text='&nbsp;No' />
                    </td>
                </tr>
            </table>
            <div class="button-box">
                <asp:LinkButton ID="LinkButtonBackToDashboard" runat="server" Text="Back To Chart Registration" PostBackUrl="../PatientChartRegistration.aspx?CycleNumber=1" Visible="false" CssClass="button" />
                <asp:LinkButton ID="ButtonSubmit" OnClick="ButtonSubmit_Click" runat="server" Text="Submit Chart" CssClass="button" />
            </div>
        </div>
        <!-- ION pim ends -->

        <script>
            $(document).ready(function () {
                var flatOD = numeral($("#<%= TextBoxKeratometryFlatOD.ClientID %>").val()).format('0.00');
                var steepOD = numeral($("#<%= TextBoxKeratometrySteepOD.ClientID %>").val()).format('0.00');
                var flatOS = numeral($("#<%= TextBoxKeratometryFlatOS.ClientID %>").val()).format('0.00');
                var steepOS = numeral($("#<%= TextBoxKeratometrySteepOS.ClientID %>").val()).format('0.00');

                $("#<%= TextBoxKeratometryFlatOD.ClientID %>").val(flatOD);
                $("#<%= TextBoxKeratometrySteepOD.ClientID %>").val(steepOD);
                $("#<%= TextBoxKeratometryFlatOS.ClientID %>").val(flatOS);
                $("#<%= TextBoxKeratometrySteepOS.ClientID %>").val(steepOS);
            });
        </script>
</asp:Content>

<asp:Content runat="server" ID="Content3" ContentPlaceHolderID="javascript">
        <script type="text/javascript" src="../../common/js/jquery.metadata.js"></script> <!--when changing defaults-->
    <script type="text/javascript" src="../../common/js/autoNumeric-1.7.5.js"></script>
	<script type="text/javascript" src="../../common/js/jquery.atooltip.js"></script>
    <script src="../../common/js/numeral.js"></script>
    <script type="text/javascript" language="javascript">
        //      Either enables or disables input boxes
        function enabledControl(el, disabled, checked) {
            //alert("Hello world from enabled control");
            try {
                if (disabled) {
                    el.disabled = disabled;
                    el.setAttribute("disabled", true);
                }
                else {
                    el.disabled = disabled;
                    el.removeAttribute("disabled");
                }

                if (checked == true)
                    el.checked = false;
            }
            catch (E) {
            }
            if (el.childNodes && el.childNodes.length > 0) {
                for (var x = 0; x < el.childNodes.length; x++) {
                    enabledControl(el.childNodes[x], disabled, checked);
                }
            }
        }
        //      Either enables or disables dropdown boxes
        function enabledControlDropDown(el, disabled, checked) {
            //alert("Hello world from enabled control");
            try {
                if (disabled) {
                    el.disabled = disabled;
                    el.setAttribute("disabled", true);
                }
                else {
                    el.disabled = disabled;
                    el.removeAttribute("disabled");
                }

                if (checked == true)
                    el.options[0].selected = true;
            }
            catch (E) {
            }
            if (el.childNodes && el.childNodes.length > 0) {
                for (var x = 0; x < el.childNodes.length; x++) {
                    enabledControl(el.childNodes[x], disabled, checked);
                }
            }
        }
        //      Either enables or disables text boxes
        function enabledControlTextField(el, disabled, checked) {
            //alert("Hello world from enabled control");
            try {
                if (disabled) {
                    el.disabled = disabled;
                    el.setAttribute("disabled", true);
                }
                else {
                    el.disabled = disabled;
                    el.removeAttribute("disabled");
                }

                if (checked == true) {
                    el.value = '';
                }
            }
            catch (E) {
            }
            if (el.childNodes && el.childNodes.length > 0) {
                for (var x = 0; x < el.childNodes.length; x++) {
                    enabledControl(el.childNodes[x], disabled, checked);
                }
            }
        }

        function generate(arr1, arr2, istrue) {
            if (istrue == true) {
                for (var i = 0; i < arr1.length; i++) {
                    document.getElementById(arr1[i]).style.color = "gray";
                }
                for (var i = 0; i < arr2.length; i++) {
                    $(arr2[i] + ' :input').attr('disabled', true);
                    $(arr2[i] + ' :input').removeAttr("checked");
                }
            }
            if (istrue == false) {
                for (var i = 0; i < arr1.length; i++) {
                    document.getElementById(arr1[i]).style.color = "#333";
                }
                for (var i = 0; i < arr2.length; i++) {
                    $(arr2[i] + ' :input').removeAttr('disabled');
                }
            }
        }
        // QE1
        function toggleQE1A() {
            document.getElementById("<%= RadioButtonRefractiveCorrectionOD1SignYes.ClientID %>").checked = false;
            document.getElementById("<%= RadioButtonRefractiveCorrectionOD1SignNo.ClientID %>").checked = false;
            document.getElementById("<%= TextBoxRefractiveCorrectionOD1.ClientID %>").value = '';
            document.getElementById("<%= TextBoxRefractiveCorrectionOD2.ClientID %>").value = '';
            document.getElementById("<%= TextBoxRefractiveCorrectionOD3.ClientID %>").value = '';
            document.getElementById("<%= RadioButtonRefractiveCorrectionOS1SignYes.ClientID %>").checked = false;
            document.getElementById("<%= RadioButtonRefractiveCorrectionOS1SignNo.ClientID %>").checked = false;
            document.getElementById("<%= TextBoxRefractiveCorrectionOS1.ClientID %>").value = '';
            document.getElementById("<%= TextBoxRefractiveCorrectionOS2.ClientID %>").value = '';
            document.getElementById("<%= TextBoxRefractiveCorrectionOS3.ClientID %>").value = '';
        }
        function toggleQE1B() {
            document.getElementById("<%= CheckBoxRefractiveCorrectionNA.ClientID %>").checked = false;
        }
        // QE2 OD
        function toggleQE2ODB() {
            document.getElementById("<%= RadioButtonManifestRefractionOD1SignYes.ClientID %>").checked = false;
            document.getElementById("<%= RadioButtonManifestRefractionOD1SignNo.ClientID %>").checked = false;
            document.getElementById("<%= TextBoxManifestRefractionOD1.ClientID %>").value = '';
            document.getElementById("<%= TextBoxManifestRefractionOD2.ClientID %>").value = '';
            document.getElementById("<%= TextBoxManifestRefractionOD3.ClientID %>").value = '';
        }
        function toggleQE2ODA() {
            document.getElementById("<%= CheckBoxManifestRefractionODNA.ClientID %>").checked = false;
        }
        // QE2 OS
        function toggleQE2OSB() {
            document.getElementById("<%= RadioButtonManifestRefractionOS1SignYes.ClientID %>").checked = false;
            document.getElementById("<%= RadioButtonManifestRefractionOS1SignNo.ClientID %>").checked = false;
            document.getElementById("<%= TextBoxManifestRefractionOS1.ClientID %>").value = '';
            document.getElementById("<%= TextBoxManifestRefractionOS2.ClientID %>").value = '';
            document.getElementById("<%= TextBoxManifestRefractionOS3.ClientID %>").value = '';
        }
        function toggleQE2OSA() {
            document.getElementById("<%= CheckBoxManifestRefractionOSNA.ClientID %>").checked = false;
        }
        // QE4
        function toggleQE4DD() {
            document.getElementById("<%= DropDownListDistVAManifestRefractionOD.ClientID %>").options[0].selected = true;
            document.getElementById("<%= DropDownListDistVAManifestRefractionOS.ClientID %>").options[0].selected = true;
        }
        function toggleQE4CB() {
            document.getElementById("<%= CheckBoxDistVAManifestRefractionNA.ClientID %>").checked = false;
        }
        // QE5
        function toggleQE5CB() {
            document.getElementById("<%= CheckBoxNearVAManifestRefractionNA.ClientID %>").checked = false;
        }
        function toggleQE5DD() {
            document.getElementById("<%= DropDownListNearVAManifestRefractionOD.ClientID %>").options[0].selected = true;
            document.getElementById("<%= DropDownListNearVAManifestRefractionOS.ClientID %>").options[0].selected = true;
        }
        // QE6 OD
        function toggleQE6ODB() {
            document.getElementById("<%= RadioButtonCycloplegicRefractionOD1SignYes.ClientID %>").checked = false;
            document.getElementById("<%= RadioButtonCycloplegicRefractionOD1SignNo.ClientID %>").checked = false;
            document.getElementById("<%= TextBoxCycloplegicRefractionOD1.ClientID %>").value = '';
            document.getElementById("<%= TextBoxCycloplegicRefractionOD2.ClientID %>").value = '';
            document.getElementById("<%= TextBoxCycloplegicRefractionOD3.ClientID %>").value = '';
        }
        function toggleQE6ODA() {
            document.getElementById("<%= CheckBoxCycloplegicRefractionODNA.ClientID %>").checked = false;
        }
        // QE6 OS
        function toggleQE6OSB() {
            document.getElementById("<%= RadioButtonCycloplegicRefractionOS1SignYes.ClientID %>").checked = false;
            document.getElementById("<%= RadioButtonCycloplegicRefractionOS1SignNo.ClientID %>").checked = false;
            document.getElementById("<%= TextBoxCycloplegicRefractionOS1.ClientID %>").value = '';
            document.getElementById("<%= TextBoxCycloplegicRefractionOS2.ClientID %>").value = '';
            document.getElementById("<%= TextBoxCycloplegicRefractionOS3.ClientID %>").value = '';
        }
        function toggleQE6OSA() {
            document.getElementById("<%= CheckBoxCycloplegicRefractionOSNA.ClientID %>").checked = false;
        }
        // QE7

        function toggleQE7B() {
            document.getElementById("<%= CheckBoxDistanceVACycloplegicNA.ClientID %>").checked = false;
        }
        // QE11

        function toggleQE11B() {
            document.getElementById("<%= CheckBoxPupilSizeNA.ClientID %>").checked = false;
        }
        // QE12 OD
        function toggleQE12ODTB() {
            document.getElementById("<%= TextBoxIOPOD.ClientID %>").value = '';
        }
        function toggleQE12ODCB() {
            document.getElementById("<%= CheckBoxIOPODNA.ClientID %>").checked = false;
        }

        // QE12 OS
        function toggleQE12OSTB() {
            document.getElementById("<%= TextBoxIOPOS.ClientID %>").value = '';
        }
        function toggleQE12OSCB() {
            document.getElementById("<%= CheckBoxIOPOSNA.ClientID %>").checked = false;
        }

        // QE13
        function toggleQE13DD() {
            document.getElementById("<%= TextBoxKeratometryFlatOD.ClientID %>").value = '';
            document.getElementById("<%= TextBoxKeratometrySteepOD.ClientID %>").value = '';
            document.getElementById("<%= TextBoxKeratometryAxisOD.ClientID %>").value = '';
            document.getElementById("<%= TextBoxKeratometryFlatOS.ClientID %>").value = '';
            document.getElementById("<%= TextBoxKeratometrySteepOS.ClientID %>").value = '';
            document.getElementById("<%= TextBoxKeratometryAxisOS.ClientID %>").value = '';
        }
        function toggleQE13CB() {
            document.getElementById("<%= CheckBoxKeratometryNA.ClientID %>").checked = false;
        }

        // QE14 OD
        function toggleQE14ODTB() {
            document.getElementById("<%= TextBoxCornealThicknessOD.ClientID %>").value = '';
        }
        function toggleQE14ODCB() {
            document.getElementById("<%= CheckBoxCornealThicknessODNA.ClientID %>").checked = false;
        }

        // QE14 OS
        function toggleQE14OSTB() {
            document.getElementById("<%= TextBoxCornealThicknessOS.ClientID %>").value = '';
        }
        function toggleQE14OSCB() {
            document.getElementById("<%= CheckBoxCornealThicknessOSNA.ClientID %>").checked = false;
        }

        // QO1 OD
        function toggleQO1ODTB() {
            document.getElementById("<%= DropDownListOutcomeVAOD.ClientID %>").options[0].selected = true;
            disableQ3onChange()
        }

        function toggleQO1ODCB() {
            document.getElementById("<%= CheckBoxOutcomeVAODNA.ClientID %>").checked = false;
            disableQ3onChange();
        }

        // QO1 OS
        function toggleQO1OSTB() {
            document.getElementById("<%= DropDownListOutcomeVAOS.ClientID %>").options[0].selected = true;
            disableQ3onChange()
        }

        function toggleQO1OSCB() {
            document.getElementById("<%= CheckBoxOutcomeVAOSNA.ClientID %>").checked = false;
            disableQ3onChange();
        }

        // Function that disables Question 3 based upon how question 1 is answered
        function disableQ3onChange() {
            if ((document.getElementById("<%= DropDownListOutcomeVAOD.ClientID %>").value > 40) || (document.getElementById("<%= DropDownListOutcomeVAOS.ClientID %>").value > 40)) {
                var myaray = new Array("QO3M3a", "QO3M3b");
                var myarray2 = new Array('#QO3M3b');
                generate(myaray, myarray2, false);
            }
            else {
                var myaray = new Array("QO3M3a", "QO3M3b");
                var myarray2 = new Array('#QO3M3b');
                generate(myaray, myarray2, true);
            }
        }

        //toggleQO2 OD
        function toggleQO2ODA() {
            document.getElementById("<%= RadioButtonOutcomeManifestRefractionOD1SignYes.ClientID %>").checked = false;
            document.getElementById("<%= RadioButtonOutcomeManifestRefractionOD1SignNo.ClientID %>").checked = false;
            document.getElementById("<%= TextBoxOutcomeManifestRefractionOD1.ClientID %>").value = '';
            document.getElementById("<%= TextBoxOutcomeManifestRefractionOD2.ClientID %>").value = '';
            document.getElementById("<%= TextBoxOutcomeManifestRefractionOD3.ClientID %>").value = '';
        }
        function toggleQO2ODB() {
            document.getElementById("<%= CheckBoxOutcomeManifestRefractionODNA.ClientID %>").checked = false;
        }

        //toggleQO2 OS
        function toggleQO2OSA() {
            document.getElementById("<%= RadioButtonOutcomeManifestRefractionOS1SignYes.ClientID %>").checked = false;
            document.getElementById("<%= RadioButtonOutcomeManifestRefractionOS1SignNo.ClientID %>").checked = false;
            document.getElementById("<%= TextBoxOutcomeManifestRefractionOS1.ClientID %>").value = '';
            document.getElementById("<%= TextBoxOutcomeManifestRefractionOS2.ClientID %>").value = '';
            document.getElementById("<%= TextBoxOutcomeManifestRefractionOS3.ClientID %>").value = '';
        }
        function toggleQO2OSB() {
            document.getElementById("<%= CheckBoxOutcomeManifestRefractionOSNA.ClientID %>").checked = false;
        }

        function toggleQO7DD() {
            document.getElementById("<%= DropDownListOutcomeEnhancementSurgeryODMonth.ClientID %>").options[0].selected = true;
            document.getElementById("<%= DropDownListOutcomeEnhancementSurgeryODYear.ClientID %>").options[0].selected = true;
            document.getElementById("<%= DropDownListOutcomeEnhancementSurgeryOSMonth.ClientID %>").options[0].selected = true;
            document.getElementById("<%= DropDownListOutcomeEnhancementSurgeryOSYear.ClientID %>").options[0].selected = true;
        }
        function toggleQO7CB() {
            document.getElementById("<%= CheckBoxOutcomeEnhancementSurgeryNA.ClientID %>").checked = false;
        }

        //toggleQE7
        function toggleQE7DD() {
            document.getElementById("<%= DropDownListDistanceVACycloplegicOD.ClientID %>").options[0].selected = true;
            document.getElementById("<%= DropDownListDistanceVACycloplegicOS.ClientID %>").options[0].selected = true;
        }
        function toggleQE7CB() {
            document.getElementById("<%= CheckBoxDistanceVACycloplegicNA.ClientID %>").checked = false;
        }

    </script>

    <script  type="text/javascript" language="javascript">
        $(document).ready(function () {
            disableQ3onChange();

            $("#<%= RadioButtonHistoryOtherYes.ClientID %>").click(function () {
                if ($('#<%= RadioButtonHistoryOtherYes.ClientID %>').prop("checked") == true) {
                    enabledControlTextField(document.getElementById("<%= TextBoxHistoryOtherText.ClientID %>"), false, false);
                }
                else {
                    enabledControlTextField(document.getElementById("<%= TextBoxHistoryOtherText.ClientID %>"), true, true);
                }
            });
            $("#<%= RadioButtonHistoryOtherNo.ClientID %>").click(function () {
                if ($('#<%= RadioButtonHistoryOtherYes.ClientID %>').prop("checked") == true) {
                    enabledControlTextField(document.getElementById("<%= TextBoxHistoryOtherText.ClientID %>"), false, false);
                }
                else {
                    enabledControlTextField(document.getElementById("<%= TextBoxHistoryOtherText.ClientID %>"), true, true);
                }
            });

            if ($('#<%= RadioButtonHistoryOtherYes.ClientID %>').prop("checked") == true) {
                enabledControlTextField(document.getElementById("<%= TextBoxHistoryOtherText.ClientID %>"), false, false);
            }
            else {
                enabledControlTextField(document.getElementById("<%= TextBoxHistoryOtherText.ClientID %>"), true, true);
            }

            $("#<%= CheckBoxIntraopOther.ClientID %>").click(function () {
                if ($('#<%= CheckBoxIntraopOther.ClientID %>').prop("checked") == true) {
                    enabledControlTextField(document.getElementById("<%= TextBoxIntraopOtherText.ClientID %>"), false, false);
                }
                else {
                    enabledControlTextField(document.getElementById("<%= TextBoxIntraopOtherText.ClientID %>"), true, true);
                }
            });

            if ($('#<%= CheckBoxIntraopOther.ClientID %>').prop("checked") == true) {
                enabledControlTextField(document.getElementById("<%= TextBoxIntraopOtherText.ClientID %>"), false, false);
            }
            else {
                enabledControlTextField(document.getElementById("<%= TextBoxIntraopOtherText.ClientID %>"), true, true);
            }

            $("#<%= CheckBoxAdjunctiveOther.ClientID %>").click(function () {
                if ($('#<%= CheckBoxAdjunctiveOther.ClientID %>').prop("checked") == true) {
                    enabledControlTextField(document.getElementById("<%= TextBoxAdjunctiveOtherText.ClientID %>"), false, false);
                }
                else {
                    enabledControlTextField(document.getElementById("<%= TextBoxAdjunctiveOtherText.ClientID %>"), true, true);
                }
            });

            if ($('#<%= CheckBoxAdjunctiveOther.ClientID %>').prop("checked") == true) {
                enabledControlTextField(document.getElementById("<%= TextBoxAdjunctiveOtherText.ClientID %>"), false, false);
            }
            else {
                enabledControlTextField(document.getElementById("<%= TextBoxAdjunctiveOtherText.ClientID %>"), true, true);
            }

            $("#<%= CheckBoxOutcomeVAOther.ClientID %>").click(function () {
                if ($('#<%= CheckBoxOutcomeVAOther.ClientID %>').prop("checked") == true) {
                    enabledControlTextField(document.getElementById("<%= TextBoxOutcomeVAOtherText.ClientID %>"), false, false);
                }
                else {
                    enabledControlTextField(document.getElementById("<%= TextBoxOutcomeVAOtherText.ClientID %>"), true, true);
                }
            });

            if ($('#<%= CheckBoxOutcomeVAOther.ClientID %>').prop("checked") == true) {
                enabledControlTextField(document.getElementById("<%= TextBoxOutcomeVAOtherText.ClientID %>"), false, false);
            }
            else {
                enabledControlTextField(document.getElementById("<%= TextBoxOutcomeVAOtherText.ClientID %>"), true, true);
            }

            $("#<%= CheckBoxOutcomeComplicationOther.ClientID %>").click(function () {
                if ($('#<%= CheckBoxOutcomeComplicationOther.ClientID %>').prop("checked") == true) {
                    enabledControlTextField(document.getElementById("<%= TextBoxOutcomeComplicationOtherText.ClientID %>"), false, false);
                }
                else {
                    enabledControlTextField(document.getElementById("<%= TextBoxOutcomeComplicationOtherText.ClientID %>"), true, true);
                }
            });

            if ($('#<%= CheckBoxOutcomeComplicationOther.ClientID %>').prop("checked") == true) {
                enabledControlTextField(document.getElementById("<%= TextBoxOutcomeComplicationOtherText.ClientID %>"), false, false);
            }
            else {
                enabledControlTextField(document.getElementById("<%= TextBoxOutcomeComplicationOtherText.ClientID %>"), true, true);
            }

            // Question 6 --- Outcome at Three Months After Surgery
            $("#<%= RadioButtonListRBOutcomeEnhancementSurgery.ClientID %>").click(function () {
                if (($('#<%= RadioButtonListRBOutcomeEnhancementSurgery.ClientID %>').find('input:checked').val() == '2') || ($('#<%= RadioButtonListRBOutcomeEnhancementSurgery.ClientID %>').find('input:checked').val() == '3')) {
                    var myaray = new Array("Q7O3Ma", "Q7O3Mb", "Q7O3Mc", "Q7O3Md", "Q7O3Me", "Q7O3Mf");
                    var myarray2 = new Array('#Q7O3Mc', '#Q7O3Me', '#Q7O3Mf');
                    generate(myaray, myarray2, true);

                    enabledControlDropDown(document.getElementById("<%= DropDownListOutcomeEnhancementSurgeryODMonth.ClientID %>"), true, true);
                    enabledControlDropDown(document.getElementById("<%= DropDownListOutcomeEnhancementSurgeryODYear.ClientID %>"), true, true);
                    enabledControlDropDown(document.getElementById("<%= DropDownListOutcomeEnhancementSurgeryOSMonth.ClientID %>"), true, true);
                    enabledControlDropDown(document.getElementById("<%= DropDownListOutcomeEnhancementSurgeryOSYear.ClientID %>"), true, true);
                }
                else {
                    var myaray = new Array("Q7O3Ma", "Q7O3Mb", "Q7O3Mc", "Q7O3Md", "Q7O3Me", "Q7O3Mf");
                    var myarray2 = new Array('#Q7O3Mc', '#Q7O3Me', '#Q7O3Mf');
                    generate(myaray, myarray2, false);

                    enabledControlDropDown(document.getElementById("<%= DropDownListOutcomeEnhancementSurgeryODMonth.ClientID %>"), false, false);
                    enabledControlDropDown(document.getElementById("<%= DropDownListOutcomeEnhancementSurgeryODYear.ClientID %>"), false, false);
                    enabledControlDropDown(document.getElementById("<%= DropDownListOutcomeEnhancementSurgeryOSMonth.ClientID %>"), false, false);
                    enabledControlDropDown(document.getElementById("<%= DropDownListOutcomeEnhancementSurgeryOSYear.ClientID %>"), false, false);
                }
            });
            if (($('#<%= RadioButtonListRBOutcomeEnhancementSurgery.ClientID %>').find('input:checked').val() == '2') || ($('#<%= RadioButtonListRBOutcomeEnhancementSurgery.ClientID %>').find('input:checked').val() == '3')) {
                var myaray = new Array("Q7O3Ma", "Q7O3Mb", "Q7O3Mc", "Q7O3Md", "Q7O3Me", "Q7O3Mf");
                var myarray2 = new Array('#Q7O3Mc', '#Q7O3Me', '#Q7O3Mf');
                generate(myaray, myarray2, true);

                enabledControlDropDown(document.getElementById("<%= DropDownListOutcomeEnhancementSurgeryODMonth.ClientID %>"), true, true);
                enabledControlDropDown(document.getElementById("<%= DropDownListOutcomeEnhancementSurgeryODYear.ClientID %>"), true, true);
                enabledControlDropDown(document.getElementById("<%= DropDownListOutcomeEnhancementSurgeryOSMonth.ClientID %>"), true, true);
                enabledControlDropDown(document.getElementById("<%= DropDownListOutcomeEnhancementSurgeryOSYear.ClientID %>"), true, true);
            }
            else {
                var myaray = new Array("Q7O3Ma", "Q7O3Mb", "Q7O3Mc", "Q7O3Md", "Q7O3Me", "Q7O3Mf");
                var myarray2 = new Array('#Q7O3Mc', '#Q7O3Me', '#Q7O3Mf');
                generate(myaray, myarray2, false);

                enabledControlDropDown(document.getElementById("<%= DropDownListOutcomeEnhancementSurgeryODMonth.ClientID %>"), false, false);
                enabledControlDropDown(document.getElementById("<%= DropDownListOutcomeEnhancementSurgeryODYear.ClientID %>"), false, false);
                enabledControlDropDown(document.getElementById("<%= DropDownListOutcomeEnhancementSurgeryOSMonth.ClientID %>"), false, false);
                enabledControlDropDown(document.getElementById("<%= DropDownListOutcomeEnhancementSurgeryOSYear.ClientID %>"), false, false);
            }
        });
    </script>

    <script type="text/javascript" language="javascript">
        $(document).ready(function () {
            /** instruct the metadata plugin where to look the metadata
            * jQuery.metadata.setType( type, name );
            * please read the metadata instructions for additional information
            * http://plugins.jquery.com/project/metadata
            */
            $.metadata.setType('attr', 'meta');

            /** To call autoNumeric
            * $(selector).autoNumeric({options}); 
            * The below example uses the input & class selector
            */
            $('input.auto').autoNumeric();
            /* scripts for metadata code generator  */

            /* rountine that prevents  numeric characters from being entered the the altDec field  */
            $('#altDecb').keypress(function (e) {
                var cc = String.fromCharCode(e.which);
                if (e.which != 32 && cc >= 0 && cc <= 9) {
                    e.preventDefault();
                }
            });

            /* rountine that prevents  apostrophe, comma, more than one period (full stop) or numeric characters from being entered the the aSign field  */
            $('#aSignb').keypress(function (e) {
                var cc = String.fromCharCode(e.which);
                if ((e.which != 32 && cc >= 0 && cc <= 9) || cc == "," || cc == "'" || cc == "." && this.value.lastIndexOf('.') != -1) {
                    e.preventDefault();
                }
            });

            $("input.md").bind('click keyup blur', function () {
                var metaCode = '', aSep = '', dGroup = '', aDec = '', altDec = '', aSign = '', pSign = '', vMin = '', vMax = '', mDec = '', mRound = '', aPad = '', wEmpty = '', aForm = '';
                if ($("input:radio[name=aSep]:checked").attr('id') == 'aSepc') {
                    $('input:radio[name=aDec]:nth(0)').removeAttr("disabled");
                    $('input:radio[name=aDec]:nth(0)').attr('checked', true);
                    $('input:radio[name=aDec]:nth(1)').attr("disabled", true);
                }
                if ($("input:radio[name=aSep]:checked").attr('id') == 'aSepp') {
                    $('input:radio[name=aDec]:nth(1)').removeAttr("disabled");
                    $('input:radio[name=aDec]:nth(1)').attr('checked', true);
                    $('input:radio[name=aDec]:nth(0)').attr("disabled", true);
                }
                if ($("input:radio[name=aSep]:checked").attr('id') != 'aSepc' || $("input:radio[name=aSep]:checked").attr('id') != 'aSepp') {
                    $('input:radio[name=aDec]:nth(0)').removeAttr("disabled");
                    $('input:radio[name=aDec]:nth(1)').removeAttr("disabled");
                }
                aSep = $("input:radio[name=aSep]:checked").val();
                dGroup = $("input:radio[name=dGroup]:checked").val();
                aDec = $("input:radio[name=aDec]:checked").val();

                if ($("input:radio[name=altDec]:checked").attr('id') == 'altDecd') {
                    $('#altDecb').val('');
                    $('#altDecb').attr("disabled", true);
                }
                if ($("input:radio[name=altDec]:checked").attr('id') == 'altDeca') {
                    $('#altDecb').removeAttr("disabled");
                    altDec = $('#altDecb').val();
                }

                if ($("input:radio[name=aSign]:checked").attr('id') == 'aSignd') {
                    $('#aSignb').val('');
                    $('#aSignb').attr("disabled", true);
                }
                if ($("input:radio[name=aSign]:checked").attr('id') == 'aSigna') {
                    $('#aSignb').removeAttr("disabled");
                    aSign = $('#aSignb').val();
                }

                pSign = $("input:radio[name=pSign]:checked").val();
                if ($("input:radio[name=vMin]:checked").attr('id') == 'vMind') {
                    $('#vMinb').val('');
                    $('#vMinb').attr("disabled", true);
                }
                if ($("input:radio[name=vMin]:checked").attr('id') == 'vMina') {
                    $('#vMinb').removeAttr("disabled");
                    vMin = $('#vMinb').val();
                }
                if ($("input:radio[name=vMax]:checked").attr('id') == 'vMaxd') {
                    $('#vMaxb').val('');
                    $('#vMaxb').attr("disabled", true);
                }
                if ($("input:radio[name=vMax]:checked").attr('id') == 'vMaxa') {
                    $('#vMaxb').removeAttr("disabled");
                    vMax = $('#vMaxb').val();
                }
                if ($("input:radio[name=mDec]:checked").attr('id') == 'mDecd') {
                    $('#mDecbb').val('');
                    $('#mDecbb').attr("disabled", true);
                }
                if ($("input:radio[name=mDec]:checked").attr('id') == 'mDeca') {
                    $('#mDecbb').removeAttr("disabled");
                    mDec = $('#mDecbb').val();
                }
                mRound = $("input:radio[name=mRound]:checked").val();
                aPad = $("input:radio[name=aPad]:checked").val();
                wEmpty = $("input:radio[name=wEmpty]:checked").val();
                if (aSep != '') {
                    metaCode = aSep;
                }
                if (dGroup != '') {
                    if (metaCode != '') {
                        metaCode = metaCode + ", " + dGroup;
                    }
                    else {
                        metaCode = dGroup;
                    }
                }
                if (aDec != '') {
                    if (metaCode != '') {
                        metaCode = metaCode + ", " + aDec;
                    }
                    else {
                        metaCode = aDec;
                    }
                }
                if (altDec != '') {
                    if (metaCode != '') {
                        metaCode = metaCode + ", altDec: '" + altDec + "'";
                    }
                    else {
                        metaCode = "altDec: '" + altDec + "'";
                    }
                }
                if (aSign != '') {
                    if (metaCode != '') {
                        metaCode = metaCode + ", aSign: '" + aSign + "'";
                    }
                    else {
                        metaCode = "aSign: '" + aSign + "'";
                    }
                }
                if (pSign != '') {
                    if (metaCode != '') {
                        metaCode = metaCode + ", " + pSign;
                    }
                    else {
                        metaCode = pSign;
                    }
                }
                if (vMin != '') {
                    if (metaCode != '') {
                        metaCode = metaCode + ", vMin: '" + vMin + "'";
                    }
                    else {
                        metaCode = "vMin: '" + vMin + "'";
                    }
                }
                if (vMax != '') {
                    if (metaCode != '') {
                        metaCode = metaCode + ", vMax: '" + vMax + "'";
                    }
                    else {
                        metaCode = "vMax: '" + vMax + "'";
                    }
                }
                if (mDec != '') {
                    if (metaCode != '') {
                        metaCode = metaCode + ", mDec: '" + $('#mDecbb').val() + "'";
                    }
                    else {
                        metaCode = "mDec: '" + $('#mDecbb').val() + "'";
                    }
                }

                if (mRound != '') {
                    if (metaCode != '') {
                        metaCode = metaCode + ", " + mRound;
                    }
                    else {
                        metaCode = mRound;
                    }
                }
                if (aPad != '') {
                    if (metaCode != '') {
                        metaCode = metaCode + ", " + aPad;
                    }
                    else {
                        metaCode = aPad;
                    }
                }
                if (wEmpty != '') {
                    if (metaCode != '') {
                        metaCode = metaCode + ", " + wEmpty;
                    }
                    else {
                        metaCode = wEmpty;
                    }
                }
                $('#metaCode').text('');
                if (metaCode != '') {
                    $('#metaCode').text('meta="{' + metaCode + '}"');
                }
            });

            /* clears the metadata code  */
            $('#rd').click(function () {
                $('#metaCode').text('');
            });
            /* ends scripts for metadata code generator  */

            /* script  for defaults demo  */
            $('#d_noMeta').blur(function () {
                var convertInput = '';
                convertInput = $(this).autoNumericGet();
                $('#d_Get').val(convertInput);
                $('#d_Set').autoNumericSet(convertInput);
            });
            /* end script  for defaults demo  */

            /* script  for various samples demo  */
            $('input[name$="sample"]').blur(function () {
                var convertInput = '';
                var row = 'row_' + this.id.charAt(4);
                convertInput = $(this).autoNumericGet();
                $('#' + row + 'b').val(convertInput);
                $('#' + row + 'c').autoNumericSet(convertInput);
            });
            /* end script  for various samples demo  */

            /* script  for rounding methods  */
            $('#roundValue').blur(function () {
                if (this.value != '') {
                    convertInput = $('#roundValue').autoNumericGet();
                    var i = 1;
                    for (i = 1; i <= 9; i++) {
                        $('#roundMethod' + i).autoNumericSet(convertInput);
                    }
                }
            });

            $('#roundDecimal').change(function () { /* changes decimal places */
                convertInput = $('#roundValue').autoNumericGet();
                if (convertInput > 0) {
                    var i = 1;
                    for (i = 1; i <= 9; i++) {
                        $('#roundMethod' + i).autoNumericSet(convertInput);
                    }
                }
            });
            /* end script  for rounding methods  */

            /* script for dynamically loaded values  demo*/
            $.getJSON("test_JSON.php", function (data) {
                var valueFormatted = '';
                $.each(data, function (key, value) { // loops through JSON keys and returns value	
                    $('#' + key).autoNumericSet(value);
                });
            });
            /* end script for dynamically loaded values demo*/

            /* script for callback demo*/
            $.autoNumeric.get_mDec = function () { /* get_mDec function attached to autoNumeric() */
                var set_mDec = $('#get_metricUnit').val();
                if (set_mDec == ' km') {
                    set_mDec = 3;
                } else {
                    set_mDec = 0;
                }
                return set_mDec; /* set mDec decimal places */
            }

            var get_vMax = function () { /* set the maximum value allowed based on the metric unit */
                var set_vMax = $('#get_metricUnit').val();
                if (set_vMax == ' km') {
                    set_vMax = '99999.999';
                } else {
                    set_vMax = '99999999';
                }
                return set_vMax;
            }

            $('#length').autoNumeric({ vMax: get_vMax }); /* calls autoNumeric and passes function get_vMax */

            $('#get_metricUnit').change(function () {
                var set_value = $('#length').autoNumericGet();
                if (this.value == ' km') {
                    set_value = set_value / 1000;
                } else {
                    set_value = set_value * 1000;
                }
                $('#length').autoNumericSet(set_value);
            });
            /* end script for callback demo*/


            // Examination, Question 2, sph, -30 to 30, 2 decimals
            $('<%= TextBoxRefractiveCorrectionOD1.ClientID %>.auto').autoNumeric();

            // Examination, Question 2, cyl XX.XX
            $('<%= TextBoxRefractiveCorrectionOD2.ClientID %>.auto').autoNumeric();
            // Examination, Question 2, axis 0 - 180
            $('<%= TextBoxRefractiveCorrectionOD3.ClientID %>.auto').autoNumeric();

            // Pre-Operative Management, Question 8, tens 0 - 100 (no decimal)
            $('<%= TextBoxRefractiveCorrectionOS1.ClientID %>.auto').autoNumeric();
            // Pre-Operative Management, Question 8, tenths 0 - 99 (no decimal)
            $('<%= TextBoxRefractiveCorrectionOS2.ClientID %>.auto').autoNumeric();
            // Pre-Operative Management, Question 8, axis 0 - 180
            $('<%= TextBoxRefractiveCorrectionOS3.ClientID %>.auto').autoNumeric();
            // Pre-Operative Management, Question 8, tens 0 - 100 (no decimal)
            $('<%= TextBoxManifestRefractionOD1.ClientID %>.auto').autoNumeric();
            // Pre-Operative Management, Question 8, tenths 0 - 99 (no decimal)
            $('<%= TextBoxManifestRefractionOD2.ClientID %>.auto').autoNumeric();
            // Pre-Operative Management, Question 8, axis 0 - 180
            $('<%= TextBoxManifestRefractionOD3.ClientID %>.auto').autoNumeric();

            $('<%= TextBoxManifestRefractionOS1.ClientID %>.auto').autoNumeric();
            // Pre-Operative Management, Question 8, tenths 0 - 99 (no decimal)
            $('<%= TextBoxManifestRefractionOS2.ClientID %>.auto').autoNumeric();
            // Pre-Operative Management, Question 8, axis 0 - 180
            $('<%= TextBoxManifestRefractionOS3.ClientID %>.auto').autoNumeric();

            $('<%= TextBoxCycloplegicRefractionOD1.ClientID %>.auto').autoNumeric();
            // Pre-Operative Management, Question 8, tenths 0 - 99 (no decimal)
            $('<%= TextBoxCycloplegicRefractionOD2.ClientID %>.auto').autoNumeric();
            // Pre-Operative Management, Question 8, axis 0 - 180
            $('<%= TextBoxCycloplegicRefractionOD3.ClientID %>.auto').autoNumeric();

            $('<%= TextBoxCycloplegicRefractionOS1.ClientID %>.auto').autoNumeric();
            // Pre-Operative Management, Question 8, tenths 0 - 99 (no decimal)
            $('<%= TextBoxCycloplegicRefractionOS2.ClientID %>.auto').autoNumeric();
            // Pre-Operative Management, Question 8, axis 0 - 180
            $('<%= TextBoxCycloplegicRefractionOS3.ClientID %>.auto').autoNumeric();


            $('<%= TextBoxKeratometryFlatOD.ClientID %>.auto').autoNumeric();
            // Pre-Operative Management, Question 8, tenths 0 - 99 (no decimal)
            $('<%= TextBoxKeratometrySteepOD.ClientID %>.auto').autoNumeric();
            // Pre-Operative Management, Question 8, axis 0 - 180
            $('<%= TextBoxKeratometryAxisOD.ClientID %>.auto').autoNumeric();

            $('<%= TextBoxKeratometryFlatOS.ClientID %>.auto').autoNumeric();
            // Pre-Operative Management, Question 8, tenths 0 - 99 (no decimal)
            $('<%= TextBoxKeratometrySteepOS.ClientID %>.auto').autoNumeric();
            // Pre-Operative Management, Question 8, axis 0 - 180
            $('<%= TextBoxKeratometryAxisOS.ClientID %>.auto').autoNumeric();

            $('<%= TextBoxOutcomeManifestRefractionOD1.ClientID %>.auto').autoNumeric();
            $('<%= TextBoxOutcomeManifestRefractionOD2.ClientID %>.auto').autoNumeric();
            $('<%= TextBoxOutcomeManifestRefractionOD3.ClientID %>.auto').autoNumeric();
            $('<%= TextBoxOutcomeManifestRefractionOS1.ClientID %>.auto').autoNumeric();
            $('<%= TextBoxOutcomeManifestRefractionOS2.ClientID %>.auto').autoNumeric();
            $('<%= TextBoxOutcomeManifestRefractionOS3.ClientID %>.auto').autoNumeric();

        });
    </script>

    <script type="text/javascript">
        $(function () {
            $('#QE1').aToolTip({
                clickIt: true,
                tipContent: 'Ranges for sph [+/- 0.00 thru 30.00],<br />cyl [0.00 thru 20.00], axis [integer 0 thru 180]'
            });
            $('#QE2').aToolTip({
                clickIt: true,
                tipContent: 'Ranges for sph [+/- 0.00 thru 30.00],<br />cyl [0.00 thru 20.00], axis [integer 0 thru 180]'
            });
            $('#QE6').aToolTip({
                clickIt: true,
                tipContent: 'Ranges for sph [+/- 0.00 thru 30.00],<br />cyl [0.00 thru 20.00], axis [integer 0 thru 180]'
            });
            $('#QO2').aToolTip({
                clickIt: true,
                tipContent: 'Ranges for sph [+/- 0.00 thru 30.00],<br />cyl [0.00 thru 20.00], axis [integer 0 thru 180]'
            });

            $('#QE4').aToolTip({
                clickIt: true,
                tipContent: '20/800 or count fingers @ 5 ft<br />20/1000 or count fingers @ 4 ft<br />20/1600 or count fingers @ 3ft<br />20/2000 or count fingers @ 2 ft<br />20/4000 or count fingers @ 1 ft<br />20/7777 =CF<br />20/8888 =HM<br />20/9999 =LP<br />20/0000 =NLP'
            });

            $('#QE5').aToolTip({
                clickIt: true,
                tipContent: '20/800 or count fingers @ 5 ft<br />20/1000 or count fingers @ 4 ft<br />20/1600 or count fingers @ 3ft<br />20/2000 or count fingers @ 2 ft<br />20/4000 or count fingers @ 1 ft<br />20/7777 =CF<br />20/8888 =HM<br />20/9999 =LP<br />20/0000 =NLP'
            });

            $('#QE7').aToolTip({
                clickIt: true,
                tipContent: '20/800 or count fingers @ 5 ft<br />20/1000 or count fingers @ 4 ft<br />20/1600 or count fingers @ 3ft<br />20/2000 or count fingers @ 2 ft<br />20/4000 or count fingers @ 1 ft<br />20/7777 =CF<br />20/8888 =HM<br />20/9999 =LP<br />20/0000 =NLP'
            });

            $('#QO1').aToolTip({
                clickIt: true,
                tipContent: '20/800 or count fingers @ 5 ft<br />20/1000 or count fingers @ 4 ft<br />20/1600 or count fingers @ 3ft<br />20/2000 or count fingers @ 2 ft<br />20/4000 or count fingers @ 1 ft<br />20/7777 =CF<br />20/8888 =HM<br />20/9999 =LP<br />20/0000 =NLP'
            });
        });
    </script>
</asp:Content>