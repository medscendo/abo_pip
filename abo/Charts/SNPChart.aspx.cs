﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Transactions;
using NetHealthPIMModel;

public partial class abo_SNPChart : BasePage
{
    protected void Page_Load(object sender, EventArgs e)
    {
        if (!IsPostBack)
        {
            InitializePage();
        }

    }
    protected void InitializePage()
    {
        ((PIMMasterPage)Page.Master).SetTitle("Chart");

        using (NetHealthPIMEntities pim = new NetHealthPIMEntities())
        {
            int ModuleID = (int)Session[Constants.SESSION_WORKINGMODULEID];
            Module module = pim.Module.First(c => c.ModuleID == ModuleID);
            ((PIMMasterPage)Page.Master).SetHeader(module.ModuleName + " Chart Abstraction");

            ParticipantModuleCycle prev = (ParticipantModuleCycle)Session[Constants.SESSION_PREVIOUSCYCLE];
            String CycleNumber = Request.QueryString["CycleNumber"];

            int cycleID = ctx.ActiveModuleCycleID;
            if (CycleNumber == "1")
            {
                cycleID = prev.ParticipantModuleCycleID;
                LinkButtonBackToDashboard.Visible = true;
                ButtonSubmit.Visible = false;
            }

            ParticipantModuleCycle cycle = pim.ParticipantModuleCycle.First(c => c.ParticipantModuleCycleID == cycleID);
            String numberOfRecord = Request.QueryString["nr"];
            String totalRecords = Request.QueryString["t"];
            if (cycle.CycleNumber == 1)
            {
                ((PIMMasterPage)Page.Master).SetStatus(2);
                LiteralAbstractionNumber.Text = "Initial Abstraction: Chart " + numberOfRecord + " of " + totalRecords;
            }
            else
            {
                ((PIMMasterPage)Page.Master).SetStatus(3);
                LiteralAbstractionNumber.Text = "Second Abstraction: Chart " + numberOfRecord + " of " + totalRecords;
            }
            
            DropDownListMonthOfBirth.DataSource = CycleManager.getMonthList();
            DropDownListYearOfBirth.DataSource = CycleManager.getDOBYearList(0, 100);
            DropDownListMonthOfFirstExam.DataSource = CycleManager.getMonthList();
            DropDownListYearOfFirstExam.DataSource = CycleManager.getDOBYearList(0, 100);
            DataBind();
            DropDownListYearOfBirth.Items.Insert(0, new ListItem("Year", "0"));
            DropDownListYearOfFirstExam.Items.Insert(0, new ListItem("Year", "0"));
           
            string recordIdentifier = Request.QueryString["ri"];
            HiddenFieldRecordIdentifier.Value = recordIdentifier;
            LiteralRecordIdentifier.Text = recordIdentifier;

            CycleManager.UpdateChartDates(cycleID, recordIdentifier, Constants.ABO_SNP_MODULEID, false);

            var count = pim.ChartAbstractionSixthNervePalsy.Where(ps => ps.ParticipantModuleCycle.ParticipantModuleCycleID == cycleID & ps.RecordIdentifier == recordIdentifier).Count();
               if (count == 0)
               {
                   using (TransactionScope transaction = new TransactionScope())
                   {
                        //Module module = pim.Module.First(m => m.ModuleID == Constants.ABO_AMBLYOPIA_MODULEID);

                       NetHealthPIMModel.ChartRegistration chartRegistration = (from cr in pim.ChartRegistration
                                                           where cr.ParticipantModuleCycleID == cycleID
                                                           & cr.RecordIdentifier == recordIdentifier
                                                           & cr.ModuleID == 29///// NEEDS TO BE CHANGED
                                                           select cr).First<NetHealthPIMModel.ChartRegistration>();

                    DateTime initialVisit = Convert.ToDateTime(chartRegistration.InitialVisit);
                    DateTime DOB = Convert.ToDateTime(chartRegistration.DOB);

                    ChartAbstractionSixthNervePalsy abstractRecord = new ChartAbstractionSixthNervePalsy();
                    abstractRecord.ParticipantModuleCycle = cycle;
                    abstractRecord.RecordIdentifier = recordIdentifier;
                    abstractRecord.MonthOfFirstExam = initialVisit.Month;
                    abstractRecord.YearOfFirstExam = initialVisit.Year;
                    abstractRecord.MonthOfBirth = DOB.Month;
                    abstractRecord.YearOfBirth = DOB.Year;
                    DropDownListYearOfBirth.SelectedValue = abstractRecord.YearOfBirth.ToString();
                    DropDownListMonthOfBirth.SelectedValue = abstractRecord.MonthOfBirth.ToString();
                    DropDownListMonthOfFirstExam.SelectedValue = abstractRecord.MonthOfFirstExam.ToString();
                    DropDownListYearOfFirstExam.SelectedValue = abstractRecord.YearOfFirstExam.ToString();
                    abstractRecord.CreatedOn = DateTime.Now;
                    abstractRecord.LastUpdateDate = DateTime.Now;
                    pim.SaveChanges();

                    transaction.Complete();
                   }
               }
       
               else
               {
                   ChartAbstractionSixthNervePalsy abstractRecord = (from c in pim.ChartAbstractionSixthNervePalsy 
                   where c.ParticipantModuleCycle.ParticipantModuleCycleID == cycleID & c.RecordIdentifier == recordIdentifier
                   select c).First<ChartAbstractionSixthNervePalsy>();
                   if (abstractRecord.RBHistoryVisualComplaint != null) RadioButtonListRBHistoryVisualComplaint.SelectedValue = abstractRecord.RBHistoryVisualComplaint.ToString();

                   if (abstractRecord.RBExamIncomitancePresent != null) RadioButtonListRBExamIncomitance.SelectedValue = abstractRecord.RBExamIncomitancePresent.ToString();


                   if (abstractRecord.RBSymptomsAdditionalCranialNeuropathies != null) RadioButtonListRBSymptomsAdditionalCranialNeuropathies.SelectedValue = abstractRecord.RBSymptomsAdditionalCranialNeuropathies.ToString();
                   if (abstractRecord.SymptomsFacialNumbness != null) CheckBoxSymptomsFacialNumbness.Checked = ((bool)abstractRecord.SymptomsFacialNumbness);
                   if (abstractRecord.SymptomsFacialWeakness != null) CheckBoxSymptomsFacialWeakness.Checked = ((bool)abstractRecord.SymptomsFacialWeakness);
                   if (abstractRecord.SymptomsVerticalDiplopia != null) CheckBoxSymptomsVerticalDiplopia.Checked = ((bool)abstractRecord.SymptomsVerticalDiplopia);
                   DropDownListYearOfBirth.SelectedValue = abstractRecord.YearOfBirth.ToString();
                   DropDownListMonthOfBirth.SelectedValue = abstractRecord.MonthOfBirth.ToString();
                   DropDownListMonthOfFirstExam.SelectedValue = abstractRecord.MonthOfFirstExam.ToString();
                   DropDownListYearOfFirstExam.SelectedValue = abstractRecord.YearOfFirstExam.ToString();
                   if (abstractRecord.SymptomsPtosis!= null) CheckBoxSymptomsPtosis.Checked = ((bool)abstractRecord.SymptomsPtosis);
                   if (abstractRecord.SymptomsOther!= null) CheckBoxSymptomsOther.Checked = ((bool)abstractRecord.SymptomsOther);
                   if (abstractRecord.SymptomsNone != null) CheckBoxSymptomsNone.Checked = ((bool)abstractRecord.SymptomsNone);

                   if (abstractRecord.SymptomsVasculopathicRiskFactorsDiabetes != null) CheckBoxCBSymptomsVasculopathicRiskFactorsDiabetes.Checked = ((bool)abstractRecord.SymptomsVasculopathicRiskFactorsDiabetes);
                   if (abstractRecord.SymptomsVasculopathicRiskFactorsHyperlipidemia != null) CheckBoxCBSymptomsVasculopathicRiskFactorsHyperlipidemia.Checked = ((bool)abstractRecord.SymptomsVasculopathicRiskFactorsHyperlipidemia);
                   if (abstractRecord.SymptomsVasculopathicRiskFactorsHypertension != null) CheckBoxCBSymptomsVasculopathicRiskFactorsHypertension.Checked = ((bool)abstractRecord.SymptomsVasculopathicRiskFactorsHypertension);

                   if (abstractRecord.SymptomsVasculopathicRiskFactorsSmoking != null) CheckBoxCBSymptomsVasculopathicRiskFactorsSmoking.Checked = ((bool)abstractRecord.SymptomsVasculopathicRiskFactorsSmoking);

                   if (abstractRecord.SymptomsVasculopathicRiskFactorsOther != null) CheckBoxCBSymptomsVasculopathicRiskFactorsOther.Checked = ((bool)abstractRecord.SymptomsVasculopathicRiskFactorsOther);
                   if (abstractRecord.SymptomsVasculopathicRiskFactorsNone != null) CheckBoxCBSymptomsVasculopathicRiskFactorsNone.Checked = ((bool)abstractRecord.SymptomsVasculopathicRiskFactorsNone);

                   if (abstractRecord.OutcomeChangesNotedDataNotAvailabe != null) CheckBoxOutcomeChangesNotedDataNotAvailabe.Checked = ((bool)abstractRecord.OutcomeChangesNotedDataNotAvailabe);
                   
                  
                   if (abstractRecord.SymptomsGiantCellOther != null) CheckBoxSymptomsGiantCellOther.Checked = ((bool)abstractRecord.SymptomsGiantCellOther);
                   if (abstractRecord.SymptomsGiantCellNone != null) CheckBoxSymptomsGiantCellNone.Checked = ((bool)abstractRecord.SymptomsGiantCellNone);
                   DropDownListMonthOfFirstExam.SelectedValue = abstractRecord.MonthOfFirstExam.ToString();
                   DropDownListYearOfFirstExam.SelectedValue = abstractRecord.YearOfFirstExam.ToString();
                   if (abstractRecord.RBSymptomsGeneralNeurologic != null) RadioButtonListRBSymptomsGeneralNeurologic.SelectedValue = abstractRecord.RBSymptomsGeneralNeurologic.ToString();
                   if (abstractRecord.RBSymptomsGeneralNeurologic != null) RadioButtonListRBSymptomsGeneralNeurologic.SelectedValue = abstractRecord.RBSymptomsGeneralNeurologic.ToString();

                   if (abstractRecord.SymptomsGiantCellArteritis != null) RadioButtonListRBSymptomsGiantCellArteritis.SelectedValue = abstractRecord.SymptomsGiantCellArteritis.ToString();
                   if (abstractRecord.RBExamImcomitanceNoted!= null)  RadioButtonListRBExamImcomitanceNoted.SelectedValue= abstractRecord.RBExamImcomitanceNoted.ToString();
                   if (abstractRecord.RBSymptomsVasculopathicRiskFactors != null) RadioButtonListRBSymptomsVasculopathicRisk.SelectedValue = abstractRecord.RBSymptomsVasculopathicRiskFactors.ToString();

                   if (abstractRecord.SymptomsDyspnea != null) CheckboxSymptomsDyspnea.Checked = ((bool)abstractRecord.SymptomsDyspnea);
                   if (abstractRecord.SymptomsDysphagia!= null) CheckboxSymptomsDysphagia.Checked = ((bool)abstractRecord.SymptomsDysphagia);
                   if (abstractRecord.SymptomsGeneralized!= null) CheckboxSymptomsGeneralized.Checked = ((bool)abstractRecord.SymptomsGeneralized);
                   if (abstractRecord.SymptomsNeurologicalOther != null) CheckboxSymptomsOther1.Checked = ((bool)abstractRecord.SymptomsNeurologicalOther);
                  if(abstractRecord.SymptomsNeurologicalNone!=null) CheckboxSymptomsNone1.Checked = ((bool)abstractRecord.SymptomsNeurologicalNone);
                   
                   if (abstractRecord.SymptomsFever != null) CheckBoxSymptomsFever.Checked = ((bool)abstractRecord.SymptomsFever);
                   if (abstractRecord.SymptomsHeadache != null) CheckBoxSymptomsHeadache.Checked = ((bool)abstractRecord.SymptomsHeadache);
                   if (abstractRecord.SymptomsJawClaudication != null) CheckBoxSymptomsJawClaudication.Checked = ((bool)abstractRecord.SymptomsJawClaudication);
                   if (abstractRecord.SymptomsScalp != null) CheckBoxSymptomsScalp.Checked = ((bool)abstractRecord.SymptomsScalp);
                 
                   if (abstractRecord.RBHistorySystemicMalignancy != null) RadioButtonListRBHistorySystemicMalignancy.SelectedValue = abstractRecord.RBHistorySystemicMalignancy.ToString();
                   if(abstractRecord.HistoryMalignancy!=null)TextBoxHistoryMalignancy.Text=abstractRecord.HistoryMalignancy.ToString();
                   if (abstractRecord.RBExamAbnormalityVersions != null) RadioButtonListRBExamAbnormalityVersions.SelectedValue = abstractRecord.RBExamAbnormalityVersions.ToString();
                   if (abstractRecord.RBExamAbnormality != null) RadioButtonListRBExamAbnormality.SelectedValue = abstractRecord.RBExamAbnormality.ToString();
                   if (abstractRecord.RBExamDegreeLimitedVersion != null) RadioButtonListRBExamDegreeLimitedVersion.SelectedValue = abstractRecord.RBExamDegreeLimitedVersion.ToString();
                   if (abstractRecord.RBExamDegreeNoted != null) RadioButtonListRBExamDegreeNoted.SelectedValue = abstractRecord.RBExamDegreeNoted.ToString();
                   if(abstractRecord.ExamAlignment!=null) TextBoxExamAlignment.Text=abstractRecord.ExamAlignment.ToString();
                  
                   if (abstractRecord.RBExamPupillaryAbnormality != null) RadioButtonListRBExamPupillaryAbnormality.SelectedValue = abstractRecord.RBExamPupillaryAbnormality.ToString();
                   if (abstractRecord.RBExamPupillaryAbnormalityType != null) RadioButtonListRBExamPupillaryAbnormalityType.SelectedValue = abstractRecord.RBExamPupillaryAbnormalityType.ToString();
                   if (abstractRecord.RBExamCranialNerveAbnormality != null) RadioButtonListRBExamCranialNerveAbnormality.SelectedValue = abstractRecord.RBExamCranialNerveAbnormality.ToString();
                   if (abstractRecord.ExamCranialNerveAbnormality3rd != null) CheckBoxExamCranialNerveAbnormality3rd.Checked = ((bool)abstractRecord.ExamCranialNerveAbnormality3rd);
                   if (abstractRecord.ExamCranialNerveAbnormality4th!= null) CheckBoxExamCranialNerveAbnormality4th.Checked = ((bool)abstractRecord.ExamCranialNerveAbnormality4th);
                   if (abstractRecord.ExamCranialNerveAbnormality5th!= null) CheckBoxExamCranialNerveAbnormality5th.Checked = ((bool)abstractRecord.ExamCranialNerveAbnormality5th);
                   if (abstractRecord.ExamCranialNerveAbnormality6th != null) CheckBoxExamCranialNerveAbnormality6th.Checked = ((bool)abstractRecord.ExamCranialNerveAbnormality6th);
                   if (abstractRecord.ExamCranialNerveAbnormality7th != null) CheckBoxExamCranialNerveAbnormality7th.Checked = ((bool)abstractRecord.ExamCranialNerveAbnormality7th);
                   if (abstractRecord.ExamCranialNerveAbnormalityOther != null) CheckBoxExamCranialNerveAbnormalityOther.Checked = ((bool)abstractRecord.ExamCranialNerveAbnormalityOther);
                   if (abstractRecord.RBExamOpticDiscDescribed != null) RadioButtonListRBExamOpticDiscDescribed.SelectedValue = abstractRecord.RBExamOpticDiscDescribed.ToString();
                   if (abstractRecord.RBExamOpticDiscAppearance != null) RadioButtonListRBExamOpticDiscAppearance.SelectedValue = abstractRecord.RBExamOpticDiscAppearance.ToString();
                   if (abstractRecord.RBExamErythrocyteSedimentationRate != null) RadioButtonListRBExamErythrocyteSedimentationRate.SelectedValue = abstractRecord.RBExamErythrocyteSedimentationRate.ToString();
                   if (abstractRecord.RBExamNeuroimagingStudy != null) RadioButtonListRBExamNeuroimagingStudy.SelectedValue = abstractRecord.RBExamNeuroimagingStudy.ToString();

                   if (abstractRecord.ExamMultipleCranialNeuropathies != null) CheckBoxExamMultipleCranialNeuropathies.Checked = ((bool)abstractRecord.ExamMultipleCranialNeuropathies);
                   if (abstractRecord.ExamHistoryMalignancy != null) CheckBoxExamHistoryMalignancy.Checked = ((bool)abstractRecord.ExamHistoryMalignancy);
                   if (abstractRecord.ExamNeuroimagingOther != null) CheckBoxExamNeuroimagingOther.Checked = ((bool)abstractRecord.ExamNeuroimagingOther);
                   if (abstractRecord.RBManagementDiplopia != null) RadioButtonListRBManagementDiplopia.SelectedValue = abstractRecord.RBManagementDiplopia.ToString();
                   if (abstractRecord.ManagementOcclusion != null) CheckBoxManagementOcclusion.Checked = ((bool)abstractRecord.ManagementOcclusion);
                   if (abstractRecord.ManagementPrism != null) CheckBoxManagementPrism.Checked = ((bool)abstractRecord.ManagementPrism);
                   if (abstractRecord.ManagementSurgery != null) CheckBoxManagementSurgery.Checked = ((bool)abstractRecord.ManagementSurgery);
                   if (abstractRecord.ManagementOther != null) CheckBoxManagementOther.Checked = ((bool)abstractRecord.ManagementOther);
                   if (abstractRecord.RBOutcomeVasculopathicScreening != null) RadioButtonListRBOutcomeVasculopathicScreening.SelectedValue = abstractRecord.RBOutcomeVasculopathicScreening.ToString();
                   if (abstractRecord.RBOutcomeFollowUpExam != null) RadioButtonListRBOutcomeFollowUpExam.SelectedValue = abstractRecord.RBOutcomeFollowUpExam.ToString();
                   if (abstractRecord.RBOutcomeChangesOcularAlignment!= null) RadioButtonListRBOutcomeChangesOcularAlignment.SelectedValue = abstractRecord.RBOutcomeChangesOcularAlignment.ToString();
                   if (abstractRecord.OutcomeChangesNotedImprovement != null) CheckBoxOutcomeChangesNotedImprovement.Checked = ((bool)abstractRecord.OutcomeChangesNotedImprovement);
                   if (abstractRecord.OutcomeChangesNotedWorsening != null) CheckBoxOutcomeChangesNotedWorsening.Checked = ((bool)abstractRecord.OutcomeChangesNotedWorsening);
                   if (abstractRecord.RBOutcomeNeuroimaging != null) RadioButtonListRBOutcomeNeuroimaging.SelectedValue = abstractRecord.RBOutcomeNeuroimaging.ToString();
                   if (abstractRecord.RBOutcomeFinalEtiology != null) RadioButtonListRBOutcomeFinalEtiology.SelectedValue = abstractRecord.RBOutcomeFinalEtiology.ToString();
                   if(abstractRecord.OutcomeEtiology!=null)  TextBoxOutcomeEtiology.Text=abstractRecord.OutcomeEtiology.ToString();
                   if (abstractRecord.ExamIncomitanceOther != null) TextBoxExamIncomitanceOther.Text = abstractRecord.ExamIncomitanceOther.ToString();
                   if (abstractRecord.SymptomsGiantCellArteritisOtherText != null) TextBoxSymptomsGiantCellArteritisOtherText.Text= abstractRecord.SymptomsGiantCellArteritisOtherText.ToString();
                   if (abstractRecord.SymptomsVasculopathicRiskFactorsOtherText != null) TextBoxSymptomsVasculopathicRiskFactorsOtherText.Text = abstractRecord.SymptomsVasculopathicRiskFactorsOtherText.ToString();
               }
       
        
        
        
        
        
        
        
        }
    }

  public void savedata()
  {
        using (NetHealthPIMEntities pim = new NetHealthPIMEntities())
        {
            int ModuleID = (int)Session[Constants.SESSION_WORKINGMODULEID];
            int cycleID = ctx.ActiveModuleCycleID;
            ParticipantModuleCycle cycle = pim.ParticipantModuleCycle.First(c => c.ParticipantModuleCycleID == cycleID);
            string recordIdentifier = HiddenFieldRecordIdentifier.Value;
            using (TransactionScope transaction = new TransactionScope())
            {
                ChartAbstractionSixthNervePalsy abstractRecord = (from c in pim.ChartAbstractionSixthNervePalsy
                                                            where c.ParticipantModuleCycle.ParticipantModuleCycleID == cycleID & c.RecordIdentifier == recordIdentifier
                                                            select c).First<ChartAbstractionSixthNervePalsy>();

                if (DropDownListMonthOfBirth.SelectedIndex > 0 ) abstractRecord.MonthOfBirth = Convert.ToInt32(DropDownListMonthOfBirth.SelectedValue);
                else abstractRecord.MonthOfBirth = 0;
                if (DropDownListYearOfBirth.SelectedIndex > 0) abstractRecord.YearOfBirth = Convert.ToInt32(DropDownListYearOfBirth.SelectedValue);
                else abstractRecord.YearOfBirth = 0;
                if (DropDownListMonthOfFirstExam.SelectedIndex > 0) abstractRecord.MonthOfFirstExam = Convert.ToInt32(DropDownListMonthOfFirstExam.SelectedValue);
                else abstractRecord.MonthOfFirstExam = 0;
                if (DropDownListYearOfFirstExam.SelectedIndex >0 ) abstractRecord.YearOfFirstExam = Convert.ToInt32(DropDownListYearOfFirstExam.SelectedValue);
                else abstractRecord.YearOfFirstExam = 0;
                if (RadioButtonListRBHistoryVisualComplaint.SelectedIndex != -1) abstractRecord.RBHistoryVisualComplaint = Convert.ToInt32(RadioButtonListRBHistoryVisualComplaint.SelectedValue);
                else abstractRecord.RBHistoryVisualComplaint = 0;


                if (RadioButtonListRBSymptomsAdditionalCranialNeuropathies.SelectedIndex != -1) abstractRecord.RBSymptomsAdditionalCranialNeuropathies = Convert.ToInt32(RadioButtonListRBSymptomsAdditionalCranialNeuropathies.SelectedValue);
                else abstractRecord.RBSymptomsAdditionalCranialNeuropathies = 0;






                if (RadioButtonListRBSymptomsGiantCellArteritis.SelectedIndex != -1) abstractRecord.SymptomsGiantCellArteritis = Convert.ToInt32(RadioButtonListRBSymptomsGiantCellArteritis.SelectedValue);
                else abstractRecord.SymptomsGiantCellArteritis = 0;
                if (RadioButtonListRBExamImcomitanceNoted.SelectedIndex != -1) abstractRecord.RBExamImcomitanceNoted = Convert.ToInt32(RadioButtonListRBExamImcomitanceNoted.SelectedValue);
                else abstractRecord.RBExamImcomitanceNoted = 0;

                if (RadioButtonListRBSymptomsVasculopathicRisk.SelectedIndex != -1) abstractRecord.RBSymptomsVasculopathicRiskFactors = Convert.ToInt32(RadioButtonListRBSymptomsVasculopathicRisk.SelectedValue);
                else abstractRecord.RBSymptomsVasculopathicRiskFactors = 0;

                if (RadioButtonListRBExamIncomitance.SelectedIndex != -1) abstractRecord.RBExamIncomitancePresent = Convert.ToInt32(RadioButtonListRBExamIncomitance.SelectedValue);
                else abstractRecord.RBExamIncomitancePresent = 0;
                
                abstractRecord.SymptomsFacialNumbness=CheckBoxSymptomsFacialNumbness.Checked;
                abstractRecord.OutcomeChangesNotedDataNotAvailabe = CheckBoxOutcomeChangesNotedDataNotAvailabe.Checked;
                abstractRecord.SymptomsFacialWeakness = CheckBoxSymptomsFacialWeakness.Checked;
                abstractRecord.SymptomsVerticalDiplopia=CheckBoxSymptomsVerticalDiplopia.Checked;
                abstractRecord.SymptomsPtosis=CheckBoxSymptomsPtosis.Checked;
                abstractRecord.SymptomsOther=CheckBoxSymptomsOther.Checked;
                abstractRecord.SymptomsNone=CheckBoxSymptomsNone.Checked;
                if (RadioButtonListRBSymptomsGeneralNeurologic.SelectedIndex != -1) abstractRecord.RBSymptomsGeneralNeurologic = Convert.ToInt32(RadioButtonListRBSymptomsGeneralNeurologic.SelectedValue);
                else abstractRecord.RBSymptomsGeneralNeurologic = 0;
                abstractRecord.SymptomsDyspnea= CheckboxSymptomsDyspnea.Checked;
                abstractRecord.SymptomsDysphagia= CheckboxSymptomsDysphagia.Checked;
                abstractRecord.SymptomsGeneralized =CheckboxSymptomsGeneralized.Checked;
                abstractRecord.SymptomsNeurologicalOther = CheckboxSymptomsOther1.Checked;
                abstractRecord.SymptomsNeurologicalNone = CheckboxSymptomsNone1.Checked;
                abstractRecord.SymptomsGiantCellOther = CheckBoxSymptomsGiantCellOther.Checked;
                abstractRecord.SymptomsVasculopathicRiskFactorsDiabetes = CheckBoxCBSymptomsVasculopathicRiskFactorsDiabetes.Checked;
                abstractRecord.SymptomsVasculopathicRiskFactorsHyperlipidemia = CheckBoxCBSymptomsVasculopathicRiskFactorsHyperlipidemia.Checked;
                abstractRecord.SymptomsVasculopathicRiskFactorsHypertension = CheckBoxCBSymptomsVasculopathicRiskFactorsHypertension.Checked;
                abstractRecord.SymptomsGiantCellNone = CheckBoxSymptomsGiantCellNone.Checked; 
                abstractRecord.SymptomsVasculopathicRiskFactorsSmoking = CheckBoxCBSymptomsVasculopathicRiskFactorsSmoking.Checked;

                abstractRecord.SymptomsVasculopathicRiskFactorsOther = CheckBoxCBSymptomsVasculopathicRiskFactorsOther.Checked;
                abstractRecord.SymptomsVasculopathicRiskFactorsNone = CheckBoxCBSymptomsVasculopathicRiskFactorsNone.Checked;

                abstractRecord.SymptomsFever=CheckBoxSymptomsFever.Checked;
                abstractRecord.SymptomsHeadache=CheckBoxSymptomsHeadache.Checked;
                abstractRecord.SymptomsJawClaudication = CheckBoxSymptomsJawClaudication.Checked;
                abstractRecord.SymptomsScalp = CheckBoxSymptomsScalp.Checked;
              
                if (RadioButtonListRBHistorySystemicMalignancy.SelectedIndex != -1) abstractRecord.RBHistorySystemicMalignancy = Convert.ToInt32(RadioButtonListRBHistorySystemicMalignancy.SelectedValue);
                else abstractRecord.RBHistorySystemicMalignancy = 0;
                try
                {
                    if (TextBoxHistoryMalignancy.Text!=null)
                        abstractRecord.HistoryMalignancy = TextBoxHistoryMalignancy.Text;
                }
                catch (Exception ex)
                {
                    abstractRecord.HistoryMalignancy = null;
                    TextBoxHistoryMalignancy.Text = "";
                }


                try
                {
                    if (TextBoxSymptomsVasculopathicRiskFactorsOtherText.Text != null)
                        abstractRecord.SymptomsVasculopathicRiskFactorsOtherText = TextBoxSymptomsVasculopathicRiskFactorsOtherText.Text;
                }
                catch (Exception ex)
                {
                    abstractRecord.SymptomsVasculopathicRiskFactorsOtherText = null;
                    TextBoxSymptomsVasculopathicRiskFactorsOtherText.Text = "";
                }

                try
                {
                    if (TextBoxExamIncomitanceOther.Text != null)
                        abstractRecord.ExamIncomitanceOther = TextBoxExamIncomitanceOther.Text;
                }
                catch (Exception ex)
                {
                    abstractRecord.ExamIncomitanceOther = null;
                    TextBoxExamIncomitanceOther.Text = "";
                }

                try
                {
                    if (TextBoxSymptomsGiantCellArteritisOtherText.Text != null)
                        abstractRecord.SymptomsGiantCellArteritisOtherText = TextBoxSymptomsGiantCellArteritisOtherText.Text;
                }
                catch (Exception ex)
                {
                    abstractRecord.SymptomsGiantCellArteritisOtherText = null;
                    TextBoxSymptomsGiantCellArteritisOtherText.Text = "";
                }





                if (RadioButtonListRBExamAbnormalityVersions.SelectedIndex != -1) abstractRecord.RBExamAbnormalityVersions = Convert.ToInt32(RadioButtonListRBExamAbnormalityVersions.SelectedValue);
                else abstractRecord.RBExamAbnormalityVersions = 0;
                if (RadioButtonListRBExamAbnormality.SelectedIndex != -1) abstractRecord.RBExamAbnormality = Convert.ToInt32(RadioButtonListRBExamAbnormality.SelectedValue);
                else abstractRecord.RBExamAbnormality = 0;
                if (RadioButtonListRBExamDegreeLimitedVersion.SelectedIndex != -1) abstractRecord.RBExamDegreeLimitedVersion = Convert.ToInt32(RadioButtonListRBExamDegreeLimitedVersion.SelectedValue);
                else abstractRecord.RBExamDegreeLimitedVersion = 0;
                if (RadioButtonListRBExamDegreeNoted.SelectedIndex != -1) abstractRecord.RBExamDegreeNoted = Convert.ToInt32(RadioButtonListRBExamDegreeNoted.SelectedValue);
                else abstractRecord.RBExamDegreeNoted = 0;
                try
                {
                    if (TextBoxExamAlignment.Text!=null)
                        abstractRecord.ExamAlignment = Convert.ToInt32(TextBoxExamAlignment.Text);
                }
                catch (Exception ex)
                {
                    abstractRecord.ExamAlignment = null;
                    TextBoxExamAlignment.Text = "";
                }
               
                if (RadioButtonListRBExamPupillaryAbnormality.SelectedIndex != -1) abstractRecord.RBExamPupillaryAbnormality = Convert.ToInt32(RadioButtonListRBExamPupillaryAbnormality.SelectedValue);
                else abstractRecord.RBExamPupillaryAbnormality = 0;
                if (RadioButtonListRBExamPupillaryAbnormalityType.SelectedIndex != -1) abstractRecord.RBExamPupillaryAbnormalityType = Convert.ToInt32(RadioButtonListRBExamPupillaryAbnormalityType.SelectedValue);
                else abstractRecord.RBExamPupillaryAbnormalityType = 0;
                if (RadioButtonListRBExamCranialNerveAbnormality.SelectedIndex != -1) abstractRecord.RBExamCranialNerveAbnormality = Convert.ToInt32(RadioButtonListRBExamCranialNerveAbnormality.SelectedValue);
                else abstractRecord.RBExamCranialNerveAbnormality = 0;
                abstractRecord.ExamCranialNerveAbnormality3rd = CheckBoxExamCranialNerveAbnormality3rd.Checked;
                abstractRecord.ExamCranialNerveAbnormality4th = CheckBoxExamCranialNerveAbnormality4th.Checked;
                abstractRecord.ExamCranialNerveAbnormality5th = CheckBoxExamCranialNerveAbnormality5th.Checked;
                abstractRecord.ExamCranialNerveAbnormality6th = CheckBoxExamCranialNerveAbnormality6th.Checked;
                abstractRecord.ExamCranialNerveAbnormality7th = CheckBoxExamCranialNerveAbnormality7th.Checked;
                abstractRecord.ExamCranialNerveAbnormalityOther = CheckBoxExamCranialNerveAbnormalityOther.Checked;
                if (RadioButtonListRBExamOpticDiscDescribed.SelectedIndex != -1) abstractRecord.RBExamOpticDiscDescribed = Convert.ToInt32(RadioButtonListRBExamOpticDiscDescribed.SelectedValue);
                else abstractRecord.RBExamOpticDiscDescribed = 0;
                if (RadioButtonListRBExamOpticDiscAppearance.SelectedIndex != -1) abstractRecord.RBExamOpticDiscAppearance = Convert.ToInt32(RadioButtonListRBExamOpticDiscAppearance.SelectedValue);
                else abstractRecord.RBExamOpticDiscAppearance = 0;
                if (RadioButtonListRBExamErythrocyteSedimentationRate.SelectedIndex != -1) abstractRecord.RBExamErythrocyteSedimentationRate = Convert.ToInt32(RadioButtonListRBExamErythrocyteSedimentationRate.SelectedValue);
                else abstractRecord.RBExamErythrocyteSedimentationRate = 0;
                if (RadioButtonListRBExamNeuroimagingStudy.SelectedIndex != -1) abstractRecord.RBExamNeuroimagingStudy = Convert.ToInt32(RadioButtonListRBExamNeuroimagingStudy.SelectedValue);
                else abstractRecord.RBExamNeuroimagingStudy = 0;
                abstractRecord.ExamMultipleCranialNeuropathies = CheckBoxExamMultipleCranialNeuropathies.Checked;
                abstractRecord.ExamHistoryMalignancy = CheckBoxExamHistoryMalignancy.Checked;
                abstractRecord.ExamNeuroimagingOther = CheckBoxExamNeuroimagingOther.Checked;
                if (RadioButtonListRBManagementDiplopia.SelectedIndex != -1) abstractRecord.RBManagementDiplopia = Convert.ToInt32(RadioButtonListRBManagementDiplopia.SelectedValue);
                else abstractRecord.RBManagementDiplopia = 0;
                abstractRecord.ManagementOcclusion = CheckBoxManagementOcclusion.Checked;
                abstractRecord.ManagementPrism = CheckBoxManagementPrism.Checked;
                abstractRecord.ManagementSurgery = CheckBoxManagementSurgery.Checked;
                abstractRecord.ManagementOther = CheckBoxManagementOther.Checked;
                if (RadioButtonListRBOutcomeVasculopathicScreening.SelectedIndex != -1) abstractRecord.RBOutcomeVasculopathicScreening = Convert.ToInt32(RadioButtonListRBOutcomeVasculopathicScreening.SelectedValue);
                else abstractRecord.RBOutcomeVasculopathicScreening = 0;
                if (RadioButtonListRBOutcomeFollowUpExam.SelectedIndex != -1) abstractRecord.RBOutcomeFollowUpExam = Convert.ToInt32(RadioButtonListRBOutcomeFollowUpExam.SelectedValue);
                else abstractRecord.RBOutcomeFollowUpExam = 0;
                if (RadioButtonListRBOutcomeChangesOcularAlignment.SelectedIndex != -1) abstractRecord.RBOutcomeChangesOcularAlignment = Convert.ToInt32(RadioButtonListRBOutcomeChangesOcularAlignment.SelectedValue);
                else abstractRecord.RBOutcomeChangesOcularAlignment = 0;
                abstractRecord.OutcomeChangesNotedImprovement= CheckBoxOutcomeChangesNotedImprovement.Checked;
                abstractRecord.OutcomeChangesNotedWorsening = CheckBoxOutcomeChangesNotedWorsening.Checked;
                if (RadioButtonListRBOutcomeNeuroimaging.SelectedIndex != -1) abstractRecord.RBOutcomeNeuroimaging = Convert.ToInt32(RadioButtonListRBOutcomeNeuroimaging.SelectedValue);
                else abstractRecord.RBOutcomeNeuroimaging = 0;
                if (RadioButtonListRBOutcomeFinalEtiology.SelectedIndex != -1) abstractRecord.RBOutcomeFinalEtiology = Convert.ToInt32(RadioButtonListRBOutcomeFinalEtiology.SelectedValue);
                else abstractRecord.RBOutcomeFinalEtiology = 0;
                try
                    {
                        if (TextBoxOutcomeEtiology != null)
                            abstractRecord.OutcomeEtiology = TextBoxOutcomeEtiology.Text;
                    }
                    catch (Exception ex)
                    {
                        abstractRecord.OutcomeEtiology = null;
                        TextBoxOutcomeEtiology.Text = "";
                    }

                abstractRecord.LastUpdateDate = DateTime.Now;
                bool ChartCompleted = isComplete();
                abstractRecord.Complete = ChartCompleted;
                var chartRegistration = (from cr in pim.ChartRegistration
                                         where cr.ParticipantModuleCycleID == cycle.ParticipantModuleCycleID
                                          & cr.ModuleID == ModuleID
                                          & cr.RecordIdentifier == recordIdentifier
                                         select cr).First<NetHealthPIMModel.ChartRegistration>();
                chartRegistration.AbstractCompleted = ChartCompleted;


                if ((DropDownListYearOfBirth.SelectedIndex > 0) && (DropDownListYearOfBirth.SelectedIndex > 0))
                {
                    chartRegistration.DOB = abstractRecord.MonthOfBirth + "/" + abstractRecord.YearOfBirth;
                }
                if ((DropDownListMonthOfFirstExam.SelectedIndex > 0) && (DropDownListYearOfFirstExam.SelectedIndex > 0))
                {
                    chartRegistration.InitialVisit = abstractRecord.MonthOfFirstExam + "/" + abstractRecord.YearOfFirstExam;
                }
                pim.SaveChanges();

                transaction.Complete();




            }

            CycleManager.UpdateChartDates(cycleID, recordIdentifier, Constants.ABO_SNP_MODULEID, true);
            CycleManager.UpdateParticipantCompletedModules(cycleID, ModuleID);
        }


  }
    protected void ButtonSubmit_Click(object sender, EventArgs e)
    {
        savedata();
        isComplete();
        if (!isComplete())
        {
            isComplete();
            string script = " var answer = confirm('Chart data is not complete.\\nClick OK to save entries and exit chart.\\nClick CANCEL to save entries and review the chart. '); if (answer==true) window.location = '../PatientChartRegistration.aspx';";
            ScriptManager.RegisterStartupScript(this, GetType(),
                          "ServerControlScript", script, true);
        }
        else
        {

            Response.Redirect("../PatientChartRegistration.aspx");
        }


    }
    protected bool isComplete()
    {
        bool ChartCompleted = true;




        LabelMonthOfBirth.Visible = false;

        LabelYearOfBirth.Visible = false;

        LabelMonthOfFirstExam.Visible = false;

        LabelYearOfFirstExam.Visible = false;





        LabelRBSymptomsAdditionalCranialNeuropathies.Visible = false;





        LabelRBSymptomsGeneralNeurologic.Visible = false;

        LabelRBSymptomsVasculopathicRisk.Visible = false;

        LabelRBHistorySystemicMalignancy.Visible = false;

        LabelHistoryMalignancy.Visible = false;

        LabelRBExamAbnormalityVersions.Visible = false;

        LabelRBExamAbnormality.Visible = false;

        LabelRBExamDegreeLimitedVersion.Visible = false;

        LabelRBExamDegreeNoted.Visible = false;


        LabelExamAlignment.Visible = false;


        LabelRBExamIncomitance.Visible = false;


        LabelRBExamPupillaryAbnormality.Visible = false;

        Label1RBExamPupillaryAbnormalityType.Visible = false;

        LabelExamCranialNerveAbnormality.Visible = false;
        Label1RBExamCranialNerveAbnormality.Visible = false;
        

        LabelRBExamOpticDiscDescribed.Visible = false;

        LabelRBExamOpticDiscAppearance.Visible = false;


        LabelRBExamNeuroimagingStudy.Visible = false;

      
            LabelRBSymptomsGiantCellArteritis.Visible = false;
        
      
          
            LabelRBExamImcomitanceNoted.Visible = false;





        LabelRBManagementDiplopia.Visible = false;

        LabelRBOutcomeVasculopathicScreening.Visible = false;

        LabelRBOutcomeFollowUpExam.Visible = false;

        LabelRBOutcomeChangesOcularAlignment.Visible = false;


        LabelRBOutcomeNeuroimaging.Visible = false;

        LabelRBOutcomeFinalEtiology.Visible = false;

        LabelOutcomeEtiology.Visible = false;

        Labelinitialage.Visible = false;

        NetHealthPIMEntities pim = new NetHealthPIMEntities();
        var moduleinfo = (from c in pim.Module where c.ModuleID == 29 select c).FirstOrDefault();
        string message = "Patient must be  at least " + moduleinfo.RegistrationMinAge + " years old<br />";
        string registrationminage = Validation.validateRegistrationMinAge(Convert.ToInt32(DropDownListYearOfFirstExam.SelectedValue), Convert.ToInt32(DropDownListYearOfBirth.SelectedValue), Convert.ToInt32(moduleinfo.RegistrationMinAge), message);

        if (registrationminage != "")
        {
            Labelinitialage.Text = message;
            Labelinitialage.Visible = true;
            ChartCompleted = false;
        }
     



        if (DropDownListMonthOfBirth.SelectedIndex == 0)
        {   LabelMonthOfBirth.Visible=true;
            ChartCompleted = false;
        }
        if (DropDownListYearOfBirth.SelectedIndex == 0) 
            {
            ChartCompleted=false;
            LabelYearOfBirth.Visible=true;
        }
    
      if (DropDownListMonthOfFirstExam.SelectedIndex == 0) 
                {
            ChartCompleted=false;
           LabelMonthOfFirstExam.Visible=true;
        }
          
          
        if (DropDownListYearOfFirstExam.SelectedIndex == 0) 
            
                   {
            ChartCompleted=false;
           LabelYearOfFirstExam.Visible=true;
        }


        if (RadioButtonListRBSymptomsGiantCellArteritis.SelectedIndex == -1)
        {

            ChartCompleted = false;
            LabelRBSymptomsGiantCellArteritis.Visible = true;
        
        }
        if (RadioButtonListRBExamImcomitanceNoted.SelectedIndex == -1)
        {
            ChartCompleted = false;
            LabelRBExamImcomitanceNoted.Visible = true;
        
        
        }
          if (RadioButtonListRBHistoryVisualComplaint.SelectedIndex == -1)
          {
              ChartCompleted = false;
              LabelRBHistoryVisualComplaint.Visible = true;
          }


        if (RadioButtonListRBSymptomsAdditionalCranialNeuropathies.SelectedIndex == -1)
        {
            LabelRBSymptomsAdditionalCranialNeuropathies.Visible = true;
            ChartCompleted = false;


        
        
        }
     

        if (RadioButtonListRBSymptomsGeneralNeurologic.SelectedIndex == -1)
        {
            LabelRBSymptomsGeneralNeurologic.Visible = true;
            ChartCompleted = false;




        }
    
        

        ///////////////////
        if (RadioButtonListRBSymptomsVasculopathicRisk.SelectedIndex == -1)
        {
            LabelRBSymptomsVasculopathicRisk.Visible = true;
            ChartCompleted = false;




        }
       
        //////////////
        if (RadioButtonListRBHistorySystemicMalignancy.SelectedIndex == -1)
        {
            LabelRBHistorySystemicMalignancy.Visible = true;
            ChartCompleted = false;




        }
        if (RadioButtonListRBHistorySystemicMalignancy.SelectedValue == "1" && TextBoxHistoryMalignancy.Text=="")
        {
            LabelHistoryMalignancy.Visible = true;
            ChartCompleted = false;




        }
        /////////////////
        if (RadioButtonListRBExamAbnormalityVersions.SelectedIndex == -1)
        {
            LabelRBExamAbnormalityVersions.Visible = true;
            ChartCompleted = false;




        }
        if (RadioButtonListRBExamAbnormalityVersions.SelectedValue == "1" && RadioButtonListRBExamAbnormality.SelectedIndex == -1)
        {
            LabelRBExamAbnormality.Visible = true;
            ChartCompleted = false;




        }
        if (RadioButtonListRBExamDegreeLimitedVersion.SelectedIndex == -1)
        {
            LabelRBExamDegreeLimitedVersion.Visible = true;
            ChartCompleted = false;
        
        
        }
        if (RadioButtonListRBExamDegreeLimitedVersion.SelectedValue == "1" && RadioButtonListRBExamDegreeNoted.SelectedIndex == -1)
        {
            LabelRBExamDegreeNoted.Visible = true;
            ChartCompleted = false;
        
        
        }
        if (TextBoxExamAlignment.Text == "")
        {
            LabelExamAlignment.Visible = true;
            ChartCompleted = false;
        
        
        }
      
        if (RadioButtonListRBExamPupillaryAbnormality.SelectedIndex == -1)
        {
            LabelRBExamPupillaryAbnormality.Visible = true;
            ChartCompleted = false;
        }
        if ((RadioButtonListRBExamPupillaryAbnormality.SelectedValue == "0") && (RadioButtonListRBExamPupillaryAbnormalityType.SelectedIndex == -1))
        {
            Label1RBExamPupillaryAbnormalityType.Visible = true;
             ChartCompleted = false;


         }

        if (RadioButtonListRBExamCranialNerveAbnormality.SelectedIndex == -1)
        {
            Label1RBExamCranialNerveAbnormality.Visible = true;
            ChartCompleted = false;
        }
     

        if (RadioButtonListRBExamOpticDiscDescribed.SelectedIndex == -1)
        {
            LabelRBExamOpticDiscDescribed.Visible = true;
        ChartCompleted = false;
        
        }
        if ((RadioButtonListRBExamOpticDiscDescribed.SelectedValue == "1") && (RadioButtonListRBExamOpticDiscAppearance.SelectedIndex == -1))
        {
            LabelRBExamOpticDiscAppearance.Visible = true;
            ChartCompleted = false;
        
        }
        if (RadioButtonListRBExamErythrocyteSedimentationRate.SelectedIndex == -1)
        {


            LabelRBExamErythrocyteSedimentationRate.Visible = true;
            ChartCompleted = false;
        }



        if( RadioButtonListRBExamNeuroimagingStudy.SelectedIndex == -1)

        {
            LabelRBExamNeuroimagingStudy.Visible = true;
            ChartCompleted = false;
        
        }
      
        if (RadioButtonListRBManagementDiplopia.SelectedIndex == -1)
        {
            LabelRBManagementDiplopia.Visible = true;
            ChartCompleted = false;
        }
       
        if (RadioButtonListRBOutcomeVasculopathicScreening.SelectedIndex == -1)
        {
            LabelRBOutcomeVasculopathicScreening.Visible = true;
            ChartCompleted = false;
        }
        if (RadioButtonListRBOutcomeFollowUpExam.SelectedIndex == -1)
        {

            LabelRBOutcomeFollowUpExam.Visible = true;
            ChartCompleted = false;
        }
        if ((RadioButtonListRBOutcomeFollowUpExam.SelectedValue == "1") && (RadioButtonListRBOutcomeChangesOcularAlignment.SelectedIndex == -1))
        {
            LabelRBOutcomeChangesOcularAlignment.Visible = true;
            ChartCompleted = false;
        }
      
        if ((RadioButtonListRBOutcomeFollowUpExam.SelectedValue == "2") && (RadioButtonListRBOutcomeNeuroimaging.SelectedIndex == -1))
        {
            LabelRBOutcomeNeuroimaging.Visible = true;
            ChartCompleted = false;
        
        }
        if (RadioButtonListRBOutcomeFinalEtiology.SelectedIndex == -1)
        {
            LabelRBOutcomeFinalEtiology.Visible = true;
            ChartCompleted = false;
        }
        if ((RadioButtonListRBOutcomeFinalEtiology.SelectedValue == "1") && (TextBoxOutcomeEtiology.Text == ""))
        {
            LabelOutcomeEtiology.Visible = true;
            ChartCompleted = false;
        
        }

        return ChartCompleted;

    }





}
