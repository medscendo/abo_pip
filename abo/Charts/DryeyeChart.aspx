<%@ Page Title="" Language="C#" MasterPageFile="~/PIMMasterPage.master" AutoEventWireup="true" CodeFile="DryeyeChart.aspx.cs" Inherits="abo_DryeyeChart" %>

<asp:Content ID="Content1" ContentPlaceHolderID="head" Runat="Server">

  <link type="text/css" href="../../common/css/atooltip.css" rel="stylesheet"  media="screen" />

	  <style type="text/css">
		.bginputa{
			float:right;
			margin:21px 0 0 1px;
			background: url(../common/images/orange_button_with_arrow.png) no-repeat;
			overflow:hidden;
			height:35px;

		}
		.bginputa a{
			color: white;
			text-align:center;
			height:35px;
			line-height:28px;
			padding:0 14px 15px  ;
			cursor:pointer;
			float:left;
			border:none;
			text-decoration:none;
			font-family:Arial, Helvetica, sans-serif; 
			font-size:12px; 
			font-weight:bold; 
			letter-spacing: 0px;
		}
		.bginputa a:hover{
			text-decoration:none;
		}
		.right {
			float:right;
			text-align:right;
		}
		
		.midCol {
			text-align:right;
		}
		
		.thirds {
			padding:0 !important;
		}
		
		.thirds table {
			width:100%;
			margin:-1px;
		}
		
		.thirds td {
			width:50%;
			font-size:0.8em;
			text-align:center;
			vertical-align:middle;
			padding: 10px 5px !important;
		}
		.thirds .rightCol {
			border-right:none !important;
		}
	</style>

</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder1" Runat="Server">
    <asp:HiddenField ID="HiddenFieldRecordIdentifier" runat="server"/>
        <!-- Esotropia pim -->
        <div class="pim">
            <div class="record-ident clearfix">
                <h3 class="record-first">RECORD IDENTIFIER: <asp:Literal runat="server" ID="LiteralRecordIdentifier"></asp:Literal></h3>
                <h3 class="record-second"><asp:Literal runat="server" ID="LiteralAbstractionNumber"></asp:Literal></h3>
            </div>
            <!-- History Section -->
            <table>
                <!-- Header - History -->
                <tr>
                    <th colspan="2" width="910px"><p>History</p></th>
                </tr>
                <!-- Question 1 -->
                <tr class="table_body">
                    <td style="width:70%;">1. Gender</td>
                    <td>
						<asp:RadioButtonList ID="RadioButtonListRBGender" RepeatDirection="Vertical" CssClass="aspxList" runat="server">
							<asp:ListItem Value="42">Male</asp:ListItem>
							<asp:ListItem Value="43">Female</asp:ListItem>
						</asp:RadioButtonList>
					</td>
                </tr>
                <!-- Question 2 -->
                <tr class="table_body_bg">
                    <td>2. Date of Birth</td>
                    <td>
						<asp:DropDownList ID="DropDownListMonthOfBirth" DataValueField="MonthID" DataTextField="MonthName" runat="server" />
						<asp:DropDownList ID="DropDownListYearOfBirth" DataValueField="YearID" DataTextField="YearName" runat="server" />
					</td>
                </tr>
				<!-- Question 3 -->
                <tr class="table_body">
                    <td>3. Ocular symptoms</td>
                    <td>
						<asp:Checkbox ID="CheckBoxPresentingSymptomBurning" runat="server" GroupName="PresentingSymptom" Text="&nbsp;Burning" />
						<br /><asp:Checkbox ID="CheckBoxPresentingSymptomBlurredVision" runat="server" GroupName="PresentingSymptom" Text="&nbsp;Blurred Vision" />
						<br /><asp:Checkbox ID="CheckBoxPresentingSymptomDryness" runat="server" GroupName="PresentingSymptom" Text="&nbsp;Dryness" />
						<br /><asp:Checkbox ID="CheckBoxPresentingSymptomForeignBody" runat="server" GroupName="PresentingSymptom" Text="&nbsp;Foreign Body / Grittiness" />
						<br /><asp:Checkbox ID="CheckBoxPresentingSymptomFluctuatingVision" runat="server" GroupName="PresentingSymptom" Text="&nbsp;Fluctuating Vision" />
						<br /><asp:Checkbox ID="CheckBoxPresentingSymptomItching" runat="server" GroupName="PresentingSymptom" Text="&nbsp;Itching" />
						<br /><asp:Checkbox ID="CheckBoxPresentingSymptomPhotophobia" runat="server" GroupName="PresentingSymptom" Text="&nbsp;Photophobia" />
						<br /><asp:Checkbox ID="CheckBoxPresentingSymptomTearing" runat="server" GroupName="PresentingSymptom" Text="&nbsp;Tearing / Discharge" />
						<br /><asp:Checkbox ID="CheckBoxPresentingSymptomNotDoc" runat="server" GroupName="PresentingSymptom" Text="&nbsp;Not documented" />
					</td>
                </tr>
                <!-- Question 4 -->
                <tr class="table_body_bg">
                    <td>4. Does the patient complain of a dry mouth?</td>
                    <td>
						<asp:RadioButtonList ID="RadioButtonListPresentingDryMouth" RepeatDirection="Horizontal" CssClass="aspxList" runat="server">
							<asp:ListItem Value="1">Yes</asp:ListItem>
							<asp:ListItem Value="2">No</asp:ListItem>
							<asp:ListItem Value="3">Not Documented</asp:ListItem>
						</asp:RadioButtonList>
					</td>
                </tr>
				<!-- Question 5 -->
                <tr class="table_body">
                    <td>5. What activities / conditions exacerbate symptoms?</td>
                    <td>
						<asp:Checkbox ID="CheckBoxPresentingExacDriving" runat="server" GroupName="PresentingExac" Text="&nbsp;Driving" />
						<br /><asp:Checkbox ID="CheckBoxPresentingExacReading" runat="server" GroupName="PresentingExac" Text="&nbsp;Reading" />
						<br /><asp:Checkbox ID="CheckBoxPresentingPresentingExacComputer" runat="server" GroupName="PresentingExac" Text="&nbsp;Computer" />
						<br /><asp:Checkbox ID="CheckBoxPresentingExacTV" runat="server" GroupName="PresentingExac" Text="&nbsp;Television" />
						<br /><asp:Checkbox ID="CheckBoxPresentingExacWindSun" runat="server" GroupName="PresentingExac" Text="&nbsp;Wind/sun" />
						<br /><asp:Checkbox ID="CheckBoxPresentingExacLowHumid" runat="server" GroupName="PresentingExac" Text="&nbsp;Low humidity" />
						<br /><asp:Checkbox ID="CheckBoxPresentingExacOther" runat="server" GroupName="PresentingExac" Text="&nbsp;Other" />&nbsp;<asp:TextBox runat="server" ID="TextBoxPresentingExacOtherText" size="20" />
						<br /><asp:Checkbox ID="CheckBoxPresentingExacNotDoc" runat="server" GroupName="PresentingExac" Text="&nbsp;Not Documented" />
					</td>
                </tr>
                <!-- Question 6 -->
                <tr class="table_body_bg">
                    <td>6. Is there concomitant systemic disease? (check all that apply)</td>
                    <td>
						<asp:Checkbox ID="CheckBoxPresentingDiseaseDiabetes" runat="server" GroupName="PresentingDisease" Text="&nbsp;Diabetes" />
						<br /><asp:Checkbox ID="CheckBoxPresentingDiseaseFacialNerve" runat="server" GroupName="PresentingDisease" Text="&nbsp;Facial nerve dysfunction" />
						<br /><asp:Checkbox ID="CheckBoxPresentingDiseaseGraft" runat="server" GroupName="PresentingDisease" Text="&nbsp;Graft vs. host disease" />
						<br /><asp:Checkbox ID="CheckBoxPresentingDiseaseImmun" runat="server" GroupName="PresentingDisease" Text="&nbsp;Immunobullous disease (e.g. cicatricial pemphigoid, Stevens Johnson)" />
						<br /><asp:Checkbox ID="CheckBoxPresentingDiseaseHypothyroid" runat="server" GroupName="PresentingDisease" Text="&nbsp;Hypothyroid / Graves disease" />
						<br /><asp:Checkbox ID="CheckBoxPresentingDiseaseNeurologic" runat="server" GroupName="PresentingDisease" Text="&nbsp;Neurologic disease (e.g. Parkinson disease, dysautonomia, excluding facial nerve. palsy)" />
						<br /><asp:Checkbox ID="CheckBoxPresentingDiseaseRA" runat="server" GroupName="PresentingDisease" Text="&nbsp;Rheumatoid arthritis" />
						<br /><asp:Checkbox ID="CheckBoxPresentingDiseaseSjogren" runat="server" GroupName="PresentingDisease" Text="&nbsp;Sjogren syndrome" />
						<br /><asp:Checkbox ID="CheckBoxPresentingDiseaseInfection" runat="server" GroupName="PresentingDisease" Text="&nbsp;Systemic infection (e.g. Hepatitis C, HIV)" />
						<br /><asp:Checkbox ID="CheckBoxPresentingDiseaseOtherAID" runat="server" GroupName="PresentingDisease" Text="&nbsp;Other autoimmune disease (e.g. sarcoid, inflammatory bowel)" />
						<br /><asp:Checkbox ID="CheckBoxPresentingDiseaseSkin" runat="server" GroupName="PresentingDisease" Text="&nbsp;Skin disease (e.g. atopic dermatitis, rosacea, scleroderma)" />
						<br /><asp:Checkbox ID="CheckBoxPresentingDiseaseOther" runat="server" GroupName="PresentingDisease" Text="&nbsp;Other" />&nbsp;<asp:TextBox runat="server" ID="TextBoxPresentingDiseaseOtherText" size="20" />
						<br /><asp:Checkbox ID="CheckBoxPresentingDiseaseNotDoc" runat="server" GroupName="PresentingDisease" Text="&nbsp;Not documented" />
					</td>
                </tr>
				<!-- Question 7 -->
                <tr class="table_body">
                    <td>7. Is this patient a cigarette smoker/tobacco user?</td>
                    <td>
						<asp:RadioButtonList ID="RadioButtonListSmoking" RepeatDirection="Vertical" CssClass="aspxList" runat="server">
							<asp:ListItem Value="1">Yes</asp:ListItem>
							<asp:ListItem Value="2">No</asp:ListItem>
							<asp:ListItem Value="3">Not Documented</asp:ListItem>
						</asp:RadioButtonList>
					</td>
                </tr>
                <!-- Question 8 -->
                <tr class="table_body_bg">
                    <td>8. Is this patient a contact lens wearer?</td>
                    <td>
						<asp:RadioButtonList ID="RadioButtonListContactLens" RepeatDirection="Vertical" CssClass="aspxList" runat="server">
							<asp:ListItem Value="1">Yes</asp:ListItem>
							<asp:ListItem Value="2">No</asp:ListItem>
							<asp:ListItem Value="3">Not Documented</asp:ListItem>
						</asp:RadioButtonList>
					</td>
                </tr>
				<!-- Question 9 -->
                <tr class="table_body">
                    <td>9. Prior eye surgery (e.g. refractive surgery, blepharoplasty) contributing to the condition? </td>
                    <td>
						<asp:RadioButtonList ID="RadioButtonListEyeSurgery" RepeatDirection="Vertical" CssClass="aspxList" runat="server">
							<asp:ListItem Value="1">Yes</asp:ListItem>
							<asp:ListItem Value="2">No</asp:ListItem>
							<asp:ListItem Value="3">Not Documented</asp:ListItem>
						</asp:RadioButtonList>
					</td>
                </tr>
                <!-- Question 10 -->
                <tr class="table_body_bg">
                    <td>10. Is there topical or oral medication use that may exacerbate dryness (diuretics, antihistamines, anti-depressants, anxiolytics, beta blockers, decongestants)?</td>
                    <td>
						<asp:RadioButtonList ID="RadioButtonListTopicalOralMed" RepeatDirection="Vertical" CssClass="aspxList" runat="server">
							<asp:ListItem Value="1">Yes</asp:ListItem>
							<asp:ListItem Value="2">No</asp:ListItem>
							<asp:ListItem Value="3">Not Documented</asp:ListItem>
						</asp:RadioButtonList>
					</td>
                </tr>
			</table>
            <br />
			<!-- Examination & Diagnostic Procedures Section -->
            <table>
                <!-- Header - Examination & Diagnostic Procedures -->
                <tr>
                    <th colspan="2" width="910px"><p>Examination &amp; Diagnostic Procedures (if findings are not symmetric, record data for the same eye as above)</p></th>
                </tr>
                <!-- Question 1 -->
                <tr class="table_body">
                    <td style="width:70%;">1. Was there an abnormality of eyelid position (e.g. ectropion, entropion) or other eyelid condition (e.g. floppy eyelid)</td>
                    <td>
						<asp:RadioButtonList ID="RadioButtonListExamAbEyelidPosition" RepeatDirection="Vertical" CssClass="aspxList" runat="server">
							<asp:ListItem Value="1">Yes</asp:ListItem>
							<asp:ListItem Value="2">No</asp:ListItem>
							<asp:ListItem Value="3">Not Documented</asp:ListItem>
						</asp:RadioButtonList>
					</td>
                </tr>
                <!-- Question 2 -->
                <tr class="table_body_bg">
                    <td>2. Abnormal or incomplete blink?</td>
                    <td>
						<asp:RadioButtonList ID="RadioButtonListExamAbnormalBlink" RepeatDirection="Vertical" CssClass="aspxList" runat="server">
							<asp:ListItem Value="1">Yes</asp:ListItem>
							<asp:ListItem Value="2">No</asp:ListItem>
							<asp:ListItem Value="3">Not Documented</asp:ListItem>
						</asp:RadioButtonList>
					</td>
                </tr>
				<!-- Question 3 -->
                <tr class="table_body">
                    <td>3. Lashes</td>
                    <td>
						<asp:RadioButtonList ID="RadioButtonListExamLashes" RepeatDirection="Vertical" CssClass="aspxList" runat="server">
							<asp:ListItem Value="104">Normal</asp:ListItem>
							<asp:ListItem Value="356">Debris</asp:ListItem>
							<asp:ListItem Value="357">Trichiasis / Distichiasis</asp:ListItem>
							<asp:ListItem Value="13">Other</asp:ListItem>
							<asp:ListItem Value="3">Not Documented</asp:ListItem>
						</asp:RadioButtonList>
					</td>
                </tr>
                <!-- Question 3, other -->
                <tr class="table_body">
                    <td><span id="Q3OtherText">If other, please specify</span></td>
                    <td><asp:TextBox runat="server" ID="TextBoxExamLashesText" size="20" /></td>
                </tr>
                <!-- Question 4 -->
                <tr class="table_body_bg">
                    <td>4. Were the patientís eyelids everted for examination?</td>
                    <td>
						<asp:RadioButtonList ID="RadioButtonListExamEyelidsEverted" RepeatDirection="Vertical" CssClass="aspxList" runat="server">
							<asp:ListItem Value="1">Yes</asp:ListItem>
							<asp:ListItem Value="2">No</asp:ListItem>
							<asp:ListItem Value="3">Not Documented</asp:ListItem>
						</asp:RadioButtonList>
					</td>
                </tr>
				<!-- Question 5 -->
                <tr class="table_body">
                    <td>5. Meibomian gland impaction or other meibomian disease?</td>
                    <td>
						<asp:RadioButtonList ID="RadioButtonListExamMeibomianDisease" RepeatDirection="Vertical" CssClass="aspxList" runat="server">
							<asp:ListItem Value="1">Yes</asp:ListItem>
							<asp:ListItem Value="2">No</asp:ListItem>
							<asp:ListItem Value="3">Not Documented</asp:ListItem>
						</asp:RadioButtonList>
					</td>
                </tr>
                <!-- Question 6 -->
                <tr class="table_body_bg">
                    <td>6. Tear meniscus height</td>
                    <td>
						<asp:RadioButtonList ID="RadioButtonListExamTearHeight" RepeatDirection="Vertical" CssClass="aspxList" runat="server">
							<asp:ListItem Value="52">High</asp:ListItem>
							<asp:ListItem Value="54">Low</asp:ListItem>
							<asp:ListItem Value="104">Normal</asp:ListItem>
							<asp:ListItem Value="3">Not Documented</asp:ListItem>
						</asp:RadioButtonList>
					</td>
                </tr>
				<!-- Question 7 -->
                <tr class="table_body">
                    <td>7. Tear breakup time</td>
                    <td>
						<asp:RadioButtonList ID="RadioButtonListExamTearBreakupTime" RepeatDirection="Vertical" CssClass="aspxList" runat="server">
							<asp:ListItem Value="10">Normal</asp:ListItem>
							<asp:ListItem Value="358">Decreased (&lt;10 sec.)</asp:ListItem>
							<asp:ListItem Value="203">Not Performed</asp:ListItem>
						</asp:RadioButtonList>
					</td>
                </tr>
                <!-- Question 8 -->
                <tr class="table_body_bg">
                    <td>8. Ocular surface staining with rose bengal / lissamine green</td>
                    <td>
						<asp:RadioButtonList ID="RadioButtonListExamOcularSurfaceStain" RepeatDirection="Vertical" CssClass="aspxList" runat="server">
							<asp:ListItem Value="241">Mild</asp:ListItem>
							<asp:ListItem Value="53">Moderate</asp:ListItem>
                            <asp:ListItem Value="286">Severe</asp:ListItem>
							<asp:ListItem Value="175">Present, severity not documented</asp:ListItem>
                            <asp:ListItem Value="128">None</asp:ListItem>
							<asp:ListItem Value="3">Not Documented</asp:ListItem>
						</asp:RadioButtonList>
					</td>
                </tr>
				<!-- Question 9 -->
                <tr class="table_body">
                    <td>9. Schirmer or other tear production test</td>
                    <td>
						<asp:RadioButtonList ID="RadioButtonListExamSchirmer" RepeatDirection="Vertical" CssClass="aspxList" runat="server">
							<asp:ListItem Value="10">Normal</asp:ListItem>
							<asp:ListItem Value="358">Decreased</asp:ListItem>
							<asp:ListItem Value="3">Not Performed</asp:ListItem>
						</asp:RadioButtonList>
					</td>
                </tr>
                <!-- Question 10 -->
                <tr class="table_body_bg">
                    <td>10. Tear osmolarity:</td>
                    <td>
                        <asp:TextBox runat="server" meta="{vMin: '0', vMax: '350'}" class="auto" ID="TextBoxExamTearOsmolarityOsmolesNbr" size="6" />&nbsp;mOsm/l
						<br /><asp:CheckBox runat="server" ID="CheckBoxExamTearOsmolarity" Text="Not Performed" />
					</td>
                </tr>
				<!-- Question 11 -->
                <tr class="table_body">
                    <td>11. Conjunctival findings</td>
                    <td>
						<asp:Checkbox ID="CheckBoxExamConjFindNormal" runat="server" GroupName="ExamConjFind" Text="&nbsp;Normal" />
						<br /><asp:Checkbox ID="CheckBoxExamConjFindConj" runat="server" GroupName="ExamConjFind" Text="&nbsp;Conjunctivochalasis" />
						<br /><asp:Checkbox ID="CheckBoxExamConjFindInjection" runat="server" GroupName="ExamConjFind" Text="&nbsp;Injection" />
						<br /><asp:Checkbox ID="CheckBoxExamConjFindSuperior" runat="server" GroupName="ExamConjFind" Text="&nbsp;Superior limbic keratoconjunctivitis" />
						<br /><asp:Checkbox ID="CheckBoxExamConjFindSymblepharon" runat="server" GroupName="ExamConjFind" Text="&nbsp;Symblepharon" />
						<br /><asp:Checkbox ID="CheckBoxExamConjFindOther" runat="server" GroupName="ExamConjFind" Text="&nbsp;Other" />&nbsp;<asp:TextBox runat="server" ID="TextBoxExamConjFindOtherText" size="20" />
						<br /><asp:Checkbox ID="CheckBoxExamConjFindNotDoc" runat="server" GroupName="ExamConjFind" Text="&nbsp;Not Documented" />
					</td>
                </tr>
                <!-- Question 12 -->
                <tr class="table_body_bg">
                    <td>12. Corneal sensation</td>
                    <td>
						<asp:RadioButtonList ID="RadioButtonListExamCornealSensation" RepeatDirection="Vertical" CssClass="aspxList" runat="server">
							<asp:ListItem Value="10">Normal</asp:ListItem>
							<asp:ListItem Value="358">Decreased</asp:ListItem>
							<asp:ListItem Value="3">Not Performed</asp:ListItem>
						</asp:RadioButtonList>
					</td>
                </tr>
				<!-- Question 13 -->
                <tr class="table_body">
                    <td>13. Corneal findings</td>
                    <td>
						<asp:Checkbox ID="CheckBoxExamCornealNormal" runat="server" GroupName="ExamCorneal" Text="&nbsp;Normal" />
						<br /><asp:Checkbox ID="CheckBoxExamCornealPunctate" runat="server" GroupName="ExamCorneal" Text="&nbsp;Punctate keratopathy" />
						<br /><asp:Checkbox ID="CheckBoxExamCornealEpithelial" runat="server" GroupName="ExamCorneal" Text="&nbsp;Epithelial defect" />
						<br /><asp:Checkbox ID="CheckBoxExamCornealFilaments" runat="server" GroupName="ExamCorneal" Text="&nbsp;Filaments" />
						<br /><asp:Checkbox ID="CheckBoxExamCornealNeovascularizations" runat="server" GroupName="ExamCorneal" Text="&nbsp;Neovascularization" />
						<br /><asp:Checkbox ID="CheckBoxExamCornealScar" runat="server" GroupName="ExamCorneal" Text="&nbsp;Scar / Haze" />
						<br /><asp:Checkbox ID="CheckBoxExamCornealStromal" runat="server" GroupName="ExamCorneal" Text="&nbsp;Stromal thinning" />
						<br /><asp:Checkbox ID="CheckBoxExamCornealOther" runat="server" GroupName="ExamCorneal" Text="&nbsp;Other" />&nbsp;<asp:TextBox runat="server" ID="TextBoxExamCornealOtherText" size="20" />
						<br /><asp:Checkbox ID="CheckBoxExamCornealNotDoc" runat="server" GroupName="ExamCorneal" Text="&nbsp;Not Documented" />
					</td>
                </tr>
			</table>
			<br />
			<!-- Assessment & Management Section -->
            <table>
                <!-- Header - Assessment & Management -->
                <tr>
                    <th colspan="3" width="910px"><p>Assessment &amp; Management (if findings are not symmetric, record data for the same eye as above)</p></th>
                </tr>
                <!-- Question 1 -->
                <tr class="table_body">
                    <td colspan="2" style="width:70%;">1. Diagnosis (check all that apply)</td>
                    <td>
						<asp:Checkbox ID="CheckBoxDiagnosisAqueousTear" runat="server" GroupName="Diagnosis" Text="&nbsp;Aqueous tear deficiency" />
						<br /><asp:Checkbox ID="CheckBoxDiagnosisEvaporativeDryEye" runat="server" GroupName="Diagnosis" Text="&nbsp;Evaporative Dry Eye due to Meibomian Dysfunction" />
						<br /><asp:Checkbox ID="CheckBoxDiagnosisEvaporativeDueToOther" runat="server" GroupName="Diagnosis" Text="&nbsp;Evaporative Dry Eye due to eyelid malposition, exposure keratopathy, or seventh nerve dysfunction" />
						<br /><asp:Checkbox ID="CheckBoxDiagnosisNeurotropic" runat="server" GroupName="Diagnosis" Text="&nbsp;Neurotrophic keratopathy" />
						<br /><asp:Checkbox ID="CheckBoxDiagnosisOcular" runat="server" GroupName="Diagnosis" Text="&nbsp;Ocular surface disease secondary to systemic condition (e.g. Sjogren syndrome, immunobullous disease, diabetes)" />
						<br /><asp:Checkbox ID="CheckBoxDiagnosisUnspec" runat="server" GroupName="Diagnosis" Text="&nbsp;Unspecified/uncertain" />
						<br /><asp:Checkbox ID="CheckBoxDiagnosisOther" runat="server" GroupName="Diagnosis" Text="&nbsp;Other" />&nbsp;<asp:TextBox runat="server" ID="TextBoxDiagnosisOtherText" size="20" />
						<br /><asp:Checkbox ID="CheckBoxDiagnosisNotDoc" runat="server" GroupName="Diagnosis" Text="&nbsp;Not Documented " />
					</td>
                </tr>
				
				
                <!-- Question 2 -->
                <tr class="table_body_bg">
                    <td colspan="2">2. Disease severity</td>
                    <td>
						<asp:RadioButtonList ID="RadioButtonListAssessDiseaseSeverity" RepeatDirection="Vertical" CssClass="aspxList" runat="server">
							<asp:ListItem Value="241">Mild</asp:ListItem>
							<asp:ListItem Value="53">Moderate</asp:ListItem>
							<asp:ListItem Value="286">Severe</asp:ListItem>
							<asp:ListItem Value="3">Not Documented</asp:ListItem>
						</asp:RadioButtonList>
					</td>
                </tr>
				
				
				<!-- Question 3 -->
                <tr class="table_body">
					<td colspan="2">3. Therapeutic options recommended at initial visit</td>
					<td class="thirds">
						<table>
							<tr>
								<td>Discussed / Recommended</td>
								<td class="rightCol">Prescribed</td>
							</tr>
						</table>
					</td>
                </tr>
				<!-- Question 3 - Topical Therapy -->
                <tr class="table_body">
                    <td style="width: 30%" rowspan="12">Topical Therapy</td>
                    <td class="midCol">Artificial tears</td>
					<td class="thirds">
						<table>
							<tr>
								<td><asp:CheckBox ID="RadioButtonDiscussArticialTears" runat="server" GroupName="DiscussArticialTears" Text="" /></td>
								<td class="rightCol"></td>
							</tr>
						</table>
					</td>
                </tr>
                <tr class="table_body">
                    <td style="width:40%;" class="midCol">Aqueous based gel lubricants</td>
                    <td class="thirds">
						<table>
							<tr>
								<td><asp:CheckBox ID="RadioButtonDiscussAqueousGelLubricant" runat="server" GroupName="DiscussAqueousGelLubricant" Text="" /></td>
								<td class="rightCol"></td>
							</tr>
						</table>
					</td>
                </tr>
				<tr class="table_body">
                    <td class="midCol">Preservative free artificial tears</td>
                    <td class="thirds">
						<table>
							<tr>
								<td><asp:CheckBox ID="RadioButtonDiscussPreservativeFreeTears" runat="server" GroupName="PreservativeFreeTears" Text="" /></td>
								<td class="rightCol"></td>
							</tr>
						</table>
					</td>
                </tr>
                <tr class="table_body">
                    <td class="midCol">Non-medicated ointment</td>
                    <td class="thirds">
						<table>
							<tr>
								<td><asp:CheckBox ID="RadioButtonDiscussNonMedOintment" runat="server" GroupName="NonMedOintment" Text="" /></td>
								<td class="rightCol"></td>
							</tr>
						</table>
					</td>
                </tr>
				<tr class="table_body">
                    <td class="midCol">Topical cyclosporine</td>
                    <td class="thirds">
						<table>
							<tr>
								<td><asp:CheckBox ID="RadioButtonDiscussTopicalCyclo" runat="server" GroupName="TopicalCyclo" Text="" /></td>
								<td class="rightCol"><asp:CheckBox ID="RadioButtonPrescribeTopicalCyclo" runat="server" GroupName="TopicalCyclo" Text="" /></td>
							</tr>
						</table>
					</td>
                </tr>
				<tr class="table_body">
                    <td class="midCol">Topical corticosteroids</td>
                    <td class="thirds">
						<table>
							<tr>
								<td><asp:CheckBox ID="RadioButtonDiscussTopicalCortico" runat="server" GroupName="TopicalCortico" Text="" /></td>
								<td class="rightCol"><asp:CheckBox ID="RadioButtonPrescribeTopicalCortico" runat="server" GroupName="TopicalCortico" Text="" /></td>
							</tr>
						</table>
					</td>
                </tr>
				<tr class="table_body">
                    <td class="midCol">Topical azithromycin</td>
                    <td class="thirds">
						<table>
							<tr>
								<td><asp:CheckBox ID="RadioButtonDiscussTopicalAzithromycin" runat="server" GroupName="TopicalAzithromycin" Text="" /></td>
								<td class="rightCol"><asp:CheckBox ID="RadioButtonPrescribeTopicalAzithromycin" runat="server" GroupName="TopicalAzithromycin" Text="" /></td>
							</tr>
						</table>
					</td>
                </tr>
				<tr class="table_body">
                    <td class="midCol">Other topical antibiotics</td>
                    <td class="thirds">
						<table>
							<tr>
								<td><asp:CheckBox ID="RadioButtonDiscussTopicalOther" runat="server" GroupName="TopicalOther" Text="" /></td>
								<td class="rightCol"><asp:CheckBox ID="RadioButtonPrescribeTopicalOther" runat="server" GroupName="TopicalOther" Text="" /></td>
							</tr>
						</table>
					</td>
                </tr>
				<tr class="table_body">
                    <td class="midCol">Topical mucolytics</td>
                    <td class="thirds">
						<table>
							<tr>
								<td><asp:CheckBox ID="RadioButtonDiscussTopicalMucolytics" runat="server" GroupName="TopicalMucolytics" Text="" /></td>
								<td class="rightCol"><asp:CheckBox ID="RadioButtonPrescribeTopicalucolytics" runat="server" GroupName="TopicalMucolytics" Text="" /></td>
							</tr>
						</table>
					</td>
                </tr>
				<tr class="table_body">
                    <td class="midCol">Autologous serum</td>
                    <td class="thirds">
						<table>
							<tr>
								<td><asp:CheckBox ID="RadioButtonDiscussAutologousSerum" runat="server" GroupName="AutologousSerum" Text="" /></td>
								<td class="rightCol"><asp:CheckBox ID="RadioButtonPrescribeAutologousSerum" runat="server" GroupName="AutologousSerum" Text="" /></td>
							</tr>
						</table>
					</td>
                </tr>
				<tr class="table_body">
                    <td class="midCol">Change current topical medications</td>
                    <td class="thirds">
						<table>
							<tr>
								<td><asp:CheckBox ID="RadioButtonDiscussChangeTopical" runat="server" GroupName="ChangeTopical" Text="" /></td>
								<td class="rightCol"><asp:CheckBox ID="RadioButtonPrescribeChangeTopical" runat="server" GroupName="ChangeTopical" Text="" /></td>
							</tr>
						</table>
					</td>
                </tr>
				<tr class="table_body">
                    <td class="midCol">Other&nbsp;<asp:TextBox runat="server" ID="TextBoxTopicalTherapyOtherText" size="20" /></td>
                    <td class="thirds">
						<table>
							<tr>
								<td><asp:CheckBox ID="RadioButtonTopicalTherapyOther" runat="server" GroupName="TopicalTherapyOther" Text="" /></td>
								<td class="rightCol"><asp:CheckBox ID="RadioButtonTopicalTherapyOtherP" runat="server" GroupName="TopicalTherapyOther" Text="" /></td>
							</tr>
						</table>
					</td>
                </tr>
				<!-- Question 3 - Oral Therapy -->
                <tr class="table_body">
                    <td rowspan="5">Oral Therapy</td>
                    <td class="midCol">Oral tetracycline or azithromycin (or derivatives, e.g. doxycycline)</td>
					<td class="thirds">
						<table style="height:4.5em;">
							<tr>
								<td><asp:CheckBox ID="RadioButtonDiscussOralTetra" runat="server" GroupName="OralTetra" Text="" /></td>
								<td class="rightCol"><asp:CheckBox ID="RadioButtonPrescribeOralTetra" runat="server" GroupName="OralTetra" Text="" /></td>
							</tr>
						</table>
					</td>
                </tr>
                <tr class="table_body">
                    <td class="midCol">Oral nutritional supplements (e.g. omega-3)</td>
                    <td class="thirds">
						<table>
							<tr>
								<td><asp:CheckBox ID="RadioButtonDiscussOralNutritionSup" runat="server" GroupName="OralNutritionSup" Text="" /></td>
								<td class="rightCol"></td>
							</tr>
						</table>
					</td>
                </tr>
				<tr class="table_body">
                    <td class="midCol">Oral secretagogues</td>
                    <td class="thirds">
						<table>
							<tr>
								<td><asp:CheckBox ID="RadioButtonDiscussOralSecretagogues" runat="server" GroupName="OralSecretagogues" Text="" /></td>
								<td class="rightCol"><asp:CheckBox ID="RadioButtonPrescribeOralSecretagogues" runat="server" GroupName="OralSecretagogues" Text="" /></td>
							</tr>
						</table>
					</td>
                </tr>
				<tr class="table_body">
                    <td class="midCol">Change current systemic medications</td>
                    <td class="thirds">
						<table>
							<tr>
								<td><asp:CheckBox ID="RadioButtonDiscussChangeSystemic" runat="server" GroupName="ChangeSystemic" Text="" /></td>
								<td class="rightCol"><asp:CheckBox ID="RadioButtonPrescribeChangeSystemic" runat="server" GroupName="ChangeSystemic" Text="" /></td>
							</tr>
						</table>
					</td>
                </tr>
				<tr class="table_body">
                    <td class="midCol">Other&nbsp;<asp:TextBox runat="server" ID="TextBoxOralTherapyOtherText" size="20" /></td>
                    <td class="thirds">
						<table style="height:3.5em;">
							<tr>
								<td><asp:CheckBox ID="RadioButtonOralTherapyOther" runat="server" GroupName="TherapyOther" Text="" /></td>
								<td class="rightCol"><asp:CheckBox ID="RadioButtonOralTherapyOtherP" runat="server" GroupName="TherapyOther" Text="" /></td>
							</tr>
						</table>
					</td>
                </tr>
				<!-- Question 3 - Procedural Intervention -->
                <tr class="table_body">
                    <td rowspan="11">Procedural Intervention</td>
                    <td class="midCol"></td>
					<td class="thirds">
						<table>
							<tr>
								<td>Discussed / Recommended</td>
								<td class="rightCol">Performed</td>
							</tr>
						</table>
					</td>
                </tr>
                <tr class="table_body">
                    <td class="midCol">Temporary punctal plugs</td>
					<td class="thirds">
						<table>
							<tr>
								<td><asp:CheckBox ID="RadioButtonDiscussTemporaryPunctal" runat="server" GroupName="TemporaryPunctal" Text="" /></td>
								<td class="rightCol"><asp:CheckBox ID="RadioButtonPrescribeTemporaryPunctal" runat="server" GroupName="TemporaryPunctal" Text="" /></td>
							</tr>
						</table>
					</td>
                </tr>
                <tr class="table_body">
                    <td class="midCol">Permanent punctal plugs</td>
                    <td class="thirds">
						<table>
							<tr>
								<td><asp:CheckBox ID="RadioButtonDiscussPermanentPunctal" runat="server" GroupName="PermanentPunctal" Text="" /></td>
								<td class="rightCol"><asp:CheckBox ID="RadioButtonPrescribePermanentPunctal" runat="server" GroupName="PermanentPunctal" Text="" /></td>
							</tr>
						</table>
					</td>
                </tr>
				<tr class="table_body">
                    <td colspan="1">Punctal obliteration by:</td>
					<td class="thirds">
						<table>
							<tr>
								<td style="height:2.1em;"></td>
								<td class="rightCol"></td>
							</tr>
						</table>
					</td>
                </tr>
				<tr class="table_body">
                    <td class="midCol">Laser</td>
                    <td class="thirds">
						<table>
							<tr>
								<td><asp:CheckBox ID="RadioButtonDiscussLaser" runat="server" GroupName="Laser" Text="" /></td>
								<td class="rightCol"><asp:CheckBox ID="RadioButtonPrescribeLaser" runat="server" GroupName="Laser" Text="" /></td>
							</tr>
						</table>
					</td>
                </tr>
				<tr class="table_body">
                    <td class="midCol">Thermocautery</td>
                    <td class="thirds">
						<table>
							<tr>
								<td><asp:CheckBox ID="RadioButtonDiscussThermocautery" runat="server" GroupName="Thermocautery" Text="" /></td>
								<td class="rightCol"><asp:CheckBox ID="RadioButtonPrescribeThermocautery" runat="server" GroupName="Thermocautery" Text="" /></td>
							</tr>
						</table>
					</td>
                </tr>
				<tr class="table_body">
                    <td class="midCol">Surgery </td>
                    <td class="thirds">
						<table>
							<tr>
								<td><asp:CheckBox ID="RadioButtonDiscussSurgery" runat="server" GroupName="Surgery" Text="" /></td>
								<td class="rightCol"><asp:CheckBox ID="RadioButtonPrescribeSurgery" runat="server" GroupName="Surgery" Text="" /></td>
							</tr>
						</table>
					</td>
                </tr>
				<tr class="table_body">
                    <td class="midCol">Meibomian gland procedure (e.g. Maskin probing, Lipiflow)</td>
                    <td class="thirds">
						<table>
							<tr>
								<td><asp:CheckBox ID="RadioButtonDiscussGlandProcedure" runat="server" GroupName="GlandProcedure" Text="" /></td>
								<td class="rightCol"><asp:CheckBox ID="RadioButtonPrescribeGlandProcedure" runat="server" GroupName="GlandProcedure" Text="" /></td>
							</tr>
						</table>
					</td>
                </tr>
				<tr class="table_body">
                    <td class="midCol">Conjunctival Surgery (e.g. cautery for SLK, excision of conjunctivochalasis)</td>
                    <td class="thirds">
						<table style="height:4.5em;">
							<tr>
								<td><asp:CheckBox ID="RadioButtonDiscussConjunctivalSurgery" runat="server" GroupName="ConjunctivalSurgery" Text="" /></td>
								<td class="rightCol"><asp:CheckBox ID="RadioButtonPrescribeConjunctivalSurgery" runat="server" GroupName="ConjunctivalSurgery" Text="" /></td>
							</tr>
						</table>
					</td>
                </tr>
				<tr class="table_body">
                    <td class="midCol">Eyelid surgery (e.g. ectropion repair, tarsorrhaphy</td>
                    <td class="thirds">
						<table>
							<tr>
								<td><asp:CheckBox ID="RadioButtonDiscussEyelidSurgery" runat="server" GroupName="EyelidSurgery" Text="" /></td>
								<td class="rightCol"><asp:CheckBox ID="RadioButtonPrescribeEyelidSurgery" runat="server" GroupName="EyelidSurgery" Text="" /></td>
							</tr>
						</table>
					</td>
                </tr>
				<tr class="table_body">
                    <td class="midCol">Other&nbsp;<asp:TextBox runat="server" ID="TextBoxSurgeryOtherText" size="20" /></td>
                    <td class="thirds">
						<table>
							<tr>
								<td style="height:2.1em"><asp:CheckBox ID="RadioButtonSurgeryOther" runat="server" GroupName="SurgeryOther" Text="" /></td>
								<td class="rightCol"><asp:CheckBox ID="RadioButtonSurgeryOtherP" runat="server" GroupName="SurgeryOther" Text="" /></td>
							</tr>
						</table>
					</td>
                </tr>
				<!-- Question 3 - Other Adjunctive Therapy -->
                <tr class="table_body">
                    <td rowspan="9">Other Adjunctive Therapy</td>
                    <td class="midCol"></td>
					<td class="thirds">
						<table>
							<tr>
								<td>Discussed / Recommended</td>
								<td class="rightCol">Prescribed</td>
							</tr>
						</table>
					</td>
                </tr>
                <tr class="table_body">
                    <td class="midCol">Eyelid hygiene</td>
					<td class="thirds">
						<table>
							<tr>
								<td><asp:CheckBox ID="RadioButtonDiscussEyelidHygiene" runat="server" GroupName="EyelidHygiene" Text="" /></td>
								<td class="rightCol"></td>
							</tr>
						</table>
					</td>
                </tr>
                <tr class="table_body">
                    <td class="midCol">Environmental modifications (i.e. humidifier)</td>
                    <td class="thirds">
						<table>
							<tr>
								<td><asp:CheckBox ID="RadioButtonDiscussEnvironmentMod" runat="server" GroupName="EnvironmentMod" Text="" /></td>
								<td class="rightCol"></td>
							</tr>
						</table>
					</td>
                </tr>
				<tr class="table_body">
                    <td colspan="1">Contact lenses</td>
					<td class="thirds">
						<table>
							<tr>
								<td style="height:2.1em;"></td>
								<td class="rightCol"></td>
							</tr>
						</table>
					</td>
                </tr>
				<tr class="table_body">
                    <td class="midCol">Soft</td>
                    <td class="thirds">
						<table>
							<tr>
								<td><asp:CheckBox ID="RadioButtonDiscussContactLensSoft" runat="server" GroupName="ContactLensSoft" Text="" /></td>
								<td class="rightCol"><asp:CheckBox ID="RadioButtonPrescribeContactLensSoft" runat="server" GroupName="ContactLensSoft" Text="" /></td>
							</tr>
						</table>
					</td>
                </tr>
				<tr class="table_body">
                    <td class="midCol">Scleral</td>
                    <td class="thirds">
						<table>
							<tr>
								<td><asp:CheckBox ID="RadioButtonDiscussContactLensScleral" runat="server" GroupName="ContactLensScleral" Text="" /></td>
								<td class="rightCol"><asp:CheckBox ID="RadioButtonPrescribeContactLensScleral" runat="server" GroupName="ContactLensScleral" Text="" /></td>
							</tr>
						</table>
					</td>
                </tr>
				<tr class="table_body">
                    <td class="midCol">Other</td>
                    <td class="thirds">
						<table>
							<tr>
								<td style="height:2.1em"><asp:CheckBox ID="RadioButtonDiscussContactLensOther" runat="server" GroupName="ContactLensOther" Text="" /></td>
								<td class="rightCol"><asp:CheckBox ID="RadioButtonPrescribeContactLensOther" runat="server" GroupName="ContactLensOther" Text="" /></td>
							</tr>
						</table>
					</td>
                </tr>
				<tr class="table_body">
                    <td class="midCol">Moisture chamber glasses, protective eyewear, sleep mask</td>
                    <td class="thirds">
						<table>
							<tr>
								<td><asp:CheckBox ID="RadioButtonDiscussMoistureChamber" runat="server" GroupName="MoistureChamber" Text="" /></td>
								<td class="rightCol"></td>
							</tr>
						</table>
					</td>
                </tr>
				<tr class="table_body">
                    <td class="midCol">Other <asp:TextBox runat="server" ID="TextBoxDiscussAdjunctiveTherapyOtherText" size="20" /></td>
                    <td class="thirds">
						<table style="height:3.5em;">
							<tr>
								<td><asp:CheckBox ID="RadioButtonPrescribeAdjunctiveTherapyOther" runat="server" GroupName="AdjunctiveTherapyOther" Text="" /></td>
								<td class="rightCol"><asp:CheckBox ID="RadioButtonDiscussAdjunctiveTherapyOther" runat="server" GroupName="AdjunctiveTherapyOther" Text="" /></td>
							</tr>
						</table>
					</td>
                </tr>
                <!-- Question 3 - Not Documented -->
                <tr class="table_body">
                    <td colspan="2">No documentation of therapeutic options prescribed or discussed</td>
					<td style="text-align:center;"><asp:Checkbox ID="CheckBox" runat="server" GroupName="TherapyNotDoc" /></td>
                </tr>
				
                <!-- Question 4 -->
                <tr class="table_body_bg">
                    <td colspan="2">4. Was the patient referred for evaluation / management of systemic disease OR did the patient continue follow-up with a previous physician for evaluation / management of systemic disease</td>
                    <td>
						<asp:RadioButtonList ID="RadioButtonListEMSystemicDisease" RepeatDirection="Vertical" CssClass="aspxList" runat="server">
							<asp:ListItem Value="1">Yes</asp:ListItem>
							<asp:ListItem Value="2">No</asp:ListItem>
							<asp:ListItem Value="3">Not Documented</asp:ListItem>
						</asp:RadioButtonList>
					</td>
                </tr>
			</table>
			<br />
			<!-- Outcomes Section -->
            <table>
                <!-- Header - Outcomes -->
                <tr>
                    <th colspan="3" width="910px"><p>Outcomes</p></th>
                </tr>
                <!-- Question 1 -->
                <tr class="table_body">
                    <td colspan="2">1. Date of last visit </td>
                    <td>
						<asp:DropDownList ID="DropDownListOutcomeDateMonth" DataValueField="MonthID" DataTextField="MonthName" runat="server" />
						<asp:DropDownList ID="DropDownListOutcomeDateYear" DataValueField="YearID" DataTextField="YearName" runat="server" />
					</td>
                </tr>
                

                <tr class="table_body">
					<td colspan="2">2. During the course of follow-up, what additional therapy was considered?</td>
					<td class="thirds">
						<table>
							<tr>
								<td>Discussed / Recommended</td>
								<td class="rightCol">Prescribed</td>
							</tr>
						</table>
					</td>
                </tr>
				<!-- Question 2 - Topical Therapy -->
                <tr class="table_body">
                    <td style="width: 30%" rowspan="12">Topical Therapy</td>
                    <td class="midCol">Artificial tears</td>
					<td class="thirds">
						<table>
							<tr>
								<td><asp:CheckBox ID="CheckBoxOutcomeAlterArtificialTears" runat="server" GroupName="DiscussArticialTears" Text="" /></td>
								<td class="rightCol"></td>
							</tr>
						</table>
					</td>
                </tr>
                <tr class="table_body">
                    <td style="width:40%;" class="midCol">Aqueous based gel lubricants</td>
                    <td class="thirds">
						<table>
							<tr>
								<td><asp:CheckBox ID="CheckBoxOutcomeAqueousGelLubricant" runat="server" GroupName="DiscussAqueousGelLubricant" Text="" /></td>
								<td class="rightCol"></td>
							</tr>
						</table>
					</td>
                </tr>
				<tr class="table_body">
                    <td class="midCol">Preservative free artificial tears</td>
                    <td class="thirds">
						<table>
							<tr>
								<td><asp:CheckBox ID="CheckBoxOutcomeAlterPreservativeFreeTears" runat="server" GroupName="PreservativeFreeTears" Text="" /></td>
								<td class="rightCol"></td>
							</tr>
						</table>
					</td>
                </tr>
                <tr class="table_body">
                    <td class="midCol">Non-medicated ointment</td>
                    <td class="thirds">
						<table>
							<tr>
								<td><asp:CheckBox ID="CheckBoxOutcomeAlterNonMedOintment" runat="server" GroupName="NonMedOintment" Text="" /></td>
								<td class="rightCol"></td>
							</tr>
						</table>
					</td>
                </tr>
				<tr class="table_body">
                    <td class="midCol">Topical cyclosporine</td>
                    <td class="thirds">
						<table>
							<tr>
								<td><asp:CheckBox ID="CheckBoxOutcomeAlterTopicalCyclo" runat="server" GroupName="TopicalCyclo" Text="" /></td>
								<td class="rightCol"><asp:CheckBox ID="CheckBoxOutcomeAlterTopicalCycloP" runat="server" GroupName="TopicalCyclo" Text="" /></td>
							</tr>
						</table>
					</td>
                </tr>
				<tr class="table_body">
                    <td class="midCol">Topical corticosteroids</td>
                    <td class="thirds">
						<table>
							<tr>
								<td><asp:CheckBox ID="CheckBoxOutcomeAlterTopicalCortico" runat="server" GroupName="TopicalCortico" Text="" /></td>
								<td class="rightCol"><asp:CheckBox ID="CheckBoxOutcomeAlterTopicalCorticoP" runat="server" GroupName="TopicalCortico" Text="" /></td>
							</tr>
						</table>
					</td>
                </tr>
				<tr class="table_body">
                    <td class="midCol">Topical azithromycin</td>
                    <td class="thirds">
						<table>
							<tr>
								<td><asp:CheckBox ID="CheckBoxOutcomeAlterTopicalAzithromycin" runat="server" GroupName="TopicalAzithromycin" Text="" /></td>
								<td class="rightCol"><asp:CheckBox ID="CheckBoxOutcomeAlterTopicalAzithromycinP" runat="server" GroupName="TopicalAzithromycin" Text="" /></td>
							</tr>
						</table>
					</td>
                </tr>
				<tr class="table_body">
                    <td class="midCol">Other topical antibiotics</td>
                    <td class="thirds">
						<table>
							<tr>
								<td><asp:CheckBox ID="CheckBoxOutcomeAlterTopicalOther" runat="server" GroupName="TopicalOther" Text="" /></td>
								<td class="rightCol"><asp:CheckBox ID="CheckBoxOutcomeAlterTopicalOtherP" runat="server" GroupName="TopicalOther" Text="" /></td>
							</tr>
						</table>
					</td>
                </tr>
				<tr class="table_body">
                    <td class="midCol">Topical mucolytics</td>
                    <td class="thirds">
						<table>
							<tr>
								<td><asp:CheckBox ID="CheckBoxOutcomeAlterTopicalMucolytics" runat="server" GroupName="TopicalMucolytics" Text="" /></td>
								<td class="rightCol"><asp:CheckBox ID="CheckBoxOutcomeAlterTopicalMucolyticsP" runat="server" GroupName="TopicalMucolytics" Text="" /></td>
							</tr>
						</table>
					</td>
                </tr>
				<tr class="table_body">
                    <td class="midCol">Autologous serum</td>
                    <td class="thirds">
						<table>
							<tr>
								<td><asp:CheckBox ID="CheckBoxOutcomeAlterAutologousSerum" runat="server" GroupName="AutologousSerum" Text="" /></td>
								<td class="rightCol"><asp:CheckBox ID="CheckBoxOutcomeAlterAutologousSerumP" runat="server" GroupName="AutologousSerum" Text="" /></td>
							</tr>
						</table>
					</td>
                </tr>
				<tr class="table_body">
                    <td class="midCol">Change current topical medications</td>
                    <td class="thirds">
						<table>
							<tr>
								<td><asp:CheckBox ID="CheckBoxOutcomeAlterChangeTopical" runat="server" GroupName="ChangeTopical" Text="" /></td>
								<td class="rightCol"><asp:CheckBox ID="CheckBoxOutcomeAlterChangeTopicalP" runat="server" GroupName="ChangeTopical" Text="" /></td>
							</tr>
						</table>
					</td>
                </tr>
				<tr class="table_body">
                    <td class="midCol">Other&nbsp;<asp:TextBox runat="server" ID="TextBoxOutcomeAlterTopicalTherapyOtherText" size="20" /></td>
                    <td class="thirds">
						<table>
							<tr>
								<td><asp:CheckBox ID="CheckBoxOutcomeAlterTopicalTherapyOther" runat="server" GroupName="TopicalTherapyOther" Text="" /></td>
								<td class="rightCol"><asp:CheckBox ID="CheckBoxOutcomeAlterTopicalTherapyOtherP" runat="server" GroupName="TopicalTherapyOther" Text="" /></td>
							</tr>
						</table>
					</td>
                </tr>
				<!-- Question 2 - Oral Therapy -->
                <tr class="table_body">
                    <td rowspan="5">Oral Therapy</td>
                    <td class="midCol">Oral tetracycline or azithromycin (or derivatives, e.g. doxycycline)</td>
					<td class="thirds">
						<table style="height:4.5em;">
							<tr>
								<td><asp:CheckBox ID="CheckBoxOutcomeAlterOralTetra" runat="server" GroupName="OralTetra" Text="" /></td>
								<td class="rightCol"><asp:CheckBox ID="CheckBoxOutcomeAlterOralTetraP" runat="server" GroupName="OralTetra" Text="" /></td>
							</tr>
						</table>
					</td>
                </tr>
                <tr class="table_body">
                    <td class="midCol">Oral nutritional supplements (e.g. omega-3)</td>
                    <td class="thirds">
						<table>
							<tr>
								<td><asp:CheckBox ID="CheckBoxOutcomeAlterOralNutritionSup" runat="server" GroupName="OralNutritionSup" Text="" /></td>
								<td class="rightCol"></td>
							</tr>
						</table>
					</td>
                </tr>
				<tr class="table_body">
                    <td class="midCol">Oral secretagogues</td>
                    <td class="thirds">
						<table>
							<tr>
								<td><asp:CheckBox ID="CheckBoxOutcomeAlterOralSecretagogues" runat="server" GroupName="OralSecretagogues" Text="" /></td>
								<td class="rightCol"><asp:CheckBox ID="CheckBoxOutcomeAlterOralSecretagoguesP" runat="server" GroupName="OralSecretagogues" Text="" /></td>
							</tr>
						</table>
					</td>
                </tr>
				<tr class="table_body">
                    <td class="midCol">Change current systemic medications</td>
                    <td class="thirds">
						<table>
							<tr>
								<td><asp:CheckBox ID="CheckBoxOutcomeAlterChangeSystemic" runat="server" GroupName="ChangeSystemic" Text="" /></td>
								<td class="rightCol"><asp:CheckBox ID="CheckBoxOutcomeAlterChangeSystemicP" runat="server" GroupName="ChangeSystemic" Text="" /></td>
							</tr>
						</table>
					</td>
                </tr>
				<tr class="table_body">
                    <td class="midCol">Other&nbsp;<asp:TextBox runat="server" ID="TextBoxOutcomeAlterOralOtherText" size="20" /></td>
                    <td class="thirds">
						<table style="height:3.5em;">
							<tr>
								<td><asp:CheckBox ID="CheckBoxOutcomeAlterOralOther" runat="server" GroupName="TherapyOther" Text="" /></td>
								<td class="rightCol"><asp:CheckBox ID="CheckBoxOutcomeAlterOralOtherP" runat="server" GroupName="TherapyOther" Text="" /></td>
							</tr>
						</table>
					</td>
                </tr>
				<!-- Question 2 - Procedural Intervention -->
                <tr class="table_body">
                    <td rowspan="11">Procedural Intervention</td>
                    <td class="midCol"></td>
					<td class="thirds">
						<table>
							<tr>
								<td>Discussed / Recommended</td>
								<td class="rightCol">Performed</td>
							</tr>
						</table>
					</td>
                </tr>
                <tr class="table_body">
                    <td class="midCol">Temporary punctal plugs</td>
					<td class="thirds">
						<table>
							<tr>
								<td><asp:CheckBox ID="CheckBoxOutcomeAlterTemporaryPunctal" runat="server" GroupName="TemporaryPunctal" Text="" /></td>
								<td class="rightCol"><asp:CheckBox ID="CheckBoxOutcomeAlterTemporaryPunctalP" runat="server" GroupName="TemporaryPunctal" Text="" /></td>
							</tr>
						</table>
					</td>
                </tr>
                <tr class="table_body">
                    <td class="midCol">Permanent punctal plugs</td>
                    <td class="thirds">
						<table>
							<tr>
								<td><asp:CheckBox ID="CheckBoxOutcomeAlterPermanentPunctal" runat="server" GroupName="PermanentPunctal" Text="" /></td>
								<td class="rightCol"><asp:CheckBox ID="CheckBoxOutcomeAlterPermanentPunctalP" runat="server" GroupName="PermanentPunctal" Text="" /></td>
							</tr>
						</table>
					</td>
                </tr>
				<tr class="table_body">
                    <td colspan="1">Punctal obliteration by:</td>
					<td class="thirds">
						<table>
							<tr>
								<td style="height:2.1em;"></td>
								<td class="rightCol"></td>
							</tr>
						</table>
					</td>
                </tr>
				<tr class="table_body">
                    <td class="midCol">Laser</td>
                    <td class="thirds">
						<table>
							<tr>
								<td><asp:CheckBox ID="CheckBoxOutcomeAlterLaser" runat="server" GroupName="Laser" Text="" /></td>
								<td class="rightCol"><asp:CheckBox ID="CheckBoxOutcomeAlterLaserP" runat="server" GroupName="Laser" Text="" /></td>
							</tr>
						</table>
					</td>
                </tr>
				<tr class="table_body">
                    <td class="midCol">Thermocautery</td>
                    <td class="thirds">
						<table>
							<tr>
								<td><asp:CheckBox ID="CheckBoxOutcomeAlterThermocautery" runat="server" GroupName="Thermocautery" Text="" /></td>
								<td class="rightCol"><asp:CheckBox ID="CheckBoxOutcomeAlterThermocauteryP" runat="server" GroupName="Thermocautery" Text="" /></td>
							</tr>
						</table>
					</td>
                </tr>
				<tr class="table_body">
                    <td class="midCol">Surgery </td>
                    <td class="thirds">
						<table>
							<tr>
								<td><asp:CheckBox ID="CheckBoxOutcomeAlterSurgery" runat="server" GroupName="Surgery" Text="" /></td>
								<td class="rightCol"><asp:CheckBox ID="CheckBoxOutcomeAlterSurgeryP" runat="server" GroupName="Surgery" Text="" /></td>
							</tr>
						</table>
					</td>
                </tr>
				<tr class="table_body">
                    <td class="midCol">Meibomian gland procedure (e.g. Maskin probing, Lipiflow)</td>
                    <td class="thirds">
						<table>
							<tr>
								<td><asp:CheckBox ID="CheckBoxOutcomeAlterGlandProcedure" runat="server" GroupName="GlandProcedure" Text="" /></td>
								<td class="rightCol"><asp:CheckBox ID="CheckBoxOutcomeAlterGlandProcedureP" runat="server" GroupName="GlandProcedure" Text="" /></td>
							</tr>
						</table>
					</td>
                </tr>
				<tr class="table_body">
                    <td class="midCol">Conjunctival Surgery (e.g. cautery for SLK, excision of conjunctivochalasis)</td>
                    <td class="thirds">
						<table style="height:4.5em;">
							<tr>
								<td><asp:CheckBox ID="CheckBoxOutcomeAlterConjunctivalSurgery" runat="server" GroupName="ConjunctivalSurgery" Text="" /></td>
								<td class="rightCol"><asp:CheckBox ID="CheckBoxOutcomeAlterConjunctivalSurgeryP" runat="server" GroupName="ConjunctivalSurgery" Text="" /></td>
							</tr>
						</table>
					</td>
                </tr>
				<tr class="table_body">
                    <td class="midCol">Eyelid surgery (e.g. ectropion repair, tarsorrhaphy</td>
                    <td class="thirds">
						<table>
							<tr>
								<td><asp:CheckBox ID="CheckBoxOutcomeAlterEyelidSurgery" runat="server" GroupName="EyelidSurgery" Text="" /></td>
								<td class="rightCol"><asp:CheckBox ID="CheckBoxOutcomeAlterEyelidSurgeryP" runat="server" GroupName="EyelidSurgery" Text="" /></td>
							</tr>
						</table>
					</td>
                </tr>
				<tr class="table_body">
                    <td class="midCol">Other&nbsp;<asp:TextBox runat="server" ID="TextBoxOutcomeAlterProcIntervOtherText" size="20" /></td>
                    <td class="thirds">
						<table>
							<tr>
								<td style="height:2.1em"><asp:CheckBox ID="CheckBoxOutcomeAlterProcIntervOther" runat="server" GroupName="SurgeryOther" Text="" /></td>
								<td class="rightCol"><asp:CheckBox ID="CheckBoxOutcomeAlterProcIntervOtherP" runat="server" GroupName="SurgeryOther" Text="" /></td>
							</tr>
						</table>
					</td>
                </tr>
				<!-- Question 2 - Other Adjunctive Therapy -->
                <tr class="table_body">
                    <td rowspan="9">Other Adjunctive Therapy</td>
                    <td class="midCol"></td>
					<td class="thirds">
						<table>
							<tr>
								<td>Discussed / Recommended</td>
								<td class="rightCol">Prescribed</td>
							</tr>
						</table>
					</td>
                </tr>
                <tr class="table_body">
                    <td class="midCol">Eyelid hygiene</td>
					<td class="thirds">
						<table>
							<tr>
								<td><asp:CheckBox ID="CheckBoxOutcomeAlterEyelidHygiene" runat="server" GroupName="EyelidHygiene" Text="" /></td>
								<td class="rightCol"></td>
							</tr>
						</table>
					</td>
                </tr>
                <tr class="table_body">
                    <td class="midCol">Environmental modifications (i.e. humidifier)</td>
                    <td class="thirds">
						<table>
							<tr>
								<td><asp:CheckBox ID="CheckBoxOutcomeAlterEnvironmentMod" runat="server" GroupName="EnvironmentMod" Text="" /></td>
								<td class="rightCol"></td>
							</tr>
						</table>
					</td>
                </tr>
				<tr class="table_body">
                    <td colspan="1">Contact lenses</td>
					<td class="thirds">
						<table>
							<tr>
								<td style="height:2.1em;"></td>
								<td class="rightCol"></td>
							</tr>
						</table>
					</td>
                </tr>
				<tr class="table_body">
                    <td class="midCol">Soft</td>
                    <td class="thirds">
						<table>
							<tr>
								<td><asp:CheckBox ID="CheckBoxOutcomeAlterContactLensSoft" runat="server" GroupName="ContactLensSoft" Text="" /></td>
								<td class="rightCol"><asp:CheckBox ID="CheckBoxOutcomeAlterContactLensSoftP" runat="server" GroupName="ContactLensSoft" Text="" /></td>
							</tr>
						</table>
					</td>
                </tr>
				<tr class="table_body">
                    <td class="midCol">Scleral</td>
                    <td class="thirds">
						<table>
							<tr>
								<td><asp:CheckBox ID="CheckBoxOutcomeAlterContactLensScleral" runat="server" GroupName="ContactLensScleral" Text="" /></td>
								<td class="rightCol"><asp:CheckBox ID="CheckBoxOutcomeAlterContactLensScleralP" runat="server" GroupName="ContactLensScleral" Text="" /></td>
							</tr>
						</table>
					</td>
                </tr>
				<tr class="table_body">
                    <td class="midCol">Other</td>
                    <td class="thirds">
						<table>
							<tr>
								<td style="height:2.1em"><asp:CheckBox ID="CheckBoxOutcomeAlterContactLensOther" runat="server" GroupName="ContactLensOther" Text="" /></td>
								<td class="rightCol"><asp:CheckBox ID="CheckBoxOutcomeAlterContactLensOtherP" runat="server" GroupName="ContactLensOther" Text="" /></td>
							</tr>
						</table>
					</td>
                </tr>
				<tr class="table_body">
                    <td class="midCol">Moisture chamber glasses, protective eyewear, sleep mask</td>
                    <td class="thirds">
						<table>
							<tr>
								<td><asp:CheckBox ID="CheckBoxOutcomeAlterMoistureChamber" runat="server" GroupName="MoistureChamber" Text="" /></td>
								<td class="rightCol"></td>
							</tr>
						</table>
					</td>
                </tr>
				<tr class="table_body">
                    <td class="midCol">Other <asp:TextBox runat="server" ID="TextBoxOutcomeAlterOtherText" size="20" /></td>
                    <td class="thirds">
						<table style="height:3.5em;">
							<tr>
								<td><asp:CheckBox ID="CheckBoxOutcomeAlterOther" runat="server" GroupName="AdjunctiveTherapyOther" Text="" /></td>
								<td class="rightCol"><asp:CheckBox ID="CheckBoxOutcomeAlterOtherP" runat="server" GroupName="AdjunctiveTherapyOther" Text="" /></td>
							</tr>
						</table>
					</td>
                </tr>
                <!-- Question 2 - Not Documented -->
                <tr class="table_body">
                    <td colspan="2">No documentation of therapeutic options prescribed or discussed</td>
					<td style="text-align:center;"><asp:Checkbox ID="CheckBoxAlterNotDocumented" runat="server" GroupName="TherapyNotDoc" /></td>
                </tr>

				<!-- Question 3 -->
                <tr class="table_body">
                    <td colspan="2">3. If punctal occlusion / obliteration was performed, what was the outcome?</td>
                    <td>
						<asp:RadioButtonList ID="RadioButtonListOutcomePunctal" RepeatDirection="Vertical" CssClass="aspxList" runat="server">
							<asp:ListItem Value="300">N/A. Not performed</asp:ListItem>
							<asp:ListItem Value="359">Performed, symptoms and/or signs not improved</asp:ListItem>
							<asp:ListItem Value="360">Performed, symptoms and/or signs improved</asp:ListItem>
							<asp:ListItem Value="361">Performed, symptoms and/or signs not documented</asp:ListItem>
							<asp:ListItem Value="362">Epiphora</asp:ListItem>
						</asp:RadioButtonList>
						<br /><strong>If performed, symptoms and/or signs improved:</strong>
						<div id="OutcomesQ3IfSo" style="padding-left:20px;">
							<asp:Checkbox ID="CheckBoxOutcomePunctalStillOccluded" runat="server" Text="&nbsp;punctum still occluded at last Follow-up." />
						</div>
						<br /><strong>If not:</strong>
						<div id="OutcomesQ3IfNot" style="padding-left:20px;">
							<asp:RadioButtonList ID="RadioButtonListOutcomePunctalFollowup" RepeatDirection="Vertical" CssClass="aspxList" runat="server">
								<asp:ListItem Value="363">Re-occlusion offered but patient refused</asp:ListItem>
								<asp:ListItem Value="364">Re-occlusion performed</asp:ListItem>
								<asp:ListItem Value="365">Re-occlusion not offered</asp:ListItem>
							</asp:RadioButtonList>
						</div>
					</td>
                </tr>
                <!-- Question 4 -->
                <tr class="table_body_bg">
                    <td colspan="2">4. Did treatment improve the patientís symptoms?</td>
                    <td>
						<asp:RadioButtonList ID="RadioButtonListOutcomeSymptomsImproved" RepeatDirection="Vertical" CssClass="aspxList" runat="server">
							<asp:ListItem Value="1">Yes</asp:ListItem>
							<asp:ListItem Value="2">No</asp:ListItem>
							<asp:ListItem Value="3">Not Documented</asp:ListItem>
						</asp:RadioButtonList>
					</td>
                </tr>
				<!-- Question 5 -->
                <tr class="table_body">
                    <td colspan="2">5. Did treatment improve the ocular findings?</td>
                    <td>
						<asp:RadioButtonList ID="RadioButtonListOutcomeFindingsImproved" RepeatDirection="Vertical" CssClass="aspxList" runat="server">
							<asp:ListItem Value="1">Yes</asp:ListItem>
							<asp:ListItem Value="2">No</asp:ListItem>
							<asp:ListItem Value="3">Not Documented</asp:ListItem>
						</asp:RadioButtonList>
					</td>
                </tr>
                <!-- Question 6 -->
                <tr class="table_body_bg">
                    <td colspan="2">6. If yes above, which ocular findings improved? (check all that apply)</td>
                    <td>
						<asp:Checkbox ID="CheckBoxOutcomeFindingsImprovedEyelid" runat="server" Text="&nbsp;Eyelid position" />
						<br /><asp:Checkbox ID="CheckBoxOutcomeFindingsImprovedBlink" runat="server" Text="&nbsp;Blink" />
						<br /><asp:Checkbox ID="CheckBoxOutcomeFindingsImprovedLashed" runat="server" Text="&nbsp;Lashes" />
						<br /><asp:Checkbox ID="CheckBoxOutcomeFindingsImprovedGlands" runat="server" Text="&nbsp;Meibomian glands" />
						<br /><asp:Checkbox ID="CheckBoxOutcomeFindingsImprovedTearMeniscus" runat="server" Text="&nbsp;Tear meniscus height" />
						<br /><asp:Checkbox ID="CheckBoxOutcomeFindingsImprovedTearTime" runat="server" Text="&nbsp;Tear breakup time" />
						<br /><asp:Checkbox ID="CheckBoxOutcomeFindingsImprovedOcularSurface" runat="server" Text="&nbsp;Ocular surface staining" />
						<br /><asp:Checkbox ID="CheckBoxOutcomeFindingsImprovedSchirmer" runat="server" Text="&nbsp;Schirmer or tear secretion test" />
						<br /><asp:Checkbox ID="CheckBoxOutcomeFindingsImprovedTearOsmolarity" runat="server" Text="&nbsp;Tear osmolarity" />
						<br /><asp:Checkbox ID="CheckBoxOutcomeFindingsImprovedConjunctival" runat="server" Text="&nbsp;Conjunctival findings" />
						<br /><asp:Checkbox ID="CheckBoxOutcomeFindingsImprovedCorneal" runat="server" Text="&nbsp;Corneal findings" />
						<br /><asp:Checkbox ID="CheckBoxOutcomeFindingsImprovedOther" runat="server" Text="&nbsp;Other" />&nbsp;<asp:TextBox runat="server" ID="TextBoxOutcomeOtherText" size="20" />
					</td>
                </tr>
			</table>
            <div class="button-box">
                <asp:LinkButton ID="LinkButtonBackToDashboard" runat="server" Text="Back To Chart Registration" PostBackUrl="../PatientChartRegistration.aspx?CycleNumber=1" Visible="false" CssClass="button" />
                <asp:ImageButton ID="ButtonSubmit" OnClick="ButtonSubmit_Click" runat="server" AlternateText="Submit Chart" CssClass="button" />
            </div>
        </div>
        <!-- Esotropia pim ends -->
</asp:Content>

<asp:Content runat="server" ID="Content4" ContentPlaceHolderID="javascript">

    <script type="text/javascript" src="../../common/js/jquery.atooltip.js"></script>
    <script type="text/javascript" src="../../common/js/jquery.metadata.js"></script> <!--when changing defaults-->
    <script type="text/javascript" src="../../common/js/autoNumeric-1.7.5.js"></script>
    <script type="text/javascript" language="javascript">
        // Validates the DOB


        // Checks that the date has happened
        function validateDate(month, year) {
            var today = new Date();
            var monthVar = month - 1;
            var checkYear = today.getFullYear() - year;
            var checkMonth = today.getMonth() - monthVar;
            if (checkMonth < 0) {
                checkYear--;
            }
            if (checkYear < 0) {
                alert("Invalid date, you must pick a date that has already occurred.");
                return false;
            }
            else {
                return true;
            }
        }

        //      Either enables or disables input boxes
        function enabledControl(el, disabled, checked) {
            //alert("Hello world from enabled control");
            try {
                if (disabled) {
                    el.disabled = disabled;
                    el.setAttribute("disabled", true);
                }
                else {
                    el.disabled = disabled;
                    el.removeAttribute("disabled");
                }

                if (checked == true)
                    el.checked = false;
            }
            catch (E) {
            }
            if (el.childNodes && el.childNodes.length > 0) {
                for (var x = 0; x < el.childNodes.length; x++) {
                    enabledControl(el.childNodes[x], disabled, checked);
                }
            }
        }
        //      Either enables or disables dropdown boxes
        function enabledControlDropDown(el, disabled, checked) {
            //alert("Hello world from enabled control");
            try {
                if (disabled) {
                    el.disabled = disabled;
                    el.setAttribute("disabled", true);
                }
                else {
                    el.disabled = disabled;
                    el.removeAttribute("disabled");
                }

                if (checked == true)
                    el.options[0].selected = true;
            }
            catch (E) {
            }
            if (el.childNodes && el.childNodes.length > 0) {
                for (var x = 0; x < el.childNodes.length; x++) {
                    enabledControl(el.childNodes[x], disabled, checked);
                }
            }
        }
        //      Either enables or disables text boxes
        function enabledControlTextField(el, disabled, checked) {
            //alert("Hello world from enabled control");
            try {
                if (disabled) {
                    el.disabled = disabled;
                    el.setAttribute("disabled", true);
                }
                else {
                    el.disabled = disabled;
                    el.removeAttribute("disabled");
                }

                if (checked == true) {
                    el.value = '';
                }
            }
            catch (E) {
            }
            if (el.childNodes && el.childNodes.length > 0) {
                for (var x = 0; x < el.childNodes.length; x++) {
                    enabledControl(el.childNodes[x], disabled, checked);
                }
            }
        }
        // Validations

        function generate(arr1, arr2, istrue) {
            if (istrue == true) {
                for (var i = 0; i < arr1.length; i++) {
                    document.getElementById(arr1[i]).style.color = "gray";
                }
                for (var i = 0; i < arr2.length; i++) {
                    $(arr2[i] + ' :input').attr('disabled', true);
                    $(arr2[i] + ' :input').removeAttr("checked");
                }
            }
            if (istrue == false) {
                for (var i = 0; i < arr1.length; i++) {
                    document.getElementById(arr1[i]).style.color = "#333";
                }
                for (var i = 0; i < arr2.length; i++) {
                    $(arr2[i] + ' :input').removeAttr('disabled');
                }
            }
        }

        $(document).ready(function () {

            // If punctal occlusion / obliteration was performed, what was the outcome?
            $("#<%= RadioButtonListOutcomePunctal.ClientID %>").click(function () {
                if ($('#<%= RadioButtonListOutcomePunctal.ClientID %>').find('input:checked').val() == '300') {
                    var myarray = new Array("OutcomesQ3IfSo");
                    var myarray2 = new Array('#OutcomesQ3IfSo');
                    generate(myarray, myarray2, true);

                    var myarray3 = new Array("OutcomesQ3IfNot");
                    var myarray4 = new Array('#OutcomesQ3IfNot');
                    generate(myarray3, myarray4, false);
                }
                else {
                    var myarray = new Array("OutcomesQ3IfSo");
                    var myarray2 = new Array('#OutcomesQ3IfSo');
                    generate(myarray, myarray2, false);

                    var myarray3 = new Array("OutcomesQ3IfNot");
                    var myarray4 = new Array('#OutcomesQ3IfNot');
                    generate(myarray3, myarray4, true);
                }
            });
            if ($('#<%= RadioButtonListOutcomePunctal.ClientID %>').find('input:checked').val() == '300') {
                var myarray = new Array("OutcomesQ3IfSo");
                var myarray2 = new Array('#OutcomesQ3IfSo');
                generate(myarray, myarray2, true);

                var myarray3 = new Array("OutcomesQ3IfNot");
                var myarray4 = new Array('#OutcomesQ3IfNot');
                generate(myarray3, myarray4, false);
            }
            else {
                var myarray = new Array("OutcomesQ3IfSo");
                var myarray2 = new Array('#OutcomesQ3IfSo');
                generate(myarray, myarray2, false);

                var myarray3 = new Array("OutcomesQ3IfNot");
                var myarray4 = new Array('#OutcomesQ3IfNot');
                generate(myarray3, myarray4, true);
            }


            // CheckBoxPresentingExacOther
            $("#<%= CheckBoxPresentingExacOther.ClientID %>").click(function () {
                if ($('#<%= CheckBoxPresentingExacOther.ClientID %>').prop("checked") == true) {
                    enabledControlTextField(document.getElementById("<%= TextBoxPresentingExacOtherText.ClientID %>"), false, false);
			    }
			    else {
			        enabledControlTextField(document.getElementById("<%= TextBoxPresentingExacOtherText.ClientID %>"), true, true);
			    }
            });
            if ($('#<%= CheckBoxPresentingExacOther.ClientID %>').prop("checked") == true) {
                enabledControlTextField(document.getElementById("<%= TextBoxPresentingExacOtherText.ClientID %>"), false, false);
            }
            else {
                enabledControlTextField(document.getElementById("<%= TextBoxPresentingExacOtherText.ClientID %>"), true, true);
            }


            // CheckBoxPresentingDiseaseOther
            $("#<%= CheckBoxPresentingDiseaseOther.ClientID %>").click(function () {
                if ($('#<%= CheckBoxPresentingDiseaseOther.ClientID %>').prop("checked") == true) {
                    enabledControlTextField(document.getElementById("<%= TextBoxPresentingDiseaseOtherText.ClientID %>"), false, false);
                }
                else {
                    enabledControlTextField(document.getElementById("<%= TextBoxPresentingDiseaseOtherText.ClientID %>"), true, true);
                }
            });
            if ($('#<%= CheckBoxPresentingDiseaseOther.ClientID %>').prop("checked") == true) {
                enabledControlTextField(document.getElementById("<%= TextBoxPresentingDiseaseOtherText.ClientID %>"), false, false);
            }
            else {
                enabledControlTextField(document.getElementById("<%= TextBoxPresentingDiseaseOtherText.ClientID %>"), true, true);
            }


            // LASHES OTHER
            // Tear osmolarity test
            $("#<%= RadioButtonListExamLashes.ClientID %>").click(function () {
                var myaray = new Array("Q3OtherText");
                var myarray2 = new Array('#Q3OtherText');

                if ($('#<%= RadioButtonListExamLashes.ClientID %>').find('input:checked').val() == ('13')) {
                    generate(myaray, myarray2, false);
                    enabledControlTextField(document.getElementById("<%= TextBoxExamLashesText.ClientID %>"), false, false);
                }
                else {
                    generate(myaray, myarray2, true);
                    enabledControlTextField(document.getElementById("<%= TextBoxExamLashesText.ClientID %>"), true, true);
                }
            });
            var myaray = new Array("Q3OtherText");
            var myarray2 = new Array('#Q3OtherText');

            if ($('#<%= RadioButtonListExamLashes.ClientID %>').find('input:checked').val() == ('13')) {
                generate(myaray, myarray2, false);
                enabledControlTextField(document.getElementById("<%= TextBoxExamLashesText.ClientID %>"), false, false);
            }
            else {
                generate(myaray, myarray2, true);
                enabledControlTextField(document.getElementById("<%= TextBoxExamLashesText.ClientID %>"), true, true);
            }


            // CheckBoxExamCornealOther
            $("#<%= CheckBoxExamCornealOther.ClientID %>").click(function () {
                if ($('#<%= CheckBoxExamCornealOther.ClientID %>').prop("checked") == true) {
                    enabledControlTextField(document.getElementById("<%= TextBoxExamCornealOtherText.ClientID %>"), false, false);
                }
                else {
                    enabledControlTextField(document.getElementById("<%= TextBoxExamCornealOtherText.ClientID %>"), true, true);
                }
            });
            if ($('#<%= CheckBoxExamCornealOther.ClientID %>').prop("checked") == true) {
                enabledControlTextField(document.getElementById("<%= TextBoxExamCornealOtherText.ClientID %>"), false, false);
            }
            else {
                enabledControlTextField(document.getElementById("<%= TextBoxExamCornealOtherText.ClientID %>"), true, true);
            }


            // Conjunctival findings
            $("#<%= CheckBoxExamConjFindOther.ClientID %>").click(function () {
                //alert($('#<%= CheckBoxExamConjFindOther.ClientID %>').attr("checked"));
			    if ($('#<%= CheckBoxExamConjFindOther.ClientID %>').prop("checked") == true) {
			        enabledControlTextField(document.getElementById("<%= TextBoxExamConjFindOtherText.ClientID %>"), false, false);
				}
				else {
				    enabledControlTextField(document.getElementById("<%= TextBoxExamConjFindOtherText.ClientID %>"), true, true);
				}
			});
            if ($('#<%= CheckBoxExamConjFindOther.ClientID %>').prop("checked") == true) {
                enabledControlTextField(document.getElementById("<%= TextBoxExamConjFindOtherText.ClientID %>"), false, false);
			}
			else {
			    enabledControlTextField(document.getElementById("<%= TextBoxExamConjFindOtherText.ClientID %>"), true, true);
			}


            //
            $("#<%= CheckBoxOutcomeFindingsImprovedOther.ClientID %>").click(function () {
                if ($('#<%= CheckBoxOutcomeFindingsImprovedOther.ClientID %>').prop("checked") == true) {
			        enabledControlTextField(document.getElementById("<%= TextBoxOutcomeOtherText.ClientID %>"), false, false);
				}
				else {
				    enabledControlTextField(document.getElementById("<%= TextBoxOutcomeOtherText.ClientID %>"), true, true);
				}
			});
            if ($('#<%= CheckBoxOutcomeFindingsImprovedOther.ClientID %>').prop("checked") == true) {
                enabledControlTextField(document.getElementById("<%= TextBoxOutcomeOtherText.ClientID %>"), false, false);
			}
			else {
			    enabledControlTextField(document.getElementById("<%= TextBoxOutcomeOtherText.ClientID %>"), true, true);
			}


            $("#<%= CheckBoxOutcomeAlterOther.ClientID %>").click(function () {
                if (($('#<%= CheckBoxOutcomeAlterOther.ClientID %>').prop("checked") == true) || ($('#<%= CheckBoxOutcomeAlterOtherP.ClientID %>').prop("checked") == true)) {
			        enabledControlTextField(document.getElementById("<%= TextBoxOutcomeAlterOtherText.ClientID %>"), false, false);
			    }
			    else {
			        enabledControlTextField(document.getElementById("<%= TextBoxOutcomeAlterOtherText.ClientID %>"), true, true);
			    }
			});
            $("#<%= CheckBoxOutcomeAlterOtherP.ClientID %>").click(function () {
                if (($('#<%= CheckBoxOutcomeAlterOther.ClientID %>').prop("checked") == true) || ($('#<%= CheckBoxOutcomeAlterOtherP.ClientID %>').prop("checked") == true)) {
                    enabledControlTextField(document.getElementById("<%= TextBoxOutcomeAlterOtherText.ClientID %>"), false, false);
                }
                else {
                    enabledControlTextField(document.getElementById("<%= TextBoxOutcomeAlterOtherText.ClientID %>"), true, true);
                }
            });
            if (($('#<%= CheckBoxOutcomeAlterOther.ClientID %>').prop("checked") == true) || ($('#<%= CheckBoxOutcomeAlterOtherP.ClientID %>').prop("checked") == true)) {
                enabledControlTextField(document.getElementById("<%= TextBoxOutcomeAlterOtherText.ClientID %>"), false, false);
            }
            else {
                enabledControlTextField(document.getElementById("<%= TextBoxOutcomeAlterOtherText.ClientID %>"), true, true);
            }



            $("#<%= CheckBoxDiagnosisOther.ClientID %>").click(function () {
                if ($('#<%= CheckBoxDiagnosisOther.ClientID %>').prop("checked") == true) {
			        enabledControlTextField(document.getElementById("<%= TextBoxDiagnosisOtherText.ClientID %>"), false, false);
				}
				else {
				    enabledControlTextField(document.getElementById("<%= TextBoxDiagnosisOtherText.ClientID %>"), true, true);
				}
			});

            //CheckBoxOutcomeAlterTopicalTherapyOther
            $("#<%= CheckBoxOutcomeAlterTopicalTherapyOther.ClientID %>").click(function () {
                if (($('#<%= CheckBoxOutcomeAlterTopicalTherapyOther.ClientID %>').prop("checked") == true) || ($('#<%= CheckBoxOutcomeAlterTopicalTherapyOtherP.ClientID %>').prop("checked") == true)) {
                    enabledControlTextField(document.getElementById("<%= TextBoxOutcomeAlterTopicalTherapyOtherText.ClientID %>"), false, false);
                }
                else {
                    enabledControlTextField(document.getElementById("<%= TextBoxOutcomeAlterTopicalTherapyOtherText.ClientID %>"), true, true);
                }
            });
            $("#<%= CheckBoxOutcomeAlterTopicalTherapyOtherP.ClientID %>").click(function () {
                if (($('#<%= CheckBoxOutcomeAlterTopicalTherapyOther.ClientID %>').prop("checked") == true) || ($('#<%= CheckBoxOutcomeAlterTopicalTherapyOtherP.ClientID %>').prop("checked") == true)) {
                    enabledControlTextField(document.getElementById("<%= TextBoxOutcomeAlterTopicalTherapyOtherText.ClientID %>"), false, false);
                }
                else {
                    enabledControlTextField(document.getElementById("<%= TextBoxOutcomeAlterTopicalTherapyOtherText.ClientID %>"), true, true);
                }
            });
            if (($('#<%= CheckBoxOutcomeAlterTopicalTherapyOther.ClientID %>').prop("checked") == true) || ($('#<%= CheckBoxOutcomeAlterTopicalTherapyOtherP.ClientID %>').prop("checked") == true)) {
                enabledControlTextField(document.getElementById("<%= TextBoxOutcomeAlterTopicalTherapyOtherText.ClientID %>"), false, false);
            }
            else {
                enabledControlTextField(document.getElementById("<%= TextBoxOutcomeAlterTopicalTherapyOtherText.ClientID %>"), true, true);
            }

            //CheckBoxOutcomeAlterOralOther
            $("#<%= CheckBoxOutcomeAlterOralOther.ClientID %>").click(function () {
                if (($('#<%= CheckBoxOutcomeAlterOralOther.ClientID %>').prop("checked") == true) || ($('#<%= CheckBoxOutcomeAlterOralOtherP.ClientID %>').prop("checked") == true)) {
                    enabledControlTextField(document.getElementById("<%= TextBoxOutcomeAlterOralOtherText.ClientID %>"), false, false);
                }
                else {
                    enabledControlTextField(document.getElementById("<%= TextBoxOutcomeAlterOralOtherText.ClientID %>"), true, true);
                }
            });
            //CheckBoxOutcomeAlterOralOther
            $("#<%= CheckBoxOutcomeAlterOralOtherP.ClientID %>").click(function () {
                if (($('#<%= CheckBoxOutcomeAlterOralOther.ClientID %>').prop("checked") == true) || ($('#<%= CheckBoxOutcomeAlterOralOtherP.ClientID %>').prop("checked") == true)) {
                    enabledControlTextField(document.getElementById("<%= TextBoxOutcomeAlterOralOtherText.ClientID %>"), false, false);
                }
                else {
                    enabledControlTextField(document.getElementById("<%= TextBoxOutcomeAlterOralOtherText.ClientID %>"), true, true);
                }
            });
            if (($('#<%= CheckBoxOutcomeAlterOralOther.ClientID %>').prop("checked") == true) || ($('#<%= CheckBoxOutcomeAlterOralOtherP.ClientID %>').prop("checked") == true)) {
                enabledControlTextField(document.getElementById("<%= TextBoxOutcomeAlterOralOtherText.ClientID %>"), false, false);
            }
            else {
                enabledControlTextField(document.getElementById("<%= TextBoxOutcomeAlterOralOtherText.ClientID %>"), true, true);
            }


            //CheckBoxOutcomeAlterOralOther
            $("#<%= CheckBoxOutcomeAlterProcIntervOther.ClientID %>").click(function () {
                if (($('#<%= CheckBoxOutcomeAlterProcIntervOther.ClientID %>').prop("checked") == true) || ($('#<%= CheckBoxOutcomeAlterProcIntervOtherP.ClientID %>').prop("checked") == true)) {
                    enabledControlTextField(document.getElementById("<%= TextBoxOutcomeAlterProcIntervOtherText.ClientID %>"), false, false);
                }
                else {
                    enabledControlTextField(document.getElementById("<%= TextBoxOutcomeAlterProcIntervOtherText.ClientID %>"), true, true);
                }
            });
            $("#<%= CheckBoxOutcomeAlterProcIntervOtherP.ClientID %>").click(function () {
                if (($('#<%= CheckBoxOutcomeAlterProcIntervOther.ClientID %>').prop("checked") == true) || ($('#<%= CheckBoxOutcomeAlterProcIntervOtherP.ClientID %>').prop("checked") == true)) {
                    enabledControlTextField(document.getElementById("<%= TextBoxOutcomeAlterProcIntervOtherText.ClientID %>"), false, false);
                }
                else {
                    enabledControlTextField(document.getElementById("<%= TextBoxOutcomeAlterProcIntervOtherText.ClientID %>"), true, true);
                }
            });
            if (($('#<%= CheckBoxOutcomeAlterProcIntervOther.ClientID %>').prop("checked") == true) || ($('#<%= CheckBoxOutcomeAlterProcIntervOtherP.ClientID %>').prop("checked") == true)) {
                enabledControlTextField(document.getElementById("<%= TextBoxOutcomeAlterProcIntervOtherText.ClientID %>"), false, false);
            }
            else {
                enabledControlTextField(document.getElementById("<%= TextBoxOutcomeAlterProcIntervOtherText.ClientID %>"), true, true);
            }


            if ($('#<%= CheckBoxDiagnosisOther.ClientID %>').prop("checked") == true) {
                enabledControlTextField(document.getElementById("<%= TextBoxDiagnosisOtherText.ClientID %>"), false, false);
			}
			else {
			    enabledControlTextField(document.getElementById("<%= TextBoxDiagnosisOtherText.ClientID %>"), true, true);
			}

            function adjunctiveOther() {
                if (($('#<%= RadioButtonPrescribeAdjunctiveTherapyOther.ClientID %>').prop("checked") == true) || ($('#<%= RadioButtonDiscussAdjunctiveTherapyOther.ClientID %>').prop("checked") == true)) {
			        enabledControlTextField(document.getElementById("<%= TextBoxDiscussAdjunctiveTherapyOtherText.ClientID %>"), false, false);
				}
				else {
				    enabledControlTextField(document.getElementById("<%= TextBoxDiscussAdjunctiveTherapyOtherText.ClientID %>"), true, true);
				}
            }

            adjunctiveOther();

            $("#<%= RadioButtonPrescribeAdjunctiveTherapyOther.ClientID %>").click(function () {
			    adjunctiveOther();
			});

			$("#<%= RadioButtonDiscussAdjunctiveTherapyOther.ClientID %>").click(function () {
                adjunctiveOther();
            });

            // Topical Therapy
            function topicalOther() {
                if (($('#<%= RadioButtonTopicalTherapyOther.ClientID %>').prop("checked") == true) || ($('#<%= RadioButtonTopicalTherapyOtherP.ClientID %>').prop("checked") == true)) {
                    enabledControlTextField(document.getElementById("<%= TextBoxTopicalTherapyOtherText.ClientID %>"), false, false);
                }
                else {
                    enabledControlTextField(document.getElementById("<%= TextBoxTopicalTherapyOtherText.ClientID %>"), true, true);
                }
            }

            topicalOther();

            $("#<%= RadioButtonTopicalTherapyOther.ClientID %>").click(function () {
                topicalOther();
            });

            $("#<%= RadioButtonTopicalTherapyOtherP.ClientID %>").click(function () {
                topicalOther();
            });



            // Oral Therapy
            function oralOther() {
                if (($('#<%= RadioButtonOralTherapyOther.ClientID %>').prop("checked") == true) || ($('#<%= RadioButtonOralTherapyOtherP.ClientID %>').prop("checked") == true)) {
                    enabledControlTextField(document.getElementById("<%= TextBoxOralTherapyOtherText.ClientID %>"), false, false);
                }
                else {
                    enabledControlTextField(document.getElementById("<%= TextBoxOralTherapyOtherText.ClientID %>"), true, true);
                }
            }

            oralOther();

            $("#<%= RadioButtonOralTherapyOther.ClientID %>").click(function () {
                oralOther();
            });

            $("#<%= RadioButtonOralTherapyOtherP.ClientID %>").click(function () {
                oralOther();
            });




            // proceduralOther
            function proceduralOther() {
                if (($('#<%= RadioButtonSurgeryOther.ClientID %>').prop("checked") == true) || ($('#<%= RadioButtonSurgeryOtherP.ClientID %>').prop("checked") == true)) {
                    enabledControlTextField(document.getElementById("<%= TextBoxSurgeryOtherText.ClientID %>"), false, false);
                }
                else {
                    enabledControlTextField(document.getElementById("<%= TextBoxSurgeryOtherText.ClientID %>"), true, true);
                }
            }

            proceduralOther();

            $("#<%= RadioButtonSurgeryOther.ClientID %>").click(function () {
                proceduralOther();
            });

            $("#<%= RadioButtonSurgeryOtherP.ClientID %>").click(function () {
                proceduralOther();
            });

            /***** Alternating checkbox and textbox *****/
            $("#<%= TextBoxExamTearOsmolarityOsmolesNbr.ClientID %>").keyup(function () {
                enabledControl(document.getElementById("<%= CheckBoxExamTearOsmolarity.ClientID %>"), false, true);
            });
            $("#<%= CheckBoxExamTearOsmolarity.ClientID %>").click(function () {
                enabledControlTextField(document.getElementById("<%= TextBoxExamTearOsmolarityOsmolesNbr.ClientID %>"), false, true);
            });
        });



    </script>

    <script type="text/javascript">
        // Tip section

        $(function () {
            $('#QH7').aToolTip({
                clickIt: true,
                tipContent: '&lt;&nbsp;32 weeks.'
            });
        });
    </script>
	
    <script type="text/javascript" language="javascript">
        $(document).ready(function () {
            /** instruct the metadata plugin where to look the metadata
            * jQuery.metadata.setType( type, name );
            * please read the metadata instructions for additional information
            * http://plugins.jquery.com/project/metadata
            */
            $.metadata.setType('attr', 'meta');

            /** To call autoNumeric
            * $(selector).autoNumeric({options}); 
            * The below example uses the input & class selector
            */
            $('input.auto').autoNumeric();
            /* scripts for metadata code generator  */

            /* rountine that prevents  numeric characters from being entered the the altDec field  */
            $('#altDecb').keypress(function (e) {
                var cc = String.fromCharCode(e.which);
                if (e.which != 32 && cc >= 0 && cc <= 9) {
                    e.preventDefault();
                }
            });

            /* rountine that prevents  apostrophe, comma, more than one period (full stop) or numeric characters from being entered the the aSign field  */
            $('#aSignb').keypress(function (e) {
                var cc = String.fromCharCode(e.which);
                if ((e.which != 32 && cc >= 0 && cc <= 9) || cc == "," || cc == "'" || cc == "." && this.value.lastIndexOf('.') != -1) {
                    e.preventDefault();
                }
            });

            $("input.md").bind('click keyup blur', function () {
                var metaCode = '', aSep = '', dGroup = '', aDec = '', altDec = '', aSign = '', pSign = '', vMin = '', vMax = '', mDec = '', mRound = '', aPad = '', wEmpty = '', aForm = '';
                if ($("input:radio[name=aSep]:checked").attr('id') == 'aSepc') {
                    $('input:radio[name=aDec]:nth(0)').removeAttr("disabled");
                    $('input:radio[name=aDec]:nth(0)').attr('checked', true);
                    $('input:radio[name=aDec]:nth(1)').attr("disabled", true);
                }
                if ($("input:radio[name=aSep]:checked").attr('id') == 'aSepp') {
                    $('input:radio[name=aDec]:nth(1)').removeAttr("disabled");
                    $('input:radio[name=aDec]:nth(1)').attr('checked', true);
                    $('input:radio[name=aDec]:nth(0)').attr("disabled", true);
                }
                if ($("input:radio[name=aSep]:checked").attr('id') != 'aSepc' || $("input:radio[name=aSep]:checked").attr('id') != 'aSepp') {
                    $('input:radio[name=aDec]:nth(0)').removeAttr("disabled");
                    $('input:radio[name=aDec]:nth(1)').removeAttr("disabled");
                }
                aSep = $("input:radio[name=aSep]:checked").val();
                dGroup = $("input:radio[name=dGroup]:checked").val();
                aDec = $("input:radio[name=aDec]:checked").val();

                if ($("input:radio[name=altDec]:checked").attr('id') == 'altDecd') {
                    $('#altDecb').val('');
                    $('#altDecb').attr("disabled", true);
                }
                if ($("input:radio[name=altDec]:checked").attr('id') == 'altDeca') {
                    $('#altDecb').removeAttr("disabled");
                    altDec = $('#altDecb').val();
                }

                if ($("input:radio[name=aSign]:checked").attr('id') == 'aSignd') {
                    $('#aSignb').val('');
                    $('#aSignb').attr("disabled", true);
                }
                if ($("input:radio[name=aSign]:checked").attr('id') == 'aSigna') {
                    $('#aSignb').removeAttr("disabled");
                    aSign = $('#aSignb').val();
                }

                pSign = $("input:radio[name=pSign]:checked").val();
                if ($("input:radio[name=vMin]:checked").attr('id') == 'vMind') {
                    $('#vMinb').val('');
                    $('#vMinb').attr("disabled", true);
                }
                if ($("input:radio[name=vMin]:checked").attr('id') == 'vMina') {
                    $('#vMinb').removeAttr("disabled");
                    vMin = $('#vMinb').val();
                }
                if ($("input:radio[name=vMax]:checked").attr('id') == 'vMaxd') {
                    $('#vMaxb').val('');
                    $('#vMaxb').attr("disabled", true);
                }
                if ($("input:radio[name=vMax]:checked").attr('id') == 'vMaxa') {
                    $('#vMaxb').removeAttr("disabled");
                    vMax = $('#vMaxb').val();
                }
                if ($("input:radio[name=mDec]:checked").attr('id') == 'mDecd') {
                    $('#mDecbb').val('');
                    $('#mDecbb').attr("disabled", true);
                }
                if ($("input:radio[name=mDec]:checked").attr('id') == 'mDeca') {
                    $('#mDecbb').removeAttr("disabled");
                    mDec = $('#mDecbb').val();
                }
                mRound = $("input:radio[name=mRound]:checked").val();
                aPad = $("input:radio[name=aPad]:checked").val();
                wEmpty = $("input:radio[name=wEmpty]:checked").val();
                if (aSep != '') {
                    metaCode = aSep;
                }
                if (dGroup != '') {
                    if (metaCode != '') {
                        metaCode = metaCode + ", " + dGroup;
                    }
                    else {
                        metaCode = dGroup;
                    }
                }
                if (aDec != '') {
                    if (metaCode != '') {
                        metaCode = metaCode + ", " + aDec;
                    }
                    else {
                        metaCode = aDec;
                    }
                }
                if (altDec != '') {
                    if (metaCode != '') {
                        metaCode = metaCode + ", altDec: '" + altDec + "'";
                    }
                    else {
                        metaCode = "altDec: '" + altDec + "'";
                    }
                }
                if (aSign != '') {
                    if (metaCode != '') {
                        metaCode = metaCode + ", aSign: '" + aSign + "'";
                    }
                    else {
                        metaCode = "aSign: '" + aSign + "'";
                    }
                }
                if (pSign != '') {
                    if (metaCode != '') {
                        metaCode = metaCode + ", " + pSign;
                    }
                    else {
                        metaCode = pSign;
                    }
                }
                if (vMin != '') {
                    if (metaCode != '') {
                        metaCode = metaCode + ", vMin: '" + vMin + "'";
                    }
                    else {
                        metaCode = "vMin: '" + vMin + "'";
                    }
                }
                if (vMax != '') {
                    if (metaCode != '') {
                        metaCode = metaCode + ", vMax: '" + vMax + "'";
                    }
                    else {
                        metaCode = "vMax: '" + vMax + "'";
                    }
                }
                if (mDec != '') {
                    if (metaCode != '') {
                        metaCode = metaCode + ", mDec: '" + $('#mDecbb').val() + "'";
                    }
                    else {
                        metaCode = "mDec: '" + $('#mDecbb').val() + "'";
                    }
                }

                if (mRound != '') {
                    if (metaCode != '') {
                        metaCode = metaCode + ", " + mRound;
                    }
                    else {
                        metaCode = mRound;
                    }
                }
                if (aPad != '') {
                    if (metaCode != '') {
                        metaCode = metaCode + ", " + aPad;
                    }
                    else {
                        metaCode = aPad;
                    }
                }
                if (wEmpty != '') {
                    if (metaCode != '') {
                        metaCode = metaCode + ", " + wEmpty;
                    }
                    else {
                        metaCode = wEmpty;
                    }
                }
                $('#metaCode').text('');
                if (metaCode != '') {
                    $('#metaCode').text('meta="{' + metaCode + '}"');
                }
            });

            /* clears the metadata code  */
            $('#rd').click(function () {
                $('#metaCode').text('');
            });
            /* ends scripts for metadata code generator  */

            /* script  for defaults demo  */
            $('#d_noMeta').blur(function () {
                var convertInput = '';
                convertInput = $(this).autoNumericGet();
                $('#d_Get').val(convertInput);
                $('#d_Set').autoNumericSet(convertInput);
            });
            /* end script  for defaults demo  */

            /* script  for various samples demo  */
            $('input[name$="sample"]').blur(function () {
                var convertInput = '';
                var row = 'row_' + this.id.charAt(4);
                convertInput = $(this).autoNumericGet();
                $('#' + row + 'b').val(convertInput);
                $('#' + row + 'c').autoNumericSet(convertInput);
            });
            /* end script  for various samples demo  */

            /* script  for rounding methods  */
            $('#roundValue').blur(function () {
                if (this.value != '') {
                    convertInput = $('#roundValue').autoNumericGet();
                    var i = 1;
                    for (i = 1; i <= 9; i++) {
                        $('#roundMethod' + i).autoNumericSet(convertInput);
                    }
                }
            });

            $('#roundDecimal').change(function () { /* changes decimal places */
                convertInput = $('#roundValue').autoNumericGet();
                if (convertInput > 0) {
                    var i = 1;
                    for (i = 1; i <= 9; i++) {
                        $('#roundMethod' + i).autoNumericSet(convertInput);
                    }
                }
            });
            /* end script  for rounding methods  */

            /* script for dynamically loaded values  demo*/
            $.getJSON("test_JSON.php", function (data) {
                var valueFormatted = '';
                $.each(data, function (key, value) { // loops through JSON keys and returns value	
                    $('#' + key).autoNumericSet(value);
                });
            });
            /* end script for dynamically loaded values demo*/

            /* script for callback demo*/
            $.autoNumeric.get_mDec = function () { /* get_mDec function attached to autoNumeric() */
                var set_mDec = $('#get_metricUnit').val();
                if (set_mDec == ' km') {
                    set_mDec = 3;
                } else {
                    set_mDec = 0;
                }
                return set_mDec; /* set mDec decimal places */
            }

            var get_vMax = function () { /* set the maximum value allowed based on the metric unit */
                var set_vMax = $('#get_metricUnit').val();
                if (set_vMax == ' km') {
                    set_vMax = '99999.999';
                } else {
                    set_vMax = '99999999';
                }
                return set_vMax;
            }

            $('#length').autoNumeric({ vMax: get_vMax }); /* calls autoNumeric and passes function get_vMax */

            $('#get_metricUnit').change(function () {
                var set_value = $('#length').autoNumericGet();
                if (this.value == ' km') {
                    set_value = set_value / 1000;
                } else {
                    set_value = set_value * 1000;
                }
                $('#length').autoNumericSet(set_value);
            });
            /* end script for callback demo*/

            /************* Textboxes to be controlled go here *******************/
            $('<%= TextBoxExamTearOsmolarityOsmolesNbr.ClientID %>.auto').autoNumeric();

        });
    </script>

</asp:Content>