﻿<%@ Page Title="" Language="C#" MasterPageFile="~/PIMMasterPage.master" AutoEventWireup="true" CodeFile="TIOLChart.aspx.cs" Inherits="abo_TIOLChart" %>

<asp:Content ID="Content1" ContentPlaceHolderID="head" Runat="Server">

    <script type="text/javascript" language="javascript" src="http://ajax.googleapis.com/ajax/libs/jquery/1.7.1/jquery.min.js"></script>
    <link type="text/css" href="../../common/css/atooltip.css" rel="stylesheet"  media="screen" />
	<script type="text/javascript" src="../../common/js/jquery.metadata.js"></script> <!--when changing defaults-->
    <script type="text/javascript" src="../../common/js/autoNumeric-1.7.5.js"></script>
    <script type="text/javascript" src="../../common/js/jquery.atooltip.js"></script>
    <script type="text/javascript" language="javascript">
        //      Either enables or disables input boxes
        function enabledControl(el, disabled, checked) {
            //alert("Hello world from enabled control");
            try {
                if (disabled) {
                    el.disabled = disabled;
                    el.setAttribute("disabled", true);
                }
                else {
                    el.disabled = disabled;
                    el.removeAttribute("disabled");
                }

                if (checked == true)
                    el.checked = false;
            }
            catch (E) {
            }
            if (el.childNodes && el.childNodes.length > 0) {
                for (var x = 0; x < el.childNodes.length; x++) {
                    enabledControl(el.childNodes[x], disabled, checked);
                }
            }
        }
        //      Either enables or disables dropdown boxes
        function enabledControlDropDown(el, disabled, checked) {
            //alert("Hello world from enabled control");
            try {
                if (disabled) {
                    el.disabled = disabled;
                    el.setAttribute("disabled", true);
                }
                else {
                    el.disabled = disabled;
                    el.removeAttribute("disabled");
                }

                if (checked == true)
                    el.options[0].selected = true;
            }
            catch (E) {
            }
            if (el.childNodes && el.childNodes.length > 0) {
                for (var x = 0; x < el.childNodes.length; x++) {
                    enabledControl(el.childNodes[x], disabled, checked);
                }
            }
        }
        //      Either enables or disables text boxes
        function enabledControlTextField(el, disabled, checked) {
            //alert("Hello world from enabled control");
            try {
                if (disabled) {
                    el.disabled = disabled;
                    el.setAttribute("disabled", true);
                }
                else {
                    el.disabled = disabled;
                    el.removeAttribute("disabled");
                }

                if (checked == true) {
                    el.value = '';
                }
            }
            catch (E) {
            }
            if (el.childNodes && el.childNodes.length > 0) {
                for (var x = 0; x < el.childNodes.length; x++) {
                    enabledControl(el.childNodes[x], disabled, checked);
                }
            }
        }

        function generate(arr1, arr2, istrue) {
            if (istrue == true) {
                for (var i = 0; i < arr1.length; i++) {
                    document.getElementById(arr1[i]).style.color = "gray";
                }
                for (var i = 0; i < arr2.length; i++) {
                    $(arr2[i] + ' :input').attr('disabled', true);
                    $(arr2[i] + ' :input').removeAttr("checked");
                }
            }
            if (istrue == false) {
                for (var i = 0; i < arr1.length; i++) {
                    document.getElementById(arr1[i]).style.color = "#333";
                }
                for (var i = 0; i < arr2.length; i++) {
                    $(arr2[i] + ' :input').removeAttr('disabled');
                }
            }
        }

        // toggleQE2TB
        function toggleQE2TB() {
            document.getElementById("<%= TextBoxExamRefraction.ClientID %>").value = '';
            document.getElementById("<%= TextBoxExamRefraction2.ClientID %>").value = '';
            document.getElementById("<%= TextBoxExamRefaction3.ClientID %>").value = '';
        }

        // toggleQE2CB
        function toggleQE2CB() {
            document.getElementById("<%= CheckBoxExamRefractionNA.ClientID %>").checked = false;
        }

        // toggleQE3TB
        function toggleQE3TB() {
            document.getElementById("<%= TextBoxExamIOP.ClientID %>").value = '';
        }
        // toggleQE3CB
        function toggleQE3CB() {
            document.getElementById("<%= CheckBoxExamIOPNA.ClientID %>").checked = false;
        }


        // toggleQO1TB
        function toggleQO1TB() {
            document.getElementById("<%= TextBoxPostopRefraction1.ClientID %>").value = '';
            document.getElementById("<%= TextBoxPostopRefraction2.ClientID %>").value = '';
            document.getElementById("<%= TextBoxPostopRefraction3.ClientID %>").value = '';
        }

        // toggleQO1CB
        function toggleQO1CB() {
            document.getElementById("<%= CheckBoxPostopRefractionNA.ClientID %>").checked = false;
        }

        // toggleQO2TB
        function QO2toggleTB() {
            document.getElementById("<%= TextBoxPostopAstigmatism.ClientID %>").value = '';
            document.getElementById("<%= TextBoxPostopAstigmatismDegree.ClientID %>").value = '';
        }

        // toggleQO2TB
        function QO2toggleCB() {
            document.getElementById("<%= CheckBoxAstigmatismDegreeNA.ClientID %>").checked = false;
        }

        // toggleQM2TB
        function QM2toggleCB() {
            document.getElementById("<%= TextBoxPredictedResAstigmatism.ClientID %>").value = '';
            document.getElementById("<%= TextBoxPredictedResAstigmatismDegree.ClientID %>").value = '';
        }

        // toggleQM2TB
        function QM2toggleTB() {
            document.getElementById("<%= CheckBoxPredictedResAstigmatismNO.ClientID %>").checked = false;
        }
    </script>

    <script type="text/javascript" language="javascript">
        $(document).ready(function() {

            // Question 1a other - Pre-operative management ***************************
            $("#<%= CheckBoxComorbidOther.ClientID %>").click(function() {
                if ($('#<%= CheckBoxComorbidOther.ClientID %>').attr("checked") == "checked") {
                    var myaray = new Array("QP1aA");
                    var myarray2 = new Array('#QP1aA');
                    generate(myaray, myarray2, false);

                    enabledControlTextField(document.getElementById("<%= TextBoxPreopKerOtherText.ClientID %>"), false, false);
                }
                else {
                    var myaray = new Array("QP1aA");
                    var myarray2 = new Array('#QP1aA');
                    generate(myaray, myarray2, true);


                    enabledControlTextField(document.getElementById("<%= TextBoxPreopKerOtherText.ClientID %>"), true, true);
                }
            });
            if ($('#<%= CheckBoxComorbidOther.ClientID %>').attr("checked") == "checked") {
                var myaray = new Array("QP1aA");
                var myarray2 = new Array('#QP1aA');
                generate(myaray, myarray2, false);

                enabledControlTextField(document.getElementById("<%= TextBoxPreopKerOtherText.ClientID %>"), false, false);
            }
            else {
                var myaray = new Array("QP1aA");
                var myarray2 = new Array('#QP1aA');
                generate(myaray, myarray2, true);


                enabledControlTextField(document.getElementById("<%= TextBoxPreopKerOtherText.ClientID %>"), true, true);
            }


            // Question 6a other - Pre-operative management ***************************
            $("#<%= CheckBoxPreopKerOther.ClientID %>").click(function() {
                if ($('#<%= CheckBoxPreopKerOther.ClientID %>').attr("checked") == "checked") {
                    var myaray = new Array("QP6aA");
                    var myarray2 = new Array('#QP6aA');
                    generate(myaray, myarray2, false);

                    enabledControlTextField(document.getElementById("<%= TextBoxPreopKerCalcOtherText.ClientID %>"), false, false);
                }
                else {
                    var myaray = new Array("QP6aA");
                    var myarray2 = new Array('#QP6aA');
                    generate(myaray, myarray2, true);

                    enabledControlTextField(document.getElementById("<%= TextBoxPreopKerCalcOtherText.ClientID %>"), true, true);
                }
            });
            if ($('#<%= CheckBoxPreopKerOther.ClientID %>').attr("checked") == "checked") {
                var myaray = new Array("QP6aA");
                var myarray2 = new Array('#QP6aA');
                generate(myaray, myarray2, false);

                enabledControlTextField(document.getElementById("<%= TextBoxPreopKerCalcOtherText.ClientID %>"), false, false);
            }
            else {
                var myaray = new Array("QP6aA");
                var myarray2 = new Array('#QP6aA');
                generate(myaray, myarray2, true);

                enabledControlTextField(document.getElementById("<%= TextBoxPreopKerCalcOtherText.ClientID %>"), true, true);
            }


            // Question 7a other - Pre-operative management ***************************
            $("#<%= RadioButtonListRBPreopKer.ClientID %>").click(function () {
                if ($('#<%= RadioButtonListRBPreopKer.ClientID %>').find('input:checked').val() == ('14')) {
                    var myaray = new Array("QP7aA");
                    var myarray2 = new Array('#QP7aA');
                    generate(myaray, myarray2, false);

                    enabledControlTextField(document.getElementById("<%= TextBoxComorbidOtherText.ClientID %>"), false, false);
                }
                else {
                    var myaray = new Array("QP7aA");
                    var myarray2 = new Array('#QP7aA');
                    generate(myaray, myarray2, true);

                    enabledControlTextField(document.getElementById("<%= TextBoxComorbidOtherText.ClientID %>"), true, true);
                }
            });
            if ($('#<%= RadioButtonListRBPreopKer.ClientID %>').find('input:checked').val() == ('14')) {
                var myaray = new Array("QP7aA");
                var myarray2 = new Array('#QP7aA');
                generate(myaray, myarray2, false);

                enabledControlTextField(document.getElementById("<%= TextBoxComorbidOtherText.ClientID %>"), false, false);
            }
            else {
                var myaray = new Array("QP7aA");
                var myarray2 = new Array('#QP7aA');
                generate(myaray, myarray2, true);

                enabledControlTextField(document.getElementById("<%= TextBoxComorbidOtherText.ClientID %>"), true, true);
            }


            // Question 9a other - Pre-operative management ***************************
            $("#<%= RadioButtonListRBPreopIOLPowerCalc.ClientID %>").click(function() {
                if ($('#<%= RadioButtonListRBPreopIOLPowerCalc.ClientID %>').find('input:checked').val() == ('14')) {
                    var myaray = new Array("QP9aA");
                    var myarray2 = new Array('#QP9aA');
                    generate(myaray, myarray2, false);

                    enabledControlTextField(document.getElementById("<%= TextBoxPreopIOLPolwerCalcOther.ClientID %>"), false, false);
                }
                else {
                    var myaray = new Array("QP9aA");
                    var myarray2 = new Array('#QP9aA');
                    generate(myaray, myarray2, true);

                    enabledControlTextField(document.getElementById("<%= TextBoxPreopIOLPolwerCalcOther.ClientID %>"), true, true);
                }
            });
            if ($('#<%= RadioButtonListRBPreopIOLPowerCalc.ClientID %>').find('input:checked').val() == ('14')) {
                var myaray = new Array("QP9aA");
                var myarray2 = new Array('#QP9aA');
                generate(myaray, myarray2, false);

                enabledControlTextField(document.getElementById("<%= TextBoxPreopIOLPolwerCalcOther.ClientID %>"), false, false);
            }
            else {
                var myaray = new Array("QP9aA");
                var myarray2 = new Array('#QP9aA');
                generate(myaray, myarray2, true);

                enabledControlTextField(document.getElementById("<%= TextBoxPreopIOLPolwerCalcOther.ClientID %>"), true, true);
            }


            // Question 3a other - Management ***************************
            $("#<%= RadioButtonListRBIOLBrand.ClientID %>").click(function() {
                if ($('#<%= RadioButtonListRBIOLBrand.ClientID %>').find('input:checked').val() == ('14')) {
                    var myaray = new Array("QP3aA");
                    var myarray2 = new Array('#QP3aA');
                    generate(myaray, myarray2, false);

                    enabledControlTextField(document.getElementById("<%= TextBoxIOLBrandOtherText.ClientID %>"), false, false);
                }
                else {
                    var myaray = new Array("QP3aA");
                    var myarray2 = new Array('#QP3aA');
                    generate(myaray, myarray2, true);

                    enabledControlTextField(document.getElementById("<%= TextBoxIOLBrandOtherText.ClientID %>"), true, true);
                }
            });
            if ($('#<%= RadioButtonListRBIOLBrand.ClientID %>').find('input:checked').val() == ('14')) {
                var myaray = new Array("QP3aA");
                var myarray2 = new Array('#QP3aA');
                generate(myaray, myarray2, false);

                enabledControlTextField(document.getElementById("<%= TextBoxIOLBrandOtherText.ClientID %>"), false, false);
            }
            else {
                var myaray = new Array("QP3aA");
                var myarray2 = new Array('#QP3aA');
                generate(myaray, myarray2, true);

                enabledControlTextField(document.getElementById("<%= TextBoxIOLBrandOtherText.ClientID %>"), true, true);
            }


            // Question 10a - Management ***************************
            $("#<%= RadioButtonSurgIntraopComplicationsYes.ClientID %>").click(function() {
                if ($('#<%= RadioButtonSurgIntraopComplicationsYes.ClientID %>').attr("checked") == "checked") {
                    var myaray = new Array("QM10aA", "QM10aB");
                    var myarray2 = new Array('#QM10aB');
                    generate(myaray, myarray2, false);
                }
                else {
                    var myaray = new Array("QM10aA", "QM10aB", "QM10bA");
                    var myarray2 = new Array('#QM10aB');
                    generate(myaray, myarray2, true);

                    enabledControlTextField(document.getElementById("<%= TextBoxSurgComplicationOtherText.ClientID %>"), true, true);
                }
            });
            $("#<%= RadioButtonSurgIntraopComplicationsNo.ClientID %>").click(function() {
                if ($('#<%= RadioButtonSurgIntraopComplicationsYes.ClientID %>').attr("checked") == "checked") {
                    var myaray = new Array("QM10aA", "QM10aB");
                    var myarray2 = new Array('#QM10aB');
                    generate(myaray, myarray2, false);
                }
                else {
                    var myaray = new Array("QM10aA", "QM10aB", "QM10bA");
                    var myarray2 = new Array('#QM10aB');
                    generate(myaray, myarray2, true);

                    enabledControlTextField(document.getElementById("<%= TextBoxSurgComplicationOtherText.ClientID %>"), true, true);
                }
            });
            if ($('#<%= RadioButtonSurgIntraopComplicationsYes.ClientID %>').attr("checked") == "checked") {
                var myaray = new Array("QM10aA", "QM10aB");
                var myarray2 = new Array('#QM10aB');
                generate(myaray, myarray2, false);
            }
            else {
                var myaray = new Array("QM10aA", "QM10aB", "QM10bA");
                var myarray2 = new Array('#QM10aB');
                generate(myaray, myarray2, true);

                enabledControlTextField(document.getElementById("<%= TextBoxSurgComplicationOtherText.ClientID %>"), true, true);
            }


            // Question 10a, other - Management ***************************
            $("#<%= CheckBoxSurgComplicationOther.ClientID %>").click(function() {
                if ($('#<%= CheckBoxSurgComplicationOther.ClientID %>').attr("checked") == "checked") {
                    var myaray = new Array("QM10bA");
                    var myarray2 = new Array('#QM10bA');
                    generate(myaray, myarray2, false);

                    enabledControlTextField(document.getElementById("<%= TextBoxSurgComplicationOtherText.ClientID %>"), false, false);
                }
                else {
                    var myaray = new Array("QM10bA");
                    var myarray2 = new Array('#QM10bA');
                    generate(myaray, myarray2, true);

                    enabledControlTextField(document.getElementById("<%= TextBoxSurgComplicationOtherText.ClientID %>"), true, true);
                }
            });
            if ($('#<%= CheckBoxSurgComplicationOther.ClientID %>').attr("checked") == "checked") {
                var myaray = new Array("QM10bA");
                var myarray2 = new Array('#QM10bA');
                generate(myaray, myarray2, false);

                enabledControlTextField(document.getElementById("<%= TextBoxSurgComplicationOtherText.ClientID %>"), false, false);
            }
            else {
                var myaray = new Array("QM10bA");
                var myarray2 = new Array('#QM10bA');
                generate(myaray, myarray2, true);

                enabledControlTextField(document.getElementById("<%= TextBoxSurgComplicationOtherText.ClientID %>"), true, true);
            }


            // Question 11a - Management ***************************
            $("#<%= RadioButtonSurgAdjunctiveProcYes.ClientID %>").click(function() {
                if ($('#<%= RadioButtonSurgAdjunctiveProcYes.ClientID %>').attr("checked") == "checked") {
                    var myaray = new Array("QM11aA", "QM11aB");
                    var myarray2 = new Array('#QM11aB');
                    generate(myaray, myarray2, false);
                }
                else {
                    var myaray = new Array("QM11aA", "QM11aB", "QM11bA");
                    var myarray2 = new Array('#QM11aB');
                    generate(myaray, myarray2, true);

                    enabledControlTextField(document.getElementById("<%= TextBoxSurgAdjunctiveProcOtherText.ClientID %>"), true, true);
                }
            });
            $("#<%= RadioButtonSurgAdjunctiveProcNo.ClientID %>").click(function() {
                if ($('#<%= RadioButtonSurgAdjunctiveProcYes.ClientID %>').attr("checked") == "checked") {
                    var myaray = new Array("QM11aA", "QM11aB");
                    var myarray2 = new Array('#QM11aB');
                    generate(myaray, myarray2, false);
                }
                else {
                    var myaray = new Array("QM11aA", "QM11aB", "QM11bA");
                    var myarray2 = new Array('#QM11aB');
                    generate(myaray, myarray2, true);

                    enabledControlTextField(document.getElementById("<%= TextBoxSurgAdjunctiveProcOtherText.ClientID %>"), true, true);
                }
            });
            if ($('#<%= RadioButtonSurgAdjunctiveProcYes.ClientID %>').attr("checked") == "checked") {
                var myaray = new Array("QM11aA", "QM11aB");
                var myarray2 = new Array('#QM11aB');
                generate(myaray, myarray2, false);
            }
            else {
                var myaray = new Array("QM11aA", "QM11aB", "QM11bA");
                var myarray2 = new Array('#QM11aB');
                generate(myaray, myarray2, true);

                enabledControlTextField(document.getElementById("<%= TextBoxSurgAdjunctiveProcOtherText.ClientID %>"), true, true);
            }


            // Question 11a, other - Management ***************************
            $("#<%= CheckBoxSurgAdjunctiveProcOther.ClientID %>").click(function() {
                if ($('#<%= CheckBoxSurgAdjunctiveProcOther.ClientID %>').attr("checked") == "checked") {
                    var myaray = new Array("QM11bA");
                    var myarray2 = new Array('#QM11bA');
                    generate(myaray, myarray2, false);

                    enabledControlTextField(document.getElementById("<%= TextBoxSurgAdjunctiveProcOtherText.ClientID %>"), false, false);
                }
                else {
                    var myaray = new Array("QM11bA");
                    var myarray2 = new Array('#QM11bA');
                    generate(myaray, myarray2, true);

                    enabledControlTextField(document.getElementById("<%= TextBoxSurgAdjunctiveProcOtherText.ClientID %>"), true, true);
                }
            });
            if ($('#<%= CheckBoxSurgAdjunctiveProcOther.ClientID %>').attr("checked") == "checked") {
                var myaray = new Array("QM11bA");
                var myarray2 = new Array('#QM11bA');
                generate(myaray, myarray2, false);

                enabledControlTextField(document.getElementById("<%= TextBoxSurgAdjunctiveProcOtherText.ClientID %>"), false, false);
            }
            else {
                var myaray = new Array("QM11bA");
                var myarray2 = new Array('#QM11bA');
                generate(myaray, myarray2, true);

                enabledControlTextField(document.getElementById("<%= TextBoxSurgAdjunctiveProcOtherText.ClientID %>"), true, true);
            }

            /// Question 4 - Outcomes ***************************
            $("#<%= DropDownListPostopBCVA.ClientID %>").change(function() {
                var sel = $('#<%= DropDownListPostopBCVA.ClientID %> option:selected').text();
                if (sel > 40) {
                    var myaray = new Array("QOu4aA", "QOu4aB");
                    var myarray2 = new Array('#QOu4aB');
                    generate(myaray, myarray2, false);
                }
                else {
                    var myaray = new Array("QOu4aA", "QOu4aB", "QOu4bA");
                    var myarray2 = new Array('#QOu4aB');
                    generate(myaray, myarray2, true);

                    enabledControlTextField(document.getElementById("<%= TextBoxRBPostopBCVAReasonOtherText.ClientID %>"), true, true);
                }
            });
            var sel = $('#<%= DropDownListPostopBCVA.ClientID %> option:selected').text();
            if (sel > 40) {
                var myaray = new Array("QOu4aA", "QOu4aB");
                var myarray2 = new Array('#QOu4aB');
                generate(myaray, myarray2, false);
            }
            else {
                var myaray = new Array("QOu4aA", "QOu4aB", "QOu4bA");
                var myarray2 = new Array('#QOu4aB');
                generate(myaray, myarray2, true);

                enabledControlTextField(document.getElementById("<%= TextBoxRBPostopBCVAReasonOtherText.ClientID %>"), true, true);
            }

            // Question 4, other - Outcomes ***************************
            $("#<%= RadioButtonListRBPostopBCVAReason.ClientID %>").click(function() {
                if ($('#<%= RadioButtonListRBPostopBCVAReason.ClientID %>').find('input:checked').val() == ('14')) {
                    var myaray = new Array("QOu4bA");
                    var myarray2 = new Array('#QOu4bA');
                    generate(myaray, myarray2, false);

                    enabledControlTextField(document.getElementById("<%= TextBoxRBPostopBCVAReasonOtherText.ClientID %>"), false, false);
                }
                else {
                    var myaray = new Array("QOu4bA");
                    var myarray2 = new Array('#QOu4bA');
                    generate(myaray, myarray2, true);

                    enabledControlTextField(document.getElementById("<%= TextBoxRBPostopBCVAReasonOtherText.ClientID %>"), true, true);
                }
            });
            if ($('#<%= RadioButtonListRBPostopBCVAReason.ClientID %>').find('input:checked').val() == ('14')) {
                var myaray = new Array("QOu4bA");
                var myarray2 = new Array('#QOu4bA');
                generate(myaray, myarray2, false);

                enabledControlTextField(document.getElementById("<%= TextBoxRBPostopBCVAReasonOtherText.ClientID %>"), false, false);
            }
            else {
                var myaray = new Array("QOu4bA");
                var myarray2 = new Array('#QOu4bA');
                generate(myaray, myarray2, true);

                enabledControlTextField(document.getElementById("<%= TextBoxRBPostopBCVAReasonOtherText.ClientID %>"), true, true);
            }


            // Question 6 (essentially an other) - Outcomes ***************************
            $("#<%= RadioButtonPostopFurtherSurgYes.ClientID %>").click(function() {
                if ($('#<%= RadioButtonPostopFurtherSurgYes.ClientID %>').attr("checked") == "checked") {
                    var myaray = new Array("QOu6aA");
                    var myarray2 = new Array('#QOu6aA');
                    generate(myaray, myarray2, false);

                    enabledControlTextField(document.getElementById("<%= TextBoxPostopFurtherSurgReason.ClientID %>"), false, false);
                }
                else {
                    var myaray = new Array("QOu6aA");
                    var myarray2 = new Array('#QOu6aA');
                    generate(myaray, myarray2, true);

                    enabledControlTextField(document.getElementById("<%= TextBoxPostopFurtherSurgReason.ClientID %>"), true, true);
                }
            });
            $("#<%= RadioButtonPostopFurtherSurgNo.ClientID %>").click(function() {
                if ($('#<%= RadioButtonPostopFurtherSurgYes.ClientID %>').attr("checked") == "checked") {
                    var myaray = new Array("QOu6aA");
                    var myarray2 = new Array('#QOu6aA');
                    generate(myaray, myarray2, false);

                    enabledControlTextField(document.getElementById("<%= TextBoxPostopFurtherSurgReason.ClientID %>"), false, false);
                }
                else {
                    var myaray = new Array("QOu6aA");
                    var myarray2 = new Array('#QOu6aA');
                    generate(myaray, myarray2, true);

                    enabledControlTextField(document.getElementById("<%= TextBoxPostopFurtherSurgReason.ClientID %>"), true, true);
                }
            });
            if ($('#<%= RadioButtonPostopFurtherSurgYes.ClientID %>').attr("checked") == "checked") {
                var myaray = new Array("QOu6aA");
                var myarray2 = new Array('#QOu6aA');
                generate(myaray, myarray2, false);

                enabledControlTextField(document.getElementById("<%= TextBoxPostopFurtherSurgReason.ClientID %>"), false, false);
            }
            else {
                var myaray = new Array("QOu6aA");
                var myarray2 = new Array('#QOu6aA');
                generate(myaray, myarray2, true);

                enabledControlTextField(document.getElementById("<%= TextBoxPostopFurtherSurgReason.ClientID %>"), true, true);
            }
        });
    </script>
    <script type="text/javascript" language="javascript">
        $(document).ready(function() {
            /** instruct the metadata plugin where to look the metadata
            * jQuery.metadata.setType( type, name );
            * please read the metadata instructions for additional information
            * http://plugins.jquery.com/project/metadata
            */
            $.metadata.setType('attr', 'meta');

            /** To call autoNumeric
            * $(selector).autoNumeric({options}); 
            * The below example uses the input & class selector
            */
            $('input.auto').autoNumeric();
            /* scripts for metadata code generator  */

            /* rountine that prevents  numeric characters from being entered the the altDec field  */
            $('#altDecb').keypress(function(e) {
                var cc = String.fromCharCode(e.which);
                if (e.which != 32 && cc >= 0 && cc <= 9) {
                    e.preventDefault();
                }
            });

            /* rountine that prevents  apostrophe, comma, more than one period (full stop) or numeric characters from being entered the the aSign field  */
            $('#aSignb').keypress(function(e) {
                var cc = String.fromCharCode(e.which);
                if ((e.which != 32 && cc >= 0 && cc <= 9) || cc == "," || cc == "'" || cc == "." && this.value.lastIndexOf('.') != -1) {
                    e.preventDefault();
                }
            });

            $("input.md").bind('click keyup blur', function() {
                var metaCode = '', aSep = '', dGroup = '', aDec = '', altDec = '', aSign = '', pSign = '', vMin = '', vMax = '', mDec = '', mRound = '', aPad = '', wEmpty = '', aForm = '';
                if ($("input:radio[name=aSep]:checked").attr('id') == 'aSepc') {
                    $('input:radio[name=aDec]:nth(0)').removeAttr("disabled");
                    $('input:radio[name=aDec]:nth(0)').attr('checked', true);
                    $('input:radio[name=aDec]:nth(1)').attr("disabled", true);
                }
                if ($("input:radio[name=aSep]:checked").attr('id') == 'aSepp') {
                    $('input:radio[name=aDec]:nth(1)').removeAttr("disabled");
                    $('input:radio[name=aDec]:nth(1)').attr('checked', true);
                    $('input:radio[name=aDec]:nth(0)').attr("disabled", true);
                }
                if ($("input:radio[name=aSep]:checked").attr('id') != 'aSepc' || $("input:radio[name=aSep]:checked").attr('id') != 'aSepp') {
                    $('input:radio[name=aDec]:nth(0)').removeAttr("disabled");
                    $('input:radio[name=aDec]:nth(1)').removeAttr("disabled");
                }
                aSep = $("input:radio[name=aSep]:checked").val();
                dGroup = $("input:radio[name=dGroup]:checked").val();
                aDec = $("input:radio[name=aDec]:checked").val();

                if ($("input:radio[name=altDec]:checked").attr('id') == 'altDecd') {
                    $('#altDecb').val('');
                    $('#altDecb').attr("disabled", true);
                }
                if ($("input:radio[name=altDec]:checked").attr('id') == 'altDeca') {
                    $('#altDecb').removeAttr("disabled");
                    altDec = $('#altDecb').val();
                }

                if ($("input:radio[name=aSign]:checked").attr('id') == 'aSignd') {
                    $('#aSignb').val('');
                    $('#aSignb').attr("disabled", true);
                }
                if ($("input:radio[name=aSign]:checked").attr('id') == 'aSigna') {
                    $('#aSignb').removeAttr("disabled");
                    aSign = $('#aSignb').val();
                }

                pSign = $("input:radio[name=pSign]:checked").val();
                if ($("input:radio[name=vMin]:checked").attr('id') == 'vMind') {
                    $('#vMinb').val('');
                    $('#vMinb').attr("disabled", true);
                }
                if ($("input:radio[name=vMin]:checked").attr('id') == 'vMina') {
                    $('#vMinb').removeAttr("disabled");
                    vMin = $('#vMinb').val();
                }
                if ($("input:radio[name=vMax]:checked").attr('id') == 'vMaxd') {
                    $('#vMaxb').val('');
                    $('#vMaxb').attr("disabled", true);
                }
                if ($("input:radio[name=vMax]:checked").attr('id') == 'vMaxa') {
                    $('#vMaxb').removeAttr("disabled");
                    vMax = $('#vMaxb').val();
                }
                if ($("input:radio[name=mDec]:checked").attr('id') == 'mDecd') {
                    $('#mDecbb').val('');
                    $('#mDecbb').attr("disabled", true);
                }
                if ($("input:radio[name=mDec]:checked").attr('id') == 'mDeca') {
                    $('#mDecbb').removeAttr("disabled");
                    mDec = $('#mDecbb').val();
                }
                mRound = $("input:radio[name=mRound]:checked").val();
                aPad = $("input:radio[name=aPad]:checked").val();
                wEmpty = $("input:radio[name=wEmpty]:checked").val();
                if (aSep != '') {
                    metaCode = aSep;
                }
                if (dGroup != '') {
                    if (metaCode != '') {
                        metaCode = metaCode + ", " + dGroup;
                    }
                    else {
                        metaCode = dGroup;
                    }
                }
                if (aDec != '') {
                    if (metaCode != '') {
                        metaCode = metaCode + ", " + aDec;
                    }
                    else {
                        metaCode = aDec;
                    }
                }
                if (altDec != '') {
                    if (metaCode != '') {
                        metaCode = metaCode + ", altDec: '" + altDec + "'";
                    }
                    else {
                        metaCode = "altDec: '" + altDec + "'";
                    }
                }
                if (aSign != '') {
                    if (metaCode != '') {
                        metaCode = metaCode + ", aSign: '" + aSign + "'";
                    }
                    else {
                        metaCode = "aSign: '" + aSign + "'";
                    }
                }
                if (pSign != '') {
                    if (metaCode != '') {
                        metaCode = metaCode + ", " + pSign;
                    }
                    else {
                        metaCode = pSign;
                    }
                }
                if (vMin != '') {
                    if (metaCode != '') {
                        metaCode = metaCode + ", vMin: '" + vMin + "'";
                    }
                    else {
                        metaCode = "vMin: '" + vMin + "'";
                    }
                }
                if (vMax != '') {
                    if (metaCode != '') {
                        metaCode = metaCode + ", vMax: '" + vMax + "'";
                    }
                    else {
                        metaCode = "vMax: '" + vMax + "'";
                    }
                }
                if (mDec != '') {
                    if (metaCode != '') {
                        metaCode = metaCode + ", mDec: '" + $('#mDecbb').val() + "'";
                    }
                    else {
                        metaCode = "mDec: '" + $('#mDecbb').val() + "'";
                    }
                }

                if (mRound != '') {
                    if (metaCode != '') {
                        metaCode = metaCode + ", " + mRound;
                    }
                    else {
                        metaCode = mRound;
                    }
                }
                if (aPad != '') {
                    if (metaCode != '') {
                        metaCode = metaCode + ", " + aPad;
                    }
                    else {
                        metaCode = aPad;
                    }
                }
                if (wEmpty != '') {
                    if (metaCode != '') {
                        metaCode = metaCode + ", " + wEmpty;
                    }
                    else {
                        metaCode = wEmpty;
                    }
                }
                $('#metaCode').text('');
                if (metaCode != '') {
                    $('#metaCode').text('meta="{' + metaCode + '}"');
                }
            });

            /* clears the metadata code  */
            $('#rd').click(function() {
                $('#metaCode').text('');
            });
            /* ends scripts for metadata code generator  */

            /* script  for defaults demo  */
            $('#d_noMeta').blur(function() {
                var convertInput = '';
                convertInput = $(this).autoNumericGet();
                $('#d_Get').val(convertInput);
                $('#d_Set').autoNumericSet(convertInput);
            });
            /* end script  for defaults demo  */

            /* script  for various samples demo  */
            $('input[name$="sample"]').blur(function() {
                var convertInput = '';
                var row = 'row_' + this.id.charAt(4);
                convertInput = $(this).autoNumericGet();
                $('#' + row + 'b').val(convertInput);
                $('#' + row + 'c').autoNumericSet(convertInput);
            });
            /* end script  for various samples demo  */

            /* script  for rounding methods  */
            $('#roundValue').blur(function() {
                if (this.value != '') {
                    convertInput = $('#roundValue').autoNumericGet();
                    var i = 1;
                    for (i = 1; i <= 9; i++) {
                        $('#roundMethod' + i).autoNumericSet(convertInput);
                    }
                }
            });

            $('#roundDecimal').change(function() { /* changes decimal places */
                convertInput = $('#roundValue').autoNumericGet();
                if (convertInput > 0) {
                    var i = 1;
                    for (i = 1; i <= 9; i++) {
                        $('#roundMethod' + i).autoNumericSet(convertInput);
                    }
                }
            });
            /* end script  for rounding methods  */

            /* script for dynamically loaded values  demo*/
            $.getJSON("test_JSON.php", function(data) {
                var valueFormatted = '';
                $.each(data, function(key, value) { // loops through JSON keys and returns value	
                    $('#' + key).autoNumericSet(value);
                });
            });
            /* end script for dynamically loaded values demo*/

            /* script for callback demo*/
            $.autoNumeric.get_mDec = function() { /* get_mDec function attached to autoNumeric() */
                var set_mDec = $('#get_metricUnit').val();
                if (set_mDec == ' km') {
                    set_mDec = 3;
                } else {
                    set_mDec = 0;
                }
                return set_mDec; /* set mDec decimal places */
            }

            var get_vMax = function() { /* set the maximum value allowed based on the metric unit */
                var set_vMax = $('#get_metricUnit').val();
                if (set_vMax == ' km') {
                    set_vMax = '99999.999';
                } else {
                    set_vMax = '99999999';
                }
                return set_vMax;
            }

            $('#length').autoNumeric({ vMax: get_vMax }); /* calls autoNumeric and passes function get_vMax */

            $('#get_metricUnit').change(function() {
                var set_value = $('#length').autoNumericGet();
                if (this.value == ' km') {
                    set_value = set_value / 1000;
                } else {
                    set_value = set_value * 1000;
                }
                $('#length').autoNumericSet(set_value);
            });
            /* end script for callback demo*/


            // Examination, Question 2, sph, -30 to 30, 2 decimals
            $('<%= TextBoxExamRefraction.ClientID %>.auto').autoNumeric();

            // Examination, Question 2, cyl XX.XX
            $('<%= TextBoxExamRefraction2.ClientID %>.auto').autoNumeric();
            // Examination, Question 2, axis 0 - 180
            $('<%= TextBoxExamRefaction3.ClientID %>.auto').autoNumeric();

            // Pre-Operative Management, Question 8, tens 0 - 100 (no decimal)
            $('<%= TextBoxPreopKeFlatValue1.ClientID %>.auto').autoNumeric();
            // Pre-Operative Management, Question 8, tenths 0 - 99 (no decimal)
            $('<%= TextBoxPreopKerFlatValue2.ClientID %>.auto').autoNumeric();
            // Pre-Operative Management, Question 8, axis 0 - 180
            $('<%= TextBoxPreopKerFlatValue3.ClientID %>.auto').autoNumeric();
            // Pre-Operative Management, Question 8, tens 0 - 100 (no decimal)
            $('<%= TextBoxPreopKerSteepValue1.ClientID %>.auto').autoNumeric();
            // Pre-Operative Management, Question 8, tenths 0 - 99 (no decimal)
            $('<%= TextBoxPreopKerSteepValue2.ClientID %>.auto').autoNumeric();
            // Pre-Operative Management, Question 8, axis 0 - 180
            $('<%= TextBoxPreopKerSteepValue3.ClientID %>.auto').autoNumeric();

            $('<%= TextBoxRefractiveTarget.ClientID %>.auto').autoNumeric();
            $('<%= TextBoxPredictedResAstigmatism.ClientID %>.auto').autoNumeric();
            $('<%= TextBoxPredictedResAstigmatismDegree.ClientID %>.auto').autoNumeric();

            $('<%= TextBoxIOLPowerSelectedSpherical.ClientID %>.auto').autoNumeric();
            $('<%= TextBoxIOLPowerSelectedCylindrical.ClientID %>.auto').autoNumeric();
            $('<%= TextBoxIOLPlacementPlannedAxis.ClientID %>.auto').autoNumeric();

            $('<%= TextBoxPostopAstigmatism.ClientID %>.auto').autoNumeric();
            $('<%= TextBoxPostopAstigmatismDegree.ClientID %>.auto').autoNumeric();

        });
    </script>
    <style type="text/css">
        .bginputa{
	        float:right;
	        margin:21px 0 0 1px;
	        background: url(../common/images/orange_button_with_arrow.png) no-repeat;
	        overflow:hidden;
	        height:35px;

        }
        .bginputa a{
	        color: white;
	        text-align:center;
	        height:35px;
	        line-height:28px;
	        padding:0 14px 15px  ;
	        cursor:pointer;
	        float:left;
	        border:none;
	        text-decoration:none;
	        font-family:Arial, Helvetica, sans-serif; 
	        font-size:12px; 
	        font-weight:bold; 
	        letter-spacing: 0px;
        }
        .bginputa a:hover{
	        text-decoration:none;
        }
        .right {
            text-align:right;
        }
    </style>
    <script type="text/javascript">
        $(function() {
            $('#QE2').aToolTip({
                clickIt: true,
                tipContent: 'Ranges for sph [+/- 0.00 thru 30.00],<br />cyl [0.00 thru 20.00], axis [integer 0 thru 180]'
            });
            $('#QO1').aToolTip({
                clickIt: true,
                tipContent: 'Ranges for sph [+/- 0.00 thru 30.00],<br />cyl [0.00 thru 20.00], axis [integer 0 thru 180]'
            });

            $('#QE1').aToolTip({
                clickIt: true,
                tipContent: '20/800 or count fingers @ 5 ft<br />20/1000 or count fingers @ 4 ft<br />20/1600 or count fingers @ 3ft<br />20/2000 or count fingers @ 2 ft<br />20/4000 or count fingers @ 1 ft<br />20/7777 =CF<br />20/8888 =HM<br />20/9999 =LP<br />20/0000 =NLP'
            });

            $('#QO4').aToolTip({
                clickIt: true,
                tipContent: '20/800 or count fingers @ 5 ft<br />20/1000 or count fingers @ 4 ft<br />20/1600 or count fingers @ 3ft<br />20/2000 or count fingers @ 2 ft<br />20/4000 or count fingers @ 1 ft<br />20/7777 =CF<br />20/8888 =HM<br />20/9999 =LP<br />20/0000 =NLP'
            });
        });
    </script>
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder1" Runat="Server">
    <asp:HiddenField ID="HiddenFieldRecordIdentifier" runat="server"/>
        <!--  pim -->
        <div class="pim">
            <div class="record-ident clearfix">
                <h3 class="record-first">RECORD IDENTIFIER: <asp:Literal runat="server" ID="LiteralRecordIdentifier"></asp:Literal></h3>
                <h3 class="record-second"><asp:Literal runat="server" ID="LiteralAbstractionNumber"></asp:Literal></h3>
            </div>
            <!-- History -->
            <table>
                <tr>
                    <th colspan="2" width="910px"><p>History</p></th>
                </tr>
                <!-- Question 1 -->
                <tr class="table_body">
                    <td width="65%">
                        <strong>1. Date of birth</strong>
                        <asp:Label ID="LabelMonthOfBirth" runat="server" Visible="false"  ForeColor="Red" Text="<br />Please complete month" />
                        <asp:Label ID="LabelYearOfBirth" runat="server" Visible="false"  ForeColor="Red" Text="<br />Please complete year" />

                    </td>
                    <td>
                        <asp:DropDownList ID='DropDownListMonthOfBirth' DataValueField='MonthID' DataTextField='MonthName' runat='server' />
                        <asp:DropDownList ID='DropDownListYearOfBirth' DataValueField='YearID' DataTextField='YearName' runat='server' />
                    </td>
                </tr>
                <!-- Question 2 -->
                <tr class="table_body_bg">
                    <td>
                        <strong>2. Date of surgery</strong>
                        <asp:Label ID="LabelMonthOfSurgery" runat="server" Visible="false"  ForeColor="Red" Text="<br />Please complete month" />
                        <asp:Label ID="LabelYearOfSurgery" runat="server" Visible="false"  ForeColor="Red" Text="<br />Please complete year" />
                        <asp:Label ID="Labelinitialage" runat="server" Visible="false"  
                            ForeColor="Red" Text="<br />" />
                    </td>
                    <td>
                        <asp:DropDownList ID='DropDownListMonthOfSurgery' DataValueField='MonthID' DataTextField='MonthName' runat='server' />
                        <asp:DropDownList ID='DropDownListYearOfSurgery' DataValueField='YearID' DataTextField='YearName' runat='server' />
                    </td>
                </tr>
                <!-- Question 3 -->
                <tr class="table_body">
                    <td>
                        <strong>3. Gender</strong>
                        <asp:Label ID="LabelRBGender" runat="server" Visible="false"  ForeColor="Red" Text="<br />Please complete" />
                    </td>
                    <td>
                        <asp:RadioButtonList id='RadioButtonListRBGender' RepeatDirection='Horizontal' CssClass='aspxList' runat='server'>
                            <asp:ListItem Value='42'>&nbsp;Male</asp:ListItem>
                            <asp:ListItem Value='43'>&nbsp;Female</asp:ListItem>
                        </asp:RadioButtonList>
                    </td>
                </tr>
                <!-- Header visual complaint -->
                <tr class="table_body_bg">
                    <td colspan="2">
                        <strong>Visual complaint</strong> 
                    </td>
                </tr>
                <!-- Question 4 -->
                <tr class="table_body_bg">
                    <td>
                        <strong>4. Documented effects on activities of daily life</strong>
                        <asp:Label ID="LabelRBSymptomsDailyLiving" runat="server" Visible="false"  ForeColor="Red" Text="<br />Please complete" />
                    </td>
                    <td>
                        <asp:RadioButtonList id='RadioButtonListRBSymptomsDailyLiving' RepeatDirection='Horizontal' CssClass='aspxList' runat='server'>
                            <asp:ListItem Value="1">&nbsp;Yes</asp:ListItem>
                            <asp:ListItem Value="2">&nbsp;No</asp:ListItem>
                            <asp:ListItem Value="3">&nbsp;Not Documented</asp:ListItem>
                        </asp:RadioButtonList>
                    </td>
                </tr>
                <!-- Question 5 -->
                <tr class="table_body">
                    <td>
                        <strong>5. Do corrective lenses provide vision that meets the patient’s needs</strong>
                        <asp:Label ID="LabelRBCorrectiveLens" runat="server" Visible="false"  ForeColor="Red" Text="<br />Please complete" />    
                    </td>
                    <td>
                        <asp:RadioButtonList id='RadioButtonListRBCorrectiveLens' RepeatDirection='Horizontal' CssClass='aspxList' runat='server'>
                            <asp:ListItem Value="1">&nbsp;Yes</asp:ListItem>
                            <asp:ListItem Value="2">&nbsp;No</asp:ListItem>
                            <asp:ListItem Value="3">&nbsp;Not Documented</asp:ListItem>
                        </asp:RadioButtonList>
                    </td>
                </tr>
                <!-- Header Past ocular history -->
                <tr class="table_body_bg">
                    <td colspan="2"><strong>Past ocular history</strong></td>
                </tr>
                <!-- Question 6 -->
                <tr class="table_body_bg">
                    <td>
                        <strong>6. Previous history of trauma</strong>
                        <asp:Label ID="LabelRBPastHistoryTrauma" runat="server" Visible="false"  ForeColor="Red" Text="<br />Please complete" />
                    </td>
                    <td>
                        <asp:RadioButtonList id='RadioButtonListRBPastHistoryTrauma' RepeatDirection='Horizontal' CssClass='aspxList' runat='server'>
                            <asp:ListItem Value="1">&nbsp;Yes</asp:ListItem>
                            <asp:ListItem Value="2">&nbsp;No</asp:ListItem>
                            <asp:ListItem Value="3">&nbsp;Not Documented</asp:ListItem>
                        </asp:RadioButtonList>
                    </td>
                </tr>
                <!-- Question 7 -->
                <tr class="table_body">
                    <td>
                        <strong>7. Previous refractive surgery</strong>
                        <asp:Label ID="LabelRBPastHistoryRefractiveSurgery" runat="server" Visible="false"  ForeColor="Red" Text="<br />Please complete" />
                    </td>
                    <td>
                        <asp:RadioButtonList id='RadioButtonListRBPastHistoryRefractiveSurgery' RepeatDirection='Horizontal' CssClass='aspxList' runat='server'>
                            <asp:ListItem Value="1">&nbsp;Yes</asp:ListItem>
                            <asp:ListItem Value="2">&nbsp;No</asp:ListItem>
                            <asp:ListItem Value="3">&nbsp;Not Documented</asp:ListItem>
                        </asp:RadioButtonList>
                    </td>
                </tr>
                <!-- Question 8 -->
                <tr class="table_body_bg">
                    <td><strong>8. Previous history of other types of eye surgery</strong>
                    <asp:Label ID="LabelRBPastHistoryOtherEyeSurgery" runat="server" Visible="false"  ForeColor="Red" Text="<br />Please complete" />
                    </td>
                    <td>
                        <asp:RadioButtonList id='RadioButtonListRBPastHistoryOtherEyeSurgery' RepeatDirection='Horizontal' CssClass='aspxList' runat='server'>
                            <asp:ListItem Value="1">&nbsp;Yes</asp:ListItem>
                            <asp:ListItem Value="2">&nbsp;No</asp:ListItem>
                            <asp:ListItem Value="3">&nbsp;Not Documented</asp:ListItem>
                        </asp:RadioButtonList>
                    </td>
                </tr>
                <!-- Question 9 -->
                <tr class="table_body">
                    <td><strong>9. Amblyopia</strong>
                    <asp:Label ID="LabelRBPastHistoryAmblyopia" runat="server" Visible="false"  ForeColor="Red" Text="<br />Please complete" />
                    </td>
                    <td>
                        <asp:RadioButtonList id='RadioButtonListRBPastHistoryAmblyopia' RepeatDirection='Horizontal' CssClass='aspxList' runat='server'>
                            <asp:ListItem Value="1">&nbsp;Yes</asp:ListItem>
                            <asp:ListItem Value="2">&nbsp;No</asp:ListItem>
                            <asp:ListItem Value="3">&nbsp;Not Documented</asp:ListItem>
                        </asp:RadioButtonList>
                    </td>
                </tr>
                <!-- Header Medical history -->
                <tr class="table_body_bg">
                    <td colspan="2"><strong>Medical history</strong></td>
                </tr>
                <!-- Question 10 -->
                <tr class="table_body_bg">
                    <td><strong>10. Alpha-1a agonists (i.e. tamsulosin)</strong>
                    <asp:Label ID="LabelRBPastHistoryAlpha1aAgonists" runat="server" Visible="false"  ForeColor="Red" Text="<br />Please complete" />
                    <td>
                        <asp:RadioButtonList id='RadioButtonListRBPastHistoryAlpha1aAgonists' RepeatDirection='Horizontal' CssClass='aspxList' runat='server'>
                            <asp:ListItem Value="1">&nbsp;Yes</asp:ListItem>
                            <asp:ListItem Value="2">&nbsp;No</asp:ListItem>
                            <asp:ListItem Value="3">&nbsp;Not Documented</asp:ListItem>
                        </asp:RadioButtonList>
                    </td>
                </tr>
                <!-- Question 11 -->
                <tr class="table_body">
                    <td><strong>11. Anticoagulation</strong>
                     <asp:Label ID="LabelRBPastHistoryAnticoagulation" runat="server" Visible="false"  ForeColor="Red" Text="<br />Please complete" />
                    </td>
                    <td>
                        <asp:RadioButtonList id='RadioButtonListRBPastHistoryAnticoagulation' RepeatDirection='Horizontal' CssClass='aspxList' runat='server'>
                            <asp:ListItem Value="1">&nbsp;Yes</asp:ListItem>
                            <asp:ListItem Value="2">&nbsp;No</asp:ListItem>
                            <asp:ListItem Value="3">&nbsp;Not Documented</asp:ListItem>
                        </asp:RadioButtonList>
                    </td>
                </tr>
            </table>
            <br />
            <!-- Examination -->
            <table>
                <tr>
                    <th colspan="2" width="910px"><p>Examination</p></th>
                </tr>
                <!-- Question 1 -->
                <tr class="table_body">
                    <td width="65%">
                        <strong>1. Corrected visual acuity (with glasses or most recent refraction)</strong>&nbsp;<a href="#"><img id="QE1" src="../../common/images/tip.gif" alt="Tip" /></a>
                        <asp:Label ID="LabelBCVA" runat="server" Visible="false"  ForeColor="Red" Text="<br />Please complete" />
                    </td>
                    <td>
                        20&nbsp;/&nbsp;<asp:DropDownList ID='DropDownListBCVA' DataValueField='examValue' DataTextField='examLabel' runat='server' />
                    </td>
                </tr>
                <!-- Question 2 -->
                <tr class="table_body_bg">
                    <td><strong>2. Refraction in plus cylinder notation (performed within one year prior to surgery)</strong>&nbsp;<a href="#"><img id="QE2" src="../../common/images/tip.gif" alt="Tip" /></a></td>
                    <td>
                        <table class="aspxList">
                            <tr>
                                <td></td>
                                <td><asp:TextBox ID="TextBoxExamRefraction" meta="{vMin: '-30.00', vMax: '30.00'}" class="auto" runat="server" size="5" onkeyup="toggleQE2CB();" />&nbsp;sph</td>
                            </tr>
                            <tr>
                                <td>+</td>
                                <td>
                                    <asp:TextBox ID="TextBoxExamRefraction2" meta="{vMin: '0.00', vMax: '99.99'}" class="auto" runat="server" size="5" onkeyup="toggleQE2CB();" />&nbsp;cyl
                                </td>
                            </tr>
                            <tr>
                                <td></td>
                                <td>
                                    <asp:TextBox ID="TextBoxExamRefaction3" meta="{vMin: '0.00', vMax: '180.00'}" class="auto" runat="server" size="5" onkeyup="toggleQE2CB();" />&nbsp;axis
                                </td>
                            </tr>
                        </table>
                        <br /><asp:CheckBox ID="CheckBoxExamRefractionNA" runat="server" text="&nbsp;Not Done" onclick="toggleQE2TB();" />
                    </td>
                </tr>
                <!-- Question 3 -->
                <tr class="table_body">
                    <td><strong>3. IOP measurement</strong></td>
                    <td>
                        <asp:TextBox ID="TextBoxExamIOP" runat="server" size="5" onkeyup="toggleQE3CB();" />&nbsp;mmHg
                        <br /><asp:CheckBox ID="CheckBoxExamIOPNA" runat="server" text="&nbsp;Not Documented" onclick="toggleQE3TB();" />
                    </td>
                </tr>
                <!-- Question 4 -->
                <tr class="table_body_bg">
                    <td>
                        <strong>4. Slit lamp exam</strong>
                        <asp:Label ID="LabelExamSlitLamp" runat="server" Visible="false"  ForeColor="Red" Text="<br />Please complete" />
                    </td>
                    <td>
                        <asp:RadioButton ID="RadioButtonExamSlitLampYes" runat="server" GroupName="ExamSlitLamp" text="&nbsp;Yes" />&nbsp;&nbsp;
                        <asp:RadioButton ID="RadioButtonExamSlitLampNo" runat="server" GroupName="ExamSlitLamp" text="&nbsp;No" />
                    </td>
                </tr>
                <!-- Question 5 -->
                <tr class="table_body">
                    <td>
                        <strong>5. Dilated pupil size</strong>
                        <asp:Label ID="LabelRBExamDilatedPupil" runat="server" Visible="false"  ForeColor="Red" Text="<br />Please complete" />
                    </td>
                    <td>
                        <asp:RadioButtonList ID="RadioButtonListRBExamDilatedPupil" runat="server" CssClass="aspxList" RepeatDirection="Vertical">
                            <asp:ListItem Value='300'>&nbsp;Adequate</asp:ListItem>
                            <asp:ListItem Value='301'>&nbsp;Small</asp:ListItem>
                            <asp:ListItem Value="3">&nbsp;Not Documented</asp:ListItem>
                        </asp:RadioButtonList>
                    </td>
                </tr>
                <!-- Question 6 -->
                <tr class="table_body_bg">
                    <td>
                        <strong>6. Corneal endothelium</strong>
                        <asp:Label ID="LabelRBExamCornealEndothelium" runat="server" Visible="false"  ForeColor="Red" Text="<br />Please complete" />
                    </td>
                    <td>
                        <asp:RadioButtonList id='RadioButtonListRBExamCornealEndothelium' RepeatDirection='Vertical' CssClass='aspxList' runat='server'>
                            <asp:ListItem Value='104'>&nbsp;Normal</asp:ListItem>
                            <asp:ListItem Value='302'>&nbsp;Guttae</asp:ListItem>
                            <asp:ListItem Value='303'>&nbsp;Other Abnormality</asp:ListItem>
                            <asp:ListItem Value="3">&nbsp;Not Documented</asp:ListItem>
                        </asp:RadioButtonList>
                    </td>
                </tr>
                <!-- Question 7 -->
                <tr class="table_body">
                    <td>
                        <strong>7. Presence of pseudoexfoliative material</strong>
                        <asp:Label ID="LabelExamPseudoMaterial" runat="server" Visible="false"  ForeColor="Red" Text="<br />Please complete" />
                    </td>
                    <td>
                        <asp:RadioButton ID="RadioButtonExamPseudoMaterialYes" runat="server" GroupName="ExamPseudoMaterial" text="&nbsp;Yes" />&nbsp;&nbsp;
                        <asp:RadioButton ID="RadioButtonExamPseudoMaterialNo" runat="server" GroupName="ExamPseudoMaterial" text="&nbsp;No" />
                    </td>
                </tr>
                <!-- Question 8 -->
                <tr class="table_body_bg">
                    <td>
                        <strong> 8. Cataract grading</strong>
                        <asp:Label ID="LabelRBExamCataractGrading" runat="server" Visible="false"  ForeColor="Red" Text="<br />Please complete" />
                    </td>
                    <td>
                           <asp:RadioButtonList id='RadioButtonListRBExamCataractGrading' RepeatDirection='Vertical' CssClass='aspxList' runat='server'>
                            <asp:ListItem Value='1'>&nbsp;Documented</asp:ListItem>
                            
                            <asp:ListItem Value="3">&nbsp;Not Documented</asp:ListItem>
                        </asp:RadioButtonList>
                    </td>
                </tr>
                <!-- Question 9 -->
                <tr class="table_body">
                    <td>
                        <strong>9. Dilated fundus exam</strong>
                        <asp:Label ID="LabelExamDilatedFundus" runat="server" Visible="false"  ForeColor="Red" Text="<br />Please complete" />
                    </td>
                    <td>
                        <asp:RadioButton runat='server' ID='RadioButtonExamDilatedFundusYes' GroupName='ExamDilatedFundus' text='&nbsp;Done' />&nbsp;&nbsp;
                        <asp:RadioButton runat='server' ID='RadioButtonExamDilatedFundusNo' GroupName='ExamDilatedFundus' text='&nbsp;Not Done' />
                    </td>
                </tr>
                <!-- Question 10 -->
                <tr class="table_body_bg">
                    <td>
                        <strong>10. Macula</strong>
                        <asp:Label ID="LabelRBExamMacula" runat="server" Visible="false"  ForeColor="Red" Text="<br />Please complete" />    
                    </td>
                    <td>
                        <asp:RadioButtonList id='RadioButtonListRBExamMacula' RepeatDirection='Vertical' CssClass='aspxList' runat='server'>
                            <asp:ListItem Value='105'>&nbsp;Abnormal</asp:ListItem>
                            <asp:ListItem Value='104'>&nbsp;Normal</asp:ListItem>
                            <asp:ListItem Value="3">&nbsp;Not Documented</asp:ListItem>
                        </asp:RadioButtonList>
                    </td>
                </tr>
            </table>
            <br />
            <!-- Pre-Operative management -->
            <table>
                <tr>
                    <th colspan="3" width="910px"><p>Pre-Operative Management</p></th>
                </tr>
                <!-- Question 1 -->
                <tr class="table_body">
                <td width="65%"><strong>1.  Comorbid ocular conditions (more than one may be selected):</strong></td>
                    <td>
                        <asp:CheckBox runat='server' ID='CheckBoxComorbidARMD' text='&nbsp;ARMD' />
                        <br /><asp:CheckBox runat='server' ID='CheckBoxComorbidBlepharitis' text='&nbsp;Blepharitis' />
                        <br /><asp:CheckBox runat='server' ID='CheckBoxComorbidDR' text='&nbsp;Diabetic Retinopathy' />
                        <br /><asp:CheckBox runat='server' ID='CheckBoxComorbidGlaucoma' text='&nbsp;Glaucoma' />
                        <br /><asp:CheckBox runat='server' ID='CheckBoxComorbidPRD' text='&nbsp;Peripheral Retinal Disease' />
                        <br /><asp:CheckBox runat='server' ID='CheckBoxComorbidNone' text='&nbsp;None' />
                        <br /><asp:CheckBox runat='server' ID='CheckBoxComorbidOther' text='&nbsp;Other ' /><br />

                    </td>
                    
                </tr>
                <!-- Question 1a -->
                <tr class="table_body">
                    <td><strong><span id="QP1aA">1a. If &quot;Other,&quot; please specify:</span></strong></td>
                    <td>
                        <asp:TextBox runat='server' ID='TextBoxPreopKerOtherText' size='25' />
                    </td>
                </tr>
                <!-- Question 2 -->
                <tr class="table_body_bg">
                    <td>
                        <strong>2.  Were risks, benefits and alternatives to surgery discussed?</strong>
                        <asp:Label ID="LabelRBPreopRisks" runat="server" Visible="false"  ForeColor="Red" Text="<br />Please complete" />
                    </td>
                    <td>
                        <asp:RadioButtonList id='RadioButtonListRBPreopRisks' RepeatDirection='Horizontal' CssClass='aspxList' runat='server'>
                            <asp:ListItem Value="1">&nbsp;Yes</asp:ListItem>
                            <asp:ListItem Value="2">&nbsp;No</asp:ListItem>
                            <asp:ListItem Value="3">&nbsp;Not Documented</asp:ListItem>
                        </asp:RadioButtonList>
                    </td>
                </tr>
                <!-- Question 3 -->
                <tr class="table_body">
                    <td>
                        <strong>3.  Have the refractive goals and/or options for intraocular lenses and options been explained to the patient and documented in the medical record?</strong>
                        <asp:Label ID="LabelRBPreopRefractiveGoals" runat="server" Visible="false"  ForeColor="Red" Text="<br />Please complete" />
                    </td>
                    <td>
                        <asp:RadioButtonList id='RadioButtonListRBPreopRefractiveGoals' RepeatDirection='Horizontal' CssClass='aspxList' runat='server'>
                            <asp:ListItem Value="1">&nbsp;Yes</asp:ListItem>
                            <asp:ListItem Value="2">&nbsp;No</asp:ListItem>
                            <asp:ListItem Value="3">&nbsp;Not Documented</asp:ListItem>
                        </asp:RadioButtonList>
                    </td>
                </tr>
                <!-- Question 4 -->
                <tr class="table_body_bg">
                    <td>
                        <strong>4.  Was signed informed consent obtained?</strong>
                        <asp:Label ID="LabelRBPreopSignedConsent" runat="server" Visible="false"  ForeColor="Red" Text="<br />Please complete" />
                    </td>
                    <td>
                        <asp:RadioButtonList id='RadioButtonListRBPreopSignedConsent' RepeatDirection='Horizontal' CssClass='aspxList' runat='server'>
                            <asp:ListItem Value="1">&nbsp;Yes</asp:ListItem>
                            <asp:ListItem Value="2">&nbsp;No</asp:ListItem>
                            <asp:ListItem Value="3">&nbsp;Not Documented</asp:ListItem>
                        </asp:RadioButtonList>
                    </td>
                </tr>
                <!-- Question 5 -->
                <tr class="table_body">
                    <td>
                        <strong>5.  Method of axial length measurement</strong>
                        <asp:Label ID="LabelRBPreopAxialLengthMeasure" runat="server" Visible="false"  ForeColor="Red" Text="<br />Please complete" />
                    </td>
                    <td>
                        <asp:RadioButtonList id='RadioButtonListRBPreopAxialLengthMeasure' RepeatDirection='Vertical' CssClass='aspxList' runat='server'>
                            <asp:ListItem Value='304'>&nbsp;IOL Master</asp:ListItem>
                            <asp:ListItem Value='305'>&nbsp;Ultrasound Biometry</asp:ListItem>
                            <asp:ListItem Value='306'>&nbsp;Contact Immersion</asp:ListItem>
                            <asp:ListItem Value="3">&nbsp;Not Done</asp:ListItem>
                        </asp:RadioButtonList>
                    </td>
                </tr>
                <!-- Question 6 -->
                <tr class="table_body_bg">
                    <td>
                        <strong>6.  Method of keratometry performed (more than one may be selected):</strong>
                        <asp:Label ID="LabelPreopKerCalcOtherText" runat="server" Visible="false"  ForeColor="Red" Text="<br />Please complete" />
                    </td>
                    <td>
                        <asp:CheckBox runat='server' ID='CheckBoxPreopKerIOLMaster' text='&nbsp;IOL Master' />
                        <br /><asp:CheckBox runat='server' ID='CheckBoxPreopKerCornealTop' text='&nbsp;Corneal topography' />
                        <br /><asp:CheckBox runat='server' ID='CheckBoxPreopKerManual' text='&nbsp;Manual Keratometry' />
                        <br /><asp:CheckBox runat='server' ID='CheckBoxPreopKerHandheld' text='&nbsp;Handheld automated keratometry' />
                        <br /><asp:CheckBox runat='server' ID='CheckBoxPreopKerOther' text='&nbsp;Other' />
                    </td>
                </tr>
                <!-- Question 6a -->
                <tr class="table_body_bg">
                    <td><strong><span id="QP6aA">6a. If &quot;Other,&quot; please specify:</span></strong></td>
                    <td>
                        <asp:TextBox runat='server' ID='TextBoxPreopKerCalcOtherText' size='25' />
                    </td>
                </tr>
                <!-- Question 7 -->
                <tr class="table_body">
                    <td><strong>7.  Which keratometric value was used for calculating toric IOL power? (select only one)</strong></td>
                    <td>
                        <asp:RadioButtonList id='RadioButtonListRBPreopKer' RepeatDirection='Vertical' CssClass='aspxList' runat='server'>
                            <asp:ListItem Value='304'>&nbsp;IOL Master</asp:ListItem>
                            <asp:ListItem Value='353'>&nbsp;Corneal topography</asp:ListItem>
                            <asp:ListItem Value='354'>&nbsp;Manual Keratometry</asp:ListItem>
                            <asp:ListItem Value='355'>&nbsp;Handheld automated keratometry</asp:ListItem>
                            <asp:ListItem Value='14'>&nbsp;Other</asp:ListItem>
                        </asp:RadioButtonList>
                    </td>
                </tr>
                <!-- Question 7a -->
                <tr class="table_body">
                    <td><strong><span id="QP7aA">7a. If &quot;Other,&quot; please specify:</span></strong></td>
                    <td><asp:TextBox runat='server' ID='TextBoxComorbidOtherText' size='25' /></td>
                </tr>
                <!-- Question 8 -->
                <tr class="table_body_bg">
                    <td>
                        <strong>8.  Keratometric values used for IOL selection</strong>
                        <asp:Label ID="LabelPreopKeFlatValue" runat="server" Visible="false"  ForeColor="Red" Text="<br />Please complete flat value" />
                        <asp:Label ID="LabelPreopKerFlatValue" runat="server" Visible="false"  ForeColor="Red" Text="<br />Please complete" />
                        <asp:Label ID="LabelPreopKerSteepValue" runat="server" Visible="false"  ForeColor="Red" Text="<br />Please complete steep value" />
                    </td>
                    <td>
                        <asp:TextBox runat='server' class="auto" meta="{vMin: '0', vMax: '99'}" ID='TextBoxPreopKeFlatValue1' size='3' />.
                        <asp:TextBox runat='server' class="auto" meta="{vMin: '0', vMax: '99'}" ID='TextBoxPreopKerFlatValue2' size='3' />&nbsp;d&nbsp;&nbsp;x
                        <asp:TextBox runat='server' class="auto" meta="{vMin: '0', vMax: '180'}" ID='TextBoxPreopKerFlatValue3' size='3' />&deg;&nbsp;(flat axis)
                        
                        <br /><asp:TextBox runat='server' class="auto" meta="{vMin: '0', vMax: '99'}" ID='TextBoxPreopKerSteepValue1' size='3' />.
                        <asp:TextBox runat='server' class="auto" meta="{vMin: '0', vMax: '99'}" ID='TextBoxPreopKerSteepValue2' size='3' />&nbsp;d&nbsp;&nbsp;x
                        <asp:TextBox runat='server' class="auto" meta="{vMin: '0', vMax: '180'}" ID='TextBoxPreopKerSteepValue3' size='3' />&deg;&nbsp;(steep axis)
                    </td>
                </tr>
                <!-- Question 9 -->
                <tr class="table_body">
                    <td>
                        <strong>9. What formula was used for IOL spherical power calculation?</strong>
                        <asp:Label ID="LabelRBPreopIOLPowerCalc" runat="server" Visible="false"  ForeColor="Red" Text="<br />Please complete" />
                    </td>
                    <td>
                        <asp:RadioButtonList ID="RadioButtonListRBPreopIOLPowerCalc" runat="server" CssClass="aspxList" RepeatDirection="Vertical">
                            <asp:ListItem Value='307'>&nbsp;Haigis</asp:ListItem>
                            <asp:ListItem Value='308'>&nbsp;Hoffer Q</asp:ListItem>
                            <asp:ListItem Value='309'>&nbsp;Holladay</asp:ListItem>
                            <asp:ListItem Value='310'>&nbsp;Holladay II</asp:ListItem>
                            <asp:ListItem Value='311'>&nbsp;SRK-T</asp:ListItem>
                            <asp:ListItem Value="14">&nbsp;Other</asp:ListItem>
                        </asp:RadioButtonList>
                    </td>
                </tr>
                <!-- Question 9a -->
                <tr class="table_body">
                    <td><strong><span id="QP9aA">9a. If &quot;Other,&quot; please specify:</span></strong></td>
                    <td>
                        <asp:TextBox ID="TextBoxPreopIOLPolwerCalcOther" runat="server" size="25" />
                    </td>
                </tr>
                <!-- Question 10 -->
                <tr class="table_body_bg">
                    <td>
                        <strong>10.  What method was used for calculating toric IOL power and axis?</strong>
                        <asp:Label ID="LabelRBPreopToricIOLCalc" runat="server" Visible="false"  ForeColor="Red" Text="<br />Please complete" />
                    </td>
                    <td>
                        <asp:RadioButtonList id='RadioButtonListRBPreopToricIOLCalc' RepeatDirection='Vertical' CssClass='aspxList' runat='server'>
                            <asp:ListItem Value='312'>&nbsp;Internet based IOL calculator</asp:ListItem>
                            <asp:ListItem Value="14">&nbsp;Other</asp:ListItem>
                        </asp:RadioButtonList>
                    </td>
                </tr>
                <!-- Question 11 -->
                <tr class="table_body">
                    <td>
                        <strong>11. Were the above calculations saved in the patient’s medical record?</strong>
                        <asp:Label ID="LabelPreopToricIOLMedRecord" runat="server" Visible="false"  ForeColor="Red" Text="<br />Please complete" />
                    </td>
                    <td>
                        <asp:RadioButton ID="RadioButtonPreopToricIOLMedRecordYes" runat="server" GroupName="PreopToricIOLMedRecord" text="&nbsp;Yes" />&nbsp;&nbsp;
                        <asp:RadioButton ID="RadioButtonPreopToricIOLMedRecordNo" runat="server" GroupName="PreopToricIOLMedRecord" text="&nbsp;No" />
                    </td>
                </tr>
            </table>
            <br />
            <!-- Management -->
            <table>
                <tr>
                    <th colspan="3" width="910px"><p>Management</p></th>
                </tr>
                <!-- Question 1 -->
                <tr class="table_body">
                    <td width="65%">
                        <strong>1. What spherical refractive target was selected? (to the nearest 0.5 diopters)</strong>
                        <asp:Label ID="LabelRefractiveTarget" runat="server" Visible="false"  ForeColor="Red" Text="<br />Please complete" />
                    </td>
                    <td>
                        <asp:TextBox ID="TextBoxRefractiveTarget" class="auto" meta="{vMin: '-30.00', vMax: '30.00'}" runat="server" size="5" />&nbsp;diopters
                    </td>
                </tr>
                <!-- Question 2 -->
                <tr class="table_body_bg">
                    <td>
                        <strong>2. What was the predicted residual astigmatism? (to the nearest 0.25 diopters)</strong>
                        <asp:Label ID="LabelPredictedResAstigmatism" runat="server" Visible="false"  ForeColor="Red" Text="<br />Please complete" />
                        <asp:Label ID="LabelPredictedResAstigmatismDegree" runat="server" Visible="false"  ForeColor="Red" Text="<br />Please complete degree" />
                    </td>
                    <td>
                        <span style="vertical-align:middle;">
                            <asp:TextBox ID="TextBoxPredictedResAstigmatism" class="auto" meta="{vMin: '0', vMax: '10.00'}" runat="server" size="20" onkeyup="QM2toggleTB();" />&nbsp;d&nbsp;&nbsp;&#64;
                            <asp:TextBox ID="TextBoxPredictedResAstigmatismDegree" class="auto" meta="{vMin: '0', vMax: '180'}" runat="server" size="5" onkeyup="QM2toggleTB();" />&deg;
                        <br />
                        <asp:CheckBox ID="CheckBoxPredictedResAstigmatismNO" Text="Not Documented" runat="server" onclick="QM2toggleCB();"/>
                        </span>
                    </td>
                 
                </tr>
                <!-- Question 3 -->
                <tr class="table_body">
                    <td>
                        <strong>3. IOL brand </strong>
                        <asp:Label ID="LabelRBIOLBrand" runat="server" Visible="false"  ForeColor="Red" Text="<br />Please complete" />
                    </td>
                    <td>
                        <asp:RadioButtonList ID="RadioButtonListRBIOLBrand" runat="server" CssClass="aspxList" RepeatDirection="Horizontal">
                            <asp:ListItem Value='313'>&nbsp;Alcon </asp:ListItem>
                            <asp:ListItem Value="14">&nbsp;Other</asp:ListItem>
                                <asp:ListItem Value="3">&nbsp;Not documented</asp:ListItem>
                        </asp:RadioButtonList>
                    </td>
                </tr>
                <!-- Question 3a -->
                <tr class="table_body">
                    <td><strong><span id="QP3aA">3a. If &quot;Other,&quot; please specify:</span></strong></td>
                    <td>
                        <asp:TextBox runat="server" ID="TextBoxIOLBrandOtherText" size="25" />
                    </td>
                </tr>
                <!-- Question 4 -->
                <tr class="table_body_bg">
                    <td>
                        <strong>4. IOL power selected</strong>
                        <asp:Label ID="LabelIOLPowerSelectedSpherical" runat="server" Visible="false"  ForeColor="Red" Text="<br />Please complete spherical" />
                        <asp:Label ID="LabelIOLPowerSelectedCylindrical" runat="server" Visible="false"  ForeColor="Red" Text="<br />Please complete cylindrical" />
                    </td>
                    <td>
                        <asp:TextBox runat='server' class="auto" meta="{vMin: '0', vMax: '40.00'}" ID='TextBoxIOLPowerSelectedSpherical' size='5' />&nbsp;d&nbsp;(spherical power)
                        <br /><asp:TextBox runat='server' class="auto" meta="{vMin: '0', vMax: '10.00'}" ID='TextBoxIOLPowerSelectedCylindrical' size='5' />&nbsp;d&nbsp;(cylindrical power)&nbsp;(in IOL planer)
                    </td>
                </tr>
                <!-- Question 5 -->
                <tr class="table_body">
                    <td>
                        <strong>5. Planned axis of IOL placement </strong>
                        <asp:Label ID="LabelIOLPlacementPlannedAxis" runat="server" Visible="false"  ForeColor="Red" Text="<br />Please complete" />
                    </td>
                    <td>
                        <asp:TextBox runat='server' class="auto" meta="{vMin: '0', vMax: '180'}" ID='TextBoxIOLPlacementPlannedAxis' size='5' />&deg;
                    </td>
                </tr>
                <!-- Question 6 -->
                <tr class="table_body_bg">
                    <td>
                        <strong>6.  Which surgical intervention was used?</strong>
                        <asp:Label ID="LabelRBSurgicalIntervention" runat="server" Visible="false"  ForeColor="Red" Text="<br />Please complete" />
                    </td>
                    <td>
                        <asp:RadioButtonList id='RadioButtonListRBSurgicalIntervention' RepeatDirection='Vertical' CssClass='aspxList' runat='server'>
                            <asp:ListItem Value='314'>&nbsp;Phacoemulsification</asp:ListItem>
                            <asp:ListItem Value='315'>&nbsp;Large incision extracapsular extraction</asp:ListItem>
                            <asp:ListItem Value='316'>&nbsp;Manual small incision cataract surgery (SICS)</asp:ListItem>
                        </asp:RadioButtonList>
                    </td>
                </tr>
             
                <!-- Question 8 -->
                <tr class="table_body">
                    <td>
                        <strong>7. IOL placement</strong>
                        <asp:Label ID="LabelRBIOLPlacement" runat="server" Visible="false"  ForeColor="Red" Text="<br />Please complete" />
                    </td>
                    <td>
                        <asp:RadioButtonList id='RadioButtonListRBIOLPlacement' RepeatDirection='Vertical' CssClass='aspxList' runat='server'>
                            <asp:ListItem Value='322'>&nbsp;Anterior chamber</asp:ListItem>
                            <asp:ListItem Value='323'>&nbsp;Posterior capsule</asp:ListItem>
                            <asp:ListItem Value='324'>&nbsp;Sulcus</asp:ListItem>
                            <asp:ListItem Value='325'>&nbsp;Sulcus with suture fixation</asp:ListItem>
                            <asp:ListItem Value='326'>&nbsp;Iris fixated</asp:ListItem>
                        </asp:RadioButtonList>
                    </td>
                </tr>
                <!-- Question 9 -->
                <tr class="table_body_bg">
                    <td>
                        <strong>8. Was the eye marked with the patient in the sitting or supine position?</strong>
                        <asp:Label ID="LabelRBPreopEyeMarkedPosition" runat="server" Visible="false"  ForeColor="Red" Text="<br />Please complete" />   
                    </td>
                    <td>
                        <asp:RadioButtonList id='RadioButtonListRBPreopEyeMarkedPosition' RepeatDirection='Vertical' CssClass='aspxList' runat='server'>
                            <asp:ListItem Value='317'>&nbsp;Sitting position</asp:ListItem>
                            <asp:ListItem Value='318'>&nbsp;Supine position</asp:ListItem>
                            <asp:ListItem Value='319'>&nbsp;Eye was not marked</asp:ListItem>
                        </asp:RadioButtonList>
                    </td>
                </tr>
                <!-- Question 10 -->
                <tr class="table_body">
                    <td>
                        <strong>9. Were intraoperative complications encountered?</strong>
                        <asp:Label ID="LabelSurgIntraopComplications" runat="server" Visible="false"  ForeColor="Red" Text="<br />Please complete" />
                    </td>
                    <td>
                        <asp:RadioButton runat='server' ID='RadioButtonSurgIntraopComplicationsYes' GroupName='SurgIntraopComplications' text='&nbsp;Yes' />&nbsp;&nbsp;
                        <asp:RadioButton runat='server' ID='RadioButtonSurgIntraopComplicationsNo' GroupName='SurgIntraopComplications' text='&nbsp;No' />
                    </td>
                </tr>
                <!-- Question 10a -->
                <tr class="table_body">
                    <td><strong><span id="QM10aA">9a. If &quot;Yes,&quot; which intraoperative complications were encountered?</span></strong></td>
                    <td>
                        <span id="QM10aB">
                        <asp:CheckBox runat='server' ID='CheckBoxSurgComplicationIris' text='&nbsp;Iris prolapse or injury' />
                        <br/><asp:CheckBox runat='server' ID='CheckBoxSurgComplicationLensFragVitreous' text='&nbsp;Loss of lens fragments into vitreous' />
                        <br /><asp:CheckBox runat='server' ID='CheckBoxSurgComplicationCapsularRupture' text='&nbsp;Posterior capsular rupture' />
                        <br /><asp:CheckBox runat='server' ID='CheckBoxSurgComplicationVitreousLoss' text='&nbsp;Vitreous loss' />
                        <br /><asp:CheckBox runat='server' ID='CheckBoxSurgComplicationSuprchoroidal' text='&nbsp;Suprchoroidal hemorrhage' />
                        <br /><asp:CheckBox runat='server' ID='CheckBoxSurgComplicationZonular' text='&nbsp;Zonular dehiscence' />
                        <br /><asp:CheckBox runat='server' ID='CheckBoxSurgComplicationNone' text='&nbsp;None' />
                        <br /><asp:CheckBox runat='server' ID='CheckBoxSurgComplicationOther' text='&nbsp;Other' />
                        </span>
                    </td>
                </tr>
                <!-- Question 10b -->
                <tr class="table_body">
                    <td><strong><span id="QM10bA">9b. If &quot;Other,&quot; please specify</span></strong></td>
                    <td>
                        <asp:TextBox runat="server" ID="TextBoxSurgComplicationOtherText" size="25" />          
                    </td>
                </tr>
                <!-- Question 11 -->
                <tr class="table_body_bg">
                    <td>
                        <strong>10. Were adjunctive procedures necessary?</strong>
                        <asp:Label ID="LabelSurgAdjunctiveProc" runat="server" Visible="false"  ForeColor="Red" Text="<br />Please complete" />
                    </td>
                    <td>
                        <asp:RadioButton runat='server' ID='RadioButtonSurgAdjunctiveProcYes' GroupName='SurgAdjunctiveProc' text='&nbsp;Yes' />&nbsp;&nbsp;
                        <asp:RadioButton runat='server' ID='RadioButtonSurgAdjunctiveProcNo' GroupName='SurgAdjunctiveProc' text='&nbsp;No' />   
                    </td>
                </tr>
                <!-- Question 11a -->
                <tr class="table_body_bg">
                    <td><strong><span id="QM11aA">10a. If &quot;Yes,&quot; please indicate which adjunctive procedures were necessary</span></strong></td>
                    <td>
                        <span id="QM11aB">
                        <asp:CheckBox runat='server' ID='CheckBoxSurgAdjunctiveProcAntVitrectomy' text='&nbsp;Anterior Vitrectomy' />
                        <br /><asp:CheckBox runat='server' ID='CheckBoxSurgAdjunctiveProcCapsularTensionRing' text='&nbsp;Capsular Tension Ring' />
                        <br /><asp:CheckBox runat='server' ID='CheckBoxSurgAdjunctiveProcIrisRetractors' text='&nbsp;Iris Retractors or Expanders' />
                        <br /><asp:CheckBox runat='server' ID='CheckBoxSurgAdjunctiveProcOther' text='&nbsp;Other&nbsp;' />
                        </span>
                    </td>
                </tr>
                <!-- Question 11b -->
                <tr class="table_body_bg">
                    <td><strong><span id="QM11bA">10b. If &quot;Other,&quot; please specify</span></strong></td>
                    <td>
                        <asp:TextBox runat='server' ID='TextBoxSurgAdjunctiveProcOtherText' size='15' />
                    </td>
                </tr>
            </table>
            <br />
            <!-- Post-Operative care -->
            <table>
                <tr>
                    <th colspan="3" width="910px"><p>Post-Operative Care</p></th>
                </tr>
                <!-- Question 1 -->
                <tr class="table_body">
                    <td width="65%">
                        <strong>1. Were antibiotic drops prescribed?</strong>
                        <asp:Label ID="LabelPostopAntibioticDrops" runat="server" Visible="false"  ForeColor="Red" Text="<br />Please complete" />
                    </td>
                    <td>
                        <asp:RadioButton runat='server' ID='RadioButtonPostopAntibioticDropsYes' GroupName='PostopAntibioticDrops' text='&nbsp;Yes' />&nbsp;&nbsp;
                        <asp:RadioButton runat='server' ID='RadioButtonPostopAntibioticDropsNo' GroupName='PostopAntibioticDrops' text='&nbsp;No' />
                    </td>
                </tr>
                <!-- Question 2 -->
                <tr class="table_body_bg">
                    <td>
                        <strong>2. Were anti-inflammatory drops prescribed?</strong>
                        <asp:Label ID="LabelPostopAntiInflamDrops" runat="server" Visible="false"  ForeColor="Red" Text="<br />Please complete" />
                    </td>
                    <td>
                        <asp:RadioButton runat='server' ID='RadioButtonPostopAntiInflamDropsYes' GroupName='PostopAntiInflamDrops' text='&nbsp;Yes' />&nbsp;&nbsp;
                        <asp:RadioButton runat='server' ID='RadioButtonPostopAntiInflamDropsNo' GroupName='PostopAntiInflamDrops' text='&nbsp;No' />
                    </td>
                </tr>
                <!-- Question 3 -->
                <tr class="table_body">
                    <td>
                        <strong>3. Were verbal and/or written postoperative instructions given?</strong>
                        <asp:Label ID="LabelRBPostopInstructions" runat="server" Visible="false"  ForeColor="Red" Text="<br />Please complete" />
                    </td>
                    <td>
                        <asp:RadioButtonList id='RadioButtonListRBPostopInstructions' RepeatDirection='Horizontal' CssClass='aspxList' runat='server'>
                            <asp:ListItem Value='1'>&nbsp;Yes</asp:ListItem>
                            <asp:ListItem Value='2'>&nbsp;No</asp:ListItem>
                            <asp:ListItem Value='3'>&nbsp;Not documented</asp:ListItem>
                        </asp:RadioButtonList>
                    </td>
                </tr>
            </table>
            <!-- Outcomes -->
            <table>
                <tr>
                    <th colspan="3" width="910px"><p>Outcomes</p></th>
                </tr>
                <!-- Question 1 -->
                <tr class="table_body">
                    <td width="65%"><strong>1. Post-operative refraction</strong>&nbsp;<a href="#"><img id="QO1" src="../../common/images/tip.gif" alt="Tip" /></a></td>
                    <td>
                        <table class="aspxList">
                            <tr>
                                <td></td>
                                <td>
                                    <asp:TextBox runat='server' meta="{vMin: '-30.00', vMax: '30.00'}" class="auto" ID='TextBoxPostopRefraction1' size='3' onkeyup="toggleQO1CB();" />&nbsp;sph
                                </td>
                            </tr>
                            <tr>
                                <td>+&nbsp;</td>
                                <td>
                                    <asp:TextBox runat='server' meta="{vMin: '0.00', vMax: '99.99'}" class="auto" ID='TextBoxPostopRefraction2' size='3' onkeyup="toggleQO1CB();" />&nbsp;cyl
                                </td>
                            </tr>
                            <tr>
                                <td></td>
                                <td>
                                    <asp:TextBox runat='server' meta="{vMin: '0.00', vMax: '180.00'}" class="auto" ID='TextBoxPostopRefraction3' size='3' onkeyup="toggleQO1CB();" />&nbsp;axis
                                </td>
                            </tr>
                        </table>
                        <br /><asp:CheckBox runat='server' ID='CheckBoxPostopRefractionNA' text='&nbsp;Not done' onclick="toggleQO1TB();" />
                    </td>
                </tr>
                <!-- Question 2 -->
                <tr class="table_body_bg">
                    <td>
                        <strong>2. Residual refractive astigmatism? (to the nearest 0.25 diopters)</strong>
                        <asp:Label ID="LabelPostopAstigmatism" runat="server" Visible="false"  ForeColor="Red" Text="<br />Please complete" />
                        <asp:Label ID="LabelPostopAstigmatismDegree" runat="server" Visible="false"  ForeColor="Red" Text="<br />Please complete degree" />
                    </td>
                    <td>
                        <asp:TextBox runat="server" class="auto" meta="{vMin: '0', vMax: '10.00'}" ID="TextBoxPostopAstigmatism" size="5" onkeyup="QO2toggleCB();" />&nbsp;d&nbsp;&nbsp;&#64;&nbsp;<asp:TextBox runat="server" class="auto" meta="{vMin: '0', vMax: '180'}" ID="TextBoxPostopAstigmatismDegree" size="5" onkeyup="QO2toggleCB();" />&deg;
                        <br />
                        <asp:CheckBox ID="CheckBoxAstigmatismDegreeNA" Text="Not documented" runat="server" onclick="QO2toggleTB();"/>
                    </td>
                </tr>
                <!-- Question 3 -->
                <tr class="table_body">
                    <td>
                        <strong>3. Was the IOL placement within 15 degrees of the intended axis?</strong>
                        <asp:Label ID="LabelRBPostopIOLWithin15Degrees" runat="server" Visible="false"  ForeColor="Red" Text="<br />Please complete" />    
                    </td>
                    <td>
                        <asp:RadioButtonList id='RadioButtonListRBPostopIOLWithin15Degrees' RepeatDirection='Horizontal' CssClass='aspxList' runat='server'>
                            <asp:ListItem Value='1'>&nbsp;Yes</asp:ListItem>
                            <asp:ListItem Value='2'>&nbsp;No</asp:ListItem>
                            <asp:ListItem Value='3'>&nbsp;Not documented</asp:ListItem>
                        </asp:RadioButtonList>
                    </td>
                </tr>
                <!-- Question 4 -->
                <tr class="table_body_bg">
                    <td>
                        <strong>4. Best corrected visual acuity within 90 days of surgery</strong>&nbsp;<a href="#"><img id="QO4" src="../../common/images/tip.gif" alt="Tip" /></a>
                        <asp:Label ID="LabelPostopBCVA" runat="server" Visible="false"  ForeColor="Red" Text="<br />Please complete" />
                    </td>
                    <td>
                        20/ <asp:DropDownList ID='DropDownListPostopBCVA' DataValueField='examValue' DataTextField='examLabel' runat='server' />
                    </td>
                </tr>
                <!-- Question 4a -->
                <tr class="table_body_bg">
                    <td><strong><span id="QOu4aA">4a. If above acuity worse than 20/40, what is the reason?</span></strong>
                    <asp:Label ID="LabelRBPostopBCVAReason" runat="server" Visible="false"  ForeColor="Red" Text="<br />Please complete" />
                    </td>
                    <td>
                        <span id="QOu4aB">
                        <asp:RadioButtonList id='RadioButtonListRBPostopBCVAReason' RepeatDirection='Vertical' CssClass='aspxList' runat='server'>
                            <asp:ListItem Value='320'>&nbsp;Macular edema</asp:ListItem>
                            <asp:ListItem Value='321'>&nbsp;Macular degeneration</asp:ListItem>
                            <asp:ListItem Value='3'>&nbsp;Not documented</asp:ListItem>
                            <asp:ListItem Value='14'>&nbsp;Other</asp:ListItem>
                        </asp:RadioButtonList>
                        </span>
                    </td>
                </tr>
                <!-- Question 4b -->
                <tr class="table_body_bg">
                    <td><strong><span id="QOu4bA">4b. If &quot;Other,&quot; please specify</span></strong>
                     <asp:Label ID="LabelRBPostopBCVAReasonOtherText" runat="server" Visible="false"  ForeColor="Red" Text="<br />Please complete" />
                    </td>
                    <td>
                        <asp:TextBox runat='server' ID='TextBoxRBPostopBCVAReasonOtherText' size='25' />
                    </td>
                </tr>
                <!-- Question 5 -->
                <tr class="table_body">
                    <td><strong>5. Were any major post-operative complications encountered?</strong></td>
                    <td>
                        <asp:CheckBox runat='server' ID='CheckBoxPostopComplicationsNone' text='&nbsp;None' />
                        <br /><asp:CheckBox runat='server' ID='CheckBoxPostopComplicationsCME' text='&nbsp;CME' />
                        <br /><asp:CheckBox runat='server' ID='CheckBoxPostopComplicationsDislocatedIOL' text='&nbsp;Dislocated IOL' />
                        <br /><asp:CheckBox runat='server' ID='CheckBoxPostopComplicationsEndophthalmits' text='&nbsp;Endophthalmitis' />
                        <br /><asp:CheckBox runat='server' ID='CheckBoxPostopComplicationsNewGlaucoma' text='&nbsp;New onset of glaucoma requiring therapy beyond 90 days postoperative' />
                        <br /><asp:CheckBox runat='server' ID='CheckBoxPostopComplicationsPseudophakicKer' text='&nbsp;Pseudophakic bullous keratopathy' />
                        <br /><asp:CheckBox runat='server' ID='CheckBoxPostopComplicationsLensFragAnterior' text='&nbsp;Retained lens fragments (anterior chamber)' />
                        <br /><asp:CheckBox runat='server' ID='CheckBoxPostopComplicationsLensFragVitreous' text='&nbsp;Retained lens fragments (vitreous cavity)' />
                        <br /><asp:CheckBox runat='server' ID='CheckBoxPostopComplicationsRetinalDetach' text='&nbsp;Retinal detachment' />
                        <br /><asp:CheckBox runat='server' ID='CheckBoxPostopComplicationsRotationAbove15' text='&nbsp;Rotation of IOL &gt; 15&deg; from intended axis' />
                        <br /><asp:CheckBox runat='server' ID='CheckBoxPostopComplicationsTASS' text='&nbsp;Toxic anterior segment syndrome (TASS)' />
                        <br /><asp:CheckBox runat='server' ID='CheckBoxPostopComplicationsWoundLeak' text='&nbsp;Wound leak or dehiscence' />
                        <br /><asp:CheckBox runat='server' ID='CheckBoxPostopComplicationWrongPower' text='&nbsp;Wrong power IOL' />
                    </td>
                </tr>
                <!-- Question 6 -->
                <tr class="table_body_bg">
                    <td>
                        <strong>6. Was further surgery necessary within 90 days of cataract surgery? </strong>
                        <asp:Label ID="LabelPostopFurtherSurg" runat="server" Visible="false"  ForeColor="Red" Text="<br />Please complete" />
                    </td>
                    <td>
                        <asp:RadioButton runat='server' ID='RadioButtonPostopFurtherSurgYes' GroupName='PostopFurtherSurg' text='&nbsp;Yes' />&nbsp;&nbsp;
                        <asp:RadioButton runat='server' ID='RadioButtonPostopFurtherSurgNo' GroupName='PostopFurtherSurg' text='&nbsp;No' />
                    </td>
                </tr>
                <!-- Question 6a -->
                <tr class="table_body_bg">
                    <td><strong><span id="QOu6aA">6a. If &quot;Yes,&quot; specify reason and procedure</span></strong></td>
                    <td>
                        <asp:TextBox runat='server' ID='TextBoxPostopFurtherSurgReason' size='25' />
                    </td>
                </tr>
                <!-- Question 7 -->
                <tr class="table_body">
                    <td>
                        <strong>7. Was YAG capsulotomy necessary within 90 days of surgery?</strong>
                        <asp:Label ID="LabelRBYagNeeded" runat="server" Visible="false"  ForeColor="Red" Text="<br />Please complete" />
                    </td>
                    <td>    
                        <asp:RadioButtonList id='RadioButtonListRBYagNeeded' RepeatDirection='Horizontal' CssClass='aspxList' runat='server'>
                            <asp:ListItem Value='1'>&nbsp;Yes</asp:ListItem>
                            <asp:ListItem Value='2'>&nbsp;No</asp:ListItem>
                            <asp:ListItem Value='3'>&nbsp;Not documented</asp:ListItem>
                        </asp:RadioButtonList>
                    </td>
                </tr>
            </table>
            <div class="button-box">
                <asp:LinkButton ID="LinkButtonBackToDashboard" runat="server" Text="Back To Chart Registration" PostBackUrl="../PatientChartRegistration.aspx?CycleNumber=1" Visible="false" CssClass="button" />
                <asp:LinkButton ID="ButtonSubmit"  OnClick="ButtonSubmit_Click" runat="server" Text="Submit Chart" CssClass="button" />
            </div>
        </div>
        <!-- ION pim ends -->
    </div>
</asp:Content>

