﻿<%@ Page Title="" Language="C#" MasterPageFile="~/PIMMasterPage.master" AutoEventWireup="true" CodeFile="PtosisChart.aspx.cs" Inherits="abo_PtosisChart" %>

<asp:Content ID="Content1" ContentPlaceHolderID="head" Runat="Server">
    <script type="text/javascript" language="javascript" src="http://ajax.googleapis.com/ajax/libs/jquery/1.7.1/jquery.min.js"></script>
    <link type="text/css" href="../../common/css/atooltip.css" rel="stylesheet"  media="screen" />
	<script type="text/javascript" src="../../common/js/jquery.atooltip.js"></script>
    <script type="text/javascript" language="javascript">
        //      Either enables or disables input boxes
        function enabledControl(el, disabled, checked) {
            //alert("Hello world from enabled control");
            try {
                if (disabled) {
                    el.disabled = disabled;
                    el.setAttribute("disabled", true);
                }
                else {
                    el.disabled = disabled;
                    el.removeAttribute("disabled");
                }

                if (checked == true)
                    el.checked = false;
            }
            catch (E) {
            }
            if (el.childNodes && el.childNodes.length > 0) {
                for (var x = 0; x < el.childNodes.length; x++) {
                    enabledControl(el.childNodes[x], disabled, checked);
                }
            }
        }
        //      Either enables or disables dropdown boxes
        function enabledControlDropDown(el, disabled, checked) {
            //alert("Hello world from enabled control");
            try {
                if (disabled) {
                    el.disabled = disabled;
                    el.setAttribute("disabled", true);
                }
                else {
                    el.disabled = disabled;
                    el.removeAttribute("disabled");
                }

                if (checked == true)
                    el.options[0].selected = true;
            }
            catch (E) {
            }
            if (el.childNodes && el.childNodes.length > 0) {
                for (var x = 0; x < el.childNodes.length; x++) {
                    enabledControl(el.childNodes[x], disabled, checked);
                }
            }
        }
        //      Either enables or disables text boxes
        function enabledControlTextField(el, disabled, checked) {
            //alert("Hello world from enabled control");
            try {
                if (disabled) {
                    el.disabled = disabled;
                    el.setAttribute("disabled", true);
                }
                else {
                    el.disabled = disabled;
                    el.removeAttribute("disabled");
                }

                if (checked == true) {
                    el.value = '';
                }
            }
            catch (E) {
            }
            if (el.childNodes && el.childNodes.length > 0) {
                for (var x = 0; x < el.childNodes.length; x++) {
                    enabledControl(el.childNodes[x], disabled, checked);
                }
            }
        }

        function generate(arr1, arr2, istrue) {
            if (istrue == true) {
                for (var i = 0; i < arr1.length; i++) {
                    document.getElementById(arr1[i]).style.color = "gray";
                }
                for (var i = 0; i < arr2.length; i++) {
                    $(arr2[i] + ' :input').attr('disabled', true);
                    $(arr2[i] + ' :input').removeAttr("checked");
                }
            }
            if (istrue == false) {
                for (var i = 0; i < arr1.length; i++) {
                    document.getElementById(arr1[i]).style.color = "#333";
                }
                for (var i = 0; i < arr2.length; i++) {
                    $(arr2[i] + ' :input').removeAttr('disabled');
                }
            }
        }

    </script>

    <script  type="text/javascript" language="javascript">
        function toggleQI4TB() {
            document.getElementById("<%= TextBoxPreopODMRD1.ClientID %>").value = '';
            document.getElementById("<%= TextBoxPreopOSMRD1.ClientID %>").value = '';
        }

        function toggleQI4CB() {
            document.getElementById("<%= CheckBoxPreopNA.ClientID %>").checked = false;
        }

        function toggleQP1TB() {
            document.getElementById("<%= TextBoxLevatorExcursionOD.ClientID %>").value = '';
            document.getElementById("<%= TextBoxLevatorExcursionOS.ClientID %>").value = '';
        }

        function toggleQP1CB() {
            document.getElementById("<%= CheckBoxLevatorExcursionNA.ClientID %>").checked = false;
        }

        $(document).ready(function() {
            $("#<%= CheckBoxEtiologyOther.ClientID %>").click(function() {
                if ($('#<%= CheckBoxEtiologyOther.ClientID %>').attr("checked") == "checked") {
                    enabledControlTextField(document.getElementById("<%= TextBoxEtiologyOtherText.ClientID %>"), false, false);
                }
                else {
                    enabledControlTextField(document.getElementById("<%= TextBoxEtiologyOtherText.ClientID %>"), true, true);
                }
            });

            $("#<%= RadioButtonListRBPtosisProc.ClientID %>").click(function() {
                if ($('#<%= RadioButtonListRBPtosisProc.ClientID %>').find('input:checked').val() == ('14')) {
                    enabledControlTextField(document.getElementById("<%= TextBoxPtosisProcOtherText.ClientID %>"), false, false);
                }
                else {
                    enabledControlTextField(document.getElementById("<%= TextBoxPtosisProcOtherText.ClientID %>"), true, true);
                }
            });

            $("#<%= CheckBoxPostopSTOther.ClientID %>").click(function() {
                if ($('#<%= CheckBoxPostopSTOther.ClientID %>').attr("checked") == "checked") {
                    enabledControlTextField(document.getElementById("<%= TextBoxPostopSTOtherText.ClientID %>"), false, false);
                }
                else {
                    enabledControlTextField(document.getElementById("<%= TextBoxPostopSTOtherText.ClientID %>"), true, true);
                }
            });

            $("#<%= CheckBoxPostopLTOther.ClientID %>").click(function() {
                if ($('#<%= CheckBoxPostopLTOther.ClientID %>').attr("checked") == "checked") {
                    enabledControlTextField(document.getElementById("<%= TextBoxPostopLTOtherText.ClientID %>"), false, false);
                }
                else {
                    enabledControlTextField(document.getElementById("<%= TextBoxPostopLTOtherText.ClientID %>"), true, true);
                }
            });

            if ($('#<%= CheckBoxEtiologyOther.ClientID %>').attr("checked") == "checked") {
                enabledControlTextField(document.getElementById("<%= TextBoxEtiologyOtherText.ClientID %>"), false, false);
            }
            else {
                enabledControlTextField(document.getElementById("<%= TextBoxEtiologyOtherText.ClientID %>"), true, true);
            }

            if ($('#<%= RadioButtonListRBPtosisProc.ClientID %>').find('input:checked').val() == ('14')) {
                enabledControlTextField(document.getElementById("<%= TextBoxPtosisProcOtherText.ClientID %>"), false, false);
            }
            else {
                enabledControlTextField(document.getElementById("<%= TextBoxPtosisProcOtherText.ClientID %>"), true, true);
            }

            if ($('#<%= CheckBoxPostopSTOther.ClientID %>').attr("checked") == "checked") {
                enabledControlTextField(document.getElementById("<%= TextBoxPostopSTOtherText.ClientID %>"), false, false);
            }
            else {
                enabledControlTextField(document.getElementById("<%= TextBoxPostopSTOtherText.ClientID %>"), true, true);
            }

            if ($('#<%= CheckBoxPostopLTOther.ClientID %>').attr("checked") == "checked") {
                enabledControlTextField(document.getElementById("<%= TextBoxPostopLTOtherText.ClientID %>"), false, false);
            }
            else {
                enabledControlTextField(document.getElementById("<%= TextBoxPostopLTOtherText.ClientID %>"), true, true);
            }

            // Post-Operative Care - Question 3a
            $("#<%= RadioButtonPostopIncisionStatusYes.ClientID %>").click(function () {
                if ($('#<%= RadioButtonPostopIncisionStatusYes.ClientID %>').attr("checked") == "checked") {
                    var myaray = new Array("QPO3aA", "QPO3aB");
                    var myarray2 = new Array('#QPO3aB');
                    generate(myaray, myarray2, false);

                    enabledControlTextField(document.getElementById("<%= TextBoxPostOpFollowedMonths.ClientID %>"), false, false);
                    enabledControlTextField(document.getElementById("<%= TextBoxPostOpFollowedDays.ClientID %>"), false, false);
                }
                else {
                    var myaray = new Array("QPO3aA", "QPO3aB");
                    var myarray2 = new Array('#QPO3aB');
                    generate(myaray, myarray2, true);

                    enabledControlTextField(document.getElementById("<%= TextBoxPostOpFollowedMonths.ClientID %>"), true, true);
                    enabledControlTextField(document.getElementById("<%= TextBoxPostOpFollowedDays.ClientID %>"), true, true);
                }
            });
            $("#<%= RadioButtonPostopIncisionStatusNo.ClientID %>").click(function () {
                if ($('#<%= RadioButtonPostopIncisionStatusYes.ClientID %>').attr("checked") == "checked") {
                    var myaray = new Array("QPO3aA", "QPO3aB");
                    var myarray2 = new Array('#QPO3aB');
                    generate(myaray, myarray2, false);

                    enabledControlTextField(document.getElementById("<%= TextBoxPostOpFollowedMonths.ClientID %>"), false, false);
                    enabledControlTextField(document.getElementById("<%= TextBoxPostOpFollowedDays.ClientID %>"), false, false);
                }
                else {
                    var myaray = new Array("QPO3aA", "QPO3aB");
                    var myarray2 = new Array('#QPO3aB');
                    generate(myaray, myarray2, true);

                    enabledControlTextField(document.getElementById("<%= TextBoxPostOpFollowedMonths.ClientID %>"), true, true);
                    enabledControlTextField(document.getElementById("<%= TextBoxPostOpFollowedDays.ClientID %>"), true, true);
                }
            });
            if ($('#<%= RadioButtonPostopIncisionStatusYes.ClientID %>').attr("checked") == "checked") {
                var myaray = new Array("QPO3aA", "QPO3aB");
                var myarray2 = new Array('#QPO3aB');
                generate(myaray, myarray2, false);

                enabledControlTextField(document.getElementById("<%= TextBoxPostOpFollowedMonths.ClientID %>"), false, false);
                enabledControlTextField(document.getElementById("<%= TextBoxPostOpFollowedDays.ClientID %>"), false, false);
            }
            else {
                var myaray = new Array("QPO3aA", "QPO3aB");
                var myarray2 = new Array('#QPO3aB');
                generate(myaray, myarray2, true);

                enabledControlTextField(document.getElementById("<%= TextBoxPostOpFollowedMonths.ClientID %>"), true, true);
                enabledControlTextField(document.getElementById("<%= TextBoxPostOpFollowedDays.ClientID %>"), true, true);
            }

        });
    </script>
    <style type="text/css">
        .bginputa{
	        float:right;
	        margin:21px 0 0 1px;
	        background: url(../common/images/orange_button_with_arrow.png) no-repeat;
	        overflow:hidden;
	        height:35px;

        }
        .bginputa a{
	        color: white;
	        text-align:center;
	        height:35px;
	        line-height:28px;
	        padding:0 14px 15px  ;
	        cursor:pointer;
	        float:left;
	        border:none;
	        text-decoration:none;
	        font-family:Arial, Helvetica, sans-serif; 
	        font-size:12px; 
	        font-weight:bold; 
	        letter-spacing: 0px;
        }
        .bginputa a:hover{
	        text-decoration:none;
        }
        .right {
            text-align:right;
        }
    </style>
    <script type="text/javascript">
        $(function() {
            $('#QH3').aToolTip({
                clickIt: true,
                tipContent: 'It is important to document the medical necessity or cosmetic concerns of correcting the ptosis'
            });
        });
        $(function() {
            $('#QH9').aToolTip({
                clickIt: true,
                tipContent: 'The combination of Lasik and upper eyelid surgery within 6 months has been associated with dry eye symptoms'
            });
        });
        $(function() {
            $('#QH10').aToolTip({
                clickIt: true,
                tipContent: 'Ptosis surgery may make dry eye symptoms worse'
            });
        });
        $(function() {
            $('#QI2').aToolTip({
                clickIt: true,
                tipContent: 'It is important to rule out involvement of other cranial neuropathies; patients with poor motility are at increased risk for corneal de-compensation after ptosis repair'
            });
        });
        $(function() {
            $('#QI4').aToolTip({
                clickIt: true,
                tipContent: 'The amount of ptosis as reflected by MRD1 is an important parameter in deciding how best to correct the ptosis, as well as a reference indicator as to the success of any corrective procedure'
            });
        });
        $(function() {
            $('#QP1').aToolTip({
                clickIt: true,
                tipContent: 'Levator excursion is an important indicator as to the etiology of the ptosis and a parameter in deciding how best to correct the ptosis'
            });
        });
        $(function() {
            $('#QP2').aToolTip({
                clickIt: true,
                tipContent: 'May not be necessary in aesthetic cases; May include lids down and elevated'
            });
        });
        $(function() {
            $('#QP4').aToolTip({
                clickIt: true,
                tipContent: 'Dry eye is a common postoperative complication of ptosis repair and is more severe in patients with preexisting signs and symptoms'
            });
        });
        $(function() {
            $('#QP5').aToolTip({
                clickIt: true,
                tipContent: 'Corneal anesthesia may be a relative contraindication to ptosis repair'
            });
        });
    </script>
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder1" Runat="Server">
    <asp:HiddenField ID="HiddenFieldRecordIdentifier" runat="server"/>
        <!--  pim -->
        <div class="pim">
            <div class="record-ident clearfix">
                <h3 class="record-first">RECORD IDENTIFIER: <asp:Literal runat="server" ID="LiteralRecordIdentifier"></asp:Literal></h3>
                <h3 class="record-second"><asp:Literal runat="server" ID="LiteralAbstractionNumber"></asp:Literal></h3>
            </div>
            <!-- History -->
            <table>
                <tr>
                    <th colspan="3" width="910px"><p>History</p></th>
                </tr>
                <!-- Question 1 -->
                <tr class="table_body">
                    <td width="70%"><strong>1. Date of Birth</strong>
                         <br />
                    <asp:Label ID="LabelMonthOfBirth" runat="server" Visible="false"  ForeColor="Red" Text="Please enter Month  "></asp:Label><br />
                    <asp:Label ID="LabelYearOfBirth" runat="server" Visible="false"  ForeColor="Red"  Text="Please enter Year "></asp:Label>
                    </td>
                    <td>
                        <asp:DropDownList ID="DropDownListMonthOfBirth" DataValueField="MonthID" DataTextField="MonthName" runat="server" />
                        <asp:DropDownList ID="DropDownListYearOfBirth" DataValueField="YearID" DataTextField="YearName" runat="server" />
                    </td>
                </tr>
                <!-- Question 2 -->
                <tr class="table_body_bg">
                    <td><strong>2. Gender</strong>
                        <br />
                        <asp:Label ID="LabelRBGender" runat="server" Visible="false"  ForeColor="Red" Text="Please complete"></asp:Label>
                    </td>
                    <td>
                        <asp:RadioButtonList id="RadioButtonListRBGender" RepeatDirection="Horizontal" CssClass="aspxList" runat="server">
                        <asp:ListItem Value="42">&nbsp;Male</asp:ListItem>
                        <asp:ListItem Value="43">&nbsp;Female</asp:ListItem>
                        </asp:RadioButtonList>
                    </td>
                </tr>
                <!-- Question 3 -->
                <tr class="table_body">
                    <td colspan="2"><strong>3. Does this chart reflect patient responses to the following:</strong></td>
                </tr>
                <!-- Question 3a -->
                <tr class="table_body">
                    <td  style="padding-left:40px;"><strong>3a. Does the patient’s history include a ptotic lid effect on symptoms and activities of daily living (ADL) and/or aesthetic concerns?</strong>&nbsp;<a href="#"><img id="QH3" src="../../common/images/tip.gif" alt="Tip" /></a>
                        <br />
                        <asp:Label ID="LabelHistoryPtoticLidEffect" runat="server" Visible="false"  ForeColor="Red" Text="Please complete"></asp:Label>
                    </td>
                    <td>
                        <asp:RadioButton runat="server" ID="RadioButtonHistoryPtoticLidEffectYes" GroupName="HistoryPtoticLidEffect" text="&nbsp;Yes" />&nbsp;&nbsp;
                        <asp:RadioButton runat="server" ID="RadioButtonHistoryPtoticLidEffectNo" GroupName="HistoryPtoticLidEffect" text="&nbsp;No" />
                    </td>
                </tr>
                <!-- Question 3b -->
                <tr class="table_body">
                    <td  style="padding-left:40px;"><strong>3b. Ocular history/symptoms?</strong>
                        <br />
                        <asp:Label ID="LabelHistoryOcular" runat="server" Visible="false"  ForeColor="Red" Text="Please complete"></asp:Label>
                    </td>
                    <td>
                        <asp:RadioButton runat="server" ID="RadioButtonHistoryOcularYes" GroupName="HistoryOcular" text="&nbsp;Yes" />&nbsp;&nbsp;
                        <asp:RadioButton runat="server" ID="RadioButtonHistoryOcularNo" GroupName="HistoryOcular" text="&nbsp;No" />
                    </td>
                </tr>
                <!-- Question 3c -->
                <tr class="table_body">
                    <td  style="padding-left:40px;"><strong>3c. Onset of ptosis?</strong>
                        <br />
                        <asp:Label ID="LabelHistoryOnsetPtosis" runat="server" Visible="false"  ForeColor="Red" Text="Please complete"></asp:Label>
                   </td>
                    <td>
                        <asp:RadioButton runat="server" ID="RadioButtonHistoryOnsetPtosisYes" GroupName="HistoryOnsetPtosis" text="&nbsp;Yes" />&nbsp;&nbsp;
                        <asp:RadioButton runat="server" ID="RadioButtonHistoryOnsetPtosisNo" GroupName="HistoryOnsetPtosis" text="&nbsp;No" />
                    </td>
                </tr>
                <!-- Question 3d -->
                <tr class="table_body">
                    <td  style="padding-left:40px;"><strong>3d. Variability/constancy of ptosis?</strong>
                         <br />
                        <asp:Label ID="LabelHistoryVariabilityPtosis" runat="server" Visible="false"  ForeColor="Red" Text="Please complete"></asp:Label>
                  </td>
                    <td>
                        <asp:RadioButton runat="server" ID="RadioButtonHistoryVariabilityPtosisYes" GroupName="HistoryVariabilityPtosis" text="&nbsp;Yes" />&nbsp;&nbsp;
                        <asp:RadioButton runat="server" ID="RadioButtonHistoryVariabilityPtosisNo" GroupName="HistoryVariabilityPtosis" text="&nbsp;No" />
                    </td>
                </tr>
                <!-- Question 3e changed to 4 7/15-->
                <tr class="table_body">
                    <td><strong>4. Is there a history of previous lid/orbit/facial/sinus surgery or trauma?</strong>
                          <br />
                        <asp:Label ID="LabelHistoryPreviousLid" runat="server" Visible="false"  ForeColor="Red" Text="Please complete"></asp:Label>
                  </td>
                    <td>
                        <asp:RadioButton runat="server" ID="RadioButtonHistoryPreviousLidYes" GroupName="HistoryPreviousLid" text="&nbsp;Yes" />&nbsp;&nbsp;
                        <asp:RadioButton runat="server" ID="RadioButtonHistoryPreviousLidNo" GroupName="HistoryPreviousLid" text="&nbsp;No" />
                    </td>
                </tr>
                <!-- Question 3f changed to 5 7/15 -->
                <tr class="table_body">
                    <td><strong>5. Is there a history of diplopia?</strong>
                          <br />
                        <asp:Label ID="LabelHistoryDiplopia" runat="server" Visible="false"  ForeColor="Red" Text="Please complete"></asp:Label>
                 </td>
                    <td>
                        <asp:RadioButton runat="server" ID="RadioButtonHistoryDiplopiaYes" GroupName="HistoryDiplopia" text="&nbsp;Yes" />&nbsp;&nbsp;
                        <asp:RadioButton runat="server" ID="RadioButtonHistoryDiplopiaNo" GroupName="HistoryDiplopia" text="&nbsp;No" />
                    </td>
                </tr>
                <!-- Question 3g changed to 6 7/15 -->
                <tr class="table_body">
                    <td><strong>6. Is there a history of corneal refractive surgery?</strong>&nbsp;<a href="#"><img id="QH9" src="../../common/images/tip.gif" alt="Tip" /></a>
                           <br />
                        <asp:Label ID="LabelRBHistoryCornealSurgery" runat="server" Visible="false"  ForeColor="Red" Text="Please complete"></asp:Label>
                    </td>
                    <td>
                        <asp:RadioButtonList id="RadioButtonListRBHistoryCornealSurgery" RepeatDirection="Horizontal" CssClass="aspxList" runat="server">
                            <asp:ListItem Value="1">&nbsp;Yes</asp:ListItem>
                            <asp:ListItem Value="2">&nbsp;No</asp:ListItem>
                        </asp:RadioButtonList>
                   </td>
                </tr>
                <!-- Question 3h changed to 7 -->
                <tr class="table_body">
                    <td><strong>7. Does the patient present any symptoms of dry eye?</strong>&nbsp;<a href="#"><img id="QH10" src="../../common/images/tip.gif" alt="Tip" /></a>
                           <br />
                        <asp:Label ID="LabelRBSymptomDryEye" runat="server" Visible="false"  ForeColor="Red" Text="Please complete"></asp:Label>
                    </td>
                    <td>
                        <asp:RadioButtonList id="RadioButtonListRBSymptomDryEye" RepeatDirection="Horizontal" CssClass="aspxList" runat="server">
                        <asp:ListItem Value="1">&nbsp;Yes</asp:ListItem>
                        <asp:ListItem Value="2">&nbsp;No</asp:ListItem>
                        </asp:RadioButtonList>
                    </td>
                </tr>
            </table>
            <br />
            <!-- Initial Examination -->
            <table>
                <tr>
                    <th colspan="3" width="910px"><p>Initial Examination</p></th>
                </tr>
                <!-- Question 1 -->
                <tr class="table_body">
                    <td><strong>1. Was visual acuity documented?</strong>
                           <br />
                        <asp:Label ID="LabelVisualAcuityDocumented" runat="server" Visible="false"  ForeColor="Red" Text="Please complete"></asp:Label>
                    </td>
                    <td>
                        <asp:RadioButton runat="server" ID="RadioButtonVisualAcuityDocumentedYes" GroupName="VisualAcuityDocumented" text="&nbsp;Yes" />&nbsp;&nbsp;
                        <asp:RadioButton runat="server" ID="RadioButtonVisualAcuityDocumentedNo" GroupName="VisualAcuityDocumented" text="&nbsp;No" />
                    </td>
                </tr>
                <!-- Question 2 -->
                <tr class="table_body_bg">
                    <td><strong>2. Were ocular alignment and motility normal?</strong>&nbsp;<a href="#"><img id="QI2" src="../../common/images/tip.gif" alt="Tip" /></a>
                          <br />
                        <asp:Label ID="LabelRBOcularAlignment" runat="server" Visible="false"  ForeColor="Red" Text="Please complete"></asp:Label>
                    </td>
                    <td>
                        <asp:RadioButtonList id="RadioButtonListRBOcularAlignment" RepeatDirection="Horizontal" CssClass="aspxList" runat="server">
                        <asp:ListItem Value="1">&nbsp;Yes</asp:ListItem>
                        <asp:ListItem Value="2">&nbsp;No</asp:ListItem>
                        <asp:ListItem Value="3">&nbsp;Not Documented</asp:ListItem>
                        </asp:RadioButtonList>
                    </td>
                </tr>
                <!-- Question 3 -->
                <tr class="table_body">
                    <td><strong>3. Was pupillary examination documented?</strong>
                          <br />
                        <asp:Label ID="LabelPupillaryExam" runat="server" Visible="false"  ForeColor="Red" Text="Please complete"></asp:Label>
                    </td>
                    <td>
                        <asp:RadioButton runat="server" ID="RadioButtonPupillaryExamYes" GroupName="PupillaryExam" text="&nbsp;Yes" />&nbsp;&nbsp;
                        <asp:RadioButton runat="server" ID="RadioButtonPupillaryExamNo" GroupName="PupillaryExam" text="&nbsp;No" />
                    </td>
                </tr>
                <!-- Question 4 -->
                <tr class="table_body_bg">
                    <td width="70%"><strong>4. What were the preoperative MRD1 measurements?</strong>&nbsp;<a href="#"><img id="QI4" src="../../common/images/tip.gif" alt="Tip" /></a>
                          <br />
                        <asp:Label ID="LabelPreop" runat="server" Visible="false"  ForeColor="Red" Text="Please complete"></asp:Label>
                    </td>
                    <td>
                        OD <asp:TextBox runat="server" ID="TextBoxPreopODMRD1" size="5" onkeyup="toggleQI4CB();" />mm
                        <br />OS <asp:TextBox runat="server" ID="TextBoxPreopOSMRD1" size="5" onkeyup="toggleQI4CB();" />mm
                        <br /><asp:CheckBox runat="server" ID="CheckBoxPreopNA" text="&nbsp;Not Documented" onclick="toggleQI4TB();" />

                    </td>
                </tr>
            </table>
            <br />
            <!-- Process Measure(s) -->
            <table>
                <!-- Header -  -->
                <tr>
                    <th colspan="3" width="910px"><p>Process Measures</p></th>
                </tr>
                <!-- Question 1 -->
                <tr class="table_body">
                    <td width="70%"><strong>1. What was levator excursion (function) measurement?</strong>&nbsp;<a href="#"><img id="QP1" src="../../common/images/tip.gif" alt="Tip" /></a>
                        <br />
                        <asp:Label ID="LabelLevatorExcursion" runat="server" Visible="false"  ForeColor="Red" Text="Please complete"></asp:Label>
                    </td>
                    <td>
                        OD <asp:TextBox runat="server" ID="TextBoxLevatorExcursionOD" size="5" onkeyup="toggleQP1CB();" />mm
                        <br />OS <asp:TextBox runat="server" ID="TextBoxLevatorExcursionOS" size="5" onkeyup="toggleQP1CB();" />mm
                        <br /><asp:CheckBox runat="server" ID="CheckBoxLevatorExcursionNA" text="&nbsp; Not Documented" onclick="toggleQP1TB();" />
                    </td>
                </tr>
                <!-- Question 2 -->
                <tr class="table_body_bg">
                    <td><strong>2. Were the ptosis visual fields examined?</strong>&nbsp;<a href="#"><img id="QP2" src="../../common/images/tip.gif" alt="Tip" /></a>
                        <br />
                        <asp:Label ID="LabelPtosisVisualField" runat="server" Visible="false"  ForeColor="Red" Text="Please complete"></asp:Label>
                    </td>
                    <td>
                        <asp:RadioButton runat="server" ID="RadioButtonPtosisVisualFieldYes" GroupName="PtosisVisualField" text="&nbsp;Yes" />&nbsp;&nbsp;
                        <asp:RadioButton runat="server" ID="RadioButtonPtosisVisualFieldNo" GroupName="PtosisVisualField" text="&nbsp;No" />
                    </td>
                </tr>
                <!-- Question 3 -->
                <tr class="table_body">
                    <td><strong>3. Was an examination of conjunctiva, cornea, anterior chamber, and lens reported?</strong>
                        <br />
                        <asp:Label ID="LabelExamCCAL" runat="server" Visible="false"  ForeColor="Red" Text="Please complete"></asp:Label>
                    </td>
                    <td>
                        <asp:RadioButton runat="server" ID="RadioButtonExamCCALYes" GroupName="ExamCCAL" text="&nbsp;Yes" />&nbsp;&nbsp;
                        <asp:RadioButton runat="server" ID="RadioButtonExamCCALNo" GroupName="ExamCCAL" text="&nbsp;No" />
                    </td>
                </tr>
                <!-- Question 4 -->
                <tr class="table_body_bg">
                    <td><strong>4. Was an assessment of tear production (exam tear film, Basal Tear Production, Schirmer tests) documented?</strong>&nbsp;<a href="#"><img id="QP4" src="../../common/images/tip.gif" alt="Tip" /></a>
                        <br />
                        <asp:Label ID="LabelAssessTearProduction" runat="server" Visible="false"  ForeColor="Red" Text="Please complete"></asp:Label>
                   </td>
                    <td>
                        <asp:RadioButton runat="server" ID="RadioButtonAssessTearProductionYes" GroupName="AssessTearProduction" text="&nbsp;Yes" />&nbsp;&nbsp;
                        <asp:RadioButton runat="server" ID="RadioButtonAssessTearProductionNo" GroupName="AssessTearProduction" text="&nbsp;No" />
                    </td>
                </tr>
                <!-- Question 5 -->
                <tr class="table_body">
                    <td><strong>5. Was an assessment of corneal and/or facial sensation conducted?</strong>&nbsp;<a href="#"><img id="QP5" src="../../common/images/tip.gif" alt="Tip" /></a>
                        <br />
                        <asp:Label ID="LabelAssessCornealSensation" runat="server" Visible="false"  ForeColor="Red" Text="Please complete"></asp:Label>
                  </td>
                    <td>
                        <asp:RadioButton runat="server" ID="RadioButtonAssessCornealSensationYes" GroupName="AssessCornealSensation" text="&nbsp;Yes" />&nbsp;&nbsp;
                        <asp:RadioButton runat="server" ID="RadioButtonAssessCornealSensationNo" GroupName="AssessCornealSensation" text="&nbsp;No" />
                    </td>
                </tr>
                <!-- Question 6 -->
                <tr class="table_body_bg">
                    <td><strong>6. Was an assessment of brow position conducted?</strong>
                        <br />
                        <asp:Label ID="LabelAssessBrowPosition" runat="server" Visible="false"  ForeColor="Red" Text="Please complete"></asp:Label>
                  </td>
                    <td>
                        <asp:RadioButton runat="server" ID="RadioButtonAssessBrowPositionYes" GroupName="AssessBrowPosition" text="&nbsp;Yes" />&nbsp;&nbsp;
                        <asp:RadioButton runat="server" ID="RadioButtonAssessBrowPositionNo" GroupName="AssessBrowPosition" text="&nbsp;No" />
                    </td>
                </tr>
                <!-- Question 7 -->
                <tr class="table_body">
                    <td><strong>7. Was an eyelid fatigue, eyelid twitch sign, acetylcholine receptor antibody testing or Tensilon test conducted?</strong>
                        <br />
                        <asp:Label ID="LabelAssessEyelidFatigue" runat="server" Visible="false"  ForeColor="Red" Text="Please complete"></asp:Label>
                  </td>
                    <td>
                        <asp:RadioButtonList id="RadioButtonListAssessEyelidFatigue" RepeatDirection="Horizontal" CssClass="aspxList" runat="server">
	                        <asp:ListItem Value="1">&nbsp;Yes</asp:ListItem>
	                        <asp:ListItem Value="2">&nbsp;No</asp:ListItem>
	                        <asp:ListItem Value="3">&nbsp;Not Indicated</asp:ListItem>
                        </asp:RadioButtonList>
                    </td>
                </tr>
                <!-- Question 8 -->
                <tr class="table_body_bg">
                    <td><strong>8. Was an assessment of dermatochalasis conducted?</strong>
                        <br />
                        <asp:Label ID="LabelAssessDerma" runat="server" Visible="false"  ForeColor="Red" Text="Please complete"></asp:Label>
                 </td>
                    <td>
                        <asp:RadioButton runat="server" ID="RadioButtonAssessDermaYes" GroupName="AssessDerma" text="&nbsp;Yes" />&nbsp;&nbsp;
                        <asp:RadioButton runat="server" ID="RadioButtonAssessDermaNo" GroupName="AssessDerma" text="&nbsp;No" />
                    </td>
                </tr>
                 <!-- Question 9 -->
                <tr class="table_body">
                    <td><strong>9. Did you confirm the presence or absence of the Bell’s phenomenon?</strong>
                        <br />
                        <asp:Label ID="LabelPresenceBellsPhenom" runat="server" Visible="false"  ForeColor="Red" Text="Please complete"></asp:Label>
                 </td>
                    <td>
                        <asp:RadioButton runat="server" ID="RadioButtonPresenceBellsPhenomYes" GroupName="PresenceBellsPhenom" text="&nbsp;Yes" />&nbsp;&nbsp;
                        <asp:RadioButton runat="server" ID="RadioButtonPresenceBellsPhenomNo" GroupName="PresenceBellsPhenom" text="&nbsp;No" />
                    </td>
                </tr>
           </table>
            <br />
            <!-- Assessment -->
            <table>
                <!-- Header -  -->
                <tr>
                    <th colspan="3" width="910px"><p>Assessment</p></th>
                </tr>
                <!-- Question 1 -->
                <tr class="table_body">
                    <td width="70%"><strong>1. What was presumed etiology of the ptosis?</strong>
                       <br />
                        <asp:Label ID="LabelEtiology" runat="server" Visible="false"  ForeColor="Red" Text="Please complete"></asp:Label>
                    </td>
                    <td>
                        <asp:CheckBox runat="server" ID="CheckBoxEtiologyAponeurotic" text="&nbsp;Aponeurotic" />
                        <br /><asp:CheckBox runat="server" ID="CheckBoxEtiologyMyopathic" text="&nbsp;Myopathic" />
                        <br /><asp:CheckBox runat="server" ID="CheckBoxEtiologyNeurogenic" text="&nbsp;Neurogenic" />
                        <br /><asp:CheckBox runat="server" ID="CheckBoxEtiologyOther" text="&nbsp;Other" />
                    </td>
                </tr>
                <!-- Question 1a -->
                <tr class="table_body">
                    <td><strong>1a. If “Other” was selected, please specify:</strong>
                        <br />
                        <asp:Label ID="LabelEtiologyOtherText" runat="server" Visible="false"  ForeColor="Red" Text="Please complete"></asp:Label>
                    </td>
                    <td>
                        <asp:TextBox runat="server" ID="TextBoxEtiologyOtherText" size="25" />
                    </td>
                </tr>
                <!-- Question 2 -->
                <tr class="table_body_bg">
                    <td><strong>2.  Was informed consent discussed with the patient?</strong>
                         <br />
                        <asp:Label ID="LabelDocInformedConsent" runat="server" Visible="false"  ForeColor="Red" Text="Please complete"></asp:Label>
                    </td>
                    <td>
                        <asp:RadioButton runat="server" ID="RadioButtonDocInformedConsentYes" GroupName="DocInformedConsent" text="&nbsp;Yes" />&nbsp;&nbsp;
                        <asp:RadioButton runat="server" ID="RadioButtonDocInformedConsentNo" GroupName="DocInformedConsent" text="&nbsp;No" />
                    </td>
                </tr>
            </table>
            <br />
            <!-- Surgical Intervention -->
            <table>
                <tr>
                    <th colspan="2" width="910px"><p>Surgical Intervention</p></th>
                </tr>
                <!-- Question 1 -->
                <tr class="table_body">
                    <td width="70%"><strong>1. Was the summary of operation included in chart?</strong>
                         <br />
                        <asp:Label ID="LabelOperationSummary" runat="server" Visible="false"  ForeColor="Red" Text="Please complete"></asp:Label>
                    </td>
                    <td>
                        <asp:RadioButton runat="server" ID="RadioButtonOperationSummaryYes" GroupName="OperationSummary" text="&nbsp;Yes" />&nbsp;&nbsp;
                        <asp:RadioButton runat="server" ID="RadioButtonOperationSummaryNo" GroupName="OperationSummary" text="&nbsp;No" />
                    </td>
                </tr>
                <!-- Question 2 -->
                <tr class="table_body_bg">
                    <td><strong>2. What procedure was performed?</strong>
                         <br />
                        <asp:Label ID="LabelRBPtosisProc" runat="server" Visible="false"  ForeColor="Red" Text="Please complete"></asp:Label>
                   </td>
                    <td>
                        <asp:RadioButtonList id="RadioButtonListRBPtosisProc" RepeatDirection="Vertical" CssClass="aspxList" runat="server">
                            <asp:ListItem Value="129">&nbsp;External levator or aponeurotic surgery</asp:ListItem>
                            <asp:ListItem Value="130">&nbsp;Conjunctival-Meullerectomy</asp:ListItem>
                            <asp:ListItem Value="131">&nbsp;Frontalis Suspension</asp:ListItem>
                            <asp:ListItem Value="14">&nbsp;Other</asp:ListItem>
                            <asp:ListItem Value="3">&nbsp;Not Indicated</asp:ListItem>
                        </asp:RadioButtonList>
                   </td>
                </tr>
                <!-- Question 2a -->
                <tr class="table_body_bg">
                    <td><strong>2a. If “Other” was selected, please specify:</strong>
                         <br />
                        <asp:Label ID="LabelPtosisProcOtherText" runat="server" Visible="false"  ForeColor="Red" Text="Please complete"></asp:Label>
                  </td>
                    <td>
                        <asp:TextBox runat="server" ID="TextBoxPtosisProcOtherText" size="25" />
                    </td>
                </tr>
            </table>
            <br />
            <!-- Postoperative Care -->
            <table>
                <!-- Header -  -->
                <tr>
                    <th colspan="3" width="910px"><p>Post-Operative Care</p></th>
                </tr>
                <!-- Question 1 -->
                <tr class="table_body">
                    <td width="70%"><strong>1. Was an MRD measurement taken, or indication of lid position recorded?</strong>
                          <br />
                        <asp:Label ID="LabelPostopMRD" runat="server" Visible="false"  ForeColor="Red" Text="Please complete"></asp:Label>
                    </td>
                    <td>
                        <asp:RadioButton runat="server" ID="RadioButtonPostopMRDYes" GroupName="PostopMRD" text="&nbsp;Yes" />&nbsp;&nbsp;
                        <asp:RadioButton runat="server" ID="RadioButtonPostopMRDNo" GroupName="PostopMRD" text="&nbsp;No" />
                    </td>
                </tr>
                <!-- Question 2 -->
                <tr class="table_body_bg">
                    <td><strong>2. Was an exam of the cornea performed?</strong>
                          <br />
                        <asp:Label ID="LabelPostopCorneaExam" runat="server" Visible="false"  ForeColor="Red" Text="Please complete"></asp:Label>
                    </td>
                    <td>
                        <asp:RadioButton runat="server" ID="RadioButtonPostopCorneaExamYes" GroupName="PostopCorneaExam" text="&nbsp;Yes" />&nbsp;&nbsp;
                        <asp:RadioButton runat="server" ID="RadioButtonPostopCorneaExamNo" GroupName="PostopCorneaExam" text="&nbsp;No" />
                    </td>
                </tr>
                <!-- Question 3 -->
                <tr class="table_body">
                    <td>
                        <strong>3. Was status of incision documented?</strong>  
                        <asp:Label ID="LabelPostopIncisionStatus" runat="server" Visible="false" ForeColor="Red" Text="<br />Please complete"></asp:Label>
                    </td>
                    <td>
                        <asp:RadioButton runat="server" ID="RadioButtonPostopIncisionStatusYes" GroupName="PostopIncisionStatus" text="&nbsp;Yes" />&nbsp;&nbsp;
                        <asp:RadioButton runat="server" ID="RadioButtonPostopIncisionStatusNo" GroupName="PostopIncisionStatus" text="&nbsp;No" />
                    </td>
                </tr>
                <!-- Question 3a -->
                <tr class="table_body">
                    <td id="QPO3aA">
                        <strong>3a. How long was (has) the patient been followed after surgery (time between surgery and last appointment)?</strong>
                    </td>
                    <td id="QPO3aB">
                        #&nbsp;<asp:TextBox runat="server" ID="TextBoxPostOpFollowedMonths" size="5" />&nbsp;months&nbsp;&nbsp;
                        #&nbsp;<asp:TextBox runat="server" ID="TextBoxPostOpFollowedDays" size="5" />&nbsp;days
                    </td>
                </tr>
                <!-- Question 4 -->
                <tr class="table_body_bg">
                    <td><strong>4. Did the patient exhibit any signs of short-term complication?</strong>
                          <br />
                        <asp:Label ID="LabelPostopST" runat="server" Visible="false"  ForeColor="Red" Text="Please complete"></asp:Label>
                    </td>
                    <td>
                        <asp:CheckBox runat="server" ID="CheckBoxPostopSTInfection" text="&nbsp;Infection" />
                        <br /><asp:CheckBox runat="server" ID="CheckBoxPostopSTKeratitis" text="&nbsp;Keratitis" />
                        <br /><asp:CheckBox runat="server" ID="CheckBoxPostopSTOverCorrection" text="&nbsp;Over-Correction" />
                        <br /><asp:CheckBox runat="server" ID="CheckBoxPostopSTUnderCorrection" text="&nbsp;Under-Correction" />
                        <br /><asp:CheckBox runat="server" ID="CheckBoxPostopSTPoorContour" text="&nbsp;Poor Contour" />
                        <br /><asp:CheckBox runat="server" ID="CheckBoxPostopSTScarring" text="&nbsp;Scarring" />
                        <br /><asp:CheckBox runat="server" ID="CheckBoxPostopSTSuspension" text="&nbsp;Suspension Material Exposure" />
                        <br /><asp:CheckBox runat="server" ID="CheckBoxPostopSTOther" text="&nbsp;Other" />
                        <br /><asp:CheckBox runat="server" ID="CheckBoxPostopSTNone" text="&nbsp;None" />
                    </td>
                </tr>
                <!-- Question 4a -->
                <tr class="table_body_bg">
                    <td><strong>4a. If “Other” was selected, please specify:</strong>
                          <br />
                        <asp:Label ID="LabelPostopSTOtherText" runat="server" Visible="false"  ForeColor="Red" Text="Please complete"></asp:Label>
                    </td>
                    <td>
                        <asp:TextBox runat="server" ID="TextBoxPostopSTOtherText" size="25" />
                    </td>
                 </tr>
                <!-- Question 5 -->
                <tr class="table_body">
                    <td><strong>5. Did the patient exhibit any signs of long-term complication?</strong>
                          <br />
                        <asp:Label ID="LabelPostopLT" runat="server" Visible="false"  ForeColor="Red" Text="Please complete"></asp:Label>
                   </td>
                    <td>
                        <asp:CheckBox runat="server" ID="CheckBoxPostopLTInfection" text="&nbsp;Infection" />
                        <br /><asp:CheckBox runat="server" ID="CheckBoxPostopLTKeratitis" text="&nbsp;Keratitis" />
                        <br /><asp:CheckBox runat="server" ID="CheckBoxPostopLTOverCorrection" text="&nbsp;Over-Correction" />
                        <br /><asp:CheckBox runat="server" ID="CheckBoxPostopLTUnderCorrection" text="&nbsp;Under-Correction" />
                        <br /><asp:CheckBox runat="server" ID="CheckBoxPostopLTPoorContour" text="&nbsp;Poor Contour" />
                        <br /><asp:CheckBox runat="server" ID="CheckBoxPostopLTScarring" text="&nbsp;Scarring" />
                        <br /><asp:CheckBox runat="server" ID="CheckBoxPostopLTSuspension" text="&nbsp;Suspension Material Exposure" />
                        <br /><asp:CheckBox runat="server" ID="CheckBoxPostopLTOther" text="&nbsp;Other" />
                        <br /><asp:CheckBox runat="server" ID="CheckBoxPostopLTNone" text="&nbsp;None" />
                    </td>
                </tr>
                <!-- Question 5a -->
                <tr class="table_body">
                    <td><strong>5a. If “Other” was selected, please specify:</strong>
                          <br />
                        <asp:Label ID="LabelPostopLTOtherText" runat="server" Visible="false"  ForeColor="Red" Text="Please complete"></asp:Label>
                    </td>
                    <td>
                        <asp:TextBox runat="server" ID="TextBoxPostopLTOtherText" size="25" />
                    </td>
                </tr>
                <!-- Question 6 -->
                <tr class="table_body_bg">
                    <td><strong>6. Was a re-operation performed for eyelid height, contour adjustment, or other reason(s)?</strong>
                           <br />
                        <asp:Label ID="LabelReoperation" runat="server" Visible="false"  ForeColor="Red" Text="Please complete"></asp:Label>
                    </td>
                    <td>
                        <asp:RadioButton runat="server" ID="RadioButtonReoperationYes" GroupName="Reoperation" text="&nbsp;Yes" />&nbsp;&nbsp;
                        <asp:RadioButton runat="server" ID="RadioButtonReoperationNo" GroupName="Reoperation" text="&nbsp;No" />
                    </td>
                </tr>
            </table>
            <br />
            <!-- Outcomes -->
            <table>
                <!-- Header -  -->
                <tr>
                    <th colspan="3" width="910px"><p>Outcomes</p></th>
                </tr>
                <!-- Question 1 -->
                <tr class="table_body">
                    <td width="70%"><strong>1. Any ongoing postoperative complications affecting activities of daily living?</strong>
                           
                        <asp:Label ID="LabelPostopComplicationDailyLiving" runat="server" Visible="false"  ForeColor="Red" Text="<br />Please complete"></asp:Label>
                    </td>
                    <td>
                        <asp:RadioButton runat="server" ID="RadioButtonPostopComplicationDailyLivingYes" GroupName="PostopComplicationDailyLiving" text="&nbsp;Yes" />&nbsp;&nbsp;
                        <asp:RadioButton runat="server" ID="RadioButtonPostopComplicationDailyLivingNo" GroupName="PostopComplicationDailyLiving" text="&nbsp;No" />
                    </td>
                </tr>
                <!-- Question 2 -->
                <tr class="table_body_bg">
                    <td><strong>2. Was the lid margin position improved post-op?</strong>
                           
                        <asp:Label ID="LabelOutcomeLidMargin" runat="server" Visible="false"  ForeColor="Red" Text="<br />Please complete"></asp:Label>
                    </td>
                    <td>
                        <asp:RadioButton runat="server" ID="RadioButtonOutcomeLidMarginYes" GroupName="OutcomeLidMargin" text="&nbsp;Yes" />&nbsp;&nbsp;
                        <asp:RadioButton runat="server" ID="RadioButtonOutcomeLidMarginNo" GroupName="OutcomeLidMargin" text="&nbsp;No" />
                    </td>
                </tr>
                <!-- Question 3 -->
                <tr class="table_body">
                    <td><strong>3. Did the patient report that activities of daily living and/or cosmetic appearance improved post-op?</strong>
                           
                        <asp:Label ID="LabelOutcomeActivityImproved" runat="server" Visible="false"  ForeColor="Red" Text="<br />Please complete"></asp:Label>
                    </td>
                    <td>
                        <asp:RadioButton runat="server" ID="RadioButtonOutcomeActivityImprovedYes" GroupName="OutcomeActivityImproved" text="&nbsp;Yes" />&nbsp;&nbsp;
                        <asp:RadioButton runat="server" ID="RadioButtonOutcomeActivityImprovedNo" GroupName="OutcomeActivityImproved" text="&nbsp;No" />
                    </td>
                </tr>
            </table>
            <div class="button-box">
                <asp:LinkButton ID="LinkButtonBackToDashboard" runat="server" Text="Back To Chart Registration" PostBackUrl="../PatientChartRegistration.aspx?CycleNumber=1" Visible="false" CssClass="button" />
                <asp:LinkButton ID="ButtonSubmit" OnClick="ButtonSubmit_Click" runat="server" Text="Submit Chart" CssClass="button" />
            </div>
        </div>
        <!-- ION pim ends -->
</asp:Content>

