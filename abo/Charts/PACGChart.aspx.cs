﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Transactions;
using NetHealthPIMModel;

public partial class abo_PACGChart : BasePage
{
    protected void Page_Load(object sender, EventArgs e)
    {
        if (!IsPostBack)
        {
            InitializePage();
        }
    }
    protected void InitializePage()
    {
        ((PIMMasterPage)Page.Master).SetTitle("Chart");

        using (NetHealthPIMEntities pim = new NetHealthPIMEntities())
        {
            int ModuleID = (int)Session[Constants.SESSION_WORKINGMODULEID];
            Module module = pim.Module.First(c => c.ModuleID == ModuleID);
            ((PIMMasterPage)Page.Master).SetHeader(module.ModuleName + " Chart Abstraction");

            ParticipantModuleCycle prev = (ParticipantModuleCycle)Session[Constants.SESSION_PREVIOUSCYCLE];
            String CycleNumber = Request.QueryString["CycleNumber"];

            int cycleID = ctx.ActiveModuleCycleID;
            if (CycleNumber == "1")
            {
                cycleID = prev.ParticipantModuleCycleID;
                LinkButtonBackToDashboard.Visible = true;
                ButtonSubmit.Visible = false;
            }

            ParticipantModuleCycle cycle = pim.ParticipantModuleCycle.First(c => c.ParticipantModuleCycleID == cycleID);
            String numberOfRecord = Request.QueryString["nr"];
            String totalRecords = Request.QueryString["t"];
            if (cycle.CycleNumber == 1)
            {
                ((PIMMasterPage)Page.Master).SetStatus(2);
                LiteralAbstractionNumber.Text = "Initial Abstraction: Chart " + numberOfRecord + " of " + totalRecords;
            }
            else
            {
                ((PIMMasterPage)Page.Master).SetStatus(3);
                LiteralAbstractionNumber.Text = "Second Abstraction: Chart " + numberOfRecord + " of " + totalRecords;
            }
            DropDownListMonthOfBirth.DataSource = CycleManager.getMonthList();
            DropDownListYearOfBirth.DataSource = CycleManager.getDOBYearList(0, 100);
            DropDownListMonthofExam.DataSource = CycleManager.getMonthList();
            DropDownListMonthofYear.DataSource = CycleManager.getDOBYearList(0, 100);
            DropDownListOutcomeExamMonth.DataSource = CycleManager.getMonthList();
            DropDownListOutcomeExamYear.DataSource = CycleManager.getDOBYearList(0, 100);
            DropDownListFollowUpExamDateAmount.DataSource = CycleManager.getMonthList();
            DropDownListFollowUpExamDateType.DataSource = CycleManager.getDOBYearList(0, 100);
            DropDownListOutcomeOpticNerveExamODMonth.DataSource = CycleManager.getMonthList();
            DropDownListOutcomeOpticNerveExamODYear.DataSource = CycleManager.getDOBYearList(0, 100);
            DropDownListOutcomeOpticNerveExamOSMonth.DataSource = CycleManager.getMonthList();
            DropDownListOutcomeOpticNerveExamOSYear.DataSource = CycleManager.getDOBYearList(0, 100);

            DropDownListBCVAOS.DataSource = CycleManager.getExamValues();
            DropDownListBCVAOD.DataSource = CycleManager.getExamValues();
            DropDownListOutcomeBCVAOS.DataSource = CycleManager.getExamValues();
            DropDownListOutcomeBCVAOD.DataSource = CycleManager.getExamValues();



            DataBind();
            DropDownListYearOfBirth.Items.Insert(0, new ListItem("Year", "0"));
            DropDownListMonthofYear.Items.Insert(0, new ListItem("Year", "0"));
            DropDownListOutcomeExamYear.Items.Insert(0, new ListItem("Year", "0"));
            DropDownListFollowUpExamDateType.Items.Insert(0, new ListItem("Year", "0"));
            DropDownListOutcomeOpticNerveExamODYear.Items.Insert(0, new ListItem("Year", "0"));
            DropDownListOutcomeOpticNerveExamOSYear.Items.Insert(0, new ListItem("Year", "0"));

            string recordIdentifier = Request.QueryString["ri"];
            HiddenFieldRecordIdentifier.Value = recordIdentifier;
            LiteralRecordIdentifier.Text = recordIdentifier;
            var count = pim.ChartAbstractionPACG.Where(ps => ps.ParticipantModuleCycle.ParticipantModuleCycleID == cycleID & ps.RecordIdentifier == recordIdentifier).Count();
            if (count == 0)
            {
                using (TransactionScope transaction = new TransactionScope())
                {
                    //Module module = pim.Module.First(m => m.ModuleID == Constants.ABO_AMBLYOPIA_MODULEID);

                    NetHealthPIMModel.ChartRegistration chartRegistration = (from cr in pim.ChartRegistration
                                                           where cr.ParticipantModuleCycleID == cycleID
                                                           & cr.RecordIdentifier == recordIdentifier
                                                           & cr.ModuleID == 21///// NEEDS TO BE CHANGED
                                                           select cr).First<NetHealthPIMModel.ChartRegistration>();

                    DateTime initialVisit = Convert.ToDateTime(chartRegistration.InitialVisit);
                    DateTime DOB = Convert.ToDateTime(chartRegistration.DOB);
                    DateTime lastexam = Convert.ToDateTime(chartRegistration.LastVisit);
                    ChartAbstractionPACG abstractRecord = new ChartAbstractionPACG();
                    abstractRecord.ParticipantModuleCycle = cycle;
                    abstractRecord.RecordIdentifier = recordIdentifier;
                    abstractRecord.MonthOfExam = initialVisit.Month;
                    abstractRecord.YearOfExam = initialVisit.Year;
                    abstractRecord.MonthOfBirth = DOB.Month;
                    abstractRecord.YearOfBirth = DOB.Year;
                    abstractRecord.OutcomeExamMonth = lastexam.Month;
                    abstractRecord.OutcomeExamYear = lastexam.Year;
                    DropDownListOutcomeExamMonth.SelectedValue = abstractRecord.OutcomeExamMonth.ToString();
                        DropDownListOutcomeExamYear.SelectedValue =abstractRecord.OutcomeExamYear.ToString();
                    DropDownListYearOfBirth.SelectedValue = abstractRecord.YearOfBirth.ToString();
                    DropDownListMonthOfBirth.SelectedValue = abstractRecord.MonthOfBirth.ToString();
                    DropDownListMonthofExam.SelectedValue = abstractRecord.MonthOfExam.ToString();
                    DropDownListMonthofYear.SelectedValue = abstractRecord.YearOfExam.ToString();
                    abstractRecord.LastUpdateDate = DateTime.Now;
                    pim.SaveChanges();

                    transaction.Complete();
                }
            }
            else
            {
                ChartAbstractionPACG abstractRecord = (from c in pim.ChartAbstractionPACG
                                                       where c.ParticipantModuleCycle.ParticipantModuleCycleID == cycleID & c.RecordIdentifier == recordIdentifier
                                                       select c).First<ChartAbstractionPACG>();

                if (abstractRecord.RBGender != null) RadioButtonListRBGender.SelectedValue = abstractRecord.RBGender.ToString();
                if (abstractRecord.RBBCVAODNotAbletoPerform != null) RadioButtonListRBBCVAODNotAbletoPerform.SelectedValue = abstractRecord.RBBCVAODNotAbletoPerform.ToString();
                if (abstractRecord.RBBCVAOSNotAbletoPerform != null) RadioButtonListRBBCVAOSNotAbletoPerform.SelectedValue = abstractRecord.RBBCVAOSNotAbletoPerform.ToString();
                if (abstractRecord.RBIOPODNotAbletoPerform != null) RadioButtonListRBIOPODNotAbletoPerform.SelectedValue = abstractRecord.RBIOPODNotAbletoPerform.ToString();
                if (abstractRecord.RBIOPOSNotAbletoPerform != null) RadioButtonListRBIOPOSNotAbletoPerform.SelectedValue = abstractRecord.RBIOPOSNotAbletoPerform.ToString();
                if (abstractRecord.RBGonioscopyOD != null) RadioButtonListRBGonioscopyOD.SelectedValue = abstractRecord.RBGonioscopyOD.ToString();
                if (abstractRecord.RBGonioscopyOS != null) RadioButtonListRBGonioscopyOS.SelectedValue = abstractRecord.RBGonioscopyOS.ToString();
                if (abstractRecord.RBSlitLampOD != null) RadioButtonListRBSlitLampOD.SelectedValue = abstractRecord.RBSlitLampOD.ToString();
                if (abstractRecord.RBSlitLampOS != null) RadioButtonListRBSlitLampOS.SelectedValue = abstractRecord.RBSlitLampOS.ToString();
                if (abstractRecord.RBOpticNerveApperanceBothEyes != null) RadioButtonListRBOpticNerveApperanceBothEyes.SelectedValue = abstractRecord.RBOpticNerveApperanceBothEyes.ToString();
                if (abstractRecord.RBAngleClosureTypeOD != null) RadioButtonListRBAngleClosureTypeOD.SelectedValue = abstractRecord.RBAngleClosureTypeOD.ToString();
                if (abstractRecord.RBAngleClosureTypeOS != null) RadioButtonListRBAngleClosureTypeOS.SelectedValue = abstractRecord.RBAngleClosureTypeOS.ToString();
                if (abstractRecord.FollowUpExamDateType != null) DropDownListFollowUpExamDateType.SelectedValue = abstractRecord.FollowUpExamDateType.ToString();
                if (abstractRecord.OutcomeRBBCVAODNotAbletoPerform != null) RadioButtonListOutcomeRBBCVAODNotAbletoPerform.SelectedValue = abstractRecord.OutcomeRBBCVAODNotAbletoPerform.ToString();
                if (abstractRecord.OutcomeRBBCVAOSNotAbletoPerform != null) RadioButtonListOutcomeRBBCVAOSNotAbletoPerform.SelectedValue = abstractRecord.OutcomeRBBCVAOSNotAbletoPerform.ToString();
                if (abstractRecord.RBOutcomeIOPODNotAbletoPerform != null) RadioButtonListRBOutcomeIOPODNotAbletoPerform.SelectedValue = abstractRecord.RBOutcomeIOPODNotAbletoPerform.ToString();
                if (abstractRecord.RBOutcomeIOPOSNotAbletoPerform != null) RadioButtonListRBOutcomeIOPOSNotAbletoPerform.SelectedValue = abstractRecord.RBOutcomeIOPOSNotAbletoPerform.ToString();
                if (abstractRecord.RBOutcomeIridotomyOD != null) RadioButtonListRBOutcomeIridotomyOD.SelectedValue = abstractRecord.RBOutcomeIridotomyOD.ToString();
                if (abstractRecord.RBOutcomeIridotomyOS != null) RadioButtonListRBOutcomeIridotomyOS.SelectedValue = abstractRecord.RBOutcomeIridotomyOS.ToString();
                if (abstractRecord.RBOutcomeIncreasingCataractOD != null) RadioButtonListRBOutcomeIncreasingCataractOD.SelectedValue = abstractRecord.RBOutcomeIncreasingCataractOD.ToString();
                if (abstractRecord.RBOutcomeIncreasingCataractOS != null) RadioButtonListRBOutcomeIncreasingCataractOS.SelectedValue = abstractRecord.RBOutcomeIncreasingCataractOS.ToString();
                if (abstractRecord.RBOpticNerveAppearanceChangeFromBaselineOD != null) RadioButtonListRBOpticNerveAppearanceChangeFromBaselineOD.SelectedValue = abstractRecord.RBOpticNerveAppearanceChangeFromBaselineOD.ToString();
                if (abstractRecord.RBOpticNerveAppearanceChangeFromBaselineOS != null) RadioButtonListRBOpticNerveAppearanceChangeFromBaselineOS.SelectedValue = abstractRecord.RBOpticNerveAppearanceChangeFromBaselineOS.ToString();

                if (abstractRecord.OcularMedications != null) RadioButtonListOcularMedications.SelectedValue = abstractRecord.OcularMedications.ToString();
                if (abstractRecord.SystemicMedications != null) RadioButtonListSystemicMedications.SelectedValue = abstractRecord.SystemicMedications.ToString();


                


                       if (abstractRecord.FollowUpExamDateAmount != null) DropDownListFollowUpExamDateAmount.SelectedValue = abstractRecord.FollowUpExamDateAmount.ToString();
                       if (abstractRecord.FollowUpExamDateType != null) DropDownListFollowUpExamDateType.SelectedValue = abstractRecord.FollowUpExamDateType.ToString();



                if (abstractRecord.MonthOfBirth != null) DropDownListMonthOfBirth.SelectedValue = abstractRecord.MonthOfBirth.ToString();
                if (abstractRecord.YearOfBirth != null) DropDownListYearOfBirth.SelectedValue = abstractRecord.YearOfBirth.ToString();
                if (abstractRecord.MonthOfExam != null) DropDownListMonthofExam.SelectedValue = abstractRecord.MonthOfExam.ToString();
                if (abstractRecord.YearOfExam != null) DropDownListMonthofYear.SelectedValue = abstractRecord.YearOfExam.ToString();
                if (abstractRecord.OutcomeExamMonth != null) DropDownListOutcomeExamMonth.SelectedValue = abstractRecord.OutcomeExamMonth.ToString();
                if (abstractRecord.OutcomeExamYear != null) DropDownListOutcomeExamYear.SelectedValue = abstractRecord.OutcomeExamYear.ToString();
                if (abstractRecord.OutcomeOpticNerveExamODMonth != null) DropDownListOutcomeOpticNerveExamODMonth.SelectedValue = abstractRecord.OutcomeOpticNerveExamODMonth.ToString();
                if (abstractRecord.OutcomeOpticNerveExamODYear != null) DropDownListOutcomeOpticNerveExamODYear.SelectedValue = abstractRecord.OutcomeOpticNerveExamODYear.ToString();
                if (abstractRecord.OutcomeOpticNerveExamOSMonth != null) DropDownListOutcomeOpticNerveExamOSMonth.SelectedValue = abstractRecord.OutcomeOpticNerveExamOSMonth.ToString();
                if (abstractRecord.OutcomeOpticNerveExamOSYear != null) DropDownListOutcomeOpticNerveExamOSYear.SelectedValue = abstractRecord.OutcomeOpticNerveExamOSYear.ToString();



              
                if (abstractRecord.OcularHistory != null)
                {
                    RadioButtonOcularHistoryYes.Checked = ((bool)abstractRecord.OcularHistory);
                    RadioButtonOcularHistoryNo.Checked = !((bool)abstractRecord.OcularHistory);
                }
                if (abstractRecord.ManagementPlanFormulated != null)
                {
                    RadioButtonManagementPlanFormulatedYes.Checked = ((bool)abstractRecord.ManagementPlanFormulated);
                    RadioButtonManagementPlanFormulatedNo.Checked = !((bool)abstractRecord.ManagementPlanFormulated);
                }
                if (abstractRecord.FollowUpExamScheduled != null)
                {
                    RadioButtonFollowUpExamScheduledYes.Checked = ((bool)abstractRecord.FollowUpExamScheduled);
                    RadioButtonFollowUpExamScheduledNo.Checked = !((bool)abstractRecord.FollowUpExamScheduled);
                }
                if (abstractRecord.Iridotomy != null)
                {
                    RadioButtonIridotomyYes.Checked = ((bool)abstractRecord.Iridotomy);
                    RadioButtonIridotomyNo.Checked = !((bool)abstractRecord.Iridotomy);
                }
                if (abstractRecord.CataractSurgery != null)
                {
                    RadioButtonCataractSurgeryYes.Checked = ((bool)abstractRecord.CataractSurgery);
                    RadioButtonCataractSurgeryNo.Checked = !((bool)abstractRecord.CataractSurgery);
                }
                if (abstractRecord.FollowUpGoniosynechialysis != null)
                {
                    RadioButtonFollowUpGoniosynechialysisYes.Checked = ((bool)abstractRecord.FollowUpGoniosynechialysis);
                    RadioButtonFollowUpGoniosynechialysisNo.Checked = !((bool)abstractRecord.FollowUpGoniosynechialysis);
                }



                //  if (abstractRecord.BCVAOD != null) TextBoxBCVAOD.Text = abstractRecord.BCVAOD.ToString();
                // if (abstractRecord.BCVAOS != null) TextBoxBCVAOS.Text = abstractRecord.BCVAOS.ToString();
                if (abstractRecord.IOPODmmHg != null) TextBoxIOPODmmHg.Text = abstractRecord.IOPODmmHg.ToString();
                if (abstractRecord.IOPOSmmHg != null) TextBoxIOPOSmmHg.Text = abstractRecord.IOPOSmmHg.ToString();
                //if (abstractRecord.FollowUpExamDateAmount != null) TextBoxFollowUpExamDateAmount.Text = abstractRecord.FollowUpExamDateAmount.ToString();

                if (abstractRecord.BCVAOS != null) DropDownListBCVAOS.SelectedValue = abstractRecord.BCVAOS.ToString();
                if (abstractRecord.BCVAOD != null) DropDownListBCVAOD.SelectedValue = abstractRecord.BCVAOD.ToString();
                if (abstractRecord.OutcomeBCVAOD != null) DropDownListOutcomeBCVAOD.SelectedValue = abstractRecord.OutcomeBCVAOD.ToString();
                if (abstractRecord.OutcomeBCVAOS != null) DropDownListOutcomeBCVAOS.SelectedValue = abstractRecord.OutcomeBCVAOS.ToString();

                if (abstractRecord.OutcomeIOPODmmHg != null) TextBoxOutcomeIOPODmmHg.Text = abstractRecord.OutcomeIOPODmmHg.ToString();
                if (abstractRecord.OutcomeIOPOSmmHg != null) TextBoxOutcomeIOPOSmmHg.Text = abstractRecord.OutcomeIOPOSmmHg.ToString();



            }
        }


    }

    public void savedata()
    {
        using (NetHealthPIMEntities pim = new NetHealthPIMEntities())
        {
            int ModuleID = (int)Session[Constants.SESSION_WORKINGMODULEID];
            int cycleID = ctx.ActiveModuleCycleID;
            ParticipantModuleCycle cycle = pim.ParticipantModuleCycle.First(c => c.ParticipantModuleCycleID == cycleID);
            string recordIdentifier = HiddenFieldRecordIdentifier.Value;
            using (TransactionScope transaction = new TransactionScope())
            {
                ChartAbstractionPACG abstractRecord = (from c in pim.ChartAbstractionPACG
                                                       where c.ParticipantModuleCycle.ParticipantModuleCycleID == cycleID & c.RecordIdentifier == recordIdentifier
                                                       select c).First<ChartAbstractionPACG>();



                if (RadioButtonListRBGender.SelectedIndex != -1) abstractRecord.RBGender = Convert.ToInt32(RadioButtonListRBGender.SelectedValue);
                else abstractRecord.RBGender = 0;
                if (RadioButtonListRBBCVAODNotAbletoPerform.SelectedIndex != -1) abstractRecord.RBBCVAODNotAbletoPerform = Convert.ToInt32(RadioButtonListRBBCVAODNotAbletoPerform.SelectedValue);
                else abstractRecord.RBBCVAODNotAbletoPerform = 0;
                if (RadioButtonListRBBCVAOSNotAbletoPerform.SelectedIndex != -1) abstractRecord.RBBCVAOSNotAbletoPerform = Convert.ToInt32(RadioButtonListRBBCVAOSNotAbletoPerform.SelectedValue);
                else abstractRecord.RBBCVAOSNotAbletoPerform = 0;
                if (RadioButtonListRBIOPODNotAbletoPerform.SelectedIndex != -1) abstractRecord.RBIOPODNotAbletoPerform = Convert.ToInt32(RadioButtonListRBIOPODNotAbletoPerform.SelectedValue);
                else abstractRecord.RBIOPODNotAbletoPerform = 0;
                if (RadioButtonListRBIOPOSNotAbletoPerform.SelectedIndex != -1) abstractRecord.RBIOPOSNotAbletoPerform = Convert.ToInt32(RadioButtonListRBIOPOSNotAbletoPerform.SelectedValue);
                else abstractRecord.RBIOPOSNotAbletoPerform = 0;
                if (RadioButtonListRBGonioscopyOD.SelectedIndex != -1) abstractRecord.RBGonioscopyOD = Convert.ToInt32(RadioButtonListRBGonioscopyOD.SelectedValue);
                else abstractRecord.RBGonioscopyOD = 0;
                if (RadioButtonListRBGonioscopyOS.SelectedIndex != -1) abstractRecord.RBGonioscopyOS = Convert.ToInt32(RadioButtonListRBGonioscopyOS.SelectedValue);
                else abstractRecord.RBGonioscopyOS = 0;
                if (RadioButtonListRBSlitLampOD.SelectedIndex != -1) abstractRecord.RBSlitLampOD = Convert.ToInt32(RadioButtonListRBSlitLampOD.SelectedValue);
                else abstractRecord.RBSlitLampOD = 0;
                if (RadioButtonListRBSlitLampOS.SelectedIndex != -1) abstractRecord.RBSlitLampOS = Convert.ToInt32(RadioButtonListRBSlitLampOS.SelectedValue);
                else abstractRecord.RBSlitLampOS = 0;
                if (RadioButtonListRBOpticNerveApperanceBothEyes.SelectedIndex != -1) abstractRecord.RBOpticNerveApperanceBothEyes = Convert.ToInt32(RadioButtonListRBOpticNerveApperanceBothEyes.SelectedValue);
                else abstractRecord.RBOpticNerveApperanceBothEyes = 0;
                if (RadioButtonListRBAngleClosureTypeOD.SelectedIndex != -1) abstractRecord.RBAngleClosureTypeOD = Convert.ToInt32(RadioButtonListRBAngleClosureTypeOD.SelectedValue);
                else abstractRecord.RBAngleClosureTypeOD = 0;
                if (RadioButtonListRBAngleClosureTypeOS.SelectedIndex != -1) abstractRecord.RBAngleClosureTypeOS = Convert.ToInt32(RadioButtonListRBAngleClosureTypeOS.SelectedValue);
                else abstractRecord.RBAngleClosureTypeOS = 0;
                if (DropDownListFollowUpExamDateType.SelectedIndex == 0) abstractRecord.FollowUpExamDateType = Convert.ToInt32(DropDownListFollowUpExamDateType.SelectedValue);
                else abstractRecord.FollowUpExamDateType = 0;
                if (RadioButtonListOutcomeRBBCVAODNotAbletoPerform.SelectedIndex != -1) abstractRecord.OutcomeRBBCVAODNotAbletoPerform = Convert.ToInt32(RadioButtonListOutcomeRBBCVAODNotAbletoPerform.SelectedValue);
                else abstractRecord.OutcomeRBBCVAODNotAbletoPerform = 0;
                if (RadioButtonListOutcomeRBBCVAOSNotAbletoPerform.SelectedIndex != -1) abstractRecord.OutcomeRBBCVAOSNotAbletoPerform = Convert.ToInt32(RadioButtonListOutcomeRBBCVAOSNotAbletoPerform.SelectedValue);
                else abstractRecord.OutcomeRBBCVAOSNotAbletoPerform = 0;
                if (RadioButtonListRBOutcomeIOPODNotAbletoPerform.SelectedIndex != -1) abstractRecord.RBOutcomeIOPODNotAbletoPerform = Convert.ToInt32(RadioButtonListRBOutcomeIOPODNotAbletoPerform.SelectedValue);
                else abstractRecord.RBOutcomeIOPODNotAbletoPerform = 0;
                if (RadioButtonListRBOutcomeIOPOSNotAbletoPerform.SelectedIndex != -1) abstractRecord.RBOutcomeIOPOSNotAbletoPerform = Convert.ToInt32(RadioButtonListRBOutcomeIOPOSNotAbletoPerform.SelectedValue);
                else abstractRecord.RBOutcomeIOPOSNotAbletoPerform = 0;
                if (RadioButtonListRBOutcomeIridotomyOD.SelectedIndex != -1) abstractRecord.RBOutcomeIridotomyOD = Convert.ToInt32(RadioButtonListRBOutcomeIridotomyOD.SelectedValue);
                else abstractRecord.RBOutcomeIridotomyOD = 0;
                if (RadioButtonListRBOutcomeIridotomyOS.SelectedIndex != -1) abstractRecord.RBOutcomeIridotomyOS = Convert.ToInt32(RadioButtonListRBOutcomeIridotomyOS.SelectedValue);
                else abstractRecord.RBOutcomeIridotomyOS = 0;
                if (RadioButtonListRBOutcomeIncreasingCataractOD.SelectedIndex != -1) abstractRecord.RBOutcomeIncreasingCataractOD = Convert.ToInt32(RadioButtonListRBOutcomeIncreasingCataractOD.SelectedValue);
                else abstractRecord.RBOutcomeIncreasingCataractOD = 0;
                if (RadioButtonListRBOutcomeIncreasingCataractOS.SelectedIndex != -1) abstractRecord.RBOutcomeIncreasingCataractOS = Convert.ToInt32(RadioButtonListRBOutcomeIncreasingCataractOS.SelectedValue);
                else abstractRecord.RBOutcomeIncreasingCataractOS = 0;
                if (RadioButtonListRBOpticNerveAppearanceChangeFromBaselineOD.SelectedIndex != -1) abstractRecord.RBOpticNerveAppearanceChangeFromBaselineOD = Convert.ToInt32(RadioButtonListRBOpticNerveAppearanceChangeFromBaselineOD.SelectedValue);
                else abstractRecord.RBOpticNerveAppearanceChangeFromBaselineOD = 0;
                if (RadioButtonListRBOpticNerveAppearanceChangeFromBaselineOS.SelectedIndex != -1) abstractRecord.RBOpticNerveAppearanceChangeFromBaselineOS = Convert.ToInt32(RadioButtonListRBOpticNerveAppearanceChangeFromBaselineOS.SelectedValue);
                else abstractRecord.RBOpticNerveAppearanceChangeFromBaselineOS = 0;

                if (RadioButtonListOcularMedications.SelectedIndex != -1) abstractRecord.OcularMedications = Convert.ToInt32(RadioButtonListOcularMedications.SelectedValue);
                else abstractRecord.OcularMedications = 0;

                if (RadioButtonListSystemicMedications.SelectedIndex != -1) abstractRecord.SystemicMedications = Convert.ToInt32(RadioButtonListSystemicMedications.SelectedValue);
                else abstractRecord.SystemicMedications = 0;

                if (DropDownListMonthOfBirth.SelectedIndex > 0) abstractRecord.MonthOfBirth = Convert.ToInt32(DropDownListMonthOfBirth.SelectedValue);
                else abstractRecord.MonthOfBirth = 0;
               
                if (DropDownListFollowUpExamDateAmount.SelectedIndex > 0) abstractRecord.FollowUpExamDateAmount = Convert.ToInt32(DropDownListFollowUpExamDateAmount.SelectedValue);
                else abstractRecord.FollowUpExamDateAmount = 0;



                if (DropDownListFollowUpExamDateType.SelectedIndex > 0) 
                    abstractRecord.FollowUpExamDateType = Convert.ToInt32(DropDownListFollowUpExamDateType.SelectedValue);
                else abstractRecord.FollowUpExamDateType = 0;

                if (DropDownListYearOfBirth.SelectedIndex > 0) abstractRecord.YearOfBirth = Convert.ToInt32(DropDownListYearOfBirth.SelectedValue);
                else abstractRecord.YearOfBirth = 0;
                if (DropDownListMonthofExam.SelectedIndex > 0) abstractRecord.MonthOfExam = Convert.ToInt32(DropDownListMonthofExam.SelectedValue);
                else abstractRecord.MonthOfExam = 0;
                if (DropDownListMonthofYear.SelectedIndex > 0) abstractRecord.YearOfExam = Convert.ToInt32(DropDownListMonthofYear.SelectedValue);
                else abstractRecord.YearOfExam = 0;
                if (DropDownListOutcomeExamMonth.SelectedIndex > 0) abstractRecord.OutcomeExamMonth = Convert.ToInt32(DropDownListOutcomeExamMonth.SelectedValue);
                else abstractRecord.OutcomeExamMonth = 0;
                if (DropDownListOutcomeExamYear.SelectedIndex > 0) abstractRecord.OutcomeExamYear = Convert.ToInt32(DropDownListOutcomeExamYear.SelectedValue);
                else abstractRecord.OutcomeExamYear = 0;
                if (DropDownListOutcomeOpticNerveExamODMonth.SelectedIndex > 0) abstractRecord.OutcomeOpticNerveExamODMonth = Convert.ToInt32(DropDownListOutcomeOpticNerveExamODMonth.SelectedValue);
                else abstractRecord.OutcomeOpticNerveExamODMonth = 0;
                if (DropDownListOutcomeOpticNerveExamODYear.SelectedIndex > 0) abstractRecord.OutcomeOpticNerveExamODYear = Convert.ToInt32(DropDownListOutcomeOpticNerveExamODYear.SelectedValue);
                else abstractRecord.OutcomeOpticNerveExamODYear = 0;
                if (DropDownListOutcomeOpticNerveExamOSMonth.SelectedIndex > 0) abstractRecord.OutcomeOpticNerveExamOSMonth = Convert.ToInt32(DropDownListOutcomeOpticNerveExamOSMonth.SelectedValue);
                else abstractRecord.OutcomeOpticNerveExamOSMonth = 0;
                if (DropDownListOutcomeOpticNerveExamOSYear.SelectedIndex > 0) abstractRecord.OutcomeOpticNerveExamOSYear = Convert.ToInt32(DropDownListOutcomeOpticNerveExamOSYear.SelectedValue);
                else abstractRecord.OutcomeOpticNerveExamOSYear = 0;




                if (RadioButtonOcularHistoryYes.Checked) abstractRecord.OcularHistory = true;
                if (RadioButtonOcularHistoryNo.Checked) abstractRecord.OcularHistory = false;
                if (RadioButtonManagementPlanFormulatedYes.Checked) abstractRecord.ManagementPlanFormulated = true;
                if (RadioButtonManagementPlanFormulatedNo.Checked) abstractRecord.ManagementPlanFormulated = false;
                if (RadioButtonFollowUpExamScheduledYes.Checked ) abstractRecord.FollowUpExamScheduled = true;
                if (RadioButtonFollowUpExamScheduledNo.Checked) abstractRecord.FollowUpExamScheduled = false;
                if (RadioButtonIridotomyYes.Checked) abstractRecord.Iridotomy = true;
                if (RadioButtonIridotomyNo.Checked) abstractRecord.Iridotomy = false;
                if (RadioButtonCataractSurgeryYes.Checked) abstractRecord.CataractSurgery = true;
                if (RadioButtonCataractSurgeryNo.Checked) abstractRecord.CataractSurgery = false;
                if (RadioButtonFollowUpGoniosynechialysisYes.Checked) abstractRecord.FollowUpGoniosynechialysis = true;
                if (RadioButtonFollowUpGoniosynechialysisNo.Checked) abstractRecord.FollowUpGoniosynechialysis = false;

                    if (DropDownListBCVAOS.SelectedIndex > 0) abstractRecord.BCVAOS = Convert.ToInt32(DropDownListBCVAOS.SelectedValue);
                    else abstractRecord.BCVAOS = 0;
                    if (DropDownListBCVAOD.SelectedIndex > 0) abstractRecord.BCVAOD = Convert.ToInt32(DropDownListBCVAOD.SelectedValue);
                    else abstractRecord.BCVAOD = 0;
                    if (DropDownListOutcomeBCVAOD.SelectedIndex > 0) abstractRecord.OutcomeBCVAOD = Convert.ToInt32(DropDownListOutcomeBCVAOD.SelectedValue);
                    else abstractRecord.OutcomeBCVAOD = 0;
                    if (DropDownListOutcomeBCVAOS.SelectedIndex > 0) abstractRecord.OutcomeBCVAOS = Convert.ToInt32(DropDownListOutcomeBCVAOS.SelectedValue);
                    else abstractRecord.OutcomeBCVAOS = 0;


                //try
                //{

               
               

                try
                {
                    if (TextBoxIOPODmmHg.Text != null)
                        abstractRecord.IOPODmmHg = Convert.ToDouble(TextBoxIOPODmmHg.Text);
                }
                catch (Exception ex)
                {
                    abstractRecord.IOPODmmHg = null;
                    TextBoxIOPODmmHg.Text = "";
                }
                try
                {
                    if (TextBoxIOPOSmmHg.Text != null)
                        abstractRecord.IOPOSmmHg = Convert.ToDouble(TextBoxIOPOSmmHg.Text);
                }
                catch (Exception ex)
                {
                    abstractRecord.IOPOSmmHg = null;
                    TextBoxIOPOSmmHg.Text = "";
                }
                
                //try
                //{
                //if (TextBoxFollowUpExamDateAmount.Text != null)
                //abstractRecord.FollowUpExamDateAmount = TextBoxFollowUpExamDateAmount.Text;
                //}
                //catch (Exception ex)
                //{
                //abstractRecord.FollowUpExamDateAmount = null;
                //TextBoxFollowUpExamDateAmount.Text = "";
                //}
                //try
                //{
                //if (TextBoxOutcomeBCVAOD.Text != null)
                //abstractRecord.OutcomeBCVAOD = TextBoxOutcomeBCVAOD.Text;
                //}
                //catch (Exception ex)
                //{
                //abstractRecord.OutcomeBCVAOD = null;
                //TextBoxOutcomeBCVAOD.Text = "";
                //}
                //try
                //{
                //if (TextBoxOutcomeBCVAOS.Text != null)
                //abstractRecord.OutcomeBCVAOS = TextBoxOutcomeBCVAOS.Text;
                //}
                //catch (Exception ex)
                //{
                //abstractRecord.OutcomeBCVAOS = null;
                //TextBoxOutcomeBCVAOS.Text = "";
                //}
                try
                {
                    if (TextBoxOutcomeIOPODmmHg.Text != null)
                        abstractRecord.OutcomeIOPODmmHg = Convert.ToDouble(TextBoxOutcomeIOPODmmHg.Text);
                }
                catch (Exception ex)
                {
                    abstractRecord.OutcomeIOPODmmHg = null;
                    TextBoxOutcomeIOPODmmHg.Text = "";
                }
                try
                {
                    if (TextBoxOutcomeIOPOSmmHg.Text != null)
                        abstractRecord.OutcomeIOPOSmmHg = Convert.ToDouble(TextBoxOutcomeIOPOSmmHg.Text);
                }
                catch (Exception ex)
                {
                    abstractRecord.OutcomeIOPOSmmHg = null;
                    TextBoxOutcomeIOPOSmmHg.Text = "";
                }
                abstractRecord.LastUpdateDate = DateTime.Now;
                bool ChartCompleted = isComplete();
                abstractRecord.Complete = ChartCompleted;
                var chartRegistration = (from cr in pim.ChartRegistration
                                         where cr.ParticipantModuleCycleID == cycle.ParticipantModuleCycleID
                                          & cr.ModuleID == ModuleID
                                          & cr.RecordIdentifier == recordIdentifier
                                         select cr).First<NetHealthPIMModel.ChartRegistration>();
                chartRegistration.AbstractCompleted = ChartCompleted;

                if ((DropDownListYearOfBirth.SelectedIndex > 0) && (DropDownListYearOfBirth.SelectedIndex > 0))
                {
                    chartRegistration.DOB = abstractRecord.MonthOfBirth + "/" + abstractRecord.YearOfBirth;
                }
                if ((DropDownListMonthofExam.SelectedIndex > 0) && (DropDownListMonthofYear.SelectedIndex > 0))
                {
                    chartRegistration.InitialVisit = abstractRecord.MonthOfExam + "/" + abstractRecord.YearOfExam;
                }
                pim.SaveChanges();

                transaction.Complete();




            }

            CycleManager.UpdateParticipantCompletedModules(cycleID, ModuleID);
        }


    }

    
    protected void ButtonSubmit_Click(object sender, EventArgs e)
    {
        savedata();
        isComplete();
        if (!isComplete())
        {
            isComplete();
            string script = " var answer = confirm('Chart data is not complete.\\nClick OK to save entries and exit chart.\\nClick CANCEL to save entries and review the chart. '); if (answer==true) window.location = '../PatientChartRegistration.aspx';";
            ScriptManager.RegisterStartupScript(this, GetType(),
                          "ServerControlScript", script, true);
        }
        else
        {

            Response.Redirect("../PatientChartRegistration.aspx");
        }
    }

    protected bool isComplete()
    {
        bool ChartCompleted = true;

        LabelRBGender.Visible = false;

        LabelIOP.Visible = false;

        LabelBCVAOD.Visible = false;

        LabelRBGonioscopy.Visible = false;


        LabelRBSlitLamp.Visible = false;

        LabelRBOpticNerveApperanceBothEyes.Visible = false;

    

        LabelRBAngleClosureType.Visible = false;

       /// LabelOutcomeIOP.Visible = false;


        LabelRBOutcomeIridotomy.Visible = false;

        LabelRBOutcomeIncreasingCataract.Visible = false;

        LabelRBOpticNerveAppearanceChangeFromBaseline.Visible = false;

       




        LabelMonthOfBirth.Visible = false;

        LabelYearOfBirth.Visible = false;
        LabelMonthofExam.Visible = false;

        LabelMonthofYear.Visible = false;


        LabelOutcomeExamMonth.Visible = false;


        LabelOutcomeExamYear.Visible = false;

        LabelOutcomeOpticNerveExamODMonth.Visible = false;


        LabelOutcomeOpticNerveExamODYear.Visible = false;

        LabelOutcomeOpticNerveExamOSMonth.Visible = false;

        LabelOutcomeOpticNerveExamOSYear.Visible = false;

        LabelOutcomeBCVA.Visible = false;


        LabelOcularMedications.Visible = false;

        LabelSystemicMedications.Visible = false;

        LabelOcularHistory.Visible = false;

        LabelManagementPlanFormulated.Visible = false;

        LabelFollowUpExamScheduled.Visible = false;

        LabelIridotomy.Visible = false;

        LabelCataractSurgery.Visible = false;

        LabelGoniosynechialysis.Visible = false;

        Labelinitialage.Visible = false;
        NetHealthPIMEntities pim = new NetHealthPIMEntities();
        var moduleinfo = (from c in pim.Module where c.ModuleID == 21 select c).FirstOrDefault();
        string message = "Patient must be  at least " + moduleinfo.RegistrationMinAge + " years old<br />";
        string registrationminage = Validation.validateRegistrationMinAge(Convert.ToInt32(DropDownListMonthofYear.SelectedValue), Convert.ToInt32(DropDownListYearOfBirth.SelectedValue), Convert.ToInt32(moduleinfo.RegistrationMinAge), message);

        if (registrationminage != "")
        {
            Labelinitialage.Text = message;
            Labelinitialage.Visible = true;
            ChartCompleted = false;
        }


        if (RadioButtonListRBGender.SelectedIndex == -1)
        {
            LabelRBGender.Visible = true;
            ChartCompleted = false;
        }
        if (RadioButtonListRBIOPODNotAbletoPerform.SelectedIndex == -1 && TextBoxIOPODmmHg.Text=="")
        {
            LabelIOP.Visible = true;
            ChartCompleted = false;
        }
        if (RadioButtonListRBIOPOSNotAbletoPerform.SelectedIndex == -1 && TextBoxIOPOSmmHg.Text == "")
        {
            LabelIOP.Visible = true;
            ChartCompleted = false;
        }
        if (RadioButtonListRBGonioscopyOD.SelectedIndex == -1)
        {
            LabelRBGonioscopy.Visible = true;
            ChartCompleted = false;
        }
        if (RadioButtonListRBGonioscopyOS.SelectedIndex == -1)
        {
            LabelRBGonioscopy.Visible = true;
            ChartCompleted = false;
        }
        if (RadioButtonListRBSlitLampOD.SelectedIndex == -1)
        {
            LabelRBSlitLamp.Visible = true;
            ChartCompleted = false;
        }
        if (RadioButtonListRBSlitLampOS.SelectedIndex == -1)
        {
            LabelRBSlitLamp.Visible = true;
            ChartCompleted = false;
        }
        if (RadioButtonListRBOpticNerveApperanceBothEyes.SelectedIndex == -1)
        {
            LabelRBOpticNerveApperanceBothEyes.Visible = true;
            ChartCompleted = false;
        }
        if (RadioButtonListRBAngleClosureTypeOD.SelectedIndex == -1)
        {
            LabelRBAngleClosureType.Visible = true;
            ChartCompleted = false;
        }
        if (RadioButtonListRBAngleClosureTypeOS.SelectedIndex == -1)
        {
            LabelRBAngleClosureType.Visible = true;
            ChartCompleted = false;
        }
        if (RadioButtonListRBOutcomeIOPOSNotAbletoPerform.SelectedIndex == -1)
        {
            //LabelRBOutcomeIOPOSNotAbletoPerform.Visible = true;
            //ChartCompleted = false;
        }
        if (RadioButtonListRBOutcomeIridotomyOD.SelectedIndex == -1)
        {
            LabelRBOutcomeIridotomy.Visible = true;
            ChartCompleted = false;
        }
        if (RadioButtonListRBOutcomeIridotomyOS.SelectedIndex == -1)
        {
            LabelRBOutcomeIridotomy.Visible = true;
            ChartCompleted = false;
        }
        if (RadioButtonListRBOutcomeIncreasingCataractOD.SelectedIndex == -1)
        {
            LabelRBOutcomeIncreasingCataract.Visible = true;
            ChartCompleted = false;
        }
        if (RadioButtonListRBOutcomeIncreasingCataractOS.SelectedIndex == -1)
        {
            LabelRBOutcomeIncreasingCataract.Visible = true;
            ChartCompleted = false;
        }
        if (RadioButtonListRBOpticNerveAppearanceChangeFromBaselineOD.SelectedIndex == -1)
        {
            LabelRBOpticNerveAppearanceChangeFromBaseline.Visible = true;
            ChartCompleted = false;
        }
        if (RadioButtonListRBOpticNerveAppearanceChangeFromBaselineOS.SelectedIndex == -1)
        {
            LabelRBOpticNerveAppearanceChangeFromBaseline.Visible = true;
            ChartCompleted = false;
        }
        if (RadioButtonListRBBCVAODNotAbletoPerform.SelectedIndex == -1 && DropDownListBCVAOD.SelectedIndex == 0)
        {
            LabelBCVAOD.Visible = true;
            ChartCompleted = false;
        }
        if (RadioButtonListRBBCVAOSNotAbletoPerform.SelectedIndex == -1 && DropDownListBCVAOS.SelectedIndex == 0)
        {
            LabelBCVAOD.Visible = true;
            ChartCompleted = false;
        }
        if (DropDownListMonthOfBirth.SelectedIndex == 0)
        {
            LabelMonthOfBirth.Visible = true;
            ChartCompleted = false;
        }
        if (DropDownListYearOfBirth.SelectedIndex == 0)
        {
            LabelYearOfBirth.Visible = true;
            ChartCompleted = false;
        }
        if (DropDownListMonthofYear.SelectedIndex == 0)
        {
            LabelMonthofExam.Visible = true;
            ChartCompleted = false;
        }
        if (DropDownListMonthofExam.SelectedIndex == 0)
        {
            LabelMonthofYear.Visible = true;
            ChartCompleted = false;
        }
        if (DropDownListOutcomeExamMonth.SelectedIndex == 0)
        {
            LabelOutcomeExamMonth.Visible = true;
            ChartCompleted = false;
        }
        if (DropDownListOutcomeExamYear.SelectedIndex == 0)
        {
            LabelOutcomeExamYear.Visible = true;
            ChartCompleted = false;
        }
        if (DropDownListOutcomeOpticNerveExamODMonth.SelectedIndex == 0)
        {
            LabelOutcomeOpticNerveExamODMonth.Visible = true;
            ChartCompleted = false;
        }
        if (DropDownListOutcomeOpticNerveExamODYear.SelectedIndex == 0)
        {
            LabelOutcomeOpticNerveExamODYear.Visible = true;
            ChartCompleted = false;
        }
        if (DropDownListOutcomeOpticNerveExamOSMonth.SelectedIndex == 0)
        {
            LabelOutcomeOpticNerveExamOSMonth.Visible = true;
            ChartCompleted = false;
        }
        if (DropDownListOutcomeOpticNerveExamOSYear.SelectedIndex == 0)
        {
            LabelOutcomeOpticNerveExamOSYear.Visible = true;
            ChartCompleted = false;
        }
       

        if (RadioButtonListOcularMedications.SelectedIndex==-1)
        {
            LabelOcularMedications.Visible = true;
            ChartCompleted = false;
        }
        if (RadioButtonListSystemicMedications.SelectedIndex==-1)
        {
            LabelSystemicMedications.Visible = true;
            ChartCompleted = false;
        }
        if (RadioButtonOcularHistoryNo.Checked == false && RadioButtonOcularHistoryYes.Checked == false)
        {
            LabelOcularHistory.Visible = true;
            ChartCompleted = false;
        }
        if (RadioButtonManagementPlanFormulatedNo.Checked == false && RadioButtonManagementPlanFormulatedYes.Checked == false)
        {
            LabelManagementPlanFormulated.Visible = true;
            ChartCompleted = false;
        }
        if (RadioButtonFollowUpExamScheduledNo.Checked == false && RadioButtonFollowUpExamScheduledYes.Checked == false)
        {
            LabelFollowUpExamScheduled.Visible = true;
            ChartCompleted = false;
        }
        if (RadioButtonIridotomyNo.Checked == false && RadioButtonIridotomyYes.Checked == false)
        {
            LabelIridotomy.Visible = true;
            ChartCompleted = false;
        }
        if (RadioButtonCataractSurgeryNo.Checked == false && RadioButtonCataractSurgeryYes.Checked == false)
        {
            LabelCataractSurgery.Visible = true;
            ChartCompleted = false;
        }
        if (RadioButtonFollowUpGoniosynechialysisNo.Checked == false && RadioButtonFollowUpGoniosynechialysisYes.Checked == false)
        {
            LabelGoniosynechialysis.Visible = true;
            ChartCompleted = false;
        }
        if (DropDownListOutcomeBCVAOS.SelectedIndex == 0 && RadioButtonListOutcomeRBBCVAOSNotAbletoPerform.SelectedIndex == -1)
                {
                    LabelOutcomeBCVA.Visible = true;
                    ChartCompleted = false;
                
                }
        if (DropDownListOutcomeBCVAOD.SelectedIndex == 0 && RadioButtonListOutcomeRBBCVAODNotAbletoPerform.SelectedIndex == -1)
        {
            LabelOutcomeBCVA.Visible = true;
            ChartCompleted = false;

        }
     

        return ChartCompleted;
    }
















}
