﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Transactions;
using NetHealthPIMModel;

public partial class abo_PGFSRChart : BasePage
{
    protected void Page_Load(object sender, EventArgs e)
    {
        if (!IsPostBack)
        {
            InitializePage();
        }


    }
    protected void InitializePage()
    {
        ((PIMMasterPage)Page.Master).SetTitle("Chart");

        using (NetHealthPIMEntities pim = new NetHealthPIMEntities())
        {
            int ModuleID = (int)Session[Constants.SESSION_WORKINGMODULEID];
            Module module = pim.Module.First(c => c.ModuleID == ModuleID);
            ((PIMMasterPage)Page.Master).SetHeader(module.ModuleName + " Chart Abstraction");

            ParticipantModuleCycle prev = (ParticipantModuleCycle)Session[Constants.SESSION_PREVIOUSCYCLE];
            String CycleNumber = Request.QueryString["CycleNumber"];

            int cycleID = ctx.ActiveModuleCycleID;
            if (CycleNumber == "1")
            {
                cycleID = prev.ParticipantModuleCycleID;
                LinkButtonBackToDashboard.Visible = true;
                ButtonSubmit.Visible = false;
            }

            ParticipantModuleCycle cycle = pim.ParticipantModuleCycle.First(c => c.ParticipantModuleCycleID == cycleID);
            String numberOfRecord = Request.QueryString["nr"];
            String totalRecords = Request.QueryString["t"];
            if (cycle.CycleNumber == 1)
            {
                ((PIMMasterPage)Page.Master).SetStatus(2);
                LiteralAbstractionNumber.Text = "Initial Abstraction: Chart " + numberOfRecord + " of " + totalRecords;
            }
            else
            {
                ((PIMMasterPage)Page.Master).SetStatus(3);
                LiteralAbstractionNumber.Text = "Second Abstraction: Chart " + numberOfRecord + " of " + totalRecords;
            }
            string recordIdentifier = Request.QueryString["ri"];
            HiddenFieldRecordIdentifier.Value = recordIdentifier;
            LiteralRecordIdentifier.Text = recordIdentifier;
            var count = pim.ChartAbstractionPathologyGlobeRB.Where(ps => ps.ParticipantModuleCycle.ParticipantModuleCycleID == cycleID & ps.RecordIdentifier == recordIdentifier).Count();
            if (count == 0)
            {
                using (TransactionScope transaction = new TransactionScope())
                {
                    //Module module = pim.Module.First(m => m.ModuleID == Constants.ABO_AMBLYOPIA_MODULEID);

                    NetHealthPIMModel.ChartRegistration chartRegistration = (from cr in pim.ChartRegistration
                                                           where cr.ParticipantModuleCycleID == cycleID
                                                           & cr.RecordIdentifier == recordIdentifier
                                                           & cr.ModuleID == 64
                                                           select cr).First<NetHealthPIMModel.ChartRegistration>();

                    DateTime initialVisit = Convert.ToDateTime(chartRegistration.InitialVisit);
                    DateTime DOB = Convert.ToDateTime(chartRegistration.DOB);
                    ChartAbstractionPathologyGlobeRB abstractRecord = new ChartAbstractionPathologyGlobeRB();
                    abstractRecord.ParticipantModuleCycle = cycle;
                    abstractRecord.RecordIdentifier = recordIdentifier;
                    abstractRecord.CreatedOn = DateTime.Now;
                    abstractRecord.LastUpdateDate = DateTime.Now;

                    pim.SaveChanges();
                    transaction.Complete();
                }
            }
            else
            {
                ChartAbstractionPathologyGlobeRB abstractRecord = (from c in pim.ChartAbstractionPathologyGlobeRB
                                                                   where c.ParticipantModuleCycle.ParticipantModuleCycleID == cycleID & c.RecordIdentifier == recordIdentifier
                                                                   select c).First<ChartAbstractionPathologyGlobeRB>();

                if (abstractRecord.GlobeOpened != null)
                {
                    RadioButtonGlobeOpenedYes.Checked = ((bool)abstractRecord.GlobeOpened);
                    RadioButtonGlobeOpenedNo.Checked = !((bool)abstractRecord.GlobeOpened);
                }
                if (abstractRecord.CapsSectionedChoroidalInvasion != null)
                {
                    RadioButtonCapsSectionedChoroidalInvasionYes.Checked = ((bool)abstractRecord.CapsSectionedChoroidalInvasion);
                    RadioButtonCapsSectionedChoroidalInvasionNo.Checked = !((bool)abstractRecord.CapsSectionedChoroidalInvasion);
                }
                if (abstractRecord.ReportPresenceChoroidalInvasion != null)
                {
                    RadioButtonReportPresenceChoroidalInvasionYes.Checked = ((bool)abstractRecord.ReportPresenceChoroidalInvasion);
                    RadioButtonReportPresenceChoroidalInvasionNo.Checked = !((bool)abstractRecord.ReportPresenceChoroidalInvasion);
                }
                if (abstractRecord.ReportPresenceOpticNerveInvasion != null)
                {
                    RadioButtonReportPresenceOpticNerveInvasionYes.Checked = ((bool)abstractRecord.ReportPresenceOpticNerveInvasion);
                    RadioButtonReportPresenceOpticNerveInvasionNo.Checked = !((bool)abstractRecord.ReportPresenceOpticNerveInvasion);
                }
                if (abstractRecord.HighRiskFeatures != null)
                {
                    RadioButtonHighRiskFeaturesYes.Checked = ((bool)abstractRecord.HighRiskFeatures);
                    RadioButtonHighRiskFeaturesNo.Checked = !((bool)abstractRecord.HighRiskFeatures);
                }
                if (abstractRecord.HighRiskFeaturesReportedClinician != null)
                {
                    RadioButtonHighRiskFeaturesReportedClinicianYes.Checked = ((bool)abstractRecord.HighRiskFeaturesReportedClinician);
                    RadioButtonHighRiskFeaturesReportedClinicianNo.Checked = !((bool)abstractRecord.HighRiskFeaturesReportedClinician);
                }
                if (abstractRecord.ReportTurnaroundDays != null) TextBoxReportTurnaroundDays.Text = abstractRecord.ReportTurnaroundDays.ToString();
                if (abstractRecord.LengthOfOpticNerve != null) TextBoxLengthOfOpticNerve.Text = abstractRecord.LengthOfOpticNerve.ToString();
                if (abstractRecord.LengthOfOpticNerveNR != null) CheckboxLengthOfOpticNerveNR.Checked = (bool)abstractRecord.LengthOfOpticNerveNR;
            }
        }
    }

    public void savedata()
    {
        using (NetHealthPIMEntities pim = new NetHealthPIMEntities())
        {
            int ModuleID = (int)Session[Constants.SESSION_WORKINGMODULEID];
            int cycleID = ctx.ActiveModuleCycleID;
            ParticipantModuleCycle cycle = pim.ParticipantModuleCycle.First(c => c.ParticipantModuleCycleID == cycleID);
            string recordIdentifier = HiddenFieldRecordIdentifier.Value;
            using (TransactionScope transaction = new TransactionScope())
            {
                ChartAbstractionPathologyGlobeRB abstractRecord = (from c in pim.ChartAbstractionPathologyGlobeRB
                                                                   where c.ParticipantModuleCycle.ParticipantModuleCycleID == cycleID & c.RecordIdentifier == recordIdentifier
                                                                   select c).First<ChartAbstractionPathologyGlobeRB>();







                if (RadioButtonGlobeOpenedYes.Checked) abstractRecord.GlobeOpened = true;
                if (RadioButtonGlobeOpenedNo.Checked) abstractRecord.GlobeOpened = false;


                if (RadioButtonCapsSectionedChoroidalInvasionYes.Checked) abstractRecord.CapsSectionedChoroidalInvasion = true;
                if (RadioButtonCapsSectionedChoroidalInvasionNo.Checked) abstractRecord.CapsSectionedChoroidalInvasion = false;

                if (RadioButtonReportPresenceChoroidalInvasionYes.Checked) abstractRecord.ReportPresenceChoroidalInvasion = true;
                if (RadioButtonReportPresenceChoroidalInvasionNo.Checked) abstractRecord.ReportPresenceChoroidalInvasion = false;

                if (RadioButtonReportPresenceOpticNerveInvasionYes.Checked) abstractRecord.ReportPresenceOpticNerveInvasion = true;
                if (RadioButtonReportPresenceOpticNerveInvasionNo.Checked) abstractRecord.ReportPresenceOpticNerveInvasion = false;

                if (RadioButtonHighRiskFeaturesYes.Checked) abstractRecord.HighRiskFeatures = true;
                if (RadioButtonHighRiskFeaturesNo.Checked) abstractRecord.HighRiskFeatures = false;

                if (RadioButtonHighRiskFeaturesReportedClinicianYes.Checked) abstractRecord.HighRiskFeaturesReportedClinician = true;
                if (RadioButtonHighRiskFeaturesReportedClinicianNo.Checked) abstractRecord.HighRiskFeaturesReportedClinician = false;
                abstractRecord.LengthOfOpticNerveNR = CheckboxLengthOfOpticNerveNR.Checked;
               
                try
                {
                    if (TextBoxLengthOfOpticNerve.Text != null)
                        abstractRecord.LengthOfOpticNerve = Convert.ToInt32(TextBoxLengthOfOpticNerve.Text);
                }
                catch (Exception ex)
                {
                    abstractRecord.LengthOfOpticNerve = null;
                    TextBoxLengthOfOpticNerve.Text = "";
                }
                try
                {
                    if (TextBoxReportTurnaroundDays.Text != null)
                        abstractRecord.ReportTurnaroundDays = Convert.ToInt32(TextBoxReportTurnaroundDays.Text);
                }
                catch (Exception ex)
                {
                    abstractRecord.ReportTurnaroundDays = null;
                    TextBoxReportTurnaroundDays.Text = "";
                }
                abstractRecord.LastUpdateDate = DateTime.Now;
                bool ChartCompleted = isComplete();
                abstractRecord.Complete = ChartCompleted;
                var chartRegistration = (from cr in pim.ChartRegistration
                                         where cr.ParticipantModuleCycleID == cycle.ParticipantModuleCycleID
                                          & cr.ModuleID == ModuleID
                                          & cr.RecordIdentifier == recordIdentifier
                                         select cr).First<NetHealthPIMModel.ChartRegistration>();
                chartRegistration.AbstractCompleted = ChartCompleted;
                pim.SaveChanges();
                transaction.Complete();




            }

            CycleManager.UpdateParticipantCompletedModules(cycleID, ModuleID);
        }


    }

    protected void ButtonSubmit_Click(object sender, EventArgs e)
    {
        savedata();
        isComplete();
        if (!isComplete())
        {
            isComplete();
            string script = " var answer = confirm('Chart data is not complete.\\nClick OK to save entries and exit chart.\\nClick CANCEL to save entries and review the chart. '); if (answer==true) window.location = '../PatientChartRegistration.aspx';";
            ScriptManager.RegisterStartupScript(this, GetType(),
                          "ServerControlScript", script, true);
        }
        else
        {

            Response.Redirect("../PatientChartRegistration.aspx");
        }
    }


    protected bool isComplete()
    {

        bool ChartCompleted = true;


        LabelReportTurnaroundDays.Visible = false;

        LabelLengthOfOpticNerve.Visible = false;

        LabelCapsGlobeOpened.Visible = false;

        LabelCapsSectionedChoroidalInvasion.Visible = false;

        LabelReportPresenceChoroidalInvasion.Visible = false;

        LabelReportPresenceOpticNerveInvasion.Visible = false;

        LabelHighRiskFeatures.Visible = false;

        LabelHighRiskFeaturesReportedClinician.Visible = false;

        if (TextBoxLengthOfOpticNerve.Text == "" && CheckboxLengthOfOpticNerveNR.Checked == false)
        {

            LabelLengthOfOpticNerve.Visible = true;
            ChartCompleted = false;
        }


        if (RadioButtonGlobeOpenedYes.Checked == false && RadioButtonGlobeOpenedNo.Checked == false)
        {
            LabelCapsGlobeOpened.Visible = true;
            ChartCompleted = false;
        }
        if (RadioButtonCapsSectionedChoroidalInvasionYes.Checked == false && RadioButtonCapsSectionedChoroidalInvasionNo.Checked == false)
        {
            LabelCapsSectionedChoroidalInvasion.Visible = true;
            ChartCompleted = false;
        }

        if (RadioButtonReportPresenceChoroidalInvasionYes.Checked == false && RadioButtonReportPresenceChoroidalInvasionNo.Checked == false)
        {
            LabelReportPresenceChoroidalInvasion.Visible = true;
            ChartCompleted = false;
        }

        if (RadioButtonReportPresenceOpticNerveInvasionYes.Checked == false && RadioButtonReportPresenceOpticNerveInvasionNo.Checked == false)
        {
            LabelReportPresenceOpticNerveInvasion.Visible = true;
            ChartCompleted = false;
        }

        if (RadioButtonHighRiskFeaturesYes.Checked == false && RadioButtonHighRiskFeaturesNo.Checked == false)
        {
            LabelHighRiskFeatures.Visible = true;
            ChartCompleted = false;
        }
       
        if (RadioButtonHighRiskFeaturesYes.Checked == true)
        {
            if (RadioButtonHighRiskFeaturesReportedClinicianYes.Checked == false && RadioButtonHighRiskFeaturesReportedClinicianNo.Checked == false)
            {
                LabelHighRiskFeaturesReportedClinician.Visible = true;
                ChartCompleted = false;
            }
        }
        if (TextBoxReportTurnaroundDays.Text == "")
        {
            LabelReportTurnaroundDays.Visible = true;
            ChartCompleted = false;
        }


        return ChartCompleted;

    }

}
