﻿<%@ Page Title="" Language="C#" MasterPageFile="~/PIMMasterPage.master" AutoEventWireup="true" CodeFile="PBTAChart.aspx.cs" Inherits="abo_PBTAChart" %>

<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="Server">
    
    <link type="text/css" href="../../common/css/atooltip.css" rel="stylesheet" media="screen" />

    <style type="text/css">
        .bginputa{
	        float:right;
	        margin:21px 0 0 1px;
	        background: url(../common/images/orange_button_with_arrow.png) no-repeat;
	        overflow:hidden;
	        height:35px;

        }
        .bginputa a{
	        color: white;
	        text-align:center;
	        height:35px;
	        line-height:28px;
	        padding:0 14px 15px  ;
	        cursor:pointer;
	        float:left;
	        border:none;
	        text-decoration:none;
	        font-family:Arial, Helvetica, sans-serif; 
	        font-size:12px; 
	        font-weight:bold; 
	        letter-spacing: 0px;
        }
        .bginputa a:hover{
	        text-decoration:none;
        }
        .right {
            float:right;
            text-align:right;
        }

        label {
            padding-left:3px;
            margin-right:6px;
        }
    </style>

</asp:Content>
<asp:content id="Content2" contentplaceholderid="ContentPlaceHolder1" runat="Server">
    <asp:HiddenField ID="HiddenFieldRecordIdentifier" runat="server"/>
    <!-- CRVO pim -->
    <div class="pim">
        <div class="record-ident clearfix">
            <h3 class="record-first">RECORD IDENTIFIER: <asp:Literal runat="server" ID="LiteralRecordIdentifier"></asp:Literal></h3>
            <h3 class="record-second"><asp:Literal runat="server" ID="LiteralAbstractionNumber"></asp:Literal></h3>
        </div>
        <p><strong>Select consecutive biopsies for temporal arteritis and answer the following questions:</strong><br /><br /></p>
        <!-- History -->
        <table>
            <tr>
                <th colspan="2" width="910px"><p>Specimens&nbsp;<a href="#"><img id="header1" src="../../common/images/tip.gif" alt="Tip" /></a></p></th>
            </tr>
            <!-- Question 1 -->
            <tr class="table_body" id="q1">
                <td style="width:70%">
                    <strong>1. What was the report turnaround time (in days)?</strong>&nbsp;<a href="#"><img id="tip-q1" src="../../common/images/tip.gif" alt="Tip" /></a>
                    <asp:Label ID='LabelReportTurnaroundDays' runat="server" Visible="false"  ForeColor="Red" Text="<br />Please complete" />  
                </td>
                <td>
                    <asp:TextBox runat="server" ID="TextBoxReportTurnaroundDays" class="auto" meta="{mDec: '0'}" />
                </td>
            </tr>
            <!-- Question 2 -->
            <tr class="table_body_bg" id="q2">
                <td>
                    <strong>2. Was the length of the biopsy measured?</strong>&nbsp;<a href="#"><img id="tip-q2" src="../../common/images/tip.gif" alt="Tip" /></a>
                    <asp:Label ID='LabelLengthBiopsyMeasured' runat="server" Visible="false"  ForeColor="Red" Text="<br />Please complete" />
                </td>
                <td>
                    <asp:RadioButton runat='server' ID='RadioButtonLengthBiopsyMeasuredYes' GroupName='LengthBiopsyMeasured' text='Yes' />
                    <asp:RadioButton runat='server' ID='RadioButtonLengthBiopsyMeasuredNo' GroupName='LengthBiopsyMeasured' text='No' />
                </td>
            </tr>
            <!-- Question 3 -->
            <tr class="table_body" id="q3">
                <td>
                    <strong>3. Was the artery sectioned in cross section?</strong>
                    <asp:Label ID='LabelArteryCrossSection' runat="server" Visible="false"  ForeColor="Red" Text="<br />Please complete" />
                </td>
                <td>
                    <asp:RadioButton runat='server' ID='RadioButtonArteryCrossSectionYes' GroupName='ArteryCrossSection' text='Yes' />
                    <asp:RadioButton runat='server' ID='RadioButtonArteryCrossSectionNo' GroupName='ArteryCrossSection' text='No' />
                </td>
            </tr>
            <!-- Question 4 -->
            <tr class="table_body_bg" id="q4">
                <td>
                    <strong>4. Was the artery sampled at multiple levels?</strong>
                    <asp:Label ID='LabelArteryMultipleLevels' runat="server" Visible="false"  ForeColor="Red" Text="<br />Please complete" />
                </td>
                <td>
                    <asp:RadioButton runat='server' ID='RadioButtonArteryMultipleLevelsYes' GroupName='ArteryMultipleLevels' text='Yes' />
                    <asp:RadioButton runat='server' ID='RadioButtonArteryMultipleLevelsNo' GroupName='ArteryMultipleLevels' text='No' />
                </td>
            </tr>
            <!-- Question 5 -->
            <tr class="table_body" id="q5">
                <td>
                    <strong>5. Was the biopsy result positive?</strong>
                    <asp:Label ID='LabelBiopsyResultPositive' runat="server" Visible="false"  ForeColor="Red" Text="<br />Please complete" />
                </td>
                <td>
                    <asp:RadioButton runat='server' ID='RadioBiopsyResultPositiveYes' GroupName='ReportPresenceChoroidalInvasion' text='Yes' />
                    <asp:RadioButton runat='server' ID='RadioBiopsyResultPositiveNo' GroupName='ReportPresenceChoroidalInvasion' text='No' />
                </td>
            </tr>
            <!-- Question 6 -->
            <tr class="table_body_bg" id="q6">
                <td>
                    <strong>6. If yes, was the result called to the physician?</strong>
                    <asp:Label ID='LabelBiopsyResultToPhysician' runat="server" Visible="false"  ForeColor="Red" Text="<br />Please complete" />
                </td>
                <td>
                    <asp:RadioButton runat='server' ID='RadioButtonBiopsyResultToPhysicianYes' GroupName='BiopsyResultToPhysician' text='Yes' />
                    <asp:RadioButton runat='server' ID='RadioButtonBiopsyResultToPhysicianNo' GroupName='BiopsyResultToPhysician' text='No' />
                </td>
            </tr>
        </table>
        <div class="button-box">
            <asp:LinkButton ID="LinkButtonBackToDashboard" runat="server" Text="Back To Chart Registration" PostBackUrl="../PatientChartRegistration.aspx?CycleNumber=1" Visible="false" CssClass="button" />
            <asp:LinkButton ID="ButtonSubmit" OnClick="ButtonSubmit_Click" runat="server" Text="Submit Chart" CssClass="button" />
        </div>
    </div>
    <!-- ION pim ends -->

    <script src="//ajax.googleapis.com/ajax/libs/jquery/1.10.2/jquery.min.js"></script>
    <script type="text/javascript" src="../../common/js/jquery.atooltip.js"></script>
    <script type="text/javascript" src="../../common/js/jquery.metadata.js"></script>
    <script type="text/javascript" src="../../common/js/autoNumeric-1.7.5.js"></script>
    <script>
        $(function () {
            $('#header1').aToolTip({
                clickIt: true,
                tipContent: 'Identify the quality of sectioning of the specimen in your lab'
            });

            $('#tip-q1').aToolTip({
                clickIt: true,
                tipContent: 'Mean turnaround time or percentage based on reasonable cut off within two days'
            });
            $('#tip-q2').aToolTip({
                clickIt: true,
                tipContent: 'Rationale:  This is considered good data for possible measure development.'
            });
        });
    </script>
    <script type="text/javascript">
        $(document).ready(function () {
            /** instruct the metadata plugin where to look the metadata
            * jQuery.metadata.setType( type, name );
            * please read the metadata instructions for additional information
            * http://plugins.jquery.com/project/metadata
            */
            $.metadata.setType('attr', 'meta');

            /** To call autoNumeric
            * $(selector).autoNumeric({options}); 
            * The below example uses the input & class selector
            */
            $('input.auto').autoNumeric();
            /* scripts for metadata code generator  */

            /* rountine that prevents  numeric characters from being entered the the altDec field  */
            $('#altDecb').keypress(function (e) {
                var cc = String.fromCharCode(e.which);
                if (e.which != 32 && cc >= 0 && cc <= 9) {
                    e.preventDefault();
                }
            });

            /* rountine that prevents  apostrophe, comma, more than one period (full stop) or numeric characters from being entered the the aSign field  */
            $('#aSignb').keypress(function (e) {
                var cc = String.fromCharCode(e.which);
                if ((e.which != 32 && cc >= 0 && cc <= 9) || cc == "," || cc == "'" || cc == "." && this.value.lastIndexOf('.') != -1) {
                    e.preventDefault();
                }
            });

            $("input.md").bind('click keyup blur', function () {
                var metaCode = '', aSep = '', dGroup = '', aDec = '', altDec = '', aSign = '', pSign = '', vMin = '', vMax = '', mDec = '', mRound = '', aPad = '', wEmpty = '', aForm = '';
                if ($("input:radio[name=aSep]:checked").attr('id') == 'aSepc') {
                    $('input:radio[name=aDec]:nth(0)').removeAttr("disabled");
                    $('input:radio[name=aDec]:nth(0)').attr('checked', true);
                    $('input:radio[name=aDec]:nth(1)').attr("disabled", true);
                }
                if ($("input:radio[name=aSep]:checked").attr('id') == 'aSepp') {
                    $('input:radio[name=aDec]:nth(1)').removeAttr("disabled");
                    $('input:radio[name=aDec]:nth(1)').attr('checked', true);
                    $('input:radio[name=aDec]:nth(0)').attr("disabled", true);
                }
                if ($("input:radio[name=aSep]:checked").attr('id') != 'aSepc' || $("input:radio[name=aSep]:checked").attr('id') != 'aSepp') {
                    $('input:radio[name=aDec]:nth(0)').removeAttr("disabled");
                    $('input:radio[name=aDec]:nth(1)').removeAttr("disabled");
                }
                aSep = $("input:radio[name=aSep]:checked").val();
                dGroup = $("input:radio[name=dGroup]:checked").val();
                aDec = $("input:radio[name=aDec]:checked").val();

                if ($("input:radio[name=altDec]:checked").attr('id') == 'altDecd') {
                    $('#altDecb').val('');
                    $('#altDecb').attr("disabled", true);
                }
                if ($("input:radio[name=altDec]:checked").attr('id') == 'altDeca') {
                    $('#altDecb').removeAttr("disabled");
                    altDec = $('#altDecb').val();
                }

                if ($("input:radio[name=aSign]:checked").attr('id') == 'aSignd') {
                    $('#aSignb').val('');
                    $('#aSignb').attr("disabled", true);
                }
                if ($("input:radio[name=aSign]:checked").attr('id') == 'aSigna') {
                    $('#aSignb').removeAttr("disabled");
                    aSign = $('#aSignb').val();
                }

                pSign = $("input:radio[name=pSign]:checked").val();
                if ($("input:radio[name=vMin]:checked").attr('id') == 'vMind') {
                    $('#vMinb').val('');
                    $('#vMinb').attr("disabled", true);
                }
                if ($("input:radio[name=vMin]:checked").attr('id') == 'vMina') {
                    $('#vMinb').removeAttr("disabled");
                    vMin = $('#vMinb').val();
                }
                if ($("input:radio[name=vMax]:checked").attr('id') == 'vMaxd') {
                    $('#vMaxb').val('');
                    $('#vMaxb').attr("disabled", true);
                }
                if ($("input:radio[name=vMax]:checked").attr('id') == 'vMaxa') {
                    $('#vMaxb').removeAttr("disabled");
                    vMax = $('#vMaxb').val();
                }
                if ($("input:radio[name=mDec]:checked").attr('id') == 'mDecd') {
                    $('#mDecbb').val('');
                    $('#mDecbb').attr("disabled", true);
                }
                if ($("input:radio[name=mDec]:checked").attr('id') == 'mDeca') {
                    $('#mDecbb').removeAttr("disabled");
                    mDec = $('#mDecbb').val();
                }
                mRound = $("input:radio[name=mRound]:checked").val();
                aPad = $("input:radio[name=aPad]:checked").val();
                wEmpty = $("input:radio[name=wEmpty]:checked").val();
                if (aSep != '') {
                    metaCode = aSep;
                }
                if (dGroup != '') {
                    if (metaCode != '') {
                        metaCode = metaCode + ", " + dGroup;
                    }
                    else {
                        metaCode = dGroup;
                    }
                }
                if (aDec != '') {
                    if (metaCode != '') {
                        metaCode = metaCode + ", " + aDec;
                    }
                    else {
                        metaCode = aDec;
                    }
                }
                if (altDec != '') {
                    if (metaCode != '') {
                        metaCode = metaCode + ", altDec: '" + altDec + "'";
                    }
                    else {
                        metaCode = "altDec: '" + altDec + "'";
                    }
                }
                if (aSign != '') {
                    if (metaCode != '') {
                        metaCode = metaCode + ", aSign: '" + aSign + "'";
                    }
                    else {
                        metaCode = "aSign: '" + aSign + "'";
                    }
                }
                if (pSign != '') {
                    if (metaCode != '') {
                        metaCode = metaCode + ", " + pSign;
                    }
                    else {
                        metaCode = pSign;
                    }
                }
                if (vMin != '') {
                    if (metaCode != '') {
                        metaCode = metaCode + ", vMin: '" + vMin + "'";
                    }
                    else {
                        metaCode = "vMin: '" + vMin + "'";
                    }
                }
                if (vMax != '') {
                    if (metaCode != '') {
                        metaCode = metaCode + ", vMax: '" + vMax + "'";
                    }
                    else {
                        metaCode = "vMax: '" + vMax + "'";
                    }
                }
                if (mDec != '') {
                    if (metaCode != '') {
                        metaCode = metaCode + ", mDec: '" + $('#mDecbb').val() + "'";
                    }
                    else {
                        metaCode = "mDec: '" + $('#mDecbb').val() + "'";
                    }
                }

                if (mRound != '') {
                    if (metaCode != '') {
                        metaCode = metaCode + ", " + mRound;
                    }
                    else {
                        metaCode = mRound;
                    }
                }
                if (aPad != '') {
                    if (metaCode != '') {
                        metaCode = metaCode + ", " + aPad;
                    }
                    else {
                        metaCode = aPad;
                    }
                }
                if (wEmpty != '') {
                    if (metaCode != '') {
                        metaCode = metaCode + ", " + wEmpty;
                    }
                    else {
                        metaCode = wEmpty;
                    }
                }
                $('#metaCode').text('');
                if (metaCode != '') {
                    $('#metaCode').text('meta="{' + metaCode + '}"');
                }
            });

            /* clears the metadata code  */
            $('#rd').click(function () {
                $('#metaCode').text('');
            });
            /* ends scripts for metadata code generator  */

            /* script  for defaults demo  */
            $('#d_noMeta').blur(function () {
                var convertInput = '';
                convertInput = $(this).autoNumericGet();
                $('#d_Get').val(convertInput);
                $('#d_Set').autoNumericSet(convertInput);
            });
            /* end script  for defaults demo  */

            /* script  for various samples demo  */
            $('input[name$="sample"]').blur(function () {
                var convertInput = '';
                var row = 'row_' + this.id.charAt(4);
                convertInput = $(this).autoNumericGet();
                $('#' + row + 'b').val(convertInput);
                $('#' + row + 'c').autoNumericSet(convertInput);
            });
            /* end script  for various samples demo  */

            /* script  for rounding methods  */
            $('#roundValue').blur(function () {
                if (this.value != '') {
                    convertInput = $('#roundValue').autoNumericGet();
                    var i = 1;
                    for (i = 1; i <= 9; i++) {
                        $('#roundMethod' + i).autoNumericSet(convertInput);
                    }
                }
            });

            $('#roundDecimal').change(function () { /* changes decimal places */
                convertInput = $('#roundValue').autoNumericGet();
                if (convertInput > 0) {
                    var i = 1;
                    for (i = 1; i <= 9; i++) {
                        $('#roundMethod' + i).autoNumericSet(convertInput);
                    }
                }
            });
            /* end script  for rounding methods  */

            /* script for dynamically loaded values  demo*/
            $.getJSON("test_JSON.php", function (data) {
                var valueFormatted = '';
                $.each(data, function (key, value) { // loops through JSON keys and returns value	
                    $('#' + key).autoNumericSet(value);
                });
            });
            /* end script for dynamically loaded values demo*/

            /* script for callback demo*/
            $.autoNumeric.get_mDec = function () { /* get_mDec function attached to autoNumeric() */
                var set_mDec = $('#get_metricUnit').val();
                if (set_mDec == ' km') {
                    set_mDec = 3;
                } else {
                    set_mDec = 0;
                }
                return set_mDec; /* set mDec decimal places */
            }

            var get_vMax = function () { /* set the maximum value allowed based on the metric unit */
                var set_vMax = $('#get_metricUnit').val();
                if (set_vMax == ' km') {
                    set_vMax = '99999.999';
                } else {
                    set_vMax = '99999999';
                }
                return set_vMax;
            }

            $('#length').autoNumeric({ vMax: get_vMax }); /* calls autoNumeric and passes function get_vMax */

            $('#get_metricUnit').change(function () {
                var set_value = $('#length').autoNumericGet();
                if (this.value == ' km') {
                    set_value = set_value / 1000;
                } else {
                    set_value = set_value * 1000;
                }
                $('#length').autoNumericSet(set_value);
            });
            /* end script for callback demo*/

            $('<%= TextBoxReportTurnaroundDays.ClientID %>.auto').autoNumeric();
        });
    </script>
    <script>
        function generate(arr1, arr2, istrue) {
            if (istrue == true) {
                for (var i = 0; i < arr1.length; i++) {
                    document.getElementById(arr1[i]).style.color = "gray";
                }
                for (var i = 0; i < arr2.length; i++) {
                    $(arr2[i] + ' :input').attr('disabled', true);
                    $(arr2[i] + ' :input').removeAttr("checked");
                }
            }
            if (istrue == false) {
                for (var i = 0; i < arr1.length; i++) {
                    document.getElementById(arr1[i]).style.color = "#333";
                }
                for (var i = 0; i < arr2.length; i++) {
                    $(arr2[i] + ' :input').removeAttr('disabled');
                }
            }
        }

        $(document).ready(function () {
            // Question 5
            var Q5array = new Array("q6");
            var Q5arrayAns = new Array("#q6");

            function Question5Click() {
                if ($("#<%= RadioBiopsyResultPositiveYes.ClientID%>").prop("checked") == true) {
                    generate(Q5array, Q5arrayAns, false);
                }
                else {
                    generate(Q5array, Q5arrayAns, true);
                }
            }

            Question5Click();

            $("#<%= RadioBiopsyResultPositiveYes.ClientID%>").click(function() {
                Question5Click();
            });
            $("#<%= RadioBiopsyResultPositiveNo.ClientID%>").click(function() {
                Question5Click();
            });
        });
    </script>
</asp:content>