﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Transactions;
using NetHealthPIMModel;

public partial class abo_CataractChart : BasePage {

    public bool PQRSEnabled { get; set; }

    public bool PQRSUsed { get; set; }

    protected void Page_Load(object sender, EventArgs e) {
        if (!IsPostBack) {
            NetHealthPIMEntities pim1 = new NetHealthPIMEntities();
            var pqrsselected = pim1.ParticipantModule.First(p => p.ParticipantID == ctx.ParticipantID);
            PQRSPanel.Visible = Convert.ToBoolean(pqrsselected.PQRS);

            PQRSUsed = Convert.ToBoolean(pqrsselected.PQRS);
            PQRSEnabled = Convert.ToBoolean(System.Configuration.ConfigurationManager.AppSettings["PQRSEnabled"]);

            ((PIMMasterPage)Page.Master).SetTitle("Chart");
            
            using (NetHealthPIMEntities pim = new NetHealthPIMEntities()) {
                int ModuleID = (int)Session[Constants.SESSION_WORKINGMODULEID];
                Module module = pim.Module.First(c => c.ModuleID == ModuleID);
                ((PIMMasterPage)Page.Master).SetHeader(module.ModuleName + " Chart Abstraction"); 
                
                ParticipantModuleCycle prev = (ParticipantModuleCycle)Session[Constants.SESSION_PREVIOUSCYCLE];
                String CycleNumber = Request.QueryString["CycleNumber"];

                int cycleID = ctx.ActiveModuleCycleID;
                if (CycleNumber == "1")
                {
                    cycleID = prev.ParticipantModuleCycleID;
                    LinkButtonBackToDashboard.Visible = true;
                    ButtonSubmit.Visible = false;
                }

                ParticipantModuleCycle cycle = pim.ParticipantModuleCycle.First(c => c.ParticipantModuleCycleID == cycleID);
                String numberOfRecord = Request.QueryString["nr"];
                String totalRecords = Request.QueryString["t"];
                if (cycle.CycleNumber == 1) {
                    ((PIMMasterPage)Page.Master).SetStatus(2);
                    LiteralAbstractionNumber.Text = "Initial Abstraction: Chart " + numberOfRecord + " of " + totalRecords;
                }
                else {
                    ((PIMMasterPage)Page.Master).SetStatus(3);
                    LiteralAbstractionNumber.Text = "Second Abstraction: Chart " + numberOfRecord + " of " + totalRecords;
                }

                DropDownListMonthOfBirth.DataSource = CycleManager.getMonthList();
                DropDownListYearOfBirth.DataSource = CycleManager.getDOBYearList(0, 100);

                DropDownListMonthOfSurgery.DataSource = CycleManager.getMonthList();
                DropDownListYearOfSurgery.DataSource = CycleManager.getDOBYearList(0, 100);

                DropDownListBCVA.DataSource = CycleManager.getExamValues();
                DropDownListPostopBCVA.DataSource = CycleManager.getExamValues();
                DropDownListPQRSVisitMonth.DataSource = CycleManager.getMonthList();
                DropDownListPQRSVisitYear.DataSource = CycleManager.getDOBYearList(0, 100);

                DataBind();
                DropDownListYearOfBirth.Items.Insert(0, new ListItem("Year", "0"));
                DropDownListYearOfSurgery.Items.Insert(0, new ListItem("Year", "0"));
                DropDownListPQRSVisitYear.Items.Insert(0, new ListItem("Year", "0"));
                DropDownListYearOfSurgery.DataSource = CycleManager.getDOBYearList(0, 100);
                string recordIdentifier = Request.QueryString["ri"];
                HiddenFieldRecordIdentifier.Value = recordIdentifier;
                LiteralRecordIdentifier.Text = recordIdentifier;
                var count = pim.ChartAbstractionCS.Where(ps => ps.ParticipantModuleCycle.ParticipantModuleCycleID == cycleID & ps.RecordIdentifier == recordIdentifier).Count();
                if (count == 0) {
                    using (TransactionScope transaction = new TransactionScope()) {
                        //Module module = pim.Module.First(m => m.ModuleID == Constants.ABO_AMBLYOPIA_MODULEID);

                        var chartRegistration = (from cr in pim.ChartRegistration
                                                               where cr.ParticipantModuleCycleID == cycleID
                                                               & cr.RecordIdentifier == recordIdentifier
                                                               & cr.ModuleID == 3
                                                 select cr).First<NetHealthPIMModel.ChartRegistration>();

                        DateTime initialVisit = Convert.ToDateTime(chartRegistration.InitialVisit);
                        DateTime DOB = Convert.ToDateTime(chartRegistration.DOB);


                        ChartAbstractionCS abstractRecord = new ChartAbstractionCS();
                        abstractRecord.ParticipantModuleCycle = cycle;
                        abstractRecord.RecordIdentifier = recordIdentifier;
                        abstractRecord.MonthOfBirth = DOB.Month;
                        abstractRecord.YearOfBirth = DOB.Year;
                        DropDownListYearOfBirth.SelectedValue = abstractRecord.YearOfBirth.ToString();
                        DropDownListMonthOfBirth.SelectedValue = abstractRecord.MonthOfBirth.ToString();
                        abstractRecord.CreatedOn = DateTime.Now;
                        abstractRecord.LastUpdateDate = DateTime.Now;
                        pim.SaveChanges();

                        transaction.Complete();
                    }
                }
                else {
                    ChartAbstractionCS abstractRecord = (from c in pim.ChartAbstractionCS
                                                         where c.ParticipantModuleCycle.ParticipantModuleCycleID == cycleID & c.RecordIdentifier == recordIdentifier
                                                         select c).First<ChartAbstractionCS>();

                    Participant participant = pim.Participant.First(p => p.ParticipantID == ctx.ParticipantID);
                    pqrsselected = pim.ParticipantModule.First(p => p.ParticipantID == participant.ParticipantID);
                    bool pqrsused = Convert.ToBoolean(pqrsselected.PQRS);
                    bool pqrsenabled = Convert.ToBoolean(System.Configuration.ConfigurationManager.AppSettings["PQRSEnabled"]);
                    
                    if (pqrsused && pqrsenabled) {
                        PQRSPanel.Visible = true;

                        PQRSWebService.PPWebServiceClient client = new PQRSWebService.PPWebServiceClient();
                        client.ClientCredentials.UserName.UserName = "partner:2015";
                        client.ClientCredentials.UserName.Password = "48C1A443-54B0-4D00-A4A2-FE5E46C2F02E";



                        Int32 participantid = client.AddUser(75, "0", participant.ParticipantFirstName, participant.ParticipantLastName, participant.ParticipantEmailAddress, participant.ParticipantEmailAddress, "abo2015", "ABO"); 

                            String patient_id = Request.QueryString["ri"].ToString();
                            if  (!(cycle.CycleNumber == 1))
                            {
                                patient_id = "C2" + patient_id ;
                            }

                           // DataTable values = client.GetPatientVisit(participantid, patient_id, 2014);
                           DataTable values = client.GetPatientVisit(participantid, patient_id, 2015);

                            if (values != null && values.Rows.Count>0)
                            {
                                DataRow visit = values.Rows[0];
                                DropDownListPQRSVisitMonth.SelectedValue = visit["VisitDate"].ToString().Substring(0, 2).TrimStart('0');
                                DropDownListPQRSVisitYear.SelectedValue = visit["VisitDate"].ToString().Substring(6);
                                PQRSMedicare.SelectedValue = (Boolean)visit["IsMedicarePartB"] ? "1" : "0";

                                // try {
                                String question_id = "";

                                question_id = "G397"; if (visit.Table.Columns[question_id] != null) Age.SelectedValue = visit[question_id].ToString();
                                question_id = "G398"; if (visit.Table.Columns[question_id] != null) PQRSProcedure.SelectedValue = visit[question_id].ToString();

                                question_id = "Q4400"; if (visit.Table.Columns[question_id] != null) PQRSMedicationsList.SelectedValue = visit[question_id].ToString();    //130.1
                                question_id = "Q4401"; if (visit.Table.Columns[question_id] != null) PQRSIfNoMedList.SelectedValue = visit[question_id].ToString();    //130.2                                

                                question_id = "Q4189"; if (visit.Table.Columns[question_id] != null) PQRSComorbid.SelectedValue = visit[question_id].ToString();    //191.1
                                question_id = "Q4190"; if (visit.Table.Columns[question_id] != null) PQRSVisualAcuity.SelectedValue = visit[question_id].ToString();//191.2

                                question_id = "Q4191"; if (visit.Table.Columns[question_id] != null) PQRSComorbid2.SelectedValue = visit[question_id].ToString();   //192.1
                                question_id = "Q4192"; if (visit.Table.Columns[question_id] != null) PQRSSurgicalProcedure.SelectedValue = visit[question_id].ToString();//192.2

                                question_id = "Q4335"; if (visit.Table.Columns[question_id] != null) PQRS303SurveyDone.SelectedValue = visit[question_id].ToString();
                                question_id = "Q4336"; if (visit.Table.Columns[question_id] != null) PQRSImprovement.SelectedValue = visit[question_id].ToString();

                                question_id = "Q4337"; if (visit.Table.Columns[question_id] != null) PQRS304SurveyDone.SelectedValue = visit[question_id].ToString();
                                question_id = "Q4338"; if (visit.Table.Columns[question_id] != null) PQRSSatisfied.SelectedValue = visit[question_id].ToString();



                        question_id = "Q4402"; if (visit.Table.Columns[question_id] != null) PQRSTabaccoScreened.SelectedValue = visit[question_id].ToString();
                        question_id = "Q4403"; if (visit.Table.Columns[question_id] != null) PQRSIfNotTabaccoScreened.SelectedValue = visit[question_id].ToString();
                        question_id = "Q4404"; if (visit.Table.Columns[question_id] != null) PQRSTabaccoUser.SelectedValue = visit[question_id].ToString();
                        question_id = "Q4405"; if (visit.Table.Columns[question_id] != null) PQRSTabaccoCessation.SelectedValue = visit[question_id].ToString();
                        question_id = "Q4406"; if (visit.Table.Columns[question_id] != null) PQRSPreopRupture.SelectedValue = visit[question_id].ToString();
                        question_id = "Q4407"; if (visit.Table.Columns[question_id] != null) PQRSUnplannedRupture.SelectedValue = visit[question_id].ToString();
                        question_id = "Q4408"; if (visit.Table.Columns[question_id] != null) PQRSOcularConditions.SelectedValue = visit[question_id].ToString();
                        question_id = "Q4409"; if (visit.Table.Columns[question_id] != null) PQRSPlannedRefraction.SelectedValue = visit[question_id].ToString();
                                
                                
                                //}
                                // catch { }
                            }
                    }

                    if (abstractRecord.PreopIndVisuallySigCataract != null) CheckBoxPreopIndVisuallySigCataract.Checked = ((bool)abstractRecord.PreopIndVisuallySigCataract);
                    if (abstractRecord.PreopIndAnisometropia != null) CheckBoxPreopIndAnisometropia.Checked = ((bool)abstractRecord.PreopIndAnisometropia);
                    if (abstractRecord.PreopIndLensOpacity != null) CheckBoxPreopIndLensOpacity.Checked = ((bool)abstractRecord.PreopIndLensOpacity);
                    if (abstractRecord.PreopIndLensCausingInflam != null) CheckBoxPreopIndLensCausingInflam.Checked = ((bool)abstractRecord.PreopIndLensCausingInflam);
                    if (abstractRecord.PreopIndNarrowAngle != null) CheckBoxPreopIndNarrowAngle.Checked = ((bool)abstractRecord.PreopIndNarrowAngle);
                    if (abstractRecord.PreopIndNotDoc != null) CheckBoxPreopIndNotDoc.Checked = ((bool)abstractRecord.PreopIndNotDoc);
                    if (abstractRecord.PreopMeasureImmersionUltrasound != null) CheckBoxPreopMeasureImmersionUltrasound.Checked = ((bool)abstractRecord.PreopMeasureImmersionUltrasound);
                    if (abstractRecord.PreopMeasureOther != null) CheckBoxPreopMeasureOther.Checked = ((bool)abstractRecord.PreopMeasureOther);
                    if (abstractRecord.PreopKerCornealTop != null) CheckBoxPreopKerCornealTop.Checked = ((bool)abstractRecord.PreopKerCornealTop);
                    if (abstractRecord.PreopKerManualKer != null) CheckBoxPreopKerManualKer.Checked = ((bool)abstractRecord.PreopKerManualKer);
                    if (abstractRecord.PreopKerOther != null) CheckBoxPreopKerOther.Checked = ((bool)abstractRecord.PreopKerOther);
                    if (abstractRecord.PreopFormulaHaigis != null) CheckBoxPreopFormulaHaigis.Checked = ((bool)abstractRecord.PreopFormulaHaigis);
                    if (abstractRecord.PreopFormulaHofferQ != null) CheckBoxPreopFormulaHofferQ.Checked = ((bool)abstractRecord.PreopFormulaHofferQ);
                    if (abstractRecord.PreopFormulaHolladay != null) CheckBoxPreopFormulaHolladay.Checked = ((bool)abstractRecord.PreopFormulaHolladay);
                    if (abstractRecord.PreopFormulaHolladayII != null) CheckBoxPreopFormulaHolladayII.Checked = ((bool)abstractRecord.PreopFormulaHolladayII);
                    if (abstractRecord.PreopFormulaSRKT != null) CheckBoxPreopFormulaSRKT.Checked = ((bool)abstractRecord.PreopFormulaSRKT);
                    if (abstractRecord.PreopFormulaOther != null) CheckBoxPreopFormulaOther.Checked = ((bool)abstractRecord.PreopFormulaOther);
                    if (abstractRecord.ComplicationNone != null) CheckBoxComplicationNone.Checked = ((bool)abstractRecord.ComplicationNone);
                    if (abstractRecord.ComplicationIrisProlapse != null) CheckBoxComplicationIrisProlapse.Checked = ((bool)abstractRecord.ComplicationIrisProlapse);
                    if (abstractRecord.ComplicationLossLensFragments != null) CheckBoxComplicationLossLensFragments.Checked = ((bool)abstractRecord.ComplicationLossLensFragments);
                    if (abstractRecord.ComplicationPosteriorRupture != null) CheckBoxComplicationPosteriorRupture.Checked = ((bool)abstractRecord.ComplicationPosteriorRupture);
                    if (abstractRecord.ComplicationSupraHemorrhage != null) CheckBoxComplicationSupraHemorrhage.Checked = ((bool)abstractRecord.ComplicationSupraHemorrhage);
                    if (abstractRecord.ComplicationVitreousLoss != null) CheckBoxComplicationVitreousLoss.Checked = ((bool)abstractRecord.ComplicationVitreousLoss);
                    if (abstractRecord.ComplicationOther != null) CheckBoxComplicationOther.Checked = ((bool)abstractRecord.ComplicationOther);
                    if (abstractRecord.AdjProcNone != null) CheckBoxAdjProcNone.Checked = ((bool)abstractRecord.AdjProcNone);
                    if (abstractRecord.AdjProcAnteriorVitrectomy != null) CheckBoxAdjProcAnteriorVitrectomy.Checked = ((bool)abstractRecord.AdjProcAnteriorVitrectomy);
                    if (abstractRecord.AdjProcCapsularTension != null) CheckBoxAdjProcCapsularTension.Checked = ((bool)abstractRecord.AdjProcCapsularTension);
                    if (abstractRecord.AdjProcIrisRetractorExp != null) CheckBoxAdjProcIrisRetractorExp.Checked = ((bool)abstractRecord.AdjProcIrisRetractorExp);
                    if (abstractRecord.AdjProcOther != null) CheckBoxAdjProcOther.Checked = ((bool)abstractRecord.AdjProcOther);
                    if (abstractRecord.PostopComplicationSTNone != null) CheckBoxPostopComplicationSTNone.Checked = ((bool)abstractRecord.PostopComplicationSTNone);
                    if (abstractRecord.PostopComplicationSTElevatedPressure != null) CheckBoxPostopComplicationSTElevatedPressure.Checked = ((bool)abstractRecord.PostopComplicationSTElevatedPressure);
                    if (abstractRecord.PostopComplicationSTHyphema != null) CheckBoxPostopComplicationSTHyphema.Checked = ((bool)abstractRecord.PostopComplicationSTHyphema);
                    if (abstractRecord.PostopComplicationSTWoundLeak != null) CheckBoxPostopComplicationSTWoundLeak.Checked = ((bool)abstractRecord.PostopComplicationSTWoundLeak);
                    if (abstractRecord.PostopComplicationSTOther != null) CheckBoxPostopComplicationSTOther.Checked = ((bool)abstractRecord.PostopComplicationSTOther);
                    if (abstractRecord.PostopComplicationNone != null) CheckBoxPostopComplicationNone.Checked = ((bool)abstractRecord.PostopComplicationNone);
                    if (abstractRecord.PostopComplicationCystoidEdema != null) CheckBoxPostopComplicationCystoidEdema.Checked = ((bool)abstractRecord.PostopComplicationCystoidEdema);
                    if (abstractRecord.PostopComplicationDislocatedIOL != null) CheckBoxPostopComplicationDislocatedIOL.Checked = ((bool)abstractRecord.PostopComplicationDislocatedIOL);
                    if (abstractRecord.PostopComplicationEndophthalmitis != null) CheckBoxPostopComplicationEndophthalmitis.Checked = ((bool)abstractRecord.PostopComplicationEndophthalmitis);
                    if (abstractRecord.PostopComplicationPseudophakic != null) CheckBoxPostopComplicationPseudophakic.Checked = ((bool)abstractRecord.PostopComplicationPseudophakic);
                    if (abstractRecord.PostopComplicationLensFragAnterior != null) CheckBoxPostopComplicationLensFragAnterior.Checked = ((bool)abstractRecord.PostopComplicationLensFragAnterior);
                    if (abstractRecord.PostopComplicationLensFragVitreous != null) CheckBoxPostopComplicationLensFragVitreous.Checked = ((bool)abstractRecord.PostopComplicationLensFragVitreous);
                    if (abstractRecord.PostopComplicationRetinalDetach != null) CheckBoxPostopComplicationRetinalDetach.Checked = ((bool)abstractRecord.PostopComplicationRetinalDetach);
                    if (abstractRecord.PostopComplicationTASS != null) CheckBoxPostopComplicationTASS.Checked = ((bool)abstractRecord.PostopComplicationTASS);
                    if (abstractRecord.PostopComplicationWoundDehiscence != null) CheckBoxPostopComplicationWoundDehiscence.Checked = ((bool)abstractRecord.PostopComplicationWoundDehiscence);
                    if (abstractRecord.PostopComplicationWrongPowerIOL != null) CheckBoxPostopComplicationWrongPowerIOL.Checked = ((bool)abstractRecord.PostopComplicationWrongPowerIOL);
                    if (abstractRecord.PostopComplicationOther != null) CheckBoxPostopComplicationOther.Checked = ((bool)abstractRecord.PostopComplicationOther);
                    if (abstractRecord.PostopVAReasonCornealDisease != null) CheckBoxPostopVAReasonCornealDisease.Checked = ((bool)abstractRecord.PostopVAReasonCornealDisease);
                    if (abstractRecord.PostopVAReasonRetinalDisease != null) CheckBoxPostopVAReasonRetinalDisease.Checked = ((bool)abstractRecord.PostopVAReasonRetinalDisease);
                    if (abstractRecord.PostopVAReasonND != null) CheckBoxPostopVAReasonND.Checked = ((bool)abstractRecord.PostopVAReasonND);
                    if (abstractRecord.PostopVAReasonUndetermined != null) CheckBoxPostopVAReasonUndetermined.Checked = ((bool)abstractRecord.PostopVAReasonUndetermined);
                    if (abstractRecord.PostopVAReasonOther != null) CheckBoxPostopVAReasonOther.Checked = ((bool)abstractRecord.PostopVAReasonOther);
                    if (abstractRecord.PreopMeasureContactUltrasound != null) CheckBoxPreopMeasureContactUltrasound.Checked = ((bool)abstractRecord.PreopMeasureContactUltrasound);
                    if (abstractRecord.PreopMeasureOpticalBiometry != null) CheckBoxPreopMeasureOpticalBiometry.Checked = ((bool)abstractRecord.PreopMeasureOpticalBiometry);
                    if (abstractRecord.PreopKerIOLMaster != null) CheckBoxPreopKerIOLMaster.Checked = ((bool)abstractRecord.PreopKerIOLMaster);
                    if (abstractRecord.PreopKerHandheldKer != null) CheckBoxPreopKerHandheldKer.Checked = ((bool)abstractRecord.PreopKerHandheldKer);
                    if (abstractRecord.ComplicationZonular != null) CheckBoxComplicationZonular.Checked = ((bool)abstractRecord.ComplicationZonular);
                    if (abstractRecord.PostopVAReasonOpticNerve != null) CheckBoxPostopVAReasonOpticNerve.Checked = ((bool)abstractRecord.PostopVAReasonOpticNerve);

                    if (abstractRecord.AnesthesiaGeneralAnesthesia != null) CheckBoxAnesthesiaGeneralAnesthesia.Checked = ((bool)abstractRecord.AnesthesiaGeneralAnesthesia);
                    if (abstractRecord.AnesthesiaPeribularInjection != null) CheckBoxAnesthesiaPeribularInjection.Checked = ((bool)abstractRecord.AnesthesiaPeribularInjection);
                    if (abstractRecord.AnesthesiaRetrobulbarInjection != null) CheckBoxAnesthesiaRetrobulbarInjection.Checked = ((bool)abstractRecord.AnesthesiaRetrobulbarInjection);
                    if (abstractRecord.AnesthesiaTopical != null) CheckBoxAnesthesiaTopical.Checked = ((bool)abstractRecord.AnesthesiaTopical);
                    if (abstractRecord.AnesthesiaTopicalWithIntracameral != null) CheckBoxAnesthesiaTopicalWithIntracameral.Checked = ((bool)abstractRecord.AnesthesiaTopicalWithIntracameral);
                    if (abstractRecord.AnesthesiaSubTenon != null) CheckBoxAnesthesiaSubTenon.Checked = ((bool)abstractRecord.AnesthesiaSubTenon);
                    if (abstractRecord.AnesthesiaBoxOther != null) CheckBoxAnesthesiaBoxOther.Checked = ((bool)abstractRecord.AnesthesiaBoxOther);

                    if (abstractRecord.PreopIndOther != null) 
                         CheckBoxPreopIndOther.Checked = ((bool)abstractRecord.PreopIndOther);


                    if (abstractRecord.RBGender != null) 
                        RadioButtonListRBGender.SelectedValue = abstractRecord.RBGender.ToString();


                    if (abstractRecord.RBSymptomsDailyLiving != null) RadioButtonListRBSymptomsDailyLiving.SelectedValue = abstractRecord.RBSymptomsDailyLiving.ToString();

                    if (abstractRecord.RBHistoryAlpha1aAgonists != null) RadioButtonListRBHistoryAlpha1aAgonists.SelectedValue = abstractRecord.RBHistoryAlpha1aAgonists.ToString();
                    if (abstractRecord.RBHistoryAnticoag != null) RadioButtonListRBHistoryAnticoag.SelectedValue = abstractRecord.RBHistoryAnticoag.ToString();
                    if (abstractRecord.RBExamRefraction != null) RadioButtonListRBExamRefraction.SelectedValue = abstractRecord.RBExamRefraction.ToString();
                    if (abstractRecord.RBExamDilatedPupil != null) RadioButtonListRBExamDilatedPupil.SelectedValue = abstractRecord.RBExamDilatedPupil.ToString();
                    if (abstractRecord.RBExamCornealEndo != null) RadioButtonListRBExamCornealEndo.SelectedValue = abstractRecord.RBExamCornealEndo.ToString();
                    if (abstractRecord.RBExamMacula != null) RadioButtonListRBExamMacula.SelectedValue = abstractRecord.RBExamMacula.ToString();
                    if (abstractRecord.RBPreopRisksDiscussed != null) RadioButtonListRBPreopRisksDiscussed.SelectedValue = abstractRecord.RBPreopRisksDiscussed.ToString();
                    if (abstractRecord.RBPreopGoalsExplained != null) RadioButtonListRBPreopGoalsExplained.SelectedValue = abstractRecord.RBPreopGoalsExplained.ToString();
                    if (abstractRecord.RBPreopSignedConsent != null) RadioButtonListRBPreopSignedConsent.SelectedValue = abstractRecord.RBPreopSignedConsent.ToString();
                    if (abstractRecord.RBMgntSurgery != null) RadioButtonListRBMgntSurgery.SelectedValue = abstractRecord.RBMgntSurgery.ToString();

                    if (abstractRecord.RBMgntIOLPlacement != null) RadioButtonListRBMgntIOLPlacement.SelectedValue = abstractRecord.RBMgntIOLPlacement.ToString();
                    if (abstractRecord.RBPostopAntibioticDropsSameDay != null) RadioButtonListRBPostopAntibioticDropsSameDay.SelectedValue = abstractRecord.RBPostopAntibioticDropsSameDay.ToString();
                    if (abstractRecord.RBPostopInstructions != null) RadioButtonListRBPostopInstructions.SelectedValue = abstractRecord.RBPostopInstructions.ToString();

                    if (abstractRecord.MonthOfBirth != null) DropDownListMonthOfBirth.SelectedValue = abstractRecord.MonthOfBirth.ToString();
                    if (abstractRecord.YearOfBirth != null) DropDownListYearOfBirth.SelectedValue = abstractRecord.YearOfBirth.ToString();
                    if (abstractRecord.MonthOfSurgery != null) DropDownListMonthOfSurgery.SelectedValue = abstractRecord.MonthOfSurgery.ToString();
                    if (abstractRecord.YearOfSurgery != null) DropDownListYearOfSurgery.SelectedValue = abstractRecord.YearOfSurgery.ToString();
                    if (abstractRecord.BCVA != null) DropDownListBCVA.SelectedValue = abstractRecord.BCVA.ToString();
                    if (abstractRecord.PostopBCVA != null) DropDownListPostopBCVA.SelectedValue = abstractRecord.PostopBCVA.ToString();

                    if (abstractRecord.ExamCataractGrading != null) {
                        RadioButtonExamCataractGradingYes.Checked = ((bool)abstractRecord.ExamCataractGrading);
                        RadioButtonExamCataractGradingNo.Checked = !((bool)abstractRecord.ExamCataractGrading);
                    }
                    if (abstractRecord.ExamDilatedFundus != null) {
                        RadioButtonExamDilatedFundusYes.Checked = ((bool)abstractRecord.ExamDilatedFundus);
                        RadioButtonExamDilatedFundusNo.Checked = !((bool)abstractRecord.ExamDilatedFundus);
                    }
                    if (abstractRecord.Postop48Hours != null) {
                        RadioButtonPostop48HoursYes.Checked = ((bool)abstractRecord.Postop48Hours);
                        RadioButtonPostop48HoursNo.Checked = !((bool)abstractRecord.Postop48Hours);
                    }
                    if (abstractRecord.PostopAntibioticDrops != null) {
                        RadioButtonPostopAntibioticDropsYes.Checked = ((bool)abstractRecord.PostopAntibioticDrops);
                        RadioButtonPostopAntibioticDropsNo.Checked = !((bool)abstractRecord.PostopAntibioticDrops);
                    }
                    if (abstractRecord.PostopAntiImflamDrops != null) {
                        RadioButtonPostopAntiImflamDropsYes.Checked = ((bool)abstractRecord.PostopAntiImflamDrops);
                        RadioButtonPostopAntiImflamDropsNo.Checked = !((bool)abstractRecord.PostopAntiImflamDrops);
                    }
                    if (abstractRecord.PostopFurtherSurgery != null) {
                        RadioButtonPostopFurtherSurgeryYes.Checked = ((bool)abstractRecord.PostopFurtherSurgery);
                        RadioButtonPostopFurtherSurgeryNo.Checked = !((bool)abstractRecord.PostopFurtherSurgery);
                    }
                    if (abstractRecord.PostopSphericalEquivalentSign != null) {
                        RadioButtonPostopSphericalEquivalentSignYes.Checked = ((bool)abstractRecord.PostopSphericalEquivalentSign);
                        RadioButtonPostopSphericalEquivalentSignNo.Checked = !((bool)abstractRecord.PostopSphericalEquivalentSign);
                    }
                    if (abstractRecord.PostopVACoMorbid != null) {
                        RadioButtonPostopVACoMorbidYes.Checked = ((bool)abstractRecord.PostopVACoMorbid);
                        RadioButtonPostopVACoMorbidNo.Checked = !((bool)abstractRecord.PostopVACoMorbid);
                    }
                    if (abstractRecord.ExamGlareTest != null) {
                        RadioButtonExamGlareTestYes.Checked = ((bool)abstractRecord.ExamGlareTest);
                        RadioButtonExamGlareTestNo.Checked = !((bool)abstractRecord.ExamGlareTest);
                    }
                    if (abstractRecord.ExamPotentialAcuityTest != null) {
                        RadioButtonExamPotentialAcuityTestYes.Checked = ((bool)abstractRecord.ExamPotentialAcuityTest);
                        RadioButtonExamPotentialAcuityTestNo.Checked = !((bool)abstractRecord.ExamPotentialAcuityTest);
                    }
                    if (abstractRecord.ExamSlitLamp != null) {
                        RadioButtonExamSlitLampYes.Checked = ((bool)abstractRecord.ExamSlitLamp);
                        RadioButtonExamSlitLampNo.Checked = !((bool)abstractRecord.ExamSlitLamp);
                    }
                    if (abstractRecord.ExamPseudoexfoliative != null) {
                        RadioButtonExamPseudoexfoliativeYes.Checked = ((bool)abstractRecord.ExamPseudoexfoliative);
                        RadioButtonExamPseudoexfoliativeNo.Checked = !((bool)abstractRecord.ExamPseudoexfoliative);
                    }

                    if (abstractRecord.MgntTargetRefractionSign != null) {
                        RadioButtonMgntTargetRefractionSignYes.Checked = ((bool)abstractRecord.MgntTargetRefractionSign);
                        RadioButtonMgntTargetRefractionSignNo.Checked = !((bool)abstractRecord.MgntTargetRefractionSign);
                    }
                    if (abstractRecord.IntracameralAntibiotics != null) {
                        RadioButtonIntracameralAntibioticsYes.Checked = ((bool)abstractRecord.IntracameralAntibiotics);
                        RadioButtonIntracameralAntibioticsNo.Checked = !((bool)abstractRecord.IntracameralAntibiotics);
                    }

                    if (abstractRecord.PostopSphericalEquivalent_ != null) TextBoxPostopSphericalEquivalent.Text = abstractRecord.PostopSphericalEquivalent_.ToString();
                    if (abstractRecord.PreopMeasureOtherText != null) TextBoxPreopMeasureOtherText.Text = abstractRecord.PreopMeasureOtherText.ToString();
                    if (abstractRecord.PreopFormulaOtherText != null) TextBoxPreopFormulaOtherText.Text = abstractRecord.PreopFormulaOtherText.ToString();
                    if (abstractRecord.RBMgntAnesthesiaOtherText != null) TextBoxRBMgntAnesthesiaOtherText.Text = abstractRecord.RBMgntAnesthesiaOtherText.ToString();
                    if (abstractRecord.ComplicationOtherText != null) TextBoxComplicationOtherText.Text = abstractRecord.ComplicationOtherText.ToString();
                    if (abstractRecord.AdjProcOtherText != null) TextBoxAdjProcOtherText.Text = abstractRecord.AdjProcOtherText.ToString();
                    if (abstractRecord.PostopComplicationOtherText != null) TextBoxPostopComplicationOtherText.Text = abstractRecord.PostopComplicationOtherText.ToString();
                    if (abstractRecord.PostopFurtherSurgeryText != null) TextBoxPostopFurtherSurgeryText.Text = abstractRecord.PostopFurtherSurgeryText.ToString();
                    if (abstractRecord.PostopVAReasonOtherText != null) TextBoxPostopVAReasonOtherText.Text = abstractRecord.PostopVAReasonOtherText.ToString();
                    if (abstractRecord.PostopComplicationSTOtherText != null) TextBoxPostopComplicationSTOther.Text = abstractRecord.PostopComplicationSTOtherText.ToString();
                    if (abstractRecord.MgntTargetRefraction != null) TextBoxMgntTargetRefraction.Text = abstractRecord.MgntTargetRefraction.ToString();
                    if (abstractRecord.PreopKerOtherText != null) TextBoxPreopKerOtherText.Text = abstractRecord.PreopKerOtherText.ToString();
                }
            }
        }
    }

    public void savedata() {
        using (NetHealthPIMEntities pim = new NetHealthPIMEntities()) {
            int ModuleID = (int)Session[Constants.SESSION_WORKINGMODULEID];
            int cycleID = ctx.ActiveModuleCycleID;
            ParticipantModuleCycle cycle = pim.ParticipantModuleCycle.First(c => c.ParticipantModuleCycleID == cycleID);
            string recordIdentifier = HiddenFieldRecordIdentifier.Value;
            using (TransactionScope transaction = new TransactionScope()) {
                ChartAbstractionCS abstractRecord = (from c in pim.ChartAbstractionCS
                                                     where c.ParticipantModuleCycle.ParticipantModuleCycleID == cycleID & c.RecordIdentifier == recordIdentifier
                                                     select c).First<ChartAbstractionCS>();

                abstractRecord.PreopIndVisuallySigCataract = CheckBoxPreopIndVisuallySigCataract.Checked;
                abstractRecord.PreopIndAnisometropia = CheckBoxPreopIndAnisometropia.Checked;
                abstractRecord.PreopIndLensOpacity = CheckBoxPreopIndLensOpacity.Checked;
                abstractRecord.PreopIndLensCausingInflam = CheckBoxPreopIndLensCausingInflam.Checked;
                abstractRecord.PreopIndNarrowAngle = CheckBoxPreopIndNarrowAngle.Checked;
                abstractRecord.PreopIndNotDoc = CheckBoxPreopIndNotDoc.Checked;
                abstractRecord.PreopMeasureImmersionUltrasound = CheckBoxPreopMeasureImmersionUltrasound.Checked;
                abstractRecord.PreopMeasureOther = CheckBoxPreopMeasureOther.Checked;
                abstractRecord.PreopKerCornealTop = CheckBoxPreopKerCornealTop.Checked;
                abstractRecord.PreopKerManualKer = CheckBoxPreopKerManualKer.Checked;
                abstractRecord.PreopKerOther = CheckBoxPreopKerOther.Checked;
                abstractRecord.PreopFormulaHaigis = CheckBoxPreopFormulaHaigis.Checked;
                abstractRecord.PreopFormulaHofferQ = CheckBoxPreopFormulaHofferQ.Checked;
                abstractRecord.PreopFormulaHolladay = CheckBoxPreopFormulaHolladay.Checked;
                abstractRecord.PreopFormulaHolladayII = CheckBoxPreopFormulaHolladayII.Checked;
                abstractRecord.PreopFormulaSRKT = CheckBoxPreopFormulaSRKT.Checked;
                abstractRecord.PreopFormulaOther = CheckBoxPreopFormulaOther.Checked;
                abstractRecord.ComplicationNone = CheckBoxComplicationNone.Checked;
                abstractRecord.ComplicationIrisProlapse = CheckBoxComplicationIrisProlapse.Checked;
                abstractRecord.ComplicationLossLensFragments = CheckBoxComplicationLossLensFragments.Checked;
                abstractRecord.ComplicationPosteriorRupture = CheckBoxComplicationPosteriorRupture.Checked;
                abstractRecord.ComplicationSupraHemorrhage = CheckBoxComplicationSupraHemorrhage.Checked;
                abstractRecord.ComplicationVitreousLoss = CheckBoxComplicationVitreousLoss.Checked;
                abstractRecord.ComplicationOther = CheckBoxComplicationOther.Checked;
                abstractRecord.AdjProcNone = CheckBoxAdjProcNone.Checked;
                abstractRecord.AdjProcAnteriorVitrectomy = CheckBoxAdjProcAnteriorVitrectomy.Checked;
                abstractRecord.AdjProcCapsularTension = CheckBoxAdjProcCapsularTension.Checked;
                abstractRecord.AdjProcIrisRetractorExp = CheckBoxAdjProcIrisRetractorExp.Checked;
                abstractRecord.AdjProcOther = CheckBoxAdjProcOther.Checked;
                abstractRecord.PostopComplicationSTNone = CheckBoxPostopComplicationSTNone.Checked;
                abstractRecord.PostopComplicationSTElevatedPressure = CheckBoxPostopComplicationSTElevatedPressure.Checked;
                abstractRecord.PostopComplicationSTHyphema = CheckBoxPostopComplicationSTHyphema.Checked;
                abstractRecord.PostopComplicationSTWoundLeak = CheckBoxPostopComplicationSTWoundLeak.Checked;
                abstractRecord.PostopComplicationSTOther = CheckBoxPostopComplicationSTOther.Checked;
                abstractRecord.PostopComplicationNone = CheckBoxPostopComplicationNone.Checked;
                abstractRecord.PostopComplicationCystoidEdema = CheckBoxPostopComplicationCystoidEdema.Checked;
                abstractRecord.PostopComplicationDislocatedIOL = CheckBoxPostopComplicationDislocatedIOL.Checked;
                abstractRecord.PostopComplicationEndophthalmitis = CheckBoxPostopComplicationEndophthalmitis.Checked;
                abstractRecord.PostopComplicationPseudophakic = CheckBoxPostopComplicationPseudophakic.Checked;
                abstractRecord.PostopComplicationLensFragAnterior = CheckBoxPostopComplicationLensFragAnterior.Checked;
                abstractRecord.PostopComplicationLensFragVitreous = CheckBoxPostopComplicationLensFragVitreous.Checked;
                abstractRecord.PostopComplicationRetinalDetach = CheckBoxPostopComplicationRetinalDetach.Checked;
                abstractRecord.PostopComplicationTASS = CheckBoxPostopComplicationTASS.Checked;
                abstractRecord.PostopComplicationWoundDehiscence = CheckBoxPostopComplicationWoundDehiscence.Checked;
                abstractRecord.PostopComplicationWrongPowerIOL = CheckBoxPostopComplicationWrongPowerIOL.Checked;
                abstractRecord.PostopComplicationOther = CheckBoxPostopComplicationOther.Checked;
                abstractRecord.PostopVAReasonCornealDisease = CheckBoxPostopVAReasonCornealDisease.Checked;
                abstractRecord.PostopVAReasonRetinalDisease = CheckBoxPostopVAReasonRetinalDisease.Checked;
                abstractRecord.PostopVAReasonND = CheckBoxPostopVAReasonND.Checked;
                abstractRecord.PostopVAReasonUndetermined = CheckBoxPostopVAReasonUndetermined.Checked;
                abstractRecord.PostopVAReasonOther = CheckBoxPostopVAReasonOther.Checked;
                abstractRecord.PreopMeasureContactUltrasound = CheckBoxPreopMeasureContactUltrasound.Checked;
                abstractRecord.PreopMeasureOpticalBiometry = CheckBoxPreopMeasureOpticalBiometry.Checked;
                abstractRecord.PreopKerIOLMaster = CheckBoxPreopKerIOLMaster.Checked;
                abstractRecord.PreopKerHandheldKer = CheckBoxPreopKerHandheldKer.Checked;
                abstractRecord.ComplicationZonular = CheckBoxComplicationZonular.Checked;
                abstractRecord.PostopVAReasonOpticNerve = CheckBoxPostopVAReasonOpticNerve.Checked;

                abstractRecord.AnesthesiaGeneralAnesthesia = CheckBoxAnesthesiaGeneralAnesthesia.Checked;
                abstractRecord.AnesthesiaPeribularInjection = CheckBoxAnesthesiaPeribularInjection.Checked;
                abstractRecord.AnesthesiaRetrobulbarInjection = CheckBoxAnesthesiaRetrobulbarInjection.Checked;
                abstractRecord.AnesthesiaTopical = CheckBoxAnesthesiaTopical.Checked;
                abstractRecord.AnesthesiaTopicalWithIntracameral = CheckBoxAnesthesiaTopicalWithIntracameral.Checked;
                abstractRecord.AnesthesiaSubTenon = CheckBoxAnesthesiaSubTenon.Checked;
                abstractRecord.AnesthesiaBoxOther = CheckBoxAnesthesiaBoxOther.Checked;

                abstractRecord.PreopIndOther = CheckBoxPreopIndOther.Checked;

                if (RadioButtonListRBGender.SelectedIndex != -1) abstractRecord.RBGender = Convert.ToInt32(RadioButtonListRBGender.SelectedValue);
                else abstractRecord.RBGender = 0;
                if (RadioButtonListRBSymptomsDailyLiving.SelectedIndex != -1) abstractRecord.RBSymptomsDailyLiving = Convert.ToInt32(RadioButtonListRBSymptomsDailyLiving.SelectedValue);
                else abstractRecord.RBSymptomsDailyLiving = 0;

                if (RadioButtonListRBHistoryAlpha1aAgonists.SelectedIndex != -1) abstractRecord.RBHistoryAlpha1aAgonists = Convert.ToInt32(RadioButtonListRBHistoryAlpha1aAgonists.SelectedValue);
                else abstractRecord.RBHistoryAlpha1aAgonists = 0;
                if (RadioButtonListRBHistoryAnticoag.SelectedIndex != -1) abstractRecord.RBHistoryAnticoag = Convert.ToInt32(RadioButtonListRBHistoryAnticoag.SelectedValue);
                else abstractRecord.RBHistoryAnticoag = 0;
                if (RadioButtonListRBExamRefraction.SelectedIndex != -1) abstractRecord.RBExamRefraction = Convert.ToInt32(RadioButtonListRBExamRefraction.SelectedValue);
                else abstractRecord.RBExamRefraction = 0;
                if (RadioButtonListRBExamDilatedPupil.SelectedIndex != -1) abstractRecord.RBExamDilatedPupil = Convert.ToInt32(RadioButtonListRBExamDilatedPupil.SelectedValue);
                else abstractRecord.RBExamDilatedPupil = 0;
                if (RadioButtonListRBExamCornealEndo.SelectedIndex != -1) abstractRecord.RBExamCornealEndo = Convert.ToInt32(RadioButtonListRBExamCornealEndo.SelectedValue);
                else abstractRecord.RBExamCornealEndo = 0;
                if (RadioButtonListRBExamMacula.SelectedIndex != -1) abstractRecord.RBExamMacula = Convert.ToInt32(RadioButtonListRBExamMacula.SelectedValue);
                else abstractRecord.RBExamMacula = 0;
                if (RadioButtonListRBPreopRisksDiscussed.SelectedIndex != -1) abstractRecord.RBPreopRisksDiscussed = Convert.ToInt32(RadioButtonListRBPreopRisksDiscussed.SelectedValue);
                else abstractRecord.RBPreopRisksDiscussed = 0;
                if (RadioButtonListRBPreopGoalsExplained.SelectedIndex != -1) abstractRecord.RBPreopGoalsExplained = Convert.ToInt32(RadioButtonListRBPreopGoalsExplained.SelectedValue);
                else abstractRecord.RBPreopGoalsExplained = 0;
                if (RadioButtonListRBPreopSignedConsent.SelectedIndex != -1) abstractRecord.RBPreopSignedConsent = Convert.ToInt32(RadioButtonListRBPreopSignedConsent.SelectedValue);
                else abstractRecord.RBPreopSignedConsent = 0;
                if (RadioButtonListRBMgntSurgery.SelectedIndex != -1) abstractRecord.RBMgntSurgery = Convert.ToInt32(RadioButtonListRBMgntSurgery.SelectedValue);
                else abstractRecord.RBMgntSurgery = 0;

                if (RadioButtonListRBMgntIOLPlacement.SelectedIndex != -1) abstractRecord.RBMgntIOLPlacement = Convert.ToInt32(RadioButtonListRBMgntIOLPlacement.SelectedValue);
                else abstractRecord.RBMgntIOLPlacement = 0;
                if (RadioButtonListRBPostopAntibioticDropsSameDay.SelectedIndex != -1) abstractRecord.RBPostopAntibioticDropsSameDay = Convert.ToInt32(RadioButtonListRBPostopAntibioticDropsSameDay.SelectedValue);
                else abstractRecord.RBPostopAntibioticDropsSameDay = 0;
                if (RadioButtonListRBPostopInstructions.SelectedIndex != -1) abstractRecord.RBPostopInstructions = Convert.ToInt32(RadioButtonListRBPostopInstructions.SelectedValue);
                else abstractRecord.RBPostopInstructions = 0;

                abstractRecord.MonthOfBirth = DropDownListMonthOfBirth.SelectedIndex > 0 ? Convert.ToInt32(DropDownListMonthOfBirth.SelectedValue) : 0;
                abstractRecord.YearOfBirth = DropDownListYearOfBirth.SelectedIndex > 0 ? Convert.ToInt32(DropDownListYearOfBirth.SelectedValue) : 0;
                abstractRecord.MonthOfSurgery = DropDownListMonthOfSurgery.SelectedIndex > 0 ? Convert.ToInt32(DropDownListMonthOfSurgery.SelectedValue) : 0;
                abstractRecord.YearOfSurgery = DropDownListYearOfSurgery.SelectedIndex > 0 ? Convert.ToInt32(DropDownListYearOfSurgery.SelectedValue) : 0;
                abstractRecord.BCVA = DropDownListBCVA.SelectedIndex > 0 ? Convert.ToInt32(DropDownListBCVA.SelectedValue) : 0;
                abstractRecord.PostopBCVA = DropDownListPostopBCVA.SelectedIndex > 0 ? Convert.ToInt32(DropDownListPostopBCVA.SelectedValue) : 0;

                if (RadioButtonExamCataractGradingYes.Checked) abstractRecord.ExamCataractGrading = true;
                if (RadioButtonExamCataractGradingNo.Checked) abstractRecord.ExamCataractGrading = false;
                if (RadioButtonExamDilatedFundusYes.Checked) abstractRecord.ExamDilatedFundus = true;
                if (RadioButtonExamDilatedFundusNo.Checked) abstractRecord.ExamDilatedFundus = false;
                if (RadioButtonPostop48HoursYes.Checked) abstractRecord.Postop48Hours = true;
                if (RadioButtonPostop48HoursNo.Checked) abstractRecord.Postop48Hours = false;
                if (RadioButtonPostopAntibioticDropsYes.Checked) abstractRecord.PostopAntibioticDrops = true;
                if (RadioButtonPostopAntibioticDropsNo.Checked) abstractRecord.PostopAntibioticDrops = false;
                if (RadioButtonPostopAntiImflamDropsYes.Checked) abstractRecord.PostopAntiImflamDrops = true;
                if (RadioButtonPostopAntiImflamDropsNo.Checked) abstractRecord.PostopAntiImflamDrops = false;
                if (RadioButtonPostopFurtherSurgeryYes.Checked) abstractRecord.PostopFurtherSurgery = true;
                if (RadioButtonPostopFurtherSurgeryNo.Checked) abstractRecord.PostopFurtherSurgery = false;
                if (RadioButtonPostopSphericalEquivalentSignYes.Checked) abstractRecord.PostopSphericalEquivalentSign = true;
                if (RadioButtonPostopSphericalEquivalentSignNo.Checked) abstractRecord.PostopSphericalEquivalentSign = false;
                if (RadioButtonPostopVACoMorbidYes.Checked) abstractRecord.PostopVACoMorbid = true;
                if (RadioButtonPostopVACoMorbidNo.Checked) abstractRecord.PostopVACoMorbid = false;
                if (RadioButtonExamGlareTestYes.Checked) abstractRecord.ExamGlareTest = true;
                if (RadioButtonExamGlareTestNo.Checked) abstractRecord.ExamGlareTest = false;
                if (RadioButtonExamPotentialAcuityTestYes.Checked) abstractRecord.ExamPotentialAcuityTest = true;
                if (RadioButtonExamPotentialAcuityTestNo.Checked) abstractRecord.ExamPotentialAcuityTest = false;
                if (RadioButtonExamSlitLampYes.Checked) abstractRecord.ExamSlitLamp = true;
                if (RadioButtonExamSlitLampNo.Checked) abstractRecord.ExamSlitLamp = false;
                if (RadioButtonExamPseudoexfoliativeYes.Checked) abstractRecord.ExamPseudoexfoliative = true;
                if (RadioButtonExamPseudoexfoliativeNo.Checked) abstractRecord.ExamPseudoexfoliative = false;

                if (RadioButtonMgntTargetRefractionSignYes.Checked) abstractRecord.MgntTargetRefractionSign = true;
                if (RadioButtonMgntTargetRefractionSignNo.Checked) abstractRecord.MgntTargetRefractionSign = false;
                if (RadioButtonIntracameralAntibioticsYes.Checked) abstractRecord.IntracameralAntibiotics = true;
                if (RadioButtonIntracameralAntibioticsNo.Checked) abstractRecord.IntracameralAntibiotics = false;

                try {
                    if (TextBoxPreopKerOtherText.Text != null)
                        abstractRecord.PreopKerOtherText = TextBoxPreopKerOtherText.Text;
                }
                catch (Exception ex) {
                    abstractRecord.PreopKerOtherText = null;
                    TextBoxPreopKerOtherText.Text = "";
                }

                try {
                    if (TextBoxPostopComplicationSTOther.Text != null)
                        abstractRecord.PostopComplicationSTOtherText = TextBoxPostopComplicationSTOther.Text;
                }
                catch (Exception ex) {
                    abstractRecord.PostopComplicationSTOtherText = null;
                    TextBoxPostopComplicationSTOther.Text = "";
                }

                try {
                    if (TextBoxPostopSphericalEquivalent.Text != null)
                        abstractRecord.PostopSphericalEquivalent_ = Convert.ToDouble(TextBoxPostopSphericalEquivalent.Text);
                }
                catch (Exception ex) {
                    abstractRecord.PostopSphericalEquivalent_ = null;
                    TextBoxPostopSphericalEquivalent.Text = "";
                }

                try {
                    if (TextBoxPreopMeasureOtherText.Text != null)
                        abstractRecord.PreopMeasureOtherText = TextBoxPreopMeasureOtherText.Text;
                }
                catch (Exception ex) {
                    abstractRecord.PreopMeasureOtherText = null;
                    TextBoxPreopMeasureOtherText.Text = "";
                }

                try {
                    if (TextBoxMgntTargetRefraction.Text != null)
                        abstractRecord.MgntTargetRefraction = Convert.ToDouble(TextBoxMgntTargetRefraction.Text);
                }
                catch (Exception ex) {
                    abstractRecord.MgntTargetRefraction = null;
                    TextBoxMgntTargetRefraction.Text = "";
                }

                try {
                    if (TextBoxPreopFormulaOtherText.Text != null)
                        abstractRecord.PreopFormulaOtherText = TextBoxPreopFormulaOtherText.Text;
                }
                catch (Exception ex) {
                    abstractRecord.PreopFormulaOtherText = null;
                    TextBoxPreopFormulaOtherText.Text = "";
                }
                try {
                    if (TextBoxRBMgntAnesthesiaOtherText.Text != null)
                        abstractRecord.RBMgntAnesthesiaOtherText = TextBoxRBMgntAnesthesiaOtherText.Text;
                }
                catch (Exception ex) {
                    abstractRecord.RBMgntAnesthesiaOtherText = null;
                    TextBoxRBMgntAnesthesiaOtherText.Text = "";
                }

                try {
                    if (TextBoxComplicationOtherText.Text != null)
                        abstractRecord.ComplicationOtherText = TextBoxComplicationOtherText.Text;
                }
                catch (Exception ex) {
                    abstractRecord.ComplicationOtherText = null;
                    TextBoxComplicationOtherText.Text = "";
                }
                try {
                    if (TextBoxAdjProcOtherText.Text != null)
                        abstractRecord.AdjProcOtherText = TextBoxAdjProcOtherText.Text;
                }
                catch (Exception ex) {
                    abstractRecord.AdjProcOtherText = null;
                    TextBoxAdjProcOtherText.Text = "";
                }
                try {
                    if (TextBoxPostopComplicationOtherText.Text != null)
                        abstractRecord.PostopComplicationOtherText = TextBoxPostopComplicationOtherText.Text;
                }
                catch (Exception ex) {
                    abstractRecord.PostopComplicationOtherText = null;
                    TextBoxPostopComplicationOtherText.Text = "";
                }
                try {
                    if (TextBoxPostopFurtherSurgeryText.Text != null)
                        abstractRecord.PostopFurtherSurgeryText = TextBoxPostopFurtherSurgeryText.Text;
                }
                catch (Exception ex) {
                    abstractRecord.PostopFurtherSurgeryText = null;
                    TextBoxPostopFurtherSurgeryText.Text = "";
                }

                bool ChartCompleted = isComplete();

                Participant participant = pim.Participant.First(p => p.ParticipantID == ctx.ParticipantID);
                var pqrsselected = pim.ParticipantModule.First(p => p.ParticipantID == participant.ParticipantID);
                Boolean pqrsused = Convert.ToBoolean(pqrsselected.PQRS);
                bool pqrsenabled = Convert.ToBoolean(System.Configuration.ConfigurationManager.AppSettings["PQRSEnabled"]);
                Int32 visityear = Convert.ToInt32("0" + DropDownListPQRSVisitYear.SelectedValue);
                
                if (pqrsused && visityear == 2015 && pqrsenabled) {


                    PQRSWebService.PPWebServiceClient client = new PQRSWebService.PPWebServiceClient();
                    client.ClientCredentials.UserName.UserName = "partner:2015";
                    client.ClientCredentials.UserName.Password = "48C1A443-54B0-4D00-A4A2-FE5E46C2F02E";



                    Int32 participantid = client.AddUser(75, "0", participant.ParticipantFirstName, participant.ParticipantLastName, participant.ParticipantEmailAddress, participant.ParticipantEmailAddress, "abo2015", "ABO"); 

                        String visit_date = DropDownListPQRSVisitMonth.SelectedValue.PadLeft(2, '0') + "/01/";
                        visit_date += DropDownListPQRSVisitYear.SelectedValue;
                        Boolean is_medicare = PQRSMedicare.SelectedValue.Equals("1");

                        DataTable values = new DataTable("PQRSPROVisit");
                        values.Columns.Add("MeasureID", typeof(Int32));
                        values.Columns.Add("QuestionType", typeof(String));
                        values.Columns.Add("QuestionID", typeof(Int32));
                        values.Columns.Add("QuestionIDSuffix", typeof(String));
                        values.Columns.Add("Value", typeof(String));
                        values.Columns.Add("CPT", typeof(String));
                        values.Columns.Add("Modifier", typeof(String));
                        values.Columns.Add("SubmissionYear", typeof(Int32));



                        AddVisitDetail(ref values, 0, 397, Age.SelectedValue);
                        AddVisitDetail(ref values, 0, 398, PQRSProcedure.SelectedValue);

                        AddVisitDetail(ref values, 130, 4400, PQRSMedicationsList.SelectedValue);
                        AddVisitDetail(ref values, 130, 4401, PQRSIfNoMedList.SelectedValue);
                        
                        AddVisitDetail(ref values, 191, 4189, PQRSComorbid.SelectedValue);
                        AddVisitDetail(ref values, 191, 4190, PQRSVisualAcuity.SelectedValue);

                        AddVisitDetail(ref values, 192, 4191, PQRSComorbid2.SelectedValue);
                        AddVisitDetail(ref values, 192, 4192, PQRSSurgicalProcedure.SelectedValue);

                        AddVisitDetail(ref values, 303, 4335, PQRS303SurveyDone.SelectedValue);
                        AddVisitDetail(ref values, 303, 4336, PQRSImprovement.SelectedValue);

                        AddVisitDetail(ref values, 304, 4337, PQRS304SurveyDone.SelectedValue);
                        AddVisitDetail(ref values, 304, 4338, PQRSSatisfied.SelectedValue);

                        AddVisitDetail(ref values, 226, 4402, PQRSTabaccoScreened.SelectedValue);
                        AddVisitDetail(ref values, 226, 4403, PQRSIfNotTabaccoScreened.SelectedValue);
                        AddVisitDetail(ref values, 226, 4404, PQRSTabaccoUser.SelectedValue);
                        AddVisitDetail(ref values, 226, 4405, PQRSTabaccoCessation.SelectedValue);
                        AddVisitDetail(ref values, 304, 4406, PQRSPreopRupture.SelectedValue);
                        AddVisitDetail(ref values, 388, 4407, PQRSUnplannedRupture.SelectedValue);
                        AddVisitDetail(ref values, 389, 4408, PQRSOcularConditions.SelectedValue);
                        AddVisitDetail(ref values, 389, 4409, PQRSPlannedRefraction.SelectedValue);


                        String patient_id = Request.QueryString["ri"].ToString();
                        if  (!(cycle.CycleNumber == 1))
                        {
                            patient_id = "C2" + patient_id ;
                        }
                       // client.AddPatientVisitDT(3, (Int32)participantid, 75, patient_id, visit_date, is_medicare, "", "", values);
                        client.AddChart((Int32)participantid, 75, patient_id, visit_date, is_medicare, values);
                        client.Close();
                   
                }

                //try
                //{
                //if (TextBoxPostopSphericalEquivalent .Text != null)
                //abstractRecord.PostopSphericalEquivalent = TextBoxPostopSphericalEquivalent .Text;
                //}
                //catch (Exception ex)
                //{
                //abstractRecord.PostopSphericalEquivalent = null;
                //TextBoxPostopSphericalEquivalent .Text = '';
                //}
                try {
                    if (TextBoxPostopVAReasonOtherText.Text != null)
                        abstractRecord.PostopVAReasonOtherText = TextBoxPostopVAReasonOtherText.Text;
                }
                catch (Exception ex) {
                    abstractRecord.PostopVAReasonOtherText = null;
                    TextBoxPostopVAReasonOtherText.Text = "";
                }

                abstractRecord.LastUpdateDate = DateTime.Now;
                abstractRecord.Complete = ChartCompleted;
                var chartRegistration = (from cr in pim.ChartRegistration
                                         where cr.ParticipantModuleCycleID == cycle.ParticipantModuleCycleID
                                          & cr.ModuleID == ModuleID
                                          & cr.RecordIdentifier == recordIdentifier
                                         select cr).First<NetHealthPIMModel.ChartRegistration>();
                chartRegistration.AbstractCompleted = ChartCompleted;


                if ((DropDownListYearOfBirth.SelectedIndex > 0) && (DropDownListYearOfBirth.SelectedIndex > 0)) {
                    chartRegistration.DOB = abstractRecord.MonthOfBirth + "/" + abstractRecord.YearOfBirth;
                }

                pim.SaveChanges();

                transaction.Complete();
            }

            CycleManager.UpdateParticipantCompletedModules(cycleID, ModuleID);
        }
    }

    void AddVisitDetail(ref DataTable values, int measure_id, int question_id, string value) 
    { 
        AddVisitDetail(ref values, measure_id, question_id, value, "", "", ""); 
    }
    void AddVisitDetail(ref DataTable values, int measure_id, int question_id, string value, string cpt, string modifier, string suffix) {
        if (!String.IsNullOrEmpty(value)) {
            DataRow detail = values.NewRow();
            detail["MeasureID"] = measure_id;
            detail["QuestionType"] = (measure_id > 0 ? "M" : "G");
            detail["QuestionID"] = question_id;
            detail["QuestionIDSuffix"] = suffix;
            detail["Value"] = value;
            detail["CPT"] = cpt;
            detail["Modifier"] = modifier;
            detail["SubmissionYear"] = Convert.ToInt32(System.Configuration.ConfigurationManager.AppSettings["PQRSYear"]);
            values.Rows.Add(detail);
        }
    }

    protected void ButtonSubmit_Click(object sender, EventArgs e) {
        savedata();
        isComplete();
        if (!isComplete()) {
            isComplete();
            string script = "if (confirm('All fields are not complete.  Are you sure you want to submit?')) { window.location = '../PatientChartRegistration.aspx'; }";
            ScriptManager.RegisterStartupScript(this, GetType(), "ServerControlScript", script, true);
        }
        else
            Response.Redirect("../PatientChartRegistration.aspx", true);
    }


    protected bool isComplete() {

        bool ChartCompleted = true;

        LabelRBGender.Visible = false;

        LabelRBSymptomsDailyLiving.Visible = false;

        LabelRBHistoryAlpha1aAgonists.Visible = false;

        LabelRBHistoryAnticoag.Visible = false;

        LabelRBExamRefraction.Visible = false;

        LabelRBExamDilatedPupil.Visible = false;

        LabelRBExamCornealEndo.Visible = false;

        LabelRBExamMacula.Visible = false;

        LabelRBPreopRisksDiscussed.Visible = false;

        LabelRBPreopGoalsExplained.Visible = false;

        LabelRBPreopSignedConsent.Visible = false;

        LabelRBMgntSurgery.Visible = false;

        LabelRBMgntAnesthesia.Visible = false;

        LabelRBMgntIOLPlacement.Visible = false;

        LabelRBPostopInstructions.Visible = false;

        LabelMonthOfBirth.Visible = false;

        LabelYearOfBirth.Visible = false;

        LabelMonthOfSurgery.Visible = false;

        LabelYearOfSurgery.Visible = false;

        LabelBCVA.Visible = false;

        LabelPostopBCVA.Visible = false;

        LabelExamCataractGrading.Visible = false;

        LabelExamDilatedFundus.Visible = false;

        LabelPostop48Hours.Visible = false;

        LabelPostopAntibioticDrops.Visible = false;

        LabelPostopAntiImflamDrops.Visible = false;

        LabelPostopFurtherSurgery.Visible = false;

        LabelPostopSphericalEquivalentSign.Visible = false;

        LabelPostopVACoMorbid.Visible = false;

        LabelExamGlareTest.Visible = false;

        LabelExamPotentialAcuityTest.Visible = false;

        LabelExamSlitLamp.Visible = false;

        LabelExamPseudoexfoliative.Visible = false;

        LabelPQRSAge.Visible = false;

        LabelPQRSDOProcedureMonth.Visible = false;

        LabelPQRSDOProcedureYear.Visible = false;

        LabelPQRSMedicare.Visible = false;

        LabelPQRSProcedure.Visible = false;



        LabelPQRSTabaccoScreened.Visible = false;
        LabelPQRSIfNotTabaccoScreened.Visible = false;

        LabelPQRSTabaccoUser.Visible = false;
        LabelPQRSTabaccoCessation.Visible = false;
        LabelPQRSPreopRupture.Visible = false;
        LabelPQRSUnplannedRupture.Visible = false;
        LabelPQRSOcularConditions.Visible = false;





        LabelPQRSConmorbid.Visible = false;

        LabelPQRSSurgicalProcedure.Visible = false;

        LabelPQRSConmorbid2.Visible = false;

        LabelPQRSSurveyDone.Visible = false;

        LabelPQRSVisualImprovement.Visible = false;

        LabelPQRSAssessmentComplete.Visible = false;
        LabelPQRSVisualAcuity.Visible = false;
        LabelPQRSVisualAcuity.Visible = false;
        LabelPQRSPatientSatisfied.Visible = false;

        LabelMgntTargetRefractionSign.Visible = false;
        Labelinitialage.Visible = false;
        LabelIntracameralAntibiotics.Visible = false;
        NetHealthPIMEntities pim = new NetHealthPIMEntities();
        var moduleinfo = (from c in pim.Module where c.ModuleID == 3 select c).FirstOrDefault();
        string message = "Patient must be  at least " + moduleinfo.RegistrationMinAge + " at Initial Visit<br />";
        string registrationminage = Validation.validateRegistrationMinAge(Convert.ToInt32(DropDownListYearOfSurgery.SelectedValue), Convert.ToInt32(DropDownListYearOfBirth.SelectedValue), Convert.ToInt32(moduleinfo.RegistrationMinAge), message);

        if (registrationminage != "") {
            Labelinitialage.Text += message;
            Labelinitialage.Visible = true;
            ChartCompleted = false;
        }

        ///Pqrs Validation
        Participant participant = pim.Participant.First(p => p.ParticipantID == ctx.ParticipantID);
        var pqrsselected = pim.ParticipantModule.First(p => p.ParticipantID == participant.ParticipantID);
        bool pqrsused = Convert.ToBoolean(pqrsselected.PQRS);
        bool pqrsenabled = Convert.ToBoolean(System.Configuration.ConfigurationManager.AppSettings["PQRSEnabled"]);
        if (pqrsused && pqrsenabled && ViewState["PQRSBroken"]==null)
        {
              if (String.IsNullOrEmpty(Age.SelectedValue)) {
                    LabelPQRSAge.Visible = true;
                    ChartCompleted = false;
              }
              if (Age.SelectedValue.Equals("1"))
              {
                  if (DropDownListPQRSVisitMonth.SelectedIndex == 0)
                  {
                     LabelPQRSDOProcedureMonth.Visible = true;
                     ChartCompleted = false;
                  }
              }

            if(DropDownListPQRSVisitYear.SelectedItem.Value=="2015")
            {
                if (String.IsNullOrEmpty(PQRSMedicare.SelectedValue))
                {
                   LabelPQRSMedicare.Visible = true;
                   ChartCompleted = false;
                }
                if (String.IsNullOrEmpty(PQRSProcedure.SelectedValue))
                {
                    LabelPQRSProcedure.Visible = true;
                    ChartCompleted = false;
                }
            }

            if (PQRSProcedure.SelectedValue == "1")
            {
                if (String.IsNullOrEmpty(PQRSMedicationsList.SelectedValue))
                {
                    LabelPQRSMedicationsList.Visible = true;
                    ChartCompleted = false;
                }
                if (String.IsNullOrEmpty(PQRSComorbid.SelectedValue)) 
                {
                    LabelPQRSConmorbid.Visible = true;
                    ChartCompleted = false;
                }
                if (String.IsNullOrEmpty(PQRSComorbid2.SelectedValue))
                {
                    LabelPQRSConmorbid2.Visible = true;
                    ChartCompleted = false;
                }
                if (String.IsNullOrEmpty(PQRSTabaccoScreened.SelectedValue))
                {
                     LabelPQRSTabaccoScreened.Visible = true;
                     ChartCompleted = false;
                }
                if (String.IsNullOrEmpty(PQRS303SurveyDone.SelectedValue)) 
                {
                    LabelPQRSVisualImprovement.Visible = true;
                    ChartCompleted = false;
                }
                if (String.IsNullOrEmpty(PQRS304SurveyDone.SelectedValue))
                {
                    LabelPQRSAssessmentComplete.Visible = true;
                    ChartCompleted = false;
                }
                if (String.IsNullOrEmpty(PQRSPreopRupture.SelectedValue))
                {
                     LabelPQRSPreopRupture.Visible = true;
                     ChartCompleted = false;
                }
                if (String.IsNullOrEmpty(PQRSOcularConditions.SelectedValue))
                {
                    LabelPQRSOcularConditions.Visible = true;
                    ChartCompleted = false;
                 }
            }
            if (!String.IsNullOrEmpty(PQRSMedicationsList.SelectedValue) && PQRSMedicationsList.SelectedValue.Equals("0"))
            {
                if (String.IsNullOrEmpty(PQRSIfNoMedList.SelectedValue))
                {
                   LabelPQRSIfNoMedList.Visible = true;
                   ChartCompleted = false;
                }
            }
            if (!String.IsNullOrEmpty(PQRSComorbid.SelectedValue) && PQRSComorbid.SelectedValue.Equals("0"))
            {
                if (String.IsNullOrEmpty(PQRSVisualAcuity.SelectedValue)) 
                {
                   LabelPQRSVisualAcuity.Visible = true;
                   ChartCompleted = false;
                }
            }
            if (!String.IsNullOrEmpty(PQRSComorbid2.SelectedValue) && PQRSComorbid2.SelectedValue.Equals("0"))
            {
                if (String.IsNullOrEmpty(PQRSSurgicalProcedure.SelectedValue))                 
                {
                     LabelPQRSSurgicalProcedure.Visible = true;
                     ChartCompleted = false;
                  }
            }
            if (!String.IsNullOrEmpty(PQRSTabaccoScreened.SelectedValue) && PQRSTabaccoScreened.SelectedValue.Equals("0"))
            {
                if (String.IsNullOrEmpty(PQRSIfNotTabaccoScreened.SelectedValue))
                {
                    LabelPQRSIfNotTabaccoScreened.Visible = true;
                    ChartCompleted = false;
                }
            }
            if (!String.IsNullOrEmpty(PQRSTabaccoScreened.SelectedValue) && PQRSTabaccoScreened.SelectedValue.Equals("1"))
            {
                if (String.IsNullOrEmpty(PQRSTabaccoUser.SelectedValue))
                {
                    LabelPQRSTabaccoUser.Visible = true;
                    ChartCompleted = false;
                }
            }
            if (!String.IsNullOrEmpty(PQRSTabaccoUser.SelectedValue) && PQRSTabaccoUser.SelectedValue.Equals("1"))
            {
                if (String.IsNullOrEmpty(PQRSTabaccoCessation.SelectedValue))
                {
                    LabelPQRSTabaccoCessation.Visible = true;
                    ChartCompleted = false;
                }
            }
            if (!String.IsNullOrEmpty(PQRS303SurveyDone.SelectedValue) && PQRS303SurveyDone.SelectedValue.Equals("1"))
            {
                if (String.IsNullOrEmpty(PQRSImprovement.SelectedValue))
                {
                    LabelPQRSVisualImprovement.Visible = true;
                    ChartCompleted = false;
                }
            }
            if (!String.IsNullOrEmpty(PQRS304SurveyDone.SelectedValue) && PQRS304SurveyDone.SelectedValue.Equals("1"))
            {
                if (String.IsNullOrEmpty(PQRSSatisfied.SelectedValue))
                {
                    LabelPQRSPatientSatisfied.Visible = true;
                    ChartCompleted = false;
                }
            }
            if (!String.IsNullOrEmpty(PQRSPreopRupture.SelectedValue) && PQRSPreopRupture.SelectedValue.Equals("0"))
            {
               if(String.IsNullOrEmpty(PQRSUnplannedRupture.SelectedValue))
               {
                   LabelPQRSUnplannedRupture.Visible = true;
                   ChartCompleted = false;
               }
            }
            if (!String.IsNullOrEmpty(PQRSOcularConditions.SelectedValue) && PQRSOcularConditions.SelectedValue.Equals("0"))
            {
                if (String.IsNullOrEmpty(PQRSPlannedRefraction.SelectedValue))
                {
                    LabelPQRSPlannedRefraction.Visible = true;
                    ChartCompleted = false;
                }
            }

          // 

        //    if (DropDownListPQRSVisitYear.SelectedIndex == 0) {
        //        LabelPQRSDOProcedureYear.Visible = true;
        //        ChartCompleted = false;
        //    }

        //    if (PQRSMedicare.SelectedIndex < 0) {
        //        LabelPQRSMedicare.Visible = true;
        //        ChartCompleted = false;
        //    }

        //    if (String.IsNullOrEmpty(PQRSProcedure.SelectedValue)) {
        //        LabelPQRSProcedure.Visible = true;
        //        ChartCompleted = false;
        //    }

            

        //   

        //    

        //    if (String.IsNullOrEmpty(PQRSComorbid2.SelectedValue)) {
        //        LabelPQRSConmorbid2.Visible = true;
        //        ChartCompleted = false;
        //    }

        //    if (String.IsNullOrEmpty(PQRS303SurveyDone.SelectedValue)) {
        //        LabelPQRSSurveyDone.Visible = true;
        //        ChartCompleted = false;
        //    }





        //    if (String.IsNullOrEmpty(PQRSTabaccoScreened.SelectedValue))
        //    {
        //        LabelPQRSTabaccoScreened.Visible = true;
        //        ChartCompleted = false;
        //    }

        //    else if (PQRSTabaccoScreened.SelectedValue.Equals("0") && String.IsNullOrEmpty(PQRSIfNotTabaccoScreened.SelectedValue))
        //    {
        //        LabelPQRSIfNotTabaccoScreened.Visible = true;
        //        ChartCompleted = false;
        //    }

        //    if (String.IsNullOrEmpty(PQRSTabaccoUser.SelectedValue))
        //    {
        //        //LabelPQRSTabaccoUser.Visible = true;
        //        //ChartCompleted = false;
        //    }
        //    else if (PQRSTabaccoUser.SelectedValue.Equals("1") && String.IsNullOrEmpty(PQRSTabaccoCessation.SelectedValue))
        //    {
        //        LabelPQRSTabaccoCessation.Visible = true;
        //        ChartCompleted = false;
        //    }
        //    if (String.IsNullOrEmpty(PQRSPreopRupture.SelectedValue))
        //    {
        //        LabelPQRSPreopRupture.Visible = true;
        //        ChartCompleted = false;
        //    }
        //    else if (PQRSPreopRupture.SelectedValue.Equals("0") && String.IsNullOrEmpty(PQRSUnplannedRupture.SelectedValue))
        //    {
        //        LabelPQRSUnplannedRupture.Visible = true;
        //        ChartCompleted = false;
        //    }
        //    

        //    else if (PQRS303SurveyDone.SelectedValue.Equals("1") && String.IsNullOrEmpty(PQRSImprovement.SelectedValue)) {
        //        LabelPQRSVisualImprovement.Visible = true;
        //        ChartCompleted = false;
        //    }

        //    if (String.IsNullOrEmpty(PQRS304SurveyDone.SelectedValue)) {
        //        LabelPQRSAssessmentComplete.Visible = true;
        //        ChartCompleted = false;
        //    }
        //    else if (PQRS304SurveyDone.SelectedValue.Equals("1") && String.IsNullOrEmpty(PQRSSatisfied.SelectedValue)) {
        //        LabelPQRSPatientSatisfied.Visible = true;
        //        ChartCompleted = false;
        //    }
        }

        if (RadioButtonListRBGender.SelectedIndex == -1) {
            LabelRBGender.Visible = true;
            ChartCompleted = false;
        }
        if (RadioButtonListRBSymptomsDailyLiving.SelectedIndex == -1) {
            LabelRBSymptomsDailyLiving.Visible = true;
            ChartCompleted = false;
        }

        if (RadioButtonListRBHistoryAlpha1aAgonists.SelectedIndex == -1) {
            LabelRBHistoryAlpha1aAgonists.Visible = true;
            ChartCompleted = false;
        }
        if (RadioButtonListRBHistoryAnticoag.SelectedIndex == -1) {
            LabelRBHistoryAnticoag.Visible = true;
            ChartCompleted = false;
        }
        if (RadioButtonListRBExamRefraction.SelectedIndex == -1) {
            LabelRBExamRefraction.Visible = true;
            ChartCompleted = false;
        }
        if (RadioButtonListRBExamDilatedPupil.SelectedIndex == -1) {
            LabelRBExamDilatedPupil.Visible = true;
            ChartCompleted = false;
        }
        if (RadioButtonListRBExamCornealEndo.SelectedIndex == -1) {
            LabelRBExamCornealEndo.Visible = true;
            ChartCompleted = false;
        }
        if (RadioButtonListRBExamMacula.SelectedIndex == -1) {
            LabelRBExamMacula.Visible = true;
            ChartCompleted = false;
        }
        if (RadioButtonListRBPreopRisksDiscussed.SelectedIndex == -1) {
            LabelRBPreopRisksDiscussed.Visible = true;
            ChartCompleted = false;
        }
        if (RadioButtonListRBPreopGoalsExplained.SelectedIndex == -1) {
            LabelRBPreopGoalsExplained.Visible = true;
            ChartCompleted = false;
        }
        if (RadioButtonListRBPreopSignedConsent.SelectedIndex == -1) {
            LabelRBPreopSignedConsent.Visible = true;
            ChartCompleted = false;
        }
        if (RadioButtonListRBMgntSurgery.SelectedIndex == -1) {
            LabelRBMgntSurgery.Visible = true;
            ChartCompleted = false;
        }

        if (RadioButtonListRBMgntIOLPlacement.SelectedIndex == -1) {
            LabelRBMgntIOLPlacement.Visible = true;
            ChartCompleted = false;
        }
        if (RadioButtonListRBPostopInstructions.SelectedIndex == -1) {
            LabelRBPostopInstructions.Visible = true;
            ChartCompleted = false;
        }

        if (DropDownListMonthOfBirth.SelectedIndex == 0) {
            LabelMonthOfBirth.Visible = true;
            ChartCompleted = false;
        }
        if (DropDownListYearOfBirth.SelectedIndex == 0) {
            LabelYearOfBirth.Visible = true;
            ChartCompleted = false;
        }
        if (DropDownListMonthOfSurgery.SelectedIndex == 0) {
            LabelMonthOfSurgery.Visible = true;
            ChartCompleted = false;
        }
        if (DropDownListYearOfSurgery.SelectedIndex == 0) {
            LabelYearOfSurgery.Visible = true;
            ChartCompleted = false;
        }
        if (DropDownListBCVA.SelectedIndex == 0) {
            LabelBCVA.Visible = true;
            ChartCompleted = false;
        }
        if (DropDownListPostopBCVA.SelectedIndex == 0) {
            LabelPostopBCVA.Visible = true;
            ChartCompleted = false;
        }

        if (RadioButtonExamCataractGradingNo.Checked == false && RadioButtonExamCataractGradingYes.Checked == false) {
            LabelExamCataractGrading.Visible = true;
            ChartCompleted = false;
        }
        if (RadioButtonExamDilatedFundusNo.Checked == false && RadioButtonExamDilatedFundusYes.Checked == false) {
            LabelExamDilatedFundus.Visible = true;
            ChartCompleted = false;
        }
        if (RadioButtonPostop48HoursNo.Checked == false && RadioButtonPostop48HoursYes.Checked == false) {
            LabelPostop48Hours.Visible = true;
            ChartCompleted = false;
        }
        if (RadioButtonPostopAntibioticDropsNo.Checked == false && RadioButtonPostopAntibioticDropsYes.Checked == false) {
            LabelPostopAntibioticDrops.Visible = true;
            ChartCompleted = false;
        }
        if (RadioButtonPostopAntiImflamDropsNo.Checked == false && RadioButtonPostopAntiImflamDropsYes.Checked == false) {
            LabelPostopAntiImflamDrops.Visible = true;
            ChartCompleted = false;
        }
        if (RadioButtonPostopFurtherSurgeryNo.Checked == false && RadioButtonPostopFurtherSurgeryYes.Checked == false) {
            LabelPostopFurtherSurgery.Visible = true;
            ChartCompleted = false;
        }
        if (RadioButtonPostopSphericalEquivalentSignNo.Checked == false && RadioButtonPostopSphericalEquivalentSignYes.Checked == false) {
            LabelPostopSphericalEquivalentSign.Visible = true;
            ChartCompleted = false;
        }
        if (RadioButtonPostopVACoMorbidNo.Checked == false && RadioButtonPostopVACoMorbidYes.Checked == false) {
            LabelPostopVACoMorbid.Visible = true;
            ChartCompleted = false;
        }
        if (RadioButtonExamGlareTestNo.Checked == false && RadioButtonExamGlareTestYes.Checked == false) {
            LabelExamGlareTest.Visible = true;
            ChartCompleted = false;
        }
        if (RadioButtonExamPotentialAcuityTestNo.Checked == false && RadioButtonExamPotentialAcuityTestYes.Checked == false) {
            LabelExamPotentialAcuityTest.Visible = true;
            ChartCompleted = false;
        }
        if (RadioButtonExamSlitLampNo.Checked == false && RadioButtonExamSlitLampYes.Checked == false) {
            LabelExamSlitLamp.Visible = true;
            ChartCompleted = false;
        }
        if (RadioButtonExamPseudoexfoliativeNo.Checked == false && RadioButtonExamPseudoexfoliativeYes.Checked == false) {
            LabelExamPseudoexfoliative.Visible = true;
            ChartCompleted = false;
        }

        if (RadioButtonMgntTargetRefractionSignNo.Checked == false && RadioButtonMgntTargetRefractionSignYes.Checked == false) {
            LabelMgntTargetRefractionSign.Visible = true;
            ChartCompleted = false;
        }
        if (RadioButtonIntracameralAntibioticsNo.Checked == false && RadioButtonIntracameralAntibioticsYes.Checked == false) {
            LabelIntracameralAntibiotics.Visible = true;
            ChartCompleted = false;
        }

        return ChartCompleted;
    }
}