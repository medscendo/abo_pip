﻿<%@ Page Title="" Language="C#" MasterPageFile="~/PIMMasterPage.master" AutoEventWireup="true" CodeFile="AmblyopiaChart.aspx.cs" Inherits="abo_AmblyopiaChart" %>

<asp:Content ID="Content1" ContentPlaceHolderID="head" Runat="Server">
    <script type="text/javascript" language="javascript" src="http://ajax.googleapis.com/ajax/libs/jquery/1.7.1/jquery.min.js"></script>
    <link type="text/css" href="../../common/css/atooltip.css" rel="stylesheet"  media="screen" />
    
	<%--<script type="text/javascript" src="../../common/js/jquery.min.js"></script>--%>
	<script type="text/javascript" src="../../common/js/jquery.atooltip.js"></script>
    
    <script type="text/javascript" src="../../common/js/jquery.metadata.js"></script> <!--when changing defaults-->
    <script type="text/javascript" src="../../common/js/autoNumeric-1.7.5.js"></script>
<script type="text/javascript" language="javascript">
    $(document).ready(function() {
        /** instruct the metadata plugin where to look the metadata
        * jQuery.metadata.setType( type, name );
        * please read the metadata instructions for additional information
        * http://plugins.jquery.com/project/metadata
        */
        $.metadata.setType('attr', 'meta');

        /** To call autoNumeric
        * $(selector).autoNumeric({options}); 
        * The below example uses the input & class selector
        */
        $('input.auto').autoNumeric();
        /* scripts for metadata code generator  */

        /* rountine that prevents  numeric characters from being entered the the altDec field  */
        $('#altDecb').keypress(function(e) {
            var cc = String.fromCharCode(e.which);
            if (e.which != 32 && cc >= 0 && cc <= 9) {
                e.preventDefault();
            }
        });

        /* rountine that prevents  apostrophe, comma, more than one period (full stop) or numeric characters from being entered the the aSign field  */
        $('#aSignb').keypress(function(e) {
            var cc = String.fromCharCode(e.which);
            if ((e.which != 32 && cc >= 0 && cc <= 9) || cc == "," || cc == "'" || cc == "." && this.value.lastIndexOf('.') != -1) {
                e.preventDefault();
            }
        });

        $("input.md").bind('click keyup blur', function() {
            var metaCode = '', aSep = '', dGroup = '', aDec = '', altDec = '', aSign = '', pSign = '', vMin = '', vMax = '', mDec = '', mRound = '', aPad = '', wEmpty = '', aForm = '';
            if ($("input:radio[name=aSep]:checked").attr('id') == 'aSepc') {
                $('input:radio[name=aDec]:nth(0)').removeAttr("disabled");
                $('input:radio[name=aDec]:nth(0)').attr('checked', true);
                $('input:radio[name=aDec]:nth(1)').attr("disabled", true);
            }
            if ($("input:radio[name=aSep]:checked").attr('id') == 'aSepp') {
                $('input:radio[name=aDec]:nth(1)').removeAttr("disabled");
                $('input:radio[name=aDec]:nth(1)').attr('checked', true);
                $('input:radio[name=aDec]:nth(0)').attr("disabled", true);
            }
            if ($("input:radio[name=aSep]:checked").attr('id') != 'aSepc' || $("input:radio[name=aSep]:checked").attr('id') != 'aSepp') {
                $('input:radio[name=aDec]:nth(0)').removeAttr("disabled");
                $('input:radio[name=aDec]:nth(1)').removeAttr("disabled");
            }
            aSep = $("input:radio[name=aSep]:checked").val();
            dGroup = $("input:radio[name=dGroup]:checked").val();
            aDec = $("input:radio[name=aDec]:checked").val();

            if ($("input:radio[name=altDec]:checked").attr('id') == 'altDecd') {
                $('#altDecb').val('');
                $('#altDecb').attr("disabled", true);
            }
            if ($("input:radio[name=altDec]:checked").attr('id') == 'altDeca') {
                $('#altDecb').removeAttr("disabled");
                altDec = $('#altDecb').val();
            }

            if ($("input:radio[name=aSign]:checked").attr('id') == 'aSignd') {
                $('#aSignb').val('');
                $('#aSignb').attr("disabled", true);
            }
            if ($("input:radio[name=aSign]:checked").attr('id') == 'aSigna') {
                $('#aSignb').removeAttr("disabled");
                aSign = $('#aSignb').val();
            }

            pSign = $("input:radio[name=pSign]:checked").val();
            if ($("input:radio[name=vMin]:checked").attr('id') == 'vMind') {
                $('#vMinb').val('');
                $('#vMinb').attr("disabled", true);
            }
            if ($("input:radio[name=vMin]:checked").attr('id') == 'vMina') {
                $('#vMinb').removeAttr("disabled");
                vMin = $('#vMinb').val();
            }
            if ($("input:radio[name=vMax]:checked").attr('id') == 'vMaxd') {
                $('#vMaxb').val('');
                $('#vMaxb').attr("disabled", true);
            }
            if ($("input:radio[name=vMax]:checked").attr('id') == 'vMaxa') {
                $('#vMaxb').removeAttr("disabled");
                vMax = $('#vMaxb').val();
            }
            if ($("input:radio[name=mDec]:checked").attr('id') == 'mDecd') {
                $('#mDecbb').val('');
                $('#mDecbb').attr("disabled", true);
            }
            if ($("input:radio[name=mDec]:checked").attr('id') == 'mDeca') {
                $('#mDecbb').removeAttr("disabled");
                mDec = $('#mDecbb').val();
            }
            mRound = $("input:radio[name=mRound]:checked").val();
            aPad = $("input:radio[name=aPad]:checked").val();
            wEmpty = $("input:radio[name=wEmpty]:checked").val();
            if (aSep != '') {
                metaCode = aSep;
            }
            if (dGroup != '') {
                if (metaCode != '') {
                    metaCode = metaCode + ", " + dGroup;
                }
                else {
                    metaCode = dGroup;
                }
            }
            if (aDec != '') {
                if (metaCode != '') {
                    metaCode = metaCode + ", " + aDec;
                }
                else {
                    metaCode = aDec;
                }
            }
            if (altDec != '') {
                if (metaCode != '') {
                    metaCode = metaCode + ", altDec: '" + altDec + "'";
                }
                else {
                    metaCode = "altDec: '" + altDec + "'";
                }
            }
            if (aSign != '') {
                if (metaCode != '') {
                    metaCode = metaCode + ", aSign: '" + aSign + "'";
                }
                else {
                    metaCode = "aSign: '" + aSign + "'";
                }
            }
            if (pSign != '') {
                if (metaCode != '') {
                    metaCode = metaCode + ", " + pSign;
                }
                else {
                    metaCode = pSign;
                }
            }
            if (vMin != '') {
                if (metaCode != '') {
                    metaCode = metaCode + ", vMin: '" + vMin + "'";
                }
                else {
                    metaCode = "vMin: '" + vMin + "'";
                }
            }
            if (vMax != '') {
                if (metaCode != '') {
                    metaCode = metaCode + ", vMax: '" + vMax + "'";
                }
                else {
                    metaCode = "vMax: '" + vMax + "'";
                }
            }
            if (mDec != '') {
                if (metaCode != '') {
                    metaCode = metaCode + ", mDec: '" + $('#mDecbb').val() + "'";
                }
                else {
                    metaCode = "mDec: '" + $('#mDecbb').val() + "'";
                }
            }

            if (mRound != '') {
                if (metaCode != '') {
                    metaCode = metaCode + ", " + mRound;
                }
                else {
                    metaCode = mRound;
                }
            }
            if (aPad != '') {
                if (metaCode != '') {
                    metaCode = metaCode + ", " + aPad;
                }
                else {
                    metaCode = aPad;
                }
            }
            if (wEmpty != '') {
                if (metaCode != '') {
                    metaCode = metaCode + ", " + wEmpty;
                }
                else {
                    metaCode = wEmpty;
                }
            }
            $('#metaCode').text('');
            if (metaCode != '') {
                $('#metaCode').text('meta="{' + metaCode + '}"');
            }
        });

        /* clears the metadata code  */
        $('#rd').click(function() {
            $('#metaCode').text('');
        });
        /* ends scripts for metadata code generator  */

        /* script  for defaults demo  */
        $('#d_noMeta').blur(function() {
            var convertInput = '';
            convertInput = $(this).autoNumericGet();
            $('#d_Get').val(convertInput);
            $('#d_Set').autoNumericSet(convertInput);
        });
        /* end script  for defaults demo  */

        /* script  for various samples demo  */
        $('input[name$="sample"]').blur(function() {
            var convertInput = '';
            var row = 'row_' + this.id.charAt(4);
            convertInput = $(this).autoNumericGet();
            $('#' + row + 'b').val(convertInput);
            $('#' + row + 'c').autoNumericSet(convertInput);
        });
        /* end script  for various samples demo  */

        /* script  for rounding methods  */
        $('#roundValue').blur(function() {
            if (this.value != '') {
                convertInput = $('#roundValue').autoNumericGet();
                var i = 1;
                for (i = 1; i <= 9; i++) {
                    $('#roundMethod' + i).autoNumericSet(convertInput);
                }
            }
        });

        $('#roundDecimal').change(function() { /* changes decimal places */
            convertInput = $('#roundValue').autoNumericGet();
            if (convertInput > 0) {
                var i = 1;
                for (i = 1; i <= 9; i++) {
                    $('#roundMethod' + i).autoNumericSet(convertInput);
                }
            }
        });
        /* end script  for rounding methods  */

        /* script for dynamically loaded values  demo*/
        $.getJSON("test_JSON.php", function(data) {
            var valueFormatted = '';
            $.each(data, function(key, value) { // loops through JSON keys and returns value	
                $('#' + key).autoNumericSet(value);
            });
        });
        /* end script for dynamically loaded values demo*/

        /* script for callback demo*/
        $.autoNumeric.get_mDec = function() { /* get_mDec function attached to autoNumeric() */
            var set_mDec = $('#get_metricUnit').val();
            if (set_mDec == ' km') {
                set_mDec = 3;
            } else {
                set_mDec = 0;
            }
            return set_mDec; /* set mDec decimal places */
        }

        var get_vMax = function() { /* set the maximum value allowed based on the metric unit */
            var set_vMax = $('#get_metricUnit').val();
            if (set_vMax == ' km') {
                set_vMax = '99999.999';
            } else {
                set_vMax = '99999999';
            }
            return set_vMax;
        }

        $('#length').autoNumeric({ vMax: get_vMax }); /* calls autoNumeric and passes function get_vMax */

        $('#get_metricUnit').change(function() {
            var set_value = $('#length').autoNumericGet();
            if (this.value == ' km') {
                set_value = set_value / 1000;
            } else {
                set_value = set_value * 1000;
            }
            $('#length').autoNumericSet(set_value);
        });
        /* end script for callback demo*/


        $('<%= TextBoxCycloplegicRefractionODsph.ClientID %>.auto').autoNumeric();
        $('<%= TextBoxCycloplegicRefractionODcyl.ClientID %>.auto').autoNumeric();
        $('<%= TextBoxCycloplegicRefractionODaxis.ClientID %>.auto').autoNumeric();

        $('<%= TextBoxCycloplegicRefractionOSsph.ClientID %>.auto').autoNumeric();
        $('<%= TextBoxCycloplegicRefractionOScyl.ClientID %>.auto').autoNumeric();
        $('<%= TextBoxCycloplegicRefractionOSaxis.ClientID %>.auto').autoNumeric();

        $('<%= TextBoxPrimaryPositionDistanceLeftPD.ClientID %>.auto').autoNumeric();
        $('<%= TextBoxPrimaryPositionDistanceRightPD.ClientID %>.auto').autoNumeric();
        $('<%= TextBoxPrimaryPositionNearLeftPD.ClientID %>.auto').autoNumeric();
        $('<%= TextBoxPrimaryPositionNearRightPD.ClientID %>.auto').autoNumeric();
    });
    </script>
<script type="text/javascript">
    $(function() {
        $('#QD1').aToolTip({
            clickIt: true,
            tipContent: '20/800 or count fingers @ 5 ft<br />20/1000 or count fingers @ 4 ft<br />20/1600 or count fingers @ 3ft<br />20/2000 or count fingers @ 2 ft<br />20/4000 or count fingers @ 1 ft<br />20/7777 =CF<br />20/8888 =HM<br />20/9999 =LP<br />20/0000 =NLP'
        });
        $('#Q1aNonsnell').aToolTip({
            clickIt: true,
            tipContent: 'If method was non-Snellen, select method used.'
        });
        $('#QD2a').aToolTip({
            clickIt: true,
            tipContent: 'In arc seconds, up to 1 decimal space'
        });
        $('#QD3aa').aToolTip({
            clickIt: true,
            tipContent: 'Answers should be integers'
        });
        $('#QD3ab').aToolTip({
            clickIt: true,
            tipContent: 'Answers should be integers'
        });
        $('#QD4aa').aToolTip({
            clickIt: true,
            tipContent: 'Ranges for sph [+/- 0.00 thru 30.00],<br />cyl [0.00 thru 20.00], axis [integer 0 thru 180]'
        });
        $('#QD4ab').aToolTip({
            clickIt: true,
            tipContent: 'Ranges for sph [+/- 0.00 thru 30.00],<br />cyl [0.00 thru 20.00], axis [integer 0 thru 180]'
        });
        $('#QO1').aToolTip({
            clickIt: true,
            tipContent: '20/800 or count fingers @ 5 ft<br />20/1000 or count fingers @ 4 ft<br />20/1600 or count fingers @ 3ft<br />20/2000 or count fingers @ 2 ft<br />20/4000 or count fingers @ 1 ft<br />20/7777 =CF<br />20/8888 =HM<br />20/9999 =LP<br />20/0000 =NLP'
        });
        $('#QO2').aToolTip({
            clickIt: true,
            tipContent: 'In arc seconds, up to 1 decimal space'
        });
    }); 
		</script>
 
<style type="text/css">


.bginputa{
	float:right;
	margin:21px 0 0 1px;
	background: url(../common/images/orange_button_with_arrow.png) no-repeat;
	overflow:hidden;
	height:35px;

}
.bginputa a{
	color: white;
	text-align:center;
	height:35px;
	line-height:28px;
	padding:0 14px 15px  ;
	cursor:pointer;
	float:left;
	border:none;
	text-decoration:none;
	font-family:Arial, Helvetica, sans-serif; 
	font-size:12px; 
	font-weight:bold; 
	letter-spacing: 0px;
}
.bginputa a:hover{
	text-decoration:none;
}
    </style>
    
<script type="text/javascript">
    function openmodalWin(optionSelected) {
        if (window.showModalDialog) {
            var time = new Date().getTime();
            var ret = window.showModalDialog("../ConfirmationChartAbstraction.aspx?" + "time=" + time, "name", "dialogWidth:240px;dialogHeight:160px;status:no;scroll:no;edge:sunken;unadorned=1;");
            if (ret == "0") {
                location.replace("../PatientChartRegistration.aspx");
            }
            //alert(ret);
        }
        else {
            window.open('../ConfirmationChartAbstraction.aspx', 'name',
'height=200,width=200,toolbar=no,directories=no,status=no,menubar=no,scrollbars=no,resizable=no,modal=yes');
        }
    } 
</script>
    
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder1" Runat="Server">
    <asp:HiddenField ID="HiddenFieldRecordIdentifier" runat="server"/>
    <asp:HiddenField ID="HiddenFieldModalForm" runat="server"/>
    <!-- Amblyopia pim -->
    <div class="pim">
        <div class="record-ident clearfix">
            <h3 class="record-first">RECORD IDENTIFIER: <asp:Literal runat="server" ID="LiteralRecordIdentifier"></asp:Literal></h3>
            <h3 class="record-second"><asp:Literal runat="server" ID="LiteralAbstractionNumber"></asp:Literal></h3>
        </div>
        <!-- History Section -->
        <table>
            <tr>
                <th colspan="2" style="width: 910px;">
                    <p>History</p>
                </th>
            </tr>
            <!-- ----- Question 1 ----- -->
            <tr class="table_body">
                <td width="70%"><strong>1. Date of birth:</strong>
                    <asp:Label ID="LabelMonthOfBirth" runat="server" Visible="false"  ForeColor="Red" Text="<br />Please complete month" />
                    <asp:Label ID="LabelYearOfBirth" runat="server" Visible="false"  ForeColor="Red" Text="<br />Please complete year" />
                    <asp:Label ID="Labelinitialage" runat="server" Visible="false"  
                        ForeColor="Red" Text="<br />" />
                            <asp:Label ID="Labelinitialvis" runat="server" Visible="false"  
                        ForeColor="Red" Text="<br />" />
                </td>
                <td>
                    <asp:DropDownList ID="DropDownListDOBMonth" DataValueField="MonthID" DataTextField="MonthName" runat="server" >
                    </asp:DropDownList>
                    <asp:DropDownList ID="DropDownListDOBYear" DataValueField="YearID" DataTextField="YearName" runat="server" >
                    </asp:DropDownList>
                </td>
            </tr>
            <!--<tr><td colspan="2"><asp:Label runat="server" ID="LabelQ1NotCompleted" Visible="false" class="errorMsg"></asp:Label></td></tr>-->
            <!-- ----- Question 2 ----- -->
            <tr class="table_body_bg">
                <td>
                    <strong>2. Date of your first exam:</strong>
                    <asp:Label ID="LabelMonthOfFirstExam" runat="server" Visible="false"  ForeColor="Red" Text="<br />Please complete" />
                    <asp:Label ID="LabelYearOfFirstExam" runat="server" Visible="false"  ForeColor="Red" Text="<br />Please complete" />
                
                </td>
                <td>
                    <asp:DropDownList ID="DropDownListFirstExamMonth" DataValueField="MonthID" DataTextField="MonthName" runat="server"  />
                    <asp:DropDownList ID="DropDownListFirstExamYear" DataValueField="YearID" DataTextField="YearName" runat="server"  />
                </td>
            </tr>
            <!-- ----- Question 3 ----- -->
            <tr class="table_body">
                <td><strong>3. Date of six month follow-up exam (or nearest exam in range of 3 to 9 mos.):</strong>
                    <asp:Label ID="LabelMonthFollowUp" runat="server" Visible="false"  ForeColor="Red" Text="<br />Please complete month" />
                    <asp:Label ID="LabelYearFollowUp" runat="server" Visible="false"  ForeColor="Red" Text="<br />Please complete year" />
                    <asp:Label ID="Labelinitialfol" runat="server" Visible="false"  
                        ForeColor="Red" Text="<br />" />
                </td>
                <td>
                    <asp:DropDownList ID="DropDownListFollowUpMonth" DataValueField="MonthID" DataTextField="MonthName"
                        runat="server" >
                    </asp:DropDownList>
                    <asp:DropDownList ID="DropDownListFollowUpYear" DataValueField="YearID" DataTextField="YearName"
                        runat="server" >
                    </asp:DropDownList>
                </td>
            </tr>
        </table>
        <br />
        <!-- ----- Diagnoses / Assessment  Section ----- -->
        <table width="910px">
            <!-- Header - Diagnoses / Assessment -->
            <tr>
                <th colspan="3" style="width: 910px;"><p>Diagnoses / Assessment</p></th>
            </tr>
            <!-- ----- Question 1 ----- -->
            <tr class="table_body">
                <td rowspan="2">
                    <strong>1. Best-corrected visual acuity on initial examination</strong>&nbsp;<a href="#"><img id="QD1" src="../../common/images/tip.gif" alt="Tip" /></a>
                    <asp:Label ID="LabelBestCorrectedVisualOD" runat="server" Visible="false"  ForeColor="Red" Text="<br />Please complete OD" />
                    <asp:Label ID="LabelBestCorrectedVisualOS" runat="server" Visible="false"  ForeColor="Red" Text="<br />Please complete OS" />
                </td>
                <td><span class="right">OD</span></td>
                <td width="30%">
                    <!-- Dropdown 1 -->
                    <label>20/</label>
                    <span id="Q1a">
                        <asp:DropDownList ID="DropDownListBestCorrectedVisualOD" DataValueField="examValue" DataTextField="examLabel" runat="server">
                        </asp:DropDownList>
                    </span>
                </td>
            </tr>
            <tr class="table_body">
                <td><span class="right">OS</span></td>
                <td>
                    <!-- Dropdown 2 -->
                    <label>20/</label>
                    <span id="Q1b">
                        <asp:DropDownList ID="DropDownListBestCorrectedVisualOS" DataValueField="examValue" DataTextField="examLabel" runat="server">
                        </asp:DropDownList>
                    </span>
                </td>
            </tr>
            <!-- Question 1a -->
            <tr class="table_body">
                <td width="55%" rowspan="2">
                    <span id="Q1Atxta"><strong>1a. Method</strong></span>
                    <asp:Label ID="LabelMethodSnellenOptotypes" runat="server" Visible="false"  ForeColor="Red" Text="<br />Please complete" />
                    <asp:Label ID="LabelSnellenoptotypes" runat="server" Visible="false"  ForeColor="Red" Text="<br />Please complete Snellen optotypes " />
                </td>
                <td width="15%"><span class="right" id="Q1Atxtb">Snellen optotypes</span></td>
                <td>
                    <!-- Question 1A Snellen optotypes - yes -->
                    <asp:RadioButton runat="server" ID="RadioButtonMethodSnellenOptotypesYes" GroupName="MethodSnellenOptotypes" class="radio" onclick="deActivateQ1aSnellen();" />
                    <label><span id="Q1Atxtc">Yes</span></label>
                    <asp:RadioButton runat="server" ID="RadioButtonMethodSnellenOptotypesNo" GroupName="MethodSnellenOptotypes" class="radio" onclick="activateQ1aSnellen();"/>
                    <label><span id="Q1Atxtd">No</span></label>
                </td>
            </tr>
            <tr class="table_body">
                <td>
                    <span class="right" id="Q1Atxte">Non-Snellen<br />optotypes&nbsp;<a href="#"><img id="Q1aNonsnell" src="../../common/images/tip.gif" alt="Tip" /></a></span>
                </td>
                <td>
                    <!-- Question 1A non-Snellen optotypes - dropdown -->
                    <asp:DropDownList ID="DropDownListMethodNonSnellenOptotypesType" DataValueField="nonSellenOptotypesValue" DataTextField="nonSellenOptotypesLabel" runat="server" />
                </td>
            </tr>
            <!-- ----- Question 2 ----- -->
            <tr class="table_body_bg">
                <td colspan="2">
                    <strong>2. Was sensory testing attempted or performed?</strong>
                    <asp:Label ID="LabelSensoryTestingPerformed" runat="server" Visible="false"  ForeColor="Red" Text="<br />Please complete" />
                </td>
                <td>
                    <span id="Q2A"><asp:RadioButton runat="server" ID="RadioButtonSensoryTestingPerformedYes" GroupName="SensoryTestingPerformed" onclick="activateQ2A()" class="radio"/></span>
                    <label>Yes</label>
                    <span id="Q2B"><asp:RadioButton runat="server" ID="RadioButtonSensoryTestingPerformedNo" GroupName="SensoryTestingPerformed" onclick="deActivateQ2A()" class="radio"/></span>
                    <label>No</label>
                </td>
            </tr>
            <!-- ----- Question 2a ----- -->
            <tr class="table_body_bg">
                <td rowspan="3"><span id="Q2Atxta"><strong>2a. Method</strong></span>
                    <asp:Label runat="server" ID="LabelDiagnosesQ2a" Visible="false" class="errorMsg"><br />* Please complete</asp:Label>
                </td>
                <td><span class="right" id="Q2Atxtb">Stereoacuity&nbsp;<a href="#"><img id="QD2a" src="../../common/images/tip.gif" alt="Tip" /></a></span></td>
                <td>
                    <!-- Stereoacuity value box -->
                    <asp:TextBox runat="server" ID="TextBoxMethodStereoacuityDescription" size="5" onkeyup="diagStereoCheckBox();" Width="38px"></asp:TextBox>
                    <br />
                    <!-- Unable to complte checkbox -->
                    <asp:CheckBox runat="server" ID="CheckBoxMethodStereoacuityUnableToComplete" class="check" OnClick="diagStereoTextField();" />
                    <label><span id="Q2Atxtc">Unable to complete or perform test</span></label>
                </td>
            </tr>
            <tr class="table_body_bg">
                <td><span class="right" id="Q2Atxtd">Worth 4 dot<br />Distance:</span></td>
                <td>
                    <!-- Question 2A pull-down box -->
                    <span id="Q2Ac">
                        <asp:DropDownList ID="DropDownListWorth4DotDistanceDescription" DataValueField="worth4dotValue" DataTextField="worth4dotLabel" runat="server" onchange="diagW4dotDistanceCheckBox();" />
                        <br />
                        <!-- Unable to complete checkbox -->
                        <span id="Q2Ad"><asp:CheckBox runat="server" ID="CheckBoxWorth4DotDistanceUnableToComplete" class="check" onclick="diagW4dotDistanceDropDown();"/></span>
                        <label><span id="Q2Atxte">Unable to complete or perform test</span></label>
                    </span>
                </td>
            </tr>
            <tr class="table_body_bg">
                <!-- Question 2A pull-down box 2 -->
                <td><span class="right" id="Q2Atxtf">Worth 4 dot<br />Near:</span></td>
                <td>
                    <span id="Q2Ae">
                        <asp:DropDownList ID="DropDownListWorth4DotNearDescription" DataValueField="worth4dotValue" DataTextField="worth4dotLabel" runat="server" onchange="diagW4dotNearCheckBox();">
                        </asp:DropDownList>
                        <!-- Unable to complete checkbox -->
                        <br />
                        <span id="Q2Af"><asp:CheckBox runat="server" ID="CheckBoxWorth4DotNearUnableToComplete" class="check" onclick="diagW4dotNearDropDown();" /></span>
                        <label><span id="Q2Atxtg">Unable to complete or perform test</span></label>
                    </span>
                </td>
            </tr>
            <!-- ----- Question 3 ----- -->
            <tr class="table_body">
                <td colspan="2">
                    <strong>3. Was alignment measured?</strong>
                    <asp:Label ID="LabelAlignmentMeasured" runat="server" Visible="false"  ForeColor="Red" Text="<br />Please complete" />
                </td>
                <td>
                    <!-- Question 3 - Yes -->
					<span id="Q3a"><asp:RadioButton runat="server" ID="RadioButtonAlignmentMeasuredYes" GroupName="AlignmentMeasured" class="radio" text="Yes" /></span>
                    <!-- Question 3 - No -->
					<span id="Q3b"><asp:RadioButton runat="server" ID="RadioButtonAlignmentMeasuredNo" GroupName="AlignmentMeasured" class="radio" text="No"/></span>
                </td>
            </tr>
            <!-- ----- Question 3a ----- -->
            <tr class="table_body">
                <td rowspan="2" id="QDA3col1">
                    <strong>3a. Alignment in primary position</strong>
                    <asp:Label ID="LabelPrimaryPositionDistanceLeftPD" runat="server" Visible="false"  ForeColor="Red" Text="<br />Please complete distance PD" />
                    <asp:Label ID="LabelPrimaryPositionDistanceLeftET" runat="server" Visible="false"  ForeColor="Red" Text="<br />Please complete distance ET" />
                    <asp:Label ID="LabelPrimaryPositionDistanceRightPD" runat="server" Visible="false"  ForeColor="Red" Text="<br />Please complete distance PD HT" />
                    <asp:Label ID="LabelPrimaryPositionNearLeftPD" runat="server" Visible="false"  ForeColor="Red" Text="<br />Please complete near PD" />
                    <asp:Label ID="LabelPrimaryPositionNearLeftET" runat="server" Visible="false"  ForeColor="Red" Text="<br />Please complete near ET" />
                    <asp:Label ID="LabelPrimaryPositionNearRightPD" runat="server" Visible="false"  ForeColor="Red" Text="<br />Please complete near PD HT" />
                </td>
                <td>
                    <span class="right" id="QDA3col2row1">Distance&nbsp;<a href="#"><img id="QD3aa" src="../../common/images/tip.gif" alt="Tip" /></a></span>
                </td>
                <td>
                    <!-- Q3A - Input Distance PD 1 -->
                    <span id="OrthoAa"><asp:CheckBox ID="RadioButtonAlligmentOrthoporiaDistance" Text="Orthophoria" runat="server" /></span>
                    <br />
                    <span id="OrthoAb">
                        <asp:TextBox runat="server" meta="{vMin: '0', vMax: '180'}" class="auto" ID="TextBoxPrimaryPositionDistanceLeftPD" size="5"></asp:TextBox><label>&nbsp;PD</label>
                        <asp:RadioButton runat="server" ID="RadioButtonPrimaryPositionDistanceLeftETYes" GroupName="PrimaryPositionDistanceLeftPD" class="radio" /><label>ET</label>
                        <asp:RadioButton runat="server" ID="RadioButtonPrimaryPositionDistanceLeftETNo" GroupName="PrimaryPositionDistanceLeftPD" class="radio" /><label>XT</label>
                        <br /><asp:TextBox runat="server" meta="{vMin: '0', vMax: '180'}" class="auto" ID="TextBoxPrimaryPositionDistanceRightPD" size="5"></asp:TextBox><label>&nbsp;PD HT</label>
                    </span>
                </td>
            </tr>
            <tr class="table_body">
                <td>
                    <span class="right" id="QDA3col2row2">Near&nbsp;<a href="#"><img id="QD3ab" src="../../common/images/tip.gif" alt="Tip" /></a></span>
                </td>
                <td>
                    <span id="OrthoBa"><asp:CheckBox ID="RadioButtonAlligmentOrthoporiaNear" Text="Orthophoria" runat="server" /></span>
                    <br />
                    <span id="OrthoBb">
                        <asp:TextBox runat="server" meta="{vMin: '0', vMax: '180'}" class="auto" ID="TextBoxPrimaryPositionNearLeftPD" size="5"></asp:TextBox><label>&nbsp;PD</label>
                        <asp:RadioButton runat="server" ID="RadioButtonPrimaryPositionNearLeftETYes" GroupName="PrimaryPositionNearLeftET" class="radio" /><label>ET</label>
					    <asp:RadioButton runat="server" ID="RadioButtonPrimaryPositionNearLeftETNo" GroupName="PrimaryPositionNearLeftET" class="radio" /><label><span id="Q3Atxtk">XT</span></label>
                        <br /><asp:TextBox runat="server" meta="{vMin: '0', vMax: '180'}" class="auto" ID="TextBoxPrimaryPositionNearRightPD" size="5"></asp:TextBox><label>&nbsp;PD</label>
					    <asp:RadioButton runat="server" ID="RadioButtonPrimaryPositionNearRightETYes" GroupName="PrimaryPositionNearRightET" class="radio" Visible="false"/><label>HT</label>
					    <asp:RadioButton runat="server" ID="RadioButtonPrimaryPositionNearRightETNo" GroupName="PrimaryPositionNearRightET" class="radio"  Visible="false"/>
                    </span>
                </td>
            </tr>
            <!-- ----- Question 4 ----- -->
            <tr class="table_body_bg">
                <td colspan="2">
                    <strong>4. Was cyclopegic retinoscopy performed?</strong>
                    <asp:Label ID="LabelCyclopegicRetinoscopyPerformed" runat="server" Visible="false"  ForeColor="Red" Text="<br />Please complete" />
                </td>
                <td>
                    <!-- Question 4 - Yes -->
                    <span id="Q4A"><asp:RadioButton runat="server" ID="RadioButtonCyclopegicRetinoscopyPerformedYes" GroupName="CyclopegicRetinoscopyPerformed" class="radio" onclick="activateQ4A();" /></span>
                    <label>Yes</label>
                    <!-- Question 4 - No -->
                    <span id="Q4B"><asp:RadioButton runat="server" ID="RadioButtonCyclopegicRetinoscopyPerformedNo" GroupName="CyclopegicRetinoscopyPerformed" class="radio" onclick="deActivateQ4A();" /></span>
                    <label>No</label>
                </td>
            </tr>
            <!-- ----- Question 4a ----- -->
            <tr class="table_body_bg">
                <td rowspan="2"><strong><span id="Q4Atxta">4a. Cycloplegic refraction</span></strong>
                    <asp:Label ID="LabelCycloplegicRefractionODsphSign" runat="server" Visible="false"  ForeColor="Red" Text="<br />Please complete OD sph sign" />
                    <asp:Label ID="LabelCycloplegicRefractionODsph" runat="server" Visible="false"  ForeColor="Red" Text="<br />Please complete OD sph" />
                    <asp:Label ID="LabelCycloplegicRefractionODcyl" runat="server" Visible="false"  ForeColor="Red" Text="<br />Please complete OD cyl" />
                    <asp:Label ID="LabelCycloplegicRefractionODaxis" runat="server" Visible="false"  ForeColor="Red" Text="<br />Please complete OD axis" />
                    <asp:Label ID="LabelCycloplegicRefractionOSsphSign" runat="server" Visible="false"  ForeColor="Red" Text="<br />Please complete OS sph sign" />
                    <asp:Label ID="LabelCycloplegicRefractionOSsph" runat="server" Visible="false"  ForeColor="Red" Text="<br />Please complete OS sph" />
                    <asp:Label ID="LabelCycloplegicRefractionOScyl" runat="server" Visible="false"  ForeColor="Red" Text="<br />Please complete OS cyl" />
                    <asp:Label ID="LabelCycloplegicRefractionOSaxis" runat="server" Visible="false"  ForeColor="Red" Text="<br />Please complete OS axis" />
                </td>
                <td>
                    <span class="right" id="Q4Atxtb">OD&nbsp;<a href="#"><img id="QD4aa" src="../../common/images/tip.gif" alt="Tip" /></a></span>
                </td>
                <td>
                    <table class="aspxList">
                        <tr>
                            <td class="right">
                                <!-- Question 4a sph 1 -->
					            <asp:RadioButton ID="RadioButtonCycloplegicRefractionODcylSignPlus" runat="server" GroupName="CycloplegicRefractionODcylSign" /><span id="Q4Atxtc">&nbsp;+</span>&nbsp;
                                <asp:RadioButton ID="RadioButtonCycloplegicRefractionODcylSignMinus" runat="server" GroupName="CycloplegicRefractionODcylSign" /><span id="Q4Atxtd">&nbsp;-</span>
                            </td>
                            <td>
                                <asp:TextBox runat="server" meta="{vMin: '-30.00', vMax: '30.00'}" class="auto" ID="TextBoxCycloplegicRefractionODsph" size="5"></asp:TextBox>
                                <label><span id="Q4Atxte">sph</span></label>
                                </span>
                            </td>
                        </tr>
                        <tr>
                            <td class="right">
                                <!-- Question 4a cyl 1 -->
					            <span id="Q4Atxtf">+</span>
                            </td>
                            <td>
                                <asp:TextBox runat="server" meta="{vMin: '0.00', vMax: '99.99'}" class="auto" ID="TextBoxCycloplegicRefractionODcyl" size="5"></asp:TextBox>
                                <label><span id="Q4Atxtg">cyl</span></label>
                            </td>
                        </tr>
                        <tr>
                            <td></td>
                            <td>
                                <!-- Question 4a axis 1 -->
					            <asp:TextBox runat="server" meta="{vMin: '0.00', vMax: '180.00'}" class="auto" ID="TextBoxCycloplegicRefractionODaxis" size="5"></asp:TextBox>
                                <label><span id="Q4Atxth">axis</span></label>
                            </td>
                        </tr>
                    </table>
                </td>
            </tr>
            <tr class="table_body_bg">
                <td>
                    <span class="right" id="Q4Atxti">OS&nbsp;<a href="#"><img id="QD4ab" src="../../common/images/tip.gif" alt="Tip" /></a></span>
                </td>
                <td>
                    <table class="aspxList">
                        <tr>
                            <td class="right">
                                <asp:RadioButton ID="RadioButtonTextBoxCycloplegicRefractionOSsphSignPlus" runat="server" GroupName="RadioButtonTextBoxCycloplegicRefractionOSsphSign" /><span id="Q4Atxtj">&nbsp;+</span>&nbsp;
                                <asp:RadioButton ID="RadioButtonTextBoxCycloplegicRefractionOSsphSignMinus" runat="server" GroupName="RadioButtonTextBoxCycloplegicRefractionOSsphSign" /><span id="Q4Atxtk">&nbsp;-</span>
                            </td>
                            <td>
                                <asp:TextBox runat="server" meta="{vMin: '-30.00', vMax: '30.00'}" class="auto" ID="TextBoxCycloplegicRefractionOSsph" size="5"></asp:TextBox>
                                <label><span id="Q4Atxtl">sph</span></label>
                            </td>
                        </tr>
                        <tr>
                            <td class="right"><span id="Q4Atxtm">+&nbsp;</span></td>
                            <td>
                                <asp:TextBox runat="server" meta="{vMin: '0.00', vMax: '99.99'}" class="auto" ID="TextBoxCycloplegicRefractionOScyl" size="5"></asp:TextBox>
                                <label><span id="Q4Atxtn">cyl</span></label>
                            </td>
                        </tr>
                        <tr>
                            <td></td>
                            <td>
                                <asp:TextBox runat="server" meta="{vMin: '0.00', vMax: '180.00'}" class="auto" ID="TextBoxCycloplegicRefractionOSaxis" size="5"></asp:TextBox>
                                <label><span id="Q4Atxto">axis</span></label>
                            </td>
                        </tr>
                    </table>
                </td>
            </tr>
        </table>
        <br />
        <!-- ----- Outcomes ----- -->
        <table width="910px">
            <!-- ----- Header - Outcomes ----- -->
            <tr>
                <th colspan="3"><p>Outcomes</p></th>
            </tr>
            <!-- Note -->
            <tr><td colspan="3">Select the follow-up visit closest to six mos. after the initial evaluation for the following outcomes.</td></tr>
            <!-- ----- Question 1 ----- -->
            <tr class="table_body">
                <td width="55%" rowspan="2">
                    <strong>1. Best-corrected visual acuity on 6 month follow-up examination</strong>&nbsp;<a href="#"><img id="QO1" src="../../common/images/tip.gif" alt="Tip" /></a>
                    <asp:Label ID="LabelOutcomeBestCorrectedVisualOD" runat="server" Visible="false"  ForeColor="Red" Text="<br />Please complete OD" />
                    <asp:Label ID="LabelOutcomeBestCorrectedVisualOS" runat="server" Visible="false"  ForeColor="Red" Text="<br />Please complete OS" />
                </td>
                <td width="15%"><span class="right">OD</span></td>
                <td widht="30%">
                    <label>20/</label>
                    <asp:DropDownList ID="DropDownListOutcomeBestCorrectedVisualOD" DataValueField="examValue" DataTextField="examLabel"
                        runat="server">
                    </asp:DropDownList>
                </td>
            </tr>
            <tr class="table_body">
                <td><span class="right">OS</span></td>
                <td>
                    <label>20/</label>
                    <asp:DropDownList ID="DropDownListOutcomeBestCorrectedVisualOS" DataValueField="examValue" DataTextField="examLabel"
                        runat="server">
                    </asp:DropDownList>
                </td>
            </tr>
            <!-- ----- Question 2 ----- -->
            <tr class="table_body_bg">
                <td  rowspan="3"><strong>2. Sensory testing</strong>
                    <asp:Label runat="server" ID="LabelOutcomesQ2" Visible="false" class="errorMsg"><br />* Please complete</asp:Label>
                </td>
                <td>
                    <span class="right">Stereoacuity&nbsp;<a href="#"><img id="QO2" src="../../common/images/tip.gif" alt="Tip" /></a></span>
                </td>
                <td>
                    <asp:TextBox runat="server" ID="TextBoxOutcomeMethodStereoacuityDescription" size="5" onkeyup="outcomesStereoCheckBox()"/>
                    <br />
                    <asp:CheckBox runat="server" ID="CheckBoxOutcomeMethodStereoacuityUnableToComplete" class="check" onclick="outcomesStereoTextField();" />
                    <label>Unable to complete or perform test</label>
                </td>
            </tr>
            <tr class="table_body_bg">
                <td>
                    <span class="right">Worth 4 dot
                    <br />Distance:</span>
                </td>
                <td>
                    <asp:DropDownList ID="DropDownListOutcomeWorth4DotDistanceDescription" DataValueField="worth4dotValue" DataTextField="worth4dotLabel"
                        runat="server" onchange="outcomesW4dotDistanceCheckBox();" >
                    </asp:DropDownList>
                    <br />
                    <asp:CheckBox runat="server" ID="CheckBoxOutcomeWorth4DotDistanceUnableToComplete" class="check" onclick="outcomesW4dotDistanceDropDown()"/>
                    <label>Unable to complete or perform test</label>
                </td>
            </tr>
            <tr class="table_body_bg">
                <td>
                    <span class="right">Worth 4 dot
                    <br />Near:</span>
                </td>
                <td>
                    <asp:DropDownList ID="DropDownListOutcomeWorth4DotNearDescription" DataValueField="worth4dotValue" DataTextField="worth4dotLabel"
                        runat="server" onchange="outcomesW4dotNearCheckBox();">
                    </asp:DropDownList>
                    <br />
                    <asp:CheckBox runat="server" ID="CheckBoxOutcomeWorth4DotNearUnableToComplete" class="check" onclick="outcomesW4dotNearDropDown();"/>
                    <label>Unable to complete or perform test</label>
                </td>
            </tr>
            <!-- ----- Question 3 ----- -->
            <tr class="table_body">
                <td colspan="2"><strong>3. Was compliance assessed and documented at least once during the six month period?</strong>
                    <asp:Label ID="LabelComplianceAssessedAndDocumented" runat="server" Visible="false"  ForeColor="Red" Text="<br />Please complete" />
                </td>
                <td>
                    <asp:RadioButton runat="server" ID="RadioButtonComplianceAssessedAndDocumentedYes" GroupName="ComplianceAssessedAndDocumented" class="radio"/>
                    <label>Yes</label>
                    </span>
                    <asp:RadioButton runat="server" ID="RadioButtonComplianceAssessedAndDocumentedNo" GroupName="ComplianceAssessedAndDocumented" class="radio"/>
                    <label>No</label>
                </td>
            </tr>
            <!-- ----- Question 4 ----- -->
            <tr class="table_body">
                <td colspan="2"><strong>4. At the six month visit, was continued treatment recommended?</strong>
                    <asp:Label ID="LabelContinuedTreatmentRecommended" runat="server" Visible="false"  ForeColor="Red" Text="<br />Please complete" />
                </td>
                <td>
                    <!-- Question 4, yes -->
                    <asp:RadioButton runat="server" ID="RadioButtonContinuedTreatmentRecommendedYes" GroupName="ContinuedTreatmentRecommended" class="radio"/>
                    <label>Yes</label>
                    <!-- Question 4, no -->
                    <asp:RadioButton runat="server" ID="RadioButtonContinuedTreatmentRecommendedNo" GroupName="ContinuedTreatmentRecommended" class="radio"/>
                    <label>No</label>
                </td>
            </tr>
        </table>
        <br />
        <!-- ----- Complications Section ----- -->
        <table width="910px">
            <!-- Header - Complications -->
            <tr>
                <th colspan="3"><p>Complications</p></th>
            </tr>
            <!-- ----- Question 1 ----- -->
            <tr class="table_body">
                <td width="35%" rowspan="4"><strong>1. Did any of the following occur during the six month period?</strong>
                </td>
                <td width="35%"><strong>Atropine reaction requiring discontinuation</strong>
                    <asp:Label ID="LabelAtropineReactionRequiringDiscontinuation" runat="server" Visible="false"  ForeColor="Red" Text="<br />Please complete" />
                </td>
                <td width="30%">
                    <asp:RadioButton runat="server" ID="RadioButtonAtropineReactionRequiringDiscontinuationYes" GroupName="AtropineReactionRequiringDiscontinuation" class="radio"/>
                    <label>Yes</label>
                    <asp:RadioButton runat="server" ID="RadioButtonAtropineReactionRequiringDiscontinuationNo" GroupName="AtropineReactionRequiringDiscontinuation" class="radio"/>
                    <label>No</label>
                </td>
            </tr>
            <tr class="table_body">
                <td><strong>Reverse amblyopia (decrease of 2 lines of best corrected acuity in the non-amblyopic eye)</strong>
                    <asp:Label ID="LabelReverseAmblyopia" runat="server" Visible="false"  ForeColor="Red" Text="<br />Please complete" />
                </td>
                <td>
                    <asp:RadioButton runat="server" ID="RadioButtonReverseAmblyopiaYes" GroupName="ReverseAmblyopia" class="radio"/>
                    <label>Yes</label>
                    <asp:RadioButton runat="server" ID="RadioButtonReverseAmblyopiaNo" GroupName="ReverseAmblyopia" class="radio"/>
                    <label>No</label>
                </td>
            </tr>
            <tr class="table_body">
                <td><strong>New onset of strabismus that did not resolve with cessation of treatment</strong>
                    <asp:Label ID="LabelNewOnsetOfStrabismus" runat="server" Visible="false"  ForeColor="Red" Text="<br />Please complete" />
                </td>
                <td>
                    <asp:RadioButton runat="server" ID="RadioButtonNewOnsetOfStrabismusYes" GroupName="NewOnsetOfStrabismus" class="radio"/>
                    <label>Yes</label>
                    <asp:RadioButton runat="server" ID="RadioButtonNewOnsetOfStrabismusNo" GroupName="NewOnsetOfStrabismus" class="radio"/>
                    <label>No</label>
                </td>
            </tr>
            <tr class="table_body">
                <td><strong>Recurrence of amblyopia following cessation of treatment (loss of 2 lines from best vision during treatment)</strong>
                    <asp:Label ID="LabelRecurrenceOfAmblyopia" runat="server" Visible="false"  ForeColor="Red" Text="<br />Please complete" />
                </td>
                <td>
                    <asp:RadioButton runat="server" ID="RadioButtonRecurrenceOfAmblyopiaYes" GroupName="RecurrenceOfAmblyopia" class="radio"/>
                    <label>Yes</label>
                    <asp:RadioButton runat="server" ID="RadioButtonRecurrenceOfAmblyopiaNo" GroupName="RecurrenceOfAmblyopia" class="radio"/>
                    <label>No</label>
                </td>
            </tr>
        </table>
        
        <div class="button-box">
            <asp:LinkButton ID="LinkButtonBackToDashboard" runat="server" Text="Back To Chart Registration" PostBackUrl="../PatientChartRegistration.aspx?CycleNumber=1" Visible="false" CssClass="button" />
            <asp:LinkButton ID="LinkButtonSave" runat="server" Text="Submit"  OnClick="ButtonSubmit_Click" CssClass="button" />
        </div>
    </div>
    <!-- Amblyopia pim ends -->
<script>
    var HiddenFieldModalForm = document.getElementById("<%= HiddenFieldModalForm.ClientID %>").value;
    if (HiddenFieldModalForm == "1")
        openmodalWin(48);
</script>

<script>
    $("#<%= DropDownListFirstExamMonth.ClientID %>").change(function () {
        if (!validateTimeFromAppointment())
            wipeFollowUp()
    });
    $("#<%= DropDownListFirstExamYear.ClientID %>").change(function () {
        if (!validateTimeFromAppointment())
            wipeFollowUp()
    });
    $("#<%= DropDownListFollowUpMonth.ClientID %>").change(function () {
        if (!validateTimeFromAppointment())
            wipeFollowUp()
    });
    $("#<%= DropDownListFollowUpYear.ClientID %>").change(function () {
        if (!validateTimeFromAppointment())
            wipeFollowUp()
    });

    function wipeFollowUp() {
        var firstExamMon = document.getElementById("<%= DropDownListFirstExamMonth.ClientID %>").value;
        var firstExamYear = document.getElementById("<%= DropDownListFirstExamYear.ClientID %>").value;
        var secondExamMon = document.getElementById("<%= DropDownListFollowUpMonth.ClientID %>").value;
        var secondExamYear = document.getElementById("<%= DropDownListFollowUpYear.ClientID %>").value;

        if ((firstExamMon != 0) && (firstExamYear != 0) && (secondExamMon != 0) && (secondExamYear != 0)) {
            $("#<%= DropDownListFollowUpMonth.ClientID %>").val("0");
            $("#<%= DropDownListFollowUpYear.ClientID %>").val("0");
            alert("Follow-up exam must be within 3 to 9 months of the first exam.");
        }
    }
</script>

<script>
        // Fires off the validation for appointments when the appointment select boxes are changed    
        
        // Date Validation, Age Validation and Follow-up Date Validation

        // Checks that the date has happened
        function validateDate(month, year) {
            var today = new Date();
            var monthVar = month - 1;
            var checkYear = today.getFullYear() - year;
            var checkMonth = today.getMonth() - monthVar;
            if (checkMonth < 0) {
                checkYear--;
            }
            if (checkYear < 0) {
                alert("Invalid date, you must pick a date that has already occurred.");
                return false;
            }
            else {
                return true;
            }
        }

        // Checks to make sure the patient is within the proper age range
        function validateAge() {
            var DOBMonth = document.getElementById("<%= DropDownListDOBMonth.ClientID %>").value;
            var DOBYear = document.getElementById("<%= DropDownListDOBYear.ClientID %>").value;
            var ExamMonth = document.getElementById("<%= DropDownListFirstExamMonth.ClientID %>").value;
            var ExamYear = document.getElementById("<%= DropDownListFirstExamYear.ClientID %>").value;

            if ((DOBMonth != 0) && (DOBYear != 0) && (ExamMonth != 0) && (ExamYear != 0)) {
                var age = getAge(DOBMonth, DOBYear, ExamMonth, ExamYear);
                if ((age >= 3) && (age <= 12)) {
                    //console.log(age);
                    return true;
                }
                else {
                    //console.log(age);
                    return false;
                }
            }
        }

        // Will make sure the appropriate amount of time has elapsed between appointments
        function validateTimeFromAppointment() {
            var firstExamMon = document.getElementById("<%= DropDownListFirstExamMonth.ClientID %>").value;
            var firstExamYear = document.getElementById("<%= DropDownListFirstExamYear.ClientID %>").value;
            var secondExamMon = document.getElementById("<%= DropDownListFollowUpMonth.ClientID %>").value;
            var secondExamYear = document.getElementById("<%= DropDownListFollowUpYear.ClientID %>").value;

            if ((firstExamMon != 0) && (firstExamYear != 0) && (secondExamMon != 0) && (secondExamYear != 0)) {
                var age = howManyMonths(firstExamMon, firstExamYear, secondExamMon, secondExamYear);
                if ((age >= 3) && (age <= 9)) {
                    //alert(age);
                    return true;
                }
                else {
                    //alert(age);
                    return false;
                }
            }
        }

        // Returns how many months are between the dates
        function howManyMonths(firstMon, firstYear, secMon, secYear) {
            // Input variables
            var DOBMonth = firstMon;
            var DOBYear = firstYear;
            var ExamMonth = secMon;
            var ExamYear = secYear;
            //alert("DOBMonth: " + DOBMonth + "; DOBYear: " + DOBYear + "; ExamMonth: " + ExamMonth + "; ExamYear: " + ExamYear);
            // Calculation variables
            var yearsInMonths = 0;
            var years = ExamYear - DOBYear;
            var months = ExamMonth - DOBMonth;
            // Variable to be returned
            if (years > 0) {
                yearsInMonths = years * 12;
            }
            var totalMonths = yearsInMonths + months;
            return totalMonths;
        }

        // will return age between the two dates
        function getAge(dobMon, dobYear, secMon, secYear) {
            var DOBMonth = dobMon;
            var DOBYear = dobYear;
            var ExamMonth = secMon;
            var ExamYear = secYear;
            //alert("DOBMonth: " + DOBMonth + "; DOBYear: " + DOBYear + "; ExamMonth: " + ExamMonth + "; ExamYear: " + ExamYear);
            var age = ExamYear - DOBYear;
            var m = ExamMonth - DOBMonth;
            if (m < 0) {
                age--;
            }
            return age;
        }

        var selAboo;
        var selBboo;

        //      Either enables or disables input boxes
        function enabledControl(el, disabled, checked) {
            //alert("Hello world from enabled control");
            try {
                if (disabled) {
                    el.disabled = disabled;
                    el.setAttribute("disabled", true);
                }
                else {
                    el.disabled = disabled;
                    el.removeAttribute("disabled");
                }

                if (checked == true)
                    el.checked = false;
            }
            catch (E) {
            }
            if (el.childNodes && el.childNodes.length > 0) {
                for (var x = 0; x < el.childNodes.length; x++) {
                    enabledControl(el.childNodes[x], disabled, checked);
                }
            }
        }

        function enabledControlDropDown(el, disabled, checked) {
            //alert("Hello world from enabled control");
            try {
                if (disabled) {
                    el.disabled = disabled;
                    el.setAttribute("disabled", true);
                }
                else {
                    el.disabled = disabled;
                    el.removeAttribute("disabled");
                }

                if (checked == true)
                    el.options[0].selected = true;
            }
            catch (E) {
            }
            if (el.childNodes && el.childNodes.length > 0) {
                for (var x = 0; x < el.childNodes.length; x++) {
                    enabledControl(el.childNodes[x], disabled, checked);
                }
            }
        }

        function enabledControlTextField(el, disabled, checked) {
            //alert("Hello world from enabled control");
            try {
                if (disabled) {
                    el.disabled = disabled;
                    el.setAttribute("disabled", true);
                }
                else {
                    el.disabled = disabled;
                    el.removeAttribute("disabled");
                }

                if (checked == true) {
                    el.value = '';
                }
            }
            catch (E) {
            }
            if (el.childNodes && el.childNodes.length > 0) {
                for (var x = 0; x < el.childNodes.length; x++) {
                    enabledControl(el.childNodes[x], disabled, checked);
                }
            }
        }

        //      Runs the 'yes' option for question 2, it will enable all of the selectable fields for question 2A.
        //      The function also runs a check to see if the user is reselecting 'yes' in order not to wipe previously entered data.
        function activateQ2A() {
            //if (Q2a==checked) {
            enabledControlTextField(document.getElementById("<%= TextBoxMethodStereoacuityDescription.ClientID %>"), false, true);
            enabledControl(document.getElementById("<%= CheckBoxMethodStereoacuityUnableToComplete.ClientID %>"), false, true);
            enabledControlDropDown(document.getElementById("<%= DropDownListWorth4DotDistanceDescription.ClientID %>"), false, true);
            enabledControl(document.getElementById("<%= CheckBoxWorth4DotDistanceUnableToComplete.ClientID %>"), false, true);
            enabledControlDropDown(document.getElementById("<%= DropDownListWorth4DotNearDescription.ClientID %>"), false, true);
            enabledControl(document.getElementById("<%= CheckBoxWorth4DotNearUnableToComplete.ClientID %>"), false, true);
            unGrayQ2a();

        //          }
        }

        //      Runs the 'no' option for question 2; disabling all of the selectable fields for question 2A.
        function deActivateQ2A() {
            //enabledControl(Q4A, true, true);
            //enabledControl(Q4B, true, true);
            enabledControlTextField(document.getElementById("<%= TextBoxMethodStereoacuityDescription.ClientID %>"), true, true);
            enabledControl(document.getElementById("<%= CheckBoxMethodStereoacuityUnableToComplete.ClientID %>"), true, true);
            enabledControlDropDown(document.getElementById("<%= DropDownListWorth4DotDistanceDescription.ClientID %>"), true, true);
            enabledControl(document.getElementById("<%= CheckBoxWorth4DotDistanceUnableToComplete.ClientID %>"), true, true);
            enabledControlDropDown(document.getElementById("<%= DropDownListWorth4DotNearDescription.ClientID %>"), true, true);
            enabledControl(document.getElementById("<%= CheckBoxWorth4DotNearUnableToComplete.ClientID %>"), true, true);
            grayQ2a();
        }

        //      Runs the 'no' option for question 4; enabling all of the selectable fields for question 4A.				
        function activateQ4A() {

            enabledControl(document.getElementById("<%= RadioButtonCycloplegicRefractionODcylSignPlus.ClientID %>"), false, true);
            enabledControl(document.getElementById("<%= RadioButtonCycloplegicRefractionODcylSignMinus.ClientID %>"), false, true);
            enabledControlTextField(document.getElementById("<%= TextBoxCycloplegicRefractionODsph.ClientID %>"), false, true);
            enabledControlTextField(document.getElementById("<%= TextBoxCycloplegicRefractionODcyl.ClientID %>"), false, true);
            enabledControlTextField(document.getElementById("<%= TextBoxCycloplegicRefractionODaxis.ClientID %>"), false, true);
            enabledControl(document.getElementById("<%= RadioButtonTextBoxCycloplegicRefractionOSsphSignPlus.ClientID %>"), false, true);
            enabledControl(document.getElementById("<%= RadioButtonTextBoxCycloplegicRefractionOSsphSignMinus.ClientID %>"), false, true);
            enabledControlTextField(document.getElementById("<%= TextBoxCycloplegicRefractionOSsph.ClientID %>"), false, true);
            enabledControlTextField(document.getElementById("<%= TextBoxCycloplegicRefractionOScyl.ClientID %>"), false, true);
            enabledControlTextField(document.getElementById("<%= TextBoxCycloplegicRefractionOSaxis.ClientID %>"), false, true);
            unGrayQ4a();
        }

        //      Runs the 'no' option for question 4; disabling all of the selectable fields for question 4A.			
        function deActivateQ4A() {

            enabledControl(document.getElementById("<%= RadioButtonCycloplegicRefractionODcylSignPlus.ClientID %>"), true, true);
            enabledControl(document.getElementById("<%= RadioButtonCycloplegicRefractionODcylSignMinus.ClientID %>"), true, true);
            enabledControlTextField(document.getElementById("<%= TextBoxCycloplegicRefractionODsph.ClientID %>"), true, true);
            enabledControlTextField(document.getElementById("<%= TextBoxCycloplegicRefractionODcyl.ClientID %>"), true, true);
            enabledControlTextField(document.getElementById("<%= TextBoxCycloplegicRefractionODaxis.ClientID %>"), true, true);
            enabledControl(document.getElementById("<%= RadioButtonTextBoxCycloplegicRefractionOSsphSignPlus.ClientID %>"), true, true);
            enabledControl(document.getElementById("<%= RadioButtonTextBoxCycloplegicRefractionOSsphSignMinus.ClientID %>"), true, true);
            enabledControlTextField(document.getElementById("<%= TextBoxCycloplegicRefractionOSsph.ClientID %>"), true, true);
            enabledControlTextField(document.getElementById("<%= TextBoxCycloplegicRefractionOScyl.ClientID %>"), true, true);
            enabledControlTextField(document.getElementById("<%= TextBoxCycloplegicRefractionOSaxis.ClientID %>"), true, true);
            grayQ4a();
        }

        // If the patient had a snellen optotype, this disables the dropdown
        function deActivateQ1aSnellen() {
            enabledControlDropDown(document.getElementById("<%= DropDownListMethodNonSnellenOptotypesType.ClientID %>"), true, true);
        }

        // If the patient did not have a snellen optotype, this enables the dropdown
        function activateQ1aSnellen() {
            enabledControlDropDown(document.getElementById("<%= DropDownListMethodNonSnellenOptotypesType.ClientID %>"), false, true);
        }

        // Diagnoses / Assessment - Q2a1 --- Clears the checkbox if the textfield is changed 
        function diagStereoTextField() {
            enabledControlTextField(document.getElementById("<%= TextBoxMethodStereoacuityDescription.ClientID %>"), false, true);
        }

        // Diagnoses / Assessment - Q2a1 --- Clears the textfield if the checkbox is changed
        function diagStereoCheckBox() {
            enabledControl(document.getElementById("<%= CheckBoxMethodStereoacuityUnableToComplete.ClientID %>"), false, true);
        }

        // Diagnoses / Assessment - Q2a2 --- Clears the checkbox if the dropdown list is changed 
        function diagW4dotDistanceDropDown() {
            enabledControlDropDown(document.getElementById("<%= DropDownListWorth4DotDistanceDescription.ClientID %>"), false, true);
        }

        // Diagnoses / Assessment - Q2a2 --- Clears the dropdown list if the checkbox is changed
        function diagW4dotDistanceCheckBox() {
            enabledControl(document.getElementById("<%= CheckBoxWorth4DotDistanceUnableToComplete.ClientID %>"), false, true);
        }

        // Diagnoses / Assessment - Q2a3 --- Clears the checkbox if the dropdown list is changed 
        function diagW4dotNearDropDown() {
            enabledControlDropDown(document.getElementById("<%= DropDownListWorth4DotNearDescription.ClientID %>"), false, true);
        }

        // Diagnoses / Assessment - Q2a3 --- Clears the dropdown list if the checkbox is changed
        function diagW4dotNearCheckBox() {
            enabledControl(document.getElementById("<%= CheckBoxWorth4DotNearUnableToComplete.ClientID %>"), false, true);
        }

        // Outcomes - Q2a1 --- Clears the checkbox if the textfield is changed 
        function outcomesStereoTextField() {
            enabledControlTextField(document.getElementById("<%= TextBoxOutcomeMethodStereoacuityDescription.ClientID %>"), false, true);
        }

        // Outcomes - Q2a1 --- Clears the textfield if the checkbox is changed
        function outcomesStereoCheckBox() {
            enabledControl(document.getElementById("<%= CheckBoxOutcomeMethodStereoacuityUnableToComplete.ClientID %>"), false, true);
        }

        // Outcomes - Q2a2 --- Clears the checkbox if the dropdown list is changed 
        function outcomesW4dotDistanceDropDown() {
            enabledControlDropDown(document.getElementById("<%= DropDownListOutcomeWorth4DotDistanceDescription.ClientID %>"), false, true);
        }

        // Outcomes - Q2a2 --- Clears the dropdown list if the checkbox is changed
        function outcomesW4dotDistanceCheckBox() {
            enabledControl(document.getElementById("<%= CheckBoxOutcomeWorth4DotDistanceUnableToComplete.ClientID %>"), false, true);
        }

        // Outcomes - Q2a3 --- Clears the checkbox if the dropdown list is changed 
        function outcomesW4dotNearDropDown() {
            enabledControlDropDown(document.getElementById("<%= DropDownListOutcomeWorth4DotNearDescription.ClientID %>"), false, true);
        }

        // Outcomes - Q2a3 --- Clears the dropdown list if the checkbox is changed
        function outcomesW4dotNearCheckBox() {
            enabledControl(document.getElementById("<%= CheckBoxOutcomeWorth4DotNearUnableToComplete.ClientID %>"), false, true);
        }

        //      Disables 1a, 2a, 3a and 4a upon page load
        window.onload = function onLoadForm() {
            //        if (document.getElementById("<%= RadioButtonSensoryTestingPerformedNo.ClientID %>").checked)
            //            deActivateQ2A();
            //        if ((document.getElementById("<%= DropDownListBestCorrectedVisualOD.ClientID %>").value <= 400) && (document.getElementById("<%= DropDownListBestCorrectedVisualOS.ClientID %>").value <= 400)) {

            //            enabledControl(document.getElementById("<%= RadioButtonMethodSnellenOptotypesYes.ClientID %>"), true, true);
            //            enabledControl(document.getElementById("<%= RadioButtonMethodSnellenOptotypesNo.ClientID %>"), true, true);
            //            enabledControlDropDown(document.getElementById("<%= DropDownListMethodNonSnellenOptotypesType.ClientID %>"), true, true);
            //            grayQ1a();

            //        }
            //        if (document.getElementById("<%= RadioButtonAlignmentMeasuredNo.ClientID %>").checked)
            //            deActivateQ3A();
            //        if (document.getElementById("<%= RadioButtonCyclopegicRetinoscopyPerformedNo.ClientID %>").checked)
            //            deActivateQ4A();
            if (document.getElementById("<%= RadioButtonSensoryTestingPerformedYes.ClientID %>").checked != true) {
                deActivateQ2A();
            }
            if (document.getElementById("<%= RadioButtonAlignmentMeasuredYes.ClientID %>").checked != true) {
                //deActivateQ3A();
            }
            if (document.getElementById("<%= RadioButtonCyclopegicRetinoscopyPerformedYes.ClientID %>").checked != true) {
                deActivateQ4A();
            }
        }

        function grayQ2a() {
            document.getElementById('Q2Atxta').style.color = "gray";
            document.getElementById('Q2Atxtb').style.color = "gray";
            document.getElementById('Q2Atxtc').style.color = "gray";
            document.getElementById('Q2Atxtd').style.color = "gray";
            document.getElementById('Q2Atxte').style.color = "gray";
            document.getElementById('Q2Atxtf').style.color = "gray";
            document.getElementById('Q2Atxtg').style.color = "gray";
        }

        function unGrayQ2a() {
            document.getElementById('Q2Atxta').style.color = "#333";
            document.getElementById('Q2Atxtb').style.color = "#333";
            document.getElementById('Q2Atxtc').style.color = "#333";
            document.getElementById('Q2Atxtd').style.color = "#333";
            document.getElementById('Q2Atxte').style.color = "#333";
            document.getElementById('Q2Atxtf').style.color = "#333";
            document.getElementById('Q2Atxtg').style.color = "#333";
        }

        function grayQ4a() {
            document.getElementById('Q4Atxta').style.color = "gray";
            document.getElementById('Q4Atxtb').style.color = "gray";
            document.getElementById('Q4Atxtc').style.color = "gray";
            document.getElementById('Q4Atxtd').style.color = "gray";
            document.getElementById('Q4Atxte').style.color = "gray";
            document.getElementById('Q4Atxtf').style.color = "gray";
            document.getElementById('Q4Atxtg').style.color = "gray";
            document.getElementById('Q4Atxth').style.color = "gray";
            document.getElementById('Q4Atxti').style.color = "gray";
            document.getElementById('Q4Atxtj').style.color = "gray";
            document.getElementById('Q4Atxtk').style.color = "gray";
            document.getElementById('Q4Atxtl').style.color = "gray";
            document.getElementById('Q4Atxtm').style.color = "gray";
            document.getElementById('Q4Atxtn').style.color = "gray";
            document.getElementById('Q4Atxto').style.color = "gray";
        }

        function unGrayQ4a() {
            document.getElementById('Q4Atxta').style.color = "#333";
            document.getElementById('Q4Atxtb').style.color = "#333";
            document.getElementById('Q4Atxtc').style.color = "#333";
            document.getElementById('Q4Atxtd').style.color = "#333";
            document.getElementById('Q4Atxte').style.color = "#333";
            document.getElementById('Q4Atxtf').style.color = "#333";
            document.getElementById('Q4Atxtg').style.color = "#333";
            document.getElementById('Q4Atxth').style.color = "#333";
            document.getElementById('Q4Atxti').style.color = "#333";
            document.getElementById('Q4Atxtj').style.color = "#333";
            document.getElementById('Q4Atxtk').style.color = "#333";
            document.getElementById('Q4Atxtl').style.color = "#333";
            document.getElementById('Q4Atxtm').style.color = "#333";
            document.getElementById('Q4Atxtn').style.color = "#333";
            document.getElementById('Q4Atxto').style.color = "#333";
        }


        function generate(arr1, arr2, istrue) {
            if (istrue == true) {
                for (var i = 0; i < arr1.length; i++) {
                    document.getElementById(arr1[i]).style.color = "gray";
                }
                for (var i = 0; i < arr2.length; i++) {
                    $(arr2[i] + ' :input').attr('disabled', true);
                    $(arr2[i] + ' :input').removeAttr("checked");
                }
            }
            if (istrue == false) {
                for (var i = 0; i < arr1.length; i++) {
                    document.getElementById(arr1[i]).style.color = "#333";
                }
                for (var i = 0; i < arr2.length; i++) {
                    $(arr2[i] + ' :input').removeAttr('disabled');
                }
            }
        }

    $(document).ready(function () {
        if ($('#<%= RadioButtonAlligmentOrthoporiaDistance.ClientID %>').prop("checked") == true) {
            var myaray = new Array("OrthoAb");
            var myarray2 = new Array("#OrthoAb");
            generate(myaray, myarray2, true);

            enabledControlTextField(document.getElementById("<%= TextBoxPrimaryPositionDistanceLeftPD.ClientID %>"), true, true);
            enabledControlTextField(document.getElementById("<%= TextBoxPrimaryPositionDistanceRightPD.ClientID %>"), true, true);
        }
        else {
            var myaray = new Array("OrthoAb");
            var myarray2 = new Array("#OrthoAb");
            generate(myaray, myarray2, false);

            enabledControlTextField(document.getElementById("<%= TextBoxPrimaryPositionDistanceLeftPD.ClientID %>"), false, false);
            enabledControlTextField(document.getElementById("<%= TextBoxPrimaryPositionDistanceRightPD.ClientID %>"), false, false);
        }

        if ($('#<%= RadioButtonAlligmentOrthoporiaNear.ClientID %>').prop("checked") == true) {
            var myaray = new Array("OrthoBb");
            var myarray2 = new Array("#OrthoBb");
            generate(myaray, myarray2, true);

            enabledControlTextField(document.getElementById("<%= TextBoxPrimaryPositionNearLeftPD.ClientID %>"), true, true);
            enabledControlTextField(document.getElementById("<%= TextBoxPrimaryPositionNearRightPD.ClientID %>"), true, true);
        }
        else {
            var myaray = new Array("OrthoBb");
            var myarray2 = new Array("#OrthoBb");
            generate(myaray, myarray2, false);

            enabledControlTextField(document.getElementById("<%= TextBoxPrimaryPositionNearLeftPD.ClientID %>"), false, false);
            enabledControlTextField(document.getElementById("<%= TextBoxPrimaryPositionNearRightPD.ClientID %>"), false, false);
        }

        // Question 5, Initial Examination
        $("#<%= RadioButtonAlignmentMeasuredYes.ClientID %>").click(function () {
            if ($('#<%= RadioButtonAlignmentMeasuredYes.ClientID %>').prop("checked") == true) {
                var myaray = new Array("QDA3col1", "QDA3col2row1", "QDA3col2row2", "OrthoAa", "OrthoAb", "OrthoBa", "OrthoBb");
                var myarray2 = new Array("#OrthoAa", "#OrthoAb", "#OrthoBa", "#OrthoBb");
                generate(myaray, myarray2, false);

                enabledControlTextField(document.getElementById("<%= TextBoxPrimaryPositionDistanceLeftPD.ClientID %>"), false, false);
                enabledControlTextField(document.getElementById("<%= TextBoxPrimaryPositionNearLeftPD.ClientID %>"), false, false);
                enabledControlTextField(document.getElementById("<%= TextBoxPrimaryPositionNearRightPD.ClientID %>"), false, false);
            }
            else {
                var myaray = new Array("QDA3col1", "QDA3col2row1", "QDA3col2row2", "OrthoAa", "OrthoAb", "OrthoBa", "OrthoBb");
                var myarray2 = new Array("#OrthoAa", "#OrthoAb", "#OrthoBa", "#OrthoBb");
                generate(myaray, myarray2, true);

                enabledControlTextField(document.getElementById("<%= TextBoxPrimaryPositionDistanceLeftPD.ClientID %>"), true, true);
                enabledControlTextField(document.getElementById("<%= TextBoxPrimaryPositionNearLeftPD.ClientID %>"), true, true);
                enabledControlTextField(document.getElementById("<%= TextBoxPrimaryPositionNearRightPD.ClientID %>"), true, true);
            }
        });
        $("#<%= RadioButtonAlignmentMeasuredNo.ClientID %>").click(function () {
            if ($('#<%= RadioButtonAlignmentMeasuredYes.ClientID %>').prop("checked") == true) {
                var myaray = new Array("QDA3col1", "QDA3col2row1", "QDA3col2row2", "OrthoAa", "OrthoAb", "OrthoBa", "OrthoBb");
                var myarray2 = new Array("#OrthoAa", "#OrthoAb", "#OrthoBa", "#OrthoBb");
                generate(myaray, myarray2, false);

                enabledControlTextField(document.getElementById("<%= TextBoxPrimaryPositionDistanceLeftPD.ClientID %>"), false, false);
                enabledControlTextField(document.getElementById("<%= TextBoxPrimaryPositionNearLeftPD.ClientID %>"), false, false);
                enabledControlTextField(document.getElementById("<%= TextBoxPrimaryPositionNearRightPD.ClientID %>"), false, false);
                enabledControlTextField(document.getElementById("<%= TextBoxPrimaryPositionDistanceRightPD.ClientID %>"), false, false);
            }
            else {
                var myaray = new Array("QDA3col1", "QDA3col2row1", "QDA3col2row2", "OrthoAa", "OrthoAb", "OrthoBa", "OrthoBb");
                var myarray2 = new Array("#OrthoAa", "#OrthoAb", "#OrthoBa", "#OrthoBb");
                generate(myaray, myarray2, true);

                enabledControlTextField(document.getElementById("<%= TextBoxPrimaryPositionDistanceLeftPD.ClientID %>"), true, true);
                enabledControlTextField(document.getElementById("<%= TextBoxPrimaryPositionDistanceRightPD.ClientID %>"), true, true);
                enabledControlTextField(document.getElementById("<%= TextBoxPrimaryPositionNearLeftPD.ClientID %>"), true, true);
                enabledControlTextField(document.getElementById("<%= TextBoxPrimaryPositionNearRightPD.ClientID %>"), true, true);
            }
        });
        if ($('#<%= RadioButtonAlignmentMeasuredYes.ClientID %>').prop("checked") == true) {
            var myaray = new Array("QDA3col1", "QDA3col2row1", "QDA3col2row2", "OrthoAa", "OrthoAb", "OrthoBa", "OrthoBb");
            var myarray2 = new Array("#OrthoAa", "#OrthoAb", "#OrthoBa", "#OrthoBb");
            generate(myaray, myarray2, false);

            enabledControlTextField(document.getElementById("<%= TextBoxPrimaryPositionDistanceLeftPD.ClientID %>"), false, false);
            enabledControlTextField(document.getElementById("<%= TextBoxPrimaryPositionNearLeftPD.ClientID %>"), false, false);
            enabledControlTextField(document.getElementById("<%= TextBoxPrimaryPositionNearRightPD.ClientID %>"), false, false);
            enabledControlTextField(document.getElementById("<%= TextBoxPrimaryPositionDistanceRightPD.ClientID %>"), false, false);
        }
        else {
            var myaray = new Array("QDA3col1", "QDA3col2row1", "QDA3col2row2", "OrthoAa", "OrthoAb", "OrthoBa", "OrthoBb");
            var myarray2 = new Array("#OrthoAa", "#OrthoAb", "#OrthoBa", "#OrthoBb");
            generate(myaray, myarray2, true);

            enabledControlTextField(document.getElementById("<%= TextBoxPrimaryPositionDistanceLeftPD.ClientID %>"), true, true);
            enabledControlTextField(document.getElementById("<%= TextBoxPrimaryPositionNearLeftPD.ClientID %>"), true, true);
            enabledControlTextField(document.getElementById("<%= TextBoxPrimaryPositionNearRightPD.ClientID %>"), true, true);
            enabledControlTextField(document.getElementById("<%= TextBoxPrimaryPositionNearRightPD.ClientID %>"), true, true);
        }

        // Question 3, Initial Examination: Ortho A
        $("#<%= RadioButtonAlligmentOrthoporiaDistance.ClientID %>").click(function () {
            if ($('#<%= RadioButtonAlligmentOrthoporiaDistance.ClientID %>').prop("checked") == true) {
                var myaray = new Array("OrthoAb");
                var myarray2 = new Array("#OrthoAb");
                generate(myaray, myarray2, true);

                enabledControlTextField(document.getElementById("<%= TextBoxPrimaryPositionDistanceLeftPD.ClientID %>"), true, true);
                enabledControlTextField(document.getElementById("<%= TextBoxPrimaryPositionDistanceRightPD.ClientID %>"), true, true);
            }
            else {
                var myaray = new Array("OrthoAb");
                var myarray2 = new Array("#OrthoAb");
                generate(myaray, myarray2, false);

                enabledControlTextField(document.getElementById("<%= TextBoxPrimaryPositionDistanceLeftPD.ClientID %>"), false, false);
                enabledControlTextField(document.getElementById("<%= TextBoxPrimaryPositionDistanceRightPD.ClientID %>"), false, false);
            }
        });


        // Question 3, Initial Examination: Ortho B
        $("#<%= RadioButtonAlligmentOrthoporiaNear.ClientID %>").click(function () {
            if ($('#<%= RadioButtonAlligmentOrthoporiaNear.ClientID %>').prop("checked") == true) {
                var myaray = new Array("OrthoBb");
                var myarray2 = new Array("#OrthoBb");
                generate(myaray, myarray2, true);

                enabledControlTextField(document.getElementById("<%= TextBoxPrimaryPositionNearLeftPD.ClientID %>"), true, true);
                enabledControlTextField(document.getElementById("<%= TextBoxPrimaryPositionNearRightPD.ClientID %>"), true, true);
            }
            else {
                var myaray = new Array("OrthoBb");
                var myarray2 = new Array("#OrthoBb");
                generate(myaray, myarray2, false);

                enabledControlTextField(document.getElementById("<%= TextBoxPrimaryPositionNearLeftPD.ClientID %>"), false, false);
                enabledControlTextField(document.getElementById("<%= TextBoxPrimaryPositionNearRightPD.ClientID %>"), false, false);
            }
        });
    });
</script>

</asp:Content>


