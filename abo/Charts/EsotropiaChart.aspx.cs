﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Transactions;
using NetHealthPIMModel;

public partial class abo_EsotropiaChart : BasePage
{
    protected void Page_Load(object sender, EventArgs e)
    {
        if (!IsPostBack)
        {
            InitializePage();
        }

    }
    protected void InitializePage()
    {
        ((PIMMasterPage)Page.Master).SetTitle("Chart");

        using (NetHealthPIMEntities pim = new NetHealthPIMEntities())
        {
            int ModuleID = (int)Session[Constants.SESSION_WORKINGMODULEID];
            Module module = pim.Module.First(c => c.ModuleID == ModuleID);
            ((PIMMasterPage)Page.Master).SetHeader(module.ModuleName + " Chart Abstraction");

            ParticipantModuleCycle prev = (ParticipantModuleCycle)Session[Constants.SESSION_PREVIOUSCYCLE];
            String CycleNumber = Request.QueryString["CycleNumber"];

            int cycleID = ctx.ActiveModuleCycleID;
            if (CycleNumber == "1")
            {
                cycleID = prev.ParticipantModuleCycleID;
                LinkButtonBackToDashboard.Visible = true;
                ButtonSubmit.Visible = false;
            }

            ParticipantModuleCycle cycle = pim.ParticipantModuleCycle.First(c => c.ParticipantModuleCycleID == cycleID);
            String numberOfRecord = Request.QueryString["nr"];
            String totalRecords = Request.QueryString["t"];
            if (cycle.CycleNumber == 1)
            {
                ((PIMMasterPage)Page.Master).SetStatus(2);
                LiteralAbstractionNumber.Text = "Initial Abstraction: Chart " + numberOfRecord + " of " + totalRecords;
            }
            else
            {
                ((PIMMasterPage)Page.Master).SetStatus(3);
                LiteralAbstractionNumber.Text = "Second Abstraction: Chart " + numberOfRecord + " of " + totalRecords;
            }
            DropDownListMonthOfBirth.DataSource = CycleManager.getMonthList();
            DropDownListYearOfBirth.DataSource = CycleManager.getDOBYearList(0, 100);
            DropDownListMonthOfSurgery.DataSource = CycleManager.getMonthList();
            DropDownListYearOfSurgery.DataSource = CycleManager.getDOBYearList(0, 100);
            DropDownListMonthOfFollowUp1.DataSource = CycleManager.getMonthList();
            DropDownListYearOfFollowUp1.DataSource = CycleManager.getDOBYearList(0, 100);
            DropDownListMonthOfFollowUp2.DataSource = CycleManager.getMonthList();
            DropDownListYearOfFollowUp2.DataSource = CycleManager.getDOBYearList(0, 100);
            DropDownListBestCorrectedVisualOD.DataSource = CycleManager.getExamValues();
            DropDownListBestCorrectedVisualOS.DataSource = CycleManager.getExamValues();
            DropDownListOutcome2Worth4DotDistance.DataSource= CycleManager.getWorth4dot();
            DropDownListOutcome2Worth4DotNear.DataSource= CycleManager.getWorth4dot();
            DropDownListtWorth4dotdistance1.DataSource = CycleManager.getWorth4dot();
            DropDownListWorth4dotnear1.DataSource= CycleManager.getWorth4dot();
            DropDownListWorth4DotDistanceDescription.DataSource= CycleManager.getWorth4dot();
            DropDownListWorth4DotNearDescription.DataSource = CycleManager.getWorth4dot();
            //DropDownListMethodOptotypesType.DataSource = CycleManager.getNonSnellenOptotypes();
            DataBind();
        

            DropDownListYearOfFollowUp2.Items.Insert(0, new ListItem("Year", "0"));
            DropDownListYearOfFollowUp1.Items.Insert(0, new ListItem("Year", "0"));
            DropDownListYearOfSurgery.Items.Insert(0, new ListItem("Year", "0"));
            DropDownListYearOfBirth.Items.Insert(0, new ListItem("Year", "0"));

            string recordIdentifier = Request.QueryString["ri"];
            HiddenFieldRecordIdentifier.Value = recordIdentifier;
            LiteralRecordIdentifier.Text = recordIdentifier;
            var count = pim.ChartAbstractionEsotropia.Where(ps => ps.ParticipantModuleCycle.ParticipantModuleCycleID == cycleID & ps.RecordIdentifier == recordIdentifier).Count();
            if (count == 0)
            {
                using (TransactionScope transaction = new TransactionScope())
                {
                    //Module module = pim.Module.First(m => m.ModuleID == Constants.ABO_AMBLYOPIA_MODULEID);

                    NetHealthPIMModel.ChartRegistration chartRegistration = (from cr in pim.ChartRegistration
                                                           where cr.ParticipantModuleCycleID == cycleID
                                                           & cr.RecordIdentifier == recordIdentifier
                                                           & cr.ModuleID == Constants.ABO_ESOTROPIA_MODULEID
                                                                             select cr).First<NetHealthPIMModel.ChartRegistration>();

                    DateTime initialVisit = Convert.ToDateTime(chartRegistration.InitialVisit);
                    DateTime DOB = Convert.ToDateTime(chartRegistration.DOB);

                    ChartAbstractionEsotropia abstractRecord = new ChartAbstractionEsotropia();
                    abstractRecord.ParticipantModuleCycle = cycle;
                    abstractRecord.RecordIdentifier = recordIdentifier;
                    // abstractRecord.MonthOfFirstExam = initialVisit.Month;
                    // abstractRecord.YearOfFirstExam = initialVisit.Year;
                    abstractRecord.MonthOfBirth = DOB.Month;
                    abstractRecord.YearOfBirth = DOB.Year;
                    DropDownListMonthOfBirth.SelectedValue = abstractRecord.MonthOfBirth.ToString();
                    DropDownListYearOfBirth.SelectedValue = abstractRecord.YearOfBirth.ToString();
                    DropDownListMonthOfSurgery.SelectedValue = abstractRecord.MonthOfSurgery.ToString();
                    DropDownListYearOfSurgery.SelectedValue = abstractRecord.YearOfSurgery.ToString();
                    DropDownListYearOfFollowUp1.SelectedValue = abstractRecord.YearOfFollowUp1.ToString();
                    DropDownListMonthOfFollowUp1.SelectedValue = abstractRecord.MonthOfFollowUp1.ToString();
                    DropDownListMonthOfFollowUp2.SelectedValue = abstractRecord.MonthFollowUp2.ToString();
                    DropDownListYearOfFollowUp2.SelectedValue = abstractRecord.YearFollowUp2.ToString();
                    abstractRecord.CreatedOn = DateTime.Now;
                    abstractRecord.LastUpdateDate = DateTime.Now;
                    pim.SaveChanges();

                    transaction.Complete();
                }
            }

            else
            {
                ChartAbstractionEsotropia abstractRecord = (from c in pim.ChartAbstractionEsotropia
                                                            where c.ParticipantModuleCycle.ParticipantModuleCycleID == cycleID & c.RecordIdentifier == recordIdentifier
                                                            select c).First<ChartAbstractionEsotropia>();
                ////HISTORRY
                if (abstractRecord.RBPatientHasEsotropia!= null) RadioButtonListPatientHasEsotropia.SelectedValue = abstractRecord.RBPatientHasEsotropia.ToString();
                if (abstractRecord.MonthOfBirth != null) DropDownListMonthOfBirth.SelectedValue = abstractRecord.MonthOfBirth.ToString();
                if (abstractRecord.YearOfBirth != null) DropDownListYearOfBirth.SelectedValue = abstractRecord.YearOfBirth.ToString();
                if (abstractRecord.MonthOfSurgery != null) DropDownListMonthOfSurgery.SelectedValue = abstractRecord.MonthOfSurgery.ToString();
                if (abstractRecord.YearOfSurgery != null) DropDownListYearOfSurgery.SelectedValue = abstractRecord.YearOfSurgery.ToString();
                if (abstractRecord.MonthOfFollowUp1 != null) DropDownListMonthOfFollowUp1.SelectedValue = abstractRecord.MonthOfFollowUp1.ToString();
                if (abstractRecord.YearOfFollowUp1 != null) DropDownListYearOfFollowUp1.SelectedValue = abstractRecord.YearOfFollowUp1.ToString();
                if (abstractRecord.MonthFollowUp2 != null) DropDownListMonthOfFollowUp2.SelectedValue = abstractRecord.MonthFollowUp2.ToString();
                if (abstractRecord.YearFollowUp2 != null) DropDownListYearOfFollowUp2.SelectedValue = abstractRecord.YearFollowUp2.ToString();
                if (abstractRecord.RBFamilyHistoryStrabismus != null) RadioButtonListStrabismus.SelectedValue = abstractRecord.RBFamilyHistoryStrabismus.ToString();
               if (abstractRecord.RBHistoryPrematurity != null) RadioButtonListPermutality.SelectedValue = abstractRecord.RBHistoryPrematurity.ToString();
                ///////////////////////
               
                
                
                
                // /////INITIAL EXAMINATION
                if (abstractRecord.VisualAcuityEvaluated != null)
                {
                    RadioButtonVisualAcuityEvaluatedYes.Checked = ((bool)abstractRecord.VisualAcuityEvaluated);
                    RadioButtonVisualAcuityEvaluatedNo.Checked = !((bool)abstractRecord.VisualAcuityEvaluated);
                }
                if (abstractRecord.BestCorrectedVisualOD != null) DropDownListBestCorrectedVisualOD.SelectedValue = abstractRecord.BestCorrectedVisualOD.ToString();
                if (abstractRecord.BestCorrectedVisualOS != null) DropDownListBestCorrectedVisualOS.SelectedValue = abstractRecord.BestCorrectedVisualOS.ToString();
                if (abstractRecord.OptotypeUsed != null)
                {
                    RadioButtonOptotypeUsedYes.Checked = ((bool)abstractRecord.OptotypeUsed);
                    RadioButtonOptotypeUsedNo.Checked = !((bool)abstractRecord.OptotypeUsed);
                }
                if (abstractRecord.RBMethodOptotypesType != null) DropDownListMethodOptotypesType.SelectedValue = abstractRecord.RBMethodOptotypesType.ToString();
                if (abstractRecord.FixationalVision != null)
                {
                    RadioButtonFixationalVisionYes.Checked = ((bool)abstractRecord.FixationalVision);
                    RadioButtonFixationalVisionNo.Checked = !((bool)abstractRecord.FixationalVision);
                }
                if (abstractRecord.TellerCards != null)
                {
                    RadioButtonTellerCardsYes.Checked = ((bool)abstractRecord.TellerCards);
                    RadioButtonTellerCardsNo.Checked = !((bool)abstractRecord.TellerCards);
                }
                if (abstractRecord.AmblyopiaPresent != null)
                {
                    RadioButtonAmblyopiaPresentYes.Checked = ((bool)abstractRecord.AmblyopiaPresent);
                    RadioButtonAmblyopiaPresentNo.Checked = !((bool)abstractRecord.AmblyopiaPresent);
                }
                if (abstractRecord.StereoacuityValue != null) TextBoxStereoacuityResult.Text = abstractRecord.StereoacuityValue.ToString();
                if (abstractRecord.Outcome1StereoacuityUnableToComplete != null) CheckboxSensoryTestingPerformedOther.Checked = abstractRecord.Outcome1StereoacuityUnableToComplete.Value;
                if (abstractRecord.RBWorth4DotPerformed != null) RadioButtonListWorth4DotPerformed.SelectedValue = abstractRecord.RBWorth4DotPerformed.ToString();
                if (abstractRecord.RBWorth4DotDistanceDescription != null) DropDownListWorth4DotDistanceDescription.SelectedValue = abstractRecord.RBWorth4DotDistanceDescription.ToString();
                if (abstractRecord.Worth4DotDistanceUnableToComplete != null) CheckBoxWorth4DotDistanceUnableToComplete.Checked = abstractRecord.Worth4DotDistanceUnableToComplete.Value;
                if (abstractRecord.RBWorth4DotNearDescription != null) DropDownListWorth4DotNearDescription.SelectedValue = abstractRecord.RBWorth4DotNearDescription.ToString();
                if (abstractRecord.Worth4DotNearUnableToComplete != null) CheckBoxWorth4DotNearUnableToComplete.Checked = abstractRecord.Worth4DotNearUnableToComplete.Value;
                if (abstractRecord.AlignmentMeasured != null)
                {
                    RadioButtonAlignmentMeasuredYes.Checked = ((bool)abstractRecord.AlignmentMeasured);
                    RadioButtonAlignmentMeasuredNo.Checked = !((bool)abstractRecord.AlignmentMeasured);
                }
                if (abstractRecord.AlignmentDistancePDETValue != null) TextBoxAlignmentDistancePDET.Text = abstractRecord.AlignmentDistancePDETValue.ToString();


                if (abstractRecord.Outcome1AlignmentDistancePDHT != null) TextBoxOutcome1AlignmentDistancePDHT.Text = abstractRecord.Outcome1AlignmentDistancePDHT.ToString();


                if (abstractRecord.AlignmentDistancePDHT != null) TextBoxAlignmentDistancePDHT.Text = abstractRecord.AlignmentDistancePDHT.ToString();


                if (abstractRecord.AlignmentDistanceUnableToPerform != null) CheckBoxAlignmentDistanceUnableToPerform.Checked = ((bool)abstractRecord.AlignmentDistanceUnableToPerform);
                if (abstractRecord.AlignmentNearPDETValue != null) TextBoxAlignmentNearPDET.Text = abstractRecord.AlignmentNearPDETValue.ToString();
               
                if (abstractRecord.CyclopegicRetinoscopyPerformed != null)
                {
                    RadioButtonCyclopegicRetinoscopyPerformedYes.Checked = ((bool)abstractRecord.CyclopegicRetinoscopyPerformed);
                    RadioButtonCyclopegicRetinoscopyPerformedNo.Checked = !((bool)abstractRecord.CyclopegicRetinoscopyPerformed);
                }
                if (abstractRecord.CycloplegicRefractionODsph != null) TextBoxCycloplegicRefractionODsphSign.Text = abstractRecord.CycloplegicRefractionODsph.ToString();
                if (abstractRecord.CycloplegicRefractionODcyl != null) TextBoxCycloplegicRefractionODcylSign.Text = abstractRecord.CycloplegicRefractionODcyl.ToString();
                if (abstractRecord.CycloplegicRefractionODaxis != null) TextBoxCycloplegicRefractionODaxis.Text = abstractRecord.CycloplegicRefractionODaxis.ToString();
                if (abstractRecord.CycloplegicRefractionODsphSign != null)
                {
                    RadioButtonCycloplegicRefractionODsphPlus.Checked = ((bool)abstractRecord.CycloplegicRefractionODsphSign);
                    RadioButtonCycloplegicRefractionODsphMinus.Checked = !((bool)abstractRecord.CycloplegicRefractionODsphSign);
                }
                if (abstractRecord.CycloplegicRefractionODcylSign != null)
                {
                    RadioButtonCycloplegicRefractionODcylPlus.Checked = ((bool)abstractRecord.CycloplegicRefractionODcylSign);
                    RadioButtonCycloplegicRefractionODcylMinus.Checked = !((bool)abstractRecord.CycloplegicRefractionODcylSign);
                }

                if (abstractRecord.CycloplegicRefractionOSaxis != null) TextBoxCycloplegicRefractionOSaxis.Text = abstractRecord.CycloplegicRefractionOSaxis.ToString();
                if (abstractRecord.CycloplegicRefractionOSsphSign != null)
                {
                    RadioButtonCycloplegicRefractionOSsphPlus.Checked = ((bool)abstractRecord.CycloplegicRefractionOSsphSign);
                    RadioButtonCycloplegicRefractionOSsphMinus.Checked = !((bool)abstractRecord.CycloplegicRefractionOSsphSign);
                }
                if (abstractRecord.CycloplegicRefractionOscylSign != null)
                {
                    RadioButtonCycloplegicRefractionOScylPlus.Checked = ((bool)abstractRecord.CycloplegicRefractionOscylSign);
                    RadioButtonCycloplegicRefractionOScylMinus.Checked = !((bool)abstractRecord.CycloplegicRefractionOscylSign);
                }

                if (abstractRecord.RBDilatedFundusPerformed != null) RadioButtonListDiatedFundsExam.SelectedValue = abstractRecord.RBDilatedFundusPerformed.ToString();
                if (abstractRecord.NumberPriorMeasurement != null) TextBoxNumberPriorMeasurement.Text = abstractRecord.NumberPriorMeasurement.ToString();
                //////////////////////////////////////////////

                if (abstractRecord.Outcome2AlignmentDistancePDHT != null) TextBoxOutcome2AlignmentDistancePDHT.Text = abstractRecord.Outcome2AlignmentDistancePDHT.ToString();
                ////////////Management
                if (abstractRecord.AmblyopiaTherapy != null)
                {
                    RadioButtonAmblyopiaTherapyYes.Checked = ((bool)abstractRecord.AmblyopiaTherapy);
                    RadioButtonAmblyopiaTherapyNo.Checked = !((bool)abstractRecord.AmblyopiaTherapy);
                }
                if (abstractRecord.SurgeryRecessBilateralMedial != null)
                {
                    RadioButtonSurgeryRecessBilateralMedialYes.Checked = ((bool)abstractRecord.SurgeryRecessBilateralMedial);
                    RadioButtonSurgeryRecessBilateralMedialNo.Checked = !((bool)abstractRecord.SurgeryRecessBilateralMedial);
                }

                if (abstractRecord.SurgeryRecess1MedialResect1Lateral != null)
                {
                    RadioButtonSurgeryRecess1MedialResect1LateralYes.Checked = ((bool)abstractRecord.SurgeryRecess1MedialResect1Lateral);
                    RadioButtonSurgeryRecess1MedialResect1LateralNo.Checked = !((bool)abstractRecord.SurgeryRecess1MedialResect1Lateral);
                }
                if (abstractRecord.SurgeryRecess1Medial != null)
                {
                    RadioButtonSurgeryRecess1MedialYes.Checked = ((bool)abstractRecord.SurgeryRecess1Medial);
                    RadioButtonSurgeryRecess1MedialNo.Checked = !((bool)abstractRecord.SurgeryRecess1Medial);
                }
                if (abstractRecord.SurgeryRecess1Medial != null)
                {
                    RadioButtonSurgeryRecess1MedialYes.Checked = ((bool)abstractRecord.SurgeryRecess1Medial);
                    RadioButtonSurgeryRecess1MedialNo.Checked = !((bool)abstractRecord.SurgeryRecess1Medial);
                }
                if (abstractRecord.SurgeryRecess1Lateral != null)
                {
                    RadioButtonSurgeryRecess1LateralYes.Checked = ((bool)abstractRecord.SurgeryRecess1Lateral);
                    RadioButtonSurgeryRecess1LateralNo.Checked = !((bool)abstractRecord.SurgeryRecess1Lateral);
                }
                if (abstractRecord.SurgeryRecessBilateralLateral != null)
                {
                    RadioButtonSurgeryRecessBilateralLateralYes.Checked = ((bool)abstractRecord.SurgeryRecessBilateralLateral);
                    RadioButtonSurgeryRecessBilateralLateralNo.Checked = !((bool)abstractRecord.SurgeryRecessBilateralLateral);
                }
                //////////////////////////////////////////////////


                     ////////////////////////Outcomes
                    if (abstractRecord.Outcome1AlignmentDistancePDETValue != null) TextBoxOutcome1AlignmentDistancePDET.Text = abstractRecord.Outcome1AlignmentDistancePDETValue.ToString();
                 
          
                    if (abstractRecord.Outcome1AlignmentDistanceUnableToPerform != null) CheckBoxOutcome1AlignmentDistanceUnableToPerform.Checked = ((bool)abstractRecord.Outcome1AlignmentDistanceUnableToPerform);
                    if (abstractRecord.AlignmentNearUnableToPerform != null) CheckBoxAlignmentNearUnableToPerform.Checked = ((bool)abstractRecord.AlignmentNearUnableToPerform);

                    if (abstractRecord.Outcome1AlignmentNearPDETValue != null) TextBoxOutcome1AlignmentNearPDET.Text = abstractRecord.Outcome1AlignmentNearPDETValue.ToString();
                    if (abstractRecord.Outcome1AlignmentNearPDHT != null) TextBoxOutcome1AlignmentNearPDHT.Text = abstractRecord.Outcome1AlignmentNearPDHT.ToString();
                    if (abstractRecord.RBOutcome1AlignmentNearPDETType != null) RadioButtonListOutcome1AlignmentNearPDHT.SelectedValue = abstractRecord.RBOutcome1AlignmentNearPDETType.ToString();


                    if (abstractRecord.Outcome1StereoacuityValue != null) TextBoxOutcome1StereoacuityValue.Text = abstractRecord.Outcome1StereoacuityValue.ToString();
                    if (abstractRecord.Outcome1StereoacuityUnableToComplete != null) CheckBoxOutcome1MethodStereoacuityUnableToComplete.Checked = ((bool)abstractRecord.Outcome1StereoacuityUnableToComplete);


                    if (abstractRecord.RBOutcome1Worth4DotDistanceDescription != null) DropDownListtWorth4dotdistance1.SelectedValue = abstractRecord.RBOutcome1Worth4DotDistanceDescription.ToString();
                    if (abstractRecord.Outcome1Worth4DotDistanceUnableToComplete != null) CheckBoxOutcome1Worth4DotDistanceUnableToComplete.Checked = ((bool)abstractRecord.Outcome1Worth4DotDistanceUnableToComplete);

                    if (abstractRecord.RBOutcome1Worth4DotNearDescription != null) DropDownListWorth4dotnear1.SelectedValue = abstractRecord.RBOutcome1Worth4DotNearDescription.ToString();
                    if (abstractRecord.Outcome1Worth4DotNearUnableToComplete != null) CheckBoxOutcome1Worth4DotNearUnableToComplete.Checked = ((bool)abstractRecord.Outcome1Worth4DotNearUnableToComplete);
                                   //---2
                    if (abstractRecord.Outcome2AlignmentDistancePDETValue != null) TextBoxOutcome2AlignmentDistancePDET.Text = abstractRecord.Outcome2AlignmentDistancePDETValue.ToString();
  
                    if (abstractRecord.Outcome2AlignmentDistanceUnableToPerform != null) CheckBoxOutcome2AlignmentDistanceUnableToPerform.Checked = ((bool)abstractRecord.Outcome2AlignmentDistanceUnableToPerform);
                    if (abstractRecord.Outcome2AlignmentNearPDETValue != null) TextBoxOutcome2AlignmentNearPDET.Text = abstractRecord.Outcome2AlignmentNearPDETValue.ToString();
                    
                    if (abstractRecord.RBOutcome2AlignmentNearPDETType != null) RadioButtonListOutcome2AlignmentNearPDET.SelectedValue = abstractRecord.RBOutcome2AlignmentNearPDETType.ToString();
                    if (abstractRecord.Outcome2StereoacuityValue != null) TextBoxOutcome2StereoacuityValue.Text = abstractRecord.Outcome2StereoacuityValue.ToString();
                    if (abstractRecord.Outcome2StereoacuityUnableToComplete != null) CheckBoxOutcome2MethodStereoacuityUnableToComplete.Checked = ((bool)abstractRecord.Outcome2StereoacuityUnableToComplete);
                    if (abstractRecord.RBOutcome2Worth4DotDistanceDescription != null) DropDownListOutcome2Worth4DotDistance.SelectedValue = abstractRecord.RBOutcome2Worth4DotDistanceDescription.ToString();
                    if (abstractRecord.Outcome2Worth4DotDistanceUnableToComplete != null) CheckBoxOutcome2Worth4DotDistanceUnableToComplete.Checked = ((bool)abstractRecord.Outcome2Worth4DotDistanceUnableToComplete);
                    if (abstractRecord.RBOutcome2Worth4DotNearDescription != null) DropDownListOutcome2Worth4DotNear.SelectedValue = abstractRecord.RBOutcome2Worth4DotNearDescription.ToString();
                    if (abstractRecord.Outcome2Worth4DotNearUnableToComplete != null) CheckBoxOutcome2Worth4DotNearUnableToComplete.Checked = ((bool)abstractRecord.Outcome2Worth4DotNearUnableToComplete);
                

                    if (abstractRecord.CycloplegicRefractionOSsph != null) TextBoxCycloplegicRefractionOSsphSign.Text = abstractRecord.CycloplegicRefractionOSsph.ToString();
                    if (abstractRecord.CycloplegicRefractionOScyl != null) TextBoxCycloplegicRefractionOScylSign.Text = abstractRecord.CycloplegicRefractionOScyl.ToString();
                    if (abstractRecord.TextBoxOutcome2AlignmentNearPDHT != null) TextBoxOutcome2AlignmentNearPDHT.Text = abstractRecord.TextBoxOutcome2AlignmentNearPDHT.ToString();
                
                
                ////////////////////////////////



                    // ////////COMPLICATIONS
                    if (abstractRecord.PostoperativeInfection != null)
                    {
                        RadioButtonPostoperativeInfectionYes.Checked = ((bool)abstractRecord.PostoperativeInfection);
                        RadioButtonPostoperativeInfectionNo.Checked = !((bool)abstractRecord.PostoperativeInfection);
                    }

                    if (abstractRecord.PostoperativeConjunctivitis != null) CheckBoxPostoperativeConjunctivitis.Checked = ((bool)abstractRecord.PostoperativeConjunctivitis);
                    if (abstractRecord.PostoperativePreseptalCellulitis != null) CheckBoxPostoperativePreseptalCellulitis.Checked = ((bool)abstractRecord.PostoperativePreseptalCellulitis);
                    if (abstractRecord.PostoperativeOrbitalCellulitis != null) CheckBoxPostoperativeOrbitalCellulitis.Checked = ((bool)abstractRecord.PostoperativeOrbitalCellulitis);
                    if (abstractRecord.PostoperativeEndophthalmitis != null) CheckBoxPostoperativeEndophthalmitis.Checked = ((bool)abstractRecord.PostoperativeEndophthalmitis);
                    if (abstractRecord.PostoperativeOther != null) CheckBoxPostoperativeOther.Checked = ((bool)abstractRecord.PostoperativeOther);
                    if( abstractRecord.AlignmentDistanceOrthotropia!=null) RadioButtonAlignmentDistanceOrthotropia.Checked=((bool)abstractRecord.AlignmentDistanceOrthotropia);
                    if(abstractRecord.AlignmentNearOrthotropia!=null) RadioButtonAlignmentNearOrthotropia.Checked=((bool)abstractRecord.AlignmentNearOrthotropia);
                    if(abstractRecord.Outcome1AlignmentPDETDistanceOrthotropia!=null) RadioButtonOutcome1AlignmentPDETDistanceOrthotropia.Checked=((bool)abstractRecord.Outcome1AlignmentPDETDistanceOrthotropia);
                    if(abstractRecord.Outcome1AlignmentNearPDETOrthotropia!=null)RadioButtonOutcome1AlignmentNearPDETOrthotropia.Checked=((bool)abstractRecord.Outcome1AlignmentNearPDETOrthotropia);
                    if ( abstractRecord.Outcome2AlignmentDistancePDETOrthotropia!=null)RadioButtonOutcome2AlignmentDistancePDETOrthotropia.Checked=((bool)abstractRecord.Outcome2AlignmentDistancePDETOrthotropia);
                    if (abstractRecord.Outcome2AlignmentNearPDETOrthotropia != null) RadioButtonOutcome2AlignmentNearPDETOrthotropia.Checked = ((bool)abstractRecord.Outcome2AlignmentNearPDETOrthotropia);

                
                
                   if (abstractRecord.ComplicationReoperationWithin1Month != null)
                    {
                        RadioButtonComplicationReoperationWithin1MonthYes.Checked = ((bool)abstractRecord.ComplicationReoperationWithin1Month);
                        RadioButtonComplicationReoperationWithin1MonthNo.Checked = !((bool)abstractRecord.ComplicationReoperationWithin1Month);
                    }
                    if (abstractRecord.ComplicationEvidenceSlippedMuscle != null)
                    {
                        RadioButtonComplicationEvidenceSlippedMuscleYes.Checked = ((bool)abstractRecord.ComplicationEvidenceSlippedMuscle);
                        RadioButtonComplicationEvidenceSlippedMuscleNo.Checked = !((bool)abstractRecord.ComplicationEvidenceSlippedMuscle);
                    }
                    if (abstractRecord.ComplicationGlobePerforation != null)
                    {
                        RadioButtonComplicationGlobePerforationYes.Checked = ((bool)abstractRecord.ComplicationGlobePerforation);
                        RadioButtonComplicationGlobePerforationNo.Checked = !((bool)abstractRecord.ComplicationGlobePerforation);
                    }
                    if (abstractRecord.ComplicationOther != null) TextBoxComplicationOther.Text = abstractRecord.ComplicationOther.ToString();
               
                if (abstractRecord.TextBoxPDHT != null) TextBoxPDHT.Text = abstractRecord.TextBoxPDHT.ToString();
                /////////////////////////////
                }









            }
        
        }

   public void savedata()
   {
        using (NetHealthPIMEntities pim = new NetHealthPIMEntities())
        {
            int ModuleID = (int)Session[Constants.SESSION_WORKINGMODULEID];
            int cycleID = ctx.ActiveModuleCycleID;
            ParticipantModuleCycle cycle = pim.ParticipantModuleCycle.First(c => c.ParticipantModuleCycleID == cycleID);
            string recordIdentifier = HiddenFieldRecordIdentifier.Value;
            using (TransactionScope transaction = new TransactionScope())
            {
                ChartAbstractionEsotropia abstractRecord = (from c in pim.ChartAbstractionEsotropia
                                                            where c.ParticipantModuleCycle.ParticipantModuleCycleID == cycleID & c.RecordIdentifier == recordIdentifier
                                                           select c).First<ChartAbstractionEsotropia>();
                ////////PART1
                if (DropDownListMonthOfBirth.SelectedIndex > 0) abstractRecord.MonthOfBirth = Convert.ToInt32(DropDownListMonthOfBirth.SelectedValue);
                else abstractRecord.MonthOfBirth = 0;
                if (DropDownListYearOfBirth.SelectedIndex > 0) abstractRecord.YearOfBirth = Convert.ToInt32(DropDownListYearOfBirth.SelectedValue);
                else abstractRecord.YearOfBirth = 0;
                if (DropDownListMonthOfSurgery.SelectedIndex > 0) abstractRecord.MonthOfSurgery = Convert.ToInt32(DropDownListMonthOfSurgery.SelectedValue);
                else abstractRecord.MonthOfSurgery = 0;
                if (DropDownListYearOfSurgery.SelectedIndex > 0) abstractRecord.YearOfSurgery = Convert.ToInt32(DropDownListYearOfSurgery.SelectedValue);
                else abstractRecord.YearOfSurgery = 0;
                if (DropDownListMonthOfFollowUp1.SelectedIndex > 0) abstractRecord.MonthOfFollowUp1 = Convert.ToInt32(DropDownListMonthOfFollowUp1.SelectedValue);
                else abstractRecord.MonthOfFollowUp1 = 0;
                if (DropDownListYearOfFollowUp1.SelectedIndex > 0) abstractRecord.YearOfFollowUp1 = Convert.ToInt32(DropDownListYearOfFollowUp1.SelectedValue);
                else abstractRecord.YearOfFollowUp1 = 0;
                if (DropDownListMonthOfFollowUp2.SelectedIndex > 0) abstractRecord.MonthFollowUp2 = Convert.ToInt32(DropDownListMonthOfFollowUp2.SelectedValue);
                else abstractRecord.MonthFollowUp2 = 0;
                if (DropDownListYearOfFollowUp2.SelectedIndex > 0) abstractRecord.YearFollowUp2 = Convert.ToInt32(DropDownListYearOfFollowUp2.SelectedValue);
                else abstractRecord.YearFollowUp2 = 0;
                if (RadioButtonListStrabismus.SelectedIndex !=-1) abstractRecord.RBFamilyHistoryStrabismus = Convert.ToInt32(RadioButtonListStrabismus.SelectedValue);
                else abstractRecord.RBFamilyHistoryStrabismus = 0;
                if (RadioButtonListPermutality.SelectedIndex !=-1) abstractRecord.RBHistoryPrematurity = Convert.ToInt32(RadioButtonListPermutality.SelectedValue);
                else abstractRecord.RBHistoryPrematurity = 0;
                ///////////////////////////////////




                ///////////////////////////////PART2
                abstractRecord.AlignmentNearUnableToPerform=  CheckBoxAlignmentNearUnableToPerform.Checked ;
                if (RadioButtonVisualAcuityEvaluatedYes.Checked) abstractRecord.VisualAcuityEvaluated = true;
                if (RadioButtonVisualAcuityEvaluatedNo.Checked) abstractRecord.VisualAcuityEvaluated = false;
                if (DropDownListBestCorrectedVisualOD.SelectedIndex > 0) abstractRecord.BestCorrectedVisualOD = Convert.ToInt32(DropDownListBestCorrectedVisualOD.SelectedValue);
                else abstractRecord.BestCorrectedVisualOD = 0;
                if (DropDownListBestCorrectedVisualOS.SelectedIndex > 0) abstractRecord.BestCorrectedVisualOS = Convert.ToInt32(DropDownListBestCorrectedVisualOS.SelectedValue);
                else abstractRecord.BestCorrectedVisualOS = 0;
                if (RadioButtonOptotypeUsedYes.Checked) abstractRecord.OptotypeUsed = true;
                if (RadioButtonOptotypeUsedNo.Checked) abstractRecord.OptotypeUsed = false;
                if (DropDownListMethodOptotypesType.SelectedIndex > 0) abstractRecord.RBMethodOptotypesType = Convert.ToInt32(DropDownListMethodOptotypesType.SelectedValue);
                else abstractRecord.RBMethodOptotypesType = 0;
                if (RadioButtonFixationalVisionYes.Checked) abstractRecord.FixationalVision = true;
                if (RadioButtonFixationalVisionNo.Checked) abstractRecord.FixationalVision = false;
                if (RadioButtonTellerCardsYes.Checked) abstractRecord.TellerCards = true;
                if (RadioButtonTellerCardsNo.Checked) abstractRecord.TellerCards = false;
                if (RadioButtonAmblyopiaPresentYes.Checked) abstractRecord.AmblyopiaPresent = true;
                if (RadioButtonAmblyopiaPresentNo.Checked) abstractRecord.AmblyopiaPresent = false;
                



                try
                {
                    if (Double.Parse(TextBoxOutcome1AlignmentDistancePDHT.Text) >= 0)
                        abstractRecord.Outcome1AlignmentDistancePDHT = Convert.ToDouble(TextBoxOutcome1AlignmentDistancePDHT.Text);
                }
                catch (Exception ex)
                {
                    abstractRecord.Outcome1AlignmentDistancePDHT = null;
                    TextBoxOutcome1AlignmentDistancePDHT.Text = "";
                }



                try
                {
                    if (Double.Parse(TextBoxAlignmentDistancePDHT.Text) >= 0)
                        abstractRecord.AlignmentDistancePDHT = Convert.ToDouble(TextBoxAlignmentDistancePDHT.Text);
                }
                catch (Exception ex)
                {
                    abstractRecord.AlignmentDistancePDHT = null;
                    TextBoxAlignmentDistancePDHT.Text = "";
                }


                try
                {
                    if (Double.Parse(TextBoxStereoacuityResult.Text) >= 0)
                        abstractRecord.StereoacuityValue = Convert.ToDouble(TextBoxStereoacuityResult.Text);
                }
                catch (Exception ex)
                {
                    abstractRecord.StereoacuityValue = null;
                    TextBoxStereoacuityResult.Text = "";
                }
                abstractRecord.Outcome1StereoacuityUnableToComplete = CheckboxSensoryTestingPerformedOther.Checked;
                if (RadioButtonListWorth4DotPerformed.SelectedIndex !=-1) abstractRecord.RBWorth4DotPerformed = Convert.ToInt32(RadioButtonListWorth4DotPerformed.SelectedValue);
                else abstractRecord.RBWorth4DotPerformed = 0;
                if (DropDownListWorth4DotDistanceDescription.SelectedIndex != -1) abstractRecord.RBWorth4DotDistanceDescription = Convert.ToInt32(DropDownListWorth4DotDistanceDescription.SelectedValue);
                else abstractRecord.RBWorth4DotDistanceDescription = 0;
                abstractRecord.Worth4DotDistanceUnableToComplete = CheckBoxWorth4DotDistanceUnableToComplete.Checked;
                if (DropDownListWorth4DotNearDescription.SelectedIndex != -1) abstractRecord.RBWorth4DotNearDescription = Convert.ToInt32(DropDownListWorth4DotNearDescription.SelectedValue);
                else abstractRecord.RBWorth4DotNearDescription = 0;

                if (RadioButtonListPatientHasEsotropia.SelectedIndex != -1) abstractRecord.RBPatientHasEsotropia = Convert.ToInt32(RadioButtonListPatientHasEsotropia.SelectedValue);
                else abstractRecord.RBPatientHasEsotropia = 0;
                abstractRecord.AlignmentDistanceOrthotropia = RadioButtonAlignmentDistanceOrthotropia.Checked;
                abstractRecord.AlignmentNearOrthotropia = RadioButtonAlignmentNearOrthotropia.Checked;
                abstractRecord.Outcome1AlignmentPDETDistanceOrthotropia = RadioButtonOutcome1AlignmentPDETDistanceOrthotropia.Checked;
                abstractRecord.Outcome1AlignmentNearPDETOrthotropia =  RadioButtonOutcome1AlignmentNearPDETOrthotropia.Checked;
               abstractRecord.Outcome2AlignmentDistancePDETOrthotropia = RadioButtonOutcome2AlignmentDistancePDETOrthotropia.Checked;
               abstractRecord.Outcome2AlignmentNearPDETOrthotropia = RadioButtonOutcome2AlignmentNearPDETOrthotropia.Checked;

                abstractRecord.Outcome1StereoacuityUnableToComplete = CheckBoxOutcome1MethodStereoacuityUnableToComplete.Checked;
                abstractRecord.Worth4DotNearUnableToComplete = CheckBoxWorth4DotNearUnableToComplete.Checked;
                if (RadioButtonAlignmentMeasuredYes.Checked) abstractRecord.AlignmentMeasured = true;
                if (RadioButtonAlignmentMeasuredNo.Checked) abstractRecord.AlignmentMeasured = false;
                try
                {
                    if (Double.Parse(TextBoxAlignmentDistancePDET.Text) >= 0)
                        abstractRecord.AlignmentDistancePDETValue = Convert.ToDouble(TextBoxAlignmentDistancePDET.Text);
                }
                catch (Exception ex)
                {
                    abstractRecord.AlignmentDistancePDETValue = null;
                    TextBoxAlignmentDistancePDET.Text = "";
                }
                
                abstractRecord.AlignmentDistanceUnableToPerform = CheckBoxAlignmentDistanceUnableToPerform.Checked;
                try
                {
                    if (Double.Parse(TextBoxAlignmentNearPDET.Text) >= 0)
                        abstractRecord.AlignmentNearPDETValue = Convert.ToDouble(TextBoxAlignmentDistancePDET.Text);
                }
                catch (Exception ex)
                {
                    abstractRecord.AlignmentNearPDETValue = null;
                    TextBoxAlignmentNearPDET.Text = "";
                }
                 
                try
                {
                    if (Double.Parse(TextBoxOutcome2AlignmentDistancePDHT.Text) >= 0)
                        abstractRecord.Outcome2AlignmentDistancePDHT= Convert.ToDouble(TextBoxOutcome2AlignmentDistancePDHT.Text);
                }
                catch (Exception ex)
                {
                    abstractRecord.Outcome2AlignmentDistancePDHT = null;
                    TextBoxOutcome2AlignmentDistancePDHT.Text = "";
                }
                try
                {
                    if (Double.Parse(TextBoxPDHT.Text) >= 0)
                        abstractRecord.TextBoxPDHT = Convert.ToDouble(TextBoxPDHT.Text);
                }
                catch (Exception ex)
                {
                    abstractRecord.TextBoxPDHT = null;
                    TextBoxPDHT.Text = "";
                }
                

                    try
                {
                    if (TextBoxOutcome2AlignmentNearPDHT.Text != "")
                        abstractRecord.TextBoxOutcome2AlignmentNearPDHT = TextBoxOutcome2AlignmentNearPDHT.Text;
                }
                catch (Exception ex)
                {
                    abstractRecord.TextBoxOutcome2AlignmentNearPDHT = null;
                    TextBoxOutcome2AlignmentNearPDHT.Text = "";
                }



               
                if (RadioButtonCyclopegicRetinoscopyPerformedYes.Checked) abstractRecord.CyclopegicRetinoscopyPerformed = true;
                if (RadioButtonCyclopegicRetinoscopyPerformedNo.Checked) abstractRecord.CyclopegicRetinoscopyPerformed = false;
                try
                {
                    if (Double.Parse(TextBoxCycloplegicRefractionODsphSign.Text) >= 0)
                        abstractRecord.CycloplegicRefractionODsph =Convert.ToDouble(TextBoxCycloplegicRefractionODsphSign.Text);
                }
                catch (Exception ex)
                {
                    abstractRecord.CycloplegicRefractionODsph = null;
                    TextBoxCycloplegicRefractionODsphSign.Text = "";
                }
                try
                {
                    if (Double.Parse(TextBoxCycloplegicRefractionODcylSign.Text) >= 0)
                        abstractRecord.CycloplegicRefractionODcyl = Convert.ToDouble(TextBoxCycloplegicRefractionODcylSign.Text);
                }
                catch (Exception ex)
                {
                    abstractRecord.CycloplegicRefractionODcyl = null;
                    TextBoxCycloplegicRefractionODcylSign.Text = "";
                }
                
                try
                {
                    if (Double.Parse(TextBoxOutcome1StereoacuityValue.Text) >= 0)
                        abstractRecord.Outcome1StereoacuityValue = Convert.ToDouble(TextBoxOutcome1StereoacuityValue.Text);
                }
                catch (Exception ex)
                {
                    abstractRecord.Outcome1StereoacuityValue = null;
                    TextBoxOutcome1StereoacuityValue.Text = "";
                }
                try
                {
                    if (Double.Parse(TextBoxCycloplegicRefractionODaxis.Text) >= 0)
                        abstractRecord.CycloplegicRefractionODaxis = Convert.ToDouble(TextBoxCycloplegicRefractionODaxis.Text);
                }
                catch (Exception ex)
                {
                    abstractRecord.CycloplegicRefractionODaxis = null;
                    TextBoxCycloplegicRefractionODaxis.Text = "";
                }
                if (RadioButtonCycloplegicRefractionODsphPlus.Checked) abstractRecord.CycloplegicRefractionODsphSign = true;
                if (RadioButtonCycloplegicRefractionODsphMinus.Checked) abstractRecord.CycloplegicRefractionODsphSign = false;

                if (RadioButtonCycloplegicRefractionODcylPlus.Checked) abstractRecord.CycloplegicRefractionODcylSign = true;
                if (RadioButtonCycloplegicRefractionODcylMinus.Checked) abstractRecord.CycloplegicRefractionODcylSign = false;


                if (RadioButtonCycloplegicRefractionODcylPlus.Checked) abstractRecord.CycloplegicRefractionODcylSign = true;
                if (RadioButtonCycloplegicRefractionODcylMinus.Checked) abstractRecord.CycloplegicRefractionODcylSign = false;
                try
                {
                    if (Double.Parse(TextBoxCycloplegicRefractionOSaxis.Text) >= 0)
                        abstractRecord.CycloplegicRefractionOSaxis = Convert.ToDouble(TextBoxCycloplegicRefractionOSaxis.Text);
                }
                catch (Exception ex)
                {
                    abstractRecord.CycloplegicRefractionOSaxis = null;
                    TextBoxCycloplegicRefractionOSaxis.Text = "";
                }
                if (RadioButtonCycloplegicRefractionOSsphPlus.Checked) abstractRecord.CycloplegicRefractionOSsphSign = true;
                if (RadioButtonCycloplegicRefractionOSsphMinus.Checked) abstractRecord.CycloplegicRefractionOSsphSign = false;

                if (RadioButtonCycloplegicRefractionOScylPlus.Checked) abstractRecord.CycloplegicRefractionOscylSign = true;
                if (RadioButtonCycloplegicRefractionOScylMinus.Checked) abstractRecord.CycloplegicRefractionOscylSign = false;
                if (RadioButtonListDiatedFundsExam.SelectedIndex  !=-1) abstractRecord.RBDilatedFundusPerformed = Convert.ToInt32(RadioButtonListDiatedFundsExam.SelectedValue);
                try
                {
                    if (Double.Parse(TextBoxNumberPriorMeasurement.Text) >= 0)
                        abstractRecord.NumberPriorMeasurement = Convert.ToDouble(TextBoxNumberPriorMeasurement.Text);
                }
                catch (Exception ex)
                {
                    abstractRecord.NumberPriorMeasurement = null;
                    TextBoxNumberPriorMeasurement.Text = "";
                }
                //////////////////////////////////////////


                //////////////////////////////////////PART3
                if (RadioButtonAmblyopiaTherapyYes.Checked) abstractRecord.AmblyopiaTherapy = true;
                if (RadioButtonAmblyopiaTherapyNo.Checked) abstractRecord.AmblyopiaTherapy = false;

                if (RadioButtonSurgeryRecessBilateralMedialYes.Checked) abstractRecord.SurgeryRecessBilateralMedial = true;
                if (RadioButtonSurgeryRecessBilateralMedialNo.Checked) abstractRecord.SurgeryRecessBilateralMedial = false;

                if (RadioButtonSurgeryRecess1MedialResect1LateralYes.Checked) abstractRecord.SurgeryRecess1MedialResect1Lateral = true;
                if (RadioButtonSurgeryRecess1MedialResect1LateralNo.Checked) abstractRecord.SurgeryRecess1MedialResect1Lateral = false;

                if (RadioButtonSurgeryRecess1MedialYes.Checked) abstractRecord.SurgeryRecess1Medial = true;
                if (RadioButtonSurgeryRecess1MedialNo.Checked) abstractRecord.SurgeryRecess1Medial = false;


          

                if (RadioButtonSurgeryRecess1LateralYes.Checked) abstractRecord.SurgeryRecess1Lateral = true;
                if ( RadioButtonSurgeryRecess1LateralNo.Checked) abstractRecord.SurgeryRecess1Lateral = false;


                if (RadioButtonSurgeryRecessBilateralLateralYes.Checked) abstractRecord.SurgeryRecessBilateralLateral = true;
                if (RadioButtonSurgeryRecessBilateralLateralNo.Checked) abstractRecord.SurgeryRecessBilateralLateral = false;

                ////////////////////////////////////////////////


                /////////////////////////////////PART4

                try
                {
                    if (Double.Parse(TextBoxOutcome1AlignmentDistancePDET.Text) >= 0)
                        abstractRecord.Outcome1AlignmentDistancePDETValue = Convert.ToDouble(TextBoxOutcome1AlignmentDistancePDET.Text);
                }
                catch (Exception ex)
                {
                    abstractRecord.Outcome1AlignmentDistancePDETValue = null;
                    TextBoxOutcome1AlignmentDistancePDET.Text = "";
                }
               
             
                 abstractRecord.Outcome1AlignmentDistanceUnableToPerform= CheckBoxOutcome1AlignmentDistanceUnableToPerform.Checked;
                 try
                 {
                     if (Double.Parse(TextBoxOutcome1AlignmentNearPDET.Text) != -1)
                        abstractRecord.Outcome1AlignmentNearPDETValue = Convert.ToDouble(TextBoxOutcome1AlignmentNearPDET.Text);
                 }
                 catch (Exception ex)
                 {
                     abstractRecord.Outcome1AlignmentNearPDETValue = null;
                     TextBoxOutcome1AlignmentNearPDET.Text = "";
                 }
                 try
                 {
                     if (Double.Parse(TextBoxOutcome1AlignmentNearPDHT.Text) != -1)
                         abstractRecord.Outcome1AlignmentNearPDHT= Convert.ToDouble(TextBoxOutcome1AlignmentNearPDHT.Text);
                 }
                 catch (Exception ex)
                 {
                     abstractRecord.Outcome1AlignmentNearPDHT= null;
                     TextBoxOutcome1AlignmentNearPDHT.Text = "";
                 }

                 try
                 {
                     if (Double.Parse(TextBoxCycloplegicRefractionOSsphSign.Text) != -1)
                         abstractRecord.CycloplegicRefractionOSsph = Convert.ToDouble(TextBoxCycloplegicRefractionOSsphSign.Text);
                 }
                 catch (Exception ex)
                 {
                     abstractRecord.CycloplegicRefractionOSsph = null;
                     TextBoxCycloplegicRefractionOSsphSign.Text = "";
                 }

                 try
                 {
                     if (Double.Parse(TextBoxCycloplegicRefractionOScylSign.Text) != -1)
                         abstractRecord.CycloplegicRefractionOScyl = Convert.ToDouble(TextBoxCycloplegicRefractionOScylSign.Text);
                 }
                 catch (Exception ex)
                 {
                     abstractRecord.CycloplegicRefractionOScyl = null;
                     TextBoxCycloplegicRefractionOScylSign.Text = "";
                 }



                 if (RadioButtonListOutcome1AlignmentNearPDHT.SelectedIndex != -1) abstractRecord.RBOutcome1AlignmentNearPDETType = Convert.ToInt32(RadioButtonListOutcome1AlignmentNearPDHT.SelectedValue);

                 if (DropDownListtWorth4dotdistance1.SelectedIndex != -1) abstractRecord.RBOutcome1Worth4DotDistanceDescription = Convert.ToInt32(DropDownListtWorth4dotdistance1.SelectedValue);
                 abstractRecord.Outcome1Worth4DotDistanceUnableToComplete= CheckBoxOutcome1Worth4DotDistanceUnableToComplete.Checked;
                 if (DropDownListWorth4dotnear1.SelectedIndex != -1) abstractRecord.RBOutcome1Worth4DotNearDescription = Convert.ToInt32(DropDownListWorth4dotnear1.SelectedValue);
                 abstractRecord.Outcome1Worth4DotNearUnableToComplete=CheckBoxOutcome1Worth4DotNearUnableToComplete.Checked;
                 try
                 {
                     if (Double.Parse(TextBoxOutcome2AlignmentDistancePDET.Text) >= 0)
                         abstractRecord.Outcome2AlignmentDistancePDETValue = Convert.ToDouble(TextBoxOutcome2AlignmentDistancePDET.Text);
                 }
                 catch (Exception ex)
                 {
                     abstractRecord.Outcome2AlignmentDistancePDETValue = null;
                     TextBoxOutcome2AlignmentDistancePDET.Text = "";
                 }
                 
                 
                 abstractRecord.Outcome2AlignmentDistanceUnableToPerform = CheckBoxOutcome2AlignmentDistanceUnableToPerform.Checked;
                 try
                 {
                     if (Double.Parse(TextBoxOutcome2AlignmentNearPDET.Text) >= 0)
                         abstractRecord.Outcome2AlignmentNearPDETValue = Convert.ToDouble(TextBoxOutcome2AlignmentNearPDET.Text);
                 }
                 catch (Exception ex)
                 {
                     abstractRecord.Outcome2AlignmentNearPDETValue = null;
                     TextBoxOutcome2AlignmentNearPDET.Text = "";
                 }
                

                 if (RadioButtonListOutcome2AlignmentNearPDET.SelectedIndex != -1) abstractRecord.RBOutcome2AlignmentNearPDETType = Convert.ToInt32(RadioButtonListOutcome2AlignmentNearPDET.SelectedValue);
                 try
                 {
                     if (Double.Parse(TextBoxOutcome2StereoacuityValue.Text) >= 0)
                         abstractRecord.Outcome2StereoacuityValue = Convert.ToDouble(TextBoxOutcome2StereoacuityValue.Text);
                 }
                 catch (Exception ex)
                 {
                     abstractRecord.Outcome2StereoacuityValue = null;
                     TextBoxOutcome2StereoacuityValue.Text = "";
                 }
                 abstractRecord.Outcome2StereoacuityUnableToComplete = CheckBoxOutcome2MethodStereoacuityUnableToComplete.Checked;
                 if (DropDownListOutcome2Worth4DotDistance.SelectedIndex != -1) abstractRecord.RBOutcome2Worth4DotDistanceDescription = Convert.ToInt32(DropDownListOutcome2Worth4DotDistance.SelectedValue);
                 abstractRecord.Outcome2Worth4DotDistanceUnableToComplete = CheckBoxOutcome2Worth4DotDistanceUnableToComplete.Checked;
                 if (DropDownListOutcome2Worth4DotNear.SelectedIndex != -1) abstractRecord.RBOutcome2Worth4DotNearDescription = Convert.ToInt32(DropDownListOutcome2Worth4DotNear.SelectedValue);
                 abstractRecord.Outcome2Worth4DotNearUnableToComplete = CheckBoxOutcome2Worth4DotNearUnableToComplete.Checked;
                ////////////////////////////////////////////////   
                 


                /////////////////////////////////////////////PART5
                 if (RadioButtonPostoperativeInfectionYes.Checked) abstractRecord.PostoperativeInfection = true;
                 if (RadioButtonPostoperativeInfectionNo.Checked) abstractRecord.PostoperativeInfection = false;

                abstractRecord.PostoperativeConjunctivitis= CheckBoxPostoperativeConjunctivitis.Checked;
                abstractRecord.PostoperativePreseptalCellulitis = CheckBoxPostoperativePreseptalCellulitis.Checked;
                abstractRecord.PostoperativeOrbitalCellulitis= CheckBoxPostoperativeOrbitalCellulitis.Checked;
                abstractRecord.PostoperativeEndophthalmitis = CheckBoxPostoperativeEndophthalmitis.Checked;
                abstractRecord.PostoperativeOther = CheckBoxPostoperativeOther.Checked;
                if (RadioButtonComplicationReoperationWithin1MonthYes.Checked) abstractRecord.ComplicationReoperationWithin1Month = true;
                if (RadioButtonComplicationReoperationWithin1MonthNo.Checked) abstractRecord.ComplicationReoperationWithin1Month = false;

                if (RadioButtonComplicationEvidenceSlippedMuscleYes.Checked) abstractRecord.ComplicationEvidenceSlippedMuscle = true;
                if (RadioButtonComplicationEvidenceSlippedMuscleNo.Checked) abstractRecord.ComplicationEvidenceSlippedMuscle = false;

                if (RadioButtonComplicationGlobePerforationYes.Checked) abstractRecord.ComplicationGlobePerforation = true;
                if (RadioButtonComplicationGlobePerforationNo.Checked) abstractRecord.ComplicationGlobePerforation = false;
                try
                {
                    if (TextBoxComplicationOther.Text != "")
                        abstractRecord.ComplicationOther = TextBoxComplicationOther.Text;
                }
                catch (Exception ex)
                {
                    abstractRecord.ComplicationOther = null;
                    TextBoxComplicationOther.Text= "";
                }
                abstractRecord.LastUpdateDate = DateTime.Now;
                bool ChartCompleted = isComplete();
                abstractRecord.Complete = ChartCompleted;
                var chartRegistration = (from cr in pim.ChartRegistration
                                         where cr.ParticipantModuleCycleID == cycle.ParticipantModuleCycleID
                                          & cr.ModuleID == ModuleID
                                          & cr.RecordIdentifier == recordIdentifier
                                         select cr).First<NetHealthPIMModel.ChartRegistration>();
                chartRegistration.AbstractCompleted = ChartCompleted;


                if ((DropDownListYearOfBirth.SelectedIndex > 0) && (DropDownListYearOfBirth.SelectedIndex > 0))
                {
                    //chartRegistration.DOB = abstractRecord.MonthOfBirth + "/" + abstractRecord.YearOfBirth;
                }
                if ((DropDownListMonthOfFollowUp1.SelectedIndex > 0) && (DropDownListYearOfFollowUp1.SelectedIndex > 0))
                {
                    //chartRegistration.InitialVisit = abstractRecord.MonthOfFollowUp1 + "/" + abstractRecord.YearOfFollowUp1;
                }
                pim.SaveChanges();

                transaction.Complete();




            }

            CycleManager.UpdateParticipantCompletedModules(cycleID, ModuleID);
        }


   }
 protected void ButtonSubmit_Click(object sender, EventArgs e)
    {
        savedata();
        isComplete();
        if (!isComplete())
        {
            isComplete();
            string script = " var answer = confirm('Chart data is not complete.\\nClick OK to save entries and exit chart.\\nClick CANCEL to save entries and review the chart. '); if (answer==true) window.location = '../PatientChartRegistration.aspx';";
            ScriptManager.RegisterStartupScript(this, GetType(),
                          "ServerControlScript", script, true);
        }
        else
        {

            Response.Redirect("../PatientChartRegistration.aspx");
        }
      
}

    protected bool isComplete()
    {
        bool ChartCompleted = true;





        LabelPatientHasEsotropia.Visible = false;

        LabelNumberPriorMeasurement1.Visible = false;
        LabelMonthOfBirth.Visible = false;


        LabelMonthOfSurgery.Visible = false;

        LabelYearOfSurgery.Visible = false;


        LabelMonthOfFollowUp1.Visible = false;

        LabelYearOfFollowUp1.Visible = false;


        LabelYearOfFollowUp2.Visible = false;

        LabelStrabismus.Visible = false;


        LabelPermutality.Visible = false;



        LabelVisualAcuityEvaluated.Visible = false;

        LabelAmblyopiaPresent.Visible = false;

        LabelStereoacuityResult.Visible = false;

        // LabelDiatedFundsExam.Visible = false;

        LabelNumberPriorMeasurement.Visible = false;

        LabelBestCorrectedVisualOD.Visible = false;

        LabelBestCorrectedVisualOS.Visible = false;

        LabelOptotypeUsed.Visible = false;

        LabelComplicationGlobePerforation.Visible = false;
        LabelWorth4DotDistanceDescription.Visible = false;

        LabelWorth4DotNearDescription.Visible = false;

        LabelAlignmentDistance.Visible = false;

        LabelAlignmentNear.Visible = false;


        LabelAmblyopiaTherapy.Visible = false;
        LabelSurgeryRecessBilateralMedial.Visible = false;

        LabelComplicationReoperationWithin1Month.Visible = false;

        Labelutcome1AlignmentDistancePDET.Visible = false;

        LabelOutcome1AlignmentNearPDHT.Visible = false;

        LabelOutcome1StereoacuityValue.Visible = false;

        LabelWorth4dotdistance1.Visible = false;

        LabelWorth4dotnear1.Visible = false;

        LabelOutcome2AlignmentNearPDHT.Visible = false;

        LabelOutcome2Worth4DotDistance.Visible = false;
        LabelPostoperativeInfection.Visible = false;

        LabelPostoperativeInfection.Visible = false;
        LabelOutcome2StereoacuityValue.Visible = false;
        Labelinitialage.Visible = false;
        LabelCyclopegicRetinoscopyPerformed.Visible = false;

        LabelOutcome2Worth4DotNear.Visible = false;
        NetHealthPIMEntities pim = new NetHealthPIMEntities();

        var  moduleinfo = (from c in pim.Module where c.ModuleID == 50 select c.RegistrationFollowUpVisitMonths).FirstOrDefault();
        
        string message2 = "Follow up visit must be at least " + moduleinfo + " months after initial visit<br />";
        if (DropDownListYearOfSurgery.SelectedIndex != 0 && DropDownListMonthOfSurgery.SelectedIndex != 0 && DropDownListYearOfFollowUp2.SelectedIndex != 0 && DropDownListMonthOfFollowUp2.SelectedIndex != 0)
        {
            DateTime monthofsurgery = new DateTime(Convert.ToInt32(DropDownListYearOfSurgery.SelectedValue), Convert.ToInt32(DropDownListMonthOfSurgery.SelectedValue), 01);
            DateTime folowup = new DateTime(Convert.ToInt32(DropDownListYearOfFollowUp2.SelectedValue), Convert.ToInt32(DropDownListMonthOfFollowUp2.SelectedValue), 01);
            string registrationminage2 = Validation.validateRegistrationFolowUpMonth(monthofsurgery, folowup, Convert.ToInt32(moduleinfo), message2);
            if (registrationminage2 != "")
            {
                Labelinitialage.Text = message2;
                Labelinitialage.Visible = true;
                ChartCompleted = false;
            }
        }

if((RadioButtonSurgeryRecessBilateralLateralNo.Checked==false && RadioButtonSurgeryRecessBilateralLateralYes.Checked==false)||
(RadioButtonSurgeryRecess1LateralYes.Checked==false && RadioButtonSurgeryRecess1LateralNo.Checked==false)||
(RadioButtonSurgeryRecess1MedialYes.Checked==false && RadioButtonSurgeryRecess1MedialNo.Checked==false) || 
(RadioButtonSurgeryRecessBilateralMedialYes.Checked==false && RadioButtonSurgeryRecessBilateralMedialNo.Checked==false)||
(RadioButtonSurgeryRecess1MedialResect1LateralYes.Checked==false && RadioButtonSurgeryRecess1MedialResect1LateralNo.Checked==false))
{

    LabelRecess1MedialResect1Lateral.Visible=true;
  
     ChartCompleted = false;


}
if (RadioButtonComplicationGlobePerforationYes.Checked == false && RadioButtonComplicationGlobePerforationNo.Checked == false)
{
    LabelComplicationGlobePerforation.Visible = true;
    ChartCompleted = false;



}
if (RadioButtonCyclopegicRetinoscopyPerformedYes.Checked == false && RadioButtonCyclopegicRetinoscopyPerformedNo.Checked == false)
{

    LabelCyclopegicRetinoscopyPerformed.Visible = true;
    ChartCompleted = false;

}
if (TextBoxOutcome2StereoacuityValue.Text == "" && CheckBoxOutcome2MethodStereoacuityUnableToComplete.Checked == false)
{

    LabelOutcome2StereoacuityValue.Visible = true;
    ChartCompleted = false;


}

        if (RadioButtonListPatientHasEsotropia.SelectedIndex == -1)
        {
            ChartCompleted = false;
            LabelPatientHasEsotropia.Visible = true;
        }
        if (DropDownListMonthOfBirth.SelectedIndex == 0)
        {
            ChartCompleted = false;
            LabelMonthOfBirth.Visible = true;

        }
        if (DropDownListMonthOfSurgery.SelectedIndex == 0)
        {
            ChartCompleted = false;
            LabelMonthOfSurgery.Visible = true;

        }
        if (DropDownListYearOfSurgery.SelectedIndex == 0)
        {
            ChartCompleted = false;
            LabelYearOfSurgery.Visible = true;

        }
        if (DropDownListMonthOfFollowUp1.SelectedIndex == 0)
        {
            ChartCompleted = false;
            LabelMonthOfFollowUp1.Visible = true;

        }
        if (DropDownListYearOfFollowUp1.SelectedIndex == 0)
        {
            ChartCompleted = false;
            LabelYearOfFollowUp1.Visible = true;

        }

        if (DropDownListYearOfFollowUp2.SelectedIndex == 0)
        {

            ChartCompleted = false;
            LabelYearOfFollowUp2.Visible = true;

        }
        if (RadioButtonListStrabismus.SelectedIndex == -1)
        {

            ChartCompleted = false;
            LabelStrabismus.Visible = true;

        }
        if (RadioButtonListPermutality.SelectedIndex == -1)
        {
            ChartCompleted = false;
            LabelPermutality.Visible = true;


        }

        if (RadioButtonVisualAcuityEvaluatedYes.Checked == false && RadioButtonVisualAcuityEvaluatedNo.Checked == false)
        {
            LabelVisualAcuityEvaluated.Visible = true;
            ChartCompleted = false;
        }
        if (RadioButtonAmblyopiaPresentYes.Checked == false && RadioButtonAmblyopiaPresentNo.Checked == false)
        {
            LabelAmblyopiaPresent.Visible = true;
            ChartCompleted = false;
        }
        if (TextBoxStereoacuityResult.Text == "" && CheckboxSensoryTestingPerformedOther.Checked == false)
        {
            LabelStereoacuityResult.Visible = true;
            ChartCompleted = false;
        }
        if (RadioButtonListDiatedFundsExam.SelectedIndex == -1)
        {
           // LabelDiatedFundsExam.Visible = true;
           // ChartCompleted = false;
        }
        if (TextBoxNumberPriorMeasurement.Text == "")
        {
            LabelNumberPriorMeasurement.Visible = true;
            ChartCompleted = false;
        }
        if (RadioButtonVisualAcuityEvaluatedYes.Checked == true)
        {
            if (DropDownListBestCorrectedVisualOD.SelectedIndex == 0)
            {
                LabelBestCorrectedVisualOD.Visible = true;
                ChartCompleted = false;
            }
            if (DropDownListBestCorrectedVisualOS.SelectedIndex == 0)
            {
                LabelBestCorrectedVisualOS.Visible = true;
                ChartCompleted = false;

            }
            if (RadioButtonOptotypeUsedYes.Checked == false && RadioButtonOptotypeUsedNo.Checked == false)
            {
                LabelOptotypeUsed.Visible = true;
                ChartCompleted = false;
            }
        }
        if (RadioButtonListWorth4DotPerformed.SelectedValue == "1")
        {

            if (DropDownListWorth4DotDistanceDescription.SelectedIndex == 0 && CheckBoxWorth4DotDistanceUnableToComplete.Checked == false)
            {
                LabelWorth4DotDistanceDescription.Visible = true;
                ChartCompleted = false;
            }
            if (DropDownListWorth4DotNearDescription.SelectedIndex == 0 && CheckBoxWorth4DotNearUnableToComplete.Checked == false)
            {
                LabelWorth4DotNearDescription.Visible = true;
                ChartCompleted = false;

            }

        }
       

        if (RadioButtonAmblyopiaTherapyYes.Checked == false && RadioButtonAmblyopiaTherapyNo.Checked == false)
        {
            LabelAmblyopiaTherapy.Visible = true;
            ChartCompleted = false;


        }
        if (RadioButtonSurgeryRecessBilateralMedialYes.Checked == false && RadioButtonSurgeryRecessBilateralMedialNo.Checked == false)
        {
            LabelSurgeryRecessBilateralMedial.Visible = true;
            ChartCompleted = false;


        }
        if (TextBoxNumberPriorMeasurement.Text == "")
        {
            LabelNumberPriorMeasurement1.Visible = true;
            ChartCompleted = false;
        
        
        }



        if (TextBoxOutcome1AlignmentNearPDET.Text == "" && TextBoxOutcome1AlignmentNearPDHT.Text == "" && RadioButtonListOutcome1AlignmentNearPDHT.SelectedIndex == -1 && RadioButtonOutcome1AlignmentNearPDETOrthotropia.Checked==false)
        {
            LabelOutcome1AlignmentNearPDHT.Visible = true;
            ChartCompleted = false;

        }
        if (TextBoxOutcome1StereoacuityValue.Text == "" && CheckBoxOutcome1MethodStereoacuityUnableToComplete.Checked == false)
        {
            LabelOutcome1StereoacuityValue.Visible = true;
            ChartCompleted = false;
        }
        if (DropDownListtWorth4dotdistance1.SelectedIndex == 0 && CheckBoxOutcome1Worth4DotDistanceUnableToComplete.Checked == false)
        {
            LabelWorth4dotdistance1.Visible = true;
            ChartCompleted = false;


        }
        if (DropDownListWorth4dotnear1.SelectedIndex == 0 && CheckBoxOutcome1Worth4DotNearUnableToComplete.Checked == false)
        {
            LabelWorth4dotnear1.Visible = true;
            ChartCompleted = false;


        }
        if (RadioButtonComplicationReoperationWithin1MonthYes.Checked == false && RadioButtonComplicationReoperationWithin1MonthNo.Checked == false)
        {

            LabelComplicationReoperationWithin1Month.Visible = true;
            ChartCompleted = false;
        
        
        }



         if (TextBoxOutcome2AlignmentNearPDET.Text == "" && CheckBoxOutcome2AlignmentDistanceUnableToPerform.Checked == false && RadioButtonOutcome2AlignmentNearPDETOrthotropia.Checked==false)
        {
            LabelOutcome2AlignmentNearPDHT.Visible = true;
            ChartCompleted = false;

        }
        if (TextBoxOutcome2StereoacuityValue.Text == "" && CheckBoxOutcome2MethodStereoacuityUnableToComplete.Checked == false)
        {
            ChartCompleted = false;
        }
        if (DropDownListOutcome2Worth4DotDistance.SelectedIndex == 0 && CheckBoxOutcome2Worth4DotDistanceUnableToComplete.Checked == false)
        {
            LabelOutcome2Worth4DotNear.Visible = true;
            ChartCompleted = false;
        
        }


        if (DropDownListOutcome2Worth4DotNear.SelectedIndex == 0 && CheckBoxOutcome2Worth4DotNearUnableToComplete.Checked == false)
        {
            LabelOutcome2Worth4DotDistance.Visible = true;
            ChartCompleted = false;


        }
        if (RadioButtonPostoperativeInfectionNo.Checked == false && RadioButtonPostoperativeInfectionYes.Checked == false)
        {
            if (RadioButtonPostoperativeInfectionYes.Checked == true)
            {
                if (CheckBoxPostoperativeConjunctivitis.Checked == false && CheckBoxPostoperativePreseptalCellulitis.Checked == false && CheckBoxPostoperativeOrbitalCellulitis.Checked == false && CheckBoxPostoperativeEndophthalmitis.Checked == false && CheckBoxPostoperativeOther.Checked == false)
                {

                    ChartCompleted = false;
                    LabelPostoperativeInfection.Visible = true;
                }


            }
            ChartCompleted = false;
            LabelPostoperativeInfection.Visible = true;

        }

        return ChartCompleted;
    }



}

