﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Transactions;
using NetHealthPIMModel;

public partial class abo_IKChart : BasePage
{
    protected void Page_Load(object sender, EventArgs e)
    {
        if (!IsPostBack)
        {
            InitializePage();
        }


    }
    protected void InitializePage()
    {
        ((PIMMasterPage)Page.Master).SetTitle("Chart");

        using (NetHealthPIMEntities pim = new NetHealthPIMEntities())
        {
            int ModuleID = (int)Session[Constants.SESSION_WORKINGMODULEID];
            Module module = pim.Module.First(c => c.ModuleID == ModuleID);
            ((PIMMasterPage)Page.Master).SetHeader(module.ModuleName + " Chart Abstraction");

            ParticipantModuleCycle prev = (ParticipantModuleCycle)Session[Constants.SESSION_PREVIOUSCYCLE];
            String CycleNumber = Request.QueryString["CycleNumber"];

            int cycleID = ctx.ActiveModuleCycleID;
            if (CycleNumber == "1")
            {
                cycleID = prev.ParticipantModuleCycleID;
                LinkButtonBackToDashboard.Visible = true;
                ButtonSubmit.Visible = false;
            }

            ParticipantModuleCycle cycle = pim.ParticipantModuleCycle.First(c => c.ParticipantModuleCycleID == cycleID);
            String numberOfRecord = Request.QueryString["nr"];
            String totalRecords = Request.QueryString["t"];
            if (cycle.CycleNumber == 1)
            {
                ((PIMMasterPage)Page.Master).SetStatus(2);
                LiteralAbstractionNumber.Text = "Initial Abstraction: Chart " + numberOfRecord + " of " + totalRecords;
            }
            else
            {
                ((PIMMasterPage)Page.Master).SetStatus(3);
                LiteralAbstractionNumber.Text = "Second Abstraction: Chart " + numberOfRecord + " of " + totalRecords;
            }
            DropDownListMonthOfBirth.DataSource = CycleManager.getMonthList();
            DropDownListYearOfBirth.DataSource = CycleManager.getDOBYearList(0, 100);
            DropDownListYearOfExam.DataSource = CycleManager.getDOBYearList(0, 100);
            DropDownListMonthOfExam.DataSource = CycleManager.getMonthList();
            DropDownListMedChangeYear.DataSource = CycleManager.getDOBYearList(0, 100);
            DropDownListMedChangeMonth.DataSource = CycleManager.getMonthList();
             DropDownListMedTopicalYear.DataSource = CycleManager.getDOBYearList(0, 100);
            DropDownListMedTopicalMonth.DataSource = CycleManager.getMonthList();
          
           DropDownListBCVA.DataSource=CycleManager.getExamValues();
             DropDownListFinalBCVA.DataSource=CycleManager.getExamValues();





            DataBind();
            DropDownListYearOfBirth.Items.Insert(0, new ListItem("Year", "0"));
            DropDownListMedTopicalYear.Items.Insert(0, new ListItem("Year", "0"));
            DropDownListMedChangeYear.Items.Insert(0, new ListItem("Year", "0"));
	    DropDownListYearOfExam.Items.Insert(0, new ListItem("Year", "0"));
            string recordIdentifier = Request.QueryString["ri"];
            HiddenFieldRecordIdentifier.Value = recordIdentifier;
            LiteralRecordIdentifier.Text = recordIdentifier;
            var count = pim.ChartAbstractionIK.Where(ps => ps.ParticipantModuleCycle.ParticipantModuleCycleID == cycleID & ps.RecordIdentifier == recordIdentifier).Count();
            if (count == 0)
            {
                using (TransactionScope transaction = new TransactionScope())
                {
                    //Module module = pim.Module.First(m => m.ModuleID == Constants.ABO_AMBLYOPIA_MODULEID);

                    NetHealthPIMModel.ChartRegistration chartRegistration = (from cr in pim.ChartRegistration
                                                           where cr.ParticipantModuleCycleID == cycleID
                                                           & cr.RecordIdentifier == recordIdentifier
                                                           & cr.ModuleID == 13///// NEEDS TO BE CHANGED
                                                                             select cr).First<NetHealthPIMModel.ChartRegistration>();

                    DateTime initialVisit = Convert.ToDateTime(chartRegistration.InitialVisit);
                    DateTime DOB = Convert.ToDateTime(chartRegistration.DOB);

                    ChartAbstractionIK abstractRecord = new ChartAbstractionIK();
                    abstractRecord.ParticipantModuleCycle = cycle;
                    abstractRecord.RecordIdentifier = recordIdentifier;
                    abstractRecord.MonthOfBirth = DOB.Month;
                    abstractRecord.YearOfBirth = DOB.Year;
                    abstractRecord.MonthOfExam = initialVisit.Month;
                    abstractRecord.YearOfExam = initialVisit.Year;
                    DropDownListYearOfBirth.SelectedValue = abstractRecord.YearOfBirth.ToString();
                    DropDownListMonthOfBirth.SelectedValue = abstractRecord.MonthOfBirth.ToString();
                    DropDownListYearOfExam.SelectedValue = abstractRecord.YearOfExam.ToString();
                    DropDownListMonthOfExam.SelectedValue = abstractRecord.MonthOfExam.ToString();
                    abstractRecord.CreatedOn = DateTime.Now;
                    abstractRecord.LastUpdateDate = DateTime.Now;
                    pim.SaveChanges();

                    transaction.Complete();
                }
            }
            else
            {

                ChartAbstractionIK abstractRecord = (from c in pim.ChartAbstractionIK
                                                     where c.ParticipantModuleCycle.ParticipantModuleCycleID == cycleID & c.RecordIdentifier == recordIdentifier
                                                     select c).First<ChartAbstractionIK>();

                if (abstractRecord.PainLevelNotDocumented != null) CheckBoxPainLevelNotDocumented.Checked = ((bool)abstractRecord.PainLevelNotDocumented);
                if (abstractRecord.MedAminoglycoside != null) CheckBoxMedAminoglycoside.Checked = ((bool)abstractRecord.MedAminoglycoside);
                if (abstractRecord.MedAntifungal != null) CheckBoxMedAntifungal.Checked = ((bool)abstractRecord.MedAntifungal);
                if (abstractRecord.MedAntiparasitic != null) CheckBoxMedAntiparasitic.Checked = ((bool)abstractRecord.MedAntiparasitic);
                if (abstractRecord.Medcephalosporin != null) CheckBoxMedcephalosporin.Checked = ((bool)abstractRecord.Medcephalosporin);
                if (abstractRecord.Fluoroquinolone != null) CheckBoxFluoroquinolone.Checked = ((bool)abstractRecord.Fluoroquinolone);
                if (abstractRecord.MedMacrolide != null) CheckBoxMedMacrolide.Checked = ((bool)abstractRecord.MedMacrolide);
                if (abstractRecord.MedVancomycin != null) CheckBoxMedVancomycin.Checked = ((bool)abstractRecord.MedVancomycin);
                if (abstractRecord.MedPolymyxin != null) CheckBoxMedPolymyxin.Checked = ((bool)abstractRecord.MedPolymyxin);
                if (abstractRecord.MedNeomycin != null) CheckBoxMedNeomycin.Checked = ((bool)abstractRecord.MedNeomycin);
                if (abstractRecord.MedOther != null) CheckBoxMedOther.Checked = ((bool)abstractRecord.MedOther);
                if (abstractRecord.MedSysAntibacterial != null) CheckBoxMedSysAntibacterial.Checked = ((bool)abstractRecord.MedSysAntibacterial);
                if (abstractRecord.MedSysAntifungal != null) CheckBoxMedSysAntifungal.Checked = ((bool)abstractRecord.MedSysAntifungal);
                if (abstractRecord.MedSysAntiparasitic != null) CheckBoxMedSysAntiparasitic.Checked = ((bool)abstractRecord.MedSysAntiparasitic);
                if (abstractRecord.MicroSmear != null) CheckBoxMicroSmear.Checked = ((bool)abstractRecord.MicroSmear);
                if (abstractRecord.MicroBacterialCulture != null) CheckBoxMicroBacterialCulture.Checked = ((bool)abstractRecord.MicroBacterialCulture);
                if (abstractRecord.MicroFungal != null) CheckBoxMicroFungal.Checked = ((bool)abstractRecord.MicroFungal);
                if (abstractRecord.MicroAmoeba != null) CheckBoxMicroAmoeba.Checked = ((bool)abstractRecord.MicroAmoeba);
                if (abstractRecord.MicroViral != null) CheckBoxMicroViral.Checked = ((bool)abstractRecord.MicroViral);
                if (abstractRecord.MicroPCR != null) CheckBoxMicroPCR.Checked = ((bool)abstractRecord.MicroPCR);
                if (abstractRecord.MicroCorneal != null) CheckBoxMicroCorneal.Checked = ((bool)abstractRecord.MicroCorneal);
                if (abstractRecord.MicroConfocal != null) CheckBoxMicroConfocal.Checked = ((bool)abstractRecord.MicroConfocal);
                if (abstractRecord.MicroOther != null) CheckBoxMicroOther.Checked = ((bool)abstractRecord.MicroOther);
                if (abstractRecord.MicroNone != null) CheckBoxMicroNone.Checked = ((bool)abstractRecord.MicroNone);
                if (abstractRecord.CausativeContactLens != null) CheckBoxCausativeContactLens.Checked = ((bool)abstractRecord.CausativeContactLens);
                if (abstractRecord.CausativeDryEye != null) CheckBoxCausativeDryEye.Checked = ((bool)abstractRecord.CausativeDryEye);
                if (abstractRecord.CausativeCorticosteroid != null) CheckBoxCausativeCorticosteroid.Checked = ((bool)abstractRecord.CausativeCorticosteroid);
                if (abstractRecord.CausativeExposureKer != null) CheckBoxCausativeExposureKer.Checked = ((bool)abstractRecord.CausativeExposureKer);
                if (abstractRecord.CausativeNeurotrophicKer != null) CheckBoxCausativeNeurotrophicKer.Checked = ((bool)abstractRecord.CausativeNeurotrophicKer);
                if (abstractRecord.CausativePostKer != null) CheckBoxCausativePostKer.Checked = ((bool)abstractRecord.CausativePostKer);
                if (abstractRecord.CausativeSuture != null) CheckBoxCausativeSuture.Checked = ((bool)abstractRecord.CausativeSuture);
                if (abstractRecord.CausativeOtherPS != null) CheckBoxCausativeOtherPS.Checked = ((bool)abstractRecord.CausativeOtherPS);
                if (abstractRecord.CausativeTrauma != null) CheckBoxCausativeTrauma.Checked = ((bool)abstractRecord.CausativeTrauma);
                if (abstractRecord.CausativePrior != null) CheckBoxCausativePrior.Checked = ((bool)abstractRecord.CausativePrior);
                if (abstractRecord.CausativeOther != null) CheckBoxCausativeOther.Checked = ((bool)abstractRecord.CausativeOther);
                if (abstractRecord.InitialAminoglycoside != null) CheckBoxInitialAminoglycoside.Checked = ((bool)abstractRecord.InitialAminoglycoside);
                if (abstractRecord.InitialAntifungal != null) CheckBoxInitialAntifungal.Checked = ((bool)abstractRecord.InitialAntifungal);
                if (abstractRecord.InitialAntiparasitic != null) CheckBoxInitialAntiparasitic.Checked = ((bool)abstractRecord.InitialAntiparasitic);
                if (abstractRecord.InitialCephalosporin != null) CheckBoxInitialCephalosporin.Checked = ((bool)abstractRecord.InitialCephalosporin);
                if (abstractRecord.InitialFluoroquinolone != null) CheckBoxInitialFluoroquinolone.Checked = ((bool)abstractRecord.InitialFluoroquinolone);
                if (abstractRecord.InitialMacrolide != null) CheckBoxInitialMacrolide.Checked = ((bool)abstractRecord.InitialMacrolide);
                if (abstractRecord.InitialVancomycin != null) CheckBoxInitialVancomycin.Checked = ((bool)abstractRecord.InitialVancomycin);
                if (abstractRecord.InitialPolymyxin != null) CheckBoxInitialPolymyxin.Checked = ((bool)abstractRecord.InitialPolymyxin);
                if (abstractRecord.InitialNeomycin != null) CheckBoxInitialNeomycin.Checked = ((bool)abstractRecord.InitialNeomycin);
                if (abstractRecord.InitialOther != null) CheckBoxInitialOther.Checked = ((bool)abstractRecord.InitialOther);
                if (abstractRecord.InitialSysAntibacterial != null) CheckBoxInitialSysAntibacterial.Checked = ((bool)abstractRecord.InitialSysAntibacterial);
                if (abstractRecord.InitialSysAntifungal != null) CheckBoxInitialSysAntifungal.Checked = ((bool)abstractRecord.InitialSysAntifungal);
                if (abstractRecord.InitialSysAntiparasitic != null) CheckBoxInitialSysAntiparasitic.Checked = ((bool)abstractRecord.InitialSysAntiparasitic);
                if (abstractRecord.FindAcanthamoeba != null) CheckBoxFindAcanthamoeba.Checked = ((bool)abstractRecord.FindAcanthamoeba);
                if (abstractRecord.FindFilamentous != null) CheckBoxFindFilamentous.Checked = ((bool)abstractRecord.FindFilamentous);
                if (abstractRecord.FindGramPositive != null) CheckBoxFindGramPositive.Checked = ((bool)abstractRecord.FindGramPositive);
                if (abstractRecord.FindGramNegative != null) CheckBoxFindGramNegative.Checked = ((bool)abstractRecord.FindGramNegative);
                if (abstractRecord.FindMixedOrganisms != null) CheckBoxFindMixedOrganisms.Checked = ((bool)abstractRecord.FindMixedOrganisms);
                if (abstractRecord.FindMycobacteria != null) CheckBoxFindMycobacteria.Checked = ((bool)abstractRecord.FindMycobacteria);
                if (abstractRecord.FindNocardia != null) CheckBoxFindNocardia.Checked = ((bool)abstractRecord.FindNocardia);
                if (abstractRecord.FindYeast != null) CheckBoxFindYeast.Checked = ((bool)abstractRecord.FindYeast);
                if (abstractRecord.FindNoOrganisms != null) CheckBoxFindNoOrganisms.Checked = ((bool)abstractRecord.FindNoOrganisms);
                if (abstractRecord.FindDiagnostic != null) CheckBoxFindDiagnostic.Checked = ((bool)abstractRecord.FindDiagnostic);
                if (abstractRecord.FindOther != null) CheckBoxFindOther.Checked = ((bool)abstractRecord.FindOther);
                if (abstractRecord.MedAddAminoglycoside != null) CheckBoxMedAddAminoglycoside.Checked = ((bool)abstractRecord.MedAddAminoglycoside);
                if (abstractRecord.MedAddAntifungal != null) CheckBoxMedAddAntifungal.Checked = ((bool)abstractRecord.MedAddAntifungal);
                if (abstractRecord.MedAddAntiparasitic != null) CheckBoxMedAddAntiparasitic.Checked = ((bool)abstractRecord.MedAddAntiparasitic);
                if (abstractRecord.MedAddCephalosporin != null) CheckBoxMedAddCephalosporin.Checked = ((bool)abstractRecord.MedAddCephalosporin);
                if (abstractRecord.MedAddFluoroquinolone != null) CheckBoxMedAddFluoroquinolone.Checked = ((bool)abstractRecord.MedAddFluoroquinolone);
                if (abstractRecord.MedAddMacrolide != null) CheckBoxMedAddMacrolide.Checked = ((bool)abstractRecord.MedAddMacrolide);
                if (abstractRecord.MedAddVancomycin != null) CheckBoxMedAddVancomycin.Checked = ((bool)abstractRecord.MedAddVancomycin);
                if (abstractRecord.MedAddPolymyxin != null) CheckBoxMedAddPolymyxin.Checked = ((bool)abstractRecord.MedAddPolymyxin);
                if (abstractRecord.MedAddNeomycin != null) CheckBoxMedAddNeomycin.Checked = ((bool)abstractRecord.MedAddNeomycin);
                if (abstractRecord.MedAddOther != null) CheckBoxMedAddOther.Checked = ((bool)abstractRecord.MedAddOther);
                if (abstractRecord.MedAddSysAntibacterial != null) CheckBoxMedAddSysAntibacterial.Checked = ((bool)abstractRecord.MedAddSysAntibacterial);
                if (abstractRecord.MedAddSysAntifungal != null) CheckBoxMedAddSysAntifungal.Checked = ((bool)abstractRecord.MedAddSysAntifungal);
                if (abstractRecord.MedAddSysAntiparasitic != null) CheckBoxMedAddSysAntiparasitic.Checked = ((bool)abstractRecord.MedAddSysAntiparasitic);
                if (abstractRecord.MedDiscAminoglycoside != null) CheckBoxMedDiscAminoglycoside.Checked = ((bool)abstractRecord.MedDiscAminoglycoside);
                if (abstractRecord.MedDiscAntifungal != null) CheckBoxMedDiscAntifungal.Checked = ((bool)abstractRecord.MedDiscAntifungal);
                if (abstractRecord.MedDiscAntiparasitic != null) CheckBoxMedDiscAntiparasitic.Checked = ((bool)abstractRecord.MedDiscAntiparasitic);
                if (abstractRecord.MedDiscCephalosporin != null) CheckBoxMedDiscCephalosporin.Checked = ((bool)abstractRecord.MedDiscCephalosporin);
                if (abstractRecord.MedDiscFluoroquinolone != null) CheckBoxMedDiscFluoroquinolone.Checked = ((bool)abstractRecord.MedDiscFluoroquinolone);
                if (abstractRecord.MedDiscMacrolide != null) CheckBoxMedDiscMacrolide.Checked = ((bool)abstractRecord.MedDiscMacrolide);
                if (abstractRecord.MedDiscVancomycin != null) CheckBoxMedDiscVancomycin.Checked = ((bool)abstractRecord.MedDiscVancomycin);
                if (abstractRecord.MedDiscPolymyxin != null) CheckBoxMedDiscPolymyxin.Checked = ((bool)abstractRecord.MedDiscPolymyxin);
                if (abstractRecord.MedDiscNeomycin != null) CheckBoxMedDiscNeomycin.Checked = ((bool)abstractRecord.MedDiscNeomycin);
                if (abstractRecord.MedDiscOther != null) CheckBoxMedDiscOther.Checked = ((bool)abstractRecord.MedDiscOther);
                if (abstractRecord.MedDiscSysAntibacterial != null) CheckBoxMedDiscSysAntibacterial.Checked = ((bool)abstractRecord.MedDiscSysAntibacterial);
                if (abstractRecord.MedDiscSysAntifungal != null) CheckBoxMedDiscSysAntifungal.Checked = ((bool)abstractRecord.MedDiscSysAntifungal);
                if (abstractRecord.MedDiscSysAntiparasitic != null) CheckBoxMedDiscSysAntiparasitic.Checked = ((bool)abstractRecord.MedDiscSysAntiparasitic);
                if (abstractRecord.PathAcanthamoeba != null) CheckBoxPathAcanthamoeba.Checked = ((bool)abstractRecord.PathAcanthamoeba);
                if (abstractRecord.PathFilamentous != null) CheckBoxPathFilamentous.Checked = ((bool)abstractRecord.PathFilamentous);
                if (abstractRecord.PathGramPositive != null) CheckBoxPathGramPositive.Checked = ((bool)abstractRecord.PathGramPositive);
                if (abstractRecord.PathGramNegative != null) CheckBoxPathGramNegative.Checked = ((bool)abstractRecord.PathGramNegative);
                if (abstractRecord.PathMixedOrganisms != null) CheckBoxPathMixedOrganisms.Checked = ((bool)abstractRecord.PathMixedOrganisms);
                if (abstractRecord.PathMycobacteria != null) CheckBoxPathMycobacteria.Checked = ((bool)abstractRecord.PathMycobacteria);
                if (abstractRecord.PathNocardia != null) CheckBoxPathNocardia.Checked = ((bool)abstractRecord.PathNocardia);
                if (abstractRecord.PathYeast != null) CheckBoxPathYeast.Checked = ((bool)abstractRecord.PathYeast);
                if (abstractRecord.PathNoOrganisms != null) CheckBoxPathNoOrganisms.Checked = ((bool)abstractRecord.PathNoOrganisms);
                //if (abstractRecord.PathOther != null) TextBoxPathOther.Text = abstractRecord.PathOther.ToString();
                if (abstractRecord.PathOther != null) CheckBoxOther.Checked = ((bool)abstractRecord.PathOther);


                if (abstractRecord.RBOnsetUnilateral != null) RadioButtonListRBOnsetUnilateral.SelectedValue = abstractRecord.RBOnsetUnilateral.ToString();
                if (abstractRecord.PreviousOcularSurgery != null) RadioButtonListPreviousOcularSurgery.SelectedValue = abstractRecord.PreviousOcularSurgery.ToString();
                if (abstractRecord.PriorVisualImpairment != null) RadioButtonListPriorVisualImpairment.SelectedValue = abstractRecord.PriorVisualImpairment.ToString();
                if (abstractRecord.HistoryTrauma != null) RadioButtonListHistoryTrauma.SelectedValue = abstractRecord.HistoryTrauma.ToString();
                if (abstractRecord.HistoryContactLens != null) RadioButtonListHistoryContactLens.SelectedValue = abstractRecord.HistoryContactLens.ToString();

                if (abstractRecord.InitialTherapyNotEmpiric != null) RadioButtonListInitialTherapyNotEmpiric.SelectedValue = abstractRecord.InitialTherapyNotEmpiric.ToString();
                if (abstractRecord.RBOvernightLensWear != null) RadioButtonListRBOvernightLensWear.SelectedValue = abstractRecord.RBOvernightLensWear.ToString();
                if (abstractRecord.RBAffectedEye != null) RadioButtonListRBAffectedEye.SelectedValue = abstractRecord.RBAffectedEye.ToString();
                if (abstractRecord.RBGreatestDimEpithelial != null) RadioButtonListRBGreatestDimEpithelial.SelectedValue = abstractRecord.RBGreatestDimEpithelial.ToString();
                if (abstractRecord.RBGreatestDimInfiltrate != null) RadioButtonListRBGreatestDimInfiltrate.SelectedValue = abstractRecord.RBGreatestDimInfiltrate.ToString();
                if (abstractRecord.RBInfiltrateCentral4mm != null) RadioButtonListRBInfiltrateCentral4mm.SelectedValue = abstractRecord.RBInfiltrateCentral4mm.ToString();
                if (abstractRecord.RBSingleMultipleInfiltrates != null) RadioButtonListRBSingleMultipleInfiltrates.SelectedValue = abstractRecord.RBSingleMultipleInfiltrates.ToString();
                if (abstractRecord.RBGreatestDepthStromalTissue != null) RadioButtonListRBGreatestDepthStromalTissue.SelectedValue = abstractRecord.RBGreatestDepthStromalTissue.ToString();
                if (abstractRecord.RBHypopyonPresent != null) RadioButtonListRBHypopyonPresent.SelectedValue = abstractRecord.RBHypopyonPresent.ToString();
                if (abstractRecord.RBWorkingDiagnosis != null) RadioButtonListRBWorkingDiagnosis.SelectedValue = abstractRecord.RBWorkingDiagnosis.ToString();
                if (abstractRecord.RBCausativeFactor != null) RadioButtonListRBCausativeFactor.SelectedValue = abstractRecord.RBCausativeFactor.ToString();
                if (abstractRecord.RBFrequencyEyeDrop != null) RadioButtonListRBFrequencyEyeDrop.SelectedValue = abstractRecord.RBFrequencyEyeDrop.ToString();
                if (abstractRecord.RBTopicalPrescribed != null) RadioButtonListRBTopicalPrescribed.SelectedValue = abstractRecord.RBTopicalPrescribed.ToString();
                if (abstractRecord.RBMedChangeRationale != null) RadioButtonListRBMedChangeRationale.SelectedValue = abstractRecord.RBMedChangeRationale.ToString();
                if (abstractRecord.RBCornealBiopsy != null) RadioButtonListRBCornealBiopsy.SelectedValue = abstractRecord.RBCornealBiopsy.ToString();
                if (abstractRecord.PathologicFindingsDocuments != null) RadioButtonListPathologicFindingsDocuments.SelectedValue = abstractRecord.PathologicFindingsDocuments.ToString();
                if (abstractRecord.RBTherapeuticSurg != null) RadioButtonListRBTherapeuticSurg.SelectedValue = abstractRecord.RBTherapeuticSurg.ToString();
                if (abstractRecord.RBPatientCounseled != null) RadioButtonListRBPatientCounseled.SelectedValue = abstractRecord.RBPatientCounseled.ToString();



                if (abstractRecord.MonthOfBirth != null) DropDownListMonthOfBirth.SelectedValue = abstractRecord.MonthOfBirth.ToString();
                if (abstractRecord.YearOfBirth != null) DropDownListYearOfBirth.SelectedValue = abstractRecord.YearOfBirth.ToString();
                if (abstractRecord.MonthOfExam != null) DropDownListMonthOfExam.SelectedValue = abstractRecord.MonthOfExam.ToString();
                if (abstractRecord.YearOfExam != null) DropDownListYearOfExam.SelectedValue = abstractRecord.YearOfExam.ToString();
                if (abstractRecord.BCVA != null) DropDownListBCVA.SelectedValue = abstractRecord.BCVA.ToString();
                if (abstractRecord.MedChangeMonth != null) DropDownListMedChangeMonth.SelectedValue = abstractRecord.MedChangeMonth.ToString();
                if (abstractRecord.MedChangeYear != null) DropDownListMedChangeYear.SelectedValue = abstractRecord.MedChangeYear.ToString();
                if (abstractRecord.MedTopicalMonth != null) DropDownListMedTopicalMonth.SelectedValue = abstractRecord.MedTopicalMonth.ToString();
                if (abstractRecord.MedTopicalYear != null) DropDownListMedTopicalYear.SelectedValue = abstractRecord.MedTopicalYear.ToString();
                if (abstractRecord.FinalBCVA != null) DropDownListFinalBCVA.SelectedValue = abstractRecord.FinalBCVA.ToString();



                if (abstractRecord.AntimicrobialTherapy != null)
                {
                    RadioButtonAntimicrobialTherapyYes.Checked = ((bool)abstractRecord.AntimicrobialTherapy);
                    RadioButtonAntimicrobialTherapyNo.Checked = !((bool)abstractRecord.AntimicrobialTherapy);
                }
                if (abstractRecord.DiagnosticCornealScraping != null)
                {
                    RadioButtonDiagnosticCornealScrapingYes.Checked = ((bool)abstractRecord.DiagnosticCornealScraping);
                    RadioButtonDiagnosticCornealScrapingNo.Checked = !((bool)abstractRecord.DiagnosticCornealScraping);
                }
                if (abstractRecord.WorkingDiagnosis != null)
                {
                    RadioButtonWorkingDiagnosisYes.Checked = ((bool)abstractRecord.WorkingDiagnosis);
                    RadioButtonWorkingDiagnosisNo.Checked = !((bool)abstractRecord.WorkingDiagnosis);
                }
                //if (abstractRecord.InitialTherapyRationaleDoc != null)
                //{
                //    RadioButtonInitialTherapyRationaleDocYes.Checked = ((bool)abstractRecord.InitialTherapyRationaleDoc);
                //    RadioButtonInitialTherapyRationaleDocNo.Checked = !((bool)abstractRecord.InitialTherapyRationaleDoc);
                //}
                if (abstractRecord.ChangeMed != null)
                {
                    RadioButtonChangeMedYes.Checked = ((bool)abstractRecord.ChangeMed);
                    RadioButtonChangeMedNo.Checked = !((bool)abstractRecord.ChangeMed);
                }
                if (abstractRecord.MedTopicalCorticosteroids != null)
                {
                    RadioButtonMedTopicalCorticosteroidsYes.Checked = ((bool)abstractRecord.MedTopicalCorticosteroids);
                    RadioButtonMedTopicalCorticosteroidsNo.Checked = !((bool)abstractRecord.MedTopicalCorticosteroids);
                }
                if (abstractRecord.CornealPerforation != null)
                {
                    RadioButtonCornealPerforationYes.Checked = ((bool)abstractRecord.CornealPerforation);
                    RadioButtonCornealPerforationNo.Checked = !((bool)abstractRecord.CornealPerforation);
                }
                if (abstractRecord.SurgicalIntOpticalRehab != null)
                {
                    RadioButtonSurgicalIntOpticalRehabYes.Checked = ((bool)abstractRecord.SurgicalIntOpticalRehab);
                    RadioButtonSurgicalIntOpticalRehabNo.Checked = !((bool)abstractRecord.SurgicalIntOpticalRehab);
                }



                if (abstractRecord.PainLevel != null) TextBoxPainLevel.Text = abstractRecord.PainLevel.ToString();
                if (abstractRecord.MedOtherText != null) TextBoxMedOtherText.Text = abstractRecord.MedOtherText.ToString();
                if (abstractRecord.MicroOtherText != null) TextBoxMicroOtherText.Text = abstractRecord.MicroOtherText.ToString();
                if (abstractRecord.CausativeOtherText != null) TextBoxCausativeOtherText.Text = abstractRecord.CausativeOtherText.ToString();
                if (abstractRecord.InitialOtherText != null) TextBoxInitialOtherText.Text = abstractRecord.InitialOtherText.ToString();
                if (abstractRecord.FindOtherText != null) TextBoxFindOtherText.Text = abstractRecord.FindOtherText.ToString();
                if (abstractRecord.DaysUntilInitialVisit != null) TextBoxDaysUntilInitialVisit.Text = abstractRecord.DaysUntilInitialVisit.ToString();
                if (abstractRecord.MedAddOtherText != null) TextBoxMedAddOtherText.Text = abstractRecord.MedAddOtherText.ToString();
                //if (abstractRecord.MedDiscOtherText != null) TextBoxMedDiscOtherText.Text = abstractRecord.MedDiscOtherText.ToString();
                if (abstractRecord.PathOtherText != null) TextBoxPathOtherText.Text = abstractRecord.PathOtherText.ToString();
                if (abstractRecord.TherapeuticSurgOther != null) TextBoxTherapeuticSurgOther.Text = abstractRecord.TherapeuticSurgOther.ToString();
                if (abstractRecord.DurationInitialFinalMonths != null) TextBoxDurationInitialFinalMonths.Text = abstractRecord.DurationInitialFinalMonths.ToString();






            }

        }
    }


    public void savedata()
    {
        using (NetHealthPIMEntities pim = new NetHealthPIMEntities())
        {
            int ModuleID = (int)Session[Constants.SESSION_WORKINGMODULEID];
            int cycleID = ctx.ActiveModuleCycleID;
            ParticipantModuleCycle cycle = pim.ParticipantModuleCycle.First(c => c.ParticipantModuleCycleID == cycleID);
            string recordIdentifier = HiddenFieldRecordIdentifier.Value;
            using (TransactionScope transaction = new TransactionScope())
            {
                ChartAbstractionIK abstractRecord = (from c in pim.ChartAbstractionIK
                                                     where c.ParticipantModuleCycle.ParticipantModuleCycleID == cycleID & c.RecordIdentifier == recordIdentifier
                                                                             select c).First<ChartAbstractionIK>();
                            abstractRecord.PainLevelNotDocumented = CheckBoxPainLevelNotDocumented.Checked;
                            abstractRecord.MedAminoglycoside = CheckBoxMedAminoglycoside.Checked;
                            abstractRecord.MedAntifungal = CheckBoxMedAntifungal.Checked;
                            abstractRecord.MedAntiparasitic = CheckBoxMedAntiparasitic.Checked;
                            abstractRecord.Medcephalosporin = CheckBoxMedcephalosporin.Checked;
                            abstractRecord.Fluoroquinolone = CheckBoxFluoroquinolone.Checked;
                            abstractRecord.MedMacrolide = CheckBoxMedMacrolide.Checked;
                            abstractRecord.MedVancomycin = CheckBoxMedVancomycin.Checked;
                            abstractRecord.MedPolymyxin = CheckBoxMedPolymyxin.Checked;
                            abstractRecord.MedNeomycin = CheckBoxMedNeomycin.Checked;
                            abstractRecord.MedOther = CheckBoxMedOther.Checked;
                            abstractRecord.MedSysAntibacterial = CheckBoxMedSysAntibacterial.Checked;
                            abstractRecord.MedSysAntifungal = CheckBoxMedSysAntifungal.Checked;
                            abstractRecord.MedSysAntiparasitic = CheckBoxMedSysAntiparasitic.Checked;
                            abstractRecord.MicroSmear = CheckBoxMicroSmear.Checked;
                            abstractRecord.MicroBacterialCulture = CheckBoxMicroBacterialCulture.Checked;
                            abstractRecord.MicroFungal = CheckBoxMicroFungal.Checked;
                            abstractRecord.MicroAmoeba = CheckBoxMicroAmoeba.Checked;
                            abstractRecord.MicroViral = CheckBoxMicroViral.Checked;
                            abstractRecord.MicroPCR = CheckBoxMicroPCR.Checked;
                            abstractRecord.MicroCorneal = CheckBoxMicroCorneal.Checked;
                            abstractRecord.MicroConfocal = CheckBoxMicroConfocal.Checked;
                            abstractRecord.MicroOther = CheckBoxMicroOther.Checked;
                            abstractRecord.MicroNone = CheckBoxMicroNone.Checked;
                            abstractRecord.CausativeContactLens = CheckBoxCausativeContactLens.Checked;
                            abstractRecord.CausativeDryEye = CheckBoxCausativeDryEye.Checked;
                            abstractRecord.CausativeCorticosteroid = CheckBoxCausativeCorticosteroid.Checked;
                            abstractRecord.CausativeExposureKer = CheckBoxCausativeExposureKer.Checked;
                            abstractRecord.CausativeNeurotrophicKer = CheckBoxCausativeNeurotrophicKer.Checked;
                            abstractRecord.CausativePostKer = CheckBoxCausativePostKer.Checked;
                            abstractRecord.CausativeSuture = CheckBoxCausativeSuture.Checked;
                            abstractRecord.CausativeOtherPS = CheckBoxCausativeOtherPS.Checked;
                            abstractRecord.CausativeTrauma = CheckBoxCausativeTrauma.Checked;
                            abstractRecord.CausativePrior = CheckBoxCausativePrior.Checked;
                            abstractRecord.CausativeOther = CheckBoxCausativeOther.Checked;
                            abstractRecord.InitialAminoglycoside = CheckBoxInitialAminoglycoside.Checked;
                            abstractRecord.InitialAntifungal = CheckBoxInitialAntifungal.Checked;
                            abstractRecord.InitialAntiparasitic = CheckBoxInitialAntiparasitic.Checked;
                            abstractRecord.InitialCephalosporin = CheckBoxInitialCephalosporin.Checked;
                            abstractRecord.InitialFluoroquinolone = CheckBoxInitialFluoroquinolone.Checked;
                            abstractRecord.InitialMacrolide = CheckBoxInitialMacrolide.Checked;
                            abstractRecord.InitialVancomycin = CheckBoxInitialVancomycin.Checked;
                            abstractRecord.InitialPolymyxin = CheckBoxInitialPolymyxin.Checked;
                            abstractRecord.InitialNeomycin = CheckBoxInitialNeomycin.Checked;
                            abstractRecord.InitialOther = CheckBoxInitialOther.Checked;
                            abstractRecord.InitialSysAntibacterial = CheckBoxInitialSysAntibacterial.Checked;
                            abstractRecord.InitialSysAntifungal = CheckBoxInitialSysAntifungal.Checked;
                            abstractRecord.InitialSysAntiparasitic = CheckBoxInitialSysAntiparasitic.Checked;
                            abstractRecord.FindAcanthamoeba = CheckBoxFindAcanthamoeba.Checked;
                            abstractRecord.FindFilamentous = CheckBoxFindFilamentous.Checked;
                            abstractRecord.FindGramPositive = CheckBoxFindGramPositive.Checked;
                            abstractRecord.FindGramNegative = CheckBoxFindGramNegative.Checked;
                            abstractRecord.FindMixedOrganisms = CheckBoxFindMixedOrganisms.Checked;
                            abstractRecord.FindMycobacteria = CheckBoxFindMycobacteria.Checked;
                            abstractRecord.FindNocardia = CheckBoxFindNocardia.Checked;
                            abstractRecord.FindYeast = CheckBoxFindYeast.Checked;
                            abstractRecord.FindNoOrganisms = CheckBoxFindNoOrganisms.Checked;
                            abstractRecord.FindDiagnostic = CheckBoxFindDiagnostic.Checked;
                            abstractRecord.FindOther = CheckBoxFindOther.Checked;
                            abstractRecord.MedAddAminoglycoside = CheckBoxMedAddAminoglycoside.Checked;
                            abstractRecord.MedAddAntifungal = CheckBoxMedAddAntifungal.Checked;
                            abstractRecord.MedAddAntiparasitic = CheckBoxMedAddAntiparasitic.Checked;
                            abstractRecord.MedAddCephalosporin = CheckBoxMedAddCephalosporin.Checked;
                            abstractRecord.MedAddFluoroquinolone = CheckBoxMedAddFluoroquinolone.Checked;
                            abstractRecord.MedAddMacrolide = CheckBoxMedAddMacrolide.Checked;
                            abstractRecord.MedAddVancomycin = CheckBoxMedAddVancomycin.Checked;
                            abstractRecord.MedAddPolymyxin = CheckBoxMedAddPolymyxin.Checked;
                            abstractRecord.MedAddNeomycin = CheckBoxMedAddNeomycin.Checked;
                            abstractRecord.MedAddOther = CheckBoxMedAddOther.Checked;
                            abstractRecord.MedAddSysAntibacterial = CheckBoxMedAddSysAntibacterial.Checked;
                            abstractRecord.MedAddSysAntifungal = CheckBoxMedAddSysAntifungal.Checked;
                            abstractRecord.MedAddSysAntiparasitic = CheckBoxMedAddSysAntiparasitic.Checked;
                            abstractRecord.MedDiscAminoglycoside = CheckBoxMedDiscAminoglycoside.Checked;
                            abstractRecord.MedDiscAntifungal = CheckBoxMedDiscAntifungal.Checked;
                            abstractRecord.MedDiscAntiparasitic = CheckBoxMedDiscAntiparasitic.Checked;
                            abstractRecord.MedDiscCephalosporin = CheckBoxMedDiscCephalosporin.Checked;
                            abstractRecord.MedDiscFluoroquinolone = CheckBoxMedDiscFluoroquinolone.Checked;
                            abstractRecord.MedDiscMacrolide = CheckBoxMedDiscMacrolide.Checked;
                            abstractRecord.MedDiscVancomycin = CheckBoxMedDiscVancomycin.Checked;
                            abstractRecord.MedDiscPolymyxin = CheckBoxMedDiscPolymyxin.Checked;
                            abstractRecord.MedDiscNeomycin = CheckBoxMedDiscNeomycin.Checked;
                            abstractRecord.MedDiscOther = CheckBoxMedDiscOther.Checked;
                            abstractRecord.MedDiscSysAntibacterial = CheckBoxMedDiscSysAntibacterial.Checked;
                            abstractRecord.MedDiscSysAntifungal = CheckBoxMedDiscSysAntifungal.Checked;
                            abstractRecord.MedDiscSysAntiparasitic = CheckBoxMedDiscSysAntiparasitic.Checked;
                            abstractRecord.PathAcanthamoeba = CheckBoxPathAcanthamoeba.Checked;
                            abstractRecord.PathFilamentous = CheckBoxPathFilamentous.Checked;
                            abstractRecord.PathGramPositive = CheckBoxPathGramPositive.Checked;
                            abstractRecord.PathGramNegative = CheckBoxPathGramNegative.Checked;
                            abstractRecord.PathMixedOrganisms = CheckBoxPathMixedOrganisms.Checked;
                            abstractRecord.PathMycobacteria = CheckBoxPathMycobacteria.Checked;
                            abstractRecord.PathNocardia = CheckBoxPathNocardia.Checked;
                            abstractRecord.PathYeast = CheckBoxPathYeast.Checked;
                            abstractRecord.PathNoOrganisms = CheckBoxPathNoOrganisms.Checked;
                            //abstractRecord.PathOther = CheckBoxPathOther.Checked;
                            abstractRecord.PathOther =  CheckBoxOther.Checked;


                            if (RadioButtonListRBOnsetUnilateral.SelectedIndex != -1) abstractRecord.RBOnsetUnilateral = Convert.ToInt32(RadioButtonListRBOnsetUnilateral.SelectedValue);
                            else abstractRecord.RBOnsetUnilateral = 0;
                            if (RadioButtonListPreviousOcularSurgery.SelectedIndex != -1) abstractRecord.PreviousOcularSurgery = Convert.ToInt32(RadioButtonListPreviousOcularSurgery.SelectedValue);
                            else abstractRecord.PreviousOcularSurgery = 0;
                            if (RadioButtonListPriorVisualImpairment.SelectedIndex != -1) abstractRecord.PriorVisualImpairment = Convert.ToInt32(RadioButtonListPriorVisualImpairment.SelectedValue);
                            else abstractRecord.PriorVisualImpairment = 0;
                            if (RadioButtonListHistoryTrauma.SelectedIndex != -1) abstractRecord.HistoryTrauma = Convert.ToInt32(RadioButtonListHistoryTrauma.SelectedValue);
                            else abstractRecord.HistoryTrauma = 0;
                            if (RadioButtonListHistoryContactLens.SelectedIndex != -1) abstractRecord.HistoryContactLens = Convert.ToInt32(RadioButtonListHistoryContactLens.SelectedValue);
                            else abstractRecord.HistoryContactLens = 0;


                            if (RadioButtonListInitialTherapyNotEmpiric.SelectedIndex != -1) abstractRecord.InitialTherapyNotEmpiric = Convert.ToInt32(RadioButtonListInitialTherapyNotEmpiric.SelectedValue);
                            else abstractRecord.InitialTherapyNotEmpiric = 0;
                            if (RadioButtonListRBOvernightLensWear.SelectedIndex != -1) abstractRecord.RBOvernightLensWear = Convert.ToInt32(RadioButtonListRBOvernightLensWear.SelectedValue);
                            else abstractRecord.RBOvernightLensWear = 0;
                            if (RadioButtonListRBAffectedEye.SelectedIndex != -1) abstractRecord.RBAffectedEye = Convert.ToInt32(RadioButtonListRBAffectedEye.SelectedValue);
                            else abstractRecord.RBAffectedEye = 0;
                            if (RadioButtonListRBGreatestDimEpithelial.SelectedIndex != -1) abstractRecord.RBGreatestDimEpithelial = Convert.ToInt32(RadioButtonListRBGreatestDimEpithelial.SelectedValue);
                            else abstractRecord.RBGreatestDimEpithelial = 0;
                            if (RadioButtonListRBGreatestDimInfiltrate.SelectedIndex != -1) abstractRecord.RBGreatestDimInfiltrate = Convert.ToInt32(RadioButtonListRBGreatestDimInfiltrate.SelectedValue);
                            else abstractRecord.RBGreatestDimInfiltrate = 0;
                            if (RadioButtonListRBInfiltrateCentral4mm.SelectedIndex != -1) abstractRecord.RBInfiltrateCentral4mm = Convert.ToInt32(RadioButtonListRBInfiltrateCentral4mm.SelectedValue);
                            else abstractRecord.RBInfiltrateCentral4mm = 0;
                            if (RadioButtonListRBSingleMultipleInfiltrates.SelectedIndex != -1) abstractRecord.RBSingleMultipleInfiltrates = Convert.ToInt32(RadioButtonListRBSingleMultipleInfiltrates.SelectedValue);
                            else abstractRecord.RBSingleMultipleInfiltrates = 0;
                            if (RadioButtonListRBGreatestDepthStromalTissue.SelectedIndex != -1) abstractRecord.RBGreatestDepthStromalTissue = Convert.ToInt32(RadioButtonListRBGreatestDepthStromalTissue.SelectedValue);
                            else abstractRecord.RBGreatestDepthStromalTissue = 0;
                            if (RadioButtonListRBHypopyonPresent.SelectedIndex != -1) abstractRecord.RBHypopyonPresent = Convert.ToInt32(RadioButtonListRBHypopyonPresent.SelectedValue);
                            else abstractRecord.RBHypopyonPresent = 0;
                            if (RadioButtonListRBWorkingDiagnosis.SelectedIndex != -1) abstractRecord.RBWorkingDiagnosis = Convert.ToInt32(RadioButtonListRBWorkingDiagnosis.SelectedValue);
                            else abstractRecord.RBWorkingDiagnosis = 0;
                            if (RadioButtonListRBCausativeFactor.SelectedIndex != -1) abstractRecord.RBCausativeFactor = Convert.ToInt32(RadioButtonListRBCausativeFactor.SelectedValue);
                            else abstractRecord.RBCausativeFactor = 0;
                            if (RadioButtonListRBFrequencyEyeDrop.SelectedIndex != -1) abstractRecord.RBFrequencyEyeDrop = Convert.ToInt32(RadioButtonListRBFrequencyEyeDrop.SelectedValue);
                            else abstractRecord.RBFrequencyEyeDrop = 0;
                            if (RadioButtonListRBTopicalPrescribed.SelectedIndex != -1) abstractRecord.RBTopicalPrescribed = Convert.ToInt32(RadioButtonListRBTopicalPrescribed.SelectedValue);
                            else abstractRecord.RBTopicalPrescribed = 0;
                            if (RadioButtonListRBMedChangeRationale.SelectedIndex != -1) abstractRecord.RBMedChangeRationale = Convert.ToInt32(RadioButtonListRBMedChangeRationale.SelectedValue);
                            else abstractRecord.RBMedChangeRationale = 0;
                            if (RadioButtonListRBCornealBiopsy.SelectedIndex != -1) abstractRecord.RBCornealBiopsy = Convert.ToInt32(RadioButtonListRBCornealBiopsy.SelectedValue);
                            else abstractRecord.RBCornealBiopsy = 0;
                            if (RadioButtonListPathologicFindingsDocuments.SelectedIndex != -1) abstractRecord.PathologicFindingsDocuments = Convert.ToInt32(RadioButtonListPathologicFindingsDocuments.SelectedValue);
                            else abstractRecord.PathologicFindingsDocuments = 0;
                            if (RadioButtonListRBTherapeuticSurg.SelectedIndex != -1) abstractRecord.RBTherapeuticSurg = Convert.ToInt32(RadioButtonListRBTherapeuticSurg.SelectedValue);
                            else abstractRecord.RBTherapeuticSurg = 0;
                            if (RadioButtonListRBPatientCounseled.SelectedIndex != -1) abstractRecord.RBPatientCounseled = Convert.ToInt32(RadioButtonListRBPatientCounseled.SelectedValue);
                            else abstractRecord.RBPatientCounseled = 0;



                            if (DropDownListMonthOfBirth.SelectedIndex > 0) abstractRecord.MonthOfBirth = Convert.ToInt32(DropDownListMonthOfBirth.SelectedValue);
                            else abstractRecord.MonthOfBirth = 0;
                            if (DropDownListYearOfBirth.SelectedIndex > 0) abstractRecord.YearOfBirth = Convert.ToInt32(DropDownListYearOfBirth.SelectedValue);
                            else abstractRecord.YearOfBirth = 0;
                            if (DropDownListMonthOfExam.SelectedIndex > 0) abstractRecord.MonthOfExam = Convert.ToInt32(DropDownListMonthOfExam.SelectedValue);
                            else abstractRecord.MonthOfExam = 0;
                            if (DropDownListYearOfExam.SelectedIndex > 0) abstractRecord.YearOfExam = Convert.ToInt32(DropDownListYearOfExam.SelectedValue);
                            else abstractRecord.YearOfExam = 0;
                            if (DropDownListBCVA.SelectedIndex > 0) abstractRecord.BCVA = Convert.ToInt32(DropDownListBCVA.SelectedValue);
                            else abstractRecord.BCVA = 0;
                            if (DropDownListMedChangeMonth.SelectedIndex > 0) abstractRecord.MedChangeMonth = Convert.ToInt32(DropDownListMedChangeMonth.SelectedValue);
                            else abstractRecord.MedChangeMonth = 0;
                            if (DropDownListMedChangeYear.SelectedIndex > 0) abstractRecord.MedChangeYear = Convert.ToInt32(DropDownListMedChangeYear.SelectedValue);
                            else abstractRecord.MedChangeYear = 0;
                            if (DropDownListMedTopicalMonth.SelectedIndex > 0) abstractRecord.MedTopicalMonth = Convert.ToInt32(DropDownListMedTopicalMonth.SelectedValue);
                            else abstractRecord.MedTopicalMonth = 0;
                            if (DropDownListMedTopicalYear.SelectedIndex > 0) abstractRecord.MedTopicalYear = Convert.ToInt32(DropDownListMedTopicalYear.SelectedValue);
                            else abstractRecord.MedTopicalYear = 0;
                            if (DropDownListFinalBCVA.SelectedIndex > 0) abstractRecord.FinalBCVA = Convert.ToInt32(DropDownListFinalBCVA.SelectedValue);
                            else abstractRecord.FinalBCVA = 0;



                            if (RadioButtonAntimicrobialTherapyYes.Checked) abstractRecord.AntimicrobialTherapy = true;
                            if (RadioButtonAntimicrobialTherapyNo.Checked) abstractRecord.AntimicrobialTherapy = false;
                            if (RadioButtonDiagnosticCornealScrapingYes.Checked) abstractRecord.DiagnosticCornealScraping = true;
                            if (RadioButtonDiagnosticCornealScrapingNo.Checked) abstractRecord.DiagnosticCornealScraping = false;
                            if (RadioButtonWorkingDiagnosisYes.Checked) abstractRecord.WorkingDiagnosis = true;
                            if (RadioButtonWorkingDiagnosisNo.Checked) abstractRecord.WorkingDiagnosis = false;
                           // if (RadioButtonInitialTherapyRationaleDocYes.Checked) abstractRecord.InitialTherapyRationaleDoc = true;
                           // if (RadioButtonInitialTherapyRationaleDocNo.Checked) abstractRecord.InitialTherapyRationaleDoc = false;
                            if (RadioButtonChangeMedYes.Checked) abstractRecord.ChangeMed = true;
                            if (RadioButtonChangeMedNo.Checked) abstractRecord.ChangeMed = false;
                            if (RadioButtonMedTopicalCorticosteroidsYes.Checked) abstractRecord.MedTopicalCorticosteroids = true;
                            if (RadioButtonMedTopicalCorticosteroidsNo.Checked) abstractRecord.MedTopicalCorticosteroids = false;
                            if (RadioButtonCornealPerforationYes.Checked) abstractRecord.CornealPerforation = true;
                            if (RadioButtonCornealPerforationNo.Checked) abstractRecord.CornealPerforation = false;
                            if (RadioButtonSurgicalIntOpticalRehabYes.Checked) abstractRecord.SurgicalIntOpticalRehab = true;
                            if (RadioButtonSurgicalIntOpticalRehabNo.Checked) abstractRecord.SurgicalIntOpticalRehab = false;



                            try
                            {
                            if (TextBoxPainLevel.Text != null)
                            abstractRecord.PainLevel =Convert.ToInt32( TextBoxPainLevel.Text);
                            }
                            catch (Exception ex)
                            {
                            abstractRecord.PainLevel = null;
                            TextBoxPainLevel.Text = "";
                            }
                            try
                            {
                            if (TextBoxMedOtherText.Text != null)
                            abstractRecord.MedOtherText = TextBoxMedOtherText.Text;
                            }
                            catch (Exception ex)
                            {
                            abstractRecord.MedOtherText = null;
                            TextBoxMedOtherText.Text =  "";
                            }
                            try
                            {
                            if (TextBoxMicroOtherText.Text != null)
                            abstractRecord.MicroOtherText = TextBoxMicroOtherText.Text;
                            }
                            catch (Exception ex)
                            {
                            abstractRecord.MicroOtherText = null;
                            TextBoxMicroOtherText.Text =  "";
                            }
                            try
                            {
                            if (TextBoxCausativeOtherText.Text != null)
                            abstractRecord.CausativeOtherText = TextBoxCausativeOtherText.Text;
                            }
                            catch (Exception ex)
                            {
                            abstractRecord.CausativeOtherText = null;
                            TextBoxCausativeOtherText.Text =  "";
                            }
                            try
                            {
                            if (TextBoxInitialOtherText.Text != null)
                            abstractRecord.InitialOtherText = TextBoxInitialOtherText.Text;
                            }
                            catch (Exception ex)
                            {
                            abstractRecord.InitialOtherText = null;
                            TextBoxInitialOtherText.Text =  "";
                            }
                            try
                            {
                            if (TextBoxFindOtherText.Text != null)
                            abstractRecord.FindOtherText = TextBoxFindOtherText.Text;
                            }
                            catch (Exception ex)
                            {
                            abstractRecord.FindOtherText = null;
                            TextBoxFindOtherText.Text =  "";
                            }
                            try
                            {
                            if (TextBoxDaysUntilInitialVisit.Text != null)
                            abstractRecord.DaysUntilInitialVisit =Convert.ToInt32( TextBoxDaysUntilInitialVisit.Text);
                            }
                            catch (Exception ex)
                            {
                            abstractRecord.DaysUntilInitialVisit = null;
                            TextBoxDaysUntilInitialVisit.Text =  "";
                            }
                            try
                            {
                            if (TextBoxMedAddOtherText.Text != null)
                            abstractRecord.MedAddOtherText = TextBoxMedAddOtherText.Text;
                            }
                            catch (Exception ex)
                            {
                            abstractRecord.MedAddOtherText = null;
                            TextBoxMedAddOtherText.Text = "";
                            }
                            //try
                            //{
                            //if (TextBoxMedDiscOtherText.Text != null)
                            //abstractRecord.MedDiscOtherText = TextBoxMedDiscOtherText.Text;
                            //}
                            //catch (Exception ex)
                            //{
                            //abstractRecord.MedDiscOtherText = null;
                            //TextBoxMedDiscOtherText.Text = '';
                            //}
                            try
                            {
                            if (TextBoxPathOtherText.Text != null)
                            abstractRecord.PathOtherText = TextBoxPathOtherText.Text;
                            }
                            catch (Exception ex)
                            {
                            abstractRecord.PathOtherText = null;
                            TextBoxPathOtherText.Text =  "";
                            }
                            try
                            {
                            if (TextBoxTherapeuticSurgOther.Text != null)
                            abstractRecord.TherapeuticSurgOther = TextBoxTherapeuticSurgOther.Text;
                            }
                            catch (Exception ex)
                            {
                            abstractRecord.TherapeuticSurgOther = null;
                            TextBoxTherapeuticSurgOther.Text =  "";
                            }
                            try
                            {
                            if (TextBoxDurationInitialFinalMonths.Text != null)
                            abstractRecord.DurationInitialFinalMonths =Convert.ToInt32( TextBoxDurationInitialFinalMonths.Text);
                            }
                            catch (Exception ex)
                            {
                            abstractRecord.DurationInitialFinalMonths = null;
                            TextBoxDurationInitialFinalMonths.Text =  "";

                            }

                            abstractRecord.LastUpdateDate = DateTime.Now;
                            bool ChartCompleted = isComplete();
                            abstractRecord.Complete = ChartCompleted;
                            var chartRegistration = (from cr in pim.ChartRegistration
                                                     where cr.ParticipantModuleCycleID == cycle.ParticipantModuleCycleID
                                                      & cr.ModuleID == ModuleID
                                                      & cr.RecordIdentifier == recordIdentifier
                                                     select cr).First<NetHealthPIMModel.ChartRegistration>();
                            chartRegistration.AbstractCompleted = ChartCompleted;
                            if ((DropDownListYearOfBirth.SelectedIndex > 0) && (DropDownListYearOfBirth.SelectedIndex > 0))
                            {
                                chartRegistration.DOB = abstractRecord.MonthOfBirth + "/" + abstractRecord.YearOfBirth;
                            }
                            if ((DropDownListMonthOfExam.SelectedIndex > 0) && (DropDownListYearOfExam.SelectedIndex > 0))
                            {
                                chartRegistration.InitialVisit = abstractRecord.MonthOfExam + "/" + abstractRecord.YearOfExam;
                            }
                            pim.SaveChanges();

                            transaction.Complete();




            }

            CycleManager.UpdateParticipantCompletedModules(cycleID, ModuleID);
        }


    }

       protected void ButtonSubmit_Click(object sender, EventArgs e)
    {
        savedata();
        isComplete();
        if (!isComplete())
        {
            isComplete();
            string script = " var answer = confirm('Chart data is not complete.\\nClick OK to save entries and exit chart.\\nClick CANCEL to save entries and review the chart. '); if (answer==true) window.location = '../PatientChartRegistration.aspx';";
            ScriptManager.RegisterStartupScript(this, GetType(),
                          "ServerControlScript", script, true);
        }
        else
        {

            Response.Redirect("../PatientChartRegistration.aspx");
        }
    }

     protected bool isComplete()
    
        {         
    
             bool ChartCompleted = true;
         


                LabelRBOnsetUnilateral.Visible = false;
               
                LabelPreviousOcularSurgery.Visible = false;
                
                LabelPriorVisualImpairment.Visible = false;
                
                LabelHistoryTrauma.Visible = false;
                
               
                LabelRBAffectedEye.Visible = false;

                LabelInitialTherapyNotEmpiric.Visible = false;
                LabelRBGreatestDimInfiltrate.Visible = false;
                
                LabelRBInfiltrateCentral4mm.Visible = false;
                
                LabelRBSingleMultipleInfiltrates.Visible = false;
               
                LabelRBGreatestDepthStromalTissue.Visible = false;
               
                LabelRBHypopyonPresent.Visible = false;
                
                LabelRBWorkingDiagnosis.Visible = false;
               
                LabelRBCausativeFactor.Visible = false;
                
                LabelRBFrequencyEyeDrop.Visible = false;
               
                LabelRBTopicalPrescribed.Visible = false;
                
                LabelRBCornealBiopsy.Visible = false;
               
                LabelPathologicFindingsDocuments.Visible = false;
                
                LabelRBTherapeuticSurg.Visible = false;
                
                LabelRBPatientCounseled.Visible = false;
                
                LabelMonthOfBirth.Visible = false;
               
                LabelYearOfBirth.Visible = false;
                
                LabelMonthOfExam.Visible = false;
               
                LabelYearOfExam.Visible = false;
                
                LabelBCVA.Visible = false;
                
                LabelFinalBCVA.Visible = false;
                
                LabelPainLevel.Visible = false;
               
                LabelDaysUntilInitialVisit.Visible = false;
               
                LabelDurationInitialFinalMonths.Visible = false;
                
                LabelAntimicrobialTherapy.Visible = false;
                
                LabelDiagnosticCornealScraping.Visible = false;
               
                LabelWorkingDiagnosis.Visible = false;
                
               
                LabelChangeMed.Visible = false;
               
                LabelMedTopicalCorticosteroids.Visible = false;
                
                LabelCornealPerforation.Visible = false;
                
                LabelSurgicalIntOpticalRehab.Visible = false;
                



               if(RadioButtonListRBOnsetUnilateral.SelectedIndex == -1)
                {
                LabelRBOnsetUnilateral.Visible = true;
                ChartCompleted = false;
                }
                if(RadioButtonListPreviousOcularSurgery.SelectedIndex == -1 )
                {
                LabelPreviousOcularSurgery.Visible = true;
                ChartCompleted = false;
                }
                if(RadioButtonListPriorVisualImpairment.SelectedIndex == -1)
                {
                LabelPriorVisualImpairment.Visible = true;
                ChartCompleted = false;
                }
                if(RadioButtonListHistoryTrauma.SelectedIndex == -1)
                {
                LabelHistoryTrauma.Visible = true;
                ChartCompleted = false;
                }
                if(RadioButtonListHistoryContactLens.SelectedIndex == -1)
                {
                LabelHistoryContactLens.Visible = true;
                ChartCompleted = false;
                }
                if(RadioButtonListRBAffectedEye.SelectedIndex == -1)
                {
                LabelRBAffectedEye.Visible = true;
                ChartCompleted = false;
                }
                if (RadioButtonListInitialTherapyNotEmpiric.SelectedIndex == -1)
                {
                    LabelInitialTherapyNotEmpiric.Visible = true;
                    ChartCompleted = false;
                }
                if(RadioButtonListRBGreatestDimEpithelial.SelectedIndex == -1)
                {
                LabelRBGreatestDimEpithelial.Visible = true;
                ChartCompleted = false;
                }
                if(RadioButtonListRBGreatestDimInfiltrate.SelectedIndex == -1)
                {
                LabelRBGreatestDimInfiltrate.Visible = true;
                ChartCompleted = false;
                }
                if(RadioButtonListRBInfiltrateCentral4mm.SelectedIndex == -1)
                {
                LabelRBInfiltrateCentral4mm.Visible = true;
                ChartCompleted = false;
                }
                if(RadioButtonListRBSingleMultipleInfiltrates.SelectedIndex == -1)
                {
                LabelRBSingleMultipleInfiltrates.Visible = true;
                ChartCompleted = false;
                }
                if(RadioButtonListRBGreatestDepthStromalTissue.SelectedIndex == -1)
                {
                LabelRBGreatestDepthStromalTissue.Visible = true;
                ChartCompleted = false;
                }
                if(RadioButtonListRBHypopyonPresent.SelectedIndex == -1)
                {
                LabelRBHypopyonPresent.Visible = true;
                ChartCompleted = false;
                }
               
                if(RadioButtonListRBCausativeFactor.SelectedIndex == -1)
                {
                LabelRBCausativeFactor.Visible = true;
                ChartCompleted = false;
                }
                if(RadioButtonListRBFrequencyEyeDrop.SelectedIndex == -1)
                {
                LabelRBFrequencyEyeDrop.Visible = true;
                ChartCompleted = false;
                }
                if(RadioButtonListRBTopicalPrescribed.SelectedIndex == -1)
                {
                LabelRBTopicalPrescribed.Visible = true;
                ChartCompleted = false;
                }
                if(RadioButtonListRBCornealBiopsy.SelectedIndex == -1)
                {
                LabelRBCornealBiopsy.Visible = true;
                ChartCompleted = false;
                }
               
                if(RadioButtonListRBTherapeuticSurg.SelectedIndex == -1)
                {
                LabelRBTherapeuticSurg.Visible = true;
                ChartCompleted = false;
                }
                if(RadioButtonListRBPatientCounseled.SelectedIndex == -1)
                {
                LabelRBPatientCounseled.Visible = true;
                ChartCompleted = false;
                }



                if(DropDownListMonthOfBirth.SelectedIndex == 0)
                {
                LabelMonthOfBirth.Visible = true;
                ChartCompleted = false;
                }
                if(DropDownListYearOfBirth.SelectedIndex == 0)
                {
                LabelYearOfBirth.Visible = true;
                ChartCompleted = false;
                }
                if(DropDownListMonthOfExam.SelectedIndex == 0)
                {
                LabelMonthOfExam.Visible = true;
                ChartCompleted = false;
                }
                if(DropDownListYearOfExam.SelectedIndex == 0)
                {
                LabelYearOfExam.Visible = true;
                ChartCompleted = false;
                }
                if(DropDownListBCVA.SelectedIndex == 0)
                {
                LabelBCVA.Visible = true;
                ChartCompleted = false;
                }
                if(DropDownListFinalBCVA.SelectedIndex == 0)
                {
                LabelFinalBCVA.Visible = true;
                ChartCompleted = false;
                }
                if (string.IsNullOrEmpty(TextBoxPainLevel.Text) && CheckBoxPainLevelNotDocumented.Checked==false)
                {
                LabelPainLevel.Visible = true;
                ChartCompleted = false;
                }
                if( string.IsNullOrEmpty(TextBoxDaysUntilInitialVisit.Text))
                {
                LabelDaysUntilInitialVisit.Visible = true;
                ChartCompleted = false;
                }
                if( string.IsNullOrEmpty(TextBoxDurationInitialFinalMonths.Text))
                {
                LabelDurationInitialFinalMonths.Visible = true;
                ChartCompleted = false;
                }



                if(RadioButtonAntimicrobialTherapyNo.Checked == false && RadioButtonAntimicrobialTherapyYes.Checked == false) 
                {
                LabelAntimicrobialTherapy.Visible = true;
                ChartCompleted = false;
                }
                if(RadioButtonDiagnosticCornealScrapingNo.Checked == false && RadioButtonDiagnosticCornealScrapingYes.Checked == false) 
                {
                LabelDiagnosticCornealScraping.Visible = true;
                ChartCompleted = false;
                }
                if(RadioButtonWorkingDiagnosisNo.Checked == false && RadioButtonWorkingDiagnosisYes.Checked == false) 
                {
                LabelWorkingDiagnosis.Visible = true;
                ChartCompleted = false;
                }
                //if(RadioButtonInitialTherapyRationaleDocNo.Checked == false && RadioButtonInitialTherapyRationaleDocYes.Checked == false) 
                //{
                //LabelInitialTherapyRationaleDoc.Visible = true;
                //ChartCompleted = false;
                //}
                if(RadioButtonChangeMedNo.Checked == false && RadioButtonChangeMedYes.Checked == false) 
                {
                LabelChangeMed.Visible = true;
                ChartCompleted = false;
                }
                if(RadioButtonMedTopicalCorticosteroidsNo.Checked == false && RadioButtonMedTopicalCorticosteroidsYes.Checked == false) 
                {
                LabelMedTopicalCorticosteroids.Visible = true;
                ChartCompleted = false;
                }
                if(RadioButtonCornealPerforationNo.Checked == false && RadioButtonCornealPerforationYes.Checked == false) 
                {
                LabelCornealPerforation.Visible = true;
                ChartCompleted = false;
                }
                if(RadioButtonSurgicalIntOpticalRehabNo.Checked == false && RadioButtonSurgicalIntOpticalRehabYes.Checked == false) 
                {
                LabelSurgicalIntOpticalRehab.Visible = true;
                ChartCompleted = false;
                }











         return ChartCompleted;
     }








}