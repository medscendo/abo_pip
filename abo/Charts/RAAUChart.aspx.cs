﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Transactions;
using NetHealthPIMModel;

public partial class abo_RAAUChart : BasePage
{
    protected void Page_Load(object sender, EventArgs e)
    {

        if (!IsPostBack)
        {
            InitializePage();
        }


    }
    protected void InitializePage()
    {
        ((PIMMasterPage)Page.Master).SetTitle("Chart");

        using (NetHealthPIMEntities pim = new NetHealthPIMEntities())
        {
            int ModuleID = (int)Session[Constants.SESSION_WORKINGMODULEID];
            Module module = pim.Module.First(c => c.ModuleID == ModuleID);
            ((PIMMasterPage)Page.Master).SetHeader(module.ModuleName + " Chart Abstraction");

            ParticipantModuleCycle prev = (ParticipantModuleCycle)Session[Constants.SESSION_PREVIOUSCYCLE];
            String CycleNumber = Request.QueryString["CycleNumber"];

            int cycleID = ctx.ActiveModuleCycleID;
            if (CycleNumber == "1")
            {
                cycleID = prev.ParticipantModuleCycleID;
                LinkButtonBackToDashboard.Visible = true;
                ButtonSubmit.Visible = false;
            }

            ParticipantModuleCycle cycle = pim.ParticipantModuleCycle.First(c => c.ParticipantModuleCycleID == cycleID);
            String numberOfRecord = Request.QueryString["nr"];
            String totalRecords = Request.QueryString["t"];
            if (cycle.CycleNumber == 1)
            {
                ((PIMMasterPage)Page.Master).SetStatus(2);
                LiteralAbstractionNumber.Text = "Initial Abstraction: Chart " + numberOfRecord + " of " + totalRecords;
            }
            else
            {
                ((PIMMasterPage)Page.Master).SetStatus(3);
                LiteralAbstractionNumber.Text = "Second Abstraction: Chart " + numberOfRecord + " of " + totalRecords;
            }
            DropDownListMonthOfBirth.DataSource = CycleManager.getMonthList();
            DropDownListYearOfBirth.DataSource = CycleManager.getDOBYearList(0, 100);
            DropDownListMonthOfOnsetFirstEpisode.DataSource = CycleManager.getMonthList();
            DropDownListYearOfOnsetFirstEpisode.DataSource = CycleManager.getDOBYearList(0, 100);
            DropDownListMonthOfOnsetCurrentEpisode.DataSource = CycleManager.getMonthList();
            DropDownListYearOfOnsetCurrentEpisode.DataSource = CycleManager.getDOBYearList(0, 100);
            DropDownListMonthOfOnsetLastEpisode.DataSource = CycleManager.getMonthList();
            DropDownListYearOfOnsetLastEpisode.DataSource = CycleManager.getDOBYearList(0, 100);
            DropDownListPreviousTreatmentImmuStartMonth.DataSource = CycleManager.getMonthList();
            DropDownListPreviousTreatmentImmuStartYear.DataSource = CycleManager.getDOBYearList(0, 100);
            DropDownListPreviousTreatmentImmuStopMonth.DataSource = CycleManager.getMonthList();
            DropDownListPreviousTreatmentImmuStopYear.DataSource = CycleManager.getDOBYearList(0, 100);
            DropDownListBCVAOD.DataSource = CycleManager.getExamValues();
            DropDownListBCVAOS.DataSource = CycleManager.getExamValues();
            DropDownListOutcomeRemissionMonth.DataSource = CycleManager.getMonthList();
            DropDownListOutcomeRemissionYear.DataSource = CycleManager.getDOBYearList(0, 100);
            DropDownListOutcomeBCVAOD.DataSource = CycleManager.getExamValues();
            DropDownListOutcomeBCVAOS.DataSource = CycleManager.getExamValues();
            DropDownListOutcomeGonioscopyMonth.DataSource = CycleManager.getMonthList();
            DropDownListOutcomeGonioscopyYear.DataSource = CycleManager.getDOBYearList(0, 100);




            DataBind();
            DropDownListYearOfBirth.Items.Insert(0, new ListItem("Year", "0"));
            DropDownListYearOfOnsetFirstEpisode.Items.Insert(0, new ListItem("Year", "0"));
            DropDownListYearOfOnsetCurrentEpisode.Items.Insert(0, new ListItem("Year", "0"));
            DropDownListYearOfOnsetLastEpisode.Items.Insert(0, new ListItem("Year", "0"));
            DropDownListPreviousTreatmentImmuStartYear.Items.Insert(0, new ListItem("Year", "0"));
            DropDownListPreviousTreatmentImmuStopYear.Items.Insert(0, new ListItem("Year", "0"));
            DropDownListOutcomeRemissionYear.Items.Insert(0, new ListItem("Year", "0"));
            DropDownListOutcomeGonioscopyYear.Items.Insert(0, new ListItem("Year", "0"));

            string recordIdentifier = Request.QueryString["ri"];
            HiddenFieldRecordIdentifier.Value = recordIdentifier;
            LiteralRecordIdentifier.Text = recordIdentifier;
            var count = pim.ChartAbstractionRAAAU.Where(ps => ps.ParticipantModuleCycle.ParticipantModuleCycleID == cycleID & ps.RecordIdentifier == recordIdentifier).Count();
            if (count == 0)
            {
                using (TransactionScope transaction = new TransactionScope())
                {
                    //Module module = pim.Module.First(m => m.ModuleID == Constants.ABO_AMBLYOPIA_MODULEID);

                    NetHealthPIMModel.ChartRegistration chartRegistration = (from cr in pim.ChartRegistration
                                                           where cr.ParticipantModuleCycleID == cycleID
                                                           & cr.RecordIdentifier == recordIdentifier
                                                           & cr.ModuleID == 34
                                                           select cr).First<NetHealthPIMModel.ChartRegistration>();

                    DateTime initialVisit = Convert.ToDateTime(chartRegistration.InitialVisit);
                    DateTime DOB = Convert.ToDateTime(chartRegistration.DOB);


                    ChartAbstractionRAAAU abstractRecord = new ChartAbstractionRAAAU();
                    abstractRecord.ParticipantModuleCycle = cycle;
                    abstractRecord.RecordIdentifier = recordIdentifier;
                    abstractRecord.MonthOfBirth = DOB.Month;
                    abstractRecord.YearOfBirth = DOB.Year;
                    DropDownListYearOfBirth.SelectedValue = abstractRecord.YearOfBirth.ToString();
                    DropDownListMonthOfBirth.SelectedValue = abstractRecord.MonthOfBirth.ToString();

                    abstractRecord.CreatedOn = DateTime.Now;
                    abstractRecord.LastUpdateDate = DateTime.Now;
                    pim.SaveChanges();

                    transaction.Complete();
                }
            }
            else
            {

                ChartAbstractionRAAAU abstractRecord = (from c in pim.ChartAbstractionRAAAU
                                                     where c.ParticipantModuleCycle.ParticipantModuleCycleID == cycleID & c.RecordIdentifier == recordIdentifier
                                                       select c).First<ChartAbstractionRAAAU>();


                if (abstractRecord.PresentingSymptomPain != null) CheckBoxPresentingSymptomPain.Checked = ((bool)abstractRecord.PresentingSymptomPain);
                if (abstractRecord.PresentingSymptomPhotophobia != null) CheckBoxPresentingSymptomPhotophobia.Checked = ((bool)abstractRecord.PresentingSymptomPhotophobia);
                if (abstractRecord.PresentingSymptomBlurredVision != null) CheckBoxPresentingSymptomBlurredVision.Checked = ((bool)abstractRecord.PresentingSymptomBlurredVision);
                if (abstractRecord.PresentingSymptomFloaters != null) CheckBoxPresentingSymptomFloaters.Checked = ((bool)abstractRecord.PresentingSymptomFloaters);
                if (abstractRecord.PresentingSymptomRedness != null) CheckBoxPresentingSymptomRedness.Checked = ((bool)abstractRecord.PresentingSymptomRedness);
                if (abstractRecord.OutcomeOCTCentralSubfieldThicknessNP != null) CheckBoxOutcomeOCTCentralSubfieldThicknessNP.Checked = ((bool)abstractRecord.OutcomeOCTCentralSubfieldThicknessNP);
                if (abstractRecord.OutcomeRemissionNA != null) CheckBoxOutcomeRemissionNA.Checked = ((bool)abstractRecord.OutcomeRemissionNA);

               


                if (abstractRecord.OutcomeGonioscopy != null) RadioButtonListOutcomeGonioscopy.SelectedValue = abstractRecord.OutcomeGonioscopy.ToString();
                  
                
                if (abstractRecord.RBDiagnosisFirstEpisode != null) RadioButtonListRBDiagnosisFirstEpisode.SelectedValue = abstractRecord.RBDiagnosisFirstEpisode.ToString();
                if (abstractRecord.RBLaterality != null) RadioButtonListRBLaterality.SelectedValue = abstractRecord.RBLaterality.ToString();
                if (abstractRecord.RBLastEpisodeEye != null) RadioButtonListRBLastEpisodeEye.SelectedValue = abstractRecord.RBLastEpisodeEye.ToString();
                if (abstractRecord.RBGender != null) RadioButtonListRBGender.SelectedValue = abstractRecord.RBGender.ToString();
                if (abstractRecord.RBHistoryFamily != null) RadioButtonListRBHistoryFamily.SelectedValue = abstractRecord.RBHistoryFamily.ToString();
                if (abstractRecord.RBHistoryLowBackPain != null) RadioButtonListRBHistoryLowBackPain.SelectedValue = abstractRecord.RBHistoryLowBackPain.ToString();
                if (abstractRecord.RBHistorySIJointFilms != null) RadioButtonListRBHistorySIJointFilms.SelectedValue = abstractRecord.RBHistorySIJointFilms.ToString();
                if (abstractRecord.RBHistoryDiagnosis != null) RadioButtonListRBHistoryDiagnosis.SelectedValue = abstractRecord.RBHistoryDiagnosis.ToString();
                if (abstractRecord.RBHistorySarcoidosis != null) RadioButtonListRBHistorySarcoidosis.SelectedValue = abstractRecord.RBHistorySarcoidosis.ToString();
                if (abstractRecord.RBHistorySyphilis != null) RadioButtonListRBHistorySyphilis.SelectedValue = abstractRecord.RBHistorySyphilis.ToString();
                if (abstractRecord.RBHistoryTuberculosis != null) RadioButtonListRBHistoryTuberculosis.SelectedValue = abstractRecord.RBHistoryTuberculosis.ToString();
                if (abstractRecord.RBHistorySmoking != null) RadioButtonListRBHistorySmoking.SelectedValue = abstractRecord.RBHistorySmoking.ToString();
                if (abstractRecord.RBBCVAType != null) RadioButtonListRBBCVAType.SelectedValue = abstractRecord.RBBCVAType.ToString();
                if (abstractRecord.RBEpithelialDiseaseDetected != null) RadioButtonListRBEpithelialDiseaseDetected.SelectedValue = abstractRecord.RBEpithelialDiseaseDetected.ToString();
                if (abstractRecord.RBKPAppearance != null) RadioButtonListRBKPAppearance.SelectedValue = abstractRecord.RBKPAppearance.ToString();
                if (abstractRecord.RBKPLocation != null) RadioButtonListRBKPLocation.SelectedValue = abstractRecord.RBKPLocation.ToString();
                if (abstractRecord.RBAnteriorChamberDepth != null) RadioButtonListRBAnteriorChamberDepth.SelectedValue = abstractRecord.RBAnteriorChamberDepth.ToString();
                if (abstractRecord.RBAnteriorChamberFlare != null) RadioButtonListRBAnteriorChamberFlare.SelectedValue = abstractRecord.RBAnteriorChamberFlare.ToString();
                if (abstractRecord.RBAnteriorChamberCriteria != null) RadioButtonListRBAnteriorChamberCriteria.SelectedValue = abstractRecord.RBAnteriorChamberCriteria.ToString();
              
                if (abstractRecord.RBIrisAtrophy != null) RadioButtonListRBIrisAtrophy.SelectedValue = abstractRecord.RBIrisAtrophy.ToString();
                if (abstractRecord.RBIrisNodulesPresent != null) RadioButtonListRBIrisNodulesPresent.SelectedValue = abstractRecord.RBIrisNodulesPresent.ToString();
                if (abstractRecord.RBIrisNoduleID != null) RadioButtonListRBIrisNoduleID.SelectedValue = abstractRecord.RBIrisNoduleID.ToString();
                if (abstractRecord.RBGonioscopyNumberClockHours != null) RadioButtonListRBGonioscopyNumberClockHours.SelectedValue = abstractRecord.RBGonioscopyNumberClockHours.ToString();
                if (abstractRecord.RBVitreousHaze != null) RadioButtonListRBVitreousHaze.SelectedValue = abstractRecord.RBVitreousHaze.ToString();
                if (abstractRecord.RBChorioretinalPathology != null) RadioButtonListRBChorioretinalPathology.SelectedValue = abstractRecord.RBChorioretinalPathology.ToString();
                if (abstractRecord.RBChorioretinalRVSheathing != null) RadioButtonListRBChorioretinalRVSheathing.SelectedValue = abstractRecord.RBChorioretinalRVSheathing.ToString();
                if (abstractRecord.RBChorioretinalRVActive != null) RadioButtonListRBChorioretinalRVActive.SelectedValue = abstractRecord.RBChorioretinalRVActive.ToString();
                if (abstractRecord.RBChorioretinalRetinitis != null) RadioButtonListRBChorioretinalRetinitis.SelectedValue = abstractRecord.RBChorioretinalRetinitis.ToString();
                if (abstractRecord.RBChorioretinalRetinalNeovasc != null) RadioButtonListRBChorioretinalRetinalNeovasc.SelectedValue = abstractRecord.RBChorioretinalRetinalNeovasc.ToString();
                if (abstractRecord.RBChoroiditis != null) RadioButtonListRBChoroiditis.SelectedValue = abstractRecord.RBChoroiditis.ToString();
                if (abstractRecord.RBChoroidalNeovascularization != null) RadioButtonListRBChoroidalNeovascularization.SelectedValue = abstractRecord.RBChoroidalNeovascularization.ToString();
                if (abstractRecord.RBRetinalTear != null) RadioButtonListRBRetinalTear.SelectedValue = abstractRecord.RBRetinalTear.ToString();
                if (abstractRecord.RBRetinalDetachment != null) RadioButtonListRBRetinalDetachment.SelectedValue = abstractRecord.RBRetinalDetachment.ToString();
                if (abstractRecord.RBTestFTAABS != null) RadioButtonListRBTestFTAABS.SelectedValue = abstractRecord.RBTestFTAABS.ToString();
                if (abstractRecord.RBTestMHATP != null) RadioButtonListRBTestMHATP.SelectedValue = abstractRecord.RBTestMHATP.ToString();
                if (abstractRecord.RBTestHLAB27 != null) RadioButtonListRBTestHLAB27.SelectedValue = abstractRecord.RBTestHLAB27.ToString();
                if (abstractRecord.RBTestACE != null) RadioButtonListRBTestACE.SelectedValue = abstractRecord.RBTestACE.ToString();
                if (abstractRecord.RBTestChest != null) RadioButtonListRBTestChest.SelectedValue = abstractRecord.RBTestChest.ToString();
                if (abstractRecord.RBTestTB != null) RadioButtonListRBTestTB.SelectedValue = abstractRecord.RBTestTB.ToString();
                if (abstractRecord.RBTestPPD != null) RadioButtonListRBTestPPD.SelectedValue = abstractRecord.RBTestPPD.ToString();
                if (abstractRecord.RBTestQuantiferonTB != null) RadioButtonListRBTestQuantiferonTB.SelectedValue = abstractRecord.RBTestQuantiferonTB.ToString();
                if (abstractRecord.RBTopical != null) RadioButtonListRBTopical.SelectedValue = abstractRecord.RBTopical.ToString();
                if (abstractRecord.RBPeriocularInjections != null) RadioButtonListRBPeriocularInjections.SelectedValue = abstractRecord.RBPeriocularInjections.ToString();
                if (abstractRecord.RBIntravitrealInjectionsForAnterior != null) RadioButtonListRBIntravitrealInjectionsForAnterior.SelectedValue = abstractRecord.RBIntravitrealInjectionsForAnterior.ToString();
                if (abstractRecord.RBTherapySystemicDrug != null) RadioButtonListRBTherapySystemicDrug.SelectedValue = abstractRecord.RBTherapySystemicDrug.ToString();
                if (abstractRecord.RBCycloplegicsDrug != null) RadioButtonListRBCycloplegicsDrug.SelectedValue = abstractRecord.RBCycloplegicsDrug.ToString();
                if (abstractRecord.RBCycloplegicsFrequency != null) RadioButtonListRBCycloplegicsFrequency.SelectedValue = abstractRecord.RBCycloplegicsFrequency.ToString();
                if (abstractRecord.RBImmunPrescribedBy != null) RadioButtonListRBImmunPrescribedBy.SelectedValue = abstractRecord.RBImmunPrescribedBy.ToString();
                if (abstractRecord.RBOutcomeBCVAType != null) RadioButtonListRBOutcomeBCVAType.SelectedValue = abstractRecord.RBOutcomeBCVAType.ToString();
                if (abstractRecord.RBOutcomeKPsAppearance != null) RadioButtonListRBOutcomeKPsAppearance.SelectedValue = abstractRecord.RBOutcomeKPsAppearance.ToString();
                if (abstractRecord.RBOutcomeKPsLocation != null) RadioButtonListRBOutcomeKPsLocation.SelectedValue = abstractRecord.RBOutcomeKPsLocation.ToString();
                if (abstractRecord.RBOutcomeAnteriorDepth != null) RadioButtonListRBOutcomeAnteriorDepth.SelectedValue = abstractRecord.RBOutcomeAnteriorDepth.ToString();
                if (abstractRecord.RBOutcomeAnteriorFlare != null) RadioButtonListRBOutcomeAnteriorFlare.SelectedValue = abstractRecord.RBOutcomeAnteriorFlare.ToString();
                if (abstractRecord.RBOutcomeAnteriorCells != null) RadioButtonListRBOutcomeAnteriorCells.SelectedValue = abstractRecord.RBOutcomeAnteriorCells.ToString();
                if (abstractRecord.RBOutcomePosteriorSynechiae != null) RadioButtonListRBOutcomePosteriorSynechiae.SelectedValue = abstractRecord.RBOutcomePosteriorSynechiae.ToString();
                if (abstractRecord.RBOutcomeGonioscopyRubeosis != null) RadioButtonListRBOutcomeGonioscopyRubeosis.SelectedValue = abstractRecord.RBOutcomeGonioscopyRubeosis.ToString();
                if (abstractRecord.RBOutcomeGonioscopyInflam != null) RadioButtonListRBOutcomeGonioscopyInflam.SelectedValue = abstractRecord.RBOutcomeGonioscopyInflam.ToString();
                if (abstractRecord.RBOutcomeGonioscopyClockHrs != null) RadioButtonListRBOutcomeGonioscopyClockHrs.SelectedValue = abstractRecord.RBOutcomeGonioscopyClockHrs.ToString();
                if (abstractRecord.RBOutcomeVitreousCells != null) RadioButtonListRBOutcomeVitreousCells.SelectedValue = abstractRecord.RBOutcomeVitreousCells.ToString();
                if (abstractRecord.RBOutcomeChorioretinalPathology != null) RadioButtonListRBOutcomeChorioretinalPathology.SelectedValue = abstractRecord.RBOutcomeChorioretinalPathology.ToString();
                if (abstractRecord.RBOutcomeChorioretinalRVSheathing != null) RadioButtonListRBOutcomeChorioretinalRVSheathing.SelectedValue = abstractRecord.RBOutcomeChorioretinalRVSheathing.ToString();
                if (abstractRecord.RBOutcomeChorioretinalRVActive != null) RadioButtonListRBOutcomeChorioretinalRVActive.SelectedValue = abstractRecord.RBOutcomeChorioretinalRVActive.ToString();
                if (abstractRecord.RBOutcomeChorioretinalRetinitis != null) RadioButtonListRBOutcomeChorioretinalRetinitis.SelectedValue = abstractRecord.RBOutcomeChorioretinalRetinitis.ToString();
                if (abstractRecord.RBOutcomeChorioretinalRetinalNeovasc != null) RadioButtonListRBOutcomeChorioretinalRetinalNeovasc.SelectedValue = abstractRecord.RBOutcomeChorioretinalRetinalNeovasc.ToString();
                if (abstractRecord.RBOutcomeChoroiditis != null) RadioButtonListRBOutcomeChoroiditis.SelectedValue = abstractRecord.RBOutcomeChoroiditis.ToString();
                if (abstractRecord.RBOutcomeChoroidalNeovascularization != null) RadioButtonListRBOutcomeChoroidalNeovascularization.SelectedValue = abstractRecord.RBOutcomeChoroidalNeovascularization.ToString();
                if (abstractRecord.RBOutcomeRetinalTear != null) RadioButtonListRBOutcomeRetinalTear.SelectedValue = abstractRecord.RBOutcomeRetinalTear.ToString();
                if (abstractRecord.RBOutcomeRetinalDetachment != null) RadioButtonListRBOutcomeRetinalDetachment.SelectedValue = abstractRecord.RBOutcomeRetinalDetachment.ToString();
                if (abstractRecord.RBOutcomeInitiatedMgntUnderlying != null) RadioButtonListRBOutcomeInitiatedMgntUnderlying.SelectedValue = abstractRecord.RBOutcomeInitiatedMgntUnderlying.ToString();


                if (abstractRecord.RBtPriorEpisodes != null) RadioButtonListPriorEpisodes.SelectedValue = abstractRecord.RBtPriorEpisodes.ToString();
                if (abstractRecord.RBScleritis != null) RadioButtonListScleritis.SelectedValue = abstractRecord.RBScleritis.ToString();
                if (abstractRecord.RBVitreousCells != null) RadioButtonListVitreousCells.SelectedValue = abstractRecord.RBVitreousCells.ToString();
                if (abstractRecord.RBRPR != null) RadioButtonListRPR.SelectedValue = abstractRecord.RBRPR.ToString();
                if (abstractRecord.RBVEGFGivenForCME != null) RadioButtonListVEGFGivenForCME.SelectedValue = abstractRecord.RBVEGFGivenForCME.ToString();


                if (abstractRecord.MonthOfBirth != null) DropDownListMonthOfBirth.SelectedValue = abstractRecord.MonthOfBirth.ToString();
                if (abstractRecord.YearOfBirth != null) DropDownListYearOfBirth.SelectedValue = abstractRecord.YearOfBirth.ToString();
                if (abstractRecord.MonthOfOnsetFirstEpisode != null) DropDownListMonthOfOnsetFirstEpisode.SelectedValue = abstractRecord.MonthOfOnsetFirstEpisode.ToString();
                if (abstractRecord.YearOfOnsetFirstEpisode != null) DropDownListYearOfOnsetFirstEpisode.SelectedValue = abstractRecord.YearOfOnsetFirstEpisode.ToString();
                if (abstractRecord.MonthOfOnsetCurrentEpisode != null) DropDownListMonthOfOnsetCurrentEpisode.SelectedValue = abstractRecord.MonthOfOnsetCurrentEpisode.ToString();
                if (abstractRecord.YearOfOnsetCurrentEpisode != null) DropDownListYearOfOnsetCurrentEpisode.SelectedValue = abstractRecord.YearOfOnsetCurrentEpisode.ToString();
                if (abstractRecord.MonthOfOnsetLastEpisode != null) DropDownListMonthOfOnsetLastEpisode.SelectedValue = abstractRecord.MonthOfOnsetLastEpisode.ToString();
                if (abstractRecord.YearOfOnsetLastEpisode != null) DropDownListYearOfOnsetLastEpisode.SelectedValue = abstractRecord.YearOfOnsetLastEpisode.ToString();
                if (abstractRecord.PreviousTreatmentImmuStartMonth != null) DropDownListPreviousTreatmentImmuStartMonth.SelectedValue = abstractRecord.PreviousTreatmentImmuStartMonth.ToString();
                if (abstractRecord.PreviousTreatmentImmuStartYear != null) DropDownListPreviousTreatmentImmuStartYear.SelectedValue = abstractRecord.PreviousTreatmentImmuStartYear.ToString();
                if (abstractRecord.PreviousTreatmentImmuStopMonth != null) DropDownListPreviousTreatmentImmuStopMonth.SelectedValue = abstractRecord.PreviousTreatmentImmuStopMonth.ToString();
                if (abstractRecord.PreviousTreatmentImmuStopYear != null) DropDownListPreviousTreatmentImmuStopYear.SelectedValue = abstractRecord.PreviousTreatmentImmuStopYear.ToString();
                if (abstractRecord.BCVAOD != null) DropDownListBCVAOD.SelectedValue = abstractRecord.BCVAOD.ToString();
                if (abstractRecord.BCVAOS != null) DropDownListBCVAOS.SelectedValue = abstractRecord.BCVAOS.ToString();
                if (abstractRecord.OutcomeRemissionMonth != null) DropDownListOutcomeRemissionMonth.SelectedValue = abstractRecord.OutcomeRemissionMonth.ToString();
                if (abstractRecord.OutcomeRemissionYear != null) DropDownListOutcomeRemissionYear.SelectedValue = abstractRecord.OutcomeRemissionYear.ToString();
                if (abstractRecord.OutcomeBCVAOD != null) DropDownListOutcomeBCVAOD.SelectedValue = abstractRecord.OutcomeBCVAOD.ToString();
                if (abstractRecord.OutcomeBCVAOS != null) DropDownListOutcomeBCVAOS.SelectedValue = abstractRecord.OutcomeBCVAOS.ToString();
                if (abstractRecord.OutcomeGonioscopyMonth != null) DropDownListOutcomeGonioscopyMonth.SelectedValue = abstractRecord.OutcomeGonioscopyMonth.ToString();
                if (abstractRecord.OutcomeGonioscopyYear != null) DropDownListOutcomeGonioscopyYear.SelectedValue = abstractRecord.OutcomeGonioscopyYear.ToString();

                  if (abstractRecord.RBAnteriorHypopyon != null) 
                  {
                      RadioButtonListRBAnteriorHypopyonYes.Checked = ((bool)abstractRecord.RBAnteriorHypopyon);
                      RadioButtonListRBAnteriorHypopyonNo.Checked =! ((bool)abstractRecord.RBAnteriorHypopyon);
                  }

                if (abstractRecord.PreviousTreatmentTopicalCS != null)
                {
                    RadioButtonPreviousTreatmentTopicalCSYes.Checked = ((bool)abstractRecord.PreviousTreatmentTopicalCS);
                    RadioButtonPreviousTreatmentTopicalCSNo.Checked = !((bool)abstractRecord.PreviousTreatmentTopicalCS);
                }
                if (abstractRecord.PreviousTreatmentPeriocularCS != null)
                {
                    RadioButtonPreviousTreatmentPeriocularCSYes.Checked = ((bool)abstractRecord.PreviousTreatmentPeriocularCS);
                    RadioButtonPreviousTreatmentPeriocularCSNo.Checked = !((bool)abstractRecord.PreviousTreatmentPeriocularCS);
                }
                if (abstractRecord.PreviousTreatmentSystemicCS != null)
                {
                    RadioButtonPreviousTreatmentSystemicCSYes.Checked = ((bool)abstractRecord.PreviousTreatmentSystemicCS);
                    RadioButtonPreviousTreatmentSystemicCSNo.Checked = !((bool)abstractRecord.PreviousTreatmentSystemicCS);
                }
               
                if (abstractRecord.ConjunctivalInjection != null)
                {
                    RadioButtonConjunctivalInjectionYes.Checked = ((bool)abstractRecord.ConjunctivalInjection);
                    RadioButtonConjunctivalInjectionNo.Checked = !((bool)abstractRecord.ConjunctivalInjection);
                }
                if (abstractRecord.ConjunctivalNodules != null)
                {
                    RadioButtonConjunctivalNodulesYes.Checked = ((bool)abstractRecord.ConjunctivalNodules);
                    RadioButtonConjunctivalNodulesNo.Checked = !((bool)abstractRecord.ConjunctivalNodules);
                }
                if (abstractRecord.DendritesPresent != null)
                {
                    RadioButtonDendritesPresentYes.Checked = ((bool)abstractRecord.DendritesPresent);
                    RadioButtonDendritesPresentNo.Checked = !((bool)abstractRecord.DendritesPresent);
                }
                if (abstractRecord.StromalScars != null)
                {
                    RadioButtonStromalScarsYes.Checked = ((bool)abstractRecord.StromalScars);
                    RadioButtonStromalScarsNo.Checked = !((bool)abstractRecord.StromalScars);
                }
                if (abstractRecord.KPs != null)
                {
                    RadioButtonKPsYes.Checked = ((bool)abstractRecord.KPs);
                    RadioButtonKPsNo.Checked = !((bool)abstractRecord.KPs);
                }
                if (abstractRecord.IrisHeterochromia != null)
                {
                    RadioButtonIrisHeterochromiaYes.Checked = ((bool)abstractRecord.IrisHeterochromia);
                    RadioButtonIrisHeterochromiaNo.Checked = !((bool)abstractRecord.IrisHeterochromia);
                }
                if (abstractRecord.IrisPreviousPeripheralIridotomy != null)
                {
                    RadioButtonIrisPreviousPeripheralIridotomyYes.Checked = ((bool)abstractRecord.IrisPreviousPeripheralIridotomy);
                    RadioButtonIrisPreviousPeripheralIridotomyNo.Checked = !((bool)abstractRecord.IrisPreviousPeripheralIridotomy);
                }
                if (abstractRecord.GonioscopyPerformed != null)
                {
                    RadioButtonGonioscopyPerformedYes.Checked = ((bool)abstractRecord.GonioscopyPerformed);
                    RadioButtonGonioscopyPerformedNo.Checked = !((bool)abstractRecord.GonioscopyPerformed);
                }
                if (abstractRecord.LensPigment != null)
                {
                    RadioButtonLensPigmentYes.Checked = ((bool)abstractRecord.LensPigment);
                    RadioButtonLensPigmentNo.Checked = !((bool)abstractRecord.LensPigment);
                }
                if (abstractRecord.LensCataractPresent != null)
                {
                    RadioButtonLensCataractPresentYes.Checked = ((bool)abstractRecord.LensCataractPresent);
                    RadioButtonLensCataractPresentNo.Checked = !((bool)abstractRecord.LensCataractPresent);
                }
                if (abstractRecord.ScleritisNodular != null)
                {
                    RadioButtonScleritisNodularYes.Checked = ((bool)abstractRecord.ScleritisNodular);
                    RadioButtonScleritisNodularNo.Checked = !((bool)abstractRecord.ScleritisNodular);
                }
                if (abstractRecord.ScleritisDiffuse != null)
                {
                    RadioButtonScleritisDiffuseYes.Checked = ((bool)abstractRecord.ScleritisDiffuse);
                    RadioButtonScleritisDiffuseNo.Checked = !((bool)abstractRecord.ScleritisDiffuse);
                }
                if (abstractRecord.ScleritisNecrotizing != null)
                {
                    RadioButtonScleritisNecrotizingYes.Checked = ((bool)abstractRecord.ScleritisNecrotizing);
                    RadioButtonScleritisNecrotizingNo.Checked = !((bool)abstractRecord.ScleritisNecrotizing);
                }
            
                if (abstractRecord.OpticDiscEdemaPresent != null)
                {
                    RadioButtonOpticDiscEdemaPresentYes.Checked = ((bool)abstractRecord.OpticDiscEdemaPresent);
                    RadioButtonOpticDiscEdemaPresentNo.Checked = !((bool)abstractRecord.OpticDiscEdemaPresent);
                }
                if (abstractRecord.OpticDiscGlaucomatousChanges != null)
                {
                    RadioButtonOpticDiscGlaucomatousChangesYes.Checked = ((bool)abstractRecord.OpticDiscGlaucomatousChanges);
                    RadioButtonOpticDiscGlaucomatousChangesNo.Checked = !((bool)abstractRecord.OpticDiscGlaucomatousChanges);
                }
                if (abstractRecord.DilatedFundusExamPerformed != null)
                {
                    RadioButtonDilatedFundusExamPerformedYes.Checked = ((bool)abstractRecord.DilatedFundusExamPerformed);
                    RadioButtonDilatedFundusExamPerformedNo.Checked = !((bool)abstractRecord.DilatedFundusExamPerformed);
                }
                if (abstractRecord.CMEPresent != null)
                {
                    RadioButtonCMEPresentYes.Checked = ((bool)abstractRecord.CMEPresent);
                    RadioButtonCMEPresentNo.Checked = !((bool)abstractRecord.CMEPresent);
                }
                if (abstractRecord.FluoresceinPerformed != null)
                {
                    RadioButtonFluoresceinPerformedYes.Checked = ((bool)abstractRecord.FluoresceinPerformed);
                    RadioButtonFluoresceinPerformedNo.Checked = !((bool)abstractRecord.FluoresceinPerformed);
                }
                if (abstractRecord.ImmunTherapyOngoing != null)
                {
                    RadioButtonImmunTherapyOngoingYes.Checked = ((bool)abstractRecord.ImmunTherapyOngoing);
                    RadioButtonImmunTherapyOngoingNo.Checked = !((bool)abstractRecord.ImmunTherapyOngoing);
                }
                if (abstractRecord.SmokingCessationDiscussed != null)
                {
                    RadioButtonSmokingCessationDiscussedYes.Checked = ((bool)abstractRecord.SmokingCessationDiscussed);
                    RadioButtonSmokingCessationDiscussedNo.Checked = !((bool)abstractRecord.SmokingCessationDiscussed);
                }
                if (abstractRecord.OutcomeRemission != null)
                {
                    RadioButtonOutcomeRemissionYes.Checked = ((bool)abstractRecord.OutcomeRemission);
                    RadioButtonOutcomeRemissionNo.Checked = !((bool)abstractRecord.OutcomeRemission);
                }
                if (abstractRecord.OutcomeStromalScarts != null)
                {
                    RadioButtonOutcomeStromalScartsYes.Checked = ((bool)abstractRecord.OutcomeStromalScarts);
                    RadioButtonOutcomeStromalScartsNo.Checked = !((bool)abstractRecord.OutcomeStromalScarts);
                }
                if (abstractRecord.OutcomeKPs != null)
                {
                    RadioButtonOutcomeKPsYes.Checked = ((bool)abstractRecord.OutcomeKPs);
                    RadioButtonOutcomeKPsNo.Checked = !((bool)abstractRecord.OutcomeKPs);
                }
                if (abstractRecord.OutcomeLensPigment != null)
                {
                    RadioButtonOutcomeLensPigmentYes.Checked = ((bool)abstractRecord.OutcomeLensPigment);
                    RadioButtonOutcomeLensPigmentNo.Checked = !((bool)abstractRecord.OutcomeLensPigment);
                }
                if (abstractRecord.OutcomeLensCataract != null)
                {
                    RadioButtonOutcomeLensCataractYes.Checked = ((bool)abstractRecord.OutcomeLensCataract);
                    RadioButtonOutcomeLensCataractNo.Checked = !((bool)abstractRecord.OutcomeLensCataract);
                }
               
                    if(abstractRecord.CornealEdema!=null)
                    {
                              RadioButtonCornealEdemaYes.Checked = ((bool)abstractRecord.CornealEdema);
                     RadioButtonCornealEdemaNo.Checked = !((bool)abstractRecord.CornealEdema);
                    
                    }

                    if (abstractRecord.OutcomeCornealEdema != null)
                    {
                        RadioButtonOutcomeCornealEdemaYes.Checked = ((bool)abstractRecord.OutcomeCornealEdema);
                        RadioButtonOutcomeCornealEdemaNo.Checked = !((bool)abstractRecord.OutcomeCornealEdema);

                    }

                if (abstractRecord.OutcomeOpticDiscEdema != null)
                {
                    RadioButtonOutcomeOpticDiscEdemaYes.Checked = ((bool)abstractRecord.OutcomeOpticDiscEdema);
                    RadioButtonOutcomeOpticDiscEdemaNo.Checked = !((bool)abstractRecord.OutcomeOpticDiscEdema);
                }
                if (abstractRecord.OutcomeOpticDiscGlaucChanges != null)
                {
                    RadioButtonOutcomeOpticDiscGlaucChangesYes.Checked = ((bool)abstractRecord.OutcomeOpticDiscGlaucChanges);
                    RadioButtonOutcomeOpticDiscGlaucChangesNo.Checked = !((bool)abstractRecord.OutcomeOpticDiscGlaucChanges);
                }
                if (abstractRecord.OutcomeDilatedFundusExamPerformed != null)
                {
                    RadioButtonOutcomeDilatedFundusExamPerformedYes.Checked = ((bool)abstractRecord.OutcomeDilatedFundusExamPerformed);
                    RadioButtonOutcomeDilatedFundusExamPerformedNo.Checked = !((bool)abstractRecord.OutcomeDilatedFundusExamPerformed);
                }
                if (abstractRecord.OutcomeCMEPresent != null)
                {
                    RadioButtonOutcomeCMEPresentYes.Checked = ((bool)abstractRecord.OutcomeCMEPresent);
                    RadioButtonOutcomeCMEPresentNo.Checked = !((bool)abstractRecord.OutcomeCMEPresent);
                }
                if (abstractRecord.OutcomeFluoresceinPerformed != null)
                {
                    RadioButtonOutcomeFluoresceinPerformedYes.Checked = ((bool)abstractRecord.OutcomeFluoresceinPerformed);
                    RadioButtonOutcomeFluoresceinPerformedNo.Checked = !((bool)abstractRecord.OutcomeFluoresceinPerformed);
                }
                if( abstractRecord.ImmunomodulatoryTherapy!=null)
                {
                    RadioButtonImmunomodulatoryTherapyYes.Checked = ((bool)abstractRecord.ImmunomodulatoryTherapy);
                    RadioButtonImmunomodulatoryTherapyNo.Checked = !((bool)abstractRecord.ImmunomodulatoryTherapy);
                }
               
                if( abstractRecord.EtiologyDetermined!=null)
                {
                    RadioButtonEtiologyDeterminedYES.Checked = ((bool)abstractRecord.EtiologyDetermined);
                    RadioButtonEtiologyDeterminedNO.Checked = !((bool)abstractRecord.EtiologyDetermined);
                }
                if (abstractRecord.PreviousTreatmentNbrInjections != null) TextBoxPreviousTreatmentNbrInjections.Text = abstractRecord.PreviousTreatmentNbrInjections.ToString();
                if (abstractRecord.PreviousTreatmentSystemicDrug != null) TextBoxPreviousTreatmentSystemicDrug.Text = abstractRecord.PreviousTreatmentSystemicDrug.ToString();
                if (abstractRecord.PreviousTreatmentSystemicNbrWeeks != null) TextBoxPreviousTreatmentSystemicNbrWeeks.Text = abstractRecord.PreviousTreatmentSystemicNbrWeeks.ToString();
                if (abstractRecord.PreviousTreatmentImmuTherapyDrug != null) TextBoxPreviousTreatmentImmuTherapyDrug.Text = abstractRecord.PreviousTreatmentImmuTherapyDrug.ToString();
                if (abstractRecord.InflammationFreeDurationWeeks != null) TextBoxInflammationFreeDurationWeeks.Text = abstractRecord.InflammationFreeDurationWeeks.ToString();
                if (abstractRecord.HistoryFamilyRelationship != null) TextBoxHistoryFamilyRelationship.Text = abstractRecord.HistoryFamilyRelationship.ToString();
                if (abstractRecord.RBIrisNoduleClockHours != null) TextBoxRBIrisNoduleClockHours.Text = abstractRecord.RBIrisNoduleClockHours.ToString();
                if (abstractRecord.TextBoxVEGFInjectionsGiven != null) TextBoxVEGFInjectionsGiven.Text = abstractRecord.TextBoxVEGFInjectionsGiven.ToString();
                if (abstractRecord.IntraocularPressure != null) TextBoxIntraocularPressure.Text = abstractRecord.IntraocularPressure.ToString();
                if (abstractRecord.OCTCentralSubfieldThickness != null) TextBoxOCTCentralSubfieldThickness.Text = abstractRecord.OCTCentralSubfieldThickness.ToString();
                if (abstractRecord.RBRetinalDetachmentOther != null) TextBoxRBRetinalDetachmentOther.Text = abstractRecord.RBRetinalDetachmentOther.ToString();
                if (abstractRecord.RBTestOther != null) TextBoxRBTestOther.Text = abstractRecord.RBTestOther.ToString();
                if (abstractRecord.RBPeriocularInjectionsCount != null) TextBoxRBPeriocularInjectionsCount.Text = abstractRecord.RBPeriocularInjectionsCount.ToString();
                if (abstractRecord.RBIntravitrealInjectionsCount != null) TextBoxRBIntravitrealInjectionsCount.Text = abstractRecord.RBIntravitrealInjectionsCount.ToString();
                if (abstractRecord.RBTherapySystemicDurationTaper != null) TextBoxRBTherapySystemicDurationTaper.Text = abstractRecord.RBTherapySystemicDurationTaper.ToString();
                if (abstractRecord.ImmunDrugDosage != null) TextBoxImmunDrugDosage.Text = abstractRecord.ImmunDrugDosage.ToString();
                if (abstractRecord.ImmunDurationTherapy != null) TextBoxImmunDurationTherapy.Text = abstractRecord.ImmunDurationTherapy.ToString();
               
                if (abstractRecord.OutcomeIrisSynechiaeExtent != null) TextBoxOutcomeIrisSynechiaeExtent.Text = abstractRecord.OutcomeIrisSynechiaeExtent.ToString();
                if (abstractRecord.OutcomeIOP != null) TextBoxOutcomeIOP.Text = abstractRecord.OutcomeIOP.ToString();
                if (abstractRecord.OutcomeOCTCentralSubfieldThickness != null) TextBoxOutcomeOCTCentralSubfieldThickness.Text = abstractRecord.OutcomeOCTCentralSubfieldThickness.ToString();

                if (abstractRecord.RBOutcomeRetinalDetachmentText != null) TextBoxOutcomeRetinalDetachmentText.Text = abstractRecord.RBOutcomeRetinalDetachmentText.ToString();


                if (abstractRecord.TextBoxTopicalOther != null) TextBoxTopicalOther.Text = abstractRecord.TextBoxTopicalOther.ToString();

                if (abstractRecord.EtiologyDeterminedText != null) EtiologyDeterminedText.Text = abstractRecord.EtiologyDeterminedText.ToString();
                if (abstractRecord.PeriocularInjectionsText != null) PeriocularInjectionsText.Text = abstractRecord.PeriocularInjectionsText.ToString();




            }
        }
    }


    public void savedata()
    {
        using (NetHealthPIMEntities pim = new NetHealthPIMEntities())
        {
            int ModuleID = (int)Session[Constants.SESSION_WORKINGMODULEID];
            int cycleID = ctx.ActiveModuleCycleID;
            ParticipantModuleCycle cycle = pim.ParticipantModuleCycle.First(c => c.ParticipantModuleCycleID == cycleID);
            string recordIdentifier = HiddenFieldRecordIdentifier.Value;
            using (TransactionScope transaction = new TransactionScope())
            {
                ChartAbstractionRAAAU abstractRecord = (from c in pim.ChartAbstractionRAAAU
                                                         where c.ParticipantModuleCycle.ParticipantModuleCycleID == cycleID & c.RecordIdentifier == recordIdentifier
                                                        select c).First<ChartAbstractionRAAAU>();



                                            abstractRecord.PresentingSymptomPain = CheckBoxPresentingSymptomPain.Checked;
                                            abstractRecord.PresentingSymptomPhotophobia = CheckBoxPresentingSymptomPhotophobia.Checked;
                                            abstractRecord.PresentingSymptomBlurredVision = CheckBoxPresentingSymptomBlurredVision.Checked;
                                            abstractRecord.PresentingSymptomFloaters = CheckBoxPresentingSymptomFloaters.Checked;
                                            abstractRecord.PresentingSymptomRedness = CheckBoxPresentingSymptomRedness.Checked;
                                            abstractRecord.OutcomeOCTCentralSubfieldThicknessNP =  CheckBoxOutcomeOCTCentralSubfieldThicknessNP.Checked;
                                            abstractRecord.OutcomeRemissionNA = CheckBoxOutcomeRemissionNA.Checked;
             

                                            if (RadioButtonListOutcomeGonioscopy.SelectedIndex!=-1) abstractRecord.OutcomeGonioscopy = Convert.ToInt32(RadioButtonListOutcomeGonioscopy.SelectedValue);
                                            else abstractRecord.OutcomeGonioscopy=0;
                                            if (RadioButtonListRBDiagnosisFirstEpisode.SelectedIndex != -1) abstractRecord.RBDiagnosisFirstEpisode = Convert.ToInt32(RadioButtonListRBDiagnosisFirstEpisode.SelectedValue);
                                            else abstractRecord.RBDiagnosisFirstEpisode = 0;
                                            if (RadioButtonListRBLaterality.SelectedIndex != -1) abstractRecord.RBLaterality = Convert.ToInt32(RadioButtonListRBLaterality.SelectedValue);
                                            else abstractRecord.RBLaterality = 0;
                                            if (RadioButtonListRBLastEpisodeEye.SelectedIndex != -1) abstractRecord.RBLastEpisodeEye = Convert.ToInt32(RadioButtonListRBLastEpisodeEye.SelectedValue);
                                            else abstractRecord.RBLastEpisodeEye = 0;
                                            if (RadioButtonListRBGender.SelectedIndex != -1) abstractRecord.RBGender = Convert.ToInt32(RadioButtonListRBGender.SelectedValue);
                                            else abstractRecord.RBGender = 0;
                                            if (RadioButtonListRBHistoryFamily.SelectedIndex != -1) abstractRecord.RBHistoryFamily = Convert.ToInt32(RadioButtonListRBHistoryFamily.SelectedValue);
                                            else abstractRecord.RBHistoryFamily = 0;
                                            if (RadioButtonListRBHistoryLowBackPain.SelectedIndex != -1) abstractRecord.RBHistoryLowBackPain = Convert.ToInt32(RadioButtonListRBHistoryLowBackPain.SelectedValue);
                                            else abstractRecord.RBHistoryLowBackPain = 0;
                                            if (RadioButtonListRBHistorySIJointFilms.SelectedIndex != -1) abstractRecord.RBHistorySIJointFilms = Convert.ToInt32(RadioButtonListRBHistorySIJointFilms.SelectedValue);
                                            else abstractRecord.RBHistorySIJointFilms = 0;
                                            if (RadioButtonListRBHistoryDiagnosis.SelectedIndex != -1) abstractRecord.RBHistoryDiagnosis = Convert.ToInt32(RadioButtonListRBHistoryDiagnosis.SelectedValue);
                                            else abstractRecord.RBHistoryDiagnosis = 0;
                                            if (RadioButtonListRBHistorySarcoidosis.SelectedIndex != -1) abstractRecord.RBHistorySarcoidosis = Convert.ToInt32(RadioButtonListRBHistorySarcoidosis.SelectedValue);
                                            else abstractRecord.RBHistorySarcoidosis = 0;
                                            if (RadioButtonListRBHistorySyphilis.SelectedIndex != -1) abstractRecord.RBHistorySyphilis = Convert.ToInt32(RadioButtonListRBHistorySyphilis.SelectedValue);
                                            else abstractRecord.RBHistorySyphilis = 0;
                                            if (RadioButtonListRBHistoryTuberculosis.SelectedIndex != -1) abstractRecord.RBHistoryTuberculosis = Convert.ToInt32(RadioButtonListRBHistoryTuberculosis.SelectedValue);
                                            else abstractRecord.RBHistoryTuberculosis = 0;
                                            if (RadioButtonListRBHistorySmoking.SelectedIndex != -1) abstractRecord.RBHistorySmoking = Convert.ToInt32(RadioButtonListRBHistorySmoking.SelectedValue);
                                            else abstractRecord.RBHistorySmoking = 0;
                                            if (RadioButtonListRBBCVAType.SelectedIndex != -1) abstractRecord.RBBCVAType = Convert.ToInt32(RadioButtonListRBBCVAType.SelectedValue);
                                            else abstractRecord.RBBCVAType = 0;
                                            if (RadioButtonListRBEpithelialDiseaseDetected.SelectedIndex != -1) abstractRecord.RBEpithelialDiseaseDetected = Convert.ToInt32(RadioButtonListRBEpithelialDiseaseDetected.SelectedValue);
                                            else abstractRecord.RBEpithelialDiseaseDetected = 0;
                                            if (RadioButtonListRBKPAppearance.SelectedIndex != -1) abstractRecord.RBKPAppearance = Convert.ToInt32(RadioButtonListRBKPAppearance.SelectedValue);
                                            else abstractRecord.RBKPAppearance = 0;
                                            if (RadioButtonListRBKPLocation.SelectedIndex != -1) abstractRecord.RBKPLocation = Convert.ToInt32(RadioButtonListRBKPLocation.SelectedValue);
                                            else abstractRecord.RBKPLocation = 0;
                                            if (RadioButtonListRBAnteriorChamberDepth.SelectedIndex != -1) abstractRecord.RBAnteriorChamberDepth = Convert.ToInt32(RadioButtonListRBAnteriorChamberDepth.SelectedValue);
                                            else abstractRecord.RBAnteriorChamberDepth = 0;
                                            if (RadioButtonListRBAnteriorChamberFlare.SelectedIndex != -1) abstractRecord.RBAnteriorChamberFlare = Convert.ToInt32(RadioButtonListRBAnteriorChamberFlare.SelectedValue);
                                            else abstractRecord.RBAnteriorChamberFlare = 0;
                                            if (RadioButtonListRBAnteriorChamberCriteria.SelectedIndex != -1) abstractRecord.RBAnteriorChamberCriteria = Convert.ToInt32(RadioButtonListRBAnteriorChamberCriteria.SelectedValue);
                                            else abstractRecord.RBAnteriorChamberCriteria = 0;


                                            if (RadioButtonListPriorEpisodes.SelectedIndex != -1) abstractRecord.RBtPriorEpisodes = Convert.ToInt32(RadioButtonListPriorEpisodes.SelectedValue);
                                            else abstractRecord.RBtPriorEpisodes = 0;
                                            if (RadioButtonListScleritis.SelectedIndex != -1) abstractRecord.RBScleritis= Convert.ToInt32(RadioButtonListScleritis.SelectedValue);
                                            else abstractRecord.RBScleritis = 0;
                                            if (RadioButtonListVitreousCells.SelectedIndex != -1) abstractRecord.RBVitreousCells= Convert.ToInt32(RadioButtonListVitreousCells.SelectedValue);
                                            else abstractRecord.RBVitreousCells = 0;
                                            if (RadioButtonListRPR.SelectedIndex != -1) abstractRecord.RBRPR= Convert.ToInt32(RadioButtonListRPR.SelectedValue);
                                            else abstractRecord.RBRPR= 0;
                                            if (RadioButtonListVEGFGivenForCME.SelectedIndex != -1) abstractRecord.RBVEGFGivenForCME = Convert.ToInt32(RadioButtonListVEGFGivenForCME.SelectedValue);
                                            else abstractRecord.RBVEGFGivenForCME = 0;




                                            if (RadioButtonListRBIrisAtrophy.SelectedIndex != -1) abstractRecord.RBIrisAtrophy = Convert.ToInt32(RadioButtonListRBIrisAtrophy.SelectedValue);
                                            else abstractRecord.RBIrisAtrophy = 0;
                                            if (RadioButtonListRBIrisNodulesPresent.SelectedIndex != -1) abstractRecord.RBIrisNodulesPresent = Convert.ToInt32(RadioButtonListRBIrisNodulesPresent.SelectedValue);
                                            else abstractRecord.RBIrisNodulesPresent = 0;
                                            if (RadioButtonListRBIrisNoduleID.SelectedIndex != -1) abstractRecord.RBIrisNoduleID = Convert.ToInt32(RadioButtonListRBIrisNoduleID.SelectedValue);
                                            else abstractRecord.RBIrisNoduleID = 0;
                                            if (RadioButtonListRBGonioscopyNumberClockHours.SelectedIndex != -1) abstractRecord.RBGonioscopyNumberClockHours = Convert.ToInt32(RadioButtonListRBGonioscopyNumberClockHours.SelectedValue);
                                            else abstractRecord.RBGonioscopyNumberClockHours = 0;
                                            if (RadioButtonListRBVitreousHaze.SelectedIndex != -1) abstractRecord.RBVitreousHaze = Convert.ToInt32(RadioButtonListRBVitreousHaze.SelectedValue);
                                            else abstractRecord.RBVitreousHaze = 0;
                                            if (RadioButtonListRBChorioretinalPathology.SelectedIndex != -1) abstractRecord.RBChorioretinalPathology = Convert.ToInt32(RadioButtonListRBChorioretinalPathology.SelectedValue);
                                            else abstractRecord.RBChorioretinalPathology = 0;
                                            if (RadioButtonListRBChorioretinalRVSheathing.SelectedIndex != -1) abstractRecord.RBChorioretinalRVSheathing = Convert.ToInt32(RadioButtonListRBChorioretinalRVSheathing.SelectedValue);
                                            else abstractRecord.RBChorioretinalRVSheathing = 0;
                                            if (RadioButtonListRBChorioretinalRVActive.SelectedIndex != -1) abstractRecord.RBChorioretinalRVActive = Convert.ToInt32(RadioButtonListRBChorioretinalRVActive.SelectedValue);
                                            else abstractRecord.RBChorioretinalRVActive = 0;
                                            if (RadioButtonListRBChorioretinalRetinitis.SelectedIndex != -1) abstractRecord.RBChorioretinalRetinitis = Convert.ToInt32(RadioButtonListRBChorioretinalRetinitis.SelectedValue);
                                            else abstractRecord.RBChorioretinalRetinitis = 0;
                                            if (RadioButtonListRBChorioretinalRetinalNeovasc.SelectedIndex != -1) abstractRecord.RBChorioretinalRetinalNeovasc = Convert.ToInt32(RadioButtonListRBChorioretinalRetinalNeovasc.SelectedValue);
                                            else abstractRecord.RBChorioretinalRetinalNeovasc = 0;
                                            if (RadioButtonListRBChoroiditis.SelectedIndex != -1) abstractRecord.RBChoroiditis = Convert.ToInt32(RadioButtonListRBChoroiditis.SelectedValue);
                                            else abstractRecord.RBChoroiditis = 0;
                                            if (RadioButtonListRBChoroidalNeovascularization.SelectedIndex != -1) abstractRecord.RBChoroidalNeovascularization = Convert.ToInt32(RadioButtonListRBChoroidalNeovascularization.SelectedValue);
                                            else abstractRecord.RBChoroidalNeovascularization = 0;
                                            if (RadioButtonListRBRetinalTear.SelectedIndex != -1) abstractRecord.RBRetinalTear = Convert.ToInt32(RadioButtonListRBRetinalTear.SelectedValue);
                                            else abstractRecord.RBRetinalTear = 0;
                                            if (RadioButtonListRBRetinalDetachment.SelectedIndex != -1) abstractRecord.RBRetinalDetachment = Convert.ToInt32(RadioButtonListRBRetinalDetachment.SelectedValue);
                                            else abstractRecord.RBRetinalDetachment = 0;
                                            if (RadioButtonListRBTestFTAABS.SelectedIndex != -1) abstractRecord.RBTestFTAABS = Convert.ToInt32(RadioButtonListRBTestFTAABS.SelectedValue);
                                            else abstractRecord.RBTestFTAABS = 0;
                                            if (RadioButtonListRBTestMHATP.SelectedIndex != -1) abstractRecord.RBTestMHATP = Convert.ToInt32(RadioButtonListRBTestMHATP.SelectedValue);
                                            else abstractRecord.RBTestMHATP = 0;
                                            if (RadioButtonListRBTestHLAB27.SelectedIndex != -1) abstractRecord.RBTestHLAB27 = Convert.ToInt32(RadioButtonListRBTestHLAB27.SelectedValue);
                                            else abstractRecord.RBTestHLAB27 = 0;
                                            if (RadioButtonListRBTestACE.SelectedIndex != -1) abstractRecord.RBTestACE = Convert.ToInt32(RadioButtonListRBTestACE.SelectedValue);
                                            else abstractRecord.RBTestACE = 0;
                                            if (RadioButtonListRBTestChest.SelectedIndex != -1) abstractRecord.RBTestChest = Convert.ToInt32(RadioButtonListRBTestChest.SelectedValue);
                                            else abstractRecord.RBTestChest = 0;
                                            if (RadioButtonListRBTestTB.SelectedIndex != -1) abstractRecord.RBTestTB = Convert.ToInt32(RadioButtonListRBTestTB.SelectedValue);
                                            else abstractRecord.RBTestTB = 0;
                                            if (RadioButtonListRBTestPPD.SelectedIndex != -1) abstractRecord.RBTestPPD = Convert.ToInt32(RadioButtonListRBTestPPD.SelectedValue);
                                            else abstractRecord.RBTestPPD = 0;
                                            if (RadioButtonListRBTestQuantiferonTB.SelectedIndex != -1) abstractRecord.RBTestQuantiferonTB = Convert.ToInt32(RadioButtonListRBTestQuantiferonTB.SelectedValue);
                                            else abstractRecord.RBTestQuantiferonTB = 0;
                                            if (RadioButtonListRBTopical.SelectedIndex != -1) abstractRecord.RBTopical = Convert.ToInt32(RadioButtonListRBTopical.SelectedValue);
                                            else abstractRecord.RBTopical = 0;
                                            if (RadioButtonListRBPeriocularInjections.SelectedIndex != -1) abstractRecord.RBPeriocularInjections = Convert.ToInt32(RadioButtonListRBPeriocularInjections.SelectedValue);
                                            else abstractRecord.RBPeriocularInjections = 0;
                                            if (RadioButtonListRBIntravitrealInjectionsForAnterior.SelectedIndex != -1) abstractRecord.RBIntravitrealInjectionsForAnterior = Convert.ToInt32(RadioButtonListRBIntravitrealInjectionsForAnterior.SelectedValue);
                                            else abstractRecord.RBIntravitrealInjectionsForAnterior = 0;
                                            if (RadioButtonListRBTherapySystemicDrug.SelectedIndex != -1) abstractRecord.RBTherapySystemicDrug = Convert.ToInt32(RadioButtonListRBTherapySystemicDrug.SelectedValue);
                                            else abstractRecord.RBTherapySystemicDrug = 0;
                                            if (RadioButtonListRBCycloplegicsDrug.SelectedIndex != -1) abstractRecord.RBCycloplegicsDrug = Convert.ToInt32(RadioButtonListRBCycloplegicsDrug.SelectedValue);
                                            else abstractRecord.RBCycloplegicsDrug = 0;
                                            if (RadioButtonListRBCycloplegicsFrequency.SelectedIndex != -1) abstractRecord.RBCycloplegicsFrequency = Convert.ToInt32(RadioButtonListRBCycloplegicsFrequency.SelectedValue);
                                            else abstractRecord.RBCycloplegicsFrequency = 0;
                                            if (RadioButtonListRBImmunPrescribedBy.SelectedIndex != -1) abstractRecord.RBImmunPrescribedBy = Convert.ToInt32(RadioButtonListRBImmunPrescribedBy.SelectedValue);
                                            else abstractRecord.RBImmunPrescribedBy = 0;
                                            if (RadioButtonListRBOutcomeBCVAType.SelectedIndex != -1) abstractRecord.RBOutcomeBCVAType = Convert.ToInt32(RadioButtonListRBOutcomeBCVAType.SelectedValue);
                                            else abstractRecord.RBOutcomeBCVAType = 0;
                                            if (RadioButtonListRBOutcomeKPsAppearance.SelectedIndex != -1) abstractRecord.RBOutcomeKPsAppearance = Convert.ToInt32(RadioButtonListRBOutcomeKPsAppearance.SelectedValue);
                                            else abstractRecord.RBOutcomeKPsAppearance = 0;
                                            if (RadioButtonListRBOutcomeKPsLocation.SelectedIndex != -1) abstractRecord.RBOutcomeKPsLocation = Convert.ToInt32(RadioButtonListRBOutcomeKPsLocation.SelectedValue);
                                            else abstractRecord.RBOutcomeKPsLocation = 0;
                                            if (RadioButtonListRBOutcomeAnteriorDepth.SelectedIndex != -1) abstractRecord.RBOutcomeAnteriorDepth = Convert.ToInt32(RadioButtonListRBOutcomeAnteriorDepth.SelectedValue);
                                            else abstractRecord.RBOutcomeAnteriorDepth = 0;
                                            if (RadioButtonListRBOutcomeAnteriorFlare.SelectedIndex != -1) abstractRecord.RBOutcomeAnteriorFlare = Convert.ToInt32(RadioButtonListRBOutcomeAnteriorFlare.SelectedValue);
                                            else abstractRecord.RBOutcomeAnteriorFlare = 0;
                                            if (RadioButtonListRBOutcomeAnteriorCells.SelectedIndex != -1) abstractRecord.RBOutcomeAnteriorCells = Convert.ToInt32(RadioButtonListRBOutcomeAnteriorCells.SelectedValue);
                                            else abstractRecord.RBOutcomeAnteriorCells = 0;
                                            if (RadioButtonListRBOutcomePosteriorSynechiae.SelectedIndex != -1) abstractRecord.RBOutcomePosteriorSynechiae = Convert.ToInt32(RadioButtonListRBOutcomePosteriorSynechiae.SelectedValue);
                                            else abstractRecord.RBOutcomePosteriorSynechiae = 0;
                                            if (RadioButtonListRBOutcomeGonioscopyRubeosis.SelectedIndex != -1) abstractRecord.RBOutcomeGonioscopyRubeosis = Convert.ToInt32(RadioButtonListRBOutcomeGonioscopyRubeosis.SelectedValue);
                                            else abstractRecord.RBOutcomeGonioscopyRubeosis = 0;
                                            if (RadioButtonListRBOutcomeGonioscopyInflam.SelectedIndex != -1) abstractRecord.RBOutcomeGonioscopyInflam = Convert.ToInt32(RadioButtonListRBOutcomeGonioscopyInflam.SelectedValue);
                                            else abstractRecord.RBOutcomeGonioscopyInflam = 0;
                                            if (RadioButtonListRBOutcomeGonioscopyClockHrs.SelectedIndex != -1) abstractRecord.RBOutcomeGonioscopyClockHrs = Convert.ToInt32(RadioButtonListRBOutcomeGonioscopyClockHrs.SelectedValue);
                                            else abstractRecord.RBOutcomeGonioscopyClockHrs = 0;
                                            if (RadioButtonListRBOutcomeVitreousCells.SelectedIndex != -1) abstractRecord.RBOutcomeVitreousCells = Convert.ToInt32(RadioButtonListRBOutcomeVitreousCells.SelectedValue);
                                            else abstractRecord.RBOutcomeVitreousCells = 0;
                                            if (RadioButtonListRBOutcomeChorioretinalPathology.SelectedIndex != -1) abstractRecord.RBOutcomeChorioretinalPathology = Convert.ToInt32(RadioButtonListRBOutcomeChorioretinalPathology.SelectedValue);
                                            else abstractRecord.RBOutcomeChorioretinalPathology = 0;
                                            if (RadioButtonListRBOutcomeChorioretinalRVSheathing.SelectedIndex != -1) abstractRecord.RBOutcomeChorioretinalRVSheathing = Convert.ToInt32(RadioButtonListRBOutcomeChorioretinalRVSheathing.SelectedValue);
                                            else abstractRecord.RBOutcomeChorioretinalRVSheathing = 0;
                                            if (RadioButtonListRBOutcomeChorioretinalRVActive.SelectedIndex != -1) abstractRecord.RBOutcomeChorioretinalRVActive = Convert.ToInt32(RadioButtonListRBOutcomeChorioretinalRVActive.SelectedValue);
                                            else abstractRecord.RBOutcomeChorioretinalRVActive = 0;
                                            if (RadioButtonListRBOutcomeChorioretinalRetinitis.SelectedIndex != -1) abstractRecord.RBOutcomeChorioretinalRetinitis = Convert.ToInt32(RadioButtonListRBOutcomeChorioretinalRetinitis.SelectedValue);
                                            else abstractRecord.RBOutcomeChorioretinalRetinitis = 0;
                                            if (RadioButtonListRBOutcomeChorioretinalRetinalNeovasc.SelectedIndex != -1) abstractRecord.RBOutcomeChorioretinalRetinalNeovasc = Convert.ToInt32(RadioButtonListRBOutcomeChorioretinalRetinalNeovasc.SelectedValue);
                                            else abstractRecord.RBOutcomeChorioretinalRetinalNeovasc = 0;
                                            if (RadioButtonListRBOutcomeChoroiditis.SelectedIndex != -1) abstractRecord.RBOutcomeChoroiditis = Convert.ToInt32(RadioButtonListRBOutcomeChoroiditis.SelectedValue);
                                            else abstractRecord.RBOutcomeChoroiditis = 0;
                                            if (RadioButtonListRBOutcomeChoroidalNeovascularization.SelectedIndex != -1) abstractRecord.RBOutcomeChoroidalNeovascularization = Convert.ToInt32(RadioButtonListRBOutcomeChoroidalNeovascularization.SelectedValue);
                                            else abstractRecord.RBOutcomeChoroidalNeovascularization = 0;
                                            if (RadioButtonListRBOutcomeRetinalTear.SelectedIndex != -1) abstractRecord.RBOutcomeRetinalTear = Convert.ToInt32(RadioButtonListRBOutcomeRetinalTear.SelectedValue);
                                            else abstractRecord.RBOutcomeRetinalTear = 0;
                                            if (RadioButtonListRBOutcomeRetinalDetachment.SelectedIndex != -1) abstractRecord.RBOutcomeRetinalDetachment = Convert.ToInt32(RadioButtonListRBOutcomeRetinalDetachment.SelectedValue);
                                            else abstractRecord.RBOutcomeRetinalDetachment = 0;
                                            if (RadioButtonListRBOutcomeInitiatedMgntUnderlying.SelectedIndex != -1) abstractRecord.RBOutcomeInitiatedMgntUnderlying = Convert.ToInt32(RadioButtonListRBOutcomeInitiatedMgntUnderlying.SelectedValue);
                                            else abstractRecord.RBOutcomeInitiatedMgntUnderlying = 0;



                                            if (DropDownListMonthOfBirth.SelectedIndex > 0) abstractRecord.MonthOfBirth = Convert.ToInt32(DropDownListMonthOfBirth.SelectedValue);
                                            else abstractRecord.MonthOfBirth = 0;
                                            if (DropDownListYearOfBirth.SelectedIndex > 0) abstractRecord.YearOfBirth = Convert.ToInt32(DropDownListYearOfBirth.SelectedValue);
                                            else abstractRecord.YearOfBirth = 0;
                                            if (DropDownListMonthOfOnsetFirstEpisode.SelectedIndex > 0) abstractRecord.MonthOfOnsetFirstEpisode = Convert.ToInt32(DropDownListMonthOfOnsetFirstEpisode.SelectedValue);
                                            else abstractRecord.MonthOfOnsetFirstEpisode = 0;
                                            if (DropDownListYearOfOnsetFirstEpisode.SelectedIndex > 0) abstractRecord.YearOfOnsetFirstEpisode = Convert.ToInt32(DropDownListYearOfOnsetFirstEpisode.SelectedValue);
                                            else abstractRecord.YearOfOnsetFirstEpisode = 0;
                                            if (DropDownListMonthOfOnsetCurrentEpisode.SelectedIndex > 0) abstractRecord.MonthOfOnsetCurrentEpisode = Convert.ToInt32(DropDownListMonthOfOnsetCurrentEpisode.SelectedValue);
                                            else abstractRecord.MonthOfOnsetCurrentEpisode = 0;
                                            if (DropDownListYearOfOnsetCurrentEpisode.SelectedIndex > 0) abstractRecord.YearOfOnsetCurrentEpisode = Convert.ToInt32(DropDownListYearOfOnsetCurrentEpisode.SelectedValue);
                                            else abstractRecord.YearOfOnsetCurrentEpisode = 0;
                                            if (DropDownListMonthOfOnsetLastEpisode.SelectedIndex > 0) abstractRecord.MonthOfOnsetLastEpisode = Convert.ToInt32(DropDownListMonthOfOnsetLastEpisode.SelectedValue);
                                            else abstractRecord.MonthOfOnsetLastEpisode = 0;
                                            if (DropDownListYearOfOnsetLastEpisode.SelectedIndex > 0) abstractRecord.YearOfOnsetLastEpisode = Convert.ToInt32(DropDownListYearOfOnsetLastEpisode.SelectedValue);
                                            else abstractRecord.YearOfOnsetLastEpisode = 0;
                                            if (DropDownListPreviousTreatmentImmuStartMonth.SelectedIndex > 0) abstractRecord.PreviousTreatmentImmuStartMonth = Convert.ToInt32(DropDownListPreviousTreatmentImmuStartMonth.SelectedValue);
                                            else abstractRecord.PreviousTreatmentImmuStartMonth = 0;
                                            if (DropDownListPreviousTreatmentImmuStartYear.SelectedIndex > 0) abstractRecord.PreviousTreatmentImmuStartYear = Convert.ToInt32(DropDownListPreviousTreatmentImmuStartYear.SelectedValue);
                                            else abstractRecord.PreviousTreatmentImmuStartYear = 0;
                                            if (DropDownListPreviousTreatmentImmuStopMonth.SelectedIndex > 0) abstractRecord.PreviousTreatmentImmuStopMonth = Convert.ToInt32(DropDownListPreviousTreatmentImmuStopMonth.SelectedValue);
                                            else abstractRecord.PreviousTreatmentImmuStopMonth = 0;
                                            if (DropDownListPreviousTreatmentImmuStopYear.SelectedIndex > 0) abstractRecord.PreviousTreatmentImmuStopYear = Convert.ToInt32(DropDownListPreviousTreatmentImmuStopYear.SelectedValue);
                                            else abstractRecord.PreviousTreatmentImmuStopYear = 0;
                                            if (DropDownListBCVAOD.SelectedIndex > 0) abstractRecord.BCVAOD = Convert.ToInt32(DropDownListBCVAOD.SelectedValue);
                                            else abstractRecord.BCVAOD = 0;
                                            if (DropDownListBCVAOS.SelectedIndex > 0) abstractRecord.BCVAOS = Convert.ToInt32(DropDownListBCVAOS.SelectedValue);
                                            else abstractRecord.BCVAOS = 0;
                                            if (DropDownListOutcomeRemissionMonth.SelectedIndex > 0) abstractRecord.OutcomeRemissionMonth = Convert.ToInt32(DropDownListOutcomeRemissionMonth.SelectedValue);
                                            else abstractRecord.OutcomeRemissionMonth = 0;
                                            if (DropDownListOutcomeRemissionYear.SelectedIndex > 0) abstractRecord.OutcomeRemissionYear = Convert.ToInt32(DropDownListOutcomeRemissionYear.SelectedValue);
                                            else abstractRecord.OutcomeRemissionYear = 0;
                                            if (DropDownListOutcomeBCVAOD.SelectedIndex > 0) abstractRecord.OutcomeBCVAOD = Convert.ToInt32(DropDownListOutcomeBCVAOD.SelectedValue);
                                            else abstractRecord.OutcomeBCVAOD = 0;
                                            if (DropDownListOutcomeBCVAOS.SelectedIndex > 0) abstractRecord.OutcomeBCVAOS = Convert.ToInt32(DropDownListOutcomeBCVAOS.SelectedValue);
                                            else abstractRecord.OutcomeBCVAOS = 0;
                                            if (DropDownListOutcomeGonioscopyMonth.SelectedIndex > 0) abstractRecord.OutcomeGonioscopyMonth = Convert.ToInt32(DropDownListOutcomeGonioscopyMonth.SelectedValue);
                                            else abstractRecord.OutcomeGonioscopyMonth = 0;
                                            if (DropDownListOutcomeGonioscopyYear.SelectedIndex > 0) abstractRecord.OutcomeGonioscopyYear = Convert.ToInt32(DropDownListOutcomeGonioscopyYear.SelectedValue);
                                            else abstractRecord.OutcomeGonioscopyYear = 0;


                                            if (RadioButtonCornealEdemaYes.Checked) abstractRecord.CornealEdema = true;
                                            if (RadioButtonCornealEdemaNo.Checked) abstractRecord.CornealEdema = false;
                                            if (RadioButtonOutcomeCornealEdemaYes.Checked) abstractRecord.OutcomeCornealEdema = true;
                                            if (RadioButtonOutcomeCornealEdemaNo.Checked) abstractRecord.OutcomeCornealEdema = false;
                                            if (RadioButtonListRBAnteriorHypopyonYes.Checked) abstractRecord.RBAnteriorHypopyon=true;
                                           if (RadioButtonListRBAnteriorHypopyonNo.Checked) abstractRecord.RBAnteriorHypopyon=false;
                                            if (RadioButtonPreviousTreatmentTopicalCSYes.Checked) abstractRecord.PreviousTreatmentTopicalCS = true;
                                            if (RadioButtonPreviousTreatmentTopicalCSNo.Checked) abstractRecord.PreviousTreatmentTopicalCS = false;
                                            if (RadioButtonPreviousTreatmentPeriocularCSYes.Checked) abstractRecord.PreviousTreatmentPeriocularCS = true;
                                            if (RadioButtonPreviousTreatmentPeriocularCSNo.Checked) abstractRecord.PreviousTreatmentPeriocularCS = false;
                                            if (RadioButtonPreviousTreatmentSystemicCSYes.Checked) abstractRecord.PreviousTreatmentSystemicCS = true;
                                            if (RadioButtonPreviousTreatmentSystemicCSNo.Checked) abstractRecord.PreviousTreatmentSystemicCS = false;
                                            if (RadioButtonInflammationFreeDurationNDYes.Checked) abstractRecord.InflammationFreeDurationND = true;
                                        
                                            if (RadioButtonConjunctivalInjectionYes.Checked) abstractRecord.ConjunctivalInjection = true;
                                            if (RadioButtonConjunctivalInjectionNo.Checked) abstractRecord.ConjunctivalInjection = false;
                                            if (RadioButtonConjunctivalNodulesYes.Checked) abstractRecord.ConjunctivalNodules = true;
                                            if (RadioButtonConjunctivalNodulesNo.Checked) abstractRecord.ConjunctivalNodules = false;
                                            if (RadioButtonDendritesPresentYes.Checked) abstractRecord.DendritesPresent = true;
                                            if (RadioButtonDendritesPresentNo.Checked) abstractRecord.DendritesPresent = false;
                                            if (RadioButtonStromalScarsYes.Checked) abstractRecord.StromalScars = true;
                                            if (RadioButtonStromalScarsNo.Checked) abstractRecord.StromalScars = false;
                                            if (RadioButtonKPsYes.Checked) abstractRecord.KPs = true;
                                            if (RadioButtonKPsNo.Checked) abstractRecord.KPs = false;
                                            if (RadioButtonIrisHeterochromiaYes.Checked) abstractRecord.IrisHeterochromia = true;
                                            if (RadioButtonIrisHeterochromiaNo.Checked) abstractRecord.IrisHeterochromia = false;
                                            if (RadioButtonIrisPreviousPeripheralIridotomyYes.Checked) abstractRecord.IrisPreviousPeripheralIridotomy = true;
                                            if (RadioButtonIrisPreviousPeripheralIridotomyNo.Checked) abstractRecord.IrisPreviousPeripheralIridotomy = false;
                                            if (RadioButtonGonioscopyPerformedYes.Checked) abstractRecord.GonioscopyPerformed = true;
                                            if (RadioButtonGonioscopyPerformedNo.Checked) abstractRecord.GonioscopyPerformed = false;
                                            if (RadioButtonLensPigmentYes.Checked) abstractRecord.LensPigment = true;
                                            if (RadioButtonLensPigmentNo.Checked) abstractRecord.LensPigment = false;
                                            if (RadioButtonLensCataractPresentYes.Checked) abstractRecord.LensCataractPresent = true;
                                            if (RadioButtonLensCataractPresentNo.Checked) abstractRecord.LensCataractPresent = false;
                                            if (RadioButtonScleritisNodularYes.Checked) abstractRecord.ScleritisNodular = true;
                                            if (RadioButtonScleritisNodularNo.Checked) abstractRecord.ScleritisNodular = false;
                                            if (RadioButtonScleritisDiffuseYes.Checked) abstractRecord.ScleritisDiffuse = true;
                                            if (RadioButtonScleritisDiffuseNo.Checked) abstractRecord.ScleritisDiffuse = false;
                                            if (RadioButtonScleritisNecrotizingYes.Checked) abstractRecord.ScleritisNecrotizing = true;
                                            if (RadioButtonScleritisNecrotizingNo.Checked) abstractRecord.ScleritisNecrotizing = false;
                                         
                                            if ( RadioButtonEtiologyDeterminedYES.Checked) abstractRecord.EtiologyDetermined = true;
                                            if ( RadioButtonEtiologyDeterminedNO.Checked) abstractRecord.EtiologyDetermined = false;


                                            if (RadioButtonOpticDiscEdemaPresentYes.Checked) abstractRecord.OpticDiscEdemaPresent = true;
                                            if (RadioButtonOpticDiscEdemaPresentNo.Checked) abstractRecord.OpticDiscEdemaPresent = false;
                                            if (RadioButtonOpticDiscGlaucomatousChangesYes.Checked) abstractRecord.OpticDiscGlaucomatousChanges = true;
                                            if (RadioButtonOpticDiscGlaucomatousChangesNo.Checked) abstractRecord.OpticDiscGlaucomatousChanges = false;
                                            if (RadioButtonDilatedFundusExamPerformedYes.Checked) abstractRecord.DilatedFundusExamPerformed = true;
                                            if (RadioButtonDilatedFundusExamPerformedNo.Checked) abstractRecord.DilatedFundusExamPerformed = false;
                                            if (RadioButtonCMEPresentYes.Checked) abstractRecord.CMEPresent = true;
                                            if (RadioButtonCMEPresentNo.Checked) abstractRecord.CMEPresent = false;
                                            if (RadioButtonFluoresceinPerformedYes.Checked) abstractRecord.FluoresceinPerformed = true;
                                            if (RadioButtonFluoresceinPerformedNo.Checked) abstractRecord.FluoresceinPerformed = false;
                                            if (RadioButtonImmunTherapyOngoingYes.Checked) abstractRecord.ImmunTherapyOngoing = true;
                                            if (RadioButtonImmunTherapyOngoingNo.Checked) abstractRecord.ImmunTherapyOngoing = false;
                                            if (RadioButtonSmokingCessationDiscussedYes.Checked) abstractRecord.SmokingCessationDiscussed = true;
                                            if (RadioButtonSmokingCessationDiscussedNo.Checked) abstractRecord.SmokingCessationDiscussed = false;
                                            if (RadioButtonOutcomeRemissionYes.Checked) abstractRecord.OutcomeRemission = true;
                                            if (RadioButtonOutcomeRemissionNo.Checked) abstractRecord.OutcomeRemission = false;
                                            if (RadioButtonOutcomeStromalScartsYes.Checked) abstractRecord.OutcomeStromalScarts = true;
                                            if (RadioButtonOutcomeStromalScartsNo.Checked) abstractRecord.OutcomeStromalScarts = false;
                                            if (RadioButtonOutcomeKPsYes.Checked) abstractRecord.OutcomeKPs = true;
                                            if (RadioButtonOutcomeKPsNo.Checked) abstractRecord.OutcomeKPs = false;
                                            if (RadioButtonOutcomeLensPigmentYes.Checked) abstractRecord.OutcomeLensPigment = true;
                                            if (RadioButtonOutcomeLensPigmentNo.Checked) abstractRecord.OutcomeLensPigment = false;
                                            if (RadioButtonOutcomeLensCataractYes.Checked) abstractRecord.OutcomeLensCataract = true;
                                            if (RadioButtonOutcomeLensCataractNo.Checked) abstractRecord.OutcomeLensCataract = false;
                                            if (RadioButtonImmunomodulatoryTherapyYes.Checked) abstractRecord.ImmunomodulatoryTherapy = true;
                                            if (RadioButtonImmunomodulatoryTherapyNo.Checked) abstractRecord.ImmunomodulatoryTherapy = false;
                                            if (RadioButtonOutcomeOpticDiscEdemaYes.Checked) abstractRecord.OutcomeOpticDiscEdema = true;
                                            if (RadioButtonOutcomeOpticDiscEdemaNo.Checked) abstractRecord.OutcomeOpticDiscEdema = false;
                                            if (RadioButtonOutcomeOpticDiscGlaucChangesYes.Checked) abstractRecord.OutcomeOpticDiscGlaucChanges = true;
                                            if (RadioButtonOutcomeOpticDiscGlaucChangesNo.Checked) abstractRecord.OutcomeOpticDiscGlaucChanges = false;
                                            if (RadioButtonOutcomeDilatedFundusExamPerformedYes.Checked) abstractRecord.OutcomeDilatedFundusExamPerformed = true;
                                            if (RadioButtonOutcomeDilatedFundusExamPerformedNo.Checked) abstractRecord.OutcomeDilatedFundusExamPerformed = false;
                                            if (RadioButtonOutcomeCMEPresentYes.Checked) abstractRecord.OutcomeCMEPresent = true;
                                            if (RadioButtonOutcomeCMEPresentNo.Checked) abstractRecord.OutcomeCMEPresent = false;
                                            if (RadioButtonOutcomeFluoresceinPerformedYes.Checked) abstractRecord.OutcomeFluoresceinPerformed = true;
                                            if (RadioButtonOutcomeFluoresceinPerformedNo.Checked) abstractRecord.OutcomeFluoresceinPerformed = false;


                                            try
                                            {
                                                if (TextBoxTopicalOther != null)
                                                    abstractRecord.TextBoxTopicalOther = TextBoxTopicalOther.Text;
                                            }
                                            catch (Exception ex)
                                            {
                                                abstractRecord.TextBoxTopicalOther = null;
                                                TextBoxTopicalOther.Text = "";
                                            }



                                            try
                                            {
                                                if (EtiologyDeterminedText != null)
                                                    abstractRecord.EtiologyDeterminedText = EtiologyDeterminedText.Text;
                                            }
                                            catch (Exception ex)
                                            {
                                                abstractRecord.EtiologyDeterminedText = null;
                                                EtiologyDeterminedText.Text = "";
                                            }



                                            try
                                            {
                                                if (PeriocularInjectionsText != null)
                                                    abstractRecord.PeriocularInjectionsText = PeriocularInjectionsText.Text;
                                            }
                                            catch (Exception ex)
                                            {
                                                abstractRecord.PeriocularInjectionsText = null;
                                                PeriocularInjectionsText.Text = "";
                                            }







                                            try
                                            {
                                                if (TextBoxVEGFInjectionsGiven != null)
                                                    abstractRecord.TextBoxVEGFInjectionsGiven = TextBoxVEGFInjectionsGiven.Text;
                                            }
                                            catch (Exception ex)
                                            {
                                                abstractRecord.TextBoxVEGFInjectionsGiven = null;
                                                TextBoxVEGFInjectionsGiven.Text = "";
                                            }


                                            try
                                            {
                                            if (TextBoxPreviousTreatmentNbrInjections.Text != null)
                                            abstractRecord.PreviousTreatmentNbrInjections =Convert.ToInt32( TextBoxPreviousTreatmentNbrInjections.Text);
                                            }
                                            catch (Exception ex)
                                            {
                                            abstractRecord.PreviousTreatmentNbrInjections = null;
                                            TextBoxPreviousTreatmentNbrInjections.Text = "";
                                            }

                                            try
                                            {
                                                if (TextBoxOutcomeRetinalDetachmentText.Text != null)
                                                    abstractRecord.RBOutcomeRetinalDetachmentText = TextBoxOutcomeRetinalDetachmentText.Text;
                                            }
                                            catch (Exception ex)
                                            {
                                                abstractRecord.RBOutcomeRetinalDetachmentText = null;
                                                TextBoxOutcomeRetinalDetachmentText.Text = "";
                                            }




                                            try
                                            {
                                            if (TextBoxPreviousTreatmentSystemicDrug.Text != null)
                                            abstractRecord.PreviousTreatmentSystemicDrug = TextBoxPreviousTreatmentSystemicDrug.Text;
                                            }
                                            catch (Exception ex)
                                            {
                                            abstractRecord.PreviousTreatmentSystemicDrug = null;
                                            TextBoxPreviousTreatmentSystemicDrug.Text = "";
                                            }
                                            try
                                            {
                                            if (TextBoxPreviousTreatmentSystemicNbrWeeks.Text != null)
                                            abstractRecord.PreviousTreatmentSystemicNbrWeeks = Convert.ToInt32(TextBoxPreviousTreatmentSystemicNbrWeeks.Text);
                                            }
                                            catch (Exception ex)
                                            {
                                            abstractRecord.PreviousTreatmentSystemicNbrWeeks = null;
                                            TextBoxPreviousTreatmentSystemicNbrWeeks.Text ="";
                                            }
                                            try
                                            {
                                            if (TextBoxPreviousTreatmentImmuTherapyDrug.Text != null)
                                            abstractRecord.PreviousTreatmentImmuTherapyDrug = TextBoxPreviousTreatmentImmuTherapyDrug.Text;
                                            }
                                            catch (Exception ex)
                                            {
                                            abstractRecord.PreviousTreatmentImmuTherapyDrug = null;
                                            TextBoxPreviousTreatmentImmuTherapyDrug.Text = "";
                                            }
                                            try
                                            {
                                            if (TextBoxInflammationFreeDurationWeeks.Text != null)
                                            abstractRecord.InflammationFreeDurationWeeks = Convert.ToInt32(TextBoxInflammationFreeDurationWeeks.Text);
                                            }
                                            catch (Exception ex)
                                            {
                                            abstractRecord.InflammationFreeDurationWeeks = null;
                                            TextBoxInflammationFreeDurationWeeks.Text = "";
                                            }
                                            try
                                            {
                                            if (TextBoxHistoryFamilyRelationship.Text != null)
                                            abstractRecord.HistoryFamilyRelationship = TextBoxHistoryFamilyRelationship.Text;
                                            }
                                            catch (Exception ex)
                                            {
                                            abstractRecord.HistoryFamilyRelationship = null;
                                            TextBoxHistoryFamilyRelationship.Text = "";
                                            }
                                            try
                                            {
                                            if (TextBoxRBIrisNoduleClockHours.Text != null)
                                            abstractRecord.RBIrisNoduleClockHours = TextBoxRBIrisNoduleClockHours.Text;
                                            }
                                            catch (Exception ex)
                                            {
                                            abstractRecord.RBIrisNoduleClockHours = null;
                                            TextBoxRBIrisNoduleClockHours.Text = "";
                                            }
                                           
                                            try
                                            {
                                            if (TextBoxIntraocularPressure.Text != null)
                                            abstractRecord.IntraocularPressure = Convert.ToInt32(TextBoxIntraocularPressure.Text);
                                            }
                                            catch (Exception ex)
                                            {
                                            abstractRecord.IntraocularPressure = null;
                                            TextBoxIntraocularPressure.Text = "";
                                            }
                                            try
                                            {
                                            if (TextBoxOCTCentralSubfieldThickness.Text != null)
                                            abstractRecord.OCTCentralSubfieldThickness = TextBoxOCTCentralSubfieldThickness.Text;
                                            }
                                            catch (Exception ex)
                                            {
                                            abstractRecord.OCTCentralSubfieldThickness = null;
                                            TextBoxOCTCentralSubfieldThickness.Text = "";
                                            }
                                            try
                                            {
                                            if (TextBoxRBRetinalDetachmentOther.Text != null)
                                            abstractRecord.RBRetinalDetachmentOther = TextBoxRBRetinalDetachmentOther.Text;
                                            }
                                            catch (Exception ex)
                                            {
                                            abstractRecord.RBRetinalDetachmentOther = null;
                                            TextBoxRBRetinalDetachmentOther.Text = "";
                                            }
                                            try
                                            {
                                            if (TextBoxRBTestOther.Text != null)
                                            abstractRecord.RBTestOther = TextBoxRBTestOther.Text;
                                            }
                                            catch (Exception ex)
                                            {
                                            abstractRecord.RBTestOther = null;
                                            TextBoxRBTestOther.Text = "";
                                            }
                                            try
                                            {
                                            if (TextBoxRBPeriocularInjectionsCount.Text != null)
                                            abstractRecord.RBPeriocularInjectionsCount = Convert.ToInt32(TextBoxRBPeriocularInjectionsCount.Text);
                                            }
                                            catch (Exception ex)
                                            {
                                            abstractRecord.RBPeriocularInjectionsCount = null;
                                            TextBoxRBPeriocularInjectionsCount.Text = "";
                                            }
                                            try
                                            {
                                            if (TextBoxRBIntravitrealInjectionsCount.Text != null)
                                            abstractRecord.RBIntravitrealInjectionsCount = Convert.ToInt32(TextBoxRBIntravitrealInjectionsCount.Text);
                                            }
                                            catch (Exception ex)
                                            {
                                            abstractRecord.RBIntravitrealInjectionsCount = null;
                                            TextBoxRBIntravitrealInjectionsCount.Text = "";
                                            }
                                            try
                                            {
                                            if (TextBoxRBTherapySystemicDurationTaper.Text != null)
                                            abstractRecord.RBTherapySystemicDurationTaper = Convert.ToInt32(TextBoxRBTherapySystemicDurationTaper.Text);
                                            }
                                            catch (Exception ex)
                                            {
                                            abstractRecord.RBTherapySystemicDurationTaper = null;
                                            TextBoxRBTherapySystemicDurationTaper.Text = "";
                                            }
                                            try
                                            {
                                            if (TextBoxImmunDrugDosage.Text != null)
                                            abstractRecord.ImmunDrugDosage = TextBoxImmunDrugDosage.Text;
                                            }
                                            catch (Exception ex)
                                            {
                                            abstractRecord.ImmunDrugDosage = null;
                                            TextBoxImmunDrugDosage.Text = "";
                                            }
                                            try
                                            {
                                            if (TextBoxImmunDurationTherapy.Text != null)
                                            abstractRecord.ImmunDurationTherapy = Convert.ToInt32(TextBoxImmunDurationTherapy.Text);
                                            }
                                            catch (Exception ex)
                                            {
                                            abstractRecord.ImmunDurationTherapy = null;
                                            TextBoxImmunDurationTherapy.Text = "";
                                            }
                                           
                                            try
                                            {
                                            if (TextBoxOutcomeIrisSynechiaeExtent.Text != null)
                                            abstractRecord.OutcomeIrisSynechiaeExtent = TextBoxOutcomeIrisSynechiaeExtent.Text;
                                            }
                                            catch (Exception ex)
                                            {
                                            abstractRecord.OutcomeIrisSynechiaeExtent = null;
                                            TextBoxOutcomeIrisSynechiaeExtent.Text = "";
                                            }
                                            try
                                            {
                                            if (TextBoxOutcomeIOP.Text != null)
                                            abstractRecord.OutcomeIOP = Convert.ToInt32(TextBoxOutcomeIOP.Text);
                                            }
                                            catch (Exception ex)
                                            {
                                            abstractRecord.OutcomeIOP = null;
                                            TextBoxOutcomeIOP.Text = "";
                                            }
                                            try
                                            {
                                            if (TextBoxOutcomeOCTCentralSubfieldThickness.Text != null)
                                            abstractRecord.OutcomeOCTCentralSubfieldThickness = TextBoxOutcomeOCTCentralSubfieldThickness.Text;
                                            }
                                            catch (Exception ex)
                                            {
                                            abstractRecord.OutcomeOCTCentralSubfieldThickness = null;
                                            TextBoxOutcomeOCTCentralSubfieldThickness.Text = "";
                                            }
                


                                            abstractRecord.LastUpdateDate = DateTime.Now;
                                            bool ChartCompleted = isComplete();
                                            abstractRecord.Complete = ChartCompleted;
                                            var chartRegistration = (from cr in pim.ChartRegistration
                                                                     where cr.ParticipantModuleCycleID == cycle.ParticipantModuleCycleID
                                                                      & cr.ModuleID == ModuleID
                                                                      & cr.RecordIdentifier == recordIdentifier
                                                                     select cr).First<NetHealthPIMModel.ChartRegistration>();
                                            chartRegistration.AbstractCompleted = ChartCompleted;


                                            if ((DropDownListYearOfBirth.SelectedIndex > 0) && (DropDownListYearOfBirth.SelectedIndex > 0))
                                            {
                                                chartRegistration.DOB = abstractRecord.MonthOfBirth + "/" + abstractRecord.YearOfBirth;
                                            }

                                            pim.SaveChanges();

                                            transaction.Complete();




            }

            CycleManager.UpdateParticipantCompletedModules(cycleID, ModuleID);
        }
    }

    protected void ButtonSubmit_Click(object sender, EventArgs e)
    {
        savedata();
        isComplete();
        if (!isComplete())
        {
            isComplete();
            string script = " var answer = confirm('Chart data is not complete.\\nClick OK to save entries and exit chart.\\nClick CANCEL to save entries and review the chart. '); if (answer==true) window.location = '../PatientChartRegistration.aspx';";
            ScriptManager.RegisterStartupScript(this, GetType(),
                          "ServerControlScript", script, true);
        }
        else
        {

            Response.Redirect("../PatientChartRegistration.aspx");
        }
    }

    protected bool isComplete()
    {

        bool ChartCompleted = true;

        LabelRBDiagnosisFirstEpisode.Visible = false;

        LabelRBLaterality.Visible = false;

        LabelRBLastEpisodeEye.Visible = false;
        LabelRBGender.Visible = false;

        LabelRBHistoryFamily.Visible = false;

        LabelRBHistoryLowBackPain.Visible = false;
        LabelRBHistorySIJointFilms.Visible = false;

        LabelRBHistoryDiagnosis.Visible = false;

        LabelRBHistorySarcoidosis.Visible = false;
        LabelRBHistorySyphilis.Visible = false;

        LabelRBHistoryTuberculosis.Visible = false;

        LabelRBHistorySmoking.Visible = false;

        LabelRBBCVAType.Visible = false;

        LabelRBAnteriorChamberDepth.Visible = false;

        LabelRBAnteriorChamberFlare.Visible = false;

        LabelRBAnteriorChamberCriteria.Visible = false;

        LabelRBAnteriorHypopyon.Visible = false;

        LabelRBIrisAtrophy.Visible = false;

        LabelRBIrisNodulesPresent.Visible = false;

        LabelRBVitreousHaze.Visible = false;

        LabelRBChorioretinalPathology.Visible = false;

        LabelRBChorioretinalRVSheathing.Visible = false;

        LabelRBChorioretinalRVActive.Visible = false;

        LabelRBChorioretinalRetinitis.Visible = false;

        LabelRBChorioretinalRetinalNeovasc.Visible = false;

        LabelRBChoroiditis.Visible = false;

        LabelRBChoroidalNeovascularization.Visible = false;

        LabelRBRetinalTear.Visible = false;

        LabelRBRetinalDetachment.Visible = false;

        LabelRBTestFTAABS.Visible = false;

        LabelRBTestMHATP.Visible = false;

        LabelRBTestHLAB.Visible = false;

        LabelRBTestACE.Visible = false;

        LabelRBTestChest.Visible = false;
        LabelRBTestTB.Visible = false;

        LabelRBTestPPD.Visible = false;

        LabelRBTestQuantiferonTB.Visible = false;

        LabelRBTopical.Visible = false;

        LabelRBOutcomeBCVAType.Visible = false;

        LabelRBOutcomeKPsAppearance.Visible = false;

        LabelRBOutcomeKPsLocation.Visible = false;

        LabelRBOutcomeAnteriorDepth.Visible = false;

        LabelRBOutcomeAnteriorFlare.Visible = false;
        LabelRBOutcomeAnteriorCells.Visible = false;

        LabelRBOutcomePosteriorSynechiae.Visible = false;

        LabelRBOutcomeVitreousCells.Visible = false;

        LabelRBOutcomeChorioretinalPathology.Visible = false;

        //LabelRBOutcomeChorioretinalRVSheathing.Visible = false;

        //LabelRBOutcomeChorioretinalRVActive.Visible = false;

        //if (RadioButtonListRBOutcomeChorioretinalRetinitis.SelectedIndex == -1)
        //{
        //    LabelRBOutcomeChorioretinalRetinitis.Visible = false;
        //    ChartCompleted = false;
        //}
        //if (RadioButtonListRBOutcomeChorioretinalRetinalNeovasc.SelectedIndex == -1)
        //{
        //    LabelRBOutcomeChorioretinalRetinalNeovasc.Visible = false;
        //    ChartCompleted = false;
        //}
        //if (RadioButtonListRBOutcomeChoroiditis.SelectedIndex == -1)
        //{
        //    LabelRBOutcomeChoroiditis.Visible = false;
        //    ChartCompleted = false;
        //}
        //if (RadioButtonListRBOutcomeChoroidalNeovascularization.SelectedIndex == -1)
        //{
        //    LabelRBOutcomeChoroidalNeovascularization.Visible = false;
        //    ChartCompleted = false;
        //}
        //if (RadioButtonListRBOutcomeRetinalTear.SelectedIndex == -1)
        //{
        //    LabelRBOutcomeRetinalTear.Visible = false;
        //    ChartCompleted = false;
        //}
        //if (RadioButtonListRBOutcomeRetinalDetachment.SelectedIndex == -1)
        //{
        //    LabelRBOutcomeRetinalDetachment.Visible = false;
        //    ChartCompleted = false;
        //}
        LabelRBTherapySystemicDrug.Visible = false;
        
        LabelRBOutcomeInitiatedMgntUnderlying.Visible = false;

        LabelMonthOfBirth.Visible = false;

        LabelYearOfBirth.Visible = false;

        LabelMonthOfOnsetFirstEpisode.Visible = false;

        LabelYearOfOnsetFirstEpisode.Visible = false;

        LabelMonthOfOnsetCurrentEpisode.Visible = false;

        LabelYearOfOnsetCurrentEpisode.Visible = false;

        LabelMonthOfOnsetLastEpisode.Visible = false;

        LabelYearOfOnsetLastEpisode.Visible = false;

        LabelBCVAOD.Visible = false;

        LabelBCVAOS.Visible = false;

        LabelIntraocularPressure.Visible = false;

        LabelOutcomeBCVAOD.Visible = false;

        LabelOutcomeBCVAOS.Visible = false;

        LabelOutcomeIOP.Visible = false;

        LabelRBPeriocularInjections.Visible = false;

        LabelRBIntravitrealInjectionsForAnterior.Visible = false;
        LabelPreviousTreatmentTopicalCS.Visible = false;

        LabelPreviousTreatmentPeriocularCS.Visible = false;

        LabelPreviousTreatmentSystemicCS.Visible = false;

        LabelConjunctivalInjection.Visible = false;

        LabelConjunctivalNodules.Visible = false;

        LabelDendritesPresent.Visible = false;

        LabelStromalScars.Visible = false;

        LabelKPs.Visible = false;

        LabelIrisHeterochromia.Visible = false;

        LabelIrisPreviousPeripheralIridotomy.Visible = false;

        LabelGonioscopyPerformed.Visible = false;

        LabelLensPigment.Visible = false;

        LabelLensCataractPresent.Visible = false;

        LabelVitreousCells.Visible = false;
        LabelRBCycloplegicsFrequency.Visible = false;
        LabelOpticDiscEdemaPresent.Visible = false;

        LabelOpticDiscGlaucomatousChanges.Visible = false;

        LabelDilatedFundusExamPerformed.Visible = false;
        LabelRBCycloplegicsDrug.Visible = false;
        LabelCMEPresent.Visible = false;

        LabelFluoresceinPerformed.Visible = false;

        LabelSmokingCessationDiscussed.Visible = false;

        LabelOutcomeRemission.Visible = false;
        LabelImmunomodulatoryTherapy.Visible = false;
        LabelOutcomeStromalScarts.Visible = false;

        LabelOutcomeKPs.Visible = false;
        LabelOutcomeLensPigment.Visible = false;

        LabelOutcomeLensCataract.Visible = false;
        LabelOutcomeGonioscopy.Visible = false;

        LabelOutcomeOpticDiscEdema.Visible = false;

        LabelOutcomeOpticDiscGlaucChanges.Visible = false;

        LabelOutcomeDilatedFundusExamPerformed.Visible = false;

        LabelOutcomeCMEPresent.Visible = false;

        LabelOutcomeFluoresceinPerformed.Visible = false;
        
        if (RadioButtonListRBDiagnosisFirstEpisode.SelectedIndex == -1)
        {
            LabelRBDiagnosisFirstEpisode.Visible = true;
            ChartCompleted = false;
        }
        if (RadioButtonListRBLaterality.SelectedIndex == -1)
        {
            LabelRBLaterality.Visible = true;
            ChartCompleted = false;
        }
        if (RadioButtonListRBCycloplegicsDrug.SelectedIndex == -1)
        {
            LabelRBCycloplegicsDrug.Visible = true;
            ChartCompleted = false;
        }
        if (RadioButtonListRBLastEpisodeEye.SelectedIndex == -1)
        {
            LabelRBLastEpisodeEye.Visible = true;
            ChartCompleted = false;
        }
        if (RadioButtonListRBGender.SelectedIndex == -1)
        {
            LabelRBGender.Visible = true;
            ChartCompleted = false;
        }
        if (RadioButtonListRBHistoryFamily.SelectedIndex == -1)
        {
            LabelRBHistoryFamily.Visible = true;
            ChartCompleted = false;
        }
        if (RadioButtonListRBTherapySystemicDrug.SelectedIndex == -1)
        {
            LabelRBTherapySystemicDrug.Visible = true;
            ChartCompleted = false;
        
        }
        if (RadioButtonListRBCycloplegicsFrequency.SelectedIndex == -1)
        {
            LabelRBCycloplegicsFrequency.Visible = true;
            ChartCompleted = false;
        
        
        }
        if (RadioButtonListRBHistoryLowBackPain.SelectedIndex == -1)
        {
            LabelRBHistoryLowBackPain.Visible = true;
            ChartCompleted = false;
        }
        if (RadioButtonListRBHistorySIJointFilms.SelectedIndex == -1)
        {
            LabelRBHistorySIJointFilms.Visible = true;
            ChartCompleted = false;
        }
        if (RadioButtonListRBHistoryDiagnosis.SelectedIndex == -1)
        {
            LabelRBHistoryDiagnosis.Visible = true;
            ChartCompleted = false;
        }
        if (RadioButtonListRBHistorySarcoidosis.SelectedIndex == -1)
        {
            LabelRBHistorySarcoidosis.Visible = true;
            ChartCompleted = false;
        }
        if (RadioButtonListRBHistorySyphilis.SelectedIndex == -1)
        {
            LabelRBHistorySyphilis.Visible = true;
            ChartCompleted = false;
        }
        if (RadioButtonListRBHistoryTuberculosis.SelectedIndex == -1)
        {
            LabelRBHistoryTuberculosis.Visible = true;
            ChartCompleted = false;
        }
        if (RadioButtonListRBHistorySmoking.SelectedIndex == -1)
        {
            LabelRBHistorySmoking.Visible = true;
            ChartCompleted = false;
        }
        if (RadioButtonListRBBCVAType.SelectedIndex == -1)
        {
            LabelRBBCVAType.Visible = true;
            ChartCompleted = false;
        }
        if (RadioButtonListRBAnteriorChamberDepth.SelectedIndex == -1)
        {
            LabelRBAnteriorChamberDepth.Visible = true;
            ChartCompleted = false;
        }
        if (RadioButtonListRBIntravitrealInjectionsForAnterior.SelectedIndex == -1)
        {
            LabelRBIntravitrealInjectionsForAnterior.Visible = true;
            ChartCompleted = false;
        
        }
        if (RadioButtonListRBAnteriorChamberFlare.SelectedIndex == -1)
        {
            LabelRBAnteriorChamberFlare.Visible = true;
            ChartCompleted = false;
        }
        if (RadioButtonListRBAnteriorChamberCriteria.SelectedIndex == -1)
        {
            LabelRBAnteriorChamberCriteria.Visible = true;
            ChartCompleted = false;
        }
        if (RadioButtonListRBAnteriorHypopyonYes.Checked == false && RadioButtonListRBAnteriorHypopyonNo.Checked == false)
        {
            LabelRBAnteriorHypopyon.Visible = true;
            ChartCompleted = false;
        }
        if (RadioButtonImmunomodulatoryTherapyYes.Checked == false && RadioButtonImmunomodulatoryTherapyNo.Checked == false)
        {
            LabelImmunomodulatoryTherapy.Visible = true;
            ChartCompleted = false;
        }
        if (RadioButtonListRBIrisAtrophy.SelectedIndex == -1)
        {
            LabelRBIrisAtrophy.Visible = true;
            ChartCompleted = false;
        }
        if (RadioButtonListRBIrisNodulesPresent.SelectedIndex == -1)
        {
            LabelRBIrisNodulesPresent.Visible = true;
            ChartCompleted = false;
        }
        if (RadioButtonListRBVitreousHaze.SelectedIndex == -1)
        {
            LabelRBVitreousHaze.Visible = true;
            ChartCompleted = false;
        }
        if (RadioButtonListRBChorioretinalPathology.SelectedIndex == -1)
        {
            LabelRBChorioretinalPathology.Visible = true;
            ChartCompleted = false;
        }




        if (RadioButtonListRBPeriocularInjections.SelectedIndex == -1)
        {
            LabelRBPeriocularInjections.Visible = true;
            ChartCompleted = false;
        }
        if (RadioButtonListRBChoroidalNeovascularization.SelectedIndex == -1)
        {
            LabelRBChoroidalNeovascularization.Visible = true;
            ChartCompleted = false;
        }
        if (RadioButtonListRBRetinalTear.SelectedIndex == -1)
        {
            LabelRBRetinalTear.Visible = true;
            ChartCompleted = false;
        }
        if (RadioButtonListRBRetinalDetachment.SelectedIndex == -1)
        {
            LabelRBRetinalDetachment.Visible = true;
            ChartCompleted = false;
        }
        if (RadioButtonListRBTestFTAABS.SelectedIndex == -1)
        {
            LabelRBTestFTAABS.Visible = true;
            ChartCompleted = false;
        }
        if (RadioButtonListRBTestMHATP.SelectedIndex == -1)
        {
            LabelRBTestMHATP.Visible = true;
            ChartCompleted = false;
        }
        if (RadioButtonListRBTestHLAB27.SelectedIndex == -1)
        {
            LabelRBTestHLAB.Visible = true;
            ChartCompleted = false;
        }
        if (RadioButtonListRBTestACE.SelectedIndex == -1)
        {
            LabelRBTestACE.Visible = true;
            ChartCompleted = false;
        }
        if (RadioButtonListRBTestChest.SelectedIndex == -1)
        {
            LabelRBTestChest.Visible = true;
            ChartCompleted = false;
        }
        if (RadioButtonListRBTestTB.SelectedIndex == -1)
        {
            LabelRBTestTB.Visible = true;
            ChartCompleted = false;
        }
        if (RadioButtonListRBTestPPD.SelectedIndex == -1)
        {
            LabelRBTestPPD.Visible = true;
            ChartCompleted = false;
        }
        if (RadioButtonListRBTestQuantiferonTB.SelectedIndex == -1)
        {
            LabelRBTestQuantiferonTB.Visible = true;
            ChartCompleted = false;
        }
        if (RadioButtonListRBTopical.SelectedIndex == -1)
        {
            LabelRBTopical.Visible = true;
            ChartCompleted = false;
        }
        if (RadioButtonListRBOutcomeBCVAType.SelectedIndex == -1)
        {
            LabelRBOutcomeBCVAType.Visible = true;
            ChartCompleted = false;
        }
       
       
        if (RadioButtonListRBOutcomeAnteriorDepth.SelectedIndex == -1)
        {
            LabelRBOutcomeAnteriorDepth.Visible = true;
            ChartCompleted = false;
        }
        if (RadioButtonListRBOutcomeAnteriorFlare.SelectedIndex == -1)
        {
            LabelRBOutcomeAnteriorFlare.Visible = true;
            ChartCompleted = false;
        }
        if (RadioButtonListRBOutcomeAnteriorCells.SelectedIndex == -1)
        {
            LabelRBOutcomeAnteriorCells.Visible = true;
            ChartCompleted = false;
        }
        if (RadioButtonListRBOutcomePosteriorSynechiae.SelectedIndex == -1)
        {
            LabelRBOutcomePosteriorSynechiae.Visible = true;
            ChartCompleted = false;
        }
        if (RadioButtonListRBOutcomeVitreousCells.SelectedIndex == -1)
        {
            LabelRBOutcomeVitreousCells.Visible = true;
            ChartCompleted = false;
        }
        if (RadioButtonListRBOutcomeChorioretinalPathology.SelectedIndex == -1)
        {
            LabelRBOutcomeChorioretinalPathology.Visible = true;
            ChartCompleted = false;
        }
        //if (RadioButtonListRBOutcomeChorioretinalRVSheathing.SelectedIndex == -1)
        //{
        //    LabelRBOutcomeChorioretinalRVSheathing.Visible = true;
        //    ChartCompleted = false;
        //}
        //if (RadioButtonListRBOutcomeChorioretinalRVActive.SelectedIndex == -1)
        //{
        //    LabelRBOutcomeChorioretinalRVActive.Visible = true;
        //    ChartCompleted = false;
        //}
        //if (RadioButtonListRBOutcomeChorioretinalRetinitis.SelectedIndex == -1)
        //{
        //    LabelRBOutcomeChorioretinalRetinitis.Visible = true;
        //    ChartCompleted = false;
        //}
        //if (RadioButtonListRBOutcomeChorioretinalRetinalNeovasc.SelectedIndex == -1)
        //{
        //    LabelRBOutcomeChorioretinalRetinalNeovasc.Visible = true;
        //    ChartCompleted = false;
        //}
        //if (RadioButtonListRBOutcomeChoroiditis.SelectedIndex == -1)
        //{
        //    LabelRBOutcomeChoroiditis.Visible = true;
        //    ChartCompleted = false;
        //}
        //if (RadioButtonListRBOutcomeChoroidalNeovascularization.SelectedIndex == -1)
        //{
        //    LabelRBOutcomeChoroidalNeovascularization.Visible = true;
        //    ChartCompleted = false;
        //}
        //if (RadioButtonListRBOutcomeRetinalTear.SelectedIndex == -1)
        //{
        //    LabelRBOutcomeRetinalTear.Visible = true;
        //    ChartCompleted = false;
        //}
        //if (RadioButtonListRBOutcomeRetinalDetachment.SelectedIndex == -1)
        //{
        //    LabelRBOutcomeRetinalDetachment.Visible = true;
        //    ChartCompleted = false;
        //}
        if (RadioButtonListRBOutcomeInitiatedMgntUnderlying.SelectedIndex == -1)
        {
            LabelRBOutcomeInitiatedMgntUnderlying.Visible = true;
            ChartCompleted = false;
        }



        if (DropDownListMonthOfBirth.SelectedIndex == 0)
        {
            LabelMonthOfBirth.Visible = true;
            ChartCompleted = false;
        }
        if (DropDownListYearOfBirth.SelectedIndex == 0)
        {
            LabelYearOfBirth.Visible = true;
            ChartCompleted = false;
        }
        if (DropDownListMonthOfOnsetFirstEpisode.SelectedIndex == 0)
        {
            LabelMonthOfOnsetFirstEpisode.Visible = true;
            ChartCompleted = false;
        }
        if (DropDownListYearOfOnsetFirstEpisode.SelectedIndex == 0)
        {
            LabelYearOfOnsetFirstEpisode.Visible = true;
            ChartCompleted = false;
        }
        if (DropDownListMonthOfOnsetCurrentEpisode.SelectedIndex == 0)
        {
            LabelMonthOfOnsetCurrentEpisode.Visible = true;
            ChartCompleted = false;
        }
        if (DropDownListYearOfOnsetCurrentEpisode.SelectedIndex == 0)
        {
            LabelYearOfOnsetCurrentEpisode.Visible = true;
            ChartCompleted = false;
        }
        if (DropDownListMonthOfOnsetLastEpisode.SelectedIndex == 0)
        {
            LabelMonthOfOnsetLastEpisode.Visible = true;
            ChartCompleted = false;
        }
        if (DropDownListYearOfOnsetLastEpisode.SelectedIndex == 0)
        {
            LabelYearOfOnsetLastEpisode.Visible = true;
            ChartCompleted = false;
        }
        if (DropDownListBCVAOD.SelectedIndex == 0)
        {
            LabelBCVAOD.Visible = true;
            ChartCompleted = false;
        }
        if (DropDownListBCVAOS.SelectedIndex == 0)
        {
            LabelBCVAOS.Visible = true;
            ChartCompleted = false;
        }
        if (string.IsNullOrEmpty(TextBoxIntraocularPressure.Text))
        {
            LabelIntraocularPressure.Visible = true;
            ChartCompleted = false;
        }
        if (DropDownListOutcomeBCVAOD.SelectedIndex == 0)
        {
            LabelOutcomeBCVAOD.Visible = true;
            ChartCompleted = false;
        }
        if (DropDownListOutcomeBCVAOS.SelectedIndex == 0)
        {
            LabelOutcomeBCVAOS.Visible = true;
            ChartCompleted = false;
        }
  



   
      
 
        if (RadioButtonConjunctivalInjectionNo.Checked == false && RadioButtonConjunctivalInjectionYes.Checked == false)
        {
            LabelConjunctivalInjection.Visible = true;
            ChartCompleted = false;
        }
        if (RadioButtonConjunctivalNodulesNo.Checked == false && RadioButtonConjunctivalNodulesYes.Checked == false)
        {
            LabelConjunctivalNodules.Visible = true;
            ChartCompleted = false;
        }
        if (RadioButtonDendritesPresentNo.Checked == false && RadioButtonDendritesPresentYes.Checked == false)
        {
            LabelDendritesPresent.Visible = true;
            ChartCompleted = false;
        }
        if (RadioButtonStromalScarsNo.Checked == false && RadioButtonStromalScarsYes.Checked == false)
        {
            LabelStromalScars.Visible = true;
            ChartCompleted = false;
        }
        if (RadioButtonKPsNo.Checked == false && RadioButtonKPsYes.Checked == false)
        {
            LabelKPs.Visible = true;
            ChartCompleted = false;
        }
        if (RadioButtonIrisHeterochromiaNo.Checked == false && RadioButtonIrisHeterochromiaYes.Checked == false)
        {
            LabelIrisHeterochromia.Visible = true;
            ChartCompleted = false;
        }
        if (RadioButtonIrisPreviousPeripheralIridotomyNo.Checked == false && RadioButtonIrisPreviousPeripheralIridotomyYes.Checked == false)
        {
            LabelIrisPreviousPeripheralIridotomy.Visible = true;
            ChartCompleted = false;
        }
        if (RadioButtonGonioscopyPerformedNo.Checked == false && RadioButtonGonioscopyPerformedYes.Checked == false)
        {
            LabelGonioscopyPerformed.Visible = true;
            ChartCompleted = false;
        }
        if (RadioButtonLensPigmentNo.Checked == false && RadioButtonLensPigmentYes.Checked == false)
        {
            LabelLensPigment.Visible = true;
            ChartCompleted = false;
        }
        if (RadioButtonLensCataractPresentNo.Checked == false && RadioButtonLensCataractPresentYes.Checked == false)
        {
            LabelLensCataractPresent.Visible = true;
            ChartCompleted = false;
        }
      
        if (RadioButtonOpticDiscEdemaPresentNo.Checked == false && RadioButtonOpticDiscEdemaPresentYes.Checked == false)
        {
            LabelOpticDiscEdemaPresent.Visible = true;
            ChartCompleted = false;
        }
        if (RadioButtonOpticDiscGlaucomatousChangesNo.Checked == false && RadioButtonOpticDiscGlaucomatousChangesYes.Checked == false)
        {
            LabelOpticDiscGlaucomatousChanges.Visible = true;
            ChartCompleted = false;
        }
        if (RadioButtonDilatedFundusExamPerformedNo.Checked == false && RadioButtonDilatedFundusExamPerformedYes.Checked == false)
        {
            LabelDilatedFundusExamPerformed.Visible = true;
            ChartCompleted = false;
        }
        if (RadioButtonCMEPresentNo.Checked == false && RadioButtonCMEPresentYes.Checked == false)
        {
            LabelCMEPresent.Visible = true;
            ChartCompleted = false;
        }
        if (RadioButtonFluoresceinPerformedNo.Checked == false && RadioButtonFluoresceinPerformedYes.Checked == false)
        {
            LabelFluoresceinPerformed.Visible = true;
            ChartCompleted = false;
        }
        if (RadioButtonSmokingCessationDiscussedNo.Checked == false && RadioButtonSmokingCessationDiscussedYes.Checked == false)
        {
            LabelSmokingCessationDiscussed.Visible = true;
            ChartCompleted = false;
        }
        if (RadioButtonOutcomeRemissionNo.Checked == false && RadioButtonOutcomeRemissionYes.Checked == false)
        {
            LabelOutcomeRemission.Visible = true;
            ChartCompleted = false;
        }
        if (RadioButtonOutcomeStromalScartsNo.Checked == false && RadioButtonOutcomeStromalScartsYes.Checked == false)
        {
            LabelOutcomeStromalScarts.Visible = true;
            ChartCompleted = false;
        }
        if (RadioButtonOutcomeKPsNo.Checked == false && RadioButtonOutcomeKPsYes.Checked == false)
        {
            LabelOutcomeKPs.Visible = true;
            ChartCompleted = false;
        }
        if (RadioButtonOutcomeLensPigmentNo.Checked == false && RadioButtonOutcomeLensPigmentYes.Checked == false)
        {
            LabelOutcomeLensPigment.Visible = true;
            ChartCompleted = false;
        }
        if (RadioButtonOutcomeLensCataractNo.Checked == false && RadioButtonOutcomeLensCataractYes.Checked == false)
        {
            LabelOutcomeLensCataract.Visible = true;
            ChartCompleted = false;
        }
        if (RadioButtonListOutcomeGonioscopy.SelectedIndex==-1)
        {
            LabelOutcomeGonioscopy.Visible = true;
            ChartCompleted = false;
        }
        if (RadioButtonOutcomeOpticDiscEdemaNo.Checked == false && RadioButtonOutcomeOpticDiscEdemaYes.Checked == false)
        {
            LabelOutcomeOpticDiscEdema.Visible = true;
            ChartCompleted = false;
        }
        if (RadioButtonOutcomeOpticDiscGlaucChangesNo.Checked == false && RadioButtonOutcomeOpticDiscGlaucChangesYes.Checked == false)
        {
            LabelOutcomeOpticDiscGlaucChanges.Visible = true;
            ChartCompleted = false;
        }
        if (RadioButtonOutcomeDilatedFundusExamPerformedNo.Checked == false && RadioButtonOutcomeDilatedFundusExamPerformedYes.Checked == false)
        {
            LabelOutcomeDilatedFundusExamPerformed.Visible = true;
            ChartCompleted = false;
        }
        if (RadioButtonOutcomeCMEPresentNo.Checked == false && RadioButtonOutcomeCMEPresentYes.Checked == false)
        {
            LabelOutcomeCMEPresent.Visible = true;
            ChartCompleted = false;
        }
        if (RadioButtonOutcomeFluoresceinPerformedNo.Checked == false && RadioButtonOutcomeFluoresceinPerformedYes.Checked == false)
        {
            LabelOutcomeFluoresceinPerformed.Visible = true;
            ChartCompleted = false;
        }

        return ChartCompleted;
    }




}
