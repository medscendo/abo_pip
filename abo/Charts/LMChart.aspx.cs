﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Transactions;
using NetHealthPIMModel;

public partial class abo_LMChart : BasePage
{
    protected void Page_Load(object sender, EventArgs e)
    {

    
    if (!IsPostBack)
        {
            InitializePage();
        }


    }
    protected void InitializePage()
    {
        ((PIMMasterPage)Page.Master).SetTitle("Chart");

        using (NetHealthPIMEntities pim = new NetHealthPIMEntities())
        {
            int ModuleID = (int)Session[Constants.SESSION_WORKINGMODULEID];
            Module module = pim.Module.First(c => c.ModuleID == ModuleID);
            ((PIMMasterPage)Page.Master).SetHeader(module.ModuleName + " Chart Abstraction");

            ParticipantModuleCycle prev = (ParticipantModuleCycle)Session[Constants.SESSION_PREVIOUSCYCLE];
            String CycleNumber = Request.QueryString["CycleNumber"];

            int cycleID = ctx.ActiveModuleCycleID;
            if (CycleNumber == "1")
            {
                cycleID = prev.ParticipantModuleCycleID;
                LinkButtonBackToDashboard.Visible = true;
                ButtonSubmit.Visible = false;
            }

            ParticipantModuleCycle cycle = pim.ParticipantModuleCycle.First(c => c.ParticipantModuleCycleID == cycleID);
            String numberOfRecord = Request.QueryString["nr"];
            String totalRecords = Request.QueryString["t"];
            if (cycle.CycleNumber == 1)
            {
                ((PIMMasterPage)Page.Master).SetStatus(2);
                LiteralAbstractionNumber.Text = "Initial Abstraction: Chart " + numberOfRecord + " of " + totalRecords;
            }
            else
            {
                ((PIMMasterPage)Page.Master).SetStatus(3);
                LiteralAbstractionNumber.Text = "Second Abstraction: Chart " + numberOfRecord + " of " + totalRecords;
            }
            DropDownListMonthOfBirth.DataSource = CycleManager.getMonthList();
            DropDownListYearOfBirth.DataSource = CycleManager.getDOBYearList(0, 100);

            DropDownListMonthOfPreopExam.DataSource = CycleManager.getMonthList();
            DropDownListYearOfPreopExam.DataSource = CycleManager.getDOBYearList(0, 100);
            DropDownListOutcomeVAOD.DataSource = CycleManager.getExamValues();
            DropDownListOutcomeVAOS.DataSource = CycleManager.getExamValues();
            DropDownListDistVAManifestRefractionOD.DataSource = CycleManager.getExamValues();
            DropDownListDistVAManifestRefractionOS.DataSource = CycleManager.getExamValues();
            DropDownListNearVAManifestRefractionOD.DataSource = CycleManager.getExamValues();
            DropDownListNearVAManifestRefractionOS.DataSource = CycleManager.getExamValues();
            DropDownListDistanceVACycloplegicOD.DataSource = CycleManager.getExamValues();
            DropDownListDistanceVACycloplegicOS.DataSource = CycleManager.getExamValues();
            DataBind();
            DropDownListOutcomeEnhancementSurgeryODMonth.DataSource = CycleManager.getMonthList();
            DropDownListOutcomeEnhancementSurgeryODYear.DataSource = CycleManager.getDOBYearList(0, 100);
            DropDownListOutcomeEnhancementSurgeryOSMonth.DataSource = CycleManager.getMonthList();
            DropDownListOutcomeEnhancementSurgeryOSYear.DataSource = CycleManager.getDOBYearList(0, 100);
            DataBind();
            DropDownListYearOfBirth.Items.Insert(0, new ListItem("Year", "0"));
            DropDownListOutcomeEnhancementSurgeryOSYear.Items.Insert(0, new ListItem("Year", "0"));
            DropDownListOutcomeEnhancementSurgeryODYear.Items.Insert(0, new ListItem("Year", "0"));
            DropDownListYearOfPreopExam.Items.Insert(0, new ListItem("Year", "0"));
            string recordIdentifier = Request.QueryString["ri"];
            HiddenFieldRecordIdentifier.Value = recordIdentifier;
            LiteralRecordIdentifier.Text = recordIdentifier;
            var count = pim.ChartAbstractionLasik.Where(ps => ps.ParticipantModuleCycle.ParticipantModuleCycleID == cycleID & ps.RecordIdentifier == recordIdentifier).Count();
            if (count == 0)
            {
                using (TransactionScope transaction = new TransactionScope())
                {
                    //Module module = pim.Module.First(m => m.ModuleID == Constants.ABO_AMBLYOPIA_MODULEID);

                    NetHealthPIMModel.ChartRegistration chartRegistration = (from cr in pim.ChartRegistration
                                                           where cr.ParticipantModuleCycleID == cycleID
                                                           & cr.RecordIdentifier == recordIdentifier
                                                           & cr.ModuleID == 51
                                                           select cr).First<NetHealthPIMModel.ChartRegistration>();

                    DateTime initialVisit = Convert.ToDateTime(chartRegistration.InitialVisit);
                    DateTime DOB = Convert.ToDateTime(chartRegistration.DOB);
                    ChartAbstractionLasik abstractRecord = new ChartAbstractionLasik();
                    abstractRecord.ParticipantModuleCycle = cycle;
                    abstractRecord.RecordIdentifier = recordIdentifier;
                    abstractRecord.MonthOfBirth = DOB.Month;
                    abstractRecord.YearOfBirth = DOB.Year;
                 
                    DropDownListYearOfBirth.SelectedValue = abstractRecord.YearOfBirth.ToString();
                    DropDownListMonthOfBirth.SelectedValue = abstractRecord.MonthOfBirth.ToString();
                    abstractRecord.CreatedOn = DateTime.Now;
                    abstractRecord.LastUpdateDate = DateTime.Now;
                    pim.SaveChanges();

                    transaction.Complete();
                }
            }
            else
            {
                ChartAbstractionLasik abstractRecord = (from c in pim.ChartAbstractionLasik
                                                            where c.ParticipantModuleCycle.ParticipantModuleCycleID == cycleID & c.RecordIdentifier == recordIdentifier
                                                        select c).First<ChartAbstractionLasik>();
                if (abstractRecord.RefractiveCorrectionNA != null) CheckBoxRefractiveCorrectionNA.Checked = ((bool)abstractRecord.RefractiveCorrectionNA);
                if (abstractRecord.ManifestRefractionODNA != null) CheckBoxManifestRefractionODNA.Checked = ((bool)abstractRecord.ManifestRefractionODNA);
                if (abstractRecord.ManifestRefractionOSNA != null) CheckBoxManifestRefractionOSNA.Checked = ((bool)abstractRecord.ManifestRefractionOSNA);
               
                if (abstractRecord.NearVAManifestRefractionNA != null) CheckBoxNearVAManifestRefractionNA.Checked = ((bool)abstractRecord.NearVAManifestRefractionNA);
                if (abstractRecord.CycloplegicRefractionODNA != null) CheckBoxCycloplegicRefractionODNA.Checked = ((bool)abstractRecord.CycloplegicRefractionODNA);
                if (abstractRecord.CycloplegicRefractionOSNA != null) CheckBoxCycloplegicRefractionOSNA.Checked = ((bool)abstractRecord.CycloplegicRefractionOSNA);
                if (abstractRecord.DistanceVACycloplegicNA != null) CheckBoxDistanceVACycloplegicNA.Checked = ((bool)abstractRecord.DistanceVACycloplegicNA);
                if (abstractRecord.DistVAManifestrefractionNA != null) CheckBoxDistVAManifestRefractionNA.Checked = ((bool)abstractRecord.DistVAManifestrefractionNA);


                if (abstractRecord.PupilSizeNA != null) CheckBoxPupilSizeNA.Checked = ((bool)abstractRecord.PupilSizeNA);
                if (abstractRecord.IOPODNA != null) CheckBoxIOPODNA.Checked = ((bool)abstractRecord.IOPODNA);
                if (abstractRecord.IOPOSNA != null) CheckBoxIOPOSNA.Checked = ((bool)abstractRecord.IOPOSNA);
                if (abstractRecord.KeratometryNA != null) CheckBoxKeratometryNA.Checked = ((bool)abstractRecord.KeratometryNA);
                if (abstractRecord.CornealThicknessODNA != null) CheckBoxCornealThicknessODNA.Checked = ((bool)abstractRecord.CornealThicknessODNA);
                if (abstractRecord.CornealThicknessOSNA != null) CheckBoxCornealThicknessOSNA.Checked = ((bool)abstractRecord.CornealThicknessOSNA);
                if (abstractRecord.TestWaveFrontMeasure != null) CheckBoxTestWaveFrontMeasure.Checked = ((bool)abstractRecord.TestWaveFrontMeasure);
                if (abstractRecord.TestSchirmerTear != null) CheckBoxTestSchirmerTear.Checked = ((bool)abstractRecord.TestSchirmerTear);
                if (abstractRecord.TestVitalDye != null) CheckBoxTestVitalDye.Checked = ((bool)abstractRecord.TestVitalDye);
                if (abstractRecord.SlitLampBlepharitis != null) CheckBoxSlitLampBlepharitis.Checked = ((bool)abstractRecord.SlitLampBlepharitis);
                if (abstractRecord.SlitLampAnterior != null) CheckBoxSlitLampAnterior.Checked = ((bool)abstractRecord.SlitLampAnterior);
                if (abstractRecord.SlitLampStromal != null) CheckBoxSlitLampStromal.Checked = ((bool)abstractRecord.SlitLampStromal);
                if (abstractRecord.SlitLampGuttata != null) CheckBoxSlitLampGuttata.Checked = ((bool)abstractRecord.SlitLampGuttata);
                if (abstractRecord.SlitLampCornealScar != null) CheckBoxSlitLampCornealScar.Checked = ((bool)abstractRecord.SlitLampCornealScar);
                if (abstractRecord.SlitLampVogts != null) CheckBoxSlitLampVogts.Checked = ((bool)abstractRecord.SlitLampVogts);
                if (abstractRecord.SlitLampNone != null) CheckBoxSlitLampNone.Checked = ((bool)abstractRecord.SlitLampNone);
                if (abstractRecord.SlitLampNA != null) CheckBoxSlitLampNA.Checked = ((bool)abstractRecord.SlitLampNA);
                if (abstractRecord.IntraopFlap != null) CheckBoxIntraopFlap.Checked = ((bool)abstractRecord.IntraopFlap);
                if (abstractRecord.IntraopIncompleteFlap != null) CheckBoxIntraopIncompleteFlap.Checked = ((bool)abstractRecord.IntraopIncompleteFlap);
                if (abstractRecord.IntraopIncompleteSide != null) CheckBoxIntraopIncompleteSide.Checked = ((bool)abstractRecord.IntraopIncompleteSide);
                if (abstractRecord.IntraopFlapTear != null) CheckBoxIntraopFlapTear.Checked = ((bool)abstractRecord.IntraopFlapTear);
                if (abstractRecord.IntraopFree != null) CheckBoxIntraopFree.Checked = ((bool)abstractRecord.IntraopFree);
                if (abstractRecord.IntraopGlobePerforation != null) CheckBoxIntraopGlobePerforation.Checked = ((bool)abstractRecord.IntraopGlobePerforation);
                if (abstractRecord.IntraopButtonhole != null) CheckBoxIntraopButtonhole.Checked = ((bool)abstractRecord.IntraopButtonhole);
                if (abstractRecord.IntraopOther != null) CheckBoxIntraopOther.Checked = ((bool)abstractRecord.IntraopOther);
                if (abstractRecord.AdjunctiveFlapLift != null) CheckBoxAdjunctiveFlapLift.Checked = ((bool)abstractRecord.AdjunctiveFlapLift);
                if (abstractRecord.AdjunctiveFlapSuturing != null) CheckBoxAdjunctiveFlapSuturing.Checked = ((bool)abstractRecord.AdjunctiveFlapSuturing);
                if (abstractRecord.AdjunctiveLaserLamellar != null) CheckBoxAdjunctiveLaserLamellar.Checked = ((bool)abstractRecord.AdjunctiveLaserLamellar);
                if (abstractRecord.AdjunctiveLaserSideCut != null) CheckBoxAdjunctiveLaserSideCut.Checked = ((bool)abstractRecord.AdjunctiveLaserSideCut);
                if (abstractRecord.AdjunctiveOther != null) CheckBoxAdjunctiveOther.Checked = ((bool)abstractRecord.AdjunctiveOther);
                if (abstractRecord.AdjunctiveNont != null) CheckBoxAdjunctiveNont.Checked = ((bool)abstractRecord.AdjunctiveNont);
                if (abstractRecord.PostopInfection != null) CheckBoxPostopInfection.Checked = ((bool)abstractRecord.PostopInfection);
                if (abstractRecord.PostopCorneal != null) CheckBoxPostopCorneal.Checked = ((bool)abstractRecord.PostopCorneal);
                if (abstractRecord.PostopFlap != null) CheckBoxPostopFlap.Checked = ((bool)abstractRecord.PostopFlap);
                if (abstractRecord.PostopDiffuse != null) CheckBoxPostopDiffuse.Checked = ((bool)abstractRecord.PostopDiffuse);
                if (abstractRecord.PostopElevated != null) CheckBoxPostopElevated.Checked = ((bool)abstractRecord.PostopElevated);
                if (abstractRecord.PostopOther != null) CheckBoxPostopOther.Checked = ((bool)abstractRecord.PostopOther);
                if (abstractRecord.PostopNone != null) CheckBoxPostopNone.Checked = ((bool)abstractRecord.PostopNone);
                if (abstractRecord.OutcomeVAODNA != null) CheckBoxOutcomeVAODNA.Checked = ((bool)abstractRecord.OutcomeVAODNA);
                if (abstractRecord.OutcomeVAOSNA != null) CheckBoxOutcomeVAOSNA.Checked = ((bool)abstractRecord.OutcomeVAOSNA);
                if (abstractRecord.OutcomeManifestRefractionODNA != null) CheckBoxOutcomeManifestRefractionODNA.Checked = ((bool)abstractRecord.OutcomeManifestRefractionODNA);
                if (abstractRecord.OutcomeManifestRefractionOSNA != null) CheckBoxOutcomeManifestRefractionOSNA.Checked = ((bool)abstractRecord.OutcomeManifestRefractionOSNA);
                if (abstractRecord.OutcomeVABadNA != null) CheckBoxOutcomeVABadNA.Checked = ((bool)abstractRecord.OutcomeVABadNA);
                if (abstractRecord.OutcomeVABadResidual != null) CheckBoxOutcomeVABadResidual.Checked = ((bool)abstractRecord.OutcomeVABadResidual);
                if (abstractRecord.OutcomeVADryEye != null) CheckBoxOutcomeVADryEye.Checked = ((bool)abstractRecord.OutcomeVADryEye);
                if (abstractRecord.OutcomeVAFlap != null) CheckBoxOutcomeVAFlap.Checked = ((bool)abstractRecord.OutcomeVAFlap);
                if (abstractRecord.OutcomeVADiffuse != null) CheckBoxOutcomeVADiffuse.Checked = ((bool)abstractRecord.OutcomeVADiffuse);
                if (abstractRecord.OutcomeVAPlanned != null) CheckBoxOutcomeVAPlanned.Checked = ((bool)abstractRecord.OutcomeVAPlanned);
                if (abstractRecord.OutcomeVAUnknown != null) CheckBoxOutcomeVAUnknown.Checked = ((bool)abstractRecord.OutcomeVAUnknown);
                if (abstractRecord.OutcomeVAOther != null) CheckBoxOutcomeVAOther.Checked = ((bool)abstractRecord.OutcomeVAOther);
                if (abstractRecord.OutcomeVANA != null) CheckBoxOutcomeVANA.Checked = ((bool)abstractRecord.OutcomeVANA);
                if (abstractRecord.OutcomeComplicationNone != null) CheckBoxOutcomeComplicationNone.Checked = ((bool)abstractRecord.OutcomeComplicationNone);
                if (abstractRecord.OutcomeComplicationMicro != null) CheckBoxOutcomeComplicationMicro.Checked = ((bool)abstractRecord.OutcomeComplicationMicro);
                if (abstractRecord.OutcomeComplicationMacro != null) CheckBoxOutcomeComplicationMacro.Checked = ((bool)abstractRecord.OutcomeComplicationMacro);
                if (abstractRecord.OutcomeComplicationEpith != null) CheckBoxOutcomeComplicationEpith.Checked = ((bool)abstractRecord.OutcomeComplicationEpith);
                if (abstractRecord.OutcomeComplicationDiffuse != null) CheckBoxOutcomeComplicationDiffuse.Checked = ((bool)abstractRecord.OutcomeComplicationDiffuse);
                if (abstractRecord.OutcomeComplicationRetinal != null) CheckBoxOutcomeComplicationRetinal.Checked = ((bool)abstractRecord.OutcomeComplicationRetinal);
                if (abstractRecord.OutcomeComplicationIrregular != null) CheckBoxOutcomeComplicationIrregular.Checked = ((bool)abstractRecord.OutcomeComplicationIrregular);
                if (abstractRecord.OutcomeComplicationOther != null) CheckBoxOutcomeComplicationOther.Checked = ((bool)abstractRecord.OutcomeComplicationOther);
                if (abstractRecord.OutcomeComplicationNA != null) CheckBoxOutcomeComplicationNA.Checked = ((bool)abstractRecord.OutcomeComplicationNA);
                if (abstractRecord.OutcomeEnhancementSurgeryNA != null) CheckBoxOutcomeEnhancementSurgeryNA.Checked = ((bool)abstractRecord.OutcomeEnhancementSurgeryNA);



                if (abstractRecord.RBGender != null) RadioButtonListRBGender.SelectedValue = abstractRecord.RBGender.ToString();
                if (abstractRecord.RBHistoryEyeSurgery != null) RadioButtonListRBHistoryEyeSurgery.SelectedValue = abstractRecord.RBHistoryEyeSurgery.ToString();
                if (abstractRecord.RBRequiredAdd != null) RadioButtonListRBRequiredAdd.SelectedValue = abstractRecord.RBRequiredAdd.ToString();
                if (abstractRecord.RBOcularDominance != null) RadioButtonListRBOcularDominance.SelectedValue = abstractRecord.RBOcularDominance.ToString();
                if (abstractRecord.RBEyelidFunction != null) RadioButtonListRBEyelidFunction.SelectedValue = abstractRecord.RBEyelidFunction.ToString();
                if (abstractRecord.RBGoalsExplained != null) RadioButtonListRBGoalsExplained.SelectedValue = abstractRecord.RBGoalsExplained.ToString();
                if (abstractRecord.RBOutOfLenses != null) RadioButtonListRBOutOfLenses.SelectedValue = abstractRecord.RBOutOfLenses.ToString();
                if (abstractRecord.AssessmentEye != null) RadioButtonListAssessmentEye.SelectedValue = abstractRecord.AssessmentEye.ToString();
                
                if (abstractRecord.RBEyelidsScrubbed != null) RadioButtonListRBEyelidsScrubbed.SelectedValue = abstractRecord.RBEyelidsScrubbed.ToString();
                if (abstractRecord.RBSterileSurgicalGloves != null) RadioButtonListRBSterileSurgicalGloves.SelectedValue = abstractRecord.RBSterileSurgicalGloves.ToString();
                if (abstractRecord.RBSurgicalIntervention != null) RadioButtonListRBSurgicalIntervention.SelectedValue = abstractRecord.RBSurgicalIntervention.ToString();
                if (abstractRecord.RBIntraopComplications != null) RadioButtonListRBIntraopComplications.SelectedValue = abstractRecord.RBIntraopComplications.ToString();
                if (abstractRecord.RBPostop48Hours != null) RadioButtonListRBPostop48Hours.SelectedValue = abstractRecord.RBPostop48Hours.ToString();
                if (abstractRecord.RBPostopAntibiotic != null) RadioButtonListRBPostopAntibiotic.SelectedValue = abstractRecord.RBPostopAntibiotic.ToString();
                if (abstractRecord.RBPostopAntiInflam != null) RadioButtonListRBPostopAntiInflam.SelectedValue = abstractRecord.RBPostopAntiInflam.ToString();
                if (abstractRecord.RBOutcomeTargetOD != null) RadioButtonListRBOutcomeTargetOD.SelectedValue = abstractRecord.RBOutcomeTargetOD.ToString();
                if (abstractRecord.RBOutcomeTargetOS != null) RadioButtonListRBOutcomeTargetOS.SelectedValue = abstractRecord.RBOutcomeTargetOS.ToString();
                if (abstractRecord.RBOutcomeEnhancementSurgery != null) RadioButtonListRBOutcomeEnhancementSurgery.SelectedValue = abstractRecord.RBOutcomeEnhancementSurgery.ToString();
                if (abstractRecord.OutcomeVAOD != null) DropDownListOutcomeVAOD.SelectedValue = abstractRecord.OutcomeVAOD.ToString();
                if (abstractRecord.OutcomeVAOS != null) DropDownListOutcomeVAOS.SelectedValue = abstractRecord.OutcomeVAOS.ToString();
                if (abstractRecord.DistVAManifestrefractionOD != null) DropDownListDistVAManifestRefractionOD.SelectedValue = abstractRecord.DistVAManifestrefractionOD.ToString();
                if (abstractRecord.DistVAManifestrefractionOS != null) DropDownListDistVAManifestRefractionOS.SelectedValue = abstractRecord.DistVAManifestrefractionOS.ToString();
                if (abstractRecord.MonthOfBirth != null) DropDownListMonthOfBirth.SelectedValue = abstractRecord.MonthOfBirth.ToString();
                if (abstractRecord.YearOfBirth != null) DropDownListYearOfBirth.SelectedValue = abstractRecord.YearOfBirth.ToString();
                if (abstractRecord.MonthOfPreopExam != null) DropDownListMonthOfPreopExam.SelectedValue = abstractRecord.MonthOfPreopExam.ToString();
                if (abstractRecord.YearOfPreopExam != null) DropDownListYearOfPreopExam.SelectedValue = abstractRecord.YearOfPreopExam.ToString();
                if (abstractRecord.NearVAManifestRefractionOD != null) DropDownListNearVAManifestRefractionOD.SelectedValue = abstractRecord.NearVAManifestRefractionOD.ToString();
                if (abstractRecord.NearVAManifestRefractionOS != null) DropDownListNearVAManifestRefractionOS.SelectedValue = abstractRecord.NearVAManifestRefractionOS.ToString();
                if (abstractRecord.OutcomeEnhancementSurgeryODMonth != null) DropDownListOutcomeEnhancementSurgeryODMonth.SelectedValue = abstractRecord.OutcomeEnhancementSurgeryODMonth.ToString();
                if (abstractRecord.OutcomeEnhancementSurgeryODYeaer != null) DropDownListOutcomeEnhancementSurgeryODYear.SelectedValue = abstractRecord.OutcomeEnhancementSurgeryODYeaer.ToString();
                if (abstractRecord.OutcomeEnhancementSurgeryOSMonth != null) DropDownListOutcomeEnhancementSurgeryOSMonth.SelectedValue = abstractRecord.OutcomeEnhancementSurgeryOSMonth.ToString();
                if (abstractRecord.OutcomeEnhancementSurgeryOSYear != null) DropDownListOutcomeEnhancementSurgeryOSYear.SelectedValue = abstractRecord.OutcomeEnhancementSurgeryOSYear.ToString();
                if (abstractRecord.DistanceVACycloplegicOD != null) DropDownListDistanceVACycloplegicOD.SelectedValue = abstractRecord.DistanceVACycloplegicOD.ToString();
                if (abstractRecord.DistanceVACycloplegicOS != null) DropDownListDistanceVACycloplegicOS.SelectedValue = abstractRecord.DistanceVACycloplegicOS.ToString();


                if (abstractRecord.PupilSizeODLight != null) DropDownListPupilSizeODLight.SelectedValue = abstractRecord.PupilSizeODLight.ToString();
                if (abstractRecord.PupilSizeOSLight != null) DropDownListPupilSizeOSLight.SelectedValue = abstractRecord.PupilSizeOSLight.ToString();
                if (abstractRecord.PupilSizeODDark != null) DropDownListPupilSizeODDark.SelectedValue = abstractRecord.PupilSizeODDark.ToString();
                if (abstractRecord.PupilSizeOSDark != null) DropDownListPupilSizeOSDark.SelectedValue = abstractRecord.PupilSizeOSDark.ToString();



                if (abstractRecord.MonovisionSimulation != null)
                {


                    RadioButtonMonovisionSimulationYes.Checked = ((bool)abstractRecord.MonovisionSimulation);
                    RadioButtonMonovisionSimulationNo.Checked = !((bool)abstractRecord.MonovisionSimulation);
                }

                if (abstractRecord.VisualRequirementsDoc != null)
                {
                    RadioButtonVisualRequirementsDocYes.Checked = ((bool)abstractRecord.VisualRequirementsDoc);
                    RadioButtonVisualRequirementsDocNo.Checked = !((bool)abstractRecord.VisualRequirementsDoc);
                }
                if (abstractRecord.HistoryDryEye != null)
                {
                    RadioButtonHistoryDryEyeYes.Checked = ((bool)abstractRecord.HistoryDryEye);
                    RadioButtonHistoryDryEyeNo.Checked = !((bool)abstractRecord.HistoryDryEye);
                }
                if (abstractRecord.HistoryKeratoconus != null)
                {
                    RadioButtonHistoryKeratoconusYes.Checked = ((bool)abstractRecord.HistoryKeratoconus);
                    RadioButtonHistoryKeratoconusNo.Checked = !((bool)abstractRecord.HistoryKeratoconus);
                }
                if (abstractRecord.HistoryInfection != null)
                {
                    RadioButtonHistoryInfectionYes.Checked = ((bool)abstractRecord.HistoryInfection);
                    RadioButtonHistoryInfectionNo.Checked = !((bool)abstractRecord.HistoryInfection);
                }
                if (abstractRecord.HistoryGlaucoma != null)
                {
                    RadioButtonHistoryGlaucomaYes.Checked = ((bool)abstractRecord.HistoryGlaucoma);
                    RadioButtonHistoryGlaucomaNo.Checked = !((bool)abstractRecord.HistoryGlaucoma);
                }
                if (abstractRecord.HistoryStrabismus != null)
                {
                    RadioButtonHistoryStrabismusYes.Checked = ((bool)abstractRecord.HistoryStrabismus);
                    RadioButtonHistoryStrabismusNo.Checked = !((bool)abstractRecord.HistoryStrabismus);
                }
                if (abstractRecord.HistoryOther != null)
                {
                    RadioButtonHistoryOtherYes.Checked = ((bool)abstractRecord.HistoryOther);
                    RadioButtonHistoryOtherNo.Checked = !((bool)abstractRecord.HistoryOther);
                }
                if (abstractRecord.RefractiveCorrectionOD1Sign != null)
                {
                    RadioButtonRefractiveCorrectionOD1SignYes.Checked = ((bool)abstractRecord.RefractiveCorrectionOD1Sign);
                    RadioButtonRefractiveCorrectionOD1SignNo.Checked = !((bool)abstractRecord.RefractiveCorrectionOD1Sign);
                }
                if (abstractRecord.RefractiveCorrectionOS1Sign != null)
                {
                    RadioButtonRefractiveCorrectionOS1SignYes.Checked = ((bool)abstractRecord.RefractiveCorrectionOS1Sign);
                    RadioButtonRefractiveCorrectionOS1SignNo.Checked = !((bool)abstractRecord.RefractiveCorrectionOS1Sign);
                }
                if (abstractRecord.ManifestRefractionOD1Sign != null)
                {
                    RadioButtonManifestRefractionOD1SignYes.Checked = ((bool)abstractRecord.ManifestRefractionOD1Sign);
                    RadioButtonManifestRefractionOD1SignNo.Checked = !((bool)abstractRecord.ManifestRefractionOD1Sign);
                }
                if (abstractRecord.ManifestRefractionOS1Sign != null)
                {
                    RadioButtonManifestRefractionOS1SignYes.Checked = ((bool)abstractRecord.ManifestRefractionOS1Sign);
                    RadioButtonManifestRefractionOS1SignNo.Checked = !((bool)abstractRecord.ManifestRefractionOS1Sign);
                }
                if (abstractRecord.CycloplegicRefractionOD1Sign != null)
                {
                    RadioButtonCycloplegicRefractionOD1SignYes.Checked = ((bool)abstractRecord.CycloplegicRefractionOD1Sign);
                    RadioButtonCycloplegicRefractionOD1SignNo.Checked = !((bool)abstractRecord.CycloplegicRefractionOD1Sign);
                }
                if (abstractRecord.CycloplegicRefractionOS1Sign != null)
                {
                    RadioButtonCycloplegicRefractionOS1SignYes.Checked = ((bool)abstractRecord.CycloplegicRefractionOS1Sign);
                    RadioButtonCycloplegicRefractionOS1SignNo.Checked = !((bool)abstractRecord.CycloplegicRefractionOS1Sign);
                }
                if (abstractRecord.CornealTopDoc != null)
                {
                    RadioButtonCornealTopDocYes.Checked = ((bool)abstractRecord.CornealTopDoc);
                    RadioButtonCornealTopDocNo.Checked = !((bool)abstractRecord.CornealTopDoc);
                }
                if (abstractRecord.OutcomeManifestRefractionOD1Sign != null)
                {
                    RadioButtonOutcomeManifestRefractionOD1SignYes.Checked = ((bool)abstractRecord.OutcomeManifestRefractionOD1Sign);
                    RadioButtonOutcomeManifestRefractionOD1SignNo.Checked = !((bool)abstractRecord.OutcomeManifestRefractionOD1Sign);
                }
                if (abstractRecord.OutcomeManifestRefractionOS1Sign != null)
                {
                    RadioButtonOutcomeManifestRefractionOS1SignYes.Checked = ((bool)abstractRecord.OutcomeManifestRefractionOS1Sign);
                    RadioButtonOutcomeManifestRefractionOS1SignNo.Checked = !((bool)abstractRecord.OutcomeManifestRefractionOS1Sign);
                }
                if (abstractRecord.OutcomeBCVADecrease != null)
                {
                    RadioButtonOutcomeBCVADecreaseYes.Checked = ((bool)abstractRecord.OutcomeBCVADecrease);
                    RadioButtonOutcomeBCVADecreaseNo.Checked = !((bool)abstractRecord.OutcomeBCVADecrease);
                }


                if (abstractRecord.IntraopOtherText != null) TextBoxIntraopOtherText.Text = abstractRecord.IntraopOtherText.ToString();
                if (abstractRecord.HistoryOtherText != null) TextBoxHistoryOtherText.Text = abstractRecord.HistoryOtherText.ToString();
                if (abstractRecord.RefractiveCorrectionOD1 != null) TextBoxRefractiveCorrectionOD1.Text = abstractRecord.RefractiveCorrectionOD1.ToString();
                if (abstractRecord.RefractiveCorrectionOD2 != null) TextBoxRefractiveCorrectionOD2.Text = abstractRecord.RefractiveCorrectionOD2.ToString();
                if (abstractRecord.RefractiveCorrectionOD3 != null) TextBoxRefractiveCorrectionOD3.Text = abstractRecord.RefractiveCorrectionOD3.ToString();
                if (abstractRecord.RefractiveCorrectionOS1 != null) TextBoxRefractiveCorrectionOS1.Text = abstractRecord.RefractiveCorrectionOS1.ToString();
                if (abstractRecord.RefractiveCorrectionOS2 != null) TextBoxRefractiveCorrectionOS2.Text = abstractRecord.RefractiveCorrectionOS2.ToString();
                if (abstractRecord.RefractiveCorrectionOS3 != null) TextBoxRefractiveCorrectionOS3.Text = abstractRecord.RefractiveCorrectionOS3.ToString();
                if (abstractRecord.ManifestRefractionOD1 != null) TextBoxManifestRefractionOD1.Text = abstractRecord.ManifestRefractionOD1.ToString();
                if (abstractRecord.ManifestRefractionOD2 != null) TextBoxManifestRefractionOD2.Text = abstractRecord.ManifestRefractionOD2.ToString();
                if (abstractRecord.ManifestRefractionOD3 != null) TextBoxManifestRefractionOD3.Text = abstractRecord.ManifestRefractionOD3.ToString();
                if (abstractRecord.ManifestRefractionOS1 != null) TextBoxManifestRefractionOS1.Text = abstractRecord.ManifestRefractionOS1.ToString();
                if (abstractRecord.ManifestRefractionOS2 != null) TextBoxManifestRefractionOS2.Text = abstractRecord.ManifestRefractionOS2.ToString();
                if (abstractRecord.ManifestRefractionOS3 != null) TextBoxManifestRefractionOS3.Text = abstractRecord.ManifestRefractionOS3.ToString();
                if (abstractRecord.CycloplegicRefractionOD1 != null) TextBoxCycloplegicRefractionOD1.Text = abstractRecord.CycloplegicRefractionOD1.ToString();
                if (abstractRecord.CycloplegicRefractionOD2 != null) TextBoxCycloplegicRefractionOD2.Text = abstractRecord.CycloplegicRefractionOD2.ToString();
                if (abstractRecord.CycloplegicRefractionOD3 != null) TextBoxCycloplegicRefractionOD3.Text = abstractRecord.CycloplegicRefractionOD3.ToString();
                if (abstractRecord.CycloplegicRefractionOS1 != null) TextBoxCycloplegicRefractionOS1.Text = abstractRecord.CycloplegicRefractionOS1.ToString();
                if (abstractRecord.CycloplegicRefractionOS2 != null) TextBoxCycloplegicRefractionOS2.Text = abstractRecord.CycloplegicRefractionOS2.ToString();
                if (abstractRecord.CycloplegicRefractionOS3 != null) TextBoxCycloplegicRefractionOS3.Text = abstractRecord.CycloplegicRefractionOS3.ToString();
 
               
                if (abstractRecord.IOPOD != null) TextBoxIOPOD.Text = abstractRecord.IOPOD.ToString();
                if (abstractRecord.IOPOS != null) TextBoxIOPOS.Text = abstractRecord.IOPOS.ToString();
                if (abstractRecord.KeratometryFlatOD != null) TextBoxKeratometryFlatOD.Text = abstractRecord.KeratometryFlatOD.ToString();
                if (abstractRecord.KeratometrySteepOD != null) TextBoxKeratometrySteepOD.Text = abstractRecord.KeratometrySteepOD.ToString();
                if (abstractRecord.KeratometryAxisOD != null) TextBoxKeratometryAxisOD.Text = abstractRecord.KeratometryAxisOD.ToString();
                if (abstractRecord.KeratometryFlatOS != null) TextBoxKeratometryFlatOS.Text = abstractRecord.KeratometryFlatOS.ToString();
                if (abstractRecord.KeratometrySteepOS != null) TextBoxKeratometrySteepOS.Text = abstractRecord.KeratometrySteepOS.ToString();
                if (abstractRecord.KeratometryAxisOS != null) TextBoxKeratometryAxisOS.Text = abstractRecord.KeratometryAxisOS.ToString();
                if (abstractRecord.CornealThicknessOD != null) TextBoxCornealThicknessOD.Text = abstractRecord.CornealThicknessOD.ToString();
                if (abstractRecord.CornealThicknessOS != null) TextBoxCornealThicknessOS.Text = abstractRecord.CornealThicknessOS.ToString();
                if (abstractRecord.RBIntendedPostopRefractionOD != null) TextBoxRBIntendedPostopRefractionOD.Text = abstractRecord.RBIntendedPostopRefractionOD.ToString();
                if (abstractRecord.RBIntendedPostopRefractionOS != null) TextBoxRBIntendedPostopRefractionOS.Text = abstractRecord.RBIntendedPostopRefractionOS.ToString();
                if (abstractRecord.AdjunctiveOtherText != null) TextBoxAdjunctiveOtherText.Text = abstractRecord.AdjunctiveOtherText.ToString();
               
                if (abstractRecord.OutcomeManifestRefractionOD1 != null) TextBoxOutcomeManifestRefractionOD1.Text = abstractRecord.OutcomeManifestRefractionOD1.ToString();
                if (abstractRecord.OutcomeManifestRefractionOD2 != null) TextBoxOutcomeManifestRefractionOD2.Text = abstractRecord.OutcomeManifestRefractionOD2.ToString();
                if (abstractRecord.OutcomeManifestRefractionOD3 != null) TextBoxOutcomeManifestRefractionOD3.Text = abstractRecord.OutcomeManifestRefractionOD3.ToString();
                if (abstractRecord.OutcomeManifestRefractionOS1 != null) TextBoxOutcomeManifestRefractionOS1.Text = abstractRecord.OutcomeManifestRefractionOS1.ToString();
                if (abstractRecord.OutcomeManifestRefractionOS2 != null) TextBoxOutcomeManifestRefractionOS2.Text = abstractRecord.OutcomeManifestRefractionOS2.ToString();
                if (abstractRecord.OutcomeManifestRefractionOS3 != null) TextBoxOutcomeManifestRefractionOS3.Text = abstractRecord.OutcomeManifestRefractionOS3.ToString();
                if (abstractRecord.OutcomeVAOtherText != null) TextBoxOutcomeVAOtherText.Text = abstractRecord.OutcomeVAOtherText.ToString();
                if (abstractRecord.OutcomeComplicationOtherText != null) TextBoxOutcomeComplicationOtherText.Text = abstractRecord.OutcomeComplicationOtherText.ToString();
              

            }
        }
    }

    public void savedata()
    {
        using (NetHealthPIMEntities pim = new NetHealthPIMEntities())
        {
            int ModuleID = (int)Session[Constants.SESSION_WORKINGMODULEID];
            int cycleID = ctx.ActiveModuleCycleID;
            ParticipantModuleCycle cycle = pim.ParticipantModuleCycle.First(c => c.ParticipantModuleCycleID == cycleID);
            string recordIdentifier = HiddenFieldRecordIdentifier.Value;
            using (TransactionScope transaction = new TransactionScope())
            {
                 ChartAbstractionLasik abstractRecord = (from c in pim. ChartAbstractionLasik
                                                            where c.ParticipantModuleCycle.ParticipantModuleCycleID == cycleID & c.RecordIdentifier == recordIdentifier
                                                            select c).First< ChartAbstractionLasik>();
           
            
            
            
            
                        abstractRecord.RefractiveCorrectionNA = CheckBoxRefractiveCorrectionNA.Checked;
                        abstractRecord.ManifestRefractionODNA = CheckBoxManifestRefractionODNA.Checked;
                        abstractRecord.ManifestRefractionOSNA = CheckBoxManifestRefractionOSNA.Checked;
                   
                        abstractRecord.NearVAManifestRefractionNA = CheckBoxNearVAManifestRefractionNA.Checked;
                        abstractRecord.CycloplegicRefractionODNA = CheckBoxCycloplegicRefractionODNA.Checked;
                        abstractRecord.CycloplegicRefractionOSNA = CheckBoxCycloplegicRefractionOSNA.Checked;
                        abstractRecord.DistanceVACycloplegicNA = CheckBoxDistanceVACycloplegicNA.Checked;

                       abstractRecord.DistVAManifestrefractionNA= CheckBoxDistVAManifestRefractionNA.Checked;

                        abstractRecord.PupilSizeNA = CheckBoxPupilSizeNA.Checked;
                        abstractRecord.IOPODNA = CheckBoxIOPODNA.Checked;
                        abstractRecord.IOPOSNA = CheckBoxIOPOSNA.Checked;
                        abstractRecord.KeratometryNA = CheckBoxKeratometryNA.Checked;
                        abstractRecord.CornealThicknessODNA = CheckBoxCornealThicknessODNA.Checked;
                        abstractRecord.CornealThicknessOSNA = CheckBoxCornealThicknessOSNA.Checked;
                        abstractRecord.TestWaveFrontMeasure = CheckBoxTestWaveFrontMeasure.Checked;
                        abstractRecord.TestSchirmerTear = CheckBoxTestSchirmerTear.Checked;
                        abstractRecord.TestVitalDye = CheckBoxTestVitalDye.Checked;
                        abstractRecord.SlitLampBlepharitis = CheckBoxSlitLampBlepharitis.Checked;
                        abstractRecord.SlitLampAnterior = CheckBoxSlitLampAnterior.Checked;
                        abstractRecord.SlitLampStromal = CheckBoxSlitLampStromal.Checked;
                        abstractRecord.SlitLampGuttata = CheckBoxSlitLampGuttata.Checked;
                        abstractRecord.SlitLampCornealScar = CheckBoxSlitLampCornealScar.Checked;
                        abstractRecord.SlitLampVogts = CheckBoxSlitLampVogts.Checked;
                        abstractRecord.SlitLampNone = CheckBoxSlitLampNone.Checked;
                        abstractRecord.SlitLampNA = CheckBoxSlitLampNA.Checked;
                        abstractRecord.IntraopFlap = CheckBoxIntraopFlap.Checked;
                        abstractRecord.IntraopIncompleteFlap = CheckBoxIntraopIncompleteFlap.Checked;
                        abstractRecord.IntraopIncompleteSide = CheckBoxIntraopIncompleteSide.Checked;
                        abstractRecord.IntraopFlapTear = CheckBoxIntraopFlapTear.Checked;
                        abstractRecord.IntraopFree = CheckBoxIntraopFree.Checked;
                        abstractRecord.IntraopGlobePerforation = CheckBoxIntraopGlobePerforation.Checked;
                        abstractRecord.IntraopButtonhole = CheckBoxIntraopButtonhole.Checked;
                        abstractRecord.IntraopOther = CheckBoxIntraopOther.Checked;
                        abstractRecord.AdjunctiveFlapLift = CheckBoxAdjunctiveFlapLift.Checked;
                        abstractRecord.AdjunctiveFlapSuturing = CheckBoxAdjunctiveFlapSuturing.Checked;
                        abstractRecord.AdjunctiveLaserLamellar = CheckBoxAdjunctiveLaserLamellar.Checked;
                        abstractRecord.AdjunctiveLaserSideCut = CheckBoxAdjunctiveLaserSideCut.Checked;
                        abstractRecord.AdjunctiveOther = CheckBoxAdjunctiveOther.Checked;
                        abstractRecord.AdjunctiveNont = CheckBoxAdjunctiveNont.Checked;
                        abstractRecord.PostopInfection = CheckBoxPostopInfection.Checked;
                        abstractRecord.PostopCorneal = CheckBoxPostopCorneal.Checked;
                        abstractRecord.PostopFlap = CheckBoxPostopFlap.Checked;
                        abstractRecord.PostopDiffuse = CheckBoxPostopDiffuse.Checked;
                        abstractRecord.PostopElevated = CheckBoxPostopElevated.Checked;
                        abstractRecord.PostopOther = CheckBoxPostopOther.Checked;
                        abstractRecord.PostopNone = CheckBoxPostopNone.Checked;
                        abstractRecord.OutcomeVAODNA = CheckBoxOutcomeVAODNA.Checked;
                        abstractRecord.OutcomeVAOSNA = CheckBoxOutcomeVAOSNA.Checked;
                        abstractRecord.OutcomeManifestRefractionODNA = CheckBoxOutcomeManifestRefractionODNA.Checked;
                        abstractRecord.OutcomeManifestRefractionOSNA = CheckBoxOutcomeManifestRefractionOSNA.Checked;
                        abstractRecord.OutcomeVABadNA = CheckBoxOutcomeVABadNA.Checked;
                        abstractRecord.OutcomeVABadResidual = CheckBoxOutcomeVABadResidual.Checked;
                        abstractRecord.OutcomeVADryEye = CheckBoxOutcomeVADryEye.Checked;
                        abstractRecord.OutcomeVAFlap = CheckBoxOutcomeVAFlap.Checked;
                        abstractRecord.OutcomeVADiffuse = CheckBoxOutcomeVADiffuse.Checked;
                        abstractRecord.OutcomeVAPlanned = CheckBoxOutcomeVAPlanned.Checked;
                        abstractRecord.OutcomeVAUnknown = CheckBoxOutcomeVAUnknown.Checked;
                        abstractRecord.OutcomeVAOther = CheckBoxOutcomeVAOther.Checked;
                        abstractRecord.OutcomeVANA = CheckBoxOutcomeVANA.Checked;
                        abstractRecord.OutcomeComplicationNone = CheckBoxOutcomeComplicationNone.Checked;
                        abstractRecord.OutcomeComplicationMicro = CheckBoxOutcomeComplicationMicro.Checked;
                        abstractRecord.OutcomeComplicationMacro = CheckBoxOutcomeComplicationMacro.Checked;
                        abstractRecord.OutcomeComplicationEpith = CheckBoxOutcomeComplicationEpith.Checked;
                        abstractRecord.OutcomeComplicationDiffuse = CheckBoxOutcomeComplicationDiffuse.Checked;
                        abstractRecord.OutcomeComplicationRetinal = CheckBoxOutcomeComplicationRetinal.Checked;
                        abstractRecord.OutcomeComplicationIrregular = CheckBoxOutcomeComplicationIrregular.Checked;
                        abstractRecord.OutcomeComplicationOther = CheckBoxOutcomeComplicationOther.Checked;
                        abstractRecord.OutcomeComplicationNA = CheckBoxOutcomeComplicationNA.Checked;
                        abstractRecord.OutcomeEnhancementSurgeryNA = CheckBoxOutcomeEnhancementSurgeryNA.Checked;



                        if (RadioButtonListRBGender.SelectedIndex != -1) abstractRecord.RBGender = Convert.ToInt32(RadioButtonListRBGender.SelectedValue);
                        else abstractRecord.RBGender = 0;
                        if (RadioButtonListRBHistoryEyeSurgery.SelectedIndex != -1) abstractRecord.RBHistoryEyeSurgery = Convert.ToInt32(RadioButtonListRBHistoryEyeSurgery.SelectedValue);
                        else abstractRecord.RBHistoryEyeSurgery = 0;
                        if (RadioButtonListRBRequiredAdd.SelectedIndex != -1) abstractRecord.RBRequiredAdd = Convert.ToInt32(RadioButtonListRBRequiredAdd.SelectedValue);
                        else abstractRecord.RBRequiredAdd = 0;
                        if (RadioButtonListRBOcularDominance.SelectedIndex != -1) abstractRecord.RBOcularDominance = Convert.ToInt32(RadioButtonListRBOcularDominance.SelectedValue);
                        else abstractRecord.RBOcularDominance = 0;
                        if (RadioButtonListRBEyelidFunction.SelectedIndex != -1) abstractRecord.RBEyelidFunction = Convert.ToInt32(RadioButtonListRBEyelidFunction.SelectedValue);
                        else abstractRecord.RBEyelidFunction = 0;
                        if (RadioButtonListRBGoalsExplained.SelectedIndex != -1) abstractRecord.RBGoalsExplained = Convert.ToInt32(RadioButtonListRBGoalsExplained.SelectedValue);
                        else abstractRecord.RBGoalsExplained = 0;
                        if (RadioButtonListRBOutOfLenses.SelectedIndex != -1) abstractRecord.RBOutOfLenses = Convert.ToInt32(RadioButtonListRBOutOfLenses.SelectedValue);
                        else abstractRecord.RBOutOfLenses = 0;
                        if (RadioButtonListRBEyelidsScrubbed.SelectedIndex != -1) abstractRecord.RBEyelidsScrubbed = Convert.ToInt32(RadioButtonListRBEyelidsScrubbed.SelectedValue);
                        else abstractRecord.RBEyelidsScrubbed = 0;
                        if (RadioButtonListRBSterileSurgicalGloves.SelectedIndex != -1) abstractRecord.RBSterileSurgicalGloves = Convert.ToInt32(RadioButtonListRBSterileSurgicalGloves.SelectedValue);
                        else abstractRecord.RBSterileSurgicalGloves = 0;
                        if (RadioButtonListRBSurgicalIntervention.SelectedIndex != -1) abstractRecord.RBSurgicalIntervention = Convert.ToInt32(RadioButtonListRBSurgicalIntervention.SelectedValue);
                        else abstractRecord.RBSurgicalIntervention = 0;
                        if (RadioButtonListRBIntraopComplications.SelectedIndex != -1) abstractRecord.RBIntraopComplications = Convert.ToInt32(RadioButtonListRBIntraopComplications.SelectedValue);
                        else abstractRecord.RBIntraopComplications = 0;
                        if (RadioButtonListRBPostop48Hours.SelectedIndex != -1) abstractRecord.RBPostop48Hours = Convert.ToInt32(RadioButtonListRBPostop48Hours.SelectedValue);
                        else abstractRecord.RBPostop48Hours = 0;
                        if (RadioButtonListRBPostopAntibiotic.SelectedIndex != -1) abstractRecord.RBPostopAntibiotic = Convert.ToInt32(RadioButtonListRBPostopAntibiotic.SelectedValue);
                        else abstractRecord.RBPostopAntibiotic = 0;
                        if (RadioButtonListRBPostopAntiInflam.SelectedIndex != -1) abstractRecord.RBPostopAntiInflam = Convert.ToInt32(RadioButtonListRBPostopAntiInflam.SelectedValue);
                        else abstractRecord.RBPostopAntiInflam = 0;
                        if (RadioButtonListRBOutcomeTargetOD.SelectedIndex != -1) abstractRecord.RBOutcomeTargetOD = Convert.ToInt32(RadioButtonListRBOutcomeTargetOD.SelectedValue);
                        else abstractRecord.RBOutcomeTargetOD = 0;
                        if (RadioButtonListRBOutcomeTargetOS.SelectedIndex != -1) abstractRecord.RBOutcomeTargetOS = Convert.ToInt32(RadioButtonListRBOutcomeTargetOS.SelectedValue);
                        else abstractRecord.RBOutcomeTargetOS = 0;
                        if (RadioButtonListRBOutcomeEnhancementSurgery.SelectedIndex != -1) abstractRecord.RBOutcomeEnhancementSurgery = Convert.ToInt32(RadioButtonListRBOutcomeEnhancementSurgery.SelectedValue);
                        else abstractRecord.RBOutcomeEnhancementSurgery = 0;


                        if (RadioButtonListAssessmentEye.SelectedIndex != -1) abstractRecord.AssessmentEye = Convert.ToInt32(RadioButtonListAssessmentEye.SelectedValue);
                        else abstractRecord.AssessmentEye = 0;

                        if (DropDownListOutcomeVAOD.SelectedIndex > 0) abstractRecord.OutcomeVAOD = Convert.ToInt32(DropDownListOutcomeVAOD.SelectedValue);
                        else abstractRecord.OutcomeVAOD = 0;

                        if (DropDownListOutcomeVAOS.SelectedIndex > 0) abstractRecord.OutcomeVAOS = Convert.ToInt32(DropDownListOutcomeVAOS.SelectedValue);
                        else abstractRecord.OutcomeVAOS = 0;
                        if (DropDownListDistVAManifestRefractionOD.SelectedIndex > 0) abstractRecord.DistVAManifestrefractionOD = Convert.ToInt32(DropDownListDistVAManifestRefractionOD.SelectedValue);
                        else abstractRecord.DistVAManifestrefractionOD = 0;
                        if (DropDownListDistVAManifestRefractionOS.SelectedIndex > 0) abstractRecord.DistVAManifestrefractionOS = Convert.ToInt32(DropDownListDistVAManifestRefractionOS.SelectedValue);
                        else abstractRecord.DistVAManifestrefractionOS = 0;
                        if (DropDownListMonthOfBirth.SelectedIndex > 0) abstractRecord.MonthOfBirth = Convert.ToInt32(DropDownListMonthOfBirth.SelectedValue);
                        else abstractRecord.MonthOfBirth = 0;
                        if (DropDownListYearOfBirth.SelectedIndex > 0) abstractRecord.YearOfBirth = Convert.ToInt32(DropDownListYearOfBirth.SelectedValue);
                        else abstractRecord.YearOfBirth = 0;
                        if (DropDownListMonthOfPreopExam.SelectedIndex > 0) abstractRecord.MonthOfPreopExam = Convert.ToInt32(DropDownListMonthOfPreopExam.SelectedValue);
                        else abstractRecord.MonthOfPreopExam = 0;
                        if (DropDownListYearOfPreopExam.SelectedIndex > 0) abstractRecord.YearOfPreopExam = Convert.ToInt32(DropDownListYearOfPreopExam.SelectedValue);
                        else abstractRecord.YearOfPreopExam = 0;
                     
                        if (DropDownListNearVAManifestRefractionOD.SelectedIndex > 0) abstractRecord.NearVAManifestRefractionOD = Convert.ToInt32(DropDownListNearVAManifestRefractionOD.SelectedValue);
                        else abstractRecord.NearVAManifestRefractionOD = 0;
                        if (DropDownListNearVAManifestRefractionOS.SelectedIndex > 0) abstractRecord.NearVAManifestRefractionOS = Convert.ToInt32(DropDownListNearVAManifestRefractionOS.SelectedValue);
                        else abstractRecord.NearVAManifestRefractionOS = 0;
                        if (DropDownListOutcomeEnhancementSurgeryODMonth.SelectedIndex > 0) abstractRecord.OutcomeEnhancementSurgeryODMonth = Convert.ToInt32(DropDownListOutcomeEnhancementSurgeryODMonth.SelectedValue);
                        else abstractRecord.OutcomeEnhancementSurgeryODMonth = 0;
                        if (DropDownListOutcomeEnhancementSurgeryODYear.SelectedIndex > 0) abstractRecord.OutcomeEnhancementSurgeryODYeaer = Convert.ToInt32(DropDownListOutcomeEnhancementSurgeryODYear.SelectedValue);
                        else abstractRecord.OutcomeEnhancementSurgeryODYeaer = 0;
                        if (DropDownListOutcomeEnhancementSurgeryOSMonth.SelectedIndex > 0) abstractRecord.OutcomeEnhancementSurgeryOSMonth = Convert.ToInt32(DropDownListOutcomeEnhancementSurgeryOSMonth.SelectedValue);
                        else abstractRecord.OutcomeEnhancementSurgeryOSMonth = 0;
                        if (DropDownListOutcomeEnhancementSurgeryOSYear.SelectedIndex > 0) abstractRecord.OutcomeEnhancementSurgeryOSYear = Convert.ToInt32(DropDownListOutcomeEnhancementSurgeryOSYear.SelectedValue);
                        else abstractRecord.OutcomeEnhancementSurgeryOSYear = 0;

                        if (DropDownListDistanceVACycloplegicOD.SelectedIndex > 0) abstractRecord.DistanceVACycloplegicOD = Convert.ToInt32(DropDownListDistanceVACycloplegicOD.SelectedValue);
                        else abstractRecord.DistanceVACycloplegicOD = 0;

                        if (DropDownListDistanceVACycloplegicOS.SelectedIndex > 0) abstractRecord.DistanceVACycloplegicOS = Convert.ToInt32(DropDownListDistanceVACycloplegicOS.SelectedValue);
                        else abstractRecord.DistanceVACycloplegicOS = 0;


                        if (DropDownListPupilSizeODLight.SelectedIndex > 0) abstractRecord.PupilSizeODLight = Convert.ToInt32(DropDownListPupilSizeODLight.SelectedValue);
                        else abstractRecord.PupilSizeODLight = 0;

                        if (DropDownListPupilSizeOSLight.SelectedIndex > 0) abstractRecord.PupilSizeOSLight = Convert.ToInt32(DropDownListPupilSizeOSLight.SelectedValue);
                        else abstractRecord.PupilSizeOSLight = 0;

                        if (DropDownListPupilSizeODDark.SelectedIndex > 0) abstractRecord.PupilSizeODDark = Convert.ToInt32(DropDownListPupilSizeODDark.SelectedValue);
                        else abstractRecord.PupilSizeODDark = 0;

                        if (DropDownListPupilSizeOSDark.SelectedIndex > 0) abstractRecord.PupilSizeOSDark = Convert.ToInt32(DropDownListPupilSizeOSDark.SelectedValue);
                        else abstractRecord.PupilSizeOSDark = 0;

                



                        if (RadioButtonMonovisionSimulationYes.Checked) abstractRecord.MonovisionSimulation = true;
                        if (RadioButtonMonovisionSimulationNo.Checked) abstractRecord.MonovisionSimulation = false;



                        if (RadioButtonVisualRequirementsDocYes.Checked) abstractRecord.VisualRequirementsDoc = true;
                        if (RadioButtonVisualRequirementsDocNo.Checked) abstractRecord.VisualRequirementsDoc = false;
                        if (RadioButtonHistoryDryEyeYes.Checked) abstractRecord.HistoryDryEye = true;
                        if (RadioButtonHistoryDryEyeNo.Checked) abstractRecord.HistoryDryEye = false;
                        if (RadioButtonHistoryKeratoconusYes.Checked) abstractRecord.HistoryKeratoconus = true;
                        if (RadioButtonHistoryKeratoconusNo.Checked) abstractRecord.HistoryKeratoconus = false;
                        if (RadioButtonHistoryInfectionYes.Checked) abstractRecord.HistoryInfection = true;
                        if (RadioButtonHistoryInfectionNo.Checked) abstractRecord.HistoryInfection = false;
                        if (RadioButtonHistoryGlaucomaYes.Checked) abstractRecord.HistoryGlaucoma = true;
                        if (RadioButtonHistoryGlaucomaNo.Checked) abstractRecord.HistoryGlaucoma = false;
                        if (RadioButtonHistoryStrabismusYes.Checked) abstractRecord.HistoryStrabismus = true;
                        if (RadioButtonHistoryStrabismusNo.Checked) abstractRecord.HistoryStrabismus = false;
                        if (RadioButtonHistoryOtherYes.Checked) abstractRecord.HistoryOther = true;
                        if (RadioButtonHistoryOtherNo.Checked) abstractRecord.HistoryOther = false;
                        if (RadioButtonRefractiveCorrectionOD1SignYes.Checked) abstractRecord.RefractiveCorrectionOD1Sign = true;
                        if (RadioButtonRefractiveCorrectionOD1SignNo.Checked) abstractRecord.RefractiveCorrectionOD1Sign = false;
                        if (RadioButtonRefractiveCorrectionOS1SignYes.Checked) abstractRecord.RefractiveCorrectionOS1Sign = true;
                        if (RadioButtonRefractiveCorrectionOS1SignNo.Checked) abstractRecord.RefractiveCorrectionOS1Sign = false;
                        if (RadioButtonManifestRefractionOD1SignYes.Checked) abstractRecord.ManifestRefractionOD1Sign = true;
                        if (RadioButtonManifestRefractionOD1SignNo.Checked) abstractRecord.ManifestRefractionOD1Sign = false;
                        if (RadioButtonManifestRefractionOS1SignYes.Checked) abstractRecord.ManifestRefractionOS1Sign = true;
                        if (RadioButtonManifestRefractionOS1SignNo.Checked) abstractRecord.ManifestRefractionOS1Sign = false;
                        if (RadioButtonCycloplegicRefractionOD1SignYes.Checked) abstractRecord.CycloplegicRefractionOD1Sign = true;
                        if (RadioButtonCycloplegicRefractionOD1SignNo.Checked) abstractRecord.CycloplegicRefractionOD1Sign = false;
                        if (RadioButtonCycloplegicRefractionOS1SignYes.Checked) abstractRecord.CycloplegicRefractionOS1Sign = true;
                        if (RadioButtonCycloplegicRefractionOS1SignNo.Checked) abstractRecord.CycloplegicRefractionOS1Sign = false;
                        if (RadioButtonCornealTopDocYes.Checked) abstractRecord.CornealTopDoc = true;
                        if (RadioButtonCornealTopDocNo.Checked) abstractRecord.CornealTopDoc = false;
                        if (RadioButtonOutcomeManifestRefractionOD1SignYes.Checked) abstractRecord.OutcomeManifestRefractionOD1Sign = true;
                        if (RadioButtonOutcomeManifestRefractionOD1SignNo.Checked) abstractRecord.OutcomeManifestRefractionOD1Sign = false;
                        if (RadioButtonOutcomeManifestRefractionOS1SignYes.Checked) abstractRecord.OutcomeManifestRefractionOS1Sign = true;
                        if (RadioButtonOutcomeManifestRefractionOS1SignNo.Checked) abstractRecord.OutcomeManifestRefractionOS1Sign = false;
                        if (RadioButtonOutcomeBCVADecreaseYes.Checked) abstractRecord.OutcomeBCVADecrease = true;
                        if (RadioButtonOutcomeBCVADecreaseNo.Checked) abstractRecord.OutcomeBCVADecrease = false;
                        try
                        {
                            if (TextBoxIntraopOtherText.Text != null)
                                abstractRecord.IntraopOtherText = TextBoxIntraopOtherText.Text;
                        }
                        catch (Exception ex)
                        {
                            abstractRecord.IntraopOtherText = null;
                            TextBoxIntraopOtherText.Text = "";
                        }


                        try
                        {
                        if (TextBoxHistoryOtherText.Text != null)
                        abstractRecord.HistoryOtherText = TextBoxHistoryOtherText.Text;
                        }
                        catch (Exception ex)
                        {
                        abstractRecord.HistoryOtherText = null;
                        TextBoxHistoryOtherText.Text = "";
                        }
                        try
                        {
                        if (TextBoxRefractiveCorrectionOD1.Text != null)
                        abstractRecord.RefractiveCorrectionOD1 = Convert.ToDouble(TextBoxRefractiveCorrectionOD1.Text);
                        }
                        catch (Exception ex)
                        {
                        abstractRecord.RefractiveCorrectionOD1 = null;
                        TextBoxRefractiveCorrectionOD1.Text = "";
                        }
                        try
                        {
                        if (TextBoxRefractiveCorrectionOD2.Text != null)
                        abstractRecord.RefractiveCorrectionOD2 = Convert.ToDouble(TextBoxRefractiveCorrectionOD2.Text);
                        }
                        catch (Exception ex)
                        {
                        abstractRecord.RefractiveCorrectionOD2 = null;
                        TextBoxRefractiveCorrectionOD2.Text = "";
                        }
                        try
                        {
                        if (TextBoxRefractiveCorrectionOD3.Text != null)
                        abstractRecord.RefractiveCorrectionOD3 = Convert.ToDouble(TextBoxRefractiveCorrectionOD3.Text);
                        }
                        catch (Exception ex)
                        {
                        abstractRecord.RefractiveCorrectionOD3 = null;
                        TextBoxRefractiveCorrectionOD3.Text = "";
                        }
                        try
                        {
                        if (TextBoxRefractiveCorrectionOS1.Text != null)
                        abstractRecord.RefractiveCorrectionOS1 =Convert.ToDouble( TextBoxRefractiveCorrectionOS1.Text);
                        }
                        catch (Exception ex)
                        {
                        abstractRecord.RefractiveCorrectionOS1 = null;
                        TextBoxRefractiveCorrectionOS1.Text = "";
                        }
                        try
                        {
                        if (TextBoxRefractiveCorrectionOS2.Text != null)
                        abstractRecord.RefractiveCorrectionOS2 = Convert.ToDouble(TextBoxRefractiveCorrectionOS2.Text);
                        }
                        catch (Exception ex)
                        {
                        abstractRecord.RefractiveCorrectionOS2 = null;
                        TextBoxRefractiveCorrectionOS2.Text ="";
                        }
                        try
                        {
                        if (TextBoxRefractiveCorrectionOS3.Text != null)
                        abstractRecord.RefractiveCorrectionOS3 = Convert.ToDouble(TextBoxRefractiveCorrectionOS3.Text);
                        }
                        catch (Exception ex)
                        {
                        abstractRecord.RefractiveCorrectionOS3 = null;
                        TextBoxRefractiveCorrectionOS3.Text = "";
                        }
                        try
                        {
                        if (TextBoxManifestRefractionOD1.Text != null)
                        abstractRecord.ManifestRefractionOD1 = Convert.ToDouble(TextBoxManifestRefractionOD1.Text);
                        }
                        catch (Exception ex)
                        {
                        abstractRecord.ManifestRefractionOD1 = null;
                        TextBoxManifestRefractionOD1.Text ="";
                        }
                        try
                        {
                        if (TextBoxManifestRefractionOD2.Text != null)
                        abstractRecord.ManifestRefractionOD2 = Convert.ToDouble(TextBoxManifestRefractionOD2.Text);
                        }
                        catch (Exception ex)
                        {
                        abstractRecord.ManifestRefractionOD2 = null;
                        TextBoxManifestRefractionOD2.Text = "";
                        }
                        try
                        {
                            if (TextBoxManifestRefractionOD3.Text != null)
                                abstractRecord.ManifestRefractionOD3 = Convert.ToDouble(TextBoxManifestRefractionOD3.Text);
                        }
                        catch (Exception ex)
                        {
                        abstractRecord.ManifestRefractionOD3 = null;
                        TextBoxManifestRefractionOD3.Text = "";
                        }
                        try
                        {
                        if (TextBoxManifestRefractionOS1.Text != null)
                        abstractRecord.ManifestRefractionOS1 = Convert.ToDouble(TextBoxManifestRefractionOS1.Text);
                        }
                        catch (Exception ex)
                        {
                        abstractRecord.ManifestRefractionOS1 = null;
                        TextBoxManifestRefractionOS1.Text = "";
                        }
                        try
                        {
                        if (TextBoxManifestRefractionOS2.Text != null)
                        abstractRecord.ManifestRefractionOS2 =Convert.ToDouble( TextBoxManifestRefractionOS2.Text);
                        }
                        catch (Exception ex)
                        {
                        abstractRecord.ManifestRefractionOS2 = null;
                        TextBoxManifestRefractionOS2.Text ="";
                        }
                        try
                        {
                        if (TextBoxManifestRefractionOS3.Text != null)
                        abstractRecord.ManifestRefractionOS3 =Convert.ToDouble(TextBoxManifestRefractionOS3.Text);
                        }
                        catch (Exception ex)
                        {
                        abstractRecord.ManifestRefractionOS3 = null;
                        TextBoxManifestRefractionOS3.Text = "";
                        }
                        try
                        {
                        if (TextBoxCycloplegicRefractionOD1.Text != null)
                        abstractRecord.CycloplegicRefractionOD1 = Convert.ToDouble(TextBoxCycloplegicRefractionOD1.Text);
                        }
                        catch (Exception ex)
                        {
                        abstractRecord.CycloplegicRefractionOD1 = null;
                        TextBoxCycloplegicRefractionOD1.Text = "";
                        }
                        try
                        {
                        if (TextBoxCycloplegicRefractionOD2.Text != null)
                        abstractRecord.CycloplegicRefractionOD2 = Convert.ToDouble(TextBoxCycloplegicRefractionOD2.Text);
                        }
                        catch (Exception ex)
                        {
                        abstractRecord.CycloplegicRefractionOD2 = null;
                        TextBoxCycloplegicRefractionOD2.Text = "";
                        }
                        try
                        {
                        if (TextBoxCycloplegicRefractionOD3.Text != null)
                        abstractRecord.CycloplegicRefractionOD3 =Convert.ToDouble(TextBoxCycloplegicRefractionOD3.Text);
                        }
                        catch (Exception ex)
                        {
                        abstractRecord.CycloplegicRefractionOD3 = null;
                        TextBoxCycloplegicRefractionOD3.Text = "";
                        }
                        try
                        {
                        if (TextBoxCycloplegicRefractionOS1.Text != null)
                        abstractRecord.CycloplegicRefractionOS1 = Convert.ToDouble(TextBoxCycloplegicRefractionOS1.Text);
                        }
                        catch (Exception ex)
                        {
                        abstractRecord.CycloplegicRefractionOS1 = null;
                        TextBoxCycloplegicRefractionOS1.Text = "";
                        }
                        try
                        {
                        if (TextBoxCycloplegicRefractionOS2.Text != null)
                        abstractRecord.CycloplegicRefractionOS2 =Convert.ToDouble(TextBoxCycloplegicRefractionOS2.Text);
                        }
                        catch (Exception ex)
                        {
                        abstractRecord.CycloplegicRefractionOS2 = null;
                        TextBoxCycloplegicRefractionOS2.Text = "";
                        }
                        try
                        {
                        if (TextBoxCycloplegicRefractionOS3.Text != null)
                        abstractRecord.CycloplegicRefractionOS3 =Convert.ToDouble(TextBoxCycloplegicRefractionOS3.Text);
                        }
                        catch (Exception ex)
                        {
                        abstractRecord.CycloplegicRefractionOS3 = null;
                        TextBoxCycloplegicRefractionOS3.Text = "";
                        }
                       
                        
                        
                       
                        try
                        {
                        if (TextBoxIOPOD.Text != null)
                        abstractRecord.IOPOD =Convert.ToDouble(TextBoxIOPOD.Text);
                        }
                        catch (Exception ex)
                        {
                        abstractRecord.IOPOD = null;
                        TextBoxIOPOD.Text ="";
                        }
                        try
                        {
                        if (TextBoxIOPOS.Text != null)
                        abstractRecord.IOPOS =Convert.ToDouble( TextBoxIOPOS.Text);
                        }
                        catch (Exception ex)
                        {
                        abstractRecord.IOPOS = null;
                        TextBoxIOPOS.Text = "";
                        }
                        try
                        {
                        if (TextBoxKeratometryFlatOD.Text != null)
                        abstractRecord.KeratometryFlatOD = Convert.ToDouble(TextBoxKeratometryFlatOD.Text);
                        }
                        catch (Exception ex)
                        {
                        abstractRecord.KeratometryFlatOD = null;
                        TextBoxKeratometryFlatOD.Text = "";
                        }
                        try
                        {
                        if (TextBoxKeratometrySteepOD.Text != null)
                        abstractRecord.KeratometrySteepOD =Convert.ToDouble( TextBoxKeratometrySteepOD.Text);
                        }
                        catch (Exception ex)
                        {
                        abstractRecord.KeratometrySteepOD = null;
                        TextBoxKeratometrySteepOD.Text = "";
                        }
                        try
                        {
                        if (TextBoxKeratometryAxisOD.Text != null)
                        abstractRecord.KeratometryAxisOD =Convert.ToDouble( TextBoxKeratometryAxisOD.Text);
                        }
                        catch (Exception ex)
                        {
                        abstractRecord.KeratometryAxisOD = null;
                        TextBoxKeratometryAxisOD.Text = "";
                        }
                        try
                        {
                        if (TextBoxKeratometryFlatOS.Text != null)
                        abstractRecord.KeratometryFlatOS = Convert.ToDouble(TextBoxKeratometryFlatOS.Text);
                        }
                        catch (Exception ex)
                        {
                        abstractRecord.KeratometryFlatOS = null;
                        TextBoxKeratometryFlatOS.Text ="";
                        }
                        try
                        {
                        if (TextBoxKeratometrySteepOS.Text != null)
                        abstractRecord.KeratometrySteepOS = Convert.ToDouble(TextBoxKeratometrySteepOS.Text);
                        }
                        catch (Exception ex)
                        {
                        abstractRecord.KeratometrySteepOS = null;
                        TextBoxKeratometrySteepOS.Text = "";
                        }
                        try
                        {
                        if (TextBoxKeratometryAxisOS.Text != null)
                        abstractRecord.KeratometryAxisOS =Convert.ToDouble( TextBoxKeratometryAxisOS.Text);
                        }
                        catch (Exception ex)
                        {
                        abstractRecord.KeratometryAxisOS = null;
                        TextBoxKeratometryAxisOS.Text = "";
                        }
                        try
                        {
                        if (TextBoxCornealThicknessOD.Text != null)
                        abstractRecord.CornealThicknessOD =Convert.ToDouble( TextBoxCornealThicknessOD.Text);
                        }
                        catch (Exception ex)
                        {
                        abstractRecord.CornealThicknessOD = null;
                        TextBoxCornealThicknessOD.Text = "";
                        }
                        try
                        {
                        if (TextBoxCornealThicknessOS.Text != null)
                        abstractRecord.CornealThicknessOS = Convert.ToDouble(TextBoxCornealThicknessOS.Text);
                        }
                        catch (Exception ex)
                        {
                        abstractRecord.CornealThicknessOS = null;
                        TextBoxCornealThicknessOS.Text = "";
                        }
                        try
                        {
                        if (TextBoxRBIntendedPostopRefractionOD.Text != null)
                        abstractRecord.RBIntendedPostopRefractionOD = Convert.ToDouble(TextBoxRBIntendedPostopRefractionOD.Text);
                        }
                        catch (Exception ex)
                        {
                        abstractRecord.RBIntendedPostopRefractionOD = null;
                        TextBoxRBIntendedPostopRefractionOD.Text = "";
                        }
                        try
                        {
                        if (TextBoxRBIntendedPostopRefractionOS.Text != null)
                        abstractRecord.RBIntendedPostopRefractionOS = Convert.ToDouble(TextBoxRBIntendedPostopRefractionOS.Text);
                        }
                        catch (Exception ex)
                        {
                        abstractRecord.RBIntendedPostopRefractionOS = null;
                        TextBoxRBIntendedPostopRefractionOS.Text = "";
                        }
                        try
                        {
                        if (TextBoxAdjunctiveOtherText.Text != null)
                        abstractRecord.AdjunctiveOtherText = TextBoxAdjunctiveOtherText.Text;
                        }
                        catch (Exception ex)
                        {
                        abstractRecord.AdjunctiveOtherText = null;
                        TextBoxAdjunctiveOtherText.Text = "";
                        }
                        
                        try
                        {
                        if (TextBoxOutcomeManifestRefractionOD1.Text != null)
                        abstractRecord.OutcomeManifestRefractionOD1 =Convert.ToDouble( TextBoxOutcomeManifestRefractionOD1.Text);
                        }
                        catch (Exception ex)
                        {
                        abstractRecord.OutcomeManifestRefractionOD1 = null;
                        TextBoxOutcomeManifestRefractionOD1.Text = "";
                        }
                        try
                        {
                        if (TextBoxOutcomeManifestRefractionOD2.Text != null)
                        abstractRecord.OutcomeManifestRefractionOD2 =Convert.ToDouble( TextBoxOutcomeManifestRefractionOD2.Text);
                        }
                        catch (Exception ex)
                        {
                        abstractRecord.OutcomeManifestRefractionOD2 = null;
                        TextBoxOutcomeManifestRefractionOD2.Text = "";
                        }
                        try
                        {
                        if (TextBoxOutcomeManifestRefractionOD3.Text != null)
                        abstractRecord.OutcomeManifestRefractionOD3 =Convert.ToDouble( TextBoxOutcomeManifestRefractionOD3.Text);
                        }
                        catch (Exception ex)
                        {
                        abstractRecord.OutcomeManifestRefractionOD3 = null;
                        TextBoxOutcomeManifestRefractionOD3.Text = "";
                        }
                        try
                        {
                        if (TextBoxOutcomeManifestRefractionOS1.Text != null)
                        abstractRecord.OutcomeManifestRefractionOS1 =Convert.ToDouble( TextBoxOutcomeManifestRefractionOS1.Text);
                        }
                        catch (Exception ex)
                        {
                        abstractRecord.OutcomeManifestRefractionOS1 = null;
                        TextBoxOutcomeManifestRefractionOS1.Text = "";
                        }
                        try
                        {
                        if (TextBoxOutcomeManifestRefractionOS2.Text != null)
                        abstractRecord.OutcomeManifestRefractionOS2 = Convert.ToDouble(TextBoxOutcomeManifestRefractionOS2.Text);
                        }
                        catch (Exception ex)
                        {
                        abstractRecord.OutcomeManifestRefractionOS2 = null;
                        TextBoxOutcomeManifestRefractionOS2.Text = "";
                        }
                        try
                        {
                        if (TextBoxOutcomeManifestRefractionOS3.Text != null)
                        abstractRecord.OutcomeManifestRefractionOS3 =Convert.ToDouble( TextBoxOutcomeManifestRefractionOS3.Text);
                        }
                        catch (Exception ex)
                        {
                        abstractRecord.OutcomeManifestRefractionOS3 = null;
                        TextBoxOutcomeManifestRefractionOS3.Text = "";
                        }
                        try
                        {
                        if (TextBoxOutcomeVAOtherText.Text != null)
                        abstractRecord.OutcomeVAOtherText = TextBoxOutcomeVAOtherText.Text;
                        }
                        catch (Exception ex)
                        {
                        abstractRecord.OutcomeVAOtherText = null;
                        TextBoxOutcomeVAOtherText.Text = "";
                        }
                        try
                        {
                        if (TextBoxOutcomeComplicationOtherText.Text != null)
                        abstractRecord.OutcomeComplicationOtherText = TextBoxOutcomeComplicationOtherText.Text;
                        }
                        catch (Exception ex)
                        {
                        abstractRecord.OutcomeComplicationOtherText = null;
                        TextBoxOutcomeComplicationOtherText.Text = "";
                        }
                        abstractRecord.LastUpdateDate = DateTime.Now;
                        bool ChartCompleted = isComplete();
                        abstractRecord.Complete = ChartCompleted;
                        var chartRegistration = (from cr in pim.ChartRegistration
                                                 where cr.ParticipantModuleCycleID == cycle.ParticipantModuleCycleID
                                                  & cr.ModuleID == ModuleID
                                                  & cr.RecordIdentifier == recordIdentifier
                                                 select cr).First<NetHealthPIMModel.ChartRegistration>();
                        chartRegistration.AbstractCompleted = ChartCompleted;

                        if ((DropDownListYearOfBirth.SelectedIndex > 0) && (DropDownListYearOfBirth.SelectedIndex > 0))
                        {
                            chartRegistration.DOB = abstractRecord.MonthOfBirth + "/" + abstractRecord.YearOfBirth;
                        }
                      
                        pim.SaveChanges();

                        transaction.Complete();




            }

            CycleManager.UpdateParticipantCompletedModules(cycleID, ModuleID);
        }


    }
    protected void ButtonSubmit_Click(object sender, EventArgs e)
    {
        savedata();
        isComplete();
        if (!isComplete())
        {
            isComplete();
            string script = " var answer = confirm('Chart data is not complete.\\nClick OK to save entries and exit chart.\\nClick CANCEL to save entries and review the chart. '); if (answer==true) window.location = '../PatientChartRegistration.aspx';";
            ScriptManager.RegisterStartupScript(this, GetType(),
                          "ServerControlScript", script, true);
        }
        else
        {

            Response.Redirect("../PatientChartRegistration.aspx");
        }
    }


    protected bool isComplete()
    {

        bool ChartCompleted = true;
        LabelRBGender.Visible = false;

        LabelRBHistoryEyeSurgery.Visible = false;

        LabelRBRequiredAdd.Visible = false;

        LabelRBOcularDominance.Visible = false;

        LabelRBEyelidFunction.Visible = false;

        LabelRBGoalsExplained.Visible = false;

        LabelRBOutOfLenses.Visible = false;

        LabelRBEyelidsScrubbed.Visible = false;

        LabelRBSterileSurgicalGloves.Visible = false;

        LabelRBSurgicalIntervention.Visible = false;

        LabelRBIntraopComplications.Visible = false;

        LabelRBPostop48Hours.Visible = false;

        LabelRBPostopAntibiotic.Visible = false;

        LabelRBPostopAntiInflam.Visible = false;

        LabelRBOutcomeTargetOD.Visible = false;

        LabelRBOutcomeTargetOS.Visible = false;

        LabelRBOutcomeEnhancementSurgery.Visible = false;

        LabelMonthOfBirth.Visible = false;

        LabelYearOfBirth.Visible = false;

        LabelMonthOfPreopExam.Visible = false;

        LabelYearOfPreopExam.Visible = false;
  
        LabelRBIntendedPostopRefractionOD.Visible = false;

        LabelRBIntendedPostopRefractionOS.Visible = false;

        LabelVisualRequirementsDoc.Visible = false;
        LabelHistoryDryEye.Visible = false;

        LabelHistoryKeratoconus.Visible = false;
        LabelMonovisionSimulation.Visible = false;
        LabelHistoryInfection.Visible = false;

        LabelHistoryGlaucoma.Visible = false;
        LabelAssessmentEye.Visible = false; 
        LabelHistoryStrabismus.Visible = false;
        LabelNearVAManifestRefractionNA.Visible = false;
        LabelHistoryOther.Visible = false;
        LabelDistVAManifestRefractionNA.Visible = false;
        LabelCornealTopDoc.Visible = false;

        LabelOutcomeBCVADecrease.Visible = false;

        Labelinitialage.Visible = false;

        NetHealthPIMEntities pim = new NetHealthPIMEntities();
        var moduleinfo = (from c in pim.Module where c.ModuleID == 29 select c).FirstOrDefault();
        string message = "Patient must be  at least " + moduleinfo.RegistrationMinAge + " years old<br />";
        string registrationminage = Validation.validateRegistrationMinAge(Convert.ToInt32(DropDownListYearOfPreopExam.SelectedValue), Convert.ToInt32(DropDownListYearOfBirth.SelectedValue), Convert.ToInt32(moduleinfo.RegistrationMinAge), message);

        if (registrationminage != "")
        {
            Labelinitialage.Text = message;
            Labelinitialage.Visible = true;
            ChartCompleted = false;
        }


        if (DropDownListDistVAManifestRefractionOD.SelectedIndex==0 && DropDownListDistVAManifestRefractionOS.SelectedIndex==0 && CheckBoxDistVAManifestRefractionNA.Checked==false)

        {
            LabelDistVAManifestRefractionNA.Visible = true;
            ChartCompleted = false;
        }
        if (DropDownListNearVAManifestRefractionOD.SelectedIndex == 0 && DropDownListNearVAManifestRefractionOS.SelectedIndex == 0 && CheckBoxNearVAManifestRefractionNA.Checked == false)
        {
            LabelNearVAManifestRefractionNA.Visible = true;
            ChartCompleted = false;
        }
        if (RadioButtonListRBGender.SelectedIndex == -1)
        {
            LabelRBGender.Visible = true;
            ChartCompleted = false;
        }
        if (RadioButtonListAssessmentEye.SelectedIndex == -1)
        {
            LabelAssessmentEye.Visible = true;
            ChartCompleted = false;
        }

        if (RadioButtonListRBHistoryEyeSurgery.SelectedIndex == -1)
        {
            LabelRBHistoryEyeSurgery.Visible = true;
            ChartCompleted = false;
        }
        if (RadioButtonListRBRequiredAdd.SelectedIndex == -1)
        {
            LabelRBRequiredAdd.Visible = true;
            ChartCompleted = false;
        }
        if (RadioButtonListRBOcularDominance.SelectedIndex == -1)
        {
            LabelRBOcularDominance.Visible = true;
            ChartCompleted = false;
        }
        if (RadioButtonListRBEyelidFunction.SelectedIndex == -1)
        {
            LabelRBEyelidFunction.Visible = true;
            ChartCompleted = false;
        }
        if (RadioButtonListRBGoalsExplained.SelectedIndex == -1)
        {
            LabelRBGoalsExplained.Visible = true;
            ChartCompleted = false;
        }
        if (RadioButtonListRBOutOfLenses.SelectedIndex == -1)
        {
            LabelRBOutOfLenses.Visible = true;
            ChartCompleted = false;
        }
        if (RadioButtonListRBEyelidsScrubbed.SelectedIndex == -1)
        {
            LabelRBEyelidsScrubbed.Visible = true;
            ChartCompleted = false;
        }
        if (RadioButtonListRBSterileSurgicalGloves.SelectedIndex == -1)
        {
            LabelRBSterileSurgicalGloves.Visible = true;
            ChartCompleted = false;
        }
        if (RadioButtonListRBSurgicalIntervention.SelectedIndex == -1)
        {
            LabelRBSurgicalIntervention.Visible = true;
            ChartCompleted = false;
        }
        if (RadioButtonListRBIntraopComplications.SelectedIndex == -1)
        {
            LabelRBIntraopComplications.Visible = true;
            ChartCompleted = false;
        }
        if (RadioButtonListRBPostop48Hours.SelectedIndex == -1)
        {
            LabelRBPostop48Hours.Visible = true;
            ChartCompleted = false;
        }
        if (RadioButtonListRBPostopAntibiotic.SelectedIndex == -1)
        {
            LabelRBPostopAntibiotic.Visible = true;
            ChartCompleted = false;
        }
        if (RadioButtonListRBPostopAntiInflam.SelectedIndex == -1)
        {
            LabelRBPostopAntiInflam.Visible = true;
            ChartCompleted = false;
        }
        if (RadioButtonListRBOutcomeTargetOD.SelectedIndex == -1)
        {
            LabelRBOutcomeTargetOD.Visible = true;
            ChartCompleted = false;
        }
        if (RadioButtonListRBOutcomeTargetOS.SelectedIndex == -1)
        {
            LabelRBOutcomeTargetOS.Visible = true;
            ChartCompleted = false;
        }
        if (RadioButtonListRBOutcomeEnhancementSurgery.SelectedIndex == -1)
        {
            LabelRBOutcomeEnhancementSurgery.Visible = true;
            ChartCompleted = false;
        }
   


        if (DropDownListMonthOfBirth.SelectedIndex == 0)
        {
            LabelMonthOfBirth.Visible = true;
            ChartCompleted = false;
        }
        if (DropDownListYearOfBirth.SelectedIndex == 0)
        {
            LabelYearOfBirth.Visible = true;
            ChartCompleted = false;
        }
        if (DropDownListMonthOfPreopExam.SelectedIndex == 0)
        {
            LabelMonthOfPreopExam.Visible = true;
            ChartCompleted = false;
        }
        if (DropDownListYearOfPreopExam.SelectedIndex == 0)
        {
            LabelYearOfPreopExam.Visible = true;
            ChartCompleted = false;
        }
        if (string.IsNullOrEmpty(TextBoxRBIntendedPostopRefractionOD.Text))
        {
            LabelRBIntendedPostopRefractionOD.Visible = true;
            ChartCompleted = false;
        }
        if (string.IsNullOrEmpty(TextBoxRBIntendedPostopRefractionOS.Text))
        {
            LabelRBIntendedPostopRefractionOS.Visible = true;
            ChartCompleted = false;
        }
        if (RadioButtonMonovisionSimulationYes.Checked == false && RadioButtonMonovisionSimulationNo.Checked == false)
        {

            LabelMonovisionSimulation.Visible = true;
            ChartCompleted = false;
        }


        if (RadioButtonVisualRequirementsDocNo.Checked == false && RadioButtonVisualRequirementsDocYes.Checked == false)
        {
            LabelVisualRequirementsDoc.Visible = true;
            ChartCompleted = false;
        }
        if (RadioButtonHistoryDryEyeNo.Checked == false && RadioButtonHistoryDryEyeYes.Checked == false)
        {
            LabelHistoryDryEye.Visible = true;
            ChartCompleted = false;
        }
        if (RadioButtonHistoryKeratoconusNo.Checked == false && RadioButtonHistoryKeratoconusYes.Checked == false)
        {
            LabelHistoryKeratoconus.Visible = true;
            ChartCompleted = false;
        }
        if (RadioButtonHistoryInfectionNo.Checked == false && RadioButtonHistoryInfectionYes.Checked == false)
        {
            LabelHistoryInfection.Visible = true;
            ChartCompleted = false;
        }
        if (RadioButtonHistoryGlaucomaNo.Checked == false && RadioButtonHistoryGlaucomaYes.Checked == false)
        {
            LabelHistoryGlaucoma.Visible = true;
            ChartCompleted = false;
        }
        if (RadioButtonHistoryStrabismusNo.Checked == false && RadioButtonHistoryStrabismusYes.Checked == false)
        {
            LabelHistoryStrabismus.Visible = true;
            ChartCompleted = false;
        }
        if (RadioButtonHistoryOtherNo.Checked == false && RadioButtonHistoryOtherYes.Checked == false)
        {
            LabelHistoryOther.Visible = true;
            ChartCompleted = false;
        }
        if (RadioButtonCornealTopDocNo.Checked == false && RadioButtonCornealTopDocYes.Checked == false)
        {
            LabelCornealTopDoc.Visible = true;
            ChartCompleted = false;
        }
        if (RadioButtonOutcomeBCVADecreaseNo.Checked == false && RadioButtonOutcomeBCVADecreaseYes.Checked == false)
        {
            LabelOutcomeBCVADecrease.Visible = true;
            ChartCompleted = false;
        }



        return ChartCompleted;

    }      
            
            
            
            
            
            
            
}

