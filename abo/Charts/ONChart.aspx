﻿<%@ Page Title="" Language="C#" MasterPageFile="~/PIMMasterPage.master" AutoEventWireup="true" CodeFile="ONChart.aspx.cs" Inherits="abo_IONChart" %>

<asp:Content ID="Content1" ContentPlaceHolderID="head" Runat="Server">

    <link type="text/css" href="../../common/css/atooltip.css" rel="stylesheet"  media="screen" />

    <style type="text/css">
        .bginputa{
	        float:right;
	        margin:21px 0 0 1px;
	        background: url(../../common/images/orange_button_with_arrow.png) no-repeat;
	        overflow:hidden;
	        height:35px;

        }
        .bginputa a{
	        color: white;
	        text-align:center;
	        height:35px;
	        line-height:28px;
	        padding:0 14px 15px  ;
	        cursor:pointer;
	        float:left;
	        border:none;
	        text-decoration:none;
	        font-family:Arial, Helvetica, sans-serif; 
	        font-size:12px; 
	        font-weight:bold; 
	        letter-spacing: 0px;
        }
        .bginputa a:hover{
	        text-decoration:none;
        }
        .right {
            float:right;
            text-align:right;
        }
    </style>
    
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder1" Runat="Server">
    <asp:HiddenField ID="HiddenFieldRecordIdentifier" runat="server"/>
        <!-- Esotropia pim -->
        <div class="pim">
            <div class="record-ident clearfix">
                <h3 class="record-first">RECORD IDENTIFIER: <asp:Literal runat="server" ID="LiteralRecordIdentifier"></asp:Literal></h3>
                <h3 class="record-second"><asp:Literal runat="server" ID="LiteralAbstractionNumber"></asp:Literal></h3>
            </div>
            <!-- History Section -->
            <table>
                <tr>
                    <th colspan="2" style="width: 910px;">
                        <p>History</p>
                    </th>
                </tr>
                <!-- Question 1 -->
                <tr class="table_body">
                    <td width="70%">
                        <strong>1. Date of birth:</strong>
                        <asp:Label ID="LabelMonthOfBirth" runat="server" Visible="false"  ForeColor="Red" Text="<br />Please enter Month"></asp:Label>
                        <asp:Label ID="LabelYearOfBirth" runat="server" Visible="false"  ForeColor="Red"  Text="<br />Please enter Year"></asp:Label>
                    </td>
                    <td>
                        <asp:DropDownList ID="DropDownListMonthOfBirth" DataValueField="MonthID" DataTextField="MonthName" runat="server" onchange="DOBValidation();" />
                        <asp:DropDownList ID="DropDownListYearOfBirth" DataValueField="YearID" DataTextField="YearName" runat="server" onchange="DOBValidation();" />
                    </td>
                </tr>
                <!-- Question 2 -->
                <tr class="table_body_bg">
                    <td>
                        <strong>2. Date of first exam:</strong>
                        <asp:Label ID="LabelMonthOfFirstExam" runat="server" Visible="false"  ForeColor="Red" Text="<br />Please enter Month  "></asp:Label>
                        <asp:Label ID="LabelYearOfFirstExam" runat="server" Visible="false"  ForeColor="Red"  Text="<br />Please enter Year "></asp:Label>
                        <asp:Label ID="Labelinitialage" runat="server" Visible="false"  ForeColor="Red"  Text="<br />"></asp:Label>
                    </td>
                    <td>
                        <asp:DropDownList ID="DropDownListMonthOfFirstExam" DataValueField="MonthID" DataTextField="MonthName" runat="server" onchange="firstExamValidation();" />
                        <asp:DropDownList ID="DropDownListYearOfFirstExam" DataValueField="YearID" DataTextField="YearName" runat="server" onchange="firstExamValidation();" />
                    </td>
                </tr>
                <!-- Question 3 -->
                <tr class="table_body">
                    <td>
                        <strong>3. Gender</strong>
                        <asp:Label ID="LabelRBGenderTypeID" runat="server" Visible="false"  ForeColor="Red" Text="<br />Please complete"></asp:Label>
                    </td>
                    <td>
                        <asp:RadioButtonList id="RadioButtonListRBGenderTypeID" CssClass="aspxList" runat="server">
	                        <asp:ListItem Value="42">&nbsp;Male</asp:ListItem>
	                        <asp:ListItem Value="43">&nbsp;Female</asp:ListItem>
                        </asp:RadioButtonList>
                    </td>
                </tr>
                <!-- Question 4 -->
                <tr class="table_body_bg">
                    <td>
                        <strong>4. What was the patient’s visual complaint?</strong>
                        <asp:Label ID="LabelHistoryVisualComplaint" runat="server" Visible="false"  ForeColor="Red" Text="<br />Please complete"></asp:Label>
                    </td>
                    <td>
                        <asp:CheckBox runat="server" ID="CheckBoxHistoryVisualComplaintBlurredVision" text="&nbsp;Blurred Vision" class="check"/>
                        <br /><asp:CheckBox runat="server" ID="CheckBoxHistoryVisualComplaintColorVision" text="&nbsp;Color Vision Loss" class="check"/>
                        <br /><asp:CheckBox runat="server" ID="CheckBoxHistoryVisualComplaintDimming" text="&nbsp;Dimming" class="check"/>
                        <br /><asp:CheckBox runat="server" ID="CheckBoxHistoryVisualComplaintVisualFieldDefect" text="&nbsp;Visual Field Defect" class="check"/>
                        <br /><asp:CheckBox runat="server" ID="CheckBoxHistoryVisualComplaintNotRecorded" text="&nbsp;Not Recorded" class="check"/>
                        <br /><asp:CheckBox runat="server" ID="CheckBoxHistoryVisualComplaintOther" text="&nbsp;Other" class="check"/>
                    </td>
                </tr>
                <!-- Question 5 -->
                <tr class="table_body">
                    <td>
                        <strong>5. Was pain with eye movement reported?</strong>
                        <asp:Label ID="LabelRBHistoryPainEyeMovement" runat="server" Visible="false"  ForeColor="Red" Text="<br />Please complete"></asp:Label>
                    </td>
                    <td>
                        <asp:RadioButtonList id="RadioButtonListRBHistoryPainEyeMovement" RepeatDirection="Horizontal" CssClass="aspxList" runat="server">
	                        <asp:ListItem Value="1">&nbsp;Yes</asp:ListItem>
	                        <asp:ListItem Value="2">&nbsp;No</asp:ListItem>
	                        <asp:ListItem Value="3">&nbsp;Not Recorded</asp:ListItem>
                        </asp:RadioButtonList>
                    </td>
                </tr>
                <!-- Question 6 -->
                <tr class="table_body_bg">
                    <td>
                        <strong>6. Were prior or concurrent neurologic symptoms reported?</strong>
                        <asp:Label ID="LabelRBSymptomsGeneralNeurologic" runat="server" Visible="false"  ForeColor="Red" Text="<br />Please complete"></asp:Label>
                    </td>
                    <td>
                        <asp:RadioButtonList id="RadioButtonListRBSymptomsGeneralNeurologic" RepeatDirection="Horizontal" CssClass="aspxList" runat="server">
                            <asp:ListItem Value="1">&nbsp;Yes</asp:ListItem>
                            <asp:ListItem Value="2">&nbsp;No</asp:ListItem>
                            <asp:ListItem Value="3">&nbsp;Not Recorded</asp:ListItem>
                        </asp:RadioButtonList>
                    </td>
                </tr>
                <!-- Question 6a -->
                <tr class="table_body_bg">
                    <td>
                        <span id="QH7txta"><strong>6a. If so, which were identified?</strong></span>
                        <asp:Label ID="LabelSymptoms" runat="server" Visible="false"  ForeColor="Red" Text="<br />Please complete"></asp:Label>
                    </td>
                    <td>
                        <span id="QH7txtb">
                        <asp:CheckBox ID="CheckBoxSymptomsAtaxia" runat="server" /><label>Ataxia</label>
                        <br /><asp:CheckBox ID="CheckBoxSymptomsDiplopia" runat="server" /><label>Diplopia</label>
                        <br /><asp:CheckBox ID="CheckBoxSymptomsParesthesia" runat="server" /><label>Paresthesia</label>
                        <br /><asp:CheckBox ID="CheckBoxSymptomsWeakness" runat="server" /><label>Weakness</label>
                        <br /><asp:CheckBox ID="CheckBoxSymptomsOther" runat="server" /><label>Other</label>
                        </span>
                    </td>
                </tr>
                <!-- Question 7 -->
                <tr class="table_body">
                    <td>
                        <strong>7. Was a history of autoimmune disease reported?</strong>
                        <asp:Label ID="LabelRBHistoryAutoimmuneDisease" runat="server" Visible="false"  ForeColor="Red" Text="<br />Please complete"></asp:Label>
                    </td>
                    <td>
                        <asp:RadioButtonList id="RadioButtonListRBHistoryAutoimmuneDisease" RepeatDirection="Horizontal" CssClass="aspxList" runat="server">
                            <asp:ListItem Value="1">&nbsp;Yes</asp:ListItem>
                            <asp:ListItem Value="2">&nbsp;No</asp:ListItem>
                            <asp:ListItem Value="3">&nbsp;Not&nbsp;Recorded</asp:ListItem>
                        </asp:RadioButtonList>
                    </td>
                </tr>
                <!-- Question 7a -->
                <tr class="table_body">
                    <td>
                        <span id="QH9txta"><strong>7a. If so, which were identified?</strong></span>
                        <asp:Label ID="LabelHistoryAI" runat="server" Visible="false"  ForeColor="Red" Text="<br />Please complete"></asp:Label>
                    </td>
                    <td>
                        <span id="QH9txtb">
                        <asp:CheckBox runat="server" ID="CheckBoxHistoryAISystemiclupus" class="check" text="&nbsp;Systemic lupus erythematosus" />
                        <br /><asp:CheckBox runat="server" ID="CheckBoxHistoryAISarcoidosis" class="check" text="&nbsp;Sarcoidosis" />
                        <br /><asp:CheckBox runat="server" ID="CheckBoxHistoryAIOther" class="check" text="&nbsp;Other" />
                        </span>
                    </td>
                </tr>
                <!-- Question 8 -->
                <tr class="table_body">
                    <td>
                        <strong>8. Was a history suggesting systemic infection reported?</strong>
                        <asp:Label ID="LabelRBHistorySystemic" runat="server" Visible="false"  ForeColor="Red" Text="<br />Please complete"></asp:Label>
                    </td>
                    <td>
                        <asp:RadioButtonList id="RadioButtonListRBHistorySystemic" RepeatDirection="Horizontal" CssClass="aspxList" runat="server">
                            <asp:ListItem Value="1">&nbsp;Yes</asp:ListItem>
                            <asp:ListItem Value="2">&nbsp;No</asp:ListItem>
                            <asp:ListItem Value="3">&nbsp;Not&nbsp;Recorded</asp:ListItem>
                        </asp:RadioButtonList>
                    </td>
                </tr>
                <!-- Question 8a -->
                <tr class="table_body">
                    <td id="QH8aA">
                        <strong>8a. If yes, which were identified?</strong>
                        <asp:Label ID="Label" runat="server" Visible="false"  ForeColor="Red" Text="<br />Please complete"></asp:Label>
                    </td>
                    <td id="QH8aB">
                        <asp:CheckBox runat="server" ID="CheckBoxHistorySIBartonella" class="check" text="&nbsp;Bartonella" />
                        <br /><asp:CheckBox runat="server" ID="CheckBoxHistorySISyphillis" class="check" text="&nbsp;Syphilis" />
                        <br /><asp:CheckBox runat="server" ID="CheckBoxHistorySIViral" class="check" text="&nbsp;Viral" />
                        <br /><asp:CheckBox runat="server" ID="CheckBoxHistorySIOther" class="check" text="&nbsp;Other" />
                    </td>
                </tr>
            </table>
            <br />
            <!-- Examination (Diagnostic Testing) Section -->
            <table width="910px">
                <!-- Header - Examination (Diagnostic Testing) -->
                <tr>
                    <th colspan="3" style="width: 910px;"><p>Examination (Diagnostic Testing)</p></th>
                </tr>
                <!-- Question 1 -->
                <tr class="table_body">
                    <td rowspan="2" width="50%">
                        <strong>1. Best corrected visual acuity</strong>&nbsp;<a href="#"><img id="QE1" src="../../common/images/tip.gif" alt="Tip" /></a>
                        <asp:Label ID="LabelExamBestCorrectedVisualOD" runat="server" Visible="false"  ForeColor="Red" Text="<br />Please complete OD"></asp:Label>
                        <asp:Label ID="LabelExamBestCorrectedVisualOS" runat="server" Visible="false"  ForeColor="Red" Text="<br />Please complete OS"></asp:Label>
                    </td>
                    <td width="20%"><span class="right">OD</span></td>
                    <td>
                        20 / <asp:DropDownList ID="DropDownExamBestCorrectedVisualOD" DataValueField="examValue" DataTextField="examLabel" runat="server" />
                    </td>
                </tr>
                <tr class="table_body">
                    <td><span class="right">OS</span></td>
                    <td>
                        20 / <asp:DropDownList ID="DropDownExamBestCorrectedVisualOS" DataValueField="examValue" DataTextField="examLabel" runat="server" />
                    </td>
                </tr>
                <!-- Question 2 -->
                <tr class="table_body_bg">
                    <td colspan="2">
                        <strong>2. Was color vision abnormal?</strong>
                        <asp:Label ID="LabelRBExamAbnormalityColorVision" runat="server" Visible="false"  ForeColor="Red" Text="<br />Please complete"></asp:Label>
                    </td>
                    <td>
                        <asp:RadioButtonList id="RadioButtonListRBExamAbnormalityColorVision" RepeatDirection="Horizontal" CssClass="aspxList" runat="server">
	                        <asp:ListItem Value="1">&nbsp;Yes</asp:ListItem>
	                        <asp:ListItem Value="2">&nbsp;No</asp:ListItem>
	                        <asp:ListItem Value="3">&nbsp;Not Recorded</asp:ListItem>
                        </asp:RadioButtonList>
                    </td>
                </tr>
                <!-- Question 3 -->
                <tr class="table_body">
                    <td colspan="2">
                        <strong>3. Was a relative afferent pupillary defect noted?</strong>
                        <asp:Label ID="LabelRBExamRelativeAfferentPupillaryDefect" runat="server" Visible="false"  ForeColor="Red" Text="<br />Please complete"></asp:Label>
                    </td>
                    <td>
                        <asp:RadioButtonList id="RadioButtonListRBExamRelativeAfferentPupillaryDefect" RepeatDirection="Horizontal" CssClass="aspxList" runat="server">
	                        <asp:ListItem Value="1">&nbsp;Yes</asp:ListItem>
	                        <asp:ListItem Value="2">&nbsp;No</asp:ListItem>
	                        <asp:ListItem Value="3">&nbsp;Not Recorded</asp:ListItem>
                        </asp:RadioButtonList>
                    </td>
                </tr>
                <!-- Question 4 -->
                <tr class="table_body_bg">
                    <td colspan="2">
                        <strong>4. Was ocular motility assessed?</strong>
                        <asp:Label ID="LabelRBExamOcularMotility" runat="server" Visible="false"  ForeColor="Red" Text="<br />Please complete"></asp:Label>
                    </td>
                    <td>
                        <asp:RadioButtonList id="RadioButtonListRBExamOcularMotility" RepeatDirection="Horizontal" CssClass="aspxList" runat="server">
	                        <asp:ListItem Value="1">&nbsp;Yes</asp:ListItem>
	                        <asp:ListItem Value="2">&nbsp;No</asp:ListItem>
	                        <asp:ListItem Value="3">&nbsp;Not Recorded</asp:ListItem>
                        </asp:RadioButtonList>
                    </td>
                </tr>
                <!-- Question 4a -->
                <tr class="table_body_bg">
                    <td colspan="2">
                        <span id="QE5txta"><strong>4a. If an abnormality was detected, specify type</strong></span>
                        <asp:Label ID="LabelRBExamOcularMotilityType" runat="server" Visible="false"  ForeColor="Red" Text="<br />Please complete"></asp:Label>
                    </td>
                    <td>
                        <span id="QE5txtb">
                        <asp:RadioButtonList id="RadioButtonListRBExamOcularMotilityType" RepeatDirection="Vertical" CssClass="aspxList" runat="server">
	                        <asp:ListItem Value="27">&nbsp;Nystagmus</asp:ListItem>
	                        <asp:ListItem Value="28">&nbsp;Internuclear Ophthalmoplegia</asp:ListItem>
	                        <asp:ListItem Value="29">&nbsp;Other</asp:ListItem>
                        </asp:RadioButtonList>
                        </span>
                    </td>
                </tr>
                <!-- Question 5 -->
                <tr class="table_body">
                    <td colspan="2">
                        <strong>5. Was uveitis noted?</strong>
                        <asp:Label ID="LabelRBExamUveitis" runat="server" Visible="false"  ForeColor="Red" Text="<br />Please complete"></asp:Label>
                    </td>
                    <td>
                        <asp:RadioButtonList id="RadioButtonListRBExamUveitis" RepeatDirection="Horizontal" CssClass="aspxList" runat="server">
	                        <asp:ListItem Value="1">&nbsp;Yes</asp:ListItem>
	                        <asp:ListItem Value="2">&nbsp;No</asp:ListItem>
	                        <asp:ListItem Value="3">&nbsp;Not Recorded</asp:ListItem>
                        </asp:RadioButtonList>
                    </td>
                </tr>
                <!-- Question 6 -->
                <tr class="table_body_bg">
                    <td colspan="2">
                        <strong>6. Was the optic disc described?</strong>
                        <asp:Label ID="LabelRBExamOpticDisc" runat="server" Visible="false"  ForeColor="Red" Text="<br />Please complete"></asp:Label>
                    </td>
                    <td>
                        <asp:RadioButtonList id="RadioButtonListRBExamOpticDisc" RepeatDirection="Horizontal" CssClass="aspxList" runat="server">
	                        <asp:ListItem Value="1">&nbsp;Yes</asp:ListItem>
	                        <asp:ListItem Value="2">&nbsp;No</asp:ListItem>
	                        <asp:ListItem Value="3">&nbsp;Not Recorded</asp:ListItem>
                        </asp:RadioButtonList>
                    </td>
                </tr>
                <!-- Question 6a -->
                <tr class="table_body_bg">
                    <td colspan="2">
                        <span id="QE8txta"><strong>6a. If so, what was the appearance?</strong></span>
                        <asp:Label ID="LabelRBExamOpticDiscAppearance" runat="server" Visible="false"  ForeColor="Red" Text="<br />Please complete"></asp:Label>
                    </td>
                    <td>
                        <span id="QE8txtb">
                        <asp:RadioButtonList id="RadioButtonListRBExamOpticDiscAppearance" RepeatDirection="Horizontal" CssClass="aspxList" runat="server">
	                        <asp:ListItem Value="24">&nbsp;Normal</asp:ListItem>
	                        <asp:ListItem Value="25">&nbsp;Edematous</asp:ListItem>
	                        <asp:ListItem Value="26">&nbsp;Pale</asp:ListItem>
                        </asp:RadioButtonList>
                        </span>
                    </td>
                </tr>
                <!-- Question 7 -->
                <tr class="table_body">
                    <td colspan="2">
                        <strong>7. Was baseline perimetry performed?</strong>
                        <asp:Label ID="LabelExamBaselinePerimetry" runat="server" Visible="false"  ForeColor="Red" Text="<br />Please complete"></asp:Label>
                    </td>
                    <td>
                        <asp:RadioButton ID="RadioButtonExamBaselinePerimetryYes" runat="server" GroupName="ExamBaselinePerimetry" Text="&nbsp;Yes" onclick="activateQE10();" />
                        <asp:RadioButton ID="RadioButtonExamBaselinePerimetryNo" runat="server" GroupName="ExamBaselinePerimetry" Text="&nbsp;No" onclick="deActivateQE10();" />
                    </td>
                </tr>
                <!-- Question 7a -->
                <tr class="table_body">
                    <td colspan="2">
                        <span id="QE10txta"><strong>7a. If so, what defect was present?</strong></span>
                        <asp:Label ID="LabelRBExamBaselinePerimetryDefect" runat="server" Visible="false"  ForeColor="Red" Text="<br />Please complete"></asp:Label>
                    </td>
                    <td>
                        <span id="QE10txtb">
                        <asp:RadioButtonList id="RadioButtonListRBExamBaselinePerimetryDefect" RepeatDirection="Vertical" CssClass="aspxList" runat="server">
	                        <asp:ListItem Value="30">&nbsp;Arculate Scotoma</asp:ListItem>
	                        <asp:ListItem Value="31">&nbsp;Altitudinal Defect</asp:ListItem>
	                        <asp:ListItem Value="32">&nbsp;Central/Cecocentral Scotoma</asp:ListItem>
                            <asp:ListItem Value="33">&nbsp;Generalized Depression</asp:ListItem>
                            <asp:ListItem Value="34">&nbsp;Other</asp:ListItem>
                        </asp:RadioButtonList>
                        </span>
                    </td>
                </tr>
                <!-- Question 8 -->
                <tr class="table_body_bg">
                    <td colspan="2">
                        <strong>8. If automated static perimetry performed, mean defect:</strong>
                        <asp:Label ID="LabelExamMeanDefect" runat="server" Visible="false"  ForeColor="Red" Text="<br />Please complete"></asp:Label></td>
                    <td>
                        <asp:TextBox runat="server" ID="TextBoxExamMeanDefectDB" size="3" onkeyup="QE11toggleCB();" meta="{vMin: '-99999', vMax: '99999'}" CssClass="auto" />&nbsp;dB
                    </td>
                </tr>
            </table>
            <br />
            <!-- Management -->
            <table width="910px">
                <!-- Header - Management -->
                <tr>
                    <th colspan="2"><p>Management</p></th>
                </tr>
                <!-- Question 1 -->
                <tr class="table_body">
                    <td width="70%">
                        <strong>1. Was the relationship of optic neuritis to multiple sclerosis discussed?</strong>
                        <asp:Label ID="LabelRBManageRelationship" runat="server" Visible="false"  ForeColor="Red" Text="<br />Please complete"></asp:Label>
                    </td>
                    <td>
                        <asp:RadioButtonList id="RadioButtonListRBManageRelationship" RepeatDirection="Horizontal" CssClass="aspxList" runat="server">
	                        <asp:ListItem Value="1">&nbsp;Yes</asp:ListItem>
	                        <asp:ListItem Value="2">&nbsp;No</asp:ListItem>
	                        <asp:ListItem Value="3">&nbsp;Not Recorded</asp:ListItem>
                        </asp:RadioButtonList>
                    </td>
                </tr>
                <!-- Question 2 -->
                <tr class="table_body_bg">
                    <td>
                        <strong>2. Was brain MRI performed for this complaint prior to your initial examination? If no, was brain MRI ordered by you?</strong>
                        <asp:Label ID="LabelRBManageMRI" runat="server" Visible="false"  ForeColor="Red" Text="<br />Please complete"></asp:Label>
                    </td>
                    <td>
                        <asp:RadioButtonList id="RadioButtonListRBManageMRI" RepeatDirection="Horizontal" CssClass="aspxList" runat="server">
	                        <asp:ListItem Value="1">&nbsp;Yes</asp:ListItem>
	                        <asp:ListItem Value="2">&nbsp;No</asp:ListItem>
	                        <asp:ListItem Value="3">&nbsp;Not Recorded</asp:ListItem>
                        </asp:RadioButtonList>
                    </td>
                </tr>
                <!-- Question 2a -->
                <tr class="table_body_bg">
                    <td>
                        <span id="QM3txta"><strong>2a. If so, was at least one white matter lesion consistent with demyelination present?</strong></span>
                        <asp:Label ID="LabelRBManageWhiteMatterLesion" runat="server" Visible="false"  ForeColor="Red" Text="<br />Please complete"></asp:Label>
                    </td>
                    <td>
                        <span id="QM3txtb">
                        <asp:RadioButtonList id="RadioButtonListRBManageWhiteMatterLesion" RepeatDirection="Horizontal" CssClass="aspxList" runat="server">
	                        <asp:ListItem Value="1">&nbsp;Yes</asp:ListItem>
	                        <asp:ListItem Value="2">&nbsp;No</asp:ListItem>
	                        <asp:ListItem Value="3">&nbsp;Not Recorded</asp:ListItem>
                        </asp:RadioButtonList>
                        </span>
                    </td>
                </tr>
                <!-- Question 3 -->
                <tr class="table_body">
                    <td>
                        <strong>3. Was systemic corticosteroid therapy instituted prior to your initial examination?</strong>
                        <asp:Label ID="LabelRBManageSystemicCorticosteroid" runat="server" Visible="false"  ForeColor="Red" Text="<br />Please complete"></asp:Label>
                    </td>
                    <td>
                        <asp:RadioButtonList id="RadioButtonListRBManageSystemicCorticosteroid" RepeatDirection="Horizontal" CssClass="aspxList" runat="server">
	                        <asp:ListItem Value="1">&nbsp;Yes</asp:ListItem>
	                        <asp:ListItem Value="2">&nbsp;No</asp:ListItem>
	                        <asp:ListItem Value="3">&nbsp;Not Recorded</asp:ListItem>
                        </asp:RadioButtonList>
                    </td>
                </tr>
                <!-- Question 3a -->
                <tr class="table_body">
                    <td id="QM3aA">
                        <strong>3a. If so, what dosage and route of administration?</strong>
                    </td>
                    <td id="QM3aB">
                        <asp:TextBox runat="server" ID="TextBoxSystemicCSText" size="25" />
                    </td>
                </tr>
                <!-- Question 3b -->
                <tr class="table_body">
                    <td id="QM3bA">
                        <strong>3b. If no, was corticosteroid therapy instituted by you?</strong>
                    </td>
                    <td id="QM3bB">
                        <asp:RadioButtonList id="RadioButtonListCorticosteroidTherapy" RepeatDirection="Horizontal" CssClass="aspxList" runat="server">
	                        <asp:ListItem Value="1">&nbsp;Yes</asp:ListItem>
	                        <asp:ListItem Value="2">&nbsp;No</asp:ListItem>
                        </asp:RadioButtonList>
                    </td>
                </tr>
                <!-- Question 3c -->
                <tr class="table_body">
                    <td id="QM3cA">
                        <strong>3c. If so, were corticosteroid side-effects discussed?</strong>
                        <asp:Label ID="LabelRBManageSystemicCorticosteroidSE" runat="server" Visible="false"  ForeColor="Red" Text="<br />Please complete"></asp:Label>
                    </td>
                    <td id="QM3cB">
                        <asp:RadioButtonList id="RadioButtonListRBManageSystemicCorticosteroidSE" RepeatDirection="Horizontal" CssClass="aspxList" runat="server">
	                        <asp:ListItem Value="1">&nbsp;Yes</asp:ListItem>
	                        <asp:ListItem Value="2">&nbsp;No</asp:ListItem>
	                        <asp:ListItem Value="3">&nbsp;Not Recorded</asp:ListItem>
                        </asp:RadioButtonList>
                    </td>
                </tr>
            </table>
            <br />
            <!-- Outcomes Section -->
            <table>
                <!-- Header - Outcomes -->
                <tr>
                    <th colspan="3" width="910px"><p>Outcomes</p></th>
                </tr>
                <!-- Question 1 -->
                <tr class="table_body">
                    <td colspan="2">
                        <strong>1. Was a follow-up examination within 2 months recommended or performed?</strong>
                        <asp:Label ID="LabelOutcomeFollowUpExam" runat="server" Visible="false"  ForeColor="Red" Text="<br />Please complete"></asp:Label>
                    </td>
                    <td>
                        <asp:RadioButton runat="server" ID="RadioButtonOutcomeFollowUpExamYes" GroupName="OutcomeFollowUpExam" text="Yes"/>
                        <asp:RadioButton runat="server" ID="RadioButtonOutcomeFollowUpExamNo" GroupName="OutcomeFollowUpExam" text="No" />
                    </td>
                </tr>
                <!-- Question 1a -->
                <tr class="table_body">
                    <td colspan="2" id="QO1aA">
                        <strong>1a. Was follow-up best corrected or pinhole visual acuity recorded?</strong>
                        <asp:Label ID="LabelRBOutcomeFollowUpBCVARecorded" runat="server" Visible="false"  ForeColor="Red" Text="<br />Please complete"></asp:Label>
                    </td>
                    <td>
                        <span id="QO2txtb">
                        <asp:RadioButtonList id="RadioButtonListRBOutcomeFollowUpBCVARecorded" RepeatDirection="Vertical" CssClass="aspxList" runat="server">
	                        <asp:ListItem Value="1">&nbsp;Yes</asp:ListItem>
	                        <asp:ListItem Value="2">&nbsp;No</asp:ListItem>
	                        <asp:ListItem Value="3">&nbsp;Not Recorded</asp:ListItem>
                            <asp:ListItem Value="350">&nbsp;Data Not Available</asp:ListItem>
                        </asp:RadioButtonList>
                        </span>
                        
                    </td>
                </tr>
                <!-- Question 1b -->
                <tr class="table_body">
                    <td style="width:50%;" rowspan="2">
                        <strong><span id="QO3txta">1b. If so, what was the acuity?</span>&nbsp;<a href="#"><img id="QO3" src="../../common/images/tip.gif" alt="Tip" /></a></strong>
                        <asp:Label ID="LabelOutcomeBestCorrectedVisualOD" runat="server" Visible="false"  ForeColor="Red" Text="<br />Please complete"></asp:Label>
                        <asp:Label ID="LabelOutcomeBestCorrectedVisualOS" runat="server" Visible="false"  ForeColor="Red" Text="<br />Please complete"></asp:Label>
                    </td>
                    <td  style="width:20%;"><span class="right" id="QO3txtb">OD</span></td>
                    <td>
                        <span id="QO3aA">20&nbsp;/&nbsp;<asp:DropDownList ID="DropdownListOutcomeBestCorrectedVisualOD" DataValueField="examValue" DataTextField="examLabel" runat="server" onchange="QO1bODCB();" />
                        <asp:CheckBox ID="CheckBoxDropdownListOutcomeBestCorrectedVisualODNA" Text="Data not available" runat="server" onclick="QO1bODDD();" /></span>
                    </td>
                </tr>
                <tr class="table_body">
                    <td><span class="right" id="QO3txtc">OS</span></td>
                    <td id="QO3aB">
                        20&nbsp;/&nbsp;<asp:DropDownList ID="DropdownListOutcomeBestCorrectedVisualOS" DataValueField="examValue" DataTextField="examLabel" runat="server" onchange="QO1bOSCB();" />
                        <asp:CheckBox ID="CheckBoxDropdownListOutcomeBestCorrectedVisualOSNA" Text="Data not available" runat="server" onclick="QO1bOSDD();" />
                    </td>
                </tr>
                <!-- Question 1c -->
                <tr class="table_body">
                    <td colspan="2">
                        <strong><span id="QO4txta">1c. Was follow-up perimetry performed?</span></strong>
                        <asp:Label ID="LabelOutcomePerimetry" runat="server" Visible="false"  ForeColor="Red" Text="<br />Please complete"></asp:Label>
                    </td>
                    <td>
                        <span id="QO4txtb">
                            <asp:RadioButtonList id="RadioButtonListOutcomePerimetry" RepeatDirection="Vertical" CssClass="aspxList" runat="server">
	                        <asp:ListItem Value="1">&nbsp;Yes</asp:ListItem>
	                        <asp:ListItem Value="2">&nbsp;No</asp:ListItem>
	                      
                            <asp:ListItem Value="350">&nbsp;Data Not Available</asp:ListItem>
                        </asp:RadioButtonList>
                        </span>
                    </td>
                </tr>
                <!-- Question 1d -->
                <tr class="table_body">
                    <td colspan="2">
                        <span id="QO5txta"><strong>1d. If so, what defect was present?</strong></span>
                        <asp:Label ID="LabelRBOutcomePerimetryDefect" runat="server" Visible="false"  ForeColor="Red" Text="<br />Please complete"></asp:Label>
                    </td>
                    <td>
                        <span id="QO5txtb">
                        <asp:RadioButtonList id="RadioButtonListRBOutcomePerimetryDefect" RepeatDirection="Vertical" CssClass="aspxList" runat="server">
	                        <asp:ListItem Value="30">&nbsp;Arculate Scotoma</asp:ListItem>
	                        <asp:ListItem Value="31">&nbsp;Altitudinal Defect</asp:ListItem>
	                        <asp:ListItem Value="32">&nbsp;Central/Cecocentral Scotoma</asp:ListItem>
                            <asp:ListItem Value="33">&nbsp;Generalized Depression</asp:ListItem>
                            <asp:ListItem Value="350" >&nbsp;Data Not Available</asp:ListItem>
                            <asp:ListItem Value="34">&nbsp;Other</asp:ListItem>
                        </asp:RadioButtonList>
                        </span>
                    </td>
                </tr>
                <!-- Question 1e -->
                <tr class="table_body">
                    <td colspan="2">
                        <strong><span id="QO6txta">1e. If automated static perimetry  performed, mean defect:</span></strong>
                        <asp:Label ID="LabelOutcomeMeanDefect" runat="server" Visible="false"  ForeColor="Red" Text="<br />Please complete"></asp:Label>
                    </td>
                    <td>
                        <span id="QO6txtb">
                            <asp:TextBox runat="server" ID="TextBoxOutcomeMeanDefectDB" size="3" onkeyup="QO6toggleCB();" meta="{vMin: '-99999', vMax: '99999'}" CssClass="auto" /><label> dB</label>
                        </span>
                    </td>
                </tr>
                <!-- Question 2 -->
                <tr class="table_body_bg">
                    <td colspan="2">
                        <strong><span id="QO7txta">2. If MRI showed at least one white matter lesion consistent with demyelination, was referral to a neurologist for consideration of immunomodulation agents discussed?</span></strong>
                        <asp:Label ID="LabelRBOutcomeWhiteMatterLesionReferral" runat="server" Visible="false"  ForeColor="Red" Text="<br />Please complete"></asp:Label>
                    </td>
                    <td>
                        <span id="QO7txtb">
                        <asp:RadioButtonList id="RadioButtonListRBOutcomeWhiteMatterLesionReferral" RepeatDirection="Horizontal" CssClass="aspxList" runat="server">
	                        <asp:ListItem Value="1">&nbsp;Yes</asp:ListItem>
	                        <asp:ListItem Value="2">&nbsp;No</asp:ListItem>
	                        <asp:ListItem Value="3">&nbsp;Not Recorded</asp:ListItem>
                        </asp:RadioButtonList>
                        </span>
                    </td>
                </tr>
                <!-- Question 3 -->
                <tr class="table_body">
                    <td colspan="2">
                        <strong><span id="QO8txta">3. Was final diagnosis of etiology recorded?</span></strong>
                        <asp:Label ID="LabelRBOutcomeFinalEtiology" runat="server" Visible="false"  ForeColor="Red" Text="<br />Please complete"></asp:Label>
                    </td>
                    <td>
                        <span id="QO8txtb">
                        <asp:RadioButtonList id="RadioButtonListRBOutcomeFinalEtiology" RepeatDirection="Horizontal" CssClass="aspxList" runat="server">
	                        <asp:ListItem Value="1">&nbsp;Yes</asp:ListItem>
	                        <asp:ListItem Value="2">&nbsp;No</asp:ListItem>
	                        <asp:ListItem Value="3">&nbsp;Not Recorded</asp:ListItem>
                        </asp:RadioButtonList>
                        </span>
                    </td>
                </tr>
            </table>
            <div class="button-box">
                <asp:LinkButton ID="LinkButtonBackToDashboard" runat="server" Text="Back To Chart Registration" PostBackUrl="../PatientChartRegistration.aspx?CycleNumber=1" Visible="false" CssClass="button" />
                <asp:LinkButton ID="ButtonSubmit" OnClick="ButtonSubmit_Click" runat="server" Text="Submit Chart" CssClass="button" />
            </div>
        </div>
        <!-- ION pim ends -->
</asp:Content>

<asp:Content runat="server" ID="content4" ContentPlaceHolderID="javascript">
    	<script type="text/javascript" src="../../common/js/jquery.atooltip.js"></script>
    <script type="text/javascript" language="javascript">

        function DOBValidation() {
            var month = document.getElementById("<%= DropDownListMonthOfBirth.ClientID %>").value;
            var year = document.getElementById("<%= DropDownListYearOfBirth.ClientID %>").value;
            var monthsecond = document.getElementById("<%= DropDownListMonthOfFirstExam.ClientID %>").value;
            var yearsecond = document.getElementById("<%= DropDownListYearOfFirstExam.ClientID %>").value;
            if (((month != 0) && (year != 0)) && (validateDate(month, year) == true) && (monthsecond != 0) && (yearsecond != 0)) {
                if (validateAge() != true) {
                    alert("Invalid age, patient must have been over the age 18 at the first exam.");
                    enabledControlDropDown(document.getElementById("<%= DropDownListMonthOfBirth.ClientID %>"), false, true);
                    enabledControlDropDown(document.getElementById("<%= DropDownListYearOfBirth.ClientID %>"), false, true);
                }
            }
        }

        function firstExamValidation() {
            var month = document.getElementById("<%= DropDownListMonthOfBirth.ClientID %>").value;
            var year = document.getElementById("<%= DropDownListYearOfBirth.ClientID %>").value;
            var monthsecond = document.getElementById("<%= DropDownListMonthOfFirstExam.ClientID %>").value;
            var yearsecond = document.getElementById("<%= DropDownListYearOfFirstExam.ClientID %>").value;
            if (((month != 0) && (year != 0)) && (validateDate(month, year) == true) && (monthsecond != 0) && (yearsecond != 0)) {
                if (validateAge() != true) {
                    alert("Please check date, patient must have been over the age 18 at the first exam.");
                    enabledControlDropDown(document.getElementById("<%= DropDownListMonthOfFirstExam.ClientID %>"), false, true);
                    enabledControlDropDown(document.getElementById("<%= DropDownListYearOfFirstExam.ClientID %>"), false, true);
                }
            }
        }

        // Checks that the date has happened
        function validateDate(month, year) {
            var today = new Date();
            var monthVar = month - 1;
            var checkYear = today.getFullYear() - year;
            var checkMonth = today.getMonth() - monthVar;
            if (checkMonth < 0) {
                checkYear--;
            }
            if (checkYear < 0) {
                alert("Invalid date, you must pick a date that has already occurred.");
                return false;
            }
            else {
                return true;
            }
        }

        // Checks to make sure the patient is within the proper age range
        function validateAge() {
            var DOBMonth = document.getElementById("<%= DropDownListMonthOfBirth.ClientID %>").value;
            var DOBYear = document.getElementById("<%= DropDownListYearOfBirth.ClientID %>").value;
            var ExamMonth = document.getElementById("<%= DropDownListMonthOfFirstExam.ClientID %>").value;
            var ExamYear = document.getElementById("<%= DropDownListYearOfFirstExam.ClientID %>").value;

            if ((DOBMonth != 0) && (DOBYear != 0) && (ExamMonth != 0) && (ExamYear != 0)) {
                var age = getAge(DOBMonth, DOBYear, ExamMonth, ExamYear);
                if (age >= 18) {
                    //alert(age);
                    return true;
                }
                else {
                    //alert(age);
                    return false;
                }
            }
        }

        // will return age between the two dates
        function getAge(dobMon, dobYear, secMon, secYear) {
            var DOBMonth = dobMon;
            var DOBYear = dobYear;
            var ExamMonth = secMon;
            var ExamYear = secYear;
            //alert("DOBMonth: " + DOBMonth + "; DOBYear: " + DOBYear + "; ExamMonth: " + ExamMonth + "; ExamYear: " + ExamYear);
            var age = ExamYear - DOBYear;
            var m = ExamMonth - DOBMonth;
            if (m < 0) {
                age--;
            }
            return age;
        }

        //      Either enables or disables input boxes
        function enabledControl(el, disabled, checked) {
            //alert("Hello world from enabled control");
            try {
                if (disabled) {
                    el.disabled = disabled;
                    el.setAttribute("disabled", true);
                }
                else {
                    el.disabled = disabled;
                    el.removeAttribute("disabled");
                }

                if (checked == true)
                    el.checked = false;
            }
            catch (E) {
            }
            if (el.childNodes && el.childNodes.length > 0) {
                for (var x = 0; x < el.childNodes.length; x++) {
                    enabledControl(el.childNodes[x], disabled, checked);
                }
            }
        }
        //      Either enables or disables dropdown boxes
        function enabledControlDropDown(el, disabled, checked) {
            //alert("Hello world from enabled control");
            try {
                if (disabled) {
                    el.disabled = disabled;
                    el.setAttribute("disabled", true);
                }
                else {
                    el.disabled = disabled;
                    el.removeAttribute("disabled");
                }

                if (checked == true)
                    el.options[0].selected = true;
            }
            catch (E) {
            }
            if (el.childNodes && el.childNodes.length > 0) {
                for (var x = 0; x < el.childNodes.length; x++) {
                    enabledControl(el.childNodes[x], disabled, checked);
                }
            }
        }
        //      Either enables or disables text boxes
        function enabledControlTextField(el, disabled, checked) {
            //alert("Hello world from enabled control");
            try {
                if (disabled) {
                    el.disabled = disabled;
                    el.setAttribute("disabled", true);
                }
                else {
                    el.disabled = disabled;
                    el.removeAttribute("disabled");
                }

                if (checked == true) {
                    el.value = '';
                }
            }
            catch (E) {
            }
            if (el.childNodes && el.childNodes.length > 0) {
                for (var x = 0; x < el.childNodes.length; x++) {
                    enabledControl(el.childNodes[x], disabled, checked);
                }
            }
        }

        function generate(arr1, arr2, istrue) {
            if (istrue == true) {
                for (var i = 0; i < arr1.length; i++) {
                    document.getElementById(arr1[i]).style.color = "gray";
                }
                for (var i = 0; i < arr2.length; i++) {
                    $(arr2[i] + ' :input').attr('disabled', true);
                    $(arr2[i] + ' :input').removeAttr("checked");
                }
            }
            if (istrue == false) {
                for (var i = 0; i < arr1.length; i++) {
                    document.getElementById(arr1[i]).style.color = "#333";
                }
                for (var i = 0; i < arr2.length; i++) {
                    $(arr2[i] + ' :input').removeAttr('disabled');
                }
            }
        }


        // Activates Question seven in the history section
        function activateQH7() {
            enabledControl(document.getElementById("<%= CheckBoxSymptomsAtaxia.ClientID %>"), false, false);
            enabledControl(document.getElementById("<%= CheckBoxSymptomsDiplopia.ClientID %>"), false, false);
            enabledControl(document.getElementById("<%= CheckBoxSymptomsParesthesia.ClientID %>"), false, false);
            enabledControl(document.getElementById("<%= CheckBoxSymptomsWeakness.ClientID %>"), false, false);
            enabledControl(document.getElementById("<%= CheckBoxSymptomsOther.ClientID %>"), false, false);
            document.getElementById("QH7txta").style.color = "#333";
            document.getElementById("QH7txtb").style.color = "#333";
        }

        // deactivates Question seven in the history section
        function deActivateQH7() {
            enabledControl(document.getElementById("<%= CheckBoxSymptomsAtaxia.ClientID %>"), true, true);
            enabledControl(document.getElementById("<%= CheckBoxSymptomsDiplopia.ClientID %>"), true, true);
            enabledControl(document.getElementById("<%= CheckBoxSymptomsParesthesia.ClientID %>"), true, true);
            enabledControl(document.getElementById("<%= CheckBoxSymptomsWeakness.ClientID %>"), true, true);
            enabledControl(document.getElementById("<%= CheckBoxSymptomsOther.ClientID %>"), true, true);
            document.getElementById("QH7txta").style.color = "gray";
            document.getElementById("QH7txtb").style.color = "gray";
        }
        // Activates Question seven in the history section
        function activateQH9() {
            enabledControl(document.getElementById("<%= CheckBoxHistorySIBartonella.ClientID %>"), false, false);
            enabledControl(document.getElementById("<%= CheckBoxHistorySISyphillis.ClientID %>"), false, false);
            enabledControl(document.getElementById("<%= CheckBoxHistorySIViral.ClientID %>"), false, false);
     
            enabledControl(document.getElementById("<%= CheckBoxHistoryAIOther.ClientID %>"), false, false);
            document.getElementById("QH9txta").style.color = "#333";
            document.getElementById("QH9txtb").style.color = "#333";
        }

        // deactivates Question seven in the history section
        function deActivateQH9() {
            enabledControl(document.getElementById("<%= CheckBoxHistorySIBartonella.ClientID %>"), true, true);
            enabledControl(document.getElementById("<%= CheckBoxHistorySISyphillis.ClientID %>"), true, true);
            enabledControl(document.getElementById("<%= CheckBoxHistorySIViral.ClientID %>"), true, true);
          
            enabledControl(document.getElementById("<%= CheckBoxHistoryAIOther.ClientID %>"), true, true);
            document.getElementById("QH9txta").style.color = "gray";
            document.getElementById("QH9txtb").style.color = "gray";
        }
        // EXAM
        function activateQE5() {
            enabledControl(document.getElementById("<%= RadioButtonListRBExamOcularMotilityType.ClientID %>"), false, false);
            document.getElementById("QE5txta").style.color = "#333";
            document.getElementById("QE5txtb").style.color = "#333";
        }

        // deactivates Question seven in the history section
        function deActivateQE5() {
            enabledControl(document.getElementById("<%= RadioButtonListRBExamOcularMotilityType.ClientID %>"), true, true);
            document.getElementById("QE5txta").style.color = "gray";
            document.getElementById("QE5txtb").style.color = "gray";
        }

        function activateQE10() {
            enabledControl(document.getElementById("<%= RadioButtonListRBExamBaselinePerimetryDefect.ClientID %>"), false, false);
            document.getElementById("QE10txta").style.color = "#333";
            document.getElementById("QE10txtb").style.color = "#333";
        }

        // deactivates Question seven in the history section
        function deActivateQE10() {
            enabledControl(document.getElementById("<%= RadioButtonListRBExamBaselinePerimetryDefect.ClientID %>"), true, true);
            document.getElementById("QE10txta").style.color = "gray";
            document.getElementById("QE10txtb").style.color = "gray";
        }

        function activateQM3() {
            enabledControl(document.getElementById("<%= RadioButtonListRBManageWhiteMatterLesion.ClientID %>"), false, false);
            document.getElementById("QM3txta").style.color = "#333";
            document.getElementById("QM3txtb").style.color = "#333";
        }

        // deactivates Question seven in the history section
        function deActivateQM3() {
            enabledControl(document.getElementById("<%= RadioButtonListRBManageWhiteMatterLesion.ClientID %>"), true, false);
            document.getElementById("QM3txta").style.color = "gray";
            document.getElementById("QM3txtb").style.color = "gray";
        }

        function activateQM5and6() {

            enabledControl(document.getElementById("<%= RadioButtonListRBManageSystemicCorticosteroidSE.ClientID %>"), false, false);
            enabledControlTextField(document.getElementById("<%= TextBoxSystemicCSText.ClientID %>"), false, false);
            document.getElementById("QM5txta").style.color = "#333";
            document.getElementById("QM5txtb").style.color = "#333";
            document.getElementById("QM6txta").style.color = "#333";
            document.getElementById("QM6txtb").style.color = "#333";
        }

        // deactivates Question seven in the history section
        function deActivateQM5and6() {

            enabledControl(document.getElementById("<%= RadioButtonListRBManageSystemicCorticosteroidSE.ClientID %>"), true, true);
            enabledControlTextField(document.getElementById("<%= TextBoxSystemicCSText.ClientID %>"), true, true);
            ena
            document.getElementById("QM5txta").style.color = "gray";
            document.getElementById("QM5txtb").style.color = "gray";
            document.getElementById("QM6txta").style.color = "gray";
            document.getElementById("QM6txtb").style.color = "gray";
        }

        function QE11toggleTB() {
            enabledControlTextField(document.getElementById("<%= TextBoxExamMeanDefectDB.ClientID %>"), false, true);
        }



        function QO1clicked() {
            if (document.getElementById("<%= RadioButtonOutcomeFollowUpExamYes.ClientID %>").checked == true) {
                activateQO2();
                var QO2isYes = $('#<%= RadioButtonListRBOutcomeFollowUpBCVARecorded.ClientID %>').find('input:checked').val();
                if (QO2isYes == '1') {
                    activateQO3();
                }
                activateQO4();
                activateQO5();
                activateQO6();
                activateQO7();
                activateQO8();
            }
            else {
                deActivateQO2();
                deActivateQO3();
                deActivateQO4();
                deActivateQO5();
                deActivateQO6();
                deActivateQO7();
                deActivateQO8();
            }
        }

        function QO2clicked() {
            var QO2isYes = $('#<%= RadioButtonListRBOutcomeFollowUpBCVARecorded.ClientID %>').find('input:checked').val();
            if (QO2isYes == '1') {
                activateQO3();
            }
            else {
                deActivateQO3();
            }
        }



        function QO6toggleTB() {
            enabledControlTextField(document.getElementById("<%= TextBoxOutcomeMeanDefectDB.ClientID %>"), false, true);

        }



        function activateQO2() {
            enabledControl(document.getElementById("<%= RadioButtonListRBOutcomeFollowUpBCVARecorded.ClientID %>"), false, false);
            document.getElementById("QO2txta").style.color = "#333";
            document.getElementById("QO2txtb").style.color = "#333";
        }

        function deActivateQO2() {
            enabledControl(document.getElementById("<%= RadioButtonListRBOutcomeFollowUpBCVARecorded.ClientID %>"), true, true);
            document.getElementById("QO2txta").style.color = "gray";
            document.getElementById("QO2txtb").style.color = "gray";
        }

        function activateQO3() {
            enabledControl(document.getElementById("<%= DropdownListOutcomeBestCorrectedVisualOD.ClientID %>"), false, false);
            enabledControl(document.getElementById("<%= DropdownListOutcomeBestCorrectedVisualOS.ClientID %>"), false, false);
            document.getElementById("QO3txta").style.color = "#333";
            document.getElementById("QO3txtb").style.color = "#333";
            document.getElementById("QO3txtc").style.color = "#333";
        }

        function deActivateQO3() {
            enabledControl(document.getElementById("<%= DropdownListOutcomeBestCorrectedVisualOD.ClientID %>"), true, true);
            enabledControl(document.getElementById("<%= DropdownListOutcomeBestCorrectedVisualOS.ClientID %>"), true, true);
            document.getElementById("QO3txta").style.color = "gray";
            document.getElementById("QO3txtb").style.color = "gray";
            document.getElementById("QO3txtc").style.color = "gray";
        }





        function activateQO5() {
            enabledControl(document.getElementById("<%= RadioButtonListRBOutcomePerimetryDefect.ClientID %>"), false, false);
            document.getElementById("QO5txta").style.color = "#333";
            document.getElementById("QO5txtb").style.color = "#333";
        }

        // deactivates Question seven in the history section
        function deActivateQO5() {
            enabledControl(document.getElementById("<%= RadioButtonListRBOutcomePerimetryDefect.ClientID %>"), true, true);
            document.getElementById("QO5txta").style.color = "gray";
            document.getElementById("QO5txtb").style.color = "gray";
        }

        function activateQO6() {

            enabledControl(document.getElementById("<%= TextBoxOutcomeMeanDefectDB.ClientID %>"), false, false);
            document.getElementById("QO6txta").style.color = "#333";
            document.getElementById("QO6txtb").style.color = "#333";
        }

        function deActivateQO6() {

            enabledControl(document.getElementById("<%= TextBoxOutcomeMeanDefectDB.ClientID %>"), true, true);
            document.getElementById("QO6txta").style.color = "gray";
            document.getElementById("QO6txtb").style.color = "gray";
        }

        function activateQO7() {
            enabledControl(document.getElementById("<%= RadioButtonListRBOutcomeWhiteMatterLesionReferral.ClientID %>"), false, false);
            document.getElementById("QO7txta").style.color = "#333";
            document.getElementById("QO7txtb").style.color = "#333";
        }

        function deActivateQO7() {
            enabledControl(document.getElementById("<%= RadioButtonListRBOutcomeWhiteMatterLesionReferral.ClientID %>"), true, true);
            document.getElementById("QO7txta").style.color = "gray";
            document.getElementById("QO7txtb").style.color = "gray";
        }

        function activateQO8() {
            enabledControl(document.getElementById("<%= RadioButtonListRBOutcomeFinalEtiology.ClientID %>"), false, false);
            document.getElementById("QO8txta").style.color = "#333";
            document.getElementById("QO8txtb").style.color = "#333";
        }

        function deActivateQO8() {
            enabledControl(document.getElementById("<%= RadioButtonListRBOutcomeFinalEtiology.ClientID %>"), true, true);
            document.getElementById("QO8txta").style.color = "gray";
            document.getElementById("QO8txtb").style.color = "gray";
        }

        // QO1 toggles
        function QO1bODDD() {
            enabledControlDropDown(document.getElementById("<%= DropdownListOutcomeBestCorrectedVisualOD.ClientID %>"), false, true);
        }
        function QO1bODCB() {
            enabledControl(document.getElementById("<%= CheckBoxDropdownListOutcomeBestCorrectedVisualODNA.ClientID %>"), false, true);
        }
        function QO1bOSDD() {
            enabledControlDropDown(document.getElementById("<%= DropdownListOutcomeBestCorrectedVisualOS.ClientID %>"), false, true);
        }
        function QO1bOSCB() {
            enabledControl(document.getElementById("<%= CheckBoxDropdownListOutcomeBestCorrectedVisualOSNA.ClientID %>"), false, true);
        }

        window.onload = function onLoadForm() {
            var QH6; /* = $('#<%= RadioButtonListRBSymptomsGeneralNeurologic.ClientID %>').find('input:checked').val();*/
            var QH8; /* = $('#<%= RadioButtonListRBHistoryAutoimmuneDisease.ClientID %>').find('input:checked').val();*/
            var QE4 = $('#<%= RadioButtonListRBExamOcularMotility.ClientID %>').find('input:checked').val();
            var QE7 = $('#<%= RadioButtonListRBExamOpticDisc.ClientID %>').find('input:checked').val();
            var QE9 = document.getElementById("<%= RadioButtonExamBaselinePerimetryYes.ClientID %>").checked;
            var QM2 = $('#<%= RadioButtonListRBManageMRI.ClientID %>').find('input:checked').val();
            var QM4 = $('#<%= RadioButtonListRBManageSystemicCorticosteroid.ClientID %>').find('input:checked').val();

            QO2clicked();
            QO4clicked();

            if (QE9 != true) {
                deActivateQE10();
            }

            if (QM2 == '2' || QM2 == '3') {
            }

            if (QM4 == '2' || QM4 == '3') {
                deActivateQM5and6();
            }
        }


        $(document).ready(function() {

            // QM3
            $("#<%= RadioButtonListRBManageSystemicCorticosteroid.ClientID %>").click(function() {
                if ($('#<%= RadioButtonListRBManageSystemicCorticosteroid.ClientID %>').find('input:checked').val() == ('1')) {
                    var myaray = new Array("QM3aA", "QM3aB");
                    var myarray2 = new Array('#QM3aB');
                    generate(myaray, myarray2, false);

                    enabledControlTextField(document.getElementById("<%= TextBoxSystemicCSText.ClientID %>"), false, false);
                    disable3C();
                }
                else {
                    var myaray = new Array("QM3aA", "QM3aB");
                    var myarray2 = new Array('#QM3aB');
                    generate(myaray, myarray2, true);

                    enabledControlTextField(document.getElementById("<%= TextBoxSystemicCSText.ClientID %>"), true, true);
                }
            });

            // QM3b, When 3 is no
            $("#<%= RadioButtonListRBManageSystemicCorticosteroid.ClientID %>").click(function() {
                if ($('#<%= RadioButtonListRBManageSystemicCorticosteroid.ClientID %>').find('input:checked').val() == ('2')) {
                    var myaray = new Array("QM3bA", "QM3bB");
                    var myarray2 = new Array('#QM3bB');
                    generate(myaray, myarray2, false);
                }
                else {
                    var myaray = new Array("QM3bA", "QM3bB");
                    var myarray2 = new Array('#QM3bB');
                    generate(myaray, myarray2, true);
                }
            });
            if ($('#<%= RadioButtonListRBManageSystemicCorticosteroid.ClientID %>').find('input:checked').val() == ('2')) {
                var myaray = new Array("QM3bA", "QM3bB");
                var myarray2 = new Array('#QM3bB');
                generate(myaray, myarray2, false);
            }
            else {
                var myaray = new Array("QM3bA", "QM3bB");
                var myarray2 = new Array('#QM3bB');
                generate(myaray, myarray2, true);
            }


            // QM3c, When 3b is yes
            $("#<%= RadioButtonListCorticosteroidTherapy.ClientID %>").click(function() {
                if ($('#<%= RadioButtonListCorticosteroidTherapy.ClientID %>').find('input:checked').val() == ('1')) {
                    var myaray = new Array("QM3cA", "QM3cB");
                    var myarray2 = new Array('#QM3cB');
                    generate(myaray, myarray2, false);
                }
                else {
                    var myaray = new Array("QM3cA", "QM3cB");
                    var myarray2 = new Array('#QM3cB');
                    generate(myaray, myarray2, true);
                }
            });
            if ($('#<%= RadioButtonListCorticosteroidTherapy.ClientID %>').find('input:checked').val() == ('1')) {
                var myaray = new Array("QM3cA", "QM3cB");
                var myarray2 = new Array('#QM3cB');
                generate(myaray, myarray2, false);
            }
            else {
                var myaray = new Array("QM3cA", "QM3cB");
                var myarray2 = new Array('#QM3cB');
                generate(myaray, myarray2, true);
            }

            function disable3C() {
                var myaray = new Array("QM3cA", "QM3cB");
                var myarray2 = new Array('#QM3cB');
                generate(myaray, myarray2, true);
            }

            // QM3
            $("#<%= RadioButtonListRBOutcomeFollowUpBCVARecorded.ClientID %>").click(function() {
                if ($('#<%= RadioButtonListRBOutcomeFollowUpBCVARecorded.ClientID %>').find('input:checked').val() == ('1')) {
                    var myaray = new Array("QO3txta", "QO3txtb", "QO3txtc", "QO3aA", "QO3aB");
                    var myarray2 = new Array('#QO3aA', '#QO3aB');
                    generate(myaray, myarray2, false);

                    enabledControlTextField(document.getElementById("<%= DropdownListOutcomeBestCorrectedVisualOD.ClientID %>"), false, false);
                    enabledControlTextField(document.getElementById("<%= DropdownListOutcomeBestCorrectedVisualOS.ClientID %>"), false, false);
                }
                else {
                    var myaray = new Array("QO3txta", "QO3txtb", "QO3txtc", "QO3aA", "QO3aB");
                    var myarray2 = new Array('#QO3aA', '#QO3aB');
                    generate(myaray, myarray2, true);

                    enabledControlTextField(document.getElementById("<%= DropdownListOutcomeBestCorrectedVisualOD.ClientID %>"), true, true);
                    enabledControlTextField(document.getElementById("<%= DropdownListOutcomeBestCorrectedVisualOS.ClientID %>"), true, true);
                }
            });
            if ($('#<%= RadioButtonListRBOutcomeFollowUpBCVARecorded.ClientID %>').find('input:checked').val() == ('1')) {
                var myaray = new Array("QO3txta", "QO3txtb", "QO3txtc", "QO3aA", "QO3aB");
                var myarray2 = new Array('#QO3aA', '#QO3aB');
                generate(myaray, myarray2, false);

                enabledControlTextField(document.getElementById("<%= DropdownListOutcomeBestCorrectedVisualOD.ClientID %>"), false, false);
                enabledControlTextField(document.getElementById("<%= DropdownListOutcomeBestCorrectedVisualOS.ClientID %>"), false, false);
            }
            else {
                var myaray = new Array("QO3txta", "QO3txtb", "QO3txtc", "QO3aA", "QO3aB");
                var myarray2 = new Array('#QO3aA', '#QO3aB');
                generate(myaray, myarray2, true);

                enabledControlTextField(document.getElementById("<%= DropdownListOutcomeBestCorrectedVisualOD.ClientID %>"), true, true);
                enabledControlTextField(document.getElementById("<%= DropdownListOutcomeBestCorrectedVisualOS.ClientID %>"), true, true);
            }

            //QH6a
            $("#<%= RadioButtonListRBSymptomsGeneralNeurologic.ClientID %>").click(function() {
                if ($('#<%= RadioButtonListRBSymptomsGeneralNeurologic.ClientID %>').find('input:checked').val() == ('1')) {
                    var myaray = new Array("QH7txta", "QH7txtb");
                    var myarray2 = new Array('#QH7txtb');
                    generate(myaray, myarray2, false);
                }
                else {
                    var myaray = new Array("QH7txta", "QH7txtb");
                    var myarray2 = new Array('#QH7txtb');
                    generate(myaray, myarray2, true);
                }
            });
            if ($('#<%= RadioButtonListRBSymptomsGeneralNeurologic.ClientID %>').find('input:checked').val() == ('1')) {
                var myaray = new Array("QH7txta", "QH7txtb");
                var myarray2 = new Array('#QH7txtb');
                generate(myaray, myarray2, false);
            }
            else {
                var myaray = new Array("QH7txta", "QH7txtb");
                var myarray2 = new Array('#QH7txtb');
                generate(myaray, myarray2, true);
            }

            //QH7a
            $("#<%= RadioButtonListRBHistoryAutoimmuneDisease.ClientID %>").click(function() {
                if ($('#<%= RadioButtonListRBHistoryAutoimmuneDisease.ClientID %>').find('input:checked').val() == ('1')) {
                    var myaray = new Array("QH9txta", "QH9txtb");
                    var myarray2 = new Array('#QH9txtb');
                    generate(myaray, myarray2, false);
                }
                else {
                    var myaray = new Array("QH9txta", "QH9txtb");
                    var myarray2 = new Array('#QH9txtb');
                    generate(myaray, myarray2, true);
                }
            });
            if ($('#<%= RadioButtonListRBHistoryAutoimmuneDisease.ClientID %>').find('input:checked').val() == ('1')) {
                var myaray = new Array("QH9txta", "QH9txtb");
                var myarray2 = new Array('#QH9txtb');
                generate(myaray, myarray2, false);
            }
            else {
                var myaray = new Array("QH9txta", "QH9txtb");
                var myarray2 = new Array('#QH9txtb');
                generate(myaray, myarray2, true);
            }

            //QH8a
            $("#<%= RadioButtonListRBHistorySystemic.ClientID %>").click(function() {
                if ($('#<%= RadioButtonListRBHistorySystemic.ClientID %>').find('input:checked').val() == ('1')) {
                    var myaray = new Array("QH8aA", "QH8aB");
                    var myarray2 = new Array('#QH8aB');
                    generate(myaray, myarray2, false);
                }
                else {
                    var myaray = new Array("QH8aA", "QH8aB");
                    var myarray2 = new Array('#QH8aB');
                    generate(myaray, myarray2, true);
                }
            });
            if ($('#<%= RadioButtonListRBHistorySystemic.ClientID %>').find('input:checked').val() == ('1')) {
                var myaray = new Array("QH8aA", "QH8aB");
                var myarray2 = new Array('#QH8aB');
                generate(myaray, myarray2, false);
            }
            else {
                var myaray = new Array("QH8aA", "QH8aB");
                var myarray2 = new Array('#QH8aB');
                generate(myaray, myarray2, true);
            }


            //QEDT4a
            $("#<%= RadioButtonListRBExamOcularMotility.ClientID %>").click(function() {
                if ($('#<%= RadioButtonListRBExamOcularMotility.ClientID %>').find('input:checked').val() == ('1')) {
                    var myaray = new Array("QE5txta", "QE5txtb");
                    var myarray2 = new Array('#QE5txtb');
                    generate(myaray, myarray2, false);
                }
                else {
                    var myaray = new Array("QE5txta", "QE5txtb");
                    var myarray2 = new Array('#QE5txtb');
                    generate(myaray, myarray2, true);
                }
            });
            if ($('#<%= RadioButtonListRBExamOcularMotility.ClientID %>').find('input:checked').val() == ('1')) {
                var myaray = new Array("QE5txta", "QE5txtb");
                var myarray2 = new Array('#QE5txtb');
                generate(myaray, myarray2, false);
            }
            else {
                var myaray = new Array("QE5txta", "QE5txtb");
                var myarray2 = new Array('#QE5txtb');
                generate(myaray, myarray2, true);
            }

            //QM2a
            $("#<%= RadioButtonListRBManageMRI.ClientID %>").click(function() {
                if ($('#<%= RadioButtonListRBManageMRI.ClientID %>').find('input:checked').val() == ('1')) {
                    var myaray = new Array("QM3txta", "QM3txtb");
                    var myarray2 = new Array('#QM3txtb');
                    generate(myaray, myarray2, false);
                }
                else {
                    var myaray = new Array("QM3txta", "QM3txtb");
                    var myarray2 = new Array('#QM3txtb');
                    generate(myaray, myarray2, true);
                }
            });
            if ($('#<%= RadioButtonListRBManageMRI.ClientID %>').find('input:checked').val() == ('1')) {
                var myaray = new Array("QM3txta", "QM3txtb");
                var myarray2 = new Array('#QM3txtb');
                generate(myaray, myarray2, false);
            }
            else {
                var myaray = new Array("QM3txta", "QM3txtb");
                var myarray2 = new Array('#QM3txtb');
                generate(myaray, myarray2, true);
            }

            //QE6a
            $("#<%= RadioButtonListRBExamOpticDisc.ClientID %>").click(function() {
                if ($('#<%= RadioButtonListRBExamOpticDisc.ClientID %>').find('input:checked').val() == ('1')) {
                    var myaray = new Array("QE8txta", "QE8txtb");
                    var myarray2 = new Array('#QE8txtb');
                    generate(myaray, myarray2, false);
                }
                else {
                    var myaray = new Array("QE8txta", "QE8txtb");
                    var myarray2 = new Array('#QE8txtb');
                    generate(myaray, myarray2, true);
                }
            });
            if ($('#<%= RadioButtonListRBExamOpticDisc.ClientID %>').find('input:checked').val() == ('1')) {
                var myaray = new Array("QE8txta", "QE8txtb");
                var myarray2 = new Array('#QE8txtb');
                generate(myaray, myarray2, false);
            }
            else {
                var myaray = new Array("QE8txta", "QE8txtb");
                var myarray2 = new Array('#QE8txtb');
                generate(myaray, myarray2, true);
            }
        });

    </script>
    <script type="text/javascript">
        $(function() {
            $('#QE1').aToolTip({
                clickIt: true,
                tipContent: '20/800 or count fingers @ 5 ft<br />20/1000 or count fingers @ 4 ft<br />20/1600 or count fingers @ 3ft<br />20/2000 or count fingers @ 2 ft<br />20/4000 or count fingers @ 1 ft<br />20/7777 =CF<br />20/8888 =HM<br />20/9999 =LP<br />20/0000 =NLP'
            });
        });
        $(function() {
            $('#QO3').aToolTip({
                clickIt: true,
                tipContent: '20/800 or count fingers @ 5 ft<br />20/1000 or count fingers @ 4 ft<br />20/1600 or count fingers @ 3ft<br />20/2000 or count fingers @ 2 ft<br />20/4000 or count fingers @ 1 ft<br />20/7777 =CF<br />20/8888 =HM<br />20/9999 =LP<br />20/0000 =NLP'
            });
        });
    </script>

    <script type="text/javascript" src="../../common/js/jquery.metadata.js"></script> <!--when changing defaults-->
    <script type="text/javascript" src="../../common/js/autoNumeric-1.7.5.js"></script>
    <script type="text/javascript" language="javascript">
        $(document).ready(function () {
            /** instruct the metadata plugin where to look the metadata
            * jQuery.metadata.setType( type, name );
            * please read the metadata instructions for additional information
            * http://plugins.jquery.com/project/metadata
            */
            $.metadata.setType('attr', 'meta');

            /** To call autoNumeric
            * $(selector).autoNumeric({options}); 
            * The below example uses the input & class selector
            */
            $('input.auto').autoNumeric();
            /* scripts for metadata code generator  */

            /* rountine that prevents  numeric characters from being entered the the altDec field  */
            $('#altDecb').keypress(function (e) {
                var cc = String.fromCharCode(e.which);
                if (e.which != 32 && cc >= 0 && cc <= 9) {
                    e.preventDefault();
                }
            });

            /* rountine that prevents  apostrophe, comma, more than one period (full stop) or numeric characters from being entered the the aSign field  */
            $('#aSignb').keypress(function (e) {
                var cc = String.fromCharCode(e.which);
                if ((e.which != 32 && cc >= 0 && cc <= 9) || cc == "," || cc == "'" || cc == "." && this.value.lastIndexOf('.') != -1) {
                    e.preventDefault();
                }
            });

            $("input.md").bind('click keyup blur', function () {
                var metaCode = '', aSep = '', dGroup = '', aDec = '', altDec = '', aSign = '', pSign = '', vMin = '', vMax = '', mDec = '', mRound = '', aPad = '', wEmpty = '', aForm = '';
                if ($("input:radio[name=aSep]:checked").attr('id') == 'aSepc') {
                    $('input:radio[name=aDec]:nth(0)').removeAttr("disabled");
                    $('input:radio[name=aDec]:nth(0)').attr('checked', true);
                    $('input:radio[name=aDec]:nth(1)').attr("disabled", true);
                }
                if ($("input:radio[name=aSep]:checked").attr('id') == 'aSepp') {
                    $('input:radio[name=aDec]:nth(1)').removeAttr("disabled");
                    $('input:radio[name=aDec]:nth(1)').attr('checked', true);
                    $('input:radio[name=aDec]:nth(0)').attr("disabled", true);
                }
                if ($("input:radio[name=aSep]:checked").attr('id') != 'aSepc' || $("input:radio[name=aSep]:checked").attr('id') != 'aSepp') {
                    $('input:radio[name=aDec]:nth(0)').removeAttr("disabled");
                    $('input:radio[name=aDec]:nth(1)').removeAttr("disabled");
                }
                aSep = $("input:radio[name=aSep]:checked").val();
                dGroup = $("input:radio[name=dGroup]:checked").val();
                aDec = $("input:radio[name=aDec]:checked").val();

                if ($("input:radio[name=altDec]:checked").attr('id') == 'altDecd') {
                    $('#altDecb').val('');
                    $('#altDecb').attr("disabled", true);
                }
                if ($("input:radio[name=altDec]:checked").attr('id') == 'altDeca') {
                    $('#altDecb').removeAttr("disabled");
                    altDec = $('#altDecb').val();
                }

                if ($("input:radio[name=aSign]:checked").attr('id') == 'aSignd') {
                    $('#aSignb').val('');
                    $('#aSignb').attr("disabled", true);
                }
                if ($("input:radio[name=aSign]:checked").attr('id') == 'aSigna') {
                    $('#aSignb').removeAttr("disabled");
                    aSign = $('#aSignb').val();
                }

                pSign = $("input:radio[name=pSign]:checked").val();
                if ($("input:radio[name=vMin]:checked").attr('id') == 'vMind') {
                    $('#vMinb').val('');
                    $('#vMinb').attr("disabled", true);
                }
                if ($("input:radio[name=vMin]:checked").attr('id') == 'vMina') {
                    $('#vMinb').removeAttr("disabled");
                    vMin = $('#vMinb').val();
                }
                if ($("input:radio[name=vMax]:checked").attr('id') == 'vMaxd') {
                    $('#vMaxb').val('');
                    $('#vMaxb').attr("disabled", true);
                }
                if ($("input:radio[name=vMax]:checked").attr('id') == 'vMaxa') {
                    $('#vMaxb').removeAttr("disabled");
                    vMax = $('#vMaxb').val();
                }
                if ($("input:radio[name=mDec]:checked").attr('id') == 'mDecd') {
                    $('#mDecbb').val('');
                    $('#mDecbb').attr("disabled", true);
                }
                if ($("input:radio[name=mDec]:checked").attr('id') == 'mDeca') {
                    $('#mDecbb').removeAttr("disabled");
                    mDec = $('#mDecbb').val();
                }
                mRound = $("input:radio[name=mRound]:checked").val();
                aPad = $("input:radio[name=aPad]:checked").val();
                wEmpty = $("input:radio[name=wEmpty]:checked").val();
                if (aSep != '') {
                    metaCode = aSep;
                }
                if (dGroup != '') {
                    if (metaCode != '') {
                        metaCode = metaCode + ", " + dGroup;
                    }
                    else {
                        metaCode = dGroup;
                    }
                }
                if (aDec != '') {
                    if (metaCode != '') {
                        metaCode = metaCode + ", " + aDec;
                    }
                    else {
                        metaCode = aDec;
                    }
                }
                if (altDec != '') {
                    if (metaCode != '') {
                        metaCode = metaCode + ", altDec: '" + altDec + "'";
                    }
                    else {
                        metaCode = "altDec: '" + altDec + "'";
                    }
                }
                if (aSign != '') {
                    if (metaCode != '') {
                        metaCode = metaCode + ", aSign: '" + aSign + "'";
                    }
                    else {
                        metaCode = "aSign: '" + aSign + "'";
                    }
                }
                if (pSign != '') {
                    if (metaCode != '') {
                        metaCode = metaCode + ", " + pSign;
                    }
                    else {
                        metaCode = pSign;
                    }
                }
                if (vMin != '') {
                    if (metaCode != '') {
                        metaCode = metaCode + ", vMin: '" + vMin + "'";
                    }
                    else {
                        metaCode = "vMin: '" + vMin + "'";
                    }
                }
                if (vMax != '') {
                    if (metaCode != '') {
                        metaCode = metaCode + ", vMax: '" + vMax + "'";
                    }
                    else {
                        metaCode = "vMax: '" + vMax + "'";
                    }
                }
                if (mDec != '') {
                    if (metaCode != '') {
                        metaCode = metaCode + ", mDec: '" + $('#mDecbb').val() + "'";
                    }
                    else {
                        metaCode = "mDec: '" + $('#mDecbb').val() + "'";
                    }
                }

                if (mRound != '') {
                    if (metaCode != '') {
                        metaCode = metaCode + ", " + mRound;
                    }
                    else {
                        metaCode = mRound;
                    }
                }
                if (aPad != '') {
                    if (metaCode != '') {
                        metaCode = metaCode + ", " + aPad;
                    }
                    else {
                        metaCode = aPad;
                    }
                }
                if (wEmpty != '') {
                    if (metaCode != '') {
                        metaCode = metaCode + ", " + wEmpty;
                    }
                    else {
                        metaCode = wEmpty;
                    }
                }
                $('#metaCode').text('');
                if (metaCode != '') {
                    $('#metaCode').text('meta="{' + metaCode + '}"');
                }
            });

            /* clears the metadata code  */
            $('#rd').click(function () {
                $('#metaCode').text('');
            });
            /* ends scripts for metadata code generator  */

            /* script  for defaults demo  */
            $('#d_noMeta').blur(function () {
                var convertInput = '';
                convertInput = $(this).autoNumericGet();
                $('#d_Get').val(convertInput);
                $('#d_Set').autoNumericSet(convertInput);
            });
            /* end script  for defaults demo  */

            /* script  for various samples demo  */
            $('input[name$="sample"]').blur(function () {
                var convertInput = '';
                var row = 'row_' + this.id.charAt(4);
                convertInput = $(this).autoNumericGet();
                $('#' + row + 'b').val(convertInput);
                $('#' + row + 'c').autoNumericSet(convertInput);
            });
            /* end script  for various samples demo  */

            /* script  for rounding methods  */
            $('#roundValue').blur(function () {
                if (this.value != '') {
                    convertInput = $('#roundValue').autoNumericGet();
                    var i = 1;
                    for (i = 1; i <= 9; i++) {
                        $('#roundMethod' + i).autoNumericSet(convertInput);
                    }
                }
            });

            $('#roundDecimal').change(function () { /* changes decimal places */
                convertInput = $('#roundValue').autoNumericGet();
                if (convertInput > 0) {
                    var i = 1;
                    for (i = 1; i <= 9; i++) {
                        $('#roundMethod' + i).autoNumericSet(convertInput);
                    }
                }
            });
            /* end script  for rounding methods  */

            /* script for dynamically loaded values  demo*/
            $.getJSON("test_JSON.php", function (data) {
                var valueFormatted = '';
                $.each(data, function (key, value) { // loops through JSON keys and returns value	
                    $('#' + key).autoNumericSet(value);
                });
            });
            /* end script for dynamically loaded values demo*/

            /* script for callback demo*/
            $.autoNumeric.get_mDec = function () { /* get_mDec function attached to autoNumeric() */
                var set_mDec = $('#get_metricUnit').val();
                if (set_mDec == ' km') {
                    set_mDec = 3;
                } else {
                    set_mDec = 0;
                }
                return set_mDec; /* set mDec decimal places */
            }

            var get_vMax = function () { /* set the maximum value allowed based on the metric unit */
                var set_vMax = $('#get_metricUnit').val();
                if (set_vMax == ' km') {
                    set_vMax = '99999.999';
                } else {
                    set_vMax = '99999999';
                }
                return set_vMax;
            }

            $('#length').autoNumeric({ vMax: get_vMax }); /* calls autoNumeric and passes function get_vMax */

            $('#get_metricUnit').change(function () {
                var set_value = $('#length').autoNumericGet();
                if (this.value == ' km') {
                    set_value = set_value / 1000;
                } else {
                    set_value = set_value * 1000;
                }
                $('#length').autoNumericSet(set_value);
            });
            /* end script for callback demo*/

            $('<%= TextBoxExamMeanDefectDB.ClientID %>.auto').autoNumeric();
            $('<%= TextBoxOutcomeMeanDefectDB.ClientID %>.auto').autoNumeric();
        });
    </script>
</asp:Content>