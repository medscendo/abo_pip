﻿<%@ Page Title="" Language="C#" MasterPageFile="~/PIMMasterPage.master" AutoEventWireup="true" CodeFile="SNPChart.aspx.cs" Inherits="abo_SNPChart" %>

<asp:Content ID="Content1" ContentPlaceHolderID="head" Runat="Server">
    <script type="text/javascript" language="javascript" src="http://ajax.googleapis.com/ajax/libs/jquery/1.7.1/jquery.min.js"></script>
    <link type="text/css" href="../../common/css/atooltip.css" rel="stylesheet"  media="screen" />
	<script type="text/javascript" src="../../common/js/jquery.atooltip.js"></script>
    <script type="text/javascript" language="javascript">
        function enabledControl(el, disabled, checked) {
            //alert("Hello world from enabled control");
            try {
                if (disabled) {
                    el.disabled = disabled;
                    el.setAttribute("disabled", true);
                }
                else {
                    el.disabled = disabled;
                    el.removeAttribute("disabled");
                }

                if (checked == true)
                    el.checked = false;
            }
            catch (E) {
            }
            if (el.childNodes && el.childNodes.length > 0) {
                for (var x = 0; x < el.childNodes.length; x++) {
                    enabledControl(el.childNodes[x], disabled, checked);
                }
            }
        }
        //      Either enables or disables dropdown boxes
        function enabledControlDropDown(el, disabled, checked) {
            //alert("Hello world from enabled control");
            try {
                if (disabled) {
                    el.disabled = disabled;
                    el.setAttribute("disabled", true);
                }
                else {
                    el.disabled = disabled;
                    el.removeAttribute("disabled");
                }

                if (checked == true)
                    el.options[0].selected = true;
            }
            catch (E) {
            }
            if (el.childNodes && el.childNodes.length > 0) {
                for (var x = 0; x < el.childNodes.length; x++) {
                    enabledControl(el.childNodes[x], disabled, checked);
                }
            }
        }
        //      Either enables or disables text boxes
        function enabledControlTextField(el, disabled, checked) {
            //alert("Hello world from enabled control");
            try {
                if (disabled) {
                    el.disabled = disabled;
                    el.setAttribute("disabled", true);
                }
                else {
                    el.disabled = disabled;
                    el.removeAttribute("disabled");
                }

                if (checked == true) {
                    el.value = '';
                }
            }
            catch (E) {
            }
            if (el.childNodes && el.childNodes.length > 0) {
                for (var x = 0; x < el.childNodes.length; x++) {
                    enabledControl(el.childNodes[x], disabled, checked);
                }
            }
        }
        function toggleDisabled(el, disable) {
            var section = el;
            if (disable) {
                $('#section :input').attr('disabled', true);
                $("#section :input").removeAttr("checked");

                document.getElementById("QE7txta").style.color = "gray";

            }
            else {
                $('#section :input').removeAttr('disabled');
            }
        }

        function generate(arr1, arr2, istrue) {
            if (istrue == true) {
                for (var i = 0; i < arr1.length; i++) {
                    document.getElementById(arr1[i]).style.color = "gray";
                }
                for (var i = 0; i < arr2.length; i++) {
                    $(arr2[i] + ' :input').attr('disabled', true);
                    $(arr2[i] + ' :input').removeAttr("checked");
                }
            }
            if (istrue == false) {
                for (var i = 0; i < arr1.length; i++) {
                    document.getElementById(arr1[i]).style.color = "#333";
                }
                for (var i = 0; i < arr2.length; i++) {
                    $(arr2[i] + ' :input').removeAttr('disabled');
                }
            }
        }
    </script>

    <script  type="text/javascript" language="javascript">
        $(document).ready(function() {

            // Question 4 History **************
            $("#<%= RadioButtonListRBSymptomsAdditionalCranialNeuropathies.ClientID %>").click(function() {
                if (($('#<%= RadioButtonListRBSymptomsAdditionalCranialNeuropathies.ClientID %>').find('input:checked').val() == '2') || ($('#<%= RadioButtonListRBSymptomsAdditionalCranialNeuropathies.ClientID %>').find('input:checked').val() == '3')) {
                    document.getElementById("QH4aA").style.color = "gray";
                    document.getElementById("QH4aB").style.color = "gray";
                    $('#QH4aB :input').attr('disabled', true);
                    $("#QH4aB :input").removeAttr("checked");
                }
            });
            $("#<%= RadioButtonListRBSymptomsAdditionalCranialNeuropathies.ClientID %>").click(function() {
                if ($('#<%= RadioButtonListRBSymptomsAdditionalCranialNeuropathies.ClientID %>').find('input:checked').val() == ('1')) {
                    document.getElementById("QH4aA").style.color = "#333";
                    document.getElementById("QH4aB").style.color = "#333";
                    $('#QH4aB :input').removeAttr('disabled');
                }
            });

            // Question 5 History **************
            $("#<%= RadioButtonListRBSymptomsGeneralNeurologic.ClientID %>").click(function() {
                if (($('#<%= RadioButtonListRBSymptomsGeneralNeurologic.ClientID %>').find('input:checked').val() == '2') || ($('#<%= RadioButtonListRBSymptomsGeneralNeurologic.ClientID %>').find('input:checked').val() == '3')) {
                    document.getElementById("QH5aA").style.color = "gray";
                    document.getElementById("QH5aB").style.color = "gray";
                    $('#QH5aB :input').attr('disabled', true);
                    $("#QH5aB :input").removeAttr("checked");
                }
            });
            $("#<%= RadioButtonListRBSymptomsGeneralNeurologic.ClientID %>").click(function() {
                if ($('#<%= RadioButtonListRBSymptomsGeneralNeurologic.ClientID %>').find('input:checked').val() == ('1')) {
                    document.getElementById("QH5aA").style.color = "#333";
                    document.getElementById("QH5aB").style.color = "#333";
                    $('#QH5aB :input').removeAttr('disabled');
                }
            });

            // Question 6 History **************
            $("#<%= RadioButtonListRBSymptomsVasculopathicRisk.ClientID %>").click(function() {
                if (($('#<%= RadioButtonListRBSymptomsVasculopathicRisk.ClientID %>').find('input:checked').val() == '2') || ($('#<%= RadioButtonListRBSymptomsVasculopathicRisk.ClientID %>').find('input:checked').val() == '3')) {
                    document.getElementById("QH6aA").style.color = "gray";
                    document.getElementById("QH6aB").style.color = "gray";
                    $('#QH6aB :input').attr('disabled', true);
                    $("#QH6aB :input").removeAttr("checked");
                }
            });
            $("#<%= RadioButtonListRBSymptomsVasculopathicRisk.ClientID %>").click(function() {
                if ($('#<%= RadioButtonListRBSymptomsVasculopathicRisk.ClientID %>').find('input:checked').val() == ('1')) {
                    document.getElementById("QH6aA").style.color = "#333";
                    document.getElementById("QH6aB").style.color = "#333";
                    $('#QH6aB :input').removeAttr('disabled');
                    if ($('#<%= CheckBoxCBSymptomsVasculopathicRiskFactorsOther.ClientID %>').attr("checked") == "checked") {
                        enabledControlTextField(document.getElementById("<%= TextBoxSymptomsVasculopathicRiskFactorsOtherText.ClientID %>"), false, false);
                    }
                    else {
                        enabledControlTextField(document.getElementById("<%= TextBoxSymptomsVasculopathicRiskFactorsOtherText.ClientID %>"), true, true);
                    }
                }
            });
            if (($('#<%= RadioButtonListRBSymptomsVasculopathicRisk.ClientID %>').find('input:checked').val() == '2') || ($('#<%= RadioButtonListRBSymptomsVasculopathicRisk.ClientID %>').find('input:checked').val() == '3')) {
                document.getElementById("QH6aA").style.color = "gray";
                document.getElementById("QH6aB").style.color = "gray";
                $('#QH6aB :input').attr('disabled', true);
                $("#QH6aB :input").removeAttr("checked");
            }
            if ($('#<%= RadioButtonListRBSymptomsVasculopathicRisk.ClientID %>').find('input:checked').val() == ('1')) {
                document.getElementById("QH6aA").style.color = "#333";
                document.getElementById("QH6aB").style.color = "#333";
                $('#QH6aB :input').removeAttr('disabled');
                if ($('#<%= CheckBoxCBSymptomsVasculopathicRiskFactorsOther.ClientID %>').attr("checked") == "checked") {
                    enabledControlTextField(document.getElementById("<%= TextBoxSymptomsVasculopathicRiskFactorsOtherText.ClientID %>"), false, false);
                }
                else {
                    enabledControlTextField(document.getElementById("<%= TextBoxSymptomsVasculopathicRiskFactorsOtherText.ClientID %>"), true, true);
                }
            }

            //<!-- Q6 History Other-->
            $("#<%= CheckBoxCBSymptomsVasculopathicRiskFactorsOther.ClientID %>").click(function() {
                if ($('#<%= CheckBoxCBSymptomsVasculopathicRiskFactorsOther.ClientID %>').attr("checked") == "checked") {
                    enabledControlTextField(document.getElementById("<%= TextBoxSymptomsVasculopathicRiskFactorsOtherText.ClientID %>"), false, false);
                }
                else {
                    enabledControlTextField(document.getElementById("<%= TextBoxSymptomsVasculopathicRiskFactorsOtherText.ClientID %>"), true, true);
                }
            });
            if ($('#<%= CheckBoxCBSymptomsVasculopathicRiskFactorsOther.ClientID %>').attr("checked") == "checked") {
                enabledControlTextField(document.getElementById("<%= TextBoxSymptomsVasculopathicRiskFactorsOtherText.ClientID %>"), false, false);
            }
            else {
                enabledControlTextField(document.getElementById("<%= TextBoxSymptomsVasculopathicRiskFactorsOtherText.ClientID %>"), true, true);
            }

            // Question 7 History **************
            $("#<%= RadioButtonListRBHistorySystemicMalignancy.ClientID %>").click(function() {
                if (($('#<%= RadioButtonListRBHistorySystemicMalignancy.ClientID %>').find('input:checked').val() == '2') || ($('#<%= RadioButtonListRBHistorySystemicMalignancy.ClientID %>').find('input:checked').val() == '3')) {
                    document.getElementById("QH7aA").style.color = "gray";
                    document.getElementById("QH7aB").style.color = "gray";
                    $('#QH7aB :input').attr('disabled', true);
                    $("#QH7aB :input").removeAttr("checked");
                    document.getElementById("<%= TextBoxHistoryMalignancy.ClientID %>").value = '';
                }
            });
            $("#<%= RadioButtonListRBHistorySystemicMalignancy.ClientID %>").click(function() {
                if ($('#<%= RadioButtonListRBHistorySystemicMalignancy.ClientID %>').find('input:checked').val() == ('1')) {
                    document.getElementById("QH7aA").style.color = "#333";
                    document.getElementById("QH7aB").style.color = "#333";
                    $('#QH7aB :input').removeAttr('disabled');
                }
            });

            // Question 1 Examination **************
            $("#<%= RadioButtonListRBExamAbnormalityVersions.ClientID %>").click(function() {
                if (($('#<%= RadioButtonListRBExamAbnormalityVersions.ClientID %>').find('input:checked').val() == '2') || ($('#<%= RadioButtonListRBExamAbnormalityVersions.ClientID %>').find('input:checked').val() == '3')) {
                    document.getElementById("QE1aA").style.color = "gray";
                    document.getElementById("QE1aB").style.color = "gray";
                    $('#QE1aB :input').attr('disabled', true);
                    $("#QE1aB :input").removeAttr("checked");
                }
            });
            $("#<%= RadioButtonListRBExamAbnormalityVersions.ClientID %>").click(function() {
                if ($('#<%= RadioButtonListRBExamAbnormalityVersions.ClientID %>').find('input:checked').val() == ('1')) {
                    document.getElementById("QE1aA").style.color = "#333";
                    document.getElementById("QE1aB").style.color = "#333";
                    $('#QE1aB :input').removeAttr('disabled');
                }
            });

            // Question 2 Examination **************
            $("#<%= RadioButtonListRBExamDegreeLimitedVersion.ClientID %>").click(function() {
                if (($('#<%= RadioButtonListRBExamDegreeLimitedVersion.ClientID %>').find('input:checked').val() == '2') || ($('#<%= RadioButtonListRBExamDegreeLimitedVersion.ClientID %>').find('input:checked').val() == '3')) {
                    document.getElementById("QE2aA").style.color = "gray";
                    document.getElementById("QE2aB").style.color = "gray";
                    $('#QE2aB :input').attr('disabled', true);
                    $("#QE2aB :input").removeAttr("checked");
                }
            });
            $("#<%= RadioButtonListRBExamDegreeLimitedVersion.ClientID %>").click(function() {
                if ($('#<%= RadioButtonListRBExamDegreeLimitedVersion.ClientID %>').find('input:checked').val() == ('1')) {
                    document.getElementById("QE2aA").style.color = "#333";
                    document.getElementById("QE2aB").style.color = "#333";
                    $('#QE2aB :input').removeAttr('disabled');
                }
            });

            // Question 4 Examination **************
            $("#<%= RadioButtonListRBExamPupillaryAbnormality.ClientID %>").click(function() {
                if (($('#<%= RadioButtonListRBExamPupillaryAbnormality.ClientID %>').find('input:checked').val() == '2') || ($('#<%= RadioButtonListRBExamPupillaryAbnormality.ClientID %>').find('input:checked').val() == '3')) {
                    document.getElementById("QE4aA").style.color = "gray";
                    document.getElementById("QE4aB").style.color = "gray";
                    $('#QE4aB :input').attr('disabled', true);
                    $("#QE4aB :input").removeAttr("checked");
                }
            });
            $("#<%= RadioButtonListRBExamPupillaryAbnormality.ClientID %>").click(function() {
                if ($('#<%= RadioButtonListRBExamPupillaryAbnormality.ClientID %>').find('input:checked').val() == ('1')) {
                    document.getElementById("QE4aA").style.color = "#333";
                    document.getElementById("QE4aB").style.color = "#333";
                    $('#QE4aB :input').removeAttr('disabled');
                }
            });

            // Question 6 Examination **************
            $("#<%= RadioButtonListRBExamCranialNerveAbnormality.ClientID %>").click(function() {
                if (($('#<%= RadioButtonListRBExamCranialNerveAbnormality.ClientID %>').find('input:checked').val() == '2') || ($('#<%= RadioButtonListRBExamCranialNerveAbnormality.ClientID %>').find('input:checked').val() == '3')) {
                    document.getElementById("QE6aA").style.color = "gray";
                    document.getElementById("QE6aB").style.color = "gray";
                    $('#QE6aB :input').attr('disabled', true);
                    $("#QE6aB :input").removeAttr("checked");
                }
            });
            $("#<%= RadioButtonListRBExamCranialNerveAbnormality.ClientID %>").click(function() {
                if ($('#<%= RadioButtonListRBExamCranialNerveAbnormality.ClientID %>').find('input:checked').val() == ('1')) {
                    document.getElementById("QE6aA").style.color = "#333";
                    document.getElementById("QE6aB").style.color = "#333";
                    $('#QE6aB :input').removeAttr('disabled');
                }
            });

            // Question 7 Examination **************
            $("#<%= RadioButtonListRBExamOpticDiscDescribed.ClientID %>").click(function() {
                if (($('#<%= RadioButtonListRBExamOpticDiscDescribed.ClientID %>').find('input:checked').val() == '2') || ($('#<%= RadioButtonListRBExamOpticDiscDescribed.ClientID %>').find('input:checked').val() == '3')) {
                    document.getElementById("QE7aA").style.color = "gray";
                    document.getElementById("QE7aB").style.color = "gray";
                    $('#QE7aB :input').attr('disabled', true);
                    $("#QE7aB :input").removeAttr("checked");
                }
            });
            $("#<%= RadioButtonListRBExamOpticDiscDescribed.ClientID %>").click(function() {
                if ($('#<%= RadioButtonListRBExamOpticDiscDescribed.ClientID %>').find('input:checked').val() == ('1')) {
                    document.getElementById("QE7aA").style.color = "#333";
                    document.getElementById("QE7aB").style.color = "#333";
                    $('#QE7aB :input').removeAttr('disabled');
                }
            });

            // Question 9 Examination **************
            $("#<%= RadioButtonListRBExamNeuroimagingStudy.ClientID %>").click(function() {
                if (($('#<%= RadioButtonListRBExamNeuroimagingStudy.ClientID %>').find('input:checked').val() == '2') || ($('#<%= RadioButtonListRBExamNeuroimagingStudy.ClientID %>').find('input:checked').val() == '3')) {
                    document.getElementById("QE9aA").style.color = "gray";
                    document.getElementById("QE9aB").style.color = "gray";
                    $('#QE9aB :input').attr('disabled', true);
                    $("#QE9aB :input").removeAttr("checked");
                }
            });
            $("#<%= RadioButtonListRBExamNeuroimagingStudy.ClientID %>").click(function() {
                if ($('#<%= RadioButtonListRBExamNeuroimagingStudy.ClientID %>').find('input:checked').val() == ('1')) {
                    document.getElementById("QE9aA").style.color = "#333";
                    document.getElementById("QE9aB").style.color = "#333";
                    $('#QE9aB :input').removeAttr('disabled');
                }
            });

            // Question 1 Management **************
            $("#<%= RadioButtonListRBManagementDiplopia.ClientID %>").click(function() {
                if (($('#<%= RadioButtonListRBManagementDiplopia.ClientID %>').find('input:checked').val() == '2') || ($('#<%= RadioButtonListRBManagementDiplopia.ClientID %>').find('input:checked').val() == '3')) {
                    document.getElementById("QM1aA").style.color = "gray";
                    document.getElementById("QM1aB").style.color = "gray";
                    $('#QM1aB :input').attr('disabled', true);
                    $("#QM1aB :input").removeAttr("checked");
                }
            });
            $("#<%= RadioButtonListRBManagementDiplopia.ClientID %>").click(function() {
                if ($('#<%= RadioButtonListRBManagementDiplopia.ClientID %>').find('input:checked').val() == ('1')) {
                    document.getElementById("QM1aA").style.color = "#333";
                    document.getElementById("QM1aB").style.color = "#333";
                    $('#QM1aB :input').removeAttr('disabled');
                }
            });

            // Question 2 Outcomes **************
            $("#<%= RadioButtonListRBOutcomeFollowUpExam.ClientID %>").click(function() {
                if ($('#<%= RadioButtonListRBOutcomeFollowUpExam.ClientID %>').find('input:checked').val() == ('1')) {
                    var myaray = new Array("QO2aA", "QO2aB");
                    var myarray2 = new Array("#QO2aB");
                    generate(myaray, myarray2, false);
                }
                else {
                    var myaray = new Array("QO2aA", "QO2aB", "QO2bA", "QO2bB");
                    var myarray2 = new Array("#QO2aB", "#QO2bB");
                    generate(myaray, myarray2, true);
                }
            });
            if ($('#<%= RadioButtonListRBOutcomeFollowUpExam.ClientID %>').find('input:checked').val() == ('1')) {
                var myaray = new Array("QO2aA", "QO2aB");
                var myarray2 = new Array("#QO2aB");
                generate(myaray, myarray2, false);
            }
            else {
                var myaray = new Array("QO2aA", "QO2aB", "QO2bA", "QO2bB");
                var myarray2 = new Array("#QO2aB", "#QO2bB");
                generate(myaray, myarray2, true);
            }

            // Question 2a Outcomes **************
            $("#<%= RadioButtonListRBOutcomeChangesOcularAlignment.ClientID %>").click(function() {
                if ($('#<%= RadioButtonListRBOutcomeChangesOcularAlignment.ClientID %>').find('input:checked').val() == ('1')) {
                    var myaray = new Array("QO2bA", "QO2bB");
                    var myarray2 = new Array("#QO2bB");
                    generate(myaray, myarray2, false);
                }
                else {
                    var myaray = new Array("QO2bA", "QO2bB");
                    var myarray2 = new Array("#QO2bB");
                    generate(myaray, myarray2, true);
                }
            });
            if ($('#<%= RadioButtonListRBOutcomeChangesOcularAlignment.ClientID %>').find('input:checked').val() == ('1')) {
                var myaray = new Array("QO2bA", "QO2bB");
                var myarray2 = new Array("#QO2bB");
                generate(myaray, myarray2, false);
            }
            else {
                var myaray = new Array("QO2bA", "QO2bB");
                var myarray2 = new Array("#QO2bB");
                generate(myaray, myarray2, true);
            }

            // Question 4 Outcomes **************
            $("#<%= RadioButtonListRBOutcomeFinalEtiology.ClientID %>").click(function() {
                if (($('#<%= RadioButtonListRBOutcomeFinalEtiology.ClientID %>').find('input:checked').val() == '2') || ($('#<%= RadioButtonListRBOutcomeFinalEtiology.ClientID %>').find('input:checked').val() == '3')) {
                    document.getElementById("QO4aA").style.color = "gray";
                    document.getElementById("QO4aB").style.color = "gray";
                    $('#QO4aB :input').attr('disabled', true);
                    $("#QO4aB :input").removeAttr("checked");
                    document.getElementById("<%= TextBoxOutcomeEtiology.ClientID %>").value = '';
                }
            });
            $("#<%= RadioButtonListRBOutcomeFinalEtiology.ClientID %>").click(function() {
                if ($('#<%= RadioButtonListRBOutcomeFinalEtiology.ClientID %>').find('input:checked').val() == ('1')) {
                    document.getElementById("QO4aA").style.color = "#333";
                    document.getElementById("QO4aB").style.color = "#333";
                    $('#QO4aB :input').removeAttr('disabled');
                }
            });

            if ($('#<%= RadioButtonListRBOutcomeFinalEtiology.ClientID %>').find('input:checked').val() == ('1')) {
                document.getElementById("QO4aA").style.color = "#333";
                document.getElementById("QO4aB").style.color = "#333";
                $('#QO4aB :input').removeAttr('disabled');
            }


            if (($('#<%= RadioButtonListRBSymptomsAdditionalCranialNeuropathies.ClientID %>').find('input:checked').val() == '2') || ($('#<%= RadioButtonListRBSymptomsAdditionalCranialNeuropathies.ClientID %>').find('input:checked').val() == '3')) {
                document.getElementById("QH4aA").style.color = "gray";
                document.getElementById("QH4aB").style.color = "gray";
                $('#QH4aB :input').attr('disabled', true);
                $("#QH4aB :input").removeAttr("checked");
            }

            if ($('#<%= RadioButtonListRBSymptomsAdditionalCranialNeuropathies.ClientID %>').find('input:checked').val() == ('1')) {
                document.getElementById("QH4aA").style.color = "#333";
                document.getElementById("QH4aB").style.color = "#333";
                $('#QH4aB :input').removeAttr('disabled');
            }

            if (($('#<%= RadioButtonListRBSymptomsGeneralNeurologic.ClientID %>').find('input:checked').val() == '2') || ($('#<%= RadioButtonListRBSymptomsGeneralNeurologic.ClientID %>').find('input:checked').val() == '3')) {
                document.getElementById("QH5aA").style.color = "gray";
                document.getElementById("QH5aB").style.color = "gray";
                $('#QH5aB :input').attr('disabled', true);
                $("#QH5aB :input").removeAttr("checked");
            }

            if ($('#<%= RadioButtonListRBSymptomsGeneralNeurologic.ClientID %>').find('input:checked').val() == ('1')) {
                document.getElementById("QH5aA").style.color = "#333";
                document.getElementById("QH5aB").style.color = "#333";
                $('#QH5aB :input').removeAttr('disabled');
            }

            /*if (($('#<%= RadioButtonListRBSymptomsVasculopathicRisk.ClientID %>').find('input:checked').val() == '2') || ($('#<%= RadioButtonListRBSymptomsVasculopathicRisk.ClientID %>').find('input:checked').val() == '3')) {
            document.getElementById("QH6aA").style.color = "gray";
            document.getElementById("QH6aB").style.color = "gray";
            $('#QH6aB :input').attr('disabled', true);
            $("#QH6aB :input").removeAttr("checked");
            }

            if ($('#<%= RadioButtonListRBSymptomsVasculopathicRisk.ClientID %>').find('input:checked').val() == ('1')) {
            document.getElementById("QH6aA").style.color = "#333";
            document.getElementById("QH6aB").style.color = "#333";
            $('#QH6aB :input').removeAttr('disabled');
            }*/

            if (($('#<%= RadioButtonListRBHistorySystemicMalignancy.ClientID %>').find('input:checked').val() == '2') || ($('#<%= RadioButtonListRBHistorySystemicMalignancy.ClientID %>').find('input:checked').val() == '3')) {
                document.getElementById("QH7aA").style.color = "gray";
                document.getElementById("QH7aB").style.color = "gray";
                $('#QH7aB :input').attr('disabled', true);
                $("#QH7aB :input").removeAttr("checked");
                document.getElementById("<%= TextBoxHistoryMalignancy.ClientID %>").value = '';
            }

            if ($('#<%= RadioButtonListRBHistorySystemicMalignancy.ClientID %>').find('input:checked').val() == ('1')) {
                document.getElementById("QH7aA").style.color = "#333";
                document.getElementById("QH7aB").style.color = "#333";
                $('#QH7aB :input').removeAttr('disabled');
            }

            if (($('#<%= RadioButtonListRBExamAbnormalityVersions.ClientID %>').find('input:checked').val() == '2') || ($('#<%= RadioButtonListRBExamAbnormalityVersions.ClientID %>').find('input:checked').val() == '3')) {
                document.getElementById("QE1aA").style.color = "gray";
                document.getElementById("QE1aB").style.color = "gray";
                $('#QE1aB :input').attr('disabled', true);
                $("#QE1aB :input").removeAttr("checked");
            }

            if ($('#<%= RadioButtonListRBExamAbnormalityVersions.ClientID %>').find('input:checked').val() == ('1')) {
                document.getElementById("QE1aA").style.color = "#333";
                document.getElementById("QE1aB").style.color = "#333";
                $('#QE1aB :input').removeAttr('disabled');
            }

            if (($('#<%= RadioButtonListRBExamDegreeLimitedVersion.ClientID %>').find('input:checked').val() == '2') || ($('#<%= RadioButtonListRBExamDegreeLimitedVersion.ClientID %>').find('input:checked').val() == '3')) {
                document.getElementById("QE2aA").style.color = "gray";
                document.getElementById("QE2aB").style.color = "gray";
                $('#QE2aB :input').attr('disabled', true);
                $("#QE2aB :input").removeAttr("checked");
            }

            if ($('#<%= RadioButtonListRBExamDegreeLimitedVersion.ClientID %>').find('input:checked').val() == ('1')) {
                document.getElementById("QE2aA").style.color = "#333";
                document.getElementById("QE2aB").style.color = "#333";
                $('#QE2aB :input').removeAttr('disabled');
            }

            if (($('#<%= RadioButtonListRBExamPupillaryAbnormality.ClientID %>').find('input:checked').val() == '2') || ($('#<%= RadioButtonListRBExamPupillaryAbnormality.ClientID %>').find('input:checked').val() == '3')) {
                document.getElementById("QE4aA").style.color = "gray";
                document.getElementById("QE4aB").style.color = "gray";
                $('#QE4aB :input').attr('disabled', true);
                $("#QE4aB :input").removeAttr("checked");
            }

            if ($('#<%= RadioButtonListRBExamPupillaryAbnormality.ClientID %>').find('input:checked').val() == ('1')) {
                document.getElementById("QE4aA").style.color = "#333";
                document.getElementById("QE4aB").style.color = "#333";
                $('#QE4aB :input').removeAttr('disabled');
            }

            if (($('#<%= RadioButtonListRBExamCranialNerveAbnormality.ClientID %>').find('input:checked').val() == '2') || ($('#<%= RadioButtonListRBExamCranialNerveAbnormality.ClientID %>').find('input:checked').val() == '3')) {
                document.getElementById("QE6aA").style.color = "gray";
                document.getElementById("QE6aB").style.color = "gray";
                $('#QE6aB :input').attr('disabled', true);
                $("#QE6aB :input").removeAttr("checked");
            }

            if ($('#<%= RadioButtonListRBExamCranialNerveAbnormality.ClientID %>').find('input:checked').val() == ('1')) {
                document.getElementById("QE6aA").style.color = "#333";
                document.getElementById("QE6aB").style.color = "#333";
                $('#QE6aB :input').removeAttr('disabled');
            }

            if (($('#<%= RadioButtonListRBExamOpticDiscDescribed.ClientID %>').find('input:checked').val() == '2') || ($('#<%= RadioButtonListRBExamOpticDiscDescribed.ClientID %>').find('input:checked').val() == '3')) {
                document.getElementById("QE7aA").style.color = "gray";
                document.getElementById("QE7aB").style.color = "gray";
                $('#QE7aB :input').attr('disabled', true);
                $("#QE7aB :input").removeAttr("checked");
            }

            if ($('#<%= RadioButtonListRBExamOpticDiscDescribed.ClientID %>').find('input:checked').val() == ('1')) {
                document.getElementById("QE7aA").style.color = "#333";
                document.getElementById("QE7aB").style.color = "#333";
                $('#QE7aB :input').removeAttr('disabled');
            }

            if (($('#<%= RadioButtonListRBExamNeuroimagingStudy.ClientID %>').find('input:checked').val() == '2') || ($('#<%= RadioButtonListRBExamNeuroimagingStudy.ClientID %>').find('input:checked').val() == '3')) {
                document.getElementById("QE9aA").style.color = "gray";
                document.getElementById("QE9aB").style.color = "gray";
                $('#QE9aB :input').attr('disabled', true);
                $("#QE9aB :input").removeAttr("checked");
            }

            if ($('#<%= RadioButtonListRBExamNeuroimagingStudy.ClientID %>').find('input:checked').val() == ('1')) {
                document.getElementById("QE9aA").style.color = "#333";
                document.getElementById("QE9aB").style.color = "#333";
                $('#QE9aB :input').removeAttr('disabled');
            }

            if (($('#<%= RadioButtonListRBManagementDiplopia.ClientID %>').find('input:checked').val() == '2') || ($('#<%= RadioButtonListRBManagementDiplopia.ClientID %>').find('input:checked').val() == '3')) {
                document.getElementById("QM1aA").style.color = "gray";
                document.getElementById("QM1aB").style.color = "gray";
                $('#QM1aB :input').attr('disabled', true);
                $("#QM1aB :input").removeAttr("checked");
            }

            if ($('#<%= RadioButtonListRBManagementDiplopia.ClientID %>').find('input:checked').val() == ('1')) {
                document.getElementById("QM1aA").style.color = "#333";
                document.getElementById("QM1aB").style.color = "#333";
                $('#QM1aB :input').removeAttr('disabled');
            }

            /*if (($('#<%= RadioButtonListRBOutcomeFollowUpExam.ClientID %>').find('input:checked').val() == '2') || ($('#<%= RadioButtonListRBOutcomeFollowUpExam.ClientID %>').find('input:checked').val() == '3')) {
            document.getElementById("QO2aA").style.color = "gray";
            document.getElementById("QO2aB").style.color = "gray";
            $('#QO2aB :input').attr('disabled', true);
            $("#QO2aB :input").removeAttr("checked");
            document.getElementById("QO2bA").style.color = "gray";
            document.getElementById("QO2bB").style.color = "gray";
            $('#QO2bB :input').attr('disabled', true);
            $("#QO2bB :input").removeAttr("checked");
            }

            if ($('#<%= RadioButtonListRBOutcomeFollowUpExam.ClientID %>').find('input:checked').val() == ('1')) {
            document.getElementById("QO2aA").style.color = "#333";
            document.getElementById("QO2aB").style.color = "#333";
            $('#QO2aB :input').removeAttr('disabled');
            document.getElementById("QO2bA").style.color = "#333";
            document.getElementById("QO2bB").style.color = "#333";
            $('#QO2bB :input').removeAttr('disabled');
            }*/

            if (($('#<%= RadioButtonListRBOutcomeFinalEtiology.ClientID %>').find('input:checked').val() == '2') || ($('#<%= RadioButtonListRBOutcomeFinalEtiology.ClientID %>').find('input:checked').val() == '3')) {
                document.getElementById("QO4aA").style.color = "gray";
                document.getElementById("QO4aB").style.color = "gray";
                $('#QO4aB :input').attr('disabled', true);
                $("#QO4aB :input").removeAttr("checked");
                document.getElementById("<%= TextBoxOutcomeEtiology.ClientID %>").value = '';
            }

            if ($('#<%= RadioButtonListRBOutcomeFinalEtiology.ClientID %>').find('input:checked').val() == ('1')) {
                document.getElementById("QO4aA").style.color = "#333";
                document.getElementById("QO4aB").style.color = "#333";
                $('#QO4aB :input').removeAttr('disabled');
            }

            //<!-- Q7 History -->
            $("#<%= RadioButtonListRBSymptomsGiantCellArteritis.ClientID %>").click(function() {
                if ($('#<%= RadioButtonListRBSymptomsGiantCellArteritis.ClientID %>').find('input:checked').val() == ('1')) {
                    var myaray = new Array("NewQH7aA", "NewQH7aB");
                    var myarray2 = new Array("#NewQH7aB");
                    generate(myaray, myarray2, false);
                    if ($('#<%= CheckBoxSymptomsGiantCellOther.ClientID %>').attr("checked") == "checked") {
                        enabledControlTextField(document.getElementById("<%= TextBoxSymptomsGiantCellArteritisOtherText.ClientID %>"), false, false);
                    }
                    else {
                        enabledControlTextField(document.getElementById("<%= TextBoxSymptomsGiantCellArteritisOtherText.ClientID %>"), true, true);
                    }
                }
                else {
                    var myaray = new Array("NewQH7aA", "NewQH7aB");
                    var myarray2 = new Array("#NewQH7aB");
                    generate(myaray, myarray2, true);

                    enabledControlTextField(document.getElementById("<%= TextBoxSymptomsGiantCellArteritisOtherText.ClientID %>"), true, true);
                }
            });
            if ($('#<%= RadioButtonListRBSymptomsGiantCellArteritis.ClientID %>').find('input:checked').val() == ('1')) {
                var myaray = new Array("NewQH7aA", "NewQH7aB");
                var myarray2 = new Array("#NewQH7aB");
                generate(myaray, myarray2, false);
                if ($('#<%= CheckBoxSymptomsGiantCellOther.ClientID %>').attr("checked") == "checked") {
                    enabledControlTextField(document.getElementById("<%= TextBoxSymptomsGiantCellArteritisOtherText.ClientID %>"), false, false);
                }
                else {
                    enabledControlTextField(document.getElementById("<%= TextBoxSymptomsGiantCellArteritisOtherText.ClientID %>"), true, true);
                }
            }
            else {
                var myaray = new Array("NewQH7aA", "NewQH7aB");
                var myarray2 = new Array("#NewQH7aB");
                generate(myaray, myarray2, true);

                enabledControlTextField(document.getElementById("<%= TextBoxSymptomsGiantCellArteritisOtherText.ClientID %>"), true, true);
            }

            //<!-- Q7 History Other-->
            $("#<%= CheckBoxSymptomsGiantCellOther.ClientID %>").click(function() {
                if ($('#<%= CheckBoxSymptomsGiantCellOther.ClientID %>').attr("checked") == "checked") {
                    enabledControlTextField(document.getElementById("<%= TextBoxSymptomsGiantCellArteritisOtherText.ClientID %>"), false, false);
                }
                else {
                    enabledControlTextField(document.getElementById("<%= TextBoxSymptomsGiantCellArteritisOtherText.ClientID %>"), true, true);
                }
            });
            if ($('#<%= CheckBoxSymptomsGiantCellOther.ClientID %>').attr("checked") == "checked") {
                enabledControlTextField(document.getElementById("<%= TextBoxSymptomsGiantCellArteritisOtherText.ClientID %>"), false, false);
            }
            else {
                enabledControlTextField(document.getElementById("<%= TextBoxSymptomsGiantCellArteritisOtherText.ClientID %>"), true, true);
            }


            //<!-- Q4 -->
            $("#<%= RadioButtonListRBExamImcomitanceNoted.ClientID %>").click(function() {
                if ($('#<%= RadioButtonListRBExamImcomitanceNoted.ClientID %>').find('input:checked').val() == ('1')) {
                    var myaray = new Array("NewQ4aA", "NewQ4aB");
                    var myarray2 = new Array("#NewQ4aB");
                    generate(myaray, myarray2, false);
                }
                else {
                    var myaray = new Array("NewQ4aA", "NewQ4aB", "NewQ4bA");
                    var myarray2 = new Array("#NewQ4aB");
                    generate(myaray, myarray2, true);

                    enabledControlTextField(document.getElementById("<%= TextBoxExamIncomitanceOther.ClientID %>"), true, true);
                }
            });
            if ($('#<%= RadioButtonListRBExamImcomitanceNoted.ClientID %>').find('input:checked').val() == ('1')) {
                var myaray = new Array("NewQ4aA", "NewQ4aB");
                var myarray2 = new Array("#NewQ4aB");
                generate(myaray, myarray2, false);
            }
            else {
                var myaray = new Array("NewQ4aA", "NewQ4aB", "NewQ4bA");
                var myarray2 = new Array("#NewQ4aB");
                generate(myaray, myarray2, true);

                enabledControlTextField(document.getElementById("<%= TextBoxExamIncomitanceOther.ClientID %>"), true, true);
            }

            //<!-- Q4b Examination Other -->
            $("#<%= RadioButtonListRBExamIncomitance.ClientID %>").click(function() {
                if ($('#<%= RadioButtonListRBExamIncomitance.ClientID %>').find('input:checked').val() == ('14')) {
                    var myaray = new Array("NewQ4bA");
                    var myarray2 = new Array("NewQ4bA");
                    generate(myaray, myarray2, false);

                    enabledControlTextField(document.getElementById("<%= TextBoxExamIncomitanceOther.ClientID %>"), false, false);
                }
                else {
                    var myaray = new Array("NewQ4bA");
                    var myarray2 = new Array("#NewQ4bA");
                    generate(myaray, myarray2, true);

                    enabledControlTextField(document.getElementById("<%= TextBoxExamIncomitanceOther.ClientID %>"), true, true);
                }
            });

            if ($('#<%= RadioButtonListRBExamIncomitance.ClientID %>').find('input:checked').val() == ('14')) {
                var myaray = new Array("NewQ4bA");
                var myarray2 = new Array("#NewQ4bA");
                generate(myaray, myarray2, false);

                enabledControlTextField(document.getElementById("<%= TextBoxExamIncomitanceOther.ClientID %>"), false, false);
            }
            else {
                var myaray = new Array("NewQ4bA");
                var myarray2 = new Array("#NewQ4bA");
                generate(myaray, myarray2, true);

                enabledControlTextField(document.getElementById("<%= TextBoxExamIncomitanceOther.ClientID %>"), true, true);
            }
        });
    </script>

    <style type="text/css">
        .bginputa{
	        float:right;
	        margin:21px 0 0 1px;
	        background: url(../common/images/orange_button_with_arrow.png) no-repeat;
	        overflow:hidden;
	        height:35px;

        }
        .bginputa a{
	        color: white;
	        text-align:center;
	        height:35px;
	        line-height:28px;
	        padding:0 14px 15px  ;
	        cursor:pointer;
	        float:left;
	        border:none;
	        text-decoration:none;
	        font-family:Arial, Helvetica, sans-serif; 
	        font-size:12px; 
	        font-weight:bold; 
	        letter-spacing: 0px;
        }
        .bginputa a:hover{
	        text-decoration:none;
        }
        .style1
        {
            width: 67%;
        }
    </style>
    <script type="text/javascript">
        $(function() {
            $('#QE2a').aToolTip({
                clickIt: true,
                tipContent: 'Answer in degrees'
            });
        });
    </script>
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder1" Runat="Server">
    <asp:HiddenField ID="HiddenFieldRecordIdentifier" runat="server"/>
        <!-- Esotropia pim -->
        <div class="pim">
            <div class="record-ident clearfix">
                <h3 class="record-first">RECORD IDENTIFIER: <asp:Literal runat="server" ID="LiteralRecordIdentifier"></asp:Literal></h3>
                <h3 class="record-second"><asp:Literal runat="server" ID="LiteralAbstractionNumber"></asp:Literal></h3>
            </div>
            <!-- History Section -->
            <table>
                <tr>
                    <th colspan="2" style="width: 910px;">
                        <p>History</p>
                    </th>
                </tr>
                <!-- Question 1 -->
                <tr class="table_body">
                    <td width="70%"><strong>1. Date of birth:</strong><br />
                    <asp:Label ID="LabelMonthOfBirth" runat="server" Visible="false"  ForeColor="Red" Text="Please enter Month  "></asp:Label><br />
                    <asp:Label ID="LabelYearOfBirth" runat="server" Visible="false"  ForeColor="Red"  Text="Please enter  Year "></asp:Label><br />
               
               </td>
                    <td>
                        <asp:DropDownList ID="DropDownListMonthOfBirth" DataValueField="MonthID" DataTextField="MonthName" runat="server" />
                        <asp:DropDownList ID="DropDownListYearOfBirth" DataValueField="YearID" DataTextField="YearName" runat="server" />
                    </td>
                </tr>
                <!-- Question 2 -->
                <tr class="table_body_bg">
                    <td><strong>2. Date of first exam:</strong><br />
                    <asp:Label ID="LabelMonthOfFirstExam" runat="server" Visible="false"  ForeColor="Red" Text="Please enter Month "></asp:Label><br />
                    <asp:Label ID="LabelYearOfFirstExam" runat="server" Visible="false"  ForeColor="Red"  Text="Please enter  Year "></asp:Label>
                        <br />
                    <asp:Label ID="Labelinitialage" runat="server" Visible="false"  
                            ForeColor="Red"  Text=" "></asp:Label><br />
                    </td>
                    <td>
                        <asp:DropDownList ID="DropDownListMonthOfFirstExam" DataValueField="MonthID" DataTextField="MonthName" runat="server" />
                        <asp:DropDownList ID="DropDownListYearOfFirstExam" DataValueField="YearID" DataTextField="YearName" runat="server" />
                    </td>
                </tr>
                <!-- Question 3 -->
                <tr class="table_body">
                    <td><strong>3. What was the patient's visual complaint?</strong>
                    <br />
                    <asp:Label ID="LabelRBHistoryVisualComplaint" runat="server" Visible="false"  ForeColor="Red" Text="Please complete"></asp:Label>
              
                    </td>
                    <td>
                        <asp:RadioButtonList id="RadioButtonListRBHistoryVisualComplaint" RepeatDirection="Vertical" CssClass="aspxList" style="border: 0px;" runat="server">
                            <asp:ListItem Value="12">&nbsp;Blurred Vision</asp:ListItem>
                            <asp:ListItem Value="13">&nbsp;Diplopia</asp:ListItem>
                            <asp:ListItem Value="14">&nbsp;Other</asp:ListItem>
                        </asp:RadioButtonList>
                    </td>
                </tr>
                <!-- Question 4 -->
                <tr class="table_body_bg">
                    <td>
                        <strong>4. Were symptoms of additional cranial neuropathies reported?</strong>
                        <br />
                        <asp:Label ID="LabelRBSymptomsAdditionalCranialNeuropathies" runat="server" Visible="false"  ForeColor="Red" Text="Please complete"></asp:Label>
                    </td>
                    <td>
                        <asp:RadioButtonList id="RadioButtonListRBSymptomsAdditionalCranialNeuropathies" RepeatDirection="Horizontal" CssClass="aspxList" runat="server">
                            <asp:ListItem Value="1">&nbsp;Yes</asp:ListItem>
                            <asp:ListItem Value="2">&nbsp;No</asp:ListItem>
                            <asp:ListItem Value="3">&nbsp;Not Recorded</asp:ListItem>
                        </asp:RadioButtonList>
                    </td>
                </tr>
                <!-- Question 4a -->
                <tr class="table_body_bg">
                    <td>
                        <span id="QH4aA">
                        <strong>4a. If so, which were identified?</strong><br />
                        <asp:Label ID="LabelSymptomsFacialNumbness" runat="server" Visible="false"  ForeColor="Red" Text="Please complete"></asp:Label>
                        </span>
                    </td>
                    <td>
                        <span id="QH4aB">
                        <asp:CheckBox runat="server" ID="CheckBoxSymptomsFacialNumbness" class="check" text="&nbsp;Facial Numbness" />
                        <br /><asp:CheckBox runat="server" ID="CheckBoxSymptomsFacialWeakness" class="check" text="&nbsp;Facial Weakness" />
                        <br /><asp:CheckBox runat="server" ID="CheckBoxSymptomsVerticalDiplopia" class="check" text="&nbsp;Vertical Diplopia" />
                        <br /><asp:CheckBox runat="server" ID="CheckBoxSymptomsPtosis" class="check" text="&nbsp;Ptosis" />
                        <br /><asp:CheckBox runat="server" ID="CheckBoxSymptomsOther" class="check" text="&nbsp;Other" />
                        <br /><asp:CheckBox runat="server" ID="CheckBoxSymptomsNone" class="check" text="&nbsp;None" />
                        </span>
                    </td>
                </tr>
                <!-- Question 5 -->
                <tr class="table_body">
                    <td><strong>5. Were associated general neurologic symptoms reported?</strong><br />
                        <asp:Label ID="LabelRBSymptomsGeneralNeurologic" runat="server" Visible="false"  ForeColor="Red" Text="Please complete"></asp:Label>
                    </td>
                    <td>
                        <asp:RadioButtonList id="RadioButtonListRBSymptomsGeneralNeurologic" RepeatDirection="Horizontal" CssClass="aspxList" runat="server">
                            <asp:ListItem Value="1">&nbsp;Yes</asp:ListItem>
                            <asp:ListItem Value="2">&nbsp;No</asp:ListItem>
                            <asp:ListItem Value="3">&nbsp;Not Recorded</asp:ListItem>
                        </asp:RadioButtonList>
                    </td>
                </tr>
                <!-- Question 5a -->
                <tr class="table_body">
                    <td><span id="QH5aA"><strong>5a. If so, which symptoms were identified?</strong> <br /></span>
                        <asp:Label ID="LabelSymptoms" runat="server" Visible="false"  ForeColor="Red" Text="Please complete"></asp:Label>
                    </td>
                    <td>
                        <span id="QH5aB">
                        <asp:Checkbox  ID="CheckboxSymptomsDyspnea" runat="server" GroupName="Symptoms" Text="&nbsp;Dyspnea" />
                        <br /><asp:Checkbox ID="CheckboxSymptomsDysphagia" runat="server" GroupName="Symptoms" Text="&nbsp;Dysphagia" />
                        <br /><asp:Checkbox  ID="CheckboxSymptomsGeneralized" runat="server" GroupName="Symptoms" Text="&nbsp;Generalized or Extremity Weakness" />
                        <br /><asp:Checkbox  ID="CheckboxSymptomsOther1" runat="server" GroupName="Symptoms" Text="&nbsp;Other" />
                        <br /><asp:Checkbox  ID="CheckboxSymptomsNone1" runat="server" GroupName="Symptoms" Text="&nbsp;None" />
                        </span>
                    </td>
                </tr>
                <!-- Question 6 -->
                <tr class="table_body_bg">
                    <td><strong>6. Were potential vasculopathic risk factors reported?</strong><br />
                        <asp:Label ID="LabelRBSymptomsVasculopathicRisk" runat="server" Visible="false"  ForeColor="Red" Text="Please complete"></asp:Label>
                    </td>
                    <td>
                        <asp:RadioButtonList id="RadioButtonListRBSymptomsVasculopathicRisk" RepeatDirection="Horizontal" CssClass="aspxList" runat="server">
                            <asp:ListItem Value="1">&nbsp;Yes</asp:ListItem>
                            <asp:ListItem Value="2">&nbsp;No</asp:ListItem>
                            <asp:ListItem Value="3">&nbsp;Not&nbsp;Recorded</asp:ListItem>
                        </asp:RadioButtonList>
                    </td>
                </tr>
                <!-- Question 6a -->
                <tr class="table_body_bg">
                    <td><span id="QH6aA"><strong>6a. If so, which were identified?</strong></span><br />
                        <asp:Label ID="LabelRBSymptomsVasculopathicRiskFactors" runat="server" Visible="false"  ForeColor="Red" Text="Please complete"></asp:Label>
                    </td>
                    <td>
                        <span id="QH6aB"> 
                        <asp:Checkbox  ID="CheckBoxCBSymptomsVasculopathicRiskFactorsDiabetes" runat="server" GroupName="SymptomsVasculopathic" Text="&nbsp;Diabetes" />
                        <br /><asp:Checkbox ID="CheckBoxCBSymptomsVasculopathicRiskFactorsHyperlipidemia" runat="server" GroupName="SymptomsVasculopathic" Text="&nbsp;Hyperlipidemia" />
                        <br /><asp:Checkbox  ID="CheckBoxCBSymptomsVasculopathicRiskFactorsHypertension" runat="server" GroupName="SymptomsVasculopathic" Text="&nbsp;Hypertension" />
                        <br /><asp:Checkbox  ID="CheckBoxCBSymptomsVasculopathicRiskFactorsSmoking" runat="server" GroupName="SymptomsVasculopathic" Text="&nbsp;Smoking" />
                        <br /><asp:Checkbox  ID="CheckBoxCBSymptomsVasculopathicRiskFactorsOther" runat="server" GroupName="SymptomsVasculopathic" Text="&nbsp;Other" />
                        &nbsp;<asp:TextBox runat="server" ID="TextBoxSymptomsVasculopathicRiskFactorsOtherText" size="25" />
                        <br /><asp:Checkbox  ID="CheckBoxCBSymptomsVasculopathicRiskFactorsNone" runat="server" GroupName="SymptomsVasculopathic" Text="&nbsp;None" />
                        </span>
                    </td>
                </tr>
                <!-- Question 7 -->
                <tr class="table_body">
                    <td><strong>7. Were symptoms of giant cell arteritis reported?</strong><br />
                        <asp:Label ID="LabelRBSymptomsGiantCellArteritis" runat="server" Visible="false"  ForeColor="Red" Text="Please complete"></asp:Label>
                    </td>
                    <td>
                        <asp:RadioButtonList id="RadioButtonListRBSymptomsGiantCellArteritis" RepeatDirection="Horizontal" CssClass="aspxList" runat="server">
                            <asp:ListItem Value="1">&nbsp;Yes</asp:ListItem>
                            <asp:ListItem Value="2">&nbsp;No</asp:ListItem>
                            <asp:ListItem Value="3">&nbsp;Not&nbsp;Recorded</asp:ListItem>
                        </asp:RadioButtonList>
                    </td>
                </tr>
                <!-- Question 7a -->
                <tr class="table_body">
                    <td><span id="NewQH7aA"><strong>7a. If so, which symptoms were identified?</strong></span><br />
                        <asp:Label ID="LabelSymptomsFever" runat="server" Visible="false"  ForeColor="Red" Text="Please complete"></asp:Label>
                    </td>
                    <td>
                        <span id="NewQH7aB">
                        <asp:CheckBox runat="server" ID="CheckBoxSymptomsFever" class="check" text="&nbsp;Fever" />
                        <br /><asp:CheckBox runat="server" ID="CheckBoxSymptomsHeadache" class="check" text="&nbsp;Headache" />
                        <br /><asp:CheckBox runat="server" ID="CheckBoxSymptomsJawClaudication" class="check" text="&nbsp;Jaw Claudication" />
                        <br /><asp:CheckBox runat="server" ID="CheckBoxSymptomsScalp" class="check" text="&nbsp;Scalp Tenderness" />
                        <br /><asp:CheckBox runat="server" ID="CheckBoxSymptomsGiantCellOther" class="check" text="&nbsp;Other" />
                        &nbsp;<asp:TextBox runat="server" ID="TextBoxSymptomsGiantCellArteritisOtherText" size="25" />
                        <br /><asp:CheckBox runat="server" ID="CheckBoxSymptomsGiantCellNone" class="check" text="&nbsp;None" />
                        </span>
                    </td>
                </tr>
                <!-- Question 8 -->
                <tr class="table_body_bg">
                    <td><strong>8. Was a prior history of systemic malignancy reported?</strong><br />
                        <asp:Label ID="LabelRBHistorySystemicMalignancy" runat="server" Visible="false"  ForeColor="Red" Text="Please complete"></asp:Label>
                    </td>
                    <td>
                        <asp:RadioButtonList id="RadioButtonListRBHistorySystemicMalignancy" RepeatDirection="Horizontal"  CssClass="aspxList" runat="server">
                            <asp:ListItem Value="1">&nbsp;Yes</asp:ListItem>
                            <asp:ListItem Value="2">&nbsp;No</asp:ListItem>
                            <asp:ListItem Value="3">&nbsp;Not&nbsp;Recorded</asp:ListItem>
                        </asp:RadioButtonList>
                    </td>
                </tr>
                <!-- Question 8a -->
                <tr class="table_body_bg">
                    <td>
                        <span id="QH7aA"><strong>8a. If so, what malignancy?</strong></span><br />
                        <asp:Label ID="LabelHistoryMalignancy" runat="server" Visible="false"  ForeColor="Red" Text="Please complete"></asp:Label>
                        <!--<img class="left-bottom" src="../images/bg-corner-left-bottom.gif" alt="image description" />-->
                    </td>
                    <td>
                        <span id="QH7aB">
                        <asp:TextBox runat="server" ID="TextBoxHistoryMalignancy" TextMode="Multiline" Rows="3" size="255" />
                        </span>
                    </td>
                </tr>
            </table>
            <br />
            <!-- Examination (Diagnostic Testing) Section -->
            <table width="910px">
                <!-- Header - Examination (Diagnostic Testing) -->
                <tr>
                    <th colspan="2" style="width: 910px;"><p>Exam / Diagnostic Testing</p></th>
                </tr>
                <!-- Question 1 -->
                <tr class="table_body">
                    <td width="70%">
                        <strong>1. Was an abnormality of ductions noted?</strong><br />
                        <asp:Label ID="LabelRBExamAbnormalityVersions" runat="server" Visible="false"  ForeColor="Red" Text="Please complete"></asp:Label>
                    
                    </td>

                    <td>
                        <asp:RadioButtonList id="RadioButtonListRBExamAbnormalityVersions" RepeatDirection="Horizontal" CssClass="aspxList" runat="server">
	                        <asp:ListItem Value="1">&nbsp;Yes</asp:ListItem>
	                        <asp:ListItem Value="2">&nbsp;No</asp:ListItem>
	                        <asp:ListItem Value="3">&nbsp;Not Recorded</asp:ListItem>
                        </asp:RadioButtonList>
                    </td>
                </tr>
                <!-- Question 1a -->
                <tr class="table_body">
                    <td>
                        <strong><span id="QE1aA">1a. If so, what abnormality was present?</span></strong><br />
                         <asp:Label ID="LabelRBExamAbnormality" runat="server" Visible="false"  ForeColor="Red" Text="Please complete"></asp:Label>
                    </td>
                    <td>
                        <span id="QE1aB">
                        <asp:RadioButtonList id="RadioButtonListRBExamAbnormality" RepeatDirection="Horizontal" CssClass="aspxList" runat="server">
	                        <asp:ListItem Value="15">&nbsp;Limited Abduction</asp:ListItem>
	                        <asp:ListItem Value="14">&nbsp;Other</asp:ListItem>
                        </asp:RadioButtonList>
                        </span>
                    </td>
                </tr>
                <!-- Question 2 -->
                <tr class="table_body_bg">
                    <td>
                        <strong>2. Was degree of limited version recorded?</strong>
                        <br /><asp:Label ID="LabelRBExamDegreeLimitedVersion" runat="server" Visible="false"  ForeColor="Red" Text="Please complete"></asp:Label>
                    </td>
                    <td>
                        <asp:RadioButtonList id="RadioButtonListRBExamDegreeLimitedVersion" RepeatDirection="Horizontal" CssClass="aspxList" runat="server">
	                        <asp:ListItem Value="1">&nbsp;Yes</asp:ListItem>
	                        <asp:ListItem Value="2">&nbsp;No</asp:ListItem>
	                        <asp:ListItem Value="3">&nbsp;Not Recorded</asp:ListItem>
                        </asp:RadioButtonList>
                    </td>
                </tr>
                <!-- Question 2a -->
                <tr class="table_body_bg">
                    <td><span id="QE2aA"><strong>2a. If so, what degree was noted?</strong></span>&nbsp;<a href="#"><img id="QE2a" src="../../common/images/tip.gif" alt="Tip" /></a>
                    <br /><asp:Label ID="LabelRBExamDegreeNoted" runat="server" Visible="false"  ForeColor="Red" Text="Please complete"></asp:Label>
                    </td>
                    <td>
                        <span id="QE2aB">
                        <asp:RadioButtonList id="RadioButtonListRBExamDegreeNoted" RepeatDirection="Horizontal" CssClass="aspxList" runat="server">
	                        <asp:ListItem Value="16">&nbsp;-1</asp:ListItem>
	                        <asp:ListItem Value="17">&nbsp;-2</asp:ListItem>
	                        <asp:ListItem Value="18">&nbsp;-3</asp:ListItem>
                            <asp:ListItem Value="19">&nbsp;-4</asp:ListItem>
                        </asp:RadioButtonList>
                        </span>
                    </td>
                </tr>
                <!-- Question 3 -->
                <tr class="table_body">
                    <td><strong>3. Alignment in primary gaze</strong><br />
                        <asp:Label ID="LabelExamAlignment" runat="server" Visible="false"  ForeColor="Red" Text="Please complete"></asp:Label>
                    </td>
                    <td>
                        <asp:TextBox runat="server" ID="TextBoxExamAlignment" size="15" />&nbsp;PD
                    </td>
                </tr>
                <!-- Question 4 -->
                <tr class="table_body_bg">
                    <td>
                        <strong>4. Was incomitance noted?</strong>
                        <asp:Label ID="LabelRBExamImcomitanceNoted" runat="server" Visible="false"  ForeColor="Red" Text="<br />Please complete"></asp:Label>
                    </td>
                    <td>
                        <asp:RadioButtonList id="RadioButtonListRBExamImcomitanceNoted" RepeatDirection="Horizontal" CssClass="aspxList" runat="server">
	                        <asp:ListItem Value="1">&nbsp;Yes</asp:ListItem>
	                        <asp:ListItem Value="2">&nbsp;No</asp:ListItem>
	                        <asp:ListItem Value="3">&nbsp;Not Recorded</asp:ListItem>
                        </asp:RadioButtonList>
                    </td>
                </tr>
                <!-- Question 4a -->
                <tr class="table_body_bg">
                    <td><span id="NewQ4aA"><strong>4a. What incomitance was present?</strong></span>
                        <asp:Label ID="LabelRBExamIncomitance" runat="server" Visible="false"  ForeColor="Red" Text="<br />Please complete"></asp:Label>
                    </td>
                    <td>
                        <span id="NewQ4aB">
                        <asp:RadioButtonList id="RadioButtonListRBExamIncomitance" RepeatDirection="Vertical" CssClass="aspxList" runat="server">
	                        <asp:ListItem Value="20">&nbsp;Esodeviation increases in direction of limited abduction</asp:ListItem>
	                        <asp:ListItem Value="14">&nbsp;Other</asp:ListItem>
                        </asp:RadioButtonList>
                        </span>
                    </td>
                </tr>
                <!-- Question 4b -->
                <tr class="table_body_bg">
                    <td><span id="NewQ4bA"><strong>4b. If other, please specify:</strong></span></td>
                    <td>
                        <asp:TextBox runat="server" ID="TextBoxExamIncomitanceOther" size="15" />
                    </td>
                </tr>
                <!-- Question 5 -->
                <tr class="table_body">
                    <td><strong>5. Was a pupillary abnormality noted?</strong><br />
                        <asp:Label ID="LabelRBExamPupillaryAbnormality" runat="server" Visible="false"  ForeColor="Red" Text="Please complete"></asp:Label>
                    </td>
                    <td>
                        <asp:RadioButtonList id="RadioButtonListRBExamPupillaryAbnormality" RepeatDirection="Horizontal" CssClass="aspxList" runat="server">
	                        <asp:ListItem Value="1">&nbsp;Yes</asp:ListItem>
	                        <asp:ListItem Value="2">&nbsp;No</asp:ListItem>
	                        <asp:ListItem Value="3">&nbsp;Not Recorded</asp:ListItem>
                        </asp:RadioButtonList>
                    </td>
                </tr>
                <!-- Question 5a -->
                <tr class="table_body">
                    <td>
                        <span id="QE4aA"><strong>5a. If so, what abnormality?</strong></span>
                        <br /><asp:Label ID="Label1RBExamPupillaryAbnormalityType" runat="server" Visible="false"  ForeColor="Red" Text="Please complete"></asp:Label>
                    </td>
                    <td>
                        <span id="QE4aB">
                        <asp:RadioButtonList id="RadioButtonListRBExamPupillaryAbnormalityType" RepeatDirection="Vertical" CssClass="aspxList" runat="server">
	                        <asp:ListItem Value="21">&nbsp;Anisocoria</asp:ListItem>
	                        <asp:ListItem Value="22">&nbsp;Efferent pupillary defect</asp:ListItem>
	                        <asp:ListItem Value="23">&nbsp;Relative afferent pupillary defect</asp:ListItem>
                            <asp:ListItem Value="14">&nbsp;Other</asp:ListItem>
                        </asp:RadioButtonList>
                        </span>
                    </td>
                </tr>
                <!-- Question 6 -->
                <tr class="table_body_bg">
                    <td>
                        <strong>6. Were other cranial nerve or brainstem/cerebellar abnormalities noted?</strong><br />
                        <asp:Label ID="Label1RBExamCranialNerveAbnormality" runat="server" Visible="false"  ForeColor="Red" Text="Please complete"></asp:Label>
                    
                    </td>
                    <td>
                        <asp:RadioButtonList id="RadioButtonListRBExamCranialNerveAbnormality" RepeatDirection="Horizontal" CssClass="aspxList" runat="server">
	                        <asp:ListItem Value="1">&nbsp;Yes</asp:ListItem>
	                        <asp:ListItem Value="2">&nbsp;No</asp:ListItem>
	                        <asp:ListItem Value="3">&nbsp;Not Recorded</asp:ListItem>
                        </asp:RadioButtonList>
                    </td>
                </tr>
                <!-- Question 6a -->
                <tr class="table_body_bg">
                    <td>
                        <span id="QE6aA"><strong>6a. If so, which were abnormal?</strong></span>
                        <br /><asp:Label ID="LabelExamCranialNerveAbnormality" runat="server" Visible="false"  ForeColor="Red" Text="Please complete"></asp:Label>
                    </td>
                    <td>
                        <span id="QE6aB">
                        <asp:CheckBox runat="server" ID="CheckBoxExamCranialNerveAbnormality3rd" text="&nbsp;3<sup>rd</sup>" class="check"/>
                        <br /><asp:CheckBox runat="server" ID="CheckBoxExamCranialNerveAbnormality4th" text="&nbsp;4<sup>th</sup>" class="check"/>
                        <br /><asp:CheckBox runat="server" ID="CheckBoxExamCranialNerveAbnormality5th" text="&nbsp;5<sup>th</sup>" class="check"/>
                        <!--<br /><asp:CheckBox runat="server" ID="CheckBoxExamCranialNerveAbnormality6th" text="&nbsp;6<sup>th</sup>" class="check"/>-->
                        <br /><asp:CheckBox runat="server" ID="CheckBoxExamCranialNerveAbnormality7th" text="&nbsp;7<sup>th</sup>" class="check"/>
                        <br /><asp:CheckBox runat="server" ID="CheckBoxExamCranialNerveAbnormalityOther" text="&nbsp;Other" class="check"/>
                        </span>
                    </td>
                </tr>
                <!-- Question 7 -->
                <tr class="table_body">
                    <td><strong>7. Was the optic disc described?</strong>
                    <br /><asp:Label ID="LabelRBExamOpticDiscDescribed" runat="server" Visible="false"  ForeColor="Red" Text="Please complete"></asp:Label>
                    </td>
                    <td>
                        <asp:RadioButtonList id="RadioButtonListRBExamOpticDiscDescribed" RepeatDirection="Horizontal" CssClass="aspxList" runat="server">
	                        <asp:ListItem Value="1">&nbsp;Yes</asp:ListItem>
	                        <asp:ListItem Value="2">&nbsp;No</asp:ListItem>
	                        <asp:ListItem Value="3">&nbsp;Not Recorded</asp:ListItem>
                        </asp:RadioButtonList>
                    </td>
                </tr>
                <!-- Question 7a -->
                <tr class="table_body">
                    <td>
                        <span id="QE7aA"><strong>7a. If so, what was the appearance?</strong></span>
                        <br /><asp:Label ID="LabelRBExamOpticDiscAppearance" runat="server" Visible="false"  ForeColor="Red" Text="Please complete"></asp:Label>
                    </td>
                    <td>
                        <span id="QE7aB">
                        <asp:RadioButtonList id="RadioButtonListRBExamOpticDiscAppearance" RepeatDirection="Horizontal" CssClass="aspxList" runat="server">
	                        <asp:ListItem Value="24">&nbsp;Normal</asp:ListItem>
	                        <asp:ListItem Value="25">&nbsp;Edematous</asp:ListItem>
	                        <asp:ListItem Value="26">&nbsp;Pale</asp:ListItem>
                        </asp:RadioButtonList>
                        </span>
                    </td>
                </tr>
                <!-- Question 8 -->
                <tr class="table_body_bg">
                    <td>
                        <strong>8. Was erythrocyte sedimentation rate or c-reactive protein level tested?</strong><br />
                        <asp:Label ID="LabelRBExamErythrocyteSedimentationRate" runat="server" Visible="false"  ForeColor="Red" Text="Please complete"></asp:Label>
                    </td>
                    <td>
                        <asp:RadioButtonList id="RadioButtonListRBExamErythrocyteSedimentationRate" RepeatDirection="Horizontal" CssClass="aspxList" runat="server">
	                        <asp:ListItem Value="1">&nbsp;Yes</asp:ListItem>
	                        <asp:ListItem Value="2">&nbsp;No</asp:ListItem>
	                        <asp:ListItem Value="3">&nbsp;Not Recorded</asp:ListItem>
                        </asp:RadioButtonList>
                    </td>
                </tr>
                <!-- Question 9 -->
                <tr class="table_body">
                    <td><strong>9. Was a neuroimaging study performed?</strong><br />
                        <asp:Label ID="LabelRBExamNeuroimagingStudy" runat="server" Visible="false"  ForeColor="Red" Text="Please complete"></asp:Label>
                    </td>
                    <td>
                        <asp:RadioButtonList id="RadioButtonListRBExamNeuroimagingStudy" RepeatDirection="Horizontal" CssClass="aspxList" runat="server">
	                        <asp:ListItem Value="1">&nbsp;Yes</asp:ListItem>
	                        <asp:ListItem Value="2">&nbsp;No</asp:ListItem>
	                        <asp:ListItem Value="3">&nbsp;Not Recorded</asp:ListItem>
                        </asp:RadioButtonList>
                    </td>
                </tr>
                <!-- Question 9a -->
                <tr class="table_body">
                    <td>
                        <span id="QE9aA"><strong>9a. If so, specify indications</strong></span>
                        <br /><asp:Label ID="LabelExamIndications" runat="server" Visible="false"  ForeColor="Red" Text="Please complete"></asp:Label>
                    </td>
                    <td>
                        <span id="QE9aB">
                        <asp:CheckBox runat="server" ID="CheckBoxExamMultipleCranialNeuropathies" text="&nbsp;Multiple&nbsp;Cranial&nbsp;Neuropathies" class="check"/>
                        <br /><asp:CheckBox runat="server" ID="CheckBoxExamHistoryMalignancy" text="&nbsp;History&nbsp;of&nbsp;Malignancy" class="check"/>
                        <br /><asp:CheckBox runat="server" ID="CheckBoxExamNeuroimagingOther" text="&nbsp;Other" class="check"/>
                        </span>
                    </td>
                </tr>
            </table>
            <br />
            <!-- Management -->
            <table width="910px">
                <!-- Header - Management -->
                <tr>
                    <th colspan="2"><p>Management</p></th>
                </tr>
                <!-- Question 1 -->
                <tr class="table_body">
                    <td width="70%"><strong>1. Was treatment of diplopia recommended?</strong><br />
                        <asp:Label ID="LabelRBManagementDiplopia" runat="server" Visible="false"  ForeColor="Red" Text="Please complete"></asp:Label>
                    </td>
                    <td>
                        <asp:RadioButtonList id="RadioButtonListRBManagementDiplopia" RepeatDirection="Horizontal" CssClass="aspxList" runat="server">
	                        <asp:ListItem Value="1">&nbsp;Yes</asp:ListItem>
	                        <asp:ListItem Value="2">&nbsp;No</asp:ListItem>
	                        <asp:ListItem Value="3">&nbsp;Not Recorded</asp:ListItem>
                        </asp:RadioButtonList>
                    </td>
                </tr>
                <!-- Question 1a -->
                <tr class="table_body">
                    <td>
                        <span id="QM1aA"><strong>1a. If so, what measures were recommended?</strong></span>
                        <br /><asp:Label ID="LabelManagement" runat="server" Visible="false"  ForeColor="Red" Text="Please complete"></asp:Label>
                    </td>
                    <td>
                        <span id="QM1aB">
                        <asp:CheckBox runat="server" ID="CheckBoxManagementOcclusion" text="&nbsp;Occlusion" class="check"/>
                        <br /><asp:CheckBox runat="server" ID="CheckBoxManagementPrism" text="&nbsp;Prisms" class="check"/>
                        <br /><asp:CheckBox runat="server" ID="CheckBoxManagementSurgery" text="&nbsp;Surgery" class="check"/>
                        <br /><asp:CheckBox runat="server" ID="CheckBoxManagementOther" text="&nbsp;Other" class="check"/>
                        </span>
                    </td>
                </tr>
            </table>
            <br />
            <!-- Outcomes Section -->
            <table width="910px">
                <!-- Header - Outcomes -->
                <tr>
                    <th colspan="2"><p>Outcomes</p></th>
                </tr>
                <!-- Question 1 -->
                <tr class="table_body">
                    <td class="style1"><strong>1. If prior neuroimaging was negative, were screening tests for vasculopathic risk factors obtained or considered?</strong><br />
                        <asp:Label ID="LabelRBOutcomeVasculopathicScreening" runat="server" Visible="false"  ForeColor="Red" Text="Please complete"></asp:Label>
                    </td>
                    <td>
                        <asp:RadioButtonList id="RadioButtonListRBOutcomeVasculopathicScreening" RepeatDirection="Horizontal" CssClass="aspxList" runat="server">
	                        <asp:ListItem Value="1">&nbsp;Yes</asp:ListItem>
	                        <asp:ListItem Value="2">&nbsp;No</asp:ListItem>
	                        <asp:ListItem Value="3">&nbsp;Not Recorded</asp:ListItem>
                        </asp:RadioButtonList>
                    </td>
                </tr>
                <!-- Question 2 -->
                <tr class="table_body_bg">
                    <td class="style1"><strong>2. Was follow-up examination within 3 months recommended or performed?</strong><br />
                        <asp:Label ID="LabelRBOutcomeFollowUpExam" runat="server" Visible="false"  ForeColor="Red" Text="Please complete"></asp:Label>
                    </td>
                    <td>
                        <asp:RadioButtonList id="RadioButtonListRBOutcomeFollowUpExam" RepeatDirection="Vertical" CssClass="aspxList" runat="server">
	                        <asp:ListItem Value="1">&nbsp;Yes</asp:ListItem>
	                        <asp:ListItem Value="2">&nbsp;No</asp:ListItem>
	                        <asp:ListItem Value="3">&nbsp;Not Recorded</asp:ListItem>
	                        <asp:ListItem Value="128">&nbsp;Data Not Availabe</asp:ListItem>
                        </asp:RadioButtonList>
                    </td>
                </tr>
                <!-- Question 2a -->
                <tr class="table_body_bg">
                    <td class="style1"><span id="QO2aA"><strong>2a. If so, were changes in ocular alignment and/or versions noted?</strong></span>
                    <br /><asp:Label ID="LabelRBOutcomeChangesOcularAlignment" runat="server" Visible="false"  ForeColor="Red" Text="Please complete"></asp:Label>
                    </td>
                    <td>
                        <span id="QO2aB">
                        <asp:RadioButtonList id="RadioButtonListRBOutcomeChangesOcularAlignment" RepeatDirection="Vertical" CssClass="aspxList" runat="server">
	                        <asp:ListItem Value="1">&nbsp;Yes</asp:ListItem>
	                        <asp:ListItem Value="2">&nbsp;No</asp:ListItem>
	                        <asp:ListItem Value="3">&nbsp;Not Recorded</asp:ListItem>
	                        <asp:ListItem Value="350">&nbsp;Data not Available</asp:ListItem>
                        </asp:RadioButtonList>
                        </span>
                    </td>
                </tr>
                <!-- Question 2b -->
                <tr class="table_body_bg">
                    <td class="style1">
                        <span id="QO2bA"><strong>2b. If so, what changes were noted?</strong></span>
                        <br /><asp:Label ID="LabelChangesNoted" runat="server" Visible="false"  ForeColor="Red" Text="Please complete"></asp:Label>
                    </td>
                    <td>
                        <span id="QO2bB">
                        <asp:CheckBox runat="server" ID="CheckBoxOutcomeChangesNotedImprovement" text="&nbsp;Improvement" class="check"/>
                        <br /><asp:CheckBox runat="server" ID="CheckBoxOutcomeChangesNotedWorsening" text="&nbsp;Worsening" class="check"/>
                               <br /><asp:CheckBox runat="server" ID="CheckBoxOutcomeChangesNotedDataNotAvailabe" text="&nbsp;Data Not Available" class="check"/>
                        </span>
                    </td>
                </tr>
                <!-- Question 3 -->
                <tr class="table_body">
                    <td class="style1"><strong>3. Was a recomendation made that if no resolution by 3-4 months, 
                        neuroimagimg should be performed?</strong><br />
                        <asp:Label ID="LabelRBOutcomeNeuroimaging" runat="server" Visible="false"  ForeColor="Red" Text="Please complete"></asp:Label>
                    </td>
                    <td>
                        <asp:RadioButtonList id="RadioButtonListRBOutcomeNeuroimaging" RepeatDirection="Horizontal" CssClass="aspxList" runat="server">
	                        <asp:ListItem Value="1">&nbsp;Yes</asp:ListItem>
	                        <asp:ListItem Value="2">&nbsp;No</asp:ListItem>
	                        <asp:ListItem Value="3">&nbsp;Not Recorded</asp:ListItem>
	                        
                        </asp:RadioButtonList>
                    </td>
                </tr>
                <!-- Question 4 -->
                <tr class="table_body_bg">
                    <td class="style1"><strong>4. Was final etiology of the 6<sup>th</sup> nerve palsy recorded?</strong><br />
                        <asp:Label ID="LabelRBOutcomeFinalEtiology" runat="server" Visible="false"  ForeColor="Red" Text="Please complete"></asp:Label>
                    </td>
                    <td>
                        <asp:RadioButtonList id="RadioButtonListRBOutcomeFinalEtiology" RepeatDirection="Horizontal" CssClass="aspxList" runat="server">
	                        <asp:ListItem Value="1">&nbsp;Yes</asp:ListItem>
	                        <asp:ListItem Value="2">&nbsp;No</asp:ListItem>
	                        <asp:ListItem Value="3">&nbsp;Not Recorded</asp:ListItem>
                        </asp:RadioButtonList>
                    </td>
                </tr>
                <!-- Question 4a -->
                <tr class="table_body_bg">
                    <td class="style1"><span id="QO4aA"><strong>4a. If so, what etiology was recorded?</strong></span>
                    <br /><asp:Label ID="LabelOutcomeEtiology" runat="server" Visible="false"  ForeColor="Red" Text="Please complete"></asp:Label>
                    </td>
                    <td>
                        <span id="QO4aB">
                        <asp:TextBox runat="server" ID="TextBoxOutcomeEtiology" size="25" />
                        </span>
                    </td>
                </tr>
            </table>
            <div class="button-box">
                <asp:LinkButton ID="LinkButtonBackToDashboard" runat="server" Text="Back To Chart Registration" PostBackUrl="../PatientChartRegistration.aspx?CycleNumber=1" Visible="false" CssClass="button" />
                <asp:LinkButton ID="ButtonSubmit" runat="server" OnClick="ButtonSubmit_Click" Text="Submit Chart" CssClass="button" />
            </div>
        </div>
        <!-- SNP pim ends -->
</asp:Content>