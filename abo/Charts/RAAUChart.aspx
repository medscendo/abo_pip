﻿<%@ Page Title="" Language="C#" MasterPageFile="~/PIMMasterPage.master" AutoEventWireup="true" CodeFile="RAAUChart.aspx.cs" Inherits="abo_RAAUChart" %>

<asp:Content ID="Content1" ContentPlaceHolderID="head" Runat="Server">


    <link type="text/css" href="../../common/css/atooltip.css" rel="stylesheet"  media="screen" />
	<script type="text/javascript" src="../../common/js/jquery.atooltip.js"></script>
    
    <style type="text/css">
        .bginputa{
	        float:right;
	        margin:21px 0 0 1px;
	        background: url(../common/images/orange_button_with_arrow.png) no-repeat;
	        overflow:hidden;
	        height:35px;

        }
        .bginputa a{
	        color: white;
	        text-align:center;
	        height:35px;
	        line-height:28px;
	        padding:0 14px 15px  ;
	        cursor:pointer;
	        float:left;
	        border:none;
	        text-decoration:none;
	        font-family:Arial, Helvetica, sans-serif; 
	        font-size:12px; 
	        font-weight:bold; 
	        letter-spacing: 0px;
        }
        .bginputa a:hover{
	        text-decoration:none;
        }
        .right {
            text-align:right;
        }
    </style>
    
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder1" Runat="Server">
    <asp:HiddenField ID="HiddenFieldRecordIdentifier" runat="server"/>
        <!-- RAAU pim -->
        <div class="pim">
            <div class="record-ident clearfix">
                <h3 class="record-first">RECORD IDENTIFIER: <asp:Literal runat="server" ID="LiteralRecordIdentifier"></asp:Literal></h3>
                <h3 class="record-second"><asp:Literal runat="server" ID="LiteralAbstractionNumber"></asp:Literal></h3>
            </div>
            <!-- History -->
            <table>
                <tr>
                    <th colspan="3" width="910px"><p>History</p></th>
                </tr>
                <!-- Question 1 -->
                <tr class="table_body">
                    <td colspan="2">
                        <strong>1. Date of birth</strong>
                        <asp:Label ID="LabelMonthOfBirth" runat="server" Visible="false"  ForeColor="Red" Text="<br />Please complete month" />
                        <asp:Label ID="LabelYearOfBirth" runat="server" Visible="false"  ForeColor="Red" Text="<br />Please complete year" />
                    </td>
                    <td>
                        <asp:DropDownList ID='DropDownListMonthOfBirth' DataValueField='MonthID' DataTextField='MonthName' runat='server' />
                        <asp:DropDownList ID='DropDownListYearOfBirth' DataValueField='YearID' DataTextField='YearName' runat='server' />
                    </td>
                </tr>
                <!-- Question 2 -->
                <tr class="table_body_bg">
                    <td width="70%" colspan="2">
                        <strong>2. Date of onset of anterior uveitis </strong>
                        <asp:Label ID="LabelMonthOfOnsetFirstEpisode" runat="server" Visible="false"  ForeColor="Red" Text="<br />Please complete month" />
                        <asp:Label ID="LabelYearOfOnsetFirstEpisode" runat="server" Visible="false"  ForeColor="Red" Text="<br />Please complete year" />
                    </td>
                    <td>
                        <asp:DropDownList ID='DropDownListMonthOfOnsetFirstEpisode' DataValueField='MonthID' DataTextField='MonthName' runat='server' />                        
                        <asp:DropDownList ID='DropDownListYearOfOnsetFirstEpisode' DataValueField='YearID' DataTextField='YearName' runat='server' />
                    </td>
                </tr>
                <!-- Question 2a -->
                <tr class="table_body_bg">
                    <td colspan="2">
                        <strong>2a. First episode, which eye?</strong>
                        <asp:Label ID="LabelRBDiagnosisFirstEpisode" runat="server" Visible="false"  ForeColor="Red" Text="<br />Please complete" />
                    </td>
                    <td>
                        <asp:RadioButtonList id='RadioButtonListRBDiagnosisFirstEpisode' RepeatDirection='Horizontal' CssClass='aspxList' runat='server'>
                            <asp:ListItem Value='49'>&nbsp;Right</asp:ListItem>
                            <asp:ListItem Value='50'>&nbsp;Left</asp:ListItem>
                            <asp:ListItem Value='110'>&nbsp;Both</asp:ListItem>
                        </asp:RadioButtonList>
                    </td>
                </tr>
                    <tr class="table_body_bg">
                    <td width="70%" colspan="2">
                        <strong>2b. Etiology determined?</strong>
                    </td>
                    <td>
                        <asp:RadioButton ID="RadioButtonEtiologyDeterminedYES" Text="Yes" groupname="EtiologyDetermined" runat="server" />                       
                        <br /><asp:RadioButton ID="RadioButtonEtiologyDeterminedNO" Text="No" groupname="EtiologyDetermined" runat="server" />  
                        <br /><span id="QH2btxt">Diagnosis:</span>&nbsp;<asp:TextBox ID="EtiologyDeterminedText" runat="server" Width="81px"></asp:TextBox>
                    </td>
                </tr>
                <!-- Question 3 -->
                <tr class="table_body">
                    <td colspan="2">
                        <strong>3. Date of onset of current episode</strong>
                        <asp:Label ID="LabelMonthOfOnsetCurrentEpisode" runat="server" Visible="false"  ForeColor="Red" Text="<br />Please complete month" />
                        <asp:Label ID="LabelYearOfOnsetCurrentEpisode" runat="server" Visible="false"  ForeColor="Red" Text="<br />Please complete year" />
                    </td>
                    <td>
                        <asp:DropDownList ID='DropDownListMonthOfOnsetCurrentEpisode' DataValueField='MonthID' DataTextField='MonthName' runat='server' />
                        <asp:DropDownList ID='DropDownListYearOfOnsetCurrentEpisode' DataValueField='YearID' DataTextField='YearName' runat='server' />
                    </td>
                </tr>
                <!-- Question 3a -->
                <tr class="table_body">
                    <td colspan="2">
                        <strong>3a. Laterality</strong>
                        <asp:Label ID="LabelRBLaterality" runat="server" Visible="false"  ForeColor="Red" Text="<br />Please complete" />
                    </td>
                    <td>
                        <asp:RadioButtonList id='RadioButtonListRBLaterality' RepeatDirection='Horizontal' CssClass='aspxList' runat='server'>
                            <asp:ListItem Value='49'>&nbsp;Right</asp:ListItem>
                            <asp:ListItem Value='50'>&nbsp;Left</asp:ListItem>
                            <asp:ListItem Value='110'>&nbsp;Both</asp:ListItem>
                        </asp:RadioButtonList>
                    </td>
                </tr>
                <!-- Question 4 -->
                <tr class="table_body_bg">
                    <td colspan="2">
                        <strong>4. Most recent prior episode</strong>
                        <asp:Label ID="LabelMonthOfOnsetLastEpisode" runat="server" Visible="false"  ForeColor="Red" Text="<br />Please complete month" />
                        <asp:Label ID="LabelYearOfOnsetLastEpisode" runat="server" Visible="false"  ForeColor="Red" Text="<br />Please complete year" />
                    </td>
                    <td>
                        <asp:DropDownList ID='DropDownListMonthOfOnsetLastEpisode' DataValueField='MonthID' DataTextField='MonthName' runat='server' />
                        <asp:DropDownList ID='DropDownListYearOfOnsetLastEpisode' DataValueField='YearID' DataTextField='YearName' runat='server' />
                        
                    </td>
                </tr>
                <!-- Question 4a -->
                <tr class="table_body_bg">
                    <td colspan="2">
                        <strong>4a. Which eye</strong>
                        <asp:Label ID="LabelRBLastEpisodeEye" runat="server" Visible="false"  ForeColor="Red" Text="<br />Please complete" />
                    </td>
                    <td>
                        <asp:RadioButtonList id='RadioButtonListRBLastEpisodeEye' RepeatDirection='Horizontal' CssClass='aspxList' runat='server'>
                            <asp:ListItem Value='49'>&nbsp;Right</asp:ListItem>
                            <asp:ListItem Value='50'>&nbsp;Left</asp:ListItem>
                            <asp:ListItem Value='110'>&nbsp;Both</asp:ListItem>
                        </asp:RadioButtonList>
                    </td>
                </tr>
                <!-- Question 5 -->
                <tr class="table_body">
                    <td colspan="2">
                        <strong>5. Gender</strong>
                        <asp:Label ID="LabelRBGender" runat="server" Visible="false"  ForeColor="Red" Text="<br />Please complete" />
                    </td>
                    <td>
                        <asp:RadioButtonList id='RadioButtonListRBGender' RepeatDirection='Horizontal' CssClass='aspxList' runat='server'>
                            <asp:ListItem Value='42'>&nbsp;Male</asp:ListItem>
                            <asp:ListItem Value='43'>&nbsp;Female</asp:ListItem>
                        </asp:RadioButtonList>
                    </td>
                </tr>
                <!-- Question 6 -->
                <tr class="table_body_bg">
                    <td colspan="2">
                        <strong>6. Presenting symptoms (for current episode)</strong>
                        <br />(Check those that apply)
                    </td>
                    <td>
                        <asp:CheckBox runat='server' ID='CheckBoxPresentingSymptomPain' text='&nbsp;Pain' />
                        <br /><asp:CheckBox runat='server' ID='CheckBoxPresentingSymptomPhotophobia' text='&nbsp;Photophobia' />
                        <br /><asp:CheckBox runat='server' ID='CheckBoxPresentingSymptomBlurredVision' text='&nbsp;Blurred Vision' />
                        <br /><asp:CheckBox runat='server' ID='CheckBoxPresentingSymptomFloaters' text='&nbsp;Floaters' />
                        <br /><asp:CheckBox runat='server' ID='CheckBoxPresentingSymptomRedness' text='&nbsp;Redness' />
                    </td>
                </tr>
                <!-- Question 7 -->
                <tr class="table_body">
                    <td width="35%" rowspan="5" style="vertical-align: top; padding-top:30px;">
                        <strong>7. Previous treatment</strong>
                    </td>
                    <td width="25%">
                        <span class="right">Has there been any treatment for prior episodes?</span>
                        <asp:Label ID="Label1" runat="server" Visible="false"  ForeColor="Red" Text="<br />Please complete" />
                    </td>
                    <td>
                       <asp:RadioButtonList id='RadioButtonListPriorEpisodes' RepeatDirection='Horizontal' CssClass='aspxList' runat='server'>
                            <asp:ListItem Value="1">&nbsp;Yes</asp:ListItem>
	                        <asp:ListItem Value="2">&nbsp;No</asp:ListItem>
	                        <asp:ListItem Value="3">&nbsp;Not Recorded</asp:ListItem>
                        </asp:RadioButtonList>
                    </td>
                    </tr>
                    <tr class="table_body">
                    <td width="25%" id="QH7aA">
                        <span class="right">Were topical corticosteroids used?</span>
                        <asp:Label ID="LabelPreviousTreatmentTopicalCS" runat="server" Visible="false"  ForeColor="Red" Text="<br />Please complete" />
                    </td>
                    <td id="QH7aB">
                        <asp:RadioButton runat='server' ID='RadioButtonPreviousTreatmentTopicalCSYes' GroupName='PreviousTreatmentTopicalCS' text='&nbsp;Yes' />&nbsp;&nbsp;
                        <asp:RadioButton runat='server' ID='RadioButtonPreviousTreatmentTopicalCSNo' GroupName='PreviousTreatmentTopicalCS' text='&nbsp;No' />
                    </td>
                </tr>
                <tr class="table_body">
                    <td id="QH7bA">
                        <span class="right">Were periocular corticosteroids used?</span>
                        <asp:Label ID="LabelPreviousTreatmentPeriocularCS" runat="server" Visible="false"  ForeColor="Red" Text="<br />Please complete" />    
                    </td>
                    <td id="QH7bB">
                        <asp:RadioButton runat='server' ID='RadioButtonPreviousTreatmentPeriocularCSYes' GroupName='PreviousTreatmentPeriocularCS' text='&nbsp;Yes' />&nbsp;&nbsp;
                        <asp:RadioButton runat='server' ID='RadioButtonPreviousTreatmentPeriocularCSNo' GroupName='PreviousTreatmentPeriocularCS' text='&nbsp;No' />
                    </td>
                </tr>
                <tr class="table_body">
                    <td id="QH7cA"><span class="right">If yes, what was the total number of injections  required to achieve quiescence?</span></td>
                    <td>
                        <asp:TextBox runat='server' ID='TextBoxPreviousTreatmentNbrInjections' size='5' />
                    </td>
                </tr>
                <tr class="table_body">
                    <td id="QH7dAa">
                        <span class="right">Were systemic corticosteroids used?</span>
                        <asp:Label ID="LabelPreviousTreatmentSystemicCS" runat="server" Visible="false"  ForeColor="Red" Text="<br />Please complete" />
                    </td>
                    <td id="QH7dB">
                        <asp:RadioButton runat='server' ID='RadioButtonPreviousTreatmentSystemicCSYes' GroupName='PreviousTreatmentSystemicCS' text='&nbsp;Yes' />&nbsp;&nbsp;
                        <asp:RadioButton runat='server' ID='RadioButtonPreviousTreatmentSystemicCSNo' GroupName='PreviousTreatmentSystemicCS' text='&nbsp;No' />
                        <span id="QH7dA">
                            <br />Drug: <asp:TextBox runat='server' ID='TextBoxPreviousTreatmentSystemicDrug' size='20' />
                            <br /><br />Duration: <asp:TextBox runat='server' ID='TextBoxPreviousTreatmentSystemicNbrWeeks' size='5' /> weeks
                        </span>
                    </td>
                </tr>
                <tr class="table_body">
                    <td rowspan="2" id="QH7aaA"><strong>7a. Immunomodulatory therapy</strong></td>
                    <td id="QH7aaB">
                        <span class="right">Drug:</span>
                    </td>
                    <td>
                        <asp:TextBox runat='server' ID='TextBoxPreviousTreatmentImmuTherapyDrug' size='25' />
                    </td>
                </tr>
                <tr class="table_body">
                    <td id="QH7abA"><span class="right">Start and stop date:</span></td>
                    <td>
                        <asp:DropDownList ID='DropDownListPreviousTreatmentImmuStartMonth' DataValueField='MonthID' DataTextField='MonthName' runat='server' />
                        <asp:DropDownList ID='DropDownListPreviousTreatmentImmuStartYear' DataValueField='YearID' DataTextField='YearName' runat='server' />
                        <br /><asp:DropDownList ID='DropDownListPreviousTreatmentImmuStopMonth' DataValueField='MonthID' DataTextField='MonthName' runat='server' />
                        <asp:DropDownList ID='DropDownListPreviousTreatmentImmuStopYear' DataValueField='YearID' DataTextField='YearName' runat='server' />
                    </td>
                </tr>
                <!-- Question 8 -->
                <tr class="table_body_bg">
                    <td colspan="2">
                        <strong>8. Duration of inflammation free state before recurrence</strong>
                    </td>
                    
                    <td>
                        <asp:TextBox runat='server' ID='TextBoxInflammationFreeDurationWeeks' size='5' onkeyup="toggleQH8CB();" /> weeks
                        <br /><asp:Checkbox ID="RadioButtonInflammationFreeDurationNDYes"  runat="server"  onclick="toggleQH8TB();" Text="Not Documented" />
                        
                    </td>
                </tr>
                <!-- Question 9 -->
                <tr class="table_body">
                    <td colspan="2">
                        <strong>9. Family history (anyone with ankylosing spondylitis, reactive arthritis, psoriatic arthritis, or inflammatory bowel disease in family?</strong>
                        <asp:Label ID="LabelRBHistoryFamily" runat="server" Visible="false"  ForeColor="Red" Text="<br />Please complete" />
                    </td>
                    <td>
                        <asp:RadioButtonList id='RadioButtonListRBHistoryFamily' RepeatDirection='Horizontal' CssClass='aspxList' runat='server'>
                            <asp:ListItem Value="1">&nbsp;Yes</asp:ListItem>
	                        <asp:ListItem Value="2">&nbsp;No</asp:ListItem>
	                        <asp:ListItem Value="3">&nbsp;Not Recorded</asp:ListItem>
                        </asp:RadioButtonList>
                    </td>
                </tr>
                <!-- Question 9a -->
                <tr class="table_body">
                    <td colspan="2">
                        <span id="QH9a">
                        <strong>9a. If yes, relationship:</strong>
                        </span>
                    </td>
                    <td>
                        <asp:TextBox runat='server' ID='TextBoxHistoryFamilyRelationship' size='25' />
                    </td>
                </tr>
                <!-- Question 10 -->
                <tr class="table_body_bg">
                    <td rowspan="6" style="vertical-align: top; padding-top: 30px;">
                        <strong>10. Past medical history</strong>
                    </td>
                    <td>
                        <span class="right">Low back pain</span>
                        <asp:Label ID="LabelRBHistoryLowBackPain" runat="server" Visible="false"  ForeColor="Red" Text="<br />Please complete" />
                    </td>
                    <td>
                        <asp:RadioButtonList id='RadioButtonListRBHistoryLowBackPain' RepeatDirection='Horizontal' CssClass='aspxList' runat='server'>
                            <asp:ListItem Value="1">&nbsp;Yes</asp:ListItem>
	                        <asp:ListItem Value="2">&nbsp;No</asp:ListItem>
	                        <asp:ListItem Value="3">&nbsp;Not Recorded</asp:ListItem>
                        </asp:RadioButtonList>
                    </td>
                </tr>
                <tr class="table_body_bg">
                    <td>
                        <span class="right">SI joint films done</span>
                        <asp:Label ID="LabelRBHistorySIJointFilms" runat="server" Visible="false"  ForeColor="Red" Text="<br />Please complete" />
                    </td>
                    <td>
                        <asp:RadioButtonList id='RadioButtonListRBHistorySIJointFilms' RepeatDirection='Horizontal' CssClass='aspxList' runat='server'>
                            <asp:ListItem Value="1">&nbsp;Yes</asp:ListItem>
	                        <asp:ListItem Value="2">&nbsp;No</asp:ListItem>
	                        <asp:ListItem Value="3">&nbsp;Not Recorded</asp:ListItem>
                        </asp:RadioButtonList>
                    </td>
                </tr>
                <tr class="table_body_bg">
                    <td>
                        <span class="right">Diagnosis of previous seronegative spondyloarthropathy, reactive arthritis, 
                        aphthous ulcers,  psoriatic arthritis, inflammatory bowel disease</span>
                        <asp:Label ID="LabelRBHistoryDiagnosis" runat="server" Visible="false"  ForeColor="Red" Text="<br />Please complete" />
                    </td>
                    <td>
                        <asp:RadioButtonList id='RadioButtonListRBHistoryDiagnosis' RepeatDirection='Horizontal' CssClass='aspxList' runat='server'>
                            <asp:ListItem Value="1">&nbsp;Yes</asp:ListItem>
	                        <asp:ListItem Value="2">&nbsp;No</asp:ListItem>
	                        <asp:ListItem Value="3">&nbsp;Not Recorded</asp:ListItem>
                        </asp:RadioButtonList>
                    </td>
                </tr>
                <tr class="table_body_bg">
                    <td>
                        <span class="right">Sarcoidosis – Chest CT or radiograph, biopsy proven</span>
                        <asp:Label ID="LabelRBHistorySarcoidosis" runat="server" Visible="false"  ForeColor="Red" Text="<br />Please complete" />
                    </td>
                    <td>
                        <asp:RadioButtonList id='RadioButtonListRBHistorySarcoidosis' RepeatDirection='Horizontal' CssClass='aspxList' runat='server'>
                            <asp:ListItem Value="1">&nbsp;Yes</asp:ListItem>
	                        <asp:ListItem Value="2">&nbsp;No</asp:ListItem>
	                        <asp:ListItem Value="3">&nbsp;Not Recorded</asp:ListItem>
                        </asp:RadioButtonList>
                    </td>
                </tr>
                <tr class="table_body_bg">
                    <td>
                        <span class="right">Syphilis – FTA-ABS or MHA-TP positive</span>
                        <asp:Label ID="LabelRBHistorySyphilis" runat="server" Visible="false"  ForeColor="Red" Text="<br />Please complete" />
                        
                    </td>
                    <td>
                        <asp:RadioButtonList id='RadioButtonListRBHistorySyphilis' RepeatDirection='Horizontal' CssClass='aspxList' runat='server'>
                            <asp:ListItem Value="1">&nbsp;Yes</asp:ListItem>
	                        <asp:ListItem Value="2">&nbsp;No</asp:ListItem>
	                        <asp:ListItem Value="3">&nbsp;Not Recorded</asp:ListItem>
                        </asp:RadioButtonList>
                    </td>
                </tr>
                <tr class="table_body_bg">
                    <td>
                        <span class="right">Tuberculosis – PPD skin test or quantiferon positive</span>
                        <asp:Label ID="LabelRBHistoryTuberculosis" runat="server" Visible="false"  ForeColor="Red" Text="<br />Please complete" />
                    </td>
                    <td>
                        <asp:RadioButtonList id='RadioButtonListRBHistoryTuberculosis' RepeatDirection='Horizontal' CssClass='aspxList' runat='server'>
                            <asp:ListItem Value="1">&nbsp;Yes</asp:ListItem>
	                        <asp:ListItem Value="2">&nbsp;No</asp:ListItem>
	                        <asp:ListItem Value="3">&nbsp;Not Recorded</asp:ListItem>
                        </asp:RadioButtonList>
                    </td>
                </tr>
                <!-- Question 11 -->
                <tr class="table_body">
                    <td colspan="2">
                        <strong>11. Smoking history</strong>
                        <asp:Label ID="LabelRBHistorySmoking" runat="server" Visible="false"  ForeColor="Red" Text="<br />Please complete" />
                    </td>
                    <td>
                        <asp:RadioButtonList id='RadioButtonListRBHistorySmoking' RepeatDirection='Horizontal' CssClass='aspxList' runat='server'>
                            <asp:ListItem Value="1">&nbsp;Yes</asp:ListItem>
	                        <asp:ListItem Value="2">&nbsp;No</asp:ListItem>
	                        <asp:ListItem Value="3">&nbsp;Not Recorded</asp:ListItem>
                        </asp:RadioButtonList>
                    </td>
                </tr>
            </table>
            <br />
            <!-- Clinical Examination Findings -->
            <table>
                <tr>
                    <th colspan="3" width="910px"><p>Clinical Examination Findings</p></th>
                </tr>
                <!-- Question 1 -->
                <tr class="table_body">
                    <td rowspan="2">
                        <strong>1. Best corrected visual acuity?</strong>&nbsp;<a href="#"><img id="QCE1" src="../../common/images/tip.gif" alt="Tip" /></a>
                        <asp:Label ID="LabelBCVAOD" runat="server" Visible="false"  ForeColor="Red" Text="<br />Please complete OD" />
                        <asp:Label ID="LabelBCVAOS" runat="server" Visible="false"  ForeColor="Red" Text="<br />Please complete OS" />
                    </td>
                    <td><span class="right">OD</span></td>
                    <td>
                        20/ <asp:DropDownList ID='DropDownListBCVAOD' DataValueField='examValue' DataTextField='examLabel' runat='server' />
                    </td>
                </tr>
                <tr class="table_body">
                    <td><span class="right">OS</span></td>
                    <td>
                        20/ <asp:DropDownList ID='DropDownListBCVAOS' DataValueField='examValue' DataTextField='examLabel' runat='server' />
                    </td>
                </tr>
                <!-- Question 1a -->
                <tr class="table_body">
                    <td colspan="2">
                        <strong>1a. Type</strong>
                        <asp:Label ID="LabelRBBCVAType" runat="server" Visible="false"  ForeColor="Red" Text="<br />Please complete" />
                    </td>
                    <td>
                        <asp:RadioButtonList id='RadioButtonListRBBCVAType' RepeatDirection='Horizontal' CssClass='aspxList' runat='server'>
                            <asp:ListItem Value='155'>&nbsp;Snellen</asp:ListItem>
                            <asp:ListItem Value='156'>&nbsp;ETDRS</asp:ListItem>
                        </asp:RadioButtonList>
                    </td>
                </tr>
                <!-- Question 2 -->
                <tr class="table_body_bg">
                    <td width="35%" rowspan="2">
                        <strong>2. Conjunctiva</strong>
                    </td>
                    <td width="25%">
                        <span class="right">Was conjunctival injection present?</span>
                        <asp:Label ID="LabelConjunctivalInjection" runat="server" Visible="false"  ForeColor="Red" Text="<br />Please complete" />
                    </td>
                    <td>
                        <asp:RadioButton runat='server' ID='RadioButtonConjunctivalInjectionYes' GroupName='ConjunctivalInjection' text='&nbsp;Yes' />&nbsp;&nbsp;
                        <asp:RadioButton runat='server' ID='RadioButtonConjunctivalInjectionNo' GroupName='ConjunctivalInjection' text='&nbsp;No' />
                    </td>
                </tr>
                <tr class="table_body_bg">
                    <td>
                        <span class="right">Were conjunctival nodules present? (i.e. sarcoid acute anterior uveitis)</span>
                        <asp:Label ID="LabelConjunctivalNodules" runat="server" Visible="false"  ForeColor="Red" Text="<br />Please complete" />    
                    </td>
                    <td>
                        <asp:RadioButton runat='server' ID='RadioButtonConjunctivalNodulesYes' GroupName='ConjunctivalNodules ' text='&nbsp;Yes' />&nbsp;&nbsp;
                        <asp:RadioButton runat='server' ID='RadioButtonConjunctivalNodulesNo' GroupName='ConjunctivalNodules ' text='&nbsp;No' />
                    </td>
                </tr>
                <!-- Question 3 -->
                <tr class="table_body">
                    <td colspan="2">
                        <strong>3. Were dendrites present?</strong>
                        <asp:Label ID="LabelDendritesPresent" runat="server" Visible="false"  ForeColor="Red" Text="<br />Please complete" />
                    </td>
                    <td>
                        <asp:RadioButton runat='server' ID='RadioButtonDendritesPresentYes' GroupName='DendritesPresent' text='&nbsp;Yes' />&nbsp;&nbsp;
                        <asp:RadioButton runat='server' ID='RadioButtonDendritesPresentNo' GroupName='DendritesPresent' text='&nbsp;No' />
                    </td>
                </tr>
                <tr class="table_body">
                    <td colspan="2"><span id="QC3aA"><strong>3a. If yes, which epithelial disease was detected:</strong></span></td>
                    <td>
                        <span id="QC3aB">
                        <asp:RadioButtonList id='RadioButtonListRBEpithelialDiseaseDetected' RepeatDirection='Horizontal' CssClass='aspxList' runat='server'>
                            <asp:ListItem Value='157'>&nbsp;Herpetic</asp:ListItem>
                            <asp:ListItem Value='158'>&nbsp;Zoster Keratitis</asp:ListItem>
                        </asp:RadioButtonList>
                        </span>
                    </td>
                </tr>
                <!-- Question 4 -->
                <tr class="table_body_bg">
                    <td colspan="2">
                        <strong>4. Stromal scars</strong>
                        <asp:Label ID="LabelStromalScars" runat="server" Visible="false"  ForeColor="Red" Text="<br />Please complete" />
                    </td>
                    <td>
                        <asp:RadioButton runat='server' ID='RadioButtonStromalScarsYes' GroupName='StromalScars' text='&nbsp;Yes' />&nbsp;&nbsp;
                        <asp:RadioButton runat='server' ID='RadioButtonStromalScarsNo' GroupName='StromalScars' text='&nbsp;No' />
                    </td>
                </tr>
                <tr class="table_body_bg">
                    <td colspan="2">
                        <strong>4a. Corneal Edema</strong>
                        <asp:Label ID="LabelCornealEdema" runat="server" Visible="false"  ForeColor="Red" Text="<br />Please complete" />
                    </td>
                    <td>
                        <asp:RadioButton runat='server' ID='RadioButtonCornealEdemaYes' GroupName='CornealEdema' text='&nbsp;Yes' />&nbsp;&nbsp;
                        <asp:RadioButton runat='server' ID='RadioButtonCornealEdemaNo' GroupName='CornealEdema' text='&nbsp;No' />
                    </td>
                </tr>
                <!-- Question 5 -->
                <tr class="table_body">
                    <td colspan="2">
                        <strong>5. KPs</strong>
                        <asp:Label ID="LabelKPs" runat="server" Visible="false"  ForeColor="Red" Text="<br />Please complete" />
                    </td>
                    <td>
                        <asp:RadioButton runat='server' ID='RadioButtonKPsYes' GroupName='KPs' text='&nbsp;Yes' />&nbsp;&nbsp;
                        <asp:RadioButton runat='server' ID='RadioButtonKPsNo' GroupName='KPs' text='&nbsp;No' />
                    </td>
                </tr>
                <!-- Question 5a -->
                <tr class="table_body">
                    <td colspan="2">
                        <strong><span id="QCE5aA">5a. Appearance</span></strong>
                    </td>
                    <td>
                        <span id="QCE5aB">
                        <asp:RadioButtonList id='RadioButtonListRBKPAppearance' RepeatDirection='Vertical' CssClass='aspxList' runat='server'>
                            <asp:ListItem Value='159'>&nbsp;Granulomatous</asp:ListItem>
                            <asp:ListItem Value='160'>&nbsp;Fine</asp:ListItem>
                            <asp:ListItem Value='161'>&nbsp;Pigmented</asp:ListItem>
                            <asp:ListItem Value='128'>&nbsp;None</asp:ListItem>
                        </asp:RadioButtonList>
                        </span>
                    </td>
                </tr>
                <!-- Question 5b -->
                <tr class="table_body">
                    <td colspan="2">
                        <strong><span id="QCE5bA">5b. Location</span></strong>
                    </td>
                    <td>
                        <span id="QCE5bB">
                        <asp:RadioButtonList id='RadioButtonListRBKPLocation' RepeatDirection='Vertical' CssClass='aspxList' runat='server'>
                            <asp:ListItem Value='162'>&nbsp;Inferior (Arlt's Triangle)</asp:ListItem>
                            <asp:ListItem Value='163'>&nbsp;Diffuse</asp:ListItem>
                        </asp:RadioButtonList>
                        </span>
                    </td>
                </tr>
                <!-- Question 6 -->
                <tr class="table_body_bg">
                    <td rowspan="4" style="vertical-align: top; padding-top: 30px;">
                        <strong>6. Anterior chamber</strong>
                    </td>
                    <td>
                        <span class="right">Depth</span>
                        <asp:Label ID="LabelRBAnteriorChamberDepth" runat="server" Visible="false"  ForeColor="Red" Text="<br />Please complete" />
                    </td>
                    <td>
                        
                        <asp:RadioButtonList id='RadioButtonListRBAnteriorChamberDepth' RepeatDirection='Horizontal' CssClass='aspxList' runat='server'>
                            <asp:ListItem Value='164'>&nbsp;Flat</asp:ListItem>
                            <asp:ListItem Value='165'>&nbsp;Shallow</asp:ListItem>
                            <asp:ListItem Value='166'>&nbsp;Deep</asp:ListItem>
                            <asp:ListItem Value='167'>&nbsp;Iris Bombe</asp:ListItem>
                        </asp:RadioButtonList>
                    </td>
                </tr>
                <tr class="table_body_bg">
                    <td>
                        <span class="right">Flare description</span>
                        <asp:Label ID="LabelRBAnteriorChamberFlare" runat="server" Visible="false"  ForeColor="Red" Text="<br />Please complete" />
                    </td>
                    <td>
                        <asp:RadioButtonList id='RadioButtonListRBAnteriorChamberFlare' RepeatDirection='Vertical' CssClass='aspxList' runat='server'>
                            <asp:ListItem Value='168'>&nbsp;-0 None</asp:ListItem>
                            <asp:ListItem Value='169'>&nbsp;1+ Faint</asp:ListItem>
                            <asp:ListItem Value='170'>&nbsp;2+ Moderate Iris &amp; Lens Clear</asp:ListItem>
                            <asp:ListItem Value='171'>&nbsp;3+ Marked Iris &amp; Lens Hazy</asp:ListItem>
                            <asp:ListItem Value='172'>&nbsp;4+ Intense Fibrin/Plasmoid Aqueous</asp:ListItem>
                        </asp:RadioButtonList>
                    </td>
                </tr>
                <tr class="table_body_bg">
                    <td>
                        <span class="right">Criteria cells in field (high-intensity 1x1mm slit beam)</span>
                        <asp:Label ID="LabelRBAnteriorChamberCriteria" runat="server" Visible="false"  ForeColor="Red" Text="<br />Please complete" />
                    </td>
                    <td>
                        
                        <asp:RadioButtonList id='RadioButtonListRBAnteriorChamberCriteria' RepeatDirection='Vertical' CssClass='aspxList' runat='server'>
                            <asp:ListItem Value='230'>&nbsp;-0 &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&lt;1 Criteria Cells</asp:ListItem>
                            <asp:ListItem Value='231'>&nbsp;0.5+ &nbsp;&nbsp;1-5</asp:ListItem>
                            <asp:ListItem Value='232'>&nbsp;1+ &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;6-15</asp:ListItem>
                            <asp:ListItem Value='233'>&nbsp;2+ &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;16-25</asp:ListItem>
                            <asp:ListItem Value='234'>&nbsp;3+ &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;26-50</asp:ListItem>
                            <asp:ListItem Value='235'>&nbsp;4+ &nbsp;&nbsp;&nbsp;&nbsp;&gt;50</asp:ListItem>
                        </asp:RadioButtonList>
                    </td>
                </tr>
                <tr class="table_body_bg">
                    <td>
                        <span class="right">Was a hypopyon present?</span>
                        <asp:Label ID="LabelRBAnteriorHypopyon" runat="server" Visible="false"  ForeColor="Red" Text="<br />Please complete" />
                    </td>
                    <td>
                        <asp:RadioButton runat='server' ID='RadioButtonListRBAnteriorHypopyonYes' GroupName='RadioButtonListRBAnteriorHypopyon ' text='&nbsp;Yes' />&nbsp;&nbsp;
                        <asp:RadioButton runat='server' ID='RadioButtonListRBAnteriorHypopyonNo' GroupName='RadioButtonListRBAnteriorHypopyon ' text='&nbsp;No' />
                    </td>
                </tr>
                <!-- Question 7 -->
                <tr class="table_body">
                    <td rowspan="5" style="vertical-align: top; padding-top: 30px;"><strong>7. Iris</strong></td>
                    <td>
                        <span class="right">Was sectoral or diffuse iris atrophy present</span>
                        <asp:Label ID="LabelRBIrisAtrophy" runat="server" Visible="false"  ForeColor="Red" Text="<br />Please complete" />
                    </td>
                    <td>
                        <asp:RadioButtonList id='RadioButtonListRBIrisAtrophy' RepeatDirection='Horizontal' CssClass='aspxList' runat='server'>
                            <asp:ListItem Value='173'>&nbsp;Sectoral</asp:ListItem>
                            <asp:ListItem Value='174'>&nbsp;Diffuse</asp:ListItem>
                            <asp:ListItem Value='128'>&nbsp;None</asp:ListItem>
                        </asp:RadioButtonList>
                    </td>
                </tr>
                <tr class="table_body">
                    <td>
                        <span class="right">Was heterochromia present?</span>
                        <asp:Label ID="LabelIrisHeterochromia" runat="server" Visible="false"  ForeColor="Red" Text="<br />Please complete" />
                    </td>
                    <td>
                        <asp:RadioButton runat='server' ID='RadioButtonIrisHeterochromiaYes' GroupName='IrisHeterochromia ' text='&nbsp;Yes' />&nbsp;&nbsp;
                        <asp:RadioButton runat='server' ID='RadioButtonIrisHeterochromiaNo' GroupName='IrisHeterochromia ' text='&nbsp;No' />
                    </td>
                </tr>
                <tr class="table_body">
                    <td>
                        <span class="right">Previous peripheral iridotomy present?</span>
                        <asp:Label ID="LabelIrisPreviousPeripheralIridotomy" runat="server" Visible="false"  ForeColor="Red" Text="<br />Please complete" />
                    </td>
                    <td>
                        <asp:RadioButton runat='server' ID='RadioButtonIrisPreviousPeripheralIridotomyYes' GroupName='IrisPreviousPeripheralIridotomy ' text='&nbsp;Yes' />&nbsp;&nbsp;
                        <asp:RadioButton runat='server' ID='RadioButtonIrisPreviousPeripheralIridotomyNo' GroupName='IrisPreviousPeripheralIridotomy ' text='&nbsp;No' />
                    </td>
                </tr>
                <tr class="table_body">
                    <td>
                        <span class="right">Were nodules present?</span>
                        <asp:Label ID="LabelRBIrisNodulesPresent" runat="server" Visible="false"  ForeColor="Red" Text="<br />Please complete" />
                    </td>
                    <td>
                        <asp:RadioButtonList id='RadioButtonListRBIrisNodulesPresent' RepeatDirection='Horizontal' CssClass='aspxList' runat='server'>
                            <asp:ListItem Value='175'>&nbsp;Present</asp:ListItem>
                            <asp:ListItem Value='176'>&nbsp;Not Present</asp:ListItem>
                        </asp:RadioButtonList>
                    </td>
                </tr>
                <tr class="table_body">
                    <td><span class="right" id="QCE7eA">If present, please identify nodule:</span></td>
                    <td>
                        <span id="QCE7eB">
                        <asp:RadioButtonList id='RadioButtonListRBIrisNoduleID' RepeatDirection='Horizontal' CssClass='aspxList' runat='server'>
                            <asp:ListItem Value='177'>&nbsp;Koeppe</asp:ListItem>
                            <asp:ListItem Value='178'>&nbsp;Busacca</asp:ListItem>
                        </asp:RadioButtonList>
                        Extent in clock hours: <asp:TextBox runat='server' ID='TextBoxRBIrisNoduleClockHours' size='10' />
                        </span>
                    </td>
                </tr>
                <tr class="table_body" >
                    <td rowspan="2"><strong>7a. Posterior synechiae</strong></td>
          
                    <td>
                        <span class="right">Was a gonioscopy performed?</span>
                        <asp:Label ID="LabelGonioscopyPerformed" runat="server" Visible="false"  ForeColor="Red" Text="<br />Please complete" />
                    </td>
                    <td>
                        <asp:RadioButton runat='server' ID='RadioButtonGonioscopyPerformedYes' GroupName='GonioscopyPerformed' text='&nbsp;Yes' />&nbsp;&nbsp;
                        <asp:RadioButton runat='server' ID='RadioButtonGonioscopyPerformedNo' GroupName='GonioscopyPerformed' text='&nbsp;No' />
                    </td>
                </tr>
                <tr class="table_body">
                    <td><span class="right">Number of clock hours, of post synechiae</span></td>
                    <td>
                        <asp:RadioButtonList id='RadioButtonListRBGonioscopyNumberClockHours' RepeatDirection='Horizontal' CssClass='aspxList' runat='server'>
                            <asp:ListItem Value='179'>&nbsp;0</asp:ListItem>
                            <asp:ListItem Value='180'>&nbsp;1-3</asp:ListItem>
                            <asp:ListItem Value='181'>&nbsp;4-6</asp:ListItem>
                            <asp:ListItem Value='182'>&nbsp;7-9</asp:ListItem>
                            <asp:ListItem Value='183'>&nbsp;10-12</asp:ListItem>
                        </asp:RadioButtonList>
                    </td>
                </tr>
                <!-- Question 8 -->
                <tr class="table_body_bg">
                    <td rowspan="2"><strong>8. Lens</strong></td>
                    <td>
                        <span class="right">Pigment on anterior lens capsule:</span>
                        <asp:Label ID="LabelLensPigment" runat="server" Visible="false"  ForeColor="Red" Text="<br />Please complete" />
                    </td>
                    <td>
                        <asp:RadioButton runat='server' ID='RadioButtonLensPigmentYes' GroupName='LensPigment ' text='&nbsp;Yes' />&nbsp;&nbsp;
                        <asp:RadioButton runat='server' ID='RadioButtonLensPigmentNo' GroupName='LensPigment ' text='&nbsp;No' />
                    </td>
                </tr>
                <tr class="table_body_bg">
                    <td>
                        <span class="right">Cataract present:</span>
                        <asp:Label ID="LabelLensCataractPresent" runat="server" Visible="false"  ForeColor="Red" Text="<br />Please complete" />
                    </td>
                    <td>
                        <asp:RadioButton runat='server' ID='RadioButtonLensCataractPresentYes' GroupName='LensCataractPresent ' text='&nbsp;Yes' />&nbsp;&nbsp;
                        <asp:RadioButton runat='server' ID='RadioButtonLensCataractPresentNo' GroupName='LensCataractPresent ' text='&nbsp;No' />
                    </td>
                </tr>
                <!-- Question 9 -->
                <tr class="table_body">
                    <td colspan="2">
                        <strong>9. Scleritis</strong>
                        <asp:Label ID="LabelScleritis" runat="server" Visible="false"  ForeColor="Red" Text="<br />Please complete" />
                    </td>
                    <td>
                        <asp:RadioButtonList id='RadioButtonListScleritis' RepeatDirection='Horizontal' CssClass='aspxList' runat='server'>
                            <asp:ListItem Value="1">&nbsp;Yes</asp:ListItem>
	                        <asp:ListItem Value="2">&nbsp;No</asp:ListItem>
	                        <asp:ListItem Value="3">&nbsp;Not Recorded</asp:ListItem>
                        </asp:RadioButtonList>
                    </td>
                </tr>
                <!-- Question 9a -->
                <tr class="table_body">
                    <td colspan="2"><span id="QC9aA"><strong>9a. Nodular</strong></span></td>
                    <td>
                        <span id="QC9aB">
                        <asp:RadioButton runat='server' ID='RadioButtonScleritisNodularYes' GroupName='ScleritisNodular ' text='&nbsp;Yes' />&nbsp;&nbsp;
                        <asp:RadioButton runat='server' ID='RadioButtonScleritisNodularNo' GroupName='ScleritisNodular ' text='&nbsp;No' />
                        </span>
                    </td>
                </tr>
                <!-- Question 9b -->
                <tr class="table_body">
                    <td colspan="2"><span id="QC9bA"><strong>9b. Diffuse</strong></span></td>
                    <td>
                        <span id="QC9bB">
                        <asp:RadioButton runat='server' ID='RadioButtonScleritisDiffuseYes' GroupName='ScleritisDiffuse ' text='&nbsp;Yes' />&nbsp;&nbsp;
                        <asp:RadioButton runat='server' ID='RadioButtonScleritisDiffuseNo' GroupName='ScleritisDiffuse ' text='&nbsp;No' />
                        </span>
                    </td>
                </tr>
                <!-- Question 9c -->
                <tr class="table_body">
                    <td colspan="2"><span id="QC9cA"><strong>9c. Necrotizing</strong></span></td>
                    <td>
                        <span id="QC9cB">
                        <asp:RadioButton runat='server' ID='RadioButtonScleritisNecrotizingYes' GroupName='ScleritisNecrotizing ' text='&nbsp;Yes' />&nbsp;&nbsp;
                        <asp:RadioButton runat='server' ID='RadioButtonScleritisNecrotizingNo' GroupName='ScleritisNecrotizing ' text='&nbsp;No' />
                        </span>
                    </td>
                </tr>
                <!-- Question 10 -->
                <tr class="table_body_bg">
                    <td colspan="2">
                        <strong>10.	Intraocular pressure</strong>
                        <asp:Label ID="LabelIntraocularPressure" runat="server" Visible="false"  ForeColor="Red" Text="<br />Please complete" />    
                    </td>
                    <td>
                        <asp:TextBox runat='server' ID='TextBoxIntraocularPressure' size='5' /> mmHg
                    </td>
                </tr>
                <!-- Question 11 -->
                <tr class="table_body">
                    <td colspan="2">
                        <strong>11. Vitreous cells</strong>
                        <asp:Label ID="LabelVitreousCells" runat="server" Visible="false"  ForeColor="Red" Text="<br />Please complete" />
                    </td>
                    <td>
                        <asp:RadioButtonList id='RadioButtonListVitreousCells' 
                            RepeatDirection='Horizontal' CssClass='aspxList' runat='server'>
                            <asp:ListItem Value="1">&nbsp;Yes</asp:ListItem>
	                        <asp:ListItem Value="2">&nbsp;No</asp:ListItem>
	                        <asp:ListItem Value="3">&nbsp;Not Documented</asp:ListItem>
                        </asp:RadioButtonList>
                        </td>
                </tr>
                <!-- Question 11a -->
                <tr class="table_body">
                    <td colspan="2">
                        <strong>11a. Vitreous haze</strong>
                        <asp:Label ID="LabelRBVitreousHaze" runat="server" Visible="false"  ForeColor="Red" Text="<br />Please complete" />
                    </td>
                    <td>
                        <asp:RadioButtonList id='RadioButtonListRBVitreousHaze' RepeatDirection='Vertical' CssClass='aspxList' runat='server'>
                            <asp:ListItem Value='128'>&nbsp;None</asp:ListItem>
                            <asp:ListItem Value='184'>&nbsp;0.5+</asp:ListItem>
                            <asp:ListItem Value='185'>&nbsp;1+</asp:ListItem>
                            <asp:ListItem Value='186'>&nbsp;2+</asp:ListItem>
                            <asp:ListItem Value='187'>&nbsp;More</asp:ListItem>
                             <asp:ListItem Value='3'>&nbsp;Not Documented</asp:ListItem>
                        </asp:RadioButtonList>
                    </td>
                </tr>
                <!-- Question 12 -->
                <tr class="table_body_bg">
                    <td rowspan="2"><strong>12. Optic disc</strong></td>
                    <td>
                        <span class="right">Was edema present?</span>
                        <asp:Label ID="LabelOpticDiscEdemaPresent" runat="server" Visible="false"  ForeColor="Red" Text="<br />Please complete" />
                    </td>
                    <td>
                        <asp:RadioButton runat='server' ID='RadioButtonOpticDiscEdemaPresentYes' GroupName='OpticDiscEdemaPresent ' text='&nbsp;Yes' />&nbsp;&nbsp;
                        <asp:RadioButton runat='server' ID='RadioButtonOpticDiscEdemaPresentNo' GroupName='OpticDiscEdemaPresent ' text='&nbsp;No' />
                    </td>
                </tr>
                <tr class="table_body_bg">
                    <td>
                        <span class="right">Are glaucomatous changes present?</span>
                        <asp:Label ID="LabelOpticDiscGlaucomatousChanges" runat="server" Visible="false"  ForeColor="Red" Text="<br />Please complete" />    
                    </td>
                    <td>
                        <asp:RadioButton runat='server' ID='RadioButtonOpticDiscGlaucomatousChangesYes' GroupName='OpticDiscGlaucomatousChanges ' text='&nbsp;Yes' />&nbsp;&nbsp;
                        <asp:RadioButton runat='server' ID='RadioButtonOpticDiscGlaucomatousChangesNo' GroupName='OpticDiscGlaucomatousChanges ' text='&nbsp;No' />
                    </td>
                </tr>
                <!-- Question 13 -->
                <tr class="table_body">
                    <td rowspan="4" style="vertical-align: top; padding-top: 30px;"><strong>13. Retina and choroid</strong></td>
                    <td>
                        <span class="right">Dilated fundus exam performed?</span>
                        <asp:Label ID="LabelDilatedFundusExamPerformed" runat="server" Visible="false"  ForeColor="Red" Text="<br />Please complete" />
                    </td>
                    <td>
                        <asp:RadioButton runat='server' ID='RadioButtonDilatedFundusExamPerformedYes' GroupName='DilatedFundusExamPerformed ' text='&nbsp;Yes' />&nbsp;&nbsp;
                        <asp:RadioButton runat='server' ID='RadioButtonDilatedFundusExamPerformedNo' GroupName='DilatedFundusExamPerformed ' text='&nbsp;No' />
                    </td>
                </tr>
                <tr class="table_body">
                    <td>
                        <span class="right">CME present?</span>
                        <asp:Label ID="LabelCMEPresent" runat="server" Visible="false"  ForeColor="Red" Text="<br />Please complete" />
                    </td>
                    <td>
                        <asp:RadioButton runat='server' ID='RadioButtonCMEPresentYes' GroupName='CMEPresent ' text='&nbsp;Yes' />&nbsp;&nbsp;
                        <asp:RadioButton runat='server' ID='RadioButtonCMEPresentNo' GroupName='CMEPresent ' text='&nbsp;No' />
                    </td>
                </tr>
                <tr class="table_body">
                    <td><span class="right" id="QC13A">If yes, please indicate the following: OCT – Central subfield thickness</span></td>
                    <td>
                        <asp:TextBox runat='server' ID='TextBoxOCTCentralSubfieldThickness' size='5' />
                    </td>
                </tr>
                <tr class="table_body">
                    <td>
                        <span class="right">Fluorescein angiogram performed?</span>
                        <asp:Label ID="LabelFluoresceinPerformed" runat="server" Visible="false"  ForeColor="Red" Text="<br />Please complete" />
                    </td>
                    <td>
                        <asp:RadioButton runat='server' ID='RadioButtonFluoresceinPerformedYes' GroupName='FluoresceinPerformed ' text='&nbsp;Yes' />&nbsp;&nbsp;
                        <asp:RadioButton runat='server' ID='RadioButtonFluoresceinPerformedNo' GroupName='FluoresceinPerformed ' text='&nbsp;No' />
                    </td>
                </tr>
                <tr class="table_body">
                    <td rowspan="11" style="vertical-align: top; padding-top: 30px;"><strong>13a. Chorioretinal pathology</strong></td>
                    <td>
                        <span class="right">Chorioretinal pathology?</span>
                        <asp:Label ID="LabelRBChorioretinalPathology" runat="server" Visible="false"  ForeColor="Red" Text="<br />Please complete" />    
                    </td>
                    <td>
                        <asp:RadioButtonList id='RadioButtonListRBChorioretinalPathology' RepeatDirection='Horizontal' CssClass='aspxList' runat='server'>
                            <asp:ListItem Value='175'>&nbsp;Present</asp:ListItem>
                            <asp:ListItem Value='176'>&nbsp;Absent</asp:ListItem>
                        </asp:RadioButtonList>
                    </td>
                </tr>
                <tr class="table_body">
                    <td colspan="2"><span id="QC13fHeader"><strong>If chorioretinal pathology present, describe:</strong></span></td>
                </tr>
                <tr class="table_body">
                    <td>
                        <span class="right" id="QC13fA">Retinal venous sheathing</span>
                        <asp:Label ID="LabelRBChorioretinalRVSheathing" runat="server" Visible="false"  ForeColor="Red" Text="<br />Please complete" />
                    </td>
                    <td>
                        <span id="QC13fB">
                        <asp:RadioButtonList id='RadioButtonListRBChorioretinalRVSheathing' RepeatDirection='Horizontal' CssClass='aspxList' runat='server'>
                            <asp:ListItem Value='175'>&nbsp;Present</asp:ListItem>
                            <asp:ListItem Value='176'>&nbsp;Absent</asp:ListItem>
                        </asp:RadioButtonList>
                        </span>
                    </td>
                </tr>
                <tr class="table_body">
                    <td>
                        <span class="right" id="QC13fC">Retinal vasculitis - active - (hemorrhages, and perivascular inflammation)</span>
                        <asp:Label ID="LabelRBChorioretinalRVActive" runat="server" Visible="false"  ForeColor="Red" Text="<br />Please complete" />
                    </td>
                    <td>
                        <span id="QC13fD">
                        <asp:RadioButtonList id='RadioButtonListRBChorioretinalRVActive' RepeatDirection='Horizontal' CssClass='aspxList' runat='server'>
                            <asp:ListItem Value='175'>&nbsp;Present</asp:ListItem>
                            <asp:ListItem Value='176'>&nbsp;Absent</asp:ListItem>
                        </asp:RadioButtonList>
                        </span>
                    </td>
                </tr>
                <tr class="table_body">
                    <td>
                        <span class="right" id="QC13fE">Retinitis (necrotizing)</span>
                        <asp:Label ID="LabelRBChorioretinalRetinitis" runat="server" Visible="false"  ForeColor="Red" Text="<br />Please complete" />
                    </td>
                    <td>
                        <span id="QC13fF">
                        <asp:RadioButtonList id='RadioButtonListRBChorioretinalRetinitis' RepeatDirection='Horizontal' CssClass='aspxList' runat='server'>
                            <asp:ListItem Value='175'>&nbsp;Present</asp:ListItem>
                            <asp:ListItem Value='176'>&nbsp;Absent</asp:ListItem>
                        </asp:RadioButtonList>
                        </span>
                    </td>
                </tr>
                <tr class="table_body">
                    <td>
                        <span class="right" id="QC13fG">Retinal neovascularization</span>
                        <asp:Label ID="LabelRBChorioretinalRetinalNeovasc" runat="server" Visible="false"  ForeColor="Red" Text="<br />Please complete" />
                    </td>
                    <td>
                        <span id="QC13fH">
                        <asp:RadioButtonList id='RadioButtonListRBChorioretinalRetinalNeovasc' RepeatDirection='Horizontal' CssClass='aspxList' runat='server'>
                            <asp:ListItem Value='175'>&nbsp;Present</asp:ListItem>
                            <asp:ListItem Value='176'>&nbsp;Absent</asp:ListItem>
                        </asp:RadioButtonList>
                        </span>
                    </td>
                </tr>
                <tr class="table_body">
                    <td>
                        <span class="right" id="QC13fI">Choroiditis</span>
                        <asp:Label ID="LabelRBChoroiditis" runat="server" Visible="false"  ForeColor="Red" Text="<br />Please complete" />
                    </td>
                    <td>
                        <span id="QC13fJ">
                        <asp:RadioButtonList id='RadioButtonListRBChoroiditis' RepeatDirection='Vertical' CssClass='aspxList' runat='server'>
                            <asp:ListItem Value='175'>&nbsp;Present</asp:ListItem>
                            <asp:ListItem Value='176'>&nbsp;Absent</asp:ListItem>
                            <asp:ListItem Value='236'>&nbsp;Active</asp:ListItem>
                            <asp:ListItem Value='188'>&nbsp;Inactive</asp:ListItem>
                            <asp:ListItem Value='189'>&nbsp;Posterior pole</asp:ListItem>
                            <asp:ListItem Value='190'>&nbsp;Peripheral</asp:ListItem>
                            <asp:ListItem Value='191'>&nbsp;Multifocal</asp:ListItem>
                            <asp:ListItem Value='192'>&nbsp;Unifocal</asp:ListItem>
                        </asp:RadioButtonList>
                        </span>
                    </td>
                </tr>
                <tr class="table_body">
                    <td>
                        <span class="right">Choroidal neovascularization</span>
                        <asp:Label ID="LabelRBChoroidalNeovascularization" runat="server" Visible="false"  ForeColor="Red" Text="<br />Please complete" />
                    </td>
                    <td>
                        <asp:RadioButtonList id='RadioButtonListRBChoroidalNeovascularization' RepeatDirection='Vertical' CssClass='aspxList' runat='server'>
                            <asp:ListItem Value='175'>&nbsp;Present</asp:ListItem>
                            <asp:ListItem Value='176'>&nbsp;Absent</asp:ListItem>
                            <asp:ListItem Value='193'>&nbsp;Peripapillary</asp:ListItem>
                            <asp:ListItem Value='194'>&nbsp;Extrafoveal</asp:ListItem>
                            <asp:ListItem Value='195'>&nbsp;Juxtafoveal</asp:ListItem>
                            <asp:ListItem Value='196'>&nbsp;Subfoveal</asp:ListItem>
                        </asp:RadioButtonList>
                    </td>
                </tr>
                <tr class="table_body">
                    <td>
                        <span class="right">Retinal tear</span>
                        <asp:Label ID="LabelRBRetinalTear" runat="server" Visible="false"  ForeColor="Red" Text="<br />Please complete" />
                    </td>
                    <td>
                        <asp:RadioButtonList id='RadioButtonListRBRetinalTear' RepeatDirection='Vertical' CssClass='aspxList' runat='server'>
                            <asp:ListItem Value='175'>&nbsp;Present</asp:ListItem>
                            <asp:ListItem Value='176'>&nbsp;Absent</asp:ListItem>
                        </asp:RadioButtonList>
                    </td>
                </tr>
                <tr class="table_body">
                    <td>
                        <span class="right">Retinal detachment</span>
                        <asp:Label ID="LabelRBRetinalDetachment" runat="server" Visible="false"  ForeColor="Red" Text="<br />Please complete" />
                    </td>
                    <td>
                        <asp:RadioButtonList id='RadioButtonListRBRetinalDetachment' RepeatDirection='Vertical' CssClass='aspxList' runat='server'>
                            <asp:ListItem Value='175'>&nbsp;Present</asp:ListItem>
                            <asp:ListItem Value='176'>&nbsp;Absent</asp:ListItem>
                            <asp:ListItem Value='197'>&nbsp;Tractional</asp:ListItem>
                            <asp:ListItem Value='198'>&nbsp;Exudative</asp:ListItem>
                            <asp:ListItem Value='199'>&nbsp;Rhegmatogenous</asp:ListItem>
                            <asp:ListItem Value='14'>&nbsp;Other</asp:ListItem>
                        </asp:RadioButtonList>
                    </td>
                </tr>
                <tr class="table_body">
                    <td><span class="right" id="Q13aText">If other, please specify</span></td>
                    <td>
                        <asp:TextBox runat='server' ID='TextBoxRBRetinalDetachmentOther' size='25' />
                    </td>
                </tr>
            </table>
            <br />
            <!-- Management (Labratory Work-Up) -->
            <table>
                <tr>
                    <th colspan="3" width="910px"><p>Management (Labratory Work-Up)</p></th>
                </tr>
                <!-- Question 1 -->
                <tr class="table_body">
                    <td width="35%" rowspan="10" style="vertical-align: top; padding-top: 30px;">
                        <strong>1. Results of testing:</strong>
                    </td>
                    <td width="25%"><span class="right">FTA-ABS<br />
                        <asp:Label ID="LabelRBTestFTAABS" runat="server" Visible="false"  ForeColor="Red" Text="<br />Please complete" />
                        </span></td>
                    <td>
                        <asp:RadioButtonList id='RadioButtonListRBTestFTAABS' RepeatDirection='Horizontal' CssClass='aspxList' runat='server'>
                            <asp:ListItem Value='200'>&nbsp;Reactive</asp:ListItem>
                            <asp:ListItem Value='201'>&nbsp;Non-Reactive</asp:ListItem>
                            <asp:ListItem Value='3'>&nbsp;Not&nbsp;Performed</asp:ListItem>
                        </asp:RadioButtonList>
                    </td>
                </tr>
                <tr class="table_body">
                    <td>
                        <span class="right">MHA-TP</span>
                        <asp:Label ID="LabelRBTestMHATP" runat="server" Visible="false"  ForeColor="Red" Text="<br />Please complete" />
                    </td>
                    <td>
                        <asp:RadioButtonList id='RadioButtonListRBTestMHATP' RepeatDirection='Horizontal' CssClass='aspxList' runat='server'>
                            <asp:ListItem Value='200'>&nbsp;Reactive</asp:ListItem>
                            <asp:ListItem Value='201'>&nbsp;Non-Reactive</asp:ListItem>
                            <asp:ListItem Value='3'>&nbsp;Not&nbsp;Performed</asp:ListItem>
                        </asp:RadioButtonList>
                    </td>
                </tr>
                    <tr class="table_body">
                    <td>
                        <span class="right">RPR</span>
                        <asp:Label ID="Label2" runat="server" Visible="false"  ForeColor="Red" Text="<br />Please complete" />
                    </td>
                    <td>
                        <asp:RadioButtonList id='RadioButtonListRPR' RepeatDirection='Horizontal' CssClass='aspxList' runat='server'>
                            <asp:ListItem Value='200'>&nbsp;Reactive</asp:ListItem>
                            <asp:ListItem Value='201'>&nbsp;Non-Reactive</asp:ListItem>
                            <asp:ListItem Value='3'>&nbsp;Not&nbsp;Performed</asp:ListItem>
                        </asp:RadioButtonList>
                    </td>
                </tr>
                <tr class="table_body">
                    <td>
                        <span class="right">HLAB27</span>
                        <asp:Label ID="LabelRBTestHLAB" runat="server" Visible="false"  ForeColor="Red" Text="<br />Please complete" />    
                    </td>
                    <td>
                        <asp:RadioButtonList id='RadioButtonListRBTestHLAB27' RepeatDirection='Horizontal' CssClass='aspxList' runat='server'>
                            <asp:ListItem Value='200'>&nbsp;Reactive</asp:ListItem>
                            <asp:ListItem Value='201'>&nbsp;Non-Reactive</asp:ListItem>
                            <asp:ListItem Value='3'>&nbsp;Not&nbsp;Performed</asp:ListItem>
                        </asp:RadioButtonList>
                    </td>
                </tr>
                <tr class="table_body">
                    <td>
                        <span class="right">Angiotensin converting enzyme</span>
                        <asp:Label ID="LabelRBTestACE" runat="server" Visible="false"  ForeColor="Red" Text="<br />Please complete" />
                    </td>
                    <td>
                        <asp:RadioButtonList id='RadioButtonListRBTestACE' RepeatDirection='Horizontal' CssClass='aspxList' runat='server'>
                            <asp:ListItem Value='104'>&nbsp;Normal</asp:ListItem>
                            <asp:ListItem Value='202'>&nbsp;Elevated</asp:ListItem>
                            <asp:ListItem Value='3'>&nbsp;Not&nbsp;Performed</asp:ListItem>
                        </asp:RadioButtonList>
                    </td>
                </tr>
                <tr class="table_body">
                    <td>
                        <span class="right">Chest radiograph</span>
                        <asp:Label ID="LabelRBTestChest" runat="server" Visible="false"  ForeColor="Red" Text="<br />Please complete" />
                    </td>
                    <td>
                        <asp:RadioButtonList id='RadioButtonListRBTestChest' RepeatDirection='Vertical' CssClass='aspxList' runat='server'>
                            <asp:ListItem Value='104'>&nbsp;Normal</asp:ListItem>
                            <asp:ListItem Value='105'>&nbsp;Abnormal</asp:ListItem>
                            <asp:ListItem Value='204'>&nbsp;Signs of sarcoidosis</asp:ListItem>
                            <asp:ListItem Value='205'>&nbsp;Negative</asp:ListItem>
                            <asp:ListItem Value='203'>&nbsp;Not&nbsp;Performed</asp:ListItem>
                        </asp:RadioButtonList>
                    </td>
                </tr>
                <tr class="table_body">
                    <td>
                        <span class="right">Signs of tuberculosis</span>
                        <asp:Label ID="LabelRBTestTB" runat="server" Visible="false"  ForeColor="Red" Text="<br />Please complete" />
                    </td>
                    <td>
                        <asp:RadioButtonList id='RadioButtonListRBTestTB' RepeatDirection='Horizontal' CssClass='aspxList' runat='server'>
                             <asp:ListItem Value='343'>&nbsp;Positive</asp:ListItem>
                            <asp:ListItem Value='205'>&nbsp;Negative</asp:ListItem>
                            <asp:ListItem Value='203'>&nbsp;Not&nbsp;Performed</asp:ListItem>
                        </asp:RadioButtonList>
                    </td>
                </tr>
                <tr class="table_body">
                    <td>
                        <span class="right">PPD skin test</span>
                        <asp:Label ID="LabelRBTestPPD" runat="server" Visible="false"  ForeColor="Red" Text="<br />Please complete" />
                    </td>
                    <td>
                        <asp:RadioButtonList id='RadioButtonListRBTestPPD' RepeatDirection='Horizontal' CssClass='aspxList' runat='server'>
                           <asp:ListItem Value='343'>&nbsp;Positive</asp:ListItem>
                            <asp:ListItem Value='205'>&nbsp;Negative</asp:ListItem>
                            <asp:ListItem Value='203'>&nbsp;Not&nbsp;Performed</asp:ListItem>
                        </asp:RadioButtonList>
                    </td>
                </tr>
                <tr class="table_body">
                    <td>
                        <span class="right">Quantiferon TB</span>
                        <asp:Label ID="LabelRBTestQuantiferonTB" runat="server" Visible="false"  ForeColor="Red" Text="<br />Please complete" />
                    </td>
                    <td>
                        <asp:RadioButtonList id='RadioButtonListRBTestQuantiferonTB' RepeatDirection='Horizontal' CssClass='aspxList' runat='server'>
                            <asp:ListItem Value='205'>&nbsp;Negative</asp:ListItem>
                            <asp:ListItem Value='202'>&nbsp;Elevated</asp:ListItem>
                            <asp:ListItem Value='203'>&nbsp;Not&nbsp;Performed</asp:ListItem>
                        </asp:RadioButtonList>
                    </td>
                </tr>
                <tr class="table_body">
                    <td>
                        <span class="right">Other tests</span> 
                    </td>
                    <td>
                        <asp:TextBox runat='server' ID='TextBoxRBTestOther' size='25' />
                    </td>
                </tr>

                <!-- Question 3 -->
                <tr class="table_body_bg">
                    <td rowspan="3"><strong>2. Therapy corticosteroids</strong></td>
                    <td>
                        <span class="right">Topical agent</span>
                        <asp:Label ID="LabelRBTopical" runat="server" Visible="false"  ForeColor="Red" Text="<br />Please complete" />
                    </td>
                    <td>
                        <asp:RadioButtonList id='RadioButtonListRBTopical' RepeatDirection='Vertical' CssClass='aspxList' runat='server'>
                            <asp:ListItem Value='206'>&nbsp;Prednisolone</asp:ListItem>
                            <asp:ListItem Value='344'>&nbsp;Dexamethasone</asp:ListItem>
                            <asp:ListItem Value='207'>&nbsp;Fluorometholone</asp:ListItem>
                            <asp:ListItem Value='14'>&nbsp;Others</asp:ListItem>
                        </asp:RadioButtonList>
                        <br />
                        <span id="QM3aOther">Other:</span>&nbsp;<asp:TextBox ID="TextBoxTopicalOther" runat="server"></asp:TextBox>
                    </td>
                </tr>
                <tr class="table_body_bg">
                    <td><span class="right">Periocular injections (agent)</span>
                    <asp:Label ID="LabelRBPeriocularInjections" runat="server" Visible="false"  ForeColor="Red" Text="<br />Please complete" />
                    </td>
                    <td>
                        <asp:RadioButtonList id='RadioButtonListRBPeriocularInjections' RepeatDirection='Vertical' CssClass='aspxList' runat='server'>
                            <asp:ListItem Value='208'>&nbsp;Triamcinolone</asp:ListItem>
                            <asp:ListItem Value='209'>&nbsp;Depo-Methylprednisolone</asp:ListItem>
                              <asp:ListItem Value='14'>&nbsp;Others</asp:ListItem>
                        </asp:RadioButtonList>
                        <br /><span id="QM3aOther2">Other:</span>&nbsp;<asp:TextBox ID="PeriocularInjectionsText" runat="server"></asp:TextBox>
                        
                    </td>
                </tr>
                <tr class="table_body_bg">
                    <td><span class="right">Total number of injections given:</span></td>
                    <td>
                        Number of injections: <asp:TextBox runat='server' ID='TextBoxRBPeriocularInjectionsCount' size='5' />
                    </td>
                </tr>
                <tr class="table_body_bg">
                    <td rowspan="2"><strong>2a. Intravitreal injections - 
                    <br />Preservative free triamcinolone</strong></td>
               
                    <td><span class="right">Total number of injections given:</span>
                   
                    </td>
                    <td>
                        Number of injections: <asp:TextBox runat='server' ID='TextBoxRBIntravitrealInjectionsCount' size='5' />
                    </td>
                </tr>
                <tr class="table_body_bg">
                    <td><span class="right">Given for anterior uveitis or CME?</span>
                     <asp:Label ID="LabelRBIntravitrealInjectionsForAnterior" runat="server" Visible="false"  ForeColor="Red" Text="<br />Please complete" />
                    </td>
                    <td>
                        <asp:RadioButtonList id='RadioButtonListRBIntravitrealInjectionsForAnterior' RepeatDirection='Horizontal' CssClass='aspxList' runat='server'>
                            <asp:ListItem Value='224'>&nbsp;Anterior Uveitis</asp:ListItem>
                            <asp:ListItem Value='225'>&nbsp;CME</asp:ListItem>
                        </asp:RadioButtonList>
                    </td>
                </tr>
                <!-- Question 2b -->
                <tr class="table_body_bg">
                    <td rowspan="2">
                        <strong>2b. Intravitreal injections - 
                        <br />Anti-VEGF agents</strong>
                    </td>
                    <td>
                        <span class="right">Total number of injections given:</span>
                   
                    </td>
                    <td>
                        Number of injections:&nbsp;<asp:TextBox runat='server' ID='TextBoxVEGFInjectionsGiven' size='5' />
                    </td>
                </tr>
                <tr class="table_body_bg">
                    <td>
                        <span class="right">Given for CME or neovascularization or other?</span>
                   
                    </td>
                    <td>
                        <asp:RadioButtonList id='RadioButtonListVEGFGivenForCME' RepeatDirection='Horizontal' CssClass='aspxList' runat='server'>
                            <asp:ListItem Value='348'>&nbsp;CME</asp:ListItem>
                            <asp:ListItem Value='349'>&nbsp;Neovascularization</asp:ListItem>
                            <asp:ListItem Value='14'>&nbsp;Other</asp:ListItem>
                        </asp:RadioButtonList>
                    </td>
                </tr>
                <!-- Question 2c -->
                <tr class="table_body_bg">
                    <td rowspan="2"><strong>2c. Systemic</strong></td>
             
                    <td><span class="right">Drug</span>
                    <asp:Label ID="LabelRBTherapySystemicDrug" runat="server" Visible="false"  ForeColor="Red" Text="<br />Please complete" />
                    
                    </td>
                    <td>
                        <asp:RadioButtonList id='RadioButtonListRBTherapySystemicDrug' RepeatDirection='Horizontal' CssClass='aspxList' runat='server'>
                            <asp:ListItem Value='210'>&nbsp;Prednisone</asp:ListItem>
                            <asp:ListItem Value='14'>&nbsp;Others</asp:ListItem>
                        </asp:RadioButtonList>
                    </td>
                </tr>
                <tr class="table_body_bg">
                    <td><span class="right">Duration of taper</span></td>
                    <td>
                        Number of weeks: <asp:TextBox runat='server' ID='TextBoxRBTherapySystemicDurationTaper' size='5' />
                    </td>
                </tr>
                <!-- Question 3 -->
                <tr class="table_body">
                    <td rowspan="2" style="vertical-align: top; padding-top: 30px;"><strong>3. Cycloplegics</strong></td>
                    <td><span class="right">Drug</span>
                     <asp:Label ID="LabelRBCycloplegicsDrug" runat="server" Visible="false"  ForeColor="Red" Text="<br />Please complete" />
                    </td>
                    <td>
                        <asp:RadioButtonList id='RadioButtonListRBCycloplegicsDrug' RepeatDirection='Vertical' CssClass='aspxList' runat='server'>
                            <asp:ListItem Value='211'>&nbsp;Atropine - 1%</asp:ListItem>
                            <asp:ListItem Value='212'>&nbsp;Cyclopentolate - 1%</asp:ListItem>
                            <asp:ListItem Value='213'>&nbsp;Cyclopentolate - 2%</asp:ListItem>
                            <asp:ListItem Value='214'>&nbsp;Homatropine - 2%</asp:ListItem>
                            <asp:ListItem Value='215'>&nbsp;Homatropine - 5%</asp:ListItem>
                            <asp:ListItem Value='216'>&nbsp;Scopolamine - 0.25%</asp:ListItem>
                            <asp:ListItem Value='217'>&nbsp;Tropicamide - 0.5%</asp:ListItem>
                            <asp:ListItem Value='218'>&nbsp;Tropicamide - 1%</asp:ListItem>
                        </asp:RadioButtonList>
                    </td>
                </tr>
                <tr class="table_body">
                    <td><span class="right">Frequency</span>
                     <asp:Label ID="LabelRBCycloplegicsFrequency" runat="server" Visible="false"  ForeColor="Red" Text="<br />Please complete" />
                    </td>
                    <td>
                        <asp:RadioButtonList id='RadioButtonListRBCycloplegicsFrequency' RepeatDirection='Vertical' CssClass='aspxList' runat='server'>
                            <asp:ListItem Value='219'>&nbsp;W=Qweek</asp:ListItem>
                            <asp:ListItem Value='220'>&nbsp;1=Qday</asp:ListItem>
                            <asp:ListItem Value='221'>&nbsp;2=BID</asp:ListItem>
                            <asp:ListItem Value='222'>&nbsp;3=TID</asp:ListItem>
                            <asp:ListItem Value='223'>&nbsp;4=QID</asp:ListItem>
                        </asp:RadioButtonList>
                    </td>
                </tr>
                <!-- Question 4 -->
                <tr class="table_body_bg">
                    <td colspan="2">
                        <strong>4. Immunomodulatory therapy</strong>
                        <asp:Label ID="LabelImmunomodulatoryTherapy" runat="server" Visible="false"  ForeColor="Red" Text="<br />Please complete" />
                    </td>
                    <td>
                        <asp:RadioButton runat='server' ID='RadioButtonImmunomodulatoryTherapyYes' GroupName='ImmunomodulatoryTherapy' text='&nbsp;Yes' />&nbsp;&nbsp;
                        <asp:RadioButton runat='server' ID='RadioButtonImmunomodulatoryTherapyNo' GroupName='ImmunomodulatoryTherapy' text='&nbsp;No' />
                    </td>
                </tr>
                <tr class="table_body_bg">
                    <td colspan="2"><span id="QM4aA"><strong>4a. Drug/Dosage</strong></span></td>
                    <td>
                        <asp:TextBox runat='server' ID='TextBoxImmunDrugDosage' size='25' />
                    </td>
                </tr>
                <tr class="table_body_bg">
                    <td colspan="2"><span id="QM4bA"><strong>4b. Prescribed and managed by:</strong></span></td>
                    <td>
                        <span id="QM4bB">
                        <asp:RadioButtonList id='RadioButtonListRBImmunPrescribedBy' RepeatDirection='Vertical' CssClass='aspxList' runat='server'>
                            <asp:ListItem Value='226'>&nbsp;Self</asp:ListItem>
                            <asp:ListItem Value='227'>&nbsp;Rheumatologist</asp:ListItem>
                            <asp:ListItem Value='228'>&nbsp;Internist</asp:ListItem>
                        </asp:RadioButtonList>
                        </span>
                    </td>
                </tr>
                <tr class="table_body_bg">
                    <td colspan="2"><span id="QM4cA"><strong>4c. Duration of therapy</strong></span></td>
                    <td>
                        <span id="QM4cB">Number of months: <asp:TextBox runat='server' ID='TextBoxImmunDurationTherapy' size='5' /></span>
                    </td>
                </tr>
                <tr class="table_body_bg">
                    <td colspan="2"><span id="QM4dA"><strong>4d. Is the therapy ongoing?</strong></span></td>
                    <td>
                        <span id="QM4dB">
                        <asp:RadioButton runat='server' ID='RadioButtonImmunTherapyOngoingYes' GroupName='ImmunTherapyOngoing ' text='&nbsp;Yes' />&nbsp;&nbsp;
                        <asp:RadioButton runat='server' ID='RadioButtonImmunTherapyOngoingNo' GroupName='ImmunTherapyOngoing ' text='&nbsp;No' />
                        </span>
                    </td>
                </tr>
                <!-- Question 5 -->
                <tr class="table_body">
                    <td colspan="2">
                        <strong>5. Was smoking cessation recommended/discussed?</strong>
                        <asp:Label ID="LabelSmokingCessationDiscussed" runat="server" Visible="false"  ForeColor="Red" Text="<br />Please complete" />
                    </td>
                    <td>
                        <asp:RadioButton runat='server' ID='RadioButtonSmokingCessationDiscussedYes' GroupName='SmokingCessationDiscussed ' text='&nbsp;Yes' />&nbsp;&nbsp;
                        <asp:RadioButton runat='server' ID='RadioButtonSmokingCessationDiscussedNo' GroupName='SmokingCessationDiscussed ' text='&nbsp;No' />
                    </td>
                </tr>
            </table>
            <br />
            <!-- Outcomes -->
            <table>
                <tr>
                    <th colspan="3" width="910px"><p>Outcomes</p></th>
                </tr>
                <!-- Question 1 -->
                <tr class="table_body">
                    <td colspan="2">
                        <strong>1. Was remission achieved with discontinuation of all therapeutic agents?</strong>
                        <asp:Label ID="LabelOutcomeRemission" runat="server" Visible="false"  ForeColor="Red" Text="<br />Please complete" />
                    </td>
                    <td>
                        <asp:RadioButton runat='server' ID='RadioButtonOutcomeRemissionYes' GroupName='OutcomeRemission ' text='&nbsp;Yes' />&nbsp;&nbsp;
                        <asp:RadioButton runat='server' ID='RadioButtonOutcomeRemissionNo' GroupName='OutcomeRemission ' text='&nbsp;No' />
                    </td>
                </tr>
                <!-- Question 1a -->
                <tr class="table_body">
                    <td colspan="2"><span id="QO1aA"><strong>1a. Date when achieved:</strong></span></td>
                    <td>
                        <span id="QO1aB">
                        <asp:DropDownList ID='DropDownListOutcomeRemissionMonth' DataValueField='MonthID' DataTextField='MonthName' runat='server' onchange="toggleQO1aCB();" />
                        <asp:DropDownList ID='DropDownListOutcomeRemissionYear' DataValueField='YearID' DataTextField='YearName' runat='server' onchange="toggleQO1aCB();" />
                        <br /><asp:CheckBox runat='server' ID='CheckBoxOutcomeRemissionNA' text='&nbsp;Not achieved' onclick="toggleQO1aDD();" />
                        </span>
                    </td>
                </tr>
                <!-- Question 2 -->
                <tr class="table_body_bg">
                    <td rowspan="2">
                        <strong>2. Best corrected visual acuity?</strong>&nbsp;<a href="#"><img id="QO2" src="../../common/images/tip.gif" alt="Tip" /></a>
                    </td>
                    <td><span class="right">OD<br />
                         <asp:Label ID="LabelOutcomeBCVAOD" runat="server" Visible="false"  ForeColor="Red" Text="<br />Please complete" />
                        </span></td>
                    <td>
                        20/ <asp:DropDownList ID='DropDownListOutcomeBCVAOD' DataValueField='examValue' DataTextField='examLabel' runat='server' />
                    </td>
                </tr>
                <tr class="table_body_bg">
                    <td>
                        <span class="right">OS</span>
                        <asp:Label ID="LabelOutcomeBCVAOS" runat="server" Visible="false"  ForeColor="Red" Text="<br />Please complete" />
                    </td>
                    <td>
                       20/ <asp:DropDownList ID='DropDownListOutcomeBCVAOS' DataValueField='examValue' DataTextField='examLabel' runat='server' />
                    </td>
                </tr>
                <!-- Question 2a -->
                <tr class="table_body_bg">
                    <td colspan="2">
                        <strong>2a. Type</strong>
                        <asp:Label ID="LabelRBOutcomeBCVAType" runat="server" Visible="false"  ForeColor="Red" Text="<br />Please complete" />
                    </td>
                    <td>
                        <asp:RadioButtonList id='RadioButtonListRBOutcomeBCVAType' RepeatDirection='Horizontal' CssClass='aspxList' runat='server'>
                            <asp:ListItem Value='155'>&nbsp;Snellen</asp:ListItem>
                            <asp:ListItem Value='156'>&nbsp;ETDRS</asp:ListItem>
                        </asp:RadioButtonList>
                    </td>
                </tr>
                <!-- Question 3 -->
                <tr class="table_body">
                    <td colspan="2">
                        <strong>3. Stromal scars</strong>
                        <asp:Label ID="LabelOutcomeStromalScarts" runat="server" Visible="false"  ForeColor="Red" Text="<br />Please complete" />
                    </td>
                    <td>
                        <asp:RadioButton runat='server' ID='RadioButtonOutcomeStromalScartsYes' GroupName='OutcomeStromalScarts ' text='&nbsp;Yes' />&nbsp;&nbsp;
                        <asp:RadioButton runat='server' ID='RadioButtonOutcomeStromalScartsNo' GroupName='OutcomeStromalScarts ' text='&nbsp;No' />
                    </td>
                </tr>
                <tr class="table_body">
                    <td colspan="2">
                        <strong>3a. Corneal Edema</strong>
                        <asp:Label ID="LabelOutcomeCornealEdema" runat="server" Visible="false"  ForeColor="Red" Text="<br />Please complete" />
                    </td>
                    <td>
                        <asp:RadioButton runat='server' ID='RadioButtonOutcomeCornealEdemaYes' GroupName='OutcomeCornealEdema' text='&nbsp;Yes' />&nbsp;&nbsp;
                        <asp:RadioButton runat='server' ID='RadioButtonOutcomeCornealEdemaNo' GroupName='OutcomeCornealEdema' text='&nbsp;No' />
                    </td>
                </tr>
                <!-- Question 4 -->
                <tr class="table_body_bg">
                    <td colspan="2">
                        <strong>4. KPs</strong>
                        <asp:Label ID="LabelOutcomeKPs" runat="server" Visible="false"  ForeColor="Red" Text="<br />Please complete" />
                    </td>
                    <td>
                        <asp:RadioButton runat='server' ID='RadioButtonOutcomeKPsYes' GroupName='OutcomeKPs ' text='&nbsp;Yes' />&nbsp;&nbsp;
                        <asp:RadioButton runat='server' ID='RadioButtonOutcomeKPsNo' GroupName='OutcomeKPs ' text='&nbsp;No' />
                    </td>
                </tr>
                <!-- Question 4a -->
                <tr class="table_body_bg">
                    <td colspan="2">
                        <strong><span id="QO4aA">4a. Appearance</span></strong>
                        <asp:Label ID="LabelRBOutcomeKPsAppearance" runat="server" Visible="false"  ForeColor="Red" Text="<br />Please complete" />
                    </td>
                    <td>
                        <span id="QO4aB">
                        <asp:RadioButtonList id='RadioButtonListRBOutcomeKPsAppearance' RepeatDirection='Horizontal'  repeatcolumns="2" CssClass='aspxList' runat='server'>
                            <asp:ListItem Value='159'>&nbsp;Granulomatous</asp:ListItem>
                            <asp:ListItem Value='160'>&nbsp;Fine</asp:ListItem>
                            <asp:ListItem Value='161'>&nbsp;Pigmented</asp:ListItem>
                            <asp:ListItem Value='128'>&nbsp;None</asp:ListItem>
                        </asp:RadioButtonList>
                        </span>
                    </td>
                </tr>
                <!-- Question 4b -->
                <tr class="table_body_bg">
                    <td colspan="2">
                        <strong><span id="QO4bA">4b. Location</span></strong>
                        <asp:Label ID="LabelRBOutcomeKPsLocation" runat="server" Visible="false"  ForeColor="Red" Text="<br />Please complete" />
                    </td>
                    <td>
                        <span id="QO4bB">
                        <asp:RadioButtonList id='RadioButtonListRBOutcomeKPsLocation' RepeatDirection='Horizontal' CssClass='aspxList' runat='server'>
                            <asp:ListItem Value='162'>&nbsp;Inferior (Arlt’s Triangle)</asp:ListItem>
                            <asp:ListItem Value='163'>&nbsp;Diffuse</asp:ListItem>
                        </asp:RadioButtonList>
                        </span>
                    </td>
                </tr>
                <!-- Question 5 -->
                <tr class="table_body">
                    <td width="35%" rowspan="3">
                        <strong>5. Anterior chamber</strong>
                    </td>
                    <td width="25%"><span class="right">Depth<asp:Label ID="LabelRBOutcomeAnteriorDepth" runat="server" Visible="false"  ForeColor="Red" Text="<br />Please complete" />
                        </span></td>
                    <td>
                        <asp:RadioButtonList id='RadioButtonListRBOutcomeAnteriorDepth' RepeatDirection='Horizontal' CssClass='aspxList' runat='server'>
                            <asp:ListItem Value='164'>&nbsp;Flat</asp:ListItem>
                            <asp:ListItem Value='165'>&nbsp;Shallow</asp:ListItem>
                            <asp:ListItem Value='166'>&nbsp;Deep</asp:ListItem>
                            <asp:ListItem Value='167'>&nbsp;Iris Bombe</asp:ListItem>
                        </asp:RadioButtonList>
                    </td>
                </tr>
                <tr class="table_body">
                    <td>
                        <span class="right">Flare description</span>
                        <asp:Label ID="LabelRBOutcomeAnteriorFlare" runat="server" Visible="false"  ForeColor="Red" Text="<br />Please complete" />
                    </td>
                    <td>
                        <asp:RadioButtonList id='RadioButtonListRBOutcomeAnteriorFlare' RepeatDirection='Vertical' CssClass='aspxList' runat='server'>
                            <asp:ListItem Value='168'>&nbsp;None to Trace</asp:ListItem>
                            <asp:ListItem Value='345'>&nbsp;1-2+</asp:ListItem>
                            <asp:ListItem Value='346'>&nbsp;3-4+</asp:ListItem>
                        </asp:RadioButtonList>
                    </td>
                </tr>
                <tr class="table_body">
                    <td><span class="right">Cells in FIELD (High-intensity 1x1mm slit beam)</span>
                        <asp:Label ID="LabelRBOutcomeAnteriorCells" runat="server" Visible="false"  ForeColor="Red" Text="<br />Please complete" />
                    </td>
                    <td>
                        <asp:RadioButtonList id='RadioButtonListRBOutcomeAnteriorCells'  CssClass='aspxList' runat='server'>
                                      <asp:ListItem Value='230'>&nbsp;-0 &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&lt;1 Criteria Cells</asp:ListItem>
                            <asp:ListItem Value='231'>&nbsp;0.5+ &nbsp;&nbsp;1-5</asp:ListItem>
                            <asp:ListItem Value='232'>&nbsp;1+ &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;6-15</asp:ListItem>
                            <asp:ListItem Value='233'>&nbsp;2+ &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;16-25</asp:ListItem>
                            <asp:ListItem Value='234'>&nbsp;3+ &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;26-50</asp:ListItem>
                            <asp:ListItem Value='235'>&nbsp;4+ &nbsp;&nbsp;&nbsp;&nbsp;&gt;50</asp:ListItem>
                        </asp:RadioButtonList>
                      
                    </td>
                </tr>
                <!-- Question 6 -->
                <tr class="table_body_bg">
                    <td rowspan="2"><strong>6. Iris</strong></td>
                    <td>
                        <span class="right">Posterior synechiae</span>
                        <asp:Label ID="LabelRBOutcomePosteriorSynechiae" runat="server" Visible="false"  ForeColor="Red" Text="<br />Please complete" />
                    </td>
                    <td>                        
                        <asp:RadioButtonList id='RadioButtonListRBOutcomePosteriorSynechiae' RepeatDirection='Horizontal' CssClass='aspxList' runat='server'>
                            <asp:ListItem Value='175'>&nbsp;Present</asp:ListItem>
                            <asp:ListItem Value='347'>&nbsp;Absent</asp:ListItem>
                            <asp:ListItem Value='132'>&nbsp;Not examined</asp:ListItem>
                        </asp:RadioButtonList>
                    </td>
                </tr>
                <tr class="table_body_bg">
                    <td><span class="right" id="QO6bA">Extent in clock hours</span></td>
                    <td>
                        <asp:TextBox runat='server' ID='TextBoxOutcomeIrisSynechiaeExtent' size='25' />
                    </td>
                </tr>
                <!-- Question 7a -->
                <tr class="table_body">
                    <td rowspan="2"><strong>7. Lens</strong></td>
                    <td>
                        <span class="right">7a. Pigment on anterior lens capsule:</span>
                        <asp:Label ID="LabelOutcomeLensPigment" runat="server" Visible="false"  ForeColor="Red" Text="<br />Please complete" />
                    </td>
                    <td>
                        <asp:RadioButton runat='server' ID='RadioButtonOutcomeLensPigmentYes' GroupName='OutcomeLensPigment ' text='&nbsp;Yes' />&nbsp;&nbsp;
                        <asp:RadioButton runat='server' ID='RadioButtonOutcomeLensPigmentNo' GroupName='OutcomeLensPigment ' text='&nbsp;No' />
                    </td>
                </tr>
                <!-- Question 7b -->
                <tr class="table_body">
                    <td>
                        <span class="right">7b. Cataract</span>
                        <asp:Label ID="LabelOutcomeLensCataract" runat="server" Visible="false"  ForeColor="Red" Text="<br />Please complete" />
                    </td>
                    <td>
                        <asp:RadioButton runat='server' ID='RadioButtonOutcomeLensCataractYes' GroupName='OutcomeLensCataract ' text='&nbsp;Yes' />&nbsp;&nbsp;
                        <asp:RadioButton runat='server' ID='RadioButtonOutcomeLensCataractNo' GroupName='OutcomeLensCataract ' text='&nbsp;No' />
                    </td>
                </tr>
                <!-- Question 8 -->
                <tr class="table_body_bg">
                    <td colspan="2">
                        <strong>8. Intraocular pressure</strong>
                        <asp:Label ID="LabelOutcomeIOP" runat="server" Visible="false"  ForeColor="Red" Text="<br />Please complete" />
                    </td>
                    <td>
                        <asp:TextBox runat='server' ID='TextBoxOutcomeIOP' size='25' />
                    </td>
                </tr>
                <!-- Question 9 -->
                <tr class="table_body">
                    <td colspan="2">
                        <strong>9. Was a gonioscopy performed?</strong>
                        <asp:Label ID="LabelOutcomeGonioscopy" runat="server" Visible="false"  ForeColor="Red" Text="<br />Please complete" />    
                    </td>
                    <td>
                         <asp:RadioButtonList id='RadioButtonListOutcomeGonioscopy' RepeatDirection='Horizontal' CssClass='aspxList' runat='server'>
                            <asp:ListItem Value='1'>&nbsp;Yes</asp:ListItem>
                             <asp:ListItem Value='2'>&nbsp;No</asp:ListItem>
                        </asp:RadioButtonList>
                    </td>
                </tr>
                <!-- Question 9a -->
                <tr class="table_body">
                    <td colspan="2"><span id="QO9aA"><strong>9a. If yes, please list the date:</strong></span></td>
                    <td>
                        <asp:DropDownList ID='DropDownListOutcomeGonioscopyMonth' DataValueField='MonthID' DataTextField='MonthName' runat='server' />
                        <asp:DropDownList ID='DropDownListOutcomeGonioscopyYear' DataValueField='YearID' DataTextField='YearName' runat='server' />
                    </td>
                </tr>
                <!-- Question 9b -->
                <tr class="table_body">
                    <td rowspan="3"><span id="QO9bA"><strong>9b. If yes, please list the findings:</strong></span></td>
                    <td><span class="right" id="QO9bB">Rubeosis</span></td>
                    <td>
                        <span id="QO9bC">
                        <asp:RadioButtonList id='RadioButtonListRBOutcomeGonioscopyRubeosis' RepeatDirection='Horizontal' CssClass='aspxList' runat='server'>
                            <asp:ListItem Value='175'>&nbsp;Present</asp:ListItem>
                            <asp:ListItem Value='176'>&nbsp;Absent</asp:ListItem>
                        </asp:RadioButtonList>
                        </span>
                    </td>
                </tr>
                <tr class="table_body">
                    <td><span class="right" id="QO9bD">Inflammatory nodules/granuloma</span></td>
                    <td>
                        <span id="QO9bE">
                        <asp:RadioButtonList id='RadioButtonListRBOutcomeGonioscopyInflam' RepeatDirection='Horizontal' CssClass='aspxList' runat='server'>
                            <asp:ListItem Value='175'>&nbsp;Present</asp:ListItem>
                            <asp:ListItem Value='176'>&nbsp;Absent</asp:ListItem>
                        </asp:RadioButtonList>
                        </span>
                    </td>
                </tr>
                <tr class="table_body">
                    <td><span class="right" id="QO9bF">Number of clock hours of PAS</span></td>
                    <td>
                        <span id="QO9bG">
                        <asp:RadioButtonList id='RadioButtonListRBOutcomeGonioscopyClockHrs' RepeatDirection='Vertical' CssClass='aspxList' runat='server'>
                            <asp:ListItem Value='179'>&nbsp;1-3</asp:ListItem>
                            <asp:ListItem Value='180'>&nbsp;4-6</asp:ListItem>
                            <asp:ListItem Value='181'>&nbsp;7-9</asp:ListItem>
                            <asp:ListItem Value='182'>&nbsp;10-12</asp:ListItem>
                        </asp:RadioButtonList>
                        </span>
                    </td>
                </tr>
                <!-- Question 10 -->
                <tr class="table_body_bg">
                    <td colspan="2">
                        <strong>10. Vitreous cells</strong>
                        <asp:Label ID="LabelRBOutcomeVitreousCells" runat="server" Visible="false"  ForeColor="Red" Text="<br />Please complete" />
                    </td>
                    <td>
                        <asp:RadioButtonList id='RadioButtonListRBOutcomeVitreousCells' RepeatDirection='Vertical' CssClass='aspxList' runat='server'>
                            <asp:ListItem Value='239'>&nbsp;None</asp:ListItem>
                            <asp:ListItem Value='240'>&nbsp;Trace</asp:ListItem>
                            <asp:ListItem Value='241'>&nbsp;Mild</asp:ListItem>
                            <asp:ListItem Value='53'>&nbsp;Moderate</asp:ListItem>
                            <asp:ListItem Value='242'>&nbsp;Dense</asp:ListItem>
                        </asp:RadioButtonList>
                    </td>
                </tr>
                <!-- Question 11 -->
                <tr class="table_body">
                    <td rowspan="2"><strong>11. Optic disc</strong>
                     <asp:Label ID="LabelOutcomeOpticDiscEdema" runat="server" Visible="false"  ForeColor="Red" Text="<br />Please complete" />
                     <asp:Label ID="LabelOutcomeOpticDiscGlaucChanges" runat="server" Visible="false"  ForeColor="Red" Text="<br />Please complete" /></td>
                    <td>
                        <span class="right">Was edema present?</span>
                       
                    </td>
                    <td>
                        <asp:RadioButton runat='server' ID='RadioButtonOutcomeOpticDiscEdemaYes' GroupName='OutcomeOpticDiscEdema ' text='&nbsp;Yes' />&nbsp;&nbsp;
                        <asp:RadioButton runat='server' ID='RadioButtonOutcomeOpticDiscEdemaNo' GroupName='OutcomeOpticDiscEdema ' text='&nbsp;No' />
                    </td>
                </tr>
                <tr class="table_body">
                    <td>
                        <span class="right">Are glaucomatous changes present?</span>
                    </td>
                    <td>
                        <asp:RadioButton runat='server' ID='RadioButtonOutcomeOpticDiscGlaucChangesYes' GroupName='OutcomeOpticDiscGlaucChanges ' text='&nbsp;Yes' />&nbsp;&nbsp;
                        <asp:RadioButton runat='server' ID='RadioButtonOutcomeOpticDiscGlaucChangesNo' GroupName='OutcomeOpticDiscGlaucChanges ' text='&nbsp;No' />
                    </td>
                </tr>
                <!-- Question 12 -->
                <tr class="table_body_bg">
                    <td rowspan="4" style="vertical-align: top; padding-top: 30px;" >
                        <strong>12. Retina and choroid</strong>
                    </td>
                    <td>
                        <span class="right">Dilated fundus exam performed?</span>
                        <asp:Label ID="LabelOutcomeDilatedFundusExamPerformed" runat="server" Visible="false"  ForeColor="Red" Text="<br />Please complete" />
                    </td>
                    <td>
                        <asp:RadioButton runat='server' ID='RadioButtonOutcomeDilatedFundusExamPerformedYes' GroupName='OutcomeDilatedFundusExamPerformed ' text='&nbsp;Yes' />&nbsp;&nbsp;
                        <asp:RadioButton runat='server' ID='RadioButtonOutcomeDilatedFundusExamPerformedNo' GroupName='OutcomeDilatedFundusExamPerformed ' text='&nbsp;No' />
                    </td>
                </tr>
                <tr class="table_body_bg">
                    <td>
                        <span class="right">CME present?</span>
                        <asp:Label ID="LabelOutcomeCMEPresent" runat="server" Visible="false"  ForeColor="Red" Text="<br />Please complete" />
                    </td>
                    <td>
                        <asp:RadioButton runat='server' ID='RadioButtonOutcomeCMEPresentYes' GroupName='OutcomeCMEPresent ' text='&nbsp;Yes' />&nbsp;&nbsp;
                        <asp:RadioButton runat='server' ID='RadioButtonOutcomeCMEPresentNo' GroupName='OutcomeCMEPresent ' text='&nbsp;No' />
                    </td>
                </tr>
                <tr class="table_body_bg">
                    <td>
                        <span class="right" id="QO12">If yes, enter OCT subfield thickness</span>
                    </td>
                    <td id="QO12B">
                        <asp:TextBox runat='server' ID='TextBoxOutcomeOCTCentralSubfieldThickness' size='5' onkeyup="toggleQO12CB();" />
                        <br /><asp:CheckBox ID="CheckBoxOutcomeOCTCentralSubfieldThicknessNP" Text="Not Performed" runat="server" onclick="toggleQO12TB();" />
                    </td>
                </tr>
                <tr class="table_body_bg">
                    <td>
                        <span class="right">Fluorescein angiogram performed?</span>
                        <asp:Label ID="LabelOutcomeFluoresceinPerformed" runat="server" Visible="false"  ForeColor="Red" Text="<br />Please complete" />
                    </td>
                    <td>
                        <asp:RadioButton runat='server' ID='RadioButtonOutcomeFluoresceinPerformedYes' GroupName='OutcomeFluoresceinPerformed ' text='&nbsp;Yes' />&nbsp;&nbsp;
                        <asp:RadioButton runat='server' ID='RadioButtonOutcomeFluoresceinPerformedNo' GroupName='OutcomeFluoresceinPerformed ' text='&nbsp;No' />
                    </td>
                </tr>
                <tr class="table_body_bg">
                <td rowspan="11" style="vertical-align: top; padding-top: 30px;"  >
                      <strong>12a. Chorioretinal pathology</strong>
                       </td>
                    <td>
                        <span class="right">Chorioretinal pathology?</span>
                        <asp:Label ID="LabelRBOutcomeChorioretinalPathology" runat="server" Visible="false"  ForeColor="Red" Text="<br />Please complete" />
                    </td>
                    <td>
                        <asp:RadioButtonList id='RadioButtonListRBOutcomeChorioretinalPathology' RepeatDirection='Horizontal' CssClass='aspxList' runat='server'>
                            <asp:ListItem Value='175'>&nbsp;Present</asp:ListItem>
                            <asp:ListItem Value='176'>&nbsp;Absent</asp:ListItem>
                        </asp:RadioButtonList>
                    </td>
                </tr>
                <tr class="table_body_bg">
                    <td colspan="2">
                        <strong><span id="QO12aA">If chorioretinal pathology present, describe</span></strong>
                    </td>
                </tr>
                <tr class="table_body_bg">
                    <td><span class="right" id="QO12bA">Retinal venous sheathing</span></td>
                    <td>
                        <span id="QO12bB">
                        <asp:RadioButtonList id='RadioButtonListRBOutcomeChorioretinalRVSheathing' RepeatDirection='Horizontal' CssClass='aspxList' runat='server'>
                            <asp:ListItem Value='175'>&nbsp;Present</asp:ListItem>
                            <asp:ListItem Value='176'>&nbsp;Absent</asp:ListItem>
                        </asp:RadioButtonList>
                        </span>
                    </td>
                </tr>
                <tr class="table_body_bg">
                    <td><span class="right" id="QO12cA">Retinal vasculitis - active - (hemorrhages, and perivascular inflammation) </span></td>
                    <td>
                        <span id="QO12cB">
                        <asp:RadioButtonList id='RadioButtonListRBOutcomeChorioretinalRVActive' RepeatDirection='Horizontal' CssClass='aspxList' runat='server'>
                            <asp:ListItem Value='175'>&nbsp;Present</asp:ListItem>
                            <asp:ListItem Value='176'>&nbsp;Absent</asp:ListItem>
                        </asp:RadioButtonList>
                        </span>
                    </td>
                </tr>
                <tr class="table_body_bg">
                    <td><span class="right" id="QO12dA">Retinitis (necrotizing)</span></td>
                    <td>
                        <span id="QO12dB">
                        <asp:RadioButtonList id='RadioButtonListRBOutcomeChorioretinalRetinitis' RepeatDirection='Horizontal' CssClass='aspxList' runat='server'>
                            <asp:ListItem Value='175'>&nbsp;Present</asp:ListItem>
                            <asp:ListItem Value='176'>&nbsp;Absent</asp:ListItem>
                        </asp:RadioButtonList>
                        </span>
                    </td>
                </tr>
                <tr class="table_body_bg">
                    <td><span class="right" id="QO12eA">Retinal neovascularization</span></td>
                    <td>
                        <span id="QO12eB">
                        <asp:RadioButtonList id='RadioButtonListRBOutcomeChorioretinalRetinalNeovasc' RepeatDirection='Horizontal' CssClass='aspxList' runat='server'>
                            <asp:ListItem Value='175'>&nbsp;Present</asp:ListItem>
                            <asp:ListItem Value='176'>&nbsp;Absent</asp:ListItem>
                        </asp:RadioButtonList>
                        </span>
                    </td>
                </tr>
                <tr class="table_body_bg">
                    <td><span class="right" id="QO12fA">Choroiditis</span></td>
                    <td>
                        <span id="QO12fB">
                        <asp:RadioButtonList id='RadioButtonListRBOutcomeChoroiditis' RepeatDirection='Vertical' CssClass='aspxList' runat='server'>
                            <asp:ListItem Value='175'>&nbsp;Present</asp:ListItem>
                            <asp:ListItem Value='176'>&nbsp;Absent</asp:ListItem>
                            <asp:ListItem Value='236'>&nbsp;Active</asp:ListItem>
                            <asp:ListItem Value='188'>&nbsp;Inactive</asp:ListItem>
                            <asp:ListItem Value='189'>&nbsp;Posterior pole</asp:ListItem>
                            <asp:ListItem Value='190'>&nbsp;Peripheral</asp:ListItem>
                            <asp:ListItem Value='191'>&nbsp;Multifocal</asp:ListItem>
                            <asp:ListItem Value='192'>&nbsp;Unifocal</asp:ListItem>
                        </asp:RadioButtonList>
                        </span>
                    </td>
                </tr>
                <tr class="table_body_bg">
                    <td><span class="right" id="QO12gA">Choroidal neovascualrization </span></td>
                    <td>
                        <span id="QO12gB">
                        <asp:RadioButtonList id='RadioButtonListRBOutcomeChoroidalNeovascularization' RepeatDirection='Vertical' CssClass='aspxList' runat='server'>
                            <asp:ListItem Value='175'>&nbsp;Present</asp:ListItem>
                            <asp:ListItem Value='176'>&nbsp;Absent</asp:ListItem>
                            <asp:ListItem Value='193'>&nbsp;Peripapillary</asp:ListItem>
                            <asp:ListItem Value='194'>&nbsp;Extrafoveal</asp:ListItem>
                            <asp:ListItem Value='195'>&nbsp;Juxtafoveal</asp:ListItem>
                            <asp:ListItem Value='196'>&nbsp;Subfoveal</asp:ListItem>
                        </asp:RadioButtonList>
                        </span>
                    </td>
                </tr>
                <tr class="table_body_bg">
                    <td><span class="right" id="QO12hA">Retinal tear</span></td>
                    <td>
                        <span id="QO12hB">
                        <asp:RadioButtonList id='RadioButtonListRBOutcomeRetinalTear' RepeatDirection='Horizontal' CssClass='aspxList' runat='server'>
                            <asp:ListItem Value='175'>&nbsp;Present</asp:ListItem>
                            <asp:ListItem Value='176'>&nbsp;Absent</asp:ListItem>
                        </asp:RadioButtonList>
                        </span>
                    </td>
                </tr>
                <tr class="table_body_bg">
                    <td><span class="right" id="QO12iA">Retinal detachment</span></td>
                    <td>
                        <span id="QO12iB">
                        <asp:RadioButtonList id='RadioButtonListRBOutcomeRetinalDetachment' RepeatDirection='Vertical' CssClass='aspxList' runat='server'>
                            <asp:ListItem Value='175'>&nbsp;Present</asp:ListItem>
                            <asp:ListItem Value='176'>&nbsp;Absent</asp:ListItem>
                            <asp:ListItem Value='197'>&nbsp;Tractional</asp:ListItem>
                            <asp:ListItem Value='198'>&nbsp;Exudative</asp:ListItem>
                            <asp:ListItem Value='199'>&nbsp;Rhegmatogenous</asp:ListItem>
                            <asp:ListItem Value='14'>&nbsp;Other</asp:ListItem>
                        </asp:RadioButtonList>
                        </span>
                    </td>
                </tr>
                <tr class="table_body_bg">
                    <td><span class="right" id="QO12jA">If "Other," please specify:</span></td>
                    <td>
                        <span id="QO12jB">
                        <asp:TextBox runat="server" ID="TextBoxOutcomeRetinalDetachmentText" size="20" />
                        </span>
                    </td>
                </tr>
                <!-- Question 13 -->
                <tr class="table_body">
                    <td colspan="2">
                        <strong>13. Initiated management of underlying systemic condition</strong>
                        <asp:Label ID="LabelRBOutcomeInitiatedMgntUnderlying" runat="server" Visible="false"  ForeColor="Red" Text="<br />Please complete" />
                    </td>
                    <td>
                        <asp:RadioButtonList id='RadioButtonListRBOutcomeInitiatedMgntUnderlying' RepeatDirection='Horizontal' CssClass='aspxList' runat='server'>
                            <asp:ListItem Value='1'>&nbsp;Yes</asp:ListItem>
                            <asp:ListItem Value='2'>&nbsp;No</asp:ListItem>
                            <asp:ListItem Value='48'>&nbsp;Not applicable</asp:ListItem>
                        </asp:RadioButtonList>
                    </td>
                </tr>
            </table>
            <div class="button-box">
                <asp:LinkButton ID="LinkButtonBackToDashboard" runat="server" Text="Back To Chart Registration" PostBackUrl="../PatientChartRegistration.aspx?CycleNumber=1" Visible="false" CssClass="button" />
                <asp:LinkButton ID="ButtonSubmit"  OnClick="ButtonSubmit_Click" runat="server" Text="Submit Chart" CssClass="button" />
            </div>
        </div>
        <!-- ION pim ends -->
    </asp:Content>
<asp:Content runat="server" ID="Content4" ContentPlaceHolderID="javascript">
    <script type="text/javascript">
        $(function () {
            $('#QCE1').aToolTip({
                clickIt: true,
                tipContent: '20/800 or count fingers @ 5 ft<br />20/1000 or count fingers @ 4 ft<br />20/1600 or count fingers @ 3ft<br />20/2000 or count fingers @ 2 ft<br />20/4000 or count fingers @ 1 ft<br />20/7777 =CF<br />20/8888 =HM<br />20/9999 =LP<br />20/0000 =NLP'
            });

            $('#QO2').aToolTip({
                clickIt: true,
                tipContent: '20/800 or count fingers @ 5 ft<br />20/1000 or count fingers @ 4 ft<br />20/1600 or count fingers @ 3ft<br />20/2000 or count fingers @ 2 ft<br />20/4000 or count fingers @ 1 ft<br />20/7777 =CF<br />20/8888 =HM<br />20/9999 =LP<br />20/0000 =NLP'
            });
        });
    </script>
    <script type="text/javascript" language="javascript">
        //      Either enables or disables input boxes
        function enabledControl(el, disabled, checked) {
            //alert("Hello world from enabled control");
            try {
                if (disabled) {
                    el.disabled = disabled;
                    el.setAttribute("disabled", true);
                }
                else {
                    el.disabled = disabled;
                    el.removeAttribute("disabled");
                }

                if (checked == true)
                    el.checked = false;
            }
            catch (E) {
            }
            if (el.childNodes && el.childNodes.length > 0) {
                for (var x = 0; x < el.childNodes.length; x++) {
                    enabledControl(el.childNodes[x], disabled, checked);
                }
            }
        }
        //      Either enables or disables dropdown boxes
        function enabledControlDropDown(el, disabled, checked) {
            //alert("Hello world from enabled control");
            try {
                if (disabled) {
                    el.disabled = disabled;
                    el.setAttribute("disabled", true);
                }
                else {
                    el.disabled = disabled;
                    el.removeAttribute("disabled");
                }

                if (checked == true)
                    el.options[0].selected = true;
            }
            catch (E) {
            }
            if (el.childNodes && el.childNodes.length > 0) {
                for (var x = 0; x < el.childNodes.length; x++) {
                    enabledControl(el.childNodes[x], disabled, checked);
                }
            }
        }
        //      Either enables or disables text boxes
        function enabledControlTextField(el, disabled, checked) {
            //alert("Hello world from enabled control");
            try {
                if (disabled) {
                    el.disabled = disabled;
                    el.setAttribute("disabled", true);
                }
                else {
                    el.disabled = disabled;
                    el.removeAttribute("disabled");
                }

                if (checked == true) {
                    el.value = '';
                }
            }
            catch (E) {
            }
            if (el.childNodes && el.childNodes.length > 0) {
                for (var x = 0; x < el.childNodes.length; x++) {
                    enabledControl(el.childNodes[x], disabled, checked);
                }
            }
        }

        function generate(arr1, arr2, istrue) {
            if (istrue == true) {
                for (var i = 0; i < arr1.length; i++) {
                    document.getElementById(arr1[i]).style.color = "gray";
                }
                for (var i = 0; i < arr2.length; i++) {
                    $(arr2[i] + ' :input').attr('disabled', true);
                    $(arr2[i] + ' :input').removeAttr("checked");
                }
            }
            if (istrue == false) {
                for (var i = 0; i < arr1.length; i++) {
                    document.getElementById(arr1[i]).style.color = "#333";
                }
                for (var i = 0; i < arr2.length; i++) {
                    $(arr2[i] + ' :input').removeAttr('disabled');
                }
            }
        }

        // Toggle QE5 
        function toggleQH8TB() {
            document.getElementById("<%= TextBoxInflammationFreeDurationWeeks.ClientID %>").value = '';
        }
        function toggleQH8CB() {
            document.getElementById("<%= RadioButtonInflammationFreeDurationNDYes.ClientID %>").checked = false;
        }

        // Toggle QO1
        function toggleQO1aDD() {
            document.getElementById("<%= DropDownListOutcomeRemissionMonth.ClientID %>").options[0].selected = true;
            document.getElementById("<%= DropDownListOutcomeRemissionYear.ClientID %>").options[0].selected = true;
        }

        function toggleQO1aCB() {
            document.getElementById("<%= CheckBoxOutcomeRemissionNA.ClientID %>").checked = false;
        }

        // Toggle QO12 
        function toggleQO12TB() {
            document.getElementById("<%= TextBoxOutcomeOCTCentralSubfieldThickness.ClientID %>").value = '';
        }
        function toggleQO12CB() {
            document.getElementById("<%= CheckBoxOutcomeOCTCentralSubfieldThicknessNP.ClientID %>").checked = false;
        }
    </script>

    <script  type="text/javascript" language="javascript">
        $(document).ready(function () {

            /* JAVASCRIPT ADDITIONS AS OF: 6/28/12 */
            // Question 7 - History
            $("#<%= RadioButtonListPriorEpisodes.ClientID %>").click(function () {
                if ($('#<%= RadioButtonListPriorEpisodes.ClientID %>').find('input:checked').val() == ('1')) {
                    var myaray = new Array("QH7aA", "QH7aB", "QH7bA", "QH7bB", "QH7dA", "QH7dAa", /*"QH7cA",*/"QH7dB", "QH7aaA", "QH7aaB", "QH7abA");
                    var myarray2 = new Array('#QH7aB', '#QH7bB', '#QH7dB');
                    generate(myaray, myarray2, false);

                    //enabledControlTextField(document.getElementById("<%= TextBoxPreviousTreatmentNbrInjections.ClientID %>"), false, false);
                    enabledControlTextField(document.getElementById("<%= TextBoxPreviousTreatmentImmuTherapyDrug.ClientID %>"), false, false);
                    enabledControlDropDown(document.getElementById("<%= DropDownListPreviousTreatmentImmuStartMonth.ClientID %>"), false, false);
                    enabledControlDropDown(document.getElementById("<%= DropDownListPreviousTreatmentImmuStartYear.ClientID %>"), false, false);
                    enabledControlDropDown(document.getElementById("<%= DropDownListPreviousTreatmentImmuStopMonth.ClientID %>"), false, false);
                    enabledControlDropDown(document.getElementById("<%= DropDownListPreviousTreatmentImmuStopYear.ClientID %>"), false, false);
                }
                else {
                    var myaray = new Array("QH7aA", "QH7aB", "QH7bA", "QH7bB", "QH7dA", "QH7dAa", "QH7cA", "QH7dB", "QH7aaA", "QH7aaB", "QH7abA");
                    var myarray2 = new Array('#QH7aB', '#QH7bB', '#QH7dB');
                    generate(myaray, myarray2, true);

                    enabledControlTextField(document.getElementById("<%= TextBoxPreviousTreatmentNbrInjections.ClientID %>"), true, true);
                    enabledControlTextField(document.getElementById("<%= TextBoxPreviousTreatmentImmuTherapyDrug.ClientID %>"), true, true);
                    enabledControlDropDown(document.getElementById("<%= DropDownListPreviousTreatmentImmuStartMonth.ClientID %>"), true, true);
                    enabledControlDropDown(document.getElementById("<%= DropDownListPreviousTreatmentImmuStartYear.ClientID %>"), true, true);
                    enabledControlDropDown(document.getElementById("<%= DropDownListPreviousTreatmentImmuStopMonth.ClientID %>"), true, true);
                    enabledControlDropDown(document.getElementById("<%= DropDownListPreviousTreatmentImmuStopYear.ClientID %>"), true, true);
                }
            });
            if ($('#<%= RadioButtonListPriorEpisodes.ClientID %>').find('input:checked').val() == ('1')) {
                var myaray = new Array("QH7aA", "QH7aB", "QH7bA", "QH7bB", "QH7dA", "QH7dAa", /*"QH7cA",*/"QH7dB", "QH7aaA", "QH7aaB", "QH7abA");
                var myarray2 = new Array('#QH7aB', '#QH7bB', '#QH7dB');
                generate(myaray, myarray2, false);

                //enabledControlTextField(document.getElementById("<%= TextBoxPreviousTreatmentNbrInjections.ClientID %>"), false, false);
                enabledControlTextField(document.getElementById("<%= TextBoxPreviousTreatmentImmuTherapyDrug.ClientID %>"), false, false);
                enabledControlDropDown(document.getElementById("<%= DropDownListPreviousTreatmentImmuStartMonth.ClientID %>"), false, false);
                enabledControlDropDown(document.getElementById("<%= DropDownListPreviousTreatmentImmuStartYear.ClientID %>"), false, false);
                enabledControlDropDown(document.getElementById("<%= DropDownListPreviousTreatmentImmuStopMonth.ClientID %>"), false, false);
                enabledControlDropDown(document.getElementById("<%= DropDownListPreviousTreatmentImmuStopYear.ClientID %>"), false, false);
            }
            else {
                var myaray = new Array("QH7aA", "QH7aB", "QH7bA", "QH7bB", "QH7dA", "QH7dAa", "QH7cA", "QH7dB", "QH7aaA", "QH7aaB", "QH7abA");
                var myarray2 = new Array('#QH7aB', '#QH7bB', '#QH7dB');
                generate(myaray, myarray2, true);

                enabledControlTextField(document.getElementById("<%= TextBoxPreviousTreatmentNbrInjections.ClientID %>"), true, true);
                enabledControlTextField(document.getElementById("<%= TextBoxPreviousTreatmentImmuTherapyDrug.ClientID %>"), true, true);
                enabledControlDropDown(document.getElementById("<%= DropDownListPreviousTreatmentImmuStartMonth.ClientID %>"), true, true);
                enabledControlDropDown(document.getElementById("<%= DropDownListPreviousTreatmentImmuStartYear.ClientID %>"), true, true);
                enabledControlDropDown(document.getElementById("<%= DropDownListPreviousTreatmentImmuStopMonth.ClientID %>"), true, true);
                enabledControlDropDown(document.getElementById("<%= DropDownListPreviousTreatmentImmuStopYear.ClientID %>"), true, true);
            }


            // Question 9 Clinical Examination Findings ***************************
            $("#<%= RadioButtonListScleritis.ClientID %>").click(function () {
                if ($('#<%= RadioButtonListScleritis.ClientID %>').find('input:checked').val() == ('1')) {
                    var myaray = new Array("QC9aA", "QC9aB", "QC9bA", "QC9bB", "QC9cA", "QC9cB");
                    var myarray2 = new Array('#QC9aB', '#QC9bB', '#QC9cB');
                    generate(myaray, myarray2, false);
                }
                else {
                    var myaray = new Array("QC9aA", "QC9aB", "QC9bA", "QC9bB", "QC9cA", "QC9cB");
                    var myarray2 = new Array('#QC9aB', '#QC9bB', '#QC9cB');
                    generate(myaray, myarray2, true);
                }
            });
            if ($('#<%= RadioButtonListScleritis.ClientID %>').find('input:checked').val() == ('1')) {
                var myaray = new Array("QC9aA", "QC9aB", "QC9bA", "QC9bB", "QC9cA", "QC9cB");
                var myarray2 = new Array('#QC9aB', '#QC9bB', '#QC9cB');
                generate(myaray, myarray2, false);
            }
            else {
                var myaray = new Array("QC9aA", "QC9aB", "QC9bA", "QC9bB", "QC9cA", "QC9cB");
                var myarray2 = new Array('#QC9aB', '#QC9bB', '#QC9cB');
                generate(myaray, myarray2, true);
            }


            // Question 3 Management, other ***************************
            $("#<%= RadioButtonListRBTopical.ClientID %>").click(function () {
                if ($('#<%= RadioButtonListRBTopical.ClientID %>').find('input:checked').val() == ('1')) {
                    var myaray = new Array("QM3aOther");
                    var myarray2 = new Array('#QM3aOther');
                    generate(myaray, myarray2, false);

                    enabledControlTextField(document.getElementById("<%= TextBoxTopicalOther.ClientID %>"), false, false);
                }
                else {
                    var myaray = new Array("QM3aOther");
                    var myarray2 = new Array('#QM3aOther');
                    generate(myaray, myarray2, true);

                    enabledControlTextField(document.getElementById("<%= TextBoxTopicalOther.ClientID %>"), true, true);
                }
            });
            if ($('#<%= RadioButtonListRBTopical.ClientID %>').find('input:checked').val() == ('1')) {
                var myaray = new Array("QM3aOther");
                var myarray2 = new Array('#QM3aOther');
                generate(myaray, myarray2, false);

                enabledControlTextField(document.getElementById("<%= TextBoxTopicalOther.ClientID %>"), false, false);
            }
            else {
                var myaray = new Array("QM3aOther");
                var myarray2 = new Array('#QM3aOther');
                generate(myaray, myarray2, true);

                enabledControlTextField(document.getElementById("<%= TextBoxTopicalOther.ClientID %>"), true, true);
            }

            // Question 2b - History
            $("#<%= RadioButtonEtiologyDeterminedYES.ClientID %>").click(function () {
                if ($('#<%= RadioButtonEtiologyDeterminedYES.ClientID %>').prop("checked") == true) {
                    var myaray = new Array("QH2btxt");
                    var myarray2 = new Array('#QH2btxt');
                    generate(myaray, myarray2, false);

                    enabledControlTextField(document.getElementById("<%= EtiologyDeterminedText.ClientID %>"), false, false);
                }
                else {
                    var myaray = new Array("QH2btxt");
                    var myarray2 = new Array('#QH2btxt');
                    generate(myaray, myarray2, true);

                    enabledControlTextField(document.getElementById("<%= EtiologyDeterminedText.ClientID %>"), true, true);
                }
            });
            $("#<%= RadioButtonEtiologyDeterminedNO.ClientID %>").click(function () {
                if ($('#<%= RadioButtonEtiologyDeterminedYES.ClientID %>').prop("checked") == true) {
                    var myaray = new Array("QH2btxt");
                    var myarray2 = new Array('#QH2btxt');
                    generate(myaray, myarray2, false);

                    enabledControlTextField(document.getElementById("<%= EtiologyDeterminedText.ClientID %>"), false, false);
                }
                else {
                    var myaray = new Array("QH2btxt");
                    var myarray2 = new Array('#QH2btxt');
                    generate(myaray, myarray2, true);

                    enabledControlTextField(document.getElementById("<%= EtiologyDeterminedText.ClientID %>"), true, true);
                }
            });
            if ($('#<%= RadioButtonEtiologyDeterminedYES.ClientID %>').prop("checked") == true) {
                var myaray = new Array("QH2btxt");
                var myarray2 = new Array('#QH2btxt');
                generate(myaray, myarray2, false);

                enabledControlTextField(document.getElementById("<%= EtiologyDeterminedText.ClientID %>"), false, false);
            }
            else {
                var myaray = new Array("QH2btxt");
                var myarray2 = new Array('#QH2btxt');
                generate(myaray, myarray2, true);

                enabledControlTextField(document.getElementById("<%= EtiologyDeterminedText.ClientID %>"), true, true);
            }


            // Question 2b - Management Other 1
            $("#<%= RadioButtonListRBTopical.ClientID %>").click(function () {
                if ($('#<%= RadioButtonListRBTopical.ClientID %>').find('input:checked').val() == ('14')) {
                    var myaray = new Array("QM3aOther");
                    var myarray2 = new Array('#QM3aOther');
                    generate(myaray, myarray2, false);

                    enabledControlTextField(document.getElementById("<%= TextBoxTopicalOther.ClientID %>"), false, false);
                }
                else {
                    var myaray = new Array("QM3aOther");
                    var myarray2 = new Array('#QM3aOther');
                    generate(myaray, myarray2, true);

                    enabledControlTextField(document.getElementById("<%= TextBoxTopicalOther.ClientID %>"), true, true);
                }
            });
            if ($('#<%= RadioButtonListRBTopical.ClientID %>').find('input:checked').val() == ('14')) {
                var myaray = new Array("QM3aOther");
                var myarray2 = new Array('#QM3aOther');
                generate(myaray, myarray2, false);

                enabledControlTextField(document.getElementById("<%= TextBoxTopicalOther.ClientID %>"), false, false);
            }
            else {
                var myaray = new Array("QM3aOther");
                var myarray2 = new Array('#QM3aOther');
                generate(myaray, myarray2, true);

                enabledControlTextField(document.getElementById("<%= TextBoxTopicalOther.ClientID %>"), true, true);
            }

            // Question 2b - Management Other 2
            $("#<%= RadioButtonListRBPeriocularInjections.ClientID %>").click(function () {
                if ($('#<%= RadioButtonListRBPeriocularInjections.ClientID %>').find('input:checked').val() == ('14')) {
                    var myaray = new Array("QM3aOther2");
                    var myarray2 = new Array('#QM3aOther2');
                    generate(myaray, myarray2, false);

                    enabledControlTextField(document.getElementById("<%= PeriocularInjectionsText.ClientID %>"), false, false);
                }
                else {
                    var myaray = new Array("QM3aOther2");
                    var myarray2 = new Array('#QM3aOther2');
                    generate(myaray, myarray2, true);

                    enabledControlTextField(document.getElementById("<%= PeriocularInjectionsText.ClientID %>"), true, true);
                }
            });
            if ($('#<%= RadioButtonListRBPeriocularInjections.ClientID %>').find('input:checked').val() == ('14')) {
                var myaray = new Array("QM3aOther2");
                var myarray2 = new Array('#QM3aOther2');
                generate(myaray, myarray2, false);

                enabledControlTextField(document.getElementById("<%= PeriocularInjectionsText.ClientID %>"), false, false);
            }
            else {
                var myaray = new Array("QM3aOther2");
                var myarray2 = new Array('#QM3aOther2');
                generate(myaray, myarray2, true);

                enabledControlTextField(document.getElementById("<%= PeriocularInjectionsText.ClientID %>"), true, true);
            }








            /* First implemented code */
            // Question 7c History ***************************
            $("#<%= RadioButtonPreviousTreatmentPeriocularCSYes.ClientID %>").click(function () {
                if ($('#<%= RadioButtonPreviousTreatmentPeriocularCSYes.ClientID %>').prop("checked") == true) {
                    var myaray = new Array("QH7cA");
                    var myarray2 = new Array('#QH7cA');
                    generate(myaray, myarray2, false);
                    enabledControlTextField(document.getElementById("<%= TextBoxPreviousTreatmentNbrInjections.ClientID %>"), false, false);
                }
                else {
                    var myaray = new Array("QH7cA");
                    var myarray2 = new Array("#QH7cA");
                    generate(myaray, myarray2, true);
                    enabledControlTextField(document.getElementById("<%= TextBoxPreviousTreatmentNbrInjections.ClientID %>"), true, true);
                }
            });
            $("#<%= RadioButtonPreviousTreatmentPeriocularCSNo.ClientID %>").click(function () {
                if ($('#<%= RadioButtonPreviousTreatmentPeriocularCSYes.ClientID %>').prop("checked") == true) {
                    var myaray = new Array("QH7cA");
                    var myarray2 = new Array('#QH7cA');
                    generate(myaray, myarray2, false);
                    enabledControlTextField(document.getElementById("<%= TextBoxPreviousTreatmentNbrInjections.ClientID %>"), false, false);
                }
                else {
                    var myaray = new Array("QH7cA");
                    var myarray2 = new Array("#QH7cA");
                    generate(myaray, myarray2, true);
                    enabledControlTextField(document.getElementById("<%= TextBoxPreviousTreatmentNbrInjections.ClientID %>"), true, true);
                }
            });

            if ($('#<%= RadioButtonPreviousTreatmentPeriocularCSYes.ClientID %>').prop("checked") == true) {
                var myaray = new Array("QH7cA");
                var myarray2 = new Array('#QH7cA');
                generate(myaray, myarray2, false);
                enabledControlTextField(document.getElementById("<%= TextBoxPreviousTreatmentNbrInjections.ClientID %>"), false, false);
            }
            else {
                var myaray = new Array("QH7cA");
                var myarray2 = new Array("#QH7cA");
                generate(myaray, myarray2, true);
                enabledControlTextField(document.getElementById("<%= TextBoxPreviousTreatmentNbrInjections.ClientID %>"), true, true);
            }


            // Question 7d History ***************************
            $("#<%= RadioButtonPreviousTreatmentSystemicCSYes.ClientID %>").click(function () {
                if ($('#<%= RadioButtonPreviousTreatmentSystemicCSYes.ClientID %>').prop("checked") == true) {
                    var myaray = new Array("QH7dA");
                    var myarray2 = new Array('#QH7dA');
                    generate(myaray, myarray2, false);
                    enabledControlTextField(document.getElementById("<%= TextBoxPreviousTreatmentSystemicDrug.ClientID %>"), false, false);
                    enabledControlTextField(document.getElementById("<%= TextBoxPreviousTreatmentSystemicNbrWeeks.ClientID %>"), false, false);
                }
                else {
                    var myaray = new Array("QH7dA");
                    var myarray2 = new Array("#QH7dA");
                    generate(myaray, myarray2, true);
                    enabledControlTextField(document.getElementById("<%= TextBoxPreviousTreatmentSystemicDrug.ClientID %>"), true, true);
                    enabledControlTextField(document.getElementById("<%= TextBoxPreviousTreatmentSystemicNbrWeeks.ClientID %>"), true, true);
                }
            });
            $("#<%= RadioButtonPreviousTreatmentSystemicCSNo.ClientID %>").click(function () {
                if ($('#<%= RadioButtonPreviousTreatmentSystemicCSYes.ClientID %>').prop("checked") == true) {
                    var myaray = new Array("QH7dA");
                    var myarray2 = new Array('#QH7dA');
                    generate(myaray, myarray2, false);
                    enabledControlTextField(document.getElementById("<%= TextBoxPreviousTreatmentSystemicDrug.ClientID %>"), false, false);
                    enabledControlTextField(document.getElementById("<%= TextBoxPreviousTreatmentSystemicNbrWeeks.ClientID %>"), false, false);
                }
                else {
                    var myaray = new Array("QH7dA");
                    var myarray2 = new Array("#QH7dA");
                    generate(myaray, myarray2, true);
                    enabledControlTextField(document.getElementById("<%= TextBoxPreviousTreatmentSystemicDrug.ClientID %>"), true, true);
                    enabledControlTextField(document.getElementById("<%= TextBoxPreviousTreatmentSystemicNbrWeeks.ClientID %>"), true, true);
                }
            });

            if ($('#<%= RadioButtonPreviousTreatmentSystemicCSYes.ClientID %>').prop("checked") == true) {
                var myaray = new Array("QH7dA");
                var myarray2 = new Array('#QH7dA');
                generate(myaray, myarray2, false);
                enabledControlTextField(document.getElementById("<%= TextBoxPreviousTreatmentSystemicDrug.ClientID %>"), false, false);
                enabledControlTextField(document.getElementById("<%= TextBoxPreviousTreatmentSystemicNbrWeeks.ClientID %>"), false, false);
            }
            else {
                var myaray = new Array("QH7dA");
                var myarray2 = new Array("#QH7dA");
                generate(myaray, myarray2, true);
                enabledControlTextField(document.getElementById("<%= TextBoxPreviousTreatmentSystemicDrug.ClientID %>"), true, true);
                enabledControlTextField(document.getElementById("<%= TextBoxPreviousTreatmentSystemicNbrWeeks.ClientID %>"), true, true);
            }


            // Question 9a History *************************** 
            $("#<%= RadioButtonListRBHistoryFamily.ClientID %>").click(function () {
                if ($('#<%= RadioButtonListRBHistoryFamily.ClientID %>').find('input:checked').val() == ('1')) {
                    var myaray = new Array("QH9a");
                    var myarray2 = new Array("#QH9a");
                    generate(myaray, myarray2, false);

                    enabledControlTextField(document.getElementById("<%= TextBoxHistoryFamilyRelationship.ClientID %>"), false, false);
                }
                else {
                    var myaray = new Array("QH9a");
                    var myarray2 = new Array("#QH9a");
                    generate(myaray, myarray2, true);

                    enabledControlTextField(document.getElementById("<%= TextBoxHistoryFamilyRelationship.ClientID %>"), true, true);
                }
            });

            if ($('#<%= RadioButtonListRBHistoryFamily.ClientID %>').find('input:checked').val() == ('1')) {
                var myaray = new Array("QH9a");
                var myarray2 = new Array("#QH9a");
                generate(myaray, myarray2, false);

                enabledControlTextField(document.getElementById("<%= TextBoxHistoryFamilyRelationship.ClientID %>"), false, false);
            }
            else {
                var myaray = new Array("QH9a");
                var myarray2 = new Array("#QH9a");
                generate(myaray, myarray2, true);

                enabledControlTextField(document.getElementById("<%= TextBoxHistoryFamilyRelationship.ClientID %>"), true, true);
            }


            // Question 3a Clinical Examination Findings *************************** 
            $("#<%= RadioButtonDendritesPresentYes.ClientID %>").click(function () {
                if ($('#<%= RadioButtonDendritesPresentYes.ClientID %>').prop("checked") == true) {
                    var myaray = new Array("QC3aA", "QC3aB");
                    var myarray2 = new Array('#QC3aB');
                    generate(myaray, myarray2, false);
                }
                else {
                    var myaray = new Array("QC3aA", "QC3aB");
                    var myarray2 = new Array('#QC3aB');
                    generate(myaray, myarray2, true);
                }
            });
            $("#<%= RadioButtonDendritesPresentNo.ClientID %>").click(function () {
                if ($('#<%= RadioButtonDendritesPresentYes.ClientID %>').prop("checked") == true) {
                    var myaray = new Array("QC3aA", "QC3aB");
                    var myarray2 = new Array('#QC3aB');
                    generate(myaray, myarray2, false);
                }
                else {
                    var myaray = new Array("QC3aA", "QC3aB");
                    var myarray2 = new Array('#QC3aB');
                    generate(myaray, myarray2, true);
                }
            });

            if ($('#<%= RadioButtonDendritesPresentYes.ClientID %>').prop("checked") == true) {
                var myaray = new Array("QC3aA", "QC3aB");
                var myarray2 = new Array('#QC3aB');
                generate(myaray, myarray2, false);
            }
            else {
                var myaray = new Array("QC3aA", "QC3aB");
                var myarray2 = new Array('#QC3aB');
                generate(myaray, myarray2, true);
            }


            // Question 9 Clinical Examination Findings *************************** 





            // Question 13 Clinical Examination Findings ***************************
            $("#<%= RadioButtonCMEPresentYes.ClientID %>").click(function () {
                if ($('#<%= RadioButtonCMEPresentYes.ClientID %>').prop("checked") == true) {
                    var myaray = new Array("QC13A");
                    var myarray2 = new Array("#QC13A");
                    generate(myaray, myarray2, false);
                    enabledControlTextField(document.getElementById("<%= TextBoxOCTCentralSubfieldThickness.ClientID %>"), false, false);
                }
                else {
                    var myaray = new Array("QC13A");
                    var myarray2 = new Array("#QC13A");
                    generate(myaray, myarray2, true);
                    enabledControlTextField(document.getElementById("<%= TextBoxOCTCentralSubfieldThickness.ClientID %>"), true, true);
                }
            });
            $("#<%= RadioButtonCMEPresentNo.ClientID %>").click(function () {
                if ($('#<%= RadioButtonCMEPresentYes.ClientID %>').prop("checked") == true) {
                    var myaray = new Array("QC13A");
                    var myarray2 = new Array("#QC13A");
                    generate(myaray, myarray2, false);
                    enabledControlTextField(document.getElementById("<%= TextBoxOCTCentralSubfieldThickness.ClientID %>"), false, false);
                }
                else {
                    var myaray = new Array("QC13A");
                    var myarray2 = new Array("#QC13A");
                    generate(myaray, myarray2, true);
                    enabledControlTextField(document.getElementById("<%= TextBoxOCTCentralSubfieldThickness.ClientID %>"), true, true);
                }
            });

            if ($('#<%= RadioButtonCMEPresentYes.ClientID %>').prop("checked") == true) {
                var myaray = new Array("QC13A");
                var myarray2 = new Array("#QC13A");
                generate(myaray, myarray2, false);
                enabledControlTextField(document.getElementById("<%= TextBoxOCTCentralSubfieldThickness.ClientID %>"), false, false);
            }
            else {
                var myaray = new Array("QC13A");
                var myarray2 = new Array("#QC13A");
                generate(myaray, myarray2, true);
                enabledControlTextField(document.getElementById("<%= TextBoxOCTCentralSubfieldThickness.ClientID %>"), true, true);
            }


            // Question 13f Clinical Examination Findings *************************** 
            $("#<%= RadioButtonListRBChorioretinalPathology.ClientID %>").click(function () {
                if ($('#<%= RadioButtonListRBChorioretinalPathology.ClientID %>').find('input:checked').val() == ('175')) {
                    var myaray = new Array("QC13fHeader", "QC13fA", "QC13fB", "QC13fC", "QC13fD", "QC13fE", "QC13fF", "QC13fG", "QC13fH", "QC13fI", "QC13fJ");
                    var myarray2 = new Array("#QC13fB", "#QC13fD", "#QC13fF", "#QC13fH", "#QC13fJ");
                    generate(myaray, myarray2, false);
                }
                else {
                    var myaray = new Array("QC13fHeader", "QC13fA", "QC13fB", "QC13fC", "QC13fD", "QC13fE", "QC13fF", "QC13fG", "QC13fH", "QC13fI", "QC13fJ");
                    var myarray2 = new Array("#QC13fB", "#QC13fD", "#QC13fF", "#QC13fH", "#QC13fJ");
                    generate(myaray, myarray2, true);
                }
            });

            if ($('#<%= RadioButtonListRBChorioretinalPathology.ClientID %>').find('input:checked').val() == ('175')) {
                var myaray = new Array("QC13fHeader", "QC13fA", "QC13fB", "QC13fC", "QC13fD", "QC13fE", "QC13fF", "QC13fG", "QC13fH", "QC13fI", "QC13fJ");
                var myarray2 = new Array("#QC13fB", "#QC13fD", "#QC13fF", "#QC13fH", "#QC13fJ");
                generate(myaray, myarray2, false);
            }
            else {
                var myaray = new Array("QC13fHeader", "QC13fA", "QC13fB", "QC13fC", "QC13fD", "QC13fE", "QC13fF", "QC13fG", "QC13fH", "QC13fI", "QC13fJ");
                var myarray2 = new Array("#QC13fB", "#QC13fD", "#QC13fF", "#QC13fH", "#QC13fJ");
                generate(myaray, myarray2, true);
            }


            // Question 13 option toggle ***************************
            $("#<%= RadioButtonListRBRetinalDetachment.ClientID %>").click(function () {
                if ($('#<%= RadioButtonListRBRetinalDetachment.ClientID %>').find('input:checked').val() == ('14')) {
                    var myaray = new Array("Q13aText");
                    var myarray2 = new Array("#Q13aText");
                    generate(myaray, myarray2, false);

                    enabledControlTextField(document.getElementById("<%= TextBoxRBRetinalDetachmentOther.ClientID %>"), false, false);
                }
                else {
                    var myaray = new Array("Q13aText");
                    var myarray2 = new Array("#Q13aText");
                    generate(myaray, myarray2, true);

                    enabledControlTextField(document.getElementById("<%= TextBoxRBRetinalDetachmentOther.ClientID %>"), true, true);
                }
            });
            if ($('#<%= RadioButtonListRBRetinalDetachment.ClientID %>').find('input:checked').val() == ('14')) {
                var myaray = new Array("Q13aText");
                var myarray2 = new Array("#Q13aText");
                generate(myaray, myarray2, false);

                enabledControlTextField(document.getElementById("<%= TextBoxRBRetinalDetachmentOther.ClientID %>"), false, false);
            }
            else {
                var myaray = new Array("Q13aText");
                var myarray2 = new Array("#Q13aText");
                generate(myaray, myarray2, true);

                enabledControlTextField(document.getElementById("<%= TextBoxRBRetinalDetachmentOther.ClientID %>"), true, true);
            }


            // QM4 ***************************
            $("#<%= RadioButtonImmunomodulatoryTherapyYes.ClientID %>").click(function () {
                if ($('#<%= RadioButtonImmunomodulatoryTherapyYes.ClientID %>').prop("checked") == true) {
                    var myaray = new Array("QM4aA", "QM4bA", "QM4bB", "QM4cA", "QM4cB", "QM4dA", "QM4dB");
                    var myarray2 = new Array('#QM4bB', '#QM4dB');
                    generate(myaray, myarray2, false);

                    enabledControlTextField(document.getElementById("<%= TextBoxImmunDrugDosage.ClientID %>"), false, false);
                    enabledControlTextField(document.getElementById("<%= TextBoxImmunDurationTherapy.ClientID %>"), false, false);
                }
                else {
                    var myaray = new Array("QM4aA", "QM4bA", "QM4bB", "QM4cA", "QM4cB", "QM4dA", "QM4dB");
                    var myarray2 = new Array('#QM4bB', '#QM4dB');
                    generate(myaray, myarray2, true);

                    enabledControlTextField(document.getElementById("<%= TextBoxImmunDrugDosage.ClientID %>"), true, true);
                    enabledControlTextField(document.getElementById("<%= TextBoxImmunDurationTherapy.ClientID %>"), true, true);
                }
            });
            $("#<%= RadioButtonImmunomodulatoryTherapyNo.ClientID %>").click(function () {
                if ($('#<%= RadioButtonImmunomodulatoryTherapyYes.ClientID %>').prop("checked") == true) {
                    var myaray = new Array("QM4aA", "QM4bA", "QM4bB", "QM4cA", "QM4cB", "QM4dA", "QM4dB");
                    var myarray2 = new Array('#QM4bB', '#QM4dB');
                    generate(myaray, myarray2, false);

                    enabledControlTextField(document.getElementById("<%= TextBoxImmunDrugDosage.ClientID %>"), false, false);
                    enabledControlTextField(document.getElementById("<%= TextBoxImmunDurationTherapy.ClientID %>"), false, false);
                }
                else {
                    var myaray = new Array("QM4aA", "QM4bA", "QM4bB", "QM4cA", "QM4cB", "QM4dA", "QM4dB");
                    var myarray2 = new Array('#QM4bB', '#QM4dB');
                    generate(myaray, myarray2, true);

                    enabledControlTextField(document.getElementById("<%= TextBoxImmunDrugDosage.ClientID %>"), true, true);
                    enabledControlTextField(document.getElementById("<%= TextBoxImmunDurationTherapy.ClientID %>"), true, true);
                }
            });
            if ($('#<%= RadioButtonImmunomodulatoryTherapyYes.ClientID %>').prop("checked") == true) {
                var myaray = new Array("QM4aA", "QM4bA", "QM4bB", "QM4cA", "QM4cB", "QM4dA", "QM4dB");
                var myarray2 = new Array('#QM4bB', '#QM4dB');
                generate(myaray, myarray2, false);

                enabledControlTextField(document.getElementById("<%= TextBoxImmunDrugDosage.ClientID %>"), false, false);
                enabledControlTextField(document.getElementById("<%= TextBoxImmunDurationTherapy.ClientID %>"), false, false);
            }
            else {
                var myaray = new Array("QM4aA", "QM4bA", "QM4bB", "QM4cA", "QM4cB", "QM4dA", "QM4dB");
                var myarray2 = new Array('#QM4bB', '#QM4dB');
                generate(myaray, myarray2, true);

                enabledControlTextField(document.getElementById("<%= TextBoxImmunDrugDosage.ClientID %>"), true, true);
                enabledControlTextField(document.getElementById("<%= TextBoxImmunDurationTherapy.ClientID %>"), true, true);
            }

            // QO9 ***************************
            $("#<%= RadioButtonListOutcomeGonioscopy.ClientID %>").click(function () {
                if ($('#<%= RadioButtonListOutcomeGonioscopy.ClientID %>').find('input:checked').val() == ('1')) {
                    var myaray = new Array("QO9bA", "QO9bA", "QO9bB", "QO9bC", "QO9bD", "QO9bE", "QO9bF", "QO9bG");
                    var myarray2 = new Array('#QO9bC', '#QO9bE', '#QO9bG');
                    generate(myaray, myarray2, false);

                    enabledControlDropDown(document.getElementById("<%= DropDownListOutcomeGonioscopyMonth.ClientID %>"), false, false);
                    enabledControlDropDown(document.getElementById("<%= DropDownListOutcomeGonioscopyYear.ClientID %>"), false, false);
                }
                else {
                    var myaray = new Array("QO9bA", "QO9bA", "QO9bB", "QO9bC", "QO9bD", "QO9bE", "QO9bF", "QO9bG");
                    var myarray2 = new Array('#QO9bC', '#QO9bE', '#QO9bG');
                    generate(myaray, myarray2, true);

                    enabledControlDropDown(document.getElementById("<%= DropDownListOutcomeGonioscopyMonth.ClientID %>"), true, true);
                    enabledControlDropDown(document.getElementById("<%= DropDownListOutcomeGonioscopyYear.ClientID %>"), true, true);
                }
            });

            if ($('#<%= RadioButtonListOutcomeGonioscopy.ClientID %>').find('input:checked').val() == ('1')) {
                var myaray = new Array("QO9bA", "QO9bA", "QO9bB", "QO9bC", "QO9bD", "QO9bE", "QO9bF", "QO9bG");
                var myarray2 = new Array('#QO9bC', '#QO9bE', '#QO9bG');
                generate(myaray, myarray2, false);

                enabledControlDropDown(document.getElementById("<%= DropDownListOutcomeGonioscopyMonth.ClientID %>"), false, false);
                enabledControlDropDown(document.getElementById("<%= DropDownListOutcomeGonioscopyYear.ClientID %>"), false, false);
            }
            else {
                var myaray = new Array("QO9bA", "QO9bA", "QO9bB", "QO9bC", "QO9bD", "QO9bE", "QO9bF", "QO9bG");
                var myarray2 = new Array('#QO9bC', '#QO9bE', '#QO9bG');
                generate(myaray, myarray2, true);

                enabledControlDropDown(document.getElementById("<%= DropDownListOutcomeGonioscopyMonth.ClientID %>"), true, true);
                enabledControlDropDown(document.getElementById("<%= DropDownListOutcomeGonioscopyYear.ClientID %>"), true, true);
            }


            // QO12
            $("#<%= RadioButtonOutcomeCMEPresentYes.ClientID %>").click(function () {
                if ($('#<%= RadioButtonOutcomeCMEPresentYes.ClientID %>').prop("checked") == true) {
                    var myaray = new Array("QO12", "QO12B");
                    var myarray2 = new Array("#QO12B");
                    generate(myaray, myarray2, false);
                    enabledControlTextField(document.getElementById("<%= TextBoxOutcomeOCTCentralSubfieldThickness.ClientID %>"), false, false);
                }
                else {
                    var myaray = new Array("QO12", "QO12B");
                    var myarray2 = new Array("#QO12B");
                    generate(myaray, myarray2, true);
                    enabledControlTextField(document.getElementById("<%= TextBoxOutcomeOCTCentralSubfieldThickness.ClientID %>"), true, true);
                }
            });
            $("#<%= RadioButtonOutcomeCMEPresentNo.ClientID %>").click(function () {
                if ($('#<%= RadioButtonOutcomeCMEPresentYes.ClientID %>').prop("checked") == true) {
                    var myaray = new Array("QO12", "QO12B");
                    var myarray2 = new Array("#QO12B");
                    generate(myaray, myarray2, false);
                    enabledControlTextField(document.getElementById("<%= TextBoxOutcomeOCTCentralSubfieldThickness.ClientID %>"), false, false);
                }
                else {
                    var myaray = new Array("QO12", "QO12B");
                    var myarray2 = new Array("#QO12B");
                    generate(myaray, myarray2, true);
                    enabledControlTextField(document.getElementById("<%= TextBoxOutcomeOCTCentralSubfieldThickness.ClientID %>"), true, true);
                }
            });
            if ($('#<%= RadioButtonOutcomeCMEPresentYes.ClientID %>').prop("checked") == true) {
                var myaray = new Array("QO12", "QO12B");
                var myarray2 = new Array("#QO12B");
                generate(myaray, myarray2, false);
                enabledControlTextField(document.getElementById("<%= TextBoxOutcomeOCTCentralSubfieldThickness.ClientID %>"), false, false);
            }
            else {
                var myaray = new Array("QO12", "QO12B");
                var myarray2 = new Array("#QO12B");
                generate(myaray, myarray2, true);
                enabledControlTextField(document.getElementById("<%= TextBoxOutcomeOCTCentralSubfieldThickness.ClientID %>"), true, true);
            }


            // Other text outcomes
            $("#<%= RadioButtonListRBOutcomeRetinalDetachment.ClientID %>").click(function () {
                if ($('#<%= RadioButtonListRBOutcomeRetinalDetachment.ClientID %>').find('input:checked').val() == ('14')) {

                    enabledControlTextField(document.getElementById("<%= TextBoxOutcomeRetinalDetachmentText.ClientID %>"), false, false);
                }
                else {

                    enabledControlTextField(document.getElementById("<%= TextBoxOutcomeRetinalDetachmentText.ClientID %>"), true, true);
                }
            });
            if ($('#<%= RadioButtonListRBOutcomeRetinalDetachment.ClientID %>').find('input:checked').val() == ('14')) {

                enabledControlTextField(document.getElementById("<%= TextBoxOutcomeRetinalDetachmentText.ClientID %>"), false, false);
            }
            else {

                enabledControlTextField(document.getElementById("<%= TextBoxOutcomeRetinalDetachmentText.ClientID %>"), true, true);
            }


            // Question 5 - Clinical Examination
            $("#<%= RadioButtonKPsYes.ClientID %>").click(function () {
                if ($('#<%= RadioButtonKPsYes.ClientID %>').prop("checked") == true) {
                    var myaray = new Array("QCE5aA", "QCE5aB", "QCE5bA", "QCE5bB");
                    var myarray2 = new Array('#QCE5aB', '#QCE5bB');
                    generate(myaray, myarray2, false);
                }
                else {
                    var myaray = new Array("QCE5aA", "QCE5aB", "QCE5bA", "QCE5bB");
                    var myarray2 = new Array('#QCE5aB', '#QCE5bB');
                    generate(myaray, myarray2, true);
                }
            });
            $("#<%= RadioButtonKPsNo.ClientID %>").click(function () {
                if ($('#<%= RadioButtonKPsYes.ClientID %>').prop("checked") == true) {
                    var myaray = new Array("QCE5aA", "QCE5aB", "QCE5bA", "QCE5bB");
                    var myarray2 = new Array('#QCE5aB', '#QCE5bB');
                    generate(myaray, myarray2, false);
                }
                else {
                    var myaray = new Array("QCE5aA", "QCE5aB", "QCE5bA", "QCE5bB");
                    var myarray2 = new Array('#QCE5aB', '#QCE5bB');
                    generate(myaray, myarray2, true);
                }
            });
            if ($('#<%= RadioButtonKPsYes.ClientID %>').prop("checked") == true) {
                var myaray = new Array("QCE5aA", "QCE5aB", "QCE5bA", "QCE5bB");
                var myarray2 = new Array('#QCE5aB', '#QCE5bB');
                generate(myaray, myarray2, false);
            }
            else {
                var myaray = new Array("QCE5aA", "QCE5aB", "QCE5bA", "QCE5bB");
                var myarray2 = new Array('#QCE5aB', '#QCE5bB');
                generate(myaray, myarray2, true);
            }


            // Question 7d - Clinical Examination
            $("#<%= RadioButtonListRBIrisNodulesPresent.ClientID %>").click(function () {
                if ($('#<%= RadioButtonListRBIrisNodulesPresent.ClientID %>').find('input:checked').val() == ('175')) {
                    var myaray = new Array("QCE7eA", "QCE7eB");
                    var myarray2 = new Array('#QCE7eB');
                    generate(myaray, myarray2, false);
                    enabledControlTextField(document.getElementById("<%= TextBoxRBIrisNoduleClockHours.ClientID %>"), false, false);
                }
                else {
                    var myaray = new Array("QCE7eA", "QCE7eB");
                    var myarray2 = new Array('#QCE7eB');
                    generate(myaray, myarray2, true);
                    enabledControlTextField(document.getElementById("<%= TextBoxRBIrisNoduleClockHours.ClientID %>"), true, true);
                }
            });
            if ($('#<%= RadioButtonListRBIrisNodulesPresent.ClientID %>').find('input:checked').val() == ('175')) {
                var myaray = new Array("QCE7eA", "QCE7eB");
                var myarray2 = new Array('#QCE7eB');
                generate(myaray, myarray2, false);
                enabledControlTextField(document.getElementById("<%= TextBoxRBIrisNoduleClockHours.ClientID %>"), false, false);
            }
            else {
                var myaray = new Array("QCE7eA", "QCE7eB");
                var myarray2 = new Array('#QCE7eB');
                generate(myaray, myarray2, true);
                enabledControlTextField(document.getElementById("<%= TextBoxRBIrisNoduleClockHours.ClientID %>"), true, true);
            }


            // Question 4 - Outcomes
            $("#<%= RadioButtonOutcomeKPsYes.ClientID %>").click(function () {
                if ($('#<%= RadioButtonOutcomeKPsYes.ClientID %>').prop("checked") == true) {
                    var myaray = new Array("QO4aA", "QO4aB", "QO4bA", "QO4bB");
                    var myarray2 = new Array('#QO4aB', '#QO4bB');
                    generate(myaray, myarray2, false);
                }
                else {
                    var myaray = new Array("QO4aA", "QO4aB", "QO4bA", "QO4bB");
                    var myarray2 = new Array('#QO4aB', '#QO4bB');
                    generate(myaray, myarray2, true);
                }
            });
            $("#<%= RadioButtonOutcomeKPsNo.ClientID %>").click(function () {
                if ($('#<%= RadioButtonOutcomeKPsYes.ClientID %>').prop("checked") == true) {
                    var myaray = new Array("QO4aA", "QO4aB", "QO4bA", "QO4bB");
                    var myarray2 = new Array('#QO4aB', '#QO4bB');
                    generate(myaray, myarray2, false);
                }
                else {
                    var myaray = new Array("QO4aA", "QO4aB", "QO4bA", "QO4bB");
                    var myarray2 = new Array('#QO4aB', '#QO4bB');
                    generate(myaray, myarray2, true);
                }
            });
            if ($('#<%= RadioButtonOutcomeKPsYes.ClientID %>').prop("checked") == true) {
                var myaray = new Array("QO4aA", "QO4aB", "QO4bA", "QO4bB");
                var myarray2 = new Array('#QO4aB', '#QO4bB');
                generate(myaray, myarray2, false);
            }
            else {
                var myaray = new Array("QO4aA", "QO4aB", "QO4bA", "QO4bB");
                var myarray2 = new Array('#QO4aB', '#QO4bB');
                generate(myaray, myarray2, true);
            }


            // Question 6 - Outcomes ****************
            $("#<%= RadioButtonListRBOutcomePosteriorSynechiae.ClientID %>").click(function () {
                if ($('#<%= RadioButtonListRBOutcomePosteriorSynechiae.ClientID %>').find('input:checked').val() == ('132')) {
                    var myaray = new Array("QO6aA", "QO6bA");
                    var myarray2 = new Array(null);
                    generate(myaray, myarray2, true);

                }
                else {
                    var myaray = new Array("QO6aA", "QO6bA");
                    var myarray2 = new Array(null);
                    generate(myaray, myarray2, false);

                }
            });
            if ($('#<%= RadioButtonListRBOutcomePosteriorSynechiae.ClientID %>').find('input:checked').val() == ('132')) {
                var myaray = new Array("QO6aA", "QO6bA");
                var myarray2 = new Array(null);
                generate(myaray, myarray2, true);

            }
            else {
                var myaray = new Array("QO6aA", "QO6bA");
                var myarray2 = new Array(null);
                generate(myaray, myarray2, false);

            }


            // Question 12a - Outcomes
            $("#<%= RadioButtonListRBOutcomeChorioretinalPathology.ClientID %>").click(function () {
                if ($('#<%= RadioButtonListRBOutcomeChorioretinalPathology.ClientID %>').find('input:checked').val() == ('175')) {
                    var myaray = new Array("QO12aA", "QO12bA", "QO12bB", "QO12cA", "QO12cB", "QO12dA", "QO12dB", "QO12eA", "QO12eB", "QO12fA", "QO12fB", "QO12gA", "QO12gB", "QO12hA", "QO12hB", "QO12iA", "QO12iB", "QO12jA", "QO12jB");
                    var myarray2 = new Array('#QO12bB', '#QO12cB', '#QO12dB', '#QO12eB', '#QO12fB', '#QO12gB', '#QO12hB', '#QO12iB');
                    generate(myaray, myarray2, false);
                }
                else {
                    var myaray = new Array("QO12aA", "QO12bA", "QO12bB", "QO12cA", "QO12cB", "QO12dA", "QO12dB", "QO12eA", "QO12eB", "QO12fA", "QO12fB", "QO12gA", "QO12gB", "QO12hA", "QO12hB", "QO12iA", "QO12iB", "QO12jA", "QO12jB");
                    var myarray2 = new Array('#QO12bB', '#QO12cB', '#QO12dB', '#QO12eB', '#QO12fB', '#QO12gB', '#QO12hB', '#QO12iB', '#QO12jB');
                    generate(myaray, myarray2, true);
                    enabledControlTextField(document.getElementById("<%= TextBoxOutcomeRetinalDetachmentText.ClientID %>"), true, true);
                }
            });
            if ($('#<%= RadioButtonListRBOutcomeChorioretinalPathology.ClientID %>').find('input:checked').val() == ('175')) {
                var myaray = new Array("QO12aA", "QO12bA", "QO12bB", "QO12cA", "QO12cB", "QO12dA", "QO12dB", "QO12eA", "QO12eB", "QO12fA", "QO12fB", "QO12gA", "QO12gB", "QO12hA", "QO12hB", "QO12iA", "QO12iB", "QO12jA", "QO12jB");
                var myarray2 = new Array('#QO12bB', '#QO12cB', '#QO12dB', '#QO12eB', '#QO12fB', '#QO12gB', '#QO12hB', '#QO12iB');
                generate(myaray, myarray2, false);
            }
            else {
                var myaray = new Array("QO12aA", "QO12bA", "QO12bB", "QO12cA", "QO12cB", "QO12dA", "QO12dB", "QO12eA", "QO12eB", "QO12fA", "QO12fB", "QO12gA", "QO12gB", "QO12hA", "QO12hB", "QO12iA", "QO12iB", "QO12jA", "QO12jB");
                var myarray2 = new Array('#QO12bB', '#QO12cB', '#QO12dB', '#QO12eB', '#QO12fB', '#QO12gB', '#QO12hB', '#QO12iB', '#QO12jB');
                generate(myaray, myarray2, true);
                enabledControlTextField(document.getElementById("<%= TextBoxOutcomeRetinalDetachmentText.ClientID %>"), true, true);
            }
        });
    </script>
</asp:Content>

