﻿<%@ Page Title="" Language="C#" MasterPageFile="~/PIMMasterPage.master" AutoEventWireup="true" CodeFile="EsotropiaChart.aspx.cs" Inherits="abo_EsotropiaChart" %>

<asp:Content ID="Content1" ContentPlaceHolderID="head" Runat="Server">

    <link type="text/css" href="../../common/css/atooltip.css" rel="stylesheet"  media="screen" />

    <style type="text/css">
        .bginputa{
	        float:right;
	        margin:21px 0 0 1px;
	        background: url(../common/images/orange_button_with_arrow.png) no-repeat;
	        overflow:hidden;
	        height:35px;

        }
        .bginputa a{
	        color: white;
	        text-align:center;
	        height:35px;
	        line-height:28px;
	        padding:0 14px 15px  ;
	        cursor:pointer;
	        float:left;
	        border:none;
	        text-decoration:none;
	        font-family:Arial, Helvetica, sans-serif; 
	        font-size:12px; 
	        font-weight:bold; 
	        letter-spacing: 0px;
        }
        .bginputa a:hover{
	        text-decoration:none;
        }
        .right {
            float:right;
            text-align:right;
        }
    </style>

</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder1" Runat="Server">
    <asp:HiddenField ID="HiddenFieldRecordIdentifier" runat="server"/>
        <!-- Esotropia pim -->
        <div class="pim">
            <div class="record-ident clearfix">
                <h3 class="record-first">RECORD IDENTIFIER: <asp:Literal runat="server" ID="LiteralRecordIdentifier"></asp:Literal></h3>
                <h3 class="record-second"><asp:Literal runat="server" ID="LiteralAbstractionNumber"></asp:Literal></h3>
            </div>
            <!-- History Section -->
            <table>
                <!-- Header - History -->
                <tr>
                    <th colspan="2" width="910px"><p>History</p></th>
                </tr>
                <!-- Question 1 -->
                <tr class="table_body">
                    <td style="width:70%;">
                        <strong>1. Date of birth:</strong>
                        <asp:Label ID="LabelMonthOfBirth" runat="server" ForeColor="Red" Visible="false" Text="<br />Please enter Month "></asp:Label>
                        <asp:Label ID="LabelYearOfBirth" runat="server" ForeColor="Red" Visible="false"  Text="<br />Please enter Year"></asp:Label>
                    </td>
                    <td>
                        <asp:DropDownList ID="DropDownListMonthOfBirth" DataValueField="MonthID" DataTextField="MonthName" runat="server" />
                        <asp:DropDownList ID="DropDownListYearOfBirth" DataValueField="YearID" DataTextField="YearName" runat="server" />
                    </td>
                </tr>
                <!-- Question 2 -->
                <tr class="table_body_bg">
                    <td>
                        <strong>2. Date of surgery:</strong>
                        <asp:Label ID="LabelMonthOfSurgery" runat="server" Visible="false"  ForeColor="Red" Text="<br />Please enter Month"></asp:Label>
                        <asp:Label ID="LabelYearOfSurgery" runat="server" Visible="false"  ForeColor="Red"  Text="<br />Please enter Year "></asp:Label>
                    </td>
                    <td>

                        <asp:DropDownList ID="DropDownListMonthOfSurgery" DataValueField="MonthID" DataTextField="MonthName" runat="server" />
                        <asp:DropDownList ID="DropDownListYearOfSurgery" DataValueField="YearID" DataTextField="YearName" runat="server" />
                    </td>
                </tr>
                <!-- Question 3 -->
                <tr class="table_body">
                    <td>
                        <strong>3. Date of follow-up exam #1:</strong> (Closest exam 1-2 mos. post surgery.)
                        <asp:Label ID="LabelMonthOfFollowUp1" runat="server" Visible="false"  ForeColor="Red" Text="<br />Please enter Month "></asp:Label>
                        <asp:Label ID="LabelYearOfFollowUp1" runat="server" Visible="false"  ForeColor="Red"  Text="<br />Please enter  Year"></asp:Label>
                    </td>
                    <td>
                        <asp:DropDownList ID="DropDownListMonthOfFollowUp1" DataValueField="MonthID" DataTextField="MonthName" runat="server" />
                        <asp:DropDownList ID="DropDownListYearOfFollowUp1" DataValueField="YearID" DataTextField="YearName" runat="server" />
                    </td>
                </tr>
                <!-- Question 4 -->
                <tr class="table_body_bg">
                    <td>
                        <strong>4. Date of follow-up exam #2:</strong> (Closest exam 6-8 mos. post surgery.)
                        <asp:Label ID="LabelMonthOfFollowUp2" runat="server" Visible="false"  ForeColor="Red" Text="<br />Please enter Month"></asp:Label>
                        <asp:Label ID="LabelYearOfFollowUp2" runat="server" Visible="false"  ForeColor="Red"  Text="<br />Please enter  Year "></asp:Label>
                        <asp:Label ID="Labelinitialage" runat="server" Visible="false"  ForeColor="Red" Text="<br />"></asp:Label>
                    </td>
                    <td>
                        <asp:DropDownList ID="DropDownListMonthOfFollowUp2" DataValueField="MonthID" DataTextField="MonthName" runat="server" />
                        <asp:DropDownList ID="DropDownListYearOfFollowUp2" DataValueField="YearID" DataTextField="YearName" runat="server" />
                    </td>
                </tr>
                <!-- Question 5 -->
                <tr class="table_body">
                    <td width="60%">
                        <strong>5. Does this patient have nonaccommodative comitant esotropia or partially accommodative comitant esotropia?</strong>
                        <asp:Label ID="LabelPatientHasEsotropia" runat="server" Visible="false"  ForeColor="Red" Text="<br />Please enter"></asp:Label>
                    </td>
                    <td>
                        <asp:RadioButtonList ID="RadioButtonListPatientHasEsotropia" RepeatDirection="Vertical" CssClass="aspxList" runat="server">
                            <asp:ListItem Value="6">Nonaccommodative</asp:ListItem>
                            <asp:ListItem Value="7">Partially accommodative</asp:ListItem>
                        </asp:RadioButtonList>
                    </td>
                </tr>
                <!-- Question 6 -->
                <tr class="table_body_bg">
                    <td>
                        <strong>6. Is there a family history of strabismus?</strong>
                        <asp:Label ID="LabelStrabismus" runat="server" Visible="false"  ForeColor="Red" Text="<br />Please enter"></asp:Label>
                    </td>
                    <td>
                        <asp:RadioButtonList ID="RadioButtonListStrabismus" RepeatDirection="Vertical" CssClass="aspxList"  runat="server">
                            <asp:ListItem Value="1">Yes</asp:ListItem>
                            <asp:ListItem Value="2">No</asp:ListItem>
                            <asp:ListItem Value="3">Not noted</asp:ListItem>
                            <asp:ListItem Value="176">Unknown</asp:ListItem>
                        </asp:RadioButtonList>
                    </td>
                </tr>
                <!-- Question 7 -->
                <tr class="table_body">
                    <td>
                        <strong>7. Does the patient have a history of prematurity?</strong>&nbsp;<a href="#"><img id="QH7" src="../../common/images/tip.gif" alt="Tip" /></a>
                        <asp:Label ID="LabelPermutality" runat="server" Visible="false"  ForeColor="Red" Text="<br />Please enter"></asp:Label>
                    </td>
                    <td>
                        <asp:RadioButtonList ID="RadioButtonListPermutality" RepeatDirection="Horizontal" CssClass="aspxList"  runat="server">
                            <asp:ListItem Value="1">Yes</asp:ListItem>
                            <asp:ListItem Value="2">No</asp:ListItem>
                            <asp:ListItem Value="3">Not noted</asp:ListItem>
                        </asp:RadioButtonList>
                    </td>
                </tr>
            </table>
            <br />
            <!-- Initial Examination -->
            <table>
                <tr width="910px">
                    <th colspan="3" width="910px"><p>Initial Examination</p></th>
                </tr>
                <!-- Question 1 -->
                <tr class="table_body">
                    <td colspan="2">
                        <strong>1. Was visual acuity (with correction if medically appropriate) evaluated?</strong>
                        <asp:Label ID="LabelVisualAcuityEvaluated" runat="server" Visible="false"  ForeColor="Red" Text="<br />Please enter"></asp:Label>
                    </td>
                    <td>
                        <asp:RadioButton ID="RadioButtonVisualAcuityEvaluatedYes" runat="server" GroupName="VisualAcuityEvaluated" Text="&nbsp;Yes" onclick="QI1isClicked();" />&nbsp;&nbsp;
                        <asp:RadioButton ID="RadioButtonVisualAcuityEvaluatedNo" runat="server" GroupName="VisualAcuityEvaluated" Text="&nbsp;No" onclick="QI1isClicked();" />
                    </td>
                </tr>
                <!-- Question 1a -->
                <tr class="table_body">
                    <td rowspan="2" style="width:50%;">
                        <span id="QI1txta"><strong>1a. Best corrected visual acuity on initial examination</strong></span>&nbsp;<a href="#"><img id="QI1a" src="../../common/images/tip.gif" alt="Tip" /></a>
                        <asp:Label ID="LabelBestCorrectedVisualOD" runat="server" Visible="false"  ForeColor="Red" Text="<br />Please enter Best Corrected Visual OD "></asp:Label>
                        <asp:Label ID="LabelBestCorrectedVisualOS" runat="server" Visible="false"  ForeColor="Red" Text="<br />Please enter Best Corrected Visual OS "></asp:Label>
                    </td>
                    <td width="20%" id="QI1txtl"><span class="right">OD</span></td>
                    <td id="QI1txtb">
                        <label>20&nbsp;/&nbsp;</label>
                        <asp:DropDownList ID="DropDownListBestCorrectedVisualOD" DataValueField="examValue" DataTextField="examLabel" runat="server" />
                    </td>
                </tr>
                <tr class="table_body">
                    <td id="QI1txtc"><span class="right">OS</span></td>
                    <td id="QI1txtd">
                        <label>20&nbsp;/&nbsp;</label>
                        <asp:DropDownList ID="DropDownListBestCorrectedVisualOS" DataValueField="examValue" DataTextField="examLabel" runat="server" />
                    </td>
                </tr>
                <!-- Question 1b -->
                <tr class="table_body">
                    <td rowspan="3" id="QI1txte">
                        <strong>1b. Method</strong>
                        <asp:Label ID="LabelOptotypeUsed" runat="server" Visible="false"  ForeColor="Red" Text="<br />Please enter Optotype Used"></asp:Label>
                        <asp:Label ID="LabelFixationalVision" runat="server" Visible="false"  ForeColor="Red" Text="<br />Please enter Fixational Vision"></asp:Label>
                        <asp:Label ID="LabelTellerCards" runat="server" Visible="false"  ForeColor="Red" Text="<br />Please enter Teller Cards"></asp:Label>
                    </td>
                    <td id="QI1txtf"><span class="right">Optotype (symbols, letters or numbers)</span></td>
                    <td id="QI1txtg">
                        <asp:RadioButton ID="RadioButtonOptotypeUsedYes" runat="server" GroupName="OptotypeUsed" Text="&nbsp;Yes" />&nbsp;&nbsp;
                        <asp:RadioButton ID="RadioButtonOptotypeUsedNo" runat="server" GroupName="OptotypeUsed" Text="&nbsp;No" />
                        <!--<br /><asp:DropDownList ID="DropDownListMethodOptotypesType" DataValueField="examValue" DataTextField="examLabel" runat="server" /> -->
                    </td>
                </tr>
                <tr class="table_body">
                    <td id="QI1txth"><span class="right">Fixational vision or<br />equivalent&nbsp;<a href="#"><img id="QI1b" src="../../common/images/tip.gif" alt="Tip" /></a></span></td>
                    <td id="QI1txti">
                        <asp:RadioButton ID="RadioButtonFixationalVisionYes" runat="server" GroupName="FixationalVision" Text="&nbsp;Yes" />&nbsp;&nbsp;
                        <asp:RadioButton ID="RadioButtonFixationalVisionNo" runat="server" GroupName="FixationalVision" Text="&nbsp;No" />
                    </td>
                </tr>
                <tr class="table_body">
                    <td id="QI1txtj"><span class="right">Teller cards</span></td>
                    <td id="QI1txtk">
                        <asp:RadioButton ID="RadioButtonTellerCardsYes" runat="server" GroupName="TellerCards" Text="&nbsp;Yes" />&nbsp;&nbsp;
                        <asp:RadioButton ID="RadioButtonTellerCardsNo" runat="server" GroupName="TellerCards" Text="&nbsp;No" />
                    </td>
                </tr>
                <!-- Question 2 -->
                <tr class="table_body_bg">
                    <td colspan="2">
                        <strong>2. Was amblyopia present?</strong>
                        <asp:Label ID="LabelAmblyopiaPresent" runat="server" Visible="false" ForeColor="Red" Text="<br />Please enter"></asp:Label>
                    </td>
                    <td>
                        <asp:RadioButton ID="RadioButtonAmblyopiaPresentYes" runat="server" GroupName="AmblyopiaPresent" Text="&nbsp;Yes" />&nbsp;&nbsp;
                        <asp:RadioButton ID="RadioButtonAmblyopiaPresentNo" runat="server" GroupName="AmblyopiaPresent" Text="&nbsp;No" />
                    </td>
                </tr>
                <!-- Question 3 -->
                <tr class="table_body">
                    <td colspan="2">
                        <strong>3. Stereoacuity</strong>
                        <asp:Label ID="LabelStereoacuityResult" runat="server" Visible="false"  ForeColor="Red" Text="<br />Please enter"></asp:Label>
                    </td>
                    <td>
                        <asp:TextBox runat="server" ID="TextBoxStereoacuityResult" class="auto" meta="{mDec: '1'}" size="10" onkeyup="QI3CheckBox();" />&nbsp;sec.
                        <br /><asp:Checkbox ID="CheckboxSensoryTestingPerformedOther" runat="server" GroupName="SensoryTestingPerformed" Text="&nbsp;Unable to perform or complete test" onclick="QI3TextField()" />
                    </td>
                </tr>
                <!-- Question 4 -->
                <tr class="table_body_bg">
                    <td colspan="2">
                        <strong>4. Was a Worth 4 dot test performed?</strong>
                        <asp:Label ID="LabelWorth4DotPerformed" runat="server" Visible="false"  ForeColor="Red" Text="<br />Please enter Worth4Dot Performed"></asp:Label>
                    </td>
                    <td>
                        <asp:RadioButtonList ID="RadioButtonListWorth4DotPerformed" RepeatDirection="Vertical" CssClass="aspxList" runat="server">
                            <asp:ListItem Value="1">Yes</asp:ListItem>
                            <asp:ListItem Value="2">No</asp:ListItem>
                            <asp:ListItem Value="4">Unable to Perform or Complete Test</asp:ListItem>
                        </asp:RadioButtonList>
                    </td>
                </tr>
                <!-- Question 4a -->
                <tr class="table_body_bg">
                    <td rowspan="2" id="QI4txta">
                        <strong>4a. Worth 4 dot results</strong><br />
                        <asp:Label ID="LabelWorth4DotDistanceDescription" runat="server" Visible="false" ForeColor="Red" Text="<br />Please enter Worth4Dot Distance Description"></asp:Label>
                        <asp:Label ID="LabelWorth4DotNearDescription" runat="server" Visible="false" ForeColor="Red" Text="<br />Please enter Worth4Dot Near Description"></asp:Label>
                    </td>
                    <td id="QI4txtb"><span class="right">Worth 4 dot distance</span></td>
                    <td id="QI4txtc">
                        <asp:DropDownList ID="DropDownListWorth4DotDistanceDescription" DataValueField="worth4dotValue" DataTextField="worth4dotLabel" runat="server" onchange="QI4aDistanceCB();"></asp:DropDownList>
                        <br /><asp:Checkbox ID="CheckBoxWorth4DotDistanceUnableToComplete" runat="server" GroupName="Worth4DotDistanceUnableToComplete" Text="&nbsp;Unable to perform or complete test" onclick="QI4aDistanceRB()" />
                    </td>
                </tr>
                <tr class="table_body_bg">
                    <td id="QI4txtd"><span class="right">Worth 4 dot near</span></td>
                    <td id="QI4txte">
                        <asp:DropDownList ID="DropDownListWorth4DotNearDescription" DataValueField="worth4dotValue" DataTextField="worth4dotLabel" runat="server" onchange="QI4aNearCB();"></asp:DropDownList>
                        <br /><asp:Checkbox ID="CheckBoxWorth4DotNearUnableToComplete" runat="server" GroupName="Worth4DotNearUnableToComplete" Text="&nbsp;Unable to perform or complete test" onclick="QI4aNearRB()" />
                    </td>
                </tr>
                <!-- Question 5 -->
                <tr class="table_body">
                    <td colspan="2">
                        <strong>5. Was alignment evaluated?</strong>
                        <asp:Label ID="LabelAlignmentMeasured" runat="server" Visible="false"  ForeColor="Red" Text="<br />Please enter Alignment Measured "></asp:Label>
                    </td>
                    <td>
                        <asp:RadioButton ID="RadioButtonAlignmentMeasuredYes" runat="server" GroupName="AlignmentMeasured" Text="&nbsp;Yes"/>&nbsp;&nbsp;
                        <asp:RadioButton ID="RadioButtonAlignmentMeasuredNo" runat="server" GroupName="AlignmentMeasured" Text="&nbsp;No"/>
                    </td>
                </tr>
                <!-- Question 5a -->
                <tr class="table_body">
                    <td rowspan="2" id="QIE5col1">
                        <strong>5a. Alignment</strong>
                        <asp:Label ID="LabelAlignmentDistance" runat="server" Visible="false"  ForeColor="Red" Text="<br />Please enter Alignment Distance "></asp:Label>
                        <asp:Label ID="LabeltAlignmentNear" runat="server" Visible="false"  ForeColor="Red" Text="<br />Please enter Alignment Near "></asp:Label>
                        <asp:Label ID="LabelAlignmentNear" runat="server" Visible="false"  ForeColor="Red" Text="<br />Please enter Cyclopegic Retinoscopy Performed "></asp:Label>
                    </td>
                    <td id="QIE5col2row1"><span class="right">Distance:</span></td>
                    <td>
                        <span id="OrthoAa"><asp:CheckBox ID="RadioButtonAlignmentDistanceOrthotropia" Text="Orthotropia" runat="server" /></span>
                        <br />
                        <table id="OrthoAb" class="aspxList">
                            <tr>
                                <td style="width:75px;">
                                    <asp:TextBox runat="server" meta="{vMin: '0', vMax: '180'}" class="auto" ID="TextBoxAlignmentDistancePDET" size="3" onkeyup="QI5nearRB();"/>&nbsp;PD&nbsp;ET
                                </td>
<%--                                <td>
                                    <asp:RadioButtonList ID="RadioButtonListAlignmentDistance" RepeatDirection="Horizontal" CssClass="aspxList" runat="server" onclick="QI5nearRB();" >
                                        <asp:ListItem Value="10">ET</asp:ListItem>
                                        <asp:ListItem Value="11">XT</asp:ListItem>
                                    </asp:RadioButtonList>
                                </td>--%>
                            </tr>
                            <tr>
                                <td><asp:TextBox runat="server" meta="{vMin: '0', vMax: '180'}" class="auto" ID="TextBoxAlignmentDistancePDHT" size="3" onkeyup="QI5aDistanceCB();" />&nbsp;PD&nbsp;HT</td>
                            </tr>
                            <tr>
                                <td><asp:CheckBox runat="server" ID="CheckBoxAlignmentDistanceUnableToPerform" text="&nbsp;unable to perform" class="check" onclick="QI5aDistanceFirst()" /></td>
                            </tr>
                        </table>
                    </td>
                </tr>
                <tr class="table_body">
                    <td id="QIE3col2row2"><span class="right">Near:</span></td>
                    <td>
                        <span id="OrthoBa"><asp:CheckBox ID="RadioButtonAlignmentNearOrthotropia" Text="Orthotropia" runat="server" /></span>
                        <table id="OrthoBb" class="aspxList">
                            <tr>
                                <td style="width:75px;">
                                    <asp:TextBox runat="server" meta="{vMin: '0', vMax: '180'}" class="auto" ID="TextBoxAlignmentNearPDET" size="3" onkeyup="QI5nearRB();"/>&nbsp;PD&nbsp;ET
                                </td>
                                <%--<td>
                                    <asp:RadioButtonList ID="RadioButtonListAlignmentNear" RepeatDirection="Horizontal" CssClass="aspxList" runat="server" onclick="QI5nearRB();" >
                                        <asp:ListItem Value="10">ET</asp:ListItem>
                                        <asp:ListItem Value="11">XT</asp:ListItem>
                                    </asp:RadioButtonList>
                                </td>--%>
                            </tr>
                            <tr>
                                <td>
                                    <asp:TextBox ID="TextBoxPDHT" runat="server" meta="{vMin: '0', vMax: '180'}" class="auto" size="3" onclick="QI5nearRB();"></asp:TextBox>&nbsp;PD HT
                                </td>
                            </tr>
                            <tr>
                                <td>
                                    <asp:CheckBox runat="server" ID="CheckBoxAlignmentNearUnableToPerform" text="&nbsp;unable to perform" class="check" onclick="QI5nearTB()" />
                                </td>
                            </tr>
                        </table>
                    </td>
                </tr>
                <!-- Question 6 -->
                <tr class="table_body_bg">
                    <td colspan="2">
                        <strong>6. Was a cycloplegic refraction performed?</strong>
                        <asp:Label ID="LabelCyclopegicRetinoscopyPerformed" runat="server" Visible="false"  ForeColor="Red" Text="<br />Please enter Cyclopegic Retinoscopy Performed"></asp:Label>
                    </td>
                    <td>
                        <asp:RadioButton ID="RadioButtonCyclopegicRetinoscopyPerformedYes" runat="server" GroupName="CyclopegicRetinoscopyPerformed" Text="&nbsp;Yes" onclick="QI6isClicked();" />&nbsp;&nbsp;
                        <asp:RadioButton ID="RadioButtonCyclopegicRetinoscopyPerformedNo" runat="server" GroupName="CyclopegicRetinoscopyPerformed" Text="&nbsp;No" onclick="QI6isClicked();" />
                    </td>
                </tr>
                <!-- Question 6a -->
                <tr class="table_body_bg">
                    <td rowspan="2" id="QI6txta"><strong>6a. Cycloplegic refraction</strong>&nbsp;<a href="#"><img id="QIE6a" src="../../common/images/tip.gif" alt="Tip" /></a></td>
                    <td id="QI6txtb"><span class="right">OD</span></td>
                    <td>
                        <table class="aspxList">
                            <tr>
                                <td id="QI6txtc">    
                                    <span class="right">
                                        <asp:RadioButton ID="RadioButtonCycloplegicRefractionODsphPlus" runat="server" GroupName="CycloplegicRefractionODsph" Text="&nbsp;+" />&nbsp;
                                        <asp:RadioButton ID="RadioButtonCycloplegicRefractionODsphMinus" runat="server" GroupName="CycloplegicRefractionODsph" Text="&nbsp;-" />
                                    </span>
                                </td>
                                <td id="QI6txtd"><asp:TextBox runat="server" meta="{vMin: '-30.00', vMax: '30.00'}" class="auto" ID="TextBoxCycloplegicRefractionODsphSign" size="3" />&nbsp;sph</td>
                            </tr>
                            <tr>
                                <td id="QI6txte"><span class="right">+ </span></td>
                                <td id="QI6txtf"><asp:TextBox runat="server" meta="{vMin: '0.00', vMax: '99.99'}" class="auto" ID="TextBoxCycloplegicRefractionODcylSign" size="3" />&nbsp;cyl</td>
                            </tr>
                            <tr>
                                <td></td>
                                <td id="QI6txtg"><asp:TextBox runat="server" meta="{vMin: '0.00', vMax: '180.00'}" class="auto" ID="TextBoxCycloplegicRefractionODaxis" size="3" />&nbsp;axis</td>
                            </tr>
                        </table>
                        <!--<asp:RadioButton ID="RadioButtonCycloplegicRefractionODcylPlus" runat="server" GroupName="CycloplegicRefractionODcyl" Text="&nbsp;+" />&nbsp;
                        <asp:RadioButton ID="RadioButtonCycloplegicRefractionODcylMinus" runat="server" GroupName="CycloplegicRefractionODcyl" Text="&nbsp;-" />&nbsp;-->
                    </td>
                </tr>
                <tr class="table_body_bg">
                    <td id="QI6txtm"><span class="right">OS</span></td>
                    <td>
                        <table class="aspxList">
                            <tr>
                                <td id="QI6txth">
                                    <span class="right">
                                        <asp:RadioButton ID="RadioButtonCycloplegicRefractionOSsphPlus" runat="server" GroupName="CycloplegicRefractionOSsph" Text="&nbsp;+" />&nbsp;
                                        <asp:RadioButton ID="RadioButtonCycloplegicRefractionOSsphMinus" runat="server" GroupName="CycloplegicRefractionOSsph" Text="&nbsp;-" />
                                    </span>
                                </td>
                                <td id="QI6txti"><asp:TextBox runat="server" meta="{vMin: '-30.00', vMax: '30.00'}" class="auto" ID="TextBoxCycloplegicRefractionOSsphSign" size="3" />&nbsp;sph</td>
                            </tr>
                            <tr>
                                <td><span class="right" id="QI6txtj">+ </span></td>
                                <td><span id="QI6txtk"><asp:TextBox runat="server" meta="{vMin: '0.00', vMax: '99.99'}" class="auto" ID="TextBoxCycloplegicRefractionOScylSign" size="3" />&nbsp;cyl</span></td>
                            </tr>
                            <tr>
                                <td></td>
                                <td id="QI6txtl"><asp:TextBox runat="server" meta="{vMin: '0.00', vMax: '180.00'}" class="auto" ID="TextBoxCycloplegicRefractionOSaxis" size="3" />&nbsp;axis</td>
                            </tr>
                        </table>
                        <!--<asp:RadioButton ID="RadioButtonCycloplegicRefractionOScylPlus" runat="server" GroupName="CycloplegicRefractionOScyl" Text="&nbsp;+" />&nbsp;
                        <asp:RadioButton ID="RadioButtonCycloplegicRefractionOScylMinus" runat="server" GroupName="CycloplegicRefractionOScyl" Text="&nbsp;-" />&nbsp;-->
                    </td>
                </tr>
                <!-- Question 7 -->
                <tr class="table_body">
                    <td colspan="2"><strong>7. Was dilated fundus exam performed?</strong> (&lt;32 weeks)</td>
                    <td>
                        <asp:RadioButtonList ID="RadioButtonListDiatedFundsExam" RepeatDirection="Vertical" CssClass="aspxList" runat="server">
                            <asp:ListItem Value="1">Yes</asp:ListItem>
                            <asp:ListItem Value="2">No</asp:ListItem>
                            <asp:ListItem Value="4">Unable to Perform or Complete Test</asp:ListItem>
                            <asp:ListItem Value="5">Deferred for Medical or Patient Reason</asp:ListItem>
                        </asp:RadioButtonList>
                    </td>
                </tr>
                <!-- Question 8 -->
                <tr class="table_body_bg">
                    <td colspan="2">
                        <strong>8. At how many visits prior to surgery were measurements taken?</strong>
			            <asp:Label ID="LabelNumberPriorMeasurement1" runat="server" Visible="false" ForeColor="Red" Text="<br />Please enter"></asp:Label>
                    </td>
                    <td><asp:TextBox runat="server" ID="TextBoxNumberPriorMeasurement" size="5" /></td>
                </tr>
            </table>
            <br />
            <!-- Management -->
            <table>
                <!-- Header - Management -->
                <tr>
                    <th colspan="3" width="910px"><p>Management</p></th>
                </tr>
                <!-- Question 1 -->
                <tr class="table_body">
                    <td width="50%" colspan="2">
                        <strong>1. Did the patient have amblyopia therapy prior to surgical intervention?</strong>
                        <asp:Label ID="LabelAmblyopiaTherapy" runat="server" Visible="false"  ForeColor="Red" Text="<br />Please enter Amblyopia Therapy"></asp:Label>
                    </td>
                    <td>
                        <asp:RadioButton ID="RadioButtonAmblyopiaTherapyYes" runat="server" GroupName="AmblyopiaTherapy" Text="&nbsp;Yes" />&nbsp;&nbsp;
                        <asp:RadioButton ID="RadioButtonAmblyopiaTherapyNo" runat="server" GroupName="AmblyopiaTherapy" Text="&nbsp;No" />
                    </td>
                </tr>
                <!-- Question 2 -->
                <tr class="table_body_bg">
                    <td rowspan="5" width="50%">
                        <strong>2. Which surgical intervention  was performed?</strong>
                        <asp:Label ID="LabelSurgeryRecessBilateralMedial" runat="server" Visible="false"  ForeColor="Red" Text="<br />Please enter Surgery Recess Bilateral Medial "></asp:Label>
                        <asp:Label ID="LabelRecess1MedialResect1Lateral" runat="server" Visible="false"  ForeColor="Red" Text="<br />Please enter Recess1 Medial Resect1 Lateral "></asp:Label>
                        <asp:Label ID="LabelSurgeryRecess1MedialYes" runat="server" Visible="false"  ForeColor="Red" Text="<br />Please enter Surgery Recess1 Medial Yes "></asp:Label>
                        <asp:Label ID="LabelSurgeryRecessBilateralLateralNo" runat="server" Visible="false"  ForeColor="Red" Text="<br />Please enter Surgery Recess Bilateral Lateral Nos "></asp:Label>
                        <asp:Label ID="LabelNumberPriorMeasurement" runat="server" Visible="false"  ForeColor="Red" Text="<br />Please enter Number Prior Measurement "></asp:Label>
                    </td>
                    <td width="20%"><span class="right">Recess bilateral medial rectus</span></td>
                    <td>
                        <asp:RadioButton ID="RadioButtonSurgeryRecessBilateralMedialYes" runat="server" GroupName="SurgeryRecessBilateralMedial" Text="&nbsp;Yes" />&nbsp;&nbsp;
                        <asp:RadioButton ID="RadioButtonSurgeryRecessBilateralMedialNo" runat="server" GroupName="SurgeryRecessBilateralMedial" Text="&nbsp;No" Checked="true" />
                    </td>
                </tr>
                <tr class="table_body_bg">
                    <td><span class="right">Recess 1 medial rectus/resect 1 lateral rectus</span></td>
                    <td>
                        <asp:RadioButton ID="RadioButtonSurgeryRecess1MedialResect1LateralYes" runat="server" GroupName="SurgeryRecess1MedialResect1Lateral" Text="&nbsp;Yes" />&nbsp;&nbsp;
                        <asp:RadioButton ID="RadioButtonSurgeryRecess1MedialResect1LateralNo" runat="server" GroupName="SurgeryRecess1MedialResect1Lateral" Text="&nbsp;No" Checked="true" />
                    </td>
                </tr>
                <tr class="table_body_bg">
                    <td><span class="right">Recess 1 medial rectus</span></td>
                    <td>
                        <asp:RadioButton ID="RadioButtonSurgeryRecess1MedialYes" runat="server" GroupName="SurgeryRecess1Medial" Text="&nbsp;Yes" />&nbsp;&nbsp;
                        <asp:RadioButton ID="RadioButtonSurgeryRecess1MedialNo" runat="server" GroupName="SurgeryRecess1Medial" Text="&nbsp;No" Checked="true" />
                    </td>
                </tr>
                <tr class="table_body_bg">
                    <td><span class="right">Resect 1 lateral rectus</span></td>
                    <td>
                        <asp:RadioButton ID="RadioButtonSurgeryRecess1LateralYes" runat="server" GroupName="SurgeryRecess1Lateral" Text="&nbsp;Yes" />&nbsp;&nbsp;
                        <asp:RadioButton ID="RadioButtonSurgeryRecess1LateralNo" runat="server" GroupName="SurgeryRecess1Lateral" Text="&nbsp;No" Checked="true" />
                    </td>
                </tr>
                <tr class="table_body_bg">
                    <td><span class="right">Resect bilateral lateral rectus</span></td>
                    <td>
                        <asp:RadioButton ID="RadioButtonSurgeryRecessBilateralLateralYes" runat="server" GroupName="SurgeryRecessBilateralLateral" Text="&nbsp;Yes" />&nbsp;&nbsp;
                        <asp:RadioButton ID="RadioButtonSurgeryRecessBilateralLateralNo" runat="server" GroupName="SurgeryRecessBilateralLateral" Text="&nbsp;No" Checked="true" />
                    </td>
                </tr>
            </table>
            <br />
            <!-- Outcomes Section -->
            <table>
                <!-- Header - Outcomes -->
                <tr>
                    <th colspan="3" width="910px"><p>Outcomes</p></th>
                </tr>
                <!-- Note -->
                <tr class="table_body">
                    <td colspan="3">Note: Select the 2 post surgery follow-up visits closest to: #1 (1-2 mos. post surgery) and #2 (6-8 mos. post surgery) for the following outcomes:</td>
                </tr>
                <!-- Question 1 -->
                <tr class="table_body">
                    <td rowspan="2" style="width:50%">
                        <strong>1. Visit  1: Postoperative alignment</strong>
                        <asp:Label ID="Labelutcome1AlignmentDistancePDET" runat="server" Visible="false"  ForeColor="Red" Text="<br />Please enter Outcome 1Alignment Distance PDET"></asp:Label>
                        <asp:Label ID="LabelOutcome1AlignmentNearPDHT" runat="server" Visible="false"  ForeColor="Red" Text="<br />Please enter Outcome 1Alignment Near PDHT"></asp:Label>
                    </td>
                    <td style="width:20%"><span class="right">F/U #1 Distance:</span></td>
                    <td>
                        <asp:CheckBox ID="RadioButtonOutcome1AlignmentPDETDistanceOrthotropia" Text="Orthotropia"   runat="server" />
                        <table class="aspxList" id="Q1OrthoA">
                            <tr>
                                <td>
                                    <asp:TextBox runat="server" meta="{vMin: '0', vMax: '180'}" class="auto" ID="TextBoxOutcome1AlignmentDistancePDET" size="5" />&nbsp;PD
                                </td>
                                <td>
                                    <asp:RadioButtonList ID="RadioButtonListOutcome1AlignmentDistancePDHT" RepeatDirection="Horizontal" CssClass="aspxList" runat="server">
                                        <asp:ListItem Value="10">ET</asp:ListItem>
                                        <asp:ListItem Value="11">XT</asp:ListItem>
                                    </asp:RadioButtonList>
                                </td>
                            </tr>
                            <tr>
                                <td>
                                    <asp:TextBox runat="server" meta="{vMin: '0', vMax: '180'}" class="auto" ID="TextBoxOutcome1AlignmentDistancePDHT" size="5" onkeyup="QO1DistanceCB();" />&nbsp;PD 
                                    HT
                                </td>
                                <td>
                                    
                                </td>
                            </tr>
                        </table>
                        <span id="Q1OrthoB">
                            <asp:Checkbox runat="server" ID="CheckBoxOutcome1AlignmentDistanceUnableToPerform" size="10" onclick="QO1DistanceFirst();" />&nbsp;unable to perform
                        </span>
                    </td>
                </tr>
                <tr class="table_body">
                    <td><span class="right">F/U #1 Near:</span></td>
                    <td>
                        <asp:CheckBox ID="RadioButtonOutcome1AlignmentNearPDETOrthotropia" Text="Orthotropia" runat="server" />
                        <table class="aspxList" id="Q1OrthoA2">
                            <tr>
                                <td>
                                    <asp:TextBox runat="server" meta="{vMin: '0', vMax: '180'}" class="auto" ID="TextBoxOutcome1AlignmentNearPDET" size="5" />&nbsp;PD
                                </td>
                                <td>
                                    <asp:RadioButtonList ID="RadioButtonListOutcome1AlignmentNearPDHT" RepeatDirection="Horizontal" CssClass="aspxList" runat="server">
                                        <asp:ListItem Value="10">ET</asp:ListItem>
                                        <asp:ListItem Value="11">XT</asp:ListItem>
                                    </asp:RadioButtonList>
                                </td>
                            </tr>
                        </table>
                        <span id="Q1OrthoB2">
                            <asp:TextBox runat="server" meta="{vMin: '0', vMax: '180'}" class="auto" ID="TextBoxOutcome1AlignmentNearPDHT" size="5" />&nbsp;PD HT
                        </span>
                    </td>
                </tr>
                <!-- Question 2 -->
                <tr class="table_body_bg">
                    <td>
                        <strong>2. Visit 1: Stereoacuity</strong>
                        <asp:Label ID="LabelOutcome1StereoacuityValue" runat="server" Visible="false"  ForeColor="Red" Text="<br />Please enter Outcome1 Stereoacuity Value"></asp:Label>
                    </td>
                    <td><span class="right">F/U #1</span></td>
                    <td>
                        <asp:TextBox runat="server" class="auto" meta="{mDec: '1'}" ID="TextBoxOutcome1StereoacuityValue" size="5" onkeyup="QO2CB();" />&nbsp;sec.
                        <br /><asp:CheckBox ID="CheckBoxOutcome1MethodStereoacuityUnableToComplete" runat="server" Text="&nbsp;Unable to perform or complete test" onclick="QO2TextField();" />
                    </td>
                </tr>
                <!-- Question 3 -->
                <tr class="table_body">
                    <td rowspan="2">
                        <strong>3. Visit 1: Worth 4 dot</strong>
                        <asp:Label ID="LabelWorth4dotdistance1" runat="server" Visible="false"  ForeColor="Red" Text="<br />Please enter Worth 4dot distance1 "></asp:Label>
                        <asp:Label ID="LabelWorth4dotnear1" runat="server" Visible="false"  ForeColor="Red" Text="<br />Please enter Worth 4dot near1 "></asp:Label>
                     </td>
                    <td><span class="right">F/U #1 - Worth 4 dot distance:</span></td>
                    <td>
                        <asp:DropDownList ID="DropDownListtWorth4dotdistance1" DataValueField="worth4dotValue" DataTextField="worth4dotLabel" runat="server" onclick="QO3distanceCB();"></asp:DropDownList>
                        <br /><asp:CheckBox ID="CheckBoxOutcome1Worth4DotDistanceUnableToComplete" runat="server" Text="&nbsp;Unable to perform or complete test" onclick="QO3distanceRB();" />
                    </td>
                </tr>
                <tr class="table_body">
                    <td><span class="right">F/U #1 - Worth 4 dot near:</span></td>
                    <td>
                        <asp:DropDownList ID="DropDownListWorth4dotnear1" DataValueField="worth4dotValue" DataTextField="worth4dotLabel" runat="server" onclick="QO3nearCB();"></asp:DropDownList>
                        <br /><asp:CheckBox ID="CheckBoxOutcome1Worth4DotNearUnableToComplete" runat="server" Text="&nbsp;Unable to perform or complete test" onclick="QO3nearRB();" />
                    </td>
                </tr>
                <!-- Question 4 -->
                <tr class="table_body_bg">
                    <td rowspan="2">
                        <strong>4. Visit 2: Postoperative alignment</strong>
                        <asp:Label ID="LabelOutcome2AlignmentNearPDHT" runat="server" Visible="false"  ForeColor="Red" Text="<br />Please enter Outcome2 Alignment Near PDHT"></asp:Label>
                        <asp:Label ID="LabelOutcome2AlignmentNearPDET" runat="server" Visible="false"  ForeColor="Red" Text="<br />Please enter Outcome2 Alignment Near PDET "></asp:Label>
                    </td>
                    <td><span class="right">F/U #2 Distance:</span></td>
                    <td>
                        <asp:CheckBox ID="RadioButtonOutcome2AlignmentDistancePDETOrthotropia" Text="Orthotropia" runat="server" />
                        <table class="aspxList" id="QO4OrthoA1">
                            <tr>
                                <td>
                                    <asp:TextBox runat="server" meta="{vMin: '0', vMax: '180'}" class="auto" ID="TextBoxOutcome2AlignmentDistancePDET" size="5" onkeyup="QO4DistanceCB();" />&nbsp;PD&nbsp;
                                </td>
                                <td>
                                    <asp:RadioButtonList ID="RadioButtonListOutcome2AlignmentDistancePDET" RepeatDirection="Horizontal" CssClass="aspxList" runat="server">
                                        <asp:ListItem Value="10">ET</asp:ListItem>
                                        <asp:ListItem Value="11">XT</asp:ListItem>
                                    </asp:RadioButtonList>
                                </td>
                            </tr>
                            <tr>
                                <td colspan="2">
                                    <asp:TextBox runat="server" meta="{vMin: '0', vMax: '180'}" class="auto" ID="TextBoxOutcome2AlignmentDistancePDHT" size="5" onkeyup="QO4DistanceCB();" />&nbsp;PD HT
                                </td>
                            </tr>
                        </table>
                        <span id="QO4OrthoB1">
                            <asp:CheckBox runat="server" ID="CheckBoxOutcome2AlignmentDistanceUnableToPerform" onclick="QO4DistanceFirst();" />&nbsp;unable to perform
                        </span>
                    </td>
                </tr>
                <tr class="table_body_bg">
                    <td><span class="right">F/U #2 Near:</span></td>
                    <td>
                        <asp:CheckBox ID="RadioButtonOutcome2AlignmentNearPDETOrthotropia" Text="Orthotropia" runat="server" />
                        <table class="aspxList" id="QO4OrthoA2">
                            <tr>
                                <td>
                                    <asp:TextBox runat="server" meta="{vMin: '0', vMax: '180'}" class="auto" ID="TextBoxOutcome2AlignmentNearPDET" size="5" />&nbsp;PD
                                </td>
                                <td>
                                    <asp:RadioButtonList ID="RadioButtonListOutcome2AlignmentNearPDET" RepeatDirection="Horizontal" CssClass="aspxList" runat="server">
                                        <asp:ListItem Value="10">ET</asp:ListItem>
                                        <asp:ListItem Value="11">XT</asp:ListItem>
                                    </asp:RadioButtonList>
                                </td>
                            </tr>
                        </table>
                        <span id="QO4OrthoB2">
							<asp:TextBox runat="server" meta="{vMin: '0', vMax: '180'}" class="auto" ID="TextBoxOutcome2AlignmentNearPDHT" size="5" />&nbsp;PD HT
                        </span>
                    </td>
                </tr>
                <!-- Question 5 -->
                <tr class="table_body">
                    <td>
                        <strong>5. Visit 2: Stereoacuity</strong>
                        <asp:Label ID="LabelOutcome2StereoacuityValue" runat="server" Visible="false"  ForeColor="Red" Text="<br />Please enter Outcome2 Stereoacuity Value"></asp:Label>
                    </td>
                    <td><span class="right">F/U #2</span></td>
                    <td>
                        <asp:TextBox runat="server" ID="TextBoxOutcome2StereoacuityValue" class="auto" meta="{mDec: '1'}" size="5" onkeyup="QO5CB();" />&nbsp;sec.
                        <br /><asp:CheckBox runat="server" ID="CheckBoxOutcome2MethodStereoacuityUnableToComplete" size="10" onclick="QO5TextBox();" />&nbsp;unable to perform
                    </td>
                </tr>
                <!-- Question 6 -->
                <tr class="table_body_bg">
                    <td rowspan="2">
                        <strong>6. Visit 2: Worth 4 dot</strong>
                        <asp:Label ID="LabelOutcome2Worth4DotDistance" runat="server" Visible="false"  ForeColor="Red" Text="<br />Please enter Outcome2 Worth 4Dot Distance "></asp:Label>
                        <asp:Label ID="LabelOutcome2Worth4DotNear" runat="server" Visible="false"  ForeColor="Red" Text="<br />Please enter Outcome2 Worth 4Dot Neance "></asp:Label>
                    </td>
                    <td><span class="right">F/U #2 - Worth 4 dot distance:</span></td>
                    <td>
                        <asp:DropDownList ID="DropDownListOutcome2Worth4DotDistance" DataValueField="worth4dotValue" DataTextField="worth4dotLabel" runat="server" onclick="QO6DistanceCB();" ></asp:DropDownList>
                        <br /><asp:CheckBox ID="CheckBoxOutcome2Worth4DotDistanceUnableToComplete" runat="server" Text="&nbsp;Unable to perform or complete test" onclick="QO6DistanceRB();" />
                    </td>
                </tr>
                <tr class="table_body_bg">
                    <td><span class="right">F/U #2 - Worth 4 dot near:</span></td>
                    <td>
                        <asp:DropDownList ID="DropDownListOutcome2Worth4DotNear" DataValueField="worth4dotValue" DataTextField="worth4dotLabel" runat="server" onclick="QO6NearCB();"></asp:DropDownList>
                        <br /><asp:CheckBox ID="CheckBoxOutcome2Worth4DotNearUnableToComplete" runat="server" Text="&nbsp;Unable to perform or complete test" onclick="QO6NearRB();" />
                    </td>
                </tr>
            </table>
            <br />
            <!-- Complications Section -->
            <table width="908px">
                <!-- Header - Complications -->
                <tr>
                    <th colspan="3" width="910px"><p>Complications</p></th>
                </tr>
                <!-- Question 1 -->
                <tr class="table_body">
                    <td width="70%">
                        <strong>1. Was there a postoperative infection?</strong>
                        <asp:Label ID="LabelPostoperativeInfection" runat="server" Visible="false"  ForeColor="Red" Text="<br />Please enter Postoperative Infection"></asp:Label>
                    </td>
                    <td>
                        <asp:RadioButton ID="RadioButtonPostoperativeInfectionYes" runat="server" GroupName="PostoperativeInfection" Text="&nbsp;Yes" onclick="QC1onClick();" />&nbsp;&nbsp;
                        <asp:RadioButton ID="RadioButtonPostoperativeInfectionNo" runat="server" GroupName="PostoperativeInfection" Text="&nbsp;No" onclick="QC1onClick();" />
                    </td>
                </tr>
                <!-- Question 1a -->
                <tr class="table_body">
                    <td><span id="QC1txta"><strong>1a. If yes, what types?</strong></span></td>
                    <td id="QC1txtb">
                        <asp:CheckBox runat="server" ID="CheckBoxPostoperativeConjunctivitis" />&nbsp;Conjunctivitis
                        <br /><asp:CheckBox runat="server" ID="CheckBoxPostoperativePreseptalCellulitis" />&nbsp;Preseptal cellulitis
                        <br /><asp:CheckBox runat="server" ID="CheckBoxPostoperativeOrbitalCellulitis" />&nbsp;Orbital cellulitis
                        <br /><asp:CheckBox runat="server" ID="CheckBoxPostoperativeEndophthalmitis" />&nbsp;Endophthalmitis
                        <br /><asp:CheckBox runat="server" ID="CheckBoxPostoperativeOther" size="30" />&nbsp;Other
                    </td>
                </tr>
                <!-- Question 2 -->
                <tr class="table_body_bg">
                    <td>
                        <strong>2. Was reoperation performed within one month?</strong>
                        <asp:Label ID="LabelComplicationReoperationWithin1Month" runat="server" Visible="false"  ForeColor="Red" Text="<br />Please enter Complication Reoperation Within 1Month"></asp:Label>
                    </td>
                    <td>
                        <asp:RadioButton ID="RadioButtonComplicationReoperationWithin1MonthYes" runat="server" GroupName="ComplicationReoperationWithin1Month" Text="&nbsp;Yes" />&nbsp;&nbsp;
                        <asp:RadioButton ID="RadioButtonComplicationReoperationWithin1MonthNo" runat="server" GroupName="ComplicationReoperationWithin1Month" Text="&nbsp;No" />
                    </td>
                </tr>
                <!-- Question 2a -->
                <tr class="table_body_bg">
                    <td id="QC2txta">
                        <strong>2a. Was there evidence of slipped/lost muscle on reoperation?</strong>
                        <asp:Label ID="LabelComplicationEvidenceSlippedMuscle" runat="server" Visible="false"  ForeColor="Red" Text="<br />Please enter Complication Evidence Slipped Muscle"></asp:Label>
                    </td>
                    <td id="QC2txtb">
                        <asp:RadioButton ID="RadioButtonComplicationEvidenceSlippedMuscleYes" runat="server" GroupName="ComplicationEvidenceSlippedMuscle" Text="&nbsp;Yes" />&nbsp;&nbsp;
                        <asp:RadioButton ID="RadioButtonComplicationEvidenceSlippedMuscleNo" runat="server" GroupName="ComplicationEvidenceSlippedMuscle" Text="&nbsp;No" />
                    </td>
                </tr>
                <!-- Question 3 -->
                <tr class="table_body">
                    <td>
                        <strong>3.	Was there globe perforation during surgery?</strong>
                        <asp:Label ID="LabelComplicationGlobePerforation" runat="server" Visible="false" ForeColor="Red" Text="<br />Please enter Complication Globe Perforation"></asp:Label>
                    </td>
                    <td>
                        <asp:RadioButton ID="RadioButtonComplicationGlobePerforationYes" runat="server" GroupName="ComplicationGlobePerforation" Text="&nbsp;Yes" />&nbsp;&nbsp;
                        <asp:RadioButton ID="RadioButtonComplicationGlobePerforationNo" runat="server" GroupName="ComplicationGlobePerforation" Text="&nbsp;No" />
                    </td>
                </tr>
                <!-- Question 4 -->
                <tr class="table_body_bg">
                    <td><strong>4. Other complications – please specify</strong></td>
                    <td>
                        <asp:TextBox runat="server" ID="TextBoxComplicationOther" size="1000" TextMode="MultiLine" />
                    </td>
                </tr>
            </table>
            <div class="button-box">
                <asp:LinkButton ID="LinkButtonBackToDashboard" runat="server" Text="Back To Chart Registration" PostBackUrl="../PatientChartRegistration.aspx?CycleNumber=1" Visible="false" CssClass="button" />
                <asp:ImageButton ID="ButtonSubmit" OnClick="ButtonSubmit_Click" runat="server" AlternateText="Submit Chart" CssClass="button" />
            </div>
        </div>
        <!-- Esotropia pim ends -->
</asp:Content>

<asp:Content runat="server" ID="Content4" ContentPlaceHolderID="javascript">
    	<script type="text/javascript" src="../../common/js/jquery.min.js"></script>
	<script type="text/javascript" src="../../common/js/jquery.atooltip.js"></script>
    <script type="text/javascript" src="../../common/js/jquery.metadata.js"></script> <!--when changing defaults-->
    <script type="text/javascript" src="../../common/js/autoNumeric-1.7.5.js"></script>
    <script type="text/javascript" language="javascript">
        // Validates the DOB
        function DOBValidation() {
            var month = document.getElementById("<%= DropDownListMonthOfBirth.ClientID %>").value;
            var year = document.getElementById("<%= DropDownListYearOfBirth.ClientID %>").value;
            if (validateDate(month, year) != true) {
                enabledControlDropDown(document.getElementById("<%= DropDownListMonthOfBirth.ClientID %>"), false, true);
                enabledControlDropDown(document.getElementById("<%= DropDownListYearOfBirth.ClientID %>"), false, true);
            }
        }
        // Validates the surgery
        function dateOfSurgeryValidation() {
            var month = document.getElementById("<%= DropDownListMonthOfSurgery.ClientID %>").value;
            var year = document.getElementById("<%= DropDownListYearOfSurgery.ClientID %>").value;
            if (validateDate(month, year) != true) {
                enabledControlDropDown(document.getElementById("<%= DropDownListMonthOfSurgery.ClientID %>"), false, true);
                enabledControlDropDown(document.getElementById("<%= DropDownListYearOfSurgery.ClientID %>"), false, true);
            }
        }
        // Validates the second followup
        function dateOfFollowUpOne() {
            var month = document.getElementById("<%= DropDownListMonthOfFollowUp1.ClientID %>").value;
            var year = document.getElementById("<%= DropDownListYearOfFollowUp1.ClientID %>").value;
            if (validateDate(month, year) != true) {
                enabledControlDropDown(document.getElementById("<%= DropDownListMonthOfFollowUp1.ClientID %>"), false, true);
                enabledControlDropDown(document.getElementById("<%= DropDownListYearOfFollowUp1.ClientID %>"), false, true);
            }
        }
        // Validates the second followup
        function dateOfFollowUpTwo() {
            var month = document.getElementById("<%= DropDownListMonthOfFollowUp2.ClientID %>").value;
            var year = document.getElementById("<%= DropDownListYearOfFollowUp2.ClientID %>").value;
            if (validateDate(month, year) != true) {
                enabledControlDropDown(document.getElementById("<%= DropDownListMonthOfFollowUp2.ClientID %>"), false, true);
                enabledControlDropDown(document.getElementById("<%= DropDownListYearOfFollowUp2.ClientID %>"), false, true);
            }
        }

        // Checks that the date has happened
        function validateDate(month, year) {
            var today = new Date();
            var monthVar = month - 1;
            var checkYear = today.getFullYear() - year;
            var checkMonth = today.getMonth() - monthVar;
            if (checkMonth < 0) {
                checkYear--;
            }
            if (checkYear < 0) {
                alert("Invalid date, you must pick a date that has already occurred.");
                return false;
            }
            else {
                return true;
            }
        }

        //      Either enables or disables input boxes
        function enabledControl(el, disabled, checked) {
            //alert("Hello world from enabled control");
            try {
                if (disabled) {
                    el.disabled = disabled;
                    el.setAttribute("disabled", true);
                }
                else {
                    el.disabled = disabled;
                    el.removeAttribute("disabled");
                }

                if (checked == true)
                    el.checked = false;
            }
            catch (E) {
            }
            if (el.childNodes && el.childNodes.length > 0) {
                for (var x = 0; x < el.childNodes.length; x++) {
                    enabledControl(el.childNodes[x], disabled, checked);
                }
            }
        }
        //      Either enables or disables dropdown boxes
        function enabledControlDropDown(el, disabled, checked) {
            //alert("Hello world from enabled control");
            try {
                if (disabled) {
                    el.disabled = disabled;
                    el.setAttribute("disabled", true);
                }
                else {
                    el.disabled = disabled;
                    el.removeAttribute("disabled");
                }

                if (checked == true)
                    el.options[0].selected = true;
            }
            catch (E) {
            }
            if (el.childNodes && el.childNodes.length > 0) {
                for (var x = 0; x < el.childNodes.length; x++) {
                    enabledControl(el.childNodes[x], disabled, checked);
                }
            }
        }
        //      Either enables or disables text boxes
        function enabledControlTextField(el, disabled, checked) {
            //alert("Hello world from enabled control");
            try {
                if (disabled) {
                    el.disabled = disabled;
                    el.setAttribute("disabled", true);
                }
                else {
                    el.disabled = disabled;
                    el.removeAttribute("disabled");
                }

                if (checked == true) {
                    el.value = '';
                }
            }
            catch (E) {
            }
            if (el.childNodes && el.childNodes.length > 0) {
                for (var x = 0; x < el.childNodes.length; x++) {
                    enabledControl(el.childNodes[x], disabled, checked);
                }
            }
        }
        // Validations

        function QI1isClicked() {
            if (document.getElementById("<%= RadioButtonVisualAcuityEvaluatedYes.ClientID %>").checked == true) {
                enabledControl(document.getElementById("<%= DropDownListBestCorrectedVisualOD.ClientID %>"), false, false);
                enabledControl(document.getElementById("<%= DropDownListBestCorrectedVisualOS.ClientID %>"), false, false);
                enabledControl(document.getElementById("<%= RadioButtonOptotypeUsedYes.ClientID %>"), false, false);
                enabledControl(document.getElementById("<%= RadioButtonOptotypeUsedNo.ClientID %>"), false, false);
                enabledControl(document.getElementById("<%= RadioButtonFixationalVisionYes.ClientID %>"), false, false);
                enabledControl(document.getElementById("<%= RadioButtonFixationalVisionNo.ClientID %>"), false, false);
                enabledControl(document.getElementById("<%= RadioButtonTellerCardsYes.ClientID %>"), false, false);
                enabledControl(document.getElementById("<%= RadioButtonTellerCardsNo.ClientID %>"), false, false);
                document.getElementById("QI1txta").style.color = "#333";
                document.getElementById("QI1txtb").style.color = "#333";
                document.getElementById("QI1txtc").style.color = "#333";
                document.getElementById("QI1txtd").style.color = "#333";
                document.getElementById("QI1txte").style.color = "#333";
                document.getElementById("QI1txtf").style.color = "#333";
                document.getElementById("QI1txtg").style.color = "#333";
                document.getElementById("QI1txth").style.color = "#333";
                document.getElementById("QI1txti").style.color = "#333";
                document.getElementById("QI1txtj").style.color = "#333";
                document.getElementById("QI1txtk").style.color = "#333";
                document.getElementById("QI1txtl").style.color = "#333";
            }
            else {
                enabledControl(document.getElementById("<%= DropDownListBestCorrectedVisualOD.ClientID %>"), true, true);
                enabledControl(document.getElementById("<%= DropDownListBestCorrectedVisualOS.ClientID %>"), true, true);
                enabledControl(document.getElementById("<%= RadioButtonOptotypeUsedYes.ClientID %>"), true, true);
                enabledControl(document.getElementById("<%= RadioButtonOptotypeUsedNo.ClientID %>"), true, true);
                enabledControl(document.getElementById("<%= RadioButtonFixationalVisionYes.ClientID %>"), true, true);
                enabledControl(document.getElementById("<%= RadioButtonFixationalVisionNo.ClientID %>"), true, true);
                enabledControl(document.getElementById("<%= RadioButtonTellerCardsYes.ClientID %>"), true, true);
                enabledControl(document.getElementById("<%= RadioButtonTellerCardsNo.ClientID %>"), true, true);
                document.getElementById("QI1txta").style.color = "gray";
                document.getElementById("QI1txtb").style.color = "gray";
                document.getElementById("QI1txtc").style.color = "gray";
                document.getElementById("QI1txtd").style.color = "gray";
                document.getElementById("QI1txte").style.color = "gray";
                document.getElementById("QI1txtf").style.color = "gray";
                document.getElementById("QI1txtg").style.color = "gray";
                document.getElementById("QI1txth").style.color = "gray";
                document.getElementById("QI1txti").style.color = "gray";
                document.getElementById("QI1txtj").style.color = "gray";
                document.getElementById("QI1txtk").style.color = "gray";
                document.getElementById("QI1txtl").style.color = "gray";
            }
        }

        function QI6isClicked() {
            if (document.getElementById("<%= RadioButtonCyclopegicRetinoscopyPerformedYes.ClientID %>").checked == true) {
                enabledControl(document.getElementById("<%= RadioButtonCycloplegicRefractionODsphPlus.ClientID %>"), false, false);
                enabledControl(document.getElementById("<%= RadioButtonCycloplegicRefractionODsphMinus.ClientID %>"), false, false);
                enabledControl(document.getElementById("<%= TextBoxCycloplegicRefractionODsphSign.ClientID %>"), false, false);
                enabledControl(document.getElementById("<%= TextBoxCycloplegicRefractionODcylSign.ClientID %>"), false, false);
                enabledControl(document.getElementById("<%= TextBoxCycloplegicRefractionODaxis.ClientID %>"), false, false);
                enabledControl(document.getElementById("<%= RadioButtonCycloplegicRefractionOSsphPlus.ClientID %>"), false, false);
                enabledControl(document.getElementById("<%= RadioButtonCycloplegicRefractionOSsphMinus.ClientID %>"), false, false);
                enabledControl(document.getElementById("<%= TextBoxCycloplegicRefractionOSsphSign.ClientID %>"), false, false);
                enabledControl(document.getElementById("<%= TextBoxCycloplegicRefractionOScylSign.ClientID %>"), false, false);
                enabledControl(document.getElementById("<%= TextBoxCycloplegicRefractionOSaxis.ClientID %>"), false, false);
                document.getElementById("QI6txta").style.color = "#333";
                document.getElementById("QI6txtb").style.color = "#333";
                document.getElementById("QI6txtc").style.color = "#333";
                document.getElementById("QI6txtd").style.color = "#333";
                document.getElementById("QI6txte").style.color = "#333";
                document.getElementById("QI6txtf").style.color = "#333";
                document.getElementById("QI6txtg").style.color = "#333";
                document.getElementById("QI6txth").style.color = "#333";
                document.getElementById("QI6txti").style.color = "#333";
                document.getElementById("QI6txtj").style.color = "#333";
                document.getElementById("QI6txtk").style.color = "#333";
                document.getElementById("QI6txtl").style.color = "#333";
            }
            else {
                enabledControl(document.getElementById("<%= RadioButtonCycloplegicRefractionODsphPlus.ClientID %>"), true, true);
                enabledControl(document.getElementById("<%= RadioButtonCycloplegicRefractionODsphMinus.ClientID %>"), true, true);
                enabledControl(document.getElementById("<%= TextBoxCycloplegicRefractionODsphSign.ClientID %>"), true, true);
                enabledControl(document.getElementById("<%= TextBoxCycloplegicRefractionODcylSign.ClientID %>"), true, true);
                enabledControl(document.getElementById("<%= TextBoxCycloplegicRefractionODaxis.ClientID %>"), true, true);
                enabledControl(document.getElementById("<%= RadioButtonCycloplegicRefractionOSsphPlus.ClientID %>"), true, true);
                enabledControl(document.getElementById("<%= RadioButtonCycloplegicRefractionOSsphMinus.ClientID %>"), true, true);
                enabledControl(document.getElementById("<%= TextBoxCycloplegicRefractionOSsphSign.ClientID %>"), true, true);
                enabledControl(document.getElementById("<%= TextBoxCycloplegicRefractionOScylSign.ClientID %>"), true, true);
                enabledControl(document.getElementById("<%= TextBoxCycloplegicRefractionOSaxis.ClientID %>"), true, true);
                document.getElementById("QI6txta").style.color = "gray";
                document.getElementById("QI6txtb").style.color = "gray";
                document.getElementById("QI6txtc").style.color = "gray";
                document.getElementById("QI6txtd").style.color = "gray";
                document.getElementById("QI6txte").style.color = "gray";
                document.getElementById("QI6txtf").style.color = "gray";
                document.getElementById("QI6txtg").style.color = "gray";
                document.getElementById("QI6txth").style.color = "gray";
                document.getElementById("QI6txti").style.color = "gray";
                document.getElementById("QI6txtj").style.color = "gray";
                document.getElementById("QI6txtk").style.color = "gray";
                document.getElementById("QI6txtl").style.color = "gray";
            }
        }

        // QI3
        function QI3TextField() {
            enabledControlTextField(document.getElementById("<%= TextBoxStereoacuityResult.ClientID %>"), false, true);
        }

        function QI3CheckBox() {
            enabledControl(document.getElementById("<%= CheckboxSensoryTestingPerformedOther.ClientID %>"), false, true);
        }

        // QI1a Distance ***************** Toggle controls for Q1a, distance
        function QI4aDistanceRB() {
            enabledControlDropDown(document.getElementById("<%= DropDownListWorth4DotDistanceDescription.ClientID %>"), false, true);
        }

        function QI4aDistanceCB() {
            enabledControl(document.getElementById("<%= CheckBoxWorth4DotDistanceUnableToComplete.ClientID %>"), false, true);
        }

        // QI1a Near ***************** Toggle controls for Q1a, near
        function QI4aNearRB() {
            enabledControlDropDown(document.getElementById("<%= DropDownListWorth4DotNearDescription.ClientID %>"), false, true);
        }

        function QI4aNearCB() {
            enabledControl(document.getElementById("<%= CheckBoxWorth4DotNearUnableToComplete.ClientID %>"), false, true);
        }


        // QI5a Distance
        function QI5aDistanceFirst() {
            enabledControlTextField(document.getElementById("<%= TextBoxAlignmentDistancePDET.ClientID %>"), false, true);

        }

        function QI5aDistanceCB() {
            enabledControl(document.getElementById("<%= CheckBoxAlignmentDistanceUnableToPerform.ClientID %>"), false, true);
        }



        function QI5nearRB() {
            enabledControl(document.getElementById("<%= CheckBoxAlignmentNearUnableToPerform.ClientID %>"), false, true);
        }

        // QO1 Distance
        function QO1DistanceFirst() {
            enabledControlTextField(document.getElementById("<%= TextBoxOutcome1AlignmentDistancePDET.ClientID %>"), false, true);

        }

        function QO1DistanceCB() {
            enabledControl(document.getElementById("<%= CheckBoxOutcome1AlignmentDistanceUnableToPerform.ClientID %>"), false, true);
        }

        // QO2
        function QO2TextField() {
            enabledControlTextField(document.getElementById("<%= TextBoxOutcome1StereoacuityValue.ClientID %>"), false, true);
        }

        function QO2CB() {
            enabledControl(document.getElementById("<%= CheckBoxOutcome1MethodStereoacuityUnableToComplete.ClientID %>"), false, true);
        }


        // QO3 Distance
        function QO3distanceRB() {
            enabledControlDropDown(document.getElementById("<%= DropDownListtWorth4dotdistance1.ClientID %>"), false, true);
        }
        function QO3distanceCB() {
            enabledControl(document.getElementById("<%= CheckBoxOutcome1Worth4DotDistanceUnableToComplete.ClientID %>"), false, true);
        }

        // QO3 Near
        function QO3nearRB() {
            enabledControlDropDown(document.getElementById("<%= DropDownListWorth4dotnear1.ClientID %>"), false, true);
        }
        function QO3nearCB() {
            enabledControl(document.getElementById("<%= CheckBoxOutcome1Worth4DotNearUnableToComplete.ClientID %>"), false, true);
        }


        // QO4 Distance
        function QO4DistanceFirst() {
            enabledControlTextField(document.getElementById("<%= TextBoxOutcome2AlignmentDistancePDET.ClientID %>"), false, true);
            enabledControlTextField(document.getElementById("<%= TextBoxOutcome2AlignmentDistancePDHT.ClientID %>"), false, true);
        }

        function QO4DistanceCB() {
            enabledControl(document.getElementById("<%= CheckBoxOutcome2AlignmentDistanceUnableToPerform.ClientID %>"), false, true);
        }

        // QO5
        function QO5TextBox() {
            enabledControlTextField(document.getElementById("<%= TextBoxOutcome2StereoacuityValue.ClientID %>"), false, true);
        }

        function QO5CB() {
            enabledControl(document.getElementById("<%= CheckBoxOutcome2MethodStereoacuityUnableToComplete.ClientID %>"), false, true);
        }

        // QO6 Distance
        function QO6DistanceRB() {

        }

        function QO6DistanceCB() {
            enabledControl(document.getElementById("<%= CheckBoxOutcome2Worth4DotDistanceUnableToComplete.ClientID %>"), false, true);
        }

        // QO6 Near
        function QO6NearRB() {

        }

        function QO6NearCB() {
            enabledControl(document.getElementById("<%= CheckBoxOutcome2Worth4DotNearUnableToComplete.ClientID %>"), false, true);
        }

        // Validation for Question 1
        function QC1onClick() {
            if (document.getElementById("<%= RadioButtonPostoperativeInfectionYes.ClientID %>").checked == true) {
                enabledControl(document.getElementById("<%= CheckBoxPostoperativeConjunctivitis.ClientID %>"), false, false);
                enabledControl(document.getElementById("<%= CheckBoxPostoperativePreseptalCellulitis.ClientID %>"), false, false);
                enabledControl(document.getElementById("<%= CheckBoxPostoperativeOrbitalCellulitis.ClientID %>"), false, false);
                enabledControl(document.getElementById("<%= CheckBoxPostoperativeEndophthalmitis.ClientID %>"), false, false);
                enabledControl(document.getElementById("<%= CheckBoxPostoperativeOther.ClientID %>"), false, false);
                document.getElementById("QC1txta").style.color = "#333";
                document.getElementById("QC1txtb").style.color = "#333";
                //document.getElementById("QC1txtm").style.color = "#333";
            }
            else {
                enabledControl(document.getElementById("<%= CheckBoxPostoperativeConjunctivitis.ClientID %>"), true, true);
                enabledControl(document.getElementById("<%= CheckBoxPostoperativePreseptalCellulitis.ClientID %>"), true, true);
                enabledControl(document.getElementById("<%= CheckBoxPostoperativeOrbitalCellulitis.ClientID %>"), true, true);
                enabledControl(document.getElementById("<%= CheckBoxPostoperativeEndophthalmitis.ClientID %>"), true, true);
                enabledControl(document.getElementById("<%= CheckBoxPostoperativeOther.ClientID %>"), true, true);
                document.getElementById("QC1txta").style.color = "gray";
                document.getElementById("QC1txtb").style.color = "gray";
                //document.getElementById("QC1txtm").style.color = "gray";
            }
        }

        // QO6 Distance
        function QO6DistanceRB() {
            enabledControlDropDown(document.getElementById("<%= DropDownListOutcome2Worth4DotDistance.ClientID %>"), false, true);
        }
        function QO6DistanceCB() {
            enabledControl(document.getElementById("<%= CheckBoxOutcome2Worth4DotDistanceUnableToComplete.ClientID %>"), false, true);
        }

        // QO6 Near
        function QO6NearRB() {
            enabledControlDropDown(document.getElementById("<%= DropDownListOutcome2Worth4DotNear.ClientID %>"), false, true);
        }
        function QO6NearCB() {
            enabledControl(document.getElementById("<%= CheckBoxOutcome2Worth4DotNearUnableToComplete.ClientID %>"), false, true);
        }

        window.onload = function onLoadForm() {
            QI1isClicked();
            QI6isClicked();
            QC1onClick();
        }

        function generate(arr1, arr2, istrue) {
            if (istrue == true) {
                for (var i = 0; i < arr1.length; i++) {
                    document.getElementById(arr1[i]).style.color = "gray";
                }
                for (var i = 0; i < arr2.length; i++) {
                    $(arr2[i] + ' :input').attr('disabled', true);
                    $(arr2[i] + ' :input').removeAttr("checked");
                }
            }
            if (istrue == false) {
                for (var i = 0; i < arr1.length; i++) {
                    document.getElementById(arr1[i]).style.color = "#333";
                }
                for (var i = 0; i < arr2.length; i++) {
                    $(arr2[i] + ' :input').removeAttr('disabled');
                }
            }
        }

        $(document).ready(function () {

            // Question 4, Initial Examination
            $("#<%= RadioButtonListWorth4DotPerformed.ClientID %>").click(function () {
                if ($('#<%= RadioButtonListWorth4DotPerformed.ClientID %>').find('input:checked').val() == '1') {
                    var myaray = new Array("QI4txta", "QI4txtb", "QI4txtc", "QI4txtd", "QI4txte");
                    var myarray2 = new Array('#QI4txtc', '#QI4txte');
                    generate(myaray, myarray2, false);

                    enabledControlDropDown(document.getElementById("<%= DropDownListWorth4DotDistanceDescription.ClientID %>"), false, false);
                    enabledControlDropDown(document.getElementById("<%= DropDownListWorth4DotNearDescription.ClientID %>"), false, false);
                }
                else {
                    var myaray = new Array("QI4txta", "QI4txtb", "QI4txtc", "QI4txtd", "QI4txte");
                    var myarray2 = new Array('#QI4txtc', '#QI4txte');
                    generate(myaray, myarray2, true);

                    enabledControlDropDown(document.getElementById("<%= DropDownListWorth4DotDistanceDescription.ClientID %>"), true, true);
                    enabledControlDropDown(document.getElementById("<%= DropDownListWorth4DotNearDescription.ClientID %>"), true, true);
                }
            });

            // Question 5, Initial Examination 
            $("#<%= RadioButtonAlignmentMeasuredYes.ClientID %>").click(function () {
                if ($('#<%= RadioButtonAlignmentMeasuredYes.ClientID %>').prop("checked") == true) {
                    var myaray = new Array("QIE5col1", "QIE5col2row1", "QIE3col2row2", "OrthoAa", "OrthoAb", "OrthoBa", "OrthoBb");
                    var myarray2 = new Array('#OrthoAa', '#OrthoAb', '#OrthoBa', '#OrthoBb');
                    generate(myaray, myarray2, false);

                    enabledControlTextField(document.getElementById("<%= TextBoxAlignmentDistancePDET.ClientID %>"), false, false);
                    enabledControlTextField(document.getElementById("<%= TextBoxAlignmentNearPDET.ClientID %>"), false, false);
                    enabledControlTextField(document.getElementById("<%= TextBoxPDHT.ClientID %>"), false, false);
                }
                else {
                    var myaray = new Array("QIE5col1", "QIE5col2row1", "QIE3col2row2", "OrthoAa", "OrthoAb", "OrthoBa", "OrthoBb");
                    var myarray2 = new Array('#OrthoAa', '#OrthoAb', '#OrthoBa', '#OrthoBb');
                    generate(myaray, myarray2, true);

                    enabledControlTextField(document.getElementById("<%= TextBoxAlignmentDistancePDET.ClientID %>"), true, true);
                    enabledControlTextField(document.getElementById("<%= TextBoxAlignmentNearPDET.ClientID %>"), true, true);
                    enabledControlTextField(document.getElementById("<%= TextBoxPDHT.ClientID %>"), true, true);
                }
            });
            $("#<%= RadioButtonAlignmentMeasuredNo.ClientID %>").click(function () {
                if ($('#<%= RadioButtonAlignmentMeasuredYes.ClientID %>').prop("checked") == true) {
                    var myaray = new Array("QIE5col1", "QIE5col2row1", "QIE3col2row2", "OrthoAa", "OrthoAb", "OrthoBa", "OrthoBb");
                    var myarray2 = new Array('#OrthoAa', '#OrthoAb', '#OrthoBa', '#OrthoBb');
                    generate(myaray, myarray2, false);

                    enabledControlTextField(document.getElementById("<%= TextBoxAlignmentDistancePDET.ClientID %>"), false, false);
                    enabledControlTextField(document.getElementById("<%= TextBoxAlignmentNearPDET.ClientID %>"), false, false);
                    enabledControlTextField(document.getElementById("<%= TextBoxPDHT.ClientID %>"), false, false);
                }
                else {
                    var myaray = new Array("QIE5col1", "QIE5col2row1", "QIE3col2row2", "OrthoAa", "OrthoAb", "OrthoBa", "OrthoBb");
                    var myarray2 = new Array('#OrthoAa', '#OrthoAb', '#OrthoBa', '#OrthoBb');
                    generate(myaray, myarray2, true);

                    enabledControlTextField(document.getElementById("<%= TextBoxAlignmentDistancePDET.ClientID %>"), true, true);
                    enabledControlTextField(document.getElementById("<%= TextBoxAlignmentNearPDET.ClientID %>"), true, true);
                    enabledControlTextField(document.getElementById("<%= TextBoxPDHT.ClientID %>"), true, true);
                }
            });
            if ($('#<%= RadioButtonAlignmentMeasuredYes.ClientID %>').prop("checked") == true) {
                var myaray = new Array("QIE5col1", "QIE5col2row1", "QIE3col2row2", "OrthoAa", "OrthoAb", "OrthoBa", "OrthoBb");
                var myarray2 = new Array('#OrthoAa', '#OrthoAb', '#OrthoBa', '#OrthoBb');
                generate(myaray, myarray2, false);

                enabledControlTextField(document.getElementById("<%= TextBoxAlignmentDistancePDET.ClientID %>"), false, false);
                enabledControlTextField(document.getElementById("<%= TextBoxAlignmentNearPDET.ClientID %>"), false, false);
                enabledControlTextField(document.getElementById("<%= TextBoxPDHT.ClientID %>"), false, false);
            }
            else {
                var myaray = new Array("QIE5col1", "QIE5col2row1", "QIE3col2row2", "OrthoAa", "OrthoAb", "OrthoBa", "OrthoBb");
                var myarray2 = new Array('#OrthoAa', '#OrthoAb', '#OrthoBa', '#OrthoBb');
                generate(myaray, myarray2, true);

                enabledControlTextField(document.getElementById("<%= TextBoxAlignmentDistancePDET.ClientID %>"), true, true);
                enabledControlTextField(document.getElementById("<%= TextBoxAlignmentNearPDET.ClientID %>"), true, true);
                enabledControlTextField(document.getElementById("<%= TextBoxPDHT.ClientID %>"), true, true);
            }

            // Question 5, Initial Examination: Ortho A
            $("#<%= RadioButtonAlignmentDistanceOrthotropia.ClientID %>").click(function () {
                if ($('#<%= RadioButtonAlignmentDistanceOrthotropia.ClientID %>').prop("checked") == true) {
                    var myaray = new Array("OrthoAb");
                    var myarray2 = new Array('#OrthoAb');
                    generate(myaray, myarray2, true);

                    enabledControlTextField(document.getElementById("<%= TextBoxAlignmentDistancePDET.ClientID %>"), true, true);
                }
                else {
                    var myaray = new Array("OrthoAb");
                    var myarray2 = new Array('#OrthoAb');
                    generate(myaray, myarray2, false);

                    enabledControlTextField(document.getElementById("<%= TextBoxAlignmentDistancePDET.ClientID %>"), false, false);
                }
            });
            if ($('#<%= RadioButtonAlignmentDistanceOrthotropia.ClientID %>').prop("checked") == true) {
                var myaray = new Array("OrthoAb");
                var myarray2 = new Array('#OrthoAb');
                generate(myaray, myarray2, true);

                enabledControlTextField(document.getElementById("<%= TextBoxAlignmentDistancePDET.ClientID %>"), true, true);
            }
            else {
                var myaray = new Array("OrthoAb");
                var myarray2 = new Array('#OrthoAb');
                generate(myaray, myarray2, false);

                enabledControlTextField(document.getElementById("<%= TextBoxAlignmentDistancePDET.ClientID %>"), false, false);
            }

            // Question 5, Initial Examination: Ortho B
            $("#<%= RadioButtonAlignmentNearOrthotropia.ClientID %>").click(function () {
                if ($('#<%= RadioButtonAlignmentNearOrthotropia.ClientID %>').prop("checked") == true) {
                    var myaray = new Array("OrthoBb");
                    var myarray2 = new Array('#OrthoBb');
                    generate(myaray, myarray2, true);

                    enabledControlTextField(document.getElementById("<%= TextBoxAlignmentNearPDET.ClientID %>"), true, true);
                    enabledControlTextField(document.getElementById("<%= TextBoxPDHT.ClientID %>"), true, true);
                }
                else {
                    var myaray = new Array("OrthoBb");
                    var myarray2 = new Array('#OrthoBb');
                    generate(myaray, myarray2, false);

                    enabledControlTextField(document.getElementById("<%= TextBoxAlignmentNearPDET.ClientID %>"), false, false);
                    enabledControlTextField(document.getElementById("<%= TextBoxPDHT.ClientID %>"), false, false);
                }
            });
            if ($('#<%= RadioButtonAlignmentNearOrthotropia.ClientID %>').prop("checked") == true) {
                var myaray = new Array("OrthoBb");
                var myarray2 = new Array('#OrthoBb');
                generate(myaray, myarray2, true);

                enabledControlTextField(document.getElementById("<%= TextBoxAlignmentNearPDET.ClientID %>"), true, true);
                enabledControlTextField(document.getElementById("<%= TextBoxPDHT.ClientID %>"), true, true);
            }
            else {
                var myaray = new Array("OrthoBb");
                var myarray2 = new Array('#OrthoBb');
                generate(myaray, myarray2, false);

                enabledControlTextField(document.getElementById("<%= TextBoxAlignmentNearPDET.ClientID %>"), false, false);
                enabledControlTextField(document.getElementById("<%= TextBoxPDHT.ClientID %>"), false, false);
            }

            // Question 1, Outcomes: Ortho A
            $("#<%= RadioButtonOutcome1AlignmentPDETDistanceOrthotropia.ClientID %>").click(function () {
                if ($('#<%= RadioButtonOutcome1AlignmentPDETDistanceOrthotropia.ClientID %>').prop("checked") == true) {
                    var myaray = new Array("Q1OrthoA", "Q1OrthoB");
                    var myarray2 = new Array('#Q1OrthoA', '#Q1OrthoB');
                    generate(myaray, myarray2, true);

                    enabledControlTextField(document.getElementById("<%= TextBoxOutcome1AlignmentDistancePDET.ClientID %>"), true, true);
                }
                else {
                    var myaray = new Array("Q1OrthoA", "Q1OrthoB");
                    var myarray2 = new Array('#Q1OrthoA', '#Q1OrthoB');
                    generate(myaray, myarray2, false);

                    enabledControlTextField(document.getElementById("<%= TextBoxOutcome1AlignmentDistancePDET.ClientID %>"), false, false);

                }
            });
            if ($('#<%= RadioButtonOutcome1AlignmentPDETDistanceOrthotropia.ClientID %>').prop("checked") == true) {
                var myaray = new Array("Q1OrthoA", "Q1OrthoB");
                var myarray2 = new Array('#Q1OrthoA', '#Q1OrthoB');
                generate(myaray, myarray2, true);

                enabledControlTextField(document.getElementById("<%= TextBoxOutcome1AlignmentDistancePDET.ClientID %>"), true, true);

            }
            else {
                var myaray = new Array("Q1OrthoA", "Q1OrthoB");
                var myarray2 = new Array('#Q1OrthoA', '#Q1OrthoB');
                generate(myaray, myarray2, false);

                enabledControlTextField(document.getElementById("<%= TextBoxOutcome1AlignmentDistancePDET.ClientID %>"), false, false);

            }

            // Question 1, Outcomes: Ortho B
            $("#<%= RadioButtonOutcome1AlignmentNearPDETOrthotropia.ClientID %>").click(function () {
                if ($('#<%= RadioButtonOutcome1AlignmentNearPDETOrthotropia.ClientID %>').prop("checked") == true) {
                    var myaray = new Array("Q1OrthoA2", "Q1OrthoB2");
                    var myarray2 = new Array('#Q1OrthoA2', '#Q1OrthoB2');
                    generate(myaray, myarray2, true);

                    enabledControlTextField(document.getElementById("<%= TextBoxOutcome1AlignmentNearPDET.ClientID %>"), true, true);
                    enabledControlTextField(document.getElementById("<%= TextBoxOutcome1AlignmentNearPDHT.ClientID %>"), true, true);
                }
                else {
                    var myaray = new Array("Q1OrthoA2", "Q1OrthoB2");
                    var myarray2 = new Array('#Q1OrthoA2', '#Q1OrthoB2');
                    generate(myaray, myarray2, false);

                    enabledControlTextField(document.getElementById("<%= TextBoxOutcome1AlignmentNearPDET.ClientID %>"), false, false);
                    enabledControlTextField(document.getElementById("<%= TextBoxOutcome1AlignmentNearPDHT.ClientID %>"), false, false);
                }
            });
            if ($('#<%= RadioButtonOutcome1AlignmentNearPDETOrthotropia.ClientID %>').prop("checked") == true) {
                var myaray = new Array("Q1OrthoA2", "Q1OrthoB2");
                var myarray2 = new Array('#Q1OrthoA2', '#Q1OrthoB2');
                generate(myaray, myarray2, true);

                enabledControlTextField(document.getElementById("<%= TextBoxOutcome1AlignmentNearPDET.ClientID %>"), true, true);
                enabledControlTextField(document.getElementById("<%= TextBoxOutcome1AlignmentNearPDHT.ClientID %>"), true, true);
            }
            else {
                var myaray = new Array("Q1OrthoA2", "Q1OrthoB2");
                var myarray2 = new Array('#Q1OrthoA2', '#Q1OrthoB2');
                generate(myaray, myarray2, false);

                enabledControlTextField(document.getElementById("<%= TextBoxOutcome1AlignmentNearPDET.ClientID %>"), false, false);
                enabledControlTextField(document.getElementById("<%= TextBoxOutcome1AlignmentNearPDHT.ClientID %>"), false, false);
            }


            // Question 4, Outcomes: Ortho A
            $("#<%= RadioButtonOutcome2AlignmentDistancePDETOrthotropia.ClientID %>").click(function () {
                if ($('#<%= RadioButtonOutcome2AlignmentDistancePDETOrthotropia.ClientID %>').prop("checked") == true) {
                    var myaray = new Array("QO4OrthoA1", "QO4OrthoB1");
                    var myarray2 = new Array('#QO4OrthoA1', '#QO4OrthoB1');
                    generate(myaray, myarray2, true);

                    enabledControlTextField(document.getElementById("<%= TextBoxOutcome2AlignmentDistancePDET.ClientID %>"), true, true);
                }
                else {
                    var myaray = new Array("QO4OrthoA1", "QO4OrthoB1");
                    var myarray2 = new Array('#QO4OrthoA1', '#QO4OrthoB1');
                    generate(myaray, myarray2, false);

                    enabledControlTextField(document.getElementById("<%= TextBoxOutcome2AlignmentDistancePDET.ClientID %>"), false, false);
                }
            });
            if ($('#<%= RadioButtonOutcome2AlignmentDistancePDETOrthotropia.ClientID %>').prop("checked") == true) {
                var myaray = new Array("QO4OrthoA1", "QO4OrthoB1");
                var myarray2 = new Array('#QO4OrthoA1', '#QO4OrthoB1');
                generate(myaray, myarray2, true);

                enabledControlTextField(document.getElementById("<%= TextBoxOutcome2AlignmentDistancePDET.ClientID %>"), true, true);
            }
            else {
                var myaray = new Array("QO4OrthoA1", "QO4OrthoB1");
                var myarray2 = new Array('#QO4OrthoA1', '#QO4OrthoB1');
                generate(myaray, myarray2, false);

                enabledControlTextField(document.getElementById("<%= TextBoxOutcome2AlignmentDistancePDET.ClientID %>"), false, false);
            }

            // Question 4, Outcomes: Ortho B
            $("#<%= RadioButtonOutcome2AlignmentNearPDETOrthotropia.ClientID %>").click(function () {
                if ($('#<%= RadioButtonOutcome2AlignmentNearPDETOrthotropia.ClientID %>').prop("checked") == true) {
                    var myaray = new Array("QO4OrthoA2", "QO4OrthoB2");
                    var myarray2 = new Array('#QO4OrthoA2', '#QO4OrthoB2');
                    generate(myaray, myarray2, true);

                    enabledControlTextField(document.getElementById("<%= TextBoxOutcome2AlignmentNearPDET.ClientID %>"), true, true);
                    enabledControlTextField(document.getElementById("<%= TextBoxOutcome2AlignmentNearPDHT.ClientID %>"), true, true);
                }
                else {
                    var myaray = new Array("QO4OrthoA2", "QO4OrthoB2");
                    var myarray2 = new Array('#QO4OrthoA2', '#QO4OrthoB2');
                    generate(myaray, myarray2, false);

                    enabledControlTextField(document.getElementById("<%= TextBoxOutcome2AlignmentNearPDET.ClientID %>"), false, false);
                    enabledControlTextField(document.getElementById("<%= TextBoxOutcome2AlignmentNearPDHT.ClientID %>"), false, false);
                }
            });
            if ($('#<%= RadioButtonOutcome2AlignmentNearPDETOrthotropia.ClientID %>').prop("checked") == true) {
                var myaray = new Array("QO4OrthoA2", "QO4OrthoB2");
                var myarray2 = new Array('#QO4OrthoA2', '#QO4OrthoB2');
                generate(myaray, myarray2, true);

                enabledControlTextField(document.getElementById("<%= TextBoxOutcome2AlignmentNearPDET.ClientID %>"), true, true);
                enabledControlTextField(document.getElementById("<%= TextBoxOutcome2AlignmentNearPDHT.ClientID %>"), true, true);
            }
            else {
                var myaray = new Array("QO4OrthoA2", "QO4OrthoB2");
                var myarray2 = new Array('#QO4OrthoA2', '#QO4OrthoB2');
                generate(myaray, myarray2, false);

                enabledControlTextField(document.getElementById("<%= TextBoxOutcome2AlignmentNearPDET.ClientID %>"), false, false);
                enabledControlTextField(document.getElementById("<%= TextBoxOutcome2AlignmentNearPDHT.ClientID %>"), false, false);
            }

            /*******************************************************************************************************************
            ********************************************************************************************************************
            *******************************************************************************************************************/
            // Question 2, Complications
            $("#<%= RadioButtonComplicationReoperationWithin1MonthYes.ClientID %>").click(function () {
                if ($('#<%= RadioButtonComplicationReoperationWithin1MonthYes.ClientID %>').prop("checked") == true) {
                    var myaray = new Array("QC2txta", "QC2txtb");
                    var myarray2 = new Array('#QC2txtb');
                    generate(myaray, myarray2, false);
                }
                else {
                    var myaray = new Array("QC2txta", "QC2txtb");
                    var myarray2 = new Array('#QC2txtb');
                    generate(myaray, myarray2, true);
                }
            });
            $("#<%= RadioButtonComplicationReoperationWithin1MonthNo.ClientID %>").click(function () {
                if ($('#<%= RadioButtonComplicationReoperationWithin1MonthYes.ClientID %>').prop("checked") == true) {
                    var myaray = new Array("QC2txta", "QC2txtb");
                    var myarray2 = new Array('#QC2txtb');
                    generate(myaray, myarray2, false);
                }
                else {
                    var myaray = new Array("QC2txta", "QC2txtb");
                    var myarray2 = new Array('#QC2txtb');
                    generate(myaray, myarray2, true);
                }
            });
            if ($('#<%= RadioButtonComplicationReoperationWithin1MonthYes.ClientID %>').prop("checked") == true) {
                var myaray = new Array("QC2txta", "QC2txtb");
                var myarray2 = new Array('#QC2txtb');
                generate(myaray, myarray2, false);
            }
            else {
                var myaray = new Array("QC2txta", "QC2txtb");
                var myarray2 = new Array('#QC2txtb');
                generate(myaray, myarray2, true);
            }
        });

    </script>

    <script type="text/javascript">
        // Tip section



        $(function () {
            $('#QH7').aToolTip({
                clickIt: true,
                tipContent: '&lt;&nbsp;32 weeks.'
            });

            $('#QIE6a').aToolTip({
                clickIt: true,
                tipContent: 'Ranges for sph [+/- 0.00 thru 30.00],<br />cyl [0.00 thru 20.00], axis [integer 0 thru 180]'
            });
        });

        $(function () {
            $('#QI1a').aToolTip({
                clickIt: true,
                tipContent: '20/800 or count fingers @ 5 ft<br />20/1000 or count fingers @ 4 ft<br />20/1600 or count fingers @ 3ft<br />20/2000 or count fingers @ 2 ft<br />20/4000 or count fingers @ 1 ft<br />20/7777 =CF<br />20/8888 =HM<br />20/9999 =LP<br />20/0000 =NLP'
            });
        });
        $(function () {
            $('#QI1b').aToolTip({
                clickIt: true,
                tipContent: 'i.e.-&quot;central, steady and maintained&quot;'
            });
        });
    </script>
    <script type="text/javascript" language="javascript">
        $(document).ready(function () {
            /** instruct the metadata plugin where to look the metadata
            * jQuery.metadata.setType( type, name );
            * please read the metadata instructions for additional information
            * http://plugins.jquery.com/project/metadata
            */
            $.metadata.setType('attr', 'meta');

            /** To call autoNumeric
            * $(selector).autoNumeric({options}); 
            * The below example uses the input & class selector
            */
            $('input.auto').autoNumeric();
            /* scripts for metadata code generator  */

            /* rountine that prevents  numeric characters from being entered the the altDec field  */
            $('#altDecb').keypress(function (e) {
                var cc = String.fromCharCode(e.which);
                if (e.which != 32 && cc >= 0 && cc <= 9) {
                    e.preventDefault();
                }
            });

            /* rountine that prevents  apostrophe, comma, more than one period (full stop) or numeric characters from being entered the the aSign field  */
            $('#aSignb').keypress(function (e) {
                var cc = String.fromCharCode(e.which);
                if ((e.which != 32 && cc >= 0 && cc <= 9) || cc == "," || cc == "'" || cc == "." && this.value.lastIndexOf('.') != -1) {
                    e.preventDefault();
                }
            });

            $("input.md").bind('click keyup blur', function () {
                var metaCode = '', aSep = '', dGroup = '', aDec = '', altDec = '', aSign = '', pSign = '', vMin = '', vMax = '', mDec = '', mRound = '', aPad = '', wEmpty = '', aForm = '';
                if ($("input:radio[name=aSep]:checked").attr('id') == 'aSepc') {
                    $('input:radio[name=aDec]:nth(0)').removeAttr("disabled");
                    $('input:radio[name=aDec]:nth(0)').attr('checked', true);
                    $('input:radio[name=aDec]:nth(1)').attr("disabled", true);
                }
                if ($("input:radio[name=aSep]:checked").attr('id') == 'aSepp') {
                    $('input:radio[name=aDec]:nth(1)').removeAttr("disabled");
                    $('input:radio[name=aDec]:nth(1)').attr('checked', true);
                    $('input:radio[name=aDec]:nth(0)').attr("disabled", true);
                }
                if ($("input:radio[name=aSep]:checked").attr('id') != 'aSepc' || $("input:radio[name=aSep]:checked").attr('id') != 'aSepp') {
                    $('input:radio[name=aDec]:nth(0)').removeAttr("disabled");
                    $('input:radio[name=aDec]:nth(1)').removeAttr("disabled");
                }
                aSep = $("input:radio[name=aSep]:checked").val();
                dGroup = $("input:radio[name=dGroup]:checked").val();
                aDec = $("input:radio[name=aDec]:checked").val();

                if ($("input:radio[name=altDec]:checked").attr('id') == 'altDecd') {
                    $('#altDecb').val('');
                    $('#altDecb').attr("disabled", true);
                }
                if ($("input:radio[name=altDec]:checked").attr('id') == 'altDeca') {
                    $('#altDecb').removeAttr("disabled");
                    altDec = $('#altDecb').val();
                }

                if ($("input:radio[name=aSign]:checked").attr('id') == 'aSignd') {
                    $('#aSignb').val('');
                    $('#aSignb').attr("disabled", true);
                }
                if ($("input:radio[name=aSign]:checked").attr('id') == 'aSigna') {
                    $('#aSignb').removeAttr("disabled");
                    aSign = $('#aSignb').val();
                }

                pSign = $("input:radio[name=pSign]:checked").val();
                if ($("input:radio[name=vMin]:checked").attr('id') == 'vMind') {
                    $('#vMinb').val('');
                    $('#vMinb').attr("disabled", true);
                }
                if ($("input:radio[name=vMin]:checked").attr('id') == 'vMina') {
                    $('#vMinb').removeAttr("disabled");
                    vMin = $('#vMinb').val();
                }
                if ($("input:radio[name=vMax]:checked").attr('id') == 'vMaxd') {
                    $('#vMaxb').val('');
                    $('#vMaxb').attr("disabled", true);
                }
                if ($("input:radio[name=vMax]:checked").attr('id') == 'vMaxa') {
                    $('#vMaxb').removeAttr("disabled");
                    vMax = $('#vMaxb').val();
                }
                if ($("input:radio[name=mDec]:checked").attr('id') == 'mDecd') {
                    $('#mDecbb').val('');
                    $('#mDecbb').attr("disabled", true);
                }
                if ($("input:radio[name=mDec]:checked").attr('id') == 'mDeca') {
                    $('#mDecbb').removeAttr("disabled");
                    mDec = $('#mDecbb').val();
                }
                mRound = $("input:radio[name=mRound]:checked").val();
                aPad = $("input:radio[name=aPad]:checked").val();
                wEmpty = $("input:radio[name=wEmpty]:checked").val();
                if (aSep != '') {
                    metaCode = aSep;
                }
                if (dGroup != '') {
                    if (metaCode != '') {
                        metaCode = metaCode + ", " + dGroup;
                    }
                    else {
                        metaCode = dGroup;
                    }
                }
                if (aDec != '') {
                    if (metaCode != '') {
                        metaCode = metaCode + ", " + aDec;
                    }
                    else {
                        metaCode = aDec;
                    }
                }
                if (altDec != '') {
                    if (metaCode != '') {
                        metaCode = metaCode + ", altDec: '" + altDec + "'";
                    }
                    else {
                        metaCode = "altDec: '" + altDec + "'";
                    }
                }
                if (aSign != '') {
                    if (metaCode != '') {
                        metaCode = metaCode + ", aSign: '" + aSign + "'";
                    }
                    else {
                        metaCode = "aSign: '" + aSign + "'";
                    }
                }
                if (pSign != '') {
                    if (metaCode != '') {
                        metaCode = metaCode + ", " + pSign;
                    }
                    else {
                        metaCode = pSign;
                    }
                }
                if (vMin != '') {
                    if (metaCode != '') {
                        metaCode = metaCode + ", vMin: '" + vMin + "'";
                    }
                    else {
                        metaCode = "vMin: '" + vMin + "'";
                    }
                }
                if (vMax != '') {
                    if (metaCode != '') {
                        metaCode = metaCode + ", vMax: '" + vMax + "'";
                    }
                    else {
                        metaCode = "vMax: '" + vMax + "'";
                    }
                }
                if (mDec != '') {
                    if (metaCode != '') {
                        metaCode = metaCode + ", mDec: '" + $('#mDecbb').val() + "'";
                    }
                    else {
                        metaCode = "mDec: '" + $('#mDecbb').val() + "'";
                    }
                }

                if (mRound != '') {
                    if (metaCode != '') {
                        metaCode = metaCode + ", " + mRound;
                    }
                    else {
                        metaCode = mRound;
                    }
                }
                if (aPad != '') {
                    if (metaCode != '') {
                        metaCode = metaCode + ", " + aPad;
                    }
                    else {
                        metaCode = aPad;
                    }
                }
                if (wEmpty != '') {
                    if (metaCode != '') {
                        metaCode = metaCode + ", " + wEmpty;
                    }
                    else {
                        metaCode = wEmpty;
                    }
                }
                $('#metaCode').text('');
                if (metaCode != '') {
                    $('#metaCode').text('meta="{' + metaCode + '}"');
                }
            });

            /* clears the metadata code  */
            $('#rd').click(function () {
                $('#metaCode').text('');
            });
            /* ends scripts for metadata code generator  */

            /* script  for defaults demo  */
            $('#d_noMeta').blur(function () {
                var convertInput = '';
                convertInput = $(this).autoNumericGet();
                $('#d_Get').val(convertInput);
                $('#d_Set').autoNumericSet(convertInput);
            });
            /* end script  for defaults demo  */

            /* script  for various samples demo  */
            $('input[name$="sample"]').blur(function () {
                var convertInput = '';
                var row = 'row_' + this.id.charAt(4);
                convertInput = $(this).autoNumericGet();
                $('#' + row + 'b').val(convertInput);
                $('#' + row + 'c').autoNumericSet(convertInput);
            });
            /* end script  for various samples demo  */

            /* script  for rounding methods  */
            $('#roundValue').blur(function () {
                if (this.value != '') {
                    convertInput = $('#roundValue').autoNumericGet();
                    var i = 1;
                    for (i = 1; i <= 9; i++) {
                        $('#roundMethod' + i).autoNumericSet(convertInput);
                    }
                }
            });

            $('#roundDecimal').change(function () { /* changes decimal places */
                convertInput = $('#roundValue').autoNumericGet();
                if (convertInput > 0) {
                    var i = 1;
                    for (i = 1; i <= 9; i++) {
                        $('#roundMethod' + i).autoNumericSet(convertInput);
                    }
                }
            });
            /* end script  for rounding methods  */

            /* script for dynamically loaded values  demo*/
            $.getJSON("test_JSON.php", function (data) {
                var valueFormatted = '';
                $.each(data, function (key, value) { // loops through JSON keys and returns value	
                    $('#' + key).autoNumericSet(value);
                });
            });
            /* end script for dynamically loaded values demo*/

            /* script for callback demo*/
            $.autoNumeric.get_mDec = function () { /* get_mDec function attached to autoNumeric() */
                var set_mDec = $('#get_metricUnit').val();
                if (set_mDec == ' km') {
                    set_mDec = 3;
                } else {
                    set_mDec = 0;
                }
                return set_mDec; /* set mDec decimal places */
            }

            var get_vMax = function () { /* set the maximum value allowed based on the metric unit */
                var set_vMax = $('#get_metricUnit').val();
                if (set_vMax == ' km') {
                    set_vMax = '99999.999';
                } else {
                    set_vMax = '99999999';
                }
                return set_vMax;
            }

            $('#length').autoNumeric({ vMax: get_vMax }); /* calls autoNumeric and passes function get_vMax */

            $('#get_metricUnit').change(function () {
                var set_value = $('#length').autoNumericGet();
                if (this.value == ' km') {
                    set_value = set_value / 1000;
                } else {
                    set_value = set_value * 1000;
                }
                $('#length').autoNumericSet(set_value);
            });
            /* end script for callback demo*/

            $('<%= TextBoxCycloplegicRefractionODsphSign.ClientID %>.auto').autoNumeric();
            $('<%= TextBoxCycloplegicRefractionODcylSign.ClientID %>.auto').autoNumeric();
            $('<%= TextBoxCycloplegicRefractionODaxis.ClientID %>.auto').autoNumeric();

            $('<%= TextBoxCycloplegicRefractionOSsphSign.ClientID %>.auto').autoNumeric();
            $('<%= TextBoxCycloplegicRefractionOScylSign.ClientID %>.auto').autoNumeric();
            $('<%= TextBoxCycloplegicRefractionOSaxis.ClientID %>.auto').autoNumeric();

            $('<%= TextBoxStereoacuityResult.ClientID %>.auto').autoNumeric();
            $('<%= TextBoxOutcome2StereoacuityValue.ClientID %>.auto').autoNumeric();

            $('<%= TextBoxAlignmentDistancePDET.ClientID %>.auto').autoNumeric();
            $('<%= TextBoxAlignmentDistancePDHT.ClientID %>.auto').autoNumeric();
            $('<%= TextBoxAlignmentNearPDET.ClientID %>.auto').autoNumeric();
            $('<%= TextBoxPDHT.ClientID %>.auto').autoNumeric();

            $('<%= TextBoxOutcome1AlignmentDistancePDET.ClientID %>.auto').autoNumeric();
            $('<%= TextBoxOutcome1AlignmentDistancePDHT.ClientID %>.auto').autoNumeric();
            $('<%= TextBoxOutcome1AlignmentNearPDET.ClientID %>.auto').autoNumeric();
            $('<%= TextBoxOutcome1AlignmentNearPDHT.ClientID %>.auto').autoNumeric();

            $('<%= TextBoxOutcome1StereoacuityValue.ClientID %>.auto').autoNumeric();

            $('<%= TextBoxOutcome2AlignmentDistancePDET.ClientID %>.auto').autoNumeric();
            $('<%= TextBoxOutcome2AlignmentDistancePDHT.ClientID %>.auto').autoNumeric();
            $('<%= TextBoxOutcome2AlignmentNearPDET.ClientID %>.auto').autoNumeric();
            $('<%= TextBoxOutcome2AlignmentNearPDHT.ClientID %>.auto').autoNumeric();
        });
    </script>
</asp:Content>