﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Transactions;
using NetHealthPIMModel;

public partial class abo_CSUChart : BasePage
{
    protected void Page_Load(object sender, EventArgs e)
    {
    
    if (!IsPostBack)
        {
            InitializePage();
        }


    }
    protected void InitializePage()
    {
        ((PIMMasterPage)Page.Master).SetStatus(2);
        ((PIMMasterPage)Page.Master).SetTitle("Chart");

        using (NetHealthPIMEntities pim = new NetHealthPIMEntities())
        {
            int ModuleID = (int)Session[Constants.SESSION_WORKINGMODULEID];
            Module module = pim.Module.First(c => c.ModuleID == ModuleID);
            ((PIMMasterPage)Page.Master).SetHeader(module.ModuleName + " Chart Abstraction");

            ParticipantModuleCycle prev = (ParticipantModuleCycle)Session[Constants.SESSION_PREVIOUSCYCLE];
            String CycleNumber = Request.QueryString["CycleNumber"];

            int cycleID = ctx.ActiveModuleCycleID;
            if (CycleNumber == "1")
            {
                cycleID = prev.ParticipantModuleCycleID;
                LinkButtonBackToDashboard.Visible = true;
                ButtonSubmit.Visible = false;
            }

            ParticipantModuleCycle cycle = pim.ParticipantModuleCycle.First(c => c.ParticipantModuleCycleID == cycleID);
            String numberOfRecord = Request.QueryString["nr"];
            String totalRecords = Request.QueryString["t"];
            if (cycle.CycleNumber == 1)
            {
                ((PIMMasterPage)Page.Master).SetStatus(2);
                LiteralAbstractionNumber.Text = "Initial Abstraction: Chart " + numberOfRecord + " of " + totalRecords;
            }
            else
            {
                ((PIMMasterPage)Page.Master).SetStatus(3);
                LiteralAbstractionNumber.Text = "Second Abstraction: Chart " + numberOfRecord + " of " + totalRecords;
            }
            DropDownListMonthOfBirth.DataSource = CycleManager.getMonthList();
            DropDownListYearOfBirth.DataSource = CycleManager.getDOBYearList(0, 100);
            DropDownListMonthOfOnsetFirstEpisode.DataSource = CycleManager.getMonthList();
            DropDownListYearOfOnsetFirstEpisode.DataSource = CycleManager.getDOBYearList(0, 100);
            DropDownListBCVASnellen.DataSource = CycleManager.getExamValues();
            DropDownListMonthOfCataractSurgery.DataSource = CycleManager.getMonthList();
            DropDownListYearOfCataractSurgery.DataSource = CycleManager.getDOBYearList(0, 100);
            DropDownListOutcomeBCVASnellen.DataSource = CycleManager.getExamValues();
         
            DataBind();
            DropDownListYearOfBirth.Items.Insert(0, new ListItem("Year", "0"));
            DropDownListYearOfOnsetFirstEpisode.Items.Insert(0, new ListItem("Year", "0"));
            DropDownListYearOfCataractSurgery.Items.Insert(0, new ListItem("Year", "0"));
            string recordIdentifier = Request.QueryString["ri"];
            HiddenFieldRecordIdentifier.Value = recordIdentifier;
            LiteralRecordIdentifier.Text = recordIdentifier;
            var count = pim.ChartAbstractionUveitis.Where(ps => ps.ParticipantModuleCycle.ParticipantModuleCycleID == cycleID & ps.RecordIdentifier == recordIdentifier).Count();
            if (count == 0)
            {
                using (TransactionScope transaction = new TransactionScope())
                {
                    //Module module = pim.Module.First(m => m.ModuleID == Constants.ABO_AMBLYOPIA_MODULEID);

                    NetHealthPIMModel.ChartRegistration chartRegistration = (from cr in pim.ChartRegistration
                                                           where cr.ParticipantModuleCycleID == cycleID
                                                           & cr.RecordIdentifier == recordIdentifier
                                                           & cr.ModuleID == 33
                                                           select cr).First<NetHealthPIMModel.ChartRegistration>();

                    DateTime initialVisit = Convert.ToDateTime(chartRegistration.InitialVisit);
                    DateTime DOB = Convert.ToDateTime(chartRegistration.DOB);


                    ChartAbstractionUveitis abstractRecord = new ChartAbstractionUveitis();
                    abstractRecord.ParticipantModuleCycle = cycle;
                    abstractRecord.RecordIdentifier = recordIdentifier;
                    abstractRecord.MonthOfBirth = DOB.Month;
                    abstractRecord.YearOfBirth = DOB.Year;
                    DropDownListYearOfBirth.SelectedValue = abstractRecord.YearOfBirth.ToString();
                    DropDownListMonthOfBirth.SelectedValue = abstractRecord.MonthOfBirth.ToString();

                    abstractRecord.CreatedOn = DateTime.Now;
                    abstractRecord.LastUpdateDate = DateTime.Now;
                    pim.SaveChanges();

                    transaction.Complete();
                }
            }
            else
            {

                ChartAbstractionUveitis abstractRecord = (from c in pim.ChartAbstractionUveitis
                                                        where c.ParticipantModuleCycle.ParticipantModuleCycleID == cycleID & c.RecordIdentifier == recordIdentifier
                                                          select c).First<ChartAbstractionUveitis>();
                if (abstractRecord.PresentingSymptomDecreasedVision != null) CheckBoxPresentingSymptomDecreasedVision.Checked = ((bool)abstractRecord.PresentingSymptomDecreasedVision);
                if (abstractRecord.PresentingSymptomGlare != null) CheckBoxPresentingSymptomGlare.Checked = ((bool)abstractRecord.PresentingSymptomGlare);
                if (abstractRecord.PresentingSymptomDailyLiving != null) CheckBoxPresentingSymptomDailyLiving.Checked = ((bool)abstractRecord.PresentingSymptomDailyLiving);
                if (abstractRecord.PresentingSymptomVisualizePosterior != null) CheckBoxPresentingSymptomVisualizePosterior.Checked = ((bool)abstractRecord.PresentingSymptomVisualizePosterior);
                if (abstractRecord.PresentingSymptomOther != null) CheckBoxPresentingSymptomOther.Checked = ((bool)abstractRecord.PresentingSymptomOther);
                if (abstractRecord.UveitisAnterior != null) CheckBoxUveitisAnterior.Checked = ((bool)abstractRecord.UveitisAnterior);
                if (abstractRecord.UveitisIntermediate != null) CheckBoxUveitisIntermediate.Checked = ((bool)abstractRecord.UveitisIntermediate);
                if (abstractRecord.UveitisPosterior != null) CheckBoxUveitisPosterior.Checked = ((bool)abstractRecord.UveitisPosterior);
                if (abstractRecord.UveitisPanuveitis != null) CheckBoxUveitisPanuveitis.Checked = ((bool)abstractRecord.UveitisPanuveitis);
                if (abstractRecord.UveitisRecurrent != null) CheckBoxUveitisRecurrent.Checked = ((bool)abstractRecord.UveitisRecurrent);
                if (abstractRecord.UveitisCourse != null) CheckBoxUveitisCourse.Checked = ((bool)abstractRecord.UveitisCourse);
                if (abstractRecord.EtiologyFuchs != null) CheckBoxEtiologyFuchs.Checked = ((bool)abstractRecord.EtiologyFuchs);
                if (abstractRecord.EtiologyHLA != null) CheckBoxEtiologyHLA.Checked = ((bool)abstractRecord.EtiologyHLA);
                if (abstractRecord.EtiologySarcoidosis != null) CheckBoxEtiologySarcoidosis.Checked = ((bool)abstractRecord.EtiologySarcoidosis);
                if (abstractRecord.EtiologyPars != null) CheckBoxEtiologyPars.Checked = ((bool)abstractRecord.EtiologyPars);
                if (abstractRecord.EtiologyScleritisRA != null) CheckBoxEtiologyScleritisRA.Checked = ((bool)abstractRecord.EtiologyScleritisRA);
                if (abstractRecord.EtiologyCMVRetinitis != null) CheckBoxEtiologyCMVRetinitis.Checked = ((bool)abstractRecord.EtiologyCMVRetinitis);
                if (abstractRecord.EtiologyJIA != null) CheckBoxEtiologyJIA.Checked = ((bool)abstractRecord.EtiologyJIA);
                if (abstractRecord.EtiologyIdiopathic != null) CheckBoxEtiologyIdiopathic.Checked = ((bool)abstractRecord.EtiologyIdiopathic);
                if (abstractRecord.EtiologyOther != null) CheckBoxEtiologyOther.Checked = ((bool)abstractRecord.EtiologyOther);
                if (abstractRecord.TherapyToDateCorticosteroids != null) CheckBoxTherapyToDateCorticosteroids.Checked = ((bool)abstractRecord.TherapyToDateCorticosteroids);
                if (abstractRecord.TherapyToDateCSTopical != null) CheckBoxTherapyToDateCSTopical.Checked = ((bool)abstractRecord.TherapyToDateCSTopical);
                if (abstractRecord.TherapyToDateCSPeriocular != null) CheckBoxTherapyToDateCSPeriocular.Checked = ((bool)abstractRecord.TherapyToDateCSPeriocular);
                if (abstractRecord.TherapyToDateCSSystemic != null) CheckBoxTherapyToDateCSSystemic.Checked = ((bool)abstractRecord.TherapyToDateCSSystemic);
                if (abstractRecord.TherapyToDateCSIntravitreal != null) CheckBoxTherapyToDateCSIntravitreal.Checked = ((bool)abstractRecord.TherapyToDateCSIntravitreal);
                if (abstractRecord.TherapyToDateImmu != null) CheckBoxTherapyToDateImmu.Checked = ((bool)abstractRecord.TherapyToDateImmu);
                if (abstractRecord.IrisBombe != null) CheckBoxIrisBombe.Checked = ((bool)abstractRecord.IrisBombe);
                if (abstractRecord.PreopComorbidEpiretinal != null) CheckBoxPreopComorbidEpiretinal.Checked = ((bool)abstractRecord.PreopComorbidEpiretinal);
                if (abstractRecord.PreopCormorbidCystoid != null) CheckBoxPreopCormorbidCystoid.Checked = ((bool)abstractRecord.PreopCormorbidCystoid);
                if (abstractRecord.PreopCormorbidChoroidal != null) CheckBoxPreopCormorbidChoroidal.Checked = ((bool)abstractRecord.PreopCormorbidChoroidal);
                if (abstractRecord.PreopCormorbidNeovasc != null) CheckBoxPreopCormorbidNeovasc.Checked = ((bool)abstractRecord.PreopCormorbidNeovasc);
                if (abstractRecord.PreopCormorbidTears != null) CheckBoxPreopCormorbidTears.Checked = ((bool)abstractRecord.PreopCormorbidTears);
                if (abstractRecord.PreopCormorbidGlaucoma != null) CheckBoxPreopCormorbidGlaucoma.Checked = ((bool)abstractRecord.PreopCormorbidGlaucoma);
                if (abstractRecord.PreopCormorbidKerotopathy != null) CheckBoxPreopCormorbidKerotopathy.Checked = ((bool)abstractRecord.PreopCormorbidKerotopathy);
                if (abstractRecord.PreopCormorbidOpacification != null) CheckBoxPreopCormorbidOpacification.Checked = ((bool)abstractRecord.PreopCormorbidOpacification);
                if (abstractRecord.PreopComorbidOther != null) CheckBoxPreopCormorbidOther.Checked = ((bool)abstractRecord.PreopComorbidOther);
                if (abstractRecord.OutcomeMedCS != null) CheckBoxOutcomeMedCS.Checked = ((bool)abstractRecord.OutcomeMedCS);
                if (abstractRecord.OutcomeMedCSTopical != null) CheckBoxOutcomeMedCSTopical.Checked = ((bool)abstractRecord.OutcomeMedCSTopical);
                if (abstractRecord.OutcomeMedCSPeriocular != null) CheckBoxOutcomeMedCSPeriocular.Checked = ((bool)abstractRecord.OutcomeMedCSPeriocular);
                if (abstractRecord.OutcomeMedCSSystemic != null) CheckBoxOutcomeMedCSSystemic.Checked = ((bool)abstractRecord.OutcomeMedCSSystemic);
                if (abstractRecord.OutcomeMedCSIntravitreal != null) CheckBoxOutcomeMedCSIntravitreal.Checked = ((bool)abstractRecord.OutcomeMedCSIntravitreal);
                if (abstractRecord.OutcomeMedCSImmu != null) CheckBoxOutcomeMedCSImmu.Checked = ((bool)abstractRecord.OutcomeMedCSImmu);
                if (abstractRecord.RBOutcomeAdditionalYag != null) CheckBoxRBOutcomeAdditionalYag.Checked = ((bool)abstractRecord.RBOutcomeAdditionalYag);
                if (abstractRecord.RBOutcomeAdditionalIOL != null) CheckBoxRBOutcomeAdditionalIOL.Checked = ((bool)abstractRecord.RBOutcomeAdditionalIOL);
                if (abstractRecord.RBOutcomeAdditionalAspiration != null) CheckBoxRBOutcomeAdditionalAspiration.Checked = ((bool)abstractRecord.RBOutcomeAdditionalAspiration);
                if (abstractRecord.RBOutcomeAdditionalLaser != null) CheckBoxRBOutcomeAdditionalLaser.Checked = ((bool)abstractRecord.RBOutcomeAdditionalLaser);
                if (abstractRecord.RBOutcomeAdditionalRepairDetachment != null) CheckBoxRBOutcomeAdditionalRepairDetachment.Checked = ((bool)abstractRecord.RBOutcomeAdditionalRepairDetachment);
                if (abstractRecord.RBOutcomeAdditionalParsPlana != null) CheckBoxRBOutcomeAdditionalParsPlana.Checked = ((bool)abstractRecord.RBOutcomeAdditionalParsPlana);
                if (abstractRecord.RBOutcomeAdditionalOther != null) CheckBoxRBOutcomeAdditionalOther.Checked = ((bool)abstractRecord.RBOutcomeAdditionalOther);

                if (abstractRecord.Nuclear != null) CheckBoxNuclear.Checked = ((bool)abstractRecord.Nuclear);
                if (abstractRecord.Cortical != null) Cortical.Checked = ((bool)abstractRecord.Cortical);
                if (abstractRecord.Posterior != null) Posterior.Checked = ((bool)abstractRecord.Posterior);
                if (abstractRecord.ProlifilativeDiabeticRetinopathy != null) CheckBoxProlifilativeDiabeticRetinopathy.Checked = ((bool)abstractRecord.ProlifilativeDiabeticRetinopathy);
                if (abstractRecord.NonProlifilativeDiabeticRetinopathy != null) CheckBoxNonProlifilativeDiabeticRetinopathy.Checked = ((bool)abstractRecord.NonProlifilativeDiabeticRetinopathy);
                if (abstractRecord.UnplannedAnteriorVitrectomy != null) CheckBoxUnplannedAnteriorVitrectomy.Checked = ((bool)abstractRecord.UnplannedAnteriorVitrectomy);
                if (abstractRecord.SuprachorodialHemorrhage != null) CheckBoxSuprachorodialHemorrhage.Checked = ((bool)abstractRecord.SuprachorodialHemorrhage);
                if (abstractRecord.Endophthalmitis != null) CheckBoxEndophthalmitis.Checked = ((bool)abstractRecord.Endophthalmitis);
                if (abstractRecord.RetinalDetachment != null) CheckBoxRetinalDetachment.Checked = ((bool)abstractRecord.RetinalDetachment);
                if (abstractRecord.VitreousHemorrhage != null) CheckBoxVitreousHemorrhage.Checked = ((bool)abstractRecord.VitreousHemorrhage);
                if (abstractRecord.CornealEdema != null) CheckBoxCornealEdema.Checked = ((bool)abstractRecord.CornealEdema);
                if (abstractRecord.RetainedLensMaterial != null) CheckBoxRetainedLensMaterial.Checked = ((bool)abstractRecord.RetainedLensMaterial);
                if (abstractRecord.IOLDislocation != null) CheckBoxIOLDislocation.Checked = ((bool)abstractRecord.IOLDislocation);
                if (abstractRecord.IOPElevation != null) CheckBoxIOPElevation.Checked = ((bool)abstractRecord.IOPElevation);
                if (abstractRecord.IOPOther != null) CheckBoxIOPOther.Checked = ((bool)abstractRecord.IOPOther);
                if (abstractRecord.NoMedication != null) CheckBoxNoMedication.Checked = ((bool)abstractRecord.NoMedication);
















                if (abstractRecord.RBGender != null) RadioButtonListRBGender.SelectedValue = abstractRecord.RBGender.ToString();
                if (abstractRecord.RBExamcornea != null) RadioButtonListRBExamcornea.SelectedValue = abstractRecord.RBExamcornea.ToString();
                if (abstractRecord.RBExamCorneaKeratopathy != null) RadioButtonListRBExamCorneaKeratopathy.SelectedValue = abstractRecord.RBExamCorneaKeratopathy.ToString();
                if (abstractRecord.RBExamCorneaScars != null) RadioButtonListRBExamCorneaScars.SelectedValue = abstractRecord.RBExamCorneaScars.ToString();
                if (abstractRecord.RBKPs != null) RadioButtonListRBKPs.SelectedValue = abstractRecord.RBKPs.ToString();
                if (abstractRecord.RBKPsAppearance != null) RadioButtonListRBKPsAppearance.SelectedValue = abstractRecord.RBKPsAppearance.ToString();
                if (abstractRecord.RBKPsLocation != null) RadioButtonListRBKPsLocation.SelectedValue = abstractRecord.RBKPsLocation.ToString();
                if (abstractRecord.RBAnteriorFlare != null) RadioButtonListRBAnteriorFlare.SelectedValue = abstractRecord.RBAnteriorFlare.ToString();
                if (abstractRecord.RBAnteriorCells != null) RadioButtonListRBAnteriorCells.SelectedValue = abstractRecord.RBAnteriorCells.ToString();
                if (abstractRecord.RBIris != null) RadioButtonListRBIris.SelectedValue = abstractRecord.RBIris.ToString();
                if (abstractRecord.RBIrisNeovascularization != null) RadioButtonListRBIrisNeovascularization.SelectedValue = abstractRecord.RBIrisNeovascularization.ToString();
                if (abstractRecord.RBPupillaryMembrane != null) RadioButtonListRBPupillaryMembrane.SelectedValue = abstractRecord.RBPupillaryMembrane.ToString();
                if (abstractRecord.RBAnteriorCapsule != null) RadioButtonListRBAnteriorCapsule.SelectedValue = abstractRecord.RBAnteriorCapsule.ToString();
              
                if (abstractRecord.RBCataractGradeNuclear != null) RadioButtonListRBCataractGradeNuclear.SelectedValue = abstractRecord.RBCataractGradeNuclear.ToString();
                if (abstractRecord.RBCataractGradeCortical != null) RadioButtonListRBCataractGradeCortical.SelectedValue = abstractRecord.RBCataractGradeCortical.ToString();
                if (abstractRecord.RBCataractGradePosterior != null) RadioButtonListRBCataractGradePosterior.SelectedValue = abstractRecord.RBCataractGradePosterior.ToString();
                if (abstractRecord.RBVitreousCellsGrade != null) RadioButtonListRBVitreousCellsGrade.SelectedValue = abstractRecord.RBVitreousCellsGrade.ToString();
                if (abstractRecord.RBOpticDiscAbnormalityType != null) RadioButtonListRBOpticDiscAbnormalityType.SelectedValue = abstractRecord.RBOpticDiscAbnormalityType.ToString();
                if (abstractRecord.RBCataractExtraction != null) RadioButtonListRBCataractExtraction.SelectedValue = abstractRecord.RBCataractExtraction.ToString();
                if (abstractRecord.RBIOLImplantMaterial != null) RadioButtonListRBIOLImplantMaterial.SelectedValue = abstractRecord.RBIOLImplantMaterial.ToString();
                if (abstractRecord.RBIOLImplantLocation != null) RadioButtonListRBIOLImplantLocation.SelectedValue = abstractRecord.RBIOLImplantLocation.ToString();
                
                if (abstractRecord.RBOutcomeInflammation != null) RadioButtonListRBOutcomeInflammation.SelectedValue = abstractRecord.RBOutcomeInflammation.ToString();
                if (abstractRecord.RBOutcomeNeovasc != null) RadioButtonListRBOutcomeNeovasc.SelectedValue = abstractRecord.RBOutcomeNeovasc.ToString();
                if (abstractRecord.RBOutcomeInflamDeposits != null) RadioButtonListRBOutcomeInflamDeposits.SelectedValue = abstractRecord.RBOutcomeInflamDeposits.ToString();
                if (abstractRecord.RBOutcomeWellCentered != null) RadioButtonListRBOutcomeWellCentered.SelectedValue = abstractRecord.RBOutcomeWellCentered.ToString();
                if (abstractRecord.RBOutcomeOpticCapture != null) RadioButtonListRBOutcomeOpticCapture.SelectedValue = abstractRecord.RBOutcomeOpticCapture.ToString();
                if (abstractRecord.RBOutcomeVitreousCellGrade != null) RadioButtonListRBOutcomeVitreousCellGrade.SelectedValue = abstractRecord.RBOutcomeVitreousCellGrade.ToString();
                if (abstractRecord.RBOutcomePosteriorDiseaseType != null) RadioButtonListRBOutcomePosteriorDiseaseType.SelectedValue = abstractRecord.RBOutcomePosteriorDiseaseType.ToString();
                if (abstractRecord.RBOutcomePatientSatisfied != null) RadioButtonListRBOutcomePatientSatisfied.SelectedValue = abstractRecord.RBOutcomePatientSatisfied.ToString();

                if (abstractRecord.RBAffectedEye != null) RadioButtonListAffectedEye.SelectedValue = abstractRecord.RBAffectedEye.ToString();

                if (abstractRecord.YearOfBirth != null) DropDownListYearOfBirth.SelectedValue = abstractRecord.YearOfBirth.ToString();
                if (abstractRecord.MonthOfBirth != null) DropDownListMonthOfBirth.SelectedValue = abstractRecord.MonthOfBirth.ToString();
                if (abstractRecord.MonthOfOnsetFirstEpisode != null) DropDownListMonthOfOnsetFirstEpisode.SelectedValue = abstractRecord.MonthOfOnsetFirstEpisode.ToString();
                if (abstractRecord.YearOfOnsetFirstEpisode != null) DropDownListYearOfOnsetFirstEpisode.SelectedValue = abstractRecord.YearOfOnsetFirstEpisode.ToString();
                if (abstractRecord.MonthOfCataractSurgery != null) DropDownListMonthOfCataractSurgery.SelectedValue = abstractRecord.MonthOfCataractSurgery.ToString();
                if (abstractRecord.YearOfCataractSurgery != null) DropDownListYearOfCataractSurgery.SelectedValue = abstractRecord.YearOfCataractSurgery.ToString();
                if (abstractRecord.BCVASnellen != null) DropDownListBCVASnellen.SelectedValue = abstractRecord.BCVASnellen.ToString();
                if (abstractRecord.OutcomeBCVASnellen != null) DropDownListOutcomeBCVASnellen.SelectedValue = abstractRecord.OutcomeBCVASnellen.ToString();

                if (abstractRecord.HistoryGlaucoma != null)
                {
                    RadioButtonHistoryGlaucomaYes.Checked = ((bool)abstractRecord.HistoryGlaucoma);
                    RadioButtonHistoryGlaucomaNo.Checked = !((bool)abstractRecord.HistoryGlaucoma);
                }

                if (abstractRecord.HistoryGlaucomaSurgery != null)
                {
                    RadioButtonHistoryGlaucomaSurgeryYes.Checked = ((bool)abstractRecord.HistoryGlaucomaSurgery);
                    RadioButtonHistoryGlaucomaSurgeryNo.Checked = !((bool)abstractRecord.HistoryGlaucomaSurgery);
                }
                if (abstractRecord.GonioscopyPerformed != null)
                {
                    RadioButtonGonioscopyPerformedYes.Checked = ((bool)abstractRecord.GonioscopyPerformed);
                    RadioButtonGonioscopyPerformedNo.Checked = !((bool)abstractRecord.GonioscopyPerformed);
                }
                if (abstractRecord.VitreousCells != null)
                {
                    RadioButtonVitreousCellsYes.Checked = ((bool)abstractRecord.VitreousCells);
                    RadioButtonVitreousCellsNo.Checked = !((bool)abstractRecord.VitreousCells);
                }
                if (abstractRecord.OpticDiscGlaucomatousChanges != null)
                {
                    RadioButtonOpticDiscGlaucomatousChangesYes.Checked = ((bool)abstractRecord.OpticDiscGlaucomatousChanges);
                    RadioButtonOpticDiscGlaucomatousChangesNo.Checked = !((bool)abstractRecord.OpticDiscGlaucomatousChanges);
                }
                if (abstractRecord.OpticDiscAbnormalities != null)
                {
                    RadioButtonOpticDiscAbnormalitiesYes.Checked = ((bool)abstractRecord.OpticDiscAbnormalities);
                    RadioButtonOpticDiscAbnormalitiesNo.Checked = !((bool)abstractRecord.OpticDiscAbnormalities);
                }
                if (abstractRecord.ExamDilatedFundus != null)
                {
                    RadioButtonExamDilatedFundusYes.Checked = ((bool)abstractRecord.ExamDilatedFundus);
                    RadioButtonExamDilatedFundusNo.Checked = !((bool)abstractRecord.ExamDilatedFundus);
                }
                if (abstractRecord.ExamFluorescein != null)
                {
                    RadioButtonExamFluoresceinYes.Checked = ((bool)abstractRecord.ExamFluorescein);
                    RadioButtonExamFluoresceinNo.Checked = !((bool)abstractRecord.ExamFluorescein);
                }
                if (abstractRecord.ExamOCT != null)
                {
                    RadioButtonExamOCTYes.Checked = ((bool)abstractRecord.ExamOCT);
                    RadioButtonExamOCTNo.Checked = !((bool)abstractRecord.ExamOCT);
                }
                if (abstractRecord.ExamBscan != null)
                {
                    RadioButtonExamBscanYes.Checked = ((bool)abstractRecord.ExamBscan);
                    RadioButtonExamBscanNo.Checked = !((bool)abstractRecord.ExamBscan);
                }
                if (abstractRecord.PreopCataractMainCause != null)
                {
                    RadioButtonPreopCataractMainCauseYes.Checked = ((bool)abstractRecord.PreopCataractMainCause);
                    RadioButtonPreopCataractMainCauseNo.Checked = !((bool)abstractRecord.PreopCataractMainCause);
                }
                if (abstractRecord.PreopOcularCoMorbilities != null)
                {
                    RadioButtonPreopOcularCoMorbilitiesYes.Checked = ((bool)abstractRecord.PreopOcularCoMorbilities);
                    RadioButtonPreopOcularCoMorbilitiesNo.Checked = !((bool)abstractRecord.PreopOcularCoMorbilities);
                }
                if (abstractRecord.MgntConsent != null)
                {
                    RadioButtonMgntConsentYes.Checked = ((bool)abstractRecord.MgntConsent);
                    RadioButtonMgntConsentNo.Checked = !((bool)abstractRecord.MgntConsent);
                }
                if (abstractRecord.MgntPeriopCS != null)
                {
                    RadioButtonMgntPeriopCSYes.Checked = ((bool)abstractRecord.MgntPeriopCS);
                    RadioButtonMgntPeriopCSNo.Checked = !((bool)abstractRecord.MgntPeriopCS);
                }
                if (abstractRecord.MgntPeriopCSTopical != null)
                {
                    RadioButtonMgntPeriopCSTopicalYes.Checked = ((bool)abstractRecord.MgntPeriopCSTopical);
                    RadioButtonMgntPeriopCSTopicalNo.Checked = !((bool)abstractRecord.MgntPeriopCSTopical);
                }
                if (abstractRecord.MgntPeriopCSInjection != null)
                {
                    RadioButtonMgntPeriopCSInjectionYes.Checked = ((bool)abstractRecord.MgntPeriopCSInjection);
                    RadioButtonMgntPeriopCSInjectionNo.Checked = !((bool)abstractRecord.MgntPeriopCSInjection);
                }
                if (abstractRecord.MgntPeriopCSSystemic != null)
                {
                    RadioButtonMgntPeriopCSSystemicYes.Checked = ((bool)abstractRecord.MgntPeriopCSSystemic);
                    RadioButtonMgntPeriopCSSystemicNo.Checked = !((bool)abstractRecord.MgntPeriopCSSystemic);
                }
                if (abstractRecord.MgntPeriopCSIntravitreal != null)
                {
                    RadioButtonMgntPeriopCSIntravitrealYes.Checked = ((bool)abstractRecord.MgntPeriopCSIntravitreal);
                    RadioButtonMgntPeriopCSIntravitrealNo.Checked = !((bool)abstractRecord.MgntPeriopCSIntravitreal);
                }
                if (abstractRecord.MgntPeriopCyclo != null)
                {
                    RadioButtonMgntPeriopCycloYes.Checked = ((bool)abstractRecord.MgntPeriopCyclo);
                    RadioButtonMgntPeriopCycloNo.Checked = !((bool)abstractRecord.MgntPeriopCyclo);
                }
                if (abstractRecord.SurgCapsularStaining != null)
                {
                    RadioButtonSurgCapsularStainingYes.Checked = ((bool)abstractRecord.SurgCapsularStaining);
                    RadioButtonSurgCapsularStainingNo.Checked = !((bool)abstractRecord.SurgCapsularStaining);
                }
                if (abstractRecord.SurgIrisHooks != null)
                {
                    RadioButtonSurgIrisHooksYes.Checked = ((bool)abstractRecord.SurgIrisHooks);
                    RadioButtonSurgIrisHooksNo.Checked = !((bool)abstractRecord.SurgIrisHooks);
                }
                if (abstractRecord.SurgIPupillaryExpanders != null)
                {
                    RadioButtonSurgIPupillaryExpandersYes.Checked = ((bool)abstractRecord.SurgIPupillaryExpanders);
                    RadioButtonSurgIPupillaryExpandersNo.Checked = !((bool)abstractRecord.SurgIPupillaryExpanders);
                }
                if (abstractRecord.SurgPosteriorSynechiae != null)
                {
                    RadioButtonSurgPosteriorSynechiaeYes.Checked = ((bool)abstractRecord.SurgPosteriorSynechiae);
                    RadioButtonSurgPosteriorSynechiaeNo.Checked = !((bool)abstractRecord.SurgPosteriorSynechiae);
                }
                if (abstractRecord.SurgPupillarymembrane != null)
                {
                    RadioButtonSurgPupillarymembraneYes.Checked = ((bool)abstractRecord.SurgPupillarymembrane);
                    RadioButtonSurgPupillarymembraneNo.Checked = !((bool)abstractRecord.SurgPupillarymembrane);
                }
                if (abstractRecord.IOLImplantation != null)
                {
                    RadioButtonIOLImplantationYes.Checked = ((bool)abstractRecord.IOLImplantation);
                    RadioButtonIOLImplantationNo.Checked = !((bool)abstractRecord.IOLImplantation);
                }
                if (abstractRecord.IOLCapsulotomy != null)
                {
                    RadioButtonIOLCapsulotomyYes.Checked = ((bool)abstractRecord.IOLCapsulotomy);
                    RadioButtonIOLCapsulotomyNo.Checked = !((bool)abstractRecord.IOLCapsulotomy);
                }
                if (abstractRecord.CapsularTensionRing != null)
                {
                    RadioButtonCapsularTensionRingYes.Checked = ((bool)abstractRecord.CapsularTensionRing);
                    RadioButtonCapsularTensionRingNo.Checked = !((bool)abstractRecord.CapsularTensionRing);
                }
                if (abstractRecord.IOLComplications != null)
                {
                    RadioButtonIOLComplicationsYes.Checked = ((bool)abstractRecord.IOLComplications);
                    RadioButtonIOLComplicationsNo.Checked = !((bool)abstractRecord.IOLComplications);
                }
                if (abstractRecord.OutcomeCapsularPhimosis != null)
                {
                    RadioButtonOutcomeCapsularPhimosisYes.Checked = ((bool)abstractRecord.OutcomeCapsularPhimosis);
                    RadioButtonOutcomeCapsularPhimosisNo.Checked = !((bool)abstractRecord.OutcomeCapsularPhimosis);
                }
                if (abstractRecord.OutcomeEntireIOLinBag != null)
                {
                    RadioButtonOutcomeEntireIOLinBagYes.Checked = ((bool)abstractRecord.OutcomeEntireIOLinBag);
                    RadioButtonOutcomeEntireIOLinBagNo.Checked = !((bool)abstractRecord.OutcomeEntireIOLinBag);
                }
                if (abstractRecord.OutcomeOnsetGlaucoma != null)
                {
                    RadioButtonOutcomeOnsetGlaucomaYes.Checked = ((bool)abstractRecord.OutcomeOnsetGlaucoma);
                    RadioButtonOutcomeOnsetGlaucomaNo.Checked = !((bool)abstractRecord.OutcomeOnsetGlaucoma);
                }
                if (abstractRecord.OutcomeVitreousCells != null)
                {
                    RadioButtonOutcomeVitreousCellsYes.Checked = ((bool)abstractRecord.OutcomeVitreousCells);
                    RadioButtonOutcomeVitreousCellsNo.Checked = !((bool)abstractRecord.OutcomeVitreousCells);
                }
                if (abstractRecord.OutcomeDilatedFundusExamPerformed != null)
                {
                    RadioButtonOutcomeDilatedFundusExamPerformedYes.Checked = ((bool)abstractRecord.OutcomeDilatedFundusExamPerformed);
                    RadioButtonOutcomeDilatedFundusExamPerformedNo.Checked = !((bool)abstractRecord.OutcomeDilatedFundusExamPerformed);
                }
                if (abstractRecord.OutcomeFluoresceinPerformed != null)
                {
                    RadioButtonOutcomeFluoresceinPerformedYes.Checked = ((bool)abstractRecord.OutcomeFluoresceinPerformed);
                    RadioButtonOutcomeFluoresceinPerformedNo.Checked = !((bool)abstractRecord.OutcomeFluoresceinPerformed);
                }
                if (abstractRecord.OutcomeOCTPerformed != null)
                {
                    RadioButtonOutcomeOCTPerformedYes.Checked = ((bool)abstractRecord.OutcomeOCTPerformed);
                    RadioButtonOutcomeOCTPerformedNo.Checked = !((bool)abstractRecord.OutcomeOCTPerformed);
                }
                if (abstractRecord.OutcomeCMEPresent != null)
                {
                    RadioButtonOutcomeCMEPresentYes.Checked = ((bool)abstractRecord.OutcomeCMEPresent);
                    RadioButtonOutcomeCMEPresentNo.Checked = !((bool)abstractRecord.OutcomeCMEPresent);
                }
                if (abstractRecord.OutcomeEpiretinalMembrane != null)
                {
                    RadioButtonOutcomeEpiretinalMembraneYes.Checked = ((bool)abstractRecord.OutcomeEpiretinalMembrane);
                    RadioButtonOutcomeEpiretinalMembraneNo.Checked = !((bool)abstractRecord.OutcomeEpiretinalMembrane);
                }
                if (abstractRecord.OutcomePosteriorDiseaseActive != null)
                {
                    RadioButtonOutcomePosteriorDiseaseActiveYes.Checked = ((bool)abstractRecord.OutcomePosteriorDiseaseActive);
                    RadioButtonOutcomePosteriorDiseaseActiveNo.Checked = !((bool)abstractRecord.OutcomePosteriorDiseaseActive);
                }
                if (abstractRecord.OutcomeAdditionalSurgery != null)
                {
                    RadioButtonOutcomeAdditionalSurgeryYes.Checked = ((bool)abstractRecord.OutcomeAdditionalSurgery);
                    RadioButtonOutcomeAdditionalSurgeryNo.Checked = !((bool)abstractRecord.OutcomeAdditionalSurgery);
                }



                if (abstractRecord.DiseaseDurationMonths != null) TextBoxDiseaseDurationMonths.Text = abstractRecord.DiseaseDurationMonths.ToString();
                if (abstractRecord.TherapyToDateImmuName != null) TextBoxTherapyToDateImmuName.Text = abstractRecord.TherapyToDateImmuName.ToString();
                if (abstractRecord.RBIrisClockHours != null) TextBoxRBIrisClockHours.Text = abstractRecord.RBIrisClockHours.ToString();
                if (abstractRecord.IOP != null) TextBoxIOP.Text = abstractRecord.IOP.ToString();
                if (abstractRecord.RBOpticDiscAbnormalityOther != null) TextBoxRBOpticDiscAbnormalityOther.Text = abstractRecord.RBOpticDiscAbnormalityOther.ToString();
                if (abstractRecord.PreopComorbidOtherText != null) TextBoxPreopComorbidOtherText.Text = abstractRecord.PreopComorbidOtherText.ToString();
                if (abstractRecord.IOLComplicationOtherText != null) TextBoxIOLComplicationOtherText.Text = abstractRecord.IOLComplicationOtherText.ToString();
                if (abstractRecord.OutcomeMonthsAssessed != null) TextBoxOutcomeMonthsAssessed.Text = abstractRecord.OutcomeMonthsAssessed.ToString();
                if (abstractRecord.OutcomePosteriorSynechiaeClockHours != null) TextBoxOutcomePosteriorSynechiaeClockHours.Text = abstractRecord.OutcomePosteriorSynechiaeClockHours.ToString();
                if (abstractRecord.OutcomeIOP != null) TextBoxOutcomeIOP.Text = abstractRecord.OutcomeIOP.ToString();
                if (abstractRecord.OutcomePosteriorDiseaseOther != null) TextBoxOutcomePosteriorDiseaseOther.Text = abstractRecord.OutcomePosteriorDiseaseOther.ToString();
                if (abstractRecord.OutcomeAdditionalOtherText != null) TextBoxOutcomeAdditionalOtherText.Text = abstractRecord.OutcomeAdditionalOtherText.ToString();








            }
        }
    }

    public void savedata()
    {
        using (NetHealthPIMEntities pim = new NetHealthPIMEntities())
        {
            int ModuleID = (int)Session[Constants.SESSION_WORKINGMODULEID];
            int cycleID = ctx.ActiveModuleCycleID;
            ParticipantModuleCycle cycle = pim.ParticipantModuleCycle.First(c => c.ParticipantModuleCycleID == cycleID);
            string recordIdentifier = HiddenFieldRecordIdentifier.Value;
            using (TransactionScope transaction = new TransactionScope())
            {
                ChartAbstractionUveitis abstractRecord = (from c in pim.ChartAbstractionUveitis
                                                        where c.ParticipantModuleCycle.ParticipantModuleCycleID == cycleID & c.RecordIdentifier == recordIdentifier
                                                          select c).First<ChartAbstractionUveitis>();

                abstractRecord.PresentingSymptomDecreasedVision = CheckBoxPresentingSymptomDecreasedVision.Checked;
                abstractRecord.PresentingSymptomGlare = CheckBoxPresentingSymptomGlare.Checked;
                abstractRecord.PresentingSymptomDailyLiving = CheckBoxPresentingSymptomDailyLiving.Checked;
                abstractRecord.PresentingSymptomVisualizePosterior = CheckBoxPresentingSymptomVisualizePosterior.Checked;
                abstractRecord.PresentingSymptomOther = CheckBoxPresentingSymptomOther.Checked;
                abstractRecord.UveitisAnterior = CheckBoxUveitisAnterior.Checked;
                abstractRecord.UveitisIntermediate = CheckBoxUveitisIntermediate.Checked;
                abstractRecord.UveitisPosterior = CheckBoxUveitisPosterior.Checked;
                abstractRecord.UveitisPanuveitis = CheckBoxUveitisPanuveitis.Checked;
                abstractRecord.UveitisRecurrent = CheckBoxUveitisRecurrent.Checked;
                abstractRecord.UveitisCourse = CheckBoxUveitisCourse.Checked;
                abstractRecord.EtiologyFuchs = CheckBoxEtiologyFuchs.Checked;
                abstractRecord.EtiologyHLA = CheckBoxEtiologyHLA.Checked;
                abstractRecord.EtiologySarcoidosis = CheckBoxEtiologySarcoidosis.Checked;
                abstractRecord.EtiologyPars = CheckBoxEtiologyPars.Checked;
                abstractRecord.EtiologyScleritisRA = CheckBoxEtiologyScleritisRA.Checked;
                abstractRecord.EtiologyCMVRetinitis = CheckBoxEtiologyCMVRetinitis.Checked;
                abstractRecord.EtiologyJIA = CheckBoxEtiologyJIA.Checked;
                abstractRecord.EtiologyIdiopathic = CheckBoxEtiologyIdiopathic.Checked;
                abstractRecord.EtiologyOther = CheckBoxEtiologyOther.Checked;
                abstractRecord.TherapyToDateCorticosteroids = CheckBoxTherapyToDateCorticosteroids.Checked;
                abstractRecord.TherapyToDateCSTopical = CheckBoxTherapyToDateCSTopical.Checked;
                abstractRecord.TherapyToDateCSPeriocular = CheckBoxTherapyToDateCSPeriocular.Checked;
                abstractRecord.TherapyToDateCSSystemic = CheckBoxTherapyToDateCSSystemic.Checked;
                abstractRecord.TherapyToDateCSIntravitreal = CheckBoxTherapyToDateCSIntravitreal.Checked;
                abstractRecord.TherapyToDateImmu = CheckBoxTherapyToDateImmu.Checked;
                abstractRecord.IrisBombe = CheckBoxIrisBombe.Checked;
                abstractRecord.PreopComorbidEpiretinal = CheckBoxPreopComorbidEpiretinal.Checked;
                abstractRecord.PreopCormorbidCystoid = CheckBoxPreopCormorbidCystoid.Checked;
                abstractRecord.PreopCormorbidChoroidal = CheckBoxPreopCormorbidChoroidal.Checked;
                abstractRecord.PreopCormorbidNeovasc = CheckBoxPreopCormorbidNeovasc.Checked;
                abstractRecord.PreopCormorbidTears = CheckBoxPreopCormorbidTears.Checked;
                abstractRecord.PreopCormorbidGlaucoma = CheckBoxPreopCormorbidGlaucoma.Checked;
                abstractRecord.PreopCormorbidKerotopathy = CheckBoxPreopCormorbidKerotopathy.Checked;
                abstractRecord.PreopCormorbidOpacification = CheckBoxPreopCormorbidOpacification.Checked;
                abstractRecord.PreopComorbidOther = CheckBoxPreopCormorbidOther.Checked;
                abstractRecord.OutcomeMedCS = CheckBoxOutcomeMedCS.Checked;
                abstractRecord.OutcomeMedCSTopical = CheckBoxOutcomeMedCSTopical.Checked;
                abstractRecord.OutcomeMedCSPeriocular = CheckBoxOutcomeMedCSPeriocular.Checked;
                abstractRecord.OutcomeMedCSSystemic = CheckBoxOutcomeMedCSSystemic.Checked;
                abstractRecord.OutcomeMedCSIntravitreal = CheckBoxOutcomeMedCSIntravitreal.Checked;
                abstractRecord.OutcomeMedCSImmu = CheckBoxOutcomeMedCSImmu.Checked;
                abstractRecord.RBOutcomeAdditionalYag = CheckBoxRBOutcomeAdditionalYag.Checked;
                abstractRecord.RBOutcomeAdditionalIOL = CheckBoxRBOutcomeAdditionalIOL.Checked;
                abstractRecord.RBOutcomeAdditionalAspiration = CheckBoxRBOutcomeAdditionalAspiration.Checked;
                abstractRecord.RBOutcomeAdditionalLaser = CheckBoxRBOutcomeAdditionalLaser.Checked;
                abstractRecord.RBOutcomeAdditionalRepairDetachment = CheckBoxRBOutcomeAdditionalRepairDetachment.Checked;
                abstractRecord.RBOutcomeAdditionalParsPlana = CheckBoxRBOutcomeAdditionalParsPlana.Checked;
                abstractRecord.RBOutcomeAdditionalOther = CheckBoxRBOutcomeAdditionalOther.Checked;



                abstractRecord.Nuclear =  CheckBoxNuclear.Checked;
                abstractRecord.Cortical =  Cortical.Checked;
                abstractRecord.Posterior =  Posterior.Checked;
                abstractRecord.ProlifilativeDiabeticRetinopathy =  CheckBoxProlifilativeDiabeticRetinopathy.Checked;
                abstractRecord.NonProlifilativeDiabeticRetinopathy = CheckBoxNonProlifilativeDiabeticRetinopathy.Checked;
                abstractRecord.UnplannedAnteriorVitrectomy =CheckBoxUnplannedAnteriorVitrectomy.Checked;
                abstractRecord.SuprachorodialHemorrhage =  CheckBoxSuprachorodialHemorrhage.Checked;
                abstractRecord.Endophthalmitis = CheckBoxEndophthalmitis.Checked;
                abstractRecord.RetinalDetachment =  CheckBoxRetinalDetachment.Checked;
                abstractRecord.VitreousHemorrhage =  CheckBoxVitreousHemorrhage.Checked;
                abstractRecord.CornealEdema = CheckBoxCornealEdema.Checked;
                abstractRecord.RetainedLensMaterial = CheckBoxRetainedLensMaterial.Checked;
                abstractRecord.IOLDislocation =  CheckBoxIOLDislocation.Checked;
                abstractRecord.IOPElevation =  CheckBoxIOPElevation.Checked;
                abstractRecord.IOPOther = CheckBoxIOPOther.Checked;
                abstractRecord.NoMedication = CheckBoxNoMedication.Checked;






                if (RadioButtonListRBGender.SelectedIndex != -1) abstractRecord.RBGender = Convert.ToInt32(RadioButtonListRBGender.SelectedValue);
                else abstractRecord.RBGender = 0;

                if (RadioButtonListAffectedEye.SelectedIndex != -1) abstractRecord.RBAffectedEye = Convert.ToInt32(RadioButtonListAffectedEye.SelectedValue);
                else abstractRecord.RBAffectedEye = 0;


                if (RadioButtonListRBExamcornea.SelectedIndex != -1) abstractRecord.RBExamcornea = Convert.ToInt32(RadioButtonListRBExamcornea.SelectedValue);
                else abstractRecord.RBExamcornea = 0;
                if (RadioButtonListRBExamCorneaKeratopathy.SelectedIndex != -1) abstractRecord.RBExamCorneaKeratopathy = Convert.ToInt32(RadioButtonListRBExamCorneaKeratopathy.SelectedValue);
                else abstractRecord.RBExamCorneaKeratopathy = 0;
                if (RadioButtonListRBExamCorneaScars.SelectedIndex != -1) abstractRecord.RBExamCorneaScars = Convert.ToInt32(RadioButtonListRBExamCorneaScars.SelectedValue);
                else abstractRecord.RBExamCorneaScars = 0;
                if (RadioButtonListRBKPs.SelectedIndex != -1) abstractRecord.RBKPs = Convert.ToInt32(RadioButtonListRBKPs.SelectedValue);
                else abstractRecord.RBKPs = 0;
                if (RadioButtonListRBKPsAppearance.SelectedIndex != -1) abstractRecord.RBKPsAppearance = Convert.ToInt32(RadioButtonListRBKPsAppearance.SelectedValue);
                else abstractRecord.RBKPsAppearance = 0;
                if (RadioButtonListRBKPsLocation.SelectedIndex != -1) abstractRecord.RBKPsLocation = Convert.ToInt32(RadioButtonListRBKPsLocation.SelectedValue);
                else abstractRecord.RBKPsLocation = 0;
                if (RadioButtonListRBAnteriorFlare.SelectedIndex != -1) abstractRecord.RBAnteriorFlare = Convert.ToInt32(RadioButtonListRBAnteriorFlare.SelectedValue);
                else abstractRecord.RBAnteriorFlare = 0;
                if (RadioButtonListRBAnteriorCells.SelectedIndex != -1) abstractRecord.RBAnteriorCells = Convert.ToInt32(RadioButtonListRBAnteriorCells.SelectedValue);
                else abstractRecord.RBAnteriorCells = 0;
                if (RadioButtonListRBIris.SelectedIndex != -1) abstractRecord.RBIris = Convert.ToInt32(RadioButtonListRBIris.SelectedValue);
                else abstractRecord.RBIris = 0;
                if (RadioButtonListRBIrisNeovascularization.SelectedIndex != -1) abstractRecord.RBIrisNeovascularization = Convert.ToInt32(RadioButtonListRBIrisNeovascularization.SelectedValue);
                else abstractRecord.RBIrisNeovascularization = 0;
                if (RadioButtonListRBPupillaryMembrane.SelectedIndex != -1) abstractRecord.RBPupillaryMembrane = Convert.ToInt32(RadioButtonListRBPupillaryMembrane.SelectedValue);
                else abstractRecord.RBPupillaryMembrane = 0;
                if (RadioButtonListRBAnteriorCapsule.SelectedIndex != -1) abstractRecord.RBAnteriorCapsule = Convert.ToInt32(RadioButtonListRBAnteriorCapsule.SelectedValue);
                else abstractRecord.RBAnteriorCapsule = 0;
              
                if (RadioButtonListRBCataractGradeNuclear.SelectedIndex != -1) abstractRecord.RBCataractGradeNuclear = Convert.ToInt32(RadioButtonListRBCataractGradeNuclear.SelectedValue);
                else abstractRecord.RBCataractGradeNuclear = 0;
                if (RadioButtonListRBCataractGradeCortical.SelectedIndex != -1) abstractRecord.RBCataractGradeCortical = Convert.ToInt32(RadioButtonListRBCataractGradeCortical.SelectedValue);
                else abstractRecord.RBCataractGradeCortical = 0;
                if (RadioButtonListRBCataractGradePosterior.SelectedIndex != -1) abstractRecord.RBCataractGradePosterior = Convert.ToInt32(RadioButtonListRBCataractGradePosterior.SelectedValue);
                else abstractRecord.RBCataractGradePosterior = 0;
                if (RadioButtonListRBVitreousCellsGrade.SelectedIndex != -1) abstractRecord.RBVitreousCellsGrade = Convert.ToInt32(RadioButtonListRBVitreousCellsGrade.SelectedValue);
                else abstractRecord.RBVitreousCellsGrade = 0;
                if (RadioButtonListRBOpticDiscAbnormalityType.SelectedIndex != -1) abstractRecord.RBOpticDiscAbnormalityType = Convert.ToInt32(RadioButtonListRBOpticDiscAbnormalityType.SelectedValue);
                else abstractRecord.RBOpticDiscAbnormalityType = 0;
                if (RadioButtonListRBCataractExtraction.SelectedIndex != -1) abstractRecord.RBCataractExtraction = Convert.ToInt32(RadioButtonListRBCataractExtraction.SelectedValue);
                else abstractRecord.RBCataractExtraction = 0;
                if (RadioButtonListRBIOLImplantMaterial.SelectedIndex != -1) abstractRecord.RBIOLImplantMaterial = Convert.ToInt32(RadioButtonListRBIOLImplantMaterial.SelectedValue);
                else abstractRecord.RBIOLImplantMaterial = 0;
                if (RadioButtonListRBIOLImplantLocation.SelectedIndex != -1) abstractRecord.RBIOLImplantLocation = Convert.ToInt32(RadioButtonListRBIOLImplantLocation.SelectedValue);
                else abstractRecord.RBIOLImplantLocation = 0;
               
                if (RadioButtonListRBOutcomeInflammation.SelectedIndex != -1) abstractRecord.RBOutcomeInflammation = Convert.ToInt32(RadioButtonListRBOutcomeInflammation.SelectedValue);
                else abstractRecord.RBOutcomeInflammation = 0;
                if (RadioButtonListRBOutcomeNeovasc.SelectedIndex != -1) abstractRecord.RBOutcomeNeovasc = Convert.ToInt32(RadioButtonListRBOutcomeNeovasc.SelectedValue);
                else abstractRecord.RBOutcomeNeovasc = 0;
                if (RadioButtonListRBOutcomeInflamDeposits.SelectedIndex != -1) abstractRecord.RBOutcomeInflamDeposits = Convert.ToInt32(RadioButtonListRBOutcomeInflamDeposits.SelectedValue);
                else abstractRecord.RBOutcomeInflamDeposits = 0;
                if (RadioButtonListRBOutcomeWellCentered.SelectedIndex != -1) abstractRecord.RBOutcomeWellCentered = Convert.ToInt32(RadioButtonListRBOutcomeWellCentered.SelectedValue);
                else abstractRecord.RBOutcomeWellCentered = 0;
                if (RadioButtonListRBOutcomeOpticCapture.SelectedIndex != -1) abstractRecord.RBOutcomeOpticCapture = Convert.ToInt32(RadioButtonListRBOutcomeOpticCapture.SelectedValue);
                else abstractRecord.RBOutcomeOpticCapture = 0;
                if (RadioButtonListRBOutcomeVitreousCellGrade.SelectedIndex != -1) abstractRecord.RBOutcomeVitreousCellGrade = Convert.ToInt32(RadioButtonListRBOutcomeVitreousCellGrade.SelectedValue);
                else abstractRecord.RBOutcomeVitreousCellGrade = 0;
                if (RadioButtonListRBOutcomePosteriorDiseaseType.SelectedIndex != -1) abstractRecord.RBOutcomePosteriorDiseaseType = Convert.ToInt32(RadioButtonListRBOutcomePosteriorDiseaseType.SelectedValue);
                else abstractRecord.RBOutcomePosteriorDiseaseType = 0;
                if (RadioButtonListRBOutcomePatientSatisfied.SelectedIndex != -1) abstractRecord.RBOutcomePatientSatisfied = Convert.ToInt32(RadioButtonListRBOutcomePatientSatisfied.SelectedValue);
                else abstractRecord.RBOutcomePatientSatisfied = 0;



                if (DropDownListYearOfBirth.SelectedIndex > 0) abstractRecord.YearOfBirth = Convert.ToInt32(DropDownListYearOfBirth.SelectedValue);
                else abstractRecord.YearOfBirth = 0;
                if (DropDownListMonthOfBirth.SelectedIndex > 0) abstractRecord.MonthOfBirth = Convert.ToInt32(DropDownListMonthOfBirth.SelectedValue);
                else abstractRecord.MonthOfBirth = 0;
                if (DropDownListMonthOfOnsetFirstEpisode.SelectedIndex > 0) abstractRecord.MonthOfOnsetFirstEpisode = Convert.ToInt32(DropDownListMonthOfOnsetFirstEpisode.SelectedValue);
                else abstractRecord.MonthOfOnsetFirstEpisode = 0;
                if (DropDownListYearOfOnsetFirstEpisode.SelectedIndex > 0) abstractRecord.YearOfOnsetFirstEpisode = Convert.ToInt32(DropDownListYearOfOnsetFirstEpisode.SelectedValue);
                else abstractRecord.YearOfOnsetFirstEpisode = 0;
                if (DropDownListMonthOfCataractSurgery.SelectedIndex > 0) abstractRecord.MonthOfCataractSurgery = Convert.ToInt32(DropDownListMonthOfCataractSurgery.SelectedValue);
                else abstractRecord.MonthOfCataractSurgery = 0;
                if (DropDownListYearOfCataractSurgery.SelectedIndex > 0) abstractRecord.YearOfCataractSurgery = Convert.ToInt32(DropDownListYearOfCataractSurgery.SelectedValue);
                else abstractRecord.YearOfCataractSurgery = 0;
                if (DropDownListBCVASnellen.SelectedIndex > 0) abstractRecord.BCVASnellen = Convert.ToInt32(DropDownListBCVASnellen.SelectedValue);
                else abstractRecord.BCVASnellen = 0;
                if (DropDownListOutcomeBCVASnellen.SelectedIndex > 0) abstractRecord.OutcomeBCVASnellen = Convert.ToInt32(DropDownListOutcomeBCVASnellen.SelectedValue);
                else abstractRecord.OutcomeBCVASnellen = 0;



                if (RadioButtonHistoryGlaucomaYes.Checked) abstractRecord.HistoryGlaucoma = true;
                if (RadioButtonHistoryGlaucomaNo.Checked) abstractRecord.HistoryGlaucoma = false;
                if (RadioButtonHistoryGlaucomaSurgeryYes.Checked) abstractRecord.HistoryGlaucomaSurgery = true;
                if (RadioButtonHistoryGlaucomaSurgeryNo.Checked) abstractRecord.HistoryGlaucomaSurgery = false;
                if (RadioButtonGonioscopyPerformedYes.Checked) abstractRecord.GonioscopyPerformed = true;
                if (RadioButtonGonioscopyPerformedNo.Checked) abstractRecord.GonioscopyPerformed = false;
                if (RadioButtonVitreousCellsYes.Checked) abstractRecord.VitreousCells = true;
                if (RadioButtonVitreousCellsNo.Checked) abstractRecord.VitreousCells = false;
                if (RadioButtonOpticDiscGlaucomatousChangesYes.Checked) abstractRecord.OpticDiscGlaucomatousChanges = true;
                if (RadioButtonOpticDiscGlaucomatousChangesNo.Checked) abstractRecord.OpticDiscGlaucomatousChanges = false;
                if (RadioButtonOpticDiscAbnormalitiesYes.Checked) abstractRecord.OpticDiscAbnormalities = true;
                if (RadioButtonOpticDiscAbnormalitiesNo.Checked) abstractRecord.OpticDiscAbnormalities = false;
                if (RadioButtonExamDilatedFundusYes.Checked) abstractRecord.ExamDilatedFundus = true;
                if (RadioButtonExamDilatedFundusNo.Checked) abstractRecord.ExamDilatedFundus = false;
                if (RadioButtonExamFluoresceinYes.Checked) abstractRecord.ExamFluorescein = true;
                if (RadioButtonExamFluoresceinNo.Checked) abstractRecord.ExamFluorescein = false;
                if (RadioButtonExamOCTYes.Checked) abstractRecord.ExamOCT = true;
                if (RadioButtonExamOCTNo.Checked) abstractRecord.ExamOCT = false;
                if (RadioButtonExamBscanYes.Checked) abstractRecord.ExamBscan = true;
                if (RadioButtonExamBscanNo.Checked) abstractRecord.ExamBscan = false;
                if (RadioButtonPreopCataractMainCauseYes.Checked) abstractRecord.PreopCataractMainCause = true;
                if (RadioButtonPreopCataractMainCauseNo.Checked) abstractRecord.PreopCataractMainCause = false;
                if (RadioButtonPreopOcularCoMorbilitiesYes.Checked) abstractRecord.PreopOcularCoMorbilities = true;
                if (RadioButtonPreopOcularCoMorbilitiesNo.Checked) abstractRecord.PreopOcularCoMorbilities = false;
                if (RadioButtonMgntConsentYes.Checked) abstractRecord.MgntConsent = true;
                if (RadioButtonMgntConsentNo.Checked) abstractRecord.MgntConsent = false;
                if (RadioButtonMgntPeriopCSYes.Checked) abstractRecord.MgntPeriopCS = true;
                if (RadioButtonMgntPeriopCSNo.Checked) abstractRecord.MgntPeriopCS = false;
                if (RadioButtonMgntPeriopCSTopicalYes.Checked) abstractRecord.MgntPeriopCSTopical = true;
                if (RadioButtonMgntPeriopCSTopicalNo.Checked) abstractRecord.MgntPeriopCSTopical = false;
                if (RadioButtonMgntPeriopCSInjectionYes.Checked) abstractRecord.MgntPeriopCSInjection = true;
                if (RadioButtonMgntPeriopCSInjectionNo.Checked) abstractRecord.MgntPeriopCSInjection = false;
                if (RadioButtonMgntPeriopCSSystemicYes.Checked) abstractRecord.MgntPeriopCSSystemic = true;
                if (RadioButtonMgntPeriopCSSystemicNo.Checked) abstractRecord.MgntPeriopCSSystemic = false;
                if (RadioButtonMgntPeriopCSIntravitrealYes.Checked) abstractRecord.MgntPeriopCSIntravitreal = true;
                if (RadioButtonMgntPeriopCSIntravitrealNo.Checked) abstractRecord.MgntPeriopCSIntravitreal = false;
                if (RadioButtonMgntPeriopCycloYes.Checked) abstractRecord.MgntPeriopCyclo = true;
                if (RadioButtonMgntPeriopCycloNo.Checked) abstractRecord.MgntPeriopCyclo = false;
                if (RadioButtonSurgCapsularStainingYes.Checked) abstractRecord.SurgCapsularStaining = true;
                if (RadioButtonSurgCapsularStainingNo.Checked) abstractRecord.SurgCapsularStaining = false;
                if (RadioButtonSurgIrisHooksYes.Checked) abstractRecord.SurgIrisHooks = true;
                if (RadioButtonSurgIrisHooksNo.Checked) abstractRecord.SurgIrisHooks = false;
                if (RadioButtonSurgIPupillaryExpandersYes.Checked) abstractRecord.SurgIPupillaryExpanders = true;
                if (RadioButtonSurgIPupillaryExpandersNo.Checked) abstractRecord.SurgIPupillaryExpanders = false;
                if (RadioButtonSurgPosteriorSynechiaeYes.Checked) abstractRecord.SurgPosteriorSynechiae = true;
                if (RadioButtonSurgPosteriorSynechiaeNo.Checked) abstractRecord.SurgPosteriorSynechiae = false;
                if (RadioButtonSurgPupillarymembraneYes.Checked) abstractRecord.SurgPupillarymembrane = true;
                if (RadioButtonSurgPupillarymembraneNo.Checked) abstractRecord.SurgPupillarymembrane = false;
                if (RadioButtonIOLImplantationYes.Checked) abstractRecord.IOLImplantation = true;
                if (RadioButtonIOLImplantationNo.Checked) abstractRecord.IOLImplantation = false;
                if (RadioButtonIOLCapsulotomyYes.Checked) abstractRecord.IOLCapsulotomy = true;
                if (RadioButtonIOLCapsulotomyNo.Checked) abstractRecord.IOLCapsulotomy = false;
                if (RadioButtonCapsularTensionRingYes.Checked) abstractRecord.CapsularTensionRing = true;
                if (RadioButtonCapsularTensionRingNo.Checked) abstractRecord.CapsularTensionRing = false;
                if (RadioButtonIOLComplicationsYes.Checked) abstractRecord.IOLComplications = true;
                if (RadioButtonIOLComplicationsNo.Checked) abstractRecord.IOLComplications = false;
                if (RadioButtonOutcomeCapsularPhimosisYes.Checked) abstractRecord.OutcomeCapsularPhimosis = true;
                if (RadioButtonOutcomeCapsularPhimosisNo.Checked) abstractRecord.OutcomeCapsularPhimosis = false;
                if (RadioButtonOutcomeEntireIOLinBagYes.Checked) abstractRecord.OutcomeEntireIOLinBag = true;
                if (RadioButtonOutcomeEntireIOLinBagNo.Checked) abstractRecord.OutcomeEntireIOLinBag = false;
                if (RadioButtonOutcomeOnsetGlaucomaYes.Checked) abstractRecord.OutcomeOnsetGlaucoma = true;
                if (RadioButtonOutcomeOnsetGlaucomaNo.Checked) abstractRecord.OutcomeOnsetGlaucoma = false;
                if (RadioButtonOutcomeVitreousCellsYes.Checked) abstractRecord.OutcomeVitreousCells = true;
                if (RadioButtonOutcomeVitreousCellsNo.Checked) abstractRecord.OutcomeVitreousCells = false;
                if (RadioButtonOutcomeDilatedFundusExamPerformedYes.Checked) abstractRecord.OutcomeDilatedFundusExamPerformed = true;
                if (RadioButtonOutcomeDilatedFundusExamPerformedNo.Checked) abstractRecord.OutcomeDilatedFundusExamPerformed = false;
                if (RadioButtonOutcomeFluoresceinPerformedYes.Checked) abstractRecord.OutcomeFluoresceinPerformed = true;
                if (RadioButtonOutcomeFluoresceinPerformedNo.Checked) abstractRecord.OutcomeFluoresceinPerformed = false;
                if (RadioButtonOutcomeOCTPerformedYes.Checked) abstractRecord.OutcomeOCTPerformed = true;
                if (RadioButtonOutcomeOCTPerformedNo.Checked) abstractRecord.OutcomeOCTPerformed = false;
                if (RadioButtonOutcomeCMEPresentYes.Checked) abstractRecord.OutcomeCMEPresent = true;
                if (RadioButtonOutcomeCMEPresentNo.Checked) abstractRecord.OutcomeCMEPresent = false;
                if (RadioButtonOutcomeEpiretinalMembraneYes.Checked) abstractRecord.OutcomeEpiretinalMembrane = true;
                if (RadioButtonOutcomeEpiretinalMembraneNo.Checked) abstractRecord.OutcomeEpiretinalMembrane = false;
                if (RadioButtonOutcomePosteriorDiseaseActiveYes.Checked) abstractRecord.OutcomePosteriorDiseaseActive = true;
                if (RadioButtonOutcomePosteriorDiseaseActiveNo.Checked) abstractRecord.OutcomePosteriorDiseaseActive = false;
                if (RadioButtonOutcomeAdditionalSurgeryYes.Checked) abstractRecord.OutcomeAdditionalSurgery = true;
                if (RadioButtonOutcomeAdditionalSurgeryNo.Checked) abstractRecord.OutcomeAdditionalSurgery = false;



                try
                {
                if (TextBoxDiseaseDurationMonths.Text != null)
                abstractRecord.DiseaseDurationMonths =Convert.ToInt32( TextBoxDiseaseDurationMonths.Text);
                }
                catch (Exception ex)
                {
                abstractRecord.DiseaseDurationMonths = null;
                TextBoxDiseaseDurationMonths.Text = "";
                }
                try
                {
                if (TextBoxTherapyToDateImmuName.Text != null)
                abstractRecord.TherapyToDateImmuName = TextBoxTherapyToDateImmuName.Text;
                }
                catch (Exception ex)
                {
                abstractRecord.TherapyToDateImmuName = null;
                TextBoxTherapyToDateImmuName.Text = "";
                }
                try
                {
                if (TextBoxRBIrisClockHours.Text != null)
                abstractRecord.RBIrisClockHours = TextBoxRBIrisClockHours.Text;
                }
                catch (Exception ex)
                {
                abstractRecord.RBIrisClockHours = null;
                TextBoxRBIrisClockHours.Text = "";
                }
                try
                {
                if (TextBoxIOP.Text != null)
                abstractRecord.IOP = Convert.ToDouble(TextBoxIOP.Text);
                }
                catch (Exception ex)
                {
                abstractRecord.IOP = null;
                TextBoxIOP.Text = "";
                }
                try
                {
                if (TextBoxRBOpticDiscAbnormalityOther.Text != null)
                abstractRecord.RBOpticDiscAbnormalityOther = TextBoxRBOpticDiscAbnormalityOther.Text;
                }
                catch (Exception ex)
                {
                abstractRecord.RBOpticDiscAbnormalityOther = null;
                TextBoxRBOpticDiscAbnormalityOther.Text = "";
                }
                try
                {
                if (TextBoxPreopComorbidOtherText.Text != null)
                abstractRecord.PreopComorbidOtherText = TextBoxPreopComorbidOtherText.Text;
                }
                catch (Exception ex)
                {
                abstractRecord.PreopComorbidOtherText = null;
                TextBoxPreopComorbidOtherText.Text = "";
                }
                try
                {
                if (TextBoxIOLComplicationOtherText.Text != null)
                abstractRecord.IOLComplicationOtherText = TextBoxIOLComplicationOtherText.Text;
                }
                catch (Exception ex)
                {
                abstractRecord.IOLComplicationOtherText = null;
                TextBoxIOLComplicationOtherText.Text = "";
                }
                try
                {
                if (TextBoxOutcomeMonthsAssessed.Text != null)
                abstractRecord.OutcomeMonthsAssessed = Convert.ToInt32(TextBoxOutcomeMonthsAssessed.Text);
                }
                catch (Exception ex)
                {
                abstractRecord.OutcomeMonthsAssessed = null;
                TextBoxOutcomeMonthsAssessed.Text = "";
                }
                try
                {
                if (TextBoxOutcomePosteriorSynechiaeClockHours.Text != null)
                abstractRecord.OutcomePosteriorSynechiaeClockHours = TextBoxOutcomePosteriorSynechiaeClockHours.Text;
                }
                catch (Exception ex)
                {
                abstractRecord.OutcomePosteriorSynechiaeClockHours = null;
                TextBoxOutcomePosteriorSynechiaeClockHours.Text = "";
                }
                try
                {
                if (TextBoxOutcomeIOP.Text != null)
                abstractRecord.OutcomeIOP = TextBoxOutcomeIOP.Text;
                }
                catch (Exception ex)
                {
                abstractRecord.OutcomeIOP = null;
                TextBoxOutcomeIOP.Text = "";
                }
                try
                {
                if (TextBoxOutcomePosteriorDiseaseOther.Text != null)
                abstractRecord.OutcomePosteriorDiseaseOther = TextBoxOutcomePosteriorDiseaseOther.Text;
                }
                catch (Exception ex)
                {
                abstractRecord.OutcomePosteriorDiseaseOther = null;
                TextBoxOutcomePosteriorDiseaseOther.Text = "";
                }
                try
                {
                if (TextBoxOutcomeAdditionalOtherText.Text != null)
                abstractRecord.OutcomeAdditionalOtherText = TextBoxOutcomeAdditionalOtherText.Text;
                }
                catch (Exception ex)
                {
                abstractRecord.OutcomeAdditionalOtherText = null;
                TextBoxOutcomeAdditionalOtherText.Text = "";
                }


                abstractRecord.LastUpdateDate = DateTime.Now;
                bool ChartCompleted = isComplete();
                abstractRecord.Complete = ChartCompleted;
                var chartRegistration = (from cr in pim.ChartRegistration
                                         where cr.ParticipantModuleCycleID == cycle.ParticipantModuleCycleID
                                          & cr.ModuleID == ModuleID
                                          & cr.RecordIdentifier == recordIdentifier
                                         select cr).First<NetHealthPIMModel.ChartRegistration>();
                chartRegistration.AbstractCompleted = ChartCompleted;


                if ((DropDownListYearOfBirth.SelectedIndex > 0) && (DropDownListYearOfBirth.SelectedIndex > 0))
                {
                    chartRegistration.DOB = abstractRecord.MonthOfBirth + "/" + abstractRecord.YearOfBirth;
                }

                pim.SaveChanges();

                transaction.Complete();




            }

            CycleManager.UpdateParticipantCompletedModules(cycleID, ModuleID);
        }
    }

    protected void ButtonSubmit_Click(object sender, EventArgs e)
    {
        savedata();
        isComplete();
        if (!isComplete())
        {
            isComplete();
            string script = " var answer = confirm('Chart data is not complete.\\nClick OK to save entries and exit chart.\\nClick CANCEL to save entries and review the chart. '); if (answer==true) window.location = '../PatientChartRegistration.aspx';";
            ScriptManager.RegisterStartupScript(this, GetType(),
                          "ServerControlScript", script, true);
        }
        else
        {

            Response.Redirect("../PatientChartRegistration.aspx");
        }
    }
    protected bool isComplete()
    {

        bool ChartCompleted = true;
        LabelRBGender.Visible = false;

        LabelRBExamcornea.Visible = false;

        LabelRBExamCorneaKeratopathy.Visible = false;

        LabelRBExamCorneaScars.Visible = false;

        LabelRBKPs.Visible = false;
        LabelRBKPsAppearance.Visible = false;

        LabelRBKPsLocation.Visible = false;

        LabelRBAnteriorFlare.Visible = false;

        LabelRBAnteriorCells.Visible = false;
        LabelRBIris.Visible = false;
        LabelHistoryGlaucoma.Visible = false;
        LabelHistoryGlaucomaSurgery.Visible = false;
        LabelRBIrisNeovascularization.Visible = false;

        LabelRBPupillaryMembrane.Visible = false;

        LabelRBAnteriorCapsule.Visible = false;

        LabelRBCataractGrade.Visible = false;

        LabelRBCataractGrade.Visible = false;

        LabelRBCataractGrade.Visible = false;

        LabelRBCataractGrade.Visible = false;

        LabelRBCataractExtraction.Visible = false;

        LabelRBIOLImplant.Visible = false;


        LabelRBIOLImplant.Visible = false;

        LabelRBOutcomeInflammation.Visible = false;

        LabelRBOutcomeNeovasc.Visible = false;

        LabelRBOutcomeInflamDeposits.Visible = false;

        LabelRBOutcomeWellCentered.Visible = false;

        LabelRBOutcomeOpticCapture.Visible = false;

        LabelRBOutcomePatientSatisfied.Visible = false;

        LabelYearOfBirth.Visible = false;

        LabelMonthOfBirth.Visible = false;

        LabelMonthOfOnsetFirstEpisode.Visible = false;

        LabelYearOfOnsetFirstEpisode.Visible = false;

        LabelMonthOfCataractSurgery.Visible = false;

        LabelYearOfCataractSurgery.Visible = false;

        LabelDiseaseDurationMonths.Visible = false;



        LabelIOP.Visible = false;

        LabelOutcomeMonthsAssessed.Visible = false;


        LabelOutcomePosteriorSynechiaeClockHours.Visible = false;

        LabelOutcomeIOP.Visible = false;

        LabelHistoryGlaucomaSurgery.Visible = false;

        LabelGonioscopyPerformed.Visible = false;

        LabelVitreousCells.Visible = false;

        LabelOpticDiscGlaucomatousChanges.Visible = false;

        LabelOpticDiscAbnormalities.Visible = false;

        LabelExamDilatedFundus.Visible = false;

        LabelExamFluorescein.Visible = false;

        LabelExamOCT.Visible = false;
        LabelExamBscan.Visible = false;

        LabelPreopCataractMainCause.Visible = false;

        LabelPreopOcularCoMorbilities.Visible = false;
        LabelMgntConsent.Visible = false;

        LabelMgntPeriopCS.Visible = false;

        LabelMgntPeriopCS.Visible = false;

        LabelMgntPeriopCS.Visible = false;

        LabelMgntPeriopCS.Visible = false;

        LabelMgntPeriopCS.Visible = false;

        LabelMgntPeriopCS.Visible = false;

        LabelSurgCapsularStaining.Visible = false;

        LabelSurgIrisHooks.Visible = false;

        LabelSurgIPupillaryExpanders.Visible = false;

        LabelSurgPosteriorSynechiae.Visible = false;

        LabelSurgPupillarymembrane.Visible = false;

        LabelRBIOLImplant.Visible = false;

        LabelRBIOLImplant.Visible = false;

        LabelRBIOLImplant.Visible = false;

        LabelRBIOLImplant.Visible = false;

        LabelOutcomeCapsularPhimosis.Visible = false;

        LabelOutcomeEntireIOLinBag.Visible = false;

        LabelOutcomeOnsetGlaucoma.Visible = false;

        LabelOutcomeVitreousCells.Visible = false;

        LabelOutcomeDilatedFundusExamPerformed.Visible = false;

        LabelOutcomeFluoresceinPerformed.Visible = false;
        LabelOutcomeOCTPerformed.Visible = false;

        LabelOutcomeCMEPresent.Visible = false;

        LabelOutcomeEpiretinalMembrane.Visible = false;

        LabelOutcomePosteriorDiseaseActive.Visible = false;

        LabelOutcomeAdditionalSurgery.Visible = false;
           

        if (RadioButtonListRBGender.SelectedIndex == -1)
        {
            LabelRBGender.Visible = true;
            ChartCompleted = false;
        }
        if (RadioButtonListRBExamcornea.SelectedIndex == -1)
        {
            LabelRBExamcornea.Visible = true;
            ChartCompleted = false;
        }
       
    
        if (RadioButtonListRBAnteriorFlare.SelectedIndex == -1)
        {
            LabelRBAnteriorFlare.Visible = true;
            ChartCompleted = false;
        }
        if (RadioButtonListRBAnteriorCells.SelectedIndex == -1)
        {
            LabelRBAnteriorCells.Visible = true;
            ChartCompleted = false;
        }
        if (RadioButtonListRBIris.SelectedIndex == -1)
        {
            LabelRBIris.Visible = true;
            ChartCompleted = false;
        }
   
  
        if (RadioButtonListRBOutcomeNeovasc.SelectedIndex == -1)
        {
            LabelRBOutcomeNeovasc.Visible = true;
            ChartCompleted = false;
        }
      
        if (RadioButtonListRBOutcomeWellCentered.SelectedIndex == -1)
        {
            LabelRBOutcomeWellCentered.Visible = true;
            ChartCompleted = false;
        }
      
      
        if (RadioButtonListRBOutcomePatientSatisfied.SelectedIndex == -1)
        {
            LabelRBOutcomePatientSatisfied.Visible = true;
            ChartCompleted = false;
        }



        if (DropDownListYearOfBirth.SelectedIndex == 0)
        {
            LabelYearOfBirth.Visible = true;
            ChartCompleted = false;
        }
        if (DropDownListMonthOfBirth.SelectedIndex == 0)
        {
            LabelMonthOfBirth.Visible = true;
            ChartCompleted = false;
        }
        if (DropDownListMonthOfOnsetFirstEpisode.SelectedIndex == 0)
        {
            LabelMonthOfOnsetFirstEpisode.Visible = true;
            ChartCompleted = false;
        }
        if (DropDownListYearOfOnsetFirstEpisode.SelectedIndex == 0)
        {
            LabelYearOfOnsetFirstEpisode.Visible = true;
            ChartCompleted = false;
        }
        if (DropDownListMonthOfCataractSurgery.SelectedIndex == 0)
        {
            LabelMonthOfCataractSurgery.Visible = true;
            ChartCompleted = false;
        }
        if (DropDownListYearOfCataractSurgery.SelectedIndex == 0)
        {
            LabelYearOfCataractSurgery.Visible = true;
            ChartCompleted = false;
        }
        if (string.IsNullOrEmpty(TextBoxDiseaseDurationMonths.Text))
        {
            LabelDiseaseDurationMonths.Visible = true;
            ChartCompleted = false;
        }
       
 
        if (string.IsNullOrEmpty(TextBoxOutcomeMonthsAssessed.Text))
        {
            LabelOutcomeMonthsAssessed.Visible = true;
            ChartCompleted = false;
        }
        
        if (string.IsNullOrEmpty(TextBoxOutcomePosteriorSynechiaeClockHours.Text))
        {
            LabelOutcomePosteriorSynechiaeClockHours.Visible = true;
            ChartCompleted = false;
        }
        if (string.IsNullOrEmpty(TextBoxOutcomeIOP.Text))
        {
            LabelOutcomeIOP.Visible = true;
            ChartCompleted = false;
        }
        if (RadioButtonHistoryGlaucomaNo.Checked == false && RadioButtonHistoryGlaucomaYes.Checked == false)
        {
            LabelHistoryGlaucoma.Visible = true;
            ChartCompleted = false;
        }


        if (RadioButtonHistoryGlaucomaSurgeryNo.Checked == false && RadioButtonHistoryGlaucomaSurgeryYes.Checked == false)
        {
            LabelHistoryGlaucomaSurgery.Visible = true;
            ChartCompleted = false;
        }
    
        if (RadioButtonVitreousCellsNo.Checked == false && RadioButtonVitreousCellsYes.Checked == false)
        {
            LabelVitreousCells.Visible = true;
            ChartCompleted = false;
        }
        if (RadioButtonOpticDiscGlaucomatousChangesNo.Checked == false && RadioButtonOpticDiscGlaucomatousChangesYes.Checked == false)
        {
            LabelOpticDiscGlaucomatousChanges.Visible = true;
            ChartCompleted = false;
        }
        if (RadioButtonOpticDiscAbnormalitiesNo.Checked == false && RadioButtonOpticDiscAbnormalitiesYes.Checked == false)
        {
            LabelOpticDiscAbnormalities.Visible = true;
            ChartCompleted = false;
        }
        if (RadioButtonExamDilatedFundusNo.Checked == false && RadioButtonExamDilatedFundusYes.Checked == false)
        {
            LabelExamDilatedFundus.Visible = true;
            ChartCompleted = false;
        }
        if (RadioButtonExamFluoresceinNo.Checked == false && RadioButtonExamFluoresceinYes.Checked == false)
        {
            LabelExamFluorescein.Visible = true;
            ChartCompleted = false;
        }
        if (RadioButtonExamOCTNo.Checked == false && RadioButtonExamOCTYes.Checked == false)
        {
            LabelExamOCT.Visible = true;
            ChartCompleted = false;
        }
        if (RadioButtonExamBscanNo.Checked == false && RadioButtonExamBscanYes.Checked == false)
        {
            LabelExamBscan.Visible = true;
            ChartCompleted = false;
        }
        if (RadioButtonPreopCataractMainCauseNo.Checked == false && RadioButtonPreopCataractMainCauseYes.Checked == false)
        {
            LabelPreopCataractMainCause.Visible = true;
            ChartCompleted = false;
        }
        if (RadioButtonPreopOcularCoMorbilitiesNo.Checked == false && RadioButtonPreopOcularCoMorbilitiesYes.Checked == false)
        {
            LabelPreopOcularCoMorbilities.Visible = true;
            ChartCompleted = false;
        }
        if (RadioButtonMgntConsentNo.Checked == false && RadioButtonMgntConsentYes.Checked == false)
        {
            LabelMgntConsent.Visible = true;
            ChartCompleted = false;
        }
        if (RadioButtonMgntPeriopCSNo.Checked == false && RadioButtonMgntPeriopCSYes.Checked == false)
        {
            LabelMgntPeriopCS.Visible = true;
            ChartCompleted = false;
        }
        
        if (RadioButtonSurgCapsularStainingNo.Checked == false && RadioButtonSurgCapsularStainingYes.Checked == false)
        {
            LabelSurgCapsularStaining.Visible = true;
            ChartCompleted = false;
        }
        if (RadioButtonSurgIrisHooksNo.Checked == false && RadioButtonSurgIrisHooksYes.Checked == false)
        {
            LabelSurgIrisHooks.Visible = true;
            ChartCompleted = false;
        }
        if (RadioButtonSurgIPupillaryExpandersNo.Checked == false && RadioButtonSurgIPupillaryExpandersYes.Checked == false)
        {
            LabelSurgIPupillaryExpanders.Visible = true;
            ChartCompleted = false;
        }
        if (RadioButtonSurgPosteriorSynechiaeNo.Checked == false && RadioButtonSurgPosteriorSynechiaeYes.Checked == false)
        {
            LabelSurgPosteriorSynechiae.Visible = true;
            ChartCompleted = false;
        }
        if (RadioButtonSurgPupillarymembraneNo.Checked == false && RadioButtonSurgPupillarymembraneYes.Checked == false)
        {
            LabelSurgPupillarymembrane.Visible = true;
            ChartCompleted = false;
        }
        if (RadioButtonIOLImplantationNo.Checked == false && RadioButtonIOLImplantationYes.Checked == false)
        {
            LabelRBIOLImplant.Visible = true;
            ChartCompleted = false;
        }
        
        if (RadioButtonOutcomeCapsularPhimosisNo.Checked == false && RadioButtonOutcomeCapsularPhimosisYes.Checked == false)
        {
            LabelOutcomeCapsularPhimosis.Visible = true;
            ChartCompleted = false;
        }
        if (RadioButtonOutcomeEntireIOLinBagNo.Checked == false && RadioButtonOutcomeEntireIOLinBagYes.Checked == false)
        {
            LabelOutcomeEntireIOLinBag.Visible = true;
            ChartCompleted = false;
        }
        if (RadioButtonOutcomeOnsetGlaucomaNo.Checked == false && RadioButtonOutcomeOnsetGlaucomaYes.Checked == false)
        {
            LabelOutcomeOnsetGlaucoma.Visible = true;
            ChartCompleted = false;
        }
        if (RadioButtonOutcomeVitreousCellsNo.Checked == false && RadioButtonOutcomeVitreousCellsYes.Checked == false)
        {
            LabelOutcomeVitreousCells.Visible = true;
            ChartCompleted = false;
        }
        if (RadioButtonOutcomeDilatedFundusExamPerformedNo.Checked == false && RadioButtonOutcomeDilatedFundusExamPerformedYes.Checked == false)
        {
            LabelOutcomeDilatedFundusExamPerformed.Visible = true;
            ChartCompleted = false;
        }
        if (RadioButtonOutcomeFluoresceinPerformedNo.Checked == false && RadioButtonOutcomeFluoresceinPerformedYes.Checked == false)
        {
            LabelOutcomeFluoresceinPerformed.Visible = true;
            ChartCompleted = false;
        }
        if (RadioButtonOutcomeOCTPerformedNo.Checked == false && RadioButtonOutcomeOCTPerformedYes.Checked == false)
        {
            LabelOutcomeOCTPerformed.Visible = true;
            ChartCompleted = false;
        }
        if (RadioButtonOutcomeCMEPresentNo.Checked == false && RadioButtonOutcomeCMEPresentYes.Checked == false)
        {
            LabelOutcomeCMEPresent.Visible = true;
            ChartCompleted = false;
        }
        if (RadioButtonOutcomeEpiretinalMembraneNo.Checked == false && RadioButtonOutcomeEpiretinalMembraneYes.Checked == false)
        {
            LabelOutcomeEpiretinalMembrane.Visible = true;
            ChartCompleted = false;
        }
        if (RadioButtonOutcomePosteriorDiseaseActiveNo.Checked == false && RadioButtonOutcomePosteriorDiseaseActiveYes.Checked == false)
        {
            LabelOutcomePosteriorDiseaseActive.Visible = true;
            ChartCompleted = false;
        }
        if (RadioButtonOutcomeAdditionalSurgeryNo.Checked == false && RadioButtonOutcomeAdditionalSurgeryYes.Checked == false)
        {
            LabelOutcomeAdditionalSurgery.Visible = true;
            ChartCompleted = false;
        }



        return ChartCompleted;
    }

}

