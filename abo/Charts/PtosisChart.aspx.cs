﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Transactions;
using NetHealthPIMModel;

public partial class abo_PtosisChart : BasePage
{
    protected void Page_Load(object sender, EventArgs e)
     
   {
        if (!IsPostBack)
        {
            InitializePage();
        }


    }
    protected void InitializePage()
    {
        ((PIMMasterPage)Page.Master).SetTitle("Chart");

        using (NetHealthPIMEntities pim = new NetHealthPIMEntities())
        {
            int ModuleID = (int)Session[Constants.SESSION_WORKINGMODULEID];
            Module module = pim.Module.First(c => c.ModuleID == ModuleID);
            ((PIMMasterPage)Page.Master).SetHeader(module.ModuleName + " Chart Abstraction");

            ParticipantModuleCycle prev = (ParticipantModuleCycle)Session[Constants.SESSION_PREVIOUSCYCLE];
            String CycleNumber = Request.QueryString["CycleNumber"];

            int cycleID = ctx.ActiveModuleCycleID;
            if (CycleNumber == "1")
            {
                cycleID = prev.ParticipantModuleCycleID;
                LinkButtonBackToDashboard.Visible = true;
                ButtonSubmit.Visible = false;
            }

            ParticipantModuleCycle cycle = pim.ParticipantModuleCycle.First(c => c.ParticipantModuleCycleID == cycleID);
            String numberOfRecord = Request.QueryString["nr"];
            String totalRecords = Request.QueryString["t"];
            if (cycle.CycleNumber == 1)
            {
                ((PIMMasterPage)Page.Master).SetStatus(2);
                LiteralAbstractionNumber.Text = "Initial Abstraction: Chart " + numberOfRecord + " of " + totalRecords;
            }
            else
            {
                ((PIMMasterPage)Page.Master).SetStatus(3);
                LiteralAbstractionNumber.Text = "Second Abstraction: Chart " + numberOfRecord + " of " + totalRecords;
            }
            DropDownListMonthOfBirth.DataSource = CycleManager.getMonthList();
            DropDownListYearOfBirth.DataSource = CycleManager.getDOBYearList(0, 100);
            




            DataBind();
            DropDownListYearOfBirth.Items.Insert(0, new ListItem("Year", "0"));

            string recordIdentifier = Request.QueryString["ri"];
            HiddenFieldRecordIdentifier.Value = recordIdentifier;
            LiteralRecordIdentifier.Text = recordIdentifier;
            var count = pim.ChartAbstractionPtosis.Where(ps => ps.ParticipantModuleCycle.ParticipantModuleCycleID == cycleID & ps.RecordIdentifier == recordIdentifier).Count();
            if (count == 0)
            {
                using (TransactionScope transaction = new TransactionScope())
                {
                    //Module module = pim.Module.First(m => m.ModuleID == Constants.ABO_AMBLYOPIA_MODULEID);

                    NetHealthPIMModel.ChartRegistration chartRegistration = (from cr in pim.ChartRegistration
                                                           where cr.ParticipantModuleCycleID == cycleID
                                                           & cr.RecordIdentifier == recordIdentifier
                                                           & cr.ModuleID == 36
                                                           select cr).First<NetHealthPIMModel.ChartRegistration>();

                    DateTime initialVisit = Convert.ToDateTime(chartRegistration.InitialVisit);
                    DateTime DOB = Convert.ToDateTime(chartRegistration.DOB);

                    ChartAbstractionPtosis abstractRecord = new ChartAbstractionPtosis();
                    abstractRecord.ParticipantModuleCycle = cycle;
                    abstractRecord.RecordIdentifier = recordIdentifier;
                    abstractRecord.MonthOfBirth = DOB.Month;
                    abstractRecord.YearOfBirth = DOB.Year;
                    DropDownListYearOfBirth.SelectedValue = abstractRecord.YearOfBirth.ToString();
                    DropDownListMonthOfBirth.SelectedValue = abstractRecord.MonthOfBirth.ToString();

                    abstractRecord.CreatedOn = DateTime.Now;
                    abstractRecord.LastUpdateDate = DateTime.Now;
                    pim.SaveChanges();

                    transaction.Complete();
                }
            }
            else
            {

                ChartAbstractionPtosis abstractRecord = (from c in pim.ChartAbstractionPtosis
                                                         where c.ParticipantModuleCycle.ParticipantModuleCycleID == cycleID & c.RecordIdentifier == recordIdentifier
                                                         select c).First<ChartAbstractionPtosis>();
                if (abstractRecord.PreopNA != null) CheckBoxPreopNA.Checked = ((bool)abstractRecord.PreopNA);
                if (abstractRecord.LevatorExcursionNA != null) CheckBoxLevatorExcursionNA.Checked = ((bool)abstractRecord.LevatorExcursionNA);
                if (abstractRecord.EtiologyAponeurotic != null) CheckBoxEtiologyAponeurotic.Checked = ((bool)abstractRecord.EtiologyAponeurotic);
                if (abstractRecord.EtiologyMyopathic != null) CheckBoxEtiologyMyopathic.Checked = ((bool)abstractRecord.EtiologyMyopathic);
                if (abstractRecord.EtiologyNeurogenic != null) CheckBoxEtiologyNeurogenic.Checked = ((bool)abstractRecord.EtiologyNeurogenic);
                if (abstractRecord.EtiologyOther != null) CheckBoxEtiologyOther.Checked = ((bool)abstractRecord.EtiologyOther);
                if (abstractRecord.PostopSTInfection != null) CheckBoxPostopSTInfection.Checked = ((bool)abstractRecord.PostopSTInfection);
                if (abstractRecord.PostopSTKeratitis != null) CheckBoxPostopSTKeratitis.Checked = ((bool)abstractRecord.PostopSTKeratitis);
                if (abstractRecord.PostopSTOverCorrection != null) CheckBoxPostopSTOverCorrection.Checked = ((bool)abstractRecord.PostopSTOverCorrection);
                if (abstractRecord.PostopSTUnderCorrection != null) CheckBoxPostopSTUnderCorrection.Checked = ((bool)abstractRecord.PostopSTUnderCorrection);
                if (abstractRecord.PostopSTPoorContour != null) CheckBoxPostopSTPoorContour.Checked = ((bool)abstractRecord.PostopSTPoorContour);
                if (abstractRecord.PostopSTScarring != null) CheckBoxPostopSTScarring.Checked = ((bool)abstractRecord.PostopSTScarring);
                if (abstractRecord.PostopSTSuspension != null) CheckBoxPostopSTSuspension.Checked = ((bool)abstractRecord.PostopSTSuspension);
                if (abstractRecord.PostopSTOther != null) CheckBoxPostopSTOther.Checked = ((bool)abstractRecord.PostopSTOther);
                if (abstractRecord.PostopSTNone != null) CheckBoxPostopSTNone.Checked = ((bool)abstractRecord.PostopSTNone);
                if (abstractRecord.PostopLTInfection != null) CheckBoxPostopLTInfection.Checked = ((bool)abstractRecord.PostopLTInfection);
                if (abstractRecord.PostopLTKeratitis != null) CheckBoxPostopLTKeratitis.Checked = ((bool)abstractRecord.PostopLTKeratitis);
                if (abstractRecord.PostopLTOverCorrection != null) CheckBoxPostopLTOverCorrection.Checked = ((bool)abstractRecord.PostopLTOverCorrection);
                if (abstractRecord.PostopLTUnderCorrection != null) CheckBoxPostopLTUnderCorrection.Checked = ((bool)abstractRecord.PostopLTUnderCorrection);
                if (abstractRecord.PostopLTPoorContour != null) CheckBoxPostopLTPoorContour.Checked = ((bool)abstractRecord.PostopLTPoorContour);
                if (abstractRecord.PostopLTScarring != null) CheckBoxPostopLTScarring.Checked = ((bool)abstractRecord.PostopLTScarring);
                if (abstractRecord.PostopLTSuspension != null) CheckBoxPostopLTSuspension.Checked = ((bool)abstractRecord.PostopLTSuspension);
                if (abstractRecord.PostopLTOther != null) CheckBoxPostopLTOther.Checked = ((bool)abstractRecord.PostopLTOther);
                if (abstractRecord.PostopLTNone != null) CheckBoxPostopLTNone.Checked = ((bool)abstractRecord.PostopLTNone);


                if (abstractRecord.RBAssessEyelidFatigue != null) RadioButtonListAssessEyelidFatigue.SelectedValue = abstractRecord.RBAssessEyelidFatigue.ToString(); 
                if (abstractRecord.RBGender != null) RadioButtonListRBGender.SelectedValue = abstractRecord.RBGender.ToString();
                if (abstractRecord.RBHistoryCornealSurgery != null) RadioButtonListRBHistoryCornealSurgery.SelectedValue = abstractRecord.RBHistoryCornealSurgery.ToString();
                if (abstractRecord.RBSymptomDryEye != null) RadioButtonListRBSymptomDryEye.SelectedValue = abstractRecord.RBSymptomDryEye.ToString();
                if (abstractRecord.RBOcularAlignment != null) RadioButtonListRBOcularAlignment.SelectedValue = abstractRecord.RBOcularAlignment.ToString();
                if (abstractRecord.RBPtosisProc != null) RadioButtonListRBPtosisProc.SelectedValue = abstractRecord.RBPtosisProc.ToString();



                if (abstractRecord.MonthOfBirth != null) DropDownListMonthOfBirth.SelectedValue = abstractRecord.MonthOfBirth.ToString();
                if (abstractRecord.YearOfBirth != null) DropDownListYearOfBirth.SelectedValue = abstractRecord.YearOfBirth.ToString();



                if (abstractRecord.HistoryPtoticLidEffect != null)
                {
                    RadioButtonHistoryPtoticLidEffectYes.Checked = ((bool)abstractRecord.HistoryPtoticLidEffect);
                    RadioButtonHistoryPtoticLidEffectNo.Checked = !((bool)abstractRecord.HistoryPtoticLidEffect);
                }
                if (abstractRecord.HistoryOcular != null)
                {
                    RadioButtonHistoryOcularYes.Checked = ((bool)abstractRecord.HistoryOcular);
                    RadioButtonHistoryOcularNo.Checked = !((bool)abstractRecord.HistoryOcular);
                }
                if (abstractRecord.HistoryOnsetPtosis != null)
                {
                    RadioButtonHistoryOnsetPtosisYes.Checked = ((bool)abstractRecord.HistoryOnsetPtosis);
                    RadioButtonHistoryOnsetPtosisNo.Checked = !((bool)abstractRecord.HistoryOnsetPtosis);
                }
                if (abstractRecord.HistoryVariabilityPtosis != null)
                {
                    RadioButtonHistoryVariabilityPtosisYes.Checked = ((bool)abstractRecord.HistoryVariabilityPtosis);
                    RadioButtonHistoryVariabilityPtosisNo.Checked = !((bool)abstractRecord.HistoryVariabilityPtosis);
                }
                if (abstractRecord.HistoryPreviousLid != null)
                {
                    RadioButtonHistoryPreviousLidYes.Checked = ((bool)abstractRecord.HistoryPreviousLid);
                    RadioButtonHistoryPreviousLidNo.Checked = !((bool)abstractRecord.HistoryPreviousLid);
                }
                if (abstractRecord.HistoryDiplopia != null)
                {
                    RadioButtonHistoryDiplopiaYes.Checked = ((bool)abstractRecord.HistoryDiplopia);
                    RadioButtonHistoryDiplopiaNo.Checked = !((bool)abstractRecord.HistoryDiplopia);
                }
                if (abstractRecord.VisualAcuityDocumented != null)
                {
                    RadioButtonVisualAcuityDocumentedYes.Checked = ((bool)abstractRecord.VisualAcuityDocumented);
                    RadioButtonVisualAcuityDocumentedNo.Checked = !((bool)abstractRecord.VisualAcuityDocumented);
                }
                if (abstractRecord.PupillaryExam != null)
                {
                    RadioButtonPupillaryExamYes.Checked = ((bool)abstractRecord.PupillaryExam);
                    RadioButtonPupillaryExamNo.Checked = !((bool)abstractRecord.PupillaryExam);
                }
                if (abstractRecord.PtosisVisualField != null)
                {
                    RadioButtonPtosisVisualFieldYes.Checked = ((bool)abstractRecord.PtosisVisualField);
                    RadioButtonPtosisVisualFieldNo.Checked = !((bool)abstractRecord.PtosisVisualField);
                }
                if (abstractRecord.ExamCCAL != null)
                {
                    RadioButtonExamCCALYes.Checked = ((bool)abstractRecord.ExamCCAL);
                    RadioButtonExamCCALNo.Checked = !((bool)abstractRecord.ExamCCAL);
                }
                if (abstractRecord.AssessTearProduction != null)
                {
                    RadioButtonAssessTearProductionYes.Checked = ((bool)abstractRecord.AssessTearProduction);
                    RadioButtonAssessTearProductionNo.Checked = !((bool)abstractRecord.AssessTearProduction);
                }
                if (abstractRecord.AssessCornealSensation != null)
                {
                    RadioButtonAssessCornealSensationYes.Checked = ((bool)abstractRecord.AssessCornealSensation);
                    RadioButtonAssessCornealSensationNo.Checked = !((bool)abstractRecord.AssessCornealSensation);
                }
                if (abstractRecord.AssessBrowPosition != null)
                {
                    RadioButtonAssessBrowPositionYes.Checked = ((bool)abstractRecord.AssessBrowPosition);
                    RadioButtonAssessBrowPositionNo.Checked = !((bool)abstractRecord.AssessBrowPosition);
                }
             
                if (abstractRecord.AssessDerma != null)
                {
                    RadioButtonAssessDermaYes.Checked = ((bool)abstractRecord.AssessDerma);
                    RadioButtonAssessDermaNo.Checked = !((bool)abstractRecord.AssessDerma);
                }
                if (abstractRecord.PresenceBellsPhenom != null)
                {
                    RadioButtonPresenceBellsPhenomYes.Checked = ((bool)abstractRecord.PresenceBellsPhenom);
                    RadioButtonPresenceBellsPhenomNo.Checked = !((bool)abstractRecord.PresenceBellsPhenom);
                }
                if (abstractRecord.DocInformedConsent != null)
                {
                    RadioButtonDocInformedConsentYes.Checked = ((bool)abstractRecord.DocInformedConsent);
                    RadioButtonDocInformedConsentNo.Checked = !((bool)abstractRecord.DocInformedConsent);
                }
                if (abstractRecord.OperationSummary != null)
                {
                    RadioButtonOperationSummaryYes.Checked = ((bool)abstractRecord.OperationSummary);
                    RadioButtonOperationSummaryNo.Checked = !((bool)abstractRecord.OperationSummary);
                }
                if (abstractRecord.PostopMRD != null)
                {
                    RadioButtonPostopMRDYes.Checked = ((bool)abstractRecord.PostopMRD);
                    RadioButtonPostopMRDNo.Checked = !((bool)abstractRecord.PostopMRD);
                }
                if (abstractRecord.PostopCorneaExam != null)
                {
                    RadioButtonPostopCorneaExamYes.Checked = ((bool)abstractRecord.PostopCorneaExam);
                    RadioButtonPostopCorneaExamNo.Checked = !((bool)abstractRecord.PostopCorneaExam);
                }
                if (abstractRecord.PostopIncisionStatus != null)
                {
                    RadioButtonPostopIncisionStatusYes.Checked = ((bool)abstractRecord.PostopIncisionStatus);
                    RadioButtonPostopIncisionStatusNo.Checked = !((bool)abstractRecord.PostopIncisionStatus);
                }
                if (abstractRecord.Reoperation != null)
                {
                    RadioButtonReoperationYes.Checked = ((bool)abstractRecord.Reoperation);
                    RadioButtonReoperationNo.Checked = !((bool)abstractRecord.Reoperation);
                }
                if (abstractRecord.PostopComplicationDailyLiving != null)
                {
                    RadioButtonPostopComplicationDailyLivingYes.Checked = ((bool)abstractRecord.PostopComplicationDailyLiving);
                    RadioButtonPostopComplicationDailyLivingNo.Checked = !((bool)abstractRecord.PostopComplicationDailyLiving);
                }
                if (abstractRecord.OutcomeLidMargin != null)
                {
                    RadioButtonOutcomeLidMarginYes.Checked = ((bool)abstractRecord.OutcomeLidMargin);
                    RadioButtonOutcomeLidMarginNo.Checked = !((bool)abstractRecord.OutcomeLidMargin);
                }
                if (abstractRecord.OutcomeActivityImproved != null)
                {
                    RadioButtonOutcomeActivityImprovedYes.Checked = ((bool)abstractRecord.OutcomeActivityImproved);
                    RadioButtonOutcomeActivityImprovedNo.Checked = !((bool)abstractRecord.OutcomeActivityImproved);
                }



                if (abstractRecord.PreopODMRD1 != null) TextBoxPreopODMRD1.Text = abstractRecord.PreopODMRD1.ToString();
                if (abstractRecord.PreopOSMRD1 != null) TextBoxPreopOSMRD1.Text = abstractRecord.PreopOSMRD1.ToString();
                if (abstractRecord.LevatorExcursionOD != null) TextBoxLevatorExcursionOD.Text = abstractRecord.LevatorExcursionOD.ToString();
                if (abstractRecord.LevatorExcursionOS != null) TextBoxLevatorExcursionOS.Text = abstractRecord.LevatorExcursionOS.ToString();
                if (abstractRecord.EtiologyOtherText != null) TextBoxEtiologyOtherText.Text = abstractRecord.EtiologyOtherText.ToString();
                if (abstractRecord.PtosisProcOtherText != null) TextBoxPtosisProcOtherText.Text = abstractRecord.PtosisProcOtherText.ToString();
                if (abstractRecord.PostopSTOtherText != null) TextBoxPostopSTOtherText.Text = abstractRecord.PostopSTOtherText.ToString();
                if (abstractRecord.PostopLTOtherText != null) TextBoxPostopLTOtherText.Text = abstractRecord.PostopLTOtherText.ToString();
                if (abstractRecord.PostopFollowedMonths != null) TextBoxPostOpFollowedMonths.Text = abstractRecord.PostopFollowedMonths.ToString();

                if (abstractRecord.PostofFollowedDays != null) TextBoxPostOpFollowedDays.Text = abstractRecord.PostofFollowedDays.ToString();



            }


        }

    }
    public void savedata()
    {
        using (NetHealthPIMEntities pim = new NetHealthPIMEntities())
        {
            int ModuleID = (int)Session[Constants.SESSION_WORKINGMODULEID];
            int cycleID = ctx.ActiveModuleCycleID;
            ParticipantModuleCycle cycle = pim.ParticipantModuleCycle.First(c => c.ParticipantModuleCycleID == cycleID);
            string recordIdentifier = HiddenFieldRecordIdentifier.Value;
            using (TransactionScope transaction = new TransactionScope())
            {
                ChartAbstractionPtosis abstractRecord = (from c in pim.ChartAbstractionPtosis
                                                         where c.ParticipantModuleCycle.ParticipantModuleCycleID == cycleID & c.RecordIdentifier == recordIdentifier
                                                         select c).First<ChartAbstractionPtosis>();



                                         abstractRecord.PreopNA = CheckBoxPreopNA.Checked;
                            abstractRecord.LevatorExcursionNA = CheckBoxLevatorExcursionNA.Checked;
                            abstractRecord.EtiologyAponeurotic = CheckBoxEtiologyAponeurotic.Checked;
                            abstractRecord.EtiologyMyopathic = CheckBoxEtiologyMyopathic.Checked;
                            abstractRecord.EtiologyNeurogenic = CheckBoxEtiologyNeurogenic.Checked;
                            abstractRecord.EtiologyOther = CheckBoxEtiologyOther.Checked;
                            abstractRecord.PostopSTInfection = CheckBoxPostopSTInfection.Checked;
                            abstractRecord.PostopSTKeratitis = CheckBoxPostopSTKeratitis.Checked;
                            abstractRecord.PostopSTOverCorrection = CheckBoxPostopSTOverCorrection.Checked;
                            abstractRecord.PostopSTUnderCorrection = CheckBoxPostopSTUnderCorrection.Checked;
                            abstractRecord.PostopSTPoorContour = CheckBoxPostopSTPoorContour.Checked;
                            abstractRecord.PostopSTScarring = CheckBoxPostopSTScarring.Checked;
                            abstractRecord.PostopSTSuspension = CheckBoxPostopSTSuspension.Checked;
                            abstractRecord.PostopSTOther = CheckBoxPostopSTOther.Checked;
                            abstractRecord.PostopSTNone = CheckBoxPostopSTNone.Checked;
                            abstractRecord.PostopLTInfection = CheckBoxPostopLTInfection.Checked;
                            abstractRecord.PostopLTKeratitis = CheckBoxPostopLTKeratitis.Checked;
                            abstractRecord.PostopLTOverCorrection = CheckBoxPostopLTOverCorrection.Checked;
                            abstractRecord.PostopLTUnderCorrection = CheckBoxPostopLTUnderCorrection.Checked;
                            abstractRecord.PostopLTPoorContour = CheckBoxPostopLTPoorContour.Checked;
                            abstractRecord.PostopLTScarring = CheckBoxPostopLTScarring.Checked;
                            abstractRecord.PostopLTSuspension = CheckBoxPostopLTSuspension.Checked;
                            abstractRecord.PostopLTOther = CheckBoxPostopLTOther.Checked;
                            abstractRecord.PostopLTNone = CheckBoxPostopLTNone.Checked;

                            if (RadioButtonListAssessEyelidFatigue.SelectedIndex != -1) abstractRecord.RBAssessEyelidFatigue = Convert.ToInt32(RadioButtonListAssessEyelidFatigue.SelectedValue);
                            else abstractRecord.RBAssessEyelidFatigue = 0;
                            if (RadioButtonListRBGender.SelectedIndex != -1) abstractRecord.RBGender = Convert.ToInt32(RadioButtonListRBGender.SelectedValue);
                            else abstractRecord.RBGender = 0;
                            if (RadioButtonListRBHistoryCornealSurgery.SelectedIndex != -1) abstractRecord.RBHistoryCornealSurgery = Convert.ToInt32(RadioButtonListRBHistoryCornealSurgery.SelectedValue);
                            else abstractRecord.RBHistoryCornealSurgery = 0;
                            if (RadioButtonListRBSymptomDryEye.SelectedIndex != -1) abstractRecord.RBSymptomDryEye = Convert.ToInt32(RadioButtonListRBSymptomDryEye.SelectedValue);
                            else abstractRecord.RBSymptomDryEye = 0;
                            if (RadioButtonListRBOcularAlignment.SelectedIndex != -1) abstractRecord.RBOcularAlignment = Convert.ToInt32(RadioButtonListRBOcularAlignment.SelectedValue);
                            else abstractRecord.RBOcularAlignment = 0;
                            if (RadioButtonListRBPtosisProc.SelectedIndex != -1) abstractRecord.RBPtosisProc = Convert.ToInt32(RadioButtonListRBPtosisProc.SelectedValue);
                            else abstractRecord.RBPtosisProc = 0;



                            if (DropDownListMonthOfBirth.SelectedIndex > 0) abstractRecord.MonthOfBirth = Convert.ToInt32(DropDownListMonthOfBirth.SelectedValue);
                            else abstractRecord.MonthOfBirth = 0;
                            if (DropDownListYearOfBirth.SelectedIndex > 0) abstractRecord.YearOfBirth = Convert.ToInt32(DropDownListYearOfBirth.SelectedValue);
                            else abstractRecord.YearOfBirth = 0;



                            if (RadioButtonHistoryPtoticLidEffectYes.Checked) abstractRecord.HistoryPtoticLidEffect = true;
                            if (RadioButtonHistoryPtoticLidEffectNo.Checked) abstractRecord.HistoryPtoticLidEffect = false;
                            if (RadioButtonHistoryOcularYes.Checked) abstractRecord.HistoryOcular = true;
                            if (RadioButtonHistoryOcularNo.Checked) abstractRecord.HistoryOcular = false;
                            if (RadioButtonHistoryOnsetPtosisYes.Checked) abstractRecord.HistoryOnsetPtosis = true;
                            if (RadioButtonHistoryOnsetPtosisNo.Checked) abstractRecord.HistoryOnsetPtosis = false;
                            if (RadioButtonHistoryVariabilityPtosisYes.Checked) abstractRecord.HistoryVariabilityPtosis = true;
                            if (RadioButtonHistoryVariabilityPtosisNo.Checked) abstractRecord.HistoryVariabilityPtosis = false;
                            if (RadioButtonHistoryPreviousLidYes.Checked) abstractRecord.HistoryPreviousLid = true;
                            if (RadioButtonHistoryPreviousLidNo.Checked) abstractRecord.HistoryPreviousLid = false;
                            if (RadioButtonHistoryDiplopiaYes.Checked) abstractRecord.HistoryDiplopia = true;
                            if (RadioButtonHistoryDiplopiaNo.Checked) abstractRecord.HistoryDiplopia = false;
                            if (RadioButtonVisualAcuityDocumentedYes.Checked) abstractRecord.VisualAcuityDocumented = true;
                            if (RadioButtonVisualAcuityDocumentedNo.Checked) abstractRecord.VisualAcuityDocumented = false;
                            if (RadioButtonPupillaryExamYes.Checked) abstractRecord.PupillaryExam = true;
                            if (RadioButtonPupillaryExamNo.Checked) abstractRecord.PupillaryExam = false;
                            if (RadioButtonPtosisVisualFieldYes.Checked) abstractRecord.PtosisVisualField = true;
                            if (RadioButtonPtosisVisualFieldNo.Checked) abstractRecord.PtosisVisualField = false;
                            if (RadioButtonExamCCALYes.Checked) abstractRecord.ExamCCAL = true;
                            if (RadioButtonExamCCALNo.Checked) abstractRecord.ExamCCAL = false;
                            if (RadioButtonAssessTearProductionYes.Checked) abstractRecord.AssessTearProduction = true;
                            if (RadioButtonAssessTearProductionNo.Checked) abstractRecord.AssessTearProduction = false;
                            if (RadioButtonAssessCornealSensationYes.Checked) abstractRecord.AssessCornealSensation = true;
                            if (RadioButtonAssessCornealSensationNo.Checked) abstractRecord.AssessCornealSensation = false;
                            if (RadioButtonAssessBrowPositionYes.Checked) abstractRecord.AssessBrowPosition = true;
                            if (RadioButtonAssessBrowPositionNo.Checked) abstractRecord.AssessBrowPosition = false;
                          
                            if (RadioButtonAssessDermaYes.Checked) abstractRecord.AssessDerma = true;
                            if (RadioButtonAssessDermaNo.Checked) abstractRecord.AssessDerma = false;
                            if (RadioButtonPresenceBellsPhenomYes.Checked) abstractRecord.PresenceBellsPhenom = true;
                            if (RadioButtonPresenceBellsPhenomNo.Checked) abstractRecord.PresenceBellsPhenom = false;
                            if (RadioButtonDocInformedConsentYes.Checked) abstractRecord.DocInformedConsent = true;
                            if (RadioButtonDocInformedConsentNo.Checked) abstractRecord.DocInformedConsent = false;
                            if (RadioButtonOperationSummaryYes.Checked) abstractRecord.OperationSummary = true;
                            if (RadioButtonOperationSummaryNo.Checked) abstractRecord.OperationSummary = false;
                            if (RadioButtonPostopMRDYes.Checked) abstractRecord.PostopMRD = true;
                            if (RadioButtonPostopMRDNo.Checked) abstractRecord.PostopMRD = false;
                            if (RadioButtonPostopCorneaExamYes.Checked) abstractRecord.PostopCorneaExam = true;
                            if (RadioButtonPostopCorneaExamNo.Checked) abstractRecord.PostopCorneaExam = false;
                            if (RadioButtonPostopIncisionStatusYes.Checked) abstractRecord.PostopIncisionStatus = true;
                            if (RadioButtonPostopIncisionStatusNo.Checked) abstractRecord.PostopIncisionStatus = false;
                            if (RadioButtonReoperationYes.Checked) abstractRecord.Reoperation = true;
                            if (RadioButtonReoperationNo.Checked) abstractRecord.Reoperation = false;
                            if (RadioButtonPostopComplicationDailyLivingYes.Checked) abstractRecord.PostopComplicationDailyLiving = true;
                            if (RadioButtonPostopComplicationDailyLivingNo.Checked) abstractRecord.PostopComplicationDailyLiving = false;
                            if (RadioButtonOutcomeLidMarginYes.Checked) abstractRecord.OutcomeLidMargin = true;
                            if (RadioButtonOutcomeLidMarginNo.Checked) abstractRecord.OutcomeLidMargin = false;
                            if (RadioButtonOutcomeActivityImprovedYes.Checked) abstractRecord.OutcomeActivityImproved = true;
                            if (RadioButtonOutcomeActivityImprovedNo.Checked) abstractRecord.OutcomeActivityImproved = false;
                            try
                            {
                                if (TextBoxPostOpFollowedMonths.Text != null)
                                    abstractRecord.PostopFollowedMonths = Convert.ToInt32(TextBoxPostOpFollowedMonths.Text);
                            }
                            catch (Exception ex)
                            {
                                abstractRecord.PostopFollowedMonths = null;
                                TextBoxPostOpFollowedMonths.Text = "";
                            }


                            try
                            {
                            if (TextBoxPostOpFollowedDays.Text != null)
                                abstractRecord.PostofFollowedDays = Convert.ToInt32(TextBoxPostOpFollowedDays.Text);
                            }
                            catch (Exception ex)
                            {
                                abstractRecord.PostofFollowedDays = null;
                                TextBoxPostOpFollowedDays.Text = "";
                            }

                            try
                            {
                            if (TextBoxPreopODMRD1.Text != null)
                            abstractRecord.PreopODMRD1 =Convert.ToDouble(TextBoxPreopODMRD1.Text);
                            }
                            catch (Exception ex)
                            {
                            abstractRecord.PreopODMRD1 = null;
                            TextBoxPreopODMRD1.Text = "";
                            }
                            try
                            {
                            if (TextBoxPreopOSMRD1.Text != null)
                            abstractRecord.PreopOSMRD1 = Convert.ToDouble(TextBoxPreopOSMRD1.Text);
                            }
                            catch (Exception ex)
                            {
                            abstractRecord.PreopOSMRD1 = null;
                            TextBoxPreopOSMRD1.Text = "";
                            }
                            try
                            {
                            if (TextBoxLevatorExcursionOD.Text != null)
                            abstractRecord.LevatorExcursionOD = Convert.ToDouble(TextBoxLevatorExcursionOD.Text);
                            }
                            catch (Exception ex)
                            {
                            abstractRecord.LevatorExcursionOD = null;
                            TextBoxLevatorExcursionOD.Text = "";
                            }
                            try
                            {
                            if (TextBoxLevatorExcursionOS.Text != null)
                            abstractRecord.LevatorExcursionOS = Convert.ToDouble(TextBoxLevatorExcursionOS.Text);
                            }
                            catch (Exception ex)
                            {
                            abstractRecord.LevatorExcursionOS = null;
                            TextBoxLevatorExcursionOS.Text = "";
                            }
                            try
                            {
                            if (TextBoxEtiologyOtherText.Text != null)
                            abstractRecord.EtiologyOtherText = TextBoxEtiologyOtherText.Text;
                            }
                            catch (Exception ex)
                            {
                            abstractRecord.EtiologyOtherText = null;
                            TextBoxEtiologyOtherText.Text = "";
                            }
                            try
                            {
                            if (TextBoxPtosisProcOtherText.Text != null)
                            abstractRecord.PtosisProcOtherText = TextBoxPtosisProcOtherText.Text;
                            }
                            catch (Exception ex)
                            {
                            abstractRecord.PtosisProcOtherText = null;
                            TextBoxPtosisProcOtherText.Text = "";
                            }
                            try
                            {
                            if (TextBoxPostopSTOtherText.Text != null)
                            abstractRecord.PostopSTOtherText = TextBoxPostopSTOtherText.Text;
                            }
                            catch (Exception ex)
                            {
                            abstractRecord.PostopSTOtherText = null;
                            TextBoxPostopSTOtherText.Text = "";
                            }
                            try
                            {
                            if (TextBoxPostopLTOtherText.Text != null)
                            abstractRecord.PostopLTOtherText = TextBoxPostopLTOtherText.Text;
                            }
                            catch (Exception ex)
                            {
                            abstractRecord.PostopLTOtherText = null;
                            TextBoxPostopLTOtherText.Text = "";
                            }




                            abstractRecord.LastUpdateDate = DateTime.Now;
                            bool ChartCompleted = isComplete();
                            abstractRecord.Complete = ChartCompleted;
                            var chartRegistration = (from cr in pim.ChartRegistration
                                                     where cr.ParticipantModuleCycleID == cycle.ParticipantModuleCycleID
                                                      & cr.ModuleID == ModuleID
                                                      & cr.RecordIdentifier == recordIdentifier
                                                     select cr).First<NetHealthPIMModel.ChartRegistration>();
                            chartRegistration.AbstractCompleted = ChartCompleted;


                            if ((DropDownListYearOfBirth.SelectedIndex > 0) && (DropDownListYearOfBirth.SelectedIndex > 0))
                            {
                                chartRegistration.DOB = abstractRecord.MonthOfBirth + "/" + abstractRecord.YearOfBirth;
                            }
                           
                            pim.SaveChanges();

                            transaction.Complete();




            }

            CycleManager.UpdateParticipantCompletedModules(cycleID, ModuleID);
        }


    }


    protected void ButtonSubmit_Click(object sender, EventArgs e)
    {
        savedata();
        isComplete();
        if (!isComplete())
        {
            isComplete();
            string script = " var answer = confirm('Chart data is not complete.\\nClick OK to save entries and exit chart.\\nClick CANCEL to save entries and review the chart. '); if (answer==true) window.location = '../PatientChartRegistration.aspx';";
            ScriptManager.RegisterStartupScript(this, GetType(),
                          "ServerControlScript", script, true);
        }
        else
        {

            Response.Redirect("../PatientChartRegistration.aspx");
        }
    }




    protected bool isComplete()
    {

        bool ChartCompleted = true;

        LabelRBGender.Visible = false;

        LabelRBHistoryCornealSurgery.Visible = false;

        LabelRBSymptomDryEye.Visible = false;

        LabelRBOcularAlignment.Visible = false;

        LabelRBPtosisProc.Visible = false;

        LabelMonthOfBirth.Visible = false;

        LabelYearOfBirth.Visible = false;



        LabelHistoryPtoticLidEffect.Visible = false;

        LabelHistoryOcular.Visible = false;

        LabelHistoryOnsetPtosis.Visible = false;

        LabelHistoryVariabilityPtosis.Visible = false;

        LabelHistoryPreviousLid.Visible = false;

        LabelHistoryDiplopia.Visible = false;

        LabelVisualAcuityDocumented.Visible = false;

        LabelPupillaryExam.Visible = false;

        LabelPtosisVisualField.Visible = false;

        LabelExamCCAL.Visible = false;

        LabelAssessTearProduction.Visible = false;

        LabelAssessCornealSensation.Visible = false;

        LabelAssessBrowPosition.Visible = false;

        LabelAssessEyelidFatigue.Visible = false;

        LabelAssessDerma.Visible = false;

        LabelPresenceBellsPhenom.Visible = false;

        LabelDocInformedConsent.Visible = false;

        LabelOperationSummary.Visible = false;

        LabelPostopMRD.Visible = false;

        LabelPostopCorneaExam.Visible = false;

        LabelPostopIncisionStatus.Visible = false;

        LabelReoperation.Visible = false;

        LabelPostopComplicationDailyLiving.Visible = false;

        LabelOutcomeLidMargin.Visible = false;

        LabelOutcomeActivityImproved.Visible = false;
           



        if (RadioButtonListRBGender.SelectedIndex == -1)
        {
            LabelRBGender.Visible = true;
            ChartCompleted = false;
        }
        if (RadioButtonListRBHistoryCornealSurgery.SelectedIndex == -1)
        {
            LabelRBHistoryCornealSurgery.Visible = true;
            ChartCompleted = false;
        }
        if (RadioButtonListRBSymptomDryEye.SelectedIndex == -1)
        {
            LabelRBSymptomDryEye.Visible = true;
            ChartCompleted = false;
        }
        if (RadioButtonListRBOcularAlignment.SelectedIndex == -1)
        {
            LabelRBOcularAlignment.Visible = true;
            ChartCompleted = false;
        }
        if (RadioButtonListRBPtosisProc.SelectedIndex == -1)
        {
            LabelRBPtosisProc.Visible = true;
            ChartCompleted = false;
        }



        if (DropDownListMonthOfBirth.SelectedIndex == 0)
        {
            LabelMonthOfBirth.Visible = true;
            ChartCompleted = false;
        }
        if (DropDownListYearOfBirth.SelectedIndex == 0)
        {
            LabelYearOfBirth.Visible = true;
            ChartCompleted = false;
        }



        if (RadioButtonHistoryPtoticLidEffectNo.Checked == false && RadioButtonHistoryPtoticLidEffectYes.Checked == false)
        {
            LabelHistoryPtoticLidEffect.Visible = true;
            ChartCompleted = false;
        }
        if (RadioButtonHistoryOcularNo.Checked == false && RadioButtonHistoryOcularYes.Checked == false)
        {
            LabelHistoryOcular.Visible = true;
            ChartCompleted = false;
        }
        if (RadioButtonHistoryOnsetPtosisNo.Checked == false && RadioButtonHistoryOnsetPtosisYes.Checked == false)
        {
            LabelHistoryOnsetPtosis.Visible = true;
            ChartCompleted = false;
        }
        if (RadioButtonHistoryVariabilityPtosisNo.Checked == false && RadioButtonHistoryVariabilityPtosisYes.Checked == false)
        {
            LabelHistoryVariabilityPtosis.Visible = true;
            ChartCompleted = false;
        }
        if (RadioButtonHistoryPreviousLidNo.Checked == false && RadioButtonHistoryPreviousLidYes.Checked == false)
        {
            LabelHistoryPreviousLid.Visible = true;
            ChartCompleted = false;
        }
        if (RadioButtonHistoryDiplopiaNo.Checked == false && RadioButtonHistoryDiplopiaYes.Checked == false)
        {
            LabelHistoryDiplopia.Visible = true;
            ChartCompleted = false;
        }
        if (RadioButtonVisualAcuityDocumentedNo.Checked == false && RadioButtonVisualAcuityDocumentedYes.Checked == false)
        {
            LabelVisualAcuityDocumented.Visible = true;
            ChartCompleted = false;
        }
        if (RadioButtonPupillaryExamNo.Checked == false && RadioButtonPupillaryExamYes.Checked == false)
        {
            LabelPupillaryExam.Visible = true;
            ChartCompleted = false;
        }
        if (RadioButtonPtosisVisualFieldNo.Checked == false && RadioButtonPtosisVisualFieldYes.Checked == false)
        {
            LabelPtosisVisualField.Visible = true;
            ChartCompleted = false;
        }
        if (RadioButtonExamCCALNo.Checked == false && RadioButtonExamCCALYes.Checked == false)
        {
            LabelExamCCAL.Visible = true;
            ChartCompleted = false;
        }
        if (RadioButtonAssessTearProductionNo.Checked == false && RadioButtonAssessTearProductionYes.Checked == false)
        {
            LabelAssessTearProduction.Visible = true;
            ChartCompleted = false;
        }
        if (RadioButtonAssessCornealSensationNo.Checked == false && RadioButtonAssessCornealSensationYes.Checked == false)
        {
            LabelAssessCornealSensation.Visible = true;
            ChartCompleted = false;
        }
        if (RadioButtonAssessBrowPositionNo.Checked == false && RadioButtonAssessBrowPositionYes.Checked == false)
        {
            LabelAssessBrowPosition.Visible = true;
            ChartCompleted = false;
        }
       
        if (RadioButtonAssessDermaNo.Checked == false && RadioButtonAssessDermaYes.Checked == false)
        {
            LabelAssessDerma.Visible = true;
            ChartCompleted = false;
        }
        if (RadioButtonPresenceBellsPhenomNo.Checked == false && RadioButtonPresenceBellsPhenomYes.Checked == false)
        {
            LabelPresenceBellsPhenom.Visible = true;
            ChartCompleted = false;
        }
        if (RadioButtonDocInformedConsentNo.Checked == false && RadioButtonDocInformedConsentYes.Checked == false)
        {
            LabelDocInformedConsent.Visible = true;
            ChartCompleted = false;
        }
        if (RadioButtonOperationSummaryNo.Checked == false && RadioButtonOperationSummaryYes.Checked == false)
        {
            LabelOperationSummary.Visible = true;
            ChartCompleted = false;
        }
        if (RadioButtonPostopMRDNo.Checked == false && RadioButtonPostopMRDYes.Checked == false)
        {
            LabelPostopMRD.Visible = true;
            ChartCompleted = false;
        }
        if (RadioButtonPostopCorneaExamNo.Checked == false && RadioButtonPostopCorneaExamYes.Checked == false)
        {
            LabelPostopCorneaExam.Visible = true;
            ChartCompleted = false;
        }
        if (RadioButtonPostopIncisionStatusNo.Checked == false && RadioButtonPostopIncisionStatusYes.Checked == false)
        {
            LabelPostopIncisionStatus.Visible = true;
            ChartCompleted = false;
        }
        if (RadioButtonReoperationNo.Checked == false && RadioButtonReoperationYes.Checked == false)
        {
            LabelReoperation.Visible = true;
            ChartCompleted = false;
        }
        if (RadioButtonPostopComplicationDailyLivingNo.Checked == false && RadioButtonPostopComplicationDailyLivingYes.Checked == false)
        {
            LabelPostopComplicationDailyLiving.Visible = true;
            ChartCompleted = false;
        }
        if (RadioButtonOutcomeLidMarginNo.Checked == false && RadioButtonOutcomeLidMarginYes.Checked == false)
        {
            LabelOutcomeLidMargin.Visible = true;
            ChartCompleted = false;
        }
        if (RadioButtonOutcomeActivityImprovedNo.Checked == false && RadioButtonOutcomeActivityImprovedYes.Checked == false)
        {
            LabelOutcomeActivityImproved.Visible = true;
            ChartCompleted = false;
        }










        return ChartCompleted;
    }
            
            
            
            
            
            
            
            
            
            }

