﻿<%@ Page Title="" Language="C#" MasterPageFile="~/PIMMasterPage.master" AutoEventWireup="true" CodeFile="TOChart.aspx.cs" Inherits="abo_TOChart" %>

<asp:Content ID="Content1" ContentPlaceHolderID="head" Runat="Server">

    <script type="text/javascript" language="javascript" src="http://ajax.googleapis.com/ajax/libs/jquery/1.7.1/jquery.min.js"></script>
    <link type="text/css" href="../../common/css/atooltip.css" rel="stylesheet"  media="screen" />
	<script type="text/javascript" src="../../common/js/jquery.atooltip.js"></script>
    <script type="text/javascript" language="javascript">
        //      Either enables or disables input boxes
        function enabledControl(el, disabled, checked) {
            //alert("Hello world from enabled control");
            try {
                if (disabled) {
                    el.disabled = disabled;
                    el.setAttribute("disabled", true);
                }
                else {
                    el.disabled = disabled;
                    el.removeAttribute("disabled");
                }

                if (checked == true)
                    el.checked = false;
            }
            catch (E) {
            }
            if (el.childNodes && el.childNodes.length > 0) {
                for (var x = 0; x < el.childNodes.length; x++) {
                    enabledControl(el.childNodes[x], disabled, checked);
                }
            }
        }
        //      Either enables or disables dropdown boxes
        function enabledControlDropDown(el, disabled, checked) {
            //alert("Hello world from enabled control");
            try {
                if (disabled) {
                    el.disabled = disabled;
                    el.setAttribute("disabled", true);
                }
                else {
                    el.disabled = disabled;
                    el.removeAttribute("disabled");
                }

                if (checked == true)
                    el.options[0].selected = true;
            }
            catch (E) {
            }
            if (el.childNodes && el.childNodes.length > 0) {
                for (var x = 0; x < el.childNodes.length; x++) {
                    enabledControl(el.childNodes[x], disabled, checked);
                }
            }
        }
        //      Either enables or disables text boxes
        function enabledControlTextField(el, disabled, checked) {
            //alert("Hello world from enabled control");
            try {
                if (disabled) {
                    el.disabled = disabled;
                    el.setAttribute("disabled", true);
                }
                else {
                    el.disabled = disabled;
                    el.removeAttribute("disabled");
                }

                if (checked == true) {
                    el.value = '';
                }
            }
            catch (E) {
            }
            if (el.childNodes && el.childNodes.length > 0) {
                for (var x = 0; x < el.childNodes.length; x++) {
                    enabledControl(el.childNodes[x], disabled, checked);
                }
            }
        }

        function generate(arr1, arr2, istrue) {
            if (istrue == true) {
                for (var i = 0; i < arr1.length; i++) {
                    document.getElementById(arr1[i]).style.color = "gray";
                }
                for (var i = 0; i < arr2.length; i++) {
                    $(arr2[i] + ' :input').attr('disabled', true);
                    $(arr2[i] + ' :input').removeAttr("checked");
                }
            }
            if (istrue == false) {
                for (var i = 0; i < arr1.length; i++) {
                    document.getElementById(arr1[i]).style.color = "#333";
                }
                for (var i = 0; i < arr2.length; i++) {
                    $(arr2[i] + ' :input').removeAttr('disabled');
                }
            }
        }
    </script>

    <script  type="text/javascript" language="javascript">
        $(document).ready(function() {

        });
    </script>
    <style type="text/css">
        .bginputa{
	        float:right;
	        margin:21px 0 0 1px;
	        background: url(../common/images/orange_button_with_arrow.png) no-repeat;
	        overflow:hidden;
	        height:35px;

        }
        .bginputa a{
	        color: white;
	        text-align:center;
	        height:35px;
	        line-height:28px;
	        padding:0 14px 15px  ;
	        cursor:pointer;
	        float:left;
	        border:none;
	        text-decoration:none;
	        font-family:Arial, Helvetica, sans-serif; 
	        font-size:12px; 
	        font-weight:bold; 
	        letter-spacing: 0px;
        }
        .bginputa a:hover{
	        text-decoration:none;
        }
        .right {
            text-align:right;
            float:right;
        }
    </style>
    <script type="text/javascript">
        $(function() {
            $('#QI1').aToolTip({
                clickIt: true,
                tipContent: 'Important for baseline information'
            });
        });
        $(function() {
            $('#QI3').aToolTip({
                clickIt: true,
                tipContent: 'One of the first indicators of compressive optic neuropathy'
            });
        });
        $(function() {
            $('#QI5').aToolTip({
                clickIt: true,
                tipContent: 'One of the most common diagnostic signs'
            });
        });
        $(function() {
            $('#QM1').aToolTip({
                clickIt: true,
                tipContent: 'Smoking and cigarette smoke have been associated in many studies with worsening of thyroid associated orbitopathy'
            });
        });
        $(function() {
            $('#QM5').aToolTip({
                clickIt: true,
                tipContent: 'Orbital radiation may be contraindicated in diabetics and those with stable disease'
            });
        });
        $(function() {
            $('#QM6').aToolTip({
                clickIt: true,
                tipContent: 'Elective surgical intervention is typically deferred until the disease is stable so as to not reactivate inflammation.  However, some surgical procedures may be necessary (eg orbital decompression, upper lid recession, tarsorrhaphy) during acute phase to prevent loss of function'
            });
        });
    </script>
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder1" Runat="Server">
    <asp:HiddenField ID="HiddenFieldRecordIdentifier" runat="server"/>
        <!--  pim -->
        <div class="pim">
            <div class="record-ident clearfix">
                <h3 class="record-first">RECORD IDENTIFIER: <asp:Literal runat="server" ID="LiteralRecordIdentifier"></asp:Literal></h3>
                <h3 class="record-second"><asp:Literal runat="server" ID="LiteralAbstractionNumber"></asp:Literal></h3>
            </div>
            <!-- History -->
            <table>
                <tr>
                    <th colspan="3" width="910px"><p>History</p></th>
                </tr>
                <!-- Question 1 -->
                <tr class="table_body">
                    <td colspan="2"><strong>1. Date of birth</strong>
                        <asp:Label ID="LabelMonthOfBirth" runat="server" Visible="false"  ForeColor="Red" Text="<br />Please enter Month"></asp:Label>
                        <asp:Label ID="LabelYearOfBirth" runat="server" Visible="false"  ForeColor="Red"  Text="<br />Please enter Year "></asp:Label>
                    </td>
                    <td>
                        <asp:DropDownList ID="DropDownListMonthOfBirth" DataValueField="MonthID" DataTextField="MonthName" runat="server" />
                        <asp:DropDownList ID="DropDownListYearOfBirth" DataValueField="YearID" DataTextField="YearName" runat="server" />
                    </td>
                </tr>
                <!-- Question 2 -->
                <tr class="table_body_bg">
                    <td colspan="2"><strong>2. Gender</strong>
                        <br />
                         <asp:Label ID="LabelRBGender" runat="server" Visible="false"  ForeColor="Red" Text="Please complete"></asp:Label>
                    </td>
                    <td>
                        <asp:RadioButtonList id="RadioButtonListRBGender" RepeatDirection="Horizontal" CssClass="aspxList" runat="server">
                        <asp:ListItem Value="42">&nbsp;Male</asp:ListItem>
                        <asp:ListItem Value="43">&nbsp;Female</asp:ListItem>
                        </asp:RadioButtonList>
                    </td>
                </tr>
                <!-- Question 3 -->
                <tr class="table_body">
                    <td width="50%" rowspan="2"><strong>3. Does the patient have the following symptoms?</strong>
                            <br />
                            <asp:Label ID="LabelSymptomDryness" runat="server" Visible="false"  ForeColor="Red" Text="Please complete Dryness"></asp:Label>
                            <br />
                            <asp:Label ID="LabelSymptomDoubleVision" runat="server" Visible="false"  ForeColor="Red" Text="Please complete Double Vision"></asp:Label>
                    </td>
                    <td width="20%"><span class="right">Dryness, Irritation or Pain?</span></td>
                    <td>
                      
                        <asp:RadioButtonList id="RadioButtonRadioButtonSymptomDryness" RepeatDirection="Horizontal" CssClass="aspxList" runat="server">
	                        <asp:ListItem Value="1">&nbsp;Yes</asp:ListItem>
	                        <asp:ListItem Value="2">&nbsp;No</asp:ListItem>
	                        <asp:ListItem Value="3">&nbsp;Not Documented</asp:ListItem>
                        </asp:RadioButtonList>
                    </td>
                </tr>
                <tr class="table_body">
                    <td><span class="right">Double Vision?</span>        
                            
                    </td>
                    <td>
                        
                        <asp:RadioButtonList id="RadioButtonListSymptomDoubleVision" RepeatDirection="Horizontal" CssClass="aspxList" runat="server">
	                        <asp:ListItem Value="1">&nbsp;Yes</asp:ListItem>
	                        <asp:ListItem Value="2">&nbsp;No</asp:ListItem>
	                        <asp:ListItem Value="3">&nbsp;Not Documented</asp:ListItem>
                        </asp:RadioButtonList>
                    </td>
                </tr>
                <!-- Question 4 -->
                <tr class="table_body_bg">
                    <td colspan="2"><strong>4. Was symptom stability or disease progression-activity reported?</strong>
                           <br />
                            <asp:Label ID="LabelSymptomStability" runat="server" Visible="false"  ForeColor="Red" Text="Please complete"></asp:Label>
                   </td>
                    <td>
                        <asp:RadioButton runat="server" ID="RadioButtonSymptomStabilityYes" GroupName="SymptomStability" text="&nbsp;Yes" />&nbsp;&nbsp;
                        <asp:RadioButton runat="server" ID="RadioButtonSymptomStabilityNo" GroupName="SymptomStability" text="&nbsp;No" />&nbsp;&nbsp;
                    </td>
                </tr>
                <!-- Question 5 -->
                <tr class="table_body">
                    <td colspan="2"><strong>5. Does the patient have a history of systemic thyroid disease?</strong>
                           <br />
                            <asp:Label ID="LabelHistory" runat="server" Visible="false"  ForeColor="Red" Text="Please complete"></asp:Label>
                   </td>
                    <td>
                        <asp:CheckBox runat="server" ID="CheckBoxHistorySysHyperThyroid" text="&nbsp;Hyperthyroidism" />
                        <br /><asp:CheckBox runat="server" ID="CheckBoxHistorySysHypoThyroid" text="&nbsp;Hypothyroidism" />
                        <br /><asp:CheckBox runat="server" ID="CheckBoxHistoryHashimoto" text="&nbsp;Hashimoto's disease" />
                        <br /><asp:CheckBox runat="server" ID="CheckBoxHistoryEuthyroid" text="&nbsp;Euthyroid" />
                        <br /><asp:CheckBox runat="server" ID="CheckBoxHistoryND" text="&nbsp;Not Documented" />
                    </td>
                </tr>
                <!-- Question 6 -->
                <tr class="table_body_bg">
                    <td colspan="2"><strong>6. Were thyroid function tests previously obtained or scheduled by yourself or another physician?</strong>
                            <br />
                            <asp:Label ID="LabelHistoryMostRecent" runat="server" Visible="false"  ForeColor="Red" Text="Please complete"></asp:Label>
                    </td>
                    <td>
                        <asp:RadioButton runat="server" ID="RadioButtonHistoryMostRecentYes" GroupName="HistoryMostRecent" text="&nbsp;Yes" />&nbsp;&nbsp;
                        <asp:RadioButton runat="server" ID="RadioButtonHistoryMostRecentNo" GroupName="HistoryMostRecent" text="&nbsp;No" />&nbsp;&nbsp;
                    </td>
                </tr>
                <!-- Question 7 -->
                <tr class="table_body">
                    <td colspan="2"><strong>7. What is the patient's smoking history?</strong>
                            <br />
                            <asp:Label ID="LabelRBSmoking" runat="server" Visible="false"  ForeColor="Red" Text="Please complete"></asp:Label>
                    </td>
                    <td>
                        <asp:RadioButtonList id="RadioButtonListRBSmoking" RepeatDirection="Vertical" CssClass="aspxList" runat="server">
                        <asp:ListItem Value="91">&nbsp;Current Smoker</asp:ListItem>
                        <asp:ListItem Value="92">&nbsp;Previous Smoker</asp:ListItem>
                        <asp:ListItem Value="93">&nbsp;Never Smoked</asp:ListItem>
                        <asp:ListItem Value="3">&nbsp;Not Documented</asp:ListItem>
                        </asp:RadioButtonList>
                    </td>
                </tr>
            </table>
            <br />
            <!-- Initial Examination -->
            <table>
                <tr>
                    <th colspan="2" width="910px"><p>Initial Examination</p></th>
                </tr>
                <!-- Question 1 -->
                <tr class="table_body">
                    <td width="70%"><strong>1. Was the best corrected (refraction or pinhole) visual acuity recorded?</strong>&nbsp;<a href="#"><img id="QI1" src="../../common/images/tip.gif" alt="Tip" /></a>
                            <br />
                            <asp:Label ID="LabelBCVA" runat="server" Visible="false"  ForeColor="Red" Text="Please complete"></asp:Label>
                    </td>
                    <td>
                        <asp:RadioButtonList id="RadioButtonListBCVA" RepeatDirection="Horizontal" CssClass="aspxList" runat="server">
                        <asp:ListItem Value="1">&nbsp;Yes</asp:ListItem>
                        <asp:ListItem Value="2">&nbsp;No</asp:ListItem>
                        </asp:RadioButtonList>
                    </td>
                </tr>
                <!-- Question 2 -->
                <tr class="table_body_bg">
                    <td><strong>2. Was there an afferent pupillary defect?</strong>
                            <br />
                            <asp:Label ID="LabelRBAfferentPapillaryDefect" runat="server" Visible="false"  ForeColor="Red" Text="Please complete"></asp:Label>
                    </td>
                    <td>
                        <asp:RadioButtonList id="RadioButtonListRBAfferentPapillaryDefect" RepeatDirection="Horizontal" CssClass="aspxList" runat="server">
                        <asp:ListItem Value="1">&nbsp;Yes</asp:ListItem>
                        <asp:ListItem Value="2">&nbsp;No</asp:ListItem>
                        <asp:ListItem Value="3">&nbsp;Not Documented</asp:ListItem>
                        </asp:RadioButtonList>
                    </td>
                </tr>
                <!-- Question 3 -->
                <tr class="table_body">
                    <td><strong>3. Was color vision tested and/or visual fields obtained?</strong>&nbsp;<a href="#"><img id="QI3" src="../../common/images/tip.gif" alt="Tip" /></a>
                            <br />
                            <asp:Label ID="LabelColorVisionVisualFields" runat="server" Visible="false"  ForeColor="Red" Text="Please complete"></asp:Label>
                    </td>
                    <td>
                        <asp:RadioButton runat="server" ID="RadioButtonColorVisionVisualFieldsYes" GroupName="ColorVisionVisualFields" text="&nbsp;Yes" />&nbsp;&nbsp;
                        <asp:RadioButton runat="server" ID="RadioButtonColorVisionVisualFieldsNo" GroupName="ColorVisionVisualFields" text="&nbsp;No" />&nbsp;&nbsp;
                    </td>
                </tr>
                <!-- Question 4 -->
                <tr class="table_body_bg">
                    <td><strong>4. Was motility limited?</strong>
                            <br />
                            <asp:Label ID="LabelRBMotilityRestricted" runat="server" Visible="false"  ForeColor="Red" Text="Please complete"></asp:Label>
                   </td>
                    <td>
                        <asp:RadioButtonList id="RadioButtonListRBMotilityRestricted" RepeatDirection="Horizontal" CssClass="aspxList" runat="server">
                        <asp:ListItem Value="1">&nbsp;Yes</asp:ListItem>
                        <asp:ListItem Value="2">&nbsp;No</asp:ListItem>
                        <asp:ListItem Value="3">&nbsp;Not Documented</asp:ListItem>
                       </asp:RadioButtonList>
                    </td>
                </tr>
                <!-- Question 5 -->
                <tr class="table_body">
                    <td><strong>5. Was upper lid retraction present (may be recorded as MRD1 and MRD2)?</strong>&nbsp;<a href="#"><img id="QI5" src="../../common/images/tip.gif" alt="Tip" /></a>
                             <br />
                            <asp:Label ID="LabelRBUpperLidRetraction" runat="server" Visible="false"  ForeColor="Red" Text="Please complete"></asp:Label>
                   </td>
                    <td>
                        <asp:RadioButtonList id="RadioButtonListRBUpperLidRetraction" RepeatDirection="Horizontal" CssClass="aspxList" runat="server">
                        <asp:ListItem Value="1">&nbsp;Yes</asp:ListItem>
                        <asp:ListItem Value="2">&nbsp;No</asp:ListItem>
                        <asp:ListItem Value="3">&nbsp;Not Documented</asp:ListItem>
                        </asp:RadioButtonList>
                    </td>
                </tr>
                <!-- Question 6 -->
                <tr class="table_body_bg">
                    <td><strong>6. Were exophthalmometry measurements recorded?</strong>
                              <br />
                            <asp:Label ID="LabelExophthalmometry" runat="server" Visible="false"  ForeColor="Red" Text="Please complete"></asp:Label>
                   </td>
                    <td>
                        <asp:RadioButtonList id="RadioButtonListRBExophthalmometry" RepeatDirection="Horizontal" CssClass="aspxList" runat="server">
	                        <asp:ListItem Value="1">&nbsp;Yes</asp:ListItem>
	                        <asp:ListItem Value="2">&nbsp;No</asp:ListItem>
	                        <asp:ListItem Value="3">&nbsp;Not Documented</asp:ListItem>
                        </asp:RadioButtonList>
                    </td>
                </tr>
                <!-- Question 7 -->
                <tr class="table_body">
                    <td><strong>7. Did the patient have chemosis or conjunctival injection?</strong>
                              <br />
                            <asp:Label ID="LabelChemosisInjection" runat="server" Visible="false"  ForeColor="Red" Text="Please complete"></asp:Label>
                   </td>
                    <td>
                        <asp:RadioButton runat="server" ID="RadioButtonChemosisInjectionYes" GroupName="ChemosisInjection" text="&nbsp;Yes" />&nbsp;&nbsp;
                        <asp:RadioButton runat="server" ID="RadioButtonChemosisInjectionNo" GroupName="ChemosisInjection" text="&nbsp;No" />&nbsp;&nbsp;
                    </td>
                </tr>
                <!-- Question 8 -->
                <tr class="table_body_bg">
                    <td><strong>8. Was there keratopathy in either eye?</strong>
                              <br />
                            <asp:Label ID="LabelRBKeratopathy" runat="server" Visible="false"  ForeColor="Red" Text="Please complete"></asp:Label>
                   </td>
                    <td>
                        <asp:RadioButtonList id="RadioButtonListRBKeratopathy" RepeatDirection="Horizontal" CssClass="aspxList" runat="server">
                        <asp:ListItem Value="1">&nbsp;Yes</asp:ListItem>
                        <asp:ListItem Value="2">&nbsp;No</asp:ListItem>
                        <asp:ListItem Value="3">&nbsp;Not Documented</asp:ListItem>
                        </asp:RadioButtonList>
                    </td>
                </tr>
                <!-- Question 9 -->
                <tr class="table_body">
                    <td><strong>9. Was intraocular pressure elevated in either eye (&gt;21)?</strong>
                              <br />
                            <asp:Label ID="LabelRBIOPElevated" runat="server" Visible="false"  ForeColor="Red" Text="Please complete"></asp:Label>
                  </td>
                    <td>
                        <asp:RadioButtonList id="RadioButtonListRBIOPElevated" RepeatDirection="Horizontal" CssClass="aspxList" runat="server">
                        <asp:ListItem Value="1">&nbsp;Yes</asp:ListItem>
                        <asp:ListItem Value="2">&nbsp;No</asp:ListItem>
                        <asp:ListItem Value="3">&nbsp;Not Documented</asp:ListItem>
                        </asp:RadioButtonList>
                    </td>
                </tr>
                <!-- Question 9a -->
                <tr class="table_body">
                    <td><strong>9a. Was lid swelling and/or erythema present?</strong>
                  </td>
                    <td>
                        <asp:RadioButton runat="server" ID="RadioButtonLidSwellingPresentYes" GroupName="LidSwellingPresent" text="&nbsp;Yes" />&nbsp;&nbsp;
                        <asp:RadioButton runat="server" ID="RadioButtonLidSwellingPresentNo" GroupName="LidSwellingPresent" text="&nbsp;No" />&nbsp;&nbsp;
                    </td>
                </tr>
                <!-- Question 10 -->
                <tr class="table_body_bg">
                    <td><strong>10. Were there any optic nerve abnormalities?</strong>
                              <br />
                            <asp:Label ID="LabelRBOpticNerveAbnormalities" runat="server" Visible="false"  ForeColor="Red" Text="Please complete"></asp:Label>
                    </td>
                    <td>
                        <asp:RadioButtonList id="RadioButtonListRBOpticNerveAbnormalities" RepeatDirection="Horizontal" CssClass="aspxList" runat="server">
                        <asp:ListItem Value="1">&nbsp;Yes</asp:ListItem>
                        <asp:ListItem Value="2">&nbsp;No</asp:ListItem>
                        <asp:ListItem Value="132">&nbsp;Not Examined</asp:ListItem>
                        </asp:RadioButtonList>
                    </td>
                </tr>
            </table>
            <br />
            <!-- Management -->
            <table>
                <tr>
                    <th colspan="2" width="910px"><p>Management</p></th>
                </tr>
                <!-- Question 1 -->
                <tr class="table_body">
                    <td width="70%"><strong>1. Was the importance of smoking avoidance discussed?</strong>&nbsp;<a href="#"><img id="QM1" src="../../common/images/tip.gif" alt="Tip" /></a>
                              <br />
                            <asp:Label ID="LabelRBSmokingDiscussion" runat="server" Visible="false"  ForeColor="Red" Text="Please complete"></asp:Label>
                    </td>
                    <td>
                        <asp:RadioButtonList id="RadioButtonListRBSmokingDiscussion" RepeatDirection="Horizontal" CssClass="aspxList" runat="server">
                        <asp:ListItem Value="1">&nbsp;Yes</asp:ListItem>
                        <asp:ListItem Value="2">&nbsp;No</asp:ListItem>
                        <asp:ListItem Value="48">&nbsp;Not Applicable</asp:ListItem>
                        </asp:RadioButtonList>
                    </td>
                </tr>
                <!-- Question 2 -->
                <tr class="table_body_bg">
                    <td><strong>2. Were thyroid function tests performed during the follow-up period (by you or any other provider)?</strong>
                              <br />
                            <asp:Label ID="LabelRBThyroidFunctionTest" runat="server" Visible="false"  ForeColor="Red" Text="Please complete"></asp:Label>
                    </td>
                    <td>
                        <asp:RadioButtonList id="RadioButtonListRBThyroidFunctionTest" RepeatDirection="Horizontal" CssClass="aspxList" runat="server">
                        <asp:ListItem Value="1">&nbsp;Yes</asp:ListItem>
                        <asp:ListItem Value="2">&nbsp;No</asp:ListItem>
                        <asp:ListItem Value="3">&nbsp;Not Documented</asp:ListItem>
                        </asp:RadioButtonList>
                    </td>
                </tr>
                <!-- Question 3 -->
                <tr class="table_body">
                    <td><strong>3. Was the need for ocular lubrication discussed?</strong>
                              <br />
                            <asp:Label ID="LabelRBOcularLubrication" runat="server" Visible="false"  ForeColor="Red" Text="Please complete"></asp:Label>
                    </td>
                    <td>
                        <asp:RadioButtonList id="RadioButtonListRBOcularLubrication" RepeatDirection="Horizontal" CssClass="aspxList" runat="server">
                        <asp:ListItem Value="1">&nbsp;Yes</asp:ListItem>
                        <asp:ListItem Value="2">&nbsp;No</asp:ListItem>
                        <asp:ListItem Value="48">&nbsp;Not Applicable</asp:ListItem>
                         </asp:RadioButtonList>
                    </td>
                </tr>
                <!-- Question 4 -->
                <tr class="table_body_bg">
                    <td><strong>4. Was the patient treated with steroids (oral, injectable, intravenous) or other immunomodulator agents?</strong>
                              <br />
                            <asp:Label ID="LabelTreatmentSteroids" runat="server" Visible="false"  ForeColor="Red" Text="Please complete"></asp:Label>
                    </td>
                    <td>
                        <asp:RadioButton runat="server" ID="RadioButtonTreatmentSteroidsYes" GroupName="TreatmentSteroids" text="&nbsp;Yes" />&nbsp;&nbsp;
                        <asp:RadioButton runat="server" ID="RadioButtonTreatmentSteroidsNo" GroupName="TreatmentSteroids" text="&nbsp;No" />&nbsp;&nbsp;
                    </td>
                </tr>
                <!-- Question 5 -->
                <tr class="table_body">
                    <td><strong>5. Was orbital radiation performed?</strong>&nbsp;<a href="#"><img id="QM5" src="../../common/images/tip.gif" alt="Tip" /></a>
                               <br />
                            <asp:Label ID="LabelOrbitalRadiation" runat="server" Visible="false"  ForeColor="Red" Text="Please complete"></asp:Label>
                    </td>
                    <td>
                        <asp:RadioButton runat="server" ID="RadioButtonOrbitalRadiationYes" GroupName="OrbitalRadiation" text="&nbsp;Yes" />&nbsp;&nbsp;
                        <asp:RadioButton runat="server" ID="RadioButtonOrbitalRadiationNo" GroupName="OrbitalRadiation" text="&nbsp;No" />&nbsp;&nbsp;
                    </td>
                </tr>
                <!-- Question 6 -->
                <tr class="table_body_bg">
                    <td><strong>6. Was the duration of stability or surgical indication documented prior to surgical intervention?</strong>&nbsp;<a href="#"><img id="QM6" src="../../common/images/tip.gif" alt="Tip" /></a>
                               <br />
                            <asp:Label ID="LabelRBDurationStability" runat="server" Visible="false"  ForeColor="Red" Text="Please complete"></asp:Label>
                    </td>
                    <td>
                        <asp:RadioButtonList id="RadioButtonListRBDurationStability" RepeatDirection="Horizontal" CssClass="aspxList" runat="server">
                        <asp:ListItem Value="1">&nbsp;Yes</asp:ListItem>
                        <asp:ListItem Value="2">&nbsp;No</asp:ListItem>
                        <asp:ListItem Value="48">&nbsp;Not Applicable</asp:ListItem>
                        </asp:RadioButtonList>
                    </td>
                </tr>
                <!-- Question 7 -->
                <tr class="table_body">
                    <td><strong>7. Was a surgical decompression performed?</strong>
                               <br />
                            <asp:Label ID="LabelSurgicalDecompression" runat="server" Visible="false"  ForeColor="Red" Text="Please complete"></asp:Label>
                   </td>
                    <td>
                        <asp:RadioButton runat="server" ID="RadioButtonSurgicalDecompressionYes" GroupName="SurgicalDecompression" text="&nbsp;Yes" />&nbsp;&nbsp;
                        <asp:RadioButton runat="server" ID="RadioButtonSurgicalDecompressionNo" GroupName="SurgicalDecompression" text="&nbsp;No" />&nbsp;&nbsp;
                    </td>
                </tr>
                <!-- Question 8 -->
                <tr class="table_body_bg">
                    <td><strong>8. Was strabismus surgery performed by you or another care provider?</strong>
                                 <br />
                            <asp:Label ID="LabelStrabismusSurgery" runat="server" Visible="false"  ForeColor="Red" Text="Please complete"></asp:Label>
                   </td>
                    <td>
                        <asp:RadioButton runat="server" ID="RadioButtonStrabismusSurgeryYes" GroupName="StrabismusSurgery" text="&nbsp;Yes" />&nbsp;&nbsp;
                        <asp:RadioButton runat="server" ID="RadioButtonStrabismusSurgeryNo" GroupName="StrabismusSurgery" text="&nbsp;No" />&nbsp;&nbsp;
                    </td>
                </tr>
                <!-- Question 9 -->
                <tr class="table_body">
                    <td><strong>9. Was upper or lower eyelid retraction repaired?</strong>
                                 <br />
                            <asp:Label ID="LabelEyelidRetractionRepair" runat="server" Visible="false"  ForeColor="Red" Text="Please complete"></asp:Label>
                  </td>
                    <td> 
                        <asp:RadioButton runat="server" ID="RadioButtonEyelidRetractionRepairYes" GroupName="EyelidRetractionRepair" text="&nbsp;Yes" />&nbsp;&nbsp;
                        <asp:RadioButton runat="server" ID="RadioButtonEyelidRetractionRepairNo" GroupName="EyelidRetractionRepair" text="&nbsp;No" />&nbsp;&nbsp;
                    </td>
                </tr>
            </table>
            <br />
            <!-- Outcomes -->
            <table>
                <tr>
                    <th colspan="2" width="910px"><p>Outcomes</p></th>
                </tr>
                <!-- Question 1 -->
                <tr class="table_body">
                    <td width="70%"><strong>1. Did the patient stop or significantly decrease smoking?</strong>
                                 <br />
                            <asp:Label ID="LabelRBStopSmoking" runat="server" Visible="false"  ForeColor="Red" Text="Please complete"></asp:Label>
                    </td>
                    <td>
                        <asp:RadioButtonList id="RadioButtonListRBStopSmoking" RepeatDirection="Horizontal" CssClass="aspxList" runat="server">
                        <asp:ListItem Value="1">&nbsp;Yes</asp:ListItem>
                        <asp:ListItem Value="2">&nbsp;No</asp:ListItem>
                        <asp:ListItem Value="94">&nbsp;Non-Smoker</asp:ListItem>
                        </asp:RadioButtonList>
                    </td>
                </tr>
                <!-- Question 2 -->
                <tr class="table_body_bg">
                    <td><strong>2. Was ocular comfort improved?</strong>
                                <br />
                            <asp:Label ID="LabelRBOcularComfortImproved" runat="server" Visible="false"  ForeColor="Red" Text="Please complete"></asp:Label>
                    </td>
                    <td>
                        <asp:RadioButtonList id="RadioButtonListRBOcularComfortImproved" RepeatDirection="Horizontal" CssClass="aspxList" runat="server">
                        <asp:ListItem Value="1">&nbsp;Yes</asp:ListItem>
                        <asp:ListItem Value="2">&nbsp;No</asp:ListItem>
                        <asp:ListItem Value="48">&nbsp;Not Applicable</asp:ListItem>
                        </asp:RadioButtonList>
                    </td>
                </tr>
                <!-- Question 3 -->
                <tr class="table_body">
                    <td><strong>3. Was diplopia improved?</strong>
                             <br />
                            <asp:Label ID="LabelRBDiplopiaImproved" runat="server" Visible="false"  ForeColor="Red" Text="Please complete"></asp:Label>
                    </td>
                    <td>
                        <asp:RadioButtonList id="RadioButtonListRBDiplopiaImproved" RepeatDirection="Horizontal" CssClass="aspxList" runat="server">
                        <asp:ListItem Value="1">&nbsp;Yes</asp:ListItem>
                        <asp:ListItem Value="2">&nbsp;No</asp:ListItem>
                        <asp:ListItem Value="48">&nbsp;Not Applicable</asp:ListItem>
                        </asp:RadioButtonList>
                    </td>
                </tr>
                <!-- Question 4 -->
                <tr class="table_body_bg">
                    <td><strong>4. Do you feel treatment improved patient’s quality of life?</strong>
                           <br />
                            <asp:Label ID="LabelImprovedQualityLife" runat="server" Visible="false"  ForeColor="Red" Text="Please complete"></asp:Label>
                    </td>
                    <td>
                        <asp:RadioButton runat="server" ID="RadioButtonImprovedQualityLifeYes" GroupName="ImprovedQualityLife" text="&nbsp;Yes" />&nbsp;&nbsp;
                        <asp:RadioButton runat="server" ID="RadioButtonImprovedQualityLifeNo" GroupName="ImprovedQualityLife" text="&nbsp;No" />&nbsp;&nbsp;
                    </td>
                </tr>
                <!-- Question 5 -->
                <tr class="table_body">
                    <td><strong>5. The overall quality of vision (acuity, field) was:</strong>
                         <br />
                            <asp:Label ID="LabelRBQualityVision" runat="server" Visible="false"  ForeColor="Red" Text="Please complete"></asp:Label>
                   </td>
                    <td>
                        <asp:RadioButtonList id="RadioButtonListRBQualityVision" RepeatDirection="Vertical" CssClass="aspxList" runat="server">
                        <asp:ListItem Value="95">&nbsp;Improved</asp:ListItem>
                        <asp:ListItem Value="133">&nbsp;Stable</asp:ListItem>
                        <asp:ListItem Value="134">&nbsp;Worsened</asp:ListItem>
                        </asp:RadioButtonList>

                    </td>
                </tr>
            </table>
            <div class="button-box">
                <asp:LinkButton ID="LinkButtonBackToDashboard" runat="server" Text="Back To Chart Registration" PostBackUrl="../PatientChartRegistration.aspx?CycleNumber=1" Visible="false" CssClass="button" />
                <asp:LinkButton ID="ButtonSubmit"  OnClick="ButtonSubmit_Click" runat="server" Text="Submit Chart" CssClass="button" />
            </div>
        </div>
        <!-- ION pim ends -->
</asp:Content>

