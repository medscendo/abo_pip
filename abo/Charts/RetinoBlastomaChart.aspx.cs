﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Transactions;
using NetHealthPIMModel;

public partial class abo_RetinoBlastomaChart : BasePage
{
    protected void Page_Load(object sender, EventArgs e)
    {
         if (!IsPostBack)
        {
            InitializePage();
        }


    }
    protected void InitializePage()
    {
        ((PIMMasterPage)Page.Master).SetTitle("Chart");

        using (NetHealthPIMEntities pim = new NetHealthPIMEntities())
        {
            ParticipantModuleCycle prev = (ParticipantModuleCycle)Session[Constants.SESSION_PREVIOUSCYCLE];
            String CycleNumber = Request.QueryString["CycleNumber"];

            int cycleID = ctx.ActiveModuleCycleID;
            if (CycleNumber == "1")
            {
                cycleID = prev.ParticipantModuleCycleID;
                LinkButtonBackToDashboard.Visible = true;
                ButtonSubmit.Visible = false;
            }

            ParticipantModuleCycle cycle = pim.ParticipantModuleCycle.First(c => c.ParticipantModuleCycleID == cycleID);
            DropDownListMonthOfBirth.DataSource = CycleManager.getMonthList();
            DropDownListYearOfBirth.DataSource = CycleManager.getDOBYearList(0, 100);
            DropDownListMonthOfFirstSymptoms.DataSource = CycleManager.getMonthList();
            DropDownListYearOfFirstSymptoms.DataSource=CycleManager.getDOBYearList(0, 100);
            DropDownListMonthOfTreatment.DataSource = CycleManager.getMonthList(); 
            DropDownListYearOfTreatment.DataSource = CycleManager.getDOBYearList(0, 100);
            DataBind();
            DropDownListYearOfBirth.Items.Insert(0, new ListItem("Year", "0"));
            DropDownListYearOfTreatment.Items.Insert(0, new ListItem("Year", "0"));
            DropDownListYearOfFirstSymptoms.Items.Insert(0, new ListItem("Year", "0"));
            string recordIdentifier = Request.QueryString["ri"];
            HiddenFieldRecordIdentifier.Value = recordIdentifier;
            LiteralRecordIdentifier.Text = recordIdentifier;
            var count = pim. ChartAbstractionRB.Where(ps => ps.ParticipantModuleCycle.ParticipantModuleCycleID == cycleID & ps.RecordIdentifier == recordIdentifier).Count();
            if (count == 0)
            {
                using (TransactionScope transaction = new TransactionScope())
                {
                    //Module module = pim.Module.First(m => m.ModuleID == Constants.ABO_AMBLYOPIA_MODULEID);

                    NetHealthPIMModel.ChartRegistration chartRegistration = (from cr in pim.ChartRegistration
                                                           where cr.ParticipantModuleCycleID == cycleID
                                                           & cr.RecordIdentifier == recordIdentifier
                                                           & cr.ModuleID == 42
                                                           select cr).FirstOrDefault<NetHealthPIMModel.ChartRegistration>();

                    DateTime initialVisit = Convert.ToDateTime(chartRegistration.InitialVisit);
                    DateTime DOB = Convert.ToDateTime(chartRegistration.DOB);
                      ChartAbstractionRB abstractRecord = new   ChartAbstractionRB();
                    abstractRecord.ParticipantModuleCycle = cycle;
                    abstractRecord.RecordIdentifier = recordIdentifier;
                    abstractRecord.MonthOfBirth = DOB.Month;
                    abstractRecord.YearOfBirth = DOB.Year;
                    abstractRecord.MonthOfTreatment = initialVisit.Month;
                    abstractRecord.YearOfTreatment = initialVisit.Year;
                    DropDownListYearOfBirth.SelectedValue = abstractRecord.YearOfBirth.ToString();
                    DropDownListMonthOfBirth.SelectedValue = abstractRecord.MonthOfBirth.ToString();

                    DropDownListMonthOfTreatment.SelectedValue = abstractRecord.MonthOfTreatment.ToString();
                    DropDownListYearOfTreatment.SelectedValue = abstractRecord.YearOfTreatment.ToString();
                    abstractRecord.CreatedOn = DateTime.Now;
                    abstractRecord.LastUpdateDate = DateTime.Now;
                    pim.SaveChanges();

                    transaction.Complete();
                }
            }
            else
            {
                ChartAbstractionRB abstractRecord = (from c in pim.ChartAbstractionRB
                                                          where c.ParticipantModuleCycle.ParticipantModuleCycleID == cycleID & c.RecordIdentifier == recordIdentifier
                                                          select c).First<ChartAbstractionRB>();


                if (abstractRecord.FirstSymptomsDateNA != null) CheckBoxFirstSymptomsDateNA.Checked = ((bool)abstractRecord.FirstSymptomsDateNA);
                if (abstractRecord.FundusDocDrawing != null) CheckBoxFundusDocDrawing.Checked = ((bool)abstractRecord.FundusDocDrawing);
                if (abstractRecord.FundusDocPhotography != null) CheckBoxFundusDocPhotography.Checked = ((bool)abstractRecord.FundusDocPhotography);
                if (abstractRecord.OpticNerveImagedCT != null) CheckBoxOpticNerveImagedCT.Checked = ((bool)abstractRecord.OpticNerveImagedCT);
                if (abstractRecord.OpticNerveImagedMRI != null) CheckBoxOpticNerveImagedMRI.Checked = ((bool)abstractRecord.OpticNerveImagedMRI);
                if (abstractRecord.OpticNerveNotImaged != null) CheckBoxOpticNerveNotImaged.Checked = ((bool)abstractRecord.OpticNerveNotImaged);



                if (abstractRecord.RBGender != null) RadioButtonListRBGender.SelectedValue = abstractRecord.RBGender.ToString();
                if (abstractRecord.RBVisitReason != null) RadioButtonListRBVisitReason.SelectedValue = abstractRecord.RBVisitReason.ToString();
                if (abstractRecord.RBComplaintDocumented != null) RadioButtonListRBComplaintDocumented.SelectedValue = abstractRecord.RBComplaintDocumented.ToString();
                if (abstractRecord.RBTumorStageOD != null) RadioButtonListRBTumorStageOD.SelectedValue = abstractRecord.RBTumorStageOD.ToString();
                if (abstractRecord.RBPrimaryTreatmentModalityOD != null) RadioButtonListRBPrimaryTreatmentModalityOD.SelectedValue = abstractRecord.RBPrimaryTreatmentModalityOD.ToString();
                if (abstractRecord.RBTumorStageOS != null) RadioButtonListRBTumorStageOS.SelectedValue = abstractRecord.RBTumorStageOS.ToString();
                if (abstractRecord.RBPrimaryTreatmentModalityOS != null) RadioButtonListRBPrimaryTreatmentModalityOS.SelectedValue = abstractRecord.RBPrimaryTreatmentModalityOS.ToString();
                if (abstractRecord.RBDocGeneticScreening != null) RadioButtonListRBDocGeneticScreening.SelectedValue = abstractRecord.RBDocGeneticScreening.ToString();



                if (abstractRecord.MonthOfBirth != null) DropDownListMonthOfBirth.SelectedValue = abstractRecord.MonthOfBirth.ToString();
                if (abstractRecord.YearOfBirth != null) DropDownListYearOfBirth.SelectedValue = abstractRecord.YearOfBirth.ToString();
                if (abstractRecord.MonthOfTreatment != null) DropDownListMonthOfTreatment.SelectedValue = abstractRecord.MonthOfTreatment.ToString();
                if (abstractRecord.YearOfTreatment != null) DropDownListYearOfTreatment.SelectedValue = abstractRecord.YearOfTreatment.ToString();
                if (abstractRecord.MonthOfFirstSymptoms != null) DropDownListMonthOfFirstSymptoms.SelectedValue = abstractRecord.MonthOfFirstSymptoms.ToString();
                if (abstractRecord.YearOfFirstSymptoms != null) DropDownListYearOfFirstSymptoms.SelectedValue = abstractRecord.YearOfFirstSymptoms.ToString();
                if (abstractRecord.TextBoxTumorStageOSOther != null) TextBoxTumorStageOSOther.SelectedValue = abstractRecord.TextBoxTumorStageOSOther.ToString();
                if (abstractRecord.TextBoxRBPrimaryTreatmentOtherOD != null) TextBoxTumorStageODOther.SelectedValue = abstractRecord.TextBoxRBPrimaryTreatmentOtherOD.ToString();

                if (abstractRecord.EchographyPerformed != null)
                {
                    RadioButtonEchographyPerformedYes.Checked = ((bool)abstractRecord.EchographyPerformed);
                    RadioButtonEchographyPerformedNo.Checked = !((bool)abstractRecord.EchographyPerformed);
                }
                if (abstractRecord.DocInformedConsent != null)
                {
                    RadioButtonDocInformedConsentYes.Checked = ((bool)abstractRecord.DocInformedConsent);
                    RadioButtonDocInformedConsentNo.Checked = !((bool)abstractRecord.DocInformedConsent);
                }
                if (abstractRecord.DocPCP != null)
                {
                    RadioButtonDocPCPYes.Checked = ((bool)abstractRecord.DocPCP);
                    RadioButtonDocPCPNo.Checked = !((bool)abstractRecord.DocPCP);
                }
                if (abstractRecord.DocPathologicEval != null)
                {
                    RadioButtonDocPathologicEvalYes.Checked = ((bool)abstractRecord.DocPathologicEval);
                    RadioButtonDocPathologicEvalNo.Checked = !((bool)abstractRecord.DocPathologicEval);
                }
                if (abstractRecord.GlobSparingEnucleated != null)
                {
                    RadioButtonGlobSparingEnucleatedYes.Checked = ((bool)abstractRecord.GlobSparingEnucleated);
                    RadioButtonGlobSparingEnucleatedNo.Checked = !((bool)abstractRecord.GlobSparingEnucleated);
                }
                if (abstractRecord.DocGeneticCounseling != null)
                {
                    RadioButtonDocGeneticCounselingYes.Checked = ((bool)abstractRecord.DocGeneticCounseling);
                    RadioButtonDocGeneticCounselingNo.Checked = !((bool)abstractRecord.DocGeneticCounseling);
                }
                if (abstractRecord.RBPrimaryTreatmentOtherOD != null) TextBoxRBPrimaryTreatmentOtherOD.Text = abstractRecord.RBPrimaryTreatmentOtherOD.ToString();
                if (abstractRecord.RBPrimaryTreatmentOtherOS != null) TextBoxRBPrimaryTreatmentOtherOS.Text = abstractRecord.RBPrimaryTreatmentOtherOS.ToString();










            }
        }
    }
    public void savedata()
    {
        using (NetHealthPIMEntities pim = new NetHealthPIMEntities())
        {
            int ModuleID = (int)Session[Constants.SESSION_WORKINGMODULEID];
            int cycleID = ctx.ActiveModuleCycleID;
            ParticipantModuleCycle cycle = pim.ParticipantModuleCycle.First(c => c.ParticipantModuleCycleID == cycleID);
            string recordIdentifier = HiddenFieldRecordIdentifier.Value;
            using (TransactionScope transaction = new TransactionScope())
            {
                ChartAbstractionRB abstractRecord = (from c in pim.ChartAbstractionRB
                                                         where c.ParticipantModuleCycle.ParticipantModuleCycleID == cycleID & c.RecordIdentifier == recordIdentifier
                                                         select c).First<ChartAbstractionRB>();


                abstractRecord.FirstSymptomsDateNA = CheckBoxFirstSymptomsDateNA.Checked;
                abstractRecord.FundusDocDrawing = CheckBoxFundusDocDrawing.Checked;
                abstractRecord.FundusDocPhotography = CheckBoxFundusDocPhotography.Checked;
                abstractRecord.OpticNerveImagedCT = CheckBoxOpticNerveImagedCT.Checked;
                abstractRecord.OpticNerveImagedMRI = CheckBoxOpticNerveImagedMRI.Checked;
                abstractRecord.OpticNerveNotImaged = CheckBoxOpticNerveNotImaged.Checked;



                if (RadioButtonListRBGender.SelectedIndex != -1) abstractRecord.RBGender = Convert.ToInt32(RadioButtonListRBGender.SelectedValue);
                else abstractRecord.RBGender = 0;
                if (RadioButtonListRBVisitReason.SelectedIndex != -1) abstractRecord.RBVisitReason = Convert.ToInt32(RadioButtonListRBVisitReason.SelectedValue);
                else abstractRecord.RBVisitReason = 0;
                if (RadioButtonListRBComplaintDocumented.SelectedIndex != -1) abstractRecord.RBComplaintDocumented = Convert.ToInt32(RadioButtonListRBComplaintDocumented.SelectedValue);
                else abstractRecord.RBComplaintDocumented = 0;
                if (RadioButtonListRBTumorStageOD.SelectedIndex != -1) abstractRecord.RBTumorStageOD = Convert.ToInt32(RadioButtonListRBTumorStageOD.SelectedValue);
                else abstractRecord.RBTumorStageOD = 0;
                if (RadioButtonListRBPrimaryTreatmentModalityOD.SelectedIndex != -1) abstractRecord.RBPrimaryTreatmentModalityOD = Convert.ToInt32(RadioButtonListRBPrimaryTreatmentModalityOD.SelectedValue);
                else abstractRecord.RBPrimaryTreatmentModalityOD = 0;
                if (RadioButtonListRBTumorStageOS.SelectedIndex != -1) abstractRecord.RBTumorStageOS = Convert.ToInt32(RadioButtonListRBTumorStageOS.SelectedValue);
                else abstractRecord.RBTumorStageOS = 0;
                if (RadioButtonListRBPrimaryTreatmentModalityOS.SelectedIndex != -1) abstractRecord.RBPrimaryTreatmentModalityOS = Convert.ToInt32(RadioButtonListRBPrimaryTreatmentModalityOS.SelectedValue);
                else abstractRecord.RBPrimaryTreatmentModalityOS = 0;
                if (RadioButtonListRBDocGeneticScreening.SelectedIndex != -1) abstractRecord.RBDocGeneticScreening = Convert.ToInt32(RadioButtonListRBDocGeneticScreening.SelectedValue);
                else abstractRecord.RBDocGeneticScreening = 0;



                if (DropDownListMonthOfBirth.SelectedIndex > 0) abstractRecord.MonthOfBirth = Convert.ToInt32(DropDownListMonthOfBirth.SelectedValue);
                else abstractRecord.MonthOfBirth = 0;
                if (DropDownListYearOfBirth.SelectedIndex > 0) abstractRecord.YearOfBirth = Convert.ToInt32(DropDownListYearOfBirth.SelectedValue);
                else abstractRecord.YearOfBirth = 0;
                if (DropDownListMonthOfTreatment.SelectedIndex > 0) abstractRecord.MonthOfTreatment = Convert.ToInt32(DropDownListMonthOfTreatment.SelectedValue);
                else abstractRecord.MonthOfTreatment = 0;
                if (DropDownListYearOfTreatment.SelectedIndex > 0) abstractRecord.YearOfTreatment = Convert.ToInt32(DropDownListYearOfTreatment.SelectedValue);
                else abstractRecord.YearOfTreatment = 0;
                if (DropDownListMonthOfFirstSymptoms.SelectedIndex > 0) abstractRecord.MonthOfFirstSymptoms = Convert.ToInt32(DropDownListMonthOfFirstSymptoms.SelectedValue);
                else abstractRecord.MonthOfFirstSymptoms = 0;
                if (DropDownListYearOfFirstSymptoms.SelectedIndex > 0) abstractRecord.YearOfFirstSymptoms = Convert.ToInt32(DropDownListYearOfFirstSymptoms.SelectedValue);
                else abstractRecord.YearOfFirstSymptoms = 0;
                if (TextBoxTumorStageOSOther.SelectedIndex > 0) abstractRecord.TextBoxTumorStageOSOther = Convert.ToInt32(TextBoxTumorStageOSOther.SelectedValue);
                else abstractRecord.TextBoxTumorStageOSOther = 0;
                if (TextBoxTumorStageODOther.SelectedIndex > 0) abstractRecord.TextBoxRBPrimaryTreatmentOtherOD = Convert.ToInt32(TextBoxTumorStageODOther.SelectedValue);
                else abstractRecord.TextBoxRBPrimaryTreatmentOtherOD = 0;


                if (RadioButtonEchographyPerformedYes.Checked) abstractRecord.EchographyPerformed = true;
                if (RadioButtonEchographyPerformedNo.Checked) abstractRecord.EchographyPerformed = false;
                if (RadioButtonDocInformedConsentYes.Checked) abstractRecord.DocInformedConsent = true;
                if (RadioButtonDocInformedConsentNo.Checked) abstractRecord.DocInformedConsent = false;
                if (RadioButtonDocPCPYes.Checked) abstractRecord.DocPCP = true;
                if (RadioButtonDocPCPNo.Checked) abstractRecord.DocPCP = false;
                if (RadioButtonDocPathologicEvalYes.Checked) abstractRecord.DocPathologicEval = true;
                if (RadioButtonDocPathologicEvalNo.Checked) abstractRecord.DocPathologicEval = false;
                if (RadioButtonGlobSparingEnucleatedYes.Checked) abstractRecord.GlobSparingEnucleated = true;
                if (RadioButtonGlobSparingEnucleatedNo.Checked) abstractRecord.GlobSparingEnucleated = false;
                if (RadioButtonDocGeneticCounselingYes.Checked) abstractRecord.DocGeneticCounseling = true;
                if (RadioButtonDocGeneticCounselingNo.Checked) abstractRecord.DocGeneticCounseling = false;
                try
                {
                if (TextBoxRBPrimaryTreatmentOtherOD.Text != null)
                abstractRecord.RBPrimaryTreatmentOtherOD = TextBoxRBPrimaryTreatmentOtherOD.Text;
                }
                catch (Exception ex)
                {
                abstractRecord.RBPrimaryTreatmentOtherOD = null;
                TextBoxRBPrimaryTreatmentOtherOD.Text = "";
                }
                try
                {
                if (TextBoxRBPrimaryTreatmentOtherOS.Text != null)
                abstractRecord.RBPrimaryTreatmentOtherOS = TextBoxRBPrimaryTreatmentOtherOS.Text;
                }
                catch (Exception ex)
                {
                abstractRecord.RBPrimaryTreatmentOtherOS = null;
                TextBoxRBPrimaryTreatmentOtherOS.Text = "";
                }


                abstractRecord.LastUpdateDate = DateTime.Now;
                bool ChartCompleted = isComplete();
                abstractRecord.Complete = ChartCompleted;
                NetHealthPIMModel.ChartRegistration chartRegistration = (from cr in pim.ChartRegistration
                                         where cr.ParticipantModuleCycleID == cycle.ParticipantModuleCycleID
                                          & cr.ModuleID == ModuleID
                                          & cr.RecordIdentifier == recordIdentifier
                                          select cr).First<NetHealthPIMModel.ChartRegistration>();
                chartRegistration.AbstractCompleted = ChartCompleted;


                if ((DropDownListYearOfBirth.SelectedIndex > 0) && (DropDownListYearOfBirth.SelectedIndex > 0))
                {
                    chartRegistration.DOB = abstractRecord.MonthOfBirth + "/" + abstractRecord.YearOfBirth;
                }
              
                pim.SaveChanges();

                transaction.Complete();




            }

            CycleManager.UpdateParticipantCompletedModules(cycleID, ModuleID);
        }


    }

    protected void ButtonSubmit_Click(object sender, EventArgs e)
    {
        savedata();
        isComplete();
        if (!isComplete())
        {
            isComplete();
            string script = " var answer = confirm('Chart data is not complete.\\nClick OK to save entries and exit chart.\\nClick CANCEL to save entries and review the chart. '); if (answer==true) window.location = 'PatientChartRegistration.aspx';";
            ScriptManager.RegisterStartupScript(this, GetType(),
                          "ServerControlScript", script, true);
        }
        else
        {

            Response.Redirect("../PatientChartRegistration.aspx");
        }
    }

    protected bool isComplete()
    {

        bool ChartCompleted = true;

        LabelRBGender.Visible = false;

        LabelRBVisitReason.Visible = false;

        LabelRBComplaintDocumented.Visible = false;

        LabelRBTumorStageOD.Visible = false;

        LabelRBPrimaryTreatmentModalityOD.Visible = false;

        LabelRBTumorStageOS.Visible = false;

        LabelRBPrimaryTreatmentModalityOS.Visible = false;

        LabelRBDocGeneticScreening.Visible = false;




        LabelMonthOfBirth.Visible = false;

        LabelYearOfBirth.Visible = false;


        LabelMonthOfTreatment.Visible = false;


        LabelYearOfTreatment.Visible = false;

        LabelMonthOfFirstSymptoms.Visible = false;

        LabelYearOfFirstSymptoms.Visible = false;




        LabelEchographyPerformed.Visible = false;

        LabelDocInformedConsent.Visible = false;

        LabelDocPCP.Visible = false;

        LabelDocPathologicEval.Visible = false;


        LabelDocGeneticCounseling.Visible = false;


        if (RadioButtonListRBGender.SelectedIndex == -1)
        {
            LabelRBGender.Visible = true;
            ChartCompleted = false;
        }
        if (RadioButtonListRBVisitReason.SelectedIndex == -1)
        {
            LabelRBVisitReason.Visible = true;
            ChartCompleted = false;
        }
        if (RadioButtonListRBComplaintDocumented.SelectedIndex == -1)
        {
            LabelRBComplaintDocumented.Visible = true;
            ChartCompleted = false;
        }
        if (RadioButtonListRBTumorStageOD.SelectedIndex == -1)
        {
            LabelRBTumorStageOD.Visible = true;
            ChartCompleted = false;
        }
        if (RadioButtonListRBPrimaryTreatmentModalityOD.SelectedIndex == -1)
        {
            LabelRBPrimaryTreatmentModalityOD.Visible = true;
            ChartCompleted = false;
        }
        if (RadioButtonListRBTumorStageOS.SelectedIndex == -1)
        {
            LabelRBTumorStageOS.Visible = true;
            ChartCompleted = false;
        }
        if (RadioButtonListRBPrimaryTreatmentModalityOS.SelectedIndex == -1)
        {
            LabelRBPrimaryTreatmentModalityOS.Visible = true;
            ChartCompleted = false;
        }
        if (RadioButtonListRBDocGeneticScreening.SelectedIndex == -1)
        {
            LabelRBDocGeneticScreening.Visible = true;
            ChartCompleted = false;
        }



        if (DropDownListMonthOfBirth.SelectedIndex == 0)
        {
            LabelMonthOfBirth.Visible = true;
            ChartCompleted = false;
        }
        if (DropDownListYearOfBirth.SelectedIndex == 0)
        {
            LabelYearOfBirth.Visible = true;
            ChartCompleted = false;
        }
        if (DropDownListMonthOfTreatment.SelectedIndex == 0)
        {
            LabelMonthOfTreatment.Visible = true;
            ChartCompleted = false;
        }
        if (DropDownListYearOfTreatment.SelectedIndex == 0)
        {
            LabelYearOfTreatment.Visible = true;
            ChartCompleted = false;
        }
        if (DropDownListMonthOfFirstSymptoms.SelectedIndex == 0 && CheckBoxFirstSymptomsDateNA.Checked==false)
        {
            LabelMonthOfFirstSymptoms.Visible = true;
            ChartCompleted = false;
        }
        if (DropDownListYearOfFirstSymptoms.SelectedIndex == 0 && CheckBoxFirstSymptomsDateNA.Checked == false)
        {
            LabelYearOfFirstSymptoms.Visible = true;
            ChartCompleted = false;
        }



        if (RadioButtonEchographyPerformedNo.Checked == false && RadioButtonEchographyPerformedYes.Checked == false)
        {
            LabelEchographyPerformed.Visible = true;
            ChartCompleted = false;
        }
        if (RadioButtonDocInformedConsentNo.Checked == false && RadioButtonDocInformedConsentYes.Checked == false)
        {
            LabelDocInformedConsent.Visible = true;
            ChartCompleted = false;
        }
        if (RadioButtonDocPCPNo.Checked == false && RadioButtonDocPCPYes.Checked == false)
        {
            LabelDocPCP.Visible = true;
            ChartCompleted = false;
        }
        if (RadioButtonDocPathologicEvalNo.Checked == false && RadioButtonDocPathologicEvalYes.Checked == false)
        {
            LabelDocPathologicEval.Visible = true;
            ChartCompleted = false;
        }
        if (RadioButtonGlobSparingEnucleatedNo.Checked == false && RadioButtonGlobSparingEnucleatedYes.Checked == false)
        {
            LabelGlobSparingEnucleated.Visible = true;
            ChartCompleted = false;
        }
        if (RadioButtonDocGeneticCounselingNo.Checked == false && RadioButtonDocGeneticCounselingYes.Checked == false)
        {
            LabelDocGeneticCounseling.Visible = true;
            ChartCompleted = false;
        }









        return ChartCompleted;
    }








}


