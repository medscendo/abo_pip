﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Transactions;
using NetHealthPIMModel;

public partial class abo_YAGChart : BasePage
{
    protected void Page_Load(object sender, EventArgs e)
    {

     if (!IsPostBack)
        {
            InitializePage();
        }


    }
    protected void InitializePage()
    {
        ((PIMMasterPage)Page.Master).SetTitle("Chart");

        using (NetHealthPIMEntities pim = new NetHealthPIMEntities())
        {
            int ModuleID = (int)Session[Constants.SESSION_WORKINGMODULEID];
            Module module = pim.Module.First(c => c.ModuleID == ModuleID);
            ((PIMMasterPage)Page.Master).SetHeader(module.ModuleName + " Chart Abstraction");

            ParticipantModuleCycle prev = (ParticipantModuleCycle)Session[Constants.SESSION_PREVIOUSCYCLE];
            String CycleNumber = Request.QueryString["CycleNumber"];

            int cycleID = ctx.ActiveModuleCycleID;
            if (CycleNumber == "1")
            {
                cycleID = prev.ParticipantModuleCycleID;
                LinkButtonBackToDashboard.Visible = true;
                ButtonSubmit.Visible = false;
            }

            ParticipantModuleCycle cycle = pim.ParticipantModuleCycle.First(c => c.ParticipantModuleCycleID == cycleID);
            String numberOfRecord = Request.QueryString["nr"];
            String totalRecords = Request.QueryString["t"];
            if (cycle.CycleNumber == 1)
            {
                ((PIMMasterPage)Page.Master).SetStatus(2);
                LiteralAbstractionNumber.Text = "Initial Abstraction: Chart " + numberOfRecord + " of " + totalRecords;
            }
            else
            {
                ((PIMMasterPage)Page.Master).SetStatus(3);
                LiteralAbstractionNumber.Text = "Second Abstraction: Chart " + numberOfRecord + " of " + totalRecords;
            }
            DropDownListMonthOfBirth.DataSource = CycleManager.getMonthList();
            DropDownListYearOfBirth.DataSource = CycleManager.getDOBYearList(0, 100);
            DropDownListMonthOfSurgery.DataSource = CycleManager.getMonthList();
            DropDownListYearOfSurgery.DataSource = CycleManager.getDOBYearList(0, 100);
            DropDownListOutcomeBVCA.DataSource = CycleManager.getExamValues();
            DropDownListBCVA.DataSource = CycleManager.getExamValues();
            DropDownListMonthOutcomeExam.DataSource = CycleManager.getMonthList();
            DropDownListYearOutcomeExam.DataSource = CycleManager.getDOBYearList(0, 100);




            DataBind();
            DropDownListYearOfBirth.Items.Insert(0, new ListItem("Year", "0"));
            DropDownListYearOfSurgery.Items.Insert(0, new ListItem("Year", "0"));
            DropDownListYearOutcomeExam.Items.Insert(0, new ListItem("Year", "0"));
            string recordIdentifier = Request.QueryString["ri"];
            HiddenFieldRecordIdentifier.Value = recordIdentifier;
            LiteralRecordIdentifier.Text = recordIdentifier;
            var count = pim.ChartAbstractionYAG.Where(ps => ps.ParticipantModuleCycle.ParticipantModuleCycleID == cycleID & ps.RecordIdentifier == recordIdentifier).Count();
            if (count == 0)
            {
                using (TransactionScope transaction = new TransactionScope())
                {
                    //Module module = pim.Module.First(m => m.ModuleID == Constants.ABO_AMBLYOPIA_MODULEID);

                    NetHealthPIMModel.ChartRegistration chartRegistration = (from cr in pim.ChartRegistration
                                                           where cr.ParticipantModuleCycleID == cycleID
                                                           & cr.RecordIdentifier == recordIdentifier
                                                           & cr.ModuleID == 11
                                                           select cr).First<NetHealthPIMModel.ChartRegistration>();

                    DateTime initialVisit = Convert.ToDateTime(chartRegistration.InitialVisit);
                    DateTime DOB = Convert.ToDateTime(chartRegistration.DOB);

                    ChartAbstractionYAG abstractRecord = new ChartAbstractionYAG();
                    abstractRecord.ParticipantModuleCycle = cycle;
                    abstractRecord.RecordIdentifier = recordIdentifier;
                    abstractRecord.MonthOfBirth = DOB.Month;
                    abstractRecord.YearOfBirth = DOB.Year;
                    DropDownListYearOfBirth.SelectedValue = abstractRecord.YearOfBirth.ToString();
                    DropDownListMonthOfBirth.SelectedValue = abstractRecord.MonthOfBirth.ToString();

                    abstractRecord.CreatedOn = DateTime.Now;
                    abstractRecord.LastUpdateDate = DateTime.Now;
                    pim.SaveChanges();

                    transaction.Complete();
                }
            }
            else
            {

                ChartAbstractionYAG abstractRecord = (from c in pim.ChartAbstractionYAG
                                                         where c.ParticipantModuleCycle.ParticipantModuleCycleID == cycleID & c.RecordIdentifier == recordIdentifier
                                                      select c).First<ChartAbstractionYAG>();

                if (abstractRecord.OutcomeBCVANA != null) CheckBoxOutcomeBVCANone.Checked = ((bool)abstractRecord.OutcomeBCVANA);
                if (abstractRecord.ExamIOPNA != null) CheckBoxExamIOPNA.Checked = ((bool)abstractRecord.ExamIOPNA);
                if (abstractRecord.SurgeryEnergyNA != null) CheckBoxSurgeryEnergyNA.Checked = ((bool)abstractRecord.SurgeryEnergyNA);
                if (abstractRecord.SurgeryIOP24hrsNA != null) CheckBoxSurgeryIOP24hrsNA.Checked = ((bool)abstractRecord.SurgeryIOP24hrsNA);
                if (abstractRecord.OutcomeComplicationsNone != null) CheckBoxOutcomeComplicationsNone.Checked = ((bool)abstractRecord.OutcomeComplicationsNone);
                if (abstractRecord.OutcomeComplicationsIOLdefects != null) CheckBoxOutcomeComplicationsIOLdefects.Checked = ((bool)abstractRecord.OutcomeComplicationsIOLdefects);
                if (abstractRecord.OutcomeComplicationsIOLdecentration != null) CheckBoxOutcomeComplicationsIOLdecentration.Checked = ((bool)abstractRecord.OutcomeComplicationsIOLdecentration);
                if (abstractRecord.OutcomeComplicationsVitreous != null) CheckBoxOutcomeComplicationsVitreous.Checked = ((bool)abstractRecord.OutcomeComplicationsVitreous);
                if (abstractRecord.OutcomeComplicationsMacular != null) CheckBoxOutcomeComplicationsMacular.Checked = ((bool)abstractRecord.OutcomeComplicationsMacular);
                if (abstractRecord.OutcomeComplicationsRetinal != null) CheckBoxOutcomeComplicationsRetinal.Checked = ((bool)abstractRecord.OutcomeComplicationsRetinal);
                if (abstractRecord.OutcomeComplicationsIOPelevation != null) CheckBoxOutcomeComplicationsIOPelevation.Checked = ((bool)abstractRecord.OutcomeComplicationsIOPelevation);



                if (abstractRecord.RBGender != null) RadioButtonListRBGender.SelectedValue = abstractRecord.RBGender.ToString();
                if (abstractRecord.RBSymptomsDailyLiving != null) RadioButtonListRBSymptomsDailyLiving.SelectedValue = abstractRecord.RBSymptomsDailyLiving.ToString();
                if (abstractRecord.RBHistoryGlaucoma != null) RadioButtonListRBHistoryGlaucoma.SelectedValue = abstractRecord.RBHistoryGlaucoma.ToString();
                if (abstractRecord.RBExamRefraction != null) RadioButtonListRBExamRefraction.SelectedValue = abstractRecord.RBExamRefraction.ToString();
                if (abstractRecord.RBRefractionNotPerformedReason != null) RadioButtonListRBRefractionNotPerformedReason.SelectedValue = abstractRecord.RBRefractionNotPerformedReason.ToString();
                if (abstractRecord.RBPCOVisualAxis != null) RadioButtonListRBPCOVisualAxis.SelectedValue = abstractRecord.RBPCOVisualAxis.ToString();
                if (abstractRecord.RBExamPatientDiscuss != null) RadioButtonListRBExamPatientDiscuss.SelectedValue = abstractRecord.RBExamPatientDiscuss.ToString();
                if (abstractRecord.RBSurgeryPatientInstruction != null) RadioButtonListRBSurgeryPatientInstruction.SelectedValue = abstractRecord.RBSurgeryPatientInstruction.ToString();
                if (abstractRecord.RBOutcomeAppointment != null) RadioButtonListRBOutcomeAppointment.SelectedValue = abstractRecord.RBOutcomeAppointment.ToString();
                if (abstractRecord.RBOutcomeSubImprove != null) RadioButtonListRBOutcomeSubImprove.SelectedValue = abstractRecord.RBOutcomeSubImprove.ToString();
                if (abstractRecord.RBOutcomeCapsulotomyAdequate != null) RadioButtonListRBOutcomeCapsulotomyAdequate.SelectedValue = abstractRecord.RBOutcomeCapsulotomyAdequate.ToString();
                if (abstractRecord.RBOutcomeDilatedFundus != null) RadioButtonListRBOutcomeDilatedFundus.SelectedValue = abstractRecord.RBOutcomeDilatedFundus.ToString();

               
                if (abstractRecord.OutcomeBCVA != null) DropDownListOutcomeBVCA.SelectedValue = abstractRecord.OutcomeBCVA.ToString();
                if (abstractRecord.MonthOfBirth != null) DropDownListMonthOfBirth.SelectedValue = abstractRecord.MonthOfBirth.ToString();
                if (abstractRecord.YearOfBirth != null) DropDownListYearOfBirth.SelectedValue = abstractRecord.YearOfBirth.ToString();
                if (abstractRecord.BCVA != null) DropDownListBCVA.SelectedValue = abstractRecord.BCVA.ToString();
                if (abstractRecord.MonthOfSurgery != null) DropDownListMonthOfSurgery.SelectedValue = abstractRecord.MonthOfSurgery.ToString();
                if (abstractRecord.YearOfSurgery != null) DropDownListYearOfSurgery.SelectedValue = abstractRecord.YearOfSurgery.ToString();
                if (abstractRecord.MonthOutcomeExam != null) DropDownListMonthOutcomeExam.SelectedValue = abstractRecord.MonthOutcomeExam.ToString();
                if (abstractRecord.YearOutcomeExam != null) DropDownListYearOutcomeExam.SelectedValue = abstractRecord.YearOutcomeExam.ToString();



                if (abstractRecord.ExamCapsularBlock != null)
                {
                    RadioButtonExamCapsularBlockYes.Checked = ((bool)abstractRecord.ExamCapsularBlock);
                    RadioButtonExamCapsularBlockNo.Checked = !((bool)abstractRecord.ExamCapsularBlock);
                }
                if (abstractRecord.ExamDilatedFundus != null)
                {
                    RadioButtonExamDilatedFundusYes.Checked = ((bool)abstractRecord.ExamDilatedFundus);
                    RadioButtonExamDilatedFundusNo.Checked = !((bool)abstractRecord.ExamDilatedFundus);
                }
                if (abstractRecord.ExamSignedConsent != null)
                {
                    RadioButtonExamSignedConsentYes.Checked = ((bool)abstractRecord.ExamSignedConsent);
                    RadioButtonExamSignedConsentNo.Checked = !((bool)abstractRecord.ExamSignedConsent);
                }
                if (abstractRecord.AqueousSuppressant != null)
                {
                    RadioButtonAqueousSuppressantYes.Checked = ((bool)abstractRecord.AqueousSuppressant);
                    RadioButtonAqueousSuppressantNo.Checked = !((bool)abstractRecord.AqueousSuppressant);
                }
                if (abstractRecord.OutcomeRefraction != null)
                {
                    RadioButtonOutcomeRefractionYes.Checked = ((bool)abstractRecord.OutcomeRefraction);
                    RadioButtonOutcomeRefractionNo.Checked = !((bool)abstractRecord.OutcomeRefraction);
                }
                if (abstractRecord.OutcomeRefractionNA != null)
                {
                    RadioButtonOutcomeRefractionNAYes.Checked = ((bool)abstractRecord.OutcomeRefractionNA);
                    RadioButtonOutcomeRefractionNANo.Checked = !((bool)abstractRecord.OutcomeRefractionNA);
                }
                if (abstractRecord.OutcomeFurtherSurgery != null)
                {
                    RadioButtonOutcomeFurtherSurgeryYes.Checked = ((bool)abstractRecord.OutcomeFurtherSurgery);
                    RadioButtonOutcomeFurtherSurgeryNo.Checked = !((bool)abstractRecord.OutcomeFurtherSurgery);
                }



                if (abstractRecord.ExamIOPMeasurement != null) TextBoxExamIOPMeasurement.Text = abstractRecord.ExamIOPMeasurement.ToString();
                if (abstractRecord.SurgeryEnergy != null) TextBoxSurgeryEnergy.Text = abstractRecord.SurgeryEnergy.ToString();
                if (abstractRecord.SurgeryIOP24hrs != null) TextBoxSurgeryIOP24hrs.Text = abstractRecord.SurgeryIOP24hrs.ToString();
                





            }
        }
    }
    public void savedata()
    {
        using (NetHealthPIMEntities pim = new NetHealthPIMEntities())
        {
            int ModuleID = (int)Session[Constants.SESSION_WORKINGMODULEID];
            int cycleID = ctx.ActiveModuleCycleID;
            ParticipantModuleCycle cycle = pim.ParticipantModuleCycle.First(c => c.ParticipantModuleCycleID == cycleID);
            string recordIdentifier = HiddenFieldRecordIdentifier.Value;
            using (TransactionScope transaction = new TransactionScope())
            {
                ChartAbstractionYAG abstractRecord = (from c in pim.ChartAbstractionYAG
                                                         where c.ParticipantModuleCycle.ParticipantModuleCycleID == cycleID & c.RecordIdentifier == recordIdentifier
                                                      select c).First<ChartAbstractionYAG>();

                abstractRecord.OutcomeBCVANA = CheckBoxOutcomeBVCANone.Checked;
                abstractRecord.ExamIOPNA = CheckBoxExamIOPNA.Checked;
                abstractRecord.SurgeryEnergyNA = CheckBoxSurgeryEnergyNA.Checked;
                abstractRecord.SurgeryIOP24hrsNA = CheckBoxSurgeryIOP24hrsNA.Checked;
                abstractRecord.OutcomeComplicationsNone = CheckBoxOutcomeComplicationsNone.Checked;
                abstractRecord.OutcomeComplicationsIOLdefects = CheckBoxOutcomeComplicationsIOLdefects.Checked;
                abstractRecord.OutcomeComplicationsIOLdecentration = CheckBoxOutcomeComplicationsIOLdecentration.Checked;
                abstractRecord.OutcomeComplicationsVitreous = CheckBoxOutcomeComplicationsVitreous.Checked;
                abstractRecord.OutcomeComplicationsMacular = CheckBoxOutcomeComplicationsMacular.Checked;
                abstractRecord.OutcomeComplicationsRetinal = CheckBoxOutcomeComplicationsRetinal.Checked;
                abstractRecord.OutcomeComplicationsIOPelevation = CheckBoxOutcomeComplicationsIOPelevation.Checked;



                if (RadioButtonListRBGender.SelectedIndex != -1) abstractRecord.RBGender = Convert.ToInt32(RadioButtonListRBGender.SelectedValue);
                else abstractRecord.RBGender = 0;
                if (RadioButtonListRBSymptomsDailyLiving.SelectedIndex != -1) abstractRecord.RBSymptomsDailyLiving = Convert.ToInt32(RadioButtonListRBSymptomsDailyLiving.SelectedValue);
                else abstractRecord.RBSymptomsDailyLiving = 0;
                if (RadioButtonListRBHistoryGlaucoma.SelectedIndex != -1) abstractRecord.RBHistoryGlaucoma = Convert.ToInt32(RadioButtonListRBHistoryGlaucoma.SelectedValue);
                else abstractRecord.RBHistoryGlaucoma = 0;
                if (RadioButtonListRBExamRefraction.SelectedIndex != -1) abstractRecord.RBExamRefraction = Convert.ToInt32(RadioButtonListRBExamRefraction.SelectedValue);
                else abstractRecord.RBExamRefraction = 0;
                if (RadioButtonListRBRefractionNotPerformedReason.SelectedIndex != -1) abstractRecord.RBRefractionNotPerformedReason = Convert.ToInt32(RadioButtonListRBRefractionNotPerformedReason.SelectedValue);
                else abstractRecord.RBRefractionNotPerformedReason = 0;
                if (RadioButtonListRBPCOVisualAxis.SelectedIndex != -1) abstractRecord.RBPCOVisualAxis = Convert.ToInt32(RadioButtonListRBPCOVisualAxis.SelectedValue);
                else abstractRecord.RBPCOVisualAxis = 0;
                if (RadioButtonListRBExamPatientDiscuss.SelectedIndex != -1) abstractRecord.RBExamPatientDiscuss = Convert.ToInt32(RadioButtonListRBExamPatientDiscuss.SelectedValue);
                else abstractRecord.RBExamPatientDiscuss = 0;
                if (RadioButtonListRBSurgeryPatientInstruction.SelectedIndex != -1) abstractRecord.RBSurgeryPatientInstruction = Convert.ToInt32(RadioButtonListRBSurgeryPatientInstruction.SelectedValue);
                else abstractRecord.RBSurgeryPatientInstruction = 0;
                if (RadioButtonListRBOutcomeAppointment.SelectedIndex != -1) abstractRecord.RBOutcomeAppointment = Convert.ToInt32(RadioButtonListRBOutcomeAppointment.SelectedValue);
                else abstractRecord.RBOutcomeAppointment = 0;
                if (RadioButtonListRBOutcomeSubImprove.SelectedIndex != -1) abstractRecord.RBOutcomeSubImprove = Convert.ToInt32(RadioButtonListRBOutcomeSubImprove.SelectedValue);
                else abstractRecord.RBOutcomeSubImprove = 0;
                if (RadioButtonListRBOutcomeCapsulotomyAdequate.SelectedIndex != -1) abstractRecord.RBOutcomeCapsulotomyAdequate = Convert.ToInt32(RadioButtonListRBOutcomeCapsulotomyAdequate.SelectedValue);
                else abstractRecord.RBOutcomeCapsulotomyAdequate = 0;
                if (RadioButtonListRBOutcomeDilatedFundus.SelectedIndex != -1) abstractRecord.RBOutcomeDilatedFundus = Convert.ToInt32(RadioButtonListRBOutcomeDilatedFundus.SelectedValue);
                else abstractRecord.RBOutcomeDilatedFundus = 0;



                if (DropDownListMonthOfBirth.SelectedIndex > 0) abstractRecord.MonthOfBirth = Convert.ToInt32(DropDownListMonthOfBirth.SelectedValue);
                else abstractRecord.MonthOfBirth = 0;
                if (DropDownListYearOfBirth.SelectedIndex > 0) abstractRecord.YearOfBirth = Convert.ToInt32(DropDownListYearOfBirth.SelectedValue);
                else abstractRecord.YearOfBirth = 0;
                if (DropDownListBCVA.SelectedIndex > 0) abstractRecord.BCVA = Convert.ToInt32(DropDownListBCVA.SelectedValue);
                else abstractRecord.BCVA = 0;
                if (DropDownListMonthOfSurgery.SelectedIndex > 0) abstractRecord.MonthOfSurgery = Convert.ToInt32(DropDownListMonthOfSurgery.SelectedValue);
                else abstractRecord.MonthOfSurgery = 0;
                if (DropDownListYearOfSurgery.SelectedIndex > 0) abstractRecord.YearOfSurgery = Convert.ToInt32(DropDownListYearOfSurgery.SelectedValue);
                else abstractRecord.YearOfSurgery = 0;
                if (DropDownListMonthOutcomeExam.SelectedIndex > 0) abstractRecord.MonthOutcomeExam = Convert.ToInt32(DropDownListMonthOutcomeExam.SelectedValue);
                else abstractRecord.MonthOutcomeExam = 0;
                if (DropDownListYearOutcomeExam.SelectedIndex > 0) abstractRecord.YearOutcomeExam = Convert.ToInt32(DropDownListYearOutcomeExam.SelectedValue);
                else abstractRecord.YearOutcomeExam = 0;

                if (DropDownListOutcomeBVCA.SelectedIndex > 0) abstractRecord.OutcomeBCVA = Convert.ToInt32(DropDownListOutcomeBVCA.SelectedValue);
                else abstractRecord.OutcomeBCVA = 0;

                if (RadioButtonExamCapsularBlockYes.Checked) abstractRecord.ExamCapsularBlock = true;
                if (RadioButtonExamCapsularBlockNo.Checked) abstractRecord.ExamCapsularBlock = false;
                if (RadioButtonExamDilatedFundusYes.Checked) abstractRecord.ExamDilatedFundus = true;
                if (RadioButtonExamDilatedFundusNo.Checked) abstractRecord.ExamDilatedFundus = false;
                if (RadioButtonExamSignedConsentYes.Checked) abstractRecord.ExamSignedConsent = true;
                if (RadioButtonExamSignedConsentNo.Checked) abstractRecord.ExamSignedConsent = false;
                if (RadioButtonAqueousSuppressantYes.Checked) abstractRecord.AqueousSuppressant = true;
                if (RadioButtonAqueousSuppressantNo.Checked) abstractRecord.AqueousSuppressant = false;
                if (RadioButtonOutcomeRefractionYes.Checked) abstractRecord.OutcomeRefraction = true;
                if (RadioButtonOutcomeRefractionNo.Checked) abstractRecord.OutcomeRefraction = false;
                if (RadioButtonOutcomeRefractionNAYes.Checked) abstractRecord.OutcomeRefractionNA = true;
                if (RadioButtonOutcomeRefractionNANo.Checked) abstractRecord.OutcomeRefractionNA = false;
                if (RadioButtonOutcomeFurtherSurgeryYes.Checked) abstractRecord.OutcomeFurtherSurgery = true;
                if (RadioButtonOutcomeFurtherSurgeryNo.Checked) abstractRecord.OutcomeFurtherSurgery = false;



                    try
                    {
                    if (TextBoxExamIOPMeasurement.Text != null)
                    abstractRecord.ExamIOPMeasurement = Convert.ToDouble(TextBoxExamIOPMeasurement.Text);
                    }
                    catch (Exception ex)
                    {
                    abstractRecord.ExamIOPMeasurement = null;
                    TextBoxExamIOPMeasurement.Text = "";
                    }
                    try
                    {
                    if (TextBoxSurgeryEnergy.Text != null)
                    abstractRecord.SurgeryEnergy = Convert.ToDouble(TextBoxSurgeryEnergy.Text);
                    }
                    catch (Exception ex)
                    {
                    abstractRecord.SurgeryEnergy = null;
                    TextBoxSurgeryEnergy.Text = "";
                    }
                    try
                    {
                    if (TextBoxSurgeryIOP24hrs.Text != null)
                    abstractRecord.SurgeryIOP24hrs = Convert.ToDouble(TextBoxSurgeryIOP24hrs.Text);
                    }
                    catch (Exception ex)
                    {
                    abstractRecord.SurgeryIOP24hrs = null;
                    TextBoxSurgeryIOP24hrs.Text = "";
                    }

                    abstractRecord.LastUpdateDate = DateTime.Now;
                    bool ChartCompleted = isComplete();
                    abstractRecord.Complete = ChartCompleted;
                    var chartRegistration = (from cr in pim.ChartRegistration
                                             where cr.ParticipantModuleCycleID == cycle.ParticipantModuleCycleID
                                              & cr.ModuleID == ModuleID
                                              & cr.RecordIdentifier == recordIdentifier
                                             select cr).First<NetHealthPIMModel.ChartRegistration>();
                    chartRegistration.AbstractCompleted = ChartCompleted;


                    if ((DropDownListYearOfBirth.SelectedIndex > 0) && (DropDownListYearOfBirth.SelectedIndex > 0))
                    {
                        chartRegistration.DOB = abstractRecord.MonthOfBirth + "/" + abstractRecord.YearOfBirth;
                    }

                    pim.SaveChanges();

                    transaction.Complete();




            }

            CycleManager.UpdateParticipantCompletedModules(cycleID, ModuleID);
        }


    }


    protected void ButtonSubmit_Click(object sender, EventArgs e)
    {
        savedata();
        isComplete();
        if (!isComplete())
        {
            isComplete();
            string script = " var answer = confirm('Chart data is not complete.\\nClick OK to save entries and exit chart.\\nClick CANCEL to save entries and review the chart. '); if (answer==true) window.location = '../PatientChartRegistration.aspx';";
            ScriptManager.RegisterStartupScript(this, GetType(),
                          "ServerControlScript", script, true);
        }
        else
        {

            Response.Redirect("../PatientChartRegistration.aspx");
        }
    }

    protected bool isComplete()
    {

        bool ChartCompleted = true;

        LabelRBGender.Visible = false;

        LabelRBSymptomsDailyLiving.Visible = false;

        LabelRBHistoryGlaucoma.Visible = false;

        LabelRBExamRefraction.Visible = false;

        LabelRBPCOVisualAxis.Visible = false;

        LabelRBExamPatientDiscuss.Visible = false;

        LabelRBSurgeryPatientInstruction.Visible = false;

        LabelRBOutcomeAppointment.Visible = false;




        LabelMonthOfBirth.Visible = false;

        LabelYearOfBirth.Visible = false;

        LabelBCVA.Visible = false;

        LabelMonthOfSurgery.Visible = false;

        LabelYearOfSurgery.Visible = false;





        LabelExamCapsularBlock.Visible = false;

        LabelExamDilatedFundus.Visible = false;

        LabelExamSignedConsent.Visible = false;

        LabelAqueousSuppressant.Visible = false;
        Labelinitialage.Visible = false;
        
        NetHealthPIMEntities pim = new NetHealthPIMEntities();
        var moduleinfo = (from c in pim.Module where c.ModuleID == 11 select c).FirstOrDefault();
        string message = "Patient must be  at least " + moduleinfo.RegistrationMinAge + " years old at time of surgery<br />";
        string registrationminage = Validation.validateRegistrationMinAge(Convert.ToInt32(DropDownListYearOfSurgery.SelectedValue), Convert.ToInt32(DropDownListYearOfBirth.SelectedValue), Convert.ToInt32(moduleinfo.RegistrationMinAge), message);

        if (registrationminage != "")
        {
            Labelinitialage.Text = message;
            Labelinitialage.Visible = true;
            ChartCompleted = false;
        }

        if (RadioButtonListRBGender.SelectedIndex == -1)
        {
            LabelRBGender.Visible = true;
            ChartCompleted = false;
        }
        if (RadioButtonListRBSymptomsDailyLiving.SelectedIndex == -1)
        {
            LabelRBSymptomsDailyLiving.Visible = true;
            ChartCompleted = false;
        }
        if (RadioButtonListRBHistoryGlaucoma.SelectedIndex == -1)
        {
            LabelRBHistoryGlaucoma.Visible = true;
            ChartCompleted = false;
        }
        if (RadioButtonListRBExamRefraction.SelectedIndex == -1)
        {
            LabelRBExamRefraction.Visible = true;
            ChartCompleted = false;
        }
        if (RadioButtonListRBPCOVisualAxis.SelectedIndex == -1)
        {
            LabelRBPCOVisualAxis.Visible = true;
            ChartCompleted = false;
        }
        if (RadioButtonListRBExamPatientDiscuss.SelectedIndex == -1)
        {
            LabelRBExamPatientDiscuss.Visible = true;
            ChartCompleted = false;
        }
        if (RadioButtonListRBSurgeryPatientInstruction.SelectedIndex == -1)
        {
            LabelRBSurgeryPatientInstruction.Visible = true;
            ChartCompleted = false;
        }
        if (RadioButtonListRBOutcomeAppointment.SelectedIndex == -1)
        {
            LabelRBOutcomeAppointment.Visible = true;
            ChartCompleted = false;
        }



        if (DropDownListMonthOfBirth.SelectedIndex == 0)
        {
            LabelMonthOfBirth.Visible = true;
            ChartCompleted = false;
        }
        if (DropDownListYearOfBirth.SelectedIndex == 0)
        {
            LabelYearOfBirth.Visible = true;
            ChartCompleted = false;
        }
        if (DropDownListBCVA.SelectedIndex == 0)
        {
            LabelBCVA.Visible = true;
            ChartCompleted = false;
        }
        if (DropDownListMonthOfSurgery.SelectedIndex == 0)
        {
            LabelMonthOfSurgery.Visible = true;
            ChartCompleted = false;
        }
        if (DropDownListYearOfSurgery.SelectedIndex == 0)
        {
            LabelYearOfSurgery.Visible = true;
            ChartCompleted = false;
        }



        if (RadioButtonExamCapsularBlockNo.Checked == false && RadioButtonExamCapsularBlockYes.Checked == false)
        {
            LabelExamCapsularBlock.Visible = true;
            ChartCompleted = false;
        }
        if (RadioButtonExamDilatedFundusNo.Checked == false && RadioButtonExamDilatedFundusYes.Checked == false)
        {
            LabelExamDilatedFundus.Visible = true;
            ChartCompleted = false;
        }
        if (RadioButtonExamSignedConsentNo.Checked == false && RadioButtonExamSignedConsentYes.Checked == false)
        {
            LabelExamSignedConsent.Visible = true;
            ChartCompleted = false;
        }
        if (RadioButtonAqueousSuppressantNo.Checked == false && RadioButtonAqueousSuppressantYes.Checked == false)
        {
            LabelAqueousSuppressant.Visible = true;
            ChartCompleted = false;
        }




        return ChartCompleted;
    }



}

