﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Transactions;
using NetHealthPIMModel;

public partial class abo_CEChart : BasePage
{
    protected void Page_Load(object sender, EventArgs e)
    {
       if (!IsPostBack)
        {
            InitializePage();
        }
    }
    protected void InitializePage()
    {
        ((PIMMasterPage)Page.Master).SetTitle("Chart");

        using (NetHealthPIMEntities pim = new NetHealthPIMEntities())
        {
            int ModuleID = (int)Session[Constants.SESSION_WORKINGMODULEID];
            Module module = pim.Module.First(c => c.ModuleID == ModuleID);
            ((PIMMasterPage)Page.Master).SetHeader(module.ModuleName + " Chart Abstraction");

            ParticipantModuleCycle prev = (ParticipantModuleCycle)Session[Constants.SESSION_PREVIOUSCYCLE];
            String CycleNumber = Request.QueryString["CycleNumber"];

            int cycleID = ctx.ActiveModuleCycleID;
            if (CycleNumber == "1")
            {
                cycleID = prev.ParticipantModuleCycleID;
                LinkButtonBackToDashboard.Visible = true;
                ButtonSubmit.Visible = false;
            }

            ParticipantModuleCycle cycle = pim.ParticipantModuleCycle.First(c => c.ParticipantModuleCycleID == cycleID);
            String numberOfRecord = Request.QueryString["nr"];
            String totalRecords = Request.QueryString["t"];
            if (cycle.CycleNumber == 1)
            {
                ((PIMMasterPage)Page.Master).SetStatus(2);
                LiteralAbstractionNumber.Text = "Initial Abstraction: Chart " + numberOfRecord + " of " + totalRecords;
            }
            else
            {
                ((PIMMasterPage)Page.Master).SetStatus(3);
                LiteralAbstractionNumber.Text = "Second Abstraction: Chart " + numberOfRecord + " of " + totalRecords;
            }
            DropDownListMonthOfBirth.DataSource = CycleManager.getMonthList();
            DropDownListYearOfBirth.DataSource = CycleManager.getDOBYearList(0, 100);
            DropDownListMonthofExam.DataSource = CycleManager.getMonthList();
            DropDownListYearofExam.DataSource = CycleManager.getDOBYearList(0, 100);
            DropDownListMonthofSurgery.DataSource = CycleManager.getMonthList();
            DropDownListYearofSurgery.DataSource = CycleManager.getDOBYearList(0, 100);
            DropDownListBCVADistance.DataSource = CycleManager.getExamValues();
            DropDownListOutcomeBCVA.DataSource = CycleManager.getExamValues();
            DataBind();
            DropDownListYearOfBirth.Items.Insert(0, new ListItem("Year", "0"));
            DropDownListYearofExam.Items.Insert(0, new ListItem("Year", "0"));
            DropDownListYearofSurgery.Items.Insert(0, new ListItem("Year", "0"));

            string recordIdentifier = Request.QueryString["ri"];
            HiddenFieldRecordIdentifier.Value = recordIdentifier;
            LiteralRecordIdentifier.Text = recordIdentifier;
            var count = pim.ChartAbstractionCE.Where(ps => ps.ParticipantModuleCycle.ParticipantModuleCycleID == cycleID & ps.RecordIdentifier == recordIdentifier).Count();
            if (count == 0)
            {
                using (TransactionScope transaction = new TransactionScope())
                {
                    //Module module = pim.Module.First(m => m.ModuleID == Constants.ABO_AMBLYOPIA_MODULEID);

                    NetHealthPIMModel.ChartRegistration chartRegistration = (from cr in pim.ChartRegistration
                                                           where cr.ParticipantModuleCycleID == cycleID
                                                           & cr.RecordIdentifier == recordIdentifier
                                                           & cr.ModuleID == 12///// NEEDS TO BE CHANGED
                                                           select cr).First<NetHealthPIMModel.ChartRegistration>();

                    DateTime initialVisit = Convert.ToDateTime(chartRegistration.InitialVisit);
                    DateTime DOB = Convert.ToDateTime(chartRegistration.DOB);

                    ChartAbstractionCE abstractRecord = new ChartAbstractionCE();
                    abstractRecord.ParticipantModuleCycle = cycle;
                    abstractRecord.RecordIdentifier = recordIdentifier;
                    abstractRecord.MonthOfBirth = DOB.Month;
                    abstractRecord.YearOfBirth = DOB.Year;
                    DropDownListYearOfBirth.SelectedValue = abstractRecord.YearOfBirth.ToString();
                    DropDownListMonthOfBirth.SelectedValue = abstractRecord.MonthOfBirth.ToString();
                    DropDownListMonthofExam.SelectedValue = abstractRecord.MonthofExam.ToString();
                    DropDownListYearofExam.SelectedValue = abstractRecord.YearofExam.ToString();
                    DropDownListMonthofSurgery.SelectedValue = abstractRecord.MonthofSurgery.ToString();
                    DropDownListYearofSurgery.SelectedValue = abstractRecord.YearofSurgery.ToString();
                    abstractRecord.CreatedOn = DateTime.Now;
                    abstractRecord.LastUpdateDate = DateTime.Now;
                    pim.SaveChanges();

                    transaction.Complete();
                }
            }
            else
            {

                ChartAbstractionCE abstractRecord = (from c in pim.ChartAbstractionCE
                                                        where c.ParticipantModuleCycle.ParticipantModuleCycleID == cycleID & c.RecordIdentifier == recordIdentifier
                                                    select c).First<ChartAbstractionCE>();



                if (abstractRecord.CoMoribidityAnteriorSynechiae != null) CheckBoxCoMoribidityAnteriorSynechiae.Checked = ((bool)abstractRecord.CoMoribidityAnteriorSynechiae);
                if (abstractRecord.CoMoribidityDeepStromalVS != null) CheckBoxCoMoribidityDeepStromalVS.Checked = ((bool)abstractRecord.CoMoribidityDeepStromalVS);
                if (abstractRecord.CoMoribidityFilteringBleb != null) CheckBoxCoMoribidityFilteringBleb.Checked = ((bool)abstractRecord.CoMoribidityFilteringBleb);
                if (abstractRecord.CoMoribidityGlaucomaTS != null) CheckBoxCoMoribidityGlaucomaTS.Checked = ((bool)abstractRecord.CoMoribidityGlaucomaTS);
                if (abstractRecord.CoMoribidityVitreous != null) CheckBoxCoMoribidityVitreous.Checked = ((bool)abstractRecord.CoMoribidityVitreous);
                if (abstractRecord.CornealEdemaAngleClosure != null) CheckBoxCornealEdemaAngleClosure.Checked = ((bool)abstractRecord.CornealEdemaAngleClosure);
                if (abstractRecord.CornealEdemaAnterior != null) CheckBoxCornealEdemaAnterior.Checked = ((bool)abstractRecord.CornealEdemaAnterior);
                if (abstractRecord.CornealEdemacongenital != null) CheckBoxCornealEdemacongenital.Checked = ((bool)abstractRecord.CornealEdemacongenital);
                if (abstractRecord.CornealEdemaCongenitalEndo != null) CheckBoxCornealEdemaCongenitalEndo.Checked = ((bool)abstractRecord.CornealEdemaCongenitalEndo);
                if (abstractRecord.CornealEdemaCongenitalGlac != null) CheckBoxCornealEdemaCongenitalGlac.Checked = ((bool)abstractRecord.CornealEdemaCongenitalGlac);
                if (abstractRecord.CornealEdemaEndotheliitis != null) CheckBoxCornealEdemaEndotheliitis.Checked = ((bool)abstractRecord.CornealEdemaEndotheliitis);
                if (abstractRecord.CornealEdemaForceps != null) CheckBoxCornealEdemaForceps.Checked = ((bool)abstractRecord.CornealEdemaForceps);
                if (abstractRecord.CornealEdemaFuchs != null) CheckBoxCornealEdemaFuchs.Checked = ((bool)abstractRecord.CornealEdemaFuchs);
                if (abstractRecord.CornealEdemaGlaucoma != null) CheckBoxCornealEdemaGlaucoma.Checked = ((bool)abstractRecord.CornealEdemaGlaucoma);
                if (abstractRecord.CornealEdemaIridocorneal != null) CheckBoxCornealEdemaIridocorneal.Checked = ((bool)abstractRecord.CornealEdemaIridocorneal);
                if (abstractRecord.CornealEdemaOther != null) CheckBoxCornealEdemaOther.Checked = ((bool)abstractRecord.CornealEdemaOther);
                if (abstractRecord.CornealEdemaPosterior != null) CheckBoxCornealEdemaPosterior.Checked = ((bool)abstractRecord.CornealEdemaPosterior);
                if (abstractRecord.CornealEdemaPostopEdema != null) CheckBoxCornealEdemaPostopEdema.Checked = ((bool)abstractRecord.CornealEdemaPostopEdema);
                if (abstractRecord.CornealEdemaRetainedLens != null) CheckBoxCornealEdemaRetainedLens.Checked = ((bool)abstractRecord.CornealEdemaRetainedLens);
                if (abstractRecord.CornealEdemaShunt != null) CheckBoxCornealEdemaShunt.Checked = ((bool)abstractRecord.CornealEdemaShunt);
                if (abstractRecord.CornealEdemaSilicone != null) CheckBoxCornealEdemaSilicone.Checked = ((bool)abstractRecord.CornealEdemaSilicone);
                if (abstractRecord.CornealEdemaTrab != null) CheckBoxCornealEdemaTrab.Checked = ((bool)abstractRecord.CornealEdemaTrab);
                if (abstractRecord.CornealEdemaTrauma != null) CheckBoxCornealEdemaTrauma.Checked = ((bool)abstractRecord.CornealEdemaTrauma);
                if (abstractRecord.RBBCVAdistanceNotAbletoPerform != null) CheckBoxRBBCVAdistanceNotAbletoPerform.Checked = ((bool)abstractRecord.RBBCVAdistanceNotAbletoPerform);
             
                if (abstractRecord.CBStormalScaring != null) CheckBoxStormalScaring.Checked = ((bool)abstractRecord.CBStormalScaring);
            


                if (abstractRecord.PachymetryNA != null) RadioButtonPachymetryNA.Checked = ((bool)abstractRecord.PachymetryNA);
                if (abstractRecord.CornealEdemaUveitis != null) CheckBoxCornealEdemaUveitis.Checked = ((bool)abstractRecord.CornealEdemaUveitis);
                if (abstractRecord.CornealPachymetryNP != null) CheckBoxCornealPachymetryNP.Checked = ((bool)abstractRecord.CornealPachymetryNP);
                if (abstractRecord.LensACIOL != null) CheckBoxLensACIOL.Checked = ((bool)abstractRecord.LensACIOL);
                if (abstractRecord.LensAphakic != null) CheckBoxLensAphakic.Checked = ((bool)abstractRecord.LensAphakic);
                if (abstractRecord.LensIntracapsular != null) CheckBoxLensIntracapsular.Checked = ((bool)abstractRecord.LensIntracapsular);
                if (abstractRecord.LensIris != null) CheckBoxLensIris.Checked = ((bool)abstractRecord.LensIris);
                if (abstractRecord.LensPhakic != null) CheckBoxLensPhakic.Checked = ((bool)abstractRecord.LensPhakic);
                if (abstractRecord.LensPseudophakic != null) CheckBoxLensPseudophakic.Checked = ((bool)abstractRecord.LensPseudophakic);
                if (abstractRecord.LensSuclus != null) CheckBoxLensSuclus.Checked = ((bool)abstractRecord.LensSuclus);
                if (abstractRecord.SurgicalDMEK != null) CheckBoxSurgicalDMEK.Checked = ((bool)abstractRecord.SurgicalDMEK);
                if (abstractRecord.SurgicalDSEK != null) CheckBoxSurgicalDSEK.Checked = ((bool)abstractRecord.SurgicalDSEK);
                if (abstractRecord.SurgicalLAPK != null) CheckBoxSurgicalLAPK.Checked = ((bool)abstractRecord.SurgicalLAPK);
                if (abstractRecord.SurgicalPK != null) CheckBoxSurgicalPK.Checked = ((bool)abstractRecord.SurgicalPK);
                if (abstractRecord.BCVADistance != null) DropDownListBCVADistance.SelectedValue = abstractRecord.BCVADistance.ToString();
                if (abstractRecord.MonthOfBirth != null) DropDownListMonthOfBirth.SelectedValue = abstractRecord.MonthOfBirth.ToString();
                if (abstractRecord.MonthofExam != null) DropDownListMonthofExam.SelectedValue = abstractRecord.MonthofExam.ToString();
                if (abstractRecord.MonthofSurgery != null) DropDownListMonthofSurgery.SelectedValue = abstractRecord.MonthofSurgery.ToString();
                if (abstractRecord.OutcomeBCVA != null) DropDownListOutcomeBCVA.SelectedValue = abstractRecord.OutcomeBCVA.ToString();
                if (abstractRecord.YearOfBirth != null) DropDownListYearOfBirth.SelectedValue = abstractRecord.YearOfBirth.ToString();
                if (abstractRecord.YearofExam != null) DropDownListYearofExam.SelectedValue = abstractRecord.YearofExam.ToString();
                if (abstractRecord.YearofSurgery != null) DropDownListYearofSurgery.SelectedValue = abstractRecord.YearofSurgery.ToString();
                if (abstractRecord.CoMoribidityDeepStromalVSQuadrants != null) DropDownListMoribidityNumberOfQuadrants.SelectedValue = abstractRecord.CoMoribidityDeepStromalVSQuadrants.ToString();
               
                if (abstractRecord.CornealEndoImaging != null)
                {
                    RadioButtonCornealEndoImagingYes.Checked = ((bool)abstractRecord.CornealEndoImaging);
                    RadioButtonCornealEndoImagingNo.Checked = !((bool)abstractRecord.CornealEndoImaging);
                }
                if (abstractRecord.CornealGraftClear != null)
                {
                    RadioButtonCornealGraftClearYes.Checked = ((bool)abstractRecord.CornealGraftClear);
                    RadioButtonCornealGraftClearNo.Checked = !((bool)abstractRecord.CornealGraftClear);
                }
                if (abstractRecord.EndoRejection != null)
                {
                    RadioButtonEndoRejectionYes.Checked = ((bool)abstractRecord.EndoRejection);
                    RadioButtonEndoRejectionNo.Checked = !((bool)abstractRecord.EndoRejection);
                }
                if (abstractRecord.LateEndoFailure != null)
                {
                    RadioButtonLateEndoFailureYes.Checked = ((bool)abstractRecord.LateEndoFailure);
                    RadioButtonLateEndoFailureNo.Checked = !((bool)abstractRecord.LateEndoFailure);
                }
                if (abstractRecord.OtherCompIK != null)
                {
                    RadioButtonOtherCompIKYes.Checked = ((bool)abstractRecord.OtherCompIK);
                    RadioButtonOtherCompIKNo.Checked = !((bool)abstractRecord.OtherCompIK);
                }
                if (abstractRecord.OtherComplGlaucoma != null)
                {
                    RadioButtonOtherComplGlaucomaYes.Checked = ((bool)abstractRecord.OtherComplGlaucoma);
                    RadioButtonOtherComplGlaucomaNo.Checked = !((bool)abstractRecord.OtherComplGlaucoma);
                }
                if (abstractRecord.OtherCompLossEye != null)
                {
                    RadioButtonOtherCompLossEyeYes.Checked = ((bool)abstractRecord.OtherCompLossEye);
                    RadioButtonOtherCompLossEyeNo.Checked = !((bool)abstractRecord.OtherCompLossEye);
                }
                if (abstractRecord.OtherComplRetinalDetach != null)
                {
                    RadioButtonOtherComplRetinalDetachYes.Checked = ((bool)abstractRecord.OtherComplRetinalDetach);
                    RadioButtonOtherComplRetinalDetachNo.Checked = !((bool)abstractRecord.OtherComplRetinalDetach);
                }
                if (abstractRecord.OtherFailure != null)
                {
                    RadioButtonOtherFailureYes.Checked = ((bool)abstractRecord.OtherFailure);
                    RadioButtonOtherFailureNo.Checked = !((bool)abstractRecord.OtherFailure);
                }
                if (abstractRecord.PersistentEpithelialDefect != null)
                {
                    RadioButtonPersistentEpithelialDefectYes.Checked = ((bool)abstractRecord.PersistentEpithelialDefect);
                    RadioButtonPersistentEpithelialDefectNo.Checked = !((bool)abstractRecord.PersistentEpithelialDefect);
                }
                if (abstractRecord.PostopEndo != null)
                {
                    RadioButtonPostopEndoYes.Checked = ((bool)abstractRecord.PostopEndo);
                    RadioButtonPostopEndoNo.Checked = !((bool)abstractRecord.PostopEndo);
                }
                if (abstractRecord.PrimaryGraftFailure != null)
                {
                    RadioButtonPrimaryGraftFailureYes.Checked = ((bool)abstractRecord.PrimaryGraftFailure);
                    RadioButtonPrimaryGraftFailureNo.Checked = !((bool)abstractRecord.PrimaryGraftFailure);
                }
                if (abstractRecord.Reoperation != null)
                {
                    RadioButtonReoperationYes.Checked = ((bool)abstractRecord.Reoperation);
                    RadioButtonReoperationNo.Checked = !((bool)abstractRecord.Reoperation);
                }
                if (abstractRecord.SuprachoroidalHem != null)
                {
                    RadioButtonSuprachoroidalHemYes.Checked = ((bool)abstractRecord.SuprachoroidalHem);
                    RadioButtonSuprachoroidalHemNo.Checked = !((bool)abstractRecord.SuprachoroidalHem);
                }
                if (abstractRecord.WoundLeak != null)
                {
                    RadioButtonWoundLeakYes.Checked = ((bool)abstractRecord.WoundLeak);
                    RadioButtonWoundLeakNo.Checked = !((bool)abstractRecord.WoundLeak);
                }


                if (abstractRecord.Comorbidity != null)
                {
                    RadioButtonComorbidityYes.Checked = ((bool)abstractRecord.Comorbidity);
                    RadioButtonComorbidityNo.Checked = !((bool)abstractRecord.Comorbidity);
                }



                if (abstractRecord.RBAffectedEye != null) RadioButtonListRBAffectedEye.SelectedValue = abstractRecord.RBAffectedEye.ToString();

                if (abstractRecord.RBIntraocularPressure != null) RadioButtonListRBIntraocularPressure.SelectedValue = abstractRecord.RBIntraocularPressure.ToString();

                if (abstractRecord.RBAmantadine != null) RadioButtonListRBAmantadine.SelectedValue = abstractRecord.RBAmantadine.ToString();
                if (abstractRecord.RBDailyLiving != null) RadioButtonListRBDailyLiving.SelectedValue = abstractRecord.RBDailyLiving.ToString();
                if (abstractRecord.RBEndoRejectionGraftFail != null) RadioButtonListRBEndoRejectionGraftFail.SelectedValue = abstractRecord.RBEndoRejectionGraftFail.ToString();
                if (abstractRecord.RBEndothelialGraft != null) RadioButtonListRBEndothelialGraft.SelectedValue = abstractRecord.RBEndothelialGraft.ToString();
                if (abstractRecord.RBFamilyHistoryCornealDystrophy != null) RadioButtonListRBFamilyHistoryCornealDystrophy.SelectedValue = abstractRecord.RBFamilyHistoryCornealDystrophy.ToString();
                if (abstractRecord.RBFamilyHistoryCornealTransplantation != null) RadioButtonListRBFamilyHistoryCornealTransplantation.SelectedValue = abstractRecord.RBFamilyHistoryCornealTransplantation.ToString();
                if (abstractRecord.RBGenderTypeID != null) RadioButtonListRBGenderTypeID.SelectedValue = abstractRecord.RBGenderTypeID.ToString();
                if (abstractRecord.RBOcularDiscomfort != null) RadioButtonListRBOcularDiscomfort.SelectedValue = abstractRecord.RBOcularDiscomfort.ToString();
                if (abstractRecord.RBOcularSurgery != null) RadioButtonListRBOcularSurgery.SelectedValue = abstractRecord.RBOcularSurgery.ToString();
                if (abstractRecord.RBReliefofDiscomfort != null) RadioButtonListRBReliefofDiscomfort.SelectedValue = abstractRecord.RBReliefofDiscomfort.ToString();
                if (abstractRecord.RBVisualSymptoms != null) RadioButtonListRBVisualSymptoms.SelectedValue = abstractRecord.RBVisualSymptoms.ToString();

                if (abstractRecord.CornealPachymetryValue != null) TextBoxCornealPachymetryValue.Text = abstractRecord.CornealPachymetryValue.ToString();
                if (abstractRecord.Pachymetry != null) TextBoxPachymetry.Text = abstractRecord.Pachymetry.ToString();
                if (abstractRecord.ReoperationText != null) TextBoxReoperationText.Text = abstractRecord.ReoperationText.ToString();

            }
        }
    }

    public void savedata()
      {
          using (NetHealthPIMEntities pim = new NetHealthPIMEntities())
          {
              int ModuleID = (int)Session[Constants.SESSION_WORKINGMODULEID];
              int cycleID = ctx.ActiveModuleCycleID;
              ParticipantModuleCycle cycle = pim.ParticipantModuleCycle.First(c => c.ParticipantModuleCycleID == cycleID);
              string recordIdentifier = HiddenFieldRecordIdentifier.Value;
              using (TransactionScope transaction = new TransactionScope())
              {
                  ChartAbstractionCE abstractRecord = (from c in pim.ChartAbstractionCE
                                                          where c.ParticipantModuleCycle.ParticipantModuleCycleID == cycleID & c.RecordIdentifier == recordIdentifier
                                                      select c).First < ChartAbstractionCE>();




                  abstractRecord.CoMoribidityAnteriorSynechiae = CheckBoxCoMoribidityAnteriorSynechiae.Checked;
                  abstractRecord.CoMoribidityDeepStromalVS = CheckBoxCoMoribidityDeepStromalVS.Checked;
                  abstractRecord.CoMoribidityFilteringBleb = CheckBoxCoMoribidityFilteringBleb.Checked;
                  abstractRecord.CoMoribidityGlaucomaTS = CheckBoxCoMoribidityGlaucomaTS.Checked;
                  abstractRecord.CoMoribidityVitreous = CheckBoxCoMoribidityVitreous.Checked;
                  abstractRecord.CornealEdemaAngleClosure = CheckBoxCornealEdemaAngleClosure.Checked;
                  abstractRecord.CornealEdemaAnterior = CheckBoxCornealEdemaAnterior.Checked;
                  abstractRecord.CornealEdemacongenital = CheckBoxCornealEdemacongenital.Checked;
                  abstractRecord.CornealEdemaCongenitalEndo = CheckBoxCornealEdemaCongenitalEndo.Checked;
                  abstractRecord.CornealEdemaCongenitalGlac = CheckBoxCornealEdemaCongenitalGlac.Checked;
                  abstractRecord.CornealEdemaEndotheliitis = CheckBoxCornealEdemaEndotheliitis.Checked;
                  abstractRecord.CornealEdemaForceps = CheckBoxCornealEdemaForceps.Checked;
                  abstractRecord.CornealEdemaFuchs = CheckBoxCornealEdemaFuchs.Checked;
                  abstractRecord.CornealEdemaGlaucoma = CheckBoxCornealEdemaGlaucoma.Checked;
                  abstractRecord.CornealEdemaIridocorneal = CheckBoxCornealEdemaIridocorneal.Checked;
                  abstractRecord.CornealEdemaOther = CheckBoxCornealEdemaOther.Checked;
                  abstractRecord.CornealEdemaPosterior = CheckBoxCornealEdemaPosterior.Checked;
                  abstractRecord.CornealEdemaPostopEdema = CheckBoxCornealEdemaPostopEdema.Checked;
                  abstractRecord.CornealEdemaRetainedLens = CheckBoxCornealEdemaRetainedLens.Checked;
                  abstractRecord.CornealEdemaShunt = CheckBoxCornealEdemaShunt.Checked;
                  abstractRecord.CornealEdemaSilicone = CheckBoxCornealEdemaSilicone.Checked;
                  abstractRecord.CornealEdemaTrab = CheckBoxCornealEdemaTrab.Checked;
                  abstractRecord.CornealEdemaTrauma = CheckBoxCornealEdemaTrauma.Checked;
                  abstractRecord.CornealEdemaUveitis = CheckBoxCornealEdemaUveitis.Checked;
                  abstractRecord.CornealPachymetryNP = CheckBoxCornealPachymetryNP.Checked;
                  abstractRecord.LensACIOL = CheckBoxLensACIOL.Checked;
                  abstractRecord.LensAphakic = CheckBoxLensAphakic.Checked;
                  abstractRecord.LensIntracapsular = CheckBoxLensIntracapsular.Checked;
                  abstractRecord.LensIris = CheckBoxLensIris.Checked;
                  abstractRecord.LensPhakic = CheckBoxLensPhakic.Checked;
                  abstractRecord.LensPseudophakic = CheckBoxLensPseudophakic.Checked;
                  abstractRecord.LensSuclus = CheckBoxLensSuclus.Checked;
                  abstractRecord.RBBCVAdistanceNotAbletoPerform = CheckBoxRBBCVAdistanceNotAbletoPerform.Checked;
                  abstractRecord.SurgicalDMEK = CheckBoxSurgicalDMEK.Checked;
                  abstractRecord.SurgicalDSEK = CheckBoxSurgicalDSEK.Checked;
                  abstractRecord.SurgicalLAPK = CheckBoxSurgicalLAPK.Checked;
                  abstractRecord.SurgicalPK = CheckBoxSurgicalPK.Checked;
                  abstractRecord.PachymetryNA = RadioButtonPachymetryNA.Checked;

                  abstractRecord.CBStormalScaring = CheckBoxStormalScaring.Checked;
                  abstractRecord.CBCornealEdemaOther = CheckBoxStormalScaring.Checked;

                if (DropDownListBCVADistance.SelectedIndex > 0) abstractRecord.BCVADistance = Convert.ToInt32(DropDownListBCVADistance.SelectedValue);
                    else abstractRecord.BCVADistance = 0;
                    if (DropDownListMonthOfBirth.SelectedIndex > 0) abstractRecord.MonthOfBirth = Convert.ToInt32(DropDownListMonthOfBirth.SelectedValue);
                    else abstractRecord.MonthOfBirth = 0;
                    if (DropDownListMonthofExam.SelectedIndex > 0) abstractRecord.MonthofExam = Convert.ToInt32(DropDownListMonthofExam.SelectedValue);
                    else abstractRecord.MonthofExam = 0;
                    if (DropDownListMonthofSurgery.SelectedIndex > 0) abstractRecord.MonthofSurgery = Convert.ToInt32(DropDownListMonthofSurgery.SelectedValue);
                    else abstractRecord.MonthofSurgery = 0;
                    if (DropDownListOutcomeBCVA.SelectedIndex > 0) abstractRecord.OutcomeBCVA = Convert.ToInt32(DropDownListOutcomeBCVA.SelectedValue);
                    else abstractRecord.OutcomeBCVA = 0;
                    if (DropDownListYearOfBirth.SelectedIndex > 0) abstractRecord.YearOfBirth = Convert.ToInt32(DropDownListYearOfBirth.SelectedValue);
                    else abstractRecord.YearOfBirth = 0;
                    if (DropDownListYearofExam.SelectedIndex > 0) abstractRecord.YearofExam = Convert.ToInt32(DropDownListYearofExam.SelectedValue);
                    else abstractRecord.YearofExam = 0;

                    if (DropDownListYearofSurgery.SelectedIndex > 0) abstractRecord.YearofSurgery = Convert.ToInt32(DropDownListYearofSurgery.SelectedValue);
                    else abstractRecord.YearofSurgery = 0;



                    if (DropDownListMoribidityNumberOfQuadrants.SelectedIndex > 0) abstractRecord.CoMoribidityDeepStromalVSQuadrants = Convert.ToInt32(DropDownListMoribidityNumberOfQuadrants.SelectedValue);
                    else abstractRecord.CoMoribidityDeepStromalVSQuadrants = 0;




                    try
                    {
                    if (TextBoxCornealPachymetryValue.Text != null)
                        abstractRecord.CornealPachymetryValue = Convert.ToInt32(TextBoxCornealPachymetryValue.Text);
                    }
                    catch (Exception ex)
                    {
                    abstractRecord.CornealPachymetryValue = null;
                    TextBoxCornealPachymetryValue.Text = "";
                    }
                    try
                    {
                    if (TextBoxPachymetry.Text != null)
                        abstractRecord.Pachymetry = Convert.ToInt32(TextBoxPachymetry.Text);
                    }
                    catch (Exception ex)
                    {
                    abstractRecord.Pachymetry = null;
                    TextBoxPachymetry.Text = "";
                    }
                    try
                    {
                    if (TextBoxReoperationText.Text != null)
                    abstractRecord.ReoperationText = TextBoxReoperationText.Text;
                    }
                    catch (Exception ex)
                    {
                    abstractRecord.ReoperationText = null;
                    TextBoxReoperationText.Text = "";
                    }
                    if (RadioButtonListRBAffectedEye.SelectedIndex != -1) abstractRecord.RBAffectedEye = Convert.ToInt32(RadioButtonListRBAffectedEye.SelectedValue);
                    else abstractRecord.RBAffectedEye = 0;


                    if (RadioButtonListRBIntraocularPressure.SelectedIndex != -1) abstractRecord.RBIntraocularPressure = Convert.ToInt32(RadioButtonListRBIntraocularPressure.SelectedValue);
                    else abstractRecord.RBIntraocularPressure = 0;



                    if (RadioButtonListRBAmantadine.SelectedIndex != -1) abstractRecord.RBAmantadine = Convert.ToInt32(RadioButtonListRBAmantadine.SelectedValue);
                    else abstractRecord.RBAmantadine = 0;
                    if (RadioButtonListRBDailyLiving.SelectedIndex != -1) abstractRecord.RBDailyLiving = Convert.ToInt32(RadioButtonListRBDailyLiving.SelectedValue);
                    else abstractRecord.RBDailyLiving = 0;
                    if (RadioButtonListRBEndoRejectionGraftFail.SelectedIndex != -1) abstractRecord.RBEndoRejectionGraftFail = Convert.ToInt32(RadioButtonListRBEndoRejectionGraftFail.SelectedValue);
                    else abstractRecord.RBEndoRejectionGraftFail = 0;
                    if (RadioButtonListRBEndothelialGraft.SelectedIndex != -1) abstractRecord.RBEndothelialGraft = Convert.ToInt32(RadioButtonListRBEndothelialGraft.SelectedValue);
                    else abstractRecord.RBEndothelialGraft = 0;
                    if (RadioButtonListRBFamilyHistoryCornealDystrophy.SelectedIndex != -1) abstractRecord.RBFamilyHistoryCornealDystrophy = Convert.ToInt32(RadioButtonListRBFamilyHistoryCornealDystrophy.SelectedValue);
                    else abstractRecord.RBFamilyHistoryCornealDystrophy = 0;
                    if (RadioButtonListRBFamilyHistoryCornealTransplantation.SelectedIndex != -1) abstractRecord.RBFamilyHistoryCornealTransplantation = Convert.ToInt32(RadioButtonListRBFamilyHistoryCornealTransplantation.SelectedValue);
                    else abstractRecord.RBFamilyHistoryCornealTransplantation = 0;
                    if (RadioButtonListRBGenderTypeID.SelectedIndex != -1) abstractRecord.RBGenderTypeID = Convert.ToInt32(RadioButtonListRBGenderTypeID.SelectedValue);
                    else abstractRecord.RBGenderTypeID = 0;
                    if (RadioButtonListRBOcularDiscomfort.SelectedIndex != -1) abstractRecord.RBOcularDiscomfort = Convert.ToInt32(RadioButtonListRBOcularDiscomfort.SelectedValue);
                    else abstractRecord.RBOcularDiscomfort = 0;
                    if (RadioButtonListRBOcularSurgery.SelectedIndex != -1) abstractRecord.RBOcularSurgery = Convert.ToInt32(RadioButtonListRBOcularSurgery.SelectedValue);
                    else abstractRecord.RBOcularSurgery = 0;
                    if (RadioButtonListRBReliefofDiscomfort.SelectedIndex != -1) abstractRecord.RBReliefofDiscomfort = Convert.ToInt32(RadioButtonListRBReliefofDiscomfort.SelectedValue);
                    else abstractRecord.RBReliefofDiscomfort = 0;
                    if (RadioButtonListRBVisualSymptoms.SelectedIndex != -1) abstractRecord.RBVisualSymptoms = Convert.ToInt32(RadioButtonListRBVisualSymptoms.SelectedValue);
                    else abstractRecord.RBVisualSymptoms = 0;
                  if (RadioButtonCornealEndoImagingYes.Checked) abstractRecord.CornealEndoImaging = true;
                  if (RadioButtonCornealEndoImagingNo.Checked) abstractRecord.CornealEndoImaging = false;
                  if (RadioButtonCornealGraftClearYes.Checked) abstractRecord.CornealGraftClear = true;
                    if (RadioButtonCornealGraftClearNo.Checked) abstractRecord.CornealGraftClear = false;
                    if (RadioButtonEndoRejectionYes.Checked) abstractRecord.EndoRejection = true;
                    if (RadioButtonEndoRejectionNo.Checked) abstractRecord.EndoRejection = false;
                    if (RadioButtonLateEndoFailureYes.Checked) abstractRecord.LateEndoFailure = true;
                    if (RadioButtonLateEndoFailureNo.Checked) abstractRecord.LateEndoFailure = false;
                    if (RadioButtonOtherCompIKYes.Checked) abstractRecord.OtherCompIK = true;
                    if (RadioButtonOtherCompIKNo.Checked) abstractRecord.OtherCompIK = false;
                    if (RadioButtonOtherComplGlaucomaYes.Checked) abstractRecord.OtherComplGlaucoma = true;
                    if (RadioButtonOtherComplGlaucomaNo.Checked) abstractRecord.OtherComplGlaucoma = false;
                    if (RadioButtonOtherCompLossEyeYes.Checked) abstractRecord.OtherCompLossEye = true;
                    if (RadioButtonOtherCompLossEyeNo.Checked) abstractRecord.OtherCompLossEye = false;
                    if (RadioButtonOtherComplRetinalDetachYes.Checked) abstractRecord.OtherComplRetinalDetach = true;
                    if (RadioButtonOtherComplRetinalDetachNo.Checked) abstractRecord.OtherComplRetinalDetach = false;
                    if (RadioButtonOtherFailureYes.Checked) abstractRecord.OtherFailure = true;
                    if (RadioButtonOtherFailureNo.Checked) abstractRecord.OtherFailure = false;
                    if (RadioButtonPersistentEpithelialDefectYes.Checked) abstractRecord.PersistentEpithelialDefect = true;
                    if (RadioButtonPersistentEpithelialDefectNo.Checked) abstractRecord.PersistentEpithelialDefect = false;
                    if (RadioButtonPostopEndoYes.Checked) abstractRecord.PostopEndo = true;
                    if (RadioButtonPostopEndoNo.Checked) abstractRecord.PostopEndo = false;
                    if (RadioButtonPrimaryGraftFailureYes.Checked) abstractRecord.PrimaryGraftFailure = true;
                    if (RadioButtonPrimaryGraftFailureNo.Checked) abstractRecord.PrimaryGraftFailure = false;
                    if (RadioButtonReoperationYes.Checked) abstractRecord.Reoperation = true;
                    if (RadioButtonReoperationNo.Checked) abstractRecord.Reoperation = false;
                    if (RadioButtonSuprachoroidalHemYes.Checked) abstractRecord.SuprachoroidalHem = true;
                    if (RadioButtonSuprachoroidalHemNo.Checked) abstractRecord.SuprachoroidalHem = false;


                    if (RadioButtonComorbidityYes.Checked) abstractRecord.Comorbidity = true;
                    if (RadioButtonComorbidityNo.Checked) abstractRecord.Comorbidity = false;

                    if (RadioButtonWoundLeakYes.Checked) abstractRecord.WoundLeak = true;
                    if (RadioButtonWoundLeakNo.Checked) abstractRecord.WoundLeak = false;



                    abstractRecord.LastUpdateDate = DateTime.Now;
                    bool ChartCompleted = isComplete();
                    abstractRecord.Complete = ChartCompleted;
                    var chartRegistration = (from cr in pim.ChartRegistration
                                             where cr.ParticipantModuleCycleID == cycle.ParticipantModuleCycleID
                                              & cr.ModuleID == ModuleID
                                              & cr.RecordIdentifier == recordIdentifier
                                             select cr).First<NetHealthPIMModel.ChartRegistration>();
                    chartRegistration.AbstractCompleted = ChartCompleted;

                    if ((DropDownListYearOfBirth.SelectedIndex > 0) && (DropDownListYearOfBirth.SelectedIndex > 0))
                    {
                        chartRegistration.DOB = abstractRecord.MonthOfBirth + "/" + abstractRecord.YearOfBirth;
                    }
                   // if ((DropDownListMonthofExam.SelectedIndex > 0) && (DropDownListYearofExam.SelectedIndex > 0))
                   // {
                   //     ChartRegistration.InitialVisit = abstractRecord.MonthofExam + "/" + abstractRecord.YearofExam;
                   // }
                    pim.SaveChanges();

                    transaction.Complete();

                   


              }

              CycleManager.UpdateParticipantCompletedModules(cycleID, ModuleID);
              }


              }

    protected void ButtonSubmit_Click(object sender, EventArgs e)
    {
        savedata();
        isComplete();
        if (!isComplete())
        {
            isComplete();
            string script = " var answer = confirm('Chart data is not complete.\\nClick OK to save entries and exit chart.\\nClick CANCEL to save entries and review the chart. '); if (answer==true) window.location = '../PatientChartRegistration.aspx';";
            ScriptManager.RegisterStartupScript(this, GetType(),
                          "ServerControlScript", script, true);
        }
        else
        {

            Response.Redirect("../PatientChartRegistration.aspx");
        }
    }


        protected bool isComplete()
    
        {         
    
             bool ChartCompleted = true;



             LabelRBVisualSymptoms.Visible = false;


             LabelRBDailyLiving.Visible = false;

             LabelRBOcularSurgery.Visible = false;

             LabelRBAmantadine.Visible = false;

             LabelRBFamilyHistoryCornealDystrophy.Visible = false;

             LabelRBFamilyHistoryCornealTransplantation.Visible = false;

             LabelRBAffectedEye.Visible = false;

             LabelRBIntraocularPressure.Visible = false;

             LabelRBEndothelialGraft.Visible = false;

             LabelRBReliefofDiscomfort.Visible = false;

             LabelRBEndoRejectionGraftFail.Visible = false;



             LabelMonthOfBirth.Visible = false;

             LabelYearOfBirth.Visible = false;
             
             LabelMonthofSurgery.Visible = false;

             LabelYearofSurgery.Visible = false;

             LabelMonthofExam.Visible = false;

             LabelYearofExam.Visible = false;

             LabelOutcomeBCVA.Visible = false;


             LabelPostopEndo.Visible = false;

             LabelWoundLeak.Visible = false;

             LabelPersistentEpithelialDefect.Visible = false;

             LabelPrimaryGraftFailure.Visible = false;

             LabelReoperation.Visible = false;

             LabelComorbidity.Visible = false;

             LabelCornealGraftClear.Visible = false;

             LabelEndoRejection.Visible = false;

             LabelLateEndoFailure.Visible = false;

             LabelOtherFailure.Visible = false;

             LabelOtherComplGlaucoma.Visible = false;

             LabelOtherComplRetinalDetach.Visible = false;

             LabelOtherCompIK.Visible = false;

             LabelOtherCompLossEye.Visible = false;
             LabelPachymetry.Visible = false;
             LabelCornealPachymetry.Visible = false;
             LabelRBGenderTypeID.Visible = false;
             LabelBCVADistance.Visible = false;
	     LabelCoMoribidity.Visible = false;

	     if (CheckBoxCoMoribidityDeepStromalVS.Checked && (DropDownListMoribidityNumberOfQuadrants.SelectedIndex < 1))
	     {
		LabelCoMoribidity.Visible = true;
		ChartCompleted = false;
 	     }
             if (TextBoxPachymetry.Text == "" && RadioButtonPachymetryNA.Checked == false)
             {
                 LabelPachymetry.Visible = true;
                 ChartCompleted = false;
             }

             if (TextBoxCornealPachymetryValue.Text == "" && CheckBoxCornealPachymetryNP.Checked == false)
             {
                 LabelCornealPachymetry.Visible = true;
                 ChartCompleted = false;
             
             }
             if (DropDownListBCVADistance.SelectedIndex == 0 && CheckBoxRBBCVAdistanceNotAbletoPerform.Checked == false)

             {
                 LabelBCVADistance.Visible = true;
                 ChartCompleted = false;
             }
             if (RadioButtonListRBGenderTypeID.SelectedIndex == -1)

             {
                 ChartCompleted = false;
                 LabelRBGenderTypeID.Visible = true;
             }


            if (RadioButtonListRBVisualSymptoms.SelectedIndex == -1)
            {
                LabelRBVisualSymptoms.Visible = true;
                ChartCompleted = false;
            }
            if (RadioButtonListRBOcularDiscomfort.SelectedIndex == -1)
            {
                LabelRBOcularDiscomfort.Visible = true;
                ChartCompleted = false;
            }
            if (RadioButtonListRBDailyLiving.SelectedIndex == -1)
            {
                LabelRBDailyLiving.Visible = true;
                ChartCompleted = false;
            }
            if (RadioButtonListRBOcularSurgery.SelectedIndex == -1)
            {
                LabelRBOcularSurgery.Visible = true;
                ChartCompleted = false;
            }
            if (RadioButtonListRBAmantadine.SelectedIndex == -1)
            {
                LabelRBAmantadine.Visible = true;
                ChartCompleted = false;
            }
            if (RadioButtonListRBFamilyHistoryCornealDystrophy.SelectedIndex == -1)
            {
                LabelRBFamilyHistoryCornealDystrophy.Visible = true;
                ChartCompleted = false;
            }
            if (RadioButtonListRBFamilyHistoryCornealTransplantation.SelectedIndex == -1)
            {
                LabelRBFamilyHistoryCornealTransplantation.Visible = true;
                ChartCompleted = false;
            }
            if (RadioButtonListRBAffectedEye.SelectedIndex == -1)
            {
                LabelRBAffectedEye.Visible = true;
                ChartCompleted = false;
            }
            if (RadioButtonListRBIntraocularPressure.SelectedIndex == -1)
            {
                LabelRBIntraocularPressure.Visible = true;
                ChartCompleted = false;
            }
            if (RadioButtonListRBEndothelialGraft.SelectedIndex == -1)
            {
                LabelRBEndothelialGraft.Visible = true;
                ChartCompleted = false;
            }
            if (RadioButtonListRBReliefofDiscomfort.SelectedIndex == -1)
            {
                LabelRBReliefofDiscomfort.Visible = true;
                ChartCompleted = false;
            }
        



            if (DropDownListMonthOfBirth.SelectedIndex == 0)
            {
                LabelMonthOfBirth.Visible = true;
                ChartCompleted = false;
            }
            if (DropDownListYearOfBirth.SelectedIndex == 0)
            {
                LabelYearOfBirth.Visible = true;
                ChartCompleted = false;
            }
            if (DropDownListMonthofSurgery.SelectedIndex == 0)
            {
                LabelMonthofSurgery.Visible = true;
                ChartCompleted = false;
            }
            if (DropDownListYearofSurgery.SelectedIndex == 0)
            {
                LabelYearofSurgery.Visible = true;
                ChartCompleted = false;
            }
            if (DropDownListMonthofExam.SelectedIndex == 0)
            {
                LabelMonthofExam.Visible = true;
                ChartCompleted = false;
            }
            if (DropDownListYearofExam.SelectedIndex == 0)
            {
                LabelYearofExam.Visible = true;
                ChartCompleted = false;
            }
            if (DropDownListOutcomeBCVA.SelectedIndex == 0)
            {
                LabelOutcomeBCVA.Visible = true;
                ChartCompleted = false;
            }



            if (RadioButtonPostopEndoNo.Checked == false && RadioButtonPostopEndoYes.Checked == false)
            {
                LabelPostopEndo.Visible = true;
                ChartCompleted = false;
            }
            if (RadioButtonWoundLeakNo.Checked == false && RadioButtonWoundLeakYes.Checked == false)
            {
                LabelWoundLeak.Visible = true;
                ChartCompleted = false;
            }
            if (RadioButtonPersistentEpithelialDefectNo.Checked == false && RadioButtonPersistentEpithelialDefectYes.Checked == false)
            {
                LabelPersistentEpithelialDefect.Visible = true;
                ChartCompleted = false;
            }
            if (RadioButtonPrimaryGraftFailureNo.Checked == false && RadioButtonPrimaryGraftFailureYes.Checked == false)
            {
                LabelPrimaryGraftFailure.Visible = true;
                ChartCompleted = false;
            }
            if (RadioButtonReoperationNo.Checked == false && RadioButtonReoperationYes.Checked == false)
            {
                LabelReoperation.Visible = true;
                ChartCompleted = false;
            }
            if (RadioButtonComorbidityNo.Checked == false && RadioButtonComorbidityYes.Checked == false)
            {
                LabelComorbidity.Visible = true;
                ChartCompleted = false;
            }
            if (RadioButtonCornealGraftClearNo.Checked == false && RadioButtonCornealGraftClearYes.Checked == false)
            {
                LabelCornealGraftClear.Visible = true;
                ChartCompleted = false;
            }
            if (RadioButtonEndoRejectionNo.Checked == false && RadioButtonEndoRejectionYes.Checked == false)
            {
                LabelEndoRejection.Visible = true;
                ChartCompleted = false;
            }
            if (RadioButtonLateEndoFailureNo.Checked == false && RadioButtonLateEndoFailureYes.Checked == false)
            {
                LabelLateEndoFailure.Visible = true;
                ChartCompleted = false;
            }
            if (RadioButtonOtherFailureNo.Checked == false && RadioButtonOtherFailureYes.Checked == false)
            {
                LabelOtherFailure.Visible = true;
                ChartCompleted = false;
            }
            if (RadioButtonOtherComplGlaucomaNo.Checked == false && RadioButtonOtherComplGlaucomaYes.Checked == false)
            {
                LabelOtherComplGlaucoma.Visible = true;
                ChartCompleted = false;
            }
            if (RadioButtonOtherComplRetinalDetachNo.Checked == false && RadioButtonOtherComplRetinalDetachYes.Checked == false)
            {
                LabelOtherComplRetinalDetach.Visible = true;
                ChartCompleted = false;
            }
            if (RadioButtonOtherCompIKNo.Checked == false && RadioButtonOtherCompIKYes.Checked == false)
            {
                LabelOtherCompIK.Visible = true;
                ChartCompleted = false;
            }
            if (RadioButtonOtherCompLossEyeNo.Checked == false && RadioButtonOtherCompLossEyeYes.Checked == false)
            {
                LabelOtherCompLossEye.Visible = true;
                ChartCompleted = false;
            }

               return ChartCompleted;


}



}

