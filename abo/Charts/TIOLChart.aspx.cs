﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Transactions;
using NetHealthPIMModel;

public partial class abo_TIOLChart : BasePage
{
    protected void Page_Load(object sender, EventArgs e)
    {
    
           if (!IsPostBack)
        {
            InitializePage();
        }


    }
    protected void InitializePage()
    {
        ((PIMMasterPage)Page.Master).SetTitle("Chart");

        using (NetHealthPIMEntities pim = new NetHealthPIMEntities())
        {
            int ModuleID = (int)Session[Constants.SESSION_WORKINGMODULEID];
            Module module = pim.Module.First(c => c.ModuleID == ModuleID);
            ((PIMMasterPage)Page.Master).SetHeader(module.ModuleName + " Chart Abstraction");

            ParticipantModuleCycle prev = (ParticipantModuleCycle)Session[Constants.SESSION_PREVIOUSCYCLE];
            String CycleNumber = Request.QueryString["CycleNumber"];

            int cycleID = ctx.ActiveModuleCycleID;
            if (CycleNumber == "1")
            {
                cycleID = prev.ParticipantModuleCycleID;
                LinkButtonBackToDashboard.Visible = true;
                ButtonSubmit.Visible = false;
            }

            ParticipantModuleCycle cycle = pim.ParticipantModuleCycle.First(c => c.ParticipantModuleCycleID == cycleID);
            String numberOfRecord = Request.QueryString["nr"];
            String totalRecords = Request.QueryString["t"];
            if (cycle.CycleNumber == 1)
            {
                ((PIMMasterPage)Page.Master).SetStatus(2);
                LiteralAbstractionNumber.Text = "Initial Abstraction: Chart " + numberOfRecord + " of " + totalRecords;
            }
            else
            {
                ((PIMMasterPage)Page.Master).SetStatus(3);
                LiteralAbstractionNumber.Text = "Second Abstraction: Chart " + numberOfRecord + " of " + totalRecords;
            }
            DropDownListMonthOfBirth.DataSource = CycleManager.getMonthList();
            DropDownListYearOfBirth.DataSource = CycleManager.getDOBYearList(0, 100);
            DropDownListMonthOfSurgery.DataSource = CycleManager.getMonthList();
            DropDownListYearOfSurgery.DataSource = CycleManager.getDOBYearList(0, 100);
            DropDownListPostopBCVA.DataSource = CycleManager.getExamValues();
            DropDownListBCVA.DataSource = CycleManager.getExamValues();




            DataBind();
            DropDownListYearOfBirth.Items.Insert(0, new ListItem("Year", "0"));
            DropDownListYearOfSurgery.Items.Insert(0, new ListItem("Year", "0"));

            string recordIdentifier = Request.QueryString["ri"];
            HiddenFieldRecordIdentifier.Value = recordIdentifier;
            LiteralRecordIdentifier.Text = recordIdentifier;
            var count = pim.ChartAbstractionToricIOL.Where(ps => ps.ParticipantModuleCycle.ParticipantModuleCycleID == cycleID & ps.RecordIdentifier == recordIdentifier).Count();
            if (count == 0)
            {
                using (TransactionScope transaction = new TransactionScope())
                {
                    //Module module = pim.Module.First(m => m.ModuleID == Constants.ABO_AMBLYOPIA_MODULEID);

                    NetHealthPIMModel.ChartRegistration chartRegistration = (from cr in pim.ChartRegistration
                                                           where cr.ParticipantModuleCycleID == cycleID
                                                           & cr.RecordIdentifier == recordIdentifier
                                                           & cr.ModuleID == 52
                                                           select cr).First<NetHealthPIMModel.ChartRegistration>();

                    DateTime initialVisit = Convert.ToDateTime(chartRegistration.InitialVisit);
                    DateTime DOB = Convert.ToDateTime(chartRegistration.DOB);


                    ChartAbstractionToricIOL abstractRecord = new ChartAbstractionToricIOL();
                    abstractRecord.ParticipantModuleCycle = cycle;
                    abstractRecord.RecordIdentifier = recordIdentifier;
                    abstractRecord.MonthOfBirth = DOB.Month;
                    abstractRecord.YearOfBirth = DOB.Year;
                    DropDownListYearOfBirth.SelectedValue = abstractRecord.YearOfBirth.ToString();
                    DropDownListMonthOfBirth.SelectedValue = abstractRecord.MonthOfBirth.ToString();

                    abstractRecord.CreatedOn = DateTime.Now;
                    abstractRecord.LastUpdateDate = DateTime.Now;
                    pim.SaveChanges();

                    transaction.Complete();
                }
            }
            else
            {

                ChartAbstractionToricIOL abstractRecord = (from c in pim.ChartAbstractionToricIOL
                                                           where c.ParticipantModuleCycle.ParticipantModuleCycleID == cycleID & c.RecordIdentifier == recordIdentifier
                                                           select c).First<ChartAbstractionToricIOL>();


                if (abstractRecord.ExamRefractionNA != null) CheckBoxExamRefractionNA.Checked = ((bool)abstractRecord.ExamRefractionNA);
                if (abstractRecord.ExamIOPNA != null) CheckBoxExamIOPNA.Checked = ((bool)abstractRecord.ExamIOPNA);
                if (abstractRecord.ComorbidARMD != null) CheckBoxComorbidARMD.Checked = ((bool)abstractRecord.ComorbidARMD);
                if (abstractRecord.ComorbidBlepharitis != null) CheckBoxComorbidBlepharitis.Checked = ((bool)abstractRecord.ComorbidBlepharitis);
                if (abstractRecord.ComorbidDR != null) CheckBoxComorbidDR.Checked = ((bool)abstractRecord.ComorbidDR);
                if (abstractRecord.ComorbidGlaucoma != null) CheckBoxComorbidGlaucoma.Checked = ((bool)abstractRecord.ComorbidGlaucoma);
                if (abstractRecord.ComorbidPRD != null) CheckBoxComorbidPRD.Checked = ((bool)abstractRecord.ComorbidPRD);
                if (abstractRecord.ComorbidNone != null) CheckBoxComorbidNone.Checked = ((bool)abstractRecord.ComorbidNone);
                if (abstractRecord.ComorbidOther != null) CheckBoxComorbidOther.Checked = ((bool)abstractRecord.ComorbidOther);
                if (abstractRecord.PreopKerIOLMaster != null) CheckBoxPreopKerIOLMaster.Checked = ((bool)abstractRecord.PreopKerIOLMaster);
                if (abstractRecord.PreopKerCornealTop != null) CheckBoxPreopKerCornealTop.Checked = ((bool)abstractRecord.PreopKerCornealTop);
                if (abstractRecord.PreopKerManual != null) CheckBoxPreopKerManual.Checked = ((bool)abstractRecord.PreopKerManual);
                if (abstractRecord.PreopKerHandheld != null) CheckBoxPreopKerHandheld.Checked = ((bool)abstractRecord.PreopKerHandheld);
                if (abstractRecord.PreopKerOther != null) CheckBoxPreopKerOther.Checked = ((bool)abstractRecord.PreopKerOther);

                if (abstractRecord.SurgComplicationIris != null) CheckBoxSurgComplicationIris.Checked = ((bool)abstractRecord.SurgComplicationIris);
                if (abstractRecord.SurgComplicationLensFragVitreous != null) CheckBoxSurgComplicationLensFragVitreous.Checked = ((bool)abstractRecord.SurgComplicationLensFragVitreous);
                
                if (abstractRecord.SurgComplicationVitreousLoss != null) CheckBoxSurgComplicationVitreousLoss.Checked = ((bool)abstractRecord.SurgComplicationVitreousLoss);
                if (abstractRecord.SurgComplicationSuprchoroidal != null) CheckBoxSurgComplicationSuprchoroidal.Checked = ((bool)abstractRecord.SurgComplicationSuprchoroidal);
                if (abstractRecord.SurgComplicationZonular != null) CheckBoxSurgComplicationZonular.Checked = ((bool)abstractRecord.SurgComplicationZonular);
                if (abstractRecord.SurgComplicationNone != null) CheckBoxSurgComplicationNone.Checked = ((bool)abstractRecord.SurgComplicationNone);
                if (abstractRecord.SurgComplicationOther != null) CheckBoxSurgComplicationOther.Checked = ((bool)abstractRecord.SurgComplicationOther);
                if (abstractRecord.SurgAdjunctiveProcAntVitrectomy != null) CheckBoxSurgAdjunctiveProcAntVitrectomy.Checked = ((bool)abstractRecord.SurgAdjunctiveProcAntVitrectomy);
                if (abstractRecord.SurgAdjunctiveProcCapsularTensionRing != null) CheckBoxSurgAdjunctiveProcCapsularTensionRing.Checked = ((bool)abstractRecord.SurgAdjunctiveProcCapsularTensionRing);
                if (abstractRecord.SurgAdjunctiveProcIrisRetractors != null) CheckBoxSurgAdjunctiveProcIrisRetractors.Checked = ((bool)abstractRecord.SurgAdjunctiveProcIrisRetractors);
                if (abstractRecord.SurgAdjunctiveProcOther != null) CheckBoxSurgAdjunctiveProcOther.Checked = ((bool)abstractRecord.SurgAdjunctiveProcOther);
                if (abstractRecord.PostopRefractionNA != null) CheckBoxPostopRefractionNA.Checked = ((bool)abstractRecord.PostopRefractionNA);
                if (abstractRecord.PostopComplicationsNone != null) CheckBoxPostopComplicationsNone.Checked = ((bool)abstractRecord.PostopComplicationsNone);
                if (abstractRecord.PostopComplicationsCME != null) CheckBoxPostopComplicationsCME.Checked = ((bool)abstractRecord.PostopComplicationsCME);
                if (abstractRecord.PostopComplicationsDislocatedIOL != null) CheckBoxPostopComplicationsDislocatedIOL.Checked = ((bool)abstractRecord.PostopComplicationsDislocatedIOL);
                if (abstractRecord.PostopComplicationsEndophthalmits != null) CheckBoxPostopComplicationsEndophthalmits.Checked = ((bool)abstractRecord.PostopComplicationsEndophthalmits);
                if (abstractRecord.PostopComplicationsNewGlaucoma != null) CheckBoxPostopComplicationsNewGlaucoma.Checked = ((bool)abstractRecord.PostopComplicationsNewGlaucoma);
                if (abstractRecord.PostopComplicationsPseudophakicKer != null) CheckBoxPostopComplicationsPseudophakicKer.Checked = ((bool)abstractRecord.PostopComplicationsPseudophakicKer);
                if (abstractRecord.PostopComplicationsLensFragAnterior != null) CheckBoxPostopComplicationsLensFragAnterior.Checked = ((bool)abstractRecord.PostopComplicationsLensFragAnterior);
                if (abstractRecord.PostopComplicationsLensFragVitreous != null) CheckBoxPostopComplicationsLensFragVitreous.Checked = ((bool)abstractRecord.PostopComplicationsLensFragVitreous);
                if (abstractRecord.PostopComplicationsRetinalDetach != null) CheckBoxPostopComplicationsRetinalDetach.Checked = ((bool)abstractRecord.PostopComplicationsRetinalDetach);
                if (abstractRecord.PostopComplicationsRotationAbove15 != null) CheckBoxPostopComplicationsRotationAbove15.Checked = ((bool)abstractRecord.PostopComplicationsRotationAbove15);
                if (abstractRecord.PostopComplicationsTASS != null) CheckBoxPostopComplicationsTASS.Checked = ((bool)abstractRecord.PostopComplicationsTASS);
                if (abstractRecord.PostopComplicationsWoundLeak != null) CheckBoxPostopComplicationsWoundLeak.Checked = ((bool)abstractRecord.PostopComplicationsWoundLeak);
                if (abstractRecord.PostopComplicationWrongPower != null) CheckBoxPostopComplicationWrongPower.Checked = ((bool)abstractRecord.PostopComplicationWrongPower);
                 if (abstractRecord.AstigmatismDegreeNA!= null) CheckBoxAstigmatismDegreeNA.Checked = ((bool)abstractRecord.AstigmatismDegreeNA);
                 if (abstractRecord.PredictedResAstigmatismNO != null) CheckBoxPredictedResAstigmatismNO.Checked = ((bool)abstractRecord.PredictedResAstigmatismNO);


                if (abstractRecord.RBGender != null) RadioButtonListRBGender.SelectedValue = abstractRecord.RBGender.ToString();
                if (abstractRecord.RBSymptomsDailyLiving != null) RadioButtonListRBSymptomsDailyLiving.SelectedValue = abstractRecord.RBSymptomsDailyLiving.ToString();
                if (abstractRecord.RBCorrectiveLens != null) RadioButtonListRBCorrectiveLens.SelectedValue = abstractRecord.RBCorrectiveLens.ToString();
                if (abstractRecord.RBPastHistoryTrauma != null) RadioButtonListRBPastHistoryTrauma.SelectedValue = abstractRecord.RBPastHistoryTrauma.ToString();
                if (abstractRecord.RBPastHistoryRefractiveSurgery != null) RadioButtonListRBPastHistoryRefractiveSurgery.SelectedValue = abstractRecord.RBPastHistoryRefractiveSurgery.ToString();
                if (abstractRecord.RBPastHistoryOtherEyeSurgery != null) RadioButtonListRBPastHistoryOtherEyeSurgery.SelectedValue = abstractRecord.RBPastHistoryOtherEyeSurgery.ToString();
                if (abstractRecord.RBPastHistoryAmblyopia != null) RadioButtonListRBPastHistoryAmblyopia.SelectedValue = abstractRecord.RBPastHistoryAmblyopia.ToString();
                if (abstractRecord.RBPastHistoryAlpha1aAgonists != null) RadioButtonListRBPastHistoryAlpha1aAgonists.SelectedValue = abstractRecord.RBPastHistoryAlpha1aAgonists.ToString();
                if (abstractRecord.RBPastHistoryAnticoagulation != null) RadioButtonListRBPastHistoryAnticoagulation.SelectedValue = abstractRecord.RBPastHistoryAnticoagulation.ToString();
                if (abstractRecord.RBExamDilatedPupil != null) RadioButtonListRBExamDilatedPupil.SelectedValue = abstractRecord.RBExamDilatedPupil.ToString();
                if (abstractRecord.RBExamCornealEndothelium != null) RadioButtonListRBExamCornealEndothelium.SelectedValue = abstractRecord.RBExamCornealEndothelium.ToString();
                if (abstractRecord.RBExamCataractGrading != null) RadioButtonListRBExamCataractGrading.SelectedValue = abstractRecord.RBExamCataractGrading.ToString();
                if (abstractRecord.RBExamMacula != null) RadioButtonListRBExamMacula.SelectedValue = abstractRecord.RBExamMacula.ToString();
                if (abstractRecord.RBPreopRisks != null) RadioButtonListRBPreopRisks.SelectedValue = abstractRecord.RBPreopRisks.ToString();
                if (abstractRecord.RBPreopRefractiveGoals != null) RadioButtonListRBPreopRefractiveGoals.SelectedValue = abstractRecord.RBPreopRefractiveGoals.ToString();
                if (abstractRecord.RBPreopSignedConsent != null) RadioButtonListRBPreopSignedConsent.SelectedValue = abstractRecord.RBPreopSignedConsent.ToString();
                if (abstractRecord.RBPreopAxialLengthMeasure != null) RadioButtonListRBPreopAxialLengthMeasure.SelectedValue = abstractRecord.RBPreopAxialLengthMeasure.ToString();
                if (abstractRecord.RBPreopIOLPowerCalc != null) RadioButtonListRBPreopIOLPowerCalc.SelectedValue = abstractRecord.RBPreopIOLPowerCalc.ToString();
                if (abstractRecord.RBPreopToricIOLCalc != null) RadioButtonListRBPreopToricIOLCalc.SelectedValue = abstractRecord.RBPreopToricIOLCalc.ToString();
                if (abstractRecord.RBIOLBrand != null) RadioButtonListRBIOLBrand.SelectedValue = abstractRecord.RBIOLBrand.ToString();
                if (abstractRecord.RBSurgicalIntervention != null) RadioButtonListRBSurgicalIntervention.SelectedValue = abstractRecord.RBSurgicalIntervention.ToString();
                if (abstractRecord.RBIOLPlacement != null) RadioButtonListRBIOLPlacement.SelectedValue = abstractRecord.RBIOLPlacement.ToString();
                if (abstractRecord.RBPreopEyeMarkedPosition != null) RadioButtonListRBPreopEyeMarkedPosition.SelectedValue = abstractRecord.RBPreopEyeMarkedPosition.ToString();
                if (abstractRecord.RBPostopInstructions != null) RadioButtonListRBPostopInstructions.SelectedValue = abstractRecord.RBPostopInstructions.ToString();
                if (abstractRecord.RBPostopIOLWithin15Degrees != null) RadioButtonListRBPostopIOLWithin15Degrees.SelectedValue = abstractRecord.RBPostopIOLWithin15Degrees.ToString();
                if (abstractRecord.RBPostopBCVAReason != null) RadioButtonListRBPostopBCVAReason.SelectedValue = abstractRecord.RBPostopBCVAReason.ToString();
                if (abstractRecord.RBYagNeeded != null) RadioButtonListRBYagNeeded.SelectedValue = abstractRecord.RBYagNeeded.ToString();
                if (abstractRecord.RadioButtonListRBPreopKer != null) RadioButtonListRBPreopKer.SelectedValue = abstractRecord.RadioButtonListRBPreopKer.ToString();


                if (abstractRecord.MonthOfBirth != null) DropDownListMonthOfBirth.SelectedValue = abstractRecord.MonthOfBirth.ToString();
                if (abstractRecord.YearOfBirth != null) DropDownListYearOfBirth.SelectedValue = abstractRecord.YearOfBirth.ToString();
                if (abstractRecord.MonthOfSurgery != null) DropDownListMonthOfSurgery.SelectedValue = abstractRecord.MonthOfSurgery.ToString();
                if (abstractRecord.YearOfSurgery != null) DropDownListYearOfSurgery.SelectedValue = abstractRecord.YearOfSurgery.ToString();
                if (abstractRecord.BCVA != null) DropDownListBCVA.SelectedValue = abstractRecord.BCVA.ToString();
                if (abstractRecord.PostopBCVA != null) DropDownListPostopBCVA.SelectedValue = abstractRecord.PostopBCVA.ToString();

                if (abstractRecord.ExamPseudoMaterial != null)
                {
                    RadioButtonExamPseudoMaterialYes.Checked = ((bool)abstractRecord.ExamPseudoMaterial);
                    RadioButtonExamPseudoMaterialNo.Checked = !((bool)abstractRecord.ExamPseudoMaterial);
                }

                if (abstractRecord.ExamSlitLamp != null)
                {
                    RadioButtonExamSlitLampYes.Checked = ((bool)abstractRecord.ExamSlitLamp);
                    RadioButtonExamSlitLampNo.Checked = !((bool)abstractRecord.ExamSlitLamp);
                }
                if (abstractRecord.PreopToricIOLMedRecord != null)
                {
                    RadioButtonPreopToricIOLMedRecordYes.Checked = ((bool)abstractRecord.PreopToricIOLMedRecord);
                    RadioButtonPreopToricIOLMedRecordNo.Checked = !((bool)abstractRecord.PreopToricIOLMedRecord);
                }
              
                if (abstractRecord.SurgIntraopComplications != null)
                {
                    RadioButtonSurgIntraopComplicationsYes.Checked = ((bool)abstractRecord.SurgIntraopComplications);
                    RadioButtonSurgIntraopComplicationsNo.Checked = !((bool)abstractRecord.SurgIntraopComplications);
                }
                if (abstractRecord.SurgAdjunctiveProc != null)
                {
                    RadioButtonSurgAdjunctiveProcYes.Checked = ((bool)abstractRecord.SurgAdjunctiveProc);
                    RadioButtonSurgAdjunctiveProcNo.Checked = !((bool)abstractRecord.SurgAdjunctiveProc);
                }
                if (abstractRecord.PostopAntibioticDrops != null)
                {
                    RadioButtonPostopAntibioticDropsYes.Checked = ((bool)abstractRecord.PostopAntibioticDrops);
                    RadioButtonPostopAntibioticDropsNo.Checked = !((bool)abstractRecord.PostopAntibioticDrops);
                }
                if (abstractRecord.PostopAntiInflamDrops != null)
                {
                    RadioButtonPostopAntiInflamDropsYes.Checked = ((bool)abstractRecord.PostopAntiInflamDrops);
                    RadioButtonPostopAntiInflamDropsNo.Checked = !((bool)abstractRecord.PostopAntiInflamDrops);
                }
                if (abstractRecord.PostopFurtherSurg != null)
                {
                    RadioButtonPostopFurtherSurgYes.Checked = ((bool)abstractRecord.PostopFurtherSurg);
                    RadioButtonPostopFurtherSurgNo.Checked = !((bool)abstractRecord.PostopFurtherSurg);
                }

                if (abstractRecord.ExamDilatedFundus!= null)
                {
                    RadioButtonExamDilatedFundusYes.Checked = ((bool)abstractRecord.ExamDilatedFundus);
                    RadioButtonExamDilatedFundusNo.Checked = !((bool)abstractRecord.ExamDilatedFundus);
                }

                if (abstractRecord.ExamRefraction != null) TextBoxExamRefraction.Text = abstractRecord.ExamRefraction.ToString();
                if (abstractRecord.ExamRefraction2 != null) TextBoxExamRefraction2.Text = abstractRecord.ExamRefraction2.ToString();
                if (abstractRecord.ExamRefaction3 != null) TextBoxExamRefaction3.Text = abstractRecord.ExamRefaction3.ToString();
                if (abstractRecord.ExamIOP != null) TextBoxExamIOP.Text = abstractRecord.ExamIOP.ToString();
                if (abstractRecord.ComorbidOtherText != null) TextBoxComorbidOtherText.Text = abstractRecord.ComorbidOtherText.ToString();
                if (abstractRecord.PreopKerOtherText != null) TextBoxPreopKerOtherText.Text = abstractRecord.PreopKerOtherText.ToString();
                if (abstractRecord.PreopKerCalcOtherText != null) TextBoxPreopKerCalcOtherText.Text = abstractRecord.PreopKerCalcOtherText.ToString();
                if (abstractRecord.PreopKeFlatValue1 != null) TextBoxPreopKeFlatValue1.Text = abstractRecord.PreopKeFlatValue1.ToString();
                if (abstractRecord.PreopKerFlatValue2 != null) TextBoxPreopKerFlatValue2.Text = abstractRecord.PreopKerFlatValue2.ToString();
                if (abstractRecord.PreopKerFlatValue3 != null) TextBoxPreopKerFlatValue3.Text = abstractRecord.PreopKerFlatValue3.ToString();
                if (abstractRecord.PreopKerSteepValue1 != null) TextBoxPreopKerSteepValue1.Text = abstractRecord.PreopKerSteepValue1.ToString();
                if (abstractRecord.PreopKerSteepValue2 != null) TextBoxPreopKerSteepValue2.Text = abstractRecord.PreopKerSteepValue2.ToString();
                if (abstractRecord.PreopKerSteepValue3 != null) TextBoxPreopKerSteepValue3.Text = abstractRecord.PreopKerSteepValue3.ToString();
                if (abstractRecord.PreopIOLPolwerCalcOther != null) TextBoxPreopIOLPolwerCalcOther.Text = abstractRecord.PreopIOLPolwerCalcOther.ToString();
                if (abstractRecord.RefractiveTarget != null) TextBoxRefractiveTarget.Text = abstractRecord.RefractiveTarget.ToString();
                if (abstractRecord.PredictedResAstigmatism != null) TextBoxPredictedResAstigmatism.Text = abstractRecord.PredictedResAstigmatism.ToString();
                if (abstractRecord.PredictedResAstigmatismDegree != null) TextBoxPredictedResAstigmatismDegree.Text = abstractRecord.PredictedResAstigmatismDegree.ToString();
                if (abstractRecord.IOLBrandOtherText != null) TextBoxIOLBrandOtherText.Text = abstractRecord.IOLBrandOtherText.ToString();
                if (abstractRecord.IOLPowerSelectedSpherical != null) TextBoxIOLPowerSelectedSpherical.Text = abstractRecord.IOLPowerSelectedSpherical.ToString();
                if (abstractRecord.IOLPowerSelectedCylindrical != null) TextBoxIOLPowerSelectedCylindrical.Text = abstractRecord.IOLPowerSelectedCylindrical.ToString();
                if (abstractRecord.IOLPlacementPlannedAxis != null) TextBoxIOLPlacementPlannedAxis.Text = abstractRecord.IOLPlacementPlannedAxis.ToString();
                if (abstractRecord.SurgComplicationOtherText != null) TextBoxSurgComplicationOtherText.Text = abstractRecord.SurgComplicationOtherText.ToString();
                if (abstractRecord.SurgAdjunctiveProcOtherText != null) TextBoxSurgAdjunctiveProcOtherText.Text = abstractRecord.SurgAdjunctiveProcOtherText.ToString();
                if (abstractRecord.PostopRefraction1 != null) TextBoxPostopRefraction1.Text = abstractRecord.PostopRefraction1.ToString();
                if (abstractRecord.PostopRefraction2 != null) TextBoxPostopRefraction2.Text = abstractRecord.PostopRefraction2.ToString();
                if (abstractRecord.PostopRefraction3 != null) TextBoxPostopRefraction3.Text = abstractRecord.PostopRefraction3.ToString();
                if (abstractRecord.PostopAstigmatism != null) TextBoxPostopAstigmatism.Text = abstractRecord.PostopAstigmatism.ToString();
                if (abstractRecord.PostopAstigmatismDegree != null) TextBoxPostopAstigmatismDegree.Text = abstractRecord.PostopAstigmatismDegree.ToString();
                if (abstractRecord.RBPostopBCVAReasonOtherText != null) TextBoxRBPostopBCVAReasonOtherText.Text = abstractRecord.RBPostopBCVAReasonOtherText.ToString();
                if (abstractRecord.PostopFurtherSurgReason != null) TextBoxPostopFurtherSurgReason.Text = abstractRecord.PostopFurtherSurgReason.ToString();










            }
        }
    }


    public void savedata()
    {
        using (NetHealthPIMEntities pim = new NetHealthPIMEntities())
        {
            int ModuleID = (int)Session[Constants.SESSION_WORKINGMODULEID];
            int cycleID = ctx.ActiveModuleCycleID;
            ParticipantModuleCycle cycle = pim.ParticipantModuleCycle.First(c => c.ParticipantModuleCycleID == cycleID);
            string recordIdentifier = HiddenFieldRecordIdentifier.Value;
            using (TransactionScope transaction = new TransactionScope())
            {
                ChartAbstractionToricIOL abstractRecord = (from c in pim.ChartAbstractionToricIOL
                                                           where c.ParticipantModuleCycle.ParticipantModuleCycleID == cycleID & c.RecordIdentifier == recordIdentifier
                                                           select c).First<ChartAbstractionToricIOL>();



                abstractRecord.ExamRefractionNA = CheckBoxExamRefractionNA.Checked;
                abstractRecord.ExamIOPNA = CheckBoxExamIOPNA.Checked;
                abstractRecord.ComorbidARMD = CheckBoxComorbidARMD.Checked;
                abstractRecord.ComorbidBlepharitis = CheckBoxComorbidBlepharitis.Checked;
                abstractRecord.ComorbidDR = CheckBoxComorbidDR.Checked;
                abstractRecord.ComorbidGlaucoma = CheckBoxComorbidGlaucoma.Checked;
                abstractRecord.ComorbidPRD = CheckBoxComorbidPRD.Checked;
                abstractRecord.ComorbidNone = CheckBoxComorbidNone.Checked;
                abstractRecord.ComorbidOther = CheckBoxComorbidOther.Checked;
                abstractRecord.PreopKerIOLMaster = CheckBoxPreopKerIOLMaster.Checked;
                abstractRecord.PreopKerCornealTop = CheckBoxPreopKerCornealTop.Checked;
                abstractRecord.PreopKerManual = CheckBoxPreopKerManual.Checked;
                abstractRecord.PreopKerHandheld = CheckBoxPreopKerHandheld.Checked;
                abstractRecord.PreopKerOther = CheckBoxPreopKerOther.Checked;

                abstractRecord.SurgComplicationIris = CheckBoxSurgComplicationIris.Checked;
                abstractRecord.SurgComplicationLensFragVitreous = CheckBoxSurgComplicationLensFragVitreous.Checked;
                abstractRecord.SurgComplicationCapsularRupture = CheckBoxSurgComplicationCapsularRupture.Checked;
                abstractRecord.SurgComplicationVitreousLoss = CheckBoxSurgComplicationVitreousLoss.Checked;
                abstractRecord.SurgComplicationSuprchoroidal = CheckBoxSurgComplicationSuprchoroidal.Checked;
                abstractRecord.SurgComplicationZonular = CheckBoxSurgComplicationZonular.Checked;
                abstractRecord.SurgComplicationNone = CheckBoxSurgComplicationNone.Checked;
                abstractRecord.SurgComplicationOther = CheckBoxSurgComplicationOther.Checked;
                abstractRecord.SurgAdjunctiveProcAntVitrectomy = CheckBoxSurgAdjunctiveProcAntVitrectomy.Checked;
                abstractRecord.SurgAdjunctiveProcCapsularTensionRing = CheckBoxSurgAdjunctiveProcCapsularTensionRing.Checked;
                abstractRecord.SurgAdjunctiveProcIrisRetractors = CheckBoxSurgAdjunctiveProcIrisRetractors.Checked;
                abstractRecord.SurgAdjunctiveProcOther = CheckBoxSurgAdjunctiveProcOther.Checked;
                abstractRecord.PostopRefractionNA = CheckBoxPostopRefractionNA.Checked;
                abstractRecord.PostopComplicationsNone = CheckBoxPostopComplicationsNone.Checked;
                abstractRecord.PostopComplicationsCME = CheckBoxPostopComplicationsCME.Checked;
                abstractRecord.PostopComplicationsDislocatedIOL = CheckBoxPostopComplicationsDislocatedIOL.Checked;
                abstractRecord.PostopComplicationsEndophthalmits = CheckBoxPostopComplicationsEndophthalmits.Checked;
                abstractRecord.PostopComplicationsNewGlaucoma = CheckBoxPostopComplicationsNewGlaucoma.Checked;
                abstractRecord.PostopComplicationsPseudophakicKer = CheckBoxPostopComplicationsPseudophakicKer.Checked;
                abstractRecord.PostopComplicationsLensFragAnterior = CheckBoxPostopComplicationsLensFragAnterior.Checked;
                abstractRecord.PostopComplicationsLensFragVitreous = CheckBoxPostopComplicationsLensFragVitreous.Checked;
                abstractRecord.PostopComplicationsRetinalDetach = CheckBoxPostopComplicationsRetinalDetach.Checked;
                abstractRecord.PostopComplicationsRotationAbove15 = CheckBoxPostopComplicationsRotationAbove15.Checked;
                abstractRecord.PostopComplicationsTASS = CheckBoxPostopComplicationsTASS.Checked;
                abstractRecord.PostopComplicationsWoundLeak = CheckBoxPostopComplicationsWoundLeak.Checked;
                abstractRecord.PostopComplicationWrongPower = CheckBoxPostopComplicationWrongPower.Checked;
                abstractRecord.AstigmatismDegreeNA =  CheckBoxAstigmatismDegreeNA.Checked;
                abstractRecord.PredictedResAstigmatismNO =  CheckBoxPredictedResAstigmatismNO.Checked;

                if (RadioButtonListRBPreopKer.SelectedIndex != -1) abstractRecord.RadioButtonListRBPreopKer = Convert.ToInt32(RadioButtonListRBPreopKer.SelectedValue);
                else abstractRecord.RadioButtonListRBPreopKer = 0;


                if (RadioButtonListRBGender.SelectedIndex != -1) abstractRecord.RBGender = Convert.ToInt32(RadioButtonListRBGender.SelectedValue);
                else abstractRecord.RBGender = 0;
                if (RadioButtonListRBSymptomsDailyLiving.SelectedIndex != -1) abstractRecord.RBSymptomsDailyLiving = Convert.ToInt32(RadioButtonListRBSymptomsDailyLiving.SelectedValue);
                else abstractRecord.RBSymptomsDailyLiving = 0;
                if (RadioButtonListRBCorrectiveLens.SelectedIndex != -1) abstractRecord.RBCorrectiveLens = Convert.ToInt32(RadioButtonListRBCorrectiveLens.SelectedValue);
                else abstractRecord.RBCorrectiveLens = 0;
                if (RadioButtonListRBPastHistoryTrauma.SelectedIndex != -1) abstractRecord.RBPastHistoryTrauma = Convert.ToInt32(RadioButtonListRBPastHistoryTrauma.SelectedValue);
                else abstractRecord.RBPastHistoryTrauma = 0;
                if (RadioButtonListRBPastHistoryRefractiveSurgery.SelectedIndex != -1) abstractRecord.RBPastHistoryRefractiveSurgery = Convert.ToInt32(RadioButtonListRBPastHistoryRefractiveSurgery.SelectedValue);
                else abstractRecord.RBPastHistoryRefractiveSurgery = 0;
                if (RadioButtonListRBPastHistoryOtherEyeSurgery.SelectedIndex != -1) abstractRecord.RBPastHistoryOtherEyeSurgery = Convert.ToInt32(RadioButtonListRBPastHistoryOtherEyeSurgery.SelectedValue);
                else abstractRecord.RBPastHistoryOtherEyeSurgery = 0;
                if (RadioButtonListRBPastHistoryAmblyopia.SelectedIndex != -1) abstractRecord.RBPastHistoryAmblyopia = Convert.ToInt32(RadioButtonListRBPastHistoryAmblyopia.SelectedValue);
                else abstractRecord.RBPastHistoryAmblyopia = 0;
                if (RadioButtonListRBPastHistoryAlpha1aAgonists.SelectedIndex != -1) abstractRecord.RBPastHistoryAlpha1aAgonists = Convert.ToInt32(RadioButtonListRBPastHistoryAlpha1aAgonists.SelectedValue);
                else abstractRecord.RBPastHistoryAlpha1aAgonists = 0;
                if (RadioButtonListRBPastHistoryAnticoagulation.SelectedIndex != -1) abstractRecord.RBPastHistoryAnticoagulation = Convert.ToInt32(RadioButtonListRBPastHistoryAnticoagulation.SelectedValue);
                else abstractRecord.RBPastHistoryAnticoagulation = 0;
                if (RadioButtonListRBExamDilatedPupil.SelectedIndex != -1) abstractRecord.RBExamDilatedPupil = Convert.ToInt32(RadioButtonListRBExamDilatedPupil.SelectedValue);
                else abstractRecord.RBExamDilatedPupil = 0;
                if (RadioButtonListRBExamCornealEndothelium.SelectedIndex != -1) abstractRecord.RBExamCornealEndothelium = Convert.ToInt32(RadioButtonListRBExamCornealEndothelium.SelectedValue);
                else abstractRecord.RBExamCornealEndothelium = 0;
                if (RadioButtonListRBExamCataractGrading.SelectedIndex != -1) abstractRecord.RBExamCataractGrading = Convert.ToInt32(RadioButtonListRBExamCataractGrading.SelectedValue);
                else abstractRecord.RBExamCataractGrading = 0;
                if (RadioButtonListRBExamMacula.SelectedIndex != -1) abstractRecord.RBExamMacula = Convert.ToInt32(RadioButtonListRBExamMacula.SelectedValue);
                else abstractRecord.RBExamMacula = 0;
                if (RadioButtonListRBPreopRisks.SelectedIndex != -1) abstractRecord.RBPreopRisks = Convert.ToInt32(RadioButtonListRBPreopRisks.SelectedValue);
                else abstractRecord.RBPreopRisks = 0;
                if (RadioButtonListRBPreopRefractiveGoals.SelectedIndex != -1) abstractRecord.RBPreopRefractiveGoals = Convert.ToInt32(RadioButtonListRBPreopRefractiveGoals.SelectedValue);
                else abstractRecord.RBPreopRefractiveGoals = 0;
                if (RadioButtonListRBPreopSignedConsent.SelectedIndex != -1) abstractRecord.RBPreopSignedConsent = Convert.ToInt32(RadioButtonListRBPreopSignedConsent.SelectedValue);
                else abstractRecord.RBPreopSignedConsent = 0;
                if (RadioButtonListRBPreopAxialLengthMeasure.SelectedIndex != -1) abstractRecord.RBPreopAxialLengthMeasure = Convert.ToInt32(RadioButtonListRBPreopAxialLengthMeasure.SelectedValue);
                else abstractRecord.RBPreopAxialLengthMeasure = 0;
                if (RadioButtonListRBPreopIOLPowerCalc.SelectedIndex != -1) abstractRecord.RBPreopIOLPowerCalc = Convert.ToInt32(RadioButtonListRBPreopIOLPowerCalc.SelectedValue);
                else abstractRecord.RBPreopIOLPowerCalc = 0;
                if (RadioButtonListRBPreopToricIOLCalc.SelectedIndex != -1) abstractRecord.RBPreopToricIOLCalc = Convert.ToInt32(RadioButtonListRBPreopToricIOLCalc.SelectedValue);
                else abstractRecord.RBPreopToricIOLCalc = 0;
                if (RadioButtonListRBIOLBrand.SelectedIndex != -1) abstractRecord.RBIOLBrand = Convert.ToInt32(RadioButtonListRBIOLBrand.SelectedValue);
                else abstractRecord.RBIOLBrand = 0;
                if (RadioButtonListRBSurgicalIntervention.SelectedIndex != -1) abstractRecord.RBSurgicalIntervention = Convert.ToInt32(RadioButtonListRBSurgicalIntervention.SelectedValue);
                else abstractRecord.RBSurgicalIntervention = 0;
                if (RadioButtonListRBIOLPlacement.SelectedIndex != -1) abstractRecord.RBIOLPlacement = Convert.ToInt32(RadioButtonListRBIOLPlacement.SelectedValue);
                else abstractRecord.RBIOLPlacement = 0;
                if (RadioButtonListRBPreopEyeMarkedPosition.SelectedIndex != -1) abstractRecord.RBPreopEyeMarkedPosition = Convert.ToInt32(RadioButtonListRBPreopEyeMarkedPosition.SelectedValue);
                else abstractRecord.RBPreopEyeMarkedPosition = 0;
                if (RadioButtonListRBPostopInstructions.SelectedIndex != -1) abstractRecord.RBPostopInstructions = Convert.ToInt32(RadioButtonListRBPostopInstructions.SelectedValue);
                else abstractRecord.RBPostopInstructions = 0;
                if (RadioButtonListRBPostopIOLWithin15Degrees.SelectedIndex != -1) abstractRecord.RBPostopIOLWithin15Degrees = Convert.ToInt32(RadioButtonListRBPostopIOLWithin15Degrees.SelectedValue);
                else abstractRecord.RBPostopIOLWithin15Degrees = 0;
                if (RadioButtonListRBPostopBCVAReason.SelectedIndex != -1) abstractRecord.RBPostopBCVAReason = Convert.ToInt32(RadioButtonListRBPostopBCVAReason.SelectedValue);
                else abstractRecord.RBPostopBCVAReason = 0;
                if (RadioButtonListRBYagNeeded.SelectedIndex != -1) abstractRecord.RBYagNeeded = Convert.ToInt32(RadioButtonListRBYagNeeded.SelectedValue);
                else abstractRecord.RBYagNeeded = 0;



                if (DropDownListMonthOfBirth.SelectedIndex > 0) abstractRecord.MonthOfBirth = Convert.ToInt32(DropDownListMonthOfBirth.SelectedValue);
                else abstractRecord.MonthOfBirth = 0;
                if (DropDownListYearOfBirth.SelectedIndex > 0) abstractRecord.YearOfBirth = Convert.ToInt32(DropDownListYearOfBirth.SelectedValue);
                else abstractRecord.YearOfBirth = 0;
                if (DropDownListMonthOfSurgery.SelectedIndex > 0) abstractRecord.MonthOfSurgery = Convert.ToInt32(DropDownListMonthOfSurgery.SelectedValue);
                else abstractRecord.MonthOfSurgery = 0;
                if (DropDownListYearOfSurgery.SelectedIndex > 0) abstractRecord.YearOfSurgery = Convert.ToInt32(DropDownListYearOfSurgery.SelectedValue);
                else abstractRecord.YearOfSurgery = 0;
                if (DropDownListBCVA.SelectedIndex > 0) abstractRecord.BCVA = Convert.ToInt32(DropDownListBCVA.SelectedValue);
                else abstractRecord.BCVA = 0;
                if (DropDownListPostopBCVA.SelectedIndex > 0) abstractRecord.PostopBCVA = Convert.ToInt32(DropDownListPostopBCVA.SelectedValue);
                else abstractRecord.PostopBCVA = 0;


                if (RadioButtonExamPseudoMaterialYes.Checked) abstractRecord.ExamPseudoMaterial = true;
                if (RadioButtonExamPseudoMaterialNo.Checked) abstractRecord.ExamPseudoMaterial = false;

                if (RadioButtonExamDilatedFundusYes.Checked) abstractRecord.ExamDilatedFundus = true;
                if (RadioButtonExamDilatedFundusNo.Checked) abstractRecord.ExamDilatedFundus = false; ;
                if (RadioButtonExamSlitLampYes.Checked) abstractRecord.ExamSlitLamp = true;
                if (RadioButtonExamSlitLampNo.Checked) abstractRecord.ExamSlitLamp = false;
                if (RadioButtonPreopToricIOLMedRecordYes.Checked) abstractRecord.PreopToricIOLMedRecord = true;
                if (RadioButtonPreopToricIOLMedRecordNo.Checked) abstractRecord.PreopToricIOLMedRecord = false;
           
                if (RadioButtonSurgIntraopComplicationsYes.Checked) abstractRecord.SurgIntraopComplications = true;
                if (RadioButtonSurgIntraopComplicationsNo.Checked) abstractRecord.SurgIntraopComplications = false;
                if (RadioButtonSurgAdjunctiveProcYes.Checked) abstractRecord.SurgAdjunctiveProc = true;
                if (RadioButtonSurgAdjunctiveProcNo.Checked) abstractRecord.SurgAdjunctiveProc = false;
                if (RadioButtonPostopAntibioticDropsYes.Checked) abstractRecord.PostopAntibioticDrops = true;
                if (RadioButtonPostopAntibioticDropsNo.Checked) abstractRecord.PostopAntibioticDrops = false;
                if (RadioButtonPostopAntiInflamDropsYes.Checked) abstractRecord.PostopAntiInflamDrops = true;
                if (RadioButtonPostopAntiInflamDropsNo.Checked) abstractRecord.PostopAntiInflamDrops = false;
                if (RadioButtonPostopFurtherSurgYes.Checked) abstractRecord.PostopFurtherSurg = true;
                if (RadioButtonPostopFurtherSurgNo.Checked) abstractRecord.PostopFurtherSurg = false;



                try
                {
                if (TextBoxExamRefraction.Text != null)
                abstractRecord.ExamRefraction =Convert.ToDouble(TextBoxExamRefraction.Text);
                }
                catch (Exception ex)
                {
                abstractRecord.ExamRefraction = null;
                TextBoxExamRefraction.Text = "";
                }
                try
                {
                if (TextBoxExamRefraction2.Text != null)
                abstractRecord.ExamRefraction2 = Convert.ToDouble(TextBoxExamRefraction2.Text);
                }
                catch (Exception ex)
                {
                abstractRecord.ExamRefraction2 = null;
                TextBoxExamRefraction2.Text = "";
                }
                try
                {
                if (TextBoxExamRefaction3.Text != null)
                abstractRecord.ExamRefaction3 = Convert.ToDouble(TextBoxExamRefaction3.Text);
                }
                catch (Exception ex)
                {
                abstractRecord.ExamRefaction3 = null;
                TextBoxExamRefaction3.Text = "";
                }
                try
                {
                if (TextBoxExamIOP.Text != null)
                abstractRecord.ExamIOP = Convert.ToDouble(TextBoxExamIOP.Text);
                }
                catch (Exception ex)
                {
                abstractRecord.ExamIOP = null;
                TextBoxExamIOP.Text = "";
                }
                try
                {
                if (TextBoxComorbidOtherText.Text != null)
                abstractRecord.ComorbidOtherText = TextBoxComorbidOtherText.Text;
                }
                catch (Exception ex)
                {
                abstractRecord.ComorbidOtherText = null;
                TextBoxComorbidOtherText.Text = "";
                }
                try
                {
                if (TextBoxPreopKerOtherText.Text != null)
                abstractRecord.PreopKerOtherText = TextBoxPreopKerOtherText.Text;
                }
                catch (Exception ex)
                {
                abstractRecord.PreopKerOtherText = null;
                TextBoxPreopKerOtherText.Text = "";
                }
                try
                {
                if (TextBoxPreopKerCalcOtherText.Text != null)
                abstractRecord.PreopKerCalcOtherText = TextBoxPreopKerCalcOtherText.Text;
                }
                catch (Exception ex)
                {
                abstractRecord.PreopKerCalcOtherText = null;
                TextBoxPreopKerCalcOtherText.Text = "";
                }
                try
                {
                if (TextBoxPreopKeFlatValue1.Text != null)
                abstractRecord.PreopKeFlatValue1 = Convert.ToDouble(TextBoxPreopKeFlatValue1.Text);
                }
                catch (Exception ex)
                {
                abstractRecord.PreopKeFlatValue1 = null;
                TextBoxPreopKeFlatValue1.Text = "";
                }
                try
                {
                if (TextBoxPreopKerFlatValue2.Text != null)
                abstractRecord.PreopKerFlatValue2 = Convert.ToDouble(TextBoxPreopKerFlatValue2.Text);
                }
                catch (Exception ex)
                {
                abstractRecord.PreopKerFlatValue2 = null;
                TextBoxPreopKerFlatValue2.Text = "";
                }
                try
                {
                if (TextBoxPreopKerFlatValue3.Text != null)
                abstractRecord.PreopKerFlatValue3 = Convert.ToDouble(TextBoxPreopKerFlatValue3.Text);
                }
                catch (Exception ex)
                {
                abstractRecord.PreopKerFlatValue3 = null;
                TextBoxPreopKerFlatValue3.Text = "";
                }
                try
                {
                if (TextBoxPreopKerSteepValue1.Text != null)
                abstractRecord.PreopKerSteepValue1 = Convert.ToDouble(TextBoxPreopKerSteepValue1.Text);
                }
                catch (Exception ex)
                {
                abstractRecord.PreopKerSteepValue1 = null;
                TextBoxPreopKerSteepValue1.Text = "";
                }
                try
                {
                if (TextBoxPreopKerSteepValue2.Text != null)
                abstractRecord.PreopKerSteepValue2 = Convert.ToDouble(TextBoxPreopKerSteepValue2.Text);
                }
                catch (Exception ex)
                {
                abstractRecord.PreopKerSteepValue2 = null;
                TextBoxPreopKerSteepValue2.Text = "";
                }
                try
                {
                if (TextBoxPreopKerSteepValue3.Text != null)
                abstractRecord.PreopKerSteepValue3 = Convert.ToDouble(TextBoxPreopKerSteepValue3.Text);
                }
                catch (Exception ex)
                {
                abstractRecord.PreopKerSteepValue3 = null;
                TextBoxPreopKerSteepValue3.Text = "";
                }
                try
                {
                if (TextBoxPreopIOLPolwerCalcOther.Text != null)
                abstractRecord.PreopIOLPolwerCalcOther = TextBoxPreopIOLPolwerCalcOther.Text;
                }
                catch (Exception ex)
                {
                abstractRecord.PreopIOLPolwerCalcOther = null;
                TextBoxPreopIOLPolwerCalcOther.Text = "";
                }
                try
                {
                if (TextBoxRefractiveTarget.Text != null)
                abstractRecord.RefractiveTarget =Convert.ToDouble( TextBoxRefractiveTarget.Text);
                }
                catch (Exception ex)
                {
                abstractRecord.RefractiveTarget = null;
                TextBoxRefractiveTarget.Text = "";
                }
                try
                {
                if (TextBoxPredictedResAstigmatism.Text != null)
                abstractRecord.PredictedResAstigmatism = Convert.ToDouble(TextBoxPredictedResAstigmatism.Text);
                }
                catch (Exception ex)
                {
                abstractRecord.PredictedResAstigmatism = null;
                TextBoxPredictedResAstigmatism.Text = "";
                }
                try
                {
                if (TextBoxPredictedResAstigmatismDegree.Text != null)
                abstractRecord.PredictedResAstigmatismDegree = Convert.ToDouble(TextBoxPredictedResAstigmatismDegree.Text);
                }
                catch (Exception ex)
                {
                abstractRecord.PredictedResAstigmatismDegree = null;
                TextBoxPredictedResAstigmatismDegree.Text = "";
                }
                try
                {
                if (TextBoxIOLBrandOtherText.Text != null)
                abstractRecord.IOLBrandOtherText = TextBoxIOLBrandOtherText.Text;
                }
                catch (Exception ex)
                {
                abstractRecord.IOLBrandOtherText = null;
                TextBoxIOLBrandOtherText.Text = "";
                }
                try
                {
                if (TextBoxIOLPowerSelectedSpherical.Text != null)
                abstractRecord.IOLPowerSelectedSpherical = Convert.ToDouble(TextBoxIOLPowerSelectedSpherical.Text);
                }
                catch (Exception ex)
                {
                abstractRecord.IOLPowerSelectedSpherical = null;
                TextBoxIOLPowerSelectedSpherical.Text = "";
                }
                try
                {
                if (TextBoxIOLPowerSelectedCylindrical.Text != null)
                abstractRecord.IOLPowerSelectedCylindrical = Convert.ToDouble(TextBoxIOLPowerSelectedCylindrical.Text);
                }
                catch (Exception ex)
                {
                abstractRecord.IOLPowerSelectedCylindrical = null;
                TextBoxIOLPowerSelectedCylindrical.Text = "";
                }
                try
                {
                if (TextBoxIOLPlacementPlannedAxis.Text != null)
                abstractRecord.IOLPlacementPlannedAxis = Convert.ToDouble(TextBoxIOLPlacementPlannedAxis.Text);
                }
                catch (Exception ex)
                {
                abstractRecord.IOLPlacementPlannedAxis = null;
                TextBoxIOLPlacementPlannedAxis.Text = "";
                }
                try
                {
                if (TextBoxSurgComplicationOtherText.Text != null)
                abstractRecord.SurgComplicationOtherText = TextBoxSurgComplicationOtherText.Text;
                }
                catch (Exception ex)
                {
                abstractRecord.SurgComplicationOtherText = null;
                TextBoxSurgComplicationOtherText.Text = "";
                }
                try
                {
                if (TextBoxSurgAdjunctiveProcOtherText.Text != null)
                abstractRecord.SurgAdjunctiveProcOtherText = TextBoxSurgAdjunctiveProcOtherText.Text;
                }
                catch (Exception ex)
                {
                abstractRecord.SurgAdjunctiveProcOtherText = null;
                TextBoxSurgAdjunctiveProcOtherText.Text = "";
                }
                try
                {
                if (TextBoxPostopRefraction1.Text != null)
                abstractRecord.PostopRefraction1 = Convert.ToDouble(TextBoxPostopRefraction1.Text);
                }
                catch (Exception ex)
                {
                abstractRecord.PostopRefraction1 = null;
                TextBoxPostopRefraction1.Text = "";
                }
                try
                {
                if (TextBoxPostopRefraction2.Text != null)
                abstractRecord.PostopRefraction2 = Convert.ToDouble(TextBoxPostopRefraction2.Text);
                }
                catch (Exception ex)
                {
                abstractRecord.PostopRefraction2 = null;
                TextBoxPostopRefraction2.Text = "";
                }
                try
                {
                if (TextBoxPostopRefraction3.Text != null)
                abstractRecord.PostopRefraction3 = Convert.ToDouble(TextBoxPostopRefraction3.Text);
                }
                catch (Exception ex)
                {
                abstractRecord.PostopRefraction3 = null;
                TextBoxPostopRefraction3.Text = "";
                }
                try
                {
                if (TextBoxPostopAstigmatism.Text != null)
                abstractRecord.PostopAstigmatism = Convert.ToDouble(TextBoxPostopAstigmatism.Text);
                }
                catch (Exception ex)
                {
                abstractRecord.PostopAstigmatism = null;
                TextBoxPostopAstigmatism.Text = "";
                }
                try
                {
                if (TextBoxPostopAstigmatismDegree.Text != null)
                abstractRecord.PostopAstigmatismDegree = Convert.ToDouble(TextBoxPostopAstigmatismDegree.Text);
                }
                catch (Exception ex)
                {
                abstractRecord.PostopAstigmatismDegree = null;
                TextBoxPostopAstigmatismDegree.Text = "";
                }
                try
                {
                if (TextBoxRBPostopBCVAReasonOtherText.Text != null)
                abstractRecord.RBPostopBCVAReasonOtherText = TextBoxRBPostopBCVAReasonOtherText.Text;
                }
                catch (Exception ex)
                {
                abstractRecord.RBPostopBCVAReasonOtherText = null;
                TextBoxRBPostopBCVAReasonOtherText.Text = "";
                }
                try
                {
                if (TextBoxPostopFurtherSurgReason.Text != null)
                abstractRecord.PostopFurtherSurgReason = TextBoxPostopFurtherSurgReason.Text;
                }
                catch (Exception ex)
                {
                abstractRecord.PostopFurtherSurgReason = null;
                TextBoxPostopFurtherSurgReason.Text = "";
                }
                abstractRecord.LastUpdateDate = DateTime.Now;
                bool ChartCompleted = isComplete();
                abstractRecord.Complete = ChartCompleted;
                var chartRegistration = (from cr in pim.ChartRegistration
                                         where cr.ParticipantModuleCycleID == cycle.ParticipantModuleCycleID
                                          & cr.ModuleID == ModuleID
                                          & cr.RecordIdentifier == recordIdentifier
                                         select cr).First<NetHealthPIMModel.ChartRegistration>();
                chartRegistration.AbstractCompleted = ChartCompleted;


                if ((DropDownListYearOfBirth.SelectedIndex > 0) && (DropDownListYearOfBirth.SelectedIndex > 0))
                {
                    chartRegistration.DOB = abstractRecord.MonthOfBirth + "/" + abstractRecord.YearOfBirth;
                }

                pim.SaveChanges();

                transaction.Complete();




            }

            CycleManager.UpdateParticipantCompletedModules(cycleID, ModuleID);
        }
    }


    protected void ButtonSubmit_Click(object sender, EventArgs e)
    {
        savedata();
        isComplete();
        if (!isComplete())
        {
            isComplete();
            string script = " var answer = confirm('Chart data is not complete.\\nClick OK to save entries and exit chart.\\nClick CANCEL to save entries and review the chart. '); if (answer==true) window.location = '../PatientChartRegistration.aspx';";
            ScriptManager.RegisterStartupScript(this, GetType(),
                          "ServerControlScript", script, true);
        }
        else
        {

            Response.Redirect("../PatientChartRegistration.aspx");
        }
    }

    protected bool isComplete()
    {

        bool ChartCompleted = true;
        LabelRBGender.Visible = false;

        LabelRBSymptomsDailyLiving.Visible = false;

        LabelRBCorrectiveLens.Visible = false;

        LabelRBPastHistoryTrauma.Visible = false;

        LabelRBPastHistoryRefractiveSurgery.Visible = false;

        LabelRBPastHistoryOtherEyeSurgery.Visible = false;

        LabelRBPastHistoryAmblyopia.Visible = false;

        LabelRBPastHistoryAlpha1aAgonists.Visible = false;

        LabelRBPastHistoryAnticoagulation.Visible = false;

        LabelRBExamDilatedPupil.Visible = false;

        LabelRBExamCornealEndothelium.Visible = false;

        LabelRBExamCataractGrading.Visible = false;

        LabelRBExamMacula.Visible = false;

        LabelRBPreopRisks.Visible = false;

        LabelRBPreopRefractiveGoals.Visible = false;

        LabelRBPreopSignedConsent.Visible = false;

        LabelRBPreopAxialLengthMeasure.Visible = false;

        LabelRBPreopIOLPowerCalc.Visible = false;

        LabelRBPreopToricIOLCalc.Visible = false;

        LabelRBIOLBrand.Visible = false;

        LabelRBSurgicalIntervention.Visible = false;

        LabelRBIOLPlacement.Visible = false;

        LabelRBPreopEyeMarkedPosition.Visible = false;

        LabelRBPostopInstructions.Visible = false;
        LabelRBPostopIOLWithin15Degrees.Visible = false;

        LabelRBPostopBCVAReason.Visible = false;

        LabelRBYagNeeded.Visible = false;




        LabelMonthOfBirth.Visible = false;

        LabelYearOfBirth.Visible = false;

        LabelMonthOfSurgery.Visible = false;

        LabelYearOfSurgery.Visible = false;

        LabelBCVA.Visible = false;

        LabelRefractiveTarget.Visible = false;

        LabelPredictedResAstigmatism.Visible = false;

        LabelPredictedResAstigmatismDegree.Visible = false;

        LabelIOLPowerSelectedSpherical.Visible = false;

        LabelIOLPowerSelectedCylindrical.Visible = false;

        LabelIOLPlacementPlannedAxis.Visible = false;

        LabelPostopAstigmatism.Visible = false;

        LabelPostopAstigmatismDegree.Visible = false;

        LabelPostopBCVA.Visible = false;

        LabelRBPostopBCVAReasonOtherText.Visible = false;

        LabelExamPseudoMaterial.Visible = false; ;

        LabelExamDilatedFundus.Visible = false;
        LabelExamSlitLamp.Visible = false;

        LabelPreopToricIOLMedRecord.Visible = false;

        

        LabelSurgIntraopComplications.Visible = false;

        LabelSurgAdjunctiveProc.Visible = false;

        LabelPostopAntibioticDrops.Visible = false;

        LabelPostopAntiInflamDrops.Visible = false;
        Labelinitialage.Visible = false;
        LabelPostopFurtherSurg.Visible = false;
        NetHealthPIMEntities pim = new NetHealthPIMEntities();
        var moduleinfo = (from c in pim.Module where c.ModuleID == 52 select c).FirstOrDefault();
        string message = "Patient must be  at least 18 years old<br />";
        string registrationminage = Validation.validateRegistrationMinAge(Convert.ToInt32(DropDownListYearOfSurgery.SelectedValue), Convert.ToInt32(DropDownListYearOfBirth.SelectedValue), Convert.ToInt32(moduleinfo.RegistrationMinAge), message);

        if ((Convert.ToInt32(DropDownListYearOfSurgery.SelectedValue) - Convert.ToInt32(DropDownListYearOfBirth.SelectedValue) < 18))
        {
            Labelinitialage.Text = message;
            Labelinitialage.Visible = true;
            ChartCompleted = false;
        }

        if (RadioButtonListRBGender.SelectedIndex == -1)
        {
            LabelRBGender.Visible = true;
            ChartCompleted = false;
        }
        if (RadioButtonListRBSymptomsDailyLiving.SelectedIndex == -1)
        {
            LabelRBSymptomsDailyLiving.Visible = true;
            ChartCompleted = false;
        }
        if (RadioButtonListRBCorrectiveLens.SelectedIndex == -1)
        {
            LabelRBCorrectiveLens.Visible = true;
            ChartCompleted = false;
        }
        if (RadioButtonListRBPastHistoryTrauma.SelectedIndex == -1)
        {
            LabelRBPastHistoryTrauma.Visible = true;
            ChartCompleted = false;
        }
        if (RadioButtonListRBPastHistoryRefractiveSurgery.SelectedIndex == -1)
        {
            LabelRBPastHistoryRefractiveSurgery.Visible = true;
            ChartCompleted = false;
        }
        if (RadioButtonListRBPastHistoryOtherEyeSurgery.SelectedIndex == -1)
        {
            LabelRBPastHistoryOtherEyeSurgery.Visible = true;
            ChartCompleted = false;
        }
        if (RadioButtonListRBPastHistoryAmblyopia.SelectedIndex == -1)
        {
            LabelRBPastHistoryAmblyopia.Visible = true;
            ChartCompleted = false;
        }
        if (RadioButtonListRBPastHistoryAlpha1aAgonists.SelectedIndex == -1)
        {
            LabelRBPastHistoryAlpha1aAgonists.Visible = true;
            ChartCompleted = false;
        }
        if (RadioButtonListRBPastHistoryAnticoagulation.SelectedIndex == -1)
        {
            LabelRBPastHistoryAnticoagulation.Visible = true;
            ChartCompleted = false;
        }
        if (RadioButtonListRBExamDilatedPupil.SelectedIndex == -1)
        {
            LabelRBExamDilatedPupil.Visible = true;
            ChartCompleted = false;
        }
        if (RadioButtonListRBExamCornealEndothelium.SelectedIndex == -1)
        {
            LabelRBExamCornealEndothelium.Visible = true;
            ChartCompleted = false;
        }
        if (RadioButtonListRBExamCataractGrading.SelectedIndex == -1)
        {
            LabelRBExamCataractGrading.Visible = true;
            ChartCompleted = false;
        }
        if (RadioButtonListRBExamMacula.SelectedIndex == -1)
        {
            LabelRBExamMacula.Visible = true;
            ChartCompleted = false;
        }
        if (RadioButtonListRBPreopRisks.SelectedIndex == -1)
        {
            LabelRBPreopRisks.Visible = true;
            ChartCompleted = false;
        }
        if (RadioButtonListRBPreopRefractiveGoals.SelectedIndex == -1)
        {
            LabelRBPreopRefractiveGoals.Visible = true;
            ChartCompleted = false;
        }
        if (RadioButtonListRBPreopSignedConsent.SelectedIndex == -1)
        {
            LabelRBPreopSignedConsent.Visible = true;
            ChartCompleted = false;
        }
        if (RadioButtonListRBPreopAxialLengthMeasure.SelectedIndex == -1)
        {
            LabelRBPreopAxialLengthMeasure.Visible = true;
            ChartCompleted = false;
        }
        if (RadioButtonListRBPreopIOLPowerCalc.SelectedIndex == -1)
        {
            LabelRBPreopIOLPowerCalc.Visible = true;
            ChartCompleted = false;
        }
        if (RadioButtonListRBPreopToricIOLCalc.SelectedIndex == -1)
        {
            LabelRBPreopToricIOLCalc.Visible = true;
            ChartCompleted = false;
        }
        if (RadioButtonListRBIOLBrand.SelectedIndex == -1)
        {
            LabelRBIOLBrand.Visible = true;
            ChartCompleted = false;
        }
        if (RadioButtonListRBSurgicalIntervention.SelectedIndex == -1)
        {
            LabelRBSurgicalIntervention.Visible = true;
            ChartCompleted = false;
        }
        if (RadioButtonListRBIOLPlacement.SelectedIndex == -1)
        {
            LabelRBIOLPlacement.Visible = true;
            ChartCompleted = false;
        }
        if (RadioButtonListRBPreopEyeMarkedPosition.SelectedIndex == -1)
        {
            LabelRBPreopEyeMarkedPosition.Visible = true;
            ChartCompleted = false;
        }
        if (RadioButtonListRBPostopInstructions.SelectedIndex == -1)
        {
            LabelRBPostopInstructions.Visible = true;
            ChartCompleted = false;
        }
        if (RadioButtonListRBPostopIOLWithin15Degrees.SelectedIndex == -1)
        {
            LabelRBPostopIOLWithin15Degrees.Visible = true;
            ChartCompleted = false;
        }
        
        if (RadioButtonListRBYagNeeded.SelectedIndex == -1)
        {
            LabelRBYagNeeded.Visible = true;
            ChartCompleted = false;
        }



        if (DropDownListMonthOfBirth.SelectedIndex == 0)
        {
            LabelMonthOfBirth.Visible = true;
            ChartCompleted = false;
        }
        if (DropDownListYearOfBirth.SelectedIndex == 0)
        {
            LabelYearOfBirth.Visible = true;
            ChartCompleted = false;
        }
        if (DropDownListMonthOfSurgery.SelectedIndex == 0)
        {
            LabelMonthOfSurgery.Visible = true;
            ChartCompleted = false;
        }
        if (DropDownListYearOfSurgery.SelectedIndex == 0)
        {
            LabelYearOfSurgery.Visible = true;
            ChartCompleted = false;
        }
        if (DropDownListBCVA.SelectedIndex == 0)
        {
            LabelBCVA.Visible = true;
            ChartCompleted = false;
        }
        if (string.IsNullOrEmpty(TextBoxRefractiveTarget.Text))
        {
            LabelRefractiveTarget.Visible = true;
            ChartCompleted = false;
        }
       
        if (string.IsNullOrEmpty(TextBoxIOLPowerSelectedSpherical.Text))
        {
            LabelIOLPowerSelectedSpherical.Visible = true;
            ChartCompleted = false;
        }
        if (string.IsNullOrEmpty(TextBoxIOLPowerSelectedCylindrical.Text))
        {
            LabelIOLPowerSelectedCylindrical.Visible = true;
            ChartCompleted = false;
        }
        if (string.IsNullOrEmpty(TextBoxIOLPlacementPlannedAxis.Text))
        {
            LabelIOLPlacementPlannedAxis.Visible = true;
            ChartCompleted = false;
        }
      
        if (DropDownListPostopBCVA.SelectedIndex == 0)
        {
            LabelPostopBCVA.Visible = true;
            ChartCompleted = false;
        }
       



        if (RadioButtonExamSlitLampNo.Checked == false && RadioButtonExamSlitLampYes.Checked == false)
        {
            LabelExamSlitLamp.Visible = true;
            ChartCompleted = false;
        }
        if (RadioButtonPreopToricIOLMedRecordNo.Checked == false && RadioButtonPreopToricIOLMedRecordYes.Checked == false)
        {
            LabelPreopToricIOLMedRecord.Visible = true;
            ChartCompleted = false;
        }
      
        if (RadioButtonSurgIntraopComplicationsNo.Checked == false && RadioButtonSurgIntraopComplicationsYes.Checked == false)
        {
            LabelSurgIntraopComplications.Visible = true;
            ChartCompleted = false;
        }
        if (RadioButtonSurgAdjunctiveProcNo.Checked == false && RadioButtonSurgAdjunctiveProcYes.Checked == false)
        {
            LabelSurgAdjunctiveProc.Visible = true;
            ChartCompleted = false;
        }
        if (RadioButtonPostopAntibioticDropsNo.Checked == false && RadioButtonPostopAntibioticDropsYes.Checked == false)
        {
            LabelPostopAntibioticDrops.Visible = true;
            ChartCompleted = false;
        }
        if (RadioButtonPostopAntiInflamDropsNo.Checked == false && RadioButtonPostopAntiInflamDropsYes.Checked == false)
        {
            LabelPostopAntiInflamDrops.Visible = true;
            ChartCompleted = false;
        }
        if (RadioButtonPostopFurtherSurgNo.Checked == false && RadioButtonPostopFurtherSurgYes.Checked == false)
        {
            LabelPostopFurtherSurg.Visible = true;
            ChartCompleted = false;
        }
        if (RadioButtonExamPseudoMaterialYes.Checked == false && RadioButtonExamPseudoMaterialNo.Checked == false)
        {
            LabelExamPseudoMaterial.Visible = true;
            ChartCompleted = false;
        }
        if (RadioButtonExamDilatedFundusYes.Checked == false && RadioButtonExamDilatedFundusNo.Checked == false)
        {

            LabelExamDilatedFundus.Visible = true;
            ChartCompleted = false;
        
        
        }

        return ChartCompleted;
    }



}

