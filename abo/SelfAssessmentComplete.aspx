﻿<%@ Page Title="" Language="C#" MasterPageFile="~/PIMMasterPage.master" AutoEventWireup="true" CodeFile="SelfAssessmentComplete.aspx.cs" Inherits="copd_SelfAssessmentComplete" EnableTheming="true" %>

<asp:Content ID="Content1" ContentPlaceHolderID="head" Runat="Server">
</asp:Content>
<asp:Content ID="Content3" ContentPlaceHolderID="ContentPlaceHolder1" Runat="Server">

<center>
<br />
You have completed the self-assessment survey.<br />
<br />
Please begin your Practice Chart Audit now.
<br />
<br />
    <asp:Button ID="ButtonEnterChartData" runat="server" Text="Enter Chart Data" PostBackUrl="~/copd/AbstractList.aspx" />
<br />
<br />
</center>
</asp:Content>

