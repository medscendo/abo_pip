﻿<%@ Page Title="" Language="C#" MasterPageFile="~/PIMMasterPage.master" AutoEventWireup="true" CodeFile="Congratulations.aspx.cs" Inherits="abo_Congratulations" %>

<asp:Content ID="Content1" ContentPlaceHolderID="head" Runat="Server">
    <style>
       
        
        h3 {
            margin: 40px 0 10px !important;
            width: 100%;
            border-bottom: 1px solid #ccc;
            font-weight:bold;
            font-size:1.5em;
        }
	</style>
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder1" Runat="Server">

    <div runat="server" id="div_NewParticipant">
        <p>You have completed your Part IV Practice Assessment. This will be reflected on your MOC Status page shortly.</p>
        <asp:Panel runat="server" ID="CMEPanel">
		    <h3>CME Credit</h3>
            <p>You are also eligible to claim <i>AMA PRA Category 1 Credit</i>&trade;  through the American Academy of Ophthalmology.</p>
            <p id="FirstClaim" runat="server">
                
                Click <asp:LinkButton ID="LinkButton1" runat="server" OnClick="ButtonFirstClaim_Click" Text="here" OnClientClick="aspnetForm.target ='_blank';"></asp:LinkButton>
                to login to CME Central to claim your CME credits.
			    
		    </p>
            <span id="FirstClaimadditional" runat="server"></span>
            <p id="FirstClaimCompleted" runat="server">
                You have claimed CME for this activity once in 
			    <%=System.DateTime.Today.Year.ToString() %> on <asp:Label runat="server" ID ="lblFirstClaimDate"></asp:Label>.
            </p>
            <p id="SecondClaim" runat="server">
                This is second time you are claiming CME credit for this activity in 
			    <%=System.DateTime.Today.Year.ToString() %>, 
                Click <asp:LinkButton ID="LinkButton2" runat="server" OnClick="ButtonSecondClaim_Click" Text="here" OnClientClick="aspnetForm.target ='_blank';"></asp:LinkButton> 
                to login to CME Central to claim your CME credits.
            </p>
            <p id="CreditsClaimed" runat="server">
                You have claimed CME for this activity twice in <%=System.DateTime.Today.Year.ToString() %>.  This was claimed on  
			    <asp:Label runat="server" ID ="lblFirstClaimDate2"></asp:Label> and <asp:Label runat="server" ID ="lblSecondClaimDate"></asp:Label>.
            </p>
            <p>
                Complete information about claiming CME for completion of the Part IV Practice Assessment is available at 
			    <a href="http://abop.org/maintain-certification/practice-improvement-activities/practice-improvement-modules-(pims)/claiming-cme/" target="_blank">http://abop.org/maintain-certification/practice-improvement-activities/practice-improvement-modules-(pims)/claiming-cme/</a>.
		    </p>
		    <p>If you are not an AAO member or do not have an Academy website login, you will be prompted to create a new account.</p>
		    <p>
                If you believe you have an Academy membership and are unable to log in, please do not create a new account. This will not connect to your existing member record, and will not give you access to your password-protected CME Central account. 
			    For additional information and assistance, visit the <a href="https://www.aao.org/help/login" target="_blank">Academy's Help section</a>.
			</p>
                	
		</asp:Panel>
		
		<h3>Comments and Feedback</h3>
        <p>Please feel free to visit the <a href= "ContactUs.aspx">Contact Us</a> page with any comments on the process that you would like to share.</p>
    </div>
    
    <div runat="server" id="div_OldParticipant">
              
        <p>You have completed Part IV MOC Improvement in Medical Practice and are now eligible for MOC credit.  Your completion will be reflected in your ABO profile.</p>
		<h3>Comments and Feedback</h3>
        <p>Please feel free to visit the <a href= "ContactUs.aspx">Contact Us</a> page with any comments on the process that you would like to share.</p>
    </div>
    <div class="button-box"><asp:LinkButton ID="ButtonSubmit" runat="server" Text="Back" PostBackUrl="Dashboard3.aspx" CssClass="button" /></div>
        
</asp:Content>

