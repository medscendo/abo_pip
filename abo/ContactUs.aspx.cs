﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using NetHealthPIMModel;
using System.Data.EntityClient;
using System.Data;

public partial class abo_ContactUs : BasePage
{
    protected void Page_Load(object sender, EventArgs e)
    {
        if (!IsPostBack)
        {
            ((PIMMasterPage)Page.Master).SetStatus(1);
            ((PIMMasterPage)Page.Master).SetHeader("Contact Us");
            ButtonSubmit.PostBackUrl = Request.UrlReferrer.ToString();
        }
    }
    protected void ButtonSubmit_Click(object sender, EventArgs e)
    {
        int ContactPerson = Convert.ToInt32(rdoYes.Checked);
        string ContactBy = "";
        if (rdoEmail.Checked)
        {
            ContactBy = "Email";
        }
        if (rdoPhone.Checked)
        {
            ContactBy = "Phone";
        }
        String ParticipantInfo = "";
        String ParticipantEmailAddress = "";
        String ParticipantName = "";
        UserContext ctxLocal = null;
        ctxLocal = (UserContext)Session[Constants.SESSION_USERCONTEXT];
        if (ctxLocal != null)
        {
            using (NetHealthPIMEntities pim = new NetHealthPIMEntities())
            {
                Participant part = pim.Participant.First(c => c.ParticipantID == ctxLocal.ParticipantID);
                ParticipantInfo = "<br /><b>Name:</b> " + part.ParticipantLastName + ", " + part.ParticipantFirstName;
		        ParticipantInfo = ParticipantInfo + "<br /><b>Candidate ID: </b>" + part.CandidateID.ToString();
		        ParticipantInfo = ParticipantInfo + "<br /><b>Preferred Email Address: </b> " + EmailAddress.Text.ToString();
	            ParticipantInfo = ParticipantInfo + "<br /><b>Email Address on Account: </b> " + part.ParticipantEmailAddress + " ";
	        	ParticipantInfo = ParticipantInfo + "<br /><b>Phone Number: </b> " + PhoneNumber.Text.ToString();
	        	ParticipantInfo = ParticipantInfo + "<br /><b>The best way to reach this person is by: </b> " + ContactBy;
                ParticipantEmailAddress = part.ParticipantEmailAddress;
                ParticipantName = part.ParticipantFirstName + ' ' + part.ParticipantLastName;
            }
        }
        
        string EmailTo = System.Configuration.ConfigurationManager.AppSettings["EmailTo"]; 
        string EmailSubject = System.Configuration.ConfigurationManager.AppSettings["EmailSubject"];
        string EmailBody = "<font face='Calibri'  size='2'><b>A Part 4 Support Request has been made.</b>";
        if (ParticipantInfo != "")
        {
            EmailBody = EmailBody + "<br /><br />" + "<b>Contact Information:</b> " + ParticipantInfo;
	        EmailBody = EmailBody + "<br /><br /><b>Feedback Message: </b> " + Comments.Text.ToString() + "<br /><br />";
        }

        if (EmailAddress.Text.ToString().Trim() != "")
        {
            ParticipantEmailAddress = EmailAddress.Text.ToString().Trim();
        }

        SMTPEmail mail = new SMTPEmail();
        mail.SendEmail(EmailTo, EmailSubject, EmailBody, EmailAddress.Text.ToString(), ParticipantName);
        lblMessage.Visible = true;
        Response.Redirect("FeedbackReceived.aspx");
    }

}
