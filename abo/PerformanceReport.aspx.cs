﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Web.UI.DataVisualization.Charting;
using NetHealthPIMModel;
using PIM.Repositories;
using System.Web.UI.HtmlControls;

public partial class copd_PerformanceReport : BasePage
{
    static int MeasureGroupID = 0;
    static int PreviousMeasureGroupID = -1;
    static string ModuleNameProcessMeasures = "";
    static string ModuleNameOutcomeMeasures = "";
    static string ModuleNameSystemSurvey = "";
    
    protected void Page_Load(object sender, EventArgs e)
    {


        string breadCrumb = Constants.BC_DASHBOARD_LINK + "" + Constants.BC_PERFORMANCE_REPORT;
        ((PIMMasterPage)Page.Master).SetTitle(breadCrumb);
        ((PIMMasterPage)Page.Master).SetStatus(2);
        ((PIMMasterPage)Page.Master).SetHeader("Practice Report");
        string action = Request.Params[Constants.QUERYSTRING_ACTION];
        string cycleNumber = Request.QueryString["cycle"];

        if (cycleNumber == "2")
        {
            ((PIMMasterPage)Page.Master).SetHeader("Final Practice Report");
            ((PIMMasterPage)Page.Master).SetStatus(3); 
        }

        if (!IsPostBack)
        {
            ButtonSelectMeasuresPhase1.PostBackUrl = "~/" + module.ModuleCode.ToLower() + "/SelectMeasures.aspx";
            LinkButtonMainMenuPhase1.PostBackUrl = "~/" + module.ModuleCode.ToLower() + "/MyPI.aspx";
            if (action != Constants.QUERYSTRING_ACTION_VIEW)
            {
                if (ctx.ParticipantGroupID != 0)
                    CycleManager.ComputeCycleMeasures_ASN_Participants(ctx.ActiveModuleCycleID, ctx.ParticipantGroupID);
                else
                {
                    //CycleManager.ComputeCycleMeasures_ABO(ctx.ActiveModuleCycleID);
                }

            }
        }
        if (action == Constants.QUERYSTRING_ACTION_VIEW)
        {
            PanelSelectMeasuresPhase.Visible = false;
            LinkButtonImpactStatement.Visible = false;
        }

        int cycleID = ctx.ActiveModuleCycleID;
        if (Request[Constants.QUERYSTRING_CYCLEID] != null)
        {
            cycleID = Int32.Parse(Request[Constants.QUERYSTRING_CYCLEID]);
        }

        using (NetHealthPIMEntities pim = new NetHealthPIMEntities())
        {
            ParticipantModuleCycle cycle = pim.ParticipantModuleCycle.First(c => c.ParticipantModuleCycleID == cycleID);
            ParticipantModuleCycle cycle2;

            var participantDashboardModules = (from pdm in pim.ParticipantDashboardModules_V
                      where pdm.ParticipantModuleCycleID == cycle.ParticipantModuleCycleID
                      && pdm.ChartsCompleted != pdm.TotalCharts
                                               select pdm).Count();

            bool isModuleCompleted = true;
            isModuleCompleted = (System.Configuration.ConfigurationManager.AppSettings["Environment"] == "sandbox");
            if (isModuleCompleted)
            {
            }
            else 
            {
                if ((participantDashboardModules > 0) && (cycle.CycleNumber == 1))
                {
                    Response.Redirect("Dashboard.aspx");
                }
                if ((participantDashboardModules > 0) && (cycle.CycleNumber == 2) && (cycleNumber != "1"))
                {
                    Response.Redirect("Dashboard3.aspx");
                }
            }


            if ((cycle.CycleNumber == 2) && (cycleNumber=="1"))
            {
                cycle2 = cycle;

                cycle.PrevParticipantModuleCycleReference.Load();
                ParticipantModuleCycle prev = cycle.PrevParticipantModuleCycle;
                cycle = prev;

                cycleID = cycle.ParticipantModuleCycleID;

            }

            // current cycle equals 2 and not trying to access previous cycle, then change the URL to return to dashboard 3
            if ((cycle.CycleNumber == 2) && (cycleNumber != "1"))
            {
                LinkButtonDashboard.PostBackUrl = "Dashboard3.aspx";
                //check if PEC measure exists?
                var pmPECMissing = (from pm in pim.ParticipantMeasure
                                          join m in pim.Measure on pm.MeasureID equals m.MeasureID
                                          where pm.ParticipantModuleCycleID == ctx.ActiveModuleCycleID
                                          && m.MeasureType.MeasureTypeID == 5
                                          select pm).Count();
                if (pmPECMissing==0)
                    CycleManager.AddPECMeasuresToParticipant(ctx.ActiveModuleCycleID); // will only add if current cycle uses PEC survey
            }

            var modules = (from m in pim.ParticipantModuleSelection where m.ParticipantModuleCycleID == cycleID select m).ToList();

            var oldModules = modules.Where(m => m.ModuleID < 100);
            var newModules = modules.Where(m => m.ModuleID >= 100);

            var ParticipantInfo = (from c in pim.Participant where c.ParticipantID == ctx.ParticipantID select c).FirstOrDefault();
            //NOTE 1: ALL Modules less than 100 are the "old" modules and need to show the old performance page.
            //This second validation was needed, as a user can select both old and new modules.
            //NOTE 2: There is a POSSIBILITY that a user can select BOTH a new AND old module in their final phase of Analysis and Impact.
            //IF this happens, the current code does NOT have a way to display the summary for both types of modules combined in one page, and new code will need to be implemented.
            if (ParticipantInfo.SoftwareVersion < 2016 || oldModules.Any())
            {
                foreach (var _module in oldModules)
                {
                    Module currentModule = pim.Module.First(p => p.ModuleID == _module.ModuleID);
                    CycleManager.ComputeCycleMeasures_ABO_EXT(cycleID, currentModule.ComputeCycleMeasuresProcedure);
                }
            }
            if (newModules.Any())
            {
                MeasureResourceRepository mr = new MeasureResourceRepository();
                foreach (var _module in newModules)
                {
                    mr.GetParticipantMeasure(cycleID, cycleID, _module.ModuleID);
                }
            }
            bool P2Flag = cycle.Phase2Flag ?? false;
            int activeCycle = ctx.ActiveModuleCycleID;
            ParticipantModuleCycle currentCycle = pim.ParticipantModuleCycle.First(p => p.ParticipantModuleCycleID == activeCycle);
            bool isPhase2 = currentCycle.Phase2Flag ?? false;
            if (Request[Constants.QUERYSTRING_PHASE] != null)
            {
                if (Request[Constants.QUERYSTRING_PHASE] == "1")
                {
                    P2Flag = false;
                }
                else if (Request[Constants.QUERYSTRING_PHASE] == "2")
                {
                    P2Flag = true;
                }
            }

            int measureAggregateTypeID;

                measureAggregateTypeID = Constants.MEASURE_AGGREGATE_TYPE_PEER;
                var report = from pir in pim.ParticipantInitialReport_V
                             join m in pim.Module on pir.ModuleID equals m.ModuleID
                             where pir.ParticipantModuleCycleID == cycleID
                                && pir.MeasureTypeID == 3
                                   && pir.MeasureRelated == null
                                && pir.MeasureAggregateTypeID == measureAggregateTypeID
                                && pir.ParticipantMeasureTypeID == 1
                             orderby m.ModuleID, pir.MeasureGroupSortOrder
                             select new PerformanceReportData
                             {
                                 ModuleName = m.ModuleName,
                                 MeasureID = pir.MeasureID,
                                 IsnumericMeasure=pir.IsNumericMeasure,
                                 MeasureGroupSortOrder = pir.MeasureGroupSortOrder == null ? 0 : (int)pir.MeasureGroupSortOrder,
                                 MeasureQualityIndicator = pir.MeasureQualityIndicator,
                                 MeasureTitle = pir.MeasureTitle,
                                 MeasureClinicalRecommendation = pir.MeasureClinicalRecommendation,
                                 MeasureLongDescription = pir.MeasureLongDescription,
                                 MeasureComputationMethod = pir.MeasureComputationMethod,
                                 AbstractRecordsIncluded = pir.AbstractRecordsIncluded == null ? 0 : (int)pir.AbstractRecordsIncluded,
                                 MeasurePercentCurrent = pir.MeasurePercentCurrent,
                                 PeerAbstractRecordsIncluded = pir.PeerAbstractRecordsIncluded,
                                 Peer25Percentile = pir.Peer25Percentile,
                                 Peer75Percentile = pir.Peer75Percentile,
                                 VisualAcuityResponse=pir.VisualAcuityResponse,
                                 PeerData = pir.PeerData,
                                 SelfData = pir.NumericResponse,
                                 NumericFullGraph=pir.NumericFullGraph,
                                 NumericMeasureName=pir.NumericMeasureName,
                                 IsPhase2Flag = P2Flag,
                                 IncludeInImprovementPlan = pir.IncludeInImprovementPlan,
                                 MayNeedImprovement = pir.MayNeedImprovement
                             };
                ListViewProcessMeasures.DataSource = report;

                var reportOutcomeMeasures = from pir in pim.ParticipantInitialReport_OutcomeMeasures_V
                                            join m in pim.Module on pir.ModuleID equals m.ModuleID
                                            where pir.ParticipantModuleCycleID == cycleID
                                            && pir.MeasureRelated==null
                                            && (pir.ParticipantMeasureTypeID == 3 || pir.ParticipantMeasureTypeID == 1 || pir.ParticipantMeasureTypeID == 5)
                                            orderby m.ModuleID, pir.MeasureGroupSortOrder
                             select new PerformanceReportData
                             {
                                 ModuleName = m.ModuleName,
                                 MeasureID = pir.MeasureID,
                                 IsnumericMeasure=pir.IsNumericMeasure,
                                 MeasureQualityIndicator = pir.MeasureQualityIndicator,
                                 MeasureTitle = pir.MeasureTitle,
                                 MeasureClinicalRecommendation = pir.MeasureClinicalRecommendation,
                                 MeasureLongDescription = pir.MeasureLongDescription,
                                 MeasureComputationMethod = pir.MeasureComputationMethod,
                                 AbstractRecordsIncluded = pir.AbstractRecordsIncluded == null ? 0 : (int)pir.AbstractRecordsIncluded,
                                 MeasurePercent3To6 = pir.MeasurePercent3To6,
                                 PeerData3To6 = pir.PeerData3To6,
                                 MeasurePercent7To12 = pir.MeasurePercent7To12,
                                 PeerData7To12 = pir.PeerData7To12,
                                 VisualAcuityResponse=pir.VisualAcuityResponse,
                                 MeasurePercentNewOnset = pir.MeasurePercentNewOnset,
                                 PeerDataNewOnset = pir.PeerDataNewOnset,
                                 MeasurePercentRecurrence = pir.MeasurePercentRecurrence,
                                 PeerDataRecurrence = pir.PeerDataRecurrence,
                                 SelfData = pir.NumericResponse,
                                 NumericFullGraph = pir.NumericFullGraph,
                                 NumericMeasureName = pir.NumericMeasureName,
                                 PeerAbstractRecordsIncluded=pir.PeerAbstractRecordsIncluded,
                                 Peer25Percentile=pir.Peer25Percentile,
                                 Peer75Percentile=pir.Peer75Percentile,
                                 IsPhase2Flag = P2Flag,
                                 IncludeInImprovementPlan = pir.IncludeInImprovementPlan,
                                 MayNeedImprovement = pir.MayNeedImprovement
                             };

                ListViewOutcomeMeasures.DataSource = reportOutcomeMeasures;

                var reportPEC = from pir in pim.ParticipantInitialReport_PECMeasures_V
                             where pir.ParticipantModuleCycleID == cycleID

                             orderby pir.MeasureGroupSortOrder
                             select new PerformanceReportData
                             {
                                 ModuleName = pir.ModuleName,
                                 MeasureID = pir.MeasureID,
                                 MeasureGroupSortOrder = pir.MeasureGroupSortOrder == null ? 0 : (int)pir.MeasureGroupSortOrder,
                                 MeasureQualityIndicator = pir.MeasureQualityIndicator,
                                 MeasureTitle = pir.MeasureTitle,
                                 MeasureClinicalRecommendation = pir.MeasureClinicalRecommendation,
                                 MeasureLongDescription = pir.MeasureLongDescription,
                                 MeasureComputationMethod = pir.MeasureComputationMethod,
                                 AbstractRecordsIncluded = pir.AbstractRecordsIncluded == null ? 0 : (int)pir.AbstractRecordsIncluded,
                                 MeasurePercentCurrent = pir.MeasurePercentCurrent,
                                 PeerData = pir.PeerData,
                                 SelfData = pir.SelfData,
                                 IsPhase2Flag = P2Flag,
                                 IncludeInImprovementPlan = pir.IncludeInImprovementPlan==1?true:false,
                                 MayNeedImprovement = pir.MayNeedImprovement
                             };
                ListViewPEC.DataSource = reportPEC;

                if (cycle.CycleNumber == 1)
                {
                    var totalChartsCompleted = (from chart in pim.ParticipantChartStatus_V
                                              where chart.ParticipantModuleCycleID == ctx.ActiveModuleCycleID
                                              select chart).Sum(n=>n.Completed);
                    if (totalChartsCompleted < Constants.TOTAL_NUMBER_OF_CHARTS)
                    {
                        LinkSelectMeasures.Visible = false;
                    }
                }

                if (cycle.MeasuresSelected)
                {
                    LinkSelectMeasures.Visible = false;
                }
                if ((cycle.CycleNumber == 2) && (action != Constants.QUERYSTRING_ACTION_VIEW))
                {

                    var chartsCountMissing = (from chart in pim.ParticipantChartStatus_V
                                         where chart.ParticipantModuleCycleID == ctx.ActiveModuleCycleID
                                         && chart.TotalCharts != chart.Completed
                                         select chart).Count();
                    if (chartsCountMissing==0)
                       LinkButtonImpactStatement.Visible = true;
                }
                if (isPhase2)
                {
                    LinkSelectMeasures.Visible = false;
                    currentCycle.PrevParticipantModuleCycleReference.Load();
                    ParticipantModuleCycle prev = currentCycle.PrevParticipantModuleCycle;

                   
                }

            var abstractCount = (from chart in pim.Abstract_COPD
                                 where chart.ParticipantModuleCycle.ParticipantModuleCycleID == cycleID
                                 select chart).Count();
            //Report date
            if (cycle.ReportApprovedCompletedDate != null)
            {
                //LabelReportCompletedDate.Text = cycle.ReportApprovedCompletedDate.Value.ToString("d");
                //PanelReportCompleteDate.Visible = true;
            }

           

            DataBind();
            measureAggregateTypeID = Constants.MEASURE_AGGREGATE_TYPE_POPULATION;

            if (cycle.PECCurrentID > 0)
            {

                Module modulePEC = pim.Module.First(p => p.ModuleCode == "PEC");

                CycleManager.ComputeCycleMeasures_ABO_EXT(cycleID, modulePEC.ComputeCycleMeasuresProcedure);
                tablePEC.Visible = true;

            }

            if ((isPhase2) && (cycleNumber!="1"))
            {
                PanelDevelopImpactStatement.Visible = (!cycle.ImpactAssessmentComplete);
                PanelSelectMeasuresPhase.Visible = false;
                breadCrumb = Constants.BC_DASHBOARD_ANALYSIS_LINK + "" + Constants.BC_PERFORMANCE_REPORT;
                ((PIMMasterPage)Page.Master).SetTitle(breadCrumb);
                ((PIMMasterPage)Page.Master).SetStatus(3); 
            }

            var SurveyInitial = (from s in pim.SystemSurvey
                            join sq in pim.SystemSurveyQuestion on s.SSQuestionID equals sq.SSQuestionID
                            join m in pim.Module on sq.ModuleID equals m.ModuleID
                            where s.ParticipantModuleCycleID == cycleID
                            orderby m.ModuleID, sq.SSMeasureQuestionSortOrder select new {m, sq, s,}).ToList();
            List<SystemSurveyData> survey=new List<SystemSurveyData>();
            foreach (var item in SurveyInitial)
            {
                 SystemSurveyData sr= new SystemSurveyData();
                 sr.ModuleShortName = item.m.ModuleName;
                 sr.SSMeasureQuestion = item.sq.SSMeasureQuestion;
                 sr.SSQuestionTrueFalse = item.s.SSQuestionTrueFalse == true ? 1 : 0;
                 if (item.sq.SSMeasureQuestionType == 1)
                 {
                     sr.SSQuestionResponse = item.sq.SSMeasureQuestionType == 1 ? (item.s.SSQuestionTrueFalse == true ? "Yes" : "No") : item.s.SSQuestionResponse;
                 }
                 if (item.sq.SSMeasureQuestionType == 2 || item.sq.SSMeasureQuestionType == 9)
                 {
                     sr.SSQuestionResponse = item.s.SSQuestionResponse;
                 }
                 if (item.sq.SSMeasureQuestionType == 4)
                 {
                     sr.SSQuestionResponse = string.Join("<br />", (from c in pim.SystemSyrveyChoicesUserReply 
                                                               from v in pim.SystemSurveyQuestionChoices 
                                                               where c.ChoiceID==v.ChoiceID
                                                               && c.ParticipantModuleCycleID == cycleID 
                                                               && c.SystemSurveyQuestion.SSQuestionID == item.s.SSQuestionID 
                                                               select v.Choice).ToList());
                 }
                 if (item.sq.SSMeasureQuestionType == 3)
                 {
                     sr.SSQuestionResponse = (from v in pim.SystemSurveyQuestionChoices
                                              where v.ChoiceID == item.s.SSQuestionChoiceID 
                                              select v.Choice).FirstOrDefault();
                 }
                 if (item.sq.SSMeasureQuestionType == 14)
                 {
                     sr.SSQuestionResponse = (from v in pim.SystemSurveyQuestionChoices
                                              where v.ChoiceID == item.s.SSQuestionChoiceID
                                              select v.Choice).FirstOrDefault();
                 }
                 sr.Feedback = item.sq.SSMeasureQuestionType == 1 ? (item.s.SSQuestionTrueFalse == true ? item.sq.SSFeedbackYes : item.sq.SSFeedbackNo) : "";                    survey.Add(sr);
            }
            ListViewSurvey.DataSource = survey;
            ListViewSurvey.DataBind();







            var relatedm = RelatedMeasuresChart(cycleID, P2Flag);
            if (relatedm.Count == 0)
            {
                PanelCombinedmes.Visible = false;
            }
            else
            {
                string ModuleName="";
                foreach (var item in relatedm)
                {

                    if (item.Key.Split('@')[1] != ModuleName)
                    {
                        ModuleName = item.Key.Split('@')[1];
                        HtmlTableRow rowname = new HtmlTableRow();
                        rowname.Attributes.Add("class", "module-name");
                        HtmlTableCell cell1name = new HtmlTableCell();
                        cell1name.Controls.Add(new LiteralControl("<br /><div>" + ModuleName + "</div>"));
                        rowname.Controls.Add(cell1name);
                        HtmlTableCell cell2name = new HtmlTableCell();
                        rowname.Controls.Add(cell2name);
                        HtmlTableCell cell3name = new HtmlTableCell();
                        rowname.Controls.Add(cell3name);
                        StrataTable.Controls.Add(rowname);
                        string ConsideredTable = item.Key.Split('@')[3];
                        HtmlTableRow row = new HtmlTableRow();
                        HtmlTableCell cell1 = new HtmlTableCell();
                        cell1.Controls.Add(new LiteralControl("<br /><div >" + ModuleName + "</div>"));
                        row.Controls.Add(cell1);
                        HtmlTableCell cell2 = new HtmlTableCell();
                        cell2.Controls.Add(new LiteralControl("<div>" + ConsideredTable + "</div>"));
                        row.Controls.Add(cell2);
                        HtmlTableCell cell3 = new HtmlTableCell();
                        cell3.Controls.Add(item.Value);
                        row.Controls.Add(cell3);
                        StrataTable.Controls.Add(row);
                    }
                    else
                    {
                        string ConsideredTable = item.Key.Split('@')[3];
                        HtmlTableRow row = new HtmlTableRow();
                        HtmlTableCell cell1 = new HtmlTableCell();
                        cell1.Controls.Add(new LiteralControl("<br /><div >" + ModuleName + "</div>"));
                        row.Controls.Add(cell1);
                        HtmlTableCell cell2 = new HtmlTableCell();
                        cell2.Controls.Add(new LiteralControl("<div>" + ConsideredTable + "</div>"));
                        row.Controls.Add(cell2);
                        HtmlTableCell cell3 = new HtmlTableCell();
                        cell3.Controls.Add(item.Value);
                        row.Controls.Add(cell3);
                        StrataTable.Controls.Add(row);
                    }
                }

            }

        }
        ModuleNameProcessMeasures = "";
        ModuleNameOutcomeMeasures = "";
        DrawTheChartOfSurvey();
   
    }


    protected void GridViewReport_RowDataBound(object sender, GridViewRowEventArgs e)
    {
        System.Console.Out.WriteLine(e.ToString());
        if (e.Row.RowType == DataControlRowType.DataRow)
        {

            Chart ch = (Chart)e.Row.Cells[3].FindControl("ChartPerformance");
            PerformanceReportData data = (PerformanceReportData)e.Row.DataItem;
            HyperLink chartsIncluded = (HyperLink)e.Row.Cells[2].FindControl("LinkAbstractRecordsIncluded");
            
            if (data.IsPhase2Flag == true)
            {
                ch.Series[0].Points.AddY(data.PeerData);
                ch.Series[0].Points.AddY(data.MeasurePercentCurrent);
                ch.Series[0].Points.AddY(data.MeasureGoal ?? 0);
                ch.Series[0].Points.AddY(data.MeasurePercentPrevious);
                ch.ChartAreas[0].AxisX.CustomLabels.Add(3.5, 4.5, "Your Data");
                ch.ChartAreas[0].AxisX.CustomLabels.Add(2.5, 3.5, "My Goal");
                ch.ChartAreas[0].AxisX.CustomLabels.Add(1.5, 2.5, "Final Data");
                ch.ChartAreas[0].AxisX.CustomLabels.Add(0.5, 1.5, "Peer Data");
                ch.ChartAreas[0].AxisX.Maximum = 5;
                ch.ChartAreas[0].Position.Auto = false;
                ch.ChartAreas[0].Position.X = 0;
                ch.ChartAreas[0].Position.Y = 0;
                ch.ChartAreas[0].Position.Width = 100;
                ch.ChartAreas[0].Position.Height = 100;
                ch.ChartAreas[0].InnerPlotPosition.Auto = false;
                ch.ChartAreas[0].InnerPlotPosition.X = 100;
                ch.ChartAreas[0].InnerPlotPosition.Y = 0;
                ch.ChartAreas[0].InnerPlotPosition.Width = 60;
                ch.ChartAreas[0].InnerPlotPosition.Height = 100;
            }
            else
            {
                ch.Series[0].Points.AddY(data.PeerData);
                ch.Series[0].Points.AddY(data.MeasurePercentCurrent);
                if (data.SelfData != null && data.SelfData > 10)
                {
                    ch.Series[0].Points.AddY(data.SelfData);
                    ch.ChartAreas[0].AxisX.CustomLabels.Add(2.5, 3.5, "Self Assess.");
                    ch.ChartAreas[0].AxisX.Maximum = 4.0;
                    ch.Height = 120;
                }
                ch.ChartAreas[0].AxisX.CustomLabels.Add(1.5, 2.5, "Your Data");
                ch.ChartAreas[0].AxisX.CustomLabels.Add(0.5, 1.5, "Peer Data");
            }

        }
        PreviousMeasureGroupID = MeasureGroupID;
    }
    protected void ListViewProcessMeasures_OnItemDataBound(object sender, ListViewItemEventArgs e)
    {
        Control TD = (Control)e.Item.FindControl("CellProcessMeasures");
        if (e.Item.ItemType == ListViewItemType.DataItem)
        {


            Chart ch = (Chart)e.Item.FindControl("ChartPerformance");
            ch.Series[0].LabelBackColor = System.Drawing.Color.White;
            Label LabelChartConsideredPR = (Label)e.Item.FindControl("LabelChartConsideredPR");

            PerformanceReportData data = (PerformanceReportData)((ListViewDataItem)e.Item).DataItem;
            if (ctx.PrevParticipantModuleCycleID != null && data.IsPhase2Flag == true)
            {
                LabelChartConsideredPR.Text = "<table  style='border:none'><tr style=' border-bottom: thick dotted #ff0000; border-bottom-width: 3px;' ><td  style='border:none' >Final </td><td  style='border:none'>" + data.AbstractRecordsIncluded.ToString() + "</td></tr>";
            }
            else
            {
                LabelChartConsideredPR.Text = "<table  style='border:none'><tr style=' border-bottom: thick dotted #ff0000; border-bottom-width: 3px;' ><td  style='border:none' >Your </td><td  style='border:none'>" + data.AbstractRecordsIncluded.ToString() + "</td></tr>";
            }
            LabelChartConsideredPR.Text += "<tr><td  style='border:none' >Peer </td><td  style='border:none' >" + data.PeerAbstractRecordsIncluded.ToString() + "</td></tr></table>";

            HyperLink chartsIncluded = (HyperLink)e.Item.FindControl("LinkAbstractRecordsIncluded");
            System.Web.UI.HtmlControls.HtmlTableRow divRowModuleName = (System.Web.UI.HtmlControls.HtmlTableRow)e.Item.FindControl("divRowModuleName");
            if (ModuleNameProcessMeasures != data.ModuleName)
            {
                divRowModuleName.Visible = true;
                ModuleNameProcessMeasures = data.ModuleName;
            }
            if (data.IsnumericMeasure==null || data.IsnumericMeasure==false)
            {
                
                System.Web.UI.HtmlControls.HtmlTableRow divRowChart = (System.Web.UI.HtmlControls.HtmlTableRow)e.Item.FindControl("divRowChart");
                if (divRowChart is System.Web.UI.HtmlControls.HtmlTableRow)
                {
                    if ((data.MeasurePercentCurrent < 50) && (data.MayNeedImprovement > 0))
                        divRowChart.Attributes.Add("class", "red-row");
                }


                ch.Series[0].Points.AddY(data.PeerData);
                ch.Series[0].Points.AddY(data.MeasurePercentCurrent);
                if (data.SelfData != null && data.SelfData > 10)
                {
                    ch.Series[0].Points.AddY(data.SelfData);
                    ch.ChartAreas[0].AxisX.CustomLabels.Add(2.5, 3.5, "Self Assess.");
                    ch.ChartAreas[0].AxisX.Maximum = 4.0;
                    ch.Height = 80;
                }

                if (data.IsPhase2Flag == true)
                    ch.ChartAreas[0].AxisX.CustomLabels.Add(1.5, 2.5, "Final Data");
                else
                {
                    ch.ChartAreas[0].AxisX.CustomLabels.Add(1.5, 2.5, "Your Data");

                }
                ch.ChartAreas[0].AxisX.CustomLabels.Add(0.5, 1.5, "Peer Data");
            }
            else
            {
                ch.ChartAreas[0].AxisY.Maximum = 200;
                
              
                ch.ChartAreas[0].AxisX.CustomLabels.Add(0.5, 1.5, "Peer " +( data.NumericMeasureName == null ? "Data" : data.NumericMeasureName));
                ch.Series[0].Points.AddY(Math.Round((double)data.PeerData, 0));
                if (data.IsPhase2Flag == true)
                {
                    ch.ChartAreas[0].AxisX.CustomLabels.Add(1.5, 2.5, "Final " + (data.NumericMeasureName == null ? "Data" : data.NumericMeasureName));
                }
                else
                {
                    ch.ChartAreas[0].AxisX.CustomLabels.Add(1.5, 2.5, "Your " + (data.NumericMeasureName == null ? "Data" : data.NumericMeasureName));
                }
                ch.Series[0].Points.AddY(Math.Round((double)(data.MeasurePercentCurrent), 0));
                if (data.VisualAcuityResponse != null)
                {
                    if (data.VisualAcuityResponse[0].ToString() == "2")
                    {
                        var NumericValue = Convert.ToDouble(data.VisualAcuityResponse.Split('/')[1]);
                        ch.ChartAreas[0].AxisX.CustomLabels.Add(2.5, 3.5, "VA Data");
                        ch.Series[0].Points.AddY(Math.Round(NumericValue, 0));
                    }

                }
            
            }
                        
        }

        PreviousMeasureGroupID = MeasureGroupID;
    }

    protected void ListViewPEC_OnItemDataBound(object sender, ListViewItemEventArgs e)
    {
        if (e.Item.ItemType == ListViewItemType.DataItem)
        {


            Chart ch = (Chart)e.Item.FindControl("ChartPerformance");
            ch.Series[0].LabelBackColor = System.Drawing.Color.White;

            Label LabelChartConsideredPEC = (Label)e.Item.FindControl("LabelChartConsideredPEC");

            PerformanceReportData data = (PerformanceReportData)((ListViewDataItem)e.Item).DataItem;
            LabelChartConsideredPEC.Text = data.AbstractRecordsIncluded.ToString();

            HyperLink chartsIncluded = (HyperLink)e.Item.FindControl("LinkAbstractRecordsIncluded");

            ch.ChartAreas[0].AxisX.Maximum = 2.0;
            ch.Height = 70;

            System.Web.UI.HtmlControls.HtmlTableRow divRowModuleName = (System.Web.UI.HtmlControls.HtmlTableRow)e.Item.FindControl("divRowModuleName");
            if (ModuleNameProcessMeasures != data.ModuleName)
            {
                
                ModuleNameProcessMeasures = data.ModuleName;
            }
            System.Web.UI.HtmlControls.HtmlTableRow divRowChart = (System.Web.UI.HtmlControls.HtmlTableRow)e.Item.FindControl("divRowChart");
            if (divRowChart is System.Web.UI.HtmlControls.HtmlTableRow)
            {
                if ((data.MeasurePercentCurrent < 50) && (data.MayNeedImprovement > 0))
                    divRowChart.Attributes.Add("class", "red-row");

            }
            ch.Series[0].Points.AddY(data.MeasurePercentCurrent);
            if (data.SelfData != null && data.SelfData > 10)
            {
                ch.Series[0].Points.AddY(data.SelfData);
                ch.ChartAreas[0].AxisX.CustomLabels.Add(1.5, 2.5, "Self Assess.");
                ch.ChartAreas[0].AxisX.Maximum = 3.0;
                ch.Height = 80;
            }

            if (data.IsPhase2Flag == true)
                ch.ChartAreas[0].AxisX.CustomLabels.Add(0.5, 1.5, "Final Data");
            else
                ch.ChartAreas[0].AxisX.CustomLabels.Add(0.5, 1.5, "Your Data");
        }

        PreviousMeasureGroupID = MeasureGroupID;
    }

    protected void ListViewOutcomeMeasures_OnItemDataBound(object sender, ListViewItemEventArgs e)
    {
      

        Control TD = (Control)e.Item.FindControl("ChartCell");
        if (e.Item.ItemType == ListViewItemType.DataItem)
        {
            Chart ch = (Chart)e.Item.FindControl("ChartPerformance");

            ch.Series[0].LabelBackColor = System.Drawing.Color.White;
            Label LabelChartConsideredUM = (Label)e.Item.FindControl("LabelChartConsideredUM");
           
            PerformanceReportData data = (PerformanceReportData)((ListViewDataItem)e.Item).DataItem;
            if (ctx.PrevParticipantModuleCycleID != null && data.IsPhase2Flag == true)
            {
                LabelChartConsideredUM.Text = "<table  style='border:none'><tr style=' border-bottom: thick dotted #ff0000; border-bottom-width: 3px;' ><td  style='border:none' >Final </td><td  style='border:none'>" + data.AbstractRecordsIncluded.ToString() + "</td></tr>";
            }
            else
            {
                LabelChartConsideredUM.Text = "<table  style='border:none'><tr style=' border-bottom: thick dotted #ff0000; border-bottom-width: 3px;' ><td  style='border:none' >Your </td><td  style='border:none'>" + data.AbstractRecordsIncluded.ToString() + "</td></tr>";
            }
           
            LabelChartConsideredUM.Text += "<tr  style='border:none' ><td  style='border:none' >Peer </td><td  style='border:none'>" + data.PeerAbstractRecordsIncluded.ToString() + "</td></tr></table>";

            HyperLink chartsIncluded = (HyperLink)e.Item.FindControl("LinkAbstractRecordsIncluded");


            System.Web.UI.HtmlControls.HtmlTableRow divRowModuleName = (System.Web.UI.HtmlControls.HtmlTableRow)e.Item.FindControl("divRowModuleName");
            if (ModuleNameOutcomeMeasures != data.ModuleName)
            {
                divRowModuleName.Visible = true;
                ModuleNameOutcomeMeasures = data.ModuleName;
            }
            if (data.IsnumericMeasure==null || data.IsnumericMeasure==false)
            {
                
              
                System.Web.UI.HtmlControls.HtmlTableRow divRowChart = (System.Web.UI.HtmlControls.HtmlTableRow)e.Item.FindControl("divRowChart");
                if (divRowChart is System.Web.UI.HtmlControls.HtmlTableRow)
                {
                    switch (data.MeasureQualityIndicator)
                    {
                        case "Complications":
                            if (((data.MeasurePercent3To6 < 50) && (data.MayNeedImprovement > 0)) || ((data.MeasurePercent7To12 < 50) && (data.MayNeedImprovement > 0)) || ((data.MeasurePercentRecurrence < 50) && (data.MayNeedImprovement > 0)) || ((data.MeasurePercentNewOnset < 50) && (data.MayNeedImprovement > 0)))
                                divRowChart.Attributes.Add("class", "red-row");
                            break;
                        case "Visual Acuity Improvement":
                            if (((data.MeasurePercent3To6 < 50) && (data.MayNeedImprovement > 0)) || ((data.MeasurePercent7To12 < 50) && (data.MayNeedImprovement > 0)))
                                divRowChart.Attributes.Add("class", "red-row");
                            break;
                        case "Continued Treatment":
                            if (((data.MeasurePercent3To6 < 50) && (data.MayNeedImprovement > 0)) || ((data.MeasurePercent7To12 < 50) && (data.MayNeedImprovement > 0)))
                                divRowChart.Attributes.Add("class", "red-row");
                            break;
                        case "Mean Improvement":
                            divRowChart.Attributes.Add("class", "initial_inner_row");
                            ch.ChartAreas[0].AxisY.Maximum = 50.00;
                            ch.ChartAreas[0].AxisY.Interval = 5;
                            ch.Height = 115;
                            break;
                        default:
                            if ((data.MeasurePercent3To6 < 50) && (data.MayNeedImprovement > 0))
                                divRowChart.Attributes.Add("class", "red-row");
                            break;

                    }
                }
                if (data.PeerData7To12 == -1)
                {
                    ch.Series[0].Points.AddY(data.PeerData3To6);
                    ch.Series[0].Points.AddY(data.MeasurePercent3To6);
                    ch.ChartAreas[0].AxisX.Maximum = 3.0;
                    ch.Height = 70;
                    if (data.SelfData != null && data.SelfData > 10)
                    {
                        ch.Series[0].Points.AddY(data.SelfData);
                        ch.ChartAreas[0].AxisX.CustomLabels.Add(2.5, 3.5, "Self Assess.");
                        ch.ChartAreas[0].AxisX.Maximum = 4.0;
                        ch.Height = 90;
                    }
                    ch.ChartAreas[0].AxisX.CustomLabels.Add(0.5, 1.5, "Peer Data");
                    if (data.IsPhase2Flag == true)
                        ch.ChartAreas[0].AxisX.CustomLabels.Add(1.5, 2.5, "Final Data");
                    else
                        ch.ChartAreas[0].AxisX.CustomLabels.Add(1.5, 2.5, "Your Data");
                }
                else
                {
                   
                    ch.Series[0].Points.AddY(data.PeerData7To12);
                    ch.Series[0].Points.AddY(data.MeasurePercent7To12);
                    ch.Series[0].Points.AddY(data.PeerData3To6);
                    ch.Series[0].Points.AddY(data.MeasurePercent3To6);

                    if (data.MeasurePercentNewOnset != null) 
                    {
                        ch.ChartAreas[0].AxisX.CustomLabels.Add(0.5, 1.5, "Peer Reverse");
                        ch.ChartAreas[0].AxisX.CustomLabels.Add(1.5, 2.5, "My Reverse");
                        ch.ChartAreas[0].AxisX.CustomLabels.Add(2.5, 3.5, "Peer Atropine");
                        ch.ChartAreas[0].AxisX.CustomLabels.Add(3.5, 4.5, "My Atropine");
                        ch.Series[0].Points.AddY(data.PeerDataNewOnset);
                        ch.ChartAreas[0].AxisX.CustomLabels.Add(4.5, 5.5, "Peer NewOnset");
                        ch.Series[0].Points.AddY(data.MeasurePercentNewOnset);
                        ch.ChartAreas[0].AxisX.CustomLabels.Add(5.5, 6.5, "My NewOnset");
                        ch.Series[0].Points.AddY(data.PeerDataRecurrence);
                        ch.ChartAreas[0].AxisX.CustomLabels.Add(6.5, 7.5, "Peer Recurrence");
                        ch.Series[0].Points.AddY(data.MeasurePercentRecurrence);
                        ch.ChartAreas[0].AxisX.CustomLabels.Add(7.5, 8.5, "My Recurrence");
                        if (data.SelfData != null && data.SelfData > 0)
                        {
                            ch.Series[0].Points.AddY(data.SelfData);
                            ch.ChartAreas[0].AxisX.CustomLabels.Add(8.5, 9.5, "Self Assess.");
                        }
                        ch.ChartAreas[0].AxisX.Maximum = 9.0;
                        ch.Height = 200;
                    }
                    else
                    {
                        ch.ChartAreas[0].AxisX.Maximum = 5.0;
                        ch.ChartAreas[0].AxisY.Maximum = 200;
                        if (data.SelfData != null && data.SelfData > 0)
                        {
                            ch.Series[0].Points.AddY(data.SelfData);
                            ch.ChartAreas[0].AxisX.CustomLabels.Add(4.5, 5.5, "Self Assess.");
                            ch.ChartAreas[0].AxisX.Maximum = 6.0;
                        }
                        ch.ChartAreas[0].AxisX.CustomLabels.Add(0.5, 1.5, "Peer 7-12");
                        ch.ChartAreas[0].AxisX.CustomLabels.Add(1.5, 2.5, "My 7-12 Data");
                        ch.ChartAreas[0].AxisX.CustomLabels.Add(2.5, 3.5, "Peer 3-6");
                        ch.ChartAreas[0].AxisX.CustomLabels.Add(3.5, 4.5, "My 3-6 Data");
                    }
                }
            }
            else
            {

                if (data.NumericFullGraph == true)
                {
                    ch.ChartAreas[0].AxisX.Maximum = 6.0;
                    ch.ChartAreas[0].AxisY.Maximum = 200;
                    ch.Series[0].Label = "20/#VALY";
                    ch.Height = 140;
                   // ch.Width = 500;
                   
                    if (data.Peer75Percentile != null)
                    {
                        ch.ChartAreas[0].AxisX.CustomLabels.Add(0.5, 1.5, "Peer 75th percentile");
                        ch.Series[0].Points.AddY(Math.Round((double)data.Peer75Percentile, 0));
                    }

                    if (data.PeerData3To6 != null)
                    {
                        ch.ChartAreas[0].AxisX.CustomLabels.Add(1.5, 2.5, "Peers " + (data.NumericMeasureName == null ? "Data" : data.NumericMeasureName));
                        ch.Series[0].Points.AddY(Math.Round((double)data.PeerData3To6, 0));
                    }
                    if (data.Peer25Percentile != null)
                    {
                        ch.ChartAreas[0].AxisX.CustomLabels.Add(2.5, 3.5, "Peer 25th percentile");
                        ch.Series[0].Points.AddY(Math.Round((double)data.Peer25Percentile, 0));
                    }
                    if (data.IsPhase2Flag == true)
                    {
                        ch.ChartAreas[0].AxisX.CustomLabels.Add(3.5, 4.5, "Final" + (data.NumericMeasureName == null ? "Data" : data.NumericMeasureName));
                    }
                    else
                    {
                        ch.ChartAreas[0].AxisX.CustomLabels.Add(3.5, 4.5, "Your " + (data.NumericMeasureName == null ? "Data" : data.NumericMeasureName));
                    }
                    ch.Series[0].Points.AddY(Math.Round((double)(data.MeasurePercent3To6), 0));
                    if (data.VisualAcuityResponse != null)
                    {
                        if (data.VisualAcuityResponse[0].ToString() == "2")
                        {
                            var NumericValue = Convert.ToDouble(data.VisualAcuityResponse.Split('/')[1]);
                            ch.ChartAreas[0].AxisX.CustomLabels.Add(4.5, 5.5, "VA Data");
                            ch.Series[0].Points.AddY(Math.Round(NumericValue, 0));
                        }

                    }
                }
                else
                {
                    ch.ChartAreas[0].AxisY.Maximum = 200;
                    ch.ChartAreas[0].AxisX.Maximum = 4.0;
                
                    if (data.PeerData3To6 != null)
                    {
                        ch.ChartAreas[0].AxisX.CustomLabels.Add(0.5, 1.5, "Peers " + (data.NumericMeasureName == null ? "Data" : data.NumericMeasureName));
                        ch.Series[0].Points.AddY(Math.Round((double)data.PeerData3To6, 0));
                    }
                    if (data.IsPhase2Flag == true)
                    {
                        ch.ChartAreas[0].AxisX.CustomLabels.Add(1.5, 2.5, "Final " + (data.NumericMeasureName == null ? "Data" : data.NumericMeasureName));
                    }
                    else
                    {
                        ch.ChartAreas[0].AxisX.CustomLabels.Add(1.5, 2.5, "Your " + (data.NumericMeasureName == null ? "Data" : data.NumericMeasureName));
                    }
                    ch.Series[0].Points.AddY(Math.Round((double)(data.MeasurePercent3To6), 0));
                    if (data.VisualAcuityResponse != null)
                    {
                        if (data.VisualAcuityResponse[0].ToString() == "2")
                        {
                            var NumericValue = Convert.ToDouble(data.VisualAcuityResponse.Split('/')[1]);
                            ch.ChartAreas[0].AxisX.CustomLabels.Add(2.5, 3.5, "VA Data");
                            ch.Series[0].Points.AddY(Math.Round(NumericValue, 0));
                        }

                    }
                }
              

            }
        }
        PreviousMeasureGroupID = MeasureGroupID;
    }

    protected void ListViewSurvey_OnItemDataBound(object sender, ListViewItemEventArgs e)
    {
        if (e.Item.ItemType == ListViewItemType.DataItem)
        {


            Chart ch = (Chart)e.Item.FindControl("ChartPerformance");
            SystemSurveyData data = (SystemSurveyData)((ListViewDataItem)e.Item).DataItem;

            System.Web.UI.HtmlControls.HtmlGenericControl divRowYesNoQuestion = (System.Web.UI.HtmlControls.HtmlGenericControl)e.Item.FindControl("divRowYesNoQuestion");
            System.Web.UI.HtmlControls.HtmlGenericControl divRowFreeTextQuestion = (System.Web.UI.HtmlControls.HtmlGenericControl)e.Item.FindControl("divRowFreeTextQuestion");
            System.Web.UI.HtmlControls.HtmlGenericControl divRowHeader = (System.Web.UI.HtmlControls.HtmlGenericControl)e.Item.FindControl("divRowHeader");
            if (data.SSQuestionTrueFalse == 1)
            {
                divRowYesNoQuestion.Visible = true;
                divRowFreeTextQuestion.Visible = false;
            }
            else
            {
                divRowYesNoQuestion.Visible = false;
                divRowFreeTextQuestion.Visible = true;
            }
            System.Web.UI.HtmlControls.HtmlGenericControl divRowModuleName = (System.Web.UI.HtmlControls.HtmlGenericControl)e.Item.FindControl("divRowModuleName");
            
            if (ModuleNameSystemSurvey != data.ModuleShortName)
            {
                divRowModuleName.Visible = true;
                ModuleNameSystemSurvey = data.ModuleShortName;
            }

        }
    }

    protected void LinkButtonSelectMeasures_Click(object sender, EventArgs e)
    {
        using (NetHealthPIMEntities pim = new NetHealthPIMEntities())
        {
            ParticipantModuleCycle cycle = pim.ParticipantModuleCycle.First(c => c.ParticipantModuleCycleID == ctx.ActiveModuleCycleID);
            cycle.AbstractDataComplete = true;
            cycle.AbstractDataCompletedDate = DateTime.Now;
            cycle.AbstractDataLastUpdatedDate = DateTime.Now;
            cycle.ReportApproved = true;
            cycle.ReportApprovedCompletedDate = DateTime.Now;
            cycle.ReportApprovedLastUpdatedDate = DateTime.Now;
            if (cycle.ModuleSelectionComplete != true)
            {
                cycle.ModuleSelectionComplete = true;
                cycle.ModuleSelectionCompletedDate = DateTime.Today;
            }
            pim.SaveChanges();
        }
        Response.Redirect("~/" + module.ModuleCode.ToLower() + "/SelectMeasures.aspx");
    }

    protected void LinkButtonImpactStatement_Click(object sender, EventArgs e)
    {
        using (NetHealthPIMEntities pim = new NetHealthPIMEntities())
        {
            ParticipantModuleCycle cycle = pim.ParticipantModuleCycle.First(c => c.ParticipantModuleCycleID == ctx.ActiveModuleCycleID);
            cycle.AbstractDataComplete = true;
            cycle.AbstractDataCompletedDate = DateTime.Now;
            cycle.AbstractDataLastUpdatedDate = DateTime.Now;
            cycle.ReportApproved = true;
            cycle.ReportApprovedCompletedDate = DateTime.Now;
            cycle.ReportApprovedLastUpdatedDate = DateTime.Now;
            pim.SaveChanges();
        }
        Response.Redirect("~/" + module.ModuleCode.ToLower() + "/ImpactStatement.aspx");
    }



    public Dictionary<string, Chart> RelatedMeasuresChart(int ParticipantModuleCycleID, bool? Phase2Flag)
    {
      
        Dictionary<string, Chart> ModuleCharts = new Dictionary<string, Chart>();
        double InitialConsidered = 0;
        double PeerConsidered = 0;

        string ChartTable = "";
        using (NetHealthPIMEntities pim = new NetHealthPIMEntities())
        {

            var report = from pir in pim.ParticipantInitialReport_V
                         join m in pim.Module on pir.ModuleID equals m.ModuleID
                         where pir.ParticipantModuleCycleID == ParticipantModuleCycleID
                         &&  pir.MeasureRelated!=null
                         orderby m.ModuleID, pir.MeasureGroupSortOrder descending
                         select new PerformanceReportData
                         {
                             ModuleName = m.ModuleName,
                             MeasureID = pir.MeasureID,
                             MeasureLongDescription=pir.MeasureLongDescription,
                             AbstractRecordsIncluded = (int)pir.AbstractRecordsIncluded,
                             MeasurePercentCurrent = pir.MeasurePercentCurrent,
                             PeerAbstractRecordsIncluded = pir.PeerAbstractRecordsIncluded,
                             Peer25Percentile = pir.Peer25Percentile,
                             Peer75Percentile = pir.Peer75Percentile,
                             PeerData = pir.PeerData,
                             MeasureRelated=pir.MeasureRelated,
                             SelfData = pir.NumericResponse,
                             IsPhase2Flag = Phase2Flag,
                             IncludeInImprovementPlan = pir.IncludeInImprovementPlan,
                             MayNeedImprovement = pir.MayNeedImprovement
                         };
            if (report.Count() > 0)
            {
                InitialConsidered = report.Max(c =>(double)c.AbstractRecordsIncluded);
                PeerConsidered = report.Max(c => (double)c.PeerAbstractRecordsIncluded);
                IQueryable<PerformanceReportData> ReportInitial = null;
                if ((Request.QueryString["cycle"] != null && Request.QueryString["cycle"].ToString() == "2") && ctx.PrevParticipantModuleCycleID != null)
                {
                    ReportInitial = from pir in pim.ParticipantInitialReport_V
                                    join m in pim.Module on pir.ModuleID equals m.ModuleID
                                    where pir.ParticipantModuleCycleID == ctx.PrevParticipantModuleCycleID
                                    && pir.MeasureRelated != null
                                    orderby m.ModuleID, pir.MeasureGroupSortOrder
                                    select new PerformanceReportData
                                    {
                                        ModuleName = m.ModuleName,
                                        MeasureID = pir.MeasureID,
                                        MeasureLongDescription = pir.MeasureLongDescription,
                                        AbstractRecordsIncluded = (int)pir.AbstractRecordsIncluded,
                                        MeasurePercentCurrent = pir.MeasurePercentCurrent,
                                        PeerAbstractRecordsIncluded = pir.PeerAbstractRecordsIncluded,
                                        Peer25Percentile = pir.Peer25Percentile,
                                        Peer75Percentile = pir.Peer75Percentile,
                                        PeerData = pir.PeerData,
                                        MeasureRelated = pir.MeasureRelated,
                                        SelfData = pir.NumericResponse,
                                        IsPhase2Flag = Phase2Flag,
                                        IncludeInImprovementPlan = pir.IncludeInImprovementPlan,
                                        MayNeedImprovement = pir.MayNeedImprovement
                                    };


                }

                foreach (var module in report.Select(c => c.ModuleName).Distinct())
                {
                    
                    foreach (var item in report.Where(c => c.ModuleName == module).Select(c => c.MeasureRelated).Distinct())
                    {
                        string Legend = "<table>";
                        Chart chart = new Chart();
                        Series InitailSeries;
                        Legend += "<tr>";
                        if (ReportInitial != null)
                        {
                            InitailSeries = new Series("Final Data");
                            Legend += "<td style='border:none'>Final Data</td>";
                        }
                        else
                        {
                            InitailSeries = new Series("Your Data");
                            Legend += "<td style='border:none'>Your Data</td>";
                        }
                        
                        InitailSeries.ChartType = SeriesChartType.Bar;
                        InitailSeries.Color = System.Drawing.ColorTranslator.FromHtml("#ba55d3");
                        Legend += "<td style='border:none'><div style='width:20px; height:20px; background-color:#ba55d3'></div></td></tr>";
                        InitailSeries.SetCustomProperty("DrawingStyle", "Wedge");
                        InitailSeries.IsValueShownAsLabel = true;
                        InitailSeries.IsVisibleInLegend = false;
                        InitailSeries.LabelForeColor = System.Drawing.ColorTranslator.FromHtml("#000000");
                        InitailSeries.LabelBackColor = System.Drawing.Color.Transparent;
                        chart.Series.Add(InitailSeries);
                        if (ReportInitial != null)
                        {
                            Series PastInitialSeries = new Series("Your Data");
                            Legend += "<tr><td style='border:none'>Your Data</td>";
                            PastInitialSeries.ChartType = SeriesChartType.Bar;
                            PastInitialSeries.Color = System.Drawing.ColorTranslator.FromHtml("#c71585");
                            Legend += "<td style='border:none'><div style='height:20px; width:20px; background-color:#c71585'><div></td></tr>";
                            PastInitialSeries.SetCustomProperty("DrawingStyle", "Wedge");
                            PastInitialSeries.IsValueShownAsLabel = true;
                            PastInitialSeries.IsVisibleInLegend = false;
                            PastInitialSeries.LabelForeColor = System.Drawing.ColorTranslator.FromHtml("#000000");
                            PastInitialSeries.LabelBackColor = System.Drawing.Color.Transparent;
                            chart.Series.Add(PastInitialSeries);

                        }
                        Series PeerSeries = new Series("Peer Data");
                        Legend += "<tr><td style='border:none'>Peer Data</td>";
                        PeerSeries.ChartType = SeriesChartType.Bar;
                        PeerSeries.Color = System.Drawing.ColorTranslator.FromHtml("#8a2bea");
                        Legend += "<td  style='border:none' ><div style='height:20px; width:20px; background-color:#8a2bea'><div></td></tr>";
                        PeerSeries.SetCustomProperty("DrawingStyle", "Wedge");
                        PeerSeries.IsValueShownAsLabel = true;
                        PeerSeries.IsVisibleInLegend = false;
                        PeerSeries.LabelForeColor = System.Drawing.ColorTranslator.FromHtml("#000000");
                        PeerSeries.LabelBackColor = System.Drawing.Color.Transparent;
                        chart.Series.Add(PeerSeries);
                        Legend += "</table>";
                        Legend legend = new Legend();
                        chart.Legends.Add(legend);
                        ChartArea chartArea = new ChartArea();
                        chartArea.AxisX.MajorGrid.Enabled = false;
                        chartArea.AxisX.LabelStyle.IsEndLabelVisible = false;
                        chart.Legends[0].Position.Auto = false;
                        chart.Legends[0].Position = new ElementPosition(30, 5, 150, 20);
                        chart.ChartAreas.Add(chartArea);
                        Axis yAxis = new Axis(chartArea, AxisName.Y);
                        yAxis.Maximum = 100;
                        yAxis.Interval = 20;
                        Axis xAxis = new Axis(chartArea, AxisName.X);
                        chart.ChartAreas[0].AxisX.LabelStyle.Interval = 1;
                       // chart.ChartAreas[0].AxisX.LabelStyle.IsStaggered = true;
                        chart.ChartAreas[0].AxisX.LabelStyle.TruncatedLabels = true;
                        chart.ChartAreas[0].AxisY.LabelStyle.Format = "{#}%";
                        chart.ChartAreas[0].AxisX.LabelStyle.Font
            = new System.Drawing.Font("Trebuchet MS", 10.25F, System.Drawing.FontStyle.Regular);
                        chart.ChartAreas[0].AxisY.Maximum = 100;
                      
                        var DataText = report.Where(c => c.ModuleName == module && c.MeasureRelated == item).Select(c => c.MeasureLongDescription).ToArray();
                        var InitalData = report.Where(c => c.ModuleName == module && c.MeasureRelated == item).Select(c => c.MeasurePercentCurrent).ToArray();
                        var PeerData = report.Where(c => c.ModuleName == module && c.MeasureRelated == item).Select(c => c.PeerData).ToArray();
                        if (ReportInitial != null)
                        {
                            var DataPastInitial = ReportInitial.Where(c => c.ModuleName == module && c.MeasureRelated == item).Select(c => c.MeasurePercentCurrent).ToArray();
                            chart.Series["Final Data"].Points.DataBindXY(DataText, InitalData);
                            chart.Series["Your Data"].Points.DataBindXY(DataText, DataPastInitial);
                            chart.Series["Peer Data"].Points.DataBindXY(DataText, PeerData);
                            chart.Series["Final Data"]["PixelPointWidth"] = "40";
                            chart.Series["Your Data"]["PixelPointWidth"] = "40";
                            chart.Series["Peer Data"]["PixelPointWidth"] = "40";
                        }
                        else
                        {
                            chart.Series["Your Data"].Points.DataBindXY(DataText, InitalData);
                            chart.Series["Peer Data"].Points.DataBindXY(DataText, PeerData);
                            chart.Series["Your Data"]["PixelPointWidth"] = "40";
                            chart.Series["Peer Data"]["PixelPointWidth"] = "40";
                        }
                        if (DataText.Count() > 10)
                        {
                            chart.Width = new System.Web.UI.WebControls.Unit(500, System.Web.UI.WebControls.UnitType.Pixel);
                            chart.Height = new System.Web.UI.WebControls.Unit(700, System.Web.UI.WebControls.UnitType.Pixel);
                        }
                        else if (DataText.Count() > 5)
                        {
                            chart.Width = new System.Web.UI.WebControls.Unit(500, System.Web.UI.WebControls.UnitType.Pixel);
                            chart.Height = new System.Web.UI.WebControls.Unit(DataText.Count() * 100, System.Web.UI.WebControls.UnitType.Pixel);
                        }
                        else if (DataText.Count() > 3)
                        {
                            chart.Width = new System.Web.UI.WebControls.Unit(500, System.Web.UI.WebControls.UnitType.Pixel);
                            chart.Height = new System.Web.UI.WebControls.Unit(DataText.Count() * 100, System.Web.UI.WebControls.UnitType.Pixel);
                        }

                        if (ReportInitial == null)
                        {
                            ChartTable = "<table  style='border:none'><tr style=' border-bottom: thick dotted #ff0000; border-bottom-width: 3px;' ><td  style='border:none' >Your </td><td  style='border:none'>" + InitialConsidered + "</td></tr>";
                            ChartTable += "<tr  style='border:none' ><td  style='border:none' >Peer </td><td  style='border:none'>" + PeerConsidered + "</td></tr></table><br /><br /><br />"+Legend;

                        }
                        else
                        {

                            ChartTable = "<table  style='border:none'><tr style=' border-bottom: thick dotted #ff0000; border-bottom-width: 3px;' ><td  style='border:none' >Final </td><td  style='border:none'>" + InitialConsidered + "</td></tr>";
                            ChartTable += "<tr  style='border:none' ><td  style='border:none' >Peer </td><td  style='border:none'>" + PeerConsidered + "</td></tr></table><br /><br /><br />"+Legend;

                        }




                        ModuleCharts.Add(item + "@" + module + "@" + DataText.Count() + "@" + ChartTable, chart);
                    }
                }
            }
            return ModuleCharts;
        }

     

    }


    public void DrawTheChartOfSurvey()
    {
        using (NetHealthPIMEntities pim = new NetHealthPIMEntities())
        {

            var SystemSyrveyQuestions = (from c in pim.SystemSurveyQuestion from v in pim.ParticipantModuleSelection
                                         where c.ModuleID== v.ModuleID && (c.SSMeasureQuestionType == 1 ||
                                         c.SSMeasureQuestionType == 3 ||
                                         c.SSMeasureQuestionType == 14 ||
                                         c.SSMeasureQuestionType == 4)
                                         && v.ParticipantModuleCycleID==ctx.ActiveModuleCycleID
                                         select c).ToList();

            var UserSelectedModules= (from v in pim.ParticipantModuleSelection 
                                     from n in pim.Module  
                                     where v.ParticipantModuleCycleID==ctx.ActiveModuleCycleID
                                     && n.ModuleID==v.ModuleID 
                                     select new { n.ModuleID, n.ModuleName}).ToList();


            var AllSelectedQuestionByUser = (from c in pim.SystemSurvey 
                                             where c.ParticipantModuleCycleID == ctx.ActiveModuleCycleID 
                                             select c).ToList();

            var AllSystemSyrveyQuestionChoices = (from c in pim.SystemSurveyQuestionChoices 
                                                  select c).ToList();

            var AllSystemSyrveyChoices=pim.SystemSyrveyChoicesUserReply.Select(c=>new {c.SystemSurveyQuestion.SSQuestionID, c.ParticipantModuleCycleID, c.ChoiceID}).ToList();
            foreach (var mod in UserSelectedModules)
            {
                HtmlTableRow rowmodname = new HtmlTableRow();
                rowmodname.Attributes.Add("class", "module-name");
                HtmlTableCell CellModName = new HtmlTableCell();
                CellModName.Controls.Add(new LiteralControl(mod.ModuleName));
                HtmlTableCell Cell2 = new HtmlTableCell();
                HtmlTableCell Cell3 = new HtmlTableCell();
                rowmodname.Controls.Add(CellModName);
                rowmodname.Controls.Add(Cell2);
                rowmodname.Controls.Add(Cell3);
                systemtable.Controls.Add(rowmodname);
                foreach (var item in SystemSyrveyQuestions.Where(c=>c.ModuleID==mod.ModuleID))
                {
                    HtmlTableRow row = new HtmlTableRow();
                    HtmlTableCell cellQuestion = new HtmlTableCell();
                    cellQuestion.Controls.Add(new LiteralControl(item.SSMeasureQuestion));
                    HtmlTableCell cellYourAnswer = new HtmlTableCell();



                    Chart chart = new Chart();
                    Legend legend = new Legend();
                    chart.Legends.Add(legend);
                    ChartArea chartArea = new ChartArea();
                    chartArea.AxisX.MajorGrid.Enabled = false;
                    chartArea.AxisX.LabelStyle.IsEndLabelVisible = true;
                    chart.Legends[0].Position.Auto = false;
                    chart.ChartAreas.Add(chartArea);
                    Axis yAxis = new Axis(chartArea, AxisName.Y);
                    Axis xAxis = new Axis(chartArea, AxisName.X);
                    chart.ChartAreas[0].AxisX.LabelStyle.TruncatedLabels = false;

                    chart.ChartAreas[0].AxisX.LabelStyle.IntervalOffset = 1;
                    chart.ChartAreas[0].AxisX.Interval = 1;
                    chart.ChartAreas[0].AxisX.IsLabelAutoFit = true;
                    chart.ChartAreas[0].AxisX.LabelStyle.IsEndLabelVisible = true;
                    chart.ChartAreas[0].AxisY.LabelStyle.Format = "{#}";
                    chart.ChartAreas[0].AxisX.LabelStyle.Font = new System.Drawing.Font("Trebuchet MS", 10.25F, System.Drawing.FontStyle.Regular);
                    Series Series = new Series();
                    Series.ChartType = SeriesChartType.Bar;
                    Series.Color = System.Drawing.ColorTranslator.FromHtml("#8a2bea");
                    Series.SetCustomProperty("DrawingStyle", "Wedge");
                    Series.IsValueShownAsLabel = true;
                    Series.IsVisibleInLegend = false;
                    Series.LabelForeColor = System.Drawing.ColorTranslator.FromHtml("#000000");
                    Series.LabelBackColor = System.Drawing.Color.Transparent;
                    chart.Series.Add(Series);


                    HtmlTableCell cellGraphAnswer = new HtmlTableCell();
                    if (item.SSMeasureQuestionType == 1)
                    {

                        var SelectedQuestion = (from c in AllSelectedQuestionByUser
                                                where c.SSQuestionID == item.SSQuestionID
                                                select c.SSQuestionTrueFalse).FirstOrDefault();
                        cellYourAnswer.Controls.Add(new LiteralControl(SelectedQuestion == true ? "Yes" : "No"));




                        var SlectedQuestionYES = (from c in pim.SystemSurvey
                                                  where c.SSQuestionID == item.SSQuestionID
                                                  && c.SSQuestionTrueFalse == true
                                                  select c.SSQuestionTrueFalse).Count();
                        var SlectedQuestionNO = (from c in pim.SystemSurvey
                                                 where c.SSQuestionID == item.SSQuestionID
                                                 && c.SSQuestionTrueFalse == false
                                                 select c.SSQuestionTrueFalse).Count();
                        List<GroupResult> GroupAnswers = new List<GroupResult>();
                        GroupAnswers.Add(new GroupResult { Name = "Yes", Total = SlectedQuestionYES });
                        GroupAnswers.Add(new GroupResult { Name = "No", Total = SlectedQuestionNO });
                        foreach (var ch in GroupAnswers)
                        {

                            chart.Series[0].Points.AddXY(ch.Name.ToString(), ch.Total);

                        }
                        chart.Height = new System.Web.UI.WebControls.Unit(GroupAnswers.Count() * 40, System.Web.UI.WebControls.UnitType.Pixel);
                        chart.Width = new System.Web.UI.WebControls.Unit(300, System.Web.UI.WebControls.UnitType.Pixel);

                        cellGraphAnswer.Controls.Add(chart);
                    }
                    else if (item.SSMeasureQuestionType == 3 || item.SSMeasureQuestionType == 14)
                    {

                        var SelectedQuestion = (from c in AllSelectedQuestionByUser
                                                from v in AllSystemSyrveyQuestionChoices
                                                where c.SSQuestionID == item.SSQuestionID
                                                && v.ChoiceID == c.SSQuestionChoiceID
                                                select v.Choice).FirstOrDefault();
                        cellYourAnswer.Controls.Add(new LiteralControl(SelectedQuestion.ToString()));

                        var GroupAnswers = pim.ExecuteStoreQuery<GroupResult>(@"SELECT COUNT(SystemSurvey.SSQuestionChoiceID) AS Total,                SystemSurveyQuestionChoices.Choice AS Name
          FROM SystemSurveyQuestionChoices 
          LEFT OUTER JOIN SystemSurvey ON SystemSurveyQuestionChoices.ChoiceID=SystemSurvey.SSQuestionChoiceID
          WHERE SystemSurveyQuestionChoices.QuestionID=" + item.SSQuestionID + " GROUP BY SystemSurveyQuestionChoices.Choice").ToList();

                        foreach (var ch in GroupAnswers)
                        {

                            chart.Series[0].Points.AddXY(ch.Name.ToString(), ch.Total);
                        }

                        if (GroupAnswers.Count() < 5)
                        {
                            chart.Height = new System.Web.UI.WebControls.Unit(GroupAnswers.Count() * 40, System.Web.UI.WebControls.UnitType.Pixel);
                            chart.Width = new System.Web.UI.WebControls.Unit(300, System.Web.UI.WebControls.UnitType.Pixel);
                        }
                        else if (GroupAnswers.Count() < 10)
                        {
                            chart.Height = new System.Web.UI.WebControls.Unit(GroupAnswers.Count() * 35, System.Web.UI.WebControls.UnitType.Pixel);
                            chart.Width = new System.Web.UI.WebControls.Unit(300, System.Web.UI.WebControls.UnitType.Pixel);
                        }
                        else if (GroupAnswers.Count() < 15)
                        {
                            chart.Height = new System.Web.UI.WebControls.Unit(GroupAnswers.Count() * 25, System.Web.UI.WebControls.UnitType.Pixel);
                            chart.Width = new System.Web.UI.WebControls.Unit(300, System.Web.UI.WebControls.UnitType.Pixel);
                        }
                        cellGraphAnswer.Controls.Add(chart);
                    }
                    else if (item.SSMeasureQuestionType == 4)
                    {
                        var SelectedQuestion = string.Join(", ", (from c in AllSystemSyrveyChoices
                                                                  from v in AllSystemSyrveyQuestionChoices
                                                                  where c.ChoiceID == v.ChoiceID &&
                                                                  c.ParticipantModuleCycleID == ctx.ActiveModuleCycleID
                                                                  && v.SystemSurveyQuestion.SSQuestionID == item.SSQuestionID
                                                                  select v.Choice).ToList());
                        cellYourAnswer.Controls.Add(new LiteralControl(SelectedQuestion.ToString()));


                        var GroupAnswers = pim.ExecuteStoreQuery<GroupResult>(@"SELECT COUNT(SystemSyrveyChoicesUserReply.QuestionChoicesUserReplyID) AS Total, SystemSurveyQuestionChoices.Choice AS Name
  FROM SystemSurveyQuestionChoices 
  LEFT OUTER JOIN SystemSyrveyChoicesUserReply ON SystemSurveyQuestionChoices.ChoiceID=SystemSyrveyChoicesUserReply.ChoiceID
  WHERE SystemSurveyQuestionChoices.QuestionID=" + item.SSQuestionID + " GROUP BY SystemSurveyQuestionChoices.Choice").ToList();

                        foreach (var ch in GroupAnswers)
                        {

                            chart.Series[0].Points.AddXY(ch.Name.ToString(), ch.Total);
                        }
                        if (GroupAnswers.Count() < 5)
                        {
                            chart.Height = new System.Web.UI.WebControls.Unit(GroupAnswers.Count() * 40, System.Web.UI.WebControls.UnitType.Pixel);
                            chart.Width = new System.Web.UI.WebControls.Unit(300, System.Web.UI.WebControls.UnitType.Pixel);
                        }
                        else if (GroupAnswers.Count() < 10)
                        {
                            chart.Height = new System.Web.UI.WebControls.Unit(GroupAnswers.Count() * 35, System.Web.UI.WebControls.UnitType.Pixel);
                            chart.Width = new System.Web.UI.WebControls.Unit(300, System.Web.UI.WebControls.UnitType.Pixel);
                        }
                        else if (GroupAnswers.Count() < 15)
                        {
                            chart.Height = new System.Web.UI.WebControls.Unit(GroupAnswers.Count() * 25, System.Web.UI.WebControls.UnitType.Pixel);
                            chart.Width = new System.Web.UI.WebControls.Unit(300, System.Web.UI.WebControls.UnitType.Pixel);
                        }
                        cellGraphAnswer.Controls.Add(chart);

                    }
                    row.Controls.Add(cellQuestion);
                    row.Controls.Add(cellYourAnswer);
                    row.Controls.Add(cellGraphAnswer);
                    systemtable.Controls.Add(row);
                }
            }

        }
        
    
    }

    public class GroupResult
    {
        public string Name { get; set; }
        public int Total { get; set; }
    }
}
