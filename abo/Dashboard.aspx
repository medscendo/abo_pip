﻿<%@ Page Title="" Language="C#" MasterPageFile="~/PIMMasterPage.master" AutoEventWireup="true" CodeFile="Dashboard.aspx.cs" Inherits="abo_Dashboard" %>
<asp:Content ID="Content1" ContentPlaceHolderID="head" Runat="Server">

    <style>
        
        .surveys_complete 
        {
            width: 100%;
            font-size: 18px;
            color:#3e2449;
            text-align:center;
        }

        .upper-panel {
            float:left;
            margin-bottom:10px;
        }

        .table {
            margin-bottom: 20px;
        }

        .table th, .prior-data {
            text-align:center;
        }

        .table tr th, .table tr td {
            border-right:none;
            border-left:none;
        }

        .table tr th.first, .table tr td.first {
            border-left: 1px solid #ccc;
        }
        .table tr th.last, .table tr td.last {
            border-right: 1px solid #ccc;
        }

        .right-col {
            text-align:right;
            width:20%;
        }

        .legend {
            float:right;
            margin-top:10px;
            border: 3px solid #333;
            padding: 10px;
        }
        .legend div {
            float:left;
            padding: 0 10px;
        }
    </style>

</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder1" Runat="Server">

<!--<div class="heading_box">
    <h2><asp:Literal ID="LiteralShortModuleDescription" runat="server"></asp:Literal> Dashboard</h2>
    <div class="top_nav">
    <ul>
    <li class="active"><a href="#"><span>Dashboard</span></a></li>
        <li><a href="PIMTutorial.aspx"><span>PIP Tutorial</span></a></li>          
        <li><a href="ModuleProcess.aspx"><span>Process Overview</span></a></li>           
        <li><a href="ImprovementResources.aspx"><span>Improvement Resources</span></a></li>
    </ul>
    </div>-->
    <asp:HiddenField runat="server" ID="HiddenFieldPlanProgressPct" />
    <div class="upper-panel clearfix">
        <div class="clearfix">
            <div class="text-panel">
                <p>
                    This screen illustrates your progress and current status in the process.  You may return to this page at any time by clicking &quot;Assess &amp; Improve&quot; in the top status bar.  
                </p>
                <p>
                    Click on the name of the activity to move on to that section of the activity. 
                </p>
                    <%--This page shows your current status in the process and the steps needed to complete. You can return to this page at any time, by clicking "Review &amp; Planning" in the top status bar.</p>
                <p><strong>Please complete the <asp:Literal id="LiteralModuleCount" runat="server"></asp:Literal>  and the improvement planning below.</strong></p>--%>
            </div>
            <div class="progress-panel">
                <h3>Assess &amp; Improve Progress</h3>
                <div class="progressbar">
                    <div class="progress" id="progress1"></div>
                </div>
                <asp:Label ID="Label1" runat="server">&nbsp;</asp:Label>
                <asp:LinkButton runat="server" ID="LinkButtonNextStep" OnCommand="LinkButtonNext_Click" class="button blue">Continue where you left off</asp:LinkButton>
            </div>
        </div>
        <div class="legend clearfix">
            <div><img src="../common/images/icon-check.png" /> - completed</div>
            <div><img src="../common/images/icon-dash.png" /> - to be completed</div>
        </div>
    </div>


<asp:Repeater runat="server" ID="RepeaterModules" OnItemDataBound="RepeaterModules_ItemDataBound">
    <HeaderTemplate>
    </HeaderTemplate>   
    <ItemTemplate>
    <table class="table">
        <tr>
            <th><%# Eval("ModuleShortName")%></th>
            <th>Status</th>
        </tr>
        <tr>
            <td><a href="#">Confirm Activity Selection</a> (Activity confirmation occurred during Create Your Experience)</td>
            <td class="right-col">
                <img src="../common/images/icon-check.png" /></td>
        </tr>
        <tr>
            <td><asp:LinkButton ID="LinkButtonPatientCharts" runat="server" OnCommand="ButtonCharts_Click" CommandArgument='<%# Eval("ModuleID")%>'>Register Charts</asp:LinkButton> (Please select qualified patient visit charts that meet the specified patient definition)</td>
            <td class="right-col">
                <%# Eval("PatientChartRegistrationCompleted")%> of <%# Eval("TotalCharts")%> complete
                <img src="../common/images/icon-check.png"  runat="server" id="imageChartRegistration" alt="" visible="false" />
            </td>
        </tr>
        <tr>
            <td><asp:LinkButton ID="HyperLinkSelfAssessment" runat="server" OnCommand="ButtonSelfAssessment_Click" CommandArgument='<%# Eval("ModuleID")%>'>Self-Assessment</asp:LinkButton> (Reflect on your practice and current processes of care)</td>
            <td class="right-col">
                <img runat="server" id="imageSelfAssessment" src="../common/images/icon-check.png" alt="" />
            </td>
        </tr>
        <tr>
            <td><asp:LinkButton ID="HyperLinkSystemSurvey" runat="server" OnCommand="ButtonSystemSurvey_Click" CommandArgument='<%# Eval("ModuleID")%>'>System Survey</asp:LinkButton> (Consider system characteristics that impact your practice)</td>
            <td class="right-col">
                <img src="../common/images/icon-dash.png"  runat="server" id="imageSystemSurvey" alt="" /></td>
        </tr>
        <tr>
            <td><asp:LinkButton ID="HyperLinkPatientChartRegistration" runat="server" OnCommand="ButtonCharts_Click" CommandArgument='<%# Eval("ModuleID")%>'>Chart Abstraction</asp:LinkButton> (Abstract the charts you registered for this activity)</td>
            <td class="right-col">
                <%# Eval("ChartsCompleted")%> of <%# Eval("TotalCharts")%> complete
                <img src="../common/images/icon-check.png"  runat="server" id="imageChartAbstraction" alt="" visible="false" />
            </td>
        </tr>
    </table>
    </ItemTemplate>     
    <FooterTemplate>
    </FooterTemplate>
</asp:Repeater>    


    <!-- PEC -->
    <table class="table" runat="server" id="PECSurvey">
        <tr>
            <th>Patient Experience of Care</th>
            <th>Status</th>
        </tr>
        <tr>
            <td><a href="#" target="_blank" runat="server" id="hrefPECSurvey">Patient Survey Information</a> (Collect 45 surveys or your specified targeted number for your patients)</td>
            <td class="right-col">
                <asp:Label ID="LabelPatientSurvey" runat="server"></asp:Label> of 45 recorded
            </td>
        </tr>
    </table>
    <table class="table">
        <tr>
            <th>Improvement Planning</th>
            <th>Status</th>
        </tr>
        <tr>
            <td><asp:HyperLink ID="HyperLinkReviewPerformanceReport" runat="server">Review Practice Report</asp:HyperLink> (Review initial practice feedback)</td>
            <td class="right-col">
                <img runat="server" id="imagePerformanceReport" src="../common/images/icon-dash.png" alt=""/>
            </td>
        </tr>
        <tr>
            <td><asp:HyperLink ID="HyperLinkSelectMeasures" runat="server">Select Measures for Improvement</asp:HyperLink> (Choose your process or outcome measures for improvement)</td>
            <td class="right-col">
                <img runat="server" id="imageSelectMeasures" src="../common/images/icon-dash.png" alt=""/>
                <asp:Label runat="server" id="LabelMeasures"  Visible = "false" Text="3 Selected"></asp:Label>
            </td>
        </tr>
        <tr>
            <td><asp:HyperLink ID="HyperLinkDevelopImprovementPlan" runat="server">Develop Improvement Plan</asp:HyperLink> (Choose your process or outcome measures for improvement)</td>
            <td class="right-col">
                <img runat="server" id="imageImprovementPlan" src="../common/images/icon-dash.png" alt=""/>
            </td>
        </tr>
        <tr>
        <td><asp:HyperLink ID="HyperLinkReviewImprovement" runat="server">Review Improvement</asp:HyperLink> (After re-assessment review your improvement as compared to your initial patient chart abstraction)</td>
            <td class="right-col">
                <img runat="server" id="imageReviewImprovement" src="../common/images/icon-dash.png" alt=""/>
            </td>
        </tr>
    </table>
   
    <div class="button-box"><asp:LinkButton ID="LinkButtonSubmitCompleteModule" runat="server" Text="Submit Complete Activity"  PostBackUrl="Congratulations.aspx" Visible="false" CssClass="button" /></div>
          
</asp:Content>

<asp:Content ID="Content4" ContentPlaceHolderID="javascript" runat="server">

    <script type="text/javascript">
        $(document).ready(function() {

            // This variable sets the percentage complete
            var prog = $("#<%= HiddenFieldPlanProgressPct.ClientID %>").val();
            var progressClass = document.getElementById('progress1');
            if (prog > 100)
                prog = 100;

            // Set width, changing how much progress bar shows
            $("#progress1").css("width", prog + "%");

            // Puts numbers to right 
            if (prog < 10) {
                progressClass.innerHTML = '<p style="left:30px; color:#7f238e">' + prog + '%</p>';
            }
            else {
                progressClass.innerHTML = '<p>' + prog + '%</p>';
            }
        });
        function ValidateForm() {
            return true;
        }
    </script>

    <div style="display:none;">
    <div>This Status Page shows the required steps for the selected diagnosis.  
    <ul  style="padding-left: 20px;"><li >To access a step, click the link in the second column below.
    </li><li >When the box in the first column ("Status") is checked, that step is completed. 
</li><li>Target dates are automatically generated, based on typical timing for completion
    </li><li>Once all the boxes in the first column are checked, click the orange “Submit Completed Activity” button.  
</li></ul>  
</div>
<div class="dashboard_section">
<table width="100%">
<tr>
<td style="width:675px">
    <div class="dashboard_left_section">
    <fieldset>
    <div class="row">
    <ul>
    <li class="first">
    <span class="col col1" style="text-align: center; vertical-align: middle; padding-top: 5px;">Status</span>
    <span class="col col2 left_text">Data Collection</span>
    <span class="col col3">My Data</span>
    <span class="col col4">Number to Complete</span>
    <span class="col col5">Target to Complete Section</span>
    <span class="col col6">Actual Completion Date</span>
    </li>
          
    <li>
    <span class="col col1"><asp:CheckBox ID="CheckBoxSelfAssessment" runat="server" Enabled="false"/></span>
    <span class="col col2"></span>
    <span class="col col3"><asp:Literal ID="LiteralSelfAssessmentMyData" runat="server">-</asp:Literal></span>
    <span class="col col4">1</span>
    <span class="col col5"><asp:Literal ID="LiteralSelfAssessmentTargetDate" runat="server"></asp:Literal></span>
    <span class="col col6"><asp:Literal ID="LiteralSelfAssessmentCompleteDate" runat="server">-</asp:Literal></span><br style="clear:both" />
    </li>
          
    <li class="blue">
    <span class="col col1"><asp:CheckBox ID="CheckBoxSystemSurvey" runat="server" Enabled="false"/></span>
    <span class="col col2"></span>
    <span class="col col3"><asp:Literal ID="LiteralSystemSurveyMyData" runat="server">-</asp:Literal></span>
    <span class="col col4">1</span>
    <span class="col col5"><asp:Literal ID="LiteralSystemSurveyTargetDate" runat="server"></asp:Literal></span>
    <span class="col col6"><asp:Literal ID="LiteralSystemSurveyCompleteDate" runat="server">-</asp:Literal></span><br style="clear:both" />
    </li>
          
    <li class="bottom">
    <span class="col col1"><asp:CheckBox ID="CheckBoxPatientChartRegistration" runat="server" Enabled="false"/></span>
    <span class="col col2"><a href="PatientChartRegistration.aspx"></a></span>
    <span class="col col5"><asp:Literal ID="LiteralChartAbstractionTargetDate" runat="server"></asp:Literal></span>
    <span class="col col6"><asp:Literal ID="LiteralChartAbstractionCompleteDate" runat="server">-</asp:Literal></span><br style="clear:both" />
    </li>
          
    </ul></div>
          
    <div class="row">
    <ul>
    <li class="first">
    <span class="col col1"  style="text-align: center; vertical-align: middle; padding-top: 5px;">Status</span>
    <span class="col col2 left_text"> Improvement Plan</span>
    <span class="col col3">My
Data</span>
    <span class="col col4">Number to
Complete</span>
    <span class="col col5">Target to Complete
Section</span>
    <span class="col col6">Actual Completion
Date</span>
    </li>
          
    <li>
    <span class="col col1"><asp:CheckBox ID="CheckBoxReviewPerformanceReport" runat="server" Enabled="false"/></span>
    <span class="col col2"><asp:HyperLink ID="HyperLinkReviewPerformanceReportOLD" runat="server">Review Initial Feedback Report</asp:HyperLink></span>
    <span class="col col3"><asp:Literal ID="LiteralReviewPerformanceReportCount" runat="server">-</asp:Literal></span>
    <span class="col col4">1</span>
    <span class="col col5"><asp:Literal ID="LiteralReviewPerformanceReportTargetDate" runat="server">-</asp:Literal></span>
    <span class="col col6"><asp:Literal ID="LiteralReviewPerformanceReportCompleteDate" runat="server">-</asp:Literal></span><br style="clear:both" />
    </li>
          
    <li class="blue_bottom">
    <span class="col col1"><asp:CheckBox ID="CheckBoxDevelopImprovementPlan" runat="server" Enabled="false"/></span>
    <span class="col col2"><asp:HyperLink ID="HyperLinkDevelopImprovementPlanOLD" runat="server">Develop Improvement Plan</asp:HyperLink></span>
    <span class="col col3"><asp:Literal ID="LiteralDevelopImprovementPlanCount" runat="server">-</asp:Literal></span>
    <span class="col col4">1</span>
    <span class="col col5"><asp:Literal ID="LiteralDevelopImprovementPlanTargetDate" runat="server">-</asp:Literal></span>
    <span class="col col6"><asp:Literal ID="LiteralDevelopImprovementPlanCompleteDate" runat="server">-</asp:Literal></span><br style="clear:both" />
    </li>
          
          
    </ul></div>
          
    <div class="row" runat="server" id="remeasure">
    <ul>
    <li class="first">
    <span class="col col1"  style="text-align: center; vertical-align: middle; padding-top: 5px;">Status</span>
    <span class="col col2 left_text"> Remeasurement</span>
    <span class="col col3">My
Data</span>
    <span class="col col4">Number to
Complete</span>
    <span class="col col5">Target to Complete
Section</span>
    <span class="col col6">Actual Completion
Date</span>
    </li>
          
    <li>
    <span class="col col1"><asp:CheckBox ID="CheckBoxSelfAssessmentCycle2" runat="server" Enabled="false"/></span>
    <span class="col col2"><asp:HyperLink ID="HyperLinkSelfAssessmentCycle2" runat="server">Self Assessment</asp:HyperLink></span>
    <span class="col col3"><asp:Literal ID="LiteralSelfAssessmentMyDataCycle2" runat="server">-</asp:Literal></span>
    <span class="col col4">1</span>
    <span class="col col5"><asp:Literal ID="LiteralSelfAssessmentTargetDateCycle2" runat="server">-</asp:Literal></span>
    <span class="col col6"><asp:Literal ID="LiteralSelfAssessmentCompleteDateCycle2" runat="server">-</asp:Literal></span><br style="clear:both" />
    </li>
          
    <li class="blue">
    <span class="col col1"><asp:CheckBox ID="CheckBoxSystemSurveyCycle2" runat="server" Enabled="false"/></span>
    <span class="col col2"><asp:HyperLink ID="HyperLinkSystemSurveyCycle2" runat="server">System Survey</asp:HyperLink></span>
    <span class="col col3"><asp:Literal ID="LiteralSystemSurveyMyDataCycle2" runat="server">-</asp:Literal></span>
    <span class="col col4">1</span>
    <span class="col col5"><asp:Literal ID="LiteralSystemSurveyTargetDateCycle2" runat="server">-</asp:Literal></span>
    <span class="col col6"><asp:Literal ID="LiteralSystemSurveyCompleteDateCycle2" runat="server">-</asp:Literal></span><br style="clear:both" />
    </li>
          
    <li>
    <span class="col col1"><asp:CheckBox ID="CheckBoxPatientChartRegistrationCycle2" runat="server" Enabled="false"/></span>
    <span class="col col2"><asp:HyperLink ID="HyperLinkPatientChartRegistrationCycle2" runat="server">Chart Abstraction</asp:HyperLink><a href="PatientChartRegistration.aspx"></a></span>
    <span class="col col3"><asp:Literal ID="LiteralPatientChartRegistrationCompletedCycle2" runat="server">-</asp:Literal></span>
    <span class="col col4"><asp:Literal ID="LiteralPatientChartRegistrationTotalCycle2" runat="server">-</asp:Literal></span>
    <span class="col col5"><asp:Literal ID="LiteralChartAbstractionTargetDateCycle2" runat="server">-</asp:Literal></span>
    <span class="col col6"><asp:Literal ID="LiteralChartAbstractionCompleteDateCycle2" runat="server">-</asp:Literal></span><br style="clear:both" />
    </li>
          
    <li class="blue">
    <span class="col col1"><asp:CheckBox ID="CheckBoxReviewPerformanceReportCycle2" runat="server" Enabled="false"/></span>
    <span class="col col2"><asp:HyperLink ID="HyperLinkReviewPerformanceReportCycle2" runat="server">Review Final Practice Report</asp:HyperLink></span>
    <span class="col col3"><asp:Literal ID="LiteralReviewPerformanceReportCountCycle2" runat="server">-</asp:Literal></span>
    <span class="col col4">1</span>
    <span class="col col5"><asp:Literal ID="LiteralReviewPerformanceReportTargetDateCycle2" runat="server">-</asp:Literal></span>
    <span class="col col6"><asp:Literal ID="LiteralReviewPerformanceReportCompleteDateCycle2" runat="server">-</asp:Literal></span><br style="clear:both" />
    </li>
          
    <li class="bottom">
    <span class="col col1"><asp:CheckBox ID="CheckBoxDevelopImpactStatement" runat="server" Enabled="false"/></span>
    <span class="col col2"><asp:HyperLink ID="HyperLinkDevelopImpactStatement" runat="server">Impact Statement</asp:HyperLink></span>
    <span class="col col3"><asp:Literal ID="LiteralDevelopImpactStatementCount" runat="server">-</asp:Literal></span>
    <span class="col col4">1</span>
    <span class="col col5"><asp:Literal ID="LiteralDevelopImpactStatementTargetDate" runat="server">-</asp:Literal></span>
    <span class="col col6"><asp:Literal ID="LiteralDevelopImpactStatementCompleteDate" runat="server">-</asp:Literal></span><br style="clear:both" />
    </li>
          
    </ul>



    </div>
    <div class="row" id="noAnalysisAndImpact" visible="false" runat="server">
    Analysis and Impact steps do not exists for this activity because it was not selected for improvement.
    </div>
                </fieldset>

    </div>
    </td>
    <td valign="top">
    <div class="dashboard_right_section">
    <div class="right_nav">
        <asp:ListView runat="server" ID="ListViewModuleLinks">
        <LayoutTemplate>
            <div class="bginputarrow" id="divcharts" runat="server"><asp:LinkButton ID="ButtonSubmit" runat="server" Text="Chart Abstraction" PostBackUrl="PatientChartRegistration.aspx"/></div>
            <asp:PlaceHolder runat="server" ID="itemPlaceholder"></asp:PlaceHolder>
        </LayoutTemplate>
        <ItemTemplate>
            <div class="bginputarrow"><asp:LinkButton ID="ButtonSubmit" runat="server" Text='<%# Eval("ModuleName")%>' PostBackUrl='<%# "OpenModule.aspx?mid=" +  Eval("ModuleID")%>'></asp:LinkButton></div>
        </ItemTemplate>
        </asp:ListView>            
    </div>
    </div>
          
    </td>
    </tr>
    </table>
          
    </div>
    </div>

</asp:Content>