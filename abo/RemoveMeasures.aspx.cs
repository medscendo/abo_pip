﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using NetHealthPIMModel;
using System.Data.Objects;
using System.Transactions;

public partial class copd_RemoveMeasures : BasePage
{
    bool viewMode;
    static int rowIndex = 1;
    static string MeasureTitle = "";

    protected void Page_Load(object sender, EventArgs e)
    {
        if (!IsPostBack)
        {
            InitializePage();
            int cycleID = ctx.ActiveModuleCycleID;

            if (Request[Constants.QUERYSTRING_CYCLEID] != null)
            {
                cycleID = Int32.Parse(Request[Constants.QUERYSTRING_CYCLEID]);
            }

            using (NetHealthPIMEntities pim = new NetHealthPIMEntities())
            {
                ParticipantModuleCycle cycle = pim.ParticipantModuleCycle.First(c => c.ParticipantModuleCycleID == cycleID);
                ParticipantModuleCycle cycle2;

                if (cycle.CycleNumber == 2)
                {
                    cycle2 = cycle;

                    cycle.PrevParticipantModuleCycleReference.Load();
                    ParticipantModuleCycle prev = cycle.PrevParticipantModuleCycle;
                    cycle = prev;

                    cycleID = cycle.ParticipantModuleCycleID;

                }
                else
                {
                    var meausuresSelected = (from pmr in pim.ParticipantMeasureRationale
                                             where pmr.ParticipantModuleCycleID == cycleID
                                         select pmr).Count();
                    if (meausuresSelected==0)
                        Response.Redirect("SelectMeasures.aspx");

                }

                

                // Determine from the cycle record the phase it corresponds to and overwrite if indicated by the user. 
                bool P2Flag = cycle.Phase2Flag ?? false;
                if (Request[Constants.QUERYSTRING_PHASE] != null)
                {
                    if (Request[Constants.QUERYSTRING_PHASE] == "1")
                    {
                        P2Flag = false;
                    }
                }

                if (P2Flag)
                {
                    // TODO: Similar query for the second phase of the process. Join with the previous record to get goal and past measures. 
                }
                else
                {
                    rowIndex = 1; // reset counter
                    System.Linq.IQueryable report;

                    var isGroupMeasures = false;
                    if (ctx.ParticipantGroupID != 0) //user belong to a group
                        if (ctx.ParticipantID == ctx.LeaderParticipantID)
                            isGroupMeasures = true;
                    if (isGroupMeasures == true)
                    {
                        // Get GROUP measure aggregates 
                        report = from pm in pim.ParticipantGroupMeasure
                                 from ma in pim.MeasureAggregate
                                 from mgm in pim.MeasureGroupModule
                                 from m in pim.Module
                                 where pm.MeasureID == ma.Measure.MeasureID
                                    && pm.ParticipantGroupID == ctx.ParticipantGroupID
                                    && pm.Measure.MeasureType.MeasureTypeID == 1
                                    && pm.IncludeInImprovementPlan == true
                                    && ma.Module.ModuleID == ctx.ActiveModuleID
                                    && ma.MeasureGroup.MeasureGroupID == mgm.MeasureGroupID
                                    && ma.MeasureAggregateType.MeasureAggregateTypeID == 1
                                    && ma.Module.ModuleID == m.ModuleID
                                 orderby mgm.MeasureGroupSortOrder
                                 select new PerformanceReportData
                                 {
                                     MeasureID = pm.MeasureID,
                                     MeasureGroupSortOrder = mgm.MeasureGroupSortOrder,
                                     MeasureTitle = pm.Measure.MeasureTitle,
                                     MeasureClinicalRecommendation = pm.Measure.MeasureClinicalRecommendation,
                                     MeasureLongDescription = pm.Measure.MeasureLongDescription,
                                     AbstractRecordsIncluded = pm.AbstractRecordsIncluded,
                                     MeasurePercentCurrent = pm.MeasurePercent,
                                     MeasureGoal = pm.MeasureGoal,
                                     PeerData = ma.MeasurePercent,
                                     IncludeInImprovementPlan = pm.IncludeInImprovementPlan,
                                     ModuleName = m.ModuleName
                                 };
                    }
                    else
                    {
                        // Get measure aggregates 
                        report = from pm in pim.ParticipantMeasure
                                     from ma in pim.MeasureAggregate
                                     from m in pim.Module
                                    
                                     where pm.MeasureID == ma.Measure.MeasureID
                                        && pm.ParticipantModuleCycle.ParticipantModuleCycleID == cycleID
                                        && pm.IncludeInImprovementPlan == true
                                        && ma.MeasureAggregateType.MeasureAggregateTypeID == 1
                                        && ma.Module.ModuleID == m.ModuleID
                                      orderby pm.MeasureID 
                                     select new PerformanceReportData
                                     {
                                         MeasureID = pm.MeasureID,
                                         MeasureGroupSortOrder = 1,
                                         MeasureTitle = pm.Measure.MeasureTitle,
                                         MeasureQualityIndicator = pm.Measure.MeasureQualityIndicator,
                                         MeasureClinicalRecommendation = pm.Measure.MeasureClinicalRecommendation,
                                         MeasureLongDescription = pm.Measure.MeasureLongDescription,
                                         AbstractRecordsIncluded = pm.AbstractRecordsIncluded == null ? 0 : (int)pm.AbstractRecordsIncluded,
                                         MeasurePercentCurrent = pm.MeasurePercent,
                                         MeasureGoal = pm.MeasureGoal,
                                         PeerData = ma.MeasurePercent,
                                         IsPhase2Flag = pm.ParticipantModuleCycle.Phase2Flag,
                                         IncludeInImprovementPlan = pm.IncludeInImprovementPlan,
                                         ModuleName = m.ModuleName
                                     };
                    }
                    viewMode = cycle.ImprovementPlanComplete;
                    string q = Support.TraceLinqSQL(report);
                    ListViewMeasureGoals.DataSource = report;

                    if (viewMode)
                    {

                        ButtonSaveForLater.Visible = false;
                        PanelInformation.Visible = false;
                        ButtonSubmit.Visible = false;
                    }
                }
            }
        }
    }

    protected void InitializePage()
    {
        ((PIMMasterPage)Page.Master).SetTitle(" <A href=Dashboard.aspx>Status Page Form »</A> Remove Selected Measures");
    }

    protected void GridViewMeasureGoals_RowDataBound(object sender, GridViewRowEventArgs e)
    {
        if (e.Row.RowType == DataControlRowType.DataRow)
        {
            ((Label)e.Row.Cells[0].FindControl("LabelMeasureNumber")).Text = (e.Row.RowIndex + 1).ToString();
            if (viewMode)
            {
                ((TextBox)e.Row.Cells[4].FindControl("TextBoxMeasureGoal")).Visible = false;
                ((Label)e.Row.Cells[4].FindControl("LabelPercentSign")).Visible = false;
                ((Label)e.Row.Cells[4].FindControl("LabelMeasureGoal")).Visible = true;
            }
        }
    }

    protected void ListViewMeasureGoals_OnItemDataBound(object sender, ListViewItemEventArgs e)
    {
        if (e.Item.ItemType == ListViewItemType.DataItem)
        {
            ((Label)e.Item.FindControl("LabelMeasureNumber")).Text = (rowIndex++).ToString();
            HyperLink chartsIncluded = (HyperLink)e.Item.FindControl("LinkAbstractRecordsIncluded");
            if (viewMode)
            {
                ((TextBox)e.Item.FindControl("TextBoxMeasureGoal")).Visible = false;
                ((Label)e.Item.FindControl("LabelPercentSign")).Visible = false;
                ((Label)e.Item.FindControl("LabelMeasureGoal")).Visible = true;
            }
        }
    }
    protected void ListViewAdditionalResourcesDELETE_RowDataBound(object sender, ListViewItemEventArgs e)
    {
        if (e.Item.ItemType == ListViewItemType.DataItem)
        {
           
            if (MeasureTitle != ((HiddenField)e.Item.FindControl("HiddenFieldMeasureTitle")).Value)
            {
                ((System.Web.UI.HtmlControls.HtmlGenericControl)e.Item.FindControl("liMeasureTitle")).Visible = true;
                MeasureTitle = ((HiddenField)e.Item.FindControl("HiddenFieldMeasureTitle")).Value;
            }

            bool CostFlag = Boolean.Parse(((HiddenField)e.Item.FindControl("HiddenFieldCostFlag")).Value);
            bool AAOResourceFlag = Boolean.Parse(((HiddenField)e.Item.FindControl("HiddenFieldAAOResourceFlag")).Value);

            if (CostFlag)
                ((Image)e.Item.FindControl("ImageCost")).Visible= true;
            if (AAOResourceFlag)
                ((Image)e.Item.FindControl("ImageAAO")).Visible = true;
            
            if (viewMode)
            {
                ((CheckBox)e.Item.FindControl("CheckBoxResource")).Enabled = false;
            }
        }
    }

    protected bool validForm()
    {
        return true;
    }

    protected bool isValid()
    {
        return true;
    }

    protected bool isComplete()
    {
        return true;
    }

    

    protected void ButtonSaveForLater_Click(object sender, EventArgs e)
    {
        Response.Redirect("~/" + module.ModuleCode.ToLower() + "/ImprovementPlanPartI.aspx");
    }

    protected void ButtonNext_Click(object sender, EventArgs e)
    {
        Response.Redirect("~/" + module.ModuleCode.ToLower() + "/ImprovementPlanPartII.aspx");
    }

    protected void ButtonSubmit_Click(object sender, EventArgs e)
    {
       
    }

    
    protected void LinkButtonDashboard_Click(object sender, EventArgs e)
    {
        Response.Redirect("Dashboard.aspx");
    }


}
