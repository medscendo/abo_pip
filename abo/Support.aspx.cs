﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using NetHealthPIMModel;

public partial class abo_Support : BasePage
{
    protected void Page_Load(object sender, EventArgs e)
    {
        ((PIMMasterPage)Page.Master).SetStatus(1);
        ((PIMMasterPage)Page.Master).SetHeader("Support");
        ButtonSubmit.PostBackUrl = Request.UrlReferrer.ToString();
        
        string breadCrumb = Constants.BC_SUPPORT;
        ((PIMMasterPage)Page.Master).SetHeader("Support");


        using (NetHealthPIMEntities pim = new NetHealthPIMEntities())
        {
            if (ctx != null)
            {
                ParticipantModuleCycle cycle = pim.ParticipantModuleCycle.First(c => c.ParticipantModuleCycleID == ctx.ActiveModuleCycleID);
                if (cycle.ModuleSelectionComplete != true)
                    ((PIMMasterPage)Page.Master).SetStatus(1);
                else
                    if (cycle.CycleNumber == 1)
                        ((PIMMasterPage)Page.Master).SetStatus(2);
                    else
                        ((PIMMasterPage)Page.Master).SetStatus(3);
            }
        }
    }
}
