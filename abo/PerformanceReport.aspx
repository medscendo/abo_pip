﻿<%@ Page Title="" Language="C#" MasterPageFile="~/PIMMasterPage.master" AutoEventWireup="true" CodeFile="PerformanceReport.aspx.cs" Inherits="copd_PerformanceReport" %>

<%@ Register Assembly="System.Web.DataVisualization, Version=3.5.0.0, Culture=neutral, PublicKeyToken=31bf3856ad364e35"
    Namespace="System.Web.UI.DataVisualization.Charting" TagPrefix="asp" %>

<asp:Content ID="Content1" ContentPlaceHolderID="head" Runat="Server">

<link type="text/css" href="../common/css/atooltip.css" rel="stylesheet"  media="screen" />
    
<style>

    .table {
        margin-bottom:20px;
    }

    tr.diff-color td, tr.module-name td {
        background-color: #0B91B7;
        color: #fff;
        font-weight:bold;
        border-top:none;
        border-bottom:none;
    }

    tr.module-name td {
        color: #000;
        /*text-shadow: 1px 1px 0 #fff, -1px 1px 0 #fff,1px -1px 0 #fff,-1px -1px 0 #fff;*/
        font-size:1.2em;
    }

    .width-qualind {
        width: 30.5%;
    }

    .width-rationale {
        width: 9%;
    }

    .width-pracassess {
        width:38%;
    }

    .width-response {
        width: 15%;
        text-align:center;
    }

    #PM1 {
        cursor:pointer;
    }
    #iframeModal {
        width:100%;
        height:100%;
        border:none;
    }
    #measureRationale {
        padding-right:0;
    }

</style>
  <script src="http://code.jquery.com/ui/1.10.3/jquery-ui.js"></script>
    <script type="text/javascript" language="javascript">
    
        $(document).ready(function() {
         $("#measureRationale").dialog({
                    autoOpen: false,
                    modal: true,
                    height: 350,
                    width: 600
                });
            });
           
           function openwindowj(measureID) {
                $("#iframeModal").removeAttr("src");
                $("#iframeModal").attr("src", "MeasureRationale.aspx?mid=" + measureID);
                $("#measureRationale").dialog("open"); 
           }
    </script>

</asp:Content>

<asp:Content runat="server" ID="Content4" ContentPlaceHolderID="javascript">

    <script type="text/javascript" src="../common/js/jquery.atooltip.js"></script>

    <script type="text/javascript">
        $(function () {
            $('#PM1').aToolTip({
                clickIt: true,
                tipContent: '<b>Feedback Report Legend</b><br><br><b>Self</b> = Self-assessment goals<br><b>My Goal</b> = Target that you choose for your improvement<br><b>Initial Data</b> = Calculated measure compiled from initial chart abstractions<br><b>Final Data</b> = Calculated measure compiled from second chart abstraction'
            });
        });
    </script>

</asp:Content>

<asp:Content ID="Content3" ContentPlaceHolderID="ContentPlaceHolder1" Runat="server">
<div id='measureRationale'><iframe src="" id="iframeModal"></iframe></div>
        
<p>The report below represents the data obtained from your self-assessment, system survey, and patient chart data. 
</p>
<ul>
    <li><strong>Process Measure</strong> - Process measures assess the processes of care used to treat patients and the resulting outcomes</li>
    <li><strong>Outcome Measure</strong> - Outcome measures provide insight into a physician's treatment patterns for the purposes of evaluating efficiency and impact on patient care.</li>
    <li><strong>Data Fields</strong> - A Data Field is an agreed-upon process or outcome measure that is used to determine the level of quality achieved.   A measurable variable (or characteristic) that can be used to determine the degree of adherence to a standard or achievement of quality goals such as the AAO preferred practice patterns (PPPs).</li>
    </ul>

<asp:label ID="PQRSNote" text="If you have chosen to report for PQRS using the data from this program, click <a href='PQRSNextStep.aspx'>here</a>.<br><br>" runat="server" />
          
<table class="table">
    <tr>
        <th colspan="5">Practice Report</th>
    </tr>
    <asp:ListView runat="server" ID="ListViewProcessMeasures" onitemdatabound="ListViewProcessMeasures_OnItemDataBound">
    <LayoutTemplate>
        <tr class="diff-color" id="divRowModuleName">
            <td class="width-qualind"></td>
            <td class="width-qualind">Data Field</td>
            <td class="width-rationale">Charts Reviewed</td>
            <td class="width-pracassess">Data Analysis</td>
        </tr>
        <asp:PlaceHolder runat="server" ID="itemPlaceholder"></asp:PlaceHolder>
    </LayoutTemplate> 
    <ItemTemplate>
        <tr runat="server" id="divRowModuleName" visible="false" class="module-name">
            <td>
                <asp:Literal ID="Literal2" runat="server" Text='<%# Bind("ModuleName") %>'></asp:Literal>
            </td>
            <td></td>
            <td></td>
            <td></td>
        </tr>
        <tr runat="server" id="divRowChart">
            <td>
                <asp:Label ID="LabelMeasureTitle" CssClass="label_highlight" runat="server" Text='<%# Bind("MeasureQualityIndicator") %>'></asp:Label>
                <br /><asp:Literal ID="LiteralModuleName" runat="server" Text='<%# Bind("ModuleName") %>'></asp:Literal>
            </td>
            <td><asp:Label ID="LabelMeasureLongDescription" runat="server" Text='<%# Bind("MeasureLongDescription") %>'></asp:Label></td>
            <td><asp:Label ID="LabelChartConsideredPR" runat="server"></asp:Label></td>
            <td id="CellProcessMeasures" runat="server">
                <asp:Chart ID="ChartPerformance" runat="server" Width="300" Height="80" >
                <Series>                               
                    <asp:Series Name="Initial Data" ChartType="Bar" Color="#2f5199"  BorderColor="#2f5199"  CustomProperties="DrawingStyle=Wedge" Palette="Berry" YValueMembers="MeasurePercentCurrent" IsValueShownAsLabel="true" LabelForeColor="#000000" LabelBackColor="Transparent">
                    </asp:Series>
                    <asp:Series Name="Peer Data" ChartType="Bar" Color="#ffb200" BorderColor="#ffb200" CustomProperties="DrawingStyle=Wedge" Palette="Berry" YValueMembers="PeerData" IsValueShownAsLabel="true"  LabelForeColor="#000000" LabelBackColor="Transparent" >
                    </asp:Series>
                </Series>
                <ChartAreas>
                    <asp:ChartArea Name="MainChartArea">
                        <AxisX Maximum="3" LineColor="Transparent" LineWidth="1">
                            <MajorGrid Enabled="false" />
                            <LabelStyle IsEndLabelVisible="false" />
                            <CustomLabels>
                            </CustomLabels>
                        </AxisX>
                        <AxisY Maximum="100" Interval="20" />
                    </asp:ChartArea>
                </ChartAreas>
            </asp:Chart>   
            </td>
        </tr>
    </ItemTemplate>
    </asp:ListView>
</table>
     <asp:Panel ID="PanelCombinedmes" runat="server">
    <table id="StrataTable" runat="server" class="table">
    <tr>
        <th colspan="5">&nbsp;&nbsp;&nbsp;  </th>
    </tr>
        <tr class="diff-color">
            <td style="width:30%"></td>
             <td style="width:20%">Charts Reviewed</td> 
           <td style="width:50%">Data Analysis</td> 
        </tr>
     
    </table>
       </asp:Panel>                
<table class="table">
    <tr>
        <th colspan="5">Outcome Measures</th>
    </tr>
    <asp:ListView runat="server" ID="ListViewOutcomeMeasures" onitemdatabound="ListViewOutcomeMeasures_OnItemDataBound">
    <LayoutTemplate>
        <tr class="diff-color" id="divRowModuleName">
            <td class="width-qualind"></td>
            <td class="width-qualind">Data Field</td>
            <td class="width-rationale">Charts Reviewed</td>
            <td class="width-pracassess">Data Analysis</td>
        </tr>    
            <asp:PlaceHolder runat="server" ID="itemPlaceholder"></asp:PlaceHolder>
        
    </LayoutTemplate>    
    <ItemTemplate>
        <tr runat="server" id="divRowModuleName" visible="false" class="module-name">
            <td>
                <asp:Literal ID="Literal2" runat="server" Text='<%# Bind("ModuleName") %>'></asp:Literal>
            </td>
            <td></td>
            <td></td>
            <td></td>
             
        </tr>
        <tr runat="server" id="divRowChart">
            <td>
                <asp:Label ID="LabelMeasureTitle" CssClass="label_highlight" runat="server" Text='<%# Bind("MeasureQualityIndicator") %>'></asp:Label>
                <asp:Literal ID="LiteralModuleName" runat="server" Text='<%# Bind("ModuleName") %>'></asp:Literal>
            </td>
            <td><asp:Label ID="LabelMeasureLongDescription" runat="server" Text='<%# Bind("MeasureLongDescription") %>'></asp:Label></td>
            <td><asp:Label ID="LabelChartConsideredUM" runat="server"></asp:Label></td>
            <td id="ChartCell" runat="server">                    
                <asp:Chart ID="ChartPerformance" runat="server" Width="320" Height="120">
                <Series>
                    <asp:Series Name="My Data" ChartType="Bar" Color="#914991" CustomProperties="DrawingStyle=Wedge" Palette="Berry" YValueMembers="MeasurePercent" IsValueShownAsLabel="true" LabelForeColor="#000000" LabelBackColor="Transparent" >
                    </asp:Series>
                </Series>
                <ChartAreas>
                    <asp:ChartArea Name="MainChartArea">
                        <AxisX Maximum="5" LineColor="Transparent" LineWidth="1">
                            <MajorGrid Enabled="false" />
                            <LabelStyle IsEndLabelVisible="false" />
                            <CustomLabels>
                            </CustomLabels>
                        </AxisX>
                        <AxisY Maximum="100" Interval="20" />
                    </asp:ChartArea>
                </ChartAreas>
                </asp:Chart>  
                                  
            </td>

        </tr>
    </ItemTemplate>
</asp:ListView>

</table>
<div id="tablePEC" runat="server" visible="false">
<table class="table">
    <tr>
        <th colspan="5">Patient Experience of Care</th>
    </tr>
    <asp:ListView runat="server" ID="ListViewPEC" onitemdatabound="ListViewPEC_OnItemDataBound">
    <LayoutTemplate>
        <tr class="diff-color" id="divRowModuleName">
            <td class="width-qualind"></td>
            <td class="width-qualind">Data Field</td>
            <td class="width-rationale">Charts Reviewed</td>
            <td class="width-pracassess">Data Analysis</td>
        </tr>    
            <asp:PlaceHolder runat="server" ID="itemPlaceholder"></asp:PlaceHolder>
        
    </LayoutTemplate>    
    <ItemTemplate>
        <tr runat="server" id="divRowChart">
            <td>
                <asp:Label ID="LabelMeasureTitle" CssClass="label_highlight" runat="server" Text='<%# Bind("MeasureQualityIndicator") %>'></asp:Label>
                <asp:Literal ID="LiteralModuleName" runat="server" Text='<%# Bind("ModuleName") %>'></asp:Literal>
            </td>
            <td><asp:Label ID="LabelMeasureLongDescription" runat="server" Text='<%# Bind("MeasureLongDescription") %>'></asp:Label></td>
            <td><asp:Label ID="LabelChartConsideredPEC" runat="server"></asp:Label></td>
            <td>                    
                <asp:Chart ID="ChartPerformance" runat="server" Width="320" Height="80">
                <Series>
                   
                    <asp:Series Name="Initial Data" ChartType="Bar" Color="#2f5199"  BorderColor="#2f5199"  CustomProperties="DrawingStyle=Wedge" Palette="Berry" YValueMembers="MeasurePercentCurrent" IsValueShownAsLabel="true" LabelForeColor="#000000" LabelBackColor="Transparent">
                    </asp:Series>
                    <asp:Series Name="Peer Data" ChartType="Bar" Color="#ffb200" BorderColor="#ffb200" CustomProperties="DrawingStyle=Wedge" Palette="Berry" YValueMembers="PeerData" IsValueShownAsLabel="true"  LabelForeColor="#000000" LabelBackColor="Transparent" >
                    </asp:Series>

                </Series>
                <ChartAreas>
                    <asp:ChartArea Name="MainChartArea">
                        <AxisX Maximum="3" LineColor="Transparent" LineWidth="1">
                            <MajorGrid Enabled="false" />
                            <LabelStyle IsEndLabelVisible="false" />
                            <CustomLabels>
                            </CustomLabels>
                        </AxisX>
                        <AxisY Maximum="100" Interval="20" />
                    </asp:ChartArea>
                </ChartAreas>
            </asp:Chart>   
            </td>
        </tr>
    </ItemTemplate>
</asp:ListView>

</table>
</div>
<div id="Div1" runat="server" visible="true">
<table id="systemtable" runat="server" class="table">
    <tr>
        <th colspan="5">Survey Answer Distribution</th>
    </tr>
        <tr class="diff-color" id="divRowModuleName">
            <td class="width-qualind">Question</td>
            <td class="width-qualind">Your Answer</td>
            <td class="width-pracassess">Distribution</td>
        </tr>  
</table>
</div>

        



<asp:ListView runat="server" ID="ListViewSurvey" Visible="false" onitemdatabound="ListViewSurvey_OnItemDataBound">
    <LayoutTemplate>
        <table class="table">
        <tr>
            <th colspan="3">
                System Survey
            </th>
        </tr>
        <tr class="diff-color">
            <td>Survey Question</td>
            <td>Response</td>
            <td>Feedback</td>
        </tr>
        <asp:PlaceHolder runat="server" ID="itemPlaceholder"></asp:PlaceHolder>
        </table> 
    </LayoutTemplate>    
    <ItemTemplate>
        <div runat="server" id="divRowModuleName" visible="false">
        <tr class="module-name">
            <td>
                <asp:Literal ID="LiteralModuleName" runat="server" Text='<%# Bind("ModuleShortName") %>'></asp:Literal>
            </td>
            <td></td>
            <td></td>
        </tr>
        </div>
        <div runat="server" id="divRowYesNoQuestion" visible="false">
        <tr class="table_body">
            <td  class="width-qualind"><asp:Literal ID="LiteralQuestion" runat="server" Text='<%# Eval("SSMeasureQuestion") %>'></asp:Literal></td>
            <td style="text-align:left" class="width-response"><asp:Literal ID="LiteralAnswer" runat="server" Text='<%# Eval("SSQuestionResponse") %>'></asp:Literal></td>
            <td><asp:Literal ID="LiteralFeedback" runat="server" Text='<%# Eval("Feedback") %>'></asp:Literal></td>
        </tr>
        </div>
        <div runat="server" id="divRowFreeTextQuestion" visible="false">
        <tr class="table_body">
            <td class="width-qualind"><asp:Literal ID="Literal1" runat="server" Text='<%# Eval("SSMeasureQuestion") %>'></asp:Literal></td>
            <td><asp:Literal ID="Literal3" runat="server" Text='<%# Eval("SSQuestionResponse") %>'></asp:Literal></td>
            <td></td>
        </tr>
        </div>
    </ItemTemplate>
</asp:ListView>   
              
<div class="button-box">
    <asp:LinkButton ID="LinkButtonDashboard" runat="server" Text="Return To Status Page"  PostBackUrl="Dashboard.aspx" CssClass="button" />
    <asp:LinkButton ID="LinkSelectMeasures" runat="server" Text="Proceed to Improvement Plan"  OnClick="LinkButtonSelectMeasures_Click" CssClass="button" />
    <asp:LinkButton ID="LinkButtonImpactStatement" runat="server" Text="Proceed to Impact Statement"  OnClick="LinkButtonImpactStatement_Click" Visible="false" CssClass="button" />
</div>

<!--
      <center>
        <asp:Panel ID="PanelSelectMeasuresPhase" runat="server">
        <i>After viewing your detailed report, choose the performance measures that you will address in your Improvement Plan.</i><br />
        <asp:Button ID="ButtonSelectMeasuresPhase1" runat="server" Text="Select Measures for Intervention" /><br /><br />
        </asp:Panel>
        <asp:Panel ID="PanelDevelopImpactStatement" runat="server" Visible="false">
        <i>After viewing your detailed report, Develop and Approve Impact Statement.</i><br />
        <asp:Button ID="ButtonDevelopImpactStatement" runat="server" Text="Develop/Approve Impact Statement" PostBackUrl="~/abo/ImpactStatement.aspx"/><br /><br />
        </asp:Panel>
        <asp:Button ID="LinkButtonMainMenuPhase1" runat="server" PostBackUrl="~/abo/MyPI.aspx" Text="Return to Main Menu"/>
        </center>
        
    </asp:Panel>  
    -->

</asp:Content>

