﻿<%@ Page Title="" Language="C#" MasterPageFile="~/PIMMasterPage.master" AutoEventWireup="true" CodeFile="PICME_Reporting.aspx.cs" Inherits="abo_PICME_Reporting" %>

<asp:Content ID="Content1" ContentPlaceHolderID="head" Runat="Server">

    <style type="text/css">
        table 
        {
            margin: 0 auto;
        }

    </style>
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder1" Runat="Server">
    <!-- module detaile -->
    <div class="pim" style="padding-right: 100px;">
        <div class="heading_box" >
            <h2><strong>PI-CME Reporting</strong></h2>
        </div>
        <table>
        <tr>
            <th style="width:32%;"><p>Report</p></th>
            <th><p>Description</p></th>
        </tr>
        <!-- 1.	User Listing -->
        <tr class="table_body">
            <td>
                <asp:HyperLink ID="HyperLinkUserListing" runat="server" NavigateUrl="http://204.12.55.190:8080/ABO-Reports/jsp/abouserlisting.jsp" Text="User Listing" />
            </td>
            <td>
                Tabular list of users with their  # of charts, status flags and dates
            </td>
        </tr>
        <!-- 2.	Improvement Plan Listing -->
        <tr class="table_body_bg">
            <td>
                <asp:HyperLink ID="HyperLinkImprovementPlanListing" runat="server" NavigateUrl="http://204.12.55.190:8080/ABO-Reports/jsp/aboImprovementPlanListing.jsp" Text="Improvement Plan Listing" />
            </td>
            <td>
                Tabular list of improvement plans with their  date, Important changes (comment), target start, target re-measure, and measures selected
            </td>
        </tr>
        <!-- 3.	Measures Selected for Improvement -->
        <tr class="table_body">
            <td colspan="2"><strong>Measures Selected for Improvement</strong></td>
        </tr>
        <!-- 3a. Improvement Plan Measure Choices -->
        <tr>
            <td>
                <asp:HyperLink ID="HyperLinkMeasureChoicesAll" runat="server" NavigateUrl="http://204.12.55.190:8080/ABO-Reports/jsp/aboImprovementPlanMeasureCounts.jsp?completerflag=all" Text="Improvement Plan Measure Choices - All" />
                <br /><asp:HyperLink ID="HyperLinkMeasureChoicesInProgree" runat="server" NavigateUrl="http://204.12.55.190:8080/ABO-Reports/jsp/aboImprovementPlanMeasureCounts.jsp?completerflag=No" Text="Improvement Plan Measure Choices - In Progress" />
                <br /><asp:HyperLink ID="HyperLinkMeasureChoicesComplete" runat="server" NavigateUrl="http://204.12.55.190:8080/ABO-Reports/jsp/aboImprovementPlanMeasureCounts.jsp?completerflag=Yes" Text="Improvement Plan Measure Choices - Complete" />
            </td>
            <td>
                Tabulated listing of all measures and # of participants who selected it for improvement.  Only those with ImprovementPlanComplete are included.   A parameter is sent to the report to toggle whether the listing is for all participants, Activity Completers only, or in-progress participants.  Bar charts of measures selected will appear.  You can hover over the bar to see what it represents.
            </td>
        </tr>
        <!-- 3b. Number of Measures Chosen -->
        <tr>
            <td>
                <asp:HyperLink ID="HyperLinkMeasureChosen" runat="server" NavigateUrl="http://204.12.55.190:8080/ABO-Reports/jsp/aboImprovementPlanNumMeasures.jsp" Text="Number of Measures Chosen" />
            </td>
            <td>
                Chart showing how many measures were chosen in Improvement Plans.  There will be info for those who have finalized their plan, and info for those who have completed the whole activity.
            </td>
        </tr>
        <!-- 3c. Improvement Plan Rationales for Measures Chosen -->
        <tr>
            <td>
                <asp:HyperLink ID="HyperLinkMeasureRationaleChoicesAll" runat="server" NavigateUrl="http://204.12.55.190:8080/ABO-Reports/jsp/aboImprovementPlanRationaleCounts.jsp?completerflag=All" Text="Improvement Plan Rationales for Measures Chosen - All" />
                <br /><asp:HyperLink ID="HyperLinkMeasureRationaleChoicesInProgree" runat="server" NavigateUrl="http://204.12.55.190:8080/ABO-Reports/jsp/aboImprovementPlanRationaleCounts.jsp?completerflag=No" Text="Improvement Plan Rationales for Measures Chosen - In Progress" />
                <br /><asp:HyperLink ID="HyperLinkMeasureRationaleChoicesComplete" runat="server" NavigateUrl="http://204.12.55.190:8080/ABO-Reports/jsp/aboImprovementPlanRationaleCounts.jsp?completerflag=Yes" Text="Improvement Plan Rationales for Measures Chosen - Complete" />
            </td>
            <td>
                Tabulated listing of chosen measures with the chosen rationales and # of participants who selected it as a reason for the measure.  A parameter is sent to the report to toggle whether it is for Activity Completers only, in-progress only, or all complete plans.  Only those with ImprovementPlanComplete are included.  You can hover over the Measure title to see the measure description.  Bar charts of rationales selected will appear.  You can hover over the bar to see what it represents.
            </td>
        </tr>

        <!-- 4.	Tools Used for Improvement -->
        <tr class="table_body_bg">
            <td colspan="2"><strong>Tools Used for Improvement</strong> (the first 2 are for all participants, the second 2 are for completers only)</td>
        </tr>
        <!-- 4a. Selected Resources by Resource Type -->
        <tr class="table_body_bg">
            <td>
                <asp:HyperLink ID="HyperLinkResourceCountByType" runat="server" NavigateUrl="http://204.12.55.190:8080/ABO-Reports/jsp/aboResourceCountByType.jsp" Text="Selected Resources by Resource Type" />
            </td>
            <td>
                This report presents information about tools being picked at the end of Part 2 (approve the improvement plan) before the improvement plan is executed.  This Tabulated list is of all Resource and Intervention tools selected with their descriptions and how many times they were selected for intent to be used (picked).  This includes both those who have only finalized their plan, and those who have completed the activity.   It is presented grouped by Resource Type to see how often types of resources were chosen across all measures.   Bar charts of tools selected will appear. You can hover over the bar to see what it represents.
            </td>
        </tr>
        <!-- 4b. Selected Resources by Measure -->
        <tr class="table_body_bg">
            <td>
                <asp:HyperLink ID="HyperLinkResourceCountByMeasure" runat="server" NavigateUrl="http://204.12.55.190:8080/ABO-Reports/jsp/aboResourceCountByMeasure.jsp" Text="Selected Resources by Measure" />
            </td>
            <td>
                This report presents information about tools being picked at the end of Part 2 (approve the improvement plan) before the improvement plan is executed.  This Tabulated list is of all Resource and Intervention tools selected with their descriptions and how many times they were selected for intent to be use (picked).  This includes both those who have only finalized their plan, and those who have completed the activity.  It is presented grouped by activity, and the measures within that activity that were selected for improvement with that resource.  It can be used to see which resources were chosen for which measures in which activities.
            </td>
        </tr>
        <!-- 4c. Resources Used by Resource Type -->
        <tr class="table_body_bg">
            <td>
                <asp:HyperLink ID="HyperLinkResourceCompleteByType" runat="server" NavigateUrl="http://204.12.55.190:8080/ABO-Reports/jsp/aboResourceCompleteByType.jsp" Text="Resources Used by Resource Type" />
            </td>
            <td>
                This report represents how the tools were used by the participants who completed Part 4 (improvement plan and second chart abstraction).  This Tabulated list is of all Resource and Intervention tools with their descriptions and how many times they were selected by these participants for intent to use (picked), times they were actually used (used), and how many times it was picked but not used (dropped).   It is presented grouped by Resource Type to see how types of resources were chosen across all measures.  Bar charts of tools selected will appear.  You can hover over the bar to see what it represents.
            </td>
        </tr>
        <!-- 4d. Resources Used by Measure -->
        <tr class="table_body_bg">
            <td>
                <asp:HyperLink ID="HyperLinkResourceCompleteByMeasure" runat="server" NavigateUrl="http://204.12.55.190:8080/ABO-Reports/jsp/aboResourceCompleteByMeasure.jsp" Text="Resources Used by Measure" />
            </td>
            <td>
                This report represents how the tools were used by the participants who completed Part 4 (improvement plan and second chart abstraction).  This Tabulated list is of all Resource and Intervention tools with their descriptions and how many times they were selected by these participants for intent to use (picked), times they were actually used (used), and how many times it was picked but not used (dropped).   It is presented grouped by the measure it is meant to improve, and can be used to see which resources were chosen for which measures.
            </td>
        </tr>
        <!-- 5.	Improvement Achieved -->
        <tr class="table_body">
            <td colspan="2"><strong>Improvement Achieved</strong></td>
        </tr>
        <!-- 5a. Overall Measurement Results -->
        <tr class="table_body">
            <td>
                <asp:HyperLink ID="HyperLinkmeasuretable" runat="server" NavigateUrl="http://204.12.55.190:8080/ABO-Reports/jsp/abomeasuretable.jsp" Text="Overall Measurement Results" />
            </td>
            <td>
                This report lists each measure in each activity with the number of participants, the average performance, and a graph of the distribution of scores.
            </td>
        </tr>
        <!-- 5b. Comparison Reports: Resource Outcome by Measure -->
        <tr class="table_body">
            <td>
                <asp:HyperLink ID="HyperLinkResourceOutcomeByMeasure" runat="server" NavigateUrl="http://204.12.55.190:8080/ABO-Reports/jsp/aboResourceOutcomeByMeasure.jsp" Text="Comparison Reports: Resource Outcome by Measure" />
            </td>
            <td>
                This report lists each Resource used for a Measure, presenting the frequency chosen, the initial measure average (stage 1), the improved measure average (stage 3) and the average change.  It is a way to see the effectiveness of the resources.
            </td>
        </tr>
        <!-- 5c. Impact Statements -->
        <tr class="table_body">
            <td>
                <asp:HyperLink ID="HyperLinkImpactStatementListing" runat="server" NavigateUrl="http://204.12.55.190:8080/ABO-Reports/jsp/aboImpactStatementListing.jsp" Text="Impact Statements" />
            </td>
            <td>
                This report lists each Impact statement with the answers to the questions, such as the barriers and lessons learned.  It also presents the activities that were selected in that cycle.  It includes the participants ID and Name.
            </td>
        </tr>
    </table>
    </div>
    <!-- module detaile ends --> 
</asp:Content>

