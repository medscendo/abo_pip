﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

public partial class abo_FeedbackReceived : System.Web.UI.Page
{
    protected void Page_Load(object sender, EventArgs e)
    {
        ((PIMMasterPage)Page.Master).SetStatus(1);
        ((PIMMasterPage)Page.Master).SetHeader("Feedback Received");
    }
}
