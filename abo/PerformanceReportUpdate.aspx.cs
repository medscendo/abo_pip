﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.HtmlControls;
using System.Web.UI.WebControls;
using NetHealthPIMModel;
using PIM.Repositories;
using System.Web.UI.DataVisualization.Charting;
using System.Drawing;

public partial class abo_PerformanceReportUpdate : BasePage
{

    protected void Page_Load(object sender, EventArgs e)
    {
        string breadCrumb = Constants.BC_DASHBOARD_LINK + "" + Constants.BC_PERFORMANCE_REPORT;


        ((PIMMasterPage)Page.Master).SetTitle(breadCrumb);
        ((PIMMasterPage)Page.Master).SetStatus(2);
        ((PIMMasterPage)Page.Master).SetHeader("Practice Report");
        string action = Request.Params[Constants.QUERYSTRING_ACTION];
        string cycleNumber = Request.QueryString["cycle"];
        if (cycleNumber == "2")
        {
            ((PIMMasterPage)Page.Master).SetHeader("Final Practice Report");
            ((PIMMasterPage)Page.Master).SetStatus(3);
        }


        if (action == Constants.QUERYSTRING_ACTION_VIEW)
        {
            LinkButtonImpactStatement.Visible = false;
        }


        int cycleID = ctx.ActiveModuleCycleID;

        if (Request[Constants.QUERYSTRING_CYCLEID] != null)
        {
            cycleID = Int32.Parse(Request[Constants.QUERYSTRING_CYCLEID]);
        }

        using (NetHealthPIMEntities pim = new NetHealthPIMEntities())
        {
            ParticipantModuleCycle cycle = pim.ParticipantModuleCycle.First(c => c.ParticipantModuleCycleID == cycleID);
            ParticipantModuleCycle cycle2;

            var participantDashboardModules = (from pdm in pim.ParticipantDashboardModules_V
                                               where pdm.ParticipantModuleCycleID == cycle.ParticipantModuleCycleID
                                               && pdm.ChartsCompleted != pdm.TotalCharts
                                               select pdm).Count();

          
            if ((participantDashboardModules > 0) && (cycle.CycleNumber == 1))
            {
                    Response.Redirect("Dashboard.aspx");
            }
            if ((participantDashboardModules > 0) && (cycle.CycleNumber == 2) && (cycleNumber != "1"))
            {
                    Response.Redirect("Dashboard3.aspx");
            }
            if ((cycle.CycleNumber == 2) && (cycleNumber == "1"))
            {
                cycle2 = cycle;

                cycle.PrevParticipantModuleCycleReference.Load();
                ParticipantModuleCycle prev = cycle.PrevParticipantModuleCycle;
                cycle = prev;

                cycleID = cycle.ParticipantModuleCycleID;

            }

            // current cycle equals 2 and not trying to access previous cycle, then change the URL to return to dashboard 3
            if ((cycle.CycleNumber == 2) && (cycleNumber != "1"))
            {
                LinkButtonDashboard.PostBackUrl = "Dashboard3.aspx";
                //check if PEC measure exists?
                var pmPECMissing = (from pm in pim.ParticipantMeasure
                                    join m in pim.Measure on pm.MeasureID equals m.MeasureID
                                    where pm.ParticipantModuleCycleID == ctx.ActiveModuleCycleID
                                    && m.MeasureType.MeasureTypeID == 5
                                    select pm).Count();
                if (pmPECMissing == 0)
                    CycleManager.AddPECMeasuresToParticipant(ctx.ActiveModuleCycleID); // will only add if current cycle uses PEC survey
            }

            var modules = (from m in pim.ParticipantModuleSelection 
                           from v in pim.Module
                           where m.ModuleID==v.ModuleID 
                           && m.ParticipantModuleCycleID == cycleID 
                           select new{v.ModuleVersion, m}).ToList();

            var ParticipantInfo = (from c in pim.Participant where c.ParticipantID == ctx.ParticipantID select c).FirstOrDefault();
            MeasureResourceRepository mr = new MeasureResourceRepository();
            foreach (var _module in modules)
            {
                if(_module.ModuleVersion==1)
                {
                    Module currentModule = pim.Module.First(p => p.ModuleID == _module.m.ModuleID);
                    CycleManager.ComputeCycleMeasures_ABO_EXT(cycleID, currentModule.ComputeCycleMeasuresProcedure);
                }
                else if(_module.ModuleVersion==0)
                {
                        mr.GetParticipantMeasure(cycleID, cycleID, _module.m.ModuleID);
                }

             }
           
               
              
            
            bool P2Flag = cycle.Phase2Flag ?? false;
            int activeCycle = ctx.ActiveModuleCycleID;
            ParticipantModuleCycle currentCycle = pim.ParticipantModuleCycle.First(p => p.ParticipantModuleCycleID == activeCycle);
            bool isPhase2 = currentCycle.Phase2Flag ?? false;
            if (Request[Constants.QUERYSTRING_PHASE] != null)
            {
                if (Request[Constants.QUERYSTRING_PHASE] == "1")
                {
                    P2Flag = false;
                }
                else if (Request[Constants.QUERYSTRING_PHASE] == "2")
                {
                    P2Flag = true;
                }
            }

            int measureAggregateTypeID;

            measureAggregateTypeID = Constants.MEASURE_AGGREGATE_TYPE_PEER;



            if (cycle.CycleNumber == 1)
            {
                var totalChartsCompleted = (from chart in pim.ParticipantChartStatus_V
                                            where chart.ParticipantModuleCycleID == ctx.ActiveModuleCycleID
                                            select chart).Sum(n => n.Completed);
                if (totalChartsCompleted < Constants.TOTAL_NUMBER_OF_CHARTS)
                {
                    LinkSelectMeasures.Visible = false;
                }
            }

            if (cycle.MeasuresSelected)
            {
                LinkSelectMeasures.Visible = false;
            }
            if ((cycle.CycleNumber == 2) && (action != Constants.QUERYSTRING_ACTION_VIEW))
            {

                var chartsCountMissing = (from chart in pim.ParticipantChartStatus_V
                                          where chart.ParticipantModuleCycleID == ctx.ActiveModuleCycleID
                                          && chart.TotalCharts != chart.Completed
                                          select chart).Count();
                if (chartsCountMissing == 0)
                    LinkButtonImpactStatement.Visible = true;
            }
            if (isPhase2)
            {
                LinkSelectMeasures.Visible = false;
                currentCycle.PrevParticipantModuleCycleReference.Load();
                ParticipantModuleCycle prev = currentCycle.PrevParticipantModuleCycle;


            }
            if (Request.QueryString["show"] != null && Request.QueryString["show"] == "sel")
            {
                LinkButtonNext.Visible = true;
                LinkSelectMeasures.Visible = false;
                SelectMeasuresText.Visible = true;
                part2.Visible = true;
            }
            else
            {
                part2.Visible = false;
            }
            var ParticipantSelectedModules = (from c in pim.ParticipantModuleSelection 
                                              from v in pim.Module
                                              where c.ModuleID==v.ModuleID
                                              where c.ParticipantModuleCycleID == cycleID 
                                              select new { c.ModuleID, v.ModuleName}).ToList();


            var SystemSyrveyQuestions = (from c in pim.SystemSurveyQuestion
                                         from v in pim.ParticipantModuleSelection
                                         where c.ModuleID == v.ModuleID && (c.SSMeasureQuestionType == 1 ||
                                         c.SSMeasureQuestionType == 3 ||
                                         c.SSMeasureQuestionType == 14 ||
                                         c.SSMeasureQuestionType==5 ||
                                         c.SSMeasureQuestionType == 4)
                                         && v.ParticipantModuleCycleID == ctx.ActiveModuleCycleID
                                         select c).ToList();

            var SelectedQuestionByUser = (from c in pim.SystemSurvey
                                          where c.ParticipantModuleCycleID == ctx.ActiveModuleCycleID
                                          select c).ToList();

            var SystemSyrveyQuestionChoices = (from c in pim.SystemSurveyQuestionChoices
                                               select c).ToList();

            var SystemSyrveyQuestionChoicesByUser = pim.SystemSyrveyChoicesUserReply.ToList();

            foreach (var item in ParticipantSelectedModules)
            {
                Panel pn = new Panel();
                pn.Attributes.Add("style", "margin-top:20px; box-shadow: 0px 0px 3px 1px #d8d8d3;");
                HtmlTable tbl = new HtmlTable();
                tbl.Attributes.Add("class", "table");
                HtmlTableRow tblr = new HtmlTableRow();
                HtmlTableCell tblcell = new HtmlTableCell("th");
                tblcell.Controls.Add(new LiteralControl(item.ModuleName));
                tblcell.Attributes.Add("colspan", "5");
                tblr.Controls.Add(tblcell);
                tbl.Controls.Add(tblr);
                pn.Controls.Add(tbl);
                var ElucidativeMeasures=  GetElucidativeMeasures(cycleID, measureAggregateTypeID, item.ModuleID, P2Flag);
                if (ElucidativeMeasures.Controls.Count > 1)
                {
                    pn.Controls.Add(ElucidativeMeasures);
                }
                var ElucidativeStratifiedMeasures = GetStratifiedMeasures(cycleID, measureAggregateTypeID, item.ModuleID, P2Flag, 6);
                if (ElucidativeStratifiedMeasures.Controls.Count > 0)
                {
                    pn.Controls.Add(ElucidativeStratifiedMeasures);
                }
                var ProcessMeasures = GetProcessMeasures(cycleID, measureAggregateTypeID, item.ModuleID, P2Flag);
                if (ProcessMeasures.Controls.Count > 1)
                {
                    pn.Controls.Add(ProcessMeasures);
                }
                var ProcessMeasuresStratifiedMeasures = GetStratifiedMeasures(cycleID, measureAggregateTypeID, item.ModuleID, P2Flag, 3);
                if (ProcessMeasuresStratifiedMeasures.Controls.Count > 0)
                {
                    pn.Controls.Add(ProcessMeasuresStratifiedMeasures);
                }
                var OutcomeMeasures = GetOutcomeMeasures(cycleID, measureAggregateTypeID, item.ModuleID, P2Flag);
                if (OutcomeMeasures.Controls.Count > 1)
                {
                    pn.Controls.Add(OutcomeMeasures);
                }
                var OutcomeMeasuresStratifiedMeasures = GetStratifiedMeasures(cycleID, measureAggregateTypeID, item.ModuleID, P2Flag, 4);
                if (OutcomeMeasuresStratifiedMeasures.Controls.Count > 0)
                {
                    pn.Controls.Add(OutcomeMeasuresStratifiedMeasures);
                }
                var SystemSurvey = GetChartOfSurvey(item.ModuleID, SystemSyrveyQuestions, SelectedQuestionByUser, SystemSyrveyQuestionChoices, SystemSyrveyQuestionChoicesByUser);
                if (SystemSurvey.Controls.Count > 1)
                {
                    pn.Controls.Add(SystemSurvey);
                }
                PanelContent.Controls.Add(pn);
            }

        }
    }

    public HtmlTable GetProcessMeasures(int cycleID, int measureAggregateTypeID, int ModuleID, bool P2Flag)
    {

        HtmlTable ProcessMeasuresTable = new HtmlTable();
        ProcessMeasuresTable.Attributes.Add("class", "table");
        HtmlTableRow TopHeaderRow = new HtmlTableRow();
        TopHeaderRow.Attributes.Add("class", "module-name");
        if (Request.QueryString["show"] != null && Request.QueryString["show"]=="sel")
        {
            HtmlTableCell TopHeaderCell0 = new HtmlTableCell();
            TopHeaderCell0.Attributes.Add("style", "width: 15%;");
            TopHeaderCell0.Controls.Add(new LiteralControl("Select Measures"));
            TopHeaderRow.Controls.Add(TopHeaderCell0);
        }
        HtmlTableCell TopHeaderCell1=new HtmlTableCell();
        TopHeaderCell1.Attributes.Add("style", "width: 42.7%;");
        TopHeaderCell1.Controls.Add(new LiteralControl("Process Measures"));
        TopHeaderRow.Controls.Add(TopHeaderCell1);
        HtmlTableCell TopHeaderCell2 = new HtmlTableCell();
        TopHeaderCell2.Controls.Add(new LiteralControl("Charts Reviewed"));
        TopHeaderRow.Controls.Add(TopHeaderCell2);
        HtmlTableCell TopHeaderCell3 = new HtmlTableCell();
        TopHeaderCell3.Controls.Add(new LiteralControl("Data Analysis"));
        TopHeaderRow.Controls.Add(TopHeaderCell3);
        ProcessMeasuresTable.Controls.Add(TopHeaderRow);

        using (NetHealthPIMEntities pim = new NetHealthPIMEntities())
        {
            
            var report = (from pir in pim.ParticipantInitialReport_V
                         join m in pim.Module on pir.ModuleID equals m.ModuleID
                         where pir.ParticipantModuleCycleID == cycleID
                            && pir.MeasureTypeID == 3
                            && pir.MeasureRelated == null
                            && pir.MeasureAggregateTypeID == measureAggregateTypeID
                            && pir.ParticipantMeasureTypeID == 1
                            && pir.ModuleID==ModuleID
                         orderby m.ModuleID, pir.MeasureGroupSortOrder
                         select new PerformanceReportData
                         {
                             ModuleName = m.ModuleName,
                             MeasureID = pir.MeasureID,
                             MeasureMaxAge = pir.MeasureMaxAge,
                             IsnumericMeasure = pir.IsNumericMeasure,
                             MeasureGroupSortOrder = pir.MeasureGroupSortOrder == null ? 0 : (int)pir.MeasureGroupSortOrder,
                             MeasureQualityIndicator = pir.MeasureQualityIndicator,
                             MeasureTitle = pir.MeasureTitle,
                             MeasureClinicalRecommendation = pir.MeasureClinicalRecommendation,
                             MeasureLongDescription = pir.MeasureLongDescription,
                             MeasureComputationMethod = pir.MeasureComputationMethod,
                             AbstractRecordsIncluded = pir.AbstractRecordsIncluded == null ? 0 : (int)pir.AbstractRecordsIncluded,
                             MeasurePercentCurrent = pir.MeasurePercentCurrent,
                             PeerAbstractRecordsIncluded = pir.PeerAbstractRecordsIncluded,
                             Peer25Percentile = pir.Peer25Percentile,
                             Peer75Percentile = pir.Peer75Percentile,
                             VisualAcuityResponse = pir.VisualAcuityResponse,
                             PeerData = pir.PeerData,
                             SelfData = pir.NumericResponse,
                             NumericFullGraph = pir.NumericFullGraph,
                             NumericMeasureName = pir.NumericMeasureName,
                             IsPhase2Flag = P2Flag,
                             IncludeInImprovementPlan = pir.IncludeInImprovementPlan,
                             MayNeedImprovement = pir.MayNeedImprovement
                         }).ToList();


            foreach (var item in report)
            {
                HtmlTableRow row = new HtmlTableRow();
                if (Request.QueryString["show"] != null && Request.QueryString["show"] == "sel")
                {
                    HtmlTableCell CellSelectMes = new HtmlTableCell();
                    CellSelectMes.Attributes.Add("style", "width: 15%;");
                    CheckBox chk = new CheckBox();
                    chk.ID = "Mes" + item.MeasureID;
                    CellSelectMes.Controls.Add(chk);
                    row.Controls.Add(CellSelectMes);
                }
                HtmlTableCell CellMIS = new HtmlTableCell();
                CellMIS.Controls.Add(new LiteralControl(item.MeasureLongDescription));
                row.Controls.Add(CellMIS);
                HtmlTableCell CellCC = new HtmlTableCell();
                string ChartConsideredPR = "";
                if (ctx.PrevParticipantModuleCycleID != null && item.IsPhase2Flag == true)
                {
                    ChartConsideredPR = "<table  style='border:none'><tr style=' border-bottom: thick dotted #ff0000; border-bottom-width: 3px;' ><td  style='border:none' >Final </td><td  style='border:none'>" + item.AbstractRecordsIncluded.ToString() + "</td></tr>";
                }
                else
                {
                    ChartConsideredPR = "<table  style='border:none'><tr style=' border-bottom: thick dotted #ff0000; border-bottom-width: 3px;' ><td  style='border:none' >You </td><td  style='border:none'>" + item.AbstractRecordsIncluded.ToString() + "</td></tr>";
                }
                 ChartConsideredPR += "<tr><td  style='border:none' >Peer </td><td  style='border:none' >" + item.PeerAbstractRecordsIncluded.ToString() + "</td></tr></table>";
                 CellCC.Controls.Add(new LiteralControl(ChartConsideredPR));
                 row.Controls.Add(CellCC);
                 HtmlTableCell CellDA = new HtmlTableCell();


                 Chart ch = new Chart();
                 ch.Width = new System.Web.UI.WebControls.Unit(300, System.Web.UI.WebControls.UnitType.Pixel);
                 ch.Height = new System.Web.UI.WebControls.Unit(80, System.Web.UI.WebControls.UnitType.Pixel);
                 Legend legend = new Legend();
                 ch.Legends.Add(legend);
                 ChartArea chartArea = new ChartArea();
                 chartArea.AxisX.MajorGrid.Enabled = false;
                 chartArea.AxisX.LabelStyle.IsEndLabelVisible = true;
                 ch.Legends[0].Position.Auto = false;
                 ch.ChartAreas.Add(chartArea);
                 Axis yAxis = new Axis(chartArea, AxisName.Y);
                 Axis xAxis = new Axis(chartArea, AxisName.X);
                 ch.ChartAreas[0].AxisX.LabelStyle.TruncatedLabels = false;

                 ch.ChartAreas[0].AxisX.LabelStyle.IntervalOffset = 1;
                 ch.ChartAreas[0].AxisX.Interval = 1;
                 ch.ChartAreas[0].AxisX.IsLabelAutoFit = true;
                 ch.ChartAreas[0].AxisX.LabelStyle.IsEndLabelVisible = true;
                 ch.ChartAreas[0].AxisY.LabelStyle.Format = "{#}";
                 ch.ChartAreas[0].AxisX.LabelStyle.Font = new System.Drawing.Font("Trebuchet MS", 10.25F, System.Drawing.FontStyle.Regular);

                 ch.ChartAreas[0].AxisY.Maximum = 100;

                
                 Series SeriesIN = new Series("Initial item");
                 SeriesIN.ChartType = SeriesChartType.Bar;
                 SeriesIN.Color = System.Drawing.ColorTranslator.FromHtml("#8a2bea");
                 SeriesIN.SetCustomProperty("DrawingStyle", "Wedge");
                 SeriesIN.IsValueShownAsLabel = true;
                 SeriesIN.IsVisibleInLegend = false;
                 SeriesIN.LabelForeColor = System.Drawing.ColorTranslator.FromHtml("#000000");
                 SeriesIN.LabelBackColor = System.Drawing.Color.Transparent;
                 ch.Series.Add(SeriesIN);

                 Series SeriesFN = new Series("Peer item");
                 SeriesFN.ChartType = SeriesChartType.Bar;
                 SeriesFN.Color = System.Drawing.ColorTranslator.FromHtml("#ffb200");
                 SeriesFN.SetCustomProperty("DrawingStyle", "Wedge");
                 SeriesFN.IsValueShownAsLabel = true;
                 SeriesFN.IsVisibleInLegend = false;
                 SeriesFN.LabelForeColor = System.Drawing.ColorTranslator.FromHtml("#000000");
                 SeriesFN.LabelBackColor = System.Drawing.Color.Transparent;
                 ch.Series.Add(SeriesFN);
                 if (item.IsnumericMeasure == null || item.IsnumericMeasure == false)
                 {
                     if ((item.MeasurePercentCurrent < 50))
                     {
                         row.Attributes.Add("class", "red-row");
                     }
                     ch.Series[0].Points.AddY(item.PeerData);
                     ch.Series[0].Points.AddY(item.MeasurePercentCurrent);
                     if (item.SelfData > -1)
                     {
                         ch.Series[0].Points.AddY(item.SelfData);
                         ch.ChartAreas[0].AxisX.CustomLabels.Add(2.5, 3.5, "Self Assess.");
                         ch.ChartAreas[0].AxisX.Maximum = 4.0;
                         ch.Height = 80;
                     }

                     if (item.IsPhase2Flag == true)
                         ch.ChartAreas[0].AxisX.CustomLabels.Add(1.5, 2.5, "Final Data");
                     else
                     {
                         ch.ChartAreas[0].AxisX.CustomLabels.Add(1.5, 2.5, "Your Data");

                     }
                     ch.ChartAreas[0].AxisX.CustomLabels.Add(0.5, 1.5, "Peer Data");
                 }
                 else
                 {
                     if (item.VisualAcuityResponse != null)
                     {
                         ch.ChartAreas[0].AxisX.Maximum = 6.0;
                     }
                     else
                     {
                         ch.ChartAreas[0].AxisX.Maximum = 5.0;
                     }
                     if (item.MeasureMaxAge != null)
                     {
                         ch.ChartAreas[0].AxisY.Maximum = (int)item.MeasureMaxAge;
                         ch.ChartAreas[0].AxisY.Interval = ((int)item.MeasureMaxAge / 5);
                     }
                     else
                     {
                         ch.ChartAreas[0].AxisY.Maximum = 200;
                     }
                     if (item.VisualAcuityResponse != null)
                     {
                         ch.ChartAreas[0].AxisX.Maximum = 4.0;
                     }
                     else
                     {
                         ch.ChartAreas[0].AxisX.Maximum = 3.0;
                     }

                     ch.ChartAreas[0].AxisX.CustomLabels.Add(0.5, 1.5, "Peer " + (item.NumericMeasureName == null ? " Data " : item.NumericMeasureName));
                     ch.Series[0].Points.AddY(Math.Round((double)item.PeerData, 0));
                     if (item.IsPhase2Flag == true)
                     {
                         ch.ChartAreas[0].AxisX.CustomLabels.Add(1.5, 2.5, "Final " + (item.NumericMeasureName == null ? " Data " : item.NumericMeasureName));
                     }
                     else
                     {
                         ch.ChartAreas[0].AxisX.CustomLabels.Add(1.5, 2.5, "Your " + (item.NumericMeasureName == null ? " Data " : item.NumericMeasureName));
                     }
                     ch.Series[0].Points.AddY(Math.Round((double)(item.MeasurePercentCurrent), 0));
                     if (item.VisualAcuityResponse != null)
                     {
                         if (item.VisualAcuityResponse[0].ToString() == "2")
                         {
                             var NumericValue = Convert.ToDouble(item.VisualAcuityResponse.Split('/')[1]);
                             ch.ChartAreas[0].AxisX.CustomLabels.Add(2.5, 3.5, "Self Assess.");
                             ch.Series[0].Points.AddY(Math.Round(NumericValue, 0));
                         }
                     }
                 }
                 ch.Series[0].LabelBackColor = System.Drawing.Color.White;
                 int j = 0;
                 Color[] colors = { System.Drawing.ColorTranslator.FromHtml("#8A2BE2"), System.Drawing.ColorTranslator.FromHtml("#BA55D3"), System.Drawing.ColorTranslator.FromHtml("#4169E1"), System.Drawing.ColorTranslator.FromHtml("#C71585"), System.Drawing.ColorTranslator.FromHtml("#A055A3") };
                 foreach (var cl in ch.Series[0].Points)
                 {
                     if (j < 5)
                     {
                         cl.Color = colors[j];
                     }
                     j++;
                 }
                 CellDA.Controls.Add(ch);
                 row.Controls.Add(CellDA);
                 ProcessMeasuresTable.Controls.Add(row);
            }
        }
        return ProcessMeasuresTable ;
    }


    public HtmlTable GetOutcomeMeasures(int cycleID, int measureAggregateTypeID, int ModuleID, bool P2Flag)
    {
        HtmlTable ProcessMeasuresTable = new HtmlTable();
        ProcessMeasuresTable.Attributes.Add("class", "table");
        HtmlTableRow TopHeaderRow = new HtmlTableRow();
        TopHeaderRow.Attributes.Add("class", "module-name");

        if (Request.QueryString["show"] != null && Request.QueryString["show"] == "sel")
        {
            HtmlTableCell TopHeaderCell0 = new HtmlTableCell();
            TopHeaderCell0.Attributes.Add("style", "width: 15%;");
            TopHeaderCell0.Controls.Add(new LiteralControl("Select Measures"));
            TopHeaderRow.Controls.Add(TopHeaderCell0);
        }
        HtmlTableCell TopHeaderCell1=new HtmlTableCell();
        TopHeaderCell1.Attributes.Add("style", "width: 42.7%;");
        TopHeaderCell1.Controls.Add(new LiteralControl("Outcome Measures"));
        TopHeaderRow.Controls.Add(TopHeaderCell1);
        HtmlTableCell TopHeaderCell2 = new HtmlTableCell();
        TopHeaderCell2.Controls.Add(new LiteralControl("Charts Reviewed"));
        TopHeaderRow.Controls.Add(TopHeaderCell2);
        HtmlTableCell TopHeaderCell3 = new HtmlTableCell();
        TopHeaderCell3.Controls.Add(new LiteralControl("Data Analysis"));
        TopHeaderRow.Controls.Add(TopHeaderCell3);
        ProcessMeasuresTable.Controls.Add(TopHeaderRow);

        using (NetHealthPIMEntities pim = new NetHealthPIMEntities())
        {

            var report= from pir in pim.ParticipantInitialReport_OutcomeMeasures_V
                                        join m in pim.Module on pir.ModuleID equals m.ModuleID
                                        where pir.ParticipantModuleCycleID == cycleID
                                        && pir.MeasureRelated == null
                                        && pir.ModuleID==ModuleID
                                        && (pir.ParticipantMeasureTypeID == 3 || pir.ParticipantMeasureTypeID == 1 || pir.ParticipantMeasureTypeID == 5)
                                        orderby m.ModuleID, pir.MeasureGroupSortOrder
                                        select new PerformanceReportData
                                        {
                                            ModuleName = m.ModuleName,
                                            MeasureID = pir.MeasureID,
                                            IsnumericMeasure = pir.IsNumericMeasure,
                                            MeasureMaxAge = pir.MeasureMaxAge,
                                            MeasureQualityIndicator = pir.MeasureQualityIndicator,
                                            MeasureTitle = pir.MeasureTitle,
                                            MeasureClinicalRecommendation = pir.MeasureClinicalRecommendation,
                                            MeasureLongDescription = pir.MeasureLongDescription,
                                            MeasureComputationMethod = pir.MeasureComputationMethod,
                                            AbstractRecordsIncluded = pir.AbstractRecordsIncluded == null ? 0 : (int)pir.AbstractRecordsIncluded,
                                            MeasurePercent3To6 = pir.MeasurePercent3To6,
                                            PeerData3To6 = pir.PeerData3To6,
                                            MeasurePercent7To12 = pir.MeasurePercent7To12,
                                            PeerData7To12 = pir.PeerData7To12,
                                            VisualAcuityResponse = pir.VisualAcuityResponse,
                                            MeasurePercentNewOnset = pir.MeasurePercentNewOnset,
                                            PeerDataNewOnset = pir.PeerDataNewOnset,
                                            MeasurePercentRecurrence = pir.MeasurePercentRecurrence,
                                            PeerDataRecurrence = pir.PeerDataRecurrence,
                                            SelfData = pir.NumericResponse,
                                            NumericFullGraph = pir.NumericFullGraph,
                                            NumericMeasureName = pir.NumericMeasureName,
                                            PeerAbstractRecordsIncluded = pir.PeerAbstractRecordsIncluded,
                                            Peer25Percentile = pir.Peer25Percentile,
                                            Peer75Percentile = pir.Peer75Percentile,
                                            IsPhase2Flag = P2Flag,
                                            IncludeInImprovementPlan = pir.IncludeInImprovementPlan,
                                            MayNeedImprovement = pir.MayNeedImprovement
                                        };

            foreach (var item in report)
            {
                HtmlTableRow row = new HtmlTableRow();
                if (Request.QueryString["show"] != null && Request.QueryString["show"] == "sel")
                {
                    HtmlTableCell CellSelectMes = new HtmlTableCell();
                    CellSelectMes.Attributes.Add("style", "width: 15%;");
                    CheckBox chk = new CheckBox();
                    chk.ID = "Mes" + item.MeasureID;
                    CellSelectMes.Controls.Add(chk);
                    row.Controls.Add(CellSelectMes);
                }
                HtmlTableCell CellMIS = new HtmlTableCell();
                CellMIS.Controls.Add(new LiteralControl(item.MeasureLongDescription));
                row.Controls.Add(CellMIS);
                HtmlTableCell CellCC = new HtmlTableCell();
                string ChartConsideredUM = "";
                if (ctx.PrevParticipantModuleCycleID != null && item.IsPhase2Flag == true)
                {
                    ChartConsideredUM = "<table  style='border:none'><tr style=' border-bottom: thick dotted #ff0000; border-bottom-width: 3px;' ><td  style='border:none' >Final </td><td  style='border:none'>" + item.AbstractRecordsIncluded.ToString() + "</td></tr>";
                }
                else
                {
                    ChartConsideredUM = "<table  style='border:none'><tr style=' border-bottom: thick dotted #ff0000; border-bottom-width: 3px;' ><td  style='border:none' >You </td><td  style='border:none'>" + item.AbstractRecordsIncluded.ToString() + "</td></tr>";
                }

                ChartConsideredUM += "<tr  style='border:none' ><td  style='border:none' >Peer </td><td  style='border:none'>" + item.PeerAbstractRecordsIncluded.ToString() + "</td></tr></table>";
                CellCC.Controls.Add(new LiteralControl(ChartConsideredUM));
                row.Controls.Add(CellCC);
                HtmlTableCell CellDA = new HtmlTableCell();

                Chart ch = new Chart();
                ch.Width = new System.Web.UI.WebControls.Unit(300, System.Web.UI.WebControls.UnitType.Pixel);
                ch.Height = new System.Web.UI.WebControls.Unit(80, System.Web.UI.WebControls.UnitType.Pixel);
                Legend legend = new Legend();
                ch.Legends.Add(legend);
                ChartArea chartArea = new ChartArea();
                chartArea.AxisX.MajorGrid.Enabled = false;
                chartArea.AxisX.LabelStyle.IsEndLabelVisible = true;
                ch.Legends[0].Position.Auto = false;
                ch.ChartAreas.Add(chartArea);
                Axis yAxis = new Axis(chartArea, AxisName.Y);
                Axis xAxis = new Axis(chartArea, AxisName.X);
                ch.ChartAreas[0].AxisX.LabelStyle.TruncatedLabels = false;

                ch.ChartAreas[0].AxisX.LabelStyle.IntervalOffset = 1;
                ch.ChartAreas[0].AxisX.Interval = 1;
                ch.ChartAreas[0].AxisX.IsLabelAutoFit = true;
                ch.ChartAreas[0].AxisX.LabelStyle.IsEndLabelVisible = true;
                ch.ChartAreas[0].AxisY.LabelStyle.Format = "{#}";
                ch.ChartAreas[0].AxisX.LabelStyle.Font = new System.Drawing.Font("Trebuchet MS", 10.25F, System.Drawing.FontStyle.Regular);

                ch.ChartAreas[0].AxisY.Maximum = 100;


                Series SeriesIN = new Series("Initial item");
                SeriesIN.ChartType = SeriesChartType.Bar;
                SeriesIN.Color = System.Drawing.ColorTranslator.FromHtml("#8a2bea");
                SeriesIN.SetCustomProperty("DrawingStyle", "Wedge");
                SeriesIN.IsValueShownAsLabel = true;
                SeriesIN.IsVisibleInLegend = false;
                SeriesIN.LabelForeColor = System.Drawing.ColorTranslator.FromHtml("#000000");
                SeriesIN.LabelBackColor = System.Drawing.Color.Transparent;
                ch.Series.Add(SeriesIN);

                Series SeriesFN = new Series("Peer item");
                SeriesFN.ChartType = SeriesChartType.Bar;
                SeriesFN.Color = System.Drawing.ColorTranslator.FromHtml("#ffb200");
                SeriesFN.SetCustomProperty("DrawingStyle", "Wedge");
                SeriesFN.IsValueShownAsLabel = true;
                SeriesFN.IsVisibleInLegend = false;
                SeriesFN.LabelForeColor = System.Drawing.ColorTranslator.FromHtml("#000000");
                SeriesFN.LabelBackColor = System.Drawing.Color.Transparent;
                ch.Series.Add(SeriesFN);
                if (item.IsnumericMeasure == null || item.IsnumericMeasure == false)
                {
                    if ((item.MeasurePercentCurrent < 50))
                     {
                         row.Attributes.Add("class", "red-row");
                     }
                        //switch (item.MeasureQualityIndicator)
                        //{
                        //    case "Complications":
                        //        if (((item.MeasurePercent3To6 < 50) && (item.MayNeedImprovement > 0)) || ((item.MeasurePercent7To12 < 50) && (item.MayNeedImprovement > 0)) || ((item.MeasurePercentRecurrence < 50) && (item.MayNeedImprovement > 0)) || ((item.MeasurePercentNewOnset < 50) && (item.MayNeedImprovement > 0)))
                        //            row.Attributes.Add("class", "red-row");
                        //        break;
                        //    case "Visual Acuity Improvement":
                        //        if (((item.MeasurePercent3To6 < 50) && (item.MayNeedImprovement > 0)) || ((item.MeasurePercent7To12 < 50) && (item.MayNeedImprovement > 0)))
                        //            row.Attributes.Add("class", "red-row");
                        //        break;
                        //    case "Continued Treatment":
                        //        if (((item.MeasurePercent3To6 < 50) && (item.MayNeedImprovement > 0)) || ((item.MeasurePercent7To12 < 50) && (item.MayNeedImprovement > 0)))
                        //            row.Attributes.Add("class", "red-row");
                        //        break;
                        //    case "Mean Improvement":
                        //        row.Attributes.Add("class", "initial_inner_row");
                        //        ch.ChartAreas[0].AxisY.Maximum = 50.00;
                        //        ch.ChartAreas[0].AxisY.Interval = 5;
                        //        ch.Height = 115;
                        //        break;
                        //    default:
                        //        if ((item.MeasurePercent3To6 < 50) && (item.MayNeedImprovement > 0))
                        //            row.Attributes.Add("class", "red-row");
                        //        break;

                    //}
                    if (item.PeerData7To12 == -1)
                    {
                        ch.Series[0].Points.AddY(item.PeerData3To6);
                        ch.Series[0].Points.AddY(item.MeasurePercent3To6);
                        ch.ChartAreas[0].AxisX.Maximum = 3.0;
                        ch.Height = 70;
                        if (item.SelfData > -1)
                        {
                            ch.Series[0].Points.AddY(item.SelfData);
                            ch.ChartAreas[0].AxisX.CustomLabels.Add(2.5, 3.5, "Self Assess.");
                            ch.ChartAreas[0].AxisX.Maximum = 4.0;
                            ch.Height = 90;
                        }
                        ch.ChartAreas[0].AxisX.CustomLabels.Add(0.5, 1.5, "Peer Data");
                        if (item.IsPhase2Flag == true)
                            ch.ChartAreas[0].AxisX.CustomLabels.Add(1.5, 2.5, "Final Data");
                        else
                            ch.ChartAreas[0].AxisX.CustomLabels.Add(1.5, 2.5, "Your Data");
                    }
                    else
                    {
                        ch.Series[0].Points.AddY(item.PeerData7To12);
                        ch.Series[0].Points.AddY(item.MeasurePercent7To12);
                        ch.Series[0].Points.AddY(item.PeerData3To6);
                        ch.Series[0].Points.AddY(item.MeasurePercent3To6);

                        if (item.MeasurePercentNewOnset != null)
                        {
                            ch.ChartAreas[0].AxisX.CustomLabels.Add(0.5, 1.5, "Peer Reverse");
                            ch.ChartAreas[0].AxisX.CustomLabels.Add(0.5, 1.5, "My Reverse");
                            ch.ChartAreas[0].AxisX.CustomLabels.Add(2.5, 3.5, "Peer Atropine");
                            ch.ChartAreas[0].AxisX.CustomLabels.Add(1.5, 2.5, "My Atropine");
                            ch.Series[0].Points.AddY(item.PeerDataNewOnset);
                            ch.ChartAreas[0].AxisX.CustomLabels.Add(4.5, 5.5, "Peer NewOnset");
                            ch.Series[0].Points.AddY(item.MeasurePercentNewOnset);
                            ch.ChartAreas[0].AxisX.CustomLabels.Add(2.5, 3.5, "My NewOnset");
                            ch.Series[0].Points.AddY(item.PeerDataRecurrence);
                            ch.ChartAreas[0].AxisX.CustomLabels.Add(6.5, 7.5, "Peer Recurrence");
                            ch.Series[0].Points.AddY(item.MeasurePercentRecurrence);
                            ch.ChartAreas[0].AxisX.CustomLabels.Add(3.5, 4.5, "My Recurrence");
                            ch.Series[0].Points.AddY(item.SelfData);
                            ch.ChartAreas[0].AxisX.CustomLabels.Add(4.5, 5.5, "Self Assess.");
                            ch.ChartAreas[0].AxisX.Maximum = 6.0;
                            ch.Height = 140;
                        }
                        else
                        {
                            if (item.SelfData > -1)
                            {
                                ch.Series[0].Points.AddY(item.SelfData);
                                ch.ChartAreas[0].AxisX.CustomLabels.Add(2.5, 3.5, "Self Assess.");
                                ch.ChartAreas[0].AxisX.Maximum = 4.0;
                                ch.Height = 110;
                            }
                            ch.ChartAreas[0].AxisX.CustomLabels.Add(0.5, 1.5, "Peer 7-12");
                            ch.ChartAreas[0].AxisX.CustomLabels.Add(0.5, 1.5, "My 7-12 Data");
                            ch.ChartAreas[0].AxisX.CustomLabels.Add(2.5, 3.5, "Peer 3-6");
                            ch.ChartAreas[0].AxisX.CustomLabels.Add(1.5, 2.5, "My 3-6 Data");
                        }
                    }
                }
                else
                {
                    if (item.NumericFullGraph == true)
                    {
                        if (item.VisualAcuityResponse != null)
                        {
                            ch.ChartAreas[0].AxisX.Maximum = 6.0;
                        }
                        else
                        {
                            ch.ChartAreas[0].AxisX.Maximum = 5.0;
                        }
                        if (item.MeasureMaxAge != null)
                        {
                            ch.ChartAreas[0].AxisY.Maximum = (int)item.MeasureMaxAge;
                            ch.ChartAreas[0].AxisY.Interval = ((int)item.MeasureMaxAge / 5);
                        }
                        else
                        {
                            ch.ChartAreas[0].AxisY.Maximum = 200;
                        }
                        ch.Series[0].Label = "20/#VALY";
                        ch.Height = 140;
                      
                        if (item.Peer75Percentile != null)
                        {
                            ch.ChartAreas[0].AxisX.CustomLabels.Add(0.5, 1.5, "Peer 75th percentile");
                            ch.Series[0].Points.AddY(Math.Round((double)item.Peer75Percentile, 0));
                        }

                        if (item.PeerData3To6 != null)
                        {
                            ch.ChartAreas[0].AxisX.CustomLabels.Add(1.5, 2.5, "Peer " + (item.NumericMeasureName == null ? "Data" : item.NumericMeasureName));
                            ch.Series[0].Points.AddY(Math.Round((double)item.PeerData3To6, 0));
                        }
                        if (item.Peer25Percentile != null)
                        {
                            ch.ChartAreas[0].AxisX.CustomLabels.Add(2.5, 3.5, "Peer 25th percentile");
                            ch.Series[0].Points.AddY(Math.Round((double)item.Peer25Percentile, 0));
                        }
                        if (item.IsPhase2Flag == true)
                        {
                            ch.ChartAreas[0].AxisX.CustomLabels.Add(3.5, 4.5, "Final" + (item.NumericMeasureName == null ? "Data" : item.NumericMeasureName));
                        }
                        else
                        {
                            ch.ChartAreas[0].AxisX.CustomLabels.Add(3.5, 4.5, "Your " + (item.NumericMeasureName == null ? "Data" : item.NumericMeasureName));
                        }
                        ch.Series[0].Points.AddY(Math.Round((double)(item.MeasurePercent3To6), 0));
                        if (item.VisualAcuityResponse != null)
                        {
                            if (item.VisualAcuityResponse[0].ToString() == "2")
                            {
                                var NumericValue = Convert.ToDouble(item.VisualAcuityResponse.Split('/')[1]);
                                ch.ChartAreas[0].AxisX.CustomLabels.Add(4.5, 5.5, "Self Assess.");
                                ch.Series[0].Points.AddY(Math.Round(NumericValue, 0));
                            }
                        }
                    }
                    else
                    {
                        if (item.MeasureMaxAge != null)
                        {
                            ch.ChartAreas[0].AxisY.Maximum = (int)item.MeasureMaxAge;
                            ch.ChartAreas[0].AxisY.Interval = ((int)item.MeasureMaxAge / 5);
                        }
                        else
                        {
                            ch.ChartAreas[0].AxisY.Maximum = 200;
                        }
                        if (item.VisualAcuityResponse != null)
                        {
                            ch.ChartAreas[0].AxisX.Maximum = 4.0;
                        }
                        else
                        {
                            ch.ChartAreas[0].AxisX.Maximum = 3.0;
                        }

                        if (item.PeerData3To6 != null)
                        {
                            ch.ChartAreas[0].AxisX.CustomLabels.Add(0.5, 1.5, "Peer " + (item.NumericMeasureName == null ? "Data" : item.NumericMeasureName));
                            ch.Series[0].Points.AddY(Math.Round((double)item.PeerData3To6, 0));
                        }
                        if (item.IsPhase2Flag == true)
                        {
                            ch.ChartAreas[0].AxisX.CustomLabels.Add(1.5, 2.5, "Final " + (item.NumericMeasureName == null ? "Data" : item.NumericMeasureName));
                        }
                        else
                        {
                            ch.ChartAreas[0].AxisX.CustomLabels.Add(1.5, 2.5, "Your " + (item.NumericMeasureName == null ? "Data" : item.NumericMeasureName));
                        }
                        ch.Series[0].Points.AddY(Math.Round((double)(item.MeasurePercent3To6), 0));
                        if (item.VisualAcuityResponse != null)
                        {
                            if (item.VisualAcuityResponse[0].ToString() == "2")
                            {
                                var NumericValue = Convert.ToDouble(item.VisualAcuityResponse.Split('/')[1]);
                                ch.ChartAreas[0].AxisX.CustomLabels.Add(2.5, 3.5, "Self Assessment Data");
                                ch.Series[0].Points.AddY(Math.Round(NumericValue, 0));
                            }
                        }
                    }
                }
                ch.Series[0].LabelBackColor = System.Drawing.Color.White;
                int j = 0;
                Color[] colors=  {System.Drawing.ColorTranslator.FromHtml("#8A2BE2"), System.Drawing.ColorTranslator.FromHtml("#BA55D3"), System.Drawing.ColorTranslator.FromHtml("#4169E1"), System.Drawing.ColorTranslator.FromHtml("#C71585"), System.Drawing.ColorTranslator.FromHtml("#A055A3")};
                foreach (var cl in ch.Series[0].Points)
                {
                    if (j < 5)
                    {
                        cl.Color = colors[j];
                        
                    }
                    j++;
                }
                CellDA.Controls.Add(ch);
                row.Controls.Add(CellDA);
                ProcessMeasuresTable.Controls.Add(row);
            }
        }
        return ProcessMeasuresTable;
    }

    public HtmlTable GetElucidativeMeasures(int cycleID, int measureAggregateTypeID, int ModuleID, bool P2Flag)
    {

        HtmlTable ElucidativeMeasuresTable = new HtmlTable();
        ElucidativeMeasuresTable.Attributes.Add("class", "table");
        HtmlTableRow TopHeaderRow = new HtmlTableRow();
        TopHeaderRow.Attributes.Add("class", "module-name");
        if (Request.QueryString["show"] != null && Request.QueryString["show"] == "sel")
        {
            HtmlTableCell TopHeaderCell0 = new HtmlTableCell();
            TopHeaderCell0.Attributes.Add("style", "width: 15%;");
            TopHeaderCell0.Controls.Add(new LiteralControl("Select Measures"));
            TopHeaderRow.Controls.Add(TopHeaderCell0);
        }
        HtmlTableCell TopHeaderCell1 = new HtmlTableCell();
        TopHeaderCell1.Attributes.Add("style", "width: 42.7%;");
        TopHeaderCell1.Controls.Add(new LiteralControl("Informational Measures"));
        TopHeaderRow.Controls.Add(TopHeaderCell1);
        HtmlTableCell TopHeaderCell2 = new HtmlTableCell();
        TopHeaderCell2.Controls.Add(new LiteralControl("Charts Reviewed"));
        TopHeaderRow.Controls.Add(TopHeaderCell2);
        HtmlTableCell TopHeaderCell3 = new HtmlTableCell();
        TopHeaderCell3.Controls.Add(new LiteralControl("Data Analysis"));
        TopHeaderRow.Controls.Add(TopHeaderCell3);
        ElucidativeMeasuresTable.Controls.Add(TopHeaderRow);

        using (NetHealthPIMEntities pim = new NetHealthPIMEntities())
        {
            var report = (from pir in pim.ParticipantInitialReport_V
                          join m in pim.Module on pir.ModuleID equals m.ModuleID
                          where pir.ParticipantModuleCycleID == cycleID
                             && pir.MeasureTypeID == 6
                             && pir.MeasureRelated == null
                             && pir.MeasureAggregateTypeID == measureAggregateTypeID
                             && pir.ParticipantMeasureTypeID == 1
                             && pir.ModuleID == ModuleID
                          orderby m.ModuleID, pir.MeasureGroupSortOrder
                          select new PerformanceReportData
                          {
                              ModuleName = m.ModuleName,
                              MeasureID = pir.MeasureID,
                              IsnumericMeasure = pir.IsNumericMeasure,
                              MeasureMaxAge=pir.MeasureMaxAge,
                              MeasureGroupSortOrder = pir.MeasureGroupSortOrder == null ? 0 : (int)pir.MeasureGroupSortOrder,
                              MeasureQualityIndicator = pir.MeasureQualityIndicator,
                              MeasureTitle = pir.MeasureTitle,
                              MeasureClinicalRecommendation = pir.MeasureClinicalRecommendation,
                              MeasureLongDescription = pir.MeasureLongDescription,
                              MeasureComputationMethod = pir.MeasureComputationMethod,
                              AbstractRecordsIncluded = pir.AbstractRecordsIncluded == null ? 0 : (int)pir.AbstractRecordsIncluded,
                              MeasurePercentCurrent = pir.MeasurePercentCurrent,
                              PeerAbstractRecordsIncluded = pir.PeerAbstractRecordsIncluded,
                              Peer25Percentile = pir.Peer25Percentile,
                              Peer75Percentile = pir.Peer75Percentile,
                              VisualAcuityResponse = pir.VisualAcuityResponse,
                              PeerData = pir.PeerData,
                              SelfData = pir.NumericResponse,
                              NumericFullGraph = pir.NumericFullGraph,
                              NumericMeasureName = pir.NumericMeasureName,
                              IsPhase2Flag = P2Flag,
                              IncludeInImprovementPlan = pir.IncludeInImprovementPlan,
                              MayNeedImprovement = pir.MayNeedImprovement
                          }).ToList();

            foreach (var item in report)
            {
                HtmlTableRow row = new HtmlTableRow();
                if (Request.QueryString["show"] != null && Request.QueryString["show"] == "sel")
                {
                    HtmlTableCell CellSelectMes = new HtmlTableCell();
                    CellSelectMes.Attributes.Add("style", "width: 15%;");
                    row.Controls.Add(CellSelectMes);
                }
                HtmlTableCell CellMIS = new HtmlTableCell();
                CellMIS.Controls.Add(new LiteralControl(item.MeasureLongDescription));
                row.Controls.Add(CellMIS);
                HtmlTableCell CellCC = new HtmlTableCell();
                string ChartConsideredPR = "";
                if (ctx.PrevParticipantModuleCycleID != null && item.IsPhase2Flag == true)
                {
                    ChartConsideredPR = "<table  style='border:none'><tr style=' border-bottom: thick dotted #ff0000; border-bottom-width: 3px;' ><td  style='border:none' >Final </td><td  style='border:none'>" + item.AbstractRecordsIncluded.ToString() + "</td></tr>";
                }
                else
                {
                    ChartConsideredPR = "<table  style='border:none'><tr style=' border-bottom: thick dotted #ff0000; border-bottom-width: 3px;' ><td  style='border:none' >You </td><td  style='border:none'>" + item.AbstractRecordsIncluded.ToString() + "</td></tr>";
                }
                ChartConsideredPR += "<tr><td  style='border:none' >Peer </td><td  style='border:none' >" + item.PeerAbstractRecordsIncluded.ToString() + "</td></tr></table>";
                CellCC.Controls.Add(new LiteralControl(ChartConsideredPR));
                row.Controls.Add(CellCC);
                HtmlTableCell CellDA = new HtmlTableCell();

                Chart ch = new Chart();
                ch.Width = new System.Web.UI.WebControls.Unit(300, System.Web.UI.WebControls.UnitType.Pixel);
                ch.Height = new System.Web.UI.WebControls.Unit(80, System.Web.UI.WebControls.UnitType.Pixel);
                Legend legend = new Legend();
                ch.Legends.Add(legend);
                ChartArea chartArea = new ChartArea();
                chartArea.AxisX.MajorGrid.Enabled = false;
                chartArea.AxisX.LabelStyle.IsEndLabelVisible = true;
                ch.Legends[0].Position.Auto = false;
                ch.ChartAreas.Add(chartArea);
                Axis yAxis = new Axis(chartArea, AxisName.Y);
                Axis xAxis = new Axis(chartArea, AxisName.X);
                ch.ChartAreas[0].AxisX.LabelStyle.TruncatedLabels = false;

                ch.ChartAreas[0].AxisX.LabelStyle.IntervalOffset = 1;
                ch.ChartAreas[0].AxisX.Interval = 1;
                ch.ChartAreas[0].AxisX.IsLabelAutoFit = true;
                ch.ChartAreas[0].AxisX.LabelStyle.IsEndLabelVisible = true;
                ch.ChartAreas[0].AxisY.LabelStyle.Format = "{#}";
                ch.ChartAreas[0].AxisX.LabelStyle.Font = new System.Drawing.Font("Trebuchet MS", 10.25F, System.Drawing.FontStyle.Regular);

                ch.ChartAreas[0].AxisY.Maximum = 100;

                Series SeriesIN = new Series("Initial item");
                SeriesIN.ChartType = SeriesChartType.Bar;
                SeriesIN.Color = System.Drawing.ColorTranslator.FromHtml("#8a2bea");
                SeriesIN.SetCustomProperty("DrawingStyle", "Wedge");
                SeriesIN.IsValueShownAsLabel = true;
                SeriesIN.IsVisibleInLegend = false;
                SeriesIN.LabelForeColor = System.Drawing.ColorTranslator.FromHtml("#000000");
                SeriesIN.LabelBackColor = System.Drawing.Color.Transparent;
                ch.Series.Add(SeriesIN);

                Series SeriesFN = new Series("Peer item");
                SeriesFN.ChartType = SeriesChartType.Bar;
                SeriesFN.Color = System.Drawing.ColorTranslator.FromHtml("#ffb200");
                SeriesFN.SetCustomProperty("DrawingStyle", "Wedge");
                SeriesFN.IsValueShownAsLabel = true;
                SeriesFN.IsVisibleInLegend = false;
                SeriesFN.LabelForeColor = System.Drawing.ColorTranslator.FromHtml("#000000");
                SeriesFN.LabelBackColor = System.Drawing.Color.Transparent;
                ch.Series.Add(SeriesFN);
                if (item.IsnumericMeasure == null || item.IsnumericMeasure == false)
                {
                    ch.Series[0].Points.AddY(item.PeerData);
                    ch.Series[0].Points.AddY(item.MeasurePercentCurrent);
                    if (item.SelfData > -1)
                    {
                        ch.Series[0].Points.AddY(item.SelfData);
                        ch.ChartAreas[0].AxisX.CustomLabels.Add(2.5, 3.5, "Self Assess.");
                        ch.ChartAreas[0].AxisX.Maximum = 4.0;
                        ch.Height = 80;
                    }

                    if (item.IsPhase2Flag == true)
                        ch.ChartAreas[0].AxisX.CustomLabels.Add(1.5, 2.5, "Final Data");
                    else
                    {
                        ch.ChartAreas[0].AxisX.CustomLabels.Add(1.5, 2.5, "Your Data");

                    }
                    ch.ChartAreas[0].AxisX.CustomLabels.Add(0.5, 1.5, "Peer Data");
                }
                else
                {
                    if (item.NumericFullGraph == true)
                    {
                        if (item.VisualAcuityResponse != null)
                        {
                            ch.ChartAreas[0].AxisX.Maximum = 6.0;
                        }
                        else
                        {
                            ch.ChartAreas[0].AxisX.Maximum = 5.0;
                        }
                        if (item.MeasureMaxAge != null)
                        {
                            ch.ChartAreas[0].AxisY.Maximum = (int)item.MeasureMaxAge;
                            ch.ChartAreas[0].AxisY.Interval = ((int)item.MeasureMaxAge/5);
                        }
                        else
                        {
                            ch.ChartAreas[0].AxisY.Maximum = 200;
                        }
                        ch.Series[0].Label = "20/#VALY";
                        ch.Height = 140;
                      

                        if (item.Peer75Percentile != null)
                        {
                            ch.ChartAreas[0].AxisX.CustomLabels.Add(0.5, 1.5, "Peer 75th percentile");
                            ch.Series[0].Points.AddY(Math.Round((double)item.Peer75Percentile, 0));
                        }

                        if (item.PeerData != null)
                        {
                            ch.ChartAreas[0].AxisX.CustomLabels.Add(1.5, 2.5, "Peer " + (item.NumericMeasureName == null ? "Data" : item.NumericMeasureName));
                            ch.Series[0].Points.AddY(Math.Round((double)item.PeerData, 0));
                        }
                        if (item.Peer25Percentile != null)
                        {
                            ch.ChartAreas[0].AxisX.CustomLabels.Add(2.5, 3.5, "Peer 25th percentile");
                            ch.Series[0].Points.AddY(Math.Round((double)item.Peer25Percentile, 0));
                        }
                        if (item.IsPhase2Flag == true)
                        {
                            ch.ChartAreas[0].AxisX.CustomLabels.Add(3.5, 4.5, "Final" + (item.NumericMeasureName == null ? "Data" : item.NumericMeasureName));
                        }
                        else
                        {
                            ch.ChartAreas[0].AxisX.CustomLabels.Add(3.5, 4.5, "Your " + (item.NumericMeasureName == null ? "Data" : item.NumericMeasureName));
                        }
                        ch.Series[0].Points.AddY(Math.Round((double)(item.MeasurePercentCurrent), 0));
                        if (item.VisualAcuityResponse != null)
                        {
                            if (item.VisualAcuityResponse[0].ToString() == "2")
                            {
                                var NumericValue = Convert.ToDouble(item.VisualAcuityResponse.Split('/')[1]);
                                ch.ChartAreas[0].AxisX.CustomLabels.Add(4.5, 5.5, "Self Assess.");
                                ch.Series[0].Points.AddY(Math.Round(NumericValue, 0));
                            }
                        }
                    }
                    else
                    {
                        if (item.MeasureMaxAge != null)
                        {
                            ch.ChartAreas[0].AxisY.Maximum =(int)item.MeasureMaxAge;
                            ch.ChartAreas[0].AxisY.Interval = ((int)item.MeasureMaxAge / 5);
                        }
                        else
                        {
                            ch.ChartAreas[0].AxisY.Maximum = 200;
                        }
                        if (item.VisualAcuityResponse != null)
                        {
                            ch.ChartAreas[0].AxisX.Maximum = 4.0;
                        }
                        else
                        {
                            ch.ChartAreas[0].AxisX.Maximum = 3.0;
                        }

                        if (item.PeerData != null)
                        {
                            ch.ChartAreas[0].AxisX.CustomLabels.Add(0.5, 1.5, "Peer " + (item.NumericMeasureName == null ? "Data" : item.NumericMeasureName));
                            ch.Series[0].Points.AddY(Math.Round((double)item.PeerData, 0));
                        }
                        if (item.IsPhase2Flag == true)
                        {
                            ch.ChartAreas[0].AxisX.CustomLabels.Add(1.5, 2.5, "Final " + (item.NumericMeasureName == null ? "Data" : item.NumericMeasureName));
                        }
                        else
                        {
                            ch.ChartAreas[0].AxisX.CustomLabels.Add(1.5, 2.5, "Your " + (item.NumericMeasureName == null ? "Data" : item.NumericMeasureName));
                        }
                        ch.Series[0].Points.AddY(Math.Round((double)(item.MeasurePercentCurrent), 0));
                        if (item.VisualAcuityResponse != null)
                        {
                            if (item.VisualAcuityResponse[0].ToString() == "2")
                            {
                                var NumericValue = Convert.ToDouble(item.VisualAcuityResponse.Split('/')[1]);
                                ch.ChartAreas[0].AxisX.CustomLabels.Add(2.5, 3.5, "Self Assessment Data");
                                ch.Series[0].Points.AddY(Math.Round(NumericValue, 0));
                            }
                        }
                    }
                }
                ch.Series[0].LabelBackColor = System.Drawing.Color.White;
                int j = 0;
                Color[] colors = { System.Drawing.ColorTranslator.FromHtml("#8A2BE2"), System.Drawing.ColorTranslator.FromHtml("#BA55D3"), System.Drawing.ColorTranslator.FromHtml("#4169E1"), System.Drawing.ColorTranslator.FromHtml("#C71585"), System.Drawing.ColorTranslator.FromHtml("#A055A3") };
                foreach (var cl in ch.Series[0].Points)
                {
                    if (j < 5)
                    {
                        cl.Color = colors[j];
                    }
                    j++;
                }
                CellDA.Controls.Add(ch);
                row.Controls.Add(CellDA);
                ElucidativeMeasuresTable.Controls.Add(row);
            }
        }
        return ElucidativeMeasuresTable;
    }

    public Dictionary<string, Chart> RelatedMeasuresChart(int ParticipantModuleCycleID, bool? Phase2Flag, int ModuleID, int MeasureTypeID)
    {

        Dictionary<string, Chart> ModuleCharts = new Dictionary<string, Chart>();
        double InitialConsidered = 0;
        double PeerConsidered = 0;

        string ChartTable = "";
        using (NetHealthPIMEntities pim = new NetHealthPIMEntities())
        {

            var report = from pir in pim.ParticipantInitialReport_V
                         join m in pim.Module on pir.ModuleID equals m.ModuleID
                         where pir.ParticipantModuleCycleID == ParticipantModuleCycleID
                         && pir.ModuleID==ModuleID
                         && pir.MeasureTypeID==MeasureTypeID
                         && pir.MeasureRelated != null
                         orderby m.ModuleID, pir.MeasureGroupSortOrder descending
                         select new PerformanceReportData
                         {
                             ModuleName = m.ModuleName,
                             MeasureID = pir.MeasureID,
                             MeasureLongDescription = pir.MeasureLongDescription,
                             AbstractRecordsIncluded = (int)pir.AbstractRecordsIncluded,
                             MeasurePercentCurrent = pir.MeasurePercentCurrent,
                             PeerAbstractRecordsIncluded = pir.PeerAbstractRecordsIncluded,
                             Peer25Percentile = pir.Peer25Percentile,
                             Peer75Percentile = pir.Peer75Percentile,
                             PeerData = pir.PeerData,
                             MeasureRelated = pir.MeasureRelated,
                             SelfData = pir.NumericResponse,
                             IsPhase2Flag = Phase2Flag,
                             IncludeInImprovementPlan = pir.IncludeInImprovementPlan,
                             MayNeedImprovement = pir.MayNeedImprovement
                         };
            if (report.Count() > 0)
            {
                InitialConsidered = Math.Round(report.Average(c =>(double)c.AbstractRecordsIncluded), 0);
                PeerConsidered = Math.Round(report.Average(c => (double)c.PeerAbstractRecordsIncluded), 0);
                IQueryable<PerformanceReportData> ReportInitial = null;
                if ((Request.QueryString["cycle"] != null && Request.QueryString["cycle"].ToString() == "2") && ctx.PrevParticipantModuleCycleID != null)
                {
                    ReportInitial = from pir in pim.ParticipantInitialReport_V
                                    join m in pim.Module on pir.ModuleID equals m.ModuleID
                                    where pir.ParticipantModuleCycleID == ctx.PrevParticipantModuleCycleID
                                    && pir.MeasureRelated != null
                                    && pir.MeasureTypeID == MeasureTypeID
                                    && pir.ModuleID==ModuleID
                                    orderby m.ModuleID, pir.MeasureGroupSortOrder
                                    select new PerformanceReportData
                                    {
                                        ModuleName = m.ModuleName,
                                        MeasureID = pir.MeasureID,
                                        MeasureLongDescription = pir.MeasureLongDescription,
                                        AbstractRecordsIncluded = (int)pir.AbstractRecordsIncluded,
                                        MeasurePercentCurrent = pir.MeasurePercentCurrent,
                                        PeerAbstractRecordsIncluded = pir.PeerAbstractRecordsIncluded,
                                        Peer25Percentile = pir.Peer25Percentile,
                                        Peer75Percentile = pir.Peer75Percentile,
                                        PeerData = pir.PeerData,
                                        MeasureRelated = pir.MeasureRelated,
                                        SelfData = pir.NumericResponse,
                                        IsPhase2Flag = Phase2Flag,
                                        IncludeInImprovementPlan = pir.IncludeInImprovementPlan,
                                        MayNeedImprovement = pir.MayNeedImprovement
                                    };


                }

                foreach (var module in report.Select(c => c.ModuleName).Distinct())
                {

                    foreach (var item in report.Where(c => c.ModuleName == module).Select(c => c.MeasureRelated).Distinct())
                    {
                        string Legend = "<table>";
                        Chart chart = new Chart();
                        Series InitailSeries;
                        Legend += "<tr>";
                        if (ReportInitial != null)
                        {
                            InitailSeries = new Series("Final Data");
                            Legend += "<td style='border:none'>Final Data</td>";
                        }
                        else
                        {
                            InitailSeries = new Series("Your Data");
                            Legend += "<td style='border:none'>Your Data</td>";
                        }

                        InitailSeries.ChartType = SeriesChartType.Bar;
                        InitailSeries.Color = System.Drawing.ColorTranslator.FromHtml("#ba55d3");
                        Legend += "<td style='border:none'><div style='width:20px; height:20px; background-color:#ba55d3'></div></td></tr>";
                        InitailSeries.SetCustomProperty("DrawingStyle", "Wedge");
                        InitailSeries.IsValueShownAsLabel = true;
                        InitailSeries.IsVisibleInLegend = false;
                        InitailSeries.LabelForeColor = System.Drawing.ColorTranslator.FromHtml("#000000");
                        InitailSeries.LabelBackColor = System.Drawing.Color.White;
                        chart.Series.Add(InitailSeries);
                        if (ReportInitial != null)
                        {
                            Series PastInitialSeries = new Series("Your Data");
                            Legend += "<tr><td style='border:none'>Your Data</td>";
                            PastInitialSeries.ChartType = SeriesChartType.Bar;
                            PastInitialSeries.Color = System.Drawing.ColorTranslator.FromHtml("#c71585");
                            Legend += "<td style='border:none'><div style='height:20px; width:20px; background-color:#c71585'><div></td></tr>";
                            PastInitialSeries.SetCustomProperty("DrawingStyle", "Wedge");
                            PastInitialSeries.IsValueShownAsLabel = true;
                            PastInitialSeries.IsVisibleInLegend = false;
                            PastInitialSeries.LabelForeColor = System.Drawing.ColorTranslator.FromHtml("#000000");
                            PastInitialSeries.LabelBackColor = System.Drawing.Color.White;
                            chart.Series.Add(PastInitialSeries);

                        }
                        Series PeerSeries = new Series("Peer Data");
                        Legend += "<tr><td style='border:none'>Peer Data</td>";
                        PeerSeries.ChartType = SeriesChartType.Bar;
                        PeerSeries.Color = System.Drawing.ColorTranslator.FromHtml("#8a2bea");
                        Legend += "<td  style='border:none' ><div style='height:20px; width:20px; background-color:#8a2bea'><div></td></tr>";
                        PeerSeries.SetCustomProperty("DrawingStyle", "Wedge");
                        PeerSeries.IsValueShownAsLabel = true;
                        PeerSeries.IsVisibleInLegend = false;
                        PeerSeries.LabelForeColor = System.Drawing.ColorTranslator.FromHtml("#000000");
                        PeerSeries.LabelBackColor = System.Drawing.Color.White;
                        chart.Series.Add(PeerSeries);
                        Legend += "</table>";
                        Legend legend = new Legend();
                        chart.Legends.Add(legend);
                        ChartArea chartArea = new ChartArea();
                        chartArea.AxisX.MajorGrid.Enabled = false;
                        chartArea.AxisX.LabelStyle.IsEndLabelVisible = false;
                        chart.Legends[0].Position.Auto = false;
                        chart.Legends[0].Position = new ElementPosition(30, 5, 150, 20);
                        chart.ChartAreas.Add(chartArea);
                        Axis yAxis = new Axis(chartArea, AxisName.Y);
                        yAxis.Maximum = 100;
                        yAxis.Interval = 20;
                        Axis xAxis = new Axis(chartArea, AxisName.X);
                        chart.ChartAreas[0].AxisX.LabelStyle.Interval = 1;
                        chart.ChartAreas[0].AxisX.LabelStyle.TruncatedLabels = true;
                        chart.ChartAreas[0].AxisY.LabelStyle.Format = "{#}%";
                        chart.ChartAreas[0].AxisX.LabelStyle.Font
            = new System.Drawing.Font("Trebuchet MS", 10.25F, System.Drawing.FontStyle.Regular);
                        chart.ChartAreas[0].AxisY.Maximum = 100;

                        var DataText = report.Where(c => c.ModuleName == module && c.MeasureRelated == item).Select(c => c.MeasureLongDescription).ToArray();
                        var InitalData = report.Where(c => c.ModuleName == module && c.MeasureRelated == item).Select(c => c.MeasurePercentCurrent).ToArray();
                        var PeerData = report.Where(c => c.ModuleName == module && c.MeasureRelated == item).Select(c => c.PeerData).ToArray();
                        if (ReportInitial != null)
                        {
                            var DataPastInitial = ReportInitial.Where(c => c.ModuleName == module && c.MeasureRelated == item).Select(c => c.MeasurePercentCurrent).ToArray();
                            chart.Series["Final Data"].Points.DataBindXY(DataText, InitalData);
                            chart.Series["Your Data"].Points.DataBindXY(DataText, DataPastInitial);
                            chart.Series["Peer Data"].Points.DataBindXY(DataText, PeerData);
                            chart.Series["Final Data"]["PixelPointWidth"] = "40";
                            chart.Series["Your Data"]["PixelPointWidth"] = "40";
                            chart.Series["Peer Data"]["PixelPointWidth"] = "40";
                        }
                        else
                        {
                            chart.Series["Your Data"].Points.DataBindXY(DataText, InitalData);
                            chart.Series["Peer Data"].Points.DataBindXY(DataText, PeerData);
                            chart.Series["Your Data"]["PixelPointWidth"] = "40";
                            chart.Series["Peer Data"]["PixelPointWidth"] = "40";
                        }
                        if (DataText.Count() > 10)
                        {
                            chart.Width = new System.Web.UI.WebControls.Unit(500, System.Web.UI.WebControls.UnitType.Pixel);
                            chart.Height = new System.Web.UI.WebControls.Unit(700, System.Web.UI.WebControls.UnitType.Pixel);
                        }
                        else if (DataText.Count() > 5)
                        {
                            chart.Width = new System.Web.UI.WebControls.Unit(500, System.Web.UI.WebControls.UnitType.Pixel);
                            chart.Height = new System.Web.UI.WebControls.Unit(DataText.Count() * 100, System.Web.UI.WebControls.UnitType.Pixel);
                        }
                        else if (DataText.Count() > 3)
                        {
                            chart.Width = new System.Web.UI.WebControls.Unit(500, System.Web.UI.WebControls.UnitType.Pixel);
                            chart.Height = new System.Web.UI.WebControls.Unit(DataText.Count() * 100, System.Web.UI.WebControls.UnitType.Pixel);
                        }
                        else
                        {
                            chart.Width = new System.Web.UI.WebControls.Unit(500, System.Web.UI.WebControls.UnitType.Pixel);
                            chart.Height = new System.Web.UI.WebControls.Unit(DataText.Count() * 90, System.Web.UI.WebControls.UnitType.Pixel);

                        }

                        if (ReportInitial == null)
                        {
                            ChartTable = "<table  style='border:none'><tr style=' border-bottom: thick dotted #ff0000; border-bottom-width: 3px;' ><td  style='border:none' >You </td><td  style='border:none'>" + InitialConsidered + "</td></tr>";
                            ChartTable += "<tr  style='border:none' ><td  style='border:none' >Peer </td><td  style='border:none'>" + PeerConsidered + "</td></tr></table><br /><br /><br />" + Legend;

                        }
                        else
                        {

                            ChartTable = "<table  style='border:none'><tr style=' border-bottom: thick dotted #ff0000; border-bottom-width: 3px;' ><td  style='border:none' >Final </td><td  style='border:none'>" + InitialConsidered + "</td></tr>";
                            ChartTable += "<tr  style='border:none' ><td  style='border:none' >Peer </td><td  style='border:none'>" + PeerConsidered + "</td></tr></table><br /><br /><br />" + Legend;

                        }
                        ModuleCharts.Add(item + "@" + module + "@" + DataText.Count() + "@" + ChartTable, chart);
                    }
                }
            }
            return ModuleCharts;
        }
    }


    public HtmlTable GetStratifiedMeasures(int cycleID, int measureAggregateTypeID, int ModuleID, bool P2Flag, int TypeID)
    {

            HtmlTable StratifiedMeasuresTable = new HtmlTable();
            StratifiedMeasuresTable.Attributes.Add("class", "table");
            var relatedm = RelatedMeasuresChart(cycleID, P2Flag, ModuleID, TypeID);
            foreach (var item in relatedm)
            {
                    HtmlTableRow rowname = new HtmlTableRow();
                    string ConsideredTable = item.Key.Split('@')[3];
                    string ModuleName = item.Key.Split('@')[1];
                    string RelatedMeasureID=item.Key.Split('@')[0];
                    HtmlTableRow row = new HtmlTableRow();
                    if (Request.QueryString["show"] != null && Request.QueryString["show"] == "sel")
                    {
                        HtmlTableCell CellSelectMes = new HtmlTableCell();
                        CellSelectMes.Attributes.Add("style", "width: 31%;");
                        if (TypeID != 6)
                        {
                            CheckBox chk = new CheckBox();
                            chk.ID = "Mes" + RelatedMeasureID + "Mod" + ModuleID;
                            CellSelectMes.Controls.Add(chk);
                        }
                        row.Controls.Add(CellSelectMes);
                    }

                    HtmlTableCell cell2 = new HtmlTableCell();
                    cell2.Attributes.Add("style", "width: 50%;");
                    cell2.Controls.Add(new LiteralControl("<div>" + ConsideredTable + "</div>"));
                    row.Controls.Add(cell2);

                    HtmlTableCell cell3 = new HtmlTableCell();
                    cell3.Controls.Add(item.Value);
                    row.Controls.Add(cell3);
                    StratifiedMeasuresTable.Controls.Add(row);
               
            }
            return StratifiedMeasuresTable;
    }

    public HtmlTable GetChartOfSurvey(int ModuleID, List<SystemSurveyQuestion> SystemSyrveyQuestions, List<SystemSurvey> AllSelectedQuestionByUser, List<SystemSurveyQuestionChoices> AllSystemSyrveyQuestionChoices, List<SystemSyrveyChoicesUserReply> AllSystemSyrveyChoices)
    {
        HtmlTable ChartOfSurveyTable = new HtmlTable();
        ChartOfSurveyTable.Attributes.Add("class", "table");
        HtmlTableRow TopHeaderRow = new HtmlTableRow();
        TopHeaderRow.Attributes.Add("class", "module-name");
        HtmlTableCell TopHeaderCell1 = new HtmlTableCell();
        TopHeaderCell1.Controls.Add(new LiteralControl("System Survey Questions"));
        if (Request.QueryString["show"] != null && Request.QueryString["show"] == "sel")
        {
            TopHeaderCell1.Attributes.Add("style", "width: 53.5%;");
        }
        else
        {
            TopHeaderCell1.Attributes.Add("style", "width: 42.7%;");
        }
        TopHeaderRow.Controls.Add(TopHeaderCell1);
        HtmlTableCell TopHeaderCell2 = new HtmlTableCell();
        TopHeaderCell2.Controls.Add(new LiteralControl("Your Answer"));
        TopHeaderRow.Controls.Add(TopHeaderCell2);
        HtmlTableCell TopHeaderCell3 = new HtmlTableCell();
        TopHeaderCell3.Controls.Add(new LiteralControl("Peer Answer Distribution*"));
        TopHeaderCell3.Attributes.Add("class", "PeerDistribution");
        if (Request.QueryString["show"] != null && Request.QueryString["show"] == "sel")
        {
            TopHeaderCell3.Attributes.Add("style", "width: 35%; cursor:pointer;");
        }
        else
        {
            TopHeaderCell3.Attributes.Add("style", "width: 38%; cursor:pointer;");
        }
        TopHeaderRow.Controls.Add(TopHeaderCell3);
        ChartOfSurveyTable.Controls.Add(TopHeaderRow);

        using (NetHealthPIMEntities pim = new NetHealthPIMEntities())
        {

            foreach (var item in SystemSyrveyQuestions.Where(c => c.ModuleID ==ModuleID))
                {
                    HtmlTableRow row = new HtmlTableRow();
                    HtmlTableCell cellQuestion = new HtmlTableCell();
                    cellQuestion.Controls.Add(new LiteralControl(item.SSMeasureQuestion));
                    HtmlTableCell cellYourAnswer = new HtmlTableCell();

                    Chart chart = new Chart();
                    Legend legend = new Legend();
                    chart.Legends.Add(legend);
                    //TitleCollection title = "PunctalTitle";
                    //chart.Titles.Add(title);
                    ChartArea chartArea = new ChartArea();
                    chartArea.AxisX.MajorGrid.Enabled = false;
                    chartArea.AxisX.LabelStyle.IsEndLabelVisible = true;
                    chart.Legends[0].Position.Auto = false;
                    chart.ChartAreas.Add(chartArea);
                    Axis yAxis = new Axis(chartArea, AxisName.Y);
                    Axis xAxis = new Axis(chartArea, AxisName.X);
                    chart.ChartAreas[0].AxisX.LabelStyle.TruncatedLabels = false;

                    chart.ChartAreas[0].AxisX.LabelStyle.IntervalOffset = 1;
                    chart.ChartAreas[0].AxisX.Interval = 1;
                    chart.ChartAreas[0].AxisX.IsLabelAutoFit = true;
                    chart.ChartAreas[0].AxisX.LabelStyle.IsEndLabelVisible = true;
                    chart.ChartAreas[0].AxisY.LabelStyle.Format = "{#}";
                    chart.ChartAreas[0].AxisX.LabelStyle.Font = new System.Drawing.Font("Trebuchet MS", 10.25F, System.Drawing.FontStyle.Regular);
                    Series Series = new Series();
                    Series.ChartType = SeriesChartType.Bar;
                    Series.Color = System.Drawing.ColorTranslator.FromHtml("#8a2bea");
                    Series.SetCustomProperty("DrawingStyle", "Wedge");
                    Series.IsValueShownAsLabel = true;
                    Series.IsVisibleInLegend = false;
                    Series.LabelForeColor = System.Drawing.ColorTranslator.FromHtml("#000000");
                    Series.LabelBackColor = System.Drawing.Color.White;
                    chart.Series.Add(Series);


                    HtmlTableCell cellGraphAnswer = new HtmlTableCell();
                    if (item.SSMeasureQuestionType == 1)
                    {

                        var SelectedQuestion = (from c in AllSelectedQuestionByUser
                                                where c.SSQuestionID == item.SSQuestionID
                                                select c.SSQuestionTrueFalse).FirstOrDefault();
                        cellYourAnswer.Controls.Add(new LiteralControl(SelectedQuestion == true ? "Yes" : "No"));

                        var SlectedQuestionYES = (from c in pim.SystemSurvey
                                                  where c.SSQuestionID == item.SSQuestionID
                                                  && c.SSQuestionTrueFalse == true
                                                  select c.SSQuestionTrueFalse).Count();
                        var SlectedQuestionNO = (from c in pim.SystemSurvey
                                                 where c.SSQuestionID == item.SSQuestionID
                                                 && c.SSQuestionTrueFalse == false
                                                 select c.SSQuestionTrueFalse).Count();
                        List<GroupResult> GroupAnswers = new List<GroupResult>();
                        GroupAnswers.Add(new GroupResult { Name = "No", Total = SlectedQuestionNO });
                        GroupAnswers.Add(new GroupResult { Name = "Yes", Total = SlectedQuestionYES });
                        foreach (var ch in GroupAnswers)
                        {
                            chart.Series[0].Points.AddXY(ch.Name.ToString(), ch.Total);
                        }
                        chart.Height = new System.Web.UI.WebControls.Unit(GroupAnswers.Count() * 40, System.Web.UI.WebControls.UnitType.Pixel);
                        chart.Width = new System.Web.UI.WebControls.Unit(300, System.Web.UI.WebControls.UnitType.Pixel);

                        cellGraphAnswer.Controls.Add(chart);
                    }
                    else if (item.SSMeasureQuestionType == 3 || item.SSMeasureQuestionType == 14)
                    {

                        var SelectedQuestion = (from c in AllSelectedQuestionByUser
                                                from v in AllSystemSyrveyQuestionChoices
                                                where c.SSQuestionID == item.SSQuestionID
                                                && v.ChoiceID == c.SSQuestionChoiceID
                                                select v.Choice).FirstOrDefault();
                        cellYourAnswer.Controls.Add(new LiteralControl(SelectedQuestion.ToString()));

                        var GroupAnswers = pim.ExecuteStoreQuery<GroupResult>(@"SELECT COUNT(SystemSurvey.SSQuestionChoiceID) AS Total, SystemSurveyQuestionChoices.Choice AS Name
                          FROM SystemSurveyQuestionChoices 
                          LEFT OUTER JOIN SystemSurvey ON SystemSurveyQuestionChoices.ChoiceID=SystemSurvey.SSQuestionChoiceID
                          WHERE SystemSurveyQuestionChoices.QuestionID=" + item.SSQuestionID + " GROUP BY SystemSurveyQuestionChoices.Choice").ToList();

                        foreach (var ch in GroupAnswers)
                        {
                            chart.Series[0].Points.AddXY(ch.Name.ToString(), ch.Total);
                        }

                        if (GroupAnswers.Count() < 5)
                        {
                            chart.Height = new System.Web.UI.WebControls.Unit(GroupAnswers.Count() * 40, System.Web.UI.WebControls.UnitType.Pixel);
                            chart.Width = new System.Web.UI.WebControls.Unit(300, System.Web.UI.WebControls.UnitType.Pixel);
                        }
                        else if (GroupAnswers.Count() < 10)
                        {
                            chart.Height = new System.Web.UI.WebControls.Unit(GroupAnswers.Count() * 35, System.Web.UI.WebControls.UnitType.Pixel);
                            chart.Width = new System.Web.UI.WebControls.Unit(300, System.Web.UI.WebControls.UnitType.Pixel);
                        }
                        else if (GroupAnswers.Count() < 15)
                        {
                            chart.Height = new System.Web.UI.WebControls.Unit(GroupAnswers.Count() * 25, System.Web.UI.WebControls.UnitType.Pixel);
                            chart.Width = new System.Web.UI.WebControls.Unit(300, System.Web.UI.WebControls.UnitType.Pixel);
                        }
                        cellGraphAnswer.Controls.Add(chart);
                    }
                    else if (item.SSMeasureQuestionType == 4)
                    {
                        var systemSurveyQuestions = AllSystemSyrveyQuestionChoices.Where(c => c.SystemSurveyQuestion != null).ToList();
                        var SelectedQuestion = "";
                        SelectedQuestion = string.Join(", ", (from c in AllSystemSyrveyChoices
                                               from v in systemSurveyQuestions
                                               where c.ChoiceID == v.ChoiceID &&
                                               c.ParticipantModuleCycleID == ctx.ActiveModuleCycleID
                                               && v.SystemSurveyQuestion.SSQuestionID == item.SSQuestionID
                                               select v.Choice).ToList());
                        cellYourAnswer.Controls.Add(new LiteralControl(SelectedQuestion.ToString()));

                        if (item.SSQuestionID == 244)
                        {
                            chart.Titles.Add("PunctalTitle");
                            chart.Titles["Title1"].Text = "Punctal Occlusion Methods";
                        }

                        var GroupAnswers = pim.ExecuteStoreQuery<GroupResult>(@"SELECT COUNT(SystemSyrveyChoicesUserReply.QuestionChoicesUserReplyID) AS Total, SystemSurveyQuestionChoices.Choice AS Name
                          FROM SystemSurveyQuestionChoices 
                          LEFT OUTER JOIN SystemSyrveyChoicesUserReply ON SystemSurveyQuestionChoices.ChoiceID=SystemSyrveyChoicesUserReply.ChoiceID
                          WHERE SystemSurveyQuestionChoices.QuestionID=" + item.SSQuestionID + " GROUP BY SystemSurveyQuestionChoices.Choice").ToList();

                        foreach (var ch in GroupAnswers)
                        {
                            chart.Series[0].Points.AddXY(ch.Name.ToString(), ch.Total);
                        }
                        if (GroupAnswers.Count() < 5)
                        {
                            chart.Height = new System.Web.UI.WebControls.Unit(GroupAnswers.Count() * 40, System.Web.UI.WebControls.UnitType.Pixel);
                            chart.Width = new System.Web.UI.WebControls.Unit(300, System.Web.UI.WebControls.UnitType.Pixel);
                        }
                        else if (GroupAnswers.Count() < 10)
                        {
                            chart.Height = new System.Web.UI.WebControls.Unit(GroupAnswers.Count() * 35, System.Web.UI.WebControls.UnitType.Pixel);
                            chart.Width = new System.Web.UI.WebControls.Unit(300, System.Web.UI.WebControls.UnitType.Pixel);
                        }
                        else if (GroupAnswers.Count() < 15)
                        {
                            chart.Height = new System.Web.UI.WebControls.Unit(GroupAnswers.Count() * 25, System.Web.UI.WebControls.UnitType.Pixel);
                            chart.Width = new System.Web.UI.WebControls.Unit(300, System.Web.UI.WebControls.UnitType.Pixel);
                        }
                        cellGraphAnswer.Controls.Add(chart);

                    }
                    else if (item.SSMeasureQuestionType == 5)
                    {
                        var SelectedQuestion = (from c in AllSelectedQuestionByUser
                                                where c.SSQuestionID == item.SSQuestionID
                                                select c.SSQuestionNumerResponse).FirstOrDefault();
                        cellYourAnswer.Controls.Add(new LiteralControl(SelectedQuestion.ToString()));
                        var GroupAnswers = pim.ExecuteStoreQuery<GroupResult>(@"SELECT t.range as Name, count(*) as Total
                                                                                FROM (
                                                                                  SELECT CASE  
                                                                                    WHEN SSQuestionNumerResponse between 0 and 20 THEN ' 0- 20'
                                                                                    WHEN SSQuestionNumerResponse between 21 and 40 THEN '21-40'
                                                                                    WHEN SSQuestionNumerResponse between 41 and 60 THEN '41-60'
                                                                                    WHEN SSQuestionNumerResponse between 61 and 80 THEN '61-80'
                                                                                    WHEN SSQuestionNumerResponse between 81 and 100 THEN '81-100'
                                                                                    else 'Above 100' end as range
                                                                                  FROM SystemSurvey
                                                                                WHERE SSQuestionID="+item.SSQuestionID+") t GROUP BY t.range").ToList();
                        GroupAnswers.Reverse();
                        foreach (var ch in GroupAnswers)
                        {

                            chart.Series[0].Points.AddXY(ch.Name.ToString(), ch.Total);
                        }
                        if (GroupAnswers.Count() < 5)
                        {
                            chart.Height = new System.Web.UI.WebControls.Unit(GroupAnswers.Count() * 40, System.Web.UI.WebControls.UnitType.Pixel);
                            chart.Width = new System.Web.UI.WebControls.Unit(300, System.Web.UI.WebControls.UnitType.Pixel);
                        }
                        else if (GroupAnswers.Count() < 10)
                        {
                            chart.Height = new System.Web.UI.WebControls.Unit(GroupAnswers.Count() * 35, System.Web.UI.WebControls.UnitType.Pixel);
                            chart.Width = new System.Web.UI.WebControls.Unit(300, System.Web.UI.WebControls.UnitType.Pixel);
                        }
                        else if (GroupAnswers.Count() < 15)
                        {
                            chart.Height = new System.Web.UI.WebControls.Unit(GroupAnswers.Count() * 25, System.Web.UI.WebControls.UnitType.Pixel);
                            chart.Width = new System.Web.UI.WebControls.Unit(300, System.Web.UI.WebControls.UnitType.Pixel);
                        }
                        cellGraphAnswer.Controls.Add(chart);


                    }
                    row.Controls.Add(cellQuestion);
                    row.Controls.Add(cellYourAnswer);
                    row.Controls.Add(cellGraphAnswer);
                    ChartOfSurveyTable.Controls.Add(row);
                
            }

        }

        return ChartOfSurveyTable;
    }

    public class GroupResult
    {
        public string Name { get; set; }
        public int Total { get; set; }

    }




    protected void LinkButtonSelectMeasures_Click(object sender, EventArgs e)
    {
        using (NetHealthPIMEntities pim = new NetHealthPIMEntities())
        {
            ParticipantModuleCycle cycle = pim.ParticipantModuleCycle.First(c => c.ParticipantModuleCycleID == ctx.ActiveModuleCycleID);
            cycle.AbstractDataComplete = true;
            cycle.AbstractDataCompletedDate = DateTime.Now;
            cycle.AbstractDataLastUpdatedDate = DateTime.Now;
            cycle.ReportApproved = true;
            cycle.ReportApprovedCompletedDate = DateTime.Now;
            cycle.ReportApprovedLastUpdatedDate = DateTime.Now;
            if (cycle.ModuleSelectionComplete != true)
            {
                cycle.ModuleSelectionComplete = true;
                cycle.ModuleSelectionCompletedDate = DateTime.Today;
            }
            pim.SaveChanges();
        }
        Response.Redirect("~/" + module.ModuleCode.ToLower() + "/PerformanceReportUpdate.aspx?cycle=1&show=sel");
    }



    protected void LinkButtonImpactStatement_Click(object sender, EventArgs e)
    {
        using (NetHealthPIMEntities pim = new NetHealthPIMEntities())
        {
            ParticipantModuleCycle cycle = pim.ParticipantModuleCycle.First(c => c.ParticipantModuleCycleID == ctx.ActiveModuleCycleID);
            cycle.AbstractDataComplete = true;
            cycle.AbstractDataCompletedDate = DateTime.Now;
            cycle.AbstractDataLastUpdatedDate = DateTime.Now;
            cycle.ReportApproved = true;
            cycle.ReportApprovedCompletedDate = DateTime.Now;
            cycle.ReportApprovedLastUpdatedDate = DateTime.Now;
            pim.SaveChanges();
        }
        Response.Redirect("~/" + module.ModuleCode.ToLower() + "/ImpactStatement.aspx");
    }
    protected void LinkButton1_Click(object sender, EventArgs e)
    {
        using (NetHealthPIMEntities pim = new NetHealthPIMEntities())
        {


            Dictionary<int, string> OutcomeMeasures = new Dictionary<int, string>();
            Dictionary<int, string> ProcessMeasures = new Dictionary<int, string>();
            Dictionary<int, string> StratifiedMeasures = new Dictionary<int, string>();


            var ParticipantSelectedModules = (from c in pim.ParticipantModuleSelection
                                              from v in pim.Module
                                              where c.ModuleID == v.ModuleID
                                              where c.ParticipantModuleCycleID == ctx.ActiveModuleCycleID
                                              select new { c.ModuleID, v.ModuleName }).ToList();




            foreach (var item in ParticipantSelectedModules)
            {
                var ProcessMeasuresList = (from pir in pim.ParticipantInitialReport_V
                                join m in pim.Module on pir.ModuleID equals m.ModuleID
                                where pir.ParticipantModuleCycleID == ctx.ActiveModuleCycleID
                                   && pir.MeasureTypeID == 3
                                   && pir.MeasureRelated == null
                                   && pir.ParticipantMeasureTypeID == 1
                                   && pir.ModuleID == item.ModuleID
                                orderby m.ModuleID, pir.MeasureGroupSortOrder
                                select new { pir.MeasureID, pir.MeasureTitle }).ToList();
                foreach (var prc in ProcessMeasuresList)
                {
                    CheckBox chk  =(CheckBox)PanelContent.FindControl("Mes" + prc.MeasureID);
                    if(chk!=null && chk.Checked==true)
                    {
                        ProcessMeasures.Add(prc.MeasureID, prc.MeasureTitle);
                    }
                }

                var OutcomeMeasuresList = (from pir in pim.ParticipantInitialReport_OutcomeMeasures_V
                                           join m in pim.Module on pir.ModuleID equals m.ModuleID
                                           where pir.ParticipantModuleCycleID == ctx.ActiveModuleCycleID
                                           && pir.MeasureRelated == null
                                           && pir.ModuleID ==  item.ModuleID
                                           && (pir.ParticipantMeasureTypeID == 3 || pir.ParticipantMeasureTypeID == 1 || pir.ParticipantMeasureTypeID == 5)
                                           orderby m.ModuleID, pir.MeasureGroupSortOrder
                                           select new { pir.MeasureID, pir.MeasureTitle }).ToList();
                foreach (var prc in OutcomeMeasuresList)
                {
                    CheckBox chk = (CheckBox)PanelContent.FindControl("Mes" + prc.MeasureID);
                    if (chk!=null && chk.Checked == true)
                    {
                        OutcomeMeasures.Add(prc.MeasureID, prc.MeasureTitle);
                    }
                }

                var StratifiedMeasuresForModule = (from c in pim.Measure 
                                                   where c.Module.ModuleID == item.ModuleID 
                                                   && c.MeasureRelated != null 
                                                   && c.MeasureType.MeasureTypeID!=6
                                                   select c.MeasureRelated).Distinct().ToList();
                foreach (var str in StratifiedMeasuresForModule)
                {
                    CheckBox chk = (CheckBox)PanelContent.FindControl("Mes" + str + "Mod" + item.ModuleID);
                    if (chk!=null && chk.Checked == true)
                    {
                        var AllRelatedMeasures = (from v in pim.Measure
                                                  where v.MeasureRelated == str && v.Module.ModuleID == item.ModuleID
                                                  select new { v.MeasureTitle, v.MeasureID });
                        foreach (var rel in AllRelatedMeasures)
                        {
                            StratifiedMeasures.Add(rel.MeasureID, rel.MeasureTitle);
                        }
                    }
                }

            }

            if ((ProcessMeasures.Count() + OutcomeMeasures.Count() + StratifiedMeasures.Count()) < 1) // used to be 3
            {
                MessageBox.Show("Please Select at least 1 measure");

            }
            //else if (OutcomeMeasures.Count() == 0)
            //{
            //    MessageBox.Show("You need to select at least one Outcome Measure");
            //}
            else
            {
                CycleManager.ResetIncludeInImprovementPlanFlag(ctx.ActiveModuleCycleID);
                var cycleMeasures = (from pm in pim.ParticipantMeasure
                                     where pm.ParticipantModuleCycleID == ctx.ActiveModuleCycleID
                                     select pm).ToList();
                //Set all to unselected
                foreach(var item in cycleMeasures)
                {
                    item.IncludeInImprovementPlan=false;
                    pim.SaveChanges();

                }
                foreach (var item in ProcessMeasures)
                {
                    ParticipantMeasure activeMeasure = pim.ParticipantMeasure.First(m => m.MeasureID == item.Key 
                        && m.ParticipantModuleCycleID == ctx.ActiveModuleCycleID);
                    activeMeasure.IncludeInImprovementPlan = true;
                    activeMeasure.LastUpdateDate = DateTime.Now;
                }
                pim.SaveChanges();
                foreach (var item in OutcomeMeasures)
                {
                    ParticipantMeasure activeMeasure = pim.ParticipantMeasure.First(m => m.MeasureID == item.Key
                        && m.ParticipantModuleCycleID == ctx.ActiveModuleCycleID);
                    activeMeasure.IncludeInImprovementPlan = true;
                    activeMeasure.LastUpdateDate = DateTime.Now;
                }
                foreach (var item in StratifiedMeasures)
                {
                    ParticipantMeasure activeMeasure = pim.ParticipantMeasure.First(m => m.MeasureID == item.Key
                        && m.ParticipantModuleCycleID == ctx.ActiveModuleCycleID);
                    activeMeasure.IncludeInImprovementPlan = true;
                    activeMeasure.LastUpdateDate = DateTime.Now;
                }
                pim.SaveChanges();

                ParticipantModuleCycle cycle = (from c in pim.ParticipantModuleCycle
                                                where c.ParticipantModuleCycleID == ctx.ActiveModuleCycleID
                                                select c).First<ParticipantModuleCycle>();
                if (cycle.ReportApproved == false)
                {
                    var chartsCountMissing = (from chart in pim.ParticipantChartStatus_V
                                              where chart.ParticipantModuleCycleID == ctx.ActiveModuleCycleID
                                              && chart.TotalCharts != chart.Completed
                                              select chart).Count();
                    if (chartsCountMissing == 0)
                    {
                        cycle.ReportApproved = true;
                        cycle.ReportApprovedCompletedDate = DateTime.Now;
                        cycle.ReportApprovedLastUpdatedDate = DateTime.Now;
                        cycle.AbstractDataComplete = true;
                        cycle.AbstractDataCompletedDate = DateTime.Now;
                        cycle.AbstractDataLastUpdatedDate = DateTime.Now;
                    }
                }
                cycle.MeasuresSelected = true;
                cycle.MeasuresSelectedCompletedDate = DateTime.Now;
                cycle.MeasuresSelectedLastUpdatedDate = DateTime.Now;
                cycle.LastUpdateDate = DateTime.Now;
                pim.SaveChanges();
                Response.Redirect("~/" + module.ModuleCode.ToLower() + "/ImprovementPlanPartI.aspx");

            }
        }
    }
}