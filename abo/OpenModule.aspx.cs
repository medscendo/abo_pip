﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using NetHealthPIMModel;

public partial class OpenModule : BasePage
{
    protected void Page_Load(object sender, EventArgs e)
    {
        if (!IsPostBack)
        {
            InitializePage();
        }
    }
    protected void InitializePage()
    {
        Session[Constants.SESSION_USERCONTEXT] = ctx;
        int ModuleID = Convert.ToInt32(Request.QueryString["mid"]);
        string charts = Request.QueryString["charts"];
        if (charts == null) charts = "";
        string reviewPractice = Request.QueryString["reviewPractice"];
        if (reviewPractice == null) reviewPractice = "";
        Session[Constants.SESSION_WORKINGMODULEID] = ModuleID;
        Response.Redirect("PatientChartRegistration.aspx");

    }
}
