﻿<%@ Page Title="" Language="C#" MasterPageFile="~/PIMMasterPage.master" AutoEventWireup="true" CodeFile="PQRSNextStep.aspx.cs" Inherits="abo_PQRS" %>

<asp:Content ID="Content1" ContentPlaceHolderID="head" Runat="Server">
</asp:Content>

<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder1" Runat="Server">
      
        <p>If you selected PQRS Reporting, the appropriate data has been transferred to the PQRS system</p>       
        <p><strong>To Report PQRS:</strong></p>
        <p>You may now go to <a href="http://www.pqrspro.com" target=new>PQRSPRO</a>
		<ul><li>Go to <a href="http://www.pqrspro.com" target=new>http://www.pqrspro.com</a>
		<li>To login, use your email address and the password 'abo2016'.</li>
		<li>You will be asked to pay for your submission, there is a $289 fee for this program.</li>
		<li>After you login and pay, you will be able to complete your submission.  You can watch the tutorial on the site for information on how to complete.</li>
		<li>If you have questions, you can contact us at 610.590.2229</li>
		</ul>
		</p>
       
        
        <div class="button-box"><asp:LinkButton ID="ButtonSubmit" runat="server" Text="Back" PostBackUrl="Dashboard.aspx" CssClass="button" /></div>
              
</asp:Content>

