﻿<%@ Page Title="" Language="C#" MasterPageFile="~/PIMMasterPage.master" AutoEventWireup="true" CodeFile="FrontMatter.aspx.cs" Inherits="abo_FrontMatter" %>

<asp:Content ID="Content1" ContentPlaceHolderID="head" Runat="Server">
    <style>
        label {
            padding-left: 5px;
        }


    </style>
</asp:Content>
<asp:Content ID="Content3" ContentPlaceHolderID="javascript" Runat="Server">
    <script type="text/javascript" language="javascript">
        function validateForm() {
            if (document.getElementById("<%= chkAcknowledgment.ClientID %>").checked==true) {
                return true;
            }
            alert('Please check the box above acknowledging your review of disclosures in order to continue.');
            return false;
         }
    </script>
</asp:Content>

<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder1" Runat="Server">

        <!-- Faculty -->
        <p>
            Improvement in Medical Practice Activities are developed by subject matter experts in the field relevant to the activity content.  Please review the faculty, conflict of interest policy and financial disclosures.
        </p>
        <p>
            Improvement in Medical Practice Activities are also accredited for 20 CME credits for completion of all steps on activities process.
        </p>
        <h3>FACULTY</h3>
        <p>
            Anthony C. Arnold, MD  
            <br />Keith H. Baratz, MD 
            <br />H. Culver Boldt, MD
            <br />Philip L. Custer, MD
            <br />Kenneth Goins, MD
            <br />Paul P. Lee, MD
            <br />R. Michael Siatkowski, MD
            <br />John E. Sutphin, MD 
            <br />David Wilson, MD 
        </p>
        <p>
            <strong>Conflict of Interest Policy and Financial Disclosures</strong>
            <br />Participants in the planning and development of this activity have the opportunity to affect the content of these through development, review, and testing feedback. To ensure that the Improvement in Medical Practice Activities are free of undue influence promoting the proprietary interests of any entity producing, marketing, re-selling, or distributing health care goods or services consumed by, or used on, patients, participants must disclose any relevant financial relationship they may have with commercial interests. These relationships may pose a COI which must be reviewed.
        </p>
        <p>
            If, upon review, a conflict of interest is thought to exist, it must be documented and investigated.  If a conflict is confirmed, action must be taken to resolve the conflict. Methods of resolution include but are not limited to: 
        </p>
        <ul>
            <li>Peer review of materials</li>
            <li>Changes to content of activity</li>
            <li>Documented validation of the recommendations</li>
            <li>Removal/replacement of the material or faculty exhibiting COI</li>
        </ul>
        <p>
            All conflicts must be resolved prior to Joint Sponsorship of the Improvement in Medical Practice Activities for CME credit.
        </p>
        <p>
            <strong>Financial Disclosures:</strong>
        </p>
        <p>
            <strong>The following physicians have made financial disclosures:</strong>
        </p>
        <p>
            Anthony C. Arnold, MD - Pfizer Inc., C
            <br />Keith H. Baratz, MD - Mayo Foundation for Education and Research, S
            <br />Philip L. Custer, MD - Pfizer, Johnson and Johnson, O
            <br />Paul P. Lee, MD - Genentech, C; Novartis, C; Quorum Consulting, C; Pfizer, Inc., C; GlaxoSmithKline, O; Medco Health Solutions, O; Merck, O; Vital Spring Health Technologies, O;
            <br />John E. Sutphin, MD - The Ocular Surface, L
            <br />David Wilson, MD - Oxford Biomedica, C,S;  AGTC, S; Research to Prevent Blindness, S
            <br />
            <br />C - Consultant/Advisor, E - Employee, L - Lecture Fees, O - Equity/Owner, P - Patents/Royalty, S - Grant support
        </p>
        <p>
            <strong>The following physicians have nothing to disclose:</strong>
        </p>
        <p>
            H. Culver Boldt, MD
            <br />Kenneth Goins, MD
            <br />R. Michael Siatkowski, MD
        </p>
        <!-- Medium used -->
        <h3>MEDIUM USED</h3>
        <p>
            Improvement in Medical Practice Web-based Activity
        </p>
        <!-- Method of physician participation in the learning process -->
        <h3>METHOD OF PHYSICIAN PARTICIPATION IN THE LEARNING PROCESS</h3>
        <p>
            Physicians participate in a three-stage process:
        </p>
		<ul>
            <li>Stage A:  Learning from practice performance assessment</li>
		    <li>Stage B:  Learning from the application of improvement in medical practice to patient care </li>
		    <li>Stage C:  Learning from the evaluation of the PI effort </li>
		</ul>

        <!-- Estimated time to complete the educational activity -->
        <h3>ESTIMATED TIME TO COMPLETE THE EDUCATIONAL ACTIVITY</h3>
        <p>
            It is estimated that it may take up to 20 hours to complete  the process of online chart abstraction and evaluation, exclusive of required follow-up timeframes outlined in each Improvement in Medical Practice Activity.
        </p>

        <!-- Dates of original release and most recent review or update -->
        <h3>DATES OF ORIGINAL RELEASE AND MOST RECENT REVIEW OR UPDATE</h3>
        <p>
            February 18, 2014
        </p>

        <!-- Accreditation and Credit Designation statement -->
        <h3>ACCREDITATION AND CREDIT DESIGNATION STATEMENT</h3>
        <p>
            This activity has been planned and implemented in accordance with the Essential Areas and policies of the Accreditation Council for Continuing Medical Education through the joint sponsorship of the American Academy of Ophthalmology and the American Board of Ophthalmology. The American Academy of Ophthalmology is accredited by the ACCME to provide continuing medical education for physicians.
        </p>
        <p>
            The American Academy of Ophthalmology designates this enduring material for a maximum of 20 <em>AMA PRA Category 1 Credits</em>&trade;. Physicians should claim only the credit commensurate with the extent of their participation in the activity.
        </p>
	<p>
		If you are not an AAO member or do not have an Academy website login, you will be prompted to create a new account. 
	</p>
	<p>
		If you believe you have an Academy membership and are unable to log in, 
		please do not create a new account. This will not connect to your existing member record, 
		and will not give you access to your password-protected CME Central account. 
		For additional information and assistance, <a href='http://www.aao.org/help/index.cfm' target=_blank>visit the Academy's Help section</a>.
	</p>

        <!-- Objectives -->
        <h3>OBJECTIVES</h3>
        <p>
            After completing this activity, diplomates should be able to:
        </p>
        <ul>
            <li>Describe the difference between process and outcome measures.</li>
            <li>Identify performance on process or outcome measures that are potential areas for improvement in patient care within  their practice.</li>
            <li>Utilize comparison of self-assessment and actual performance, as well as feedback provided upon completion of chart abstraction to develop an improvement plan that will address their gaps in patient care.</li>
            <li>Implement an improvement plan to eliminate gaps and improve patient care.</li>
            <li>Re-assess performance on deficient process or outcome measures following institution of the improvement plan.</li>
            <li>Employ this process to engage in a continuous, ongoing culture of improvement in medical practice in their individual practices.</li>
        </ul>

        <!-- Termination date -->
        <h3>TERMINATION DATE</h3>
        <p>
             February 18, 2017
        </p>
        
        <!-- Hardware/Software Requirements -->
        <h3>HARDWARE/SOFTWARE REQUIREMENTS</h3>
        <p>
            Browser Requirements: Internet Explorer 7 or higher, Mozilla Firefox 3.6 or higher, Adobe Acrobat Reader, and Adobe Flash Player 
        </p>
        
        <!-- Policy on Privacy and Confidentiality -->
        <h3>POLICY ON PRIVACY AND CONFIDENTIALITY</h3>
        <p>
            To view the privacy policy please click <a href="http://abop.org/privacy-policy/" target="_blank">here</a>.
        </p>
        
        <!-- Copyright -->
        <h3>COPYRIGHT</h3>
        <p>
            Copyright 2014, American Board of Ophthalmology. All rights reserved.
        </p>
        
        <!-- Check Box entry for I accept -->
        <asp:CheckBox runat="server" ID="chkAcknowledgment" text="By checking this box I acknowledge that I have reviewed these disclosures."/><br /><br />
        <asp:Button class="button" runat="server" OnClientClick=" return validateForm();" id="ButtonSubmit" OnClick="ButtonSubmit_Click" Text="Continue" />
    
</asp:Content>