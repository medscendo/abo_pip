﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using NetHealthPIMModel;
using System.Transactions;

public partial class abo_PIMTutorial : System.Web.UI.Page
{
    protected void Page_Load(object sender, EventArgs e)
    {
        ((PIMMasterPage)Page.Master).SetHeader("Activities & Minimum Improvement Periods");
        ((PIMMasterPage)Page.Master).SetStatus(1);
    }
    protected void ButtonSubmit_Click(object sender, EventArgs e)
    {
            Response.Redirect("PIPStatus.aspx");
    }
}
