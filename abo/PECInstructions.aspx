﻿<%@ Page Title="" Language="C#" MasterPageFile="~/PIMMasterPage.master" AutoEventWireup="true" CodeFile="PECInstructions.aspx.cs" Inherits="abo_PECInstructions" %>

<asp:Content ID="Content1" ContentPlaceHolderID="head" Runat="Server">

</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder1" Runat="Server">

<p>
    You have elected to include a new Patient Experience of Care Survey as part of your PIP.
</p><p>
    The ABO's Patient Experience of Care (PEC) Survey consists of 15 questions (5 demographic and 10 
	specifically designed to assess a physician's communication style during the patient/doctor encounter).
	Diplomates must submit 45 surveys completed by patients.   The PEC provides patients with easy-to-follow directions 
	along with the ability to submit their responses via paper, phone IVR, or web-based technologies. 
</p><p>
Once these surveys are completed, the results will be included in the initial Practice Report of this PIP, and you will be able to include measures in your Improvement Plan. 
</p>
<p>In order to begin a new PEC, you will first need to register for this on the ABO website.  
<a href="http://abop.org/maintain-certification/part-4-practice-performance-assessment/patient-experience-of-care-%28pec%29-survey/" target="_blank">Click Here</a> 
to register.  Once registered, you will be able to include that PEC in your PIP Plan on the prior page.
</p>

<div class="button-box">
    <a href="ModuleSelectionReview.aspx" class="button">Return to &#39;Create Your Experience&#39;</a>
</div>
</asp:Content>
<asp:Content ID="Content3" ContentPlaceHolderID="javascript" Runat="Server">
</asp:Content>

