﻿<%@ Page Title="" Language="C#" MasterPageFile="~/PIMMasterPage.master" AutoEventWireup="true" CodeFile="ModuleProcess.aspx.cs" Inherits="abo_ModuleProcess" %>

<asp:Content ID="Content1" ContentPlaceHolderID="head" Runat="Server">
    <script src="https://ajax.googleapis.com/ajax/libs/jquery/1.7.2/jquery.min.js" type="text/javascript"></script>
    <script src="https://ajax.googleapis.com/ajax/libs/jquery/1.7.2/jquery.js" type="text/javascript"></script>
    <script src="../common/js/jquery.colorbox.js" type="text/javascript"></script>
    <link rel="stylesheet" href="../common/css/colorbox.css" />
    <script type="text/javascript">
        jQuery(document).ready(function () {
            $("#Login").colorbox({ inline: true, width: "50%" });
            $("#AboPartIV").colorbox({ inline: true, width: "50%" });
            $("#SelectDiagnosis").colorbox({ inline: true, width: "50%" });
            $("#PIPDashboard").colorbox({ inline: true, width: "50%" });
            $("#PracticeReview").colorbox({ inline: true, width: "50%" });
            $("#ChartAbstraction").colorbox({ inline: true, width: "50%" });
            $("#PerformanceReport").colorbox({ inline: true, width: "50%" });
            $("#DevelopImprovement").colorbox({ inline: true, width: "50%" });
            //$("#ImplementChanges").colorbox({ inline: true, width: "50%" });
            $("#Reassessment").colorbox({ inline: true, width: "50%" });
            $("#ReceivePart").colorbox({ inline: true, width: "50%" });
            //$("#SubmitData").colorbox({ inline: true, width: "50%" });
        });
    </script>
<style type="text/css">
    ul li.arrow { list-style-image: url(../common/images/arrow.png); padding-left: 20px; list-style-position: inside; padding-top: 10px;}
    p.pro {padding-top: 10px; padding-bottom: 10px;}
    p.tip {margin-left: 20px; padding: 10px; border: solid 1px blue; background: #c2dafe;}
    .tip {margin-left: 20px; padding: 10px; border: solid 1px blue; background: #c2dafe;}

    .OptionTree 
    {
        width:700px;
        height:850px;
        background: transparent url(../common/images/Arrows_Underlay.png) no-repeat;
        margin: 0 auto;
    }
        
    .smallButton
    {
        width:226px;
        height:76px;
        display:block;
        background-image: url(../common/images/SmallButton.png);
        text-decoration:none;
    }
    .smallButton:hover
    {
        background-image: url(../common/images/SmallButtonHover.png);
    }
        
    .smallButton h3
    {
        padding: 12px 22px 0px 12px;
        font-size: 16px;
        color:#fff;
        text-decoration:none;
        text-align:center;
    }
        
    .bigButton
    {
        width:226px;
        height:96px;
        display:block;
        background-image: url(../common/images/BigButton.png);
        text-decoration:none;
    }
    .bigButton:hover
    {
        background-image: url(../common/images/BigButtonHover.png);
    }
        
    .bigButton h3
    {
        padding: 12px 22px 22px 12px;
        font-size: 18px;
        color:#fff;
        text-decoration:none;
        text-align:center;
    }
    
    .tipBox
    {
        margin-left:20px;
        padding:10px;
        border: 1px solid blue;
        background: #C2DAFE;
    }
        
    #Login 
    {
        position:relative;
        top:5px;
        left:237px;
    }
        
    #AboPartIV 
    {
        position:relative;
        top:20px;
        left:237px;
    }
        
    #SelectDiagnosis
    {
        position:relative;
        top:35px;
        left:237px;
    }
        
    #PIPDashboard
    {
        position:relative;
        top:53px;
        left:237px;
    }
        
    #PracticeReview
    {
        position:relative;
        top:40px;
        left:10px;
    }
        
    #ChartAbstraction
    {
        position:relative;
        top:-36px;
        left:469px;
    }
        
    #PerformanceReport
    {
        position:relative;
        top:-43px;
        left:237px;
    }
        
    #DevelopImprovement
    {
        position:relative;
        top:-28px;
        left:237px; 
    }
        
    #ImplementChanges
    {
        position:relative;
        top:-12px;
        left:237px;
    }
    #ImplementChanges:hover
    {
        background-image: url(../common/images/SmallButton.png);
        cursor: auto;
    }
        
    #ReceivePart
    {
        position:relative;
        top:-80px;
        left:237px;
    }
        
    #Reassessment
    {
        position:relative;
        top:71px;
        left:10px;
    }
        
    #SubmitData
    {
        position:relative;
        top:-81px;
        left:469px;
    }
    #SubmitData:hover
    {
        background-image: url(../common/images/SmallButton.png);
        cursor: auto;
    }

</style>

</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder1" Runat="Server">
         <!-- activity detaile -->
          <div class="main_content" style="padding-right: 100px;">
            <div class="heading_box" >
              <h2 ><strong>Part IV MOC Practice Performance Platform (PIP) Process Overview</strong></h2>
            </div>
            <div class="imporvenment_plan_details">
              <div class="top_details" style="padding-left:50px; padding-right: 50px;padding-top: 30px; line-height: 1.2; ">
              <p>The following provides a step-by-step overview of the American Board of Ophthalmology’s Part IV MOC Improvement in Medical Practice Platform (PIP) process. Click on any of the buttons for more information.
               </p>  
               <br />   
               <p><strong><a href="ModuleProcess.PDF" target="_blank">Download</a> PDF of this Overview</strong></p>
                  <!--<img src="~/images/ModuleProcess.png" runat="server" />-->
                <div class="OptionTree">
                    <a href="#Login_content" class="bigButton" id="Login"><h3>Login Complete Practice Profile &amp; View Tutorial</h3></a>
                    <a href="#AboPartIV_content" class="smallButton" id="AboPartIV"><h3>ABO Part IV PIP Main Page</h3></a>
                    <a href="#SelectDiagnosis_content" class="smallButton" id="SelectDiagnosis"><h3>Select Diagnosis Topic(s) and Register Charts</h3></a>
                    <a href="#PIPDashboard_content" class="smallButton" id="PIPDashboard"><h3>PIP Status Page</h3></a>
                    <a href="#PracticeReview_content" class="smallButton" id="PracticeReview"><h3>Practice Review &amp; Self-Assessment</h3></a>
                    <a href="#ChartAbstraction_content" class="smallButton" id="ChartAbstraction"><h3>Chart Abstraction</h3></a>
                    <a href="#PerformanceReport_content" class="smallButton" id="PerformanceReport"><h3>Feedback Report</h3></a>
                    <a href="#DevelopImprovement_content" class="smallButton" id="DevelopImprovement"><h3>Develop Improvement Plan</h3></a>
                    <a href="#ImplementChanges_content" class="smallButton" id="ImplementChanges"><h3>Implement Changes Utilizing Resources</h3></a>
                    <a href="#Reassessment_content" class="smallButton" id="Reassessment"><h3>Receive Part IV MOC</h3></a>
                    <a href="#ReceivePart_content" class="smallButton" id="ReceivePart"><h3>Reassessment</h3></a>
                    <a href="#SubmitData_content" class="smallButton" id="SubmitData"><h3>Submit data for PQRS incentive</h3></a>
                </div>

                <div style='display:none'>
		            <div id='Login_content' style='padding:10px; background:#fff;'>
		                <p>Log into ABO site and register for <em>ABO Part IV MOC Improvement in Medical Practice Platform (PIP)</em>, complete Practice Profile and view Tutorial</p>
                    </div>
	            </div>

                <div style='display:none'>
		            <div id='AboPartIV_content' style='padding:10px; background:#fff;'>
                        <h2>Main Page</h2>
                        <p>
                            <br />If you’re viewing this page, you have already completed the Practice Profile and viewed the PIP Tutorial.
                            <br />
                            <br />The <strong>Main Page</strong> shows your status in the process and the steps needed to complete the process. Return to this page at any time, by clicking “Main Page” in the top right hand corner of the screen. Click the “Support” button at any time for technical or clinical questions, comments, or suggestions.
                        </p>
                        <br />
                        <div class="tipBox">
                            TIP
                            <br />When returning to the PIP, continue where you left off in the PIP process by clicking the link at the bottom of the Main Page graphic labeled: &quot;To continue where you left off, Click Here&quot;
                        </div>
                        <br />
                        <ul style="margin-left:25px; list-style-image: url(../common/images/arrow.png);"> 
                            <li>Begin by clicking &quot;Select Your Activities&quot; on the blue square of the Main Page to go to the Activity Library and choose your activities.</li>
                        </ul>
                    </div>
	            </div>

                <div style='display:none'>
		            <div id='SelectDiagnosis_content' style='padding:10px; background:#fff;'>
                        <h2>Activity Library</h2>
                        <br />
                        <ul style="margin-left:20px; list-style-image:url(../common/images/arrow.png);">
                            <li>Choose 1 to 3 activities to work from by checking the square next to the diagnosis listed.</li>
                            <li>Click on &quot;Select Your Activities&quot; in the blue square to proceed.</li>
                        </ul>
                        <br />
                        <div class="tipBox">
                            TIPS
                            <br />
                            <ol>
                                <li>Click on &quot;Details&quot; next to each diagnosis area for a Activity Overview of measure content and rationale, including patient definitions, key measures, and applicable evidence-based resources and treatment guidelines. Each measure was selected by a panel of content experts, who are ABO diplomates and practicing ophthalmologists, and is based on AAO Preferred Practice Patterns and supporting evidence-based literature.</li>
                                <li>A Process Measure refers to a particular recommended procedure or system-related aspect of patient care in consideration of the chosen diagnosis and treatment. An Outcome Measure refers to the desired result of treatment and can be used to consider quality of care and desired health outcomes.</li>
                                <li>Scroll to the bottom to the orange &quot;View Chart Abstraction Detail&quot; button to access the chart details to be entered for each patient or to &quot;View Peer Data&quot; to see aggregated results for any peer data gathered on a measure to date, if available. This page also includes access to &quot;View Resources&quot; for improvement planning associated with each diagnosis.</li>
                                <li>If choosing to work with only one activity: click &quot;Confirm Chart Selection&quot; from the &quot;Activity Overview&quot; screen to move on to the &quot;Patient Chart Registration Form.&quot; If you have already chosen your activities, you may also click &quot;CONFIRM ACTIVITY SELECTION&quot; from the Overview page.</li>
                                <li>If choosing more than one activity, or seeking to view additional choices, scroll up and click on &quot;RETURN TO ACTIVITY SELECTION,&quot; to return to the Activity Library. Choose the number of charts (no less than 10 per activity) to be abstracted.</li>
                            </ol>
                        </div>
                        <br />
                        <ul style="margin-left:20px; list-style-image:url(../common/images/arrow.png);">
                            <li>Once activities are chosen, click the &quot;SUBMIT&quot; button at the bottom of the Activity Library page to review the details of each activity chosen and confirm choices. </li>
                            <li>Click &quot;Confirm&quot; next to each activity, scroll up to the upper right of the page and click &quot;CONFIRM CHART SELECTION,&quot; or scroll down to the bottom of the page and click &quot;Confirm&quot; at the bottom right. This indicates acceptance of activities selected, moving on to the Patient Chart Registration Form to ensure that the correct number of patient charts are available for each measure chosen.</li>
                        </ul>
                        <br />
                        <div class="tipBox">
                            TIPS
                            <br />A total of 30 consecutive patient charts and a minimum of 10 consecutive patient charts are required for the first chart abstraction.
                        </div>
                    </div>
	            </div>

                <div style='display:none'>
		            <div id='PIPDashboard_content' style='padding:10px; background:#fff;'>
                        <h2>Patient Chart Registration Form</h2>
                        <ul style="margin-left:20px; list-style-image:url(../common/images/arrow.png);">
                            <li>Enter relevant chart information in the spaces provided. The number of patient chart fields requiring entry is dependent upon the number of activities you selected. One selected activity will display 30 patient charts; two selected activities will display between 10 and 20 patient charts; and three selected activities will display 10 patient charts.</li>
                            <li>Once the required number of charts has been entered (a total of 30 patient charts, no less than 10 per activity), scroll down to the bottom of the page and click &quot;Submit Registration.&quot; Successfully registered charts will load under &quot;Registered Chart Data.&quot; Incomplete charts will appear under &quot;Please Enter Additional Chart Data.&quot;</li>
                            <li>Once all charts are successfully completed, click the orange &quot;Next Activity&quot; button at the bottom of the page. This will bring you back to the &quot;Activity Details&quot; page. Click the &quot;Main Page&quot; button if you wish to review your progress.</li>
                        </ul>
                        <br />
                        <div class="tipBox">
                            TIP
                            <br />If you do not have at least 10 charts for a given activity, scroll down and click on &quot;Insufficient # of Charts Select New Activity&quot; to return to the Activity Library and make another selection or raise the chosen chart number for another activity.
                        </div>
                    </div>
	            </div>

                <div style='display:none'>
		            <div id='PracticeReview_content' style='padding:10px; background:#fff;'>
		                <h2>Activity Status Page – Data Collection</h2>
                        <br />
                        <p>Self-Assessment</p>
                        <ul style="margin-left:20px; list-style-image:url(../common/images/arrow.png);">
                            <li>Self-Assessment is the next step in the PIP process. Complete the brief questions presented and click “Save Self-Assessment.” This will record the responses and return to the Status Page. </li>
                        </ul>
                        <br />
                        <p>System Survey</p>
                        <ul style="margin-left:20px; list-style-image:url(../common/images/arrow.png);">
                            <li>Self-Assessment is the next step in the PIP process. Complete the brief questions presented and click “Save Self-Assessment.” This will record the responses and return to the Status Page
                        <br />
                        <div class="tipBox">
                            TIP
                            <ol>
                                <li>Self-Assessment and System Survey must be complete prior to Chart Abstraction.</li>
                                <li>To the right, orange buttons will go to the Status Page of any other activities chosen.</li>
                            </ol>
                        </div>
                    </div>
	            </div>

                <div style='display:none'>
		            <div id='ChartAbstraction_content' style='padding:10px; background:#fff;'>
		                <h2>Chart Abstraction</h2>
                        <ul style="margin-left:20px; list-style-image:url(../common/images/arrow.png);">
                            <li>Click Chart Abstraction on the Status Page to begin entering patient chart data. Each chart abstraction question relates to evidence-based or prevailing practice standards for the activity diagnosis. Enter this data from the patient charts previously recorded in the Patient Chart Registration Form. You can return to the Dashboard at any time to review your progress or move to another activity. You will enter between 10 and 30 charts, dependent on the number entered into the Patient Registration Form. Once you complete a chart entry, click &quot;Submit&quot; to record it. If you are missing information, or have a correction, you can return at any time to review or correct chart input.</li>
                        </ul>
                        <br />
                        <div class="tipBox">
                            TIP
                            <ol>
                                <li>The column labeled &quot;My Data&quot; represents the information entered; the column titled &quot;Number to Complete&quot; represents what still needs to be completed; the column labeled &quot;Target Completion Date&quot; is an estimated, or suggested, timeline automatically generated by the PIP system, based on a 1-month timeframe to complete Data Collection and Review and 2-weeks for Improvement Plan; and &quot;Actual Completion Date&quot; is the date that the step was completed.</li>
                                <li>&quot;Tip&quot; icons (question marks in orange circles) throughout chart abstraction pages offer added information.</li>
                                <li>Once Self-Assessment, System Survey and Chart Abstraction are complete, a link will become available to Review Feedback Report in the Improvement Plan section below Data Collection.</li>
                            </ol>
                        </div>
                    </div>
	            </div>

                <div style='display:none'>
		            <div id='PerformanceReport_content' style='padding:10px; background:#fff;'>
		                <h2>Review Initial Feedback Report</h2>
                        <br />
                        <ul>
                            <li class="arrow">Click <strong>Review Initial Feedback Report</strong> to review practice performance, based on patient charts entered in comparison to Self- and System Assessment and any relevant peer data available. Note the legend containing chart column definitions.</li>
                            <li class="arrow">Click the orange circle with a question mark at the top of the page to view the legend containing chart column definitions.</li>
                            <li class="arrow">Review the data to ensure that it appears accurate. Click &quot;Return to Status Page&quot; at the bottom of the page, to revisit and correct any inconsistencies.  When satisfied that the report accurately reflects your practice, click &quot;Proceed to Improvement Plan&quot; to signify final approval of this Report and move to Develop Improvement Plan.</li>
                        </ul>
                    </div>
	            </div>

                <div style='display:none'>
		            <div id='DevelopImprovement_content' style='padding:10px; background:#fff;'>
		                <h2>Improvement Plan</h2>
                        <br />
                        <strong>Develop Improvement Plan</strong>
                        <ul>
                            <li class="arrow">Choose three to five measures to develop an individual Improvement Plan.</li>
                            <li class="arrow">To choose a measure to improve upon, check the box next to each measure.</li>
                            <li class="arrow">Click &quot;Next&quot; to proceed to submitting the Improvement Plan.</li>
                        </ul>
                        <br />
                        <div class="tipBox">
                            TIP
                            <br />Outlined boxes indicate an area you may wish to consider for improvement.  However, it is entirely up to you to decide the direction of your improvement plan.
                        </div>
                        <br />
                        <strong>Submit Improvement Plan</strong>
                        <ul>
                            <li class="arrow">Review the measures chosen to address in the Improvement Plan.</li>
                            <li class="arrow">Under &quot;Measures Selected,&quot; set concrete numerical percentage goals for improvement.</li>
                            <li class="arrow">Outline the changes you consider most important for you and your practice to make.</li>
                            <li class="arrow">Select, if applicable, additional resources pertaining to your selected measures for improvement.</li>
                            <li class="arrow">Choose the date when improvement implementation will commence and the amount of time allotted (3-6 months).</li>
                            <li class="arrow">Click &quot;Submit Final Plan&quot; to begin implementation.</li>
                        </ul>
                        <br />
                        <div class="tipBox">
                            TIPS
                            <ol>
                                <li>Once the allotted time has passed, you will receive an email reminder to return to the Status Page and complete the final phase of the PIP.</li>
                                <li>Return to the Improvement Plan at any time to explore improvement resources and review your goals.</li>
                            </ol>
                        </div>
                    </div>
	            </div>

                <div style='display:none'>
		            <div id='ImplementChanges_content' style='padding:10px; background:#fff;'>
		                <p>
                            ImplementChanges<br />
                            Lorem ipsum dolor sit amet, consectetur adipiscing elit. Sed euismod neque vitae sapien tempor laoreet ullamcorper justo rhoncus. Mauris sit amet velit sed ante porttitor scelerisque in eget arcu. Fusce eget ante ut justo tincidunt dictum vitae at velit. Etiam enim metus, commodo id tincidunt sit amet, imperdiet in arcu. Ut aliquam elit id nisi tincidunt egestas ut vel lectus. Nunc tempor, ipsum ornare lacinia ullamcorper, nisi tellus faucibus augue, sed ultrices enim nisl at dolor. Curabitur accumsan varius magna, in faucibus ipsum condimentum eget. Phasellus nunc arcu, feugiat quis elementum sed, eleifend sit amet libero. Nullam vitae adipiscing enim. Ut accumsan elit nec lacus porta eget facilisis nisl bibendum. Cras nisl eros, rutrum ac laoreet vel, tincidunt eu elit. Aliquam elementum congue magna, a tincidunt lorem imperdiet et. Donec urna dolor, dignissim vitae dapibus eget, porttitor eget est. Quisque laoreet vehicula tellus in sollicitudin.
                            <br />
                            <br />Etiam feugiat magna enim, at hendrerit nisi. Nunc et mi non risus semper vulputate. Nullam in metus in mi dignissim consequat vitae ut lacus. In vel eros vitae felis lobortis vestibulum non ut sapien. Praesent tortor felis, egestas viverra aliquam at, varius vitae sem. Pellentesque interdum tristique urna, ac facilisis nisi scelerisque consectetur. Sed lobortis pharetra ligula ac pulvinar. Nulla venenatis urna scelerisque quam bibendum vel suscipit purus volutpat. Curabitur tempor arcu et massa lacinia vitae molestie augue aliquam. Cum sociis natoque penatibus et magnis dis parturient montes, nascetur ridiculus mus. Vestibulum consectetur tristique diam eu vulputate.
                            <br />
                            <br />Vestibulum varius dolor velit. In imperdiet lorem nisi, vel eleifend neque. Phasellus molestie, eros non scelerisque ullamcorper, augue arcu lacinia erat, vitae dapibus libero augue nec lectus. Donec ultricies, tellus in pharetra bibendum, augue ipsum commodo erat, non fringilla turpis dolor a ligula. Vivamus vehicula bibendum est id mollis. Cras aliquam pretium nunc, sed aliquam nisi suscipit vitae. Nunc consequat luctus condimentum. Vivamus in arcu neque. Donec et magna nec massa tristique luctus. Pellentesque scelerisque velit id enim egestas et eleifend dolor cursus.
                        </p>
                    </div>
	            </div>

                <div style='display:none'>
		            <div id='Reassessment_content' style='padding:10px; background:#fff;'>
		                <h2>Complete Part IV MOC</h2>
                        <br />
                        <ul>
                            <li class="arrow">Once &quot;Save Impact Statement&quot; is complete, an orange &quot;Submit Complete Activity&quot; button at the bottom of the Status Page will appear.</li>
                            <li class="arrow">Click the &quot;Save Impact Statement&quot; button to complete the Part IV MOC Improvement in Medical Practice and become eligible for MOC credit.</li>
                            <li class="arrow">Congratulations! You have now completed the American Board of Ophthalmology’s Part IV Maintenance of Certification (MOC) Improvement in Medical Practice Platform.</li>
                        </ul>
                    </div>
	            </div>

                <div style='display:none'>
		            <div id='ReceivePart_content' style='padding:10px; background:#fff;'>
		                <h2>Remeasurement</h2>
                        <br />
                        <ol style="list-style-type:upper-alpha;">
                            <li><strong>Remeasurement</strong> consists of:</li>
                        </ol>
                        <ul>
                            <li class="arrow">Registering 10 consecutive patient charts per activity from patient visits that have occurred since you completed your improvement plan. </li>
                            <li class="arrow">Completing the self assessment and systems survey </li>
                            <li class="arrow">Completing the second set of chart abstractions</li>
                        </ul>
                        <br />
                        <div class="tipBox">
                        TIPS
                        <br />
                        <ol>
                            <li>10 consecutive charts are required for this step per selected activity from the Improvement plan. </li>
                            <li>Charts MUST be consecutive.</li>
                            <li>All self assessment, system survey and chart abstractions must be complete in order to proceed to the Final Feedback Report. </li>
                        </ol>
                        </div>
                        <br />
                        <ol style="list-style-type:upper-alpha;" start="2">
                            <li>
                                <strong>Review Final Feedback Report</strong>
                                <ul>
                                    <li class="arrow">Review the Final Feedback Report, noting any improvements made in relation to the goals set.  Once you are satisfied that this report accurately reflects current assessment, systems and data in the charts, click &quot;Proceed to Impact Statement&quot; to move on to the final step.</li>
                                </ul>
                            </li>
                            <li>
                                <strong>Impact Statement</strong>
                                <ul>
                                    <li class="arrow">Review the results of your selected measures for improvement. </li>
                                    <li class="arrow">Compare data from your first set of chart abstractions to your second set of chart abstractions. </li>
                                    <li class="arrow">Reflect on the changes you made.</li>
                                    <li class="arrow">Identify any barriers you encountered while implementing your improvement plan.</li>
                                    <li class="arrow">Rate the Improvement in Medical Practice Platform process.</li>
                                    <li class="arrow">When complete, click the &quot;Save Impact Statement&quot; button.</li>
                                </ul>
                            </li>
                        </ol>
                    </div>
	            </div>

                <div style='display:none'>
		            <div id='SubmitData_content' style='padding:10px; background:#fff;'>
		                <p>
                            SubmitData<br />
                            Lorem ipsum dolor sit amet, consectetur adipiscing elit. Sed euismod neque vitae sapien tempor laoreet ullamcorper justo rhoncus. Mauris sit amet velit sed ante porttitor scelerisque in eget arcu. Fusce eget ante ut justo tincidunt dictum vitae at velit. Etiam enim metus, commodo id tincidunt sit amet, imperdiet in arcu. Ut aliquam elit id nisi tincidunt egestas ut vel lectus. Nunc tempor, ipsum ornare lacinia ullamcorper, nisi tellus faucibus augue, sed ultrices enim nisl at dolor. Curabitur accumsan varius magna, in faucibus ipsum condimentum eget. Phasellus nunc arcu, feugiat quis elementum sed, eleifend sit amet libero. Nullam vitae adipiscing enim. Ut accumsan elit nec lacus porta eget facilisis nisl bibendum. Cras nisl eros, rutrum ac laoreet vel, tincidunt eu elit. Aliquam elementum congue magna, a tincidunt lorem imperdiet et. Donec urna dolor, dignissim vitae dapibus eget, porttitor eget est. Quisque laoreet vehicula tellus in sollicitudin.
                            <br />
                            <br />Etiam feugiat magna enim, at hendrerit nisi. Nunc et mi non risus semper vulputate. Nullam in metus in mi dignissim consequat vitae ut lacus. In vel eros vitae felis lobortis vestibulum non ut sapien. Praesent tortor felis, egestas viverra aliquam at, varius vitae sem. Pellentesque interdum tristique urna, ac facilisis nisi scelerisque consectetur. Sed lobortis pharetra ligula ac pulvinar. Nulla venenatis urna scelerisque quam bibendum vel suscipit purus volutpat. Curabitur tempor arcu et massa lacinia vitae molestie augue aliquam. Cum sociis natoque penatibus et magnis dis parturient montes, nascetur ridiculus mus. Vestibulum consectetur tristique diam eu vulputate.
                            <br />
                            <br />Vestibulum varius dolor velit. In imperdiet lorem nisi, vel eleifend neque. Phasellus molestie, eros non scelerisque ullamcorper, augue arcu lacinia erat, vitae dapibus libero augue nec lectus. Donec ultricies, tellus in pharetra bibendum, augue ipsum commodo erat, non fringilla turpis dolor a ligula. Vivamus vehicula bibendum est id mollis. Cras aliquam pretium nunc, sed aliquam nisi suscipit vitae. Nunc consequat luctus condimentum. Vivamus in arcu neque. Donec et magna nec massa tristique luctus. Pellentesque scelerisque velit id enim egestas et eleifend dolor cursus.
                        </p>
                    </div>
	            </div>
                 
                <p  style="padding-top: 10px; padding-bottom: 10px;">A checklist for the process is available for download <a href="../images/ABO_PIP_CHECKLIST.pdf">here</a>. </p>
                <br />
                <strong>Remember</strong>
                <br />
                <ul>
                    <li class="arrow">Save early and save often</li>
                    <li class="arrow">There is a safety feature that will log you out after 1 hour of inactivity</li>
                    <li class="arrow">Click on &quot;Support&quot; or &quot;Contact&quot; links located in the navigation bar in the top right corner of any screen if you have questions or comments</li>
                </ul>
              </div>
              <div class="bginput" ><asp:LinkButton ID="ButtonSubmit" runat="server" Text=" Back " PostBackUrl="Dashboard.aspx"/></div>
              </div>
            </div>
          <!-- activity detaile ends --> 
         </div>
</asp:Content>

