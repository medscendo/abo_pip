﻿<%@ Page Title="" Language="C#" MasterPageFile="~/PIMMasterPage.master" AutoEventWireup="true" CodeFile="SelectPEC.aspx.cs" Inherits="abo_SelectPEC" %>

<asp:Content ID="Content1" ContentPlaceHolderID="head" Runat="Server">

    <style>
        .left-col {
            float:left;
            width: 530px;
        }

        .chart-image {
            float:left;
            margin-left:40px;
        }

        .action-box {
            text-align:center;
        }

        .action-box p {
            width: 100%;
            display:inline-block;
        }

        .button-box {
            width: 80%;
            margin: 5px auto 15px;
        }

        .button-box .button {
            border: 1px solid #0083CA;
        }

        .third {
            float:left;
            -moz-box-sizing: border-box;
            -webkit-box-sizing: border-box;
            box-sizing: border-box;

            width:33.33%;

            text-align:center;
        }

        .third p {
            font-size: 0.8em;
        }

        .third.mid {
            border-right:1px solid #ccc;
            border-left:1px solid #ccc;
        }

        .third .button {
            width: 80%;
            text-align:center;
        }

        #date-bar {
            font-size: 0.79em;
        }
    </style>

</asp:Content>

<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder1" Runat="Server">

    <asp:HiddenField runat="server" ID="HiddenFieldChartsSelected" />
    <asp:HiddenField runat="server" ID="HiddenFieldPECID" />
    <asp:HiddenField runat="server" ID="HiddenFieldPECSelection" />
    <div class="row clearfix">
        <div class="left-col">
            <p>As part of your Assess &amp; Improve Phase, you can choose to conduct a Patient Experience of Care (PEC) Survey.</p>
            <p>
                Selecting this as a resource requires 45 patients to complete a patient survey. Once completed, as a part of the improvement phase
                you will receive a report of patient surveys. You can select measures from this survey to include in your improvement plan. 
            </p>
            <p>
                The survey document will include a cover sheet with instructions about how
                patient should submit their feeback either online, by phone or in a hard copy. You may wish to include a self-addressed stamped envelope 
                for those patients who wish mail in their completed survey.
            </p>
            <p>
                <a href="PECForms.aspx" runat="server" id="hrefPECSurvey"><strong>Click here to download the survey &raquo;</strong></a> 
            </p>
          
              <%--<asp:Literal runat="server" ID="PECNotAvailable"><br /></asp:Literal>
          
              You have completed 
              <asp:Literal runat="server" ID="PECCount" Visible="false"> surveys. Would you like to use that as part of this PIP?</asp:Literal>
              <asp:RadioButtonList runat="server" ID="RadioExistingYesNo" Visible="false" onclick="pecPrevious(this);"><asp:ListItem  Value="1">Yes</asp:ListItem><asp:ListItem Value="0">No</asp:ListItem></asp:RadioButtonList>
          
              Would you like to include a <asp:Literal runat="server" ID="PECNewSurvey" Visible="false">NEW </asp:Literal>Patient Experience of Care Survey in your Initial Assessment Phase?
              <asp:RadioButtonList runat="server" ID="RadioNewYesNo" onclick="pecNew(this);"><asp:ListItem Value="1">Yes</asp:ListItem><asp:ListItem Value="0">No</asp:ListItem></asp:RadioButtonList>
              </P>--%> 
        </div>
        <asp:Image runat="server" ImageURL="~/common/images/PEC-Charts.png" AlternateText="PEC Chart Example" CssClass="chart-image" />
    </div>
    <div class="action-box">
        <p>
            <strong>Would you like to include a Patient Experience of Care Survey in your Initial Assessment Phase?</strong>
        </p>
        <div class="button-box" id="divButtons" runat="server">
            <div class="third"><asp:LinkButton ID="LinkButtonNewSurvey" runat="server" Text=" Include a New Survey " OnClientClick="javascript:return setValue(this, 1);" OnCommand="LinkButtonSave_Click"/></div>
            <div class="third mid" runat="server" id="divPPECAvailable">
                <asp:LinkButton ID="LinkButtonExistingSurvey" runat="server" Text=" Use This Survey " OnClientClick="javascript:return setValue(this, 2);" OnCommand="LinkButtonSave_Click"/>
                <p><asp:Literal runat="server" ID="PECAvailable" Visible="false">You have a PEC Survey that you started on </asp:Literal> <asp:Literal runat="server" ID="PECStartedOn" Visible="false">.</asp:Literal></p>
            </div>
            <div class="third"><asp:LinkButton ID="LinkButtonDoNotIncludeSurvey" runat="server" Text=" Do Not Include a Survey " OnClientClick="javascript:return setValue(this, 3);" OnCommand="LinkButtonSave_Click"  /></div>
        </div>
        <p>
            <asp:Label runat="server" ID="LabelPECStatus" Visible="false"></asp:Label>
        </p>
        <asp:LinkButton ID="ButtonBack" runat="server" Text="Back" PostBackUrl="ModuleSelectionReview.aspx" CssClass="button" Visible="false"></asp:LinkButton>
        <%--<asp:LinkButton ID="LinkButtonSave" runat="server" OnCommand="LinkButtonSave_Click" Text=" Next " OnClientClick="javascript:return validateForm();" CssClass="button" />--%>
    </div>
</asp:Content>

<asp:Content ID="Content4" ContentPlaceHolderID="javascript" runat="server">
    <script>
        function isRadiobuttonSelected(radioButtons) {
            var inputs = radioButtons.getElementsByTagName("input");
            var selected;
            for (var i = 0; i < inputs.length; i++) {
                if (inputs[i].checked) {
                    selected = inputs[i];
                    break;
                }
            }
            return selected;
        }
        function setValue(buttonSelected, optionSelected) {
            var HiddenFieldPECSelection = document.getElementById("<%= HiddenFieldPECSelection.ClientID %>");
            HiddenFieldPECSelection.value = optionSelected;

            var LinkButtonNewSurvey = document.getElementById("<%= LinkButtonNewSurvey.ClientID %>");
            var LinkButtonExistingSurvey = document.getElementById("<%= LinkButtonExistingSurvey.ClientID %>");
            var LinkButtonDoNotIncludeSurvey = document.getElementById("<%= LinkButtonDoNotIncludeSurvey.ClientID %>");
            if (optionSelected == 1) {
                $(LinkButtonNewSurvey).addClass("active");
                $(LinkButtonExistingSurvey).removeClass("active");
                $(LinkButtonDoNotIncludeSurvey).removeClass("active");
            }
            if (optionSelected == 2) {
                $(LinkButtonNewSurvey).removeClass("active");
                $(LinkButtonExistingSurvey).addClass("active");
                $(LinkButtonDoNotIncludeSurvey).removeClass("active");
                
            }
            if (optionSelected == 3) {
                $(LinkButtonNewSurvey).removeClass("active");
                $(LinkButtonExistingSurvey).removeClass("active");
                $(LinkButtonDoNotIncludeSurvey).addClass("active");
                
                //background: url(../images/big_orange_button.png)
            }
            //buttonSelected.innerHTML = buttonSelected.innerHTML + " Selected";

            return true;
        }
        function validateForm() {
            return true;
        }

        //    function validateForm() {
        //        if (document.getElementById(" RadioExistingYesNo.ClientID ") != null) {
        //            RadioExistingYesNo = document.getElementById(" RadioExistingYesNo.ClientID ");
        //            RadioNewYesNo = document.getElementById(" RadioNewYesNo.ClientID ");

        //            if (isRadiobuttonSelected(RadioExistingYesNo) && isRadiobuttonSelected(RadioNewYesNo)) {
        //                return true;
        //            }
        //            alert('Please answer the Yes/No questions.');
        //        }
        //        else {
        //            RadioNewYesNo = document.getElementById(" RadioNewYesNo.ClientID ");

        //            if (isRadiobuttonSelected(RadioNewYesNo)) {
        //                return true;
        //            }
        //            alert('Please answer the Yes/No questions.');
        //        }
        //        return false;
        //    }
        //    function pecPrevious(radioButtons) {
        //        RadioNewYesNo = document.getElementById(" RadioNewYesNo.ClientID ");
        //        var inputs = radioButtons.getElementsByTagName("input");
        //        var selected;
        //        for (var i = 0; i < inputs.length; i++) {
        //            if (inputs[i].checked) {
        //                selected = inputs[i];
        //                break;
        //            }
        //        }
        //        if (selected.value == "1") {
        //            var ainputs = RadioNewYesNo.getElementsByTagName("input");
        //            ainputs[1].checked = true;
        //        }
        //    }
        //    function pecNew(radioButtons) {
        //        if (document.getElementById(" RadioExistingYesNo.ClientID ") != null) {
        //            RadioExistingYesNo = document.getElementById(" RadioExistingYesNo.ClientID ");
        //            var inputs = radioButtons.getElementsByTagName("input");
        //            var selected;
        //            for (var i = 0; i < inputs.length; i++) {
        //                if (inputs[i].checked) {
        //                    selected = inputs[i];
        //                    break;
        //                }
        //            }
        //            if (selected.value == "1") {
        //                var ainputs = RadioExistingYesNo.getElementsByTagName("input");
        //                ainputs[1].checked = true;
        //            }
        //        }
        //    }
    </script>
</asp:Content>