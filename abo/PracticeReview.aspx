﻿<%@ Page Title="" Language="C#" MasterPageFile="~/PIMMasterPage.master" AutoEventWireup="true" CodeFile="PracticeReview.aspx.cs" Inherits="abo_PracticeReview" %>

<asp:Content ID="Content1" ContentPlaceHolderID="head" Runat="Server">
<link type="text/css" href="../common/css/atooltip.css" rel="stylesheet"  media="screen" />
    
<style>

    .table {
        margin-bottom:20px;
    }

    tr.diff-color td, tr.module-name td {
        background-color: #0B91B7;
        color: #fff;
        font-weight:bold;
        border-top:none;
        border-bottom:none;
    }

    tr.module-name td {
        color: #000;
        /*text-shadow: 1px 1px 0 #fff, -1px 1px 0 #fff,1px -1px 0 #fff,-1px -1px 0 #fff;*/
        font-size:1.2em;
    }

    .width-qualind {
        width: 26.5%;
    }

    .width-rationale {
        width: 9%;
    }

    .width-pracassess {
        width:38%;
    }

    .width-response {
        width: 15%;
        text-align:center;
    }

    #PM1 {
        cursor:pointer;
    }
    #iframeModal {
        width:100%;
        height:100%;
        border:none;
    }
    #measureRationale {
        padding-right:0;
    }

</style>
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder1" Runat="Server">
  <%--  <script language="javascript">
        var oPopup = window.createPopup();

        function ButtonClick() {
            var oPopBody = oPopup.document.body;
            oPopBody.style.backgroundColor = "lightyellow";
            oPopBody.style.border = "solid black 1px";
            oPopBody.innerHTML = "Click outside <strong>popup</strong> to close.";
            oPopup.show(100, 100, 180, 40, document.body);
        }

        //window.open('MeasureRationale.aspx?mid=104');


    </script>

     <button onclick="ButtonClick()">Click Me!</button>--%>


<table class="table">
    <tr>
        <th colspan="4">Process Measures</th>
    </tr>
    <asp:ListView runat="server" ID="ListViewProcessMeasures" onitemdatabound="ListViewProcessMeasures_OnItemDataBound">
    <LayoutTemplate>
        <tr class="diff-color" id="divRowModuleName">
            <td class="width-qualind">Quality Indicator</td>
            <td class="width-qualind">Quality Indicator</td>
            <td class="width-rationale">Rationale</td>
            <td class="width-pracassess">My Practice Assessment</td>
        </tr>
        <asp:PlaceHolder runat="server" ID="itemPlaceholder"></asp:PlaceHolder>
    </LayoutTemplate> 
    <ItemTemplate>
        <tr runat="server" id="divRowModuleName" visible="false" class="module-name">
            <td>
                <asp:Literal ID="Literal2" runat="server" Text='<%# Bind("ModuleName") %>'></asp:Literal>
            </td>
            <td></td>
            <td></td>
            <td></td>
        </tr>
        <tr runat="server" id="divRowChart">
            <td>
                <asp:Label ID="LabelMeasureTitle" CssClass="label_highlight" runat="server" Text='<%# Bind("MeasureQualityIndicator") %>'></asp:Label>
                <br /><asp:Literal ID="LiteralModuleName" runat="server" Text='<%# Bind("ModuleName") %>'></asp:Literal>
            </td>
            <td><asp:Label ID="LabelMeasureLongDescription" runat="server" Text='<%# Bind("MeasureLongDescription") %>'></asp:Label></td>
            <td><asp:HyperLink ID="HyperLinkRationale" runat="server">Rationale</asp:HyperLink></td>
            <td>
                <asp:Chart ID="ChartPerformance" runat="server" Width="300" Height="120" >
                <Series>
                    <%--<asp:Series Name="My Data" ChartType="Bar" Color="#914991" CustomProperties="DrawingStyle=Wedge" Palette="Berry" YValueMembers="MeasurePercent" IsValueShownAsLabel="true" LabelForeColor="#000000" LabelBackColor="White" ></asp:Series>--%>                                    
                    <asp:Series Name="Initial Data" ChartType="Bar" Color="#2f5199"  BorderColor="#2f5199"  CustomProperties="DrawingStyle=Wedge" Palette="Berry" YValueMembers="MeasurePercentCurrent" IsValueShownAsLabel="true" LabelForeColor="#000000" LabelBackColor="Transparent">
                    </asp:Series>
                    <asp:Series Name="Peer Data" ChartType="Bar" Color="#ffb200" BorderColor="#ffb200" CustomProperties="DrawingStyle=Wedge" Palette="Berry" YValueMembers="PeerData" IsValueShownAsLabel="true"  LabelForeColor="#000000" LabelBackColor="Transparent" >
                    </asp:Series>
                </Series>
                <ChartAreas>
                    <asp:ChartArea Name="MainChartArea">
                        <AxisX Maximum="3" LineColor="Transparent" LineWidth="1">
                            <MajorGrid Enabled="false" />
                            <LabelStyle IsEndLabelVisible="false" />
                            <CustomLabels>
                            </CustomLabels>
                        </AxisX>
                        <AxisY Maximum="100" Interval="20" />
                    </asp:ChartArea>
                </ChartAreas>
            </asp:Chart>   
            </td>
        </tr>
    </ItemTemplate>
    </asp:ListView>
</table>
                  
<table class="table">
    <tr>
        <th colspan="4">Outcome Measures</th>
    </tr>
    <asp:ListView runat="server" ID="ListViewOutcomeMeasures" onitemdatabound="ListViewOutcomeMeasures_OnItemDataBound">
    <LayoutTemplate>
        <tr class="diff-color" id="divRowModuleName">
            <td class="width-qualind">Quality Indicator</td>
            <td class="width-qualind">Quality Indicator</td>
            <td class="width-rationale">Rationale</a></td>
            <td class="width-pracassess">My Practice Assessment</td>
        </tr>    
            <asp:PlaceHolder runat="server" ID="itemPlaceholder"></asp:PlaceHolder>
        
    </LayoutTemplate>    
    <ItemTemplate>
        <tr runat="server" id="divRowModuleName" visible="false" class="module-name">
            <td>
                <asp:Literal ID="Literal2" runat="server" Text='<%# Bind("ModuleName") %>'></asp:Literal>
            </td>
            <td></td>
            <td></td>
            <td></td>
        </tr>
        <tr runat="server" id="divRowChart">
            <td>
                <asp:Label ID="LabelMeasureTitle" CssClass="label_highlight" runat="server" Text='<%# Bind("MeasureQualityIndicator") %>'></asp:Label>
                <asp:Literal ID="LiteralModuleName" runat="server" Text='<%# Bind("ModuleName") %>'></asp:Literal>
            </td>
            <td><asp:Label ID="LabelMeasureLongDescription" runat="server" Text='<%# Bind("MeasureLongDescription") %>'></asp:Label></td>
            <td><asp:HyperLink ID="HyperLinkRationale" runat="server" >Rationale</asp:HyperLink>
            </td>
            <td>                    
                <asp:Chart ID="ChartPerformance" runat="server" Width="320" Height="120">
                <Series>
                    <asp:Series Name="My Data" ChartType="Bar" Color="#914991" CustomProperties="DrawingStyle=Wedge" Palette="Berry" YValueMembers="MeasurePercent" IsValueShownAsLabel="true" LabelForeColor="#000000" LabelBackColor="Transparent" >
                    </asp:Series>
                </Series>
                <ChartAreas>
                    <asp:ChartArea Name="MainChartArea">
                        <AxisX Maximum="10" LineColor="Transparent" LineWidth="1">
                            <MajorGrid Enabled="false" />
                            <LabelStyle IsEndLabelVisible="false" />
                            <CustomLabels>
                            </CustomLabels>
                        </AxisX>
                        <AxisY Maximum="100" Interval="20" />
                    </asp:ChartArea>
                </ChartAreas>
                </asp:Chart>                    
            </td>
        </tr>
    </ItemTemplate>
</asp:ListView>

</table>
<div class="button-box">
    <asp:LinkButton ID="LinkButtonBack" runat="server" Text="Back"  PostBackUrl="ModuleCompletedToDate.aspx" CssClass="button" />
</div>

</asp:Content>
<asp:Content ID="Content3" ContentPlaceHolderID="javascript" Runat="Server">
</asp:Content>

