﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

public partial class abo_PIMTutorial : System.Web.UI.Page
{
    protected void Page_Load(object sender, EventArgs e)
    {
        ButtonSubmit.PostBackUrl = Request.UrlReferrer.ToString();
        ((PIMMasterPage)Page.Master).SetHeader("Improvement in Medical Practice Platform Tutorial");
    }
}
