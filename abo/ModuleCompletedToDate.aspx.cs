﻿using System;
using System.Collections.Generic;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using NetHealthPIMModel;
using System.Linq;

public partial class abo_ModuleCompletedToDate : BasePage
{
    static string CurrentModuleName = "";
    static bool priorDataExists = true;
    protected void Page_Load(object sender, EventArgs e)
    {
        if (!IsPostBack)
        {
            InitializePage();
        }
    }
    protected void InitializePage()
    {
        string breadCrumb = Constants.BC_MODULES_COMPLETED_TO_DATE;
        ((PIMMasterPage)Page.Master).SetTitle(breadCrumb);
        ((PIMMasterPage)Page.Master).SetStatus(4);
        ((PIMMasterPage)Page.Master).SetHeader("Activities Completed to Date");
        using (NetHealthPIMEntities pim = new NetHealthPIMEntities())
        {
            priorDataExists = true;
            Participant participant = pim.Participant.First(p => p.ParticipantID == ctx.ParticipantID);
            var participantModulesSelected = (from mcd in pim.ModuleCompletedToDate_V
                                              where mcd.CandidateID == participant.CandidateID
                                              orderby mcd.ModuleCategoryName, mcd.ModuleName
                                              select mcd).ToList();
            RepeaterModules.DataSource = participantModulesSelected;
            RepeaterModules.DataBind();
            if (RepeaterModules.Items.Count == 0)
            {
                priorDataExists = false;
                var moduleListPerParticipant = (from mcd in pim.ModuleListPerParticipant_V
                                                  where mcd.CandidateID == participant.CandidateID
                                                orderby mcd.ModuleCategoryName, mcd.ModuleName
                                                  select mcd).ToList();
                RepeaterModules.DataSource = moduleListPerParticipant;
                RepeaterModules.DataBind();
            }
        }
    }
    protected void RepeaterModules_ItemDataBound(Object Sender, RepeaterItemEventArgs e)
    {

        // This event is raised for the header, the footer, separators, and items.

        // Execute the following logic for Items and Alternating Items.
        if (e.Item.ItemType == ListItemType.Item || e.Item.ItemType == ListItemType.AlternatingItem)
        {
            System.Web.UI.HtmlControls.HtmlAnchor hrefModule = (System.Web.UI.HtmlControls.HtmlAnchor)e.Item.FindControl("hrefModule");
            if (priorDataExists)
            {
                ModuleCompletedToDate_V data = (ModuleCompletedToDate_V)((RepeaterItem)e.Item).DataItem;
                System.Web.UI.HtmlControls.HtmlTableRow divRowModuleName = (System.Web.UI.HtmlControls.HtmlTableRow)e.Item.FindControl("divRowModuleName");
                if (CurrentModuleName != data.ModuleCategoryName)
                {
                    divRowModuleName.Visible = true;
                    CurrentModuleName = data.ModuleCategoryName;
                }
                if (data.TimesAbstracted == 0)
                    hrefModule.HRef = "";
                else
                    hrefModule.HRef = "PracticeReview.aspx?ModuleID=" + data.ModuleID.ToString();
            }
            else
            {
                ModuleListPerParticipant_V data = (ModuleListPerParticipant_V)((RepeaterItem)e.Item).DataItem;
                System.Web.UI.HtmlControls.HtmlTableRow divRowModuleName = (System.Web.UI.HtmlControls.HtmlTableRow)e.Item.FindControl("divRowModuleName");
                if (CurrentModuleName != data.ModuleCategoryName)
                {
                    divRowModuleName.Visible = true;
                    CurrentModuleName = data.ModuleCategoryName;
                }
                hrefModule.HRef = "";
            }
        }
    }
}
