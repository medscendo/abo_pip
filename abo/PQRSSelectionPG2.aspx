﻿<%@ Page Title="" Language="C#" MasterPageFile="~/PIMMasterPage.master" AutoEventWireup="true" CodeFile="PQRSSelectionPG2.aspx.cs" Inherits="abo_PQRSSelectionPG2" %>

<asp:Content ID="Content1" ContentPlaceHolderID="head" Runat="Server">

</asp:Content>

<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder1" Runat="Server">
    <div class="main_content" style="padding-right: 100px;">
        <div class="heading_box">
            <h2>
                PQRS Incentive Program</h2>
        </div>
        <div class="imporvenment_plan_details">
            You have chosen to participate in PQRS Registry Reporting, in addition to completing Part IV: Practice Performance Assessment MOC.  This means that you will either submit data on 20 charts the Cataract Measures Group (This chart information will come directly from the 30 charts entered into the ABO PIP. ) or choose <em>3 individual measures</em> from POAG, AMD and/or Diabetic Retinopathy Measures Set. 
            <br />
            <br />Once you have completed chart entry for your chosen PIP, you will seamlessly pass through to the PQRS<strong>PRO</strong> registry to continue and complete the process by January 14, 2014.  
            <br />
            <br />Any questions that you may have about PQRS or registry reporting can be submitted to <a href="mailto:PQRSSupport@nethealthinc.com">PQRSSupport@nethealthinc.com</a>
            <br />
            <br /><strong>Do you still want to take participate in PQRS registry reporting using your Improvement in Medical Practice Activity data?</strong>
            <asp:RadioButtonList ID="RBLParticipatePQRS" RepeatDirection="Horizontal" runat="server">
                <asp:ListItem Value="True" Text="Yes"></asp:ListItem>
                <asp:ListItem Value="False" Text="No"></asp:ListItem>
            </asp:RadioButtonList>
            <br />
            <br /><a href="#" class="button">Submit</a>
        </div>
    </div>
</asp:Content>

