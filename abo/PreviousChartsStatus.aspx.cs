﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using NetHealthPIMModel;

public partial class abo_PreviousChartsStatus : BasePage
{
    protected void Page_Load(object sender, EventArgs e)
    {
        Session[Constants.SESSION_USERCONTEXT] = ctx;

        using (NetHealthPIMEntities pim = new NetHealthPIMEntities())
        {
            int cycleID = ctx.ActiveModuleCycleID;
            ParticipantModuleCycle cycle = pim.ParticipantModuleCycle.First(c => c.ParticipantModuleCycleID == cycleID);

            cycle.PrevParticipantModuleCycleReference.Load();
            ParticipantModuleCycle prev = cycle.PrevParticipantModuleCycle;
            cycle = prev;

            var modulesFirstPhase = (from pms in pim.ParticipantModuleSelection
                                     join m in pim.Module on pms.ModuleID equals m.ModuleID
                                     where pms.ParticipantModuleCycleID == cycle.ParticipantModuleCycleID
                                     select new
                                     {
                                         ModuleID = m.ModuleID,
                                         ModuleName = m.ModuleName,
                                         NumberOfCharts = pms.NumberOfCharts,
                                         AbstractDataCompletedDate = pms.AbstractDataCompletedDate
                                     }).ToList();
            repeaterModules.DataSource = modulesFirstPhase;
            repeaterModules.DataBind();
            PreviousChartsInfo.Visible = true;
            LiteralStatus.Text = "Active";
        }

    }
    protected void LinkButtonSelectModule_Click(object sender, CommandEventArgs e)
    {
        int ModuleID = Convert.ToInt32(e.CommandArgument.ToString());
        Session[Constants.SESSION_WORKINGMODULEID] = ModuleID;
        Response.Redirect("PatientChartRegistration.aspx?CycleNumber=1");

    }
}
