﻿<%@ Page Title="" Language="C#" MasterPageFile="~/PIMMasterPage.master" AutoEventWireup="true" CodeFile="FeedbackReceived.aspx.cs" Inherits="abo_FeedbackReceived" %>

<asp:Content ID="Content1" ContentPlaceHolderID="head" Runat="Server">

    <style>
        .action-box {
            float:left;
            width: 850px;
            margin-bottom:10px;
            text-align:center;
            font-weight:bold;
        }
    </style>

</asp:Content>

<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder1" Runat="Server">

<div class="action-box">
    <p>Thank you for your feedback.</p>
</div>

<div class="button-box">
    <asp:LinkButton ID="ButtonSubmit" runat="server" Text="Back to Status" PostBackUrl="PIPStatus.aspx" CssClass="button" />
</div>

</asp:Content>

