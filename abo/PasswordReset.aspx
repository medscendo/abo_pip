﻿<%@ Page Title="" Language="C#" MasterPageFile="~/PIMMasterPage.master" AutoEventWireup="true" CodeFile="PasswordReset.aspx.cs" Inherits="copd_PasswordReset" %>

<asp:Content ID="Content1" ContentPlaceHolderID="head" Runat="Server">
</asp:Content>
<asp:Content ID="Content3" ContentPlaceHolderID="ContentPlaceHolder1" Runat="Server">

<table border="0" cellpadding="2" class="center_area">
    <tr><td class="content_label">Your E-mail Address:</td>
        <td><asp:Label ID="LabelEMailAddress" runat="server" Text=""></asp:Label></td>
    </tr>
    <tr><td class="content_label">Ener a New Password*:</td>
        <td><asp:TextBox ID="TextPassword" runat="server" MaxLength="20" TextMode="Password" Width="220px" SkinID="RequiredFieldSkin"></asp:TextBox>
        <asp:RegularExpressionValidator ID="RegExpPasswordValidator" runat="server" ControlToValidate="TextPassword"
            ErrorMessage="Invalid Password." Display="Dynamic"
            ToolTip="Password must at least 6 characters and must contain a digit (0-9)."
            ValidationExpression="^(?=.*\d).{6,20}$"></asp:RegularExpressionValidator>
        <asp:RequiredFieldValidator ID="RequiredPassword" runat="server"
            ErrorMessage="Invalid Password." ToolTip="Password must at least 6 characters and must contain a digit (0-9)." 
                ControlToValidate="TextPassword" Display="Dynamic"></asp:RequiredFieldValidator>
        </td>
    </tr>
    <tr><td class="content_label">Confirm Password*:</td>
        <td><asp:TextBox ID="TextPasswordConfirm" runat="server" MaxLength="20" TextMode="Password" Width="220px" SkinID="RequiredFieldSkin"></asp:TextBox>
        <asp:CompareValidator ID="ComparePassword" runat="server" 
            ErrorMessage="Passwords don't match." ToolTip="Passwords don't match."
            ControlToCompare="TextPassword" ControlToValidate="TextPasswordConfirm" Type="String"></asp:CompareValidator>
        </td>
    </tr>
    <tr><td>&nbsp;</td><td><asp:Button ID="ButtonSubmit" runat="server" Text="Submit" 
            onclick="ButtonSubmit_Click" />  
        </td>
    </tr>
</table>

</asp:Content>

