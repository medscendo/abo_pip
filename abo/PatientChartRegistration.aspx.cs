﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using NetHealthPIMModel;
using System.Transactions;
using PIM.Services;

public partial class abo_PatientChartRegistration : BasePage
{
    static bool showChartLink = true;
    static bool stillCycleOne = true;
    public const int ABO_PATHOLOGY1 = 62;
    public const int ABO_PATHOLOGY2 = 63;
    public const int ABO_PATHOLOGY3 = 64;
    public const int ABO_PATHOLOGY4 = 65;
    public bool reportApproved = true;
    static string cycleQSNumber = "1";

    protected void Page_Load(object sender, EventArgs e)
    {
        if (!IsPostBack)
        {
            InitializePage();
        }

    }
    protected void InitializePage()
    {
        ((PIMMasterPage)Page.Master).SetTitle("Patient Chart Registration");
        

        using (NetHealthPIMEntities pim = new NetHealthPIMEntities())
        {
            ParticipantModuleCycle prev = (ParticipantModuleCycle)Session[Constants.SESSION_PREVIOUSCYCLE];

            String CycleNumber = Request.QueryString["CycleNumber"];
            cycleQSNumber = CycleNumber;

            int cycleID = ctx.ActiveModuleCycleID;
            if (CycleNumber == "1")
            {
                stillCycleOne = (cycleID == prev.ParticipantModuleCycleID);
                cycleID = prev.ParticipantModuleCycleID;
            }

            Participant part=(from c in pim.Participant where c.ParticipantID==ctx.ParticipantID select c).FirstOrDefault();
            if(part.SoftwareVersion>=2015)
            {
                LinkButtonChartAbstractionDetail.Visible = false;
                LinkButtonChartAbstractionDetail2.Visible = false;
            }
            ParticipantModuleCycle cycle = pim.ParticipantModuleCycle.First(c => c.ParticipantModuleCycleID == cycleID);
            reportApproved = (cycle.ReportApproved == true);
            
            int ModuleID = (int)Session[Constants.SESSION_WORKINGMODULEID];
            Module module = pim.Module.First(c => c.ModuleID == ModuleID);
            ((PIMMasterPage)Page.Master).SetHeader(module.ModuleName + " Patient Registration");
            

            // check if cycle number 2 and module has not been selected for remeasure. If so, this should be link back to Dashboard.
            if (cycle.CycleNumber == 2)
            {
                var countModuleSelectedForRemeasure = (from pms in pim.ParticipantModuleSelection
                                                       where pms.ParticipantModuleCycleID == cycleID && pms.ModuleID == ModuleID
                                                       select pms).Count();
                if (countModuleSelectedForRemeasure == 0)
                    Response.Redirect("Dashboard3.aspx");
                ((PIMMasterPage)Page.Master).SetStatus(3);
                LinkButtonBackToDashboard.PostBackUrl = "Dashboard3.aspx";
                LiteralAbstractionNumber.Text = "Second Abstraction";

            }
            else
            {
                ((PIMMasterPage)Page.Master).SetStatus(2);
            }

            int chartsIncomplete = 0;
            var modulesNotConfirmed = (from m in pim.Module
                                   join pms in pim.ParticipantModuleSelection on m.ModuleID equals pms.ModuleID
                                   where pms.ParticipantModuleCycleID == cycle.ParticipantModuleCycleID
                                   & (pms.ModuleConfirm == null | pms.ModuleConfirm == false)
                                   & m.ModuleID == ModuleID
                                   select pms).Count();
           

            var ChartRegistration = from m in pim.Module
                                  join cr in pim.ChartRegistration on m.ModuleID equals cr.ModuleID 
                                  where cr.ParticipantModuleCycleID == cycle.ParticipantModuleCycleID
                                  & cr.ModuleID == ModuleID
                                  & cr.Completed == false
                                select new ChartRegistration
                                {
                                    ModuleName = m.ModuleName,
                                    RecordIdentifier = cr.RecordIdentifier,
                                    UserPatientID = cr.UserPatientID,
                                    PatientMeetsCategoryDef = cr.PatientMeetsCategoryDef == null ? -1 : cr.PatientMeetsCategoryDef == true ? 1 : 0,
                                    DirectPatientCareResp = cr.DirectPatientCareResp == null ? -1 : cr.DirectPatientCareResp == true ? 1 : 0,
                                    YearOfBirth = cr.YearOfBirth == null ? 0 : (int)cr.YearOfBirth,
                                    InitialVisit = cr.InitialVisit,
                                    LastVisit = cr.LastVisit,
                                    DOB = cr.DOB,
                                    DateSpecimenReceived = cr.DateSpecimenReceived
                                };
            ListViewRow.DataSource = ChartRegistration;
            ListViewRow.DataBind();
            if (ListViewRow.Items.Count == 0)
            {
                divAdditionalChartData.Visible = false;
                LinkButtonInsufficientCharts.Visible = false;
                ButtonSubmit.Visible = false;
                ((PIMMasterPage)Page.Master).SetHeader(module.ModuleName + " Registered Patients");
                ((PIMMasterPage)Page.Master).SetTitle("");

            }
            else 
            {
                if (Request.QueryString["show"] != null)
                {
                    if (Request.QueryString["show"].ToString() == "val")
                    {
                        LabelError.Visible = true;
                    }
                }
                if (cycle.CycleNumber == 1)
                    LinkButtonBackToMain.Visible = true;
            }

           
            ParticipantModuleSelection PMS = pim.ParticipantModuleSelection.First(c => c.ParticipantModuleCycleID == cycleID && c.ModuleID == ModuleID);

            var missingChartRegistration = (from cr in pim.ChartRegistration
                                            where cr.ParticipantModuleCycleID == cycle.ParticipantModuleCycleID
                                             & cr.ModuleID == ModuleID
                                             & cr.Completed == false
                                            select cr).Count();
            if ((missingChartRegistration > 0) || (chartsIncomplete > 0) || (modulesNotConfirmed > 0) || (PMS.SelfAssessmentSurveyComplete != true) || (PMS.SystemSurveyComplete != true))
            {
                LinkButtonBackToDashboard.Visible = false;
                
                if ((missingChartRegistration == 0) && (PMS.SelfAssessmentSurveyComplete != true))
                {
                    ButtonSelfAssesment.Visible = true;
                    LinkButtonBackToDashboard.Visible = true;
                }
                showChartLink = false;
            }
            else
            {
                LinkButtonBackToDashboard.Visible = true;
                showChartLink = true;
            }

            var chartCompletedRegistration = (from m in pim.Module
                                             join cr in pim.ChartRegistration on m.ModuleID equals cr.ModuleID
                                             where cr.ParticipantModuleCycleID == cycle.ParticipantModuleCycleID
                                             & cr.ModuleID == ModuleID
                                             & cr.Completed != false
                                             select new chartRegistrationComplete
                                             {
                                                 ModuleName = m.ModuleName,
                                                 RecordIdentifier = cr.RecordIdentifier,
                                                 UserPatientID = cr.UserPatientID,
                                                 PatientMeetsCategoryDef = cr.PatientMeetsCategoryDef == null ? "" : cr.PatientMeetsCategoryDef == true ? "Yes" : "No",
                                                 DirectPatientCareResp = cr.DirectPatientCareResp == null ? "" : cr.DirectPatientCareResp == true ? "Yes" : "No",
                                                 YearOfBirth = cr.YearOfBirth == null ? null : cr.YearOfBirth,
                                                 InitialVisit = cr.InitialVisit,
                                                 ModuleVersion=m.ModuleVersion,
                                                 LastVisit = cr.LastVisit,
                                                 DOB = cr.DOB,
                                                 DateSpecimenReceived = cr.DateSpecimenReceived,
                                                 LinkToChart = (chartsIncomplete == 0 & modulesNotConfirmed == 0) ? "Charts/" + m.ModuleShortName + "Chart.aspx?ri=" + cr.RecordIdentifier : "#",
                                                 ChartStatus = cr.AbstractCompleted == true ? "Completed" : (cr.Completed == true & cr.AbstractCompleted == false) ? "In Progress" : "Not Started",
                                                 ColorChartStatus = cr.AbstractCompleted == true ? "green" : (cr.Completed == true & cr.AbstractCompleted == false) ? "orange" : "blue",
                                                 ModuleShortName = m.ModuleShortName,
                                                 PreviousChartData = cr.PreviousChartData == null ? false : cr.PreviousChartData
                                             }).ToList();

          
            foreach (var item in chartCompletedRegistration)
            {
                if (item.ModuleVersion == 0)
                {
                    item.LinkToChart = "ChartAbstraction.aspx?ri=" + item.RecordIdentifier;
                }
            }
            


            ListViewCompletedRow.DataSource = chartCompletedRegistration;
            ListViewCompletedRow.DataBind();

            if (ListViewCompletedRow.Items.Count == 0)
                divRegisteredChartData.Visible = false;
            else
            {
                for (var i = 0; i < ListViewCompletedRow.Items.Count; i++)
                {
                    HyperLink HyperLinkLinkToChart = (HyperLink)ListViewCompletedRow.Items[i].FindControl("HyperLinkLinkToChart");
                    HyperLinkLinkToChart.NavigateUrl = HyperLinkLinkToChart.NavigateUrl + "&nr=" + (i+1).ToString() + "&t=" + ListViewCompletedRow.Items.Count;
                }
            }

            if ((modulesNotConfirmed > 0) && (missingChartRegistration == 0))
            {
                LinkButtonNextModule.Visible = true;
            }

            // current module chart registration has been completed but other module(s) are not completed
            if ((modulesNotConfirmed == 0) && (missingChartRegistration == 0) && (chartsIncomplete>0))
            {
                // get next module
                //Module moduleDetails = pim.Module.First(m => m.ModuleID == ModuleID);
                Module moduleNext = (from m in pim.Module
                                                join cr in pim.ChartRegistration on m.ModuleID equals cr.ModuleID
                                                where cr.ParticipantModuleCycleID == cycle.ParticipantModuleCycleID
                                                & cr.Completed == false
                                                select m).First<Module>();
                LinkButtonNextModule.Visible = true;
                LinkButtonNextModule.Text = "Register Charts For " + moduleNext.ShortModuleDescription;
                LinkButtonNextModule.PostBackUrl = "OpenModule.aspx?mid="+ moduleNext.ModuleID.ToString();
            }

            if (missingChartRegistration == 0)
            {
                divChartEntered.Visible = false;
                divChartAbstraction.Visible = true;
             
            }

            // if it is a pathology module, change last two fields.
            if ((ModuleID == ABO_PATHOLOGY1) || (ModuleID == ABO_PATHOLOGY2) || (ModuleID == ABO_PATHOLOGY3) ||(ModuleID == ABO_PATHOLOGY4))
            {
                // set Pathology module
                HiddenFieldPathology.Value = "1";
                LabelInitialVisit.InnerHtml = "Month/Year<br />Specimen received";
                LabelLastVisit.Visible = false;
                LabelInitialVisitDone.InnerHtml = "Month/Year<br />Specimen received";
                LabelLastVisitDone.Visible = false;
                for (var i = 0; i <= ListViewRow.Items.Count - 1; i++)
                {
                    ((System.Web.UI.HtmlControls.HtmlTableCell)ListViewRow.Items[i].FindControl("divTextBoxInitialVisit")).Visible = false;
                    ((System.Web.UI.HtmlControls.HtmlTableCell)ListViewRow.Items[i].FindControl("divTextBoxLastVisit")).Visible = false;
                    ((System.Web.UI.HtmlControls.HtmlTableCell)ListViewRow.Items[i].FindControl("divTextBoxDateSpecimenReceived")).Visible = true;
                   
                }
                for (var i = 0; i <= ListViewCompletedRow.Items.Count - 1; i++)
                {
                    ((System.Web.UI.HtmlControls.HtmlTableCell)ListViewCompletedRow.Items[i].FindControl("divTextBoxInitialVisit")).Visible = false;
                    ((System.Web.UI.HtmlControls.HtmlTableCell)ListViewCompletedRow.Items[i].FindControl("divTextBoxLastVisit")).Visible = false;
                    ((System.Web.UI.HtmlControls.HtmlTableCell)ListViewCompletedRow.Items[i].FindControl("divTextBoxDateSpecimenReceived")).Visible = true;
                  
                }
                
            }

            Module moduleDetails = pim.Module.First(m => m.ModuleID == ModuleID);
            LiteralModuleName.Text =  moduleDetails.ModuleName;
            LiteralModuleCriteria.Text = moduleDetails.ModuleCriteria;
            HiddenFieldRegistrationMinAge.Value = moduleDetails.RegistrationMinAge.ToString();
            HiddenFieldRegistrationMaxAge.Value = moduleDetails.RegistrationMaxAge.ToString();
            HiddenFieldRegistrationInitialVisitMonths.Value = moduleDetails.RegistrationInitialVisitMonths.ToString();
            HiddenFieldRegistrationFollowUpVisitMonths.Value = moduleDetails.RegistrationFollowUpVisitMonths.ToString();
            HiddenFieldRegistrationLastVisitMonths.Value = moduleDetails.RegistrationLastVisitMonths.ToString();

            LinkButtonChartAbstractionDetail.OnClientClick = "window.open('../documents/" + moduleDetails.ShortModuleDescription + "ChartAbstraction.PDF');return false;";
            LinkButtonChartAbstractionDetail2.OnClientClick = "window.open('../documents/" + moduleDetails.ShortModuleDescription + "ChartAbstraction.PDF');return false;";


            if (cycle.CycleNumber == 2)
            {
                LinkButtonInsufficientCharts.Visible = false;
                LinkButtonBackToDashboard.Visible = true;
            }
        }
    }
    protected void ButtonSubmit_Click(object sender, EventArgs e)
    {
        using (NetHealthPIMEntities pim = new NetHealthPIMEntities())
        {
            int cycleID = ctx.ActiveModuleCycleID;
            ParticipantModuleCycle cycle = pim.ParticipantModuleCycle.First(c => c.ParticipantModuleCycleID == cycleID);

            int ModuleID = (int)Session[Constants.SESSION_WORKINGMODULEID];
            for (int i = 0; i <= ListViewRow.Items.Count-1; i++)
            {
                string RecordIdentifier = ((HiddenField)ListViewRow.Items[i].FindControl("HiddenFieldRecordIdentifier")).Value;
                string UserPatientID = ((TextBox)ListViewRow.Items[i].FindControl("TextBoxUserPatientID")).Text;
               
                
                string DOB = ((TextBox)ListViewRow.Items[i].FindControl("TextBoxDOB")).Text;
                string InitialVisit = ((TextBox)ListViewRow.Items[i].FindControl("TextBoxInitialVisit")).Text;
                string LastVisit = ((TextBox)ListViewRow.Items[i].FindControl("TextBoxLastVisit")).Text;
                string DateSpecimenReceived = ((TextBox)ListViewRow.Items[i].FindControl("TextBoxDateSpecimenReceived")).Text;

                using (TransactionScope transaction = new TransactionScope())
                {
                    var chartData = pim.ChartRegistration.First(cr => cr.ParticipantModuleCycleID == cycle.ParticipantModuleCycleID & cr.ModuleID == ModuleID & cr.RecordIdentifier == RecordIdentifier);
                    chartData.UserPatientID = UserPatientID;
                    chartData.DOB = DOB;
                    if (chartData.DOB != "")
                    {
                        string[] arr = DOB.Split('/');
                        string _month = "00" + arr[0];
                        chartData.DOB = (_month).Substring(_month.Length - 2) + "/" + arr[1];
                        ((TextBox)ListViewRow.Items[i].FindControl("TextBoxDOB")).Text = chartData.DOB;
                    }
                    chartData.InitialVisit = InitialVisit;
                    if (chartData.InitialVisit != "")
                    {
                        string[] arr = InitialVisit.Split('/');
                        string _month = "00" + arr[0];
                        chartData.InitialVisit = (_month).Substring(_month.Length - 2) + "/" + arr[1];
                        ((TextBox)ListViewRow.Items[i].FindControl("TextBoxInitialVisit")).Text = chartData.InitialVisit;
                    }
                    chartData.LastVisit = LastVisit;
                    if (chartData.LastVisit != "")
                    {
                        string[] arr = LastVisit.Split('/');
                        string _month = "00" + arr[0];
                        chartData.LastVisit = (_month).Substring(_month.Length - 2) + "/" + arr[1];
                        ((TextBox)ListViewRow.Items[i].FindControl("TextBoxLastVisit")).Text = chartData.LastVisit;
                    }
                    chartData.DateSpecimenReceived = DateSpecimenReceived;
                    if (chartData.DateSpecimenReceived != "")
                    {
                        string[] arr = DateSpecimenReceived.Split('/');
                        string _month = "00" + arr[0];
                        chartData.DateSpecimenReceived = (_month).Substring(_month.Length - 2) + "/" + arr[1];
                        ((TextBox)ListViewRow.Items[i].FindControl("TextBoxDateSpecimenReceived")).Text = chartData.DateSpecimenReceived;
                    } 
                    var existingPatientUserID = (from cr in pim.ChartRegistration
                                                 where cr.ParticipantModuleCycleID == cycle.ParticipantModuleCycleID
                                                  & cr.ModuleID == ModuleID
                                                  & cr.UserPatientID == UserPatientID
                                                  & cr.RecordIdentifier != RecordIdentifier
                                                 select cr).Count();
                    // validate dates
                    bool areDatesValid = true;
                    DateTime newDOB = DateTime.MaxValue;
                    DateTime newInitialVisit = DateTime.MaxValue;
                    DateTime newLastVisit = DateTime.MaxValue;
                    DateTime newDateSpecimenReceived = DateTime.MaxValue;

                    if (chartData.DOB != "")
                    {
                        string[] arr = chartData.DOB.Split('/');
                        newDOB = new DateTime(Convert.ToInt32(arr[1]), Convert.ToInt32(arr[0]), 1);
                    }

                    if (chartData.InitialVisit != "")
                    {
                        string[] arr = chartData.InitialVisit.Split('/');
                        newInitialVisit = new DateTime(Convert.ToInt32(arr[1]), Convert.ToInt32(arr[0]), 1);
                    }

                    if (chartData.LastVisit != "")
                    {
                        string[] arr = chartData.LastVisit.Split('/');
                        newLastVisit = new DateTime(Convert.ToInt32(arr[1]), Convert.ToInt32(arr[0]), 1);
                    }
                    if (chartData.DateSpecimenReceived != "")
                    {
                        string[] arr = chartData.DateSpecimenReceived.Split('/');
                        newDateSpecimenReceived = new DateTime(Convert.ToInt32(arr[1]), Convert.ToInt32(arr[0]), 1);
                        if (newDOB > newDateSpecimenReceived)
                        {
                            areDatesValid = false;
                        }
                    }

                    if ((newDOB != DateTime.MaxValue) && (newInitialVisit != DateTime.MaxValue) && (newLastVisit != DateTime.MaxValue))
                    {
                        

                        int RegistrationMinAge = 0;
                        int RegistrationMaxAge = 120;
                        int InitialVisitMonths = 0;
                        int FollowUpVisitMonths = 0;
                        int LastVisitMonths = 0;
                        Module moduleDetails = pim.Module.First(m => m.ModuleID == ModuleID);
                        if (moduleDetails.RegistrationMinAge != null)
                            RegistrationMinAge = (int)moduleDetails.RegistrationMinAge;
                        else
                            RegistrationMinAge = 0;
                        if (moduleDetails.RegistrationMaxAge != null)
                            RegistrationMaxAge = (int)moduleDetails.RegistrationMaxAge;
                        else
                            RegistrationMaxAge = 0;
                        if (moduleDetails.RegistrationInitialVisitMonths != null)
                            InitialVisitMonths = (int)moduleDetails.RegistrationInitialVisitMonths;
                        else
                            InitialVisitMonths = 0;
                        if (moduleDetails.RegistrationFollowUpVisitMonths != null)
                            FollowUpVisitMonths = (int)moduleDetails.RegistrationFollowUpVisitMonths;
                        else
                            FollowUpVisitMonths = 0;
                        if (moduleDetails.RegistrationFollowUpVisitMonths != null)
                            LastVisitMonths = (int)moduleDetails.RegistrationLastVisitMonths;
                        else
                            LastVisitMonths = 0;
                        var TodayDate = DateTime.Today;
                        var CurrentDate = new DateTime(TodayDate.Year, TodayDate.Month, 1);

                        if (newDOB > newInitialVisit)
                        {
                            areDatesValid = false;
                        }
                        if (newDOB > newLastVisit)
                        {
                            areDatesValid = false;
                        }

                        DateTime VisitFrom = newInitialVisit.AddMonths(FollowUpVisitMonths);

                        DateTime InitialVisitFrom = newDOB.AddMonths(RegistrationMinAge * 12);
                        int maxAge = RegistrationMaxAge;
                        if (maxAge == 0)
                            maxAge = 120;
                        DateTime InitialVisitTo = newDOB.AddMonths(maxAge * 12);

                        DateTime LastVisitFrom = new DateTime(newInitialVisit.Year, newInitialVisit.Month, 1).AddMonths(FollowUpVisitMonths);
                        CurrentDate = CurrentDate.AddMonths(-InitialVisitMonths);
                        DateTime InitialVisitValidFrom = new DateTime(CurrentDate.Year, CurrentDate.Month, 1);

                        if (newInitialVisit > newLastVisit)
                        {
                            areDatesValid = false;
                        }

                        if ((newInitialVisit < InitialVisitFrom) || (newInitialVisit > InitialVisitTo))
                        {
                            areDatesValid = false;
                        }

                        if ((InitialVisitMonths > 0) && (newInitialVisit < InitialVisitValidFrom))
                        {
                            areDatesValid = false;
                        }

                        if (LastVisitFrom > newLastVisit)
                        {
                            areDatesValid = false;
                        }

                        if (newLastVisit > DateTime.Today)
                        {
                            areDatesValid = false;
                        }

                        DateTime LastVisitValidFrom = new DateTime(TodayDate.Year, TodayDate.Month, 1).AddMonths(-LastVisitMonths);

                        if (LastVisitMonths > 0)
                        { // only validate field if the value is greater than 0
                            if (LastVisitValidFrom > newLastVisit)
                            {
                                areDatesValid = false;
                            }
                        }

                    }
                    if ((ModuleID == ABO_PATHOLOGY1) || (ModuleID == ABO_PATHOLOGY2) || (ModuleID == ABO_PATHOLOGY3) || (ModuleID == ABO_PATHOLOGY4))
                    {
                        if ((UserPatientID != "") && (existingPatientUserID == 0) && (DOB != "") && (DateSpecimenReceived != "") && (areDatesValid == true))
                        {
                            chartData.InitialVisit = "1/1800";
                            chartData.LastVisit = "1/1800";
                            chartData.Completed = true;
                            chartData.CompletedDate = DateTime.Today;
                        }
                        else
                        {
                            chartData.Completed = false;
                        }
                    }
                    else
                    {
                        if ((UserPatientID != "") && (existingPatientUserID == 0) &&  (DOB != "") && (InitialVisit != "") && (LastVisit != "") && (areDatesValid == true))
                        {
                            chartData.Completed = true;
                            chartData.CompletedDate = DateTime.Today;
                        }
                        else
                        {
                            chartData.Completed = false;
                        }
                    }
                    pim.SaveChanges();
                    transaction.Complete();
                }
            }
            var missingRegistration = (from pcs in pim.ParticipantChartStatus_V
                                       where pcs.ParticipantModuleCycleID == cycle.ParticipantModuleCycleID
                                        & pcs.PatientChartCompleted != pcs.TotalCharts
                                       select pcs).Count();
            if (missingRegistration == 0)
            {
                if (cycle.ChartRegistrationComplete != true)
                {
                    cycle.ChartRegistrationComplete = true;
                    cycle.ChartRegistrationCompletedDate = DateTime.Today;
                    if (cycle.ModuleSelectionComplete != true) // automatic confirmation if all charts have been completed.
                    {
                        cycle.ModuleSelectionComplete = true;
                        cycle.ModuleSelectionCompletedDate = DateTime.Today;
                    }
                }
            }
            else
            {
                cycle.ChartRegistrationComplete = false;
                cycle.ChartRegistrationCompletedDate = null;
            }
            pim.SaveChanges();

        }

        Response.Redirect("PatientChartRegistration.aspx?show=val");
        //InitializePage();
    }
    protected void ChangeRegistration_Click(object sender,  CommandEventArgs e)
    {
        string recordIdentifier = e.CommandArgument.ToString();
        string AbstractionTable = "";
        using (NetHealthPIMEntities pim = new NetHealthPIMEntities())
        {
            var countChartRegistration = (from cr in pim.ChartRegistration
                                            where cr.ParticipantModuleCycleID == ctx.ActiveModuleCycleID
                                             & cr.RecordIdentifier == recordIdentifier
                                            select cr).Count();
            if (countChartRegistration > 0)
            {

                var currentChartRegistration = pim.ChartRegistration.First(c => c.ParticipantModuleCycleID == ctx.ActiveModuleCycleID & c.RecordIdentifier == recordIdentifier);
                int ModuleID = currentChartRegistration.ModuleID;
                pim.DeleteObject(currentChartRegistration);

                var lastRecordIdentifier = (from c in pim.ChartRegistration
                         where c.ParticipantModuleCycleID == ctx.ActiveModuleCycleID
                          & c.ModuleID == ModuleID
                         select c.RecordIdentifier).Max();
                string prefixRecordIdentifier = lastRecordIdentifier.Substring(0, lastRecordIdentifier.Length - 5);
                string lastnumberRecordIdentifier = lastRecordIdentifier.Substring(lastRecordIdentifier.Length - 5, 5);
                string newRecordIdentifier = "0000" + (Convert.ToInt32(lastnumberRecordIdentifier) + 1).ToString();
                newRecordIdentifier = prefixRecordIdentifier + newRecordIdentifier.Substring(newRecordIdentifier.Length - 5, 5);
                NetHealthPIMModel.ChartRegistration chartRegistration = new  NetHealthPIMModel.ChartRegistration();
                Module module = pim.Module.First(c => c.ModuleID == ModuleID);
                ParticipantModuleCycle cycle = pim.ParticipantModuleCycle.First(c => c.ParticipantModuleCycleID == ctx.ActiveModuleCycleID);
                chartRegistration.ParticipantModuleCycle = cycle;
                chartRegistration.RecordIdentifier = newRecordIdentifier;
                chartRegistration.Module = module;
                chartRegistration.Completed = false;
           
                pim.AddToChartRegistration(chartRegistration);
                
                if (module.ChartAbstrationTable != null)
                {
                    AbstractionTable = module.ChartAbstrationTable;
                    CycleManager.DeleteChartRegistration(ctx.ActiveModuleCycleID, recordIdentifier, AbstractionTable);
                }
                pim.SaveChanges();
                IChartService chserv = new ChartService();
                chserv.DeleteCharts(ctx.ParticipantID, ctx.ActiveModuleCycleID, ctx.ActiveModuleID, recordIdentifier);

            }
            Response.Redirect(Request.Url.AbsoluteUri);
        }
    }

    protected void ButtonSelfAssesment_Click(object sender, EventArgs e)
    {
        using (NetHealthPIMEntities pim = new NetHealthPIMEntities())
        {
            int cycleID = ctx.ActiveModuleCycleID;
            ParticipantModuleCycle cycle = pim.ParticipantModuleCycle.First(c => c.ParticipantModuleCycleID == cycleID);

            int ModuleID = (int)Session[Constants.SESSION_WORKINGMODULEID];
            var missingChartRegistration = (from m in pim.Module
                                              join cr in pim.ChartRegistration on m.ModuleID equals cr.ModuleID
                                              where cr.ParticipantModuleCycleID == cycle.ParticipantModuleCycleID
                                              & cr.ModuleID == ModuleID
                                              & cr.Completed == false
                                              select m).Count();
            if (missingChartRegistration == 0) // done?
            {
                var currentModule = (from m in pim.Module
                                     where m.ModuleID == ModuleID
                                     select m).First();
                if (cycleQSNumber == "1") 
                    Response.Redirect("SelfAssessment.aspx?CycleNumber=1");
                else
                    Response.Redirect("SelfAssessment.aspx");
            }
         }
    }

    protected void ListViewRow_OnDataBinding(object sender, EventArgs e)
    {
        //((DropDownList)ListViewRow.Items[0].FindControl("DropDownListPatientMeetsCategoryDef")).SelectedValue = "1";
    }

    protected void databin(object sender, EventArgs e)
    {

    }
    protected void dataout(object sender, EventArgs e)
    {

    }

    protected void ListViewCompletedRow_OnItemDataBound(object sender, ListViewItemEventArgs e)
    {
        String CycleNumber = Request.QueryString["CycleNumber"];
        if (e.Item.ItemType == ListViewItemType.DataItem)
        {
            chartRegistrationComplete cR = (chartRegistrationComplete)((ListViewDataItem)e.Item).DataItem;

            Literal LiteralLinkToChart = (Literal)e.Item.FindControl("LiteralLinkToChart");
            HyperLink HyperLinkLinkToChart = (HyperLink)e.Item.FindControl("HyperLinkLinkToChart");
            LinkButton LinkButtonChangeRegistration = (LinkButton)e.Item.FindControl("LinkButtonChangeRegistration");

            if (reportApproved)
                LinkButtonChangeRegistration.Visible = false;

            if (showChartLink == true)
            {
                LiteralLinkToChart.Visible = false;
                HyperLinkLinkToChart.Visible = true;
                if (((CycleNumber == "1") && (stillCycleOne==false)) ||(cR.PreviousChartData==true))
                    HyperLinkLinkToChart.NavigateUrl = HyperLinkLinkToChart.NavigateUrl + "&CycleNumber=1";
                if ((cR.PreviousChartData == true)) // (cR.ChartStatus == "Completed") || 
                        LinkButtonChangeRegistration.Visible = false;

            }
            else
            {
                LiteralLinkToChart.Visible = false;
                HyperLinkLinkToChart.Visible = true;
                if (cR.PreviousChartData == true)
                {
                    HyperLinkLinkToChart.NavigateUrl = HyperLinkLinkToChart.NavigateUrl + "&CycleNumber=1";
                    LinkButtonChangeRegistration.Visible = false;
                }
                else
                {
                    LiteralLinkToChart.Visible = true;
                    HyperLinkLinkToChart.Visible = false;
                }

            }
        }
    }
    protected void ListViewRow_OnItemDataBound(object sender, ListViewItemEventArgs e)
    {
      
       
        if (e.Item.ItemType == ListViewItemType.DataItem)
        {
            // Display the e-mail address in italics.
         
            
          
            var dr = ((ListViewDataItem)e.Item).DataItem;
            ChartRegistration cR = (ChartRegistration)((ListViewDataItem)e.Item).DataItem;
    
           

            string stringError = "";
            System.Web.UI.HtmlControls.HtmlGenericControl divError = (System.Web.UI.HtmlControls.HtmlGenericControl)e.Item.FindControl("divError");

            if (cR.UserPatientID != "")
            {
                using (NetHealthPIMEntities pim = new NetHealthPIMEntities())
                {
                    int cycleID = ctx.ActiveModuleCycleID;
                    ParticipantModuleCycle cycle = pim.ParticipantModuleCycle.First(c => c.ParticipantModuleCycleID == cycleID);

                    int ModuleID = (int)Session[Constants.SESSION_WORKINGMODULEID];
                    var existingPatientUserID = (from cr in pim.ChartRegistration
                                                 where cr.ParticipantModuleCycleID == cycle.ParticipantModuleCycleID
                                                  & cr.ModuleID == ModuleID
                                                  & cr.UserPatientID == cR.UserPatientID
                                                  & cr.RecordIdentifier != cR.RecordIdentifier
                                                 select cr).Count();
                    if (existingPatientUserID>0) {
                        stringError = "duplicate,";
                    }
                }
            }
            if (cR.PatientMeetsCategoryDef == 0)
            {
                stringError = stringError.Trim() + " does not meet Category,";
            }
            if (cR.DirectPatientCareResp == 0)
            {
                stringError = stringError.Trim() + " does not meet Direct Patient Care,";
            }
            if (stringError == "")
            {
                int ModuleID = (int)Session[Constants.SESSION_WORKINGMODULEID];
                if ((ModuleID == ABO_PATHOLOGY1) || (ModuleID == ABO_PATHOLOGY2) || (ModuleID == ABO_PATHOLOGY3) || (ModuleID == ABO_PATHOLOGY4))
                {
                    if (((cR.UserPatientID != "") && (cR.UserPatientID != null)) || (cR.PatientMeetsCategoryDef != -1) || (cR.DirectPatientCareResp != -1) || ((cR.DOB != "") && (cR.DOB != null)) || ((cR.DateSpecimenReceived != "") && (cR.DateSpecimenReceived != null)) )
                    {
                        stringError = "is not completed.";
                    }
                }
                else
                {
                    if (((cR.UserPatientID != "") && (cR.UserPatientID != null)) || (cR.PatientMeetsCategoryDef != -1) || (cR.DirectPatientCareResp != -1) || ((cR.DOB != "") && (cR.DOB != null)) || ((cR.InitialVisit != "") && (cR.InitialVisit != null)) || ((cR.LastVisit != "") && (cR.LastVisit != null)))
                    {
                        stringError = "is not completed.";
                    }
                }
            }

            DateTime DOB = DateTime.MaxValue;
            DateTime InitialVisit = DateTime.MaxValue;
            DateTime LastVisit = DateTime.MaxValue;

            if (cR.DOB != "")
            {
                try
                {
                    string[] arr = cR.DOB.Split('/');
                    DOB = new DateTime(Convert.ToInt32(arr[1]), Convert.ToInt32(arr[0]), 1);
                }
                catch (Exception ex) { }
            }

            if (cR.InitialVisit != "")
            {
                try
                {
                string[] arr = cR.InitialVisit.Split('/');
                InitialVisit = new DateTime(Convert.ToInt32(arr[1]), Convert.ToInt32(arr[0]), 1);
                }
                catch (Exception ex) { }
            }

            if (cR.LastVisit != "")
            {
                try
                {
                string[] arr = cR.LastVisit.Split('/');
                LastVisit = new DateTime(Convert.ToInt32(arr[1]), Convert.ToInt32(arr[0]), 1);
                }
                catch (Exception ex) { }
            }

            if ((DOB != DateTime.MaxValue) && (InitialVisit != DateTime.MaxValue) && (LastVisit != DateTime.MaxValue))
            {
                int ModuleID = (int)Session[Constants.SESSION_WORKINGMODULEID];
                int RegistrationMinAge = 0;
                int RegistrationMaxAge = 120;
                int InitialVisitMonths = 0;
                int FollowUpVisitMonths = 0;
                int LastVisitMonths = 0;
                using (NetHealthPIMEntities pim = new NetHealthPIMEntities())
                {
                    Module moduleDetails = pim.Module.First(m => m.ModuleID == ModuleID);

                    RegistrationMinAge = (int)moduleDetails.RegistrationMinAge;
                    RegistrationMaxAge = (int)moduleDetails.RegistrationMaxAge;
                    InitialVisitMonths = (int)moduleDetails.RegistrationInitialVisitMonths;
                    FollowUpVisitMonths = (int)moduleDetails.RegistrationFollowUpVisitMonths;
                    LastVisitMonths = (int)moduleDetails.RegistrationLastVisitMonths;
                }

                var TodayDate = DateTime.Today;
                var CurrentDate = new DateTime(TodayDate.Year, TodayDate.Month, 1);

                if (DOB > InitialVisit)
                {
                    stringError = " Initial Visit cannot be before Date of Birth,";
                }
                if (DOB > LastVisit)
                {
                    stringError = stringError + " Last Visit cannot be before Date of Birth,";
                }

                DateTime VisitFrom = InitialVisit.AddMonths(FollowUpVisitMonths);

                DateTime InitialVisitFrom = DOB.AddMonths(RegistrationMinAge*12);
                int maxAge = RegistrationMaxAge;
                if (maxAge == 0)
                    maxAge = 120;
                DateTime InitialVisitTo = DOB.AddMonths(maxAge * 12);

                DateTime LastVisitFrom = new DateTime(InitialVisit.Year, InitialVisit.Month, 1).AddMonths(FollowUpVisitMonths);
                CurrentDate = CurrentDate.AddMonths(-InitialVisitMonths);
                DateTime InitialVisitValidFrom = new DateTime(CurrentDate.Year, CurrentDate.Month, 1);

                
                if (InitialVisit > LastVisit)
                {
                    stringError = stringError + " Initial Visit cannot be before Last Visit,";
                }

                if ((InitialVisit < InitialVisitFrom) || (InitialVisit > InitialVisitTo)) {
                    if (RegistrationMaxAge==0)
                        stringError = stringError + " Patient should be older than " + RegistrationMinAge + " years old,";
                    else
                        stringError = stringError + " Patient should be between " + RegistrationMinAge + " to " + RegistrationMaxAge + " years old,";
                }

                if ((InitialVisitMonths > 0) && (InitialVisit < InitialVisitValidFrom))
                {
                    stringError = stringError + " Initial Visit cannot be before " + InitialVisitValidFrom.ToShortDateString() + ".\n";
                }

                if (LastVisitFrom > LastVisit)
                {
                    stringError = stringError + " Last Visit should be after " + LastVisitFrom.ToShortDateString() + ",";
                }

                if (LastVisit > DateTime.Today)
                {
                    stringError = stringError + " Last Visit date is invalid, you must pick a date that has already occurred,";
                }

                DateTime LastVisitValidFrom = new DateTime(TodayDate.Year, TodayDate.Month, 1).AddMonths(-LastVisitMonths);

                if (LastVisitMonths > 0)
                { // only validate field if the value is greater than 0
                    if (LastVisitValidFrom > LastVisit)
                    {
                        stringError = stringError + "Last Visit should be at least once in the past " + LastVisitMonths + " months.";
                    }
                }

            }

            if (stringError != "")
            {
                divError.InnerText = "* Patient " + stringError.Trim().TrimEnd(',');
                divError.Visible = true;
            }

        }
    }

}
