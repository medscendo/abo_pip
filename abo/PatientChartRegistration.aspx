﻿<%@ Page Title="" Language="C#" MasterPageFile="~/PIMMasterPage.master" AutoEventWireup="true" CodeFile="PatientChartRegistration.aspx.cs" Inherits="abo_PatientChartRegistration" %>

<asp:Content ID="Content1" ContentPlaceHolderID="head" Runat="Server">
    <style type="text/css">
        .odd td {
            background-color: #E5F2F8 !important;
        }

        .table th {
            font-size: 0.6em
        }

        .button-box {
            float:left;
        }

        .mid-page {
            margin: 0 0 20px;
        }

        .small-text {
            font-size:0.9em;
        }

        .centered {
            text-align:center;
        }

        .record {
            border: solid 1px blue;
            padding:10px;
        }

        .tableHeaders {
            margin-bottom:0;
            font-size: 20px;
            color: rgb(1, 54, 113);
        }

    </style>
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder1" Runat="Server">
    <asp:HiddenField ID="HiddenFieldRegistrationMinAge" runat="server"/>
    <asp:HiddenField ID="HiddenFieldRegistrationMaxAge" runat="server"/>
    <asp:HiddenField ID="HiddenFieldRegistrationInitialVisitMonths" runat="server"/>
    <asp:HiddenField ID="HiddenFieldRegistrationFollowUpVisitMonths" runat="server"/>
    <asp:HiddenField ID="HiddenFieldRegistrationLastVisitMonths" runat="server"/>
    <asp:HiddenField ID="HiddenFieldPathology" runat="server" Value="0"/>

            <!--<div class="record-ident clearfix">
                <h3 class="record-first"></h3>
                <h3 class="record-second"><asp:Literal runat="server" ID="LiteralAbstractionNumber"></asp:Literal></h3>
            </div>-->
                    

<div runat="server" visible="true" id="divChartEntered">
    <p>To register patient charts, complete the table below using information from charts meeting the patient definition for the activity. For each activity you have selected, you must record data for all your selected charts before proceeding to the next steps for this activity.</p>
    <p><strong>Additional Information</strong>:</p>
    <ul>
        <li>The patient charts are supplied by you. Please note that direct patient care is required. If a diplomate is in a group-partnership or teaching position, exclusive care of the patient is not mandatory for selection of patient charts, but he/she must be actively involved in the patient's care.</li>
        <li>For confidentiality purposes, personally identifiable patient information such as patient’s name, date of birth or a specific visit date should not be entered.</li>
        <li>There are computer generated numbers assigned to track the patient charts. These numbers can be viewed in the third column labeled “Record Identifier.”</li>
    </ul>
    
    <p><strong>Directions</strong>:</p>
    <ol>
        <li>In the first column, labeled "Activity," make sure you are registering charts that meet that activity definition.</li>
        <li>In the second column, enter a unique, sequential numeric identifier for each patient (e.g. enter "1" in the first row, "2" in the second row, "3" in the third row, etc.) </li>
        <li>Enter the remainder of the data.</li>
        <li>Do this for all charts that you will review. NOTE: You may submit partially completed information and return later to complete. All information entered will be saved.</li>
        <li>Once you have registered each chart, click the Update button to move on to chart review.</li>
	</ol>             
</div>

<div runat="server" visible="false" id="divChartAbstraction">
    <p><strong>Directions:</strong></p>
    <ul>
        <li>To access each patient chart, click the “Record Identifier” number in the third column. Match this number with the number you previously recorded on your corresponding patient chart.</li>
        <li>If you would like to change a chart selection, click the remove button.  This will direct you back to the chart registration page.</li>
        <li>In the last column, there are three status grades: “Not Started,” “In-process,” and “Completed.” Not Started is the default. In-process means information has been added, but the chart is not complete. Completed means the chart is successfully filled-out.</li>
        <li>All charts must be Completed in order to proceed to “Initial Feedback Report”.</li>
    </ul>
</div>

<div class="record">
    <p><strong>PATIENT SELECTION CRITERIA for <asp:Literal ID="LiteralModuleName" runat="server"></asp:Literal>:</strong> <asp:Literal ID="LiteralModuleCriteria" runat="server"></asp:Literal></p>
</div>                

<div id="divRegisteredChartData" runat="server" visible="true"> 
    <h3 class="tableHeaders">Registered Chart Data</h3>
    <div class="button-box mid-page">
        <asp:LinkButton ID="LinkButtonChartAbstractionDetail"   runat="server" Text="View Chart Abstraction .pdf" target="_blank" CssClass="button" />
    </div>
        <table class="table">
            <tr>
                <th>Activity</th>
                <th>Patient #</th>
                <th>Record Identifier</th>
                <th>Month/Year<br />of Birth</th>
                <th runat="server" id="LabelInitialVisitDone">Month/Year<br />Initial Visit</th>
                <th runat="server" id="LabelLastVisitDone">Month/Year<br />Last Visit</th>
                <th>Status</th>
            </tr>
            <asp:ListView runat="server" ID="ListViewCompletedRow" OnItemDataBound="ListViewCompletedRow_OnItemDataBound">
                <LayoutTemplate>
                <div class="rigistration_contant_box_details">
                    <asp:PlaceHolder runat="server" ID="itemPlaceholder"></asp:PlaceHolder>
                </div>
                </LayoutTemplate>
            <ItemTemplate>
            <tr>
                <td><%# Eval("ModuleName")%></td>
                <td><%# Eval("UserPatientID")%></td>
                <td>
                    <asp:Literal runat="server" ID="LiteralLinkToChart" Text='<%# Eval("RecordIdentifier")%>' Visible="false"></asp:Literal>
                    <asp:HyperLink runat="server" ID="HyperLinkLinkToChart" Visible="true" Text='<%# Eval("RecordIdentifier")%>' NavigateUrl='<%# Eval("LinkToChart")%>'></asp:HyperLink>
                    <asp:HiddenField runat="server" ID="HiddenFieldRecordIdentifier" Value=<%# Eval("RecordIdentifier")%> />
                </td>
                <td><%# Eval("DOB")%></td>
                <td runat="server" id="divTextBoxInitialVisit"><%# Eval("InitialVisit")%></td>
                <td runat="server" id="divTextBoxLastVisit"><%# Eval("LastVisit")%></td>
                <td runat="server" id="divTextBoxDateSpecimenReceived" visible="false"><%# Eval("DateSpecimenReceived")%></td>
                <td runat="server" id="divSpace" visible="false"></td>
                <td style="color:<%# Eval("ColorChartStatus")%>" class="centered">
                        <%# Eval("ChartStatus")%><br />
                        <asp:LinkButton runat="server" ID="LinkButtonChangeRegistration" Text="Remove" CssClass="buttonA" style="font-size:xx-small" OnCommand="ChangeRegistration_Click" CommandArgument='<%# Eval("RecordIdentifier")%>' OnClientClick="return areyousure();"></asp:LinkButton>
                </td>
            </tr>
        </ItemTemplate>
        </asp:ListView>
    </table>
</div>    
              
<div id="divAdditionalChartData" runat="server" visible="true">     
    <h3 runat="server" id="LabelChartRegistrationData" class="tableHeaders">Enter Chart Registration Data</h3>
    <asp:Label ID="LabelError" runat="server"   Visible="false" ForeColor="Red" Text="Please enter all the chart information."></asp:Label>
    <div class="button-box mid-page">
        
        <asp:LinkButton ID="LinkButtonChartAbstractionDetail2"  runat="server" Text="View Chart Abstraction .pdf" target="_blank" CssClass="button" />
    </div>    
                    
    <table class="table">
        <tr>
            <th>Activity</th>
            <th>Patient #</th>
            <th>Record Identifier</th>
            <th>Month/Year<br />of Birth</th>
            <th runat="server" id="LabelInitialVisit">Month/Year<br />Initial Visit</th>
            <th runat="server" id="LabelLastVisit">Month/Year<br />Last Visit</th>
            <th>Action</th>
        </tr>

        <asp:ListView runat="server" ID="ListViewRow" OnDataBinding="ListViewRow_OnDataBinding" onitemdatabound="ListViewRow_OnItemDataBound">
        <LayoutTemplate>
        <div class="rigistration_contant_box_details">
            <asp:PlaceHolder runat="server" ID="itemPlaceholder"></asp:PlaceHolder>
        </div>
        </LayoutTemplate>                        
        <ItemTemplate>
        <tr>
            <td><%# Eval("ModuleName")%></td>
            <td><asp:TextBox runat="server" id="TextBoxUserPatientID" Width="50px" MaxLength="3"  Text=<%# Eval("UserPatientID")%>></asp:TextBox>
                <ajaxToolkit:FilteredTextBoxExtender ID="ftbe" runat="server" TargetControlID="TextBoxUserPatientID"  FilterType="Numbers" />
            </td>
            <td><%# Eval("RecordIdentifier")%><asp:HiddenField runat="server" ID="HiddenFieldRecordIdentifier" Value=<%# Eval("RecordIdentifier")%>/></td>
            <td><asp:TextBox runat="server" CssClass="small-text" id="TextBoxDOB" Width="50px" Text=<%# Eval("DOB")%> onchange="return isDOBDateValid(this, 1900);"></asp:TextBox></td>
            <td runat="server" id="divTextBoxInitialVisit"><asp:TextBox runat="server" CssClass="small-text" id="TextBoxInitialVisit" Width="50px" Text=<%# Eval("InitialVisit")%> onchange="return isInitialVisitDateValid(this, 1900);"></asp:TextBox></td>
            <td runat="server" id="divTextBoxLastVisit"><asp:TextBox runat="server" CssClass="small-text" id="TextBoxLastVisit" Width="50px" Text=<%# Eval("LastVisit")%> onchange="return isLastVisitDateValid(this, 1900);"></asp:TextBox></td>
            <td runat="server" id="divTextBoxDateSpecimenReceived" visible="false"><asp:TextBox runat="server" CssClass="small-text" id="TextBoxDateSpecimenReceived" Width="50px" Text=<%# Eval("DateSpecimenReceived")%> onchange="return isDERDateValid(this, 1900);"></asp:TextBox></td>
            <td runat="server" id="divSpace" visible="false"></td>
            <td>
                <asp:Button ID="Button1" runat="server" OnClick="ButtonSubmit_Click" Text="Update" /> <!--<asp:LinkButton runat="server" OnClick="ButtonSubmit_Click" Text="Update"></asp:LinkButton> Pending --></div>
                <br /><div id="divError" runat="server" style="color:Red; margin-left:10px;" visible="false">This is an error.....</div>
            </td>
        </tr>
        </ItemTemplate>
        </asp:ListView>
    </table>
</div>
              
<div class="button-box">
    <asp:LinkButton ID="LinkButtonBackToMain" runat="server" Text="Return to &#39;Create Your Experience&#39;" PostBackUrl="ModuleSelectionReview.aspx" Visible="false" CssClass="button" />
    <asp:LinkButton ID="LinkButtonBackToDashboard" runat="server" Text="Status" PostBackUrl="Dashboard.aspx" Visible="true" CssClass="button" />
    <asp:LinkButton ID="LinkButtonInsufficientCharts" runat="server" Text="Insufficient # of Charts Select New Activity" PostBackUrl="ModuleSelection.aspx" CssClass="button" />
    <asp:LinkButton ID="ButtonSubmit" runat="server" Text="Submit Registration" OnClick="ButtonSubmit_Click" CssClass="button" />
    <asp:LinkButton ID="ButtonSelfAssesment" runat="server" Text="Proceed to Self-Assessment" OnClick="ButtonSelfAssesment_Click" Visible="false" CssClass="button" />
    <asp:LinkButton ID="LinkButtonNextModule" runat="server" Text="Next activity" PostBackUrl="ModuleDetails.aspx" Visible="false" CssClass="button" />
</div>

</asp:Content>

<asp:Content runat="server" ContentPlaceHolderID="javascript" ID="Content4">
    <script type="text/javascript" language="javascript">

        function meetCategoryDef(s) {
            if (s.value == 0) {
                alert('Patient is not eligible. Please select another patient.');
            }
        }

        function meetDirectPatientCare(s) {
            if (s.value == 0) {
                alert('Patient is not eligible. Please select another patient.');
            }
        }

        function isDOBValid(d) {
            isValid = true;
            var maxYear = new Date().getFullYear() - 3;
            var minYear = new Date().getFullYear() - 12;
            var maxMonth = new Date().getMonth() + 1;
            try {
                var arr = d.value.split('/');
                if (!(arr[0] >= 1 && arr[0] <= 12 && arr[1] >= minYear && arr[1] <= maxYear)) {
                    isValid = false;
                }
            }
            catch (err) {
                isValid = false;
            }
            //        if (isValid == false) {
            //            alert("Invalid Month/Year(MM/YYYY). Patient should be between 3 to 12 years old.");
            //            d.value = "";
            //        }
            return isValid;
        }

        function isDateValid(d, minYear) {

            isValid = true;
            try {
                var arr = d.value.split('/');
                if (!(arr[0] >= 1 && arr[0] <= 12 && arr[1] >= minYear)) {
                    isValid = false;
                }
            }
            catch (err) {
                isValid = false;
            }
            //if (isValid == false) {
            //    alert('Invalid month/year format(MM/YYYY). Year should be greater than ' + minYear);
            //    d.value = "";
            //}
            return isValid;
        }

        function validateDates(DOB, InitialVisit, LastVisit) {
            var REGISTRATION_MIN_AGE = parseInt(document.getElementById("<%= HiddenFieldRegistrationMinAge.ClientID %>").value);
        var REGISTRATION_MAX_AGE = parseInt(document.getElementById("<%= HiddenFieldRegistrationMaxAge.ClientID %>").value);
        var REGISTRATION_INITIAL_VISIT_MONTHS = parseInt(document.getElementById("<%= HiddenFieldRegistrationInitialVisitMonths.ClientID %>").value);
        var REGISTRATION_FOLLOWUP_VISIT_MONTHS = parseInt(document.getElementById("<%= HiddenFieldRegistrationFollowUpVisitMonths.ClientID %>").value);
        var REGISTRATION_LAST_VISIT_MONTHS = parseInt(document.getElementById("<%= HiddenFieldRegistrationLastVisitMonths.ClientID %>").value);

        var TODAYDATE = new Date();
        var CURRENT_DATE = new Date(TODAYDATE.getFullYear(), TODAYDATE.getMonth(), 1);


        var stringError = "";
        if ((DOB != null) && (InitialVisit != null) && (LastVisit != null)) {
            if (DOB > InitialVisit) {
                stringError = "Initial Visit cannot be before Date of Birth.\n";
            }
            if (DOB > LastVisit) {
                stringError = stringError + "Last Visit cannot be before Date of Birth.\n";
            }

            LastVisitFrom = new Date(InitialVisit.getFullYear(), InitialVisit.getMonth() + REGISTRATION_FOLLOWUP_VISIT_MONTHS, 1);

            InitialVisitValidFrom = new Date(CURRENT_DATE.getFullYear(), CURRENT_DATE.getMonth() - REGISTRATION_INITIAL_VISIT_MONTHS, 1);


            //            // calculate six months
            //            var initialMonth = InitialVisit.getMonth();
            //            var initialYear = InitialVisit.getFullYear();
            //            var monthsToFollowingYear = 0;
            //            if ((initialMonth + 5) > 11) {
            //                monthsToFollowingYear = (initialMonth + 5) - 12;
            //                initialMonth = monthsToFollowingYear;
            //                initialYear = initialYear + 1;
            //            }
            //            else {
            //                initialMonth = initialMonth + 5;
            //            }
            //            var VisitFrom = new Date(initialYear, initialMonth, 1);
            //            // calculate 18 months
            //            initialYear = initialYear + 1;
            //            var VisitTo = new Date(initialYear, initialMonth, 1);

            var InitialVisitFrom = new Date(DOB.getFullYear(), DOB.getMonth() + (REGISTRATION_MIN_AGE * 12), 1);
            var maxAge = REGISTRATION_MAX_AGE;
            if (maxAge == 0)
                maxAge = 120;
            var InitialVisitTo = new Date(DOB.getFullYear(), DOB.getMonth() + (maxAge * 12), 1);

            if (InitialVisit > LastVisit) {
                stringError = stringError + "Initial Visit cannot be before Last Visit.\n";
            }

            if (LastVisitFrom > LastVisit) {
                stringError = stringError + "Last Visit should be after " + LastVisitFrom.toDateString() + ".\n";
            }

            //            if ((LastVisit < VisitFrom) || (LastVisit > VisitTo) ){
            //                stringError = stringError + "Last Visit should be between 6 to 18 months of Initial Visit.\n";
            //            }
            if ((REGISTRATION_INITIAL_VISIT_MONTHS > 0) && (InitialVisit < InitialVisitValidFrom)) {
                stringError = stringError + "Initial Visit cannot be before " + InitialVisitValidFrom.toDateString() + ".\n";
            }

            if ((InitialVisit < InitialVisitFrom) || (InitialVisit > InitialVisitTo)) {
                if (REGISTRATION_MAX_AGE == 0)
                    stringError = stringError + "Patient should be older than " + REGISTRATION_MIN_AGE + " years old at the Initial Visit.\n";
                else
                    stringError = stringError + "Patient should be between " + REGISTRATION_MIN_AGE + " to " + REGISTRATION_MAX_AGE + " years old at the Initial Visit.\n";
            }


            if (LastVisit > CURRENT_DATE) {
                stringError = stringError + " Last Visit date is invalid, you must enter a date that has already occurred. ";
            }

            var LastVisitValidFrom = new Date(CURRENT_DATE.getFullYear(), CURRENT_DATE.getMonth() - REGISTRATION_LAST_VISIT_MONTHS, 1);

            if (REGISTRATION_LAST_VISIT_MONTHS > 0) { // only validate field if the value is greater than 0
                if (LastVisitValidFrom > LastVisit) {
                    stringError = stringError + "Last Visit should be at least once in the past " + REGISTRATION_LAST_VISIT_MONTHS + " months.\n";
                }
            }

            if (stringError != "") {
                alert(stringError);
            }
        }
    }

    function isInitialVisitDateValid(d, minYear) {
        if (!isDateValid(d, minYear)) {
            alert('Invalid month/year format(MM/YYYY). Year should be greater than ' + minYear);
            d.value = "";
        }
        else {
            var InitialVisitID = d.id;
            var TextBoxDOBID = InitialVisitID.replace("TextBoxInitialVisit", "TextBoxDOB");
            var TextBoxLastVisitID = InitialVisitID.replace("TextBoxInitialVisit", "TextBoxLastVisit");

            var InitialVisitDate = new Date(d.value.replace("/", "/1/"));

            var TextBoxDOB = document.getElementById(TextBoxDOBID).value;
            var DOBDate = null;
            if (TextBoxDOB != "") {
                DOBDate = new Date(TextBoxDOB.replace("/", "/1/"));
            }

            var TextBoxLastVisit = document.getElementById(TextBoxLastVisitID).value;
            var LastVisitDate = null;
            if (TextBoxLastVisit != "") {
                LastVisitDate = new Date(TextBoxLastVisit.replace("/", "/1/"));
            }

            validateDates(DOBDate, InitialVisitDate, LastVisitDate);
        }
    }

    function isLastVisitDateValid(d, minYear) {
        if (!isDateValid(d, minYear)) {
            alert('Invalid month/year format(MM/YYYY). Year should be greater than ' + minYear);
            d.value = "";
        }
        else {
            var LastVisitID = d.id;
            var TextBoxDOBID = LastVisitID.replace("TextBoxLastVisit", "TextBoxDOB");
            var TextBoxInitialVisitID = LastVisitID.replace("TextBoxLastVisit", "TextBoxInitialVisit");

            var LastVisitDate = new Date(d.value.replace("/", "/1/"));

            var TextBoxDOB = document.getElementById(TextBoxDOBID).value;
            var DOBDate = null;
            if (TextBoxDOB != "") {
                DOBDate = new Date(TextBoxDOB.replace("/", "/1/"));
            }

            var TextBoxInitialVisit = document.getElementById(TextBoxInitialVisitID).value;
            var InitialVisitDate = null;
            if (TextBoxInitialVisit != "") {
                InitialVisitDate = new Date(TextBoxInitialVisit.replace("/", "/1/"));
            }

            validateDates(DOBDate, InitialVisitDate, LastVisitDate);
        }
    }

    function isDERDateValid(d, minYear) {
        if (!isDateValid(d, minYear)) {
            alert('Invalid month/year format(MM/YYYY). Year should be greater than ' + minYear);
            d.value = "";
            return false;
        }
        else {
            var TODAYDATE = new Date();
            var CURRENT_DATE = new Date(TODAYDATE.getFullYear(), TODAYDATE.getMonth(), 1);
            var DERID = d.id;
            var TextBoxDOBID = DERID.replace("TextBoxDateSpecimenReceived", "TextBoxDOB");

            var DERDate = new Date(d.value.replace("/", "/1/"));

            var TextBoxDOB = document.getElementById(TextBoxDOBID).value;
            var DOBDate = null;
            if (TextBoxDOB != "") {
                DOBDate = new Date(TextBoxDOB.replace("/", "/1/"));
                if (DOBDate > DERDate) {
                    alert("Specimen Received Date cannot be before Date of Birth.\n");
                    d.value = "";
                    return false;
                }
            }
            if (DERDate > CURRENT_DATE) {
                alert("Specimen Received date is invalid, you must enter a date that has already occurred. ");
                d.value = "";
                return false;
            }
        }
        return true;
    }

    function isDOBDateValid(d, minYear) {
        //        if (!isDOBValid(d)) {
        //            alert("Invalid Month/Year(MM/YYYY). Patient should be between 3 to 12 years old.");
        //            d.value = "";
        //        }
        if (!isDateValid(d, minYear)) {
            alert('Invalid month/year format(MM/YYYY). Year should be greater than ' + minYear);
            d.value = "";
        }
        else {
            var HiddenFieldPathology = document.getElementById("<%= HiddenFieldPathology.ClientID %>").value;
            if (HiddenFieldPathology != "1") {

                var DOBID = d.id;
                var TextBoxLastVisitID = DOBID.replace("TextBoxDOB", "TextBoxLastVisit");
                var TextBoxInitialVisitID = DOBID.replace("TextBoxDOB", "TextBoxInitialVisit");

                var DOBDate = new Date(d.value.replace("/", "/1/"));

                var TextBoxLastVisit = document.getElementById(TextBoxLastVisitID).value;
                var LastVisitDate = null;
                if (TextBoxLastVisit != "") {
                    LastVisitDate = new Date(TextBoxLastVisit.replace("/", "/1/"));
                }

                var TextBoxInitialVisit = document.getElementById(TextBoxInitialVisitID).value;
                var InitialVisitDate = null;
                if (TextBoxInitialVisit != "") {
                    InitialVisitDate = new Date(TextBoxInitialVisit.replace("/", "/1/"));
                }
                validateDates(DOBDate, InitialVisitDate, LastVisitDate);
            }
        }
    }

    function isCompletedDateValid(d) {
        isValid = true;
        try {

            var x = Date.parse(d.value, "M/d/yyyy");
            if (x > 0) {
                var arr = d.value.split('/');
                var tempDate = new Date(d.value);
                if ((tempDate.getMonth() + 1 != arr[0]) || (tempDate.getDate() != arr[1]) || (tempDate.getFullYear() != arr[2]))
                    isValid = false;
            }
            else {
                isValid = false;
            }
        }
        catch (err) {
            isValid = false;
        }
        if (isValid == false) {
            alert('Invalid date. Please use format Month/Day/Full Year');
            d.value = "";
        }
        return isValid;
    }

    function areyousure() {
        return confirm("Are you sure you want to remove the Chart Registration/Abstraction?");
    }

    $(".table tr:odd").addClass("odd");
</script>
</asp:Content>