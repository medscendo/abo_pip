﻿<%@ Page Title="" Language="C#" MasterPageFile="~/PIMMasterPage.master" AutoEventWireup="true" CodeFile="ImprovementPlanPartI.aspx.cs" Inherits="copd_ImprovementPlan" %>

<asp:Content ID="Content1" ContentPlaceHolderID="head" Runat="Server">

    <style>
        .listlist {
            list-style-type:none;
        }

        .listlist input {
            float:left;
            width: 10%;
            margin-top:4px;
        }

        .listlist li {
            margin: 5px 0;
        }

        .listlist .li-header {
            margin: 15px 0 10px;
        }

        .select_details {
            float:left;
            width:90%;
        }
    </style>

</asp:Content>
<asp:Content ID="Content3" ContentPlaceHolderID="ContentPlaceHolder1" Runat="Server">

<script type="text/javascript" language="javascript">
    function showMoreResources(el, mid, tbl) {
        var table = $get(tbl);
        var rows = table.rows;
        for (var i = 0; i < rows.length; i++) {
            var midAttr = rows[i].mid;
            if (midAttr == mid) {
                rows[i].style.display = "block";
            }
        }
        el.style.display = "none";
    }

    function validateForm() {
        var noErrorFound = true;
        $('.MeasureGoal').each(function() {
            var currentValue = this.value;
            if (currentValue != Number(currentValue)) {
                noErrorFound = false;
                alert("Measure Goal is invalid");
                this.focus();
                return false;
            }
            if ((currentValue <= 0) || (currentValue > 100)) {
                noErrorFound = false;
                alert("Measure Goal should be between 1 and 100");
                this.focus();
                return false;
            }

        });
        return noErrorFound;
    }

    function submitFinalPlan() {
        var formCompleted = validateForm();
        if (formCompleted) {
            var textBoxImportantChanges = document.getElementById("<%= TextBoxImportantChanges.ClientID %>").value;
            var textBoxImportantChanges = textBoxImportantChanges.trim();
            if (textBoxImportantChanges.length == 0) {
                alert("Please enter a response to Question Number 2 before continuing");
                formCompleted = false;
            }
            if (formCompleted) {
                formCompleted = confirm('Do you want to submit your Improvement Plan?');
            }
        }
        return formCompleted;
    }
    
</script>
    
<asp:Panel ID="PanelInformation" runat="server" />

<asp:Panel ID="PanelReportCompleteDate" runat="server" Visible="false">
    This report completed on: <asp:Label ID="LabelImprovementPlanCompletedDate" runat="server" />
</asp:Panel>
    <!-- module detaile -->
    <!--<div class="heading_box">
        <h2>Improvement Plan <asp:Literal ID="LiteralCompletedDate" runat="server"></asp:Literal></h2>
    </div>-->
    
                
<p>
The Improvement Plan is required to demonstrate self-reflection and address gaps that may exist in the quality of care provided.
</p>   <p>
Based on the measures you selected for improvement, enter your improvement goals as percentages of improvement you want to reach.
</p>    <p>
Think about potential improvement strategies that you will employ and briefly outline the changes you consider most important for you to reach your improvement goals.
 </p>    <p>
The date for re-measurement is pre-determined and is listed below.
 </p>
  
<h3><asp:Label ID="LabelQ1" runat="server" Text="Label"></asp:Label>. Measures Selected for Improvement </h3> 
<div class="button-box">
    <asp:LinkButton ID="LinkButtonRemoveMeasure" runat="server" Text="Remove Measures?" OnClick="ButtonRemoveMeasure_Click" Visible="false" CssClass="button" />    
    <asp:LinkButton ID="LinkButtonRemeasure" runat="server" Text="Change Measures?" OnClick="ButtonRemeasure_Click" Visible="false" OnClientClick="return confirm('You have chosen to change measures. This means you will reselect your measures for improvement and then redevelop your Improvement plan.\n\nThis also means that your date for remeasurement will be reset.\n\nAnd,if you have started registering patient charts for the Analysis & Impact phase, those registrations will be erased. If you do not want this, click \'cancel.\' Otherwise, click OK to proceed.');" CssClass="button" />
</div>

<table class="table">
    <tr>
        <th>Measure Selected</th>
        <th>Initial Data</th>
        <th>Goal</th>
    </tr>

    <asp:ListView runat="server" ID="ListViewMeasureGoals" onitemdatabound="ListViewMeasureGoals_OnItemDataBound">
        <LayoutTemplate>            
        
            <asp:PlaceHolder runat="server" ID="itemPlaceholder"></asp:PlaceHolder>
        
        </LayoutTemplate>
    <ItemTemplate>
    <tr>
        <td>
            <asp:HiddenField ID="HiddenFieldMeasureID" Value='<%# Eval("MeasureID") %>' runat="server" />
            <p>
                <strong><asp:Label ID="LabelMeasureNumber" runat="server"></asp:Label>. <strong><%# Eval("ModuleName")%></strong> - <%# Eval("MeasureQualityIndicator")%></strong>:
                <%# Eval("MeasureClinicalRecommendation") %> 
            </p>
        </td>
        
        <td>
          
         <asp:Label ID="LabelOUT20" runat="server" Text='<%# Eval("OUT20") %>' ></asp:Label><asp:Label ID="LabelMyPracticeAssessment" runat="server" Text='<%# Eval("MeanDisplayFlag").ToString() == "True" ? Eval("MeasurePercentCurrent", "{0:D}") : Eval("MeasurePercentCurrent", "{0:D}%") %>'></asp:Label>
        </td>
        <td>
            <span style="white-space:nowrap;">
                <asp:Label ID="Label1" runat="server" Text='<%# Eval("OUT20") %>' ></asp:Label> <asp:TextBox ID="TextBoxMeasureGoal" runat="server" MaxLength="3" Width="40" Text='<%# Eval("MeasureGoal") %>' class="MeasureGoal"/>
                <asp:Label ID="LabelPercentSign" runat="server" Text='<%# Eval("MeanDisplayFlag").ToString() == "True" ? "" : "%" %>' />
            </span>
            <asp:RequiredFieldValidator ID="RequiredFieldValidatorMeasureGoal" runat="server" ErrorMessage='<%# Eval("MeasureTitle", "Missing Measure Goal: {0}") %>' Display="Dynamic" ControlToValidate="TextBoxMeasureGoal" >*</asp:RequiredFieldValidator>
            <asp:RangeValidator ID="RangeValidatorMeasureGoal" runat="server" ErrorMessage='<%# Eval("MeasureTitle", "Measure Goal is not in a valid range: {0}") %>' ControlToValidate="TextBoxMeasureGoal" MinimumValue="1" MaximumValue="100" Type="Integer" Display="Dynamic" >*</asp:RangeValidator>
            <asp:Label ID="LabelMeasureGoal" runat="server" Text='<%# Eval("MeanDisplayFlag").ToString() == "True" ? Eval("MeasureGoal", "{0:D}") : Eval("MeasureGoal", "{0:D}%") %>' Visible="false" />

            

        </td>
    </tr>
    </ItemTemplate>
    </asp:ListView>
</table>
              
<h3>  <asp:Label ID="LabelQ2" runat="server" Text="Label"></asp:Label>. Before constructing your Improvement Plan, briefly outline the changes you consider most important  for you and your practice to maintain or improve your measurement results. </h3>
				
<asp:TextBox ID="TextBoxImportantChanges" runat="server" TextMode="MultiLine" Rows="6" Width="100%" MaxLength="1500"></asp:TextBox>
<asp:Label ID="LabelImportantChanges" runat="server" Visible="false" />
			  
<div id="resourcesdiv"  runat="server">			    
<h3><asp:Label ID="LabelQ3" runat="server" Text="Label"></asp:Label>. Additional resources that are relevant to your selected measures may be found here. </h3>
<p>Please indicate which of these materials you intend to view/incorporate as part of your Improvement Plan.</p>

<asp:ListView runat="server" ID="ListViewAdditionalResources" OnItemDataBound="ListViewAdditionalResources_RowDataBound">
<LayoutTemplate>
<ul class="listlist">
    <asp:PlaceHolder runat="server" ID="itemPlaceholder"></asp:PlaceHolder>
</ul>
</LayoutTemplate>    
<ItemTemplate>
    <li runat="server" id="liMeasureTitle" visible="false" class="li-header"><h4><strong><%# Eval("MeasureTitle") %></strong></h4></li>
    <li class="clearfix">
        <asp:HiddenField ID="HiddenFieldMeasureID" Value='<%# Eval("MeasureID") %>' runat="server" />
        <asp:HiddenField ID="HiddenFieldResourceID" Value='<%# Eval("ResourceID") %>' runat="server" />
        <asp:HiddenField ID="HiddenFieldRecommendedFlag" Value='<%# Eval("RecommendedFlag") %>' runat="server" />
        <asp:HiddenField ID="HiddenFieldCostFlag" Value='<%# Eval("CostFlag") %>' runat="server" />
        <asp:HiddenField ID="HiddenFieldAAOResourceFlag" Value='<%# Eval("AAOResourceFlag") %>' runat="server" />
        <asp:HiddenField ID="HiddenFieldMeasureTitle" Value='<%# Eval("MeasureTitle") %>' runat="server" />
        <asp:CheckBox ID="CheckBoxResource" runat="server" class="check" Text="" Checked='<%# Eval("SelectedItem") %>' />
                                
        <div class="select_details">
            <strong><a href='<%# Eval("URL") %>' target="_blank"><asp:Literal ID="LabelCheckBoxResource" runat="server" Text='<%# Eval("ResourceName") %> '></asp:Literal></a></strong>
            <asp:Label ID="ResourceDescription" runat="server" Text='<%# Eval("ResourceDescription") %>' />
            <asp:Image ID="ImageCost" runat="server" ImageUrl="~/images/costforResource_icon.png" Visible="false" Height="13px" Width="13px"/>
            <asp:Image ID="ImageAAO" runat="server" ImageUrl="~/images/aao_Icon.png" Visible="false" Height="13px" Width="13px"/>
        </div>
    </li>                        
</ItemTemplate>
</asp:ListView>
</div>
<h3><asp:Label ID="LabelQ4" runat="server" Text="Label"></asp:Label>. Date for Remeasurement</h3>
You may begin remeasurement (i.e. your second chart abstraction) on <asp:Label ID="labelDateForRemeasure" runat="server"/>.
 
                  
<%--<h3>4. When will you begin your Improvement Plan? </h3>
<asp:RadioButtonList ID="RadioButtonListStartImprovement" runat="server" DataTextField="TimeframeTypeName" DataValueField="TimeframeTypeID" />
<asp:Label ID="LabelStartImprovement" runat="server" Visible="false" />

                  
<h3>5. When do you plan to remeasure (second abstraction)? </h3>
<asp:DropDownList ID="DropDownListRemeasureMonth" runat="server" DataValueField="MonthID" DataTextField="MonthName" />
<asp:DropDownList ID="DropDownListRemeasureYear" runat="server" DataValueField="YearID" DataTextField="YearName" />
<asp:Label ID="LabelRemeasure" runat="server" Visible="false" /><br /><br />
--%>                            
<div class="button-box">
    <a href="Dashboard.aspx" id="LinkDashboard" runat="server" visible="false" class="button">Return to Status Page</a>
</div>

<div class="button-box">
    
    <%--<div class="bginput"><asp:LinkButton ID="ButtonSubmitA" runat="server" Text="Submit Final Plan"  OnClientClick="return confirm('Do you want to submit your Improvement Plan?');" OnClick="ButtonSubmit_Click"/></div>
    <div class="bginput"><asp:LinkButton ID="ButtonSaveForLaterA" runat="server" Text="Save for Later"  OnClick="ButtonSaveForLater_Click"/></div>
    <div class="bginput"><asp:LinkButton ID="LinkButtonDashboardA" runat="server" Text="Return To Dashboard"  OnClick="LinkButtonDashboard_Click"/></div>--%>
    <asp:Button ID="ButtonSaveForLater" runat="server" Text="Save and Return to Status Page" CausesValidation="false" onclick="ButtonSaveForLater_Click" CssClass="button" />
    <%--<asp:Button ID="ButtonNext" runat="server" Text="Next" CausesValidation="false" onclick="ButtonNext_Click" />--%>
    <asp:Button ID="ButtonSubmit" runat="server" Text="Submit Final Plan" OnClientClick="return submitFinalPlan()" onclick="ButtonSubmit_Click" CssClass="button" />
</div>
    
</asp:Content>

