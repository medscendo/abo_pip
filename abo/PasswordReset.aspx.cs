﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using NetHealthPIMModel;

public partial class copd_PasswordReset : BasePage
{
    protected void Page_Load(object sender, EventArgs e)
    {
        if (!IsPostBack)
        {
            InitializePage();

            int ParticipantID = ctx.ParticipantID;
            using (NetHealthPIMEntities pim = new NetHealthPIMEntities())
            {
                Participant participant = (from p in pim.Participant
                                           where p.ParticipantID == ParticipantID 
                                           select p).First<Participant>();
                LabelEMailAddress.Text = participant.ParticipantEmailAddress;
            }
        }
    }

    protected void InitializePage()
    {
        ((PIMMasterPage)Page.Master).SetTitle("Password Reset");
        Session[Constants.SESSION_PAGE_ROLETYPE_LEVEL] = Constants.ROLETYPEID_GUEST;
    }

    protected void ButtonSubmit_Click(object sender, EventArgs e)
    {
        using (NetHealthPIMEntities pim = new NetHealthPIMEntities())
        {
            Participant participant = (from p in pim.Participant
                                       where p.ParticipantID == ctx.ParticipantID
                                       select p).First<Participant>();
            participant.DegreeTypeReference.Load();
            string email = participant.ParticipantEmailAddress;
            string passwordHash = Security.EncryptPassword(email, TextPassword.Text);
            string participantName = participant.ParticipantFirstName + " " + participant.ParticipantLastName + ", " + participant.DegreeType.DegreeTypeName;
            participant.Password = passwordHash;
            pim.SaveChanges();

            ctx.ParticipantName = participantName;
            ctx.IsAuthenticated = true;
            ctx.RoleTypeID = (int)participant.RoleTypeID;
            Session[Constants.SESSION_USERCONTEXT] = ctx;

            // Redirect to the default page based on role.
            Response.Redirect(GetDefaultRedirectByRole(ctx.RoleTypeID));
        }
    }
}
