﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using NetHealthPIMModel;
using System.Transactions;
using System.Web.UI.HtmlControls;
using System.Data.EntityClient;
using System.Data;

public partial class abo_ModuleSelection : BasePage
{
    protected void Page_Load(object sender, EventArgs e)
    {
        using (NetHealthPIMEntities pim = new NetHealthPIMEntities())
        {
            Participant participant = pim.Participant.First(p => p.ParticipantID == ctx.ParticipantID);
            var pqrsselected = pim.ParticipantModule.First(p => p.ParticipantID == participant.ParticipantID);
            LabelChartsRequired.Text = Constants.TOTAL_NUMBER_OF_CHARTS.ToString();
            if (Session["MinCharts"] == "0")
            {
                string script = "$(document).ready(function(){ alert('Please select 30 charts.'); });";
                Page.ClientScript.RegisterStartupScript(this.GetType(), "MyScript", script, true);
            }
            if (Convert.ToBoolean(pqrsselected.PQRS))
            {
                string script = "$(document).ready(function(){ $('#ctl00_ContentPlaceHolder1_ButtonSubmit').click(function(event){ if(!$((document.getElementById('ctl00_ContentPlaceHolder1_RepeaterCategoryCataractAndAnterior_ctl01_TextBoxNumberOfCharts').value) > 0) && !$((document.getElementById('ctl00_ContentPlaceHolder1_RepeaterCategoryRetinaVitreous_ctl02_TextBoxNumberOfCharts').value) > 0) && !$((document.getElementById('ctl00_ContentPlaceHolder1_RepeaterCategoryGlaucoma_ctl01_TextBoxNumberOfCharts').value) > 0) && !$((document.getElementById('ctl00_ContentPlaceHolder1_RepeaterCategoryGlaucoma_ctl03_TextBoxNumberOfCharts').value) > 0) && !$((document.getElementById('ctl00_ContentPlaceHolder1_RepeaterCategoryRetinaVitreous_ctl01_TextBoxNumberOfCharts').value) > 0) ){ var r=confirm('You did not select any PQRS activities, do you want to continue with no PQRS participation?'); if (r==false){ event.preventDefault(); } }}); });";
                Page.ClientScript.RegisterStartupScript(this.GetType(), "MyScript", script, true);
            }
            if (!IsPostBack)
            {
                InitializePage();
            }
            Session["MinCharts"] = "1";
        }

    }
    protected void InitializePage()
    {
        if (Request.UrlReferrer.ToString().Contains("ModuleSelectionReview"))
        {
            LinkButtonBack.PostBackUrl = Request.UrlReferrer.ToString();
            LinkButtonBack.Text = " Back ";
            LinkButtonBack.Visible = true;
        }
        else
            if ((Request.UrlReferrer.ToString().Contains("ModuleDetails")) || (Request.UrlReferrer.ToString().Contains("SelectPriorModules")))
                LinkButtonBack.Visible = false;

        if (Session["MinCharts"] == "0")
        {
            LinkButtonBack.Visible = false;
        }
        
        string breadCrumb = Constants.BC_CREATE_PLAN_LINK + "" + Constants.BC_PQRS_INCENTIVE_PROGRAM_LINK + "" +
        Constants.BC_IMPORT_DATA_LINK + "" + Constants.BC_MODULE_SELECTION;
        ((PIMMasterPage)Page.Master).SetTitle(breadCrumb);
        ((PIMMasterPage)Page.Master).SetStatus(1);
        ((PIMMasterPage)Page.Master).SetHeader("Activity Selection");
        using (NetHealthPIMEntities pim = new NetHealthPIMEntities())
        {
            ParticipantModuleCycle participantModuleCycle = pim.ParticipantModuleCycle.First(p => p.ParticipantModuleCycleID == ctx.ActiveModuleCycleID);
            if (participantModuleCycle.ModuleSelectionCompletedDate != null)
            {
                HiddenFieldModuleSelectionConfirmed.Value = "1";
                HiddenFieldPlanCreated.Value = "1";
            }
            if ((participantModuleCycle.CycleNumber == 2) || (participantModuleCycle.ImprovementPlanComplete == true))
            {
                Response.Redirect("~/" + module.ModuleCode.ToLower() + "/ModuleDetails.aspx");
            }
            int previousModulesCount = 0;
            string PreviousModulesSelected = (string)Session[Constants.SESSION_PREVIOUS_MODULES];
            if (PreviousModulesSelected == null)
            {
                var countPreviousmodules = pim.ParticipantModulesSelected_V.Count(c => (c.ParticipantModuleCycleID == ctx.ActiveModuleCycleID));
                if (countPreviousmodules > 0)
                {
                    int[] currentModulesSelected = pim.ParticipantModulesSelected_V.Where(c => (c.ParticipantModuleCycleID == ctx.ActiveModuleCycleID)).OrderBy(c => c.ModuleID).Select(c => c.ModuleID).ToArray(); 
                    string[] strArray = currentModulesSelected.Select(x => x.ToString()).ToArray();
                    PreviousModulesSelected = String.Join(",", strArray);
                    Session[Constants.SESSION_PREVIOUS_MODULES] = PreviousModulesSelected;
                    int[] currentModulesChartsSelected = pim.ParticipantModulesSelected_V.Where(c => (c.ParticipantModuleCycleID == ctx.ActiveModuleCycleID)).OrderBy(c => c.ModuleID).Select(c => (int)c.NumberOfCharts).ToArray(); 
                    string[] strArrayCharts = currentModulesChartsSelected.Select(x => x.ToString()).ToArray();
                    Session[Constants.SESSION_PREVIOUS_NUMBEROFCHARTS] = String.Join(",", strArrayCharts);
                }
            }
            var ParticipantInfo = (from c in pim.Participant where c.ParticipantID == ctx.ParticipantID select c).FirstOrDefault();
            var participantModulesSelected = (from pms in pim.ParticipantPlanStatus_V
                                              where pms.ParticipantModuleCycleID == ctx.ActiveModuleCycleID
                                              & pms.PreviousChartData > 0
                                              select pms);
            RepeaterModules.DataSource = participantModulesSelected;
            RepeaterModules.DataBind();
            if (RepeaterModules.Items.Count > 0)
            {
                PreviousModulesAreSelected.Visible = true;
            }

            ParticipantModuleCycle cycle = pim.ParticipantModuleCycle.First(c => c.ParticipantModuleCycleID == ctx.ActiveModuleCycleID);
            var count = pim.ParticipantModuleSelection.Count(p => p.ParticipantModuleCycleID == ctx.ActiveModuleCycleID && p.PreviousModuleCharts == null);
            var countConfimedModules = pim.ParticipantModuleSelection.Count(p => p.ParticipantModuleCycleID == ctx.ActiveModuleCycleID && p.ModuleConfirm == true && p.PreviousModuleCharts == null);
            if (countConfimedModules > 0) 
            {
                HiddenFieldModulesCurrentlySelected.Value = (count + previousModulesCount).ToString();
            }

            var moduleSelection = from pms in pim.ParticipantModuleSelection
                                  where pms.ParticipantModuleCycleID == cycle.ParticipantModuleCycleID
                                   && pms.PreviousModuleCharts == null
                                  select pms;
            DisplayCategory(1, pim, RepeaterCategoryCataractAndAnterior, moduleSelection, "CataractAndAnterior");
            DisplayCategory(2, pim, RepeaterCategoryCornea, moduleSelection, "Cornea");
            DisplayCategory(4, pim, RepeaterCategoryGlaucoma, moduleSelection, "Glaucoma");
            //DisplayCategory(5, pim, RepeaterCategoryNeuroOpthalmogy, moduleSelection, "NeuroOpthalmogy"); Both activites for Neuro-Ophthalmology are now discontinued. 3/12/18
            //DisplayCategory(6, pim, RepeaterCategoryUveitis, moduleSelection, "Uveitis"); Both activites for Uveitis are now discontinued. 3/12/18
            DisplayCategory(7, pim, RepeaterCategoryOculoplastics, moduleSelection, "Oculoplastics");
            DisplayCategory(8, pim, RepeaterCategoryPathologyOncology, moduleSelection, "PathologyOncology");
            DisplayCategory(9, pim, RepeaterCategoryPediatrics, moduleSelection, "Pediatrics");
            DisplayCategory(10, pim, RepeaterCategoryRefractive, moduleSelection, "Refractive");
            DisplayCategory(11, pim, RepeaterCategoryRetinaVitreous, moduleSelection, "RetinaVitreous");
            var countChartEntered = pim.ChartRegistration.Count(cr => cr.ParticipantModuleCycleID == ctx.ActiveModuleCycleID && cr.Completed == true);
            HiddenFieldDataEntered.Value = countChartEntered.ToString();
        }
    }

    protected void RepeaterCategoryPathologyOncology_ItemDataBound(Object Sender, RepeaterItemEventArgs e)
    {
        // This event is raised for the header, the footer, separators, and items.

        // Execute the following logic for Items and Alternating Items.
        if (e.Item.ItemType == ListItemType.Item || e.Item.ItemType == ListItemType.AlternatingItem)
        {
            System.Data.Common.DbDataRecord rd = (System.Data.Common.DbDataRecord)e.Item.DataItem;
        }
    }    

    protected void DisplayCategory(int ModuleCategoryID, NetHealthPIMEntities pim, Repeater RepeaterCategory, IQueryable<ParticipantModuleSelection> ModuleSelected, string ModuleName)
    {
        Participant participant = pim.Participant.First(p => p.ParticipantID == ctx.ParticipantID);
        var pqrsselected = pim.ParticipantModule.First(p => p.ParticipantID == participant.ParticipantID);
        bool pqrsused = Convert.ToBoolean(pqrsselected.PQRS);
        if (participant.SoftwareVersion < 2015)
        {
            var modules = pim.GetModulesByCategory_P(ModuleCategoryID, ctx.ActiveModuleCycleID, pqrsused).Where(c => c.ModuleNewVerActive != null && c.ModuleNewVerActive ==false && c.ModuleVersion2014==true).ToList();
            RepeaterCategory.DataSource = modules;
            RepeaterCategory.DataBind();
        }
        else
        {
            var modules = pim.GetModulesByCategory_P(ModuleCategoryID, ctx.ActiveModuleCycleID, pqrsused).Where(c => c.ModuleNewVerActive != null && c.ModuleVersion2016 == true).ToList();
            RepeaterCategory.DataSource = modules;
            RepeaterCategory.DataBind();
        }
        for (var i = 0; i <= RepeaterCategory.Items.Count - 1; i++)
        {
            if (RepeaterCategory.ID == "RepeaterCategoryPathologyOncology")
            {
                if (((HiddenField)RepeaterCategory.Items[i].FindControl("HiddenFieldIsParent")).Value == "True")
                {
                    ((TextBox)RepeaterCategory.Items[i].FindControl("TextBoxNumberOfCharts")).Visible = false;
                    ((Literal)RepeaterCategory.Items[i].FindControl("LiteralNumberOfCharts")).Visible = false;
                    ((Literal)RepeaterCategory.Items[i].FindControl("LiteralNA")).Visible = false;
                }
            } 
            
            foreach (var m in ModuleSelected)
            {
                if (Convert.ToInt32(((HiddenField)RepeaterCategory.Items[i].FindControl("HiddenFieldModuleID")).Value) == m.ModuleID)
                {
                    if (RepeaterCategory.ID == "RepeaterCategoryPathologyOncology")
                    {
                        HiddenFieldPhatologySelected.Value = "1";
                    }
                }
            }

            if (RepeaterCategory.Items[i].FindControl("spanPreviousModules") != null)
            {
                string ModuleID = ((HiddenField)RepeaterCategory.Items[i].FindControl("HiddenFieldModuleID")).Value;
                if ((ModuleID == HiddenFieldModuleSelection1.Value) || (ModuleID == HiddenFieldModuleSelection2.Value) || (ModuleID == HiddenFieldModuleSelection3.Value))
                {
                    string numberOfCharts = "0";
                    if (ModuleID == HiddenFieldModuleSelection1.Value)
                        numberOfCharts = HiddenFieldModuleCharts1.Value;
                    if (ModuleID == HiddenFieldModuleSelection2.Value)
                        numberOfCharts = HiddenFieldModuleCharts2.Value;
                    if (ModuleID == HiddenFieldModuleSelection3.Value)
                        numberOfCharts = HiddenFieldModuleCharts3.Value;
                    if (Convert.ToInt32(numberOfCharts) > 0)
                    {
                        ((System.Web.UI.HtmlControls.HtmlGenericControl)RepeaterCategory.Items[i].FindControl("spanPreviousModules")).Visible = true;
                        ((System.Web.UI.HtmlControls.HtmlGenericControl)RepeaterCategory.Items[i].FindControl("spanPreviousModules")).InnerText = numberOfCharts + " previous charts selected.";
                    }
                }
            }
        }
    }
    
    protected void RepeaterModule_ItemDataBound(Object Sender, RepeaterItemEventArgs e)
    {
        // This event is raised for the header, the footer, separators, and items.

        // Execute the following logic for Items and Alternating Items.
        if (e.Item.ItemType == ListItemType.Item || e.Item.ItemType == ListItemType.AlternatingItem)
        {
            GetModulesByCategory_P_Result rd = (GetModulesByCategory_P_Result)e.Item.DataItem;
            if (rd.PreviousModuleChartsAvailable == null) 
            {
                ((TextBox)e.Item.FindControl("TextBoxPreviousChartData")).Visible = false;
                ((Literal)e.Item.FindControl("LiteralNA")).Text = "N/A";
            }
            else
            {
                ((Literal)e.Item.FindControl("Literalof")).Text = " of ";
                ((Literal)e.Item.FindControl("LiteralCharts")).Text = " Charts";
            }

            if (Convert.ToInt32(rd.ParentModuleID) > 0)
            {
                ((HtmlGenericControl)e.Item.FindControl("spanDescription")).Attributes.Add("class", "spanDescription");
            }
        }
    }    

    protected bool validForm()
    {
        return true;
    }

    protected bool isComplete()
    {
        return true;
    }

    protected bool isValid()
    {
        return true;
    }

    protected bool IsModuleSelectionChanged()
    {
        if (!IsModulePreviouslySelected(RepeaterCategoryCataractAndAnterior, "CataractAndAnterior"))
            return true;
        if ((HiddenFieldModuleSelection1.Value == "0") && (HiddenFieldModuleSelection2.Value == "0") && (HiddenFieldModuleSelection3.Value == "0"))
            return false;
        if (!IsModulePreviouslySelected(RepeaterCategoryCornea, "Cornea"))
            return true;
        if ((HiddenFieldModuleSelection1.Value == "0") && (HiddenFieldModuleSelection2.Value == "0") && (HiddenFieldModuleSelection3.Value == "0"))
            return false;
        if (!IsModulePreviouslySelected(RepeaterCategoryGlaucoma, "Glaucoma"))
            return true;
        if ((HiddenFieldModuleSelection1.Value == "0") && (HiddenFieldModuleSelection2.Value == "0") && (HiddenFieldModuleSelection3.Value == "0"))
            return false;
        //if (!IsModulePreviouslySelected(RepeaterCategoryNeuroOpthalmogy, "NeuroOpthalmogy"))
        //    return true;
        if ((HiddenFieldModuleSelection1.Value == "0") && (HiddenFieldModuleSelection2.Value == "0") && (HiddenFieldModuleSelection3.Value == "0"))
            return false;
        //if (!IsModulePreviouslySelected(RepeaterCategoryUveitis, "Uveitis"))
        //    return true;
        if ((HiddenFieldModuleSelection1.Value == "0") && (HiddenFieldModuleSelection2.Value == "0") && (HiddenFieldModuleSelection3.Value == "0"))
            return false;
        if (!IsModulePreviouslySelected(RepeaterCategoryOculoplastics, "Oculoplastics"))
            return true;
        if ((HiddenFieldModuleSelection1.Value == "0") && (HiddenFieldModuleSelection2.Value == "0") && (HiddenFieldModuleSelection3.Value == "0"))
            return false;
        if (!IsModulePreviouslySelected(RepeaterCategoryPathologyOncology, "PathologyOncology"))
            return true;
        if ((HiddenFieldModuleSelection1.Value == "0") && (HiddenFieldModuleSelection2.Value == "0") && (HiddenFieldModuleSelection3.Value == "0"))
            return false;
        if (!IsModulePreviouslySelected(RepeaterCategoryPediatrics, "Pediatrics"))
            return true;
        if ((HiddenFieldModuleSelection1.Value == "0") && (HiddenFieldModuleSelection2.Value == "0") && (HiddenFieldModuleSelection3.Value == "0"))
            return false;
        if (!IsModulePreviouslySelected(RepeaterCategoryRefractive, "Refractive"))
            return true;
        if ((HiddenFieldModuleSelection1.Value == "0") && (HiddenFieldModuleSelection2.Value == "0") && (HiddenFieldModuleSelection3.Value == "0"))
            return false;
        if (!IsModulePreviouslySelected(RepeaterCategoryRetinaVitreous, "RetinaVitreous"))
            return true;
        if ((HiddenFieldModuleSelection1.Value == "0") && (HiddenFieldModuleSelection2.Value == "0") && (HiddenFieldModuleSelection3.Value == "0"))
            return false;
        return false;
    }

    protected bool IsModulePreviouslySelected(Repeater RepeaterCategory, string ModuleName)
    {
        for (var i = 0; i <= RepeaterCategory.Items.Count - 1; i++)
        {
            if (((System.Web.UI.WebControls.CheckBox)RepeaterCategory.Items[i].FindControl("CheckBoxModule" + ModuleName)).Checked)
            {
                string ModuleID = ((HiddenField)RepeaterCategory.Items[i].FindControl("HiddenFieldModuleID")).Value;
                if ((ModuleID != HiddenFieldModuleSelection1.Value) && (ModuleID != HiddenFieldModuleSelection2.Value) && (ModuleID != HiddenFieldModuleSelection3.Value))
                    return false;
                if (ModuleID == HiddenFieldModuleSelection1.Value)
                    HiddenFieldModuleSelection1.Value = "0";
                if (ModuleID == HiddenFieldModuleSelection2.Value)
                    HiddenFieldModuleSelection2.Value = "0";
                if (ModuleID == HiddenFieldModuleSelection3.Value)
                    HiddenFieldModuleSelection3.Value = "0";
            } 
            else
            {
                string ModuleID = ((HiddenField)RepeaterCategory.Items[i].FindControl("HiddenFieldModuleID")).Value;
                if ((ModuleID == HiddenFieldModuleSelection1.Value) || (ModuleID == HiddenFieldModuleSelection2.Value) || (ModuleID == HiddenFieldModuleSelection3.Value))
                    return false;
            }
        }
        return true;
    }

    protected void LinkButtonSave_Click(object sender, CommandEventArgs e)
    {
        string mid = (String)e.CommandArgument;
        saveRecord();
        Response.Redirect("ModuleOverview.aspx?mid=" + mid);
    }

    protected void ButtonSubmit_Click(object sender, EventArgs e)
    {
        using (NetHealthPIMEntities pim = new NetHealthPIMEntities())
        {
            bool modulesSelected = false;
            ParticipantModuleCycle cycle = pim.ParticipantModuleCycle.First(c => c.ParticipantModuleCycleID == ctx.ActiveModuleCycleID);
            int? ChartsRequired = (from c in pim.Module where c.ModuleID == ctx.ActiveModuleID select c.ChartsRequired).FirstOrDefault();
                modulesSelected = (cycle.SelectNewModulesComplete == true);

                if ((HiddenFieldModuleSelectionChanged.Value == "0") && (modulesSelected == true))
                    Response.Redirect("~/" + module.ModuleCode.ToLower() + "/ModuleDetails.aspx");

                if (saveRecord())
                {

                    Response.Redirect("~/" + module.ModuleCode.ToLower() + "/ModuleDetails.aspx");
                }
                else
                {
                    Response.Redirect("~/" + module.ModuleCode.ToLower() + "/ParticipantProfile.aspx");
                }   
        }
    }
    protected void ButtonPriorModules_Click(object sender, EventArgs e)
    {
        Response.Redirect("~/" + module.ModuleCode.ToLower() + "/SelectPriorModules.aspx");
    }

    protected bool saveRecord()
    {
        bool completeRecord = false;
        bool editMode = (HiddenFieldRecordID.Value.Length > 0);
        if (validForm())
        {
            completeRecord = isComplete();
            bool validRecord = isValid();

            using (NetHealthPIMEntities pim = new NetHealthPIMEntities())
            {
                using (TransactionScope transaction = new TransactionScope())
                {
                    int cycleID = ctx.ActiveModuleCycleID;
                    ParticipantModuleCycle cycle = pim.ParticipantModuleCycle.First(c => c.ParticipantModuleCycleID == cycleID);

                    UpdateModules(cycle, pim, RepeaterCategoryCataractAndAnterior, "CataractAndAnterior");
                    UpdateModules(cycle, pim, RepeaterCategoryCornea, "Cornea");
                    UpdateModules(cycle, pim, RepeaterCategoryGlaucoma, "Glaucoma");
                    //UpdateModules(cycle, pim, RepeaterCategoryNeuroOpthalmogy, "NeuroOpthalmogy");
                    //UpdateModules(cycle, pim, RepeaterCategoryUveitis, "Uveitis");
                    UpdateModules(cycle, pim, RepeaterCategoryOculoplastics, "Oculoplastics");
                    UpdateModules(cycle, pim, RepeaterCategoryPathologyOncology, "PathologyOncology");
                    UpdateModules(cycle, pim, RepeaterCategoryPediatrics, "Pediatrics");
                    UpdateModules(cycle, pim, RepeaterCategoryRefractive, "Refractive");
                    UpdateModules(cycle, pim, RepeaterCategoryRetinaVitreous, "RetinaVitreous");

                    if (cycle.CycleNumber == 1)
                    {
                        var totalChartsSelected = (from chart in pim.ParticipantChartStatus_V
                                                    where chart.ParticipantModuleCycleID == ctx.ActiveModuleCycleID
                                                    select chart).Sum(n => n.TotalCharts);

                        if (totalChartsSelected >= Constants.TOTAL_NUMBER_OF_CHARTS)
                        {
                            cycle.SelectNewModulesComplete = true;
                            cycle.SelectNewModulesCompletedDate = DateTime.Today;
                            cycle.ReportApproved = false;
                            if (cycle.SelectPreviousChartDataComplete == false)
                                cycle.SelectPreviousChartDataCompletedDate = DateTime.Today;
                            cycle.SelectPreviousChartDataComplete = true;
                            pim.SaveChanges();
                            transaction.Complete();
                        }
                        else
                        {
                            Session["MinCharts"] = "0";
                            Response.Redirect("~/" + module.ModuleCode.ToLower() + "/ModuleSelection.aspx");
                        }
                    }
                    else
                    {
                        cycle.SelectNewModulesComplete = true;
                        cycle.SelectNewModulesCompletedDate = DateTime.Today;
                        cycle.ReportApproved = false;
                        if (cycle.SelectPreviousChartDataComplete == false)
                            cycle.SelectPreviousChartDataCompletedDate = DateTime.Today;
                        cycle.SelectPreviousChartDataComplete = true;
                        pim.SaveChanges();
                        transaction.Complete();
                    }
                }
            }
        }
        return completeRecord;
    }

    protected void UpdateModules(ParticipantModuleCycle cycle, NetHealthPIMEntities pim, Repeater RepeaterCategory, string ModuleName)
    {
        for (var i = 0; i <= RepeaterCategory.Items.Count - 1; i++)
        {
            var ModuleID = Convert.ToInt32(((HiddenField)RepeaterCategory.Items[i].FindControl("HiddenFieldModuleID")).Value);

            int NumberOfChartsSelected = 0;
            if (((System.Web.UI.WebControls.TextBox)RepeaterCategory.Items[i].FindControl("TextBoxNumberOfCharts")) != null)
            {
                if (Convert.ToInt32(((System.Web.UI.WebControls.TextBox)RepeaterCategory.Items[i].FindControl("TextBoxNumberOfCharts")).Text) > 0)
                    NumberOfChartsSelected = Convert.ToInt32(((System.Web.UI.WebControls.TextBox)RepeaterCategory.Items[i].FindControl("TextBoxNumberOfCharts")).Text);
            }
        
            if (NumberOfChartsSelected > 0)
            {
                // delete previous data for this module. If needed it will be added in the previous module section.
                CycleManager.DeletePreviousChartRegistration(cycle.ParticipantModuleCycleID, ModuleID);

                var moduleSelected = (from pms in pim.ParticipantModuleSelection
                                      where pms.ParticipantModuleCycleID == cycle.ParticipantModuleCycleID
                                      && pms.ModuleID == ModuleID
                                      select pms).Count();

                // new module selected?
                if (moduleSelected == 0)
                {
                    DateTime baselineDate = DateTime.Today;

                    Module module = pim.Module.First(c => c.ModuleID == ModuleID);
                    ParticipantModuleSelection pms = new ParticipantModuleSelection();
                    pms.Module = module;
                    pms.ParticipantModuleCycle = cycle;
                    pms.ModuleComplete = false;
                    pms.Created = baselineDate;
                    pms.SelfAssessmentSurveyTargetDate = baselineDate.AddDays(28);
                    pms.SystemSurveyTargetDate = baselineDate.AddDays(28);
                    pms.ModuleCompleteTargetDate = baselineDate.AddDays(84);
                    pms.AbstractDataTargetDate = baselineDate.AddDays(28);
                    pms.ReportApprovedTargetDate = baselineDate.AddDays(28);
                    pms.MeasuresSelectedTargetDate = baselineDate.AddDays(42);
                    pms.ImprovementPlanTargetDate = baselineDate.AddDays(42);
                    pms.ImpactAssessmentTargetDate = baselineDate.AddDays(42);
                    pms.PreviousModuleCharts = false;
                    pim.SaveChanges();
                }
                else
                {
                    // reset modules previous selected as not confirmed.
                    ParticipantModuleSelection pms = pim.ParticipantModuleSelection.First(c => c.ParticipantModuleCycleID == cycle.ParticipantModuleCycleID && c.ModuleID == ModuleID);
                    pms.ModuleConfirm = null;
                    pms.ModuleCofirmDate = null;
                    pms.NumberOfCharts = null;
                    pms.PreviousModuleCharts = false;
                    pim.SaveChanges();
                }
            }
            else // delete module previously selected
            {
                var moduleSelected = (from pms in pim.ParticipantModuleSelection
                                      where pms.ParticipantModuleCycleID == cycle.ParticipantModuleCycleID
                                      && pms.ModuleID == ModuleID
                                      select pms).Count();

                if (moduleSelected > 0)
                {
                    var moduleToDelete = (from pms in pim.ParticipantModuleSelection
                                          where pms.ParticipantModuleCycleID == cycle.ParticipantModuleCycleID
                                          && pms.ModuleID == ModuleID
                                          select pms);
                    foreach (var m in moduleToDelete)
                    {
                        pim.DeleteObject(m);
                    }
                    var chartsToDelete = (from cr in pim.ChartRegistration
                                          where cr.ParticipantModuleCycleID == cycle.ParticipantModuleCycleID
                                          && cr.ModuleID == ModuleID
                                          select cr);
                    foreach (var c in chartsToDelete)
                    {
                        pim.DeleteObject(c);
                    }

                    var systemSurveyToDelete = (from pms in pim.SystemSurvey
                                          where pms.ParticipantModuleCycleID == cycle.ParticipantModuleCycleID
                                          && pms.ModuleID == ModuleID
                                          select pms);
                    foreach (var m in systemSurveyToDelete)
                    {
                        pim.DeleteObject(m);
                    }

                    var selfAssessmentToDelete = (from pms in pim.SelfAssessment
                                                where pms.ParticipantModuleCycleID == cycle.ParticipantModuleCycleID
                                                && pms.ModuleID == ModuleID
                                                select pms);
                    foreach (var m in selfAssessmentToDelete)
                    {
                        pim.DeleteObject(m);
                    }

                    var ChartAnswers=(from c in pim.ChartQuestionUserReply 
                                      where c.ModuleID==ModuleID 
                                      && c.ParticipantModuleCycleID ==cycle.ParticipantModuleCycleID select c).ToList();
                    foreach(var m in ChartAnswers)
                    {
                         var ChartChoicesUserReply=(from c in pim.PIMChartQuestionChoicesUserReply 
                                                    where c.ParticipantModuleCycleID==cycle.ParticipantModuleCycleID 
                                                    && c.ChartID == m.ChartID select c).ToList();
                         foreach (var choice in ChartChoicesUserReply)
                         {
                             pim.PIMChartQuestionChoicesUserReply.DeleteObject(choice);
                         }
                         pim.ChartQuestionUserReply.DeleteObject(m);
                    }

                    pim.SaveChanges();
                }
            }

            // Previous Modules
            int PreviousNumberOfChartsSelected = 0;
            if (((System.Web.UI.WebControls.TextBox)RepeaterCategory.Items[i].FindControl("TextBoxPreviousChartData")) != null)
            {
                if (Convert.ToInt32(((System.Web.UI.WebControls.TextBox)RepeaterCategory.Items[i].FindControl("TextBoxPreviousChartData")).Text) > 0)
                    PreviousNumberOfChartsSelected = Convert.ToInt32(((System.Web.UI.WebControls.TextBox)RepeaterCategory.Items[i].FindControl("TextBoxPreviousChartData")).Text);
            }
            if (PreviousNumberOfChartsSelected > 0)
            {
                Participant participant = pim.Participant.First(p => p.ParticipantID == ctx.ParticipantID);
                CycleManager.AddPreviousChartRegistration(ctx.ActiveModuleCycleID, participant.CandidateID, ModuleID, PreviousNumberOfChartsSelected);
            }

            // Confirm module.
            if (NumberOfChartsSelected > 0) 
            {
                ConfirmModule(ModuleID, NumberOfChartsSelected);
            }
        }
    }

    protected void DeletePreviousModule(ParticipantModuleCycle cycle, NetHealthPIMEntities pim)
    {
            var moduleToDelete = (from pms in pim.ParticipantModuleSelection
                                  where pms.ParticipantModuleCycleID == cycle.ParticipantModuleCycleID
                                  && pms.PreviousModuleCharts == true
                                  select pms);
            foreach (var m in moduleToDelete)
            {
                pim.DeleteObject(m);
            }
            var chartsToDelete = (from cr in pim.ChartRegistration
                                  from pms in pim.ParticipantModuleSelection 
                                  where cr.ModuleID == pms.ModuleID 
                                  && pms.ParticipantModuleCycleID == cr.ParticipantModuleCycleID
                                  && cr.ParticipantModuleCycleID == cycle.ParticipantModuleCycleID
                                  && pms.PreviousModuleCharts == true
                                  select cr);
            foreach (var c in chartsToDelete)
            {
                pim.DeleteObject(c);
            }

            pim.SaveChanges();
    }

    protected void UpdatePreviousModule(ParticipantModuleCycle cycle, NetHealthPIMEntities pim, int ModuleID, int NumberOfCharts)
    {
        var moduleSelected = (from pms in pim.ParticipantModuleSelection
                              where pms.ParticipantModuleCycleID == cycle.ParticipantModuleCycleID
                              && pms.ModuleID == ModuleID
                              select pms).Count();

        if (moduleSelected == 0)
        {
            DateTime baselineDate = DateTime.Today;

            Module module = pim.Module.First(c => c.ModuleID == ModuleID);
            ParticipantModuleSelection pms = new ParticipantModuleSelection();
            pms.Module = module;
            pms.ParticipantModuleCycle = cycle;
            pms.ModuleComplete = false;
            pms.NumberOfCharts = NumberOfCharts;
            pms.Created = baselineDate;
            pms.SelfAssessmentSurveyTargetDate = baselineDate.AddDays(28);
            pms.SystemSurveyTargetDate = baselineDate.AddDays(28);
            pms.ModuleCompleteTargetDate = baselineDate.AddDays(84);
            pms.AbstractDataTargetDate = baselineDate.AddDays(28);
            pms.ReportApprovedTargetDate = baselineDate.AddDays(28);
            pms.MeasuresSelectedTargetDate = baselineDate.AddDays(42);
            pms.ImprovementPlanTargetDate = baselineDate.AddDays(42);
            pms.ImpactAssessmentTargetDate = baselineDate.AddDays(42);
            pms.PreviousModuleCharts = true;
            pim.SaveChanges();
            Participant participant = pim.Participant.First(p => p.ParticipantID == ctx.ParticipantID);
            CycleManager.AddPreviousChartRegistration(cycle.ParticipantModuleCycleID, participant.CandidateID, ModuleID, 100);
        }
        else
        {
            Participant participant = pim.Participant.First(p => p.ParticipantID == ctx.ParticipantID);
            var chartRegistrationExists = pim.ChartRegistration.Count(p => p.ParticipantModuleCycleID == cycle.ParticipantModuleCycleID && p.ModuleID == ModuleID);
            if (chartRegistrationExists == 0)
            {
                ParticipantModuleSelection pms = pim.ParticipantModuleSelection.First(c => c.ParticipantModuleCycleID == cycle.ParticipantModuleCycleID && c.ModuleID == ModuleID);
                pms.PreviousModuleCharts = true;
                pim.SaveChanges(); 
                CycleManager.AddPreviousChartRegistration(cycle.ParticipantModuleCycleID, participant.CandidateID, ModuleID, 100);
            }
            else
            {
                // reset modules previous selected as not confirmed.
                ParticipantModuleSelection pms = pim.ParticipantModuleSelection.First(c => c.ParticipantModuleCycleID == cycle.ParticipantModuleCycleID && c.ModuleID == ModuleID);
                pms.ModuleConfirm = null;
                pms.ModuleCofirmDate = null;
                pms.NumberOfCharts = null;
                pim.SaveChanges();
            }
        }
    }
    protected void UpdateModule_Click(object sender, EventArgs e)
    {
       
    }

    protected void ConfirmModule(int ModuleID, int ChartsToEnter)
    {
        using (NetHealthPIMEntities pim = new NetHealthPIMEntities())
        {
            int cycleID = ctx.ActiveModuleCycleID;
            ParticipantModuleCycle cycle = pim.ParticipantModuleCycle.First(c => c.ParticipantModuleCycleID == cycleID);

            AddInitialChartRegistration(cycle.ParticipantModuleCycleID, ModuleID, ChartsToEnter);

            var modulesNOTConfirm = (from m in pim.Module
                                     join pms in pim.ParticipantModuleSelection on m.ModuleID equals pms.ModuleID
                                     where pms.ParticipantModuleCycleID == cycle.ParticipantModuleCycleID
                                     && (pms.ModuleConfirm == false || pms.ModuleConfirm == null)
                                     select pms).Count();
            if (modulesNOTConfirm == 0)
            {
                cycle.ModuleComplete = true;
                cycle.ModuleCompleteDate = DateTime.Today;
                pim.SaveChanges();
            }
        }
    }

    protected void AddInitialChartRegistration(int ParticipantModuleCycleID, int ModuleID, int ChartsToEnter)
    {
        using (EntityConnection conn = new EntityConnection("name=NetHealthPIMEntities"))
        {
            conn.Open();
            EntityCommand cmd = conn.CreateCommand();
            cmd.CommandText = "NetHealthPIMEntities.AddInitialChartRegistration";
            cmd.CommandType = CommandType.StoredProcedure;
            cmd.Parameters.AddWithValue("ParticipantModuleCycleID", ParticipantModuleCycleID);
            cmd.Parameters.AddWithValue("ModuleID", ModuleID);
            cmd.Parameters.AddWithValue("ChartsToEnter", ChartsToEnter);
            cmd.Parameters.AddWithValue("TOTAL_NUMBER_OF_CHARTS", Constants.TOTAL_NUMBER_OF_CHARTS);
            cmd.Parameters.AddWithValue("DeletePreviousRegistration", false);
            cmd.ExecuteNonQuery();
            conn.Close();
            ctx.WorkingModuleID = ModuleID;
            Session[Constants.SESSION_WORKINGMODULEID] = ModuleID;
        }
    }

    protected void nose_Init(object sender, EventArgs e) 
    {
        System.Web.UI.WebControls.CheckBox mycontrol = sender as System.Web.UI.WebControls.CheckBox;
        mycontrol.CssClass = "check";
        mycontrol.Checked = true;
    }
}

