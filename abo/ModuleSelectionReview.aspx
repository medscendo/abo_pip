﻿<%@ Page Title="" Language="C#" MasterPageFile="~/PIMMasterPage.master" AutoEventWireup="true" CodeFile="ModuleSelectionReview.aspx.cs" Inherits="abo_ModuleSelectionReview" %>

<asp:Content ID="Content1" ContentPlaceHolderID="head" Runat="Server">

<style>
.surveys_complete 
{
width: 100%;
font-size: 18px;
color:#3e2449;
text-align:center;
}

.upper-panel {
float:left;
margin-bottom:30px;
}

.table th, .prior-data {
text-align:center;
}

.table a:hover {
text-decoration:underline;
}

.table tr th, .table tr td {
border-right:none;
border-left:none;
}

.table tr th.first, .table tr td.first {
border-left: 1px solid #ccc;
}
.table tr th.last, .table tr td.last {
border-right: 1px solid #ccc;
}

.main-link {
font-weight: bold;
}

.legend {
float:left;
border: 3px solid #333;
padding: 10px;
font-size:10px;
}
.rbl input[type="radio"]
{
margin-left: 15px;
margin-right: 1px;
font-weight:bold;
}
.rbl label
{
font-weight: bold;
font-size: 15px;
margin-left: 5px;
}
#placeholder-content {
float:none;
}
</style>

</asp:Content>

<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder1" Runat="Server">

    <asp:HiddenField runat="server" ID="HiddenFieldPlanProgressPct" />
    <div class="heading_box">
        <%--<h2><asp:Literal ID="LiteralShortModuleDescription" runat="server"></asp:Literal>Create Your Experience</h2>--%>
    </div>
     <table class="table" style="margin-bottom:20px">
        <tr>
            <th class="first">Your Experience Includes:</th>
            <th>Status</th>
            <th>Using Prior Data?</th>
            <th class="last">Comments</th>
        </tr> 
        <tr>
            <td class="first"><a href="PQRSSelection.aspx" class="main-link">PQRS Reporting</a></td>
            <td><asp:Label runat="server" ID = "LabelPQRSInceptiveProgramStatus"></asp:Label></td>
            <td class="prior-data"><asp:Label runat="server" ID = "LabelPQRSInceptiveProgramPriorData"></asp:Label></td>
            <td class="last"><asp:Label runat="server" ID = "LabelPQRSInceptiveProgramComments"></asp:Label></td>
        </tr>
        <tr class="bg">
            <td class="first"><a runat="server" id="ModuleSelectionLink" href="SelectPriorModules.aspx" class="main-link">Improvement in Medical Practice Activities</a></td>
            <td><asp:Label runat="server" ID = "LabelModulesStatus"></asp:Label></td>
            <td class="prior-data"><asp:Label runat="server" ID = "LabelModulesPriorData"></asp:Label></td>
            <td class="last"><asp:Label runat="server" ID = "LabelModulesComments"></asp:Label></td>
        </tr>
        
        <asp:Repeater ID="RepeaterModules" runat="server">
            <HeaderTemplate>
            </HeaderTemplate>
            <ItemTemplate>
            <asp:HiddenField ID="HiddenFieldModuleID" Value='<%# Eval("ModuleID") %>' runat="server" />
            <tr class="bg">
                <td class="first" style="padding-left:20px;"><a href='OpenModule.aspx?mid=<%# Eval("ModuleID") %>'><%# Eval("ModuleName")%></a></td>
                <td><%# Eval("LabelModulesStatus")%></td>
                <td class="prior-data"><%# Eval("LabelPriorData")%></td>
                <td class="last"><%# Eval("LabelComments")%></td>
            </tr>
            </ItemTemplate>
            <FooterTemplate>
            </FooterTemplate>
        </asp:Repeater>        
         <tr>
             <td colspan="4">If you would like to use the PQRS-Enabled Improvement in Medical Practice Activities, please click on <strong>PQRS Reporting</strong> above.</td>
         </tr>    
        <tr style="display:none">
            <td class="first"><a href="SelectPEC.aspx" class="main-link">Patient Experience of Care Survey</a></td>
            <td><asp:Label runat="server" ID = "LabelPECStatus"></asp:Label></td>
            <td class="prior-data"><asp:Label runat="server" ID = "LabelPECPriorData"></asp:Label></td>
            <td class="last"><asp:Label runat="server" ID = "LabelPECComments"></asp:Label></td>                
        </tr>
    </table>
    <!-- ABO determined certain sections to no longer show on this page. -->
    <div class="upper-panel clearfix">
        <div class="text-panel" style="display:none">
            In addition to selecting one to three improvement in medical practice activitie(s), you can elect to include other incentives and/or activities in your experience. These include:  
            <p><strong>PQRS Reporting:</strong></p>
            <p>The ABO offers diplomates the ability to participate in the Centers for Medicare and Medicaid Services (CMS)'s PQRS reporting through the Part IV Improvement in Medical Practice Platform. Several ABO PIP activities align with the CMS PQRS program and the data entered may be used for participation in Healthmonix's PQRSPRO registry. You can choose this option whether or not you are submitting PQRS data through claims; CMS will take the 'best' (most accurate) results for your submission.  There are two options for participation in PQRS reporting using PQRS-enabled Improvement in Medical Practice Activities:</p> 
            <p><em>Measures Group Reporting: Cataract Surgery</em><br />
               Select this activity and 20 patient chart entries (11 of which must be Medicare patients) out of <em>the 30 charts entered will qualify for PQRS reporting</em>. Details on Cataract Measures Group reporting may be found <a href='https://www.pqrspro.com/cmsgroups/2016/cataracts'>here.</a></p>  
            <p><em>Individual Measures Reporting: Primary Open Angle Glaucoma, Advanced Macular Degeneration, Diabetic Retinopathy</em><br />
               Each of these activities includes PQRS measures, which require you to report data for 50% of Medicare patient visits on at least three measures.  These include measures from the POAG (2 available), AMD (2 measures available) and Diabetic Retinopathy (3 measures available) PIP activities. If choosing the POAG or AMD activity, then be sure to also choose one of the others listed in order to qualify (<em>at least 3 measures must be reported</em>).</p>
            <p>Report data for 50% of eligible Medicare Part B patient visits for 9 individual measures (including one cross-cutting measure), or one MAV Cluster and one cross-cutting measure. Explore the alternatives for successful 2015 PQRS reporting in the PQRSPRO Ophthalmology Measures Options.</p>
            <p>By selecting these PQRS-enabled Improvement in Medical Practice Activities, you can participate in PQRS measure reporting through the platform vendor, Healthmonix's PQRSPRO Registry. Data will be seamlessly passed over to the <a href="http://www.pqrspro.com/"> PQRSPRO</a> system, leaving minimal data entry to complete the process.  The additional cost to use the PQRS-enabled Improvement in Medical Practice Activities to submit for PQRS through the PQRSPRO system is $199 (a $90 discount for ABO diplomates). Contact the experts at Healthmonix for more information 610.590.2229 or 1.888.720.4100.</p>
            <p style="font-style:italic">The Physician Quality Reporting System (PQRS) is a continuation of the Center for Medicaid and Medicare Services (CMS) PQRI program, initiated in 2007. Offering an incentive reimbursement in 2007 through 2014, it has transitioned to a negative payment adjustment program. The program provides avoidance of negative payment adjustments from 2015 on, via the submission of de-identified patient data on specified quality measures. In 2018 a -2% "payment adjustment" will be applied to non-participants in 2016. </p>
            <asp:Label runat="server" ID="textDuring" Visible="false">
            
            </asp:Label>
        </div>
        <div style="display:none; float: right;border: 0px solid #333;padding: 10px;font-size: 10px;width: 275px;background-color:#dfe6ec;margin-top: 20px; margin-bottom:20px">
            <span style="font-size:15px;"><center>Would you like to participate in the PQRS Reporting Program?</center></span>
            <asp:RadioButtonList style="margin: 20px auto 5px;" class="rbl" RepeatDirection="Horizontal" ID="RadioButtonListPqrs" runat="server">
                <asp:ListItem Value="True">Yes</asp:ListItem>
                <asp:ListItem Value="False">No</asp:ListItem>
            </asp:RadioButtonList>
            <p><center>If you select yes, you will be presented with the PQRS-enabled Improvement in Medical Practice Activities on the Activity Selection Page.</center></p>
           <center> <asp:Button ID="ButtonPqrsUpdate" OnClientClick="return confirm('Are you sure you want to update your PQRS status?');" runat="server" Text="Update" OnClick="ButtonPqrsUpdate_Click" /></center>
        </div>
           <div class="progress-panel" style="float:left">
            <h3>Create Your Experience Progress</h3>
            <div class="progressbar">
                <div class="progress" id="progress1"></div>
            </div>
            <asp:Label ID="Label1" runat="server">&nbsp;</asp:Label>
            <asp:LinkButton runat="server" ID="LinkButtonNextStep" OnCommand="LinkButtonNext_Click" class="button blue">Continue where you left off</asp:LinkButton>
        </div>

        <div style="display:none; float: right;border: 3px solid #333;padding: 10px;font-size: 10px;width: 275px;margin-bottom:20px; margin-left:20px;">
            <div><b>Status Legend:</b></div>
            <div>Not Selected: Still need to indicate if this component is included in your experience</div>
	        <div>Included: You have selected to include this in your experience</div>
	        <div>Not Included: You have selected to NOT include this in your experience</div>
	        <div>Activities:</div>
	        <div>&nbsp;&nbsp;&nbsp;Step 1: Select activity to include</div>
	        <div>&nbsp;&nbsp;&nbsp;Step 2: Register charts for activity</div>
        </div>
    </div>
<br>
    <div class="button-box">
        <asp:LinkButton ID="LinkButtonSave" runat="server" OnCommand="LinkButtonSave_Click" Text="CONFIRM" CssClass="button" />
        <%--<asp:LinkButton ID="LinkButtonNext" runat="server" Text=" Next " OnCommand="LinkButtonNext_Click" Visible="false" CssClass="button"/>--%>
    </div>
</asp:Content>

<asp:Content ID="Content4" ContentPlaceHolderID="javascript" runat="server">

    <script type="text/javascript">
        $(document).ready(function () {

            // This variable sets the percentage complete
            var prog = $("#<%= HiddenFieldPlanProgressPct.ClientID %>").val();
            var progressClass = document.getElementById('progress1');
            if (prog > 100)
                prog = 100;

            // Set width, changing how much progress bar shows
            $("#progress1").css("width", prog + "%");

            // Puts numbers to right 
            if (prog < 10) {
                progressClass.innerHTML = '<p style="left:30px; color:#7f238e">' + prog + '%</p>';
            }
            else {
                progressClass.innerHTML = '<p>' + prog + '%</p>';
            }
        });
        function ValidateForm() {
            return true;
        }
    </script>

</asp:Content>