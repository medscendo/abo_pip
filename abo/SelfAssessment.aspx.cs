﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Web.UI.HtmlControls;
using NetHealthPIMModel;
using MoreLinq;
using AjaxControlToolkit;

    public partial class abo_SelfAssessment : BasePage
    {
      
        protected void Page_Load(object sender, EventArgs e)
        {
            ((PIMMasterPage)Page.Master).SetTitle("Self-Assessment");
            ((PIMMasterPage)Page.Master).SetStatus(2);
            using (NetHealthPIMEntities pim = new NetHealthPIMEntities())
            {

                bool stillCycleOne = true;
                ParticipantModuleCycle prev = (ParticipantModuleCycle)Session[Constants.SESSION_PREVIOUSCYCLE];
                String CycleNumber = Request.QueryString["CycleNumber"];

                int cycleID = ctx.ActiveModuleCycleID;
                if (CycleNumber == "1")
                {
                    stillCycleOne = (cycleID == prev.ParticipantModuleCycleID);
                    cycleID = prev.ParticipantModuleCycleID;
                    if (stillCycleOne == false)
                    {

                        ButtonSubmit.Visible = false;
                    }
                }
                else
                {
                    ((PIMMasterPage)Page.Master).SetStatus(3);
                    LinkButtonReturnToStatus.PostBackUrl = "Dashboard3.aspx";

                }

                ParticipantModuleCycle cycle = pim.ParticipantModuleCycle.First(c => c.ParticipantModuleCycleID == cycleID);

                int modid = (int)Session[Constants.SESSION_WORKINGMODULEID];
                Module module = pim.Module.First(c => c.ModuleID == modid);
                ((PIMMasterPage)Page.Master).SetHeader("Self-Assessment: " + module.ModuleName);
                HtmlTable table = new HtmlTable();
                var questions = (from c in pim.SelfAssessmentQuestion where c.Module.ModuleID == modid select c).OrderBy(b => b.SAMeasureQuestionSortOrder).ToList();
                var modulename = (from c in pim.Module where c.ModuleID == modid select c.ModuleName).FirstOrDefault();

                foreach (var item in questions)
                {
                    HtmlTableRow row = new HtmlTableRow();
                    HtmlTableCell cell1 = new HtmlTableCell();
                    HtmlTableCell cell2 = new HtmlTableCell();
                    cell1.InnerHtml = item.SAMeasureQuestion;
                    if (item.SAMeasureQuestionType == 1)
                    {
                        RadioButtonList rb = new RadioButtonList();
                        rb.ID = "choices" + item.SAQuestionID.ToString();
                        rb.Items.Add(new ListItem("Yes", "True"));
                        rb.Items.Add(new ListItem("No", "False"));
                        rb.RepeatDirection = System.Web.UI.WebControls.RepeatDirection.Horizontal;
                        cell2.Controls.Add(rb);
                    }
                    if (item.SAMeasureQuestionType == 9)
                    {
                        TextBox tx = new TextBox();
                        tx.ID = "choices" + item.SAQuestionID.ToString();
                        tx.TextMode = TextBoxMode.MultiLine;
                        tx.Width = 300;
                        tx.Height = 100;
                        tx.MaxLength = 100;
                        cell2.Controls.Add(tx);
                    }
                    if (item.SAMeasureQuestionType == 2)
                    {
                        TextBox tx = new TextBox();
                        tx.ID = "choices" + item.SAQuestionID.ToString();
                        tx.MaxLength = 100;
                        cell2.Controls.Add(tx);
                    }
                    if (item.SAMeasureQuestionType == 5)
                    {
                        TextBox tx = new TextBox();
                        tx.ID = "choices" + item.SAQuestionID.ToString();
                        tx.Attributes.Add("maxlength", "3");
                        MaskedEditExtender txextender = new MaskedEditExtender();
                        txextender.ID="choices" + item.SAQuestionID.ToString()+"txextender";
                        txextender.Mask = "999";
                        txextender.AutoComplete = false;
                        txextender.TargetControlID = "choices" + item.SAQuestionID.ToString();
                        txextender.MaskType = MaskedEditType.Number;
                        MaskedEditValidator txextendervalidator = new MaskedEditValidator();
                        txextendervalidator.ControlExtender = "choices" + item.SAQuestionID.ToString() + "txextender";
                        txextendervalidator.ControlToValidate = "choices" + item.SAQuestionID.ToString();
                        txextendervalidator.IsValidEmpty=false;
                        txextendervalidator.MaximumValue="100";
                        txextendervalidator.EmptyValueMessage="Number is required";
                        txextendervalidator.InvalidValueMessage = "Number is invalid";
                        txextendervalidator.MaximumValueMessage = "Number > 0";
                        txextendervalidator.MinimumValueMessage="Number <= 100";
                        txextendervalidator.MinimumValue="0";
                        txextendervalidator.EmptyValueBlurredText="*";
                        txextendervalidator.InvalidValueBlurredMessage="*";
                        txextendervalidator.MaximumValueBlurredMessage="*";
                        txextendervalidator. MinimumValueBlurredText="*";
                        txextendervalidator.TooltipMessage = "Input a number: 0 up to 100";
                        txextendervalidator.Display = ValidatorDisplay.Dynamic;
                        cell2.Controls.Add(new LiteralControl("<span>Please enter a number from 0 to 100</span>"));
                        cell2.Controls.Add(new LiteralControl("<br />"));
                        cell2.Controls.Add(tx);
                        cell2.Controls.Add(txextender);
                        cell2.Controls.Add(txextendervalidator);
                     
                    }
                    if (item.SAMeasureQuestionType == 4)
                    {
                        CheckBoxList chklist = new CheckBoxList();
                        chklist.ID = "choices" + item.SAQuestionID.ToString();
                        var choices = (from c in pim.PIMSelfAssessmentQuestionChoices where c.QuestionID == item.SAQuestionID select c).ToList();
                        foreach (var choice in choices)
                        {
                            chklist.Items.Add(new ListItem(choice.Choice, choice.ChoiceID.ToString()));
                        }
                        cell2.Controls.Add(chklist);
                    }
                    if (item.SAMeasureQuestionType == 14)
                    {
                        DropDownList ddlist = new DropDownList();
                        ddlist.ID = "choices" + item.SAQuestionID.ToString();
                        var choices = (from c in pim.PIMSelfAssessmentQuestionChoices where c.QuestionID == item.SAQuestionID select c).ToList();
                        foreach (var choice in choices)
                        {
                            ddlist.Items.Add(new ListItem(choice.Choice, choice.ChoiceID.ToString()));
                        }
                        cell2.Controls.Add(ddlist);
                    }
                    if (item.SAMeasureQuestionType == 15)
                    {
                        DropDownList ddlist = new DropDownList();
                        ddlist.ID = "choices" + item.SAQuestionID.ToString();
                        ddlist.DataSource = CycleManager.getNewExamValues();
                        ddlist.DataTextField = "examLabel";
                        ddlist.DataValueField = "examValue";
                        ddlist.DataBind();
                        ddlist.Items.Insert(0, new ListItem("Select", ""));
                        cell2.Controls.Add(ddlist);
                    }
                    if (item.SAMeasureQuestionType == 3)
                    {
                        RadioButtonList rblist = new RadioButtonList();
                        rblist.ID = "choices" + item.SAQuestionID.ToString();
                        var choices = (from c in pim.PIMSelfAssessmentQuestionChoices where c.QuestionID == item.SAQuestionID select c).ToList();
                        foreach (var choice in choices)
                        {
                            rblist.Items.Add(new ListItem(choice.Choice, choice.ChoiceID.ToString()));
                        }
                        cell2.Controls.Add(rblist);
                    }
                    row.Controls.Add(cell1);
                    row.Controls.Add(cell2);
                    table.Controls.Add(row);

                }
                Panel1.Controls.Add(table);
                var measureinfo = (from c in pim.SelfAssessment
                                   from n in pim.SelfAssessmentQuestion
                                   where c.SAQuestionID == n.SAQuestionID
                                   && c.ModuleID == n.Module.ModuleID
                                   && c.ModuleID == modid
                                   && c.ParticipantModuleCycleID == cycleID
                                   select new { c.ModuleID, n.SAMeasureQuestionType, c.MeasureValue, c.MeasureID, n.SAMeasureQuestionSortOrder, c.QuestionTrueFalse, c.QuestionResponse, c.QuestionChoiceID, c.NumericResponse, c.VisualAcuityResponse, n.SAQuestionID }).DistinctBy(c => c.SAQuestionID).OrderBy(b => b.SAMeasureQuestionSortOrder);
                foreach (var item in measureinfo)
                {
                    Control myControl1 = Panel1.FindControl("choices" + item.SAQuestionID.ToString());
                    if (item.SAMeasureQuestionType == 1)
                    {
                        RadioButtonList rbl = (RadioButtonList)myControl1;
                        if (item.QuestionTrueFalse != null)
                        {
                            rbl.SelectedValue = item.QuestionTrueFalse.ToString();
                        }
                    }
                    if (item.SAMeasureQuestionType == 2 || item.SAMeasureQuestionType == 9)
                    {
                        TextBox txt = (TextBox)myControl1;
                        if (item.QuestionResponse != null)
                        {
                            txt.Text = item.QuestionResponse.ToString();
                        }
                    }
                    if (item.SAMeasureQuestionType == 5)
                    {
                        TextBox txt = (TextBox)myControl1;
                        if (item.NumericResponse != null)
                        {
                            txt.Text = item.NumericResponse.ToString();
                        }
                    }
                    if (item.SAMeasureQuestionType == 3)
                    {
                        RadioButtonList rbl = (RadioButtonList)myControl1;
                        if (item.QuestionChoiceID != null)
                        {
                            rbl.SelectedValue = item.QuestionChoiceID.ToString();
                        }
                    }
                    if (item.SAMeasureQuestionType == 4)
                    {
                        CheckBoxList chbl = (CheckBoxList)myControl1;

                        var SelectedChoices = (from c in pim.SelfAssessmentUserReplyChoices
                                               where c.QuestionID == item.SAQuestionID
                                               && c.ParticipantModuleCycleID == cycleID
                                               select c).ToList();

                        foreach (var ch in SelectedChoices)
                        {
                            chbl.Items.FindByValue(ch.ChoiceID.ToString()).Selected = true;
                        }
                    }
                    if (item.SAMeasureQuestionType == 14)
                    {
                        DropDownList ddl = (DropDownList)myControl1;
                        if (item.QuestionChoiceID != null)
                        {
                            ddl.SelectedValue = item.QuestionChoiceID.ToString();
                        }
                    }
                    if (item.SAMeasureQuestionType == 15)
                    {
                        DropDownList ddl = (DropDownList)myControl1;
                        if (item.VisualAcuityResponse != null)
                        {
                            ddl.SelectedValue = item.VisualAcuityResponse.ToString();
                        }
                    }

                }
            }
        }

        protected bool save()
        {
            using (NetHealthPIMEntities pim = new NetHealthPIMEntities())
            {

                int cycleID = ctx.ActiveModuleCycleID;
                bool completedAssessment = true;
                ParticipantModuleCycle cycle = pim.ParticipantModuleCycle.First(c => c.ParticipantModuleCycleID == cycleID);
                int modid = (int)Session[Constants.SESSION_WORKINGMODULEID];
                var questions = (from c in pim.SelfAssessmentQuestion where c.Module.ModuleID == modid select c).OrderBy(b => b.SAMeasureQuestionSortOrder).ToList();

                foreach (var item in questions)
                {
                    Control myControl1 = Panel1.FindControl("choices" + item.SAQuestionID.ToString());
                    SelfAssessment sf = (from n in pim.SelfAssessment
                                         where n.ParticipantModuleCycleID == cycleID
                                         && n.ModuleID == modid
                                         && n.SAQuestionID == item.SAQuestionID
                                         select n).FirstOrDefault();

                    if (item.SAMeasureQuestionType == 4)
                    {
                        CheckBoxList cbl = (CheckBoxList)myControl1;
                        if (item.Required != null && item.Required == true)
                        {
                            if (cbl.Items.Cast<ListItem>().Where(li => li.Selected)
                                                                                   .Select(li => li.Value)
                                                                                   .ToList().Count() <= 0)
                                completedAssessment = false;
                        }
                        if (sf == null)
                        {
                            sf = new SelfAssessment();
                            sf.ModuleID = modid;
                            sf.SAQuestionID = item.SAQuestionID;
                            sf.ParticipantModuleCycleID = cycleID;
                            sf.LastUpdateDate = DateTime.Now;
                            pim.AddToSelfAssessment(sf);
                            pim.SaveChanges();

                        }
                        else
                        {
                            sf.LastUpdateDate = DateTime.Now;
                            pim.SaveChanges();

                        }
                        //delete past choices
                        var deletepastchoices = (from c in pim.SelfAssessmentUserReplyChoices
                                                 where
                                                     c.ParticipantModuleCycleID == cycleID
                                                     && c.QuestionID == item.SAQuestionID
                                                 select c).ToList();

                        foreach (var ch in deletepastchoices)
                        {
                            pim.SelfAssessmentUserReplyChoices.DeleteObject(ch);
                            pim.SaveChanges();
                        }
                        // insert new choices
                        List<string> selectedValues = cbl.Items.Cast<ListItem>()
                                                                                   .Where(li => li.Selected)
                                                                                   .Select(li => li.Value)
                                                                                   .ToList();
                        foreach (var val in selectedValues)
                        {
                            SelfAssessmentUserReplyChoices userrep = new SelfAssessmentUserReplyChoices();
                            userrep.ParticipantModuleCycleID = cycleID;
                            userrep.QuestionID = item.SAQuestionID;
                            userrep.ChoiceID = Convert.ToInt32(val);
                            pim.SelfAssessmentUserReplyChoices.AddObject(userrep);
                            pim.SaveChanges();
                        }

                    }
                    if (item.SAMeasureQuestionType == 1)
                    {
                        RadioButtonList rbl = (RadioButtonList)myControl1;
                        if (item.Required != null && item.Required == true)
                        {
                            if (rbl.SelectedValue == "")
                                completedAssessment = false;
                        }
                        if (sf == null)
                        {
                            sf = new SelfAssessment();
                            if (rbl.SelectedValue != "")
                            {
                                sf.QuestionTrueFalse = Convert.ToBoolean(rbl.SelectedValue);
                                sf.ModuleID = modid;
                                sf.SAQuestionID = item.SAQuestionID;
                                sf.ParticipantModuleCycleID = cycleID;
                                sf.LastUpdateDate = DateTime.Now;
                                pim.AddToSelfAssessment(sf);
                                pim.SaveChanges();
                            }
                        }
                        else
                        {
                            sf.QuestionTrueFalse = Convert.ToBoolean(rbl.SelectedValue);
                            sf.LastUpdateDate = DateTime.Now;
                            pim.SaveChanges();

                        }

                    }
                    if (item.SAMeasureQuestionType == 2 || item.SAMeasureQuestionType == 9)
                    {

                        TextBox txt = (TextBox)myControl1;
                        if (item.Required != null && item.Required == true)
                        {
                            if (txt.Text == "")
                                completedAssessment = false;
                        }
                        if (sf == null)
                        {
                            sf = new SelfAssessment();
                            sf.QuestionResponse = txt.Text;
                            sf.ModuleID = modid;
                            sf.SAQuestionID = item.SAQuestionID;
                            sf.ParticipantModuleCycleID = cycleID;
                            sf.LastUpdateDate = DateTime.Now;
                            pim.AddToSelfAssessment(sf);
                            pim.SaveChanges();

                        }
                        else
                        {
                            sf.QuestionResponse = txt.Text;
                            sf.LastUpdateDate = DateTime.Now;
                            pim.SaveChanges();

                        }
                    }


                    if (item.SAMeasureQuestionType == 5)
                    {

                        TextBox txt = (TextBox)myControl1;
                        if (item.Required != null && item.Required == true)
                        {
                            if (string.IsNullOrEmpty(txt.Text))
                            {
                                completedAssessment = false;
                            }
                        }
                        if (sf == null)
                        {
                            sf = new SelfAssessment();
                            if (!string.IsNullOrEmpty(txt.Text))
                            {
                                double n;
                                bool isNumeric = double.TryParse(txt.Text, out n);
                                if (isNumeric == false)
                                {
                                    completedAssessment = false;
                                }
                                else
                                {
                                    sf.NumericResponse = n;
                                }
                            }
                            sf.ModuleID = modid;
                            sf.SAQuestionID = item.SAQuestionID;
                            sf.ParticipantModuleCycleID = cycleID;
                            sf.LastUpdateDate = DateTime.Now;
                            pim.AddToSelfAssessment(sf);
                            pim.SaveChanges();

                        }
                        else
                        {
                            if (!string.IsNullOrEmpty(txt.Text))
                            {
                                double n;
                                bool isNumeric = double.TryParse(txt.Text, out n);
                                if (isNumeric == false)
                                {
                                    completedAssessment = false;
                                }
                                else
                                {
                                    sf.NumericResponse = n;
                                }
                            }
                            sf.LastUpdateDate = DateTime.Now;
                            pim.SaveChanges();

                        }
                    }



                    if (item.SAMeasureQuestionType == 3)
                    {
                        RadioButtonList rbl = (RadioButtonList)myControl1;
                        if (item.Required != null && item.Required == true)
                        {
                            if (rbl.SelectedValue == "")
                                completedAssessment = false;
                        }
                        if (sf == null)
                        {
                            sf = new SelfAssessment();
                            if (rbl.SelectedValue != "")
                            {
                                sf.QuestionChoiceID = Convert.ToInt32(rbl.SelectedValue);
                                sf.ModuleID = modid;
                                sf.SAQuestionID = item.SAQuestionID;
                                sf.ParticipantModuleCycleID = cycleID;
                                sf.LastUpdateDate = DateTime.Now;
                                pim.AddToSelfAssessment(sf);
                                pim.SaveChanges();
                            }
                        }
                        else
                        {
                            sf.QuestionChoiceID = Convert.ToInt32(rbl.SelectedValue);
                            sf.LastUpdateDate = DateTime.Now;
                            pim.SaveChanges();

                        }
                    }
                    if (item.SAMeasureQuestionType == 14)
                    {
                        DropDownList rbl = (DropDownList)myControl1;
                        if (item.Required != null && item.Required == true)
                        {
                            if (rbl.SelectedValue == "")
                                completedAssessment = false;
                        }
                        if (sf == null)
                        {
                            sf = new SelfAssessment();
                            if (rbl.SelectedValue != "")
                            {
                                sf.QuestionChoiceID = Convert.ToInt32(rbl.SelectedValue);
                                sf.ModuleID = modid;
                                sf.SAQuestionID = item.SAQuestionID;
                                sf.ParticipantModuleCycleID = cycleID;
                                sf.LastUpdateDate = DateTime.Now;
                                pim.AddToSelfAssessment(sf);
                                pim.SaveChanges();
                            }
                        }
                        else
                        {
                            sf.QuestionChoiceID = Convert.ToInt32(rbl.SelectedValue);
                            sf.LastUpdateDate = DateTime.Now;
                            pim.SaveChanges();

                        }
                    }
                   if (item.SAMeasureQuestionType == 15)
                    {
                        DropDownList rbl = (DropDownList)myControl1;
                        if (item.Required != null && item.Required == true)
                        {
                            if (rbl.SelectedValue == "")
                                completedAssessment = false;
                        }
                        if (sf == null)
                        {
                            sf = new SelfAssessment();
                            if (rbl.SelectedValue != "")
                            {
                                sf.VisualAcuityResponse = Convert.ToInt32(rbl.SelectedValue);
                                sf.ModuleID = modid;
                                sf.SAQuestionID = item.SAQuestionID;
                                sf.ParticipantModuleCycleID = cycleID;
                                sf.LastUpdateDate = DateTime.Now;
                                pim.AddToSelfAssessment(sf);
                                pim.SaveChanges();
                            }
                        }
                        else
                        {
                            if (rbl.SelectedValue != "")
                            {
                                sf.VisualAcuityResponse = Convert.ToInt32(rbl.SelectedValue);
                                sf.LastUpdateDate = DateTime.Now;
                                pim.SaveChanges();
                            }

                        }
                    } 

                }

                ParticipantModuleSelection moduleSelection = (from ms in pim.ParticipantModuleSelection
                                                              where ms.ParticipantModuleCycle.ParticipantModuleCycleID == cycleID & ms.ModuleID == modid
                                                              select ms).First<ParticipantModuleSelection>();
                moduleSelection.SelfAssessmentLastUpdatedDate = DateTime.Now;
                if (completedAssessment)
                {
                    moduleSelection.SelfAssessmentSurveyComplete = true;
                    moduleSelection.SelfAssessmentSurveyCompletedDate = DateTime.Now;
                }
                else
                {
                    moduleSelection.SelfAssessmentSurveyComplete = false;
                    moduleSelection.SelfAssessmentSurveyCompletedDate = null;
                }
                pim.SaveChanges();
                return completedAssessment;
            }
            
        
        }

        protected void ButtonSubmitAndContinue_Click(object sender, CommandEventArgs e)
        {
            bool IsComplete=save();
            if (IsComplete == false)
            {
                LabelError.Text = "Please answer all self-assessment questions.";
                LabelError.ForeColor = System.Drawing.Color.Red;
                LabelError.Visible = true;
                MessageBox.Show("Please answer all self-assessment questions.");
            }
            else
            {
                String CycleNumber = Request.QueryString["CycleNumber"];
                if (CycleNumber == "1")
                    Response.Redirect("SystemSurvey.aspx?CycleNumber=1");
                else
                    Response.Redirect("SystemSurvey.aspx");
            }
        }

        protected void ButtonSubmit_Click(object sender, CommandEventArgs e)
        {
              bool IsComplete=save();
              if (IsComplete == false)
              {
                  LabelError.Text = "Please answer all self-assessment questions.";
                  LabelError.ForeColor = System.Drawing.Color.Red;
                  LabelError.Visible = true;
              }
              else
              {
                  String CycleNumber = Request.QueryString["CycleNumber"];
                  if (CycleNumber == "1")
                      Response.Redirect("SystemSurvey.aspx?CycleNumber=1");
                  else
                      Response.Redirect("SystemSurvey.aspx");
              }
        }
        protected void LinkButtonReturnToStatus_Click(object sender, CommandEventArgs e)
        {
            //save();
            String CycleNumber = Request.QueryString["CycleNumber"];
            if (CycleNumber == "1")
                Response.Redirect("Dashboard.aspx");
            else
                Response.Redirect("Dashboard3.aspx");
        }    
}    
