﻿using System;
using System.Collections.Generic;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

public partial class abo_PECForms : System.Web.UI.Page
{
    protected void Page_Load(object sender, EventArgs e)
    {
        string breadCrumb = Constants.BC_SELECT_PEC_LINK + "" + Constants.BC_PEC_PRINT_FORMS;
        ((PIMMasterPage)Page.Master).SetTitle(breadCrumb);
        ((PIMMasterPage)Page.Master).SetStatus(1);
        ((PIMMasterPage)Page.Master).SetHeader("Print Forms"); 
        
        string PECURL = System.Configuration.ConfigurationManager.AppSettings["PECURL"];
        HyperlinkEnglish.NavigateUrl = PECURL + HyperlinkEnglish.NavigateUrl;
        HyperlinkSpanish.NavigateUrl = PECURL + HyperlinkSpanish.NavigateUrl;
        HyperlinkEnglish2.NavigateUrl = PECURL + HyperlinkEnglish2.NavigateUrl;
        HyperlinkSpanish2.NavigateUrl = PECURL + HyperlinkSpanish2.NavigateUrl;
    }
}
