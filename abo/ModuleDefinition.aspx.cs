﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using NetHealthPIMModel;

public partial class abo_ModuleDefinition : BasePage
{
    protected void Page_Load(object sender, EventArgs e)
    {
        if (!IsPostBack)
        {
            InitializePage();
        }

    }

    protected void InitializePage()
    {
        //((PIMMasterPage)Page.Master).SetTitle("Activity Details");

        using (NetHealthPIMEntities pim = new NetHealthPIMEntities())
        {
            int cycleID = ctx.ActiveModuleCycleID;
            ParticipantModuleCycle cycle = pim.ParticipantModuleCycle.First(c => c.ParticipantModuleCycleID == cycleID);

            int ModuleID = Convert.ToInt32(Request.QueryString["mid"]);
            var module = from m in pim.Module
                         where m.ModuleID == ModuleID
                          select m;
            var moduleDetail = module.First();
            //LiteralModuleDescription.Text = moduleDetail.ModuleDescription;
            LiteralPatientDefinition.Text = moduleDetail.PatientDefinition;
            LiteralModuleName1.Text = moduleDetail.ShortModuleDescription;
            LiteralModuleName2.Text = moduleDetail.ShortModuleDescription;
            LiteralModuleName3.Text = moduleDetail.ShortModuleDescription;
            LiteralModuleName4.Text = moduleDetail.ShortModuleDescription;
            LiteralMinimumImprovementPeriod.Text = moduleDetail.ImprovementMonths.ToString();

            System.Linq.IQueryable reportOutcomeMeasures;
            //reportOutcomeMeasures = from me in pim.Measure
            //                        join pms in pim.ParticipantModuleSelection on me.ModuleID equals pms.ModuleID
            //                        where pms.ParticipantModuleCycleID == cycle.ParticipantModuleCycleID
            //                        & me.MeasureType.MeasureTypeID == 4
            //                        & me.ModuleID == moduleDetail.ModuleID
            //                        select new ModuleMeasure
            //                        {
            //                            ModuleID = pms.ModuleID,
            //                            MeasureLongDescription = me.MeasureLongDescription
            //                        };
            reportOutcomeMeasures = from me in pim.Measure
                                   where me.MeasureType.MeasureTypeID == 4
                                   where me.Module.ModuleID == ModuleID 
                                    select new ModuleMeasure
                                    {
                                        ModuleID = (int)me.Module.ModuleID,
                                        MeasureLongDescription = me.MeasureLongDescription
                                    }; 
            ListViewOutcomeMeasures.DataSource = reportOutcomeMeasures;
            ListViewOutcomeMeasures.DataBind();

            LinkButtonChartAbstractionDetail.OnClientClick = "window.open('../documents/" + moduleDetail.ShortModuleDescription + "ChartAbstraction.PDF');return false;";
            LinkButtonPeerData.OnClientClick = "window.open('../documents/" + moduleDetail.ShortModuleDescription + "PeerData.PDF');return false;";

            var resources = from m in pim.Measure 
                            join mr in pim.MeasureResource on m.MeasureID equals mr.MeasureID
                            join r in pim.Resource on mr.ResourceID equals r.ResourceID
                            where m.Module.ModuleID == ModuleID
                            select  new 
                            {
                                ResourceName = r.ResourceName,
                                ResourceDescription = r.ResourceDescription,
                                URL = r.URL
                            };
            if (resources.Count() == 0)
            {
                ResNameID.Visible = false;
            }
            ListViewResources.DataSource = resources.Distinct();
            ListViewResources.DataBind();


        }
    }

}
