﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

public partial class copd_Logoff : BasePage
{
    protected void Page_Load(object sender, EventArgs e)
    {
        ((PIMMasterPage)Page.Master).SetTitle("Log Out");
        Session[Constants.SESSION_PAGE_ROLETYPE_LEVEL] = Constants.ROLETYPEID_GUEST;
        Response.Cookies["PBLOGIN"]["UNAME"] = null;
        Response.Cookies["PBLOGIN"].Value = null;
        Response.Cookies["PBLOGIN"].Expires = DateTime.Now.AddDays(-1);

        
    }
}
