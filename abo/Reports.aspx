﻿<%@ Page Title="" Language="C#" MasterPageFile="~/PIMMasterPage.master" AutoEventWireup="true" CodeFile="Reports.aspx.cs" Inherits="copd_Reports" %>

<asp:Content ID="Content1" ContentPlaceHolderID="head" Runat="Server">
</asp:Content>
<asp:Content ID="Content3" ContentPlaceHolderID="ContentPlaceHolder1" Runat="Server">

<br />

    <asp:GridView ID="GridViewCycleReports" runat="server" CssClass="main_table center_area"
        HeaderStyle-CssClass="table_header"
        AutoGenerateColumns="False" 
        onrowdatabound="GridViewCycleReports_RowDataBound">
        <Columns>
            <asp:TemplateField>
                <ItemTemplate>
                    <asp:Image ID="ImageCompleteStatus" runat="server" 
                        ImageUrl='<%# string.Format("~/images/copd/complete_{0}.png", Eval("ModuleComplete")) %>' Width="22px" Height="22px" ImageAlign="AbsBottom" />
                    
                </ItemTemplate>
            </asp:TemplateField>
            <asp:BoundField DataField="ModuleComplete" HeaderText="Activity Complete" ItemStyle-HorizontalAlign="Center" />
            <asp:TemplateField HeaderText="Reports">
                <ItemTemplate>
                    <asp:HyperLink ID="LinkPart1Report" runat="server" >View Part 1 Report</asp:HyperLink><br />
                    <asp:HyperLink ID="LinkImprovementPlan" runat="server" >View Approved Improvement Plan</asp:HyperLink><br />
                    <asp:HyperLink ID="LinkPart3Report" runat="server" >View Part 3 Report</asp:HyperLink><br />
                    <asp:HyperLink ID="LinkImpactStatement" runat="server" >View Approved Impact Statement</asp:HyperLink>
                </ItemTemplate>
            </asp:TemplateField>
            <asp:BoundField DataField="CycleStartDate" HeaderText="Activity Start Date" DataFormatString="{0:d}" ItemStyle-HorizontalAlign="Center" />
            <asp:BoundField DataField="CycleEndDate" HeaderText="Activity End Date" DataFormatString="{0:d}"  ItemStyle-HorizontalAlign="Center" />
        </Columns>
        <HeaderStyle CssClass="table_header" />
    </asp:GridView>

<br />

</asp:Content>

