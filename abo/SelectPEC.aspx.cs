﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using NetHealthPIMModel;
using System.Transactions;
using System.Web.UI.HtmlControls;

public partial class abo_SelectPEC : BasePage
{
    protected void Page_Load(object sender, EventArgs e)
    {
        if (!IsPostBack)
        {
            InitializePage();
        }
    }
    protected void InitializePage()
    {
        string breadCrumb = Constants.BC_CREATE_PLAN_LINK + "" + Constants.BC_PQRS_INCENTIVE_PROGRAM_LINK + "" +
            Constants.BC_IMPORT_DATA_LINK + "" + Constants.BC_MODULE_SELECTION_LINK + "" + Constants.BC_MODULE_DETAILS_LINK + "Patient Experience of Care";
        ((PIMMasterPage)Page.Master).SetTitle(breadCrumb);
        ((PIMMasterPage)Page.Master).SetStatus(1);
        ((PIMMasterPage)Page.Master).SetHeader("Select Patient Experience of Care");
        using (NetHealthPIMEntities pim = new NetHealthPIMEntities())
        {
            Participant participant = pim.Participant.First(p => p.ParticipantID == ctx.ParticipantID);
            int cycleID = ctx.ActiveModuleCycleID;
            ParticipantModuleCycle cycle = pim.ParticipantModuleCycle.First(c => c.ParticipantModuleCycleID == cycleID);
            ParticipantModuleCycle cycle2 = null;

            if (cycle.CycleNumber == 2)
            {
                cycle2 = cycle;

                cycle.PrevParticipantModuleCycleReference.Load();
                ParticipantModuleCycle prev = cycle.PrevParticipantModuleCycle;
                cycle = prev;

            }

            var PECSurveyExists = (from ps in pim.PECSurveysPerCandidate_V
                                  where ps.CandidateID == participant.CandidateID
                                  select ps).Count();
            if (PECSurveyExists > 0)
            {
                var pecSurveysPerCandidate = (from ps in pim.PECSurveysPerCandidate_V
                                              where ps.CandidateID == participant.CandidateID
                                              orderby ps.ParticipantCycleID descending
                                              select ps).First();
                if (pecSurveysPerCandidate.StartedDate != null)
                {
                    PECStartedOn.Text = Convert.ToDateTime(pecSurveysPerCandidate.StartedDate.ToString()).ToShortDateString() + PECStartedOn.Text;
                }
                HiddenFieldPECID.Value = pecSurveysPerCandidate.ParticipantEntryID.ToString();

                PECAvailable.Visible = true;
                PECStartedOn.Visible = true;
            }
            else
            {
                divPPECAvailable.Visible = false;
            }

            if (cycle.PECCurrentID != null)
            {
                if (cycle.PECCurrentID == 0)
                {
                    HiddenFieldPECSelection.Value = "3"; 
                    LinkButtonDoNotIncludeSurvey.Attributes.Add("class", "button active");
                    LinkButtonExistingSurvey.Attributes.Add("class", "button");
                    LinkButtonNewSurvey.Attributes.Add("class", "button");
                }
                else
                {
                    HiddenFieldPECSelection.Value = "2"; 
                    LinkButtonExistingSurvey.Attributes.Add("class", "button active");
                    LinkButtonDoNotIncludeSurvey.Attributes.Add("class", "button");
                    LinkButtonNewSurvey.Attributes.Add("class", "button");
                }
            }
            if (cycle.PECNextID != null)
            {
                HiddenFieldPECSelection.Value = "1"; 
                LinkButtonNewSurvey.Attributes.Add("class", "button active");
                LinkButtonDoNotIncludeSurvey.Attributes.Add("class", "button");
                LinkButtonExistingSurvey.Attributes.Add("class", "button");
            }
            if ((cycle.PECCurrentID == null) && (cycle.PECNextID == null))
            {
                LinkButtonNewSurvey.Attributes.Add("class", "button");
                LinkButtonDoNotIncludeSurvey.Attributes.Add("class", "button");
                LinkButtonExistingSurvey.Attributes.Add("class", "button");
            }
            if (cycle.CycleNumber == 2)
            {
                LinkButtonExistingSurvey.Enabled = false;
                LinkButtonNewSurvey.Enabled = false;
                LinkButtonDoNotIncludeSurvey.Enabled = false;
            }
            if ((cycle.CycleNumber == 2) || (cycle.ImprovementPlanComplete == true))
            {
                ButtonBack.Visible = true;
                LabelPECStatus.Visible = true;
                switch (HiddenFieldPECSelection.Value)  
                {
                    case "1":
                        LabelPECStatus.Text = "New Survey";
                        break;
                    case "2":
                        LabelPECStatus.Text = "Existing Survey";
                        break;
                    case "3":
                        LabelPECStatus.Text = "No Survey";
                        break;
                }
                divButtons.Visible = false;
            }
        }
    }
    protected void LinkButtonSave_Click(object sender, CommandEventArgs e)
    {
        using (NetHealthPIMEntities pim = new NetHealthPIMEntities())
        {
            ParticipantModuleCycle participantModuleCycle = pim.ParticipantModuleCycle.First(p => p.ParticipantModuleCycleID == ctx.ActiveModuleCycleID);
            if ((participantModuleCycle.CycleNumber == 2) || (participantModuleCycle.ReportApproved==true))
            {
                Response.Redirect("ModuleSelectionReview.aspx");
            }

            if (HiddenFieldPECSelection.Value == "2") 
            {
                participantModuleCycle.PECCurrentID = Convert.ToInt32(HiddenFieldPECID.Value);
                CycleManager.AddPECMeasuresToParticipant(ctx.ActiveModuleCycleID);
            }
            if (HiddenFieldPECSelection.Value == "3") 
                participantModuleCycle.PECCurrentID = 0;
            if ((HiddenFieldPECSelection.Value == "1")) 
            {
                participantModuleCycle.PECNextID = 0;
                participantModuleCycle.PECCurrentID = null;
            }
            if ((HiddenFieldPECSelection.Value == "2") || (HiddenFieldPECSelection.Value == "3"))
            {
                participantModuleCycle.SelectPECComplete = true;
                participantModuleCycle.SelectPECCompletedDate = DateTime.Today;
                if (HiddenFieldPECSelection.Value != "1")
                    participantModuleCycle.PECNextID = null;
            }
            else
            {
           
                participantModuleCycle.SelectPECComplete = null;
                participantModuleCycle.SelectPECCompletedDate = null;
            }
            pim.SaveChanges();
        }
        if (HiddenFieldPECSelection.Value == "1")
            Response.Redirect("PECInstructions.aspx");
        else
            Response.Redirect("ModuleSelectionReview.aspx");
    }
    protected void ButtonPriorModules_Click(object sender, EventArgs e)
    {
        Response.Redirect("~/" + module.ModuleCode.ToLower() + "/ModuleDetails.aspx");
    }
}
