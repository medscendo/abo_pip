﻿<%@ Page Title="" Language="C#" MasterPageFile="~/PIMMasterPage.master" AutoEventWireup="true" CodeFile="ParticipantProfile.aspx.cs" Inherits="abo_ParticipantProfile" %>

<asp:Content ID="Content1" ContentPlaceHolderID="head" Runat="Server">
    
    <link type="text/css" href="../common/css/atooltip.css" rel="stylesheet"  media="screen" />
    
    <style>
        .third {
            -moz-box-sizing: border-box;
            -webkit-box-sizing: border-box;
            box-sizing: border-box;
            float:left;
            min-height:100px;
            width: 33.33%;
            margin: 15px 0;
            border-right: 1px solid #ccc;
            padding: 0 15px;
        }

        .third.noright {
            border-right: none;
        }
        .header-box {
            margin-bottom: 30px;
        }
        .header-box .body {
            background-color: #E5F2F8;
            padding: 0;
        }

        .table .odd td {
            background-color: #E5F2F8 !important;
        }

        .ph1 {
            width: 30px;
        }

        .ph2 {
            width: 40px;
        }

        label {
            padding-left: 3px;
        }

        .note {
            font-size: 0.8em;
        }

        .table th {
            font-weight:normal;
            text-align:center;
        }
    </style>
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder1" Runat="Server">
    <asp:HiddenField ID="HiddenFieldRecordID" runat="server" />
    
<p>Welcome to the American Board of Ophthalmology’s Improvement in Medical Practice Platform. Participating in improvement in medical practice activities allows you to measure your practice performance and develop an actionable plan for improvement based on best-practices and de-identified peer benchmarks.</p>

<p>Improvement in Medical Practice Activities are self-selected online activities that require review of 30 patient records associated with a specific diagnosis or diagnoses. Improvement in Medical Practice Activities assist with the evaluation of practice standards; identification of areas for improvement through self-assessment; and a reduction of gaps in quality of care through improved patient outcomes.</p>
<table border="1">
<tr>
<td>
<span style="font-size:large;">Step 1: Select Improvement in Medical Practice Activity(s)</span>
<ul>
<li>Choose 1-3 activities.</li>
<li>Select activities reflective of your practice focus area(s).</li>
<li>Consider the total time requirements for your activity selections. Some activities can be completed in as little as 30 days while others require up to 395 days.</li>
</ul>
</td>
<td style="vertical-align:top;"><span style="font-size:large;">Step 2: Register and Review 30 Patient Charts</span>
<ul>
<li>Gather the charts that meet the specific activity(s) definition and complete the activity chart abstraction for each.  You will enter information about the processes of care and outcomes for each patient.</li>
</ul>
</td></tr>

<tr>
<td style="vertical-align:top;"><span style="font-size:large;">Step 3: Reflect on Your Practice Report and Develop a Plan for Improving on Measures of Your Choice</span>
<ul>
<li>Once you complete the chart abstraction for your 30 charts, you will receive a practice report that shows the aggregate data of the information you entered along with peer comparison data.</li>
<li>You can now choose <strong>three areas</strong> from this report for improvement and provide a brief explanation of your plans.</li>
</ul>
</td><td><span style="font-size:large;">Step 4: Implement Your Improvement Plan within the Time Allowed for the Activity(s) You Selected</span>
<ul>
<li>Minimum improvement timeframes range from 30 days to 395 days depending on the activity.</li>
<li>The minimum improvement timeframe is the time period during which you are expected to implement your improvement plan before re-measuring patient data and assessing the impact of your improvement efforts.</li>
</ul>
</td></tr>

<tr>
<td colspan="2"><span style="font-size:large;">Step 5: Register and Review 10 Additional Charts and Reflect on the Impact of Your Improvement Plan</span>
<ul>
<li>Once your improvement period has concluded, you will be asked to review 10 additional patient charts and reflect on the impact your improvement plan has had on your practice.</li>
</ul>
</td></tr>
</table>
<br />
Before you begin, you may want to review these improvement in medical practice resources:
<ul>
<li><a href="http://www.qihub.scot.nhs.uk/qi-basics/quality-improvement-glossary-of-terms.aspx" target="_blank">NHSScotland</a> has developed a glossary of QI terms that might help you gain a basic understanding of concepts or terms that are unfamiliar.</li>
<li>The <a href="http://www.ihi.org/resources/Pages/HowtoImprove/default.aspx" target="_blank">Institute for Healthcare Improvement</a> has a very robust website that has many resources to help guide you through improvement processes based on the Model for Improvement methodology. You will find valuable information on topics from setting improvement goals to identifying measures, from developing QI plans and testing changes to implementing and spreading changes.</li>
<li>The ABFM’s winter 2013 issue of The Phoenix contains an article about how to develop and implement a self-directed QI effort for Part IV credit that may be helpful for understanding how to develop relevant, meaningful QI efforts that also satisfy MC-FP requirements. A stand-alone version of the article can be found <a href="http://assets.mocactivitymanager.org/ABFM/SDQIArticle.pdf" target="_blank">here.</a></li>
<li>The American Board of Pediatrics has developed a <a href="https://abp.mocactivitymanager.org/qiguideoverview/" target="_blank">QI Guide</a> in MOCAM that has helpful information about common improvement principles.</li>
</ul>


    <div class="header-box">
        <div class="head">Contact Information</div>
        <div class="body clearfix">
            <div class="third">
                <div class="row">
                    <label>First Name:
                    <br /><asp:TextBox ID="TextBoxFirstName" runat="server" MaxLength="50"></asp:TextBox></label>
                </div>
                <div class="row">
                    <label>Last Name:
                    <br /><asp:TextBox ID="TextBoxLastName" runat="server" MaxLength="50"></asp:TextBox></label>
                </div>
            </div>
            <div class="third">
                <div class="row">
                    <label>Email Address:
                    <br /><asp:TextBox ID="TextBoxEmailAddress" runat="server" MaxLength="50"></asp:TextBox></label>
                </div>
                <div class="row">
                    Phone Number:
                    <br /><asp:TextBox ID="TextBoxPhone1" runat="server" MaxLength="3" class="ph1"></asp:TextBox>
                    <asp:TextBox ID="TextBoxPhone2" runat="server" MaxLength="3" class="ph1"></asp:TextBox>
                    <asp:TextBox ID="TextBoxPhone3" runat="server" MaxLength="4" class="ph2"></asp:TextBox>
                </div>
            </div>
            <div class="third noright">
                <div class="row">
                    Preferred Contact Method:
			        <br /><asp:CheckBox ID="CheckBoxEmail" runat="server" class="check" Text="Email" />
                    <br /><asp:CheckBox ID="CheckBoxPhone" runat="server" class="check" Text="Phone "/>  
                </div>
            </div>
        </div>
    </div>

    <!-- contact info ends -->
    <!-- ABO requested the following fields to no longer be collected. -->

    <table style="display:none" class="table">
        <tr>
            <th colspan="2">Practice Profile Information</th>
        </tr>
        <tr>
            <td><strong>1. Please provide the zip code at your primary practice site:</strong></td>
            <td>
                <asp:TextBox ID="TextBoxPostalCode" runat="server" MaxLength="20"  class="other school"></asp:TextBox>
            </td>
        </tr>
        <tr>
            <td><strong>2. Do you consider your practice primarily serving a(n):</strong></td>
            <td>
                <asp:RadioButtonList ID="RadioButtonListCommunityType" runat="server" DataValueField="PracticeCommunityTypeID" DataTextField="PracticeCommunityTypeName" RepeatLayout="Flow" />
            </td>
        </tr>
        <tr>
            <td><strong>3. What is your primary practice setting?</strong></td>
            <td>
                <asp:RadioButtonList ID="RadioButtonListPracticeType" runat="server" DataValueField="PracticeTypeID" DataTextField="PracticeTypeName" RepeatLayout="Flow" />
            </td>
        </tr>
        <tr>
            <td><strong>4. How does your practice currently maintain patient charts?</strong></td>
            <td>
                <asp:RadioButtonList ID="RadioButtonListPracticePatientCharts" runat="server" DataValueField="PracticePatientChartsID" DataTextField="PracticePatientChartsName" RepeatLayout="Flow" />
            </td>
        </tr>
        <tr>
            <td><strong>5. Does your practice participate in a Pay-for-Performance program(s)? <img id="PP1" src="../common/images/tip.gif" alt="Tip" /></a></strong></td>
            <td>
                <asp:RadioButtonList ID="RadioButtonPayForPerformance" runat="server" RepeatDirection="Horizontal" RepeatLayout="Flow">
                    <asp:ListItem Value="1" Text="Yes&nbsp;" />
                    <asp:ListItem Value="0" Text="No&nbsp;" />
                </asp:RadioButtonList>
            </td>
        </tr>
        <tr>
            <td><strong>6. Approximately how many patients do you see each week?</strong></td>
            <td>
                <asp:RadioButtonList ID="RadioButtonListPracticePatientPerWeek" runat="server" DataValueField="PracticePatientPerWeekID" DataTextField="PracticePatientPerWeekName" RepeatLayout="Flow" />
                <asp:TextBox ID="TextBoxPatientPerWeekOther" runat="server" MaxLength="100" />
            </td>
        </tr>
    </table>

    <div class="button-box">
        <asp:Button runat="server" CssClass="button" id="ButtonSubmit" OnClick="ButtonSubmit_Click" Text="Save and Proceed"  />
    </div>
    <!-- personal info ends -->

</asp:Content>

<asp:Content runat="server" ID="Content4" ContentPlaceHolderID="javascript">
    <script type="text/javascript" language="javascript" src="http://ajax.googleapis.com/ajax/libs/jquery/1.7.1/jquery.min.js"></script>
	<script type="text/javascript" src="../common/js/jquery.atooltip.js"></script>
    <script type="text/javascript">
        $(function () {
            $('#PP1').aToolTip({
                clickIt: true,
                tipContent: 'Pay-for-Performance is an emerging and evolving concept in compensation in the healthcare field. <br><br> Pay-for-performance (P4P) programs are designed to offer both financial and non-financial incentives to physicians and other health care providers to meet defined quality, efficiency, or other target measures. These performance measures can cover various aspects of healthcare delivery: clinical quality and safety, efficiency, patient experience and health information technology adoption. Sponsors of P4P programs typically include government agencies, health insurance plans (health plans), employers (purchasers), healthcare providers, and a variety of coalitions.'
            });
        });

        $(".table tr:odd").addClass("odd");

        function validateForm() {
            var msg = "";
            var TextBoxFirstName = document.getElementById("<%= TextBoxFirstName.ClientID %>").value;
               var TextBoxLastName = document.getElementById("<%= TextBoxLastName.ClientID %>").value;
               var TextBoxEmailAddress = document.getElementById("<%= TextBoxEmailAddress.ClientID %>").value;
               var TextBoxPhone1 = document.getElementById("<%= TextBoxPhone1.ClientID %>").value;
               var TextBoxPhone2 = document.getElementById("<%= TextBoxPhone2.ClientID %>").value;
               var TextBoxPhone3 = document.getElementById("<%= TextBoxPhone3.ClientID %>").value;
               var CheckBoxEmail = document.getElementById("<%= CheckBoxEmail.ClientID %>");
               var CheckBoxPhone = document.getElementById("<%= CheckBoxPhone.ClientID %>");
               var TextBoxPostalCode = document.getElementById("<%= TextBoxPostalCode.ClientID %>").value;
               var TextBoxPatientPerWeekOther = document.getElementById("<%= TextBoxPatientPerWeekOther.ClientID %>").value;
               //alert($('#<%=RadioButtonListCommunityType.ClientID %> input[type=radio]:checked').val());
               if (TextBoxFirstName.trim() == "")
                   msg = msg + "Please enter First Name\n";
               if (TextBoxLastName.trim() == "")
                   msg = msg + "Please enter Last Name\n";
               if (TextBoxEmailAddress.trim() == "")
                   msg = msg + "Please enter Email Address\n";
               if ((TextBoxPhone1.trim() == "") || (TextBoxPhone2.trim() == "") || (TextBoxPhone3.trim() == ""))
                   msg = msg + "Please enter a complete Phone Number\n";
               if ((!CheckBoxEmail.checked) && (!CheckBoxPhone.checked))
                   msg = msg + "Please select Preferred Contact Method:\n";
               if (TextBoxPostalCode.trim() == "")
                   msg = msg + "Please answer 1. Please provide the zip code at your primary practice site:\n";
               if ($('#<%=RadioButtonListCommunityType.ClientID %> input[type=radio]:checked').val() == undefined)
                   msg = msg + "Please answer 2. Do you consider your practice primarily serving a(n):\n";
               if ($('#<%=RadioButtonListPracticeType.ClientID %> input[type=radio]:checked').val() == undefined)
                   msg = msg + "Please answer 3. What is your primary practice setting?\n";
               if ($('#<%=RadioButtonListPracticePatientCharts.ClientID %> input[type=radio]:checked').val() == undefined)
                   msg = msg + "Please answer 4. How does your practice currently maintain patient charts?\n";
               if ($('#<%=RadioButtonPayForPerformance.ClientID %> input[type=radio]:checked').val() == undefined)
                   msg = msg + "Please answer 5. Does your practice participate in a Pay-for-Performance program(s)?\n";
               if ($('#<%=RadioButtonListPracticePatientPerWeek.ClientID %> input[type=radio]:checked').val() == undefined)
                   msg = msg + "Please answer 6. Approximately how many patients do you see each week?\n";
               if (msg != "") {
                   alert(msg);
                   return false;
               }
               return true;
        }

        $('.table tr:odd').addClass("odd");
    </script>
</asp:Content>