﻿<%@ Page Title="" Language="C#" MasterPageFile="~/PIMMasterPage.master" AutoEventWireup="true" CodeFile="PQRSSELECTION.aspx.cs" Inherits="abo_PQRSSELECTION" %>

<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="Server">

    <style>

        .no-close .ui-dialog-titlebar-close {
            display: none;
        }

        .ParticipatePQRS {
            font-weight:bold;
        }

        .ui-widget-header {
            background:none !important;
            border: none !important;
        }

        .categories {
            float:left;
            margin-bottom:30px;
        }

        .header-box {
            float:left;
            width:440px;
        }

        .header-box .body {
            min-height: 215px;
        }

        .action-box {
            width: 850px;
            text-align: center;
            margin-top:10px;
        }

        .choices {
            margin:20px auto 5px;
        }

        .choices label {
            padding: 0 15px 0 5px;
        }

    </style>
    <link href="../common/css/jquery-ui.min.css" rel="stylesheet" />
</asp:Content>

<asp:Content ID="Content4" ContentPlaceHolderID="javascript" runat="server">

    <script type="text/javascript" language="javascript">
        function isRadiobuttonSelected(radioButtons) {
            var inputs = radioButtons.getElementsByTagName("input");
            var selected="";
            for (var i = 0; i < inputs.length; i++) {
                if (inputs[i].checked) {
                    selected = inputs[i].value;
                    break;
                }
            }
            return selected;
        }

        function validateForm() {


            var RadioButtonList1 = document.getElementById("<%= RadioButtonList1.ClientID %>");
            var HiddenFieldPQRSSelected = document.getElementById("<%= HiddenFieldPQRSSelected.ClientID %>");
            var HiddenFieldPQRSCurrentSelection = document.getElementById("<%= HiddenFieldPQRSCurrentSelection.ClientID %>");
            var selectedValue = isRadiobuttonSelected(RadioButtonList1);
            if (HiddenFieldPQRSSelected.value == "1") {
                if (HiddenFieldPQRSCurrentSelection.value == "0" && selectedValue == "True") {
                    if (!confirm("By changing your PQRS Reporting Program answer from 'No' to 'Yes,' EVERY CHART that you have registered and entered for this current phase WILL BE ERASED! If you still wish to proceed, click OK.  If you do not wish to erase your charts, click CANCEL.")) {
                        return false;
                    }
                }
            }
            if (selectedValue == "False") {
                if (confirm("Are you sure you do not want to participate in PQRS registry reporting?")) {
                    return true;
                }
                else {
                    return false;
                }
            } else {
            if (selectedValue == "") {
                alert("Please make a selection.");
                return false;
            }
            else
                return true;
            }
        }
    </script>

</asp:Content>

<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder1" runat="Server">
    <asp:HiddenField runat="server" ID="HiddenFieldPQRSSelected" />
    <asp:HiddenField runat="server" ID="HiddenFieldPQRSCurrentSelection" />
    <div class="heading_box">
        <h2>
            PQRS Reporting Program - Updated for 2016</h2>
    </div>
        
<p>
The ABO offers diplomates the ability to participate in the Centers for Medicare and Medicaid Services (CMS)'s PQRS reporting through the Part IV Improvement in Medical Practice Platform. Several ABO PIP activities align with the CMS PQRS program and the data entered may be used for participation in Healthmonix's PQRSPRO registry. You can choose this option whether or not you are submitting PQRS data through claims; CMS will take the 'best' (most accurate) results for your submission.  There are two options for participation in PQRS reporting using PQRS-enabled Improvement in Medical Practice Activities:
 </p> <p>
Measures Group Reporting: Cataract Surgery
Select this activity and 20 patient chart entries (11 of which must be Medicare patients) out of the 30 charts entered will qualify for PQRS reporting. Details on Cataract Measures Group reporting may be found <a href='https://www.pqrspro.com/cmsgroups/2015/cataracts'>here.</a>
</p>  <p>
Individual Measures Reporting: Primary Open Angle Glaucoma, Advanced Macular Degeneration, Diabetic Retinopathy
Each of these activities includes PQRS measures, which require you to report data for 50% of Medicare patient visits on at least three measures.  These include measures from the POAG (2 available), AMD (2 measures available) and Diabetic Retinopathy (3 measures available) PIP activities. If choosing the POAG or AMD activity, then be sure to also choose one of the others listed in order to qualify (at least 3 measures must be reported).
</p> 

        
    <div class="categories clearfix">
        <div class="header-box" style="margin-right:30px";>
            <div class="head">
                Cataract Surgery Measures Group Reporting
            </div>
            <div class="body">
                <p>Select this activity and qualify with just 20 patient chart entries (11 of which must be Medicare patients). <em>This chart information will come directly from the 30 charts entered into the ABO PIP</em>.  Details on Cataract Measures Group reporting may be found <a href="https://www.pqrspro.com/cmsgroups/2015/cataracts" target="_blank">here</a>.</p>
            </div>
        </div>

        <div class="header-box">
            <div class="head">
                Individual Measures Reporting
            </div>
            <div class="body">
                <p>
                    Report data for 50% of Medicare patient visits for <strong><em>9 individual measures</em></strong> in the <a href="https://www.pqrspro.com/2015/specialty_ophthalmology" target="_blank">Ophthalmology Measures Set</a>.  
                    These include measures from the POAG (2 available), AMD (2 measures available) and Diabetic Retinopathy (3 measures available) PIP activities.  
                    If choosing the POAG or AMD activity, then be sure to also choose one of the others listed in order to qualify (<em>at least 3 measures must be reported</em>).
                </p>
            </div>
        </div>
    </div>

   <p>
Report data for 50% of eligible Medicare Part B patient visits for 9 individual measures (including one cross-cutting measure), or one MAV Cluster and one cross-cutting measure. Explore the alternatives for successful 2015 PQRS reporting in the PQRSPRO Ophthalmology Measures Options.
 </p> <p>
By selecting these PQRS-enabled Improvement in Medical Practice Activities, you can participate in PQRS measure reporting through the platform vendor, Healthmonix's PQRSPRO Registry. Data will be seamlessly passed over to the <a href="http://www.pqrspro.com/"> PQRSPRO</a> system, leaving minimal data entry to complete the process.  The additional cost to use the PQRS-enabled Improvement in Medical Practice Activities to submit for PQRS through the PQRSPRO system is $199 (a $90 discount for ABO diplomates). Contact the experts at Healthmonix for more information 610.590.2229 or 1.888.720.4100
  </p>

    <%--<br /><span style="width:100%; text-align:center;"><a href="PQRSMoreInfo.aspx" class="button">Click for More Information about PQRS</a></span>--%>   
        
    <div class="action-box">
        <asp:Label ID="ParticipatePQRS" CssClass="ParticipatePQRS" runat="server" Text="Would you like to participate in PQRS Reporting Program?"></asp:Label>
        <asp:RadioButtonList ID="RadioButtonList1" RepeatDirection="Horizontal" runat="server" CssClass="choices">
            <asp:ListItem Value="True" Text="Yes"  />
            <asp:ListItem Value="False" Text="No"></asp:ListItem>
        </asp:RadioButtonList>
        <asp:RequiredFieldValidator ID="RequiredFieldValidator1" runat="server" ControlToValidate="RadioButtonList1" ValidationGroup="some" ErrorMessage="Field is Required<br />"></asp:RequiredFieldValidator>
            
        <%--<asp:Button ID="Button1" runat="server" Text="Submit" ValidationGroup="some" OnClick="Button1_Click" OnClientClick="javascript:return validateForm();" />--%>
        <asp:LinkButton ID="ButtonSubmit" runat="server" Text="Submit" OnClick="Button1_Click" OnClientClick="javascript:return validateForm();" CssClass="button" />
        <asp:LinkButton ID="ButtonBack" runat="server" Text="Back" PostBackUrl="ModuleSelectionReview.aspx" CssClass="button" Visible="false"></asp:LinkButton>
        <%--<div class="bginput"><asp:LinkButton ID="LinkButtonPriorModules" runat="server" Text=" Previous " OnClick="ButtonPriorModules_Click"/></div>--%>            
    </div>
    <p>If you selected yes, you will be presented with the PQRS-enabled Improvement in Medical Practice Activities in the next few pages. </p>
    <p>
       The Physician Quality Reporting System (PQRS) <i>is a continuation of the Center for Medicaid and Medicare Services (CMS) PQRI program, initiated in 2007. Offering an incentive reimbursement in 2007 through 2014, it has transitioned to a negative payment adjustment program. The program provides avoidance of negative payment adjustments from 2015 on, via the submission of de-identified patient data on specified quality measures. 
In 2017 a -2% "payment adjustment" will be applied to non-participants in 2015. </i>
    </p>
    

    <div id="dialog-confirm" title="PQRS Verification" style="display:none;">
        <p>
            You have chosen to participate in PQRS Registry Reporting, in addition to completing Part IV: Practice Performance Assessment MOC.  This means that you will either submit data on at least 20 charts from the Cataract Measures Group (This chart information will come directly from the 30 charts entered into the ABO PIP) or choose <em>individual measures</em> from POAG, AMD and/or Diabetic Retinopathy Measures Set. 
            <br />
            <br />Once you have completed chart entry for your chosen PIP, you will seamlessly pass through to the PQRS<strong>PRO</strong> registry to continue and complete the process by January 14, 2016.  
            <br />
            <br />Any questions that you may have about PQRS or registry reporting can be submitted to <a href="mailto:PQRSSupport@healthmonix.com">PQRSSupport@healthmonix.com</a>.
            <br />
            <br /><strong>Do you still want to participate in PQRS registry reporting using your Improvement in Medical Practice Activity data?</strong>
        </p>
    </div>

</asp:Content>
