﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using NetHealthPIMModel;

public partial class abo_ModuleMenu : BasePage
{
    protected void Page_Load(object sender, EventArgs e)
    {
        if (!IsPostBack)
        {
            InitializePage();
        }
    }
    protected void InitializePage()
    {
        Session[Constants.SESSION_USERCONTEXT] = ctx;
        using (NetHealthPIMEntities pim = new NetHealthPIMEntities())
        {
            int cycleID = ctx.ActiveModuleCycleID;
            ParticipantModuleCycle cycle = pim.ParticipantModuleCycle.First(c => c.ParticipantModuleCycleID == cycleID);

            var modulesSelected = (from m in pim.Module
                                   join pms in pim.ParticipantModuleSelection on m.ModuleID equals pms.ModuleID
                                   where pms.ParticipantModuleCycleID == cycle.ParticipantModuleCycleID
                                   select pms).Count();
            if (modulesSelected > 0)
            {

                var chartModuleSelection = from m in pim.Module
                                           join pms in pim.ParticipantModuleSelection on m.ModuleID equals pms.ModuleID
                                           where pms.ParticipantModuleCycleID == cycle.ParticipantModuleCycleID
                                           select new
                                           {
                                               ModuleName = m.ModuleDescription,
                                               ModuleID = m.ModuleID
                                           };
                ListViewModuleLinks.DataSource = chartModuleSelection;
                ListViewModuleLinks.DataBind();


            }
        }
    }
}
