﻿<%@ Page Title="" Language="C#" MasterPageFile="~/PIMMasterPage.master" AutoEventWireup="true" CodeFile="MyPI.aspx.cs" Inherits="copd_MyPI" %>

<asp:Content ID="Content1" ContentPlaceHolderID="head" Runat="Server">
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder1" Runat="Server">

<asp:Panel ID="PanelWindowOnLoad" runat="server" Visible="false">
    <script type="text/javascript">
        document.body.onload = function() {
            var button = $get("<%= ButtonP4StartNewModule.ClientID %>");
            button.click();
        }   
    </script>
</asp:Panel>
<br />
    <asp:Table ID="TableIndex" CssClass="main_table center_area" runat="server">
        <asp:TableRow CssClass="table_header">
            <asp:TableCell>&nbsp;</asp:TableCell>
            <asp:TableCell>Part 1 - Data Collection First Abstraction</asp:TableCell>
            <asp:TableCell>My Data</asp:TableCell>
            <asp:TableCell>Number to Complete</asp:TableCell>
            <asp:TableCell>Target to Complete Section</asp:TableCell>
            <asp:TableCell>Actual Completion Date</asp:TableCell>
        </asp:TableRow>
        <asp:TableRow ID="TableRowP1Abstract">
            <asp:TableCell><asp:Image ImageUrl="~/images/abo/complete_True.png" runat="server" ID="ImageP1Abstract" Height="22px" Width="22px" ImageAlign="AbsBottom" /></asp:TableCell>
            <asp:TableCell><asp:HyperLink ID="LinkP1Abstract" runat="server" NavigateUrl="">Review/Enter Chart Data</asp:HyperLink></asp:TableCell>
            <asp:TableCell CssClass="center"><asp:Literal ID="LiteralP1AbstractCount" runat="server" Text="0"></asp:Literal></asp:TableCell>
            <asp:TableCell CssClass="center"><asp:Literal ID="LiteralP1AbstractTarget" runat="server" Text="1"></asp:Literal></asp:TableCell>
            <asp:TableCell CssClass="center"><asp:Literal ID="LiteralP1AbstractTargetDate" runat="server" Text=""></asp:Literal></asp:TableCell>
            <asp:TableCell CssClass="center"><asp:Literal ID="LiteralP1AbstractCompleteDate" runat="server" Text=""></asp:Literal></asp:TableCell>
        </asp:TableRow>
        <asp:TableRow ID="TableRowP1Profile">
            <asp:TableCell><asp:Image ImageUrl="~/images/abo/complete_False.png" runat="server" ID="ImageP1Profile" Height="22px" Width="22px" ImageAlign="AbsBottom" /></asp:TableCell>
            <asp:TableCell><asp:HyperLink ID="LinkP1Profile" runat="server" NavigateUrl="">Participant Profile</asp:HyperLink></asp:TableCell>
            <asp:TableCell CssClass="center"><asp:Literal ID="LiteralP1ProfileCount" runat="server" Text="0"></asp:Literal></asp:TableCell>
            <asp:TableCell CssClass="center"><asp:Literal ID="LiteralP1ProfileTarget" runat="server" Text="1"></asp:Literal></asp:TableCell>
            <asp:TableCell CssClass="center"><asp:Literal ID="LiteralP1ProfileTargetDate" runat="server" Text=""></asp:Literal></asp:TableCell>
            <asp:TableCell CssClass="center"><asp:Literal ID="LiteralP1ProfileCompleteDate" runat="server" Text=""></asp:Literal></asp:TableCell>
        </asp:TableRow>
        
        <asp:TableRow ID="TableRowP1SystemAnalysis">
            <asp:TableCell><asp:Image ImageUrl="~/images/abo/complete_False.png" runat="server" ID="ImageP1SystemAnalysis" Height="22px" Width="22px" ImageAlign="AbsBottom" /></asp:TableCell>
            <asp:TableCell><asp:HyperLink ID="LinkP1SystemAnalysis" runat="server" NavigateUrl="">Current Practice System Analysis</asp:HyperLink></asp:TableCell>
            <asp:TableCell CssClass="center"><asp:Literal ID="LiteralP1SystemAnalysisCount" runat="server" Text="0"></asp:Literal></asp:TableCell>
            <asp:TableCell CssClass="center"><asp:Literal ID="LiteralP1SystemAnalysisTarget" runat="server" Text="1"></asp:Literal></asp:TableCell>
            <asp:TableCell CssClass="center"><asp:Literal ID="LiteralP1SystemAnalysisTargetDate" runat="server" Text=""></asp:Literal></asp:TableCell>
            <asp:TableCell CssClass="center"><asp:Literal ID="LiteralP1SystemAnalysisCompletedDate" runat="server" Text=""></asp:Literal></asp:TableCell>
        </asp:TableRow>

        <asp:TableRow ID="TableRowP1ReviewReport">
            <asp:TableCell><asp:Image ImageUrl="~/images/abo/complete_False.png" runat="server" ID="ImageP1ReviewReport" Height="22px" Width="22px" ImageAlign="AbsBottom" /></asp:TableCell>
            <asp:TableCell><asp:HyperLink ID="LinkP1ReviewReport" runat="server" NavigateUrl="">Review/Approve Report</asp:HyperLink></asp:TableCell>
            <asp:TableCell CssClass="center"><asp:Literal ID="LiteralP1ReviewReportCount" runat="server" Text="0"></asp:Literal></asp:TableCell>
            <asp:TableCell CssClass="center"><asp:Literal ID="LiteralP1ReviewReportTarget" runat="server" Text="1"></asp:Literal></asp:TableCell>
            <asp:TableCell CssClass="center"><asp:Literal ID="LiteralP1ReviewReportTargetDate" runat="server" Text=""></asp:Literal></asp:TableCell>
            <asp:TableCell CssClass="center"><asp:Literal ID="LiteralP1ReviewReportCompleteDate" runat="server" Text=""></asp:Literal></asp:TableCell>
        </asp:TableRow>
        <asp:TableRow>
            <asp:TableCell ColumnSpan="6" BackColor="White">&nbsp;</asp:TableCell>
        </asp:TableRow>
        
        <asp:TableRow CssClass="table_header">
            <asp:TableCell>&nbsp;</asp:TableCell>
            <asp:TableCell>Part 2 - Improvement Plan</asp:TableCell>
            <asp:TableCell>My Data</asp:TableCell>
            <asp:TableCell>Number to Complete</asp:TableCell>
            <asp:TableCell>Target to Complete Section</asp:TableCell>
            <asp:TableCell>Actual Completion Date</asp:TableCell>
        </asp:TableRow>
        <asp:TableRow ID="TableRowP2ViewReport">
            <asp:TableCell>&nbsp;</asp:TableCell>
            <asp:TableCell><asp:HyperLink ID="LinkP2ViewReport" runat="server">View Part 1 Report</asp:HyperLink></asp:TableCell>
            <asp:TableCell CssClass="center"><asp:Literal ID="LiteralP2ViewReportCount" runat="server" Text=""></asp:Literal></asp:TableCell>
            <asp:TableCell CssClass="center"><asp:Literal ID="LiteralP2ViewReportTarget" runat="server" Text=""></asp:Literal></asp:TableCell>
            <asp:TableCell CssClass="center"><asp:Literal ID="LiteralP2ViewReportTargetDate" runat="server" Text=""></asp:Literal></asp:TableCell>
            <asp:TableCell CssClass="center"><asp:Literal ID="LiteralP2ViewReportCompleteDate" runat="server" Text=""></asp:Literal></asp:TableCell>
        </asp:TableRow>
        <asp:TableRow ID="TableRowP2Measures">
            <asp:TableCell><asp:Image ImageUrl="~/images/abo/complete_False.png" runat="server" ID="ImageP2Measures" Height="22px" Width="22px" ImageAlign="AbsBottom" /></asp:TableCell>
            <asp:TableCell><asp:HyperLink ID="LinkP2Measures" runat="server" NavigateUrl="">Select Measures</asp:HyperLink></asp:TableCell>
            <asp:TableCell CssClass="center"><asp:Literal ID="LiteralP2MeasuresCount" runat="server" Text="0"></asp:Literal></asp:TableCell>
            <asp:TableCell CssClass="center"><asp:Literal ID="LiteralP2MeasuresTarget" runat="server" Text="1"></asp:Literal></asp:TableCell>
            <asp:TableCell CssClass="center"><asp:Literal ID="LiteralP2MeasuresTargetDate" runat="server" Text=""></asp:Literal></asp:TableCell>
            <asp:TableCell CssClass="center"><asp:Literal ID="LiteralP2MeasuresCompleteDate" runat="server" Text=""></asp:Literal></asp:TableCell>
        </asp:TableRow>
        <asp:TableRow ID="TableRowP2DevelopPlan">
            <asp:TableCell><asp:Image ImageUrl="~/images/abo/complete_False.png" runat="server" ID="ImageP2DevelopPlan" Height="22px" Width="22px" ImageAlign="AbsBottom" /></asp:TableCell>
            <asp:TableCell><asp:HyperLink ID="LinkP2DevelopPlan" runat="server">Develop/Approve Improvement Plan</asp:HyperLink></asp:TableCell>
            <asp:TableCell CssClass="center"><asp:Literal ID="LiteralP2DevelopPlanCount" runat="server" Text="0"></asp:Literal></asp:TableCell>
            <asp:TableCell CssClass="center"><asp:Literal ID="LiteralP2DevelopPlanTarget" runat="server" Text="1"></asp:Literal></asp:TableCell>
            <asp:TableCell CssClass="center"><asp:Literal ID="LiteralP2DevelopPlanTargetDate" runat="server" Text=""></asp:Literal></asp:TableCell>
            <asp:TableCell CssClass="center"><asp:Literal ID="LiteralP2DevelopPlanCompleteDate" runat="server" Text=""></asp:Literal></asp:TableCell>
        </asp:TableRow>
        <asp:TableRow ID="TableRowP2ViewPlan">
            <asp:TableCell>&nbsp;</asp:TableCell>
            <asp:TableCell><asp:HyperLink ID="LinkP2ViewPlan" runat="server">View Approved Improvement Plan</asp:HyperLink></asp:TableCell>
            <asp:TableCell CssClass="center"><asp:Literal ID="LiteralP2ViewPlanCount" runat="server" Text=""></asp:Literal></asp:TableCell>
            <asp:TableCell CssClass="center"><asp:Literal ID="LiteralP2ViewPlanTarget" runat="server" Text=""></asp:Literal></asp:TableCell>
            <asp:TableCell CssClass="center"><asp:Literal ID="LiteralP2ViewPlanTargetDate" runat="server" Text=""></asp:Literal></asp:TableCell>
            <asp:TableCell CssClass="center"><asp:Literal ID="LiteralP2ViewPlanCompleteDate" runat="server" Text=""></asp:Literal></asp:TableCell>
        </asp:TableRow>
        <asp:TableRow ID="TableRowP2Resources">
            <asp:TableCell>&nbsp;</asp:TableCell>
            <asp:TableCell><asp:HyperLink ID="LinkP2Resources" runat="server">Access Improvement Resources</asp:HyperLink></asp:TableCell>
            <asp:TableCell CssClass="center"></asp:TableCell>
            <asp:TableCell CssClass="center"></asp:TableCell>
            <asp:TableCell CssClass="center"></asp:TableCell>
            <asp:TableCell CssClass="center"></asp:TableCell>
        </asp:TableRow>
        <asp:TableRow ID="TableRowP2Strategies">
            <asp:TableCell>&nbsp;</asp:TableCell>
            <asp:TableCell><asp:HyperLink ID="LinkP2Strategies" runat="server">Access Improvement Strategies</asp:HyperLink></asp:TableCell>
            <asp:TableCell CssClass="center"></asp:TableCell>
            <asp:TableCell CssClass="center"></asp:TableCell>
            <asp:TableCell CssClass="center"></asp:TableCell>
            <asp:TableCell CssClass="center"></asp:TableCell>
        </asp:TableRow>
        <asp:TableRow>
            <asp:TableCell ColumnSpan="6" BackColor="White">&nbsp;</asp:TableCell>
        </asp:TableRow>

        <asp:TableRow CssClass="table_header">
            <asp:TableCell>&nbsp;</asp:TableCell>
            <asp:TableCell>Part 3 - Data Collection Second Abstraction</asp:TableCell>
            <asp:TableCell>My Data</asp:TableCell>
            <asp:TableCell>Number to Complete</asp:TableCell>
            <asp:TableCell>Target to Complete Section</asp:TableCell>
            <asp:TableCell>Actual Completion Date</asp:TableCell>
        </asp:TableRow>

        <asp:TableRow ID="TableRowP3Abstract">
            <asp:TableCell><asp:Image ImageUrl="~/images/abo/complete_False.png" runat="server" ID="ImageP3Abstract" Height="22px" Width="22px" ImageAlign="AbsBottom" /></asp:TableCell>
            <asp:TableCell><asp:HyperLink ID="LinkP3Abstract" runat="server">Review/Enter Chart Data</asp:HyperLink></asp:TableCell>
            <asp:TableCell CssClass="center"><asp:Literal ID="LiteralP3AbstractCount" runat="server" Text="0"></asp:Literal></asp:TableCell>
            <asp:TableCell CssClass="center"><asp:Literal ID="LiteralP3AbstractTarget" runat="server" Text="1"></asp:Literal></asp:TableCell>
            <asp:TableCell CssClass="center"><asp:Literal ID="LiteralP3AbstractTargetDate" runat="server" Text=""></asp:Literal></asp:TableCell>
            <asp:TableCell CssClass="center"><asp:Literal ID="LiteralP3AbstractCompleteDate" runat="server" Text=""></asp:Literal></asp:TableCell>
        </asp:TableRow>
        <asp:TableRow ID="TableRowP3Profile">
            <asp:TableCell><asp:Image ImageUrl="~/images/abo/complete_False.png" runat="server" ID="ImageP3Profile" Height="22px" Width="22px" ImageAlign="AbsBottom" /></asp:TableCell>
            <asp:TableCell><asp:HyperLink ID="LinkP3Profile" runat="server">Participant Profile</asp:HyperLink></asp:TableCell>
            <asp:TableCell CssClass="center"><asp:Literal ID="LiteralP3ProfileCount" runat="server" Text="0"></asp:Literal></asp:TableCell>
            <asp:TableCell CssClass="center"><asp:Literal ID="LiteralP3ProfileTarget" runat="server" Text="1"></asp:Literal></asp:TableCell>
            <asp:TableCell CssClass="center"><asp:Literal ID="LiteralP3ProfileTargetDate" runat="server" Text=""></asp:Literal></asp:TableCell>
            <asp:TableCell CssClass="center"><asp:Literal ID="LiteralP3ProfileCompleteDate" runat="server" Text=""></asp:Literal></asp:TableCell>
        </asp:TableRow>

        <asp:TableRow ID="TableRowP3SystemAnalysis">
            <asp:TableCell><asp:Image ImageUrl="~/images/abo/complete_False.png" runat="server" ID="ImageP3SystemAnalysis" Height="22px" Width="22px" ImageAlign="AbsBottom" /></asp:TableCell>
            <asp:TableCell><asp:HyperLink ID="LinkP3SystemAnalysis" runat="server" NavigateUrl="">Current Practice System Analysis</asp:HyperLink></asp:TableCell>
            <asp:TableCell CssClass="center"><asp:Literal ID="LiteralP3SystemAnalysisCount" runat="server" Text="0"></asp:Literal></asp:TableCell>
            <asp:TableCell CssClass="center"><asp:Literal ID="LiteralP3SystemAnalysisTarget" runat="server" Text="1"></asp:Literal></asp:TableCell>
            <asp:TableCell CssClass="center"><asp:Literal ID="LiteralP3SystemAnalysisTargetDate" runat="server" Text=""></asp:Literal></asp:TableCell>
            <asp:TableCell CssClass="center"><asp:Literal ID="LiteralP3SystemAnalysisCompletedDate" runat="server" Text=""></asp:Literal></asp:TableCell>
        </asp:TableRow>

        <asp:TableRow ID="TableRowP3ReviewReport">
            <asp:TableCell><asp:Image ImageUrl="~/images/abo/complete_False.png" runat="server" ID="ImageP3ReviewReport" Height="22px" Width="22px" ImageAlign="AbsBottom" /></asp:TableCell>
            <asp:TableCell><asp:HyperLink ID="LinkP3ReviewReport" runat="server">Review/Approve Report</asp:HyperLink></asp:TableCell>
            <asp:TableCell CssClass="center"><asp:Literal ID="LiteralP3ReviewReportCount" runat="server" Text="0"></asp:Literal></asp:TableCell>
            <asp:TableCell CssClass="center"><asp:Literal ID="LiteralP3ReviewReportTarget" runat="server" Text="1"></asp:Literal></asp:TableCell>
            <asp:TableCell CssClass="center"><asp:Literal ID="LiteralP3ReviewReportTargetDate" runat="server" Text=""></asp:Literal></asp:TableCell>
            <asp:TableCell CssClass="center"><asp:Literal ID="LiteralP3ReviewReportCompleteDate" runat="server" Text=""></asp:Literal></asp:TableCell>
        </asp:TableRow>
        <asp:TableRow>
            <asp:TableCell ColumnSpan="6" BackColor="White">&nbsp;</asp:TableCell>
        </asp:TableRow>
        
        <asp:TableRow CssClass="table_header">
            <asp:TableCell>&nbsp;</asp:TableCell>
            <asp:TableCell>Part 4 - Report Results</asp:TableCell>
            <asp:TableCell>My Data</asp:TableCell>
            <asp:TableCell>Number to Complete</asp:TableCell>
            <asp:TableCell>Target to Complete Section</asp:TableCell>
            <asp:TableCell>Actual Completion Date</asp:TableCell>
        </asp:TableRow>
        <asp:TableRow ID="TableRowP4ViewReport">
            <asp:TableCell>&nbsp;</asp:TableCell>
            <asp:TableCell><asp:HyperLink ID="LinkP4ViewReport" runat="server">View Part 3 Report</asp:HyperLink></asp:TableCell>
            <asp:TableCell CssClass="center"></asp:TableCell>
            <asp:TableCell CssClass="center"></asp:TableCell>
            <asp:TableCell CssClass="center"></asp:TableCell>
            <asp:TableCell CssClass="center"></asp:TableCell>
        </asp:TableRow>
        <asp:TableRow ID="TableRowP4Impact">
            <asp:TableCell><asp:Image ImageUrl="~/images/abo/complete_False.png" runat="server" ID="ImageP4Impact" Height="22px" Width="22px" ImageAlign="AbsBottom" /></asp:TableCell>
            <asp:TableCell><asp:HyperLink ID="LinkP4Impact" runat="server">Develop/Approve Impact Statement</asp:HyperLink></asp:TableCell>
            <asp:TableCell CssClass="center"><asp:Literal ID="LiteralP4ImpactCount" runat="server" Text="0"></asp:Literal></asp:TableCell>
            <asp:TableCell CssClass="center"><asp:Literal ID="LiteralP4ImpactTarget" runat="server" Text="1"></asp:Literal></asp:TableCell>
            <asp:TableCell CssClass="center"><asp:Literal ID="LiteralP4ImpactTargetDate" runat="server" Text=""></asp:Literal></asp:TableCell>
            <asp:TableCell CssClass="center"><asp:Literal ID="LiteralP4ImpactCompleteDate" runat="server" Text=""></asp:Literal></asp:TableCell>
        </asp:TableRow>
        <asp:TableRow ID="TableRowP4VeiwImpact">
            <asp:TableCell>&nbsp;</asp:TableCell>
            <asp:TableCell><asp:HyperLink ID="LinkP4VeiwImpact" runat="server">View Approved Impact Statement</asp:HyperLink></asp:TableCell>
            <asp:TableCell CssClass="center"></asp:TableCell>
            <asp:TableCell CssClass="center"></asp:TableCell>
            <asp:TableCell CssClass="center"></asp:TableCell>
            <asp:TableCell CssClass="center"></asp:TableCell>
        </asp:TableRow>
        <asp:TableRow ID="TableRowP4SubmitCompletedModule">
            <asp:TableCell><asp:Image ImageUrl="~/images/abo/complete_False.png" runat="server" ID="ImageP4SubmitCompletedModule" Height="22px" Width="22px" ImageAlign="AbsBottom" /></asp:TableCell>
            <asp:TableCell><asp:HyperLink ID="LinkP4SubmitCompletedModule" runat="server">Submit Completed Activity</asp:HyperLink></asp:TableCell>
            <asp:TableCell CssClass="center"><asp:Literal ID="LiteralP4SubmitModuleCount" runat="server" Text="0"></asp:Literal></asp:TableCell>
            <asp:TableCell CssClass="center"><asp:Literal ID="LiteralP4SubmitModuleTarget" runat="server" Text="1"></asp:Literal></asp:TableCell>
            <asp:TableCell CssClass="center"><asp:Literal ID="LiteralP4SubmitModuleTargetDate" runat="server" Text=""></asp:Literal></asp:TableCell>
            <asp:TableCell CssClass="center"><asp:Literal ID="LiteralP4SubmitModuleCompleteDate" runat="server" Text=""></asp:Literal></asp:TableCell>
        </asp:TableRow>
        <asp:TableRow ID="TableRowP4Credits">
            <asp:TableCell ColumnSpan="6" CssClass="right"><asp:Literal ID="LiteralP4Credits" runat="server" Text=""></asp:Literal>  
                <asp:Button ID="ButtonP4Credits" Enabled="false" runat="server" Text="Claim 20 Credits for Completion" OnClick="ButtonP4Credits_Click" SkinID="ButtonHighlight" 
                OnClientClick="return confirm('Do you want to Claim the Credits for Completion?');" /></asp:TableCell>
        </asp:TableRow>
        <asp:TableRow>
            <asp:TableCell ColumnSpan="6" BackColor="White">&nbsp;</asp:TableCell>
        </asp:TableRow>
        <asp:TableRow ID="TableRowP4StartNewModule" Visible="false">
            <asp:TableCell ColumnSpan="6" CssClass="right"><asp:Literal ID="LiteralStartNewModule" runat="server" Text=""></asp:Literal>  
                <asp:Button ID="ButtonP4StartNewModule" runat="server" Text="Start New Improvement Cycle" OnClick="ButtonP4StartNewModule_Click" SkinID="ButtonHighlight" 
                OnClientClick="return confirm('Do you want to start a new Improvement Cycle using the data from your second abstaction as the foundation for a new Improvement Cycle?\n\n\nClick OK to and we will setup another Improvement Cycle and you can use your data to build an improvement plan using our recommended resources.');" /></asp:TableCell>
        </asp:TableRow>
        <asp:TableRow ID="TableRowP4StartNewModuleSeparator" Visible="false">
            <asp:TableCell ColumnSpan="6" BackColor="White">&nbsp;</asp:TableCell>
        </asp:TableRow>
    </asp:Table>
</asp:Content>

