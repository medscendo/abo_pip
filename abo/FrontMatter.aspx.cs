﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using NetHealthPIMModel;
using System.Transactions;

public partial class abo_FrontMatter : BasePage
{
    protected void Page_Load(object sender, EventArgs e)
    {
        ((PIMMasterPage)Page.Master).SetHeader("CME Disclosures &amp; Overview");
        ((PIMMasterPage)Page.Master).SetStatus(1);
        if (!IsPostBack)
        {
            using (NetHealthPIMEntities pim = new NetHealthPIMEntities())
            {
                if (Request.QueryString["fm"] == "1")
                {
                    ButtonSubmit.Visible = false;
                    chkAcknowledgment.Visible = false;
                }
                else
                {
                    Participant participant = pim.Participant.First(p => p.ParticipantID == ctx.ParticipantID);
                    if (participant.AcknowledgmentDate != null)
                    {
                        Response.Redirect("~/" + module.ModuleCode.ToLower() + "/FirstTutorial.aspx");
                    }
                }
            }
        }
    }

    protected void ButtonSubmit_Click(object sender, EventArgs e)
    {
        using (NetHealthPIMEntities pim = new NetHealthPIMEntities())
        {
            Participant participant = pim.Participant.First(p => p.ParticipantID == ctx.ParticipantID);
            participant.AcknowledgmentDate = System.DateTime.Today;
            pim.SaveChanges();
            Response.Redirect("~/" + module.ModuleCode.ToLower() + "/FirstTutorial.aspx");
        }
    }
}