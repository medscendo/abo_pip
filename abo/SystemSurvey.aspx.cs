﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Web.UI.HtmlControls;
using NetHealthPIMModel;
public partial class abo_SystemSurvey : BasePage
{

    protected void Page_Load(object sender, EventArgs e)
    {
        using (NetHealthPIMEntities pim = new NetHealthPIMEntities()) 
        {
            ((PIMMasterPage)Page.Master).SetTitle("System Survey");
            ((PIMMasterPage)Page.Master).SetStatus(2);
            ParticipantModuleCycle prev = (ParticipantModuleCycle)Session[Constants.SESSION_PREVIOUSCYCLE];
            String CycleNumber = Request.QueryString["CycleNumber"];
            bool stillCycleOne = true;

            int cycleID = ctx.ActiveModuleCycleID;
            if (CycleNumber == "1")
            {
                stillCycleOne = (cycleID == prev.ParticipantModuleCycleID);
                cycleID = prev.ParticipantModuleCycleID;
                if (stillCycleOne == false)
                {

                    ButtonSubmit.Visible = false;
                }
            }
            else
            {
                ((PIMMasterPage)Page.Master).SetStatus(3);
                LinkButtonReturnToStatus.PostBackUrl = "Dashboard3.aspx";

            }

            ParticipantModuleCycle cycle = pim.ParticipantModuleCycle.First(c => c.ParticipantModuleCycleID == cycleID);
            int modid = (int)Session[Constants.SESSION_WORKINGMODULEID];
            Module module = pim.Module.First(c => c.ModuleID == modid);
            ((PIMMasterPage)Page.Master).SetHeader("System Survey: " + module.ModuleName);
            HtmlTable table = new HtmlTable();
            var questions = (from c in pim.SystemSurveyQuestion where c.ModuleID == modid select c).OrderBy(b => b.SSMeasureQuestionSortOrder).ToList();
            var modulename = (from c in pim.Module where c.ModuleID == modid select c.ModuleName).FirstOrDefault();
            Label1.Text = modulename;
            foreach (var item in questions)
            {
                HtmlTableRow row = new HtmlTableRow();
                HtmlTableCell cell1 = new HtmlTableCell();
                HtmlTableCell cell2 = new HtmlTableCell();
                cell2.Attributes.Add("style", "vertical-align: left");

                cell1.InnerHtml = item.SSMeasureQuestion;
                if (item.SSMeasureQuestionType == 1)
                {
                    RadioButtonList rb = new RadioButtonList();
                    rb.ID = "choices" + item.SSQuestionID.ToString();
                    rb.CssClass = "tableyesno";
                    rb.Items.Add(new ListItem(" Yes", "True"));
                    rb.Items.Add(new ListItem(" No", "False"));
                    rb.RepeatDirection = RepeatDirection.Horizontal;
                    cell2.Controls.Add(rb);
                }
                if (item.SSMeasureQuestionType == 9)
                {
                    TextBox tx = new TextBox();
                    tx.ID = "choices" + item.SSQuestionID.ToString();
                    tx.TextMode = TextBoxMode.MultiLine;
                    tx.Width = 300;
                    tx.Height = 100;
                    tx.MaxLength = 100;
                    cell2.Controls.Add(tx);
                }
                if (item.SSMeasureQuestionType == 5)
                {
                    TextBox tx = new TextBox();
                    tx.ID = "choices" + item.SSQuestionID.ToString();
                    tx.MaxLength = 3;
                    AjaxControlToolkit.FilteredTextBoxExtender txtfilteralighnpd = new AjaxControlToolkit.FilteredTextBoxExtender();
                    txtfilteralighnpd.FilterType = AjaxControlToolkit.FilterTypes.Custom;
                    txtfilteralighnpd.ValidChars = "1234567890+-=/*().";
                    txtfilteralighnpd.TargetControlID = "choices" + item.SSQuestionID.ToString();
                    txtfilteralighnpd.ID = "choices" + item.SSQuestionID.ToString() + "fl";
                    cell2.Controls.Add(tx);
                    cell2.Controls.Add(txtfilteralighnpd);
                }
                if (item.SSMeasureQuestionType == 2)
                {
                    TextBox tx = new TextBox();
                    tx.ID = "choices" + item.SSQuestionID.ToString();
                    tx.MaxLength = 100;
                    cell2.Controls.Add(tx);
                }
                if (item.SSMeasureQuestionType == 4)
                {
                    CheckBoxList chklist = new CheckBoxList();
                    chklist.ID = "choices" + item.SSQuestionID.ToString();
                    chklist.CssClass = "chktable";
                    var choices = (from c in pim.SystemSurveyQuestionChoices where c.SystemSurveyQuestion.SSQuestionID == item.SSQuestionID select c).ToList();
                    foreach (var choice in choices)
                    {
                        chklist.Items.Add(new ListItem(" " + choice.Choice, choice.ChoiceID.ToString()));
                    }
                    cell2.Controls.Add(chklist);
                }
                if (item.SSMeasureQuestionType == 14)
                {
                    DropDownList ddlist = new DropDownList();
                    ddlist.ID = "choices" + item.SSQuestionID.ToString();
                    var choices = (from c in pim.SystemSurveyQuestionChoices where c.SystemSurveyQuestion.SSQuestionID == item.SSQuestionID select c).ToList();
                    foreach (var choice in choices)
                    {
                        ddlist.Items.Add(new ListItem(choice.Choice, choice.ChoiceID.ToString()));
                    }
                    cell2.Controls.Add(ddlist);
                }
                if (item.SSMeasureQuestionType == 15)
                {
                    DropDownList ddlist = new DropDownList();
                    ddlist.ID = "choices" + item.SSQuestionID.ToString();
                    ddlist.DataSource = CycleManager.getNewExamValues();
                    ddlist.DataTextField = "examLabel";
                    ddlist.DataValueField = "examValue";
                    ddlist.DataBind();
                    ddlist.Items.Insert(0, new ListItem("Select", ""));
                    cell2.Controls.Add(ddlist);
                }
                if (item.SSMeasureQuestionType == 3)
                {
                    RadioButtonList rblist = new RadioButtonList();
                    rblist.ID = "choices" + item.SSQuestionID.ToString();
                    rblist.CssClass = "chktable";
                    var choices = (from c in pim.SystemSurveyQuestionChoices where c.SystemSurveyQuestion.SSQuestionID == item.SSQuestionID select c).ToList();
                    foreach (var choice in choices)
                    {
                        rblist.Items.Add(new ListItem(choice.Choice, choice.ChoiceID.ToString()));
                    }
                    cell2.Controls.Add(rblist);
                }
                row.Controls.Add(cell1);
                row.Controls.Add(cell2);
                table.Controls.Add(row);

            }
            Panel1.Controls.Add(table);
            var measureinfo = (from c in pim.SystemSurvey
                               from v in pim.SystemSurveyQuestion
                               where c.SSQuestionID == v.SSQuestionID
                               && c.ModuleID == modid
                               && c.ParticipantModuleCycleID == cycleID
                               select new { c, v.SSMeasureQuestionType }).ToList();
            foreach (var item in measureinfo)
            {
                Control myControl1 = Panel1.FindControl("choices" + item.c.SSQuestionID.ToString());
                if (item.SSMeasureQuestionType == 1)
                {
                    RadioButtonList rbl = (RadioButtonList)myControl1;
                    if (item.c.SSQuestionTrueFalse != null)
                    {
                        rbl.SelectedValue = item.c.SSQuestionTrueFalse.ToString();
                    }
                }
                if(item.SSMeasureQuestionType==5)
                {
                    TextBox txt = (TextBox)myControl1;
                    if (item.c.SSQuestionNumerResponse != null)
                    {
                        txt.Text = item.c.SSQuestionNumerResponse.ToString();
                    }
                }
               
                if (item.SSMeasureQuestionType == 2 || item.SSMeasureQuestionType == 9)
                {
                    TextBox txt = (TextBox)myControl1;
                    if (item.c.SSQuestionResponse != null)
                    {
                        txt.Text = item.c.SSQuestionResponse.ToString();
                    }
                }
                if (item.SSMeasureQuestionType == 3)
                {
                    RadioButtonList rbl = (RadioButtonList)myControl1;
                    if (item.c.SSQuestionChoiceID != null)
                    {
                        rbl.SelectedValue = item.c.SSQuestionChoiceID.ToString();
                    }
                }
                if (item.SSMeasureQuestionType == 4)
                {
                    CheckBoxList chbl = (CheckBoxList)myControl1;
                    var SelectedChoices = (from c in pim.SystemSyrveyChoicesUserReply where c.SystemSurveyQuestion.SSQuestionID == item.c.SSQuestionID && c.ParticipantModuleCycleID == cycleID select c).ToList();
                    foreach (var ch in SelectedChoices)
                    {
                        chbl.Items.FindByValue(ch.ChoiceID.ToString()).Selected = true;
                    }
                }
                if (item.SSMeasureQuestionType == 14)
                {
                    DropDownList ddl = (DropDownList)myControl1;
                    if (item.c.SSQuestionChoiceID != null)
                    {
                        ddl.SelectedValue = item.c.SSQuestionChoiceID.ToString();
                    }
                }
                if (item.SSMeasureQuestionType == 15)
                {
                    DropDownList ddl = (DropDownList)myControl1;
                    if (item.c.VisualAcuityResponse!= null)
                    {
                        ddl.SelectedValue = item.c.VisualAcuityResponse.ToString();
                    }
                }
            }
        }

    }

    protected void ButtonSubmit_Click(object sender, CommandEventArgs e)
    {
        using (NetHealthPIMEntities pim = new NetHealthPIMEntities())
        {
            int cycleID = ctx.ActiveModuleCycleID;
            bool completedSystemSurvey = true;
            ParticipantModuleCycle cycle = pim.ParticipantModuleCycle.First(c => c.ParticipantModuleCycleID == cycleID);
            int modid = (int)Session[Constants.SESSION_WORKINGMODULEID];
            var questions = (from c in pim.SystemSurveyQuestion where c.ModuleID == modid select c).OrderBy(b => b.SSMeasureQuestionSortOrder).ToList();

            foreach (var item in questions)
            {
                Control myControl1 = Panel1.FindControl("choices" + item.SSQuestionID.ToString());
                SystemSurvey sf = (from n in pim.SystemSurvey
                                   where n.ParticipantModuleCycleID == cycleID
                                   && n.ModuleID == modid
                                   && n.SSQuestionID == item.SSQuestionID
                                   select n).FirstOrDefault();

                if (item.SSMeasureQuestionType == 4)
                {
                    CheckBoxList cbl = (CheckBoxList)myControl1;
                    if (item.Required != null && item.Required == true)
                    {
                        if (cbl.Items.Cast<ListItem>().Where(li => li.Selected)
                                                                               .Select(li => li.Value)
                                                                               .ToList().Count() <= 0)
                            completedSystemSurvey = false;
                    }
                    if (sf == null)
                    {
                        sf = new SystemSurvey();
                        sf.ModuleID = modid;
                        sf.SSQuestionID = item.SSQuestionID;
                        sf.ParticipantModuleCycleID = cycleID;
                        sf.LastUpdateDate = DateTime.Now;
                        pim.AddToSystemSurvey(sf);
                        pim.SaveChanges();

                    }
                    else
                    {
                        sf.LastUpdateDate = DateTime.Now;
                        pim.SaveChanges();

                    }
                    //delete past choices
                    var deletepastchoices = (from c in pim.SystemSyrveyChoicesUserReply
                                             where c.ParticipantModuleCycleID == cycleID
                                             && c.SystemSurveyQuestion.SSQuestionID == item.SSQuestionID
                                             select c).ToList();

                    foreach (var ch in deletepastchoices)
                    {
                        pim.SystemSyrveyChoicesUserReply.DeleteObject(ch);
                        pim.SaveChanges();
                    }
                    // insert new choices
                    List<string> selectedValues = cbl.Items.Cast<ListItem>()
                                                                               .Where(li => li.Selected)
                                                                               .Select(li => li.Value)
                                                                               .ToList();
                    foreach (var val in selectedValues)
                    {
                        SystemSyrveyChoicesUserReply userrep = new SystemSyrveyChoicesUserReply();
                        userrep.ParticipantModuleCycleID = cycleID;
                        userrep.SystemSurveyQuestion = pim.SystemSurveyQuestion.Where(c => c.SSQuestionID == item.SSQuestionID)
                            .Select(c => c).FirstOrDefault();
                        userrep.ChoiceID = Convert.ToInt32(val);
                        pim.SystemSyrveyChoicesUserReply.AddObject(userrep);
                        pim.SaveChanges();
                    }

                }
                if (item.SSMeasureQuestionType == 1)
                {
                    RadioButtonList rbl = (RadioButtonList)myControl1;
                    if (item.Required != null && item.Required == true)
                    {
                        if (rbl.SelectedValue == "")
                            completedSystemSurvey = false;
                    }
                    if (sf == null)
                    {
                        sf = new SystemSurvey();
                        if (rbl.SelectedValue != "")
                        {
                            sf.SSQuestionTrueFalse = Convert.ToBoolean(rbl.SelectedValue);
                            sf.ModuleID = modid;
                            sf.SSQuestionID = item.SSQuestionID;
                            sf.ParticipantModuleCycleID = cycleID;
                            sf.LastUpdateDate = DateTime.Now;
                            pim.AddToSystemSurvey(sf);
                            pim.SaveChanges();
                        }
                    }
                    else
                    {
                        sf.SSQuestionTrueFalse = Convert.ToBoolean(rbl.SelectedValue);
                        sf.LastUpdateDate = DateTime.Now;
                        pim.SaveChanges();

                    }

                }
                if (item.SSMeasureQuestionType == 2 || item.SSMeasureQuestionType == 9)
                {

                    TextBox txt = (TextBox)myControl1;
                    if (item.Required != null && item.Required == true)
                    {
                        if (txt.Text == "")
                            completedSystemSurvey = false;
                    }
                    if (sf == null)
                    {
                        sf = new SystemSurvey();

                        sf.SSQuestionResponse = txt.Text;
                        sf.ModuleID = modid;
                        sf.SSQuestionID = item.SSQuestionID;
                        sf.ParticipantModuleCycleID = cycleID;
                        sf.LastUpdateDate = DateTime.Now;
                        pim.AddToSystemSurvey(sf);
                        pim.SaveChanges();

                    }
                    else
                    {
                        sf.SSQuestionResponse = txt.Text;
                        sf.LastUpdateDate = DateTime.Now;
                        pim.SaveChanges();

                    }
                }
                if (item.SSMeasureQuestionType == 5)
                {

                    TextBox txt = (TextBox)myControl1;
                    if (item.Required != null && item.Required == true)
                    {
                        if (txt.Text == "")
                            completedSystemSurvey = false;
                    }
                    if (sf == null)
                    {
                        sf = new SystemSurvey();
                        double n;
                        bool isNumeric = double.TryParse(txt.Text, out n);
                        if (isNumeric == false)
                        {
                            completedSystemSurvey = false;
                        }
                        else
                        {
                            sf.SSQuestionNumerResponse = n;
                        }
                        sf.ModuleID = modid;
                        sf.SSQuestionID = item.SSQuestionID;
                        sf.ParticipantModuleCycleID = cycleID;
                        sf.LastUpdateDate = DateTime.Now;
                        pim.AddToSystemSurvey(sf);
                        pim.SaveChanges();

                    }
                    else
                    {
                        double n;
                        bool isNumeric = double.TryParse(txt.Text, out n);
                        if (isNumeric == false)
                        {
                            completedSystemSurvey = false;
                        }
                        else
                        {
                            sf.SSQuestionNumerResponse = n;
                        }
                        sf.LastUpdateDate = DateTime.Now;
                        pim.SaveChanges();

                    }
                }
                if (item.SSMeasureQuestionType == 3)
                {
                    RadioButtonList rbl = (RadioButtonList)myControl1;
                    if (item.Required != null && item.Required == true)
                    {
                        if (rbl.SelectedValue == "")
                            completedSystemSurvey = false;
                    }
                    if (sf == null)
                    {
                        sf = new SystemSurvey();
                        if (rbl.SelectedValue != "")
                        {
                            sf.SSQuestionChoiceID = Convert.ToInt32(rbl.SelectedValue);
                            sf.ModuleID = modid;
                            sf.SSQuestionID = item.SSQuestionID;
                            sf.ParticipantModuleCycleID = cycleID;
                            sf.LastUpdateDate = DateTime.Now;
                            pim.AddToSystemSurvey(sf);
                            pim.SaveChanges();
                        }
                    }
                    else
                    {
                        sf.SSQuestionChoiceID = Convert.ToInt32(rbl.SelectedValue);
                        sf.LastUpdateDate = DateTime.Now;
                        pim.SaveChanges();

                    }
                }
                if (item.SSMeasureQuestionType == 14)
                {
                    DropDownList rbl = (DropDownList)myControl1;
                    if (item.Required != null && item.Required == true)
                    {
                        if (rbl.SelectedValue == "")
                            completedSystemSurvey = false;
                    }
                    if (sf == null)
                    {
                        sf = new SystemSurvey();
                        if (rbl.SelectedValue != "")
                        {
                            sf.SSQuestionChoiceID = Convert.ToInt32(rbl.SelectedValue);
                            sf.ModuleID = modid;
                            sf.SSQuestionID = item.SSQuestionID;
                            sf.ParticipantModuleCycleID = cycleID;
                            sf.LastUpdateDate = DateTime.Now;
                            pim.AddToSystemSurvey(sf);
                            pim.SaveChanges();
                        }
                    }
                    else
                    {
                        sf.SSQuestionChoiceID = Convert.ToInt32(rbl.SelectedValue);
                        sf.LastUpdateDate = DateTime.Now;
                        pim.SaveChanges();

                    }
                }
                if (item.SSMeasureQuestionType == 15)
                {
                    DropDownList rbl = (DropDownList)myControl1;
                    if (item.Required != null && item.Required == true)
                    {
                        if (rbl.SelectedValue == "")
                            completedSystemSurvey = false;
                    }
                    if (sf == null)
                    {
                        sf = new SystemSurvey();
                        if (rbl.SelectedValue != "")
                        {
                            sf.VisualAcuityResponse = Convert.ToInt32(rbl.SelectedValue);
                            sf.ModuleID = modid;
                            sf.SSQuestionID = item.SSQuestionID;
                            sf.ParticipantModuleCycleID = cycleID;
                            sf.LastUpdateDate = DateTime.Now;
                            pim.AddToSystemSurvey(sf);
                            pim.SaveChanges();
                        }
                    }
                    else
                    {
                        sf.VisualAcuityResponse = Convert.ToInt32(rbl.SelectedValue);
                        sf.LastUpdateDate = DateTime.Now;
                        pim.SaveChanges();

                    }
                } 

            }

            ParticipantModuleSelection moduleSelection = (from ms in pim.ParticipantModuleSelection
                                                          where ms.ParticipantModuleCycle.ParticipantModuleCycleID == cycleID & ms.ModuleID == modid
                                                          select ms).First<ParticipantModuleSelection>();
            moduleSelection.SystemSurveyLastUpdatedDate = DateTime.Now;
            if (completedSystemSurvey)
            {
                moduleSelection.SystemSurveyComplete = true;
                moduleSelection.SystemSurveyCompletedDate = DateTime.Now;
            }
            else
            {
                moduleSelection.SystemSurveyComplete = false;
                moduleSelection.SystemSurveyCompletedDate = null;
            }
            pim.SaveChanges();



            if (completedSystemSurvey == false)
            {
                LabelError.Text = "Please answer all system survey questions.";
                LabelError.ForeColor = System.Drawing.Color.Red;
                LabelError.Visible = true;
                MessageBox.Show("Please answer all system survey questions.");
            }
            else
            {
                var missingChartRegistration = (from pps in pim.ParticipantChartStatus_V
                                                where pps.ParticipantModuleCycleID == cycleID
                                                 & pps.ModuleID == modid
                                                 & ((pps.PatientChartCompleted != pps.TotalCharts) || (pps.Completed != pps.TotalCharts))
                                                select pps).Count();
                if (missingChartRegistration > 0)
                {
                    ParticipantChartStatus_V ppsV = pim.ParticipantChartStatus_V.First(c => c.ParticipantModuleCycleID == cycleID & ((c.PatientChartCompleted != c.TotalCharts) || (c.Completed != c.TotalCharts)));
                    Response.Redirect("PatientChartRegistration.aspx");
                }
                else
                {
                    missingChartRegistration = (from pps in pim.ParticipantChartStatus_V
                                                where pps.ParticipantModuleCycleID == cycleID
                                                 & ((pps.PatientChartCompleted != pps.TotalCharts) || (pps.Completed != pps.TotalCharts))
                                                select pps).Count();
                    if (missingChartRegistration > 0)
                    {
                        ParticipantChartStatus_V ppsV = pim.ParticipantChartStatus_V.First(c => c.ParticipantModuleCycleID == cycleID & ((c.PatientChartCompleted != c.TotalCharts) || (c.Completed != c.TotalCharts)));
                        Session[Constants.SESSION_WORKINGMODULEID] = ppsV.ModuleID;
                        Response.Redirect("PatientChartRegistration.aspx");
                    }
                }
                if (cycle.CycleNumber == 1)
                {
                    // has confirm selecttion been made?
                    if (cycle.ModuleSelectionComplete != true)
                    {
                        if (cycle.SelectPECComplete == true)
                        {
                            cycle.ModuleSelectionComplete = true;
                            cycle.ModuleSelectionCompletedDate = DateTime.Today;
                            pim.SaveChanges();
                        }
                    }
                    if (cycle.ModuleSelectionComplete == true)
                    {
                            Response.Redirect("Dashboard.aspx");
                    }
                    else
                    {
                        Response.Redirect("ModuleSelectionReview.aspx");
                    }
                }
                else
                    Response.Redirect("Dashboard3.aspx");
            }
        }
    }
    protected void LinkButtonReturnToStatus_Click(object sender, CommandEventArgs e)
    {
        String CycleNumber = Request.QueryString["CycleNumber"];
        if (CycleNumber == "1")
            Response.Redirect("Dashboard.aspx");
        else
            Response.Redirect("Dashboard3.aspx");
    }
}

