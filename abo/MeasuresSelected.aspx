﻿<%@ Page Title="" Language="C#" MasterPageFile="~/PIMMasterPage.master" AutoEventWireup="true" CodeFile="MeasuresSelected.aspx.cs" Inherits="abo_MeasuresSelected" %>

<asp:Content ID="Content1" ContentPlaceHolderID="head" Runat="Server">
</asp:Content>

<asp:Content ID="Content2" ContentPlaceHolderID="javascript" Runat="Server">

<script type="text/javascript" language="javascript">
    function showMoreResources(el, mid, tbl) {
        var table = $get(tbl);
        var rows = table.rows;
        for (var i = 0; i < rows.length; i++) {
            var midAttr = rows[i].mid;
            if (midAttr == mid) {
                rows[i].style.display = "block";
            }
        }
        el.style.display = "none";
    }
    function confirmDelete(MeasureTypeID) {
        var HiddenFieldOutcomeMeasures = document.getElementById("<%= HiddenFieldOutcomeMeasures.ClientID %>").value;
        var HiddenFieldTotalMeasures = document.getElementById("<%= HiddenFieldTotalMeasures.ClientID %>").value;
        
        if ((HiddenFieldOutcomeMeasures == 1) && (MeasureTypeID == 4)) {
            alert('You should have at least one Outcome Measure. Please select another Measure to remove.');
            return false;
        }
        else {
            if (HiddenFieldTotalMeasures <= 3) {
                alert('You are not allow to remove more Measures. Three is the minimun number of Measures needed for your Improvement Plan.');
                return false;
            }
        }
        return true;
        //alert(MeasureTypeID);
    }
</script>

</asp:Content>

<asp:Content ID="Content3" ContentPlaceHolderID="ContentPlaceHolder1" Runat="Server">
    <asp:HiddenField runat="server" ID="HiddenFieldOutcomeMeasures" />    
    <asp:HiddenField runat="server" ID="HiddenFieldTotalMeasures" />

    
    <asp:Panel ID="PanelInformation" runat="server">
    </asp:Panel>

    <asp:Panel ID="PanelReportCompleteDate" runat="server" Visible="false">
    <p>This report completed on: <asp:Label ID="LabelImprovementPlanCompletedDate" runat="server" /></p>
    </asp:Panel>
    
    <p>
        In an effort to improve the flexibility of the Part IV Improvement in Medical Practice platform process, you have the option to remove improvement measures on this screen that you have determined to be ineffectual towards your improvement in medical practice i.e. wrong measure chosen, finding additional patients for this measure may be difficult, etc.  
    </p>
    <p>
        Click on 'Delete' to remove a measure from your plan.  Please note that you may not remove all measures as at least three measures must be maintained including at least one outcome measure to continue through your improvement phase.
    </p>

    <h3>1. Process and Outcome Measures selected for improvement</h3>

    <table class="table">
        <tr>
            <th>Measure Selected</th> 
            <th>Delete?</th>
            <th>Initial Data</th>
            <th>Peer Data</th>
            <th>Goal</th>
        </tr>
        <asp:ListView runat="server" ID="ListViewMeasureGoals" onitemdatabound="ListViewMeasureGoals_OnItemDataBound">
            <LayoutTemplate>
                <asp:PlaceHolder runat="server" ID="itemPlaceholder"></asp:PlaceHolder>
            </LayoutTemplate>
            <ItemTemplate>
                <tr>
                <td>
                <asp:HiddenField ID="HiddenFieldMeasureID" Value='<%# Eval("MeasureID") %>' runat="server" />
                <asp:HiddenField ID="HiddenFieldMeasureTypeID" Value='<%# Eval("MeasureTypeID") %>' runat="server" />
                <p>
                    
                    <strong><asp:Label ID="LabelMeasureNumber" runat="server"></asp:Label>. <strong><%# Eval("ModuleName")%></strong> - <%# Eval("MeasureQualityIndicator")%></strong>:
                    <%# Eval("MeasureClinicalRecommendation") %> 
                </p>
                </td>
                <td><asp:LinkButton ID="LinkButtonDeleteMeasure" runat="server" OnCommand="LinkButtonDeleteMeasure_Click" CommandArgument='<%# Eval("MeasureID")%>' Text="Delete" CssClass="button"  /></td>
                <td><asp:Label ID="LabelMyPracticeAssessment" runat="server" Text='<%# Eval("MeasurePercentCurrent", "{0:D}%") %>'></asp:Label></td>
                <td><asp:Label ID="LabelMyPeersData" runat="server" Text='<%# Eval("PeerData", "{0:D}%") %>'></asp:Label></td>
                <td>
                <asp:TextBox ID="TextBoxMeasureGoal" runat="server" MaxLength="3" Width="40" Text='<%# Eval("MeasureGoal") %>'></asp:TextBox>
                <asp:Label ID="LabelPercentSign" runat="server" Text="%" />                    
                <asp:RequiredFieldValidator ID="RequiredFieldValidatorMeasureGoal" runat="server" ErrorMessage='<%# Eval("MeasureTitle", "Missing Measure Goal: {0}") %>' Display="Dynamic" 
                    ControlToValidate="TextBoxMeasureGoal" >*</asp:RequiredFieldValidator>
                <asp:RangeValidator ID="RangeValidatorMeasureGoal" runat="server" ErrorMessage='<%# Eval("MeasureTitle", "Measure Goal is not in a valid range: {0}") %>' ControlToValidate="TextBoxMeasureGoal" 
                    MinimumValue="1" MaximumValue="100" Type="Integer" Display="Dynamic" >*</asp:RangeValidator>
                <asp:Label ID="LabelMeasureGoal" runat="server" Text='<%# Eval("MeasureGoal", "{0:D}%") %>' Visible="false"></asp:Label>
                </td>
                </tr>
            </ItemTemplate>
        </asp:ListView>

    </table>

    <div class="button-box">
        <a href="Dashboard.aspx" id="LinkDashboard" runat="server" visible="false" class="button">Back to Status Page</a>
        <asp:LinkButton ID="LinkButtonBackToDashboard" runat="server" Text="Back To Status Page" PostBackUrl="Dashboard.aspx" Visible="true" CssClass="button" />
        <asp:Button ID="ButtonSubmit" runat="server" Text="Submit Final Plan" OnClientClick="return confirm('Do you want to Submit your Improvement Plan?');" onclick="ButtonSubmit_Click" CssClass="button" />
    </div>
    
</asp:Content>

