﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using NetHealthPIMModel;

public partial class copd_Reports : BasePage
{
    class CycleReport
    {
         public int Part1CycleID { get; set; }
         public int Part2CycleID { get; set; }
         public bool ModuleComplete { get; set; }
         public bool Part1ReportApproved { get; set; }
         public bool ImprovementPlanApproved { get; set; }
         public bool Part3ReportApproved { get; set; }
         public bool ImpactStatementApproved { get; set; }
         public DateTime? CycleStartDate { get; set; }
         public DateTime? CycleEndDate { get; set; }
    }

    protected void Page_Load(object sender, EventArgs e)
    {
        if (!IsPostBack)
        {
            InitializePage();
        }
    }

    protected void InitializePage()
    {
        ((PIMMasterPage)Page.Master).SetTitle("Reports");
        int activeCycle = ctx.ActiveModuleCycleID;
        using (NetHealthPIMEntities pim = new NetHealthPIMEntities())
        {
            var pastCycles = from c in pim.ParticipantModuleCycle
                             where c.PrevParticipantModuleCycle.ParticipantModuleCycleID != null && c.Phase2Flag == false
                             orderby c.ParticipantModuleCycleID
                             select new CycleReport
                             {
                                 Part1CycleID = c.ParticipantModuleCycleID,
                                 Part2CycleID = c.PrevParticipantModuleCycle.ParticipantModuleCycleID,
                                 ModuleComplete = c.ModuleComplete, 
                                 Part1ReportApproved = c.PrevParticipantModuleCycle.ReportApproved,
                                 ImprovementPlanApproved = c.PrevParticipantModuleCycle.ImprovementPlanComplete,
                                 Part3ReportApproved = c.ReportApproved,
                                 ImpactStatementApproved = c.ImpactAssessmentComplete, 
                                 CycleStartDate = c.PrevParticipantModuleCycle.CycleStartDate, 
                                 CycleEndDate = c.ModuleCompleteDate
                             };


            ParticipantModuleCycle cycle = pim.ParticipantModuleCycle.First(p => p.ParticipantModuleCycleID == activeCycle);
            bool isPhase2 = cycle.Phase2Flag ?? false;

            CycleReport currentRow = new CycleReport();
            if (isPhase2)
            {
                // This cycle is complete so only show the dates and the links to view the read-only reports. 
                cycle.PrevParticipantModuleCycleReference.Load();
                currentRow.Part1CycleID = activeCycle;
                currentRow.Part2CycleID = cycle.PrevParticipantModuleCycle.ParticipantModuleCycleID;
                currentRow.CycleStartDate = cycle.PrevParticipantModuleCycle.CycleStartDate;
                currentRow.Part1ReportApproved = true;
                currentRow.ImprovementPlanApproved = true;
                currentRow.Part3ReportApproved = cycle.ReportApproved;
                currentRow.ImpactStatementApproved = cycle.ImpactAssessmentComplete;
            }
            else
            {
                // NOTE: In this list the previous and current point to phase 1 and 2 of the module. For this reason, the "current" cycle id is set to 0.
                currentRow.Part1CycleID = 0;
                currentRow.Part2CycleID = activeCycle;
                currentRow.CycleStartDate = cycle.CycleStartDate;
                currentRow.CycleEndDate = null;
                currentRow.Part1ReportApproved = cycle.ReportApproved;
                currentRow.ImprovementPlanApproved = cycle.ImprovementPlanComplete;
                currentRow.Part3ReportApproved = false;
                currentRow.ImpactStatementApproved = false;
            }

            List<CycleReport> cycles = pastCycles.ToList<CycleReport>();
            cycles.Add(currentRow);

            GridViewCycleReports.DataSource = cycles;
            GridViewCycleReports.DataBind();

        }

    }
    protected void GridViewCycleReports_RowDataBound(object sender, GridViewRowEventArgs e)
    {
        if (e.Row.RowType == DataControlRowType.DataRow)
        {
            CycleReport data = (CycleReport)e.Row.DataItem;
            if (data.Part1ReportApproved)
            {
                ((HyperLink)e.Row.Cells[2].FindControl("LinkPart1Report")).NavigateUrl =
                    "~/" + module.ModuleCode.ToLower() + "/PerformanceReport.aspx?" +
                    Constants.QUERYSTRING_ACTION + "=" + Constants.QUERYSTRING_ACTION_VIEW + "&" +
                    Constants.QUERYSTRING_PHASE + "=1&" + Constants.QUERYSTRING_CYCLEID + "=" + data.Part2CycleID.ToString();
            }
            if (data.ImprovementPlanApproved)
            {
                ((HyperLink)e.Row.Cells[2].FindControl("LinkImprovementPlan")).NavigateUrl =
                    "~/" + module.ModuleCode.ToLower() + "/ImprovementPlan.aspx?" +
                    Constants.QUERYSTRING_PHASE + "=1&" + Constants.QUERYSTRING_CYCLEID + "=" + data.Part2CycleID.ToString();
            }
            if (data.Part3ReportApproved)
            {
                ((HyperLink)e.Row.Cells[2].FindControl("LinkPart3Report")).NavigateUrl =
                    "~/" + module.ModuleCode.ToLower() + "/PerformanceReport.aspx?" +
                    Constants.QUERYSTRING_ACTION + "=" + Constants.QUERYSTRING_ACTION_VIEW + "&" +
                    Constants.QUERYSTRING_PHASE + "=2&" + Constants.QUERYSTRING_CYCLEID + "=" + data.Part1CycleID.ToString();
            }
            if (data.ImpactStatementApproved)
            {
                ((HyperLink)e.Row.Cells[2].FindControl("LinkImpactStatement")).NavigateUrl =
                    "~/" + module.ModuleCode.ToLower() + "/ImpactStatement.aspx?" + 
                    Constants.QUERYSTRING_CYCLEID + "=" + data.Part1CycleID.ToString();;
            }
        }
    }
}
