﻿<%@ Page Title="" Language="C#" MasterPageFile="~/PIMMasterPage.master" AutoEventWireup="true" CodeFile="PQRSMoreInfo.aspx.cs" Inherits="abo_PQRSMoreInfo" %>

<asp:Content ID="Content1" ContentPlaceHolderID="head" Runat="Server">
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder1" Runat="Server">
        <%--<div class="heading_box">
            <h2>
                PQRS Reporting Program</h2>
        </div>--%>
    <p>
        The <em>Physician Quality Reporting System (PQRS)</em> is a continuation of the Center for Medicaid and Medicare Services (CMS) PQRS program, initiated in 2007, offered an incentive reimbursement of 2% in 2007 to 2010, 1% in 2011, and 0.5% in 2012 to 2014 (calculated on <strong><em>all</em></strong> annual Medicare claims), the 

program requires the submission of de-identified patient data on specified quality measures.
        <br />
        <br />In the establishment stage since 2007, the incentive premium is available to all eligible professionals reporting, regardless of demonstrated improvement. Since 

2011, reporting 0% on any measure disqualifies participants from obtaining the incentive.
        <br />         <br />In 2015, penalties of -1.5% will be leveraged on eligible professionals with NPI and valid claims for <strong>non-participation</strong> in 

2013.<br />
        <br />In 2016 a -2% "payment adjustment" will be applied to non-participants in 2014.
        <br />In 2017 a -2% "payment adjustment" will be applied to non-participants in 2015.
	<br />
    </p>
    <div class="button-box"><a href="PQRSSELECTION.aspx" class="button">Back</a></div>
</asp:Content>

