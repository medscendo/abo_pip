﻿<%@ Page Title="" Language="C#" MasterPageFile="~/PIMMasterPage.master" AutoEventWireup="true" CodeFile="PerformanceReportUpdate.aspx.cs" Inherits="abo_PerformanceReportUpdate" %>

<%@ Register Assembly="System.Web.DataVisualization, Version=3.5.0.0, Culture=neutral, PublicKeyToken=31bf3856ad364e35"
    Namespace="System.Web.UI.DataVisualization.Charting" TagPrefix="asp" %>

<asp:Content ID="Content1" ContentPlaceHolderID="head" Runat="Server">

<link type="text/css" href="../common/css/atooltip.css" rel="stylesheet"  media="screen" />
<style>

    .table
    {
        box-shadow: none;
    }
    tr.diff-color td, tr.module-name td {
        background-color: #0B91B7;
        color: #fff;
        font-weight:bold;
        border-top:none;
        border-bottom:none;
    }

    tr.module-name td {
        color: #fff;
        /*text-shadow: 1px 1px 0 #fff, -1px 1px 0 #fff,1px -1px 0 #fff,-1px -1px 0 #fff;*/
        font-size:1.2em;
    }

    .width-qualind {
        width: 30.5%;
    }

    .width-rationale {
        width: 9%;
    }

    .width-pracassess {
        width:38%;
    }

    .width-response {
        width: 15%;
        text-align:center;
    }

    #PM1 {
        cursor:pointer;
    }
    #iframeModal {
        width:100%;
        height:100%;
        border:none;
    }
    #measureRationale {
        padding-right:0;
    }

</style>
  <script src="http://code.jquery.com/ui/1.10.3/jquery-ui.js"></script>
    <script type="text/javascript" language="javascript">

        $(document).ready(function () {
            $("#measureRationale").dialog({
                autoOpen: false,
                modal: true,
                height: 350,
                width: 600
            });

            $(function () {
                $('.PeerDistribution').aToolTip({
                    clickIt: true,
                    xOffset: -200,
                    tipContent: 'This answer disribution shows the aggregated responses for all peers answering this system survey question.'
                });
            });
        });

        function openwindowj(measureID) {
            $("#iframeModal").removeAttr("src");
            $("#iframeModal").attr("src", "MeasureRationale.aspx?mid=" + measureID);
            $("#measureRationale").dialog("open");
        }
    </script>

</asp:Content>

<asp:Content runat="server" ID="Content4" ContentPlaceHolderID="javascript">

    <script type="text/javascript" src="../common/js/jquery.atooltip.js"></script>

    <script type="text/javascript">
        $(function () {
            $('#PM1').aToolTip({
                clickIt: true,
                tipContent: '<b>Feedback Report Legend</b><br><br><b>Self</b> = Self-assessment goals<br><b>My Goal</b> = Target that you choose for your improvement<br><b>Initial Data</b> = Calculated measure compiled from initial chart abstractions<br><b>Final Data</b> = Calculated measure compiled from second chart abstraction'
            });
        });
    </script>
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder1" Runat="Server">
    <div runat="server" id="part2">
    <div id='measureRationale'><iframe src="" id="iframeModal"></iframe></div>
        <p>Please review the feedback below and select one to three measures for improvement. It is recommended but not mandatory that you select at least one Outcome Measure.  
            It is also recommended that you target measures that you realistically can improve or measures in which your performance is below that of your peer group.</p>
    <p>After you select your measures and complete your improvement plan, there will be a set period of time during which you will implement your improvement plan. 
        This is called the improvement period.  Your improvement period determined based upon the activity(s) you select.  <b>If you select measures from multiple activities, 
        the improvement period is always based upon on the activity with the longest improvement period.</b></p>
    <p>To complete the improvement in medical practice activity, you are required to review 10 additional patient charts for every activity that you included in your improvement plan.  
        Therefore, if you select measures from multiple activities, you will be required to review 10 patient charts for each. For example, if you select one measure from one activity, 
        then you will review 10 patient charts as the final step of this activity; if you select one or more measures from two different activities, 
        you will review 20 patient charts as the final step of this activity. This portion of the activity cannot be started until the end of the improvement period. </p>
</div>
    <div id="part1" runat="server">
<p>The report below represents the data obtained from your self-assessment, system survey, and patient chart data. 
</p>
<ul>
    <li><strong>Informational Measure</strong> - Informational measures provide feedback on the overall data about your patients. Informational measures cannot be selected for improvement.</li>
    <li><strong>Process Measure</strong> - Process measures assess the processes of care used to treat patients and may affect the  resulting outcomes.</li>
    <li><strong>Outcome Measure</strong> - Outcome measures provide insight into a physician's treatment patterns for the purposes of evaluating  the impact on patient care.</li>
    </ul>
    </div>
<asp:label ID="PQRSNote" text="If you have chosen to report for PQRS using the data from this program, click <a href='PQRSNextStep.aspx'>here</a>.<br /><br />" runat="server" />

<asp:label ID="SelectMeasuresText" runat="server" Text="" Visible="false" style="font-weight:bold;" />


    <asp:Panel ID="PanelContent" runat="server">

    </asp:Panel>



    <div class="button-box">
    <asp:LinkButton ID="LinkButtonDashboard" runat="server" Text="Return To Status Page"  PostBackUrl="Dashboard.aspx" CssClass="button" />
    <asp:LinkButton ID="LinkSelectMeasures" runat="server" Text="Proceed to Improvement Plan"  OnClick="LinkButtonSelectMeasures_Click" CssClass="button" />
    <asp:LinkButton ID="LinkButtonImpactStatement" runat="server" Text="Proceed to Impact Statement"  OnClick="LinkButtonImpactStatement_Click" Visible="false" CssClass="button" />
    <asp:LinkButton ID="LinkButtonNext" runat="server" Text="Next"   Visible="false" CssClass="button" OnClick="LinkButton1_Click" />
</div>

</asp:Content>


