﻿<%@ Page Title="" Language="C#" MasterPageFile="~/PIMMasterPage.master" AutoEventWireup="true" CodeFile="ChartAbstraction.aspx.cs" Inherits="abo_ChartAbstraction" %>
<%@   Register Assembly="AjaxControlToolkit" Namespace="AjaxControlToolkit"  TagPrefix="asp"%>

<asp:Content ID="Content1" ContentPlaceHolderID="head" Runat="Server">
</asp:Content>

<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder1" Runat="Server">
    <link type="text/css" href="../common/css/atooltip.css" rel="stylesheet"  media="screen" />
    	<script type="text/javascript" src="../common/js/jquery.atooltip.js"></script>
    <script type="text/javascript" src="../common/js/jquery.atooltip2.js"></script>
     <style>
.httable td.cellquest {
    width: 67%;
    font-weight: bold;
}
</style>
<script language="JavaScript" type="text/javascript">

function DisableTheSubmitButton() {

    $('#ButtonSubmit').hide();
    $('#submiting').show();
}


$(document).ready(function () {

    //Javascript for Standard PIM Esotropia
    $("#align171orth").click(function () {
        if ($("#align171orth").is(':checked')) {
            $("#align183orth").prop("checked", true);
        }
        else {
            $("#align183orth").prop("checked", false);
        }
    });
    $('#alighn171pd').on('keyup', function () {
        $('#alighn183pd').val($(this).val());
    });
    $('#alighn171pdht').on('keyup', function () {
        $('#alighn183pdht').val($(this).val());
    });

    $("#alighn171etxt_0").click(function () {
        if ($("#alighn171etxt_0").is(':checked')) {
            $("#alighn183etxt_0").prop("checked", true);
        }
        else {
            $("#alighn183etxt_0").prop("checked", false);
        }
    });
    $("#alighn171etxt_1").click(function () {
        if ($("#alighn171etxt_1").is(':checked')) {
            $("#alighn183etxt_1").prop("checked", true);
        }
        else {
            $("#alighn183etxt_1").prop("checked", false);
        }
    })

    //Javascript for Simplified PIM Blepharoptosis
    //What is the presumed etiology of the ptosis?
    $(".Q-671").click(function () {
        if (($(".Q-671:input:checked").val() == "True"))
        {
            $("#ctl00_ContentPlaceHolder1_yesno672_1").prop("checked", true);
            $("#ctl00_ContentPlaceHolder1_yesno673_1").prop("checked", true);
            $("#ctl00_ContentPlaceHolder1_yesno674_1").prop("checked", true);
        }
    })
    $(".Q-672").click(function () {
        if (($(".Q-672:input:checked").val() == "True")) {
            $("#ctl00_ContentPlaceHolder1_yesno671_1").prop("checked", true);
            $("#ctl00_ContentPlaceHolder1_yesno673_1").prop("checked", true);
            $("#ctl00_ContentPlaceHolder1_yesno674_1").prop("checked", true);
        }
    })
    $(".Q-673").click(function () {
        if (($(".Q-673:input:checked").val() == "True")) {
            $("#ctl00_ContentPlaceHolder1_yesno671_1").prop("checked", true);
            $("#ctl00_ContentPlaceHolder1_yesno672_1").prop("checked", true);
            $("#ctl00_ContentPlaceHolder1_yesno674_1").prop("checked", true);
        }
    })
    $(".Q-674").click(function () {
        if (($(".Q-674:input:checked").val() == "True")) {
            $("#ctl00_ContentPlaceHolder1_yesno671_1").prop("checked", true);
            $("#ctl00_ContentPlaceHolder1_yesno672_1").prop("checked", true);
            $("#ctl00_ContentPlaceHolder1_yesno673_1").prop("checked", true);
        }
    })

    //What procedure was performed?
    $(".Q-675").click(function () {
        if (($(".Q-675:input:checked").val() == "True")) {
            $("#ctl00_ContentPlaceHolder1_yesno676_1").prop("checked", true);
            $("#ctl00_ContentPlaceHolder1_yesno677_1").prop("checked", true);
            $("#ctl00_ContentPlaceHolder1_yesno678_1").prop("checked", true);
        }
    })
    $(".Q-676").click(function () {
        if (($(".Q-676:input:checked").val() == "True")) {
            $("#ctl00_ContentPlaceHolder1_yesno675_1").prop("checked", true);
            $("#ctl00_ContentPlaceHolder1_yesno677_1").prop("checked", true);
            $("#ctl00_ContentPlaceHolder1_yesno678_1").prop("checked", true);
        }
    })
    $(".Q-677").click(function () {
        if (($(".Q-677:input:checked").val() == "True")) {
            $("#ctl00_ContentPlaceHolder1_yesno675_1").prop("checked", true);
            $("#ctl00_ContentPlaceHolder1_yesno676_1").prop("checked", true);
            $("#ctl00_ContentPlaceHolder1_yesno678_1").prop("checked", true);
        }
    })
    $(".Q-678").click(function () {
        if (($(".Q-678:input:checked").val() == "True")) {
            $("#ctl00_ContentPlaceHolder1_yesno675_1").prop("checked", true);
            $("#ctl00_ContentPlaceHolder1_yesno676_1").prop("checked", true);
            $("#ctl00_ContentPlaceHolder1_yesno677_1").prop("checked", true);
        }
    })

});


</script>
    <h2><asp:Label ID="LabelChartName" runat="server" Text=""></asp:Label></h2>
    <h3><asp:Label ID="LabelChartDefinition" runat="server" Text=""></asp:Label></h3>
    <h3><asp:Label ID="LabelPatientID" runat="server" Text=""></asp:Label></h3>
    <asp:Panel ID="PanelContent" runat="server"></asp:Panel>
    
    <div class="button-box" style="text-align: left;">
        <asp:Button ID="ButtonSubmit" runat="server" Text="Submit" ClientIDMode="Static" OnClientClick="DisableTheSubmitButton();" OnClick="ButtonSubmit_Click" />
        <img id="submiting" style="display:none" src="../common/images/submitting.gif" />
        <asp:LinkButton ID="LinkButtonReturn" style="margin-left:40px" runat="server" OnClientClick="return confirm('Warning: Selecting OK will lose any changes you have made to this chart.  Select Cancel to return to the chart and save any changes you have made.');" OnClick="LinkButtonReturn_Click">Return To Dashboard</asp:LinkButton>
    </div>

</asp:Content>

<asp:Content ID="Content3" ContentPlaceHolderID="javascript" Runat="Server">
</asp:Content>


