﻿<%@ Page Title="" Language="C#" MasterPageFile="~/PIMMasterPage.master" AutoEventWireup="true" CodeFile="SiteMap.aspx.cs" Inherits="abo_SiteMap" %>

<asp:Content ID="Content1" ContentPlaceHolderID="head" Runat="Server">
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder1" Runat="Server">
         <!-- module detaile -->
            <div class="main_content" style="padding-right: 100px;">
            <div class="heading_box" >
              <h2><strong>Site Map</strong></h2>
            </div>
            <div class="imporvenment_plan_details">
              <div class="top_details">
              <p>&nbsp;</p>
                  <p class="MsoNormal">
                      <b style="mso-bidi-font-weight:normal">
                      <span style="font-family:&quot;Calibri&quot;,&quot;sans-serif&quot;;mso-bidi-font-family:&quot;Times New Roman&quot;">
                      Participant Profile<o:p></o:p></span></b></p>
                  <p class="MsoNormal">
                      <b style="mso-bidi-font-weight:normal">
                      <span style="font-family:&quot;Calibri&quot;,&quot;sans-serif&quot;;mso-bidi-font-family:&quot;Times New Roman&quot;">
                      Improvement in Medical Practice Activity Tutorial<o:p></o:p></span></b></p>
                  <p class="MsoNormal">
                      <b style="mso-bidi-font-weight:normal">
                      <span style="font-family:&quot;Calibri&quot;,&quot;sans-serif&quot;;mso-bidi-font-family:&quot;Times New Roman&quot;">
                      Status </span></b>
                      <span style="font-family:&quot;Calibri&quot;,&quot;sans-serif&quot;;mso-bidi-font-family:
&quot;Times New Roman&quot;">(Main Page)<b style="mso-bidi-font-weight:normal"><o:p></o:p></b></span></p>
                  <p class="MsoNormal">
                      <b style="mso-bidi-font-weight:normal">
                      <span style="font-family:&quot;Calibri&quot;,&quot;sans-serif&quot;;mso-bidi-font-family:&quot;Times New Roman&quot;">
                      <span style="mso-tab-count:1">&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
                      </span></span></b>
                      <span style="font-family:
&quot;Calibri&quot;,&quot;sans-serif&quot;;mso-bidi-font-family:&quot;Times New Roman&quot;">(Links to 
                      chosen Improvement in Medical Practice Activity Status Page)<o:p></o:p></span></p>
                  <p class="MsoNormal">
                      <b style="mso-bidi-font-weight:normal">
                      <span style="font-family:&quot;Calibri&quot;,&quot;sans-serif&quot;;mso-bidi-font-family:&quot;Times New Roman&quot;">
                      <span style="mso-tab-count:1">&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
                      </span>View Improvement in Medical Practice Activity Tutorial<o:p></o:p></span></b></p>
                  <p class="MsoNormal">
                      <b style="mso-bidi-font-weight:normal">
                      <span style="font-family:&quot;Calibri&quot;,&quot;sans-serif&quot;;mso-bidi-font-family:&quot;Times New Roman&quot;">
                      <span style="mso-tab-count:1">&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
                      </span>View Activity Process<o:p></o:p></span></b></p>
                      <p>&nbsp;</p>
                  <p class="MsoNormal">
                      <b style="mso-bidi-font-weight:normal">
                      <span style="font-family:&quot;Calibri&quot;,&quot;sans-serif&quot;;mso-bidi-font-family:&quot;Times New Roman&quot;">
                      <span style="mso-tab-count:1">&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
                      </span></span></b>
                      <span style="font-family:
&quot;Calibri&quot;,&quot;sans-serif&quot;;mso-bidi-font-family:&quot;Times New Roman&quot;">(Main 
                      Page: Activity Selection &amp; Self-Assessment)<o:p></o:p></span></p>
                  <p class="MsoNormal">
                      <b style="mso-bidi-font-weight:normal">
                      <span style="font-family:&quot;Calibri&quot;,&quot;sans-serif&quot;;mso-bidi-font-family:&quot;Times New Roman&quot;">
                      <span style="mso-tab-count:1">&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
                      </span>Select Your Activities<o:p></o:p></span></b></p>
                  <p class="MsoNormal">
                      <b style="mso-bidi-font-weight:normal">
                      <span style="font-family:&quot;Calibri&quot;,&quot;sans-serif&quot;;mso-bidi-font-family:&quot;Times New Roman&quot;">
                      <span style="mso-tab-count:2">&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
                      </span>Activity Library<o:p></o:p></span></b></p>
                  <p class="MsoNormal">
                      <b style="mso-bidi-font-weight:normal">
                      <span style="font-family:&quot;Calibri&quot;,&quot;sans-serif&quot;;mso-bidi-font-family:&quot;Times New Roman&quot;">
                      <span style="mso-tab-count:3">&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
                      </span></span></b>
                      <span style="font-family:&quot;Calibri&quot;,&quot;sans-serif&quot;;mso-bidi-font-family:&quot;Times New Roman&quot;">
                      (for each Improvement in Medical Practice Activity/diagnoses)<o:p></o:p></span></p>
                  <p class="MsoNormal">
                      <b style="mso-bidi-font-weight:normal">
                      <span style="font-family:&quot;Calibri&quot;,&quot;sans-serif&quot;;mso-bidi-font-family:&quot;Times New Roman&quot;">
                      <span style="mso-tab-count:3">&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
                      </span>Activity Overview<o:p></o:p></span></b></p>
                  <p class="MsoNormal">
                      <b style="mso-bidi-font-weight:normal">
                      <span style="font-family:&quot;Calibri&quot;,&quot;sans-serif&quot;;mso-bidi-font-family:&quot;Times New Roman&quot;">
                      <span style="mso-tab-count:3">&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
                      </span>View Chart Abstraction Detail<o:p></o:p></span></b></p>
                  <p class="MsoNormal">
                      <b style="mso-bidi-font-weight:normal">
                      <span style="font-family:&quot;Calibri&quot;,&quot;sans-serif&quot;;mso-bidi-font-family:&quot;Times New Roman&quot;">
                      <span style="mso-tab-count:3">&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
                      </span>View Peer Data<o:p></o:p></span></b></p>
                  <p class="MsoNormal">
                      <b style="mso-bidi-font-weight:normal">
                      <span style="font-family:&quot;Calibri&quot;,&quot;sans-serif&quot;;mso-bidi-font-family:&quot;Times New Roman&quot;">
                      <span style="mso-tab-count:3">&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
                      </span>View Resources<o:p></o:p></span></b></p>
                  <p class="MsoNormal">
                      <b style="mso-bidi-font-weight:normal">
                      <span style="font-family:&quot;Calibri&quot;,&quot;sans-serif&quot;;mso-bidi-font-family:&quot;Times New Roman&quot;">
                      <span style="mso-tab-count:3">&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
                      </span>Patient Chart Registration Form<o:p></o:p></span></b></p>
                  <p class="MsoNormal">
                      <b style="mso-bidi-font-weight:normal">
                      <span style="font-family:&quot;Calibri&quot;,&quot;sans-serif&quot;;mso-bidi-font-family:&quot;Times New Roman&quot;">
                      <span style="mso-tab-count:2">&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
                      </span>Review Your Practice<o:p></o:p></span></b></p>
                  <p class="MsoNormal">
                      <b style="mso-bidi-font-weight:normal">
                      <span style="font-family:&quot;Calibri&quot;,&quot;sans-serif&quot;;mso-bidi-font-family:&quot;Times New Roman&quot;">
                      <span style="mso-tab-count:2">&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
                      </span>Abstract your Charts<o:p></o:p></span></b></p>
                  <p class="MsoNormal">
                      <b style="mso-bidi-font-weight:normal">
                      <span style="font-family:&quot;Calibri&quot;,&quot;sans-serif&quot;;mso-bidi-font-family:&quot;Times New Roman&quot;">
                      <span style="mso-tab-count:3">&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
                      </span>Patient Chart Registration Form<o:p></o:p></span></b></p>
                  <p class="MsoNormal">
                      <b style="mso-bidi-font-weight:normal">
                      <span style="font-family:&quot;Calibri&quot;,&quot;sans-serif&quot;;mso-bidi-font-family:&quot;Times New Roman&quot;">
                      <span style="mso-tab-count:3">&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
                      </span>Improvement in Medical Practice Activity Status Page<o:p></o:p></span></b></p>
                  <p class="MsoNormal">
                      <b style="mso-bidi-font-weight:normal">
                      <span style="font-family:&quot;Calibri&quot;,&quot;sans-serif&quot;;mso-bidi-font-family:&quot;Times New Roman&quot;">
                      <span style="mso-tab-count:4">&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
                      </span></span></b>
                      <span style="font-family:&quot;Calibri&quot;,&quot;sans-serif&quot;;mso-bidi-font-family:&quot;Times New Roman&quot;">
                      (Data Collection)<o:p></o:p></span></p>
                  <p class="MsoNormal">
                      <b style="mso-bidi-font-weight:normal">
                      <span style="font-family:&quot;Calibri&quot;,&quot;sans-serif&quot;;mso-bidi-font-family:&quot;Times New Roman&quot;">
                      <span style="mso-tab-count:2">&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
                      </span><span style="mso-tab-count:
2">&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
                      </span>Self-Assessment<o:p></o:p></span></b></p>
                  <p class="MsoNormal">
                      <b style="mso-bidi-font-weight:normal">
                      <span style="font-family:&quot;Calibri&quot;,&quot;sans-serif&quot;;mso-bidi-font-family:&quot;Times New Roman&quot;">
                      <span style="mso-tab-count:4">&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
                      </span>System Survey<o:p></o:p></span></b></p>
                  <p class="MsoNormal">
                      <b style="mso-bidi-font-weight:normal">
                      <span style="font-family:&quot;Calibri&quot;,&quot;sans-serif&quot;;mso-bidi-font-family:&quot;Times New Roman&quot;">
                      <span style="mso-tab-count:4">&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
                      </span>Chart Abstraction<o:p></o:p></span></b></p>
                 <p class="MsoNormal">
                      <span style="font-family:&quot;Calibri&quot;,&quot;sans-serif&quot;;mso-bidi-font-family:&quot;Times New Roman&quot;">
                      <b style="mso-bidi-font-weight:normal">
                      <span style="mso-tab-count:4">&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
                      </span>Patient Surveys </b>(*to be 
                      developed)<o:p></o:p></span></p>
                  <p class="MsoNormal">
                      <o:p></o:p>
                  </p>
                  <p class="MsoNormal">&nbsp;
                      <o:p></o:p></p>
                  <p class="MsoNormal">
                      <span style="font-family:&quot;Calibri&quot;,&quot;sans-serif&quot;;mso-bidi-font-family:
&quot;Times New Roman&quot;"><span style="mso-tab-count:1">&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
                      </span>(Main Page: Review &amp; Planning) / (Status Page: Improvement Plan)<o:p></o:p></span></p>
                  <p class="MsoNormal">
                      <b style="mso-bidi-font-weight:normal">
                      <span style="font-family:&quot;Calibri&quot;,&quot;sans-serif&quot;;mso-bidi-font-family:&quot;Times New Roman&quot;">
                      <span style="mso-tab-count:2">&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
                      </span>Review Your Results<span style="mso-tab-count:3">&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
                      </span>Review Performance Report<o:p></o:p></span></b></p>
                  <p class="MsoNormal">
                      <b style="mso-bidi-font-weight:normal">
                      <span style="font-family:&quot;Calibri&quot;,&quot;sans-serif&quot;;mso-bidi-font-family:&quot;Times New Roman&quot;">
                      <span style="mso-tab-count:2">&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
                      </span>Develop Plan for Improvement<span style="mso-tab-count:1">&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;</span>Develop Improvement Plan<o:p></o:p></span></b></p>
                  <p class="MsoNormal">
                      <b style="mso-bidi-font-weight:normal">
                      <span style="font-family:&quot;Calibri&quot;,&quot;sans-serif&quot;;mso-bidi-font-family:&quot;Times New Roman&quot;">
                      <span style="mso-tab-count:8">&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;</span>Improvement Resources and Quality Tools </span></b>
                      </p>
                  <p class="MsoNormal">
                      <b style="mso-bidi-font-weight:normal">
                      <span style="font-family:&quot;Calibri&quot;,&quot;sans-serif&quot;;mso-bidi-font-family:&quot;Times New Roman&quot;">
                      <span style="mso-tab-count:1">&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
                      </span></span></b>
                  </p>
<p class="MsoNormal">
                      <span style="font-family:&quot;Calibri&quot;,&quot;sans-serif&quot;;mso-bidi-font-family:
&quot;Times New Roman&quot;"><span style="mso-tab-count:1">&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
                      </span>(Main Page: Analysis &amp; Impact) / (Dashboard: Remeasurement)<o:p></o:p></span></p>
                  <p class="MsoNormal">
                      <b style="mso-bidi-font-weight:normal">
                      <span style="font-family:&quot;Calibri&quot;,&quot;sans-serif&quot;;mso-bidi-font-family:&quot;Times New Roman&quot;">
                      <span style="mso-tab-count:2">&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
                      </span>Abstract Second Sample of Charts <span style="mso-tab-count:1">&nbsp;&nbsp;
                      </span></span></b>
                  </p>
                  <p class="MsoNormal">
                      <b style="mso-bidi-font-weight:normal">
                      <span style="font-family:&quot;Calibri&quot;,&quot;sans-serif&quot;;mso-bidi-font-family:&quot;Times New Roman&quot;">
                      <span style="mso-tab-count:2">&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
                      </span>Self-Assessment<o:p></o:p></span></b></p>
                  <p class="MsoNormal">
                      <b style="mso-bidi-font-weight:normal">
                      <span style="font-family:&quot;Calibri&quot;,&quot;sans-serif&quot;;mso-bidi-font-family:&quot;Times New Roman&quot;">
                      <span style="mso-tab-count:2">&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
                      </span>System Survey<o:p></o:p></span></b></p>
                  <p class="MsoNormal">
                      <b style="mso-bidi-font-weight:normal">
                      <span style="font-family:&quot;Calibri&quot;,&quot;sans-serif&quot;;mso-bidi-font-family:&quot;Times New Roman&quot;">
                      <span style="mso-tab-count:2">&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
                      </span>Chart Abstraction<o:p></o:p></span></b></p>
                  <p class="MsoNormal">
                      <b style="mso-bidi-font-weight:normal">
                      <span style="font-family:&quot;Calibri&quot;,&quot;sans-serif&quot;;mso-bidi-font-family:&quot;Times New Roman&quot;">
                      <span style="mso-tab-count:2">&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
                      </span>Compare Performance<span style="mso-tab-count:2">&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
                      </span>Review Performance Report<o:p></o:p></span></b></p>
                  <p class="MsoNormal">
                      <b style="mso-bidi-font-weight:normal">
                      <span style="font-family:&quot;Calibri&quot;,&quot;sans-serif&quot;;mso-bidi-font-family:&quot;Times New Roman&quot;">
                      <span style="mso-tab-count:2">&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
                      </span>Analyze Impact<span style="mso-tab-count:3">&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
                      </span>Impact Statement<o:p></o:p></span></b></p>
                  <p class="MsoNormal">
                      <b style="mso-bidi-font-weight:normal">
                      <span style="font-family:&quot;Calibri&quot;,&quot;sans-serif&quot;;mso-bidi-font-family:&quot;Times New Roman&quot;">
                      <span style="mso-tab-count:1">&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
                      </span><o:p></o:p></span></b>
                  </p>
                  <p class="MsoNormal">
                      <b style="mso-bidi-font-weight:normal">
                      <span style="font-family:&quot;Calibri&quot;,&quot;sans-serif&quot;;mso-bidi-font-family:&quot;Times New Roman&quot;">
                      Support<o:p></o:p></span></b></p>
                  <p class="MsoNormal">
                      <b style="mso-bidi-font-weight:normal">
                      <span style="font-family:&quot;Calibri&quot;,&quot;sans-serif&quot;;mso-bidi-font-family:&quot;Times New Roman&quot;">
                      Contact Us<span style="mso-tab-count:1">&nbsp;&nbsp;&nbsp;&nbsp;&nbsp; </span><o:p></o:p>
                      </span></b>
                  </p>
                  <p class="MsoNormal">
                      <b style="mso-bidi-font-weight:normal">
                      <span style="font-family:&quot;Calibri&quot;,&quot;sans-serif&quot;;mso-bidi-font-family:&quot;Times New Roman&quot;">
                      My Account<o:p></o:p></span></b></p>
              </div>
              <div class="bginput"><asp:LinkButton ID="ButtonSubmit" runat="server" Text=" Back " PostBackUrl="Dashboard.aspx"/></div>
              </div>
            </div>
          <!-- module detaile ends --> 
</asp:Content>

