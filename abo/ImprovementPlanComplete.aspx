<%@ Page Title="" Language="C#" MasterPageFile="~/PIMMasterPage.master" AutoEventWireup="true" CodeFile="ImprovementPlanComplete.aspx.cs" Inherits="copd_ImprovementPlanComplete" %>

<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="Server">

<style>
    .action-box {
        -moz-box-sizing: border-box;
        -webkit-box-sizing: border-box;
        box-sizing: border-box;
        float:left;
        width: 100%;
        text-align:left;
    }

    @media print {
      .action-box { 
          font-family: "Palatino Linotype", "Book Antiqua", Palatino, serif !important;
          line-height:1.6 !important;
      }
    }
</style>

</asp:Content>
<asp:Content ID="Content3" ContentPlaceHolderID="ContentPlaceHolder1" runat="Server">
    <div class="action-box" runat="server" id="divImprovementPlanComplete" visible="false">
        <h2>Congratulations</h2>
	    <p>You have completed your Improvement Plan.</p>
        <p>
            This phase of your activity starts on <strong><asp:Label runat="server" ID="LabelTodaysDate">11/26/2013</asp:Label></strong>.
            It will last until <strong><asp:Label runat="server" ID="LabelNextCycleDate1">1/26/2013</asp:Label></strong> based upon the activities you have chosen to work on:
            <br />
            <asp:Repeater runat="server" ID="RepeaterModules">
            <ItemTemplate>
            <br /><strong><asp:Label runat="server" ID="module1"  Text='<%# Bind("ModuleName") %>'></asp:Label></strong>
            </ItemTemplate>
            </asp:Repeater>
        </p>
        <p>
            NOTE: Your Improvement Phase period is calculated based upon the time needed for the longest measure in your selected activity(s) plus one month. Your activity will re-open on <strong><asp:Label runat="server" ID="LabelNextCycleDate2">1/26/2014</asp:Label></strong>. At that time, you may begin entering charts for your second abstraction.
        </p>
        <p>
            The Part IV Improvement in Medical Practice platform utilizes a follow-up timeframe requirement for each improvement in medical practice activity during the improvement plan phase. These follow-up timeframes are recommended by ABO content experts and ensure that the process and outcome measures selected are afforded the appropriate amount of follow-up prior to re-abstraction and re-measurement. The Part IV Improvement in Medical Practice platform validates the recommended follow-up timeframe and prevents re-abstraction and re-measurement until the follow-up timeframe is satisfied. Please note: when you select a Improvement in Medical Practice Activity where the longest measure has a 6 month follow-up measure you will be required to wait a minimum of 6 months before re-measurement.
        </p>
        <p>
            <strong>What should I focus on during the recommended follow-up timeframe period?</strong>
            <br />During the follow-up timeframe phase you are encouraged to evaluate the process and outcome measures you have chosen and begin to consider systems, tools and strategies that could be implemented within your practice to improve your systems, practice patterns and patient outcomes. It is also recommended that you begin to identify patients that meet the requirements of the process or outcome measures that you hope to evaluate to ensure that you have enough patient charts to accurately re-evaluate processes of care.
        </p>
        <p>
            You may come back to your activity at any time to review your reports and plan.
        </p>
        <p>
            To review your plan <span id="spanresources" runat="server"> and access any resources selected</span> , <asp:HyperLink runat="server" NavigateUrl="ImprovementPlanPartI.aspx">CLICK HERE</asp:HyperLink>
        </p>
    </div>
    <div class="action-box" runat="server" id="divImprovementPlanPhase" visible="false">
        <h2>Improvement Phase</h2>
        <p>Now that you have developed a plan of improvement, you have until <strong><asp:Label runat="server" ID="LabelNextCycleDate3">6/17/2016</asp:Label></strong> to implement your improvement strategy.  Once this timeframe is complete you will be alerted that it is time to evaluate your results by abstracting an additional ten charts and comparing the results with your initial chart abstraction.  At that time, you may begin entering charts for your second abstraction.</p>
        <%-- JH: Per Beth Ann's comments, I've added the shorter paragraph above with the hard-coded date.  It should be changed if need be.  3/31/16
	    <p>You have completed your Assessment and Improvement Plan.
            <br />You are now in your Improvement Phase.
        </p>
        <p>
            This phase of your activity started on <strong><asp:Label runat="server" ID="LabelImprovementPlanCompleteDate">11/26/2013</asp:Label></strong>
            (the date you submitted your Improvement Plan) and will end on <strong><asp:Label runat="server" ID="LabelNextCycleDate3">1/26/2013</asp:Label></strong>.
            <br />At that time, you may begin entering charts for your second abstraction.<br />
            <br />NOTE: Your Improvement Phase period is calculated based upon the time needed for the longest measure in your selected activity(s) plus one month.
        </p>--%>
        <p>
            The Part IV Improvement in Medical Practice platform utilizes a follow-up timeframe requirement for each improvement in medical practice activity during the improvement plan phase.  These follow-up timeframes are recommended by ABO content experts and ensure that the process and outcome measures selected are afforded the appropriate amount of follow-up prior to re-abstraction and re-measurement.  The Part IV Improvement in Medical Practice platform validates the recommended follow-up timeframe and prevents re-abstraction and re-measurement until the follow-up timeframe is satisfied.  Please note: when you select a Improvement in Medical Practice Activity where the longest measure has a 6 month follow-up measure you will be required to wait a minimum of 6 months before re-measurement.
        </p>
        <p>
            <strong>What should I focus on during the recommended follow-up timeframe period?</strong>
            <br />During the follow-up timeframe phase you are encouraged to evaluate the process and outcome measures you have chosen and begin to consider systems, tools and strategies that could be implemented within your practice to improve your systems, practice patterns and patient outcomes.  It is also recommended that you begin to identify patients that meet the requirements of the process or outcome measures that you hope to evaluate to ensure that you have enough patient charts to accurately re-evaluate processes of care.
        </p>
        <p>
            To review your plan and access any resources selected, <asp:HyperLink ID="HyperLink1" runat="server" NavigateUrl="ImprovementPlanPartI.aspx">CLICK HERE</asp:HyperLink>
        </p>
    </div>
</asp:Content>