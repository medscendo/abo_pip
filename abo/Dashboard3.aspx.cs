﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using NetHealthPIMModel;
using System.Transactions;
using System.Web.UI.HtmlControls;

public partial class copd_Dashboard3 : BasePage
{
    bool isModuleCompleted = true;
    protected void Page_Load(object sender, EventArgs e)
    {
        if (!IsPostBack)
        {
            if (Request.QueryString["is"] == "0")
            {
                string script = " alert('Impact Statement can not be completed at this time until all charts for all activities have been submitted.');";
                ScriptManager.RegisterStartupScript(this, GetType(),
                              "ServerControlScript", script, true);
            }
            InitializePage();
        }
    }

    protected void InitializePage()
    {
        string breadCrumb = Constants.BC_DASHBOARD;
        ((PIMMasterPage)Page.Master).SetTitle(breadCrumb);
        ((PIMMasterPage)Page.Master).SetStatus(3);
        ((PIMMasterPage)Page.Master).SetHeader("Analysis &amp; Impact: Status Page");
        using (NetHealthPIMEntities pim = new NetHealthPIMEntities())
        {
            int PlanProgressPct = 0;
            int cycleID = ctx.ActiveModuleCycleID;
            ParticipantModuleCycle cycle = pim.ParticipantModuleCycle.First(c => c.ParticipantModuleCycleID == cycleID);
            if (cycle.CycleNumber == 1)
            {
                if (Request.UrlReferrer == null)
                    Response.Redirect("ModuleSelectionReview.aspx");
                Response.Redirect(Request.UrlReferrer.ToString());
            }


            var participantDashboardModules = (from pdm in pim.ParticipantDashboardModules_V
                                               where pdm.ParticipantModuleCycleID == cycle.ParticipantModuleCycleID
                                               select new ParticipantDashboardModules
                                               {
                                                   ModuleID = pdm.ModuleID,
                                                   ParticipantModuleCycleID = pdm.ParticipantModuleCycleID,
                                                   ModuleShortName = pdm.ModuleShortName,
                                                   SelfAssessmentSurveyTargetDate = pdm.SelfAssessmentSurveyTargetDate,
                                                   SelfAssessmentSurveyCompletedDate = pdm.SelfAssessmentSurveyCompletedDate,
                                                   SelfAssessmentSurveyComplete = pdm.SelfAssessmentSurveyComplete,
                                                   SystemSurveyTargetDate = pdm.SystemSurveyTargetDate,
                                                   SystemSurveyCompletedDate = pdm.SystemSurveyCompletedDate,
                                                   SystemSurveyComplete = pdm.SystemSurveyComplete,
                                                   AbstractDataTargetDate = pdm.AbstractDataTargetDate,
                                                   AbstractDataCompletedDate = pdm.AbstractDataCompletedDate,
                                                   AbstractDataComplete = pdm.AbstractDataComplete,
                                                   ReportApproved = pdm.ReportApproved,
                                                   ReportApprovedTargetDate = pdm.ReportApprovedTargetDate,
                                                   ReportApprovedCompletedDate = pdm.ReportApprovedCompletedDate,
                                                   ImprovementPlanComplete = pdm.ImprovementPlanComplete,
                                                   ImprovementPlanTargetDate = pdm.ImprovementPlanTargetDate,
                                                   ImprovementPlanCompletedDate = pdm.ImprovementPlanCompletedDate,
                                                   PatientChartRegistrationCompleted = (int)pdm.PatientChartRegistrationCompleted,
                                                   TotalCharts = (int)pdm.TotalCharts,
                                                   ChartsCompleted = (int)pdm.ChartsCompleted
                                               });
            RepeaterModules.DataSource = participantDashboardModules;
            RepeaterModules.DataBind();
            int numberOfModules = participantDashboardModules.Count();
            int modulesCompleted = 0;
            int systemSurveyCompleted = 0;
            int selfAssessmentCompleted = 0; foreach (ParticipantDashboardModules pdm in participantDashboardModules)
            {
                if (pdm.AbstractDataComplete == true)
                    modulesCompleted++;
                if (pdm.SelfAssessmentSurveyComplete == true)
                    selfAssessmentCompleted++;
                if (pdm.SystemSurveyComplete == true)
                    systemSurveyCompleted++;
            }

            PlanProgressPct = (int)((Convert.ToDouble(selfAssessmentCompleted) / Convert.ToDouble(numberOfModules)) * 10);
            PlanProgressPct = PlanProgressPct + (int)((Convert.ToDouble(systemSurveyCompleted) / Convert.ToDouble(numberOfModules)) * 10);
            PlanProgressPct = PlanProgressPct + (int)((Convert.ToDouble(modulesCompleted) / Convert.ToDouble(numberOfModules)) * 40);

            PECSurvey.Visible = (cycle.PECCurrentID > 0);
            if (cycle.PECCurrentID > 0)
            {
                Participant participant = pim.Participant.First(p => p.ParticipantID == ctx.ParticipantID);
                var pecSurveysPerCandidate = (from ps in pim.PECSurveysPerCandidate_V
                                              where ps.CandidateID == participant.CandidateID
                                              && ps.ParticipantEntryID == cycle.PECCurrentID
                                              orderby ps.ParticipantCycleID descending
                                              select ps).First();
                LabelPatientSurvey.Text = pecSurveysPerCandidate.SurveyCount.ToString();
                if (pecSurveysPerCandidate.SurveyCount >= 45)
                    PlanProgressPct = PlanProgressPct + 10;
                string PECURL = System.Configuration.ConfigurationManager.AppSettings["PECURL"];
                var pecParticipantInfo = (from pmc in pim.ParticipantModuleCycle
                                          join pc in pim.ParticipantCycle on pmc.PECCurrentID equals pc.ParticipantEntryID
                                          join p in pim.Participant on pc.ParticipantID equals p.ParticipantID
                                          where pmc.ParticipantModuleCycleID == cycleID
                                          select p).First();
                hrefPECSurvey.HRef = PECURL + "Default.aspx?CST_KEY=" + pecParticipantInfo.ASNGUID; //pecParticipantInfo.ASNGUID;

            }

            // all chart data has been entered?
            if (isModuleCompleted)
            {
                HyperLinkReviewPerformanceReport.NavigateUrl = "PerformanceReport.aspx";
                if (cycle.ReportApproved)
                {
                    PlanProgressPct = 80;
                    imagePerformanceReport.Src = "../common/images/icon-check.png";
                    HyperLinkAnalyzeImpact.NavigateUrl = "ImpactStatement.aspx";
                    var ParticipantInfo = (from c in pim.Participant where c.ParticipantID == ctx.ParticipantID select c).FirstOrDefault();
                    if (cycle.ImpactAssessmentComplete)
                    {
                        if (ParticipantInfo.SoftwareVersion >= 2016)
                        {
                            HyperLinkReviewPerformanceReport.NavigateUrl = "PerformanceReportUpdate.aspx?cycle=2&action=view";
                        }
                        else
                        {
                            HyperLinkReviewPerformanceReport.NavigateUrl = "PerformanceReport.aspx?cycle=2&action=view";
                        }
                       
                        imageAnalyzeImpact.Src = "../common/images/icon-check.png";
                        PlanProgressPct = PlanProgressPct + 10;
                        HyperLinkSubmitModule.NavigateUrl = "Congratulations.aspx";
                        if (cycle.IsComplete==true) {
                            PlanProgressPct = 100;
                            ImageSubmitModule.Src = "../common/images/icon-check.png";
                            //HyperLinkSubmitModule.NavigateUrl = "";
                        }
                    }

                }

            }

            HiddenFieldPlanProgressPct.Value = PlanProgressPct.ToString();
        }
    }

    protected void RepeaterModules_ItemDataBound(Object Sender, RepeaterItemEventArgs e)
    {

        // This event is raised for the header, the footer, separators, and items.
        // Execute the following logic for Items and Alternating Items.
        if (e.Item.ItemType == ListItemType.Item || e.Item.ItemType == ListItemType.AlternatingItem)
        {
            //ParticipantDashboardModules
            ParticipantDashboardModules pdm = ((ParticipantDashboardModules)e.Item.DataItem);


            if (pdm.SelfAssessmentSurveyComplete == true)
                ((HtmlImage)e.Item.FindControl("imageSelfAssessment")).Src = "../common/images/icon-check.png";
            else
                ((HtmlImage)e.Item.FindControl("imageSelfAssessment")).Src = "../common/images/icon-dash.png";
            if (pdm.SystemSurveyComplete == true)
                ((HtmlImage)e.Item.FindControl("imageSystemSurvey")).Src = "../common/images/icon-check.png";
            else
                ((HtmlImage)e.Item.FindControl("imageSystemSurvey")).Src = "../common/images/icon-dash.png";

            if (pdm.TotalCharts == pdm.ChartsCompleted)
                ((HtmlImage)e.Item.FindControl("imageChartAbstraction")).Visible = true;
            if (pdm.TotalCharts == pdm.PatientChartRegistrationCompleted)
                ((HtmlImage)e.Item.FindControl("imageChartRegistration")).Visible = true;

            if ((pdm.SelfAssessmentSurveyComplete != true) || (pdm.SystemSurveyComplete != true) || (pdm.TotalCharts != pdm.ChartsCompleted))
                isModuleCompleted = false;


        }
    }
    protected void ButtonCharts_Click(object sender, CommandEventArgs e)
    {
        Session[Constants.SESSION_WORKINGMODULEID] = Convert.ToInt32(e.CommandArgument);
        Response.Redirect("PatientChartRegistration.aspx");
    }
    protected void ButtonSelfAssessment_Click(object sender, CommandEventArgs e)
    {
        Session[Constants.SESSION_WORKINGMODULEID] = Convert.ToInt32(e.CommandArgument);
        Response.Redirect("SelfAssessment.aspx");
    }
    protected void ButtonSystemSurvey_Click(object sender, CommandEventArgs e)
    {
        Session[Constants.SESSION_WORKINGMODULEID] = Convert.ToInt32(e.CommandArgument);
        Response.Redirect("SystemSurvey.aspx");
    }
    protected void LinkButtonNext_Click(object sender, CommandEventArgs e)
    {
        using (NetHealthPIMEntities pim = new NetHealthPIMEntities())
        {
            ParticipantModuleCycle cycle = pim.ParticipantModuleCycle.First(p => p.ParticipantModuleCycleID == ctx.ActiveModuleCycleID);
            Response.Redirect(CycleManager.WhereYouLeftOff(cycle.ParticipantModuleCycleID));
        }
    }
}
