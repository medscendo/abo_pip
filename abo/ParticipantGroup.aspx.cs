﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using NetHealthPIMModel;

public partial class copd_ParticipantGroup : BasePage
{
    protected void Page_Load(object sender, EventArgs e)
    {
        if (!IsPostBack)
        {
            InitializePage();
        }

    }
    protected void InitializePage()
    {

        ((PIMMasterPage)Page.Master).SetTitle("Select Group");

        using (NetHealthPIMEntities pim = new NetHealthPIMEntities())
        {

            var groupsParticipant = from g in pim.ParticipantGroup
                                    join gm in pim.ParticipantGroupMember
                                    on g.ParticipantGroupID equals gm.ParticipantGroupID
                                    where gm.ParticipantID == ctx.ParticipantID
                                    select new { g.ParticipantGroupID, g.ParticipantGroupName, g.Participant.ParticipantID };
            DropDownListGroups.DataSource = groupsParticipant;
            DataBind();

        }
    }
    protected void ButtonSubmit_Click(object sender, EventArgs e)
    {
        var participantGroupID = Int32.Parse(DropDownListGroups.SelectedValue);
        using (NetHealthPIMEntities pim = new NetHealthPIMEntities())
        {
            var groupsParticipant = (from g in pim.ParticipantGroup
                                     where g.ParticipantGroupID == participantGroupID
                                     select new { g.ParticipantGroupID, g.ParticipantGroupName, g.Participant.ParticipantID }).First();
            ctx.LeaderParticipantID = groupsParticipant.ParticipantID;
            ctx.ParticipantGroupID = groupsParticipant.ParticipantGroupID;
            Response.Redirect("~/" + module.ModuleCode.ToLower() + "/MyPI.aspx");

        }
    }

}
