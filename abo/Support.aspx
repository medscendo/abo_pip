﻿<%@ Page Title="" Language="C#" MasterPageFile="~/PIMMasterPage.master" AutoEventWireup="true" CodeFile="Support.aspx.cs" Inherits="abo_Support" %>

<asp:Content ID="Content1" ContentPlaceHolderID="head" Runat="Server">
</asp:Content>

<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder1" Runat="Server">
      
        <p>If you need assistance with this Improvement in Medical Practice Platform, the following resources are available:</p>       
        <p><strong>Personalized Support:</strong></p>
        <p>You can <a href="ContactUs.aspx">contact us</a> for support.</p>
        <p>We will respond to all inquiries within one business day.</p>
        
        <div class="button-box"><asp:LinkButton ID="ButtonSubmit" runat="server" Text="Back" PostBackUrl="Dashboard.aspx" CssClass="button" /></div>
              
</asp:Content>

