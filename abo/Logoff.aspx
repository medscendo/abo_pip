﻿<%@ Page Title="" Language="C#" MasterPageFile="~/PIMMasterPage.master" AutoEventWireup="true" CodeFile="Logoff.aspx.cs" Inherits="copd_Logoff" %>

<asp:Content ID="Content1" ContentPlaceHolderID="head" Runat="Server">
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder1" Runat="Server">
<script type="text/javascript" language="javascript">
    //document.all("divLogo").innerHTML = "";
</script>
<table border="0" cellspacing="0" class="edit_table center_area ">
    <tr>
        <td style="padding: 30px;">You have been logged off of the ABO Improvement in Medical Practice Activity. 
        <br /><br />To log on again, go to <a href="http://www.abop.org/plain/log.asp">Login to ABO</a></td>
    </tr>
</table>
</asp:Content>

