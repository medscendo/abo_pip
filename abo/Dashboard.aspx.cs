﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using NetHealthPIMModel;
using System.Transactions;
using System.Web.UI.HtmlControls;

public partial class abo_Dashboard : BasePage
{
    bool isModuleCompleted = true;
    protected void Page_Load(object sender, EventArgs e)
    {
        if (!IsPostBack)
        {
            InitializePage();
        }

    }
    protected void InitializePage()
    {
        string breadCrumb = "Status Page"; 
        ((PIMMasterPage)Page.Master).SetTitle(breadCrumb);
        ((PIMMasterPage)Page.Master).SetStatus(2);
        ((PIMMasterPage)Page.Master).SetHeader("Assess &amp; Improve: Status Page");

        using (NetHealthPIMEntities pim = new NetHealthPIMEntities())
        {
            int PlanProgressPct = 0;
            int cycleID = ctx.ActiveModuleCycleID;
            ParticipantModuleCycle cycle = pim.ParticipantModuleCycle.First(c => c.ParticipantModuleCycleID == cycleID);
            ParticipantModuleCycle cycle2 = null;

            if (cycle.CycleNumber == 2)
            {
                cycle2 = cycle;

                cycle.PrevParticipantModuleCycleReference.Load();
                ParticipantModuleCycle prev = cycle.PrevParticipantModuleCycle;
                cycle = prev;

            }

           

            var participantDashboardModules = (from pdm in pim.ParticipantDashboardModules_V
                      where pdm.ParticipantModuleCycleID == cycle.ParticipantModuleCycleID
                      select new ParticipantDashboardModules {
                            ModuleID = pdm.ModuleID,
                            ParticipantModuleCycleID = pdm.ParticipantModuleCycleID,
                            ModuleShortName = pdm.ModuleShortName,
                            SelfAssessmentSurveyTargetDate = pdm.SelfAssessmentSurveyTargetDate,
                            SelfAssessmentSurveyCompletedDate = pdm.SelfAssessmentSurveyCompletedDate,
                            SelfAssessmentSurveyComplete = pdm.SelfAssessmentSurveyComplete,
                            SystemSurveyTargetDate = pdm.SystemSurveyTargetDate,
                            SystemSurveyCompletedDate = pdm.SystemSurveyCompletedDate,
                            SystemSurveyComplete = pdm.SystemSurveyComplete,
                            AbstractDataTargetDate = pdm.AbstractDataTargetDate,
                            AbstractDataCompletedDate = pdm.AbstractDataCompletedDate,
                            AbstractDataComplete = pdm.AbstractDataComplete,
                            ReportApproved = pdm.ReportApproved,
                            ReportApprovedTargetDate = pdm.ReportApprovedTargetDate,
                            ReportApprovedCompletedDate = pdm.ReportApprovedCompletedDate,
                            ImprovementPlanComplete = pdm.ImprovementPlanComplete,
                            ImprovementPlanTargetDate = pdm.ImprovementPlanTargetDate,
                            ImprovementPlanCompletedDate = pdm.ImprovementPlanCompletedDate,
                            PatientChartRegistrationCompleted = (int)pdm.PatientChartRegistrationCompleted,
                            TotalCharts = (int)pdm.TotalCharts,
                            ChartsCompleted = (int)pdm.ChartsCompleted
                      });
            RepeaterModules.DataSource = participantDashboardModules;
            RepeaterModules.DataBind();

            int numberOfModules = participantDashboardModules.Count();
            int modulesCompleted = 0;
            int systemSurveyCompleted = 0;
            int selfAssessmentCompleted = 0;
            foreach (ParticipantDashboardModules pdm in participantDashboardModules)
            {
                if (pdm.AbstractDataComplete == true)
                    modulesCompleted++;
                if (pdm.SelfAssessmentSurveyComplete == true)
                    selfAssessmentCompleted++;
                if (pdm.SystemSurveyComplete == true)
                    systemSurveyCompleted++;
            }
            PlanProgressPct = (int)((Convert.ToDouble(selfAssessmentCompleted) / Convert.ToDouble(numberOfModules)) * 10);
            PlanProgressPct = PlanProgressPct + (int)((Convert.ToDouble(systemSurveyCompleted) / Convert.ToDouble(numberOfModules)) * 10);
            PlanProgressPct = PlanProgressPct + (int)((Convert.ToDouble(modulesCompleted) / Convert.ToDouble(numberOfModules)) * 40);

            isModuleCompleted = true; // (System.Configuration.ConfigurationSettings.AppSettings["Environment"] == "sandbox");
            if ((isModuleCompleted) && (RepeaterModules.Items.Count>0))
            {
                var ParticipantInfo = (from c in pim.Participant where c.ParticipantID == ctx.ParticipantID select c).FirstOrDefault();
                if (ParticipantInfo.SoftwareVersion >=2016)
                {
                    HyperLinkReviewPerformanceReport.NavigateUrl = "PerformanceReportUpdate.aspx?cycle=1";
                }
                else
                {
                    HyperLinkReviewPerformanceReport.NavigateUrl = "PerformanceReport.aspx?cycle=1";
                }
                if (cycle.ReportApproved)
                {
                    PlanProgressPct = 80;
                    imagePerformanceReport.Src = "../common/images/icon-check.png";
                    if (ParticipantInfo.SoftwareVersion >= 2016)
                    {
                        HyperLinkSelectMeasures.NavigateUrl = "PerformanceReportUpdate.aspx?cycle=1&show=sel";
                    }
                    else
                    {
                        HyperLinkSelectMeasures.NavigateUrl = "SelectMeasures.aspx";
                    }
                    if (cycle.MeasuresSelected)
                    {
                        imageSelectMeasures.Src = "../common/images/icon-check.png";
                        PlanProgressPct = PlanProgressPct + 10;
                    }

                    string ImprovementPage = "ImprovementPlanPartI.aspx";
                    if (cycle.ImprovementPlanComplete)
                    {
                        CheckBoxDevelopImprovementPlan.Checked = true;
                        PlanProgressPct = PlanProgressPct + 10;
                        imageImprovementPlan.Src = "../common/images/icon-check.png";
                        HyperLinkSelectMeasures.NavigateUrl = "";
                        HyperLinkReviewImprovement.NavigateUrl = "ImprovementPlanPartI.aspx"; 
                        imageReviewImprovement.Src = "../common/images/icon-check.png";
                    }
                    else
                    {
                        HyperLinkDevelopImprovementPlan.NavigateUrl = ImprovementPage;
                    }
                }
            }
            PECSurvey.Visible = (cycle.PECCurrentID > 0);
            if (cycle.PECCurrentID > 0)
            {
                Participant participant = pim.Participant.First(p => p.ParticipantID == ctx.ParticipantID);
                var pecSurveysPerCandidate = (from ps in pim.PECSurveysPerCandidate_V
                                              where ps.CandidateID == participant.CandidateID
                                              && ps.ParticipantEntryID == cycle.PECCurrentID
                                              orderby ps.ParticipantCycleID descending
                                              select ps).First();
                LabelPatientSurvey.Text = pecSurveysPerCandidate.SurveyCount.ToString();
                string PECURL = System.Configuration.ConfigurationManager.AppSettings["PECURL"];
                var pecParticipantInfo = (from pmc in pim.ParticipantModuleCycle
                                          join pc in pim.ParticipantCycle on pmc.PECCurrentID equals pc.ParticipantEntryID
                                          join p in pim.Participant on pc.ParticipantID equals p.ParticipantID
                                          where pmc.ParticipantModuleCycleID == cycle.ParticipantModuleCycleID 
                                          select p).First();
                hrefPECSurvey.HRef = PECURL + "Default.aspx?CST_KEY=" + pecParticipantInfo.ASNGUID; 
            }
            if (PlanProgressPct < 0)
                PlanProgressPct = 0;
            HiddenFieldPlanProgressPct.Value = PlanProgressPct.ToString();

            
        } 
            

    }
    protected void RepeaterModules_ItemDataBound(Object Sender, RepeaterItemEventArgs e)
    {

        // This event is raised for the header, the footer, separators, and items.
        // Execute the following logic for Items and Alternating Items.
        if (e.Item.ItemType == ListItemType.Item || e.Item.ItemType == ListItemType.AlternatingItem)
        {
        
            ParticipantDashboardModules pdm = ((ParticipantDashboardModules)e.Item.DataItem);
            if (pdm.SelfAssessmentSurveyComplete == true)
                ((HtmlImage)e.Item.FindControl("imageSelfAssessment")).Src = "../common/images/icon-check.png";
            else
                ((HtmlImage)e.Item.FindControl("imageSelfAssessment")).Src = "../common/images/icon-dash.png";
            if (pdm.SystemSurveyComplete == true)
                ((HtmlImage)e.Item.FindControl("imageSystemSurvey")).Src = "../common/images/icon-check.png";
            else
                ((HtmlImage)e.Item.FindControl("imageSystemSurvey")).Src = "../common/images/icon-dash.png";

            if (pdm.TotalCharts == pdm.ChartsCompleted)
                ((HtmlImage)e.Item.FindControl("imageChartAbstraction")).Visible = true;
            if (pdm.TotalCharts == pdm.PatientChartRegistrationCompleted)
                ((HtmlImage)e.Item.FindControl("imageChartRegistration")).Visible = true;

            if ((pdm.SelfAssessmentSurveyComplete != true) || (pdm.SystemSurveyComplete != true) || (pdm.TotalCharts != pdm.ChartsCompleted))
                isModuleCompleted = false;
            
            
        }
    }

    protected void ButtonCharts_Click(object sender, CommandEventArgs e)
    {
        Session[Constants.SESSION_WORKINGMODULEID] = Convert.ToInt32(e.CommandArgument);
        Response.Redirect("PatientChartRegistration.aspx?CycleNumber=1");
    }
    protected void ButtonSelfAssessment_Click(object sender, CommandEventArgs e)
    {
        Session[Constants.SESSION_WORKINGMODULEID] = Convert.ToInt32(e.CommandArgument);
        Response.Redirect("SelfAssessment.aspx?CycleNumber=1");
    }
    protected void ButtonSystemSurvey_Click(object sender, CommandEventArgs e)
    {
        Session[Constants.SESSION_WORKINGMODULEID] = Convert.ToInt32(e.CommandArgument);
        Response.Redirect("SystemSurvey.aspx?CycleNumber=1");
    }
    protected void LinkButtonNext_Click(object sender, CommandEventArgs e)
    {
        using (NetHealthPIMEntities pim = new NetHealthPIMEntities())
        {
            ParticipantModuleCycle cycle = pim.ParticipantModuleCycle.First(p => p.ParticipantModuleCycleID == ctx.ActiveModuleCycleID);
            string nextPage = CycleManager.WhereYouLeftOff(cycle.ParticipantModuleCycleID);
            if (nextPage == "")
            {
                nextPage = CycleManager.FirstDashboardWhereYouLeftOff(ctx.ActiveModuleCycleID, ctx.ParticipantID);
            }
            Response.Redirect(nextPage);
        }
    }
}
