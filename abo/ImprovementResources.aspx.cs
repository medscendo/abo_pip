﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using NetHealthPIMModel;

public partial class abo_ImprovementResources : BasePage
{
    protected void Page_Load(object sender, EventArgs e)
    {
        ButtonSubmit.PostBackUrl = Request.UrlReferrer.ToString();

        using (NetHealthPIMEntities pim = new NetHealthPIMEntities())
        {
            int cycleID = ctx.ActiveModuleCycleID;
            ParticipantModuleCycle cycle = pim.ParticipantModuleCycle.First(c => c.ParticipantModuleCycleID == cycleID);

            int ModuleID = (int)Session[Constants.SESSION_WORKINGMODULEID];
            var module = from m in pim.Module
                         where m.ModuleID == ModuleID
                         select m;
            var moduleDetail = module.First();
            var resources = from m in pim.Measure
                            join mr in pim.MeasureResource on m.MeasureID equals mr.MeasureID
                            join r in pim.Resource on mr.ResourceID equals r.ResourceID
                            where m.Module.ModuleID == ModuleID
                            select new
                            {
                                ResourceName = r.ResourceName,
                                ResourceDescription = r.ResourceDescription,
                                URL = r.URL
                            };
            ListViewResources.DataSource = resources.Distinct();
            ListViewResources.DataBind();
        }

    }
}
