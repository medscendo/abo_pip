﻿<%@ Page Title="" Language="C#" MasterPageFile="~/PIMMasterPage.master" AutoEventWireup="true" CodeFile="ImpactStatement.aspx.cs" Inherits="abo_ImpactStatement" %>

<asp:Content ID="Content1" ContentPlaceHolderID="head" Runat="Server">
    
<link type="text/css" href="../common/css/atooltip.css" rel="stylesheet"  media="screen" />

 <script type="text/javascript">
        $(function () {
            $('#PM1').aToolTip({
                clickIt: true,
                tipContent: '<b>Feedback Report Legend</b><br><br><b>Self</b> = Self-assessment goals<br><b>My Goal</b> = Target that you choose for your improvement<br><b>Initial Data</b> = Calculated measure compiled from initial chart abstraction<br><b>Final Data</b> = Calculated measure compiled from second chart abstraction<br>'
            });
        });

        </script>

<style>
    .plan_list_row
    {
        width:562px !important;
    }
    .plan_list_col 
    {
        width:280px !important;
    }

    hr {
        margin: 25px 0 15px;
    }

    label {
        padding-left:5px;
    }

    .checkbox-table {
        width:100%;
    }

    .checkbox-table td {
        width: 33.33%;
        padding:8px 0;
    }

    .w-options {
    }

    .w-options td, .w-options th {
        text-align:center;
    }

    .w-options tr td {
        border-left:none;
        border-right:none;
    }

    .w-options td:nth-child(1), .w-options th:nth-child(1) {
        text-align:left;
    }
    .w-options td:nth-child(1) {
        border-left: 1px solid rgb(204, 204, 204);
    }
    .w-options td:nth-child(6) {
        border-right: 1px solid rgb(204, 204, 204);
    }

    .selection {
        width: 10%;
        font-size: 0.8em;
        text-align:center;
    }

</style>

</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder1" Runat="Server">
    <p>
        The &quot;Analysis and Impact&quot; phase allows Diplomate’s to reflect on overall improvement and submit a final impact statement based on their learning and experiences throughout this process.  The table below illustrates a comparison of second round patient chart abstraction data to those entered prior to Improvement Plan implementation.  The table also displays your self-assessment data, original goal and final improvement and presents an opportunity for you to indicate barriers to improvement, rate improvement activities and submit additional comments pertinent to your learning experience.
    </p>
    <!-- Question 1 -->
    <h3>1. Performance Measures selected for improvement</h3>
    <table class="table">
    <asp:ListView runat="server" ID="ListViewProcessMeasures" onitemdatabound="ListViewProcessMeasures_OnItemDataBound">
    <LayoutTemplate>
        <tr>
            <th>Performance Measure Selected</th>
            <th>Results</th>
        </tr>
        <asp:PlaceHolder runat="server" ID="itemPlaceholder"></asp:PlaceHolder>
    </LayoutTemplate>    
    <ItemTemplate>
        <tr>
            <td>
                <strong><asp:Label ID="LabelMeasureTitle" CssClass="label_highlight" runat="server" Text='<%# Bind("MeasureQualityIndicator") %>'></asp:Label></strong> <asp:Label ID="LabelMeasureLongDescription" runat="server" Text='<%# Bind("MeasureLongDescription") %>'></asp:Label>
            </td>
            <td>
                <asp:Chart ID="ChartPerformance" runat="server" Width="300" Height="120" >
                <Series>
                    <asp:Series Name="My Data" ChartType="Bar" Color="#2f5199"  BorderColor="#2f5199"  CustomProperties="DrawingStyle=Wedge" Palette="Berry" YValueMembers="MeasurePercentCurrent" IsValueShownAsLabel="true" LabelForeColor="#000000" LabelBackColor="Transparent" />
                    <asp:Series Name="My Peers" ChartType="Bar" Color="#ffb200" BorderColor="#ffb200" CustomProperties="DrawingStyle=Wedge" Palette="Berry" YValueMembers="PeerData" IsValueShownAsLabel="true" LabelForeColor="#000000" LabelBackColor="Transparent" />
                </Series>
                <ChartAreas>
                    <asp:ChartArea Name="MainChartArea">
                        <AxisX Maximum="5" LineColor="Transparent" LineWidth="1">
                            <MajorGrid Enabled="false" />
                            <LabelStyle IsEndLabelVisible="false" />
                            <CustomLabels />
                        </AxisX>
                        <AxisY Maximum="100" Interval="20" />
                    </asp:ChartArea>
                </ChartAreas>
            </asp:Chart>
            </td>
        </tr>

        
    </ItemTemplate>
    </asp:ListView>
    
    <asp:ListView runat="server" ID="ListViewOutcomeMeasures" onitemdatabound="ListViewOutcomeMeasures_OnItemDataBound">
    <LayoutTemplate>
    <tr>
        <asp:PlaceHolder runat="server" ID="itemPlaceholder"></asp:PlaceHolder>
<p>What does this data represent <img id="PM1" src="../common/images/tip.gif" alt="Tip" /></a></p>
    </tr>
    </LayoutTemplate>    
    <ItemTemplate>
    <tr>
        <td>
            <strong><asp:Label ID="LabelMeasureTitle" CssClass="label_highlight" runat="server" Text='<%# Bind("MeasureQualityIndicator") %>'></asp:Label></strong> <asp:Label ID="LabelMeasureLongDescription" runat="server" Text='<%# Bind("MeasureLongDescription") %>'></asp:Label>

        
</td>
        <td>                 
            <asp:Chart ID="ChartPerformance" runat="server" Width="300" Height="180" >
            <Series>
                <asp:Series Name="My Data" ChartType="Bar" Color="#2f5199"  BorderColor="#2f5199"  CustomProperties="DrawingStyle=Wedge" Palette="Berry" YValueMembers="MeasurePercentCurrent" IsValueShownAsLabel="true" LabelForeColor="#000000" LabelBackColor="Transparent" />
                <asp:Series Name="My Peers" ChartType="Bar" Color="#ffb200" BorderColor="#ffb200" CustomProperties="DrawingStyle=Wedge" Palette="Berry" YValueMembers="PeerData" IsValueShownAsLabel="true" LabelForeColor="#000000" LabelBackColor="Transparent" />
            </Series>
            <ChartAreas>
                <asp:ChartArea Name="MainChartArea">
                    <AxisX Maximum="8" LineColor="Transparent" LineWidth="1">
                        <MajorGrid Enabled="false" />
                        <LabelStyle IsEndLabelVisible="false" />
                        <CustomLabels>
                        </CustomLabels>
                    </AxisX>
                    <AxisY Maximum="100" Interval="20" />
                </asp:ChartArea>
            </ChartAreas>
            </asp:Chart>                    
        </td>
    </tr>
    </ItemTemplate>
    </asp:ListView>
    </table>
    <hr />

    <!-- Question 2 -->
    <h3>2. Listed below are the changes you considered most important for you and your practice to maintain or improve your performance</h3>
    <p>
        <strong><asp:Literal ID="LiteralImportantChangesComments" runat="server"></asp:Literal></strong>
    </p>
    <p> Reflect on these changes and their importance today:</p>
    <asp:TextBox ID="TextBoxImportanceOfChanges" Rows="3" Width="100%" TextMode="MultiLine" runat="server"></asp:TextBox>
    <asp:Literal ID="LiteralImportanceOfChanges" runat="server" Visible="false"></asp:Literal>
    <hr />

    <!-- Question 3 -->
    <h3>3. What is the most useful item that you learned from this process?</h3>
    <asp:TextBox ID="TextBoxUsefulItemsLearned" Rows="3" Width="100%" TextMode="MultiLine" runat="server"></asp:TextBox>
    <asp:Literal ID="LiteralUsefulItemsLearned" runat="server" Visible="false"></asp:Literal>
    <hr />

    <!-- Question 4 -->
    <h3> 4. Indicate any barriers that you encountered in implementing your improvement plan.</h3>

    <table class="checkbox-table">
        <tr>
            <td><asp:CheckBox ID="CheckBoxBarrierNotEnoughTime" runat="server" class="check" Text="Not Enough Time&nbsp;<a href='#'><img id='Q1' src='../common/images/tip.gif' alt='Tip' /></a>" /></td>
            <td><asp:CheckBox ID="CheckBoxBarrierClinicalKnowledgeSkillExpertise" runat="server" class="check" Text="Clinical Knowledge / Skill / Expertise&nbsp;<a href='#'><img id='Q2' src='../common/images/tip.gif' alt='Tip' /></a>" /></td>
            <td><asp:CheckBox ID="CheckBoxBarrierRecallConfidenceClinicalInertia" runat="server" class="check" Text="Recall, Confidence, Clinical Inertia&nbsp;<a href='#'><img id='Q3' src='../common/images/tip.gif' alt='Tip' /></a>" /></td>
        </tr>
        <tr>
            <td><asp:CheckBox ID="CheckBoxBarrierPeerInfluence" runat="server" class="check" Text="Peer Influence&nbsp;<a href='#'><img id='Q4' src='../common/images/tip.gif' alt='Tip' /></a>" /></td>
            <td><asp:CheckBox ID="CheckBoxBarrierCulturalCompetency" runat="server" class="check" Text="Cultural Competency&nbsp;<a href='#'><img id='Q5' src='../common/images/tip.gif' alt='Tip' /></a>" /></td>
            <td><asp:CheckBox ID="CheckBoxBarrierFearLegalConcerns" runat="server" class="check" Text="Fear / Legal Concerns&nbsp;<a href='#'><img id='Q6' src='../common/images/tip.gif' alt='Tip' /></a>" /></td>
        </tr>
        <tr>
            <td><asp:CheckBox ID="CheckBoxBarrierStaffCompetence" runat="server" class="check" Text="Staff Competence&nbsp;<a href='#'><img id='Q7' src='../common/images/tip.gif' alt='Tip' /></a>" /></td>
            <td><asp:CheckBox ID="CheckBoxBarrierStaffMotivation" runat="server" class="check" Text="Staff Motivation&nbsp;<a href='#'><img id='Q8' src='../common/images/tip.gif' alt='Tip' /></a>" /></td>
            <td><asp:CheckBox ID="CheckBoxBarrierCostFunding" runat="server" class="check" Text="Cost / Funding&nbsp;<a href='#'><img id='Q10' src='../common/images/tip.gif' alt='Tip' /></a>" /></td>
        </tr>
        <tr>
            <td><asp:CheckBox ID="CheckBoxBarrierPatientCharacteristicsFactors" runat="server" class="check" Text="Patient Characteristics / Factors&nbsp;<a href='#'><img id='Q11' src='../common/images/tip.gif' alt='Tip' /></a>" /></td>
            <td><asp:CheckBox ID="CheckBoxBarrierPatientAdherence" runat="server" class="check" Text="Patient Adherence&nbsp;<a href='#'><img id='Q12' src='../common/images/tip.gif' alt='Tip' /></a>" /></td>
            <td><asp:CheckBox ID="CheckBoxBarrierWorkOverload" runat="server" class="check" Text="Work Overload&nbsp;<a href='#'><img id='Q13' src='../common/images/tip.gif' alt='Tip' /></a>" /></td>
        </tr>
        <tr>
            <td><asp:CheckBox ID="CheckBoxBarrierTeamStructure" runat="server" class="check" Text="Team Structure&nbsp;<a href='#'><img id='Q14' src='../common/images/tip.gif' alt='Tip' /></a>" /></td>
            <td><asp:CheckBox ID="CheckBoxBarrierPracticeProcess" runat="server" class="check" Text="Practice Process&nbsp;<a href='#'><img id='Q15' src='../common/images/tip.gif' alt='Tip' /></a>" /></td>
            <td><asp:CheckBox ID="CheckBoxBarrierReferralProcess" runat="server" class="check" Text="Referral Process&nbsp;<a href='#'><img id='Q16' src='../common/images/tip.gif' alt='Tip' /></a>" /></td>
        </tr>
        <tr>
            <td colspan="3">
                <asp:CheckBox ID="CheckBoxBarrierOther" runat="server" class="check" Text="Other:&nbsp;<a href='#'><img id='Q9' src='../common/images/tip.gif' alt='Tip' /></a>"/>
                <asp:TextBox ID="TextBoxImpactOthersDescription" runat="server" style="margin-left:20px"></asp:TextBox>
            </td>
        </tr>
    </table>
    <hr />

    <!-- Question 5 -->
    <h3>5. Rate these activities</h3>
    <table class="table w-options">
        <tr>
            <th>Activities</th>
            <th class="selection">Poor</th>
            <th class="selection">Fair</th>
            <th class="selection">Good</th>
            <th class="selection">Very Good</th>
            <th class="selection">Excellent</th>
        </tr>

        <tr>
            <td>Clinical usefulness overall</td>
            <td><asp:RadioButton ID="RadioButtonClinicalUsefulnessOverall1" runat="server" GroupName="ClinicalUsefulnessOverall"/></td>
            <td><asp:RadioButton ID="RadioButtonClinicalUsefulnessOverall2" runat="server" GroupName="ClinicalUsefulnessOverall"/></td>
            <td><asp:RadioButton ID="RadioButtonClinicalUsefulnessOverall3" runat="server" GroupName="ClinicalUsefulnessOverall"/></td>
            <td><asp:RadioButton ID="RadioButtonClinicalUsefulnessOverall4" runat="server" GroupName="ClinicalUsefulnessOverall"/></td>
            <td><asp:RadioButton ID="RadioButtonClinicalUsefulnessOverall5" runat="server" GroupName="ClinicalUsefulnessOverall"/></td>
        </tr>
        <tr>
            <td>Objective presentation of Educational interventions</td>
            <td><asp:RadioButton ID="RadioButtonObjectivePresentationOf1" runat="server" GroupName="ObjectivePresentationOf"/></td>
            <td><asp:RadioButton ID="RadioButtonObjectivePresentationOf2" runat="server" GroupName="ObjectivePresentationOf"/></td>
            <td><asp:RadioButton ID="RadioButtonObjectivePresentationOf3" runat="server" GroupName="ObjectivePresentationOf"/></td>
            <td><asp:RadioButton ID="RadioButtonObjectivePresentationOf4" runat="server" GroupName="ObjectivePresentationOf"/></td>
            <td><asp:RadioButton ID="RadioButtonObjectivePresentationOf5" runat="server" GroupName="ObjectivePresentationOf"/></td>
        </tr>
        <tr>
            <td>Scientific rigor</td>
            <td><asp:RadioButton ID="RadioButtonScientificRigor1" runat="server" GroupName="ScientificRigor"/></td>
            <td><asp:RadioButton ID="RadioButtonScientificRigor2" runat="server" GroupName="ScientificRigor"/></td>
            <td><asp:RadioButton ID="RadioButtonScientificRigor3" runat="server" GroupName="ScientificRigor"/></td>
            <td><asp:RadioButton ID="RadioButtonScientificRigor4" runat="server" GroupName="ScientificRigor"/></td>
            <td><asp:RadioButton ID="RadioButtonScientificRigor5" runat="server" GroupName="ScientificRigor"/></td>
        </tr>
        <tr>
            <td>Free of commercial bias</td>
            <td><asp:RadioButton ID="RadioButtonFreeOfCommercialBias1" runat="server" GroupName="FreeOfCommercialBias"/></td>
            <td><asp:RadioButton ID="RadioButtonFreeOfCommercialBias2" runat="server" GroupName="FreeOfCommercialBias"/></td>
            <td><asp:RadioButton ID="RadioButtonFreeOfCommercialBias3" runat="server" GroupName="FreeOfCommercialBias"/></td>
            <td><asp:RadioButton ID="RadioButtonFreeOfCommercialBias4" runat="server" GroupName="FreeOfCommercialBias"/></td>
            <td><asp:RadioButton ID="RadioButtonFreeOfCommercialBias5" runat="server" GroupName="FreeOfCommercialBias"/></td>
        </tr>
        <tr>
            <td>Usefulness of conducting self-assessment</td>
            <td><asp:RadioButton ID="RadioButtonUsefulnessOfConductingSelfAssessment1" runat="server" GroupName="UsefulnessOfConductingSelfAssessment"/></td>
            <td><asp:RadioButton ID="RadioButtonUsefulnessOfConductingSelfAssessment2" runat="server" GroupName="UsefulnessOfConductingSelfAssessment"/></td>
            <td><asp:RadioButton ID="RadioButtonUsefulnessOfConductingSelfAssessment3" runat="server" GroupName="UsefulnessOfConductingSelfAssessment"/></td>
            <td><asp:RadioButton ID="RadioButtonUsefulnessOfConductingSelfAssessment4" runat="server" GroupName="UsefulnessOfConductingSelfAssessment"/></td>
            <td><asp:RadioButton ID="RadioButtonUsefulnessOfConductingSelfAssessment5" runat="server" GroupName="UsefulnessOfConductingSelfAssessment"/></td>
        </tr>
        <tr>
            <td>Usefulness of conducting chart audit</td>
            <td><asp:RadioButton ID="RadioButtonUsefulnessOfConductingChartAudit1" runat="server" GroupName="UsefulnessOfConductingChartAudit"/></td>
            <td><asp:RadioButton ID="RadioButtonUsefulnessOfConductingChartAudit2" runat="server" GroupName="UsefulnessOfConductingChartAudit"/></td>
            <td><asp:RadioButton ID="RadioButtonUsefulnessOfConductingChartAudit3" runat="server" GroupName="UsefulnessOfConductingChartAudit"/></td>
            <td><asp:RadioButton ID="RadioButtonUsefulnessOfConductingChartAudit4" runat="server" GroupName="UsefulnessOfConductingChartAudit"/></td>
            <td><asp:RadioButton ID="RadioButtonUsefulnessOfConductingChartAudit5" runat="server" GroupName="UsefulnessOfConductingChartAudit"/></td>
        </tr>
        <tr>
            <td>Usefulness of feedback provided</td>
            <td><asp:RadioButton ID="RadioButtonUsefulnessOfFeedbackProvided1" runat="server" GroupName="UsefulnessOfFeedbackProvided"/></td>
            <td><asp:RadioButton ID="RadioButtonUsefulnessOfFeedbackProvided2" runat="server" GroupName="UsefulnessOfFeedbackProvided"/></td>
            <td><asp:RadioButton ID="RadioButtonUsefulnessOfFeedbackProvided3" runat="server" GroupName="UsefulnessOfFeedbackProvided"/></td>
            <td><asp:RadioButton ID="RadioButtonUsefulnessOfFeedbackProvided4" runat="server" GroupName="UsefulnessOfFeedbackProvided"/></td>
            <td><asp:RadioButton ID="RadioButtonUsefulnessOfFeedbackProvided5" runat="server" GroupName="UsefulnessOfFeedbackProvided"/></td>
        </tr>
    </table>
    <hr />

    <!-- Question 6 -->
    <h3>6. Additional comments:</h3>
    <asp:TextBox ID="TextBoxAdditionalComments" Rows="3" Width="100%" TextMode="MultiLine" runat="server"></asp:TextBox>
    <asp:Literal ID="LiteralAdditionalComments" runat="server" Visible="false"></asp:Literal>
    <div class="button-box">
        <%--<a href="2.html"><input type="button" name="" value=""" /></a>--%>
        <asp:LinkButton ID="ButtonSubmit" runat="server" Text="Save Impact Statement and Complete"  OnClick="ButtonSubmit_Click" OnClientClick="return validateForm();" CssClass="button" />
        <asp:LinkButton ID="LinkButtonSaveForLater" runat="server" Text="Save For Later"  OnClick="ButtonSaveForLater_Click" CssClass="button" />
        <asp:LinkButton ID="LinkButtonDashboard" runat="server" Text="Return To Status Page" PostBackUrl="Dashboard3.aspx" CssClass="button" />
    </div>
          
</asp:Content>

<asp:Content runat="server" ID="Content3" ContentPlaceHolderID="javascript">

<script type="text/javascript" src="../common/js/jquery.atooltip.js"></script>

<script type="text/javascript">
    $(function () {
        $('#Q1').aToolTip({
            clickIt: true,
            tipContent: 'Due to patient load and other administrative tasks the ability to research and implement potential improvement strategies is limited.'
        });
    });
    $(function () {
        $('#Q2').aToolTip({
            clickIt: true,
            tipContent: 'Practitioner&rsquo;s clinical knowledge, skill or expertise is currently not up to the recommended standard of care and requires additional research for improvement.'
        });
    });
    $(function () {
        $('#Q3').aToolTip({
            clickIt: true,
            xOffset: -200,
            tipContent:
            '<strong>Recall</strong>: The physician&rsquo;s ability to institute practical, easy to understand language to help patients comprehend medical instructions pertaining to their treatment regimen.<br /><strong>Confidence</strong>: The ability of the physician to identify the skills necessary to advance skillful treatment and gain patient&rsquo;s trust from a patient&rsquo;s perspective.<br /><strong>Clinical Inertia</strong>: Clinical inertia refers to patient resistance to common medical treatment for a given condition and/or inability of the physician to intensify treatment when a patient has not responded to treatment or reached the expected goal(s).'
        });
    });
    $(function () {
        $('#Q4').aToolTip({
            clickIt: true,
            tipContent: 'An unwillingness by the physician to research potential improvements to processes of care based on the current standard established by consensus of professional peers.'
        });
    });
    $(function () {
        $('#Q5').aToolTip({
            clickIt: true,
            tipContent: 'Physician’s ability to identify and implement the necessary skills to effectively communicate with patients from different cultures and/or belief systems other than their own.'
        });
    });
    $(function () {
        $('#Q6').aToolTip({
            clickIt: true,
            xOffset: -200,
            tipContent: 'A concern on the physician’s behalf to protect the clinical practice at the expense of the patient’s well-being with an emphasis on legal self-protection.'
        });
    });
    $(function () {
        $('#Q7').aToolTip({
            clickIt: true,
            tipContent: 'A physician’s confidence level in his or her staff’s ability to tend to tasks, learn new skills and follow appropriate processes of patient care with minimal supervision.'
        });
    });
    $(function () {
        $('#Q8').aToolTip({
            clickIt: true,
            tipContent: 'Clinical staff is not actively engaged in improving the practice and continually seeking improvement strategies to enhance the patient’s experience of care throughout the duration of their medical treatment.'
        });
    });
    $(function () {
        $('#Q9').aToolTip({
            clickIt: true,
            tipContent: 'Please use the free text box provided to input your perceived barrier(s) to improvement.'
        });
    });
    $(function () {
        $('#Q10').aToolTip({
            clickIt: true,
            xOffset: -200,
            tipContent: 'Possible improvement strategies are not deemed cost-efficient or funding to undertake these improvements is not available at this time.'
        });
    });
    $(function () {
        $('#Q11').aToolTip({
            clickIt: true,
            tipContent: 'Various cultural and demographic factors and/or the severity of the patients&rsquo; current health condition impedes the physician&rsquo;s ability to improve the outcome.'
        });
    });
    $(function () {
        $('#Q12').aToolTip({
            clickIt: true,
            tipContent: 'A physician’s confidence level in patient’s ability or willingness to adhere to the medical treatment regimen prescribed.  Positive patient outcomes are significantly reduced if the recommended treatment regimen is not followed.'
        });
    });
    $(function () {
        $('#Q13').aToolTip({
            clickIt: true,
            xOffset: -200,
            tipContent: 'Due to patient load and other administrative tasks the ability to research and implement potential improvement strategies is limited.'
        });
    });
    $(function () {
        $('#Q14').aToolTip({
            clickIt: true,
            tipContent: 'The current organizational structure inhibits the ability to implement effective improvement strategies across essential areas of the clinical practice.'
        });
    });
    $(function () {
        $('#Q15').aToolTip({
            clickIt: true,
            tipContent: 'A practice process with regard to the quality measures at hand has not been defined and implemented within the clinical practice and/or current practice processes are embedded and difficult to alter.'
        });
    });
    $(function () {
        $('#Q16').aToolTip({
            clickIt: true,
            xOffset: -200,
            tipContent: 'Current referral processes make it difficult for the physician to obtain pertinent medical information about patients in a timely manner.'
        });
    });
</script>
    
<script type="text/javascript" language="javascript">
    function validateForm() {
        var msg = "";

        var total = 0;
        var completed = true;

        var TextBoxImportanceOfChanges = document.getElementById("<%= TextBoxImportanceOfChanges.ClientID %>").value;
        if (TextBoxImportanceOfChanges == "") {
            msg = msg + " Question 2 is not completed. \n";
            completed = false;
        }

        var TextBoxUsefulItemsLearned = document.getElementById("<%= TextBoxUsefulItemsLearned.ClientID %>").value;
        if (TextBoxUsefulItemsLearned == "") {
            msg = msg + " Question 3 is not completed. \n";
            completed = false;
        }

        var CheckBoxBarrierNotEnoughTime = document.getElementById("<%= CheckBoxBarrierNotEnoughTime.ClientID %>").checked;
        var CheckBoxBarrierClinicalKnowledgeSkillExpertise = document.getElementById("<%= CheckBoxBarrierClinicalKnowledgeSkillExpertise.ClientID %>").checked;
        var CheckBoxBarrierRecallConfidenceClinicalInertia = document.getElementById("<%= CheckBoxBarrierRecallConfidenceClinicalInertia.ClientID %>").checked;
        var CheckBoxBarrierCulturalCompetency = document.getElementById("<%= CheckBoxBarrierCulturalCompetency.ClientID %>").checked;
        var CheckBoxBarrierFearLegalConcerns = document.getElementById("<%= CheckBoxBarrierFearLegalConcerns.ClientID %>").checked;
        var CheckBoxBarrierStaffCompetence = document.getElementById("<%= CheckBoxBarrierStaffCompetence.ClientID %>").checked;
        var CheckBoxBarrierStaffMotivation = document.getElementById("<%= CheckBoxBarrierStaffMotivation.ClientID %>").checked;
        var CheckBoxBarrierOther = document.getElementById("<%= CheckBoxBarrierOther.ClientID %>").checked;
        var TextBoxImpactOthersDescription = document.getElementById("<%= TextBoxImpactOthersDescription.ClientID %>").checked;
        var CheckBoxBarrierCostFunding = document.getElementById("<%= CheckBoxBarrierCostFunding.ClientID %>").checked;
        var CheckBoxBarrierPatientCharacteristicsFactors = document.getElementById("<%= CheckBoxBarrierPatientCharacteristicsFactors.ClientID %>").checked;
        var CheckBoxBarrierWorkOverload = document.getElementById("<%= CheckBoxBarrierWorkOverload.ClientID %>").checked;
        var CheckBoxBarrierTeamStructure = document.getElementById("<%= CheckBoxBarrierTeamStructure.ClientID %>").checked;
        var CheckBoxBarrierPracticeProcess = document.getElementById("<%= CheckBoxBarrierPracticeProcess.ClientID %>").checked;
        var CheckBoxBarrierReferralProcess = document.getElementById("<%= CheckBoxBarrierReferralProcess.ClientID %>").checked;
        var CheckBoxBarrierPeerInfluence = document.getElementById("<%= CheckBoxBarrierPeerInfluence.ClientID %>").checked;
        var CheckBoxBarrierPatientAdherence = document.getElementById("<%= CheckBoxBarrierPatientAdherence.ClientID %>").checked;

        if (!((CheckBoxBarrierNotEnoughTime) || (CheckBoxBarrierClinicalKnowledgeSkillExpertise) || (CheckBoxBarrierRecallConfidenceClinicalInertia)
            || (CheckBoxBarrierCulturalCompetency) || (CheckBoxBarrierFearLegalConcerns) || (CheckBoxBarrierStaffCompetence)
            || (CheckBoxBarrierStaffMotivation) || (CheckBoxBarrierOther) || (CheckBoxBarrierCostFunding)
            || (CheckBoxBarrierPatientCharacteristicsFactors) || (CheckBoxBarrierWorkOverload) || (CheckBoxBarrierTeamStructure)
            || (CheckBoxBarrierPracticeProcess) || (CheckBoxBarrierReferralProcess) || (CheckBoxBarrierPatientAdherence)
            || (CheckBoxBarrierPatientAdherence))) {
            msg = msg + " Question 4 is not completed. \n";
            completed = false;
        }

        var inputElements = document.getElementsByTagName("INPUT");
        // Clinical usefulness overall
        for (var i = 0; i < inputElements.length; i++) {
            if ((inputElements[i].type == 'radio') && (inputElements[i].name.indexOf("ClinicalUsefulnessOverall") !== -1)) {
                if (inputElements[i].checked == true) {
                    total++;
                    break;
                }
            }
        }
        //Objective Presentation Of
        for (var i = 0; i < inputElements.length; i++) {
            if ((inputElements[i].type == 'radio') && (inputElements[i].name.indexOf("ObjectivePresentationOf") !== -1)) {
                if (inputElements[i].checked == true) {
                    total++;
                    break;
                }
            }
        }
        // Educational Interventions
        for (var i = 0; i < inputElements.length; i++) {
            if ((inputElements[i].type == 'radio') && (inputElements[i].name.indexOf("EducationalInterventions") !== -1)) {
                if (inputElements[i].checked == true) {
                    total++;
                    break;
                }
            }
        }
        // Scientific Rigor
        for (var i = 0; i < inputElements.length; i++) {
            if ((inputElements[i].type == 'radio') && (inputElements[i].name.indexOf("ScientificRigor") !== -1)) {
                if (inputElements[i].checked == true) {
                    total++;
                    break;
                }
            }
        }
        // Free Of Commercial Bias
        for (var i = 0; i < inputElements.length; i++) {
            if ((inputElements[i].type == 'radio') && (inputElements[i].name.indexOf("FreeOfCommercialBias") !== -1)) {
                if (inputElements[i].checked == true) {
                    total++;
                    break;
                }
            }
        }
        // Usefulness Of Conducting SelfAssessment
        for (var i = 0; i < inputElements.length; i++) {
            if ((inputElements[i].type == 'radio') && (inputElements[i].name.indexOf("UsefulnessOfConductingSelfAssessment") !== -1)) {
                if (inputElements[i].checked == true) {
                    total++;
                    break;
                }
            }
        }
        // Usefulness Of Conducting Chart Audit
        for (var i = 0; i < inputElements.length; i++) {
            if ((inputElements[i].type == 'radio') && (inputElements[i].name.indexOf("UsefulnessOfConductingChartAudit") !== -1)) {
                if (inputElements[i].checked == true) {
                    total++;
                    break;
                }
            }
        }
        // Usefulness Of Feedback Provided
        for (var i = 0; i < inputElements.length; i++) {
            if ((inputElements[i].type == 'radio') && (inputElements[i].name.indexOf("UsefulnessOfFeedbackProvided") !== -1)) {
                if (inputElements[i].checked == true) {
                    total++;
                    break;
                }
            }
        }
        if (total != 7) {
            msg = msg + " Question 5 is not completed. \n";
            completed = false;
        }

        if (completed == 1) {
            return true;
        } else {
            alert(msg);
            return false;
        }
    }

</script>

</asp:Content>