<%@ Page Title="" Language="C#" MasterPageFile="~/PIMMasterPage.master" AutoEventWireup="true" CodeFile="CurrentStatus.aspx.cs" Inherits="copd_CurrentStatus" %>

<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="Server">

<style>
    .action-box {
        -moz-box-sizing: border-box;
        -webkit-box-sizing: border-box;
        box-sizing: border-box;
        float:left;
        width: 100%;
        text-align:center;
    }
</style>

</asp:Content>
<asp:Content ID="Content3" ContentPlaceHolderID="ContentPlaceHolder1" runat="Server">

<div class="action-box">
	<p>
        You have completed your Assessment and Improvement Plan.
        <br />You are now in your Improvement Phase.
	</p>
    <p>
        This phase of your activity started on <strong><asp:Label runat="server" ID="date">11/26/2013</asp:Label></strong>.
        <br />(The date you submitted your Improvement Plan) and will end on <asp:Label runat="server" ID="seconddate">1/26/2013</asp:Label>.
        <br />At that time, you may begin entering charts for your second abstraction.
    </p>
    <p>
        NOTE: Your Improvement Phase period is calculated based upon the time needed for the longest measure in your selected activity(s) plus one month.
    </p>
    <p>
        To review your plan and access any resources selected, <asp:HyperLink runat="server" NavigateUrl="#link">CLICK HERE</asp:HyperLink>
    </p>
</div>
</asp:Content>