﻿<%@ Page Title="" Language="C#" MasterPageFile="~/PIMMasterPage.master" AutoEventWireup="true" CodeFile="BusinessAssociateAgreement.aspx.cs" Inherits="abo_BusinessAssociateAgreement" %>

<asp:Content ID="Content1" ContentPlaceHolderID="head" Runat="Server">

    <style type="text/css">

        li {
            margin-bottom:16px !important;
        }

        label {
            padding-left: 5px;
        }
    </style>
</asp:Content>

<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder1" Runat="Server">
    <script type="text/javascript">
        $(document).ready(function () {
            $("#divPhaseI").removeAttr("onclick");
            $("#divPhaseII").removeAttr("onclick");
            $("#divPhaseIII").removeAttr("onclick");
            $("#divPhaseIV").removeAttr("onclick");
        });
    </script>
<p>
    Under the U.S. Health Insurance Portability and Accountability Act of 1996, a <a href="http://searchdatamanagement.techtarget.com/definition/HIPAA" target="_blank">HIPAA</a> business associate agreement (BAA) is a contract between a <a href="http://searchcompliance.techtarget.com/definition/HIPAA-covered-entity" target="_blank">HIPAA covered entity</a> and a HIPAA business associate (<a href="http://searchsecurity.techtarget.com/definition/business-associate" target="_blank">BA</a>). The contract protects personal health information (<a href="http://searchhealthit.techtarget.com/definition/personal-health-information" target="_blank">PHI</a>) in accordance with HIPAA guidelines. Please review carefully.
</p>
<hr />
    <h2>Agreement</h2>
    <div style="height:500px;  border: 2px solid #D0D0D0;
    padding: 10px; overflow-y:scroll ">
<ol  style="list-style-type:upper-roman;">
    <li><span><strong>
        DEFINITIONS</strong>
        <br /><br /><ol  style="list-style-type:upper-alpha;">
            <li><span><strong>Business Associate.</strong> &quot;Business Associate&quot; shall have the meaning given to such term under the Privacy and Security Rules, including, but not limited to, 45 C.F.R. &sect; 160.103, and in this case shall include ABO, NetHealth, LLC, and their respective affiliates.</span></li>
            <li><span><strong>Covered Entity.</strong> &quot;Covered Entity&quot; shall have the meaning given to such term under the Privacy and Security Rules, including, but not limited to, 45 C.F.R. &sect; 160.103, and in this case shall include any user of the Website to which this Addendum relates.</span></li>
            <li><span><strong>Designated Record Set.</strong> &quot;Designated Record Set&quot; shall have the meaning given to such term under the Privacy Rule, codified at 45 C.F.R. &sect; 164.501.</span></li>
            <li><span><strong>Electronic Protected Health Information or &quot;EPHI&quot;.</strong> &quot;Electronic protected health information&quot; or &quot;EPHI&quot; shall have the same meaning given to such term under the Security Rule, including, but not limited to, 45 C.F.R. &sect; 160.103.</span></li>
            <li><span><strong>HIPAA.</strong> &quot;HIPAA&quot; shall mean the Health Insurance Portability and Accountability Act of 1996, as amended, specifically including the Health Information Technology Economic and Clinical Health Act (&quot;HITECH&quot;), and any regulations.</span></li>
            <li><span><strong>Individual.</strong> &quot;Individual&quot; shall have the meaning given to such term under the Privacy Rule, codified at 45 C.F.R. &sect; 160.103, and shall include a person who qualifies as a personal representative in accordance with 45 C.F.R. &sect; 164.502(g).</span></li>
            <li><span><strong>Privacy Rule.</strong> &quot;Privacy Rule&quot; shall mean the Standards for Privacy of Individually Identifiable Health Information, codified at 45 C.F.R. Parts 160 and 164, subparts A and E.</span></li>
            <li><span><strong>Privacy and Security Rules.</strong> &quot;Privacy and Security Rules&quot; shall mean the federal regulations set forth at 45 C.F.R. Parts 160 and 164 issued pursuant to HIPAA.</span></li>
            <li><span><strong>Protected Health Information or &quot;PHI&quot;.</strong> &quot;Protected Health Information&quot; or &quot;PHI&quot; shall have the meaning given to such term under the Privacy and Security Rules, codified at 45 C.F.R. &sect; 160.103.</span></li>
            <li><span><strong>Required by Law.</strong> &quot;Required by Law&quot; shall have the meaning given to such term under the Privacy Rule, codified at 45 C.F.R. &sect; 164.103.</span></li>
            <li><span><strong>Security Rule.</strong> &quot;Security Rule&quot; shall mean the Security Standards for the Protection of Electronic Protected Health Information, codified at 45 C.F.R. &sect; 164 Subparts A and C.</span></li>
            <li><span><strong>Secretary.</strong> &quot;Secretary&quot; shall mean the Secretary of the United States Department of Health and Human Services or his or her designee.</span></li>
            <li><span>Other terms used, but not otherwise defined, in this Addendum shall have the same meaning as those terms in the Privacy and Security Rules.</span></li>
        </ol>
    </span></li>
    <li><span><strong>
        OBLIGATIONS AND ACTIVITIES OF BUSINESS ASSOCIATE</strong>
        <br /><br /><ol  style="list-style-type:upper-alpha;">
            <li><span><strong>Limitations on Disclosure.</strong> Business Associate agrees to not use or disclose PHI other than as permitted or required by this Addendum or as Required by Law. Business Associate shall not use or disclose PHI in a manner that would violate the Privacy Rule if done by Covered Entity, unless expressly permitted to do so pursuant to the Privacy Rule and this Addendum.</span></li>
            <li><span><strong>Safeguards.</strong> Business Associate agrees to use appropriate safeguards to prevent use or disclosure of PHI other than as provided for by this Addendum or as required by law.</span></li>
            <li><span><strong>Mitigation.</strong> Business Associate agrees to mitigate, to the extent practicable, any harmful effect that is known to Business Associate of a use or disclosure of PHI by Business Associate in violation of the requirements of this Addendum.</span></li>
            <li><span><strong>Reporting of Disclosures.</strong> Business Associate agrees to report to Covered Entity any use or disclosure of PHI not provided for by this Addendum upon becoming aware of such disclosure.</span></li>
            <li><span><strong>Agents and Subcontractors.</strong> Business Associate agrees to ensure that any agent, including a subcontractor, to whom it provides PHI received from, or created or received by Business Associate on behalf of Covered Entity, agrees to the same restrictions and conditions that apply through this Addendum to Business Associate with respect to such information.</span></li>
            <li><span><strong>Access.</strong> To the extent Business Associate has PHI in a Designated Record Set, Business Associate agrees to provide access to Covered Entity, at the request of Covered Entity, to PHI in a Designated Record Set, in order to meet the requirements under 45 C.F.R. &sect; 164.524.</span></li>
            <li><span><strong>Amendment.</strong> To the extent Business Associate has PHI in a Designated Record Set and to the extent applicable, Business Associate agrees to make PHI in a Designated Record Set available to Covered Entity for purposes of amendment, per 45 C.F.R. &sect; 164.526.</span></li>
            <li><span><strong>Accounting.</strong> To the extent applicable, Business Associate agrees to document disclosures of PHI and information related to such disclosures as would be required for Covered Entity to respond to a request by an Individual for an accounting of disclosures of PHI in accordance with 45 C.F.R. &sect; 164.528.</span></li>
            <li><span><strong>Availability of Books and Records.</strong> Business Associate agrees to make internal practices, books, and records, including policies and procedures and PHI, relating to the use and disclosure of PHI received from, or created or received by Business Associate on behalf of, Covered Entity available to the Secretary, in a time and manner designated by the Secretary, for purposes of the Secretary determining Covered Entity’s compliance with the Privacy Rule.</span></li>
            <li><span><strong>Breach.</strong> Following the discovery a breach of unsecured PHI as defined in HITECH, Business Associate shall notify Covered Entity without unreasonable delay and in no later than forty five (45) days, in a manner consistent with HITECH, of the discovery of such breach and the identification of each individual whose unsecured PHI has been, or is reasonably believed by Business Associate to have been accessed, acquired or disclosed during such breach.</span></li>
            <li><span><strong>Security.</strong> Business Associate shall secure all EPHI by a technology standard that renders PHI unusable, unreadable, or indecipherable to unauthorized individuals and is developed or endorsed by a standards developing organization that is accredited by the American National Standards Institute and is consistent with guidance issued by the Secretary specifying the technologies and methodologies that render PHI unusable, unreadable, or indecipherable to unauthorized individuals.</span></li>
        </ol>    
    </span></li>
    <li><span><strong>
        PERMITTED USES AND DISCLOSURES BY BUSINESS ASSOCIATE</strong>
        <br /><br /><ol  style="list-style-type:upper-alpha;">
            <li><span><strong>Uses and Disclosures of PHI.</strong> Except as provided in Paragraphs B, C, D and E, below in this Section III, Business Associate may only use or disclose PHI to perform functions, activities, or services for, or on behalf of, Covered Entity to provide the features, information and services offered through ABO and for the purposes of analysis, research and publication.</span></li>
            <li><span><strong>Use for Management and Administration.</strong> Except as otherwise limited in this Addendum, Business Associate may, consistent with 45 C.F.R. &sect; 164.504(e)(4), use PHI if necessary (i) for the proper management and administration of the Business Associate, or (ii) to carry out the legal responsibilities of the Business Associate.</span></li>
            <li><span><strong>Disclosure for Management and Administration.</strong> Except as otherwise limited in this Addendum, Business Associate may, consistent with 45 C.F.R. &sect; 164.504(e)(4), disclose PHI for the proper management and administration of the Business Associate, provided that (i) the disclosures are Required By Law, or (ii) Business Associate obtains reasonable assurances from the person to whom the information is disclosed (&quot;Person&quot;) that it will remain confidential and be used or further disclosed only as Required By Law or for the purpose for which it was disclosed to the Person, and the Person notifies the Business Associate in writing of any instances of which it becomes aware in which the confidentiality of the information has been breached.</span></li>
            <li><span><strong>Data Aggregation.</strong> Except as otherwise limited in this Addendum, Business Associate may use PHI to provide Data Aggregation services as permitted by 42 C.F.R. &sect; 164.504(e)(2)(i)(B).</span></li>
            <li><span><strong>De-Identification.</strong> Business Associate may de-identify PHI received from Covered Entity, consistent with the Privacy Rule’s standards for de-identification in 45 C.F.R. &sect; 164.514.</span></li>
            <li><span><strong>Reporting Violations.</strong> Business Associate may use PHI to report violations of law to appropriate Federal and State authorities, consistent with 42 C.F.R. &sect; 164.502(j)(l).</span></li>
        </ol>
    </span></li>
    <li><span><strong>
        SECURITY RULE OBLIGATIONS</strong>
        <br /><br /><ol  style="list-style-type:upper-alpha;">
            <li><span><strong>Business Associate Obligations.</strong> Business Associate shall implement the requirements set forth in this Section IV with regard to EPHI.</span></li>
            <li><span><strong>Safeguards.</strong> Business Associate shall have in place Administrative, Physical, and Technical Safeguards that reasonably and appropriately protect the confidentiality, integrity, and availability of the EPHI that it creates, receives, maintains or transmits on behalf of Covered Entity pursuant to the Addendum.</span></li>
            <li><span><strong>Subcontractors.</strong> Business Associate shall ensure that any agent, including a subcontractor, to whom it provides EPHI agrees to implement reasonable and appropriate safeguards to protect such EPHI.</span></li>
            <li><span><strong>Security Incident Reporting.</strong> Business Associate shall report any Security Incident promptly upon becoming aware of such incident.</span></li>
        </ol>
    </span></li>
    <li><span><strong>
        TERM AND TERMINATION</strong>
        <br /><br /><ol  style="list-style-type:upper-alpha;">
            <li><span><strong>Term.</strong> The Term of this Addendum shall terminate when all of the PHI provided by Covered Entity to Business Associate, or created or received by Business Associate on behalf of Covered Entity, is destroyed or returned to Covered Entity, or, if it is infeasible to return or destroy PHI (as provided in Paragraph V(C) below), protections are extended to such information, in accordance with the termination provisions in this Section.</span></li>
            <li><span><strong>
                Termination for Cause.</strong> Upon Covered Entity’s knowledge of a material breach of the terms of this Addendum by Business Associate, Covered Entity:
                <br /><br /><ol  style="list-style-type:decimal;">
                    <li><span>Shall provide an opportunity for Business Associate to cure, and, if Business Associate does not cure the breach within 30 days, Covered Entity may immediately terminate this Addendum;</span></li>
                    <li><span>May immediately terminate this Addendum if Covered Entity has determined and provided written notice to Business Associate that (a) Business Associate has breached a material term of this Addendum, and (b) cure is not possible; or</span></li>
                    <li><span>If Covered Entity determines that neither termination nor cure are feasible and provides written notice to Business Associate regarding the same, Covered Entity shall report the violation to the Secretary.</span></li>
                </ol>
            </span></li>
            <li><span><strong>
                Effect of Termination.</strong>
                <br /><br /><ol  style="list-style-type:decimal;">
                    <li><span>Except as provided below in Paragraph 2 of this Section C, upon termination of this Addendum, for any reason, Business Associate shall return or destroy all PHI received from Covered Entity, or created or received by Business Associate on behalf of Covered Entity. This provision shall apply to PHI that is in the possession of subcontractors or agents of Business Associate and Business Associate is obligated to ensure that such PHI is returned or destroyed consistent with this Addendum. Business Associate and its subcontractors or agents shall retain no copies of the PHI.</span></li>
                    <li><span>Where Business Associate asserts that returning or destroying the PHI is infeasible, Business Associate shall provide to Covered Entity notification of the conditions that make return or destruction infeasible. Upon Business Associate’s good faith representations that return or destruction of PHI is infeasible, Business Associate shall extend the protections of this Addendum to such PHI and limit further uses and disclosures of such PHI to those purposes that make the return or destruction infeasible, for so long as Business Associate maintains such PHI.</span></li>
                </ol>
            </span></li>
        </ol>
    </span></li>
    <li><span><strong>
        MISCELLANEOUS</strong>
        <br /><br /><ol  style="list-style-type:upper-alpha;">
            <li><span><strong>Regulatory References.</strong> A reference in this Addendum to a section in the Privacy or Security Rule means the section as in effect at the relevant time.</span></li>
            <li><span><strong>No Third Party Beneficiaries.</strong> Nothing expressed or implied in this Addendum is intended to confer, nor shall anything herein confer, upon any person other than Covered Entity and Business Associate and their respective successors and assigns, any rights, remedies, obligations or liabilities whatsoever.</span></li>
            <li><span><strong>Disclaimer.</strong> Business Associate expressly disclaims that it is subject to HIPAA and/or the Privacy and Security Rules, since it is not a &quot;Covered Entity&quot; as that term is defined under HIPAA, but may be subject to HIPAA from time to time with respect to its duties as a Business Associate of the Covered Entity. Business Associate makes no warranty or representation that compliance by Covered Entity with this Addendum is satisfactory for Covered Entity to comply with any obligations it may have under HIPAA, the Privacy and Security Rules Rule, or any other applicable law or regulation pertaining to the confidentiality, use or safeguarding of health information. Covered Entity is solely responsible for all decisions it makes regarding the use, disclosure or safeguarding of PHI.</span></li>
            <li><span><strong>Amendment.</strong> The parties agree to take such action as is reasonably necessary to amend this HIPAA Addendum from time to time as is necessary to comply with the requirements of HIPAA as they may be amended from time to time.</span></li>
            <li><span><strong>Notice.</strong>  Any written notice required by this Addendum by Covered Entity shall be provided via registered or overnight mail to ABO to the attention of Administrator, 111 Presidential Boulevard, Suite 241, Bala Cynwyd, PA  19004.</span></li>
        </ol>    
    </span></li>
</ol>
</div>
<div class="button-box"><asp:LinkButton ID="ButtonSubmit" runat="server" Text="I agree to the BAA terms stated above" OnClick="ButtonSubmit_Click" CssClass="button" /></div>

</asp:Content>

