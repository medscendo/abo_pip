﻿<%@ Page Title="" Language="C#" MasterPageFile="~/PIMMasterPage.master" AutoEventWireup="true" CodeFile="PreviousChartsStatus.aspx.cs" Inherits="abo_PreviousChartsStatus" %>

<asp:Content ID="Content1" ContentPlaceHolderID="head" Runat="Server">
<style>
        .chart-info {
            margin-top:10px;
            border:1px solid #12b0f1;
            border-radius:5px;
            -webkit-border-radius:5px;
        }

        .chart-info table {
            width:96%;
            margin: 1% 2%;
            border-collapse:collapse;
        }

        .chart-info td, .chart-info th {
            padding: 3px 5px;
        }

        .chart-info th {
            border-top: 1px solid #ccc;
            color:#fff;
            background: #2dabd4; /* Old browsers */
            background: -moz-linear-gradient(top,  #2dabd4 0%, #023c6a 100%); /* FF3.6+ */
            background: -webkit-gradient(linear, left top, left bottom, color-stop(0%,#2dabd4), color-stop(100%,#023c6a)); /* Chrome,Safari4+ */
            background: -webkit-linear-gradient(top,  #2dabd4 0%,#023c6a 100%); /* Chrome10+,Safari5.1+ */
            background: -o-linear-gradient(top,  #2dabd4 0%,#023c6a 100%); /* Opera 11.10+ */
            background: -ms-linear-gradient(top,  #2dabd4 0%,#023c6a 100%); /* IE10+ */
            background: linear-gradient(to bottom,  #2dabd4 0%,#023c6a 100%); /* W3C */
            filter: progid:DXImageTransform.Microsoft.gradient( startColorstr='#2dabd4', endColorstr='#023c6a',GradientType=0 ); /* IE6-9 */
        }

        .chart-info .left-col {
            border-bottom: 1px solid #ccc;
            border-right: 1px solid #ccc;    
        }

        .chart-info .mid-col {
            border-bottom: 1px solid #ccc;
        }

        .chart-info .right-col {
            border-left: 1px solid #ccc;
            border-bottom: 1px solid #ccc;
        }

        .chart-info .description {
            margin: 1% 2%;
        }
    </style>
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder1" Runat="Server">
        <!-- main content -->
        <div class="main_content">
                    <div class="heading_box">
            <h2><strong>Status:</strong> <span class="red"><asp:Literal ID="LiteralStatus" runat="server">Pending</asp:Literal></span></h2>
            <div class="top_nav">
            <ul>
            <li class="active"><a href="#"><span>Status</span></a></li>
             <li><a href="PIMTutorial.aspx"><span>Improvement in Medical Practice Tutorial</span></a></li>          
             <li><a href="ModuleProcess.aspx"><span>Process Overview</span></a></li>           
            </ul>
            </div>
          </div>
          <div class="status_section">
          
          <div class="status_right_section">
          <h3>Improvement in Medical Practice Platform</h3>
          
          
          <div runat="server" id="PreviousChartsInfo" visible="false" class="chart-info" style="clear:both;">
            <p class="description">
                <strong>Activity Status</strong>
                <br />Below is a list of the activities you originally selected and assessed.
                <br />You may click on any of these to view your data.
            </p>
            <table>
                <tr>
                    <th class="left-col">Activity Name</th>
                    <th class="mid-col">Number of Charts Abstracted</th>
                    <th class="right-col">Initial Assessment Completed Date</th>
                </tr>
                <asp:Repeater runat="server" ID="repeaterModules">
                <ItemTemplate>
                    <tr>
                        <td class="left-col"><asp:LinkButton ID="LinkButton1" runat="server" OnCommand="LinkButtonSelectModule_Click" CommandArgument='<%# Eval("ModuleID")%>'><%# Eval("ModuleName")%></asp:LinkButton> </td>
                        <td class="mid-col"><%# Eval("NumberOfCharts")%></td>
                        <td class="right-col"><%# Eval("AbstractDataCompletedDate")%></td>
                    </tr>
                </ItemTemplate>
                </asp:Repeater>
            </table>
            </div>
           </div>
        <a href="PIPStatus.aspx">Back To Main Page</a>
        </div>
    </div>
</asp:Content>

