﻿<%@ Page Language="C#" AutoEventWireup="true" CodeFile="ConfirmationChartAbstraction.aspx.cs" Inherits="abo_ConfirmationChartAbstraction" %>

<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">

<html xmlns="http://www.w3.org/1999/xhtml">
<head id="Head1" runat="server">
<title>Confirm Chart&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;</title>
<style type="text/css">
    body, div, ul, li, h1, h2, h3, h4, h5, h6, form, fieldset, p { padding:0; margin:0; font-weight:normal; }
    body { font-family:Arial, Helvetica, sans-serif; color:#616265; font-size:12px; }
    .maincontent form{width:100%; float:left;}
    .bginput{ float:left; margin: 20px 5px 0 0px; background: url(../common/images/big_orange_button.png) no-repeat; overflow:hidden; height:29px;}
    .bginput a{ color: white; text-align:left; height:29px; line-height:26px; padding:0 14px 10px; cursor:pointer; float:left; border:none; text-decoration:none; font-family:Arial, Helvetica, sans-serif; font-size:12px; font-weight:bold; letter-spacing: 0px;}
    .bginput a:hover{ text-decoration:none; }
</style>
<script type="text/javascript">
function closeForm(retValue) 
{
    window.returnValue = retValue;
    window.close();
} 
</script>
</head>
<body>
    <form id="form1" runat="server">
    <div>
        <table>
            <tr>
                <td colspan="2">
                    <p>Updates saved</p>
                </td>
            </tr>
            <tr>
                <td colspan="2">
                    <p>Chart is not complete:</p>
                </td>
            </tr>
            <tr>
                <td>
                    <div class="bginput"><a href="#" onclick="closeForm(1)" style="width:90px;" >Return to Chart</a></div>
                </td>
                <td>
                    <div class="bginput"><a href="#" onclick="closeForm(0)" style="width:50px;" >Continue</a></div>
                </td>
            </tr>
        </table>
    
    </div>
    </form>
</body>
</html>
