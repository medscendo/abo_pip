﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Web.UI.DataVisualization.Charting;
using NetHealthPIMModel;
using System.Transactions;

public partial class abo_ImpactStatement : BasePage
{
    bool viewMode;
    static int MeasureGroupID = 0;
    static int PreviousMeasureGroupID = -1;

    protected void Page_Load(object sender, EventArgs e)
    {
        if (!IsPostBack)
        {
            InitializePage();
        }

    }
    protected void InitializePage()
    {
        int cycleID = ctx.ActiveModuleCycleID;
  
        string breadCrumb = Constants.BC_DASHBOARD_ANALYSIS_LINK + "" + Constants.BC_PERFORMANCE_REPORT_LINK + "" + Constants.BC_IMPACT_STATEMENT;
        ((PIMMasterPage)Page.Master).SetTitle(breadCrumb);
        ((PIMMasterPage)Page.Master).SetStatus(3);
        ((PIMMasterPage)Page.Master).SetHeader("Develop Impact Statement");
        using (NetHealthPIMEntities pim = new NetHealthPIMEntities())
        {

            // make sure the ALL charts are completed
            var chartsCountMissing = (from chart in pim.ParticipantChartStatus_V
                                 where chart.ParticipantModuleCycleID == ctx.ActiveModuleCycleID
                                 && chart.TotalCharts != chart.Completed
                                 select chart).Count();
            if (chartsCountMissing > 0)
            {
                Response.Redirect("Dashboard3.aspx?is=0");
            }

            ParticipantModuleCycle cycle = pim.ParticipantModuleCycle.First(c => c.ParticipantModuleCycleID == cycleID);
            int measureAggregateTypeID;
            measureAggregateTypeID = Constants.MEASURE_AGGREGATE_TYPE_PEER;
            var report = from pir in pim.ParticipantInitialReportImpactStatement_V
                         where pir.ParticipantModuleCycleID == cycleID
                            && (pir.MeasureTypeID == 3 || pir.MeasureTypeID == 5)
                            && pir.MeasureAggregateTypeID == measureAggregateTypeID
                            && pir.ParticipantMeasureTypeID == 1
                            && pir.MeasureGoal > 0
                         orderby pir.ModuleID, pir.MeasureGroupSortOrder
                         select new PerformanceReportData
                         {
                             MeasureID = pir.MeasureID,
                             MeasureGroupSortOrder = (int)pir.MeasureGroupSortOrder,
                             MeasureQualityIndicator = pir.MeasureQualityIndicator,
                             MeasureTitle = pir.MeasureTitle,
                             MeasureClinicalRecommendation = pir.MeasureClinicalRecommendation,
                             MeasureLongDescription = pir.MeasureLongDescription,
                             MeasureComputationMethod = pir.MeasureComputationMethod,
                             AbstractRecordsIncluded = (int)pir.AbstractRecordsIncluded,
                             MeasurePercentCurrent = pir.MeasurePercentCurrent,
                             MeasurePercentPrevious = pir.MeasurePercentPrevious,
                             PeerData = pir.PeerData,
                             SelfData = pir.SelfData,
                             IsPhase2Flag = true,
                             IncludeInImprovementPlan = pir.IncludeInImprovementPlan,
                             MeasureGoal = pir.MeasureGoal == null ? 0 : pir.MeasureGoal
                         };
            ListViewProcessMeasures.DataSource = report;

            var reportOutcomeMeasures = from pir in pim.ParticipantInitialReport_OutcomeMeasures_ImpactStatement_V
                                        where pir.ParticipantModuleCycleID == cycleID
                                        && (pir.ParticipantMeasureTypeID == 3 || pir.ParticipantMeasureTypeID == 1 || pir.ParticipantMeasureTypeID == 5)
                                        && pir.MeasureGoal > 0
                                        orderby pir.ModuleID, pir.MeasureGroupSortOrder
                                        select new PerformanceReportData
                                        {
                                            MeasureID = pir.MeasureID,
                                            MeasureQualityIndicator = pir.MeasureQualityIndicator,
                                            MeasureTitle = pir.MeasureTitle,
                                            MeasureClinicalRecommendation = pir.MeasureClinicalRecommendation,
                                            MeasureLongDescription = pir.MeasureLongDescription,
                                            MeasureComputationMethod = pir.MeasureComputationMethod,
                                            AbstractRecordsIncluded = (int)pir.AbstractRecordsIncluded,
                                            prevMeasurePercent3To6 = pir.prevMeasurePercent3To6,
                                            MeasurePercent3To6 = pir.MeasurePercent3To6,
                                            PeerData3To6 = pir.PeerData3To6,
                                            prevMeasurePercent7To12 = pir.prevMeasurePercent7To12,
                                            MeasurePercent7To12 = pir.MeasurePercent7To12,
                                            PeerData7To12 = pir.PeerData7To12,
                                            prevMeasurePercentNewOnset = pir.prevMeasurePercentNewOnset,
                                            MeasurePercentNewOnset = pir.MeasurePercentNewOnset,
                                            PeerDataNewOnset = pir.PeerDataNewOnset,
                                            prevMeasurePercentRecurrence = pir.prevMeasurePercentRecurrence,
                                            MeasurePercentRecurrence = pir.MeasurePercentRecurrence,
                                            PeerDataRecurrence = pir.PeerDataRecurrence,
                                            SelfData = pir.SelfData,
                                            IsPhase2Flag = true,
                                            IncludeInImprovementPlan = pir.IncludeInImprovementPlan,
                                            MeasureGoal = pir.MeasureGoal == null ? 0 : pir.MeasureGoal
                                        };

            ListViewOutcomeMeasures.DataSource = reportOutcomeMeasures;

            DataBind();

            cycle.PrevParticipantModuleCycleReference.Load();
            ParticipantModuleCycle prev = cycle.PrevParticipantModuleCycle;

            if (prev.ImportantChangesComments != null)
                LiteralImportantChangesComments.Text = prev.ImportantChangesComments;

            var count = pim.ParticipantImpactStatement.Where(ps => ps.ParticipantModuleCycleID == cycleID).Count();
            if (count > 0)
            {
                ParticipantImpactStatement impact = pim.ParticipantImpactStatement.First(ps => ps.ParticipantModuleCycleID == cycleID);

                TextBoxImportanceOfChanges.Text = impact.ImportanceOfChanges;
                LiteralImportanceOfChanges.Text = impact.ImportanceOfChanges;
                TextBoxUsefulItemsLearned.Text = impact.UsefulItemsLearned;
                LiteralUsefulItemsLearned.Text = impact.UsefulItemsLearned;
                TextBoxAdditionalComments.Text = impact.AdditionalComments;
                LiteralAdditionalComments.Text = impact.AdditionalComments;

                CheckBoxBarrierNotEnoughTime.Checked = (bool)impact.BarrierNotEnoughTime;
                CheckBoxBarrierClinicalKnowledgeSkillExpertise.Checked = (bool)impact.BarrierClinicalKnowledgeSkillExpertise;
                CheckBoxBarrierRecallConfidenceClinicalInertia.Checked = (bool)impact.BarrierRecallConfidenceClinicalInertia;
                CheckBoxBarrierPeerInfluence.Checked = (bool)impact.BarrierPeerInfluence;
                CheckBoxBarrierCulturalCompetency.Checked = (bool)impact.BarrierCulturalCompetency;
                CheckBoxBarrierFearLegalConcerns.Checked = (bool)impact.BarrierFearLegalConcerns;
                CheckBoxBarrierStaffCompetence.Checked = (bool)impact.BarrierStaffCompetence;
                CheckBoxBarrierStaffMotivation.Checked = (bool)impact.BarrierStaffMotivation;
                CheckBoxBarrierCostFunding.Checked = (bool)impact.BarrierCostFunding;
                CheckBoxBarrierPatientCharacteristicsFactors.Checked = (bool)impact.BarrierPatientCharacteristicsFactors;
                CheckBoxBarrierPatientAdherence.Checked = (bool)impact.BarrierPatientAdherence;
                CheckBoxBarrierWorkOverload.Checked = (bool)impact.BarrierWorkOverload;
                CheckBoxBarrierTeamStructure.Checked = (bool)impact.BarrierTeamStructure;
                CheckBoxBarrierPracticeProcess.Checked = (bool)impact.BarrierPracticeProcess;
                CheckBoxBarrierReferralProcess.Checked = (bool)impact.BarrierReferralProcess;
                CheckBoxBarrierOther.Checked = (bool)impact.BarrierOther;
                if (impact.ImpactOthersDescription!=null)
                     TextBoxImpactOthersDescription.Text = impact.ImpactOthersDescription;

                if (impact.ClinicalUsefulnessOverall != null)
                {
                    RadioButton radio = (RadioButton)Master.FindControl("ContentPlaceHolder1").FindControl("RadioButtonClinicalUsefulnessOverall" + impact.ClinicalUsefulnessOverall);
                    radio.Checked = true;
                }
                if (impact.ObjectivePresentationOf != null)
                {
                    RadioButton radio = (RadioButton)Master.FindControl("ContentPlaceHolder1").FindControl("RadioButtonObjectivePresentationOf" + impact.ObjectivePresentationOf);
                    radio.Checked = true;
                }
                if (impact.ScientificRigor != null)
                {
                    RadioButton radio = (RadioButton)Master.FindControl("ContentPlaceHolder1").FindControl("RadioButtonScientificRigor" + impact.ScientificRigor);
                    radio.Checked = true;
                }
                if (impact.FreeOfCommercialBias != null)
                {
                    RadioButton radio = (RadioButton)Master.FindControl("ContentPlaceHolder1").FindControl("RadioButtonFreeOfCommercialBias" + impact.FreeOfCommercialBias);
                    radio.Checked = true;
                }
                if (impact.UsefulnessOfConductingSelfAssessment != null)
                {
                    RadioButton radio = (RadioButton)Master.FindControl("ContentPlaceHolder1").FindControl("RadioButtonUsefulnessOfConductingSelfAssessment" + impact.UsefulnessOfConductingSelfAssessment);
                    radio.Checked = true;
                }
                if (impact.UsefulnessOfConductingChartAudit != null)
                {
                    RadioButton radio = (RadioButton)Master.FindControl("ContentPlaceHolder1").FindControl("RadioButtonUsefulnessOfConductingChartAudit" + impact.UsefulnessOfConductingChartAudit);
                    radio.Checked = true;
                }
                if (impact.UsefulnessOfFeedbackProvided != null)
                {
                    RadioButton radio = (RadioButton)Master.FindControl("ContentPlaceHolder1").FindControl("RadioButtonUsefulnessOfFeedbackProvided" + impact.UsefulnessOfFeedbackProvided);
                    radio.Checked = true;
                }
            }
            viewMode = cycle.ImpactAssessmentComplete;

            if (viewMode)
            {
                ButtonSubmit.Visible = false;
				LinkButtonSaveForLater.Visible = false;
                TextBoxImportanceOfChanges.Enabled = false;
                TextBoxUsefulItemsLearned.Enabled = false;
                TextBoxImpactOthersDescription.Enabled = false;
                TextBoxAdditionalComments.Enabled = false;
                CheckBoxBarrierNotEnoughTime.Enabled = false;
                CheckBoxBarrierClinicalKnowledgeSkillExpertise.Enabled = false;
                CheckBoxBarrierRecallConfidenceClinicalInertia.Enabled = false;
                CheckBoxBarrierPeerInfluence.Enabled = false;
                CheckBoxBarrierCulturalCompetency.Enabled = false;
                CheckBoxBarrierFearLegalConcerns.Enabled = false;
                CheckBoxBarrierStaffCompetence.Enabled = false;
                CheckBoxBarrierStaffMotivation.Enabled = false;
                CheckBoxBarrierCostFunding.Enabled = false;
                CheckBoxBarrierPatientCharacteristicsFactors.Enabled = false;
                CheckBoxBarrierPatientAdherence.Enabled = false;
                CheckBoxBarrierWorkOverload.Enabled = false;
                CheckBoxBarrierTeamStructure.Enabled = false;
                CheckBoxBarrierPracticeProcess.Enabled = false;
                CheckBoxBarrierReferralProcess.Enabled = false;
                CheckBoxBarrierOther.Enabled = false;
                TextBoxImpactOthersDescription.Enabled = false;

                RadioButtonClinicalUsefulnessOverall1.Enabled = false;
                RadioButtonClinicalUsefulnessOverall2.Enabled = false;
                RadioButtonClinicalUsefulnessOverall3.Enabled = false;
                RadioButtonClinicalUsefulnessOverall4.Enabled = false;
                RadioButtonClinicalUsefulnessOverall5.Enabled = false;
                RadioButtonObjectivePresentationOf1.Enabled = false;
                RadioButtonObjectivePresentationOf2.Enabled = false;
                RadioButtonObjectivePresentationOf3.Enabled = false;
                RadioButtonObjectivePresentationOf4.Enabled = false;
                RadioButtonObjectivePresentationOf5.Enabled = false;

                RadioButtonScientificRigor1.Enabled = false;
                RadioButtonScientificRigor2.Enabled = false;
                RadioButtonScientificRigor3.Enabled = false;
                RadioButtonScientificRigor4.Enabled = false;
                RadioButtonScientificRigor5.Enabled = false;

                RadioButtonFreeOfCommercialBias1.Enabled = false;
                RadioButtonFreeOfCommercialBias2.Enabled = false;
                RadioButtonFreeOfCommercialBias3.Enabled = false;
                RadioButtonFreeOfCommercialBias4.Enabled = false;
                RadioButtonFreeOfCommercialBias5.Enabled = false;

                RadioButtonUsefulnessOfConductingSelfAssessment1.Enabled = false;
                RadioButtonUsefulnessOfConductingSelfAssessment2.Enabled = false;
                RadioButtonUsefulnessOfConductingSelfAssessment3.Enabled = false;
                RadioButtonUsefulnessOfConductingSelfAssessment4.Enabled = false;
                RadioButtonUsefulnessOfConductingSelfAssessment5.Enabled = false;

                RadioButtonUsefulnessOfConductingChartAudit1.Enabled = false;
                RadioButtonUsefulnessOfConductingChartAudit2.Enabled = false;
                RadioButtonUsefulnessOfConductingChartAudit3.Enabled = false;
                RadioButtonUsefulnessOfConductingChartAudit4.Enabled = false;
                RadioButtonUsefulnessOfConductingChartAudit5.Enabled = false;

                RadioButtonUsefulnessOfFeedbackProvided1.Enabled = false;
                RadioButtonUsefulnessOfFeedbackProvided2.Enabled = false;
                RadioButtonUsefulnessOfFeedbackProvided3.Enabled = false;
                RadioButtonUsefulnessOfFeedbackProvided4.Enabled = false;
                RadioButtonUsefulnessOfFeedbackProvided5.Enabled = false;
            }
        }


    }
    protected void ListViewProcessMeasures_OnItemDataBound(object sender, ListViewItemEventArgs e)
    {
        if (e.Item.ItemType == ListViewItemType.DataItem)
        {

            Chart ch = (Chart)e.Item.FindControl("ChartPerformance");
            PerformanceReportData data = (PerformanceReportData)((ListViewDataItem)e.Item).DataItem;
            HyperLink chartsIncluded = (HyperLink)e.Item.FindControl("LinkAbstractRecordsIncluded");
            ch.Series[0].LabelBackColor = System.Drawing.Color.White;

            ch.ChartAreas[0].AxisX.Maximum = 4.0;
            ch.Height = 100;
            ch.Series[0].Points.AddY(data.PeerData);
            ch.Series[0].Points.AddY(data.MeasurePercentCurrent);
            ch.Series[0].Points.AddY(data.MeasurePercentPrevious);
            ch.Series[0].Points.AddY(data.MeasureGoal);
            if (data.SelfData > 0)
            {
                ch.Series[0].Points.AddY(data.SelfData);
                ch.ChartAreas[0].AxisX.CustomLabels.Add(4.5, 5.5, "Self");
                ch.ChartAreas[0].AxisX.Maximum = 5.0;
                ch.Height = 120;
            }
            ch.ChartAreas[0].AxisX.CustomLabels.Add(0.5, 1.5, "Peer Data");
            ch.ChartAreas[0].AxisX.CustomLabels.Add(1.5, 2.5, "Final Data");
            ch.ChartAreas[0].AxisX.CustomLabels.Add(2.5, 3.5, "Initial Data");
            ch.ChartAreas[0].AxisX.CustomLabels.Add(3.5, 4.5, "My Goal");

        }

        PreviousMeasureGroupID = MeasureGroupID;
    }

    protected void ListViewOutcomeMeasures_OnItemDataBound(object sender, ListViewItemEventArgs e)
    {
        if (e.Item.ItemType == ListViewItemType.DataItem)
        {

            Chart ch = (Chart)e.Item.FindControl("ChartPerformance");
            PerformanceReportData data = (PerformanceReportData)((ListViewDataItem)e.Item).DataItem;
            HyperLink chartsIncluded = (HyperLink)e.Item.FindControl("LinkAbstractRecordsIncluded");
            ch.Series[0].LabelBackColor = System.Drawing.Color.White;

            HyperLink HyperLinkRationale = (HyperLink)e.Item.FindControl("HyperLinkRationale");
           
                switch (data.MeasureQualityIndicator)
                {
                 
                    case "Mean Improvement":
                        ch.ChartAreas[0].AxisY.Maximum = 50.00;
                        ch.ChartAreas[0].AxisY.Interval = 6;
                        ch.Height = 135;
                        break;

                }

                if (data.PeerData7To12 == -1)
                {
                    ch.Series[0].Points.AddY(data.MeasurePercent3To6);
                    ch.Series[0].Points.AddY(data.prevMeasurePercent3To6);
                    ch.Series[0].Points.AddY(data.MeasureGoal);
                    ch.ChartAreas[0].AxisX.Maximum = 4.0;
                    ch.Height = 100;
                    if (data.SelfData > 0)
                    {
                        ch.Series[0].Points.AddY(data.SelfData);
                        ch.ChartAreas[0].AxisX.CustomLabels.Add(3.5, 4.5, "Self");
                        ch.ChartAreas[0].AxisX.Maximum = 5.0;
                        ch.Height = 120;
                    }
          
                    ch.ChartAreas[0].AxisX.CustomLabels.Add(0.5, 1.5, "Final Data");
                    ch.ChartAreas[0].AxisX.CustomLabels.Add(1.5, 2.5, "Initial Data");
                    ch.ChartAreas[0].AxisX.CustomLabels.Add(2.5, 3.5, "My Goal");
                }
                else
                {
                    ch.Series[0].Points.AddY(data.PeerData7To12);
                    ch.Series[0].Points.AddY(data.MeasurePercent7To12);
                    ch.Series[0].Points.AddY(data.prevMeasurePercent7To12);
                    ch.Series[0].Points.AddY(data.PeerData3To6);
                    ch.Series[0].Points.AddY(data.MeasurePercent3To6);
                    ch.Series[0].Points.AddY(data.prevMeasurePercent3To6);

                    if (data.MeasurePercentNewOnset != null) 
                    {
                       
                        ch.ChartAreas[0].AxisX.CustomLabels.Add(0.5, 1.5, "Final Atropine Data");
                        ch.ChartAreas[0].AxisX.CustomLabels.Add(1.5, 2.5, "Initial Atropine Data");
                        ch.ChartAreas[0].AxisX.CustomLabels.Add(2.5, 3.5, "Final Reverse Data");
                        ch.ChartAreas[0].AxisX.CustomLabels.Add(3.5, 4.5, "Initial Reverse Data");
                        ch.Series[0].Points.AddY(data.MeasurePercentNewOnset);
                        ch.ChartAreas[0].AxisX.CustomLabels.Add(4.5, 5.5, "Final NewOnset Data");
                        ch.Series[0].Points.AddY(data.prevMeasurePercentNewOnset);
                        ch.ChartAreas[0].AxisX.CustomLabels.Add(5.5, 6.5, "Initial NewOnset Data");
                        ch.Series[0].Points.AddY(data.MeasurePercentRecurrence);
                        ch.ChartAreas[0].AxisX.CustomLabels.Add(6.5, 7.5, "Final Recurrence Data");
                        ch.Series[0].Points.AddY(data.prevMeasurePercentRecurrence);
                        ch.ChartAreas[0].AxisX.CustomLabels.Add(7.5, 8.5, "Initial Recurrence Data");
                        ch.Series[0].Points.AddY(data.MeasureGoal);
                        ch.ChartAreas[0].AxisX.CustomLabels.Add(8.5, 9.5, "My Goal");
                        ch.Series[0].Points.AddY(data.SelfData);
                        ch.ChartAreas[0].AxisX.CustomLabels.Add(9.5, 10.5, "Self");
                        ch.ChartAreas[0].AxisX.Maximum = 11.0;
                        ch.Height = 240;
                    }
                    else
                    {
                        ch.ChartAreas[0].AxisX.Maximum = 6.0;
                        ch.Height = 140;
                        ch.Series[0].Points.AddY(data.MeasureGoal);
                        if (data.SelfData > 0)
                        {
                            ch.Series[0].Points.AddY(data.SelfData);
                            ch.ChartAreas[0].AxisX.CustomLabels.Add(5.5, 6.5, "Self");
                            ch.ChartAreas[0].AxisX.Maximum = 7.0;
                            ch.Height = 160;
                        }
                        ch.ChartAreas[0].AxisX.CustomLabels.Add(0.5, 1.5, "Final 7-12 Data");
                        ch.ChartAreas[0].AxisX.CustomLabels.Add(1.5, 2.5, "Initial 7-12 Data");
                        ch.ChartAreas[0].AxisX.CustomLabels.Add(2.5, 3.5, "Final 3-6 Data");
                        ch.ChartAreas[0].AxisX.CustomLabels.Add(3.5, 4.5, "Initial 3-6 Data");
                        ch.ChartAreas[0].AxisX.CustomLabels.Add(4.5, 5.5, "My Goal");
                    }
                }
        }
        PreviousMeasureGroupID = MeasureGroupID;
    }

    protected bool validForm()
    {
        return true;
    }

    protected bool isComplete()
    {
        bool complete = true;

        if ((TextBoxImportanceOfChanges.Text.Trim() == "") || (TextBoxUsefulItemsLearned.Text.Trim() == ""))
            complete = false;

        if ((!CheckBoxBarrierNotEnoughTime.Checked) && (!CheckBoxBarrierClinicalKnowledgeSkillExpertise.Checked) && 
            (!CheckBoxBarrierRecallConfidenceClinicalInertia.Checked) && (!CheckBoxBarrierPeerInfluence.Checked) && 
            (!CheckBoxBarrierCulturalCompetency.Checked) && (!CheckBoxBarrierFearLegalConcerns.Checked) && 
            (!CheckBoxBarrierStaffCompetence.Checked) && (!CheckBoxBarrierStaffMotivation.Checked) && 
            (!CheckBoxBarrierCostFunding.Checked) && (!CheckBoxBarrierPatientCharacteristicsFactors.Checked) && 
            (!CheckBoxBarrierPatientAdherence.Checked) && (!CheckBoxBarrierWorkOverload.Checked) && 
            (!CheckBoxBarrierTeamStructure.Checked) && (!CheckBoxBarrierPracticeProcess.Checked) && 
            (!CheckBoxBarrierReferralProcess.Checked) && (!CheckBoxBarrierOther.Checked) )
            complete = false;

        if ((CheckBoxBarrierOther.Checked) &&  (TextBoxImpactOthersDescription.Text.Trim()==""))
            complete = false;

        if ((!RadioButtonClinicalUsefulnessOverall1.Checked) && (!RadioButtonClinicalUsefulnessOverall2.Checked) &&
            (!RadioButtonClinicalUsefulnessOverall3.Checked) && (!RadioButtonClinicalUsefulnessOverall4.Checked) && 
            (!RadioButtonClinicalUsefulnessOverall5.Checked) )
            complete = false;

        if ((!RadioButtonObjectivePresentationOf1.Checked) && (!RadioButtonObjectivePresentationOf2.Checked) &&
            (!RadioButtonObjectivePresentationOf3.Checked) && (!RadioButtonObjectivePresentationOf4.Checked)&&
            (!RadioButtonObjectivePresentationOf5.Checked))
            complete = false;

        if ((!RadioButtonScientificRigor1.Checked) && (!RadioButtonScientificRigor2.Checked)&&
            (!RadioButtonScientificRigor3.Checked) && (!RadioButtonScientificRigor4.Checked)&&
            (!RadioButtonScientificRigor5.Checked))
            complete = false;

        if ((!RadioButtonFreeOfCommercialBias1.Checked) && (!RadioButtonFreeOfCommercialBias2.Checked) &&
            (!RadioButtonFreeOfCommercialBias3.Checked) && (!RadioButtonFreeOfCommercialBias4.Checked) &&
            (!RadioButtonFreeOfCommercialBias5.Checked))
            complete = false;

        if ((!RadioButtonUsefulnessOfConductingSelfAssessment1.Checked) && (!RadioButtonUsefulnessOfConductingSelfAssessment2.Checked) &&
         (!RadioButtonUsefulnessOfConductingSelfAssessment3.Checked) && (!RadioButtonUsefulnessOfConductingSelfAssessment4.Checked) &&
         (!RadioButtonUsefulnessOfConductingSelfAssessment5.Checked))
            complete = false;

        if ((!RadioButtonUsefulnessOfConductingChartAudit1.Checked) && (!RadioButtonUsefulnessOfConductingChartAudit2.Checked)&&
            (!RadioButtonUsefulnessOfConductingChartAudit3.Checked) && (!RadioButtonUsefulnessOfConductingChartAudit4.Checked) &&
            (!RadioButtonUsefulnessOfConductingChartAudit5.Checked))
            complete = false;

        if ((!RadioButtonUsefulnessOfFeedbackProvided1.Checked) && (!RadioButtonUsefulnessOfFeedbackProvided2.Checked) &&
            (!RadioButtonUsefulnessOfFeedbackProvided3.Checked)&& (!RadioButtonUsefulnessOfFeedbackProvided4.Checked)&&
            (!RadioButtonUsefulnessOfFeedbackProvided5.Checked))
            complete = false;

        return complete;

    }

    protected void SaveImpactStatement(bool FinalFlag)
    {
        UserContext ctx = (UserContext)Session[Constants.SESSION_USERCONTEXT];
        Module module = ModuleContext.getModule();
        if (validForm())
        {
            bool completeForm = isComplete();
            using (NetHealthPIMEntities pim = new NetHealthPIMEntities())
            {
                using (TransactionScope transaction = new TransactionScope())
                {
                    ParticipantImpactStatement impact;
                    ParticipantModuleCycle cycle = pim.ParticipantModuleCycle.First(c => c.ParticipantModuleCycleID == ctx.ActiveModuleCycleID);
                    var impactFound = pim.ParticipantImpactStatement.Count(ps => ps.ParticipantModuleCycleID == ctx.ActiveModuleCycleID);
                    if (impactFound == 0)
                    {
                        impact = new ParticipantImpactStatement();
                        impact.ParticipantModuleCycle = cycle;
                    }
                    else
                    {
                        impact = pim.ParticipantImpactStatement.First(ps => ps.ParticipantModuleCycleID == ctx.ActiveModuleCycleID);
                    }

                    impact.ImportanceOfChanges = TextBoxImportanceOfChanges.Text;
                    impact.UsefulItemsLearned = TextBoxUsefulItemsLearned.Text;
                    impact.AdditionalComments = TextBoxAdditionalComments.Text;
                    impact.BarrierNotEnoughTime = CheckBoxBarrierNotEnoughTime.Checked;
                    impact.BarrierClinicalKnowledgeSkillExpertise = CheckBoxBarrierClinicalKnowledgeSkillExpertise.Checked;
                    impact.BarrierRecallConfidenceClinicalInertia = CheckBoxBarrierRecallConfidenceClinicalInertia.Checked;
                    impact.BarrierPeerInfluence = CheckBoxBarrierPeerInfluence.Checked;
                    impact.BarrierCulturalCompetency = CheckBoxBarrierCulturalCompetency.Checked;
                    impact.BarrierFearLegalConcerns = CheckBoxBarrierFearLegalConcerns.Checked;
                    impact.BarrierStaffCompetence = CheckBoxBarrierStaffCompetence.Checked;
                    impact.BarrierStaffMotivation = CheckBoxBarrierStaffMotivation.Checked;
                    impact.BarrierCostFunding = CheckBoxBarrierCostFunding.Checked;
                    impact.BarrierPatientCharacteristicsFactors = CheckBoxBarrierPatientCharacteristicsFactors.Checked;
                    impact.BarrierPatientAdherence = CheckBoxBarrierPatientAdherence.Checked;
                    impact.BarrierWorkOverload = CheckBoxBarrierWorkOverload.Checked;
                    impact.BarrierTeamStructure = CheckBoxBarrierTeamStructure.Checked;
                    impact.BarrierPracticeProcess = CheckBoxBarrierPracticeProcess.Checked;
                    impact.BarrierReferralProcess = CheckBoxBarrierReferralProcess.Checked;
                    impact.BarrierOther = CheckBoxBarrierOther.Checked;
                    if (CheckBoxBarrierOther.Checked)
                        impact.ImpactOthersDescription = TextBoxImpactOthersDescription.Text;
                    else
                        impact.ImpactOthersDescription = "";
                    if (RadioButtonClinicalUsefulnessOverall1.Checked) impact.ClinicalUsefulnessOverall = 1;
                    if (RadioButtonClinicalUsefulnessOverall2.Checked) impact.ClinicalUsefulnessOverall = 2;
                    if (RadioButtonClinicalUsefulnessOverall3.Checked) impact.ClinicalUsefulnessOverall = 3;
                    if (RadioButtonClinicalUsefulnessOverall4.Checked) impact.ClinicalUsefulnessOverall = 4;
                    if (RadioButtonClinicalUsefulnessOverall5.Checked) impact.ClinicalUsefulnessOverall = 5;

                    if (RadioButtonObjectivePresentationOf1.Checked) impact.ObjectivePresentationOf = 1;
                    if (RadioButtonObjectivePresentationOf2.Checked) impact.ObjectivePresentationOf = 2;
                    if (RadioButtonObjectivePresentationOf3.Checked) impact.ObjectivePresentationOf = 3;
                    if (RadioButtonObjectivePresentationOf4.Checked) impact.ObjectivePresentationOf = 4;
                    if (RadioButtonObjectivePresentationOf5.Checked) impact.ObjectivePresentationOf = 5;

                    if (RadioButtonScientificRigor1.Checked) impact.ScientificRigor = 1;
                    if (RadioButtonScientificRigor2.Checked) impact.ScientificRigor = 2;
                    if (RadioButtonScientificRigor3.Checked) impact.ScientificRigor = 3;
                    if (RadioButtonScientificRigor4.Checked) impact.ScientificRigor = 4;
                    if (RadioButtonScientificRigor5.Checked) impact.ScientificRigor = 5;

                    if (RadioButtonFreeOfCommercialBias1.Checked) impact.FreeOfCommercialBias = 1;
                    if (RadioButtonFreeOfCommercialBias2.Checked) impact.FreeOfCommercialBias = 2;
                    if (RadioButtonFreeOfCommercialBias3.Checked) impact.FreeOfCommercialBias = 3;
                    if (RadioButtonFreeOfCommercialBias4.Checked) impact.FreeOfCommercialBias = 4;
                    if (RadioButtonFreeOfCommercialBias5.Checked) impact.FreeOfCommercialBias = 5;

                    if (RadioButtonUsefulnessOfConductingSelfAssessment1.Checked) impact.UsefulnessOfConductingSelfAssessment = 1;
                    if (RadioButtonUsefulnessOfConductingSelfAssessment2.Checked) impact.UsefulnessOfConductingSelfAssessment = 2;
                    if (RadioButtonUsefulnessOfConductingSelfAssessment3.Checked) impact.UsefulnessOfConductingSelfAssessment = 3;
                    if (RadioButtonUsefulnessOfConductingSelfAssessment4.Checked) impact.UsefulnessOfConductingSelfAssessment = 4;
                    if (RadioButtonUsefulnessOfConductingSelfAssessment5.Checked) impact.UsefulnessOfConductingSelfAssessment = 5;

                    if (RadioButtonUsefulnessOfConductingChartAudit1.Checked) impact.UsefulnessOfConductingChartAudit = 1;
                    if (RadioButtonUsefulnessOfConductingChartAudit2.Checked) impact.UsefulnessOfConductingChartAudit = 2;
                    if (RadioButtonUsefulnessOfConductingChartAudit3.Checked) impact.UsefulnessOfConductingChartAudit = 3;
                    if (RadioButtonUsefulnessOfConductingChartAudit4.Checked) impact.UsefulnessOfConductingChartAudit = 4;
                    if (RadioButtonUsefulnessOfConductingChartAudit5.Checked) impact.UsefulnessOfConductingChartAudit = 5;

                    if (RadioButtonUsefulnessOfFeedbackProvided1.Checked) impact.UsefulnessOfFeedbackProvided = 1;
                    if (RadioButtonUsefulnessOfFeedbackProvided2.Checked) impact.UsefulnessOfFeedbackProvided = 2;
                    if (RadioButtonUsefulnessOfFeedbackProvided3.Checked) impact.UsefulnessOfFeedbackProvided = 3;
                    if (RadioButtonUsefulnessOfFeedbackProvided4.Checked) impact.UsefulnessOfFeedbackProvided = 4;
                    if (RadioButtonUsefulnessOfFeedbackProvided5.Checked) impact.UsefulnessOfFeedbackProvided = 5;

                    impact.LastUpdateDate = DateTime.Now;

                    if (FinalFlag && completeForm)
                    {
                        cycle.ImpactAssessmentComplete = true;
                        cycle.ImpactAssessmentCompletedDate = DateTime.Now;
                        cycle.LastUpdateDate = DateTime.Now;
                        cycle.ReportApproved = true;
                        cycle.ReportApprovedCompletedDate = DateTime.Now;
                    }

                    pim.SaveChanges();
                    transaction.Complete();
                }
            }
        }
        Response.Redirect("Dashboard3.aspx");
    }

    protected void ButtonSaveForLater_Click(object sender, EventArgs e)
    {
        Module module = ModuleContext.getModule();
        SaveImpactStatement(false);
        Response.Redirect("~/" + module.ModuleCode.ToLower() + "/ImpactStatement.aspx");
    }

    protected void ButtonSubmit_Click(object sender, EventArgs e)
    {
        Module module = ModuleContext.getModule();
        SaveImpactStatement(true);
    }
}
