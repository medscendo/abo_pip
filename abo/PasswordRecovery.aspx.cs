﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using NetHealthPIMModel;

public partial class copd_PasswordRecovery : BasePage
{
    protected void Page_Load(object sender, EventArgs e)
    {
        if (!IsPostBack)
        {
            InitializePage();
            int ParticipantID = 0;
            int ModuleID = module.ModuleID;
            string ResetGUID = "";

            if (Request.Params["p"] != null) ParticipantID = Int32.Parse(Request.Params["p"]);
            if (Request.Params["m"] != null) ModuleID = Int32.Parse(Request.Params["m"]);
            if (Request.Params["g"] != null) ResetGUID = Request.Params["g"];
            
            using (NetHealthPIMEntities pim = new NetHealthPIMEntities())
            {
                var count = (from p in pim.Participant
                             where p.ParticipantID == ParticipantID && p.Password == ResetGUID
                             select p).Count();
                if (count == 1)
                {
                    UserContext ctx = new UserContext();
                    ctx.ActiveModuleID = ModuleID;
                    ctx.ParticipantID = ParticipantID;
                    ctx.IsAuthenticated = false;
                    ctx.RoleTypeID = Constants.ROLETYPEID_GUEST;
                    Session[Constants.SESSION_USERCONTEXT] = ctx;

                    Response.Redirect("~/copd/PasswordReset.aspx");
                }
            }
        }
    }

    protected void InitializePage()
    {
        ((PIMMasterPage)Page.Master).SetTitle("Password Recovery");
        Session[Constants.SESSION_PAGE_ROLETYPE_LEVEL] = Constants.ROLETYPEID_GUEST;
    }

    protected void ButtonSubmit_Click(object sender, EventArgs e)
    {
        string email = TextEmailAddress.Text;
        using (NetHealthPIMEntities pim = new NetHealthPIMEntities())
        {
            var count = (from p in pim.Participant
                         where p.ParticipantEmailAddress == email
                         select p).Count();
            if (count == 1)
            {
                var participant = (from p in pim.Participant
                                   where p.ParticipantEmailAddress == email
                                   select p).First();

                // As we don't have access to the passwords, initialize the password with a known constant that indicates it needs to be initialized. 
                Guid g = System.Guid.NewGuid();
                string gstr = g.ToString().ToUpper();

                if (SendPasswordEmail(participant.ParticipantID, email, gstr))
                {
                    participant.Password = gstr;
                    pim.SaveChanges();

                    PanelPasswordRecoveryRequest.Visible = false;
                    PanelEmailConfirmation.Visible = true;
                    LabelError.Visible = false;
                }
            }
            else
            {
                LabelError.Text = "The e-mail provided is not in our records. Please try again";
                LabelError.Visible = true;
            }
        }

    }

    private bool SendPasswordEmail(int participantID, string email, string passwordResetGUID)
    {
        // TODO: There are other places where this can be configured. COPD parameter should be dynamic for other modules.
        string messageBody = "<html><head><title>NetHealth - Password Reset</title></head>";
        messageBody += "<body>Click the following link to reset your password. <br /> You will be required to enter a new password before accessing the activity. <br /><br />";
        messageBody += "<a href='" + Request.Url + "?g=" + passwordResetGUID + "&p=" + participantID + "&m=" + Constants.MODULE_COPD + "'>Click here</a></body></html>";
        EMail.SendEMail(module.ModuleCode.ToLower() + "@nethealthinc.com", email, true, "Password Reset", messageBody);
        return true;
    }

}
