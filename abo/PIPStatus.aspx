﻿<%@ Page Title="" Language="C#" MasterPageFile="~/PIMMasterPage.master" AutoEventWireup="true" CodeFile="PIPStatus.aspx.cs" Inherits="abo_PIPStatus" %>

<%@ Register Assembly="TSC.Timeout" Namespace="TSC.Timeout" TagPrefix="tsc" %>

<asp:Content ID="Content1" ContentPlaceHolderID="head" Runat="Server">
<script type="text/javascript">
function openmodalWin(optionSelected) 
{
    if (window.showModalDialog) {
    var time = new Date().getTime(); 
    var ret = window.showModalDialog("ModuleMenu.aspx?" + "time=" + time , "name", "dialogWidth:240px;dialogHeight:160px;status:no;scroll:no;edge:sunken");
    if (ret != undefined) {
        if (optionSelected == "1")
            location.replace("OpenModule.aspx?reviewPractice=1&mid=" + ret);
        else
            location.replace("OpenModule.aspx?charts=1&mid=" + ret);
    }
    //alert(ret);
} 
else {
    window.open('ModuleMenu.aspx', 'name',
'height=200,width=200,toolbar=no,directories=no,status=no,menubar=no,scrollbars=no,resizable=no,modal=yes');
}
} 
</script>

    <style>
        .chart-info {
            margin-top:10px;
            border:1px solid #12b0f1;
            border-radius:5px;
            -webkit-border-radius:5px;
        }

        .chart-info table {
            width:96%;
            margin: 1% 2%;
            border-collapse:collapse;
        }

        .chart-info td, .chart-info th {
            padding: 3px 5px;
        }

        .chart-info th {
            border-top: 1px solid #ccc;
            color:#fff;
            background: #2dabd4; /* Old browsers */
            background: -moz-linear-gradient(top,  #2dabd4 0%, #023c6a 100%); /* FF3.6+ */
            background: -webkit-gradient(linear, left top, left bottom, color-stop(0%,#2dabd4), color-stop(100%,#023c6a)); /* Chrome,Safari4+ */
            background: -webkit-linear-gradient(top,  #2dabd4 0%,#023c6a 100%); /* Chrome10+,Safari5.1+ */
            background: -o-linear-gradient(top,  #2dabd4 0%,#023c6a 100%); /* Opera 11.10+ */
            background: -ms-linear-gradient(top,  #2dabd4 0%,#023c6a 100%); /* IE10+ */
            background: linear-gradient(to bottom,  #2dabd4 0%,#023c6a 100%); /* W3C */
            filter: progid:DXImageTransform.Microsoft.gradient( startColorstr='#2dabd4', endColorstr='#023c6a',GradientType=0 ); /* IE6-9 */
        }

        .chart-info .left-col {
            border-bottom: 1px solid #ccc;
            border-right: 1px solid #ccc;    
        }

        .chart-info .mid-col {
            border-bottom: 1px solid #ccc;
        }

        .chart-info .right-col {
            border-left: 1px solid #ccc;
            border-bottom: 1px solid #ccc;
        }

        .chart-info .description {
            margin: 1% 2%;
        }
    </style>

</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder1" Runat="Server">
        <!-- main content -->
        <div class="main_content">
                    <div class="heading_box">
            <h2><strong>Status:</strong> <span class="red"><asp:Literal ID="LiteralStatus" runat="server">Pending</asp:Literal></span></h2>
            <div class="top_nav">
            <ul>
            <li class="active"><a href="#"><span>Status</span></a></li>
             <li><a href="PIMTutorial.aspx"><span>Improvement in Medical Practice Tutorial</span></a></li>          
             <li><a href="ModuleProcess.aspx"><span>Process Overview</span></a></li>           
            </ul>
            </div>
          </div>
          
          <div class="status_section">
          <div class="status_left_section">
          <div class="status_left_section"> 
          <br /><br />
          <h3>Dr. <asp:Literal ID="LabelParticipantName" runat="server" Text="Participant Name" ></asp:Literal></h3>
          <p style="font-size: 10pt; line-height: 1.2em;"><asp:Literal ID="LiteralInitialMessage" runat="server"></asp:Literal></p>

            <span style="font-size: 10pt; line-height: 1.2em;" ><strong>Note:</strong> First time registrants are required to view the <a href="PIMTutorial.aspx">Interactive Tutorial</a></span>

            <p style="font-size: 10pt; line-height: 1.2em;"><strong><asp:Literal ID="LiteralHelpLeft" runat="server" ></asp:Literal></strong></p>

                <asp:ListView runat="server" ID="ListViewModuleLinks">
                <LayoutTemplate>
                    <ul>
                    <asp:PlaceHolder runat="server" ID="itemPlaceholder"></asp:PlaceHolder>
                    </ul>
                </LayoutTemplate>
                <ItemTemplate>
                    <li style="padding-bottom: 5px; list-style-type: none; "><a href="OpenModule.aspx?mid=<%# Eval("ModuleID")%>" id="HtmlLinkOpenModule"><%# Eval("ModuleName")%> - <%# Eval("ChartStatus")%></a>
                </ItemTemplate>
                </asp:ListView>
                <br />
             <p style="font-size: 10pt; line-height: 1.2em;"><strong><asp:Literal ID="LiteralHelpLeft2" runat="server" Visible="false">Once you have completed all activities, you will be able to develop your improvement plan</asp:Literal></strong></p>
          </div>
                    </div>
                    
                    
                    <div class="status_right_section">
                    <h3>Improvement in Medical Practice Platform</h3>
                   <p style="padding-left: 100px;">This Main Page shows your status in the process and the steps needed to complete.<br />  
                   You can return to this page at any time, by clicking “Main Page” in the top right<br />
                   hand corner of the screen. You can also click the “Support” button at any time for<br />
                   technical or clinical questions, comments, or suggestions.
                   <br /><br />
                   <asp:Label runat="server" ID="LiteralImprovementPlan" Visible="false" ForeColor="Blue">You are currently on your improvement plan, your analysis is schedule to start on </asp:Label><asp:Literal runat="server" ID="LiteralImprovementPlanStart" Visible="false"></asp:Literal>
                   <br />
                    </p>

                    <div style='width:225px; float:left; padding-bottom:0px; padding-top:0px;  padding-left:50px; height:22px;' visible=false id="divPhase1" runat="server"><img src="../common/images/currentphase1.jpg" alt=" " /></div>
                    <div style='margin-left: 275px; width:225px; float:left; padding-bottom:0px; height:22px;' id="divPhase2" visible=false runat="server"><img src="../common/images/currentphase2.jpg" alt=" " /></div>
                    <div style='width:175px; float:right; padding-bottom:0px; height:22px;' id="divPhase3" visible=false runat="server"><img src="../common/images/currentphase3.jpg" alt=" " /></div>

                    <div class="performance_section">
                    <div runat="server" id="divModuleSelection" style="height:154px;">
                    <div class="col_details" runat="server">
                    <h4>Activity Selection &amp; Self-Assessment</h4>
                    <ul>
                    <li><a href="PQRSSelection.aspx" id="HTMLlinkModuleSelection" runat="server">Select Your Activities</a></li>
                    <li><a href="#" id="HTMLlinkReviewYourPractice" runat="server">Review Your Practice</a></li>
                    <li><a href="#" id="HTMLlinkAbstractYourCharts" runat="server">Abstract Your Charts</a></li>
                    </ul>
                    </div>
                    
                    
                    <div>
                    <div class="arrow_section">
                    <a href="#"><img src="../common/images/arrow_btn.png" alt=" " /></a>
                    <a href="#"><img src="../common/images/arrow_btn.png" alt=" " /></a>
                    <a href="#"><img src="../common/images/arrow_btn.png" alt=" " /></a>
                    </div>
                    </div>
                    
                    <div runat="server" id="divModuleSelection_bottom" class="curve_bottom"><img runat="server" id="blue_bottom" src="../common/images/per.blue_bottom_curve.jpg" alt="" /></div>
                    </div>

                    <div class="col orange">
                    <div runat="server" id="divPlanning">
                        <div class="col_details">
                        <h4>Review &amp; Planning</h4>
                        <ul>
                        <li><a href="#" runat="server" id="HTMLlinkViewPerformaceReport">Review Your Results</a></li>
                        <li><a href="#" runat="server" id="HTMLlinkViewImprovementPlan">Develop Plan for Improvement</a></li>
                        <li><a href="#" runat="server" id="HTMLlinkViewPhase1Charts">View Phase 1 Charts</a></li>
                        </ul>
                        </div>
                    
                        <div>
                        <div class="arrow_section">
                        <a href="#"><img src="../common/images/arrow_btn.png" alt=" " /></a>
                        <a href="#"><img src="../common/images/arrow_btn.png" alt=" " /></a>
                        <a href="#"><img src="../common/images/arrow_btn.png" alt=" " /></a>
                        </div>
                        </div>
                    
                         <div class="curve"><img runat="server" id="orange_bottom"  src="../common/images/per.orange_bottom_curve.jpg" alt="" /></div>
                    </div>
                    </div>

                    <div class="col pink">
                    <div runat="server" id="divAnalysis">
                        <div class="col_details">
                        <h4>Analysis &amp; Impact</h4>
                        <ul>
                        <li><a href="#" id="HTMLlinkAbstractSecondCharts" runat="server">Abstract Second Sample of Charts</a></li>
                        <li><a href="#" id="HTMLlinkComparePerformance" runat="server">Compare Feedback</a></li>
                        <li><a href="#" id="HTMLlinkImpactStatement" runat="server">Analyze Impact</a></li>
                        </ul>
                        </div>
                        <div class="curve"><img src="../common/images/per.pink_bottom_curve.jpg" id="pink_bottom" runat="server" alt="" /></div>
                        </div>
                     </div>
            <div class="nextstep" >
                <p style="font-size: 12pt; line-height: 1.1em;">
                    <br />&nbsp;<br />
                    <nobr>To continue where you left off, <asp:HyperLink ID="HyperLinkNextPage" runat="server" Text="Click Here"></asp:HyperLink></nobr> 
                </p>
            </div>
                    </div>
          </div>
          
        </div>
        </div>
        <!-- main content ends -->
</asp:Content>

