﻿<%@ Page Title="" Language="C#" MasterPageFile="~/PIMMasterPage.master" AutoEventWireup="true" CodeFile="SelfAssessment.aspx.cs" Inherits="abo_SelfAssessment" %>

<asp:Content ID="Content1" ContentPlaceHolderID="head" Runat="Server">
    <style>
        table {
            width:100%;
        }

        td {
            padding: 5px 10px;
            border: 1px solid #ccc;
        }

        td:nth-child(1) {
            width:70%;
        }
        
        td:nth-child(2) {
            text-align: center;
        }

        select {
            width: 50%;
        }

        .header-box .body {
            padding:0;
        }

        .header-box .head {
            padding-left:20px;
            text-align:left;
            font-weight:bold;
        }
    </style>
</asp:Content>

<asp:Content ID="Content3" ContentPlaceHolderID="javascript" Runat="Server">
    <script src="http://code.jquery.com/jquery-latest.js"></script>
   <script type="text/javascript">
       $(document).ready(function () {
           $("#<%=ButtonSubmit.ClientID%>").click(function (evt) {
               $('select').each(function (index) {
                   var value = $(this).val();
                   if (value == "-1") {
                       var answer = confirm('All fields are not complete.  Are you sure you want to submit?');
                       // answer is a boolean
                       if (answer == false) {
                           evt.preventDefault();

                       }
                       return false;
                   }
               });


           });
       });
    </script>
</asp:Content>

<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder1" Runat="Server">

    <p>Your answers to the self-assessment questions below will be presented in conjunction with the results of your chart review for this activity.</p>
                
    <div class="header-box">
        <div class="head">
            Self-Assessment Questions
        </div>
        <h3><asp:Label ID="LabelError"  Visible="false"  runat="server" Text="Label"></asp:Label></h3>
        <div class="body">
            <fieldset>
                <asp:Panel ID="Panel1" runat="server" />
            </fieldset>
        </div>
    </div>
    
    <div class="button-box">
        <asp:LinkButton ID="LinkButtonReturnToStatus" runat="server" OnCommand="LinkButtonReturnToStatus_Click" CssClass="button" Text="Return to Status Page" />
        <%--<asp:LinkButton ID="LinkButtonBackToDashboard" runat="server" Text="Dashboard" CssClass="button" PostBackUrl="Dashboard.aspx"/>--%>
        <asp:LinkButton ID="ButtonSubmit" runat="server" OnCommand="ButtonSubmit_Click" CssClass="button" Text="Save Self-Assessment" />
        
    </div>
<%--          </div>
    </div>--%>
</asp:Content>

