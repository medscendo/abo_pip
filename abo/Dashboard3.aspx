<%@ Page Title="" Language="C#" MasterPageFile="~/PIMMasterPage.master" AutoEventWireup="true" CodeFile="Dashboard3.aspx.cs" Inherits="copd_Dashboard3" %>

<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="Server">

<style>
        
    .surveys_complete 
    {
        width: 100%;
        font-size: 18px;
        color:#3e2449;
        text-align:center;
    }

    .upper-panel {
        float:left;
        margin-bottom:30px;
    }

    .table {
        margin-bottom: 20px;
    }

    .table th, .prior-data {
        text-align:center;
    }

    .table tr th, .table tr td {
        border-right:none;
        border-left:none;
    }

    .table tr th.first, .table tr td.first {
        border-left: 1px solid #ccc;
    }
    .table tr th.last, .table tr td.last {
        border-right: 1px solid #ccc;
    }

    .right-col {
        text-align:right;
        width:20%;
    }
</style>

</asp:Content>


<asp:Content ID="Content3" ContentPlaceHolderID="ContentPlaceHolder1" runat="Server">
<asp:HiddenField runat="server" ID="HiddenFieldPlanProgressPct" />
<div class="upper-panel clearfix">
    <div class="text-panel">
        <p>This screen illustrates your progress and current status in the process.  You may return to this page at any time by clicking &quot;Analysis &amp; Impact&quot; in the top status bar.</p>
    </div>
    <div class="progress-panel">
        <h3>Analysis &amp; Impact Progress</h3>
        <div class="progressbar">
            <div class="progress" id="progress1"></div>
        </div>
        <asp:Label ID="Label1" runat="server">&nbsp;</asp:Label>
        <asp:LinkButton runat="server" ID="LinkButtonNextStep" OnCommand="LinkButtonNext_Click" class="button blue">Continue where you left off</asp:LinkButton>
    </div>
</div>

<asp:Repeater runat="server" ID="RepeaterModules" OnItemDataBound="RepeaterModules_ItemDataBound">
    <HeaderTemplate>
    </HeaderTemplate>   
    <ItemTemplate>
    <table class="table">
        <tr>
            <th><%# Eval("ModuleShortName")%></th>
            <th>Status</th>
        </tr>
        <tr>
            <td><asp:LinkButton ID="LinkButtonPatientCharts" runat="server" OnCommand="ButtonCharts_Click" CommandArgument='<%# Eval("ModuleID")%>'>Register Charts</asp:LinkButton> (Please select qualified patient visit charts that meet the specified patient definition)</td>
            <td class="right-col">
                <%# Eval("PatientChartRegistrationCompleted")%> of <%# Eval("TotalCharts")%> complete
                <img src="../common/images/icon-check.png"  runat="server" id="imageChartRegistration" alt="" visible="false" />
            </td>
        </tr>
        <tr>
            <td><asp:LinkButton ID="HyperLinkSelfAssessment" runat="server" OnCommand="ButtonSelfAssessment_Click" CommandArgument='<%# Eval("ModuleID")%>'>Self-Assessment</asp:LinkButton> (Reflect on your practice and current processes of care)</td>
            <td class="right-col">
                <img runat="server" id="imageSelfAssessment" src="../common/images/icon-check.png" alt="" />
            </td>
        </tr>
        <tr>
            <td><asp:LinkButton ID="HyperLinkSystemSurvey" runat="server" OnCommand="ButtonSystemSurvey_Click" CommandArgument='<%# Eval("ModuleID")%>'>System Survey</asp:LinkButton> (Consider system characteristics that impact your practice)</td>
            <td class="right-col">
                <img src="../common/images/icon-dash.png"  runat="server" id="imageSystemSurvey" alt="" /></td>
        </tr>
        <tr>
            <td><asp:LinkButton ID="HyperLinkPatientChartRegistration" runat="server" OnCommand="ButtonCharts_Click" CommandArgument='<%# Eval("ModuleID")%>'>Chart Re-Abstraction</asp:LinkButton> (Abstract the charts you registered for this activity)</td>
            <td class="right-col">
                <%# Eval("ChartsCompleted")%> of <%# Eval("TotalCharts")%> complete
                <img src="../common/images/icon-check.png"  runat="server" id="imageChartAbstraction" alt="" visible="false" />
            </td>
        </tr>
    </table>
    </ItemTemplate>     
    <FooterTemplate>
    </FooterTemplate>
</asp:Repeater> 

    <!-- PEC -->
    <table class="table" runat="server" id="PECSurvey">
        <tr>
            <th>Patient Experience of Care</th>
            <th>Status</th>
        </tr>
        <tr>
            <td><a href="#" target="_blank" runat="server" id="hrefPECSurvey">Patient Survey Information</a> (Collect 45 surveys or your specified targeted number for your patients)</td>
            <td class="right-col">
                <asp:Label ID="LabelPatientSurvey" runat="server"></asp:Label> of 45 recorded
            </td>
        </tr>
    </table>

<table class="table">
    <tr>
        <th class="first">Impact Analysis</th>
        <th>Status</th>
    </tr>
    <tr>
        <td class="first"><asp:HyperLink ID="HyperLinkReviewPerformanceReport" runat="server">Compare Feedback</asp:HyperLink></td>
        <td class="last right-col"><img runat="server" id="imagePerformanceReport" src="../common/images/icon-dash.png" alt=""/></td>
    </tr>
    <tr>
        <td class="first"><asp:HyperLink ID="HyperLinkAnalyzeImpact" runat="server">Analyze Impact</asp:HyperLink></td>
        <td class="last right-col"><img runat="server" id="imageAnalyzeImpact" src="../common/images/icon-dash.png" alt=""/></td>
    </tr>
    <tr>
        <td class="first"><asp:HyperLink ID="HyperLinkSubmitModule" runat="server">Submit</asp:HyperLink><a href="#link"></a></td>
        <td class="last right-col"><img runat="server" id="ImageSubmitModule" src="../common/images/icon-dash.png" alt=""/></td>
    </tr>
</table>

</asp:Content>

<asp:Content ID="Content4" ContentPlaceHolderID="javascript" runat="server">

    <script type="text/javascript">
        $(document).ready(function() {

            // This variable sets the percentage complete
            var prog = $("#<%= HiddenFieldPlanProgressPct.ClientID %>").val();
            var progressClass = document.getElementById('progress1');
            if (prog > 100)
                prog = 100;

            // Set width, changing how much progress bar shows
            $("#progress1").css("width", prog + "%");

            // Puts numbers to right 
            if (prog < 10) {
                progressClass.innerHTML = '<p style="left:30px; color:#7f238e">' + prog + '%</p>';
            }
            else {
                progressClass.innerHTML = '<p>' + prog + '%</p>';
            }
        });
        function ValidateForm() {
            return true;
        }
    </script>

</asp:Content>