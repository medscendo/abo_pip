﻿<%@ Page Title="" Language="C#" MasterPageFile="~/PIMMasterPage.master" AutoEventWireup="true" CodeFile="RemoveMeasures.aspx.cs" Inherits="copd_RemoveMeasures" %>

<asp:Content ID="Content1" ContentPlaceHolderID="head" Runat="Server">
</asp:Content>
<asp:Content ID="Content3" ContentPlaceHolderID="ContentPlaceHolder1" Runat="Server">

<script type="text/javascript" language="javascript">
    function showMoreResources(el, mid, tbl) {
        var table = $get(tbl);
        var rows = table.rows;
        for (var i = 0; i < rows.length; i++) {
            var midAttr = rows[i].mid;
            if (midAttr == mid) {
                rows[i].style.display = "block";
            }
        }
        el.style.display = "none";
    }
</script>
    
    <asp:Panel ID="PanelInformation" runat="server">
    </asp:Panel>

    <asp:Panel ID="PanelReportCompleteDate" runat="server" Visible="false">
    This report completed on: <asp:Label ID="LabelImprovementPlanCompletedDate" runat="server" /><br />
    </asp:Panel>
          <!-- module detaile -->
          <div class="improvement_plan">
            <div class="heading_box">
              <h2>Remove Selected Measures</h2>
            </div>
            <div class="improvement_plan_details">
              <div class="top_details">
                
                <p> Need some text here...</p>
</div>
              <div class="measures_selected_box">
                <h3>1. Process and Outcome Measures selected for improvement </h3> 
                <div class="bginput">
                <div class="measures_box">
                  <div class="measures_top_curve">
                    <div class="measures_bottom_curve">
                      <div class="measures_contant_box">
                        <div class="measures_top">
                          <div class="top_left">Measure Selected</div>
                          <div class="top_2">Initial Data</div>
                          <div class="top_3">Peer Data</div>
                          <div class="top_right">Goal</div>
                        </div>

                       <asp:ListView runat="server" ID="ListViewMeasureGoals" onitemdatabound="ListViewMeasureGoals_OnItemDataBound">
                       <LayoutTemplate>
                            <asp:PlaceHolder runat="server" ID="itemPlaceholder"></asp:PlaceHolder>
                       </LayoutTemplate>    
                        <ItemTemplate>
                        <div class="measures_row">
                          <div class="measures_col1">
                            <asp:HiddenField ID="HiddenFieldMeasureID" Value='<%# Eval("MeasureID") %>' runat="server" />
                            <p>
                                <strong><asp:Label ID="LabelMeasureNumber" runat="server"></asp:Label>. <strong><%# Eval("ModuleName")%></strong> - <%# Eval("MeasureQualityIndicator")%></strong>:
                                <%# Eval("MeasureClinicalRecommendation") %> 
                            </p>
                          </div>
                          <div class="measures_col2"><asp:Label ID="LabelMyPracticeAssessment" runat="server" Text='<%# Eval("MeasurePercentCurrent", "{0:D}%") %>'></asp:Label></div>
                          <div class="measures_col3"><asp:Label ID="LabelMyPeersData" runat="server" Text='<%# Eval("PeerData", "{0:D}%") %>'></asp:Label></div>
                          <div class="measures_col4">
                            <asp:TextBox ID="TextBoxMeasureGoal" runat="server" MaxLength="3" Width="40" Text='<%# Eval("MeasureGoal") %>'></asp:TextBox>
                            <asp:Label ID="LabelPercentSign" runat="server" Text="%" />                    
                            <asp:RequiredFieldValidator ID="RequiredFieldValidatorMeasureGoal" runat="server" ErrorMessage='<%# Eval("MeasureTitle", "Missing Measure Goal: {0}") %>' Display="Dynamic" 
                                ControlToValidate="TextBoxMeasureGoal" >*</asp:RequiredFieldValidator>
                            <asp:RangeValidator ID="RangeValidatorMeasureGoal" runat="server" ErrorMessage='<%# Eval("MeasureTitle", "Measure Goal is not in a valid range: {0}") %>' ControlToValidate="TextBoxMeasureGoal" 
                                MinimumValue="1" MaximumValue="100" Type="Integer" Display="Dynamic" >*</asp:RangeValidator>
                            <asp:Label ID="LabelMeasureGoal" runat="server" Text='<%# Eval("MeasureGoal", "{0:D}%") %>' Visible="false"></asp:Label>
                          </div>
                        </div>
                        </ItemTemplate>
                        </asp:ListView>
                  
                        <!--
                        <div class="measures_row">
                          <div class="measures_col1">
                            <p><strong>1. Sensory Testing</strong> should be performed in all patients suspected of Amblyiopia</p>
                          </div>
                          <div class="measures_col2"><img src="common/images/measerment_result1.jpg" alt="" /></div>
                          <div class="measures_col3"><img src="common/images/measerment_result1.jpg" alt="" /></div>
                        </div>
                        <div class="measures_row last">
                          <div class="measures_col1">
                            <p><strong>2. Compliance Assessment</strong> should be performed in all patients suspected of Amblyiopia</p>
                          </div>
                          <div class="measures_col2">a</div>
                          <div class="measures_col3">b</div>
                          <div class="measures_col4">c</div>
                        </div>-->
                      </div>
                    </div>
                  </div>
                </div>
              </div>
              
            </div>
          </div>
          <!-- module detaile ends -->
              
<div class="button-box">
    <asp:Button ID="ButtonSaveForLater" runat="server" Text="Save" CausesValidation="false" onclick="ButtonSaveForLater_Click" CssClass="button" />
    <asp:Button ID="ButtonSubmit" runat="server" Text="Submit Final Plan" OnClientClick="return confirm('Do you want to Submit your Improvement Plan?');" onclick="ButtonSubmit_Click" CssClass="button" />
</div>
    
</asp:Content>

