﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using NetHealthPIMModel;
using System.Transactions;
using System.Web.UI.HtmlControls;

public partial class abo_ModuleSelectionReview : BasePage
{
    protected void Page_Load(object sender, EventArgs e)
    {
        if (!IsPostBack)
        {
            InitializePage();
        }
    }
    protected void InitializePage()
    {

        ((PIMMasterPage)Page.Master).SetTitle("Status");
        ((PIMMasterPage)Page.Master).SetStatus(1);
        ((PIMMasterPage)Page.Master).SetHeader("Activity Selection");
        using (NetHealthPIMEntities pim = new NetHealthPIMEntities())
        {
            int PlanProgressPct = 0;
            Participant participant = pim.Participant.First(p => p.ParticipantID == ctx.ParticipantID);
            
            int cycleID = ctx.ActiveModuleCycleID;
            ParticipantModuleCycle cycle = pim.ParticipantModuleCycle.First(c => c.ParticipantModuleCycleID == cycleID);
            ParticipantModuleCycle cycle2 = null;

            if (cycle.CycleNumber == 2)
            {
                cycle2 = cycle;

                cycle.PrevParticipantModuleCycleReference.Load();
                ParticipantModuleCycle prev = cycle.PrevParticipantModuleCycle;
                cycle = prev;

            }
            int ParticipantModuleCycleID = cycle.ParticipantModuleCycleID;

            ParticipantModule mod = (from c in pim.ParticipantModule where c.ParticipantID == participant.ParticipantID select c).FirstOrDefault();
            if (mod.PQRS == null)
                LabelPQRSInceptiveProgramStatus.Text = "Not Selected";
            else
            {
                if (mod.PQRS == true)
                    LabelPQRSInceptiveProgramStatus.Text = "Included";
                else
                    LabelPQRSInceptiveProgramStatus.Text = "Not Included";
                // PQRS Selection
                PlanProgressPct = PlanProgressPct + 10;
            }

            ParticipantModuleCycle pmc = pim.ParticipantModuleCycle.First(p => p.ParticipantModuleCycleID == ParticipantModuleCycleID);
            if (pmc.PECNextID == 0)
            {
                LabelPECStatus.Text = "Included";
                LabelPECComments.Text = "Pending ABO Creation Request.";
            }
            else if (pmc.PECNextID == 1)
            {
                LabelPECStatus.Text = "Included";
            }
            else if (pmc.PECCurrentID == null)
                LabelPECStatus.Text = "Not Selected";
            else
            {
                if (pmc.PECCurrentID > 0)
                {
                    LabelPECStatus.Text = "Included";
                    LabelPECPriorData.Text = "Yes";
                    var pecSurveysPerCandidate = (from ps in pim.PECSurveysPerCandidate_V
                                                  where ps.CandidateID == participant.CandidateID
                                                  orderby ps.ParticipantCycleID descending
                                                  select ps).First();
                    LabelPECComments.Text = "Survey which was initiated " + Convert.ToDateTime(pecSurveysPerCandidate.StartedDate.ToString()).ToShortDateString() + " is included";
                }
                else
                    LabelPECStatus.Text = "Not Included";
            }
            // Previous Charts
            if (pmc.SelectPreviousChartDataComplete==true)
                PlanProgressPct = PlanProgressPct + 10;
            // New Modules
            if (pmc.SelectNewModulesComplete == true)
                PlanProgressPct = PlanProgressPct + 20; // this 20% includes module confirmation
            // PEC Selection
            if (pmc.SelectPECComplete == true)
                PlanProgressPct = PlanProgressPct + 10;
            // Plan completed
            if (pmc.ModuleSelectionComplete == true)
                PlanProgressPct = PlanProgressPct + 10;

            var participantModulesSelected = (from pms in pim.ParticipantPlanStatus_V
                                                                       where pms.ParticipantModuleCycleID == ParticipantModuleCycleID
                                                                       select pms);
            RepeaterModules.DataSource = participantModulesSelected;
            RepeaterModules.DataBind();

            for (var i = 0; i <= RepeaterModules.Items.Count - 1; i++)
            {
                Session[Constants.SESSION_WORKINGMODULEID] = Convert.ToInt32(((HiddenField)RepeaterModules.Items[i].FindControl("HiddenFieldModuleID")).Value);
                break;
            }

            if (cycle.ImprovementPlanComplete == true)
                ModuleSelectionLink.HRef = "ModuleDetails.aspx";
            if ((pmc.ModuleSelectionCompletedDate != null) || (PlanProgressPct < 50))
            {
                LinkButtonSave.Visible = false;
            }
            else
            {
                LinkButtonSave.Visible = true;
            }
            if (pmc.SelectNewModulesComplete == true) 
            {
                var missingChartRegistration = (from cr in pim.ChartRegistration
                                                where cr.ParticipantModuleCycleID == ParticipantModuleCycleID
                                                 & cr.Completed == false
                                                select cr).Count();
                if (missingChartRegistration == 0)
                {
                   
                    PlanProgressPct = PlanProgressPct + 40;
                }
              
            }

            HiddenFieldPlanProgressPct.Value = PlanProgressPct.ToString();
            if (mod.PQRS != null)
            {

                RadioButtonListPqrs.Items.FindByValue(mod.PQRS.ToString()).Selected = true;
            }

        }
    }
    protected void LinkButtonSave_Click(object sender, CommandEventArgs e)
    {
        using (NetHealthPIMEntities pim = new NetHealthPIMEntities())
        {
            ParticipantModuleCycle participantModuleCycle = pim.ParticipantModuleCycle.First(p => p.ParticipantModuleCycleID == ctx.ActiveModuleCycleID);
            participantModuleCycle.ModuleSelectionComplete = true;
            participantModuleCycle.ModuleSelectionCompletedDate = DateTime.Today;
            pim.SaveChanges();
            
        }
        LinkButtonNext_Click(sender, e);
    }
    protected void LinkButtonNext_Click(object sender, CommandEventArgs e)
    {
        Response.Redirect(CycleManager.FirstDashboardWhereYouLeftOff(ctx.ActiveModuleCycleID, ctx.ParticipantID));
    }
    protected void ButtonPqrsUpdate_Click(object sender, EventArgs e)
    {
        using (NetHealthPIMEntities pim = new NetHealthPIMEntities())
        {
            bool selection = Convert.ToBoolean(RadioButtonListPqrs.SelectedItem.Value);
            ParticipantModule mod = (from c in pim.ParticipantModule where c.ParticipantID == ctx.ParticipantID select c).FirstOrDefault();
            mod.PQRS = selection;
            pim.SaveChanges();
        }
    }
}
