﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using NetHealthPIMModel;
using System.Data.Objects;
using System.Transactions;

public partial class abo_MeasuresSelected : BasePage
{
    bool viewMode = true;
    static int rowIndex = 1;
    static string MeasureTitle = "";
    int NumberOfMeasures = 10;
    static int OutcomeMeasure = 0;

    protected void Page_Load(object sender, EventArgs e)
    {
            InitializePage();
            int cycleID = ctx.ActiveModuleCycleID;
            OutcomeMeasure = 0;

            using (NetHealthPIMEntities pim = new NetHealthPIMEntities())
            {
               
                var moduleExists = (from pms in pim.ParticipantModuleSelection
                            where pms.ParticipantModuleCycleID == cycleID
                            select pms).Count();
                if (moduleExists == 0) 
                {
                    var selectModule = (from pms in pim.ParticipantModuleSelection
                                        where pms.ParticipantModuleCycleID == cycleID
                                        select pms).FirstOrDefault();
                    Session[Constants.SESSION_WORKINGMODULEID] = selectModule.ModuleID;
                }

                rowIndex = 1; // reset counter
                System.Linq.IQueryable report;

                ParticipantModuleCycle cycle = pim.ParticipantModuleCycle.First(c => c.ParticipantModuleCycleID == cycleID);

                // bring previous cycle if we are currently on cycle 2
                if (cycle.CycleNumber == 2)
                {
                    cycle.PrevParticipantModuleCycleReference.Load();
                    ParticipantModuleCycle prev = cycle.PrevParticipantModuleCycle;
                    cycle = prev;
                }

                cycleID = cycle.ParticipantModuleCycleID;

                var isGroupMeasures = false;
                if (ctx.ParticipantGroupID != 0) //user belong to a group
                    if (ctx.ParticipantID == ctx.LeaderParticipantID)
                        isGroupMeasures = true;
                if (isGroupMeasures == true)
                {
                    NumberOfMeasures = (from pm in pim.ParticipantGroupMeasure
                                        from ma in pim.MeasureAggregate
                                        from mgm in pim.MeasureGroupModule
                                        from m in pim.Module
                                        where pm.MeasureID == ma.Measure.MeasureID
                                           && pm.ParticipantGroupID == ctx.ParticipantGroupID
                                           && pm.Measure.MeasureType.MeasureTypeID == 1
                                           && pm.IncludeInImprovementPlan == true
                                           && ma.Module.ModuleID == ctx.ActiveModuleID
                                           && ma.MeasureGroup.MeasureGroupID == mgm.MeasureGroupID
                                           && ma.MeasureAggregateType.MeasureAggregateTypeID == 1
                                           && ma.Module.ModuleID == m.ModuleID
                                        select pm).Count();
                    // Get GROUP measure aggregates 
                    report = from pm in pim.ParticipantGroupMeasure
                             from ma in pim.MeasureAggregate
                             from mgm in pim.MeasureGroupModule
                             from m in pim.Module
                             where pm.MeasureID == ma.Measure.MeasureID
                                && pm.ParticipantGroupID == ctx.ParticipantGroupID
                                && pm.Measure.MeasureType.MeasureTypeID == 1
                                && pm.IncludeInImprovementPlan == true
                                && ma.Module.ModuleID == ctx.ActiveModuleID
                                && ma.MeasureGroup.MeasureGroupID == mgm.MeasureGroupID
                                && ma.MeasureAggregateType.MeasureAggregateTypeID == 1
                                && ma.Module.ModuleID == m.ModuleID
                             orderby mgm.MeasureGroupSortOrder
                             select new PerformanceReportData
                             {
                                 MeasureID = pm.MeasureID,
                                 MeasureGroupSortOrder = mgm.MeasureGroupSortOrder,
                                 MeasureTitle = pm.Measure.MeasureTitle,
                                 MeasureClinicalRecommendation = pm.Measure.MeasureClinicalRecommendation,
                                 MeasureLongDescription = pm.Measure.MeasureLongDescription,
                                 AbstractRecordsIncluded = pm.AbstractRecordsIncluded,
                                 MeasurePercentCurrent = pm.MeasurePercent,
                                 MeasureGoal = pm.MeasureGoal,
                                 PeerData = ma.MeasurePercent,
                                 IncludeInImprovementPlan = pm.IncludeInImprovementPlan,
                                 ModuleName = m.ModuleName
                             };
                }
                else
                {
                    NumberOfMeasures = (from pm in pim.ParticipantMeasure
                                        from ma in pim.MeasureAggregate
                                        from m in pim.Module
                                           where pm.MeasureID == ma.Measure.MeasureID
                                           && pm.ParticipantModuleCycle.ParticipantModuleCycleID == cycleID
                                           && pm.IncludeInImprovementPlan == true
                                           && ma.MeasureAggregateType.MeasureAggregateTypeID == 1
                                           && ma.Module.ModuleID == m.ModuleID
                                        select pm).Count();
                    // Get measure aggregates 
                    report = from pm in pim.ParticipantMeasure
                             from ma in pim.MeasureAggregate
                             from m in pim.Module
                             from me in pim.Measure
                             where pm.MeasureID == ma.Measure.MeasureID
                                && pm.ParticipantModuleCycle.ParticipantModuleCycleID == cycleID
                                && pm.IncludeInImprovementPlan == true
                                && ma.MeasureAggregateType.MeasureAggregateTypeID == 1
                                && ma.Module.ModuleID == m.ModuleID
                                && pm.MeasureID == me.MeasureID
                             orderby pm.MeasureID 
                             select new PerformanceReportData
                             {
                                 MeasureID = pm.MeasureID,
                                 MeasureGroupSortOrder = 1,
                                 MeasureTitle = pm.Measure.MeasureTitle,
                                 MeasureQualityIndicator = pm.Measure.MeasureQualityIndicator,
                                 MeasureClinicalRecommendation = pm.Measure.MeasureClinicalRecommendation,
                                 MeasureLongDescription = pm.Measure.MeasureLongDescription,
                                 AbstractRecordsIncluded =(int)pm.AbstractRecordsIncluded,
                                 MeasurePercentCurrent = pm.MeasurePercent,
                                 MeasureGoal = pm.MeasureGoal,
                                 PeerData = ma.MeasurePercent,
                                 IsPhase2Flag = pm.ParticipantModuleCycle.Phase2Flag,
                                 IncludeInImprovementPlan = pm.IncludeInImprovementPlan,
                                 ModuleName = m.ModuleName,
                                 MeasureTypeID = me.MeasureType.MeasureTypeID
                             };
                }
                string q = Support.TraceLinqSQL(report);
                
                ListViewMeasureGoals.DataSource = report;
                ListViewMeasureGoals.DataBind();
                ListViewMeasureGoals.Visible = true;
                int totalMeasures = ListViewMeasureGoals.Items.Count;
                HiddenFieldOutcomeMeasures.Value = OutcomeMeasure.ToString();
                HiddenFieldTotalMeasures.Value = totalMeasures.ToString();

                if (viewMode)
                {
                    PanelInformation.Visible = false;
                    ButtonSubmit.Visible = false;
                }
                
            }
    }
    protected void InitializePage()
    {
        string breadCrumb = Constants.BC_DASHBOARD_LINK + "" + Constants.BC_SELECTED_MEASURES_FOR_IMPROVEMENT;
        ((PIMMasterPage)Page.Master).SetTitle(breadCrumb);
        ((PIMMasterPage)Page.Master).SetStatus(2);
        ((PIMMasterPage)Page.Master).SetHeader("Delete Measures for Improvement");
    }
    protected void ListViewMeasureGoals_OnItemDataBound(object sender, ListViewItemEventArgs e)
    {
        if (e.Item.ItemType == ListViewItemType.DataItem)
        {
            PerformanceReportData data = (PerformanceReportData)((ListViewDataItem)e.Item).DataItem;
            ((Label)e.Item.FindControl("LabelMeasureNumber")).Text = (rowIndex++).ToString();
            HyperLink chartsIncluded = (HyperLink)e.Item.FindControl("LinkAbstractRecordsIncluded");
            LinkButton LinkButtonDeleteMeasure = (LinkButton)e.Item.FindControl("LinkButtonDeleteMeasure");
            if (LinkButtonDeleteMeasure != null)
            {
                LinkButtonDeleteMeasure.Attributes.Add("onclick", "return confirmDelete(" + data.MeasureTypeID.ToString() + ")");
                if (data.MeasureTypeID == 4)
                    OutcomeMeasure++;
            }
            if (viewMode)
            {
                ((TextBox)e.Item.FindControl("TextBoxMeasureGoal")).Visible = false;
                ((Label)e.Item.FindControl("LabelPercentSign")).Visible = false;
                ((Label)e.Item.FindControl("LabelMeasureGoal")).Visible = true;
            }
            if (NumberOfMeasures <= Constants.MIN_NUMBER_OF_MEASURES)
            {
                ((LinkButton)e.Item.FindControl("LinkButtonDeleteMeasure")).Visible = false;
            }
        }
    }
    protected void ButtonSubmit_Click(object sender, EventArgs e)
    {

    }
    protected void LinkButtonDeleteMeasure_Click(object sender, CommandEventArgs e)
    {
        int MeasureID = Convert.ToInt32(e.CommandArgument.ToString());
        int cycleID = ctx.ActiveModuleCycleID;
        
        using (NetHealthPIMEntities pim = new NetHealthPIMEntities())
        {
            ParticipantModuleCycle cycle = pim.ParticipantModuleCycle.First(c => c.ParticipantModuleCycleID == cycleID);

            if (cycle.CycleNumber == 2)
            {
                cycle.PrevParticipantModuleCycleReference.Load();
                ParticipantModuleCycle prev = cycle.PrevParticipantModuleCycle;
                cycle = prev;
            }

            int PREVParticipantModuleCycleID = cycle.ParticipantModuleCycleID;
            CycleManager.DeleteMeasure(PREVParticipantModuleCycleID, cycleID, MeasureID);
        }
        
        Response.Redirect("MeasuresSelected.aspx");

    }
}
