﻿<%@ Page Title="" Language="C#" MasterPageFile="~/PIMMasterPage.master" AutoEventWireup="true" CodeFile="ModuleCompletedToDate.aspx.cs" Inherits="abo_ModuleCompletedToDate" %>
<asp:Content ID="Content1" ContentPlaceHolderID="head" Runat="Server">
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder1" Runat="Server">

<asp:Repeater ID="RepeaterModules" runat="server" OnItemDataBound="RepeaterModules_ItemDataBound">
<HeaderTemplate>
    <table class="table">
</HeaderTemplate>
<ItemTemplate>
        <tr runat="server" id="divRowModuleName" visible="false">
            <th colspan="2" class="firstwidth">
                <%# Eval("ModuleCategoryName") %>
            </th>
            <th class="midwidth">Last abstraction completed date</th>
            <th class="lastwidth">Number of times abstracted</th>
        </tr>
        <asp:HiddenField ID="HiddenFieldModuleID" Value='<%# Eval("ModuleID") %>' runat="server" />
        <tr id="currentRow">
            <td class="icon-cell"><asp:Image ID="Image1" runat="server" ImageUrl="~/common/images/icon-dash.png" /></td>
            <td>
                <span class="text">
                    <!-- ID needs to be unique for each item (a) -->
                    <a runat="server" id="hrefModule" class="example"><%# Eval("ModuleName")%></a>
                </span>
            </td>
            <td><asp:Literal runat="server" ID="LiteralLastAbstractionCompletedDate"></asp:Literal><%# Eval("LastAbstractionCompletedDate") %></asp:Literal></td>
            <td><asp:Literal runat="server" ID="LiteralTimesAbstracted"></asp:Literal><%# Eval("TimesAbstracted")%></asp:Literal></td>
        </tr>
        
</ItemTemplate>
<FooterTemplate>
    </table>
</FooterTemplate>    
</asp:Repeater>
    
</asp:Content>
<asp:Content ID="Content3" ContentPlaceHolderID="javascript" Runat="Server">
</asp:Content>

