﻿<%@ Page Title="" Language="C#" MasterPageFile="~/PIMMasterPage.master" AutoEventWireup="true" CodeFile="SelectMeasures.aspx.cs" Inherits="copd_SelectMeasures" %>

<asp:Content ID="Content1" ContentPlaceHolderID="head" Runat="Server">

    <link type="text/css" href="../common/css/atooltip.css" rel="stylesheet"  media="screen" />

    <style>
        tr.diff-color td, tr.module-name td {
            background-color: #0B91B7;
            color: #fff;
            font-weight:bold;
            border-top:none;
            border-bottom:none;
        }

        tr.module-name td {
            color: #000;
            font-size:1.2em;
            /*text-shadow: 1px 1px 0 #fff, -1px 1px 0 #fff,1px -1px 0 #fff,-1px -1px 0 #fff;*/
        }

        .table {
            margin-bottom: 20px;
        }

        #PM1 {
            cursor:pointer;
        }

        .first-cell {
            width: 25%;
        }
    #iframeModal {
        width:100%;
        height:100%;
        border:none;
    }
    #measureRationale {
        padding-right:0;
    }
    </style>	

</asp:Content>

<asp:Content ID="Content2" ContentPlaceHolderID="javascript" Runat="Server">
    <script type="text/javascript" src="../common/js/jquery.atooltip.js"></script>
    <script src="http://code.jquery.com/ui/1.10.3/jquery-ui.js"></script>
    <script type="text/javascript">
        $(function () {
            $('#PM1').aToolTip({
                clickIt: true,
                tipContent: '<b>Feedback Report Legend</b><br><br><b>Self</b> = Self-assessment goals<br><b>My Goal</b> = Target that you choose for your improvement<br><b>Initial Data</b> = Calculated measure compiled from initial chart abstractions<br><b>Final Data</b> = Calculated measure compiled from second chart abstraction'
            });
        });
        $(document).ready(function() {
            $("#measureRationale").dialog({
                autoOpen: false,
                modal: true,
                height: 350,
                width: 600
            });
        });

        function openwindowj(measureID) {
            $("#iframeModal").removeAttr("src");
            $("#iframeModal").attr("src", "MeasureRationale.aspx?mid=" + measureID);
            $("#measureRationale").dialog("open");
        }        
    </script>

    <script type="text/javascript">
        function validateForm() {

            var processMeasuresCount = 0;
            $("span.ProcessMeasures input:checked").each(function() {
                processMeasuresCount++;
                //var sThisVal = (this.checked ? $(this).val() : "");
            });

            var outcomeMeasures = 0;
            $("span.OutcomeMeasures input:checked").each(function() {
                outcomeMeasures++;
            });

            var existsOutcomeMeasures = 0;
            $("span.OutcomeMeasures input").each(function() {
                existsOutcomeMeasures++;
            });

//            if (processMeasuresCount == 0) {
//                alert("Please select at least one Process Measure for improvement.");
//                return false;
//            }

            if ((existsOutcomeMeasures > 0) && (outcomeMeasures == 0)) {
                alert("Please select at least one Outcome Measure for improvement.");
                return false;
            }
            
            var total = 0;
            var min = document.getElementById("<%= HiddenMinImprovementMeasures.ClientID %>").value;
            var max = document.getElementById("<%= HiddenMaxImprovementMeasures.ClientID %>").value;

            var inputElements = document.getElementsByTagName("INPUT");
            for (var i = 0; i < inputElements.length; i++) {
                if (inputElements[i].checked == true) total++;
            }
            if (total >= min && total <= max) {
                return true;
            } else {
                // Change this to a resource or server side label
                alert(min + " to " + max + " measures must be selected before proceeding and one of those selected measures must be an outcome measure");
                return false;
            }
        }

        function validateRationale() {
            var total = 0;

            var inputElements = document.getElementsByTagName("INPUT");

            for (var i = 0; i < inputElements.length; i++) {
                if ((inputElements[i].type == 'checkbox') && (inputElements[i].name.indexOf("CheckBoxListRationale") !== -1)) {
                    if (inputElements[i].checked == true) total++;
                }
            }
            if (total >= 1) {
                return true;
            } else {
                alert("Please select at least one reason why you choose these measures.");
                return false;
            }
        }

        function checkOther(el, txtID) {
            if (el.checked) {
                document.getElementById(txtID).disabled = false;
            }
            else {
                document.getElementById(txtID).disabled = true;
                document.getElementById(txtID).value = "";
            }
        }

        function CheckGivenRel(item, num) {
            if ($(item).is(':checked')) {
                $("span[dataval='some" + num + "']").find("input").prop("checked", true);
            }
            else {
                $("span[dataval='some" + num + "']").find("input").prop("checked", false);
            }
           
        }



    </script>

</asp:Content>

<asp:Content ID="Content3" ContentPlaceHolderID="ContentPlaceHolder1" Runat="Server">
   <div id='measureRationale'><iframe src="" id="iframeModal"></iframe></div>
    <asp:HiddenField ID="HiddenMinImprovementMeasures" runat="server" />
    <asp:HiddenField ID="HiddenMaxImprovementMeasures" runat="server" />
    
    <asp:Panel ID="PanelInformationPhase1" runat="server">
        <asp:Label ID="LabelImprovementMeasures" Visible="false" runat="server" Text=""></asp:Label>
        <p>
           Please review the performance feedback and select between three and five areas of your performance that you would like to improve. It is recommended that you target indicators that are below 50% in your baseline performance data for improvement.   </p>
    </asp:Panel>
    <p>
        Process measures measure the processes of care used to treat patients and the resulting outcomes.  This information will be used by you to:
    </p>
    <ul>
        <li>Evaluate your current processes of care and their impact on patient outcomes</li>
        <li>Develop an Improvement Plan to help you improve on your process measures</li>
        <li>Establish performance goals based on these process measures to assess patient care</li>
    </ul>
          
    
              
    <table class="table">
    <tr>
        <th colspan="4">&nbsp;&nbsp;&nbsp;  <asp:Literal ID="LiteralModuleName1" runat="server"></asp:Literal></th>
    </tr>
    <asp:ListView runat="server" ID="ListViewProcessMeasures" onitemdatabound="ListViewProcessMeasures_OnItemDataBound">
        <LayoutTemplate>
            <tr class="diff-color">
                <td class="first-cell">Measure for Improvement</td>
                <td>Data Field</td>
                <td class="width-rationale">Chart Reviewed</td>
                <td>Data Analysis</td>
            </tr>
                        
            <asp:PlaceHolder runat="server" ID="itemPlaceholder"></asp:PlaceHolder>
                        
        </LayoutTemplate>
        <ItemTemplate>
            <tr runat="server" id="divRowModuleName" visible="false" class="module-name">
                <td>
                    <asp:Literal ID="LiteralModuleName" runat="server" Text='<%# Bind("ModuleName") %>'></asp:Literal>
                </td>
                <td></td>
                <td></td>
                <td></td>
             
            </tr>
            <tr runat="server" id="divRowChart">
                <td>
                    <asp:CheckBox ID="CheckBoxIncludeInImprovementPlan"  runat="server" CssClass="ProcessMeasures"/>
                    <asp:HiddenField ID="MeasureID" Value='<%# Bind("MeasureID") %>' runat="server" />
                    <br />
                        <asp:Literal ID="Literal2" runat="server" Text='<%# Bind("ModuleName") %>'></asp:Literal>
                    <asp:Label ID="LabelMeasureTitle" CssClass="label_highlight" runat="server" Text='<%# Bind("MeasureQualityIndicator") %>'></asp:Label>
                </td>
                <td>
                    <asp:Label ID="LabelMeasureLongDescription" runat="server" Text='<%# Bind("MeasureLongDescription") %>'></asp:Label>
                </td>
              
                  <td><asp:Label ID="LabelChartConsideredPR" runat="server"></asp:Label></td>
                <td>
                    <asp:Chart ID="ChartPerformance" runat="server" Width="300" Height="80" >
                        <Series>
                            <asp:Series Name="Initial Data" ChartType="Bar" Color="#2f5199"  BorderColor="#2f5199"  CustomProperties="DrawingStyle=Wedge" Palette="Berry" YValueMembers="MeasurePercentCurrent" IsValueShownAsLabel="true" LabelForeColor="#000000" LabelBackColor="Transparent">
                            </asp:Series>
                            <asp:Series Name="Peer Data" ChartType="Bar" Color="#ffb200" BorderColor="#ffb200" CustomProperties="DrawingStyle=Wedge" Palette="Berry" YValueMembers="PeerData" IsValueShownAsLabel="true" LabelForeColor="#000000" LabelBackColor="Transparent" >
                            </asp:Series>
                        </Series>
                        <ChartAreas>
                            <asp:ChartArea Name="MainChartArea">
                                <AxisX Maximum="3" LineColor="Transparent" LineWidth="1">
                                    <MajorGrid Enabled="false" />
                                    <LabelStyle IsEndLabelVisible="false" />
                                    <CustomLabels>
                                    </CustomLabels>
                                </AxisX>
                                <AxisY Maximum="100" Interval="20" />
                            </asp:ChartArea>
                        </ChartAreas>
                    </asp:Chart>
                </td>
            </tr>
        </ItemTemplate>
    </asp:ListView>
</table>
     <asp:Panel ID="PanelCombinedmes" runat="server">
    <table id="StrataTable" runat="server" class="table">
    <tr>
        <th colspan="5">&nbsp;&nbsp;&nbsp;  </th>
    </tr>
        <tr class="diff-color">
            <td style="width:30%"></td>
             <td style="width:20%">Chart Reviewed</td> 
           <td style="width:50%">Data Analysis</td> 
        </tr>
     
    </table>
       </asp:Panel>
    <p>Outcome measures provide insight into a physician's treatment patterns for the purposes of evaluating efficiency and impact on patient care.  It is recommended that you identify outcome measures that are pertinent to your practice and that are most likely to have the greatest impact on your patient care.  Evaluation and assessment of outcome measures related to your practice patterns provides the following benefits:</p>
    <ul>
        <li>Identifies areas of improvement that promote improved patient care</li>
        <li>Promotes improvement opportunities and guidance of treatment decisions</li>
        <li>Encourages continuous learning by generating interest in professional education and training</li>
    </ul>
    <!-- Outcome Measures -->
	<table class="table">
        <tr>
            <th colspan="4">Outcome Measures <asp:Literal ID="LiteralModuleName2" runat="server"></asp:Literal></th>
        </tr>
        <asp:ListView runat="server" ID="ListViewOutcomeMeasures" onitemdatabound="ListViewOutcomeMeasures_OnItemDataBound">
            <LayoutTemplate>
                <tr class="diff-color">
                    <td class="first-cell">Measure for Improvement</td>
                    <td>Data Field</td>
                    <td class="width-rationale">Chart Reviewed</td>
                    <td>Data Analysis</td>
                </tr>
                        
                <asp:PlaceHolder runat="server" ID="itemPlaceholder"></asp:PlaceHolder>
                        
            </LayoutTemplate>
            <ItemTemplate>
                <tr runat="server" id="divRowModuleName" visible="false" class="module-name">
                    <td>
                        <asp:Literal ID="LiteralModuleName" runat="server" Text='<%# Bind("ModuleName") %>'></asp:Literal>
                    </td>
                    <td></td>
                    <td></td>
                    <td></td>
                   
                </tr>
                <tr runat="server" id="divRowChart">
                    <td>
                        <asp:CheckBox ID="CheckBoxIncludeInImprovementPlan" runat="server"  CssClass="OutcomeMeasures"/>
                        <asp:HiddenField ID="MeasureID" Value='<%# Bind("MeasureID") %>' runat="server" /><br />
                        <asp:Literal ID="Literal2" runat="server" Text='<%# Bind("ModuleName") %>'></asp:Literal>
                        <asp:Label ID="LabelMeasureTitle" CssClass="label_highlight" runat="server" Text='<%# Bind("MeasureQualityIndicator") %>'></asp:Label>
                    </td>
                    <td>
                        <asp:Label ID="LabelMeasureLongDescription" runat="server" Text='<%# Bind("MeasureLongDescription") %>'></asp:Label>
                    </td>
                   
                      <td><asp:Label ID="LabelChartConsideredUM" runat="server"></asp:Label></td>
                    <td>
                               <asp:Chart ID="ChartPerformance" runat="server" Width="320" Height="120">
                            <Series>
                                <asp:Series Name="Initial Data" ChartType="Bar" Color="#2f5199"  BorderColor="#2f5199"  CustomProperties="DrawingStyle=Wedge" Palette="Berry" YValueMembers="MeasurePercentCurrent" IsValueShownAsLabel="true" LabelForeColor="#000000" LabelBackColor="Transparent">
                                </asp:Series>
                                <asp:Series Name="Peer Data" ChartType="Bar" Color="#ffb200" BorderColor="#ffb200" CustomProperties="DrawingStyle=Wedge" Palette="Berry" YValueMembers="PeerData" IsValueShownAsLabel="true" LabelForeColor="#000000" LabelBackColor="Transparent" >
                                </asp:Series>
                            </Series>
                            <ChartAreas>
                                  <asp:ChartArea Name="MainChartArea">
                        <AxisX Maximum="5" LineColor="Transparent" LineWidth="1">
                            <MajorGrid Enabled="false" />
                            <LabelStyle IsEndLabelVisible="false" />
                            <CustomLabels>
                            </CustomLabels>
                        </AxisX>
                        <AxisY Maximum="100" Interval="20" />
                    </asp:ChartArea>
                            </ChartAreas>
                        </asp:Chart>
                    </td>
                </tr>
            </ItemTemplate>
        </asp:ListView>
    </table>
            <div  runat="server" id="PECMeasuresTable">
                <p>Patient Experience of Care measures provide insight into your patients’ perception of care. It is recommended that you review the measures below and identify any areas in which a focused improvement will likely to have an impact on your patient care.</p>
             <table class="table">
                <tr>
                    <th colspan="4">PEC  Measures <asp:Literal ID="Literal1" runat="server"></asp:Literal></th>
                </tr>
                <asp:ListView runat="server" ID="ListViewPECMeasures" onitemdatabound="ListViewPECMeasures_OnItemDataBound">
                    <LayoutTemplate>
                        <tr class="diff-color">
                            <td class="first-cell">Measure for Improvement</td>
                            <td>Data Field</td>
                            <td>Rationale</a></td>
                            <td>Data Analysis</td>
                        </tr>
                        
                        <asp:PlaceHolder runat="server" ID="itemPlaceholder"></asp:PlaceHolder>
                        
                    </LayoutTemplate>
                    <ItemTemplate>
                        <tr runat="server" id="divRowModuleName" visible="false" class="module-name">
                            <td>
                                <asp:Literal ID="LiteralModuleName" runat="server" Text='<%# Bind("ModuleName") %>'></asp:Literal>
                            </td>
                            <td></td>
                            <td></td>
                            <td></td>
                        </tr>
                        <tr runat="server" id="divRowChart">
                            <td>
                                <asp:CheckBox ID="CheckBoxIncludeInImprovementPlan" runat="server"  CssClass="PECMeasures"/>
                                <asp:HiddenField ID="MeasureID" Value='<%# Bind("MeasureID") %>' runat="server" />
                                <asp:Label ID="LabelMeasureTitle" CssClass="label_highlight" runat="server" Text='<%# Bind("MeasureQualityIndicator") %>'></asp:Label>
                            </td>
                            <td>
                                <asp:Label ID="LabelMeasureLongDescription" runat="server" Text='<%# Bind("MeasureLongDescription") %>'></asp:Label>
                            </td>
                            <td>
                                <a href='javascript:void();' onclick='openwindowj(<%# Eval("MeasureID") %>);'>Rationale</a>
                            </td>
                            <td>
                                <asp:Chart ID="ChartPerformance" runat="server" Width="300" Height="80" >
                                    <Series>
                                        <asp:Series Name="Initial Data" ChartType="Bar" Color="#2f5199"  BorderColor="#2f5199"  CustomProperties="DrawingStyle=Wedge" Palette="Berry" YValueMembers="MeasurePercentCurrent" IsValueShownAsLabel="true" LabelForeColor="#000000" LabelBackColor="Transparent">
                                        </asp:Series>
                                        <asp:Series Name="Peer Data" ChartType="Bar" Color="#ffb200" BorderColor="#ffb200" CustomProperties="DrawingStyle=Wedge" Palette="Berry" YValueMembers="PeerData" IsValueShownAsLabel="true" LabelForeColor="#000000" LabelBackColor="Transparent" >
                                        </asp:Series>
                                    </Series>
                                    <ChartAreas>
                                        <asp:ChartArea Name="MainChartArea">
                                            <AxisX Maximum="3" LineColor="Transparent" LineWidth="1">
                                                <MajorGrid Enabled="false" />
                                                <LabelStyle IsEndLabelVisible="false" />
                                                <CustomLabels>
                                                </CustomLabels>
                                            </AxisX>
                                            <AxisY Maximum="100" Interval="20" />
                                        </asp:ChartArea>
                                    </ChartAreas>
                                </asp:Chart>
                            </td>
                        </tr>
                    </ItemTemplate>
                </asp:ListView>
            </table>
            </div>

            <asp:Panel ID="PanelRationale" runat="server" Visible="false">
            <p><strong>Why did you choose these measures? (Check all that apply)</strong></p>
            <asp:GridView ID="GridViewRationale" runat="server" CssClass="table center_area"
                HeaderStyle-CssClass="table_header" 
                AutoGenerateColumns="False" EnableViewState="true" 
                onrowdatabound="GridViewRationale_RowDataBound">
                <Columns>
                    <asp:TemplateField ItemStyle-Wrap="false" HeaderText="Select Rationale" ItemStyle-VerticalAlign="Top">
                        <ItemTemplate>
                            <asp:HiddenField ID="MeasureID" Value='<%# Bind("MeasureID") %>' runat="server" />
                            <asp:Label ID="LabelMeasureTitleRationale" CssClass="label_highlight" runat="server" Text='<%# Bind("MeasureTitle") %>'></asp:Label><br />
                            <asp:CheckBoxList ID="CheckBoxListRationale" runat="server" DataTextField="RationaleTypeName" 
                            DataValueField="RationaleTypeID" RepeatLayout="Flow" >
                            </asp:CheckBoxList>
                            <asp:TextBox ID="TextBoxRationaleOther" runat="server" MaxLength="100" Enabled="false" Width="300"></asp:TextBox>
                        </ItemTemplate>            
                    </asp:TemplateField>
                </Columns>
            </asp:GridView>
            <br />
            </asp:Panel>
            <div class="button-box">
                <asp:LinkButton ID="LinkButtonBack" runat="server" Text="Previous Page"  Visible="false" CssClass="button" PostBackUrl="SelectMeasureBack.aspx" />
                <asp:LinkButton ID="LinkButtonDashboard" runat="server" Text="Return To Status Page"  OnClick="LinkButtonDashboard_Click" CssClass="button" />
                <asp:LinkButton ID="ButtonDevelopImprovementPlan" runat="server" Text="Develop Improvement Plan"  OnClientClick="return validateRationale();" OnClick="ButtonDevelopImprovementPlan_Click" Visible="false" CssClass="button" />
                <asp:LinkButton ID="LinkButtonNext" runat="server" Text="Next"  OnClientClick="return validateForm();" CssClass="button"/>
                
            </div>
            <!--
            <div style="float:right;">
            <table>
                <tr>
                    <td>
                        <asp:LinkButton ID = "LinkButtonDashboardq" runat="server" PostBackUrl="Dashboard.aspx">
                        <div class="button_orange">
                            <img src="../common/images/small_button.png" alt="" width="150px" height="28px"/>
                            <h5><span>Return To Dashboard</span></h5>
                        </div>
                        </asp:LinkButton>
                    </td>
                    <td>
                        <asp:LinkButton ID = "LinkButtonDevelopImprovementPlan" runat="server" OnClick="ButtonDevelopImprovementPlan_Click"  Visible="false">
                        <div class="button_orange">
                            <img src="../common/images/small_button.png" alt="" width="180px" height="28px"/>
                            <h5><span>Develop Improvement Plan</span></h5>
                        </div>
                        </asp:LinkButton>
                    </td>
                    <td>
                        <asp:LinkButton ID = "LinkButtonNextq" runat="server" OnClientClick="return validateForm();">
                        <div class="button_orange">
                            <img src="../common/images/small_button.png" alt="" width="125px" height="28px"/>
                            <h5><span>Next</span></h5>
                        </div>
                        </asp:LinkButton>
                    </td>
                </tr>
            </table>
            </div>
            -->
            <!--
            <div class="initial_performance_bottom">
              <ul>
                <li><a href="Dashboard.aspx"><img src="../common/images/return_to_dashboard_btn.jpg" alt="" /></a></li>
                <li><a href="SelectMeasures.aspx"><img src="../common/images/proceed_improvement_btn.jpg" alt="" /></a></li>
              </ul>
            </div>-->

   
        <!--
        <asp:Button ID="ButtonSelectRationale" runat="server" Text="Next" 
            OnClientClick="return validateForm();" />
        <asp:Button ID="ButtonDevelopImprovementPlanq" runat="server" 
            Text="Develop Improvement Plan" Visible="false" onclick="ButtonDevelopImprovementPlan_Click"
             />-->
</asp:Content>

