﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using NetHealthPIMModel;

public partial class copd_SelfAssessmentComplete : BasePage
{
    protected void Page_Load(object sender, EventArgs e)
    {
        if (!IsPostBack)
        {
            InitializePage();
        }
    }

    protected void InitializePage()
    {
        ((PIMMasterPage)Page.Master).SetTitle("Self Assessment Complete");
        ButtonEnterChartData.PostBackUrl = "~/" + module.ModuleCode.ToLower() + "/AbstractList.aspx";
        int cycleID = ctx.ActiveModuleCycleID;

        using (NetHealthPIMEntities pim = new NetHealthPIMEntities())
        {
            ParticipantModuleCycle cycle = pim.ParticipantModuleCycle.First(c => c.ParticipantModuleCycleID == cycleID);
            cycle.SelfAssessmentLastUpdatedDate = DateTime.Now;
            cycle.SelfAssessmentSurveyCompletedDate = DateTime.Now;
            cycle.SelfAssessmentSurveyComplete = true;
            cycle.LastUpdateDate = DateTime.Now;
            pim.SaveChanges();
        }
    }

}
