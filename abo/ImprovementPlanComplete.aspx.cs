﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.IO;
using System.Web.UI.HtmlControls;
using NetHealthPIMModel;

public partial class copd_ImprovementPlanComplete : BasePage
{
    protected void Page_Load(object sender, EventArgs e)
    {
        if (!IsPostBack)
        {
            InitializePage();
        }
    }

    protected void InitializePage()
    {
        ((PIMMasterPage)Page.Master).SetTitle("Improvement Plan Completed");
        string breadCrumb = Constants.BC_DASHBOARD_LINK + "" + Constants.BC_PERFORMANCE_REPORT_LINK + "" +
                            Constants.BC_IMPROVEMENT_PLAN_LINK + "" + Constants.BC_IMPROVEMENT_PLAN_COMPLETE;
        ((PIMMasterPage)Page.Master).SetTitle(breadCrumb);
        ((PIMMasterPage)Page.Master).SetStatus(3);
        ((PIMMasterPage)Page.Master).SetHeader("Improvement Plan Completed");
        using (NetHealthPIMEntities pim = new NetHealthPIMEntities())
        {
            //JH: I updated it such that only one page shows.  This aligned with an ABO platform update. 4/19/2016
	        //if (Request.UrlReferrer != null)
            //{
            	//if (Request.UrlReferrer.ToString().Contains("ImprovementPlanPartI"))
               		divImprovementPlanComplete.Visible = true;
		        //else
                	//divImprovementPlanPhase.Visible = true;
	       //}
	       //else
           //  	divImprovementPlanPhase.Visible = true;

            ParticipantModuleCycle cycle = pim.ParticipantModuleCycle.First(c => c.ParticipantModuleCycleID == ctx.ActiveModuleCycleID);
            if (cycle.ImprovementPlanCompletedDate != null)
            {
                LabelTodaysDate.Text = ((DateTime)(cycle.ImprovementPlanCompletedDate)).ToShortDateString(); //DateTime.Today.ToShortDateString();
            }
            //LabelImprovementPlanCompleteDate.Text = ((DateTime)(cycle.ImprovementPlanCompletedDate)).ToShortDateString();

          
            var modules = (from pm in pim.ParticipantMeasure
                          join me in pim.Measure on pm.MeasureID equals me.MeasureID
                          join m in pim.Module on me.Module.ModuleID equals m.ModuleID
                          where pm.ParticipantModuleCycleID == ctx.ActiveModuleCycleID
                           && pm.IncludeInImprovementPlan == true
                          select new
                          {
                              ModuleName = m.ModuleName
                          }).Distinct(); 
            
            RepeaterModules.DataSource = modules;
            RepeaterModules.DataBind();
            if (cycle.RemeasureDate != null)
            {
                LabelNextCycleDate1.Text = ((DateTime)(cycle.RemeasureDate)).ToShortDateString();
                LabelNextCycleDate2.Text = ((DateTime)(cycle.RemeasureDate)).ToShortDateString();
                LabelNextCycleDate3.Text = ((DateTime)(cycle.RemeasureDate)).ToShortDateString();
            }


            var cycleid=(from c in pim.ParticipantModuleCycle
                         where c.ParticipantModule.ParticipantID==ctx.ParticipantID
                         && c.CycleNumber==1 select c.ParticipantModuleCycleID).FirstOrDefault();
            var resources = from pm in pim.ParticipantMeasure
                            join m in pim.Measure on pm.MeasureID equals m.MeasureID
                            join mr in pim.MeasureResource on pm.MeasureID equals mr.MeasureID
                            join r in pim.Resource on mr.ResourceID equals r.ResourceID
                            where pm.ParticipantModuleCycleID == cycleid && pm.IncludeInImprovementPlan == true
                            && r.RecommendedFlag == false
                            orderby pm.MeasureID, r.ResourceName
                            select new
                            {
                                MeasureGroupSortOrder = 1,
                                MeasureTitle = m.MeasureTitle,
                                MeasureID = m.MeasureID,
                                ResourceID = r.ResourceID,
                                ResourceTypeID = r.ResourceType.ResourceTypeID,
                                ResourceName = r.ResourceName,
                                ResourceDescription = r.ResourceDescription,
                                URL = r.URL,
                                RecommendedFlag = r.RecommendedFlag,
                                ClientFlag = r.ClientFlag,
                                CostFlag = r.CostFlag,
                                AAOResourceFlag = r.AAOResourceFlag
                            };
            if (resources.Count() == 0)
            {
                spanresources.Visible = false;
            }







        }
    }
}
