﻿using System;
using System.Collections.Generic;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using NetHealthPIMModel;
using System.Linq;
using System.Web.UI.DataVisualization.Charting;

public partial class abo_PracticeReview : BasePage
{
    static string CurrentModuleName = "";
    static int MeasureGroupID = 0;
    static int PreviousMeasureGroupID = -1;
    static string ModuleNameProcessMeasures = "";
    static string ModuleNameOutcomeMeasures = "";
    static string ModuleNameSystemSurvey = "";
    static List<PracticeReport> processMeasureList = null;
    static List<PracticeReport> outcomeMeasureList = null;

    protected void Page_Load(object sender, EventArgs e)
    {
        if (!IsPostBack)
        {
            InitializePage();
        }
    }
    protected void InitializePage()
    {
        string breadCrumb = Constants.BC_MODULES_COMPLETED_TO_DATE_LINK + "" + Constants.BC_PRACTICE_REVIEW;
        ((PIMMasterPage)Page.Master).SetTitle(breadCrumb);
        ((PIMMasterPage)Page.Master).SetStatus(4);
        ((PIMMasterPage)Page.Master).SetHeader("Practice Review");
        int ModuleID = Convert.ToInt32(Request.QueryString["ModuleID"]);
        //int ModuleID = 53; //just for testing
        using (NetHealthPIMEntities pim = new NetHealthPIMEntities())
        {
            Participant participant = pim.Participant.First(p => p.ParticipantID == ctx.ParticipantID);
            //get headers Process Measures
            var report = from pr in pim.PracticeReview_V
                                 where pr.CandidateID == participant.CandidateID
                                    && pr.MeasureTypeID == 3
                                    && pr.ModuleID == ModuleID
                                    && pr.MeasureAggregateTypeID == 1
                                    && pr.ParticipantMeasureTypeID == 1
                                 orderby pr.ModuleID, pr.MeasureID, pr.ParticipantModuleCycleID, pr.MeasureSortOrder
                                 select new PracticeReport
                                 {
                                     ModuleName = pr.ModuleName,
                                     MeasureID = pr.MeasureID,
                                     MeasureSortOrder = (int)pr.MeasureSortOrder,
                                     MeasureQualityIndicator = pr.MeasureQualityIndicator,
                                     MeasureLongDescription = pr.MeasureLongDescription
                                 };
            // get details Process Measures
            processMeasureList = (from pr in pim.PracticeReview_V
                         where pr.CandidateID == participant.CandidateID
                            && pr.MeasureTypeID == 3
                            && pr.ModuleID == ModuleID
                            && pr.MeasureAggregateTypeID == 1
                            && pr.ParticipantMeasureTypeID == 1
                         orderby pr.ModuleID, pr.MeasureID, pr.ParticipantModuleCycleID, pr.MeasureSortOrder
                                  select new PracticeReport
                         {
                             ModuleName = pr.ModuleName,
                             MeasureID = pr.MeasureID,
                             MeasureSortOrder = (int)pr.MeasureSortOrder,
                             MeasureQualityIndicator = pr.MeasureQualityIndicator,
                             MeasureLongDescription = pr.MeasureLongDescription,
                             MeasurePercentCurrent = pr.MeasurePercent,
                             MayNeedImprovement = pr.MayNeedImprovement,
                             AbstractDataCompletedDate = pr.AbstractDataCompletedDate
                         }).ToList();
            ListViewProcessMeasures.DataSource = report.Distinct(); 
            ListViewProcessMeasures.DataBind();

            ModuleNameProcessMeasures = "";
            //get headers Outcome Measures
            var headerOutcomeMeasures = from pr in pim.PracticeReview_V
                                 where pr.CandidateID == participant.CandidateID
                                    && (pr.ParticipantMeasureTypeID == 3
                                    || pr.ParticipantMeasureTypeID == 1
                                    || pr.ParticipantMeasureTypeID == 5)
                                    && pr.ModuleID == ModuleID
                                 orderby pr.ModuleID, pr.MeasureID, pr.ParticipantModuleCycleID, pr.MeasureSortOrder
                                 select new PracticeReport
                                 {
                                     ModuleName = pr.ModuleName,
                                     MeasureID = pr.MeasureID,
                                     MeasureSortOrder = (int)pr.MeasureSortOrder,
                                     MeasureQualityIndicator = pr.MeasureQualityIndicator,
                                     MeasureLongDescription = pr.MeasureLongDescription
                                 };
            // get details Process Measures
            outcomeMeasureList = (from pr in pim.PracticeReview_V
                                  where pr.CandidateID == participant.CandidateID
                                    && (pr.ParticipantMeasureTypeID == 3
                                    || pr.ParticipantMeasureTypeID == 1
                                    || pr.ParticipantMeasureTypeID == 5)
                                    && pr.ModuleID == ModuleID
                                  orderby pr.ModuleID, pr.MeasureID, pr.ParticipantModuleCycleID, pr.MeasureSortOrder
                                  select new PracticeReport
                                  {
                                      ModuleName = pr.ModuleName,
                                      MeasureID = pr.MeasureID,
                                      MeasureSortOrder = (int)pr.MeasureSortOrder,
                                      MeasureQualityIndicator = pr.MeasureQualityIndicator,
                                      MeasureLongDescription = pr.MeasureLongDescription,
                                      MeasurePercentCurrent = pr.MeasurePercent,
                                      MayNeedImprovement = pr.MayNeedImprovement,
                                      AbstractDataCompletedDate = pr.AbstractDataCompletedDate
                                  }).ToList();
            ListViewOutcomeMeasures.DataSource = headerOutcomeMeasures.Distinct();
            ListViewOutcomeMeasures.DataBind();
        }
    }
    protected void ListViewProcessMeasures_OnItemDataBound(object sender, ListViewItemEventArgs e)
    {
        if (e.Item.ItemType == ListViewItemType.DataItem)
        {


            Chart ch = (Chart)e.Item.FindControl("ChartPerformance");

            ch.Series[0].LabelBackColor = System.Drawing.Color.White;
      

            PracticeReport data = (PracticeReport)((ListViewDataItem)e.Item).DataItem;
            HyperLink chartsIncluded = (HyperLink)e.Item.FindControl("LinkAbstractRecordsIncluded");
          
            HyperLink HyperLinkRationale = (HyperLink)e.Item.FindControl("HyperLinkRationale");
      
            HyperLinkRationale.Attributes.Add("onclick", "window.open('MeasureRationale.aspx?mid="+data.MeasureID+"', null, 'height=200,width=760,status=no,toolbar=no,scrollbars=yes,menubar=no,location=no')");
            HyperLinkRationale.Attributes.Add("style", "cursor:pointer");

            System.Web.UI.HtmlControls.HtmlTableRow divRowModuleName = (System.Web.UI.HtmlControls.HtmlTableRow)e.Item.FindControl("divRowModuleName");
            if (ModuleNameProcessMeasures != data.ModuleName)
            {
                divRowModuleName.Visible = true;
                ModuleNameProcessMeasures = data.ModuleName;
            }
         
            int MeasureId = (int)data.MeasureID;
            int barNumber = 0;
            for (var i = 0; i < processMeasureList.Count(); i++)
            {
                if (processMeasureList[i].MeasureID == MeasureId)
                {
                    ch.Series[0].Points.AddY(processMeasureList[i].MeasurePercentCurrent);
                    ch.ChartAreas[0].AxisX.CustomLabels.Add(barNumber + 0.5, barNumber + 1.5,

                        processMeasureList[i].AbstractDataCompletedDate==null ? "" : ((DateTime)processMeasureList[i].AbstractDataCompletedDate).ToShortDateString());
                    barNumber++;
                }
            }
            ch.ChartAreas[0].AxisX.Maximum = barNumber+1;
            ch.Height = 24 * (barNumber + 1);
        }

        PreviousMeasureGroupID = MeasureGroupID;
    }


    protected void ListViewOutcomeMeasures_OnItemDataBound(object sender, ListViewItemEventArgs e)
    {
        if (e.Item.ItemType == ListViewItemType.DataItem)
        {


            Chart ch = (Chart)e.Item.FindControl("ChartPerformance");
         
            ch.Series[0].LabelBackColor = System.Drawing.Color.White;
      

            PracticeReport data = (PracticeReport)((ListViewDataItem)e.Item).DataItem;
         
            HyperLink chartsIncluded = (HyperLink)e.Item.FindControl("LinkAbstractRecordsIncluded");
     
            HyperLink HyperLinkRationale = (HyperLink)e.Item.FindControl("HyperLinkRationale");


            HyperLinkRationale.Attributes.Add("onclick", "window.open('MeasureRationale.aspx?mid=" + data.MeasureID + "', null, 'height=200,width=760,status=no,toolbar=no,scrollbars=yes,menubar=no,location=no')");
            HyperLinkRationale.Attributes.Add("style", "cursor:pointer");

            System.Web.UI.HtmlControls.HtmlTableRow divRowModuleName = (System.Web.UI.HtmlControls.HtmlTableRow)e.Item.FindControl("divRowModuleName");
            if (ModuleNameProcessMeasures != data.ModuleName)
            {
                divRowModuleName.Visible = true;
                ModuleNameProcessMeasures = data.ModuleName;
            }

            int MeasureId =(int)data.MeasureID;
            int barNumber = 0;
            for (var i = 0; i < outcomeMeasureList.Count(); i++)
            {
                if (outcomeMeasureList[i].MeasureID == MeasureId)
                {
                    ch.Series[0].Points.AddY(outcomeMeasureList[i].MeasurePercentCurrent);
                    ch.ChartAreas[0].AxisX.CustomLabels.Add(barNumber + 0.5, barNumber + 1.5, outcomeMeasureList[i].AbstractDataCompletedDate==null ? "" : ((DateTime)outcomeMeasureList[i].AbstractDataCompletedDate).ToShortDateString());
                    barNumber++;
                }
            }
            ch.ChartAreas[0].AxisX.Maximum = barNumber + 1;
            ch.Height = 24 * (barNumber + 1);
        }

        PreviousMeasureGroupID = MeasureGroupID;
    }

}
