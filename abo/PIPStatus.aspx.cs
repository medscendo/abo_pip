﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using NetHealthPIMModel;

public partial class abo_PIPStatus : BasePage
{
    protected void Page_Load(object sender, EventArgs e)
    {
        using (NetHealthPIMEntities pim = new NetHealthPIMEntities())
        {
            ParticipantModuleCycle cycle = pim.ParticipantModuleCycle.First(p => p.ParticipantModuleCycleID == ctx.ActiveModuleCycleID);
            if(cycle.CycleNumber == 2)
                Response.Redirect("Dashboard3.aspx");
            else
                if (cycle.ModuleSelectionComplete == true)
                {
                    if ((cycle.IsComplete == true) && (cycle.RemeasureDate > DateTime.Today))
                        Response.Redirect("ImprovementPlanComplete.aspx");
                    else
                        Response.Redirect("Dashboard.aspx");
                }
                else
                    Response.Redirect("ModuleSelectionReview.aspx");
        }
        
        //if (!IsPostBack)
        //{
        //    InitializePage();
        //}

    }
    protected void InitializePage()
    {
        Session[Constants.SESSION_USERCONTEXT] = ctx;
        LabelParticipantName.Text = ctx.ParticipantName;
        using (NetHealthPIMEntities pim = new NetHealthPIMEntities())
        {
            int cycleID = ctx.ActiveModuleCycleID;
            ParticipantModuleCycle cycle = pim.ParticipantModuleCycle.First(c => c.ParticipantModuleCycleID == cycleID);

            var modulesSelected = (from m in pim.Module
                                   join pms in pim.ParticipantModuleSelection on m.ModuleID equals pms.ModuleID
                                   where pms.ParticipantModuleCycleID == cycle.ParticipantModuleCycleID
                                   select pms).Count();
            if (modulesSelected > 0)
            {
                divModuleSelection.Attributes.Add("class", "col blue_selected");
                blue_bottom.Attributes["src"] = ResolveUrl("../common/images/per.blue_bottom_curve_selected.jpg");
                divPhase1.Visible = true;
                HTMLlinkModuleSelection.HRef = "javascript:alert('Activities have been selected')";
                LiteralStatus.Text = "Active";

                if (cycle.CycleNumber == 1)
                {
                    HTMLlinkModuleSelection.HRef = "ModuleDetails.aspx";
                    HTMLlinkModuleSelection.InnerText = "View Activities Selected";
                }

                var chartModuleSelection = from m in pim.Module
                                           join pms in pim.ParticipantModuleSelection on m.ModuleID equals pms.ModuleID
                                           join pcs in pim.ParticipantChartStatus_V 
                                            on pms.ParticipantModuleCycleID equals pcs.ParticipantModuleCycleID 
                                           where pms.ParticipantModuleCycleID == cycle.ParticipantModuleCycleID
                                           & pcs.ModuleID == m.ModuleID
                                           select new
                                           {
                                               ModuleName = m.ModuleDescription,
                                               ModuleID = m.ModuleID,
                                               ChartStatus = pcs.TotalCharts == pcs.Completed ? "all charts complete" : (pcs.ChartsCompleted == null ? "0" : pcs.ChartsCompleted) + " charts complete"
                                           };
                ListViewModuleLinks.DataSource = chartModuleSelection;
                ListViewModuleLinks.DataBind();


                var missingConfirmModules = (from m in pim.Module
                                             join pms in pim.ParticipantModuleSelection on m.ModuleID equals pms.ModuleID
                                             where pms.ParticipantModuleCycleID == cycle.ParticipantModuleCycleID
                                             & pms.ModuleConfirm == null
                                             select pms).Count();
                if (missingConfirmModules > 0)
                {
                    HyperLinkNextPage.NavigateUrl = "ModuleDetails.aspx";
                    LiteralHelpLeft.Text = "You have selected your activities.  Before continuing, please confirm all activities selected.";
                }
                else
                {
                    if (cycle.CycleNumber == 1)
                    {
                        HTMLlinkReviewYourPractice.Attributes.Add("onclick", "openmodalWin(1)");
                        HTMLlinkAbstractYourCharts.Attributes.Add("onclick", "openmodalWin(2)");
                    }
                    var missingChartRegistration = (from cr in pim.ChartRegistration
                                                   where cr.ParticipantModuleCycleID == cycle.ParticipantModuleCycleID 
                                                    & cr.Completed == false
                                                    select cr).Count();
                    if (missingChartRegistration > 0)
                    {
                        var firstModule = (from cr in pim.ChartRegistration
                                           where cr.ParticipantModuleCycleID == cycle.ParticipantModuleCycleID 
                                           & cr.Completed == false
                                           select cr).First(); 
                        HyperLinkNextPage.NavigateUrl = "OpenModule.aspx?mid="+firstModule.ModuleID;
                        LiteralHelpLeft.Text = "You have selected your activities.  Before continuing to Review your Practice, you must select all your charts for all activities.";
                        if (cycle.CycleNumber == 2)
                            HTMLlinkAbstractSecondCharts.HRef = "OpenModule.aspx?mid=" + firstModule.ModuleID;
                        //<%# Eval("ModuleName")%>

                    }
                    else
                    {
                        var firstModule = (from cr in pim.ChartRegistration
                                           where cr.ParticipantModuleCycleID == cycle.ParticipantModuleCycleID
                                           select cr).First();

                        Session[Constants.SESSION_WORKINGMODULEID] = firstModule.ModuleID; 

                        HyperLinkNextPage.NavigateUrl = "Dashboard.aspx";
                        //LiteralHelpLeft.Text = "You have selected your activities and charts for this activity.  Please continue to Review your Practice and Abstract your charts.";
                        LiteralHelpLeft.Text = "You are currently in the Activity Selection and Self-Assessment phase.  For your selected activities, here is your status:";
                        LiteralHelpLeft2.Visible = true;
                        //<%# Eval("ModuleName")%>

                        var moduleStatus = (from cr in pim.ParticipantModuleStatusPerParticipant_V
                                           where cr.ParticipantModuleCycleID == cycle.ParticipantModuleCycleID
                                           select cr).First();
                        // LiteralHelpLeft2.Text = "improvementplancomplete = " + moduleStatus.ImprovementPlanComplete.ToString() + " systemsurvey =   " + moduleStatus.SystemSurveyComplete.ToString() + "   moduleStatus.SelfAssessmentSurveyComplete " + moduleStatus.SelfAssessmentSurveyComplete.ToString() + "  moduleStatus.AbstractDataComplete = " + moduleStatus.AbstractDataComplete.ToString();
                        if ((moduleStatus.ImprovementPlanComplete == 0) || (cycle.CycleNumber==2))
                        {
                            // Phase I should NOT be selected
                            divModuleSelection.Attributes.Remove("class");
                            divModuleSelection.Attributes.Add("class", "col blue");
                            blue_bottom.Attributes["src"] = ResolveUrl("../common/images/per.blue_bottom_curve.jpg");
                            divPhase1.Visible = false;
                            // Phase III should be selected
                            divAnalysis.Attributes.Add("class", "col pink_selected");
                            pink_bottom.Attributes["src"] = ResolveUrl("../common/images/per.pink_bottom_curve_selected.jpg");
                            divPhase3.Visible = true;
                        }
                        else
                            //if (moduleStatus.ImprovementPlanComplete > 0)
                            if ((moduleStatus.SystemSurveyComplete == 0) && (moduleStatus.SelfAssessmentSurveyComplete == 0) && (moduleStatus.AbstractDataComplete == 0))
                            {
                                // Phase I should NOT be selected
                                divModuleSelection.Attributes.Remove("class");
                                divModuleSelection.Attributes.Add("class", "col blue");
                                blue_bottom.Attributes["src"] = ResolveUrl("../common/images/per.blue_bottom_curve.jpg");
                                divPhase1.Visible = false;
                                // Phase II should be selected
                                divPlanning.Attributes.Add("class", "col orange_selected");
                                orange_bottom.Attributes["src"] = ResolveUrl("../common/images/per.orange_bottom_curve_selected.jpg");
                                divPhase2.Visible = true;
                            }
                            else
                            {
                                // Phase I should be selected   
                            }

                    }

                }
            }
            else
            {
                if (cycle.CycleNumber == 1)
                {
                    HyperLinkNextPage.NavigateUrl = "PQRSSelection.aspx";
                    LiteralHelpLeft.Text = "Before entering the Improvement in Medical Practice Process, please select 1-3 activities. (<a href='pqrsselection.aspx'>Click here</a> to select)";
                    LiteralInitialMessage.Text = "Welcome to the Main Page of the ABO Part IV Improvement in Medical Practice Platform (PIP).  This page displays your current status and the steps needed to complete the process.";
                    divModuleSelection.Attributes.Add("class", "col blue");
                    divPhase1.Visible = true;
                    LiteralStatus.Text = "Pending";
                }
            }
            if (cycle.CycleNumber == 2)
            {
                divModuleSelection.Attributes.Remove("class");
                divModuleSelection.Attributes.Add("class", "col blue");
                blue_bottom.Attributes["src"] = ResolveUrl("../common/images/per.blue_bottom_curve.jpg");
                divPhase1.Visible = false;
                // Phase III should be selected
                divAnalysis.Attributes.Add("class", "col pink_selected");
                pink_bottom.Attributes["src"] = ResolveUrl("../common/images/per.pink_bottom_curve_selected.jpg");
                divPhase3.Visible = true;
                LiteralStatus.Text = "Active";
                HTMLlinkModuleSelection.HRef = "#";
                LiteralHelpLeft.Text = "You have completed your improvement plan. Please select all your charts for all activities.";
                LiteralHelpLeft2.Text = "Once you have completed all activities, you will be able to review your Performance Report and Impact Statement";
                
                var firstModule = (from cr in pim.ChartRegistration
                                   where cr.ParticipantModuleCycleID == cycle.ParticipantModuleCycleID
                                   select cr).First(); 
                Session[Constants.SESSION_WORKINGMODULEID] = firstModule.ModuleID;
                HTMLlinkViewPerformaceReport.HRef = "PerformanceReport.aspx?cycle=1";
                HTMLlinkViewPerformaceReport.InnerText = "View Performance Report";
                HTMLlinkViewImprovementPlan.HRef = "ImprovementPlanPartI.aspx";
                HTMLlinkViewImprovementPlan.InnerText = "View Improvement Plan";
                HTMLlinkViewPhase1Charts.HRef = "PreviousChartsStatus.aspx";
                if (cycle.ImpactAssessmentComplete)
                {
                    LiteralHelpLeft.Text = "PROCESS COMPLETED";
                    LiteralHelpLeft2.Visible = false;
                    HTMLlinkComparePerformance.HRef = "PerformanceReport.aspx?cycle=2&action=view";
                    HTMLlinkImpactStatement.HRef = "ImpactStatement.aspx";
                }

                //var firstModule = (from cr in pim.ChartRegistration
                //                   where cr.ParticipantModuleCycleID == cycle.ParticipantModuleCycleID
                //                   & cr.Completed == false
                //                   select cr).First();
                //HyperLinkNextPage.NavigateUrl = "OpenModule.aspx?mid=" + firstModule.ModuleID;
                //LiteralHelpLeft.Text = "Abstract Second Sample of Charts.";

            }
        }
    }
}
