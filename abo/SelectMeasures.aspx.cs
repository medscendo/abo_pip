﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using NetHealthPIMModel;
using System.Web.UI.DataVisualization.Charting;
using System.Linq.Expressions;
using System.Transactions;
using System.Web.UI.HtmlControls;

public partial class copd_SelectMeasures : BasePage
{

    List<RationaleType> rationale;
    static int MeasureGroupID = 0;
    static int PreviousMeasureGroupID = -1;
    static string ModuleNameProcessMeasures = "";
    static string ModuleNameOutcomeMeasures = "";

    protected void Page_Load(object sender, EventArgs e)
    {
        if (!IsPostBack)
        {

            InitializePage();
            int ModuleID = 0; 
            LabelImprovementMeasures.Text = String.Format("Select between {0} and {1} areas of your performance that you would like to improve.",
            module.MinImprovementMeasures, module.MaxImprovementMeasures);
            HiddenMinImprovementMeasures.Value = module.MinImprovementMeasures.ToString();
            HiddenMaxImprovementMeasures.Value = "20";/// module.MaxImprovementMeasures.ToString();
            ButtonDevelopImprovementPlan.PostBackUrl = Request.AppRelativeCurrentExecutionFilePath + "?" + Constants.QUERYSTRING_ACTION + "=" + Constants.QUERYSTRING_ACTION_SAVE;

            // TODO: Pending grouping of measures
            // TODO: Query to determine if this is first or second phase. 
            using (NetHealthPIMEntities pim = new NetHealthPIMEntities())
            {

                ParticipantModuleCycle cycle = pim.ParticipantModuleCycle.First(c => c.ParticipantModuleCycleID == ctx.ActiveModuleCycleID);
                        var report = from pir in pim.ParticipantInitialReport_V
                                     join m in pim.Module on pir.ModuleID equals m.ModuleID
                                     where pir.ParticipantModuleCycleID == ctx.ActiveModuleCycleID
                                        && pir.MeasureTypeID == 3
                                        && pir.MeasureAggregateTypeID == 1
                                        && pir.ParticipantMeasureTypeID == 1
                                     orderby m.ModuleID, pir.MeasureGroupSortOrder
                                     select new PerformanceReportData
                                     {
                                         ModuleName = m.ModuleName,
                                         MeasureID = pir.MeasureID,
                                         IsnumericMeasure = pir.IsNumericMeasure,
                                         MeasureGroupSortOrder = pir.MeasureGroupSortOrder == null ? 0 : (int)pir.MeasureGroupSortOrder,
                                         MeasureQualityIndicator = pir.MeasureQualityIndicator,
                                         MeasureTitle = pir.MeasureTitle,
                                         VisualAcuityResponse=pir.VisualAcuityResponse,
                                         NumericMeasureName = pir.NumericMeasureName,
                                         NumericFullGraph = pir.NumericFullGraph,
                                         MeasureClinicalRecommendation = pir.MeasureClinicalRecommendation,
                                         MeasureLongDescription = pir.MeasureLongDescription,
                                         MeasureComputationMethod = pir.MeasureComputationMethod,
                                         AbstractRecordsIncluded = pir.AbstractRecordsIncluded == null ? 0 : (int)pir.AbstractRecordsIncluded,
                                         MeasurePercentCurrent = pir.MeasurePercentCurrent,
                                         PeerAbstractRecordsIncluded = pir.PeerAbstractRecordsIncluded,
                                         Peer25Percentile = pir.Peer25Percentile,
                                         Peer75Percentile = pir.Peer75Percentile,
                                         PeerData = pir.PeerData,
                                         MeasureRelated=pir.MeasureRelated,
                                         SelfData = pir.SelfData,
                                         IncludeInImprovementPlan = pir.IncludeInImprovementPlan,
                                         MayNeedImprovement = pir.MayNeedImprovement
                                     };
                        ListViewProcessMeasures.DataSource = report;

                        var reportOutcomeMeasures = from pir in pim.ParticipantInitialReport_OutcomeMeasures_V
                                                    join m in pim.Module on pir.ModuleID equals m.ModuleID
                                                    where pir.ParticipantModuleCycleID == ctx.ActiveModuleCycleID
                                                    && (pir.ParticipantMeasureTypeID == 3
                                                    || pir.ParticipantMeasureTypeID == 1 
                                                    || pir.ParticipantMeasureTypeID == 5)

                                                    orderby m.ModuleID, pir.MeasureGroupSortOrder
                                                    select new PerformanceReportData
                                                    {
                                                        ModuleName = m.ModuleName,
                                                        MeasureID = pir.MeasureID,
                                                        IsnumericMeasure = pir.IsNumericMeasure,
                                                        MeasureQualityIndicator = pir.MeasureQualityIndicator,
                                                        MeasureTitle = pir.MeasureTitle,
                                                        MeasureClinicalRecommendation = pir.MeasureClinicalRecommendation,
                                                        MeasureLongDescription = pir.MeasureLongDescription,
                                                        MeasureComputationMethod = pir.MeasureComputationMethod,
                                                        AbstractRecordsIncluded = pir.AbstractRecordsIncluded == null ? 0 : (int)pir.AbstractRecordsIncluded,
                                                        MeasurePercent3To6 = pir.MeasurePercent3To6,
                                                        PeerData3To6 = pir.PeerData3To6,
                                                        VisualAcuityResponse=pir.VisualAcuityResponse,
                                                        NumericMeasureName = pir.NumericMeasureName,
                                                        NumericFullGraph = pir.NumericFullGraph,
                                                        MeasurePercent7To12 = pir.MeasurePercent7To12,
                                                        PeerData7To12 = pir.PeerData7To12,
                                                        MeasurePercentNewOnset = pir.MeasurePercentNewOnset,
                                                        PeerDataNewOnset = pir.PeerDataNewOnset,
                                                        MeasurePercentRecurrence = pir.MeasurePercentRecurrence,
                                                        PeerDataRecurrence = pir.PeerDataRecurrence,
                                                        SelfData = pir.SelfData,
                                                        MeasureRelated=pir.MeasureRelated,
                                                        PeerAbstractRecordsIncluded = pir.PeerAbstractRecordsIncluded,
                                                        Peer25Percentile = pir.Peer25Percentile,
                                                        Peer75Percentile = pir.Peer75Percentile,
                                                        IncludeInImprovementPlan = pir.IncludeInImprovementPlan,
                                                        MayNeedImprovement = pir.MayNeedImprovement
                                                    };

                        ListViewOutcomeMeasures.DataSource = reportOutcomeMeasures;

                        var reportPEC = from pir in pim.ParticipantInitialReport_PECMeasures_V
                                        where pir.ParticipantModuleCycleID == ctx.ActiveModuleCycleID
                                        orderby ModuleID, pir.MeasureGroupSortOrder
                                        select new PerformanceReportData
                                        {
                                            ModuleName = pir.ModuleName,
                                            MeasureID = pir.MeasureID,
                                            MeasureGroupSortOrder = pir.MeasureGroupSortOrder == null ? 0 : (int)pir.MeasureGroupSortOrder,
                                            MeasureQualityIndicator = pir.MeasureQualityIndicator,
                                            MeasureTitle = pir.MeasureTitle,
                                            MeasureClinicalRecommendation = pir.MeasureClinicalRecommendation,
                                            MeasureLongDescription = pir.MeasureLongDescription,
                                            MeasureComputationMethod = pir.MeasureComputationMethod,
                                            AbstractRecordsIncluded = pir.AbstractRecordsIncluded == null ? 0 : (int)pir.AbstractRecordsIncluded,
                                            MeasurePercentCurrent = pir.MeasurePercentCurrent,
                                            PeerData = pir.PeerData,
                                            SelfData = pir.SelfData,
                                            IncludeInImprovementPlan = pir.IncludeInImprovementPlan == 1 ? true : false,
                                            MayNeedImprovement = pir.MayNeedImprovement
                                        };

                        ListViewPECMeasures.DataSource = reportPEC;
                        DataBind();

                }
   
                if (ListViewPECMeasures.Items.Count == 0)
                {
                    PECMeasuresTable.Visible = false;
                }
                var relatedmes = RelatedMeasuresChart(ctx.ActiveModuleCycleID, false);
                if (relatedmes.Count == 0)
                {
                    PanelCombinedmes.Visible = false;
                }
                else
                {
                    string ModuleName = "";

                    foreach (var item in relatedmes)
                    {
                        var ModuleData = item.Key.Split('@');
                        if (ModuleData[2] != ModuleName)
                        {
                            ModuleName = item.Key.Split('@')[2];
                            HtmlTableRow rowname = new HtmlTableRow();
                            rowname.Attributes.Add("class", "module-name");
                            HtmlTableCell cell1name = new HtmlTableCell();
                            cell1name.Controls.Add(new LiteralControl("<br /><div>" + ModuleName + "</div>"));
                            rowname.Controls.Add(cell1name);
                            HtmlTableCell cell2name = new HtmlTableCell();
                            rowname.Controls.Add(cell2name);
                            HtmlTableCell cell3name = new HtmlTableCell();
                            rowname.Controls.Add(cell3name);
                            StrataTable.Controls.Add(rowname);

                            string ConsideredTable = ModuleData[3];
                            HtmlTableRow row = new HtmlTableRow();
                            HtmlTableCell cell1 = new HtmlTableCell();
                            cell1.Controls.Add(new LiteralControl("<input type='checkbox' onclick='CheckGivenRel(this, " + ModuleData[0] + ")'><div>" + ModuleName + "</div>"));
                            row.Controls.Add(cell1);
                            HtmlTableCell cell2 = new HtmlTableCell();
                            cell2.Controls.Add(new LiteralControl("<div>" + ConsideredTable + "</div>"));
                            row.Controls.Add(cell2);
                            HtmlTableCell cell3 = new HtmlTableCell();
                            cell3.Controls.Add(item.Value);
                            row.Controls.Add(cell3);
                            StrataTable.Controls.Add(row);
                        }
                        else
                        {
                            string ConsideredTable = ModuleData[3];
                            HtmlTableRow row = new HtmlTableRow();
                            HtmlTableCell cell1 = new HtmlTableCell();
                            cell1.Controls.Add(new LiteralControl("<input type='checkbox' onclick='CheckGivenRel(this, " + ModuleData[0] + ")'><div>" + ModuleName + "</div>"));
                            row.Controls.Add(cell1);
                            HtmlTableCell cell2 = new HtmlTableCell();
                            cell2.Controls.Add(new LiteralControl("<div>" + ConsideredTable + "</div>"));
                            row.Controls.Add(cell2);
                            HtmlTableCell cell3 = new HtmlTableCell();
                            cell3.Controls.Add(item.Value);
                            row.Controls.Add(cell3);
                            StrataTable.Controls.Add(row);
                        }
                    }
                }
        }
        else
        {
            int MeasureGroupCheckedID = 0;
            int PreviousMeasureGroupCheckedID = 0;
            string action = Request.Params[Constants.QUERYSTRING_ACTION];
            PanelCombinedmes.Visible = false;
            if (action == null || action != Constants.QUERYSTRING_ACTION_SAVE)
            {
                PanelRationale.Visible = true;
                PanelInformationPhase1.Visible = false;
                ButtonSelectRationale.Visible = false;
                LinkButtonNext.Visible = false;
                ButtonDevelopImprovementPlan.Visible = true;
                LinkButtonBack.Visible = true;
                List<int> selectedMeasures = new List<int>();

                for (int i = 0; i < ListViewProcessMeasures.Items.Count; i++)
                {
                    CheckBox box = (CheckBox)ListViewProcessMeasures.Items[i].FindControl("CheckBoxIncludeInImprovementPlan");
                    HiddenField measure = (HiddenField)ListViewProcessMeasures.Items[i].FindControl("MeasureID");
                    HiddenField measureGroup = (HiddenField)ListViewProcessMeasures.Items[i].FindControl("MeasureGroupID");
                    if (box.Checked)
                    {
                        selectedMeasures.Add(Int32.Parse(measure.Value));
                    }
                    box.Visible = false;
                    PreviousMeasureGroupCheckedID = MeasureGroupCheckedID;
                }
                for (int i = 0; i < ListViewOutcomeMeasures.Items.Count; i++)
                {
                    CheckBox box = (CheckBox)ListViewOutcomeMeasures.Items[i].FindControl("CheckBoxIncludeInImprovementPlan");
                    HiddenField measure = (HiddenField)ListViewOutcomeMeasures.Items[i].FindControl("MeasureID");
                    HiddenField measureGroup = (HiddenField)ListViewOutcomeMeasures.Items[i].FindControl("MeasureGroupID");
                    if (box.Checked)
                    {
                        selectedMeasures.Add(Int32.Parse(measure.Value));
                    }
                    box.Visible = false;
                    PreviousMeasureGroupCheckedID = MeasureGroupCheckedID;
                }
                for (int i = 0; i < ListViewPECMeasures.Items.Count; i++)
                {
                    CheckBox box = (CheckBox)ListViewPECMeasures.Items[i].FindControl("CheckBoxIncludeInImprovementPlan");
                    HiddenField measure = (HiddenField)ListViewPECMeasures.Items[i].FindControl("MeasureID");
                    HiddenField measureGroup = (HiddenField)ListViewPECMeasures.Items[i].FindControl("MeasureGroupID");
                    if (box.Checked)
                    {
                        selectedMeasures.Add(Int32.Parse(measure.Value));
                    }
                    box.Visible = false;
                    PreviousMeasureGroupCheckedID = MeasureGroupCheckedID;
                }

                if (ListViewPECMeasures.Items.Count == 0)
                {
                    PECMeasuresTable.Visible = false;
                }

                int GroupID = ctx.ParticipantGroupID;

                ViewState["selectedMeasures"] = selectedMeasures;

                using (NetHealthPIMEntities pim = new NetHealthPIMEntities())
                {
                    ParticipantModuleCycle cycle = pim.ParticipantModuleCycle.First(c => c.ParticipantModuleCycleID == ctx.ActiveModuleCycleID);

                            var report = from pir in pim.ParticipantInitialReport_V
                                         join m in pim.Module on pir.ModuleID equals m.ModuleID
                                         where pir.ParticipantModuleCycleID == ctx.ActiveModuleCycleID
                                            && pir.MeasureTypeID == 3
                                            && pir.MeasureAggregateTypeID == 1
                                            && pir.ParticipantMeasureTypeID == 1
                                         orderby m.ModuleID, pir.MeasureGroupSortOrder
                                         select new PerformanceReportData
                                         {
                                             ModuleName = m.ModuleName,
                                             MeasureID = pir.MeasureID,
                                             IsnumericMeasure = pir.IsNumericMeasure,
                                             VisualAcuityResponse=pir.VisualAcuityResponse,
                                             MeasureGroupSortOrder = pir.MeasureGroupSortOrder == null ? 0 : (int)pir.MeasureGroupSortOrder,
                                             MeasureQualityIndicator = pir.MeasureQualityIndicator,
                                             MeasureTitle = pir.MeasureTitle,
                                             MeasureClinicalRecommendation = pir.MeasureClinicalRecommendation,
                                             MeasureLongDescription = pir.MeasureLongDescription,
                                             NumericMeasureName = pir.NumericMeasureName,
                                             NumericFullGraph = pir.NumericFullGraph,
                                             MeasureComputationMethod = pir.MeasureComputationMethod,
                                             AbstractRecordsIncluded = pir.AbstractRecordsIncluded == null ? 0 : (int)pir.AbstractRecordsIncluded,
                                             MeasurePercentCurrent = pir.MeasurePercentCurrent,
                                             PeerAbstractRecordsIncluded = pir.PeerAbstractRecordsIncluded,
                                             Peer25Percentile = pir.Peer25Percentile,
                                             Peer75Percentile = pir.Peer75Percentile,
                                             PeerData = pir.PeerData,
                                             SelfData = pir.SelfData,
                                             IncludeInImprovementPlan = pir.IncludeInImprovementPlan,
                                             MayNeedImprovement = pir.MayNeedImprovement
                                         };

                           
                            var filteredReports = report.WhereIn(r => r.MeasureID, selectedMeasures);
                            ListViewProcessMeasures.DataSource = filteredReports;
                            GridViewRationale.DataSource = filteredReports;

                            var reportOutcomeMeasures = from pir in pim.ParticipantInitialReport_OutcomeMeasures_V
                                                        join m in pim.Module on pir.ModuleID equals m.ModuleID
                                                        where pir.ParticipantModuleCycleID == ctx.ActiveModuleCycleID
                                                        && (pir.ParticipantMeasureTypeID == 3
                                                        || pir.ParticipantMeasureTypeID == 1
                                                        || pir.ParticipantMeasureTypeID == 5)
                                                        orderby m.ModuleID, pir.MeasureGroupSortOrder
                                                        select new PerformanceReportData
                                                        {
                                                            ModuleName = m.ModuleName,
                                                            MeasureID = pir.MeasureID,
                                                            IsnumericMeasure = pir.IsNumericMeasure,
                                                            MeasureQualityIndicator = pir.MeasureQualityIndicator,
                                                            MeasureTitle = pir.MeasureTitle,
                                                            MeasureClinicalRecommendation = pir.MeasureClinicalRecommendation,
                                                            MeasureLongDescription = pir.MeasureLongDescription,
                                                            MeasureComputationMethod = pir.MeasureComputationMethod,
                                                            AbstractRecordsIncluded = pir.AbstractRecordsIncluded == null ? 0 : (int)pir.AbstractRecordsIncluded,
                                                            MeasurePercent3To6 = pir.MeasurePercent3To6,
                                                            PeerData3To6 = pir.PeerData3To6,
                                                            VisualAcuityResponse=pir.VisualAcuityResponse,
                                                            NumericMeasureName=pir.NumericMeasureName,
                                                            NumericFullGraph=pir.NumericFullGraph,
                                                            MeasurePercent7To12 = pir.MeasurePercent7To12,
                                                            PeerData7To12 = pir.PeerData7To12,
                                                            MeasurePercentNewOnset = pir.MeasurePercentNewOnset,
                                                            PeerDataNewOnset = pir.PeerDataNewOnset,
                                                            MeasurePercentRecurrence = pir.MeasurePercentRecurrence,
                                                            PeerDataRecurrence = pir.PeerDataRecurrence,
                                                            SelfData = pir.SelfData,
                                                            PeerAbstractRecordsIncluded = pir.PeerAbstractRecordsIncluded,
                                                            Peer25Percentile = pir.Peer25Percentile,
                                                            Peer75Percentile = pir.Peer75Percentile,
                                                            IncludeInImprovementPlan = pir.IncludeInImprovementPlan,
                                                            MayNeedImprovement = pir.MayNeedImprovement
                                                        };
                            var filteredOutcomeMeasures = reportOutcomeMeasures.WhereIn(r => r.MeasureID, selectedMeasures);
                            ListViewOutcomeMeasures.DataSource = filteredOutcomeMeasures;

                            var reportPEC = from pir in pim.ParticipantInitialReport_PECMeasures_V
                                            where pir.ParticipantModuleCycleID == ctx.ActiveModuleCycleID
                                            orderby pir.MeasureGroupSortOrder
                                            select new PerformanceReportData
                                            {
                                                ModuleName = pir.ModuleName,
                                                MeasureID = pir.MeasureID,
                                                MeasureGroupSortOrder = pir.MeasureGroupSortOrder == null ? 0 : (int)pir.MeasureGroupSortOrder,
                                                MeasureQualityIndicator = pir.MeasureQualityIndicator,
                                                MeasureTitle = pir.MeasureTitle,
                                                MeasureClinicalRecommendation = pir.MeasureClinicalRecommendation,
                                                MeasureLongDescription = pir.MeasureLongDescription,
                                                MeasureComputationMethod = pir.MeasureComputationMethod,
                                                AbstractRecordsIncluded = pir.AbstractRecordsIncluded == null ? 0 : (int)pir.AbstractRecordsIncluded,
                                                MeasurePercentCurrent = pir.MeasurePercentCurrent,
                                                PeerData = pir.PeerData,
                                                SelfData = pir.SelfData,
                                                IncludeInImprovementPlan = pir.IncludeInImprovementPlan == 1 ? true : false,
                                                MayNeedImprovement = pir.MayNeedImprovement
                                            };
                            var filteredPECMeasures = reportPEC.WhereIn(r => r.MeasureID, selectedMeasures);
                            ListViewPECMeasures.DataSource = filteredPECMeasures;

                            var reportRationale = from m in pim.Measure
                                                  select new
                                                  {
                                                      MeasureID = m.MeasureID,
                                                      MeasureTitle = m.MeasureTitle
                                                  };
                            var filteredRationale = reportRationale.WhereIn(r => r.MeasureID, selectedMeasures);
                            GridViewRationale.DataSource = filteredRationale;
                            rationale = pim.RationaleType.ToList<RationaleType>();

                            DataBind();

                    for (int i = 0; i < ListViewProcessMeasures.Items.Count; i++)
                    {
                        CheckBox box = (CheckBox)ListViewProcessMeasures.Items[i].FindControl("CheckBoxIncludeInImprovementPlan");
                        box.Visible = false;
                    }
                    for (int i = 0; i < ListViewOutcomeMeasures.Items.Count; i++)
                    {
                        CheckBox box = (CheckBox)ListViewOutcomeMeasures.Items[i].FindControl("CheckBoxIncludeInImprovementPlan");
                        box.Visible = false;
                    }
                    for (int i = 0; i < ListViewPECMeasures.Items.Count; i++)
                    {
                        CheckBox box = (CheckBox)ListViewPECMeasures.Items[i].FindControl("CheckBoxIncludeInImprovementPlan");
                        box.Visible = false;
                    }
                    if (ListViewPECMeasures.Items.Count == 0)
                        PECMeasuresTable.Visible = false;
                }
            }
        }
        if (((ListViewProcessMeasures.Items.Count + ListViewOutcomeMeasures.Items.Count) < module.MinImprovementMeasures) && ((ListViewProcessMeasures.Items.Count + ListViewOutcomeMeasures.Items.Count) > 0))
        {
            HiddenMinImprovementMeasures.Value = (ListViewProcessMeasures.Items.Count + ListViewOutcomeMeasures.Items.Count).ToString();
            LabelImprovementMeasures.Text = String.Format("Select between {0} and {1} areas of your performance that you would like to improve.",
                (ListViewProcessMeasures.Items.Count + ListViewOutcomeMeasures.Items.Count), module.MaxImprovementMeasures);
        }
    }








    protected void InitializePage()
    {
        string breadCrumb = Constants.BC_DASHBOARD_LINK + "" + Constants.BC_PERFORMANCE_REPORT_LINK + "" + Constants.BC_SELECT_MEASURES;
        ((PIMMasterPage)Page.Master).SetTitle(breadCrumb);
        ((PIMMasterPage)Page.Master).SetStatus(2);
        ((PIMMasterPage)Page.Master).SetHeader("Select Measures for Improvement");

    }



    protected void ListViewProcessMeasures_OnItemDataBound(object sender, ListViewItemEventArgs e)
    {
        if (e.Item.ItemType == ListViewItemType.DataItem)
        {
            Chart ch = (Chart)e.Item.FindControl("ChartPerformance");
            ch.Series[0].LabelBackColor = System.Drawing.Color.White;
            Label LabelChartConsideredPR = (Label)e.Item.FindControl("LabelChartConsideredPR");
            HtmlTableRow TR = (HtmlTableRow)e.Item.FindControl("divRowChart");
            PerformanceReportData data = (PerformanceReportData)((ListViewDataItem)e.Item).DataItem;
            CheckBox box = (CheckBox)e.Item.FindControl("CheckBoxIncludeInImprovementPlan");
            if (data.MeasureRelated != null)
            {
                box.Attributes.Add("DataVal", "some" + data.MeasureRelated);
                TR.Attributes.Add("style", "display:none");
            }

            LabelChartConsideredPR.Text = @"<table style='border:none'>
                                           <tr  style=' border-bottom: thick dotted #ff0000; border-bottom-width: 3px;' >
                                              <td  style='border:none' >Your </td>
                                              <td style='border:none'>" + data.AbstractRecordsIncluded.ToString() + @"</td>
                                           </tr>";

            LabelChartConsideredPR.Text += @"<tr>
                                                 <td  style='border:none' >Peer </td>
                                                 <td  style='border:none' >" + data.PeerAbstractRecordsIncluded.ToString() + @"</td>
                                             </tr></table>";
            HtmlTableRow divRowModuleName = (HtmlTableRow)e.Item.FindControl("divRowModuleName");
            if (ModuleNameProcessMeasures != data.ModuleName)
            {
                divRowModuleName.Visible = true;
                ModuleNameProcessMeasures = data.ModuleName;
            }

            if (data.IsnumericMeasure == null || data.IsnumericMeasure == false)
            {
             
                HtmlTableRow divRowChart = (HtmlTableRow)e.Item.FindControl("divRowChart");
                if (divRowChart is System.Web.UI.HtmlControls.HtmlTableRow)
                {
                    if ((data.MeasurePercentCurrent < 50) && (data.MayNeedImprovement > 0))
                        divRowChart.Attributes.Add("class", "red-row");
                }
                ch.Series[0].Points.AddY(data.PeerData);
                ch.Series[0].Points.AddY(data.MeasurePercentCurrent);
                if (data.SelfData > 0)
                {
                    ch.Series[0].Points.AddY(data.SelfData);
                    ch.ChartAreas[0].AxisX.CustomLabels.Add(2.5, 3.5, "Self Assess.");
                    ch.ChartAreas[0].AxisX.Maximum = 4.0;
                    ch.Height = 80;
                }
                if (data.IsPhase2Flag == true)
                    ch.ChartAreas[0].AxisX.CustomLabels.Add(1.5, 2.5, "Final Data");
                else
                {
                    ch.ChartAreas[0].AxisX.CustomLabels.Add(1.5, 2.5, "Your Data");

                }
                ch.ChartAreas[0].AxisX.CustomLabels.Add(0.5, 1.5, "Peer Data");
            }
            else
            {
                ch.ChartAreas[0].AxisY.Maximum = 200;
              
           
                ch.ChartAreas[0].AxisX.CustomLabels.Add(0.5, 1.5, "Peer " + (data.NumericMeasureName == null ? "Data" : data.NumericMeasureName));
                ch.Series[0].Points.AddY(Math.Round((double)data.PeerData, 0));
                ch.ChartAreas[0].AxisX.CustomLabels.Add(1.5, 2.5, "Your " + (data.NumericMeasureName == null ? "Data" : data.NumericMeasureName));
                ch.Series[0].Points.AddY(Math.Round((double)(data.MeasurePercentCurrent), 0));
                if (data.VisualAcuityResponse != null)
                {
                    if (data.VisualAcuityResponse[0].ToString() == "2")
                    {
                        var NumericValue = Convert.ToDouble(data.VisualAcuityResponse.Split('/')[1]);
                        ch.ChartAreas[0].AxisX.CustomLabels.Add(2.5, 3.5, "VA Data");
                        ch.Series[0].Points.AddY(Math.Round(NumericValue, 0));
                    }

                }
            }
        }

        PreviousMeasureGroupID = MeasureGroupID;
    }





    protected void ListViewOutcomeMeasures_OnItemDataBound(object sender, ListViewItemEventArgs e)
    {
        if (e.Item.ItemType == ListViewItemType.DataItem)
        {
            Chart ch = (Chart)e.Item.FindControl("ChartPerformance");
            ch.Series[0].LabelBackColor = System.Drawing.Color.White;
            Label LabelChartConsideredUM = (Label)e.Item.FindControl("LabelChartConsideredUM");
            HtmlTableRow TR = (HtmlTableRow)e.Item.FindControl("divRowChart");
            PerformanceReportData data = (PerformanceReportData)((ListViewDataItem)e.Item).DataItem;
            CheckBox box = (CheckBox)e.Item.FindControl("CheckBoxIncludeInImprovementPlan");
            if (data.MeasureRelated != null)
            {
                box.Attributes.Add("DataVal", "some" + data.MeasureRelated);
                TR.Attributes.Add("style", "display:none");
            }

            
            LabelChartConsideredUM.Text = @"<table  style='border:none'>
                                                <tr style=' border-bottom: thick dotted #ff0000; border-bottom-width: 3px;' >
                                                  <td  style='border:none' >Your </td>
                                                  <td  style='border:none'>" + data.AbstractRecordsIncluded.ToString() + @"</td>
                                               </tr>";

            LabelChartConsideredUM.Text += @"<tr  style='border:none' >
                                                <td  style='border:none' >Peer </td>
                                                <td  style='border:none'>" + data.PeerAbstractRecordsIncluded.ToString() + @"</td>
                                             </tr></table>";
            System.Web.UI.HtmlControls.HtmlTableRow divRowModuleName = (System.Web.UI.HtmlControls.HtmlTableRow)e.Item.FindControl("divRowModuleName");
            if (ModuleNameOutcomeMeasures != data.ModuleName)
            {
                divRowModuleName.Visible = true;
                ModuleNameOutcomeMeasures = data.ModuleName;
            }
            if (data.IsnumericMeasure==null || data.IsnumericMeasure==false)
            {
               
                System.Web.UI.HtmlControls.HtmlTableRow divRowChart = (System.Web.UI.HtmlControls.HtmlTableRow)e.Item.FindControl("divRowChart");
                if (divRowChart is System.Web.UI.HtmlControls.HtmlTableRow)
                {
                    switch (data.MeasureQualityIndicator)
                    {
                        case "Complications":
                            if (((data.MeasurePercent3To6 < 50) && (data.MayNeedImprovement > 0)) 
                                || ((data.MeasurePercent7To12 < 50) && (data.MayNeedImprovement > 0)) || ((data.MeasurePercentRecurrence < 50) && (data.MayNeedImprovement > 0)) || ((data.MeasurePercentNewOnset < 50) && (data.MayNeedImprovement > 0)))
                                divRowChart.Attributes.Add("class", "red-row");
                            break;
                        case "Visual Acuity Improvement":
                            if (((data.MeasurePercent3To6 < 50) && (data.MayNeedImprovement > 0)) || ((data.MeasurePercent7To12 < 50) && (data.MayNeedImprovement > 0)))
                                divRowChart.Attributes.Add("class", "red-row");
                            break;
                        case "Continued Treatment":
                            if (((data.MeasurePercent3To6 < 50) && (data.MayNeedImprovement > 0)) || ((data.MeasurePercent7To12 < 50) && (data.MayNeedImprovement > 0)))
                                divRowChart.Attributes.Add("class", "red-row");
                            break;
                        case "Mean Improvement":
                            divRowChart.Attributes.Add("class", "initial_inner_row");
                            ch.ChartAreas[0].AxisY.Maximum = 50.00;
                            ch.ChartAreas[0].AxisY.Interval = 5;
                            ch.Height = 115;
                            break;
                        default:
                            if ((data.MeasurePercent3To6 < 50) && (data.MayNeedImprovement > 0))
                                divRowChart.Attributes.Add("class", "red-row");
                            break;

                    }
                }
                if (data.PeerData7To12 == -1)
                {
                    ch.Series[0].Points.AddY(data.PeerData3To6);
                    ch.Series[0].Points.AddY(data.MeasurePercent3To6);
                    ch.ChartAreas[0].AxisX.Maximum = 3.0;
                    ch.Height = 70;
                    if (data.SelfData > 0)
                    {
                        ch.Series[0].Points.AddY(data.SelfData);
                        ch.ChartAreas[0].AxisX.CustomLabels.Add(2.5, 3.5, "Self Assess.");
                        ch.ChartAreas[0].AxisX.Maximum = 4.0;
                        ch.Height = 90;
                    }
                    ch.ChartAreas[0].AxisX.CustomLabels.Add(0.5, 1.5, "Peer Data");
                    if (data.IsPhase2Flag == true)
                        ch.ChartAreas[0].AxisX.CustomLabels.Add(1.5, 2.5, "Final Data");
                    else
                        ch.ChartAreas[0].AxisX.CustomLabels.Add(1.5, 2.5, "Your Data");
                }
                else
                {
                    ch.Series[0].Points.AddY(data.PeerData7To12);
                    ch.Series[0].Points.AddY(data.MeasurePercent7To12);
                    ch.Series[0].Points.AddY(data.PeerData3To6);
                    ch.Series[0].Points.AddY(data.MeasurePercent3To6);

                    if (data.MeasurePercentNewOnset != null) 
                    {
                        ch.ChartAreas[0].AxisX.CustomLabels.Add(0.5, 1.5, "Peer Reverse");
                        ch.ChartAreas[0].AxisX.CustomLabels.Add(0.5, 1.5, "My Reverse");
                        ch.ChartAreas[0].AxisX.CustomLabels.Add(2.5, 3.5, "Peer Atropine");
                        ch.ChartAreas[0].AxisX.CustomLabels.Add(1.5, 2.5, "My Atropine");
                        ch.Series[0].Points.AddY(data.PeerDataNewOnset);
                        ch.ChartAreas[0].AxisX.CustomLabels.Add(4.5, 5.5, "Peer NewOnset");
                        ch.Series[0].Points.AddY(data.MeasurePercentNewOnset);
                        ch.ChartAreas[0].AxisX.CustomLabels.Add(2.5, 3.5, "My NewOnset");
                        ch.Series[0].Points.AddY(data.PeerDataRecurrence);
                        ch.ChartAreas[0].AxisX.CustomLabels.Add(6.5, 7.5, "Peer Recurrence");
                        ch.Series[0].Points.AddY(data.MeasurePercentRecurrence);
                        ch.ChartAreas[0].AxisX.CustomLabels.Add(3.5, 4.5, "My Recurrence");
                        ch.Series[0].Points.AddY(data.SelfData);
                        ch.ChartAreas[0].AxisX.CustomLabels.Add(4.5, 5.5, "Self Assess.");
                        ch.ChartAreas[0].AxisX.Maximum = 6.0;
                        ch.Height = 140;
                    }
                    else
                    {
                        if (data.SelfData > 0)
                        {
                            ch.Series[0].Points.AddY(data.SelfData);
                            ch.ChartAreas[0].AxisX.CustomLabels.Add(2.5, 3.5, "Self Assess.");
                            ch.ChartAreas[0].AxisX.Maximum = 4.0;
                            ch.Height = 110;
                        }
                        ch.ChartAreas[0].AxisX.CustomLabels.Add(0.5, 1.5, "Peer 7-12");
                        ch.ChartAreas[0].AxisX.CustomLabels.Add(0.5, 1.5, "My 7-12 Data");
                        ch.ChartAreas[0].AxisX.CustomLabels.Add(2.5, 3.5, "Peer 3-6");
                        ch.ChartAreas[0].AxisX.CustomLabels.Add(1.5, 2.5, "My 3-6 Data");
                    }
                }
            }
            else
            {
                if (data.NumericFullGraph == true)
                {
                    ch.ChartAreas[0].AxisX.Maximum = 6.0;
                    ch.ChartAreas[0].AxisY.Maximum = 200;
                    ch.Series[0].Label = "20/#VALY";
                    ch.Height = 140;
                  
                    if (data.Peer75Percentile != null)
                    {
                        ch.ChartAreas[0].AxisX.CustomLabels.Add(0.5, 1.5, "Peers 75th Percentile");
                        ch.Series[0].Points.AddY(Math.Round((double)data.Peer75Percentile, 0));
                    }

                    if (data.PeerData3To6 != null)
                    {
                        ch.ChartAreas[0].AxisX.CustomLabels.Add(1.5, 2.5, "Peers " + (data.NumericMeasureName == null ? "Data" : data.NumericMeasureName));
                        ch.Series[0].Points.AddY(Math.Round((double)data.PeerData3To6, 0));
                    }
                    if (data.Peer25Percentile != null)
                    {
                        ch.ChartAreas[0].AxisX.CustomLabels.Add(2.5, 3.5, "Peers 25th Percentile");
                        ch.Series[0].Points.AddY(Math.Round((double)data.Peer25Percentile, 0));
                    }
                    if (data.IsPhase2Flag == true)
                    {
                        ch.ChartAreas[0].AxisX.CustomLabels.Add(3.5, 4.5, "Final" + (data.NumericMeasureName == null ? "Data" : data.NumericMeasureName));
                    }
                    else
                    {
                        ch.ChartAreas[0].AxisX.CustomLabels.Add(3.5, 4.5, "Your " + (data.NumericMeasureName == null ? "Data" : data.NumericMeasureName));
                    }
                    ch.Series[0].Points.AddY(Math.Round((double)(data.MeasurePercent3To6), 0));
                    if (data.VisualAcuityResponse != null)
                    {
                        if (data.VisualAcuityResponse[0].ToString() == "2")
                        {
                            var NumericValue = Convert.ToDouble(data.VisualAcuityResponse.Split('/')[1]);
                            ch.ChartAreas[0].AxisX.CustomLabels.Add(4.5, 5.5, "VA Data");
                            ch.Series[0].Points.AddY(Math.Round(NumericValue, 0));
                        }

                    }
                }
                else
                {
                    ch.ChartAreas[0].AxisY.Maximum = 200;
                    ch.ChartAreas[0].AxisX.Maximum = 4.0;
                 
                    if (data.PeerData3To6 != null)
                    {
                        ch.ChartAreas[0].AxisX.CustomLabels.Add(0.5, 1.5, "Peers " + (data.NumericMeasureName == null ? "Data" : data.NumericMeasureName));
                        ch.Series[0].Points.AddY(Math.Round((double)data.PeerData3To6, 0));
                    }
                    if (data.IsPhase2Flag == true)
                    {
                        ch.ChartAreas[0].AxisX.CustomLabels.Add(1.5, 2.5, "Final " + (data.NumericMeasureName == null ? "Data" : data.NumericMeasureName));
                    }
                    else
                    {
                        ch.ChartAreas[0].AxisX.CustomLabels.Add(1.5, 2.5, "Your " + (data.NumericMeasureName == null ? "Data" : data.NumericMeasureName));
                    }
                    ch.Series[0].Points.AddY(Math.Round((double)(data.MeasurePercent3To6), 0));
                    if (data.VisualAcuityResponse != null)
                    {
                        if (data.VisualAcuityResponse[0].ToString() == "2")
                        {
                            var NumericValue = Convert.ToDouble(data.VisualAcuityResponse.Split('/')[1]);
                            ch.ChartAreas[0].AxisX.CustomLabels.Add(23.5, 3.5, "VA Data");
                            ch.Series[0].Points.AddY(Math.Round(NumericValue, 0));
                        }

                    }
                }
              

            }
        }
        PreviousMeasureGroupID = MeasureGroupID;
    }

    protected void ListViewPECMeasures_OnItemDataBound(object sender, ListViewItemEventArgs e)
    {
        if (e.Item.ItemType == ListViewItemType.DataItem)
        {

            Chart ch = (Chart)e.Item.FindControl("ChartPerformance");
            PerformanceReportData data = (PerformanceReportData)((ListViewDataItem)e.Item).DataItem;
            HyperLink chartsIncluded = (HyperLink)e.Item.FindControl("LinkAbstractRecordsIncluded");
            ch.Series[0].LabelBackColor = System.Drawing.Color.White;

            //System.Web.UI.HtmlControls.HtmlGenericControl divRowModuleName = (System.Web.UI.HtmlControls.HtmlGenericControl)e.Item.FindControl("divRowModuleName");
            System.Web.UI.HtmlControls.HtmlTableRow divRowModuleName = (System.Web.UI.HtmlControls.HtmlTableRow)e.Item.FindControl("divRowModuleName");
            if (ModuleNameProcessMeasures != data.ModuleName)
            {
                divRowModuleName.Visible = true;
                ModuleNameProcessMeasures = data.ModuleName;
            }
            System.Web.UI.HtmlControls.HtmlTableRow divRowChart = (System.Web.UI.HtmlControls.HtmlTableRow)e.Item.FindControl("divRowChart");
            if (divRowChart is System.Web.UI.HtmlControls.HtmlTableRow)
            {
                if (data.MeasurePercentCurrent < 50)
                    divRowChart.Attributes.Add("class", "red-row");
                else
                    divRowChart.Attributes.Add("class", "initial_inner_row");
            }
            if (data.IsPhase2Flag == true)
            {
                //ch.Series[0].Points.AddY(data.PeerData);
                ch.Series[0].Points.AddY(data.MeasurePercentCurrent);
                ch.Series[0].Points.AddY(data.MeasureGoal ?? 0);
                ch.Series[0].Points.AddY(data.MeasurePercentPrevious);
                ch.ChartAreas[0].AxisX.CustomLabels.Add(2.5, 3.5, "Your Data");
                ch.ChartAreas[0].AxisX.CustomLabels.Add(1.5, 2.5, "Goal");
                ch.ChartAreas[0].AxisX.CustomLabels.Add(0.5, 1.5, "Final Data");
                //ch.ChartAreas[0].AxisX.CustomLabels.Add(0.5, 1.5, "Peer Data");
                ch.ChartAreas[0].AxisX.Maximum = 4;
                ch.ChartAreas[0].Position.Auto = false;
                ch.ChartAreas[0].Position.X = 0;
                ch.ChartAreas[0].Position.Y = 0;
                ch.ChartAreas[0].Position.Width = 100;
                ch.ChartAreas[0].Position.Height = 100;
                ch.ChartAreas[0].InnerPlotPosition.Auto = false;
                ch.ChartAreas[0].InnerPlotPosition.X = 100;
                ch.ChartAreas[0].InnerPlotPosition.Y = 0;
                ch.ChartAreas[0].InnerPlotPosition.Width = 60;
                ch.ChartAreas[0].InnerPlotPosition.Height = 100;
            }
            else
            {
                //ch.Series[0].Points.AddY(data.PeerData);
                ch.ChartAreas[0].AxisX.Maximum = 2.0;
                ch.Height = 70;
                ch.Series[0].Points.AddY(data.MeasurePercentCurrent);
                if (data.SelfData > 0)
                {
                    ch.Series[0].Points.AddY(data.SelfData);
                    ch.ChartAreas[0].AxisX.CustomLabels.Add(1.5, 2.5, "Self Assess.");
                    ch.ChartAreas[0].AxisX.Maximum = 3.0;
                    ch.Height = 90;
                }
                ch.ChartAreas[0].AxisX.CustomLabels.Add(0.5, 1.5, "Your Data");
                //ch.ChartAreas[0].AxisX.CustomLabels.Add(0.5, 1.5, "Peer Data");

            }
        }

        PreviousMeasureGroupID = MeasureGroupID;
    }

    protected void GridViewRationale_RowDataBound(object sender, GridViewRowEventArgs e)
    {
        if (e.Row.RowType == DataControlRowType.DataRow)
        {
            CheckBoxList list = (CheckBoxList)e.Row.Cells[0].FindControl("CheckBoxListRationale");
            list.DataSource = rationale;
            list.DataBind();
            ListItem other = new ListItem(Constants.SELECTEDTEXT_OTHER, Constants.SELECTEDVALUE_OTHER);
            TextBox textOther = (TextBox)e.Row.Cells[0].FindControl("TextBoxRationaleOther");
            other.Attributes.Add("onclick", "checkOther(this, '" + textOther.ClientID + "')");
            list.Items.Add(other);
        }
    }

    protected bool validForm()
    {
        return true;
    }

    protected void ButtonDevelopImprovementPlan_Click(object sender, EventArgs e)
    {
        List<int> selectedMeasures = (List<int>)ViewState["selectedMeasures"];
        if (validForm())
        {
            using (NetHealthPIMEntities pim = new NetHealthPIMEntities())
            {
                ParticipantModuleCycle cycle = (from c in pim.ParticipantModuleCycle
                                                where c.ParticipantModuleCycleID == ctx.ActiveModuleCycleID
                                                select c).First<ParticipantModuleCycle>();
                var cycleMeasures = from pm in pim.ParticipantMeasure
                                    where pm.ParticipantModuleCycleID == ctx.ActiveModuleCycleID
                                    select pm;
                var measures = cycleMeasures.WhereIn(m => m.MeasureID, selectedMeasures);
                var rationaleTypes = pim.RationaleType;
                using (TransactionScope transaction = new TransactionScope())
                {
                        var measureRationale = (from pmr in pim.ParticipantMeasureRationale
                                                where pmr.ParticipantModuleCycleID == cycle.ParticipantModuleCycleID
                                              select pmr);
                        foreach (var mr in measureRationale)
                        {
                            pim.DeleteObject(mr);
                        }

                        CycleManager.ResetIncludeInImprovementPlanFlag(cycle.ParticipantModuleCycleID);

                        pim.SaveChanges();

                        for (int i = 0; i < GridViewRationale.Rows.Count; i++)
                        {
                            CheckBoxList cbList = (CheckBoxList)GridViewRationale.Rows[i].Cells[0].FindControl("CheckBoxListRationale");
                            string rationaleOther = ((TextBox)GridViewRationale.Rows[i].Cells[0].FindControl("TextBoxRationaleOther")).Text;
                            int measureID = Int32.Parse(((HiddenField)GridViewRationale.Rows[i].Cells[0].FindControl("MeasureID")).Value);
                            ParticipantMeasure activeMeasure = measures.First(m => m.MeasureID == measureID && m.ParticipantModuleCycleID == ctx.ActiveModuleCycleID);
                            activeMeasure.IncludeInImprovementPlan = true;
                            if (rationaleOther.Trim().Length > 0)
                            {
                                activeMeasure.MeasureRationaleOther = rationaleOther.Trim();
                            }
                            activeMeasure.LastUpdateDate = DateTime.Now;

                            // Exclude the "other" option
                            for (int j = 0; j < (cbList.Items.Count - 1); j++)
                            {
                                if (cbList.Items[j].Selected)
                                {
                                    int rationaleTypeID = Int32.Parse(cbList.Items[j].Value);
                                    ParticipantMeasureRationale pmr = new ParticipantMeasureRationale();

                                    // TODO ECG need to check the rationale FKs
                                    //pmr.ParticipantMeasure = measures.First(m => m.MeasureID == measureID);
                                    pmr.ParticipantModuleCycleID = ctx.ActiveModuleCycleID;
                                    pmr.MeasureID = measureID;

                                    pmr.RationaleType = rationaleTypes.First(rt => rt.RationaleTypeID == rationaleTypeID);
                                    pmr.LastUpdateDate = DateTime.Now;
                                }
                            }
                        }
                    if (cycle.ReportApproved == false)
                    {
                        var chartsCountMissing = (from chart in pim.ParticipantChartStatus_V
                                                  where chart.ParticipantModuleCycleID == ctx.ActiveModuleCycleID
                                                  && chart.TotalCharts != chart.Completed
                                                  select chart).Count();
                        if (chartsCountMissing == 0)
                        {
                            cycle.ReportApproved = true;
                            cycle.ReportApprovedCompletedDate = DateTime.Now;
                            cycle.ReportApprovedLastUpdatedDate = DateTime.Now;
                            cycle.AbstractDataComplete = true;
                            cycle.AbstractDataCompletedDate = DateTime.Now;
                            cycle.AbstractDataLastUpdatedDate = DateTime.Now;
                        }
                    }

                    cycle.MeasuresSelected = true;
                    cycle.MeasuresSelectedCompletedDate = DateTime.Now;
                    cycle.MeasuresSelectedLastUpdatedDate = DateTime.Now;
                    cycle.LastUpdateDate = DateTime.Now;
                    pim.SaveChanges();
                    transaction.Complete();
                }
            }
            Response.Redirect("~/" + module.ModuleCode.ToLower() + "/ImprovementPlanPartI.aspx");
        }
    }




    public Dictionary<string, Chart> RelatedMeasuresChart(int ParticipantModuleCycleID, bool? Phase2Flag)
    {

        Dictionary<string, Chart> ModuleCharts = new Dictionary<string, Chart>();
        double InitialConsidered = 0;
        double PeerConsidered = 0;

        string ChartTable = "";
        using (NetHealthPIMEntities pim = new NetHealthPIMEntities())
        {

            var report = from pir in pim.ParticipantInitialReport_V
                         join m in pim.Module on pir.ModuleID equals m.ModuleID
                         where pir.ParticipantModuleCycleID == ParticipantModuleCycleID
                         && pir.MeasureRelated != null
                         orderby m.ModuleID, pir.MeasureGroupSortOrder
                         select new PerformanceReportData
                         {
                             ModuleName = m.ModuleName,
                             MeasureID = pir.MeasureID,
                             MeasureLongDescription = pir.MeasureLongDescription,
                             AbstractRecordsIncluded = (int)pir.AbstractRecordsIncluded,
                             MeasurePercentCurrent = pir.MeasurePercentCurrent,
                             PeerAbstractRecordsIncluded = pir.PeerAbstractRecordsIncluded,
                             Peer25Percentile = pir.Peer25Percentile,
                             Peer75Percentile = pir.Peer75Percentile,
                             PeerData = pir.PeerData,
                             MeasureRelated = pir.MeasureRelated,
                             SelfData = pir.NumericResponse,
                             IsPhase2Flag = Phase2Flag,
                             IncludeInImprovementPlan = pir.IncludeInImprovementPlan,
                             MayNeedImprovement = pir.MayNeedImprovement
                         };
            if (report.Count() > 0)
            {
                InitialConsidered = Math.Round(report.Average(c =>(double)c.AbstractRecordsIncluded), 0);
                PeerConsidered = Math.Round(report.Average(c => (double)c.PeerAbstractRecordsIncluded), 0);
                foreach (var module in report.Select(c => c.ModuleName).Distinct())
                {

                    foreach (var item in report.Where(c => c.ModuleName == module).Select(c => c.MeasureRelated).Distinct())
                    {

                        string Legend = "<table>";
                        Chart chart = new Chart();
                        Series InitailSeries;
                        Legend += "<tr>";
                        InitailSeries = new Series("Your Data");
                        Legend += "<td style='border:none'>Your Data</td>";
                        InitailSeries.ChartType = SeriesChartType.Bar;
                        InitailSeries.Color = System.Drawing.ColorTranslator.FromHtml("#ba55d3");
                        Legend += "<td style='border:none'><div style='width:20px; height:20px; background-color:#ba55d3'></div></td></tr>";
                        InitailSeries.SetCustomProperty("DrawingStyle", "Wedge");
                        InitailSeries.IsValueShownAsLabel = true;
                        InitailSeries.IsVisibleInLegend = false;
                        InitailSeries.LabelForeColor = System.Drawing.ColorTranslator.FromHtml("#000000");
                        InitailSeries.LabelBackColor = System.Drawing.Color.Transparent;
                        chart.Series.Add(InitailSeries);
                        Legend += "<tr><td style='border:none'>Peer Data</td>";
                        Series PeerSeries = new Series("Peer Data");
                        PeerSeries.ChartType = SeriesChartType.Bar;
                        PeerSeries.Color = System.Drawing.ColorTranslator.FromHtml("#8a2bea");
                        Legend += "<td  style='border:none' ><div style='height:20px; width:20px; background-color:#8a2bea'><div></td></tr>";
                        PeerSeries.SetCustomProperty("DrawingStyle", "Wedge");
                        PeerSeries.IsValueShownAsLabel = true;
                        PeerSeries.IsVisibleInLegend = false;
                        PeerSeries.LabelForeColor = System.Drawing.ColorTranslator.FromHtml("#000000");
                        PeerSeries.LabelBackColor = System.Drawing.Color.Transparent;
                        chart.Series.Add(PeerSeries);
                        Legend += "</table>";
                        Legend legend = new Legend();
                        chart.Legends.Add(legend);
                        ChartArea chartArea = new ChartArea();
                        chartArea.AxisX.MajorGrid.Enabled = false;
                        chartArea.AxisX.LabelStyle.IsEndLabelVisible = false;
                        chart.Legends[0].Position.Auto = false;
                        chart.Legends[0].Position = new ElementPosition(30, 5, 150, 20);
                        chart.ChartAreas.Add(chartArea);
                        Axis yAxis = new Axis(chartArea, AxisName.Y);
                        yAxis.Maximum = 100;
                        yAxis.Interval = 20;
                        Axis xAxis = new Axis(chartArea, AxisName.X);
                        chart.ChartAreas[0].AxisX.LabelStyle.Interval = 1;
                        chart.ChartAreas[0].AxisY.LabelStyle.Format = "{#}%";
                        chart.ChartAreas[0].AxisX.LabelStyle.Font = new System.Drawing.Font("Trebuchet MS", 10.25F, System.Drawing.FontStyle.Regular);
                        chart.ChartAreas[0].AxisY.Maximum = 100;
                        var DataText = report.Where(c => c.ModuleName == module && c.MeasureRelated == item).Select(c => c.MeasureLongDescription).ToArray();
                        var InitalData = report.Where(c => c.ModuleName == module && c.MeasureRelated == item).Select(c => c.MeasurePercentCurrent).ToArray();
                        var PeerData = report.Where(c => c.ModuleName == module && c.MeasureRelated == item).Select(c => c.PeerData).ToArray();
                        chart.Series["Your Data"].Points.DataBindXY(DataText, InitalData);
                        chart.Series["Peer Data"].Points.DataBindXY(DataText, PeerData);
                        chart.Series["Your Data"]["PixelPointWidth"] = "40";
                        chart.Series["Peer Data"]["PixelPointWidth"] = "40";
                        if (DataText.Count() > 10)
                        {
                            chart.Width = new System.Web.UI.WebControls.Unit(500, System.Web.UI.WebControls.UnitType.Pixel);
                            chart.Height = new System.Web.UI.WebControls.Unit(600, System.Web.UI.WebControls.UnitType.Pixel);
                        }
                        else if (DataText.Count() > 5)
                        {
                            chart.Width = new System.Web.UI.WebControls.Unit(500, System.Web.UI.WebControls.UnitType.Pixel);
                            chart.Height = new System.Web.UI.WebControls.Unit(DataText.Count() * 80, System.Web.UI.WebControls.UnitType.Pixel);
                        }
                        else if (DataText.Count() > 3)
                        {
                            chart.Width = new System.Web.UI.WebControls.Unit(500, System.Web.UI.WebControls.UnitType.Pixel);
                            chart.Height = new System.Web.UI.WebControls.Unit(DataText.Count() * 100, System.Web.UI.WebControls.UnitType.Pixel);
                        }
                        ChartTable = "<table  style='border:none'><tr style=' border-bottom: thick dotted #ff0000; border-bottom-width: 3px;' ><td  style='border:none' >Your </td><td  style='border:none'>" + InitialConsidered + "</td></tr>";
                        ChartTable += "<tr  style='border:none' ><td  style='border:none' >Peer </td><td  style='border:none'>" + PeerConsidered + "</td></tr></table>"+Legend;
                        ModuleCharts.Add(item.ToString() + "@" +item.ToString()+"@"+ module + "@" + ChartTable, chart);
                    }
                }
            }
            return ModuleCharts;
        }
    }

    protected void LinkButtonDashboard_Click(object sender, EventArgs e)
    {
        Response.Redirect("Dashboard.aspx");
    }
}
