﻿<%@ Page Title="" Language="C#" MasterPageFile="~/PIMMasterPage.master" AutoEventWireup="true" CodeFile="ParticipantGroup.aspx.cs" Inherits="copd_ParticipantGroup" %>

<asp:Content ID="Content1" ContentPlaceHolderID="head" Runat="Server">
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder1" Runat="Server">
<table border="0" cellspacing="0" class="edit_table center_area ">
    <tr>
        <td>Please select the group you want to work today:
                <asp:DropDownList ID="DropDownListGroups" runat="server" DataTextField="ParticipantGroupName" DataValueField="ParticipantGroupID">
                </asp:DropDownList>
        </td>
        <td>
            <asp:Button ID="ButtonSubmit" runat="server" Text="Submit" CausesValidation="true" onclick="ButtonSubmit_Click" ValidationGroup="PatientChartData"/>
        </td>
    </tr>
</table>
</asp:Content>

