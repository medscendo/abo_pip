﻿using System;
using System.Collections.Generic;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

public partial class abo_PECInstructions : BasePage
{
    protected void Page_Load(object sender, EventArgs e)
    {
        ((PIMMasterPage)Page.Master).SetStatus(1);
        ((PIMMasterPage)Page.Master).SetHeader("PEC Instructions");
    }
}
