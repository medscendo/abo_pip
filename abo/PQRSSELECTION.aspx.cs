﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using NetHealthPIMModel;
using System.Transactions;



public partial class abo_PQRSSELECTION  : BasePage
{
    protected void Page_Load(object sender, EventArgs e)
    {
        if (!IsPostBack)
        {
            string breadCrumb = Constants.BC_CREATE_PLAN_LINK + "" + Constants.BC_PQRS_INCENTIVE_PROGRAM;
            ((PIMMasterPage)Page.Master).SetTitle(breadCrumb);
            ((PIMMasterPage)Page.Master).SetStatus(1);
            ((PIMMasterPage)Page.Master).SetHeader("PQRS Incentive Program");
            using (NetHealthPIMEntities db = new NetHealthPIMEntities())
            {
                int participantid = ctx.ParticipantID;
                var mod = (from c in db.ParticipantModule where c.ParticipantID == participantid select c.PQRS).FirstOrDefault();
                RadioButtonList1.SelectedValue = mod.ToString();
                ParticipantModuleCycle cycle = db.ParticipantModuleCycle.First(c => c.ParticipantModuleCycleID == ctx.ActiveModuleCycleID);
                if ((cycle.CycleNumber == 2) || (cycle.ImprovementPlanComplete==true))
                {
                    ButtonSubmit.Visible = false;
                    RadioButtonList1.Enabled = false;
                    ButtonBack.Visible = true;
                }
                if (mod == null)
                    HiddenFieldPQRSSelected.Value = "0";
                else
                {
                    HiddenFieldPQRSSelected.Value = "1";
                    if (mod == true)
                    {
                        HiddenFieldPQRSCurrentSelection.Value = "1";
                    }
                    else
                    {
                        HiddenFieldPQRSCurrentSelection.Value = "0";
                    }
                }
            }
        }


    }
    protected void Button1_Click(object sender, EventArgs e)
    {
        using(NetHealthPIMEntities db=new NetHealthPIMEntities())
        {
            int participantid = ctx.ParticipantID;
            bool selection = Convert.ToBoolean(RadioButtonList1.SelectedItem.Value);
            ParticipantModule mod = (from c in db.ParticipantModule where c.ParticipantID == participantid select c).FirstOrDefault();
            mod.PQRS = selection;
            if (HiddenFieldPQRSSelected.Value == "1")
            {
                // if PQRS was changed from No to Yes. Modules should be review/selected again
                if (HiddenFieldPQRSCurrentSelection.Value == "0" && mod.PQRS == true)
                {
                    ParticipantModuleCycle cycle = db.ParticipantModuleCycle.First(c => c.ParticipantModuleCycleID == ctx.ActiveModuleCycleID);
                    CycleManager.DropModulesAndAbstractions(ctx.ActiveModuleCycleID);
                    var ChartAnswers = (from c in db.ChartQuestionUserReply
                                        where c.ParticipantModuleCycleID == cycle.ParticipantModuleCycleID
                                        select c).ToList();
                    foreach (var m in ChartAnswers)
                    {
                        var ChartChoicesUserReply = (from c in db.PIMChartQuestionChoicesUserReply
                                                     where c.ParticipantModuleCycleID == cycle.ParticipantModuleCycleID
                                                     && c.ChartID == m.ChartID
                                                     select c).ToList();
                        foreach (var choice in ChartChoicesUserReply)
                        {
                            db.PIMChartQuestionChoicesUserReply.DeleteObject(choice);
                        }
                        db.ChartQuestionUserReply.DeleteObject(m);
                    }
                   
                    cycle.SelectNewModulesComplete = false;
                    cycle.SelectNewModulesCompletedDate = null;
                    cycle.ModuleSelectionComplete = false;
                    cycle.ModuleSelectionCompletedDate = null;
                }
            }

            db.SaveChanges();
            Response.Redirect("SelectPriorModules.aspx");

        }
    }
    protected void ButtonPriorModules_Click(object sender, EventArgs e)
    {
        Response.Redirect("~/" + module.ModuleCode.ToLower() + "/PQRSSelection.aspx");
    }
}
