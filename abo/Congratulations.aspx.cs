﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using NetHealthPIMModel;

public partial class abo_Congratulations : BasePage
{
    
    protected void Page_Load(object sender, EventArgs e)
    {
       if (!IsPostBack)
       {
           ((PIMMasterPage)Page.Master).SetStatus(3);
           ((PIMMasterPage)Page.Master).SetHeader("Congratulations");
           using (NetHealthPIMEntities pim = new NetHealthPIMEntities())
           {
               int cycleID = ctx.ActiveModuleCycleID;
               ParticipantModuleCycle cycle = pim.ParticipantModuleCycle.First(c => c.ParticipantModuleCycleID == cycleID);
               Participant participant = pim.Participant.First(p => p.ParticipantID == ctx.ParticipantID);

               string ParticipantGUID = "";
               string CandidateID = "";
               int ApplicationID = 0;

               bool CMEExists = false;
               string currentYear = System.DateTime.Today.Year.ToString();

               ParticipantGUID = participant.ASNGUID;
               CandidateID = participant.CandidateID;
               ApplicationID = Convert.ToInt32(participant.ApplicationID);
               CandidateID = participant.CandidateID;
               if (ctx.SoftwareVersion>0)
               {

                   var CMEStatusCount = (from cme in pim.participantCMECreditsClaim
                                         where cme.CandidateID == CandidateID &&
                                         cme.CMEClaimYear == currentYear
                                         select cme).Count();

                   CMEExists = (CMEStatusCount > 0);
                   if (!CMEExists)
                   { 
                       participantCMECreditsClaim participantCMEStatus = new participantCMECreditsClaim();
                       participantCMEStatus.CandidateID = CandidateID;
                       participantCMEStatus.CMEClaimYear = System.DateTime.Today.Year.ToString();
                       participantCMEStatus.DateCMEFirstClaim = null;
                       participantCMEStatus.DateCMESecondClaim = null;
                       pim.AddToparticipantCMECreditsClaim(participantCMEStatus);
                       pim.SaveChanges();
                   }

                   participantCMECreditsClaim CMEStatus = pim.participantCMECreditsClaim
                       .First(cme => cme.CandidateID == CandidateID 
                           && cme.CMEClaimYear == currentYear);


                       FirstClaim.Visible = true;
                       FirstClaimadditional.Visible = true;
                       SecondClaim.Visible = false;
                       FirstClaimCompleted.Visible = false;
                       CreditsClaimed.Visible = false;
                   div_NewParticipant.Visible = true;
                   div_OldParticipant.Visible = false;
               }
               else
               {
                   div_NewParticipant.Visible = false;
                   div_OldParticipant.Visible = true;
               }

               ABOWebService.PracticeAssessmentManager proxyABO = new ABOWebService.PracticeAssessmentManager();
               DateTime currentDate = System.DateTime.Now;
               proxyABO.UpdatePracticeAssessmentRecord(CandidateID, ParticipantGUID, ApplicationID, ABOWebService.PracticeAssessmentGradeEnum.Passed, currentDate, 1);

               if (cycle.IsComplete != null)
               {
                   if (cycle.IsComplete == false)
                   {
                       cycle.IsComplete = true;
                       cycle.CycleEndDate = DateTime.Now;
                       cycle.LastUpdateDate = DateTime.Now;
                       pim.SaveChanges();
                   }
               }

           }
       }
    }

    protected void ButtonFirstClaim_Click(object sender, EventArgs e)
    {
        using (NetHealthPIMEntities pim = new NetHealthPIMEntities())
        {
            string currentYear = System.DateTime.Today.Year.ToString();
            Participant participant = pim.Participant.First(p => p.ParticipantID == ctx.ParticipantID);
            participantCMECreditsClaim participantCMEStatus = pim.participantCMECreditsClaim.First(cme => cme.CandidateID == participant.CandidateID && cme.CMEClaimYear == currentYear);
            participantCMEStatus.DateCMEFirstClaim = System.DateTime.Now;
            pim.SaveChanges();
            Response.Redirect("https://secure.aao.org/aao/cme-central?CME_Act=2018_ABOPIM_1");
        }
    }

    protected void ButtonSecondClaim_Click(object sender, EventArgs e)
    {
        using (NetHealthPIMEntities pim = new NetHealthPIMEntities())
        {
            string currentYear = System.DateTime.Today.Year.ToString();
            Participant participant = pim.Participant.First(p => p.ParticipantID == ctx.ParticipantID);
            participantCMECreditsClaim participantCMEStatus = pim.participantCMECreditsClaim.First(cme => cme.CandidateID == participant.CandidateID && cme.CMEClaimYear == currentYear);
            participantCMEStatus.DateCMESecondClaim = System.DateTime.Now;
            pim.SaveChanges();
            Response.Redirect("https://secure.aao.org/aao/cme-central?CME_Act=2018_ABOPIM_2");
        }
    }
}
