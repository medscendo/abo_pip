﻿<%@ Page Title="" Language="C#" MasterPageFile="~/PIMMasterPage.master" AutoEventWireup="true" CodeFile="PQRSFAQ.aspx.cs" Inherits="abo_Default" %>

<asp:Content ID="Content1" ContentPlaceHolderID="head" Runat="Server">

    <style type="text/css">
        .important
        {
            font-weight:bold;
            text-align:center;
        }
        .red
        {
            color:Red;
        }
        
        ul
        {
            padding-left:20px !important;
        }
    </style>

</asp:Content>

<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder1" Runat="Server">
     <!-- activity detaile -->
    <div class="pim" style="padding-right: 100px;">
        <div class="heading_box">
            <h2>
                PQRS FAQ</h2>
        </div>
        <div class="imporvenment_plan_details">
            The American Board of Ophthalmology (ABO) currently offers diplomates the ability to participate in the PQRS: MOC incentive program through the Part IV Improvement in Medical Practice Platform.  
            <br />
            <br /><strong>In order to better assist diplomates, the ABO is pleased to announce the ability for diplomates to also participate in the CMS’ PQRS reporting program through the Part IV Improvement in Medical Practice Platform.</strong>
            <br />
            <br />The FAQs below are intended to explain how the process works.
            <br />
            <br /><strong>What is PQRS?</strong>
            <br />The Physician Quality Reporting System (PQRS), a provision of the Affordable Care Act, offers participating physicians an incentive payment equal to 0.5% of their estimated total allowed charges for covered Medicare Part B services.  PQRS measure reporting is separate from PQRS: MOC, and involves the reporting of CMS approved measures for 80% of your Medicare Part B cases to obtain a 0.5% bonus incentive payment.  
            <br />
            <br /><strong>What is PQRS: MOC?</strong>
            <br />PQRS: MOC is a reporting program that offers physicians who are participating in the PQRS reporting program the opportunity to receive an extra incentive payment of 0.5% by participating in a PQRS-approved Maintenance of Certification (MOC) program.  The American Board of Ophthalmology's MOC program has been vetted and approved by CMS for this incentive.  In accordance with PQRS: MOC rules, to qualify for the extra incentive payment, diplomates must meet the requirements of the MOC program AND participate in the program &quot;more frequently&quot; than is required for the calendar year.
            <br />
            <br /><strong>How do these CMS incentive programs relate to each other?</strong>
            <br />PQRS is a reporting program that offers an incentive for reporting certain information to CMS. PQRS: MOC is a reporting program that may be completed <em>in addition</em> to PQRS in order to receive an <em>additional</em> incentive. 
            <br />
            <br />Participation in PQRS is one of the requirements for participation in PQRS: MOC. Therefore, diplomates who are not participating in the PQRS reporting program cannot complete the requirements for PQRS: MOC.
            <br />
            <br /><span class="important">The ABO now offers participation in both of the above programs through the Part IV Platform as a service to our diplomates. <em>PQRS and PQRS: MOC are NOT ABO programs and are NOT requirements of the ABO MOC program</em>.</span>
            <br />
            <br /><strong>How does the ABO Part IV Improvement in Medical Practice platform allow me the opportunity to participate in PQRS measure reporting?</strong>
            <br />Several ABO Improvement in Medical Practice Activities within the Part IV Improvement in Medical Practice platform align with the CMS PQRS Incentive Program (PQRS-enabled Improvement in Medical Practice Activities).  These activities are clearly identified as PQRS-enabled and contain CMS-approved ophthalmic measures.  By selecting these PQRS-enabled Improvement in Medical Practice Activities, you can participate in PQRS measure reporting. 
            <br />
            <br /><strong>Is there a listing of CMS-approved ophthalmic measures for 2012 available?</strong>
            <br />Yes. The following ophthalmic measures are approved by CMS for 2012:
            <ul>
                <li>Measure 12: Primary Open-Angle Glaucoma (POAG): Optic Nerve Evaluation</li>
                <li>Measure 14: Age-Related Macular Degeneration (AMD): Dilated Macular Examination</li>
                <li>Measure 18: Diabetic Retinopathy: Documentation of Presence or Absence of Macular Edema and Level of Severity of Retinopathy</li>
                <li>Measure 19: Diabetic Retinopathy: Communication with the Physician Managing Ongoing Diabetes Care</li>
                <li>Measure 117: Diabetes Mellitus: Dilated Eye Exam in Diabetic Patient</li>
                <li>Measure 140: Age-Related Macular Degeneration (AMD): Counseling on Antioxidant Supplement</li>
                <li>Measure 141: Primary Open-Angle Glaucoma (POAG): Reduction of Intraocular Pressure (IOP) by 15% OR Documentation of a Plan of Care</li>
            </ul>
            <br /><strong>Which of the ABO improvement in medical practice activities are PQRS-enabled and with which CMS measures are they aligned?</strong>
            <table width="70%;" style="margin-top:10px;">
                <tr>
                    <th><p>PQRS Enabled Improvement in Medical Practice Activities</p></th>
                    <th><p>CMS Approved Measures</p></th>
                </tr>
                <tr class="table_body">
                    <td>Cataract Surgical Management</td>
                    <td>#191, #192, #303 and #304</td>
                </tr>
                <tr class="table_body_bg">
                    <td>Primary Open Angle Glaucoma</td>
                    <td>#12 and #141</td>
                </tr>
                <tr class="table_body">
                    <td>Primary Open Angle Glaucoma (Suspect)</td>
                    <td>#12 and #141</td>
                </tr>
                <tr class="table_body_bg">
                    <td>Exudative AMD</td>
                    <td>#14 and #140</td>
                </tr>
                <tr class="table_body">
                    <td>Diabetic Retinopathy</td>
                    <td>#18, #19 and #117</td>
                </tr>
            </table>
            <br /><strong>PQRS measures can be reported individually or as a measures group.  Given these options, how will the ABO’s PQRS-enabled Improvement in Medical Practice Activities accommodate diplomate reporting needs?</strong>
            <br />Correct, PQRS measures can be reported individually or as part of an approved measures group.  If you wish to use your selected Improvement in Medical Practice Activities to report individual ophthalmic measures for PQRS reporting you have the following options:
            <br />
            <br /><strong>GLAUCOMA – 2 INDIVIDUAL MEASURES</strong>
            <br />There are only two glaucoma measures and PQRS requires that you submit at least three measures, therefore you will need to select three Improvement in Medical Practice Activities including the two glaucoma measures and at least one other Improvement in Medical Practice Activity that is PQRS-enabled. For example, to submit three measures you would select Primary Open Angle Glaucoma or Primary Open Angle Glaucoma Suspect then select one of the other PQRS-Enabled activities.  
            <br />
            <br /><strong>EXUDATIVE AMD – 2 INDIVIDUAL MEASURES</strong>
            <br />There are only two retina measures listed under Exudative AMD and PQRS requires that you submit at least three measures; therefore you will need to select additional PQRS-enabled Improvement in Medical Practice Activities. 
            <br />
            <br /><strong>DIABETIC RETINOPATHY IMPROVEMENT IN MEDICAL ACTIVITY – 3 INDIVIDUAL MEASURES</strong>
            <br />Another option for diplomates is to report using the diabetic retinopathy activity.  The diabetic retinopathy activity contains three individual measures (#18, #19 and #117).  These measures can be reported individually, but 80% of your eligible Medicare Part B cases for each measure must be reported for the entire year to satisfy PQRS. 
            <br />
            <br /><strong>What are the requirements for individual measure reporting?</strong>
            <br />When selecting individual measures you must select <strong><em>three individual measures</em></strong> and you are required to enter 80% of your eligible Medicare Part B cases for each measure to be eligible for your 0.5% incentive payment on all of your Medicare Part B allowable charges for the year.
            <br />
            <br /><strong>CMS has stated that for individual measures I only need to report on 50% of my part B Medicare patients.  Why is 80% being suggested for ophthalmic individual measures?</strong>
            <br />Claims-based reporting using ICD9/CPT combinations require only 50% of part B Medicare patients.  However, registry-based reporting requires 80% of part B Medicare patients and the ABO Part IV platform supports registry-based reporting.
            <br />
            <br /><strong>How do I determine when I have obtained 80% of all diagnosed patients seen throughout the year for a particular diagnosis?</strong>
            <br />CMS will validate this based upon the diagnosis and encounter codes that are submitted on your claims.  At your request the billing office could also run a report that provides a list of all patients with the appropriate decision/diagnosis codes and compare these against the number entered in the system.  However, CMS recommends reporting on more than 80% of your individual measures to ensure successful reporting will occur. 
            <br />
            <br /><strong>Individual measure reporting seems like a lot of extra work for a practicing ophthalmologist, is there a way to reduce my reporting burden and still obtain the additional 0.5% bonus incentive payment?</strong>
            <br />Yes. The most convenient reporting option for ophthalmologists is the Cataracts Measures Group.  This group only requires entry of 30 Medicare Part B patients for the entire year.  By submitting via this measures group you are still eligible for the additional 0.5% bonus incentive payment on all of your Medicare Part B allowable charges for the reporting year.  However, you must have patients that fit the measures being assessed 30 &ndash; 90 days following cataract surgery. 
            <br />
            <br /><strong>CATARACT & ANTERIOR – MEASURES GROUP</strong>
            <br /><span class="red">Please note:</span> The cataract measures group only requires entry of 30 Medicare Part B patients for the entire year.  Completing this process makes you eligible for an additional 0.5% bonus incentive payment on all of your Medicare Part B allowable charges for the reporting year.  The cataract measures group must be reported through a registry-based only mechanism i.e. claims-based reporting is not permitted.
            <br />
            <br /><strong>Can I choose to submit cataract measures individually, or must they always be reported as part of the measures group?</strong>
            <br />Yes. Cataract measures can be reported individually, but 80% of your eligible Medicare Part B cases for each measure must be reported for the entire year and these individual measures must be reported through a registry-based mechanism.
            <br />
            <br /><strong>Which CMS approved measures currently make up the Cataract Measure group?</strong>
            <br /><em>The Cataract Measures Group consists of the following CMS approved measures</em>:
            <ul>
                <li>Measure 191 – Cataracts: 20/40 or Better Visual Acuity within 90 days Following Cataract Surgery</li>
                <li>Measure 192 – Cataracts: Complications within 30 Days Following Cataract Surgery Requiring Additional Surgical Procedures</li>
                <li>Measure 303 - Cataracts: Improvement in Patient's Visual Function within 90 Days Following Cataract Surgery</li>
                <li>Measure 304 - Patient Satisfaction within 90 Days Following Cataract Surgery <strong><em>(<span class="red">Please note:</span>  Measure #304 requires a patient experience of care (PEC) survey administered by a third party.)</em></strong></li>
            </ul>
            <br /><strong>Are all of these measures reported directly through the ABO Part IV Improvement in Medical Practice platform, or will I be required to access another system?</strong>
            <br />PQRS enabled Improvement in Medical Practice Activities are selected and entered through the ABO Part IV Improvement in Medical Practice platform.  However, once the ABO collects your data through the PQRS enabled Improvement in Medical Practice Activity it is transmitted to our vendor’s PQRS PRO system where it is checked for accuracy and then submitted to CMS at your request.  A user profile is required to register with PQRS PRO, a CMS qualified registry.
            <br />
            <br /><strong>Will I need to register and pay to participate in PQRS measure reporting and how does this registration process differ from the current ABO opt-in for PQRS: MOC on my MOC status page?</strong>
            <br />Yes. PQRS measure reporting requires registration and $50 fee which is handled within the ABO Part IV Improvement in Medical Practice platform once you select a PQRS enabled Improvement in Medical Practice Activity and agree to use it for the purposes of PQRS measure reporting.
            <br />
            <br /><strong>Why do I need to leave the ABO Part IV Improvement in Medical Practice platform and access the PQRS PRO system to report measures to CMS?</strong>
            <br />The PQRS PRO system is linked to the ABO Part IV Improvement in Medical Practice platform and our CMS approved registry vendor provides the ability for diplomates to submit measures directly to CMS to request your bonus incentive payment.
            <br />
            <br /><strong>Is there a tutorial available to diplomates to help them understand the PQRS PRO system and how this submission process will work?</strong>
            <br />Yes, you can access the PQRS PRO tutorial at the link provided:
            <br /><a href="../ABO_Tutorials/ABO_PIP_Tutorial/player.html">http://www.mynethealth.com/ABO_Tutorials/ABO_PIP_Tutorial/player.html</a>
            <br />
            <br /><strong>Is there a fee associated with my annual submission of PQRS measures to CMS through the PQRS PRO system?</strong>
            <br />Yes. The CMS qualified registry vendor will apply a $50.00 submission fee at the time of your annual submission. This submission fee includes auditing on behalf of the diplomate to ensure that the submitted measure data is accurate and in compliance with CMS reporting standards.  
            <br />
            <br /><strong>Will the PQRS PRO system support a large number of patient charts i.e. 400 or more in the event that this number represents 80% of my diagnosed patients seen throughout the year for a particular diagnosis?</strong>
            <br />Yes. The PQRS PRO system is capable of receiving, documenting and facilitating the necessary auditing required to efficiently and accurately submit a large quantity of patient charts.
            <br />
            <br /><strong>How will CMS be able to identify me and ensure that I receive my bonus incentive payment for the successful submission of my CMS approved measures?</strong>
            <br />CMS will identify you through your individual NPI/TIN number that you enter into the PQRS PRO system as part of your user profile.  Once this information is documented and your successful measure submission is confirmed, CMS will send your bonus incentive payment to you directly.
            <br />
            <br /><strong>When reporting to CMS I prefer to use my group NPI number.  Is there any restriction to using a group NPI number for reporting, or is this acceptable?</strong>
            <br />Providing the correct <em>individual</em> NPI/TIN number combination to receive your bonus incentive payment is critical.  CMS uses your <em>individual</em> NPI number to identify you and ensure that your bonus incentive payment is sent to you. <strong><em>Therefore, please submit your individual NPI number, not the group NPI number when reporting measures to CMS.</em></strong>
            <br />
            <br /><strong><em>(Please note:  There is a limited timeframe to correct invalid TIN/NPI submissions.  If CMS does not receive correct TIN/NPI information, you will not be able to receive your bonus incentive payment, even if you report satisfactorily.)</em></strong>
            <br />
            <div class="bginput"><asp:LinkButton ID="ButtonBack" runat="server" Text="Back" PostBackUrl="PQRSSELECTION.aspx"/></div>
        </div>
    </div>
</asp:Content>