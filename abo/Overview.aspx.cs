﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using NetHealthPIMModel;

public partial class abo_FrontMatter : BasePage
{
    protected void Page_Load(object sender, EventArgs e)
    {
        if (!IsPostBack)
        {
            InitializePage();
        }
    }

    protected void InitializePage()
    {
        ((PIMMasterPage)Page.Master).SetTitle("Overview");
        ((PIMMasterPage)Page.Master).DisplayWelcomeMessage();
        Session[Constants.SESSION_PAGE_ROLETYPE_LEVEL] = Constants.ROLETYPEID_GUEST;
    }

    /// <summary>
    /// There is client validation that prevents form submit until the Parcicipant has accepted the program.
    /// At this point, we update participant's information.
    /// </summary>
    /// <param name="sender"></param>
    /// <param name="e"></param>
    protected void ButtonSubmit_Click(object sender, EventArgs e)
    {
        // Update participant-module record
        int ParticipantID = ctx.ParticipantID;
        int ModuleID = module.ModuleID;
        DateTime eventDate = DateTime.Now;
        using (NetHealthPIMEntities pim = new NetHealthPIMEntities())
        {
            ParticipantModule pm = (from p in pim.ParticipantModule 
                                    where  p.ParticipantID == ParticipantID && p.ModuleID == ModuleID
                                    select p).First<ParticipantModule>();
            pm.ModuleEducationMeetingReference.Load();

            DateTime lstUpdDt = DateTime.Now;
            pm.FacultyDisclosuresCheck = true;
            pm.FacultyDisclosuresCheckDate = lstUpdDt;
            pm.LearningObjectivesCheck = true;
            pm.LearningObjectivesCheckDate = lstUpdDt;
            pm.OverviewCheck = true;
            pm.OverviewCheckDate = lstUpdDt;
            pm.LastUpdateDate = lstUpdDt;
            if (pm.ModuleEducationMeeting != null)
            {
                eventDate = pm.ModuleEducationMeeting.MeetingDate;
            }
            pim.SaveChanges();
        }

        // Call here the CycleManager function that creates the cycle record using the meeting date and a NULL previous cycle. 
        int cycle = CycleManager.GetActiveCycleId(ctx.ParticipantID, ctx.ActiveModuleID, null, eventDate);
        ctx.ActiveModuleCycleID = cycle;
        ctx.IsAuthenticated = true;
        Session[Constants.SESSION_USERCONTEXT] = ctx;
        Response.Redirect("~/" + module.ModuleCode.ToLower() + "/MyPI.aspx");
    }
}
