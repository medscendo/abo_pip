﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using NetHealthPIMModel;

public partial class abo_MeasureRationale : BasePage
{
    protected void Page_Load(object sender, EventArgs e)
    {
        if (!IsPostBack)
        {
            InitializePage();
        }

    }
    protected void InitializePage()
    {


        using (NetHealthPIMEntities pim = new NetHealthPIMEntities())
        {
            int cycleID = ctx.ActiveModuleCycleID;
            ParticipantModuleCycle cycle = pim.ParticipantModuleCycle.First(c => c.ParticipantModuleCycleID == cycleID);

            string strMeasureID = Request.QueryString["mid"];
            if (strMeasureID == "")
                strMeasureID = "0";
            int MeasureID = Convert.ToInt32(strMeasureID);
            try
            {
                Measure measure = pim.Measure.First(m => m.MeasureID == MeasureID);
                LiteralMeasureQualityIndicator2.Text = measure.MeasureQualityIndicator;
                LiteralMeasureLongDescription.Text = measure.MeasureLongDescription;
            }
            catch (Exception e)
            {

            }
        }
    }
}
