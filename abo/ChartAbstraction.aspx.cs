﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.HtmlControls;
using System.Web.UI.WebControls;
using PIM.Services;
using PIM.ViewModels;
using AjaxControlToolkit;
using PIM.Models;
using PIM.Repositories;
using MoreLinq;
using System.Text;
using NetHealthPIMModel;


public partial class abo_ChartAbstraction : BasePage
{


    protected void InitializePage(int Cycle)
    {
        using (NetHealthPIMEntities db = new NetHealthPIMEntities())
        {
         
            ((PIMMasterPage)Page.Master).SetTitle("Chart Abstraction");
            ((PIMMasterPage)Page.Master).SetStatus(Cycle+1);
            ((PIMMasterPage)Page.Master).SetHeader("Chart Abstraction");
        }
    }



    protected void Page_Load(object sender, EventArgs e)
    {
        UserContext ctx = null;
        ctx = (UserContext)Session[Constants.SESSION_USERCONTEXT];
        ButtonSubmit.Enabled = true;
        using (NetHealthPIMEntities db = new NetHealthPIMEntities())
        {

            if (Request.QueryString["CycleNumber"] != null)
            {
                if (Convert.ToInt32(Request.QueryString["CycleNumber"].ToString()) == 1 && ctx.PrevParticipantModuleCycleID != null)
                {
                    ViewState["ParticipantModuleCycleID"] = ctx.PrevParticipantModuleCycleID;
                }
                else
                {
                    ViewState["ParticipantModuleCycleID"] = ctx.ActiveModuleCycleID;
                }
            }
            else
            {
                ViewState["ParticipantModuleCycleID"] = ctx.ActiveModuleCycleID;
            }


        int ActiveModuleCycleID = Convert.ToInt32(ViewState["ParticipantModuleCycleID"].ToString());
        var CycleNumber = (from c in db.ParticipantModuleCycle 
                           where c.ParticipantModuleCycleID == ActiveModuleCycleID 
                           select c.CycleNumber).FirstOrDefault();
        InitializePage(CycleNumber);
        IChartService cr=new ChartService();

        var IsPQRSSelected = (from c in db.ParticipantModule
                              where c.ParticipantID==ctx.ParticipantID 
                              && c.ModuleID==ctx.ActiveModuleID
                              select c.PQRS).FirstOrDefault();
        string ChartAbstractionID = Request.QueryString["ri"];
        if (ViewState["ModuleID"] == null)
        {

            var ChartRegistration = (from c in db.ChartRegistration
                                     where c.RecordIdentifier == ChartAbstractionID
                                     && c.ParticipantModuleCycleID == ActiveModuleCycleID
                                     select new { c.ModuleID, c.DOB, c.UserPatientID, c.InitialVisit, c.LastVisit}).FirstOrDefault();

            ViewState["ModuleID"] = ChartRegistration.ModuleID;
            ViewState["DateOfBirth"] = ChartRegistration.DOB;
            ViewState["LastVisitDate"] = ChartRegistration.LastVisit;
            ViewState["CycleNumber"] = CycleNumber;
            ViewState["InitialExam"] = ChartRegistration.InitialVisit;
            LabelPatientID.Text = "Patient ID: " + ChartRegistration.UserPatientID;
            }
            int ModuleID=(int)ViewState["ModuleID"];
            var ModuleName = (from c in db.Module where c.ModuleID == ModuleID select c.ModuleName).FirstOrDefault();
            LabelChartName.Text = ModuleName;
            List<ChartAbstractionViewModel> questions;
            if (ViewState["questions"] == null)
            {
                questions = cr.GetChartQuestionsForEachMeasure((int)ViewState["ModuleID"], ActiveModuleCycleID, ActiveModuleCycleID, ctx.ParticipantID, ChartAbstractionID, IsPQRSSelected);
                ViewState["questions"] = questions;
            }
            else
            {
                questions = (List<ChartAbstractionViewModel>)ViewState["questions"];
            }

            var groupedquestion = GroupQuestionByMesureGroup(questions, IsPQRSSelected);
            int j = 1;
            foreach (var item in groupedquestion)
            {
                Panel pn = new Panel();
                pn.ID = "panel" + j;
                pn.Controls.Add(new LiteralControl("<br />"));
                if (Request.QueryString["vl"] != null)
                {
                    pn.Controls.Add(GeneratePannel(item.Value, item.Key, cr.GetListOfErrorMesssages(ActiveModuleCycleID, ctx.ParticipantID, ChartAbstractionID)));
                }
                else
                {
                    pn.Controls.Add(GeneratePannel(item.Value, item.Key, new Dictionary<int, string>()));
                }
                pn.Controls.Add(new LiteralControl("<br />"));
                j++;
                PanelContent.Controls.Add(pn);
            }
            ClientScript.RegisterStartupScript(this.GetType(), "JSScript", GenerateJS((int)ViewState["ModuleID"], ActiveModuleCycleID, ActiveModuleCycleID, ctx.ParticipantID));
            if (!IsPostBack)
            {
                foreach (var item in questions)
                {
                    FillUpAnswers(item);
                }
            }
        }

    }

    public Panel GeneratePannel(List<ChartQuestionViewModel> question, string QuestionGroupName, Dictionary<int, string> ErrorMesages)
    {
             Panel pl = new Panel();
             bool IsInlineValidation = false;
             
             pl.Controls.Add(new LiteralControl("<h2 class='pannelheader'>" + QuestionGroupName + "</h2>"));
             HtmlTable table = new HtmlTable();
             table.Attributes.Add("class", "httable");
             int l = 1;
             foreach (var item in question.OrderBy(c=>c.Question.SortOrder))
             {
                 HtmlTableRow row = new HtmlTableRow();
                 row.Attributes.Add("class", "htrow");
                 Label lbquestion = new Label();
                 lbquestion.ID = "label" + item.Question.ChartQuestionID;
                 if (item.Question.IsGrouped == true)
                 {
                     lbquestion.Attributes.Add("style", "margin-left:300px; display: block; font-weight: normal;");
                     lbquestion.Text =item.Question.ChartQuestion.ToString();
                 }
                 else
                 {
                     lbquestion.Text = l + ". " + item.Question.ChartQuestion.ToString();
                 }
                 if (!string.IsNullOrEmpty(item.Question.ToolTip))
                 {
                     if (item.Question.IsGrouped == true)
                     {
                         lbquestion.Text += "<a style='margin-left: 12px;' href='javascript:void(0)'><img id='ToolTip" + item.Question.ChartQuestionID + "' src='../common/images/tip.gif' alt='Tip' /></a>";
                     }
                     else
                     {
                         lbquestion.Text += "<a style='margin-left: 12px;' href='javascript:void(0)'><img id='ToolTip" + item.Question.ChartQuestionID + "' src='../common/images/tip.gif' alt='Tip' /></a>";
                     }


                     string trimmedByLine = item.Question.ToolTip.Replace('\r', ' ').Replace('\n', ' ');


                     string ToolTipScript = "<script>$(function () { $('#ToolTip" + item.Question.ChartQuestionID + "').aToolTip2({ clickIt: true, yOffset: -75, tipContent: '" + trimmedByLine + "' });   });</script>";
                     ClientScript.RegisterClientScriptBlock(this.GetType(), "JSScriptToolTip" + item.Question.ChartQuestionID, ToolTipScript);

                 }
                 lbquestion.Attributes.Add("class", "lbquestion");
                 HtmlTableCell cellquestiontext = new HtmlTableCell();
                 if (item.Question.ChartQuestionTypeID== 6)
                 {

                     cellquestiontext.Attributes.Add("style", "border-right:none");
                 }
                 cellquestiontext.Attributes.Add("class", "cellquest");
                 if (item.UserReply != null)
                 {
                     HiddenField hdfl = new HiddenField();
                     hdfl.ID = "hidden" + item.UserReply.ChartAnswerID;
                     cellquestiontext.Controls.Add(hdfl);
                 }
                 cellquestiontext.Controls.Add(lbquestion);
                 row.Controls.Add(cellquestiontext);
                 HtmlTableCell cellanswertype = new HtmlTableCell();
                 cellanswertype.Attributes.Add("class", "cellanswer");
                 if (item.Question.ChartQuestionTypeID == 1 || item.Question.ChartQuestionTypeID == 10)
                 {
                     Panel wrap = new Panel();
                     wrap.ClientIDMode = ClientIDMode.Static;
                     wrap.ID = item.Question.ChartQuestionID + "-divq";
                     ABORadioButtonList list = new ABORadioButtonList();
                     list.EnableViewState = false;
                     list.ID = "yesno" + item.Question.ChartQuestionID;
                     ListItem yes = new ListItem("Yes", "True");
                     yes.Attributes.Add("class", "Q-" + item.Question.ChartQuestionID);
                     ListItem no = new ListItem("No", "False");
                     no.Attributes.Add("class", "Q-" + item.Question.ChartQuestionID);
                     list.Items.Add(yes);
                     list.Items.Add(no);
                     if (item.Question.IsGrouped == true)
                     {
                         list.RepeatDirection = System.Web.UI.WebControls.RepeatDirection.Horizontal;
                         if (item.UserReply == null || item.UserReply.TrueFalseResponse == null)
                         {
                             list.Items.FindByValue("False").Selected = true;
                         }
                     }
                     list.Attributes.Add("class", "yesno");
                     wrap.Controls.Add(list);
                     if (item.Question.IsGrouped == null || item.Question.IsGrouped == false)
                     {
                         wrap.Controls.Add(new LiteralControl("<br />"));
                     }
                     Label erm = new Label();
                     erm.ID = "ermes" + item.Question.ChartQuestionID;
                     erm.Text = item.Question.ErrorMessage;
                     erm.ForeColor = System.Drawing.Color.Red;
                     erm.Visible = false;
                     if (ErrorMesages.ContainsKey(item.Question.ChartQuestionID))
                     {
                         erm.Visible = true;
                     }
                     wrap.Controls.Add(erm);
                     if (item.Question.IsGrouped == null || item.Question.IsGrouped == false)
                     {
                         wrap.Controls.Add(new LiteralControl("<br />"));
                     }
                     if (item.Question.Required == true && IsInlineValidation == true)
                     {
                         RequiredFieldValidator rq = new RequiredFieldValidator();
                         rq.ControlToValidate = "yesno" + item.Question.ChartQuestionID;
                         rq.ErrorMessage = item.Question.ErrorMessage;
                         wrap.Controls.Add(new LiteralControl("<br />"));
                         wrap.Controls.Add(rq);
                         wrap.Controls.Add(new LiteralControl("<br />"));

                     }
                     cellanswertype.Controls.Add(wrap);
                 }
                 else if (item.Question.ChartQuestionTypeID == 2)
                 {
                     Panel wrap = new Panel();
                     wrap.ClientIDMode = ClientIDMode.Static;
                     wrap.ID = item.Question.ChartQuestionID + "-divq";
                     TextBox box = new TextBox();
                     box.EnableViewState = false;
                     box.ID = "singletext" + item.Question.ChartQuestionID;
                     box.Attributes.Add("class", "Q-" + item.Question.ChartQuestionID);
                     wrap.Controls.Add(box);
                     wrap.Controls.Add(new LiteralControl("<br />"));
                     Label erm = new Label();
                     erm.ID = "ermes" + item.Question.ChartQuestionID;
                     erm.Text = item.Question.ErrorMessage;
                     erm.ForeColor = System.Drawing.Color.Red;
                     erm.Visible = false;
                     if (ErrorMesages.ContainsKey(item.Question.ChartQuestionID))
                     {
                         erm.Visible = true;
                     }
                     wrap.Controls.Add(erm);
                     wrap.Controls.Add(new LiteralControl("<br />"));
                     if (item.Question.Required == true && IsInlineValidation == true)
                     {

                         RequiredFieldValidator rq = new RequiredFieldValidator();
                         rq.ControlToValidate = "singletext" + item.Question.ChartQuestionID;
                         rq.ErrorMessage = item.Question.ErrorMessage;
                         wrap.Controls.Add(new LiteralControl("<br />"));
                         wrap.Controls.Add(rq);
                         wrap.Controls.Add(new LiteralControl("<br />"));
                     }
                     cellanswertype.Controls.Add(wrap);
                 }
                 else if (item.Question.ChartQuestionTypeID == 3)
                 {
                     Panel wrap = new Panel();
                     wrap.ClientIDMode = ClientIDMode.Static;
                     wrap.ID = item.Question.ChartQuestionID + "-divq";
                     ABORadioButtonList radiolist = new ABORadioButtonList();
                     radiolist.ID = "radiolist" + item.Question.ChartQuestionID;
                     radiolist.EnableViewState = false;
                     foreach (var choice in item.QuestionChoices)
                     {
                         ListItem ch = new ListItem(choice.ChartChoiceName, choice.ChartChoiceID.ToString());
                         ch.Attributes.Add("class", "Q-" + item.Question.ChartQuestionID);
                         radiolist.Items.Add(ch);
                     }
                     radiolist.Attributes.Add("class", "radiolist");
                     wrap.Controls.Add(radiolist);
                     wrap.Controls.Add(new LiteralControl("<br />"));
                     Label erm = new Label();
                     erm.ID = "ermes" + item.Question.ChartQuestionID;
                     erm.Text = item.Question.ErrorMessage;
                     erm.ForeColor = System.Drawing.Color.Red;
                     erm.Visible = false;
                     if (ErrorMesages.ContainsKey(item.Question.ChartQuestionID))
                     {
                         erm.Visible = true;
                     }
                     wrap.Controls.Add(erm);
                     wrap.Controls.Add(new LiteralControl("<br />"));
                     if (item.Question.Required == true && IsInlineValidation == true)
                     {
                         RequiredFieldValidator rq = new RequiredFieldValidator();
                         rq.ControlToValidate = "radiolist" + item.Question.ChartQuestionID;
                         rq.ErrorMessage = item.Question.ErrorMessage;
                         wrap.Controls.Add(new LiteralControl("<br />"));
                         wrap.Controls.Add(rq);
                         wrap.Controls.Add(new LiteralControl("<br />"));
                     }
                     cellanswertype.Controls.Add(wrap);
                 }

                 else if (item.Question.ChartQuestionTypeID == 4)
                 {
                     Panel wrap = new Panel();
                     wrap.ClientIDMode = ClientIDMode.Static;
                     wrap.ID = item.Question.ChartQuestionID + "-divq";
                     ABOCheckBoxList chklist = new ABOCheckBoxList();
                     chklist.EnableViewState = false;
                     chklist.ID = "chklist" + item.Question.ChartQuestionID;
                     foreach (var choice in item.QuestionChoices)
                     {
                         ListItem ch = new ListItem(choice.ChartChoiceName, choice.ChartChoiceID.ToString());
                         ch.Attributes.Add("class", "Q-" + item.Question.ChartQuestionID + "-" + choice.ChartChoiceID);
                         ch.Attributes.Add("lang", item.Question.ChartQuestionID.ToString());
                         chklist.Items.Add(ch);
                     }
                     chklist.Attributes.Add("class", "chklist");

                     wrap.Controls.Add(chklist);
                     wrap.Controls.Add(new LiteralControl("<br />"));
                     Label erm = new Label();
                     erm.ID = "ermes" + item.Question.ChartQuestionID;
                     erm.Text = item.Question.ErrorMessage;
                     erm.ForeColor = System.Drawing.Color.Red;
                     erm.Visible = false;
                     if (ErrorMesages.ContainsKey(item.Question.ChartQuestionID))
                     {
                         erm.Visible = true;
                     }
                     wrap.Controls.Add(erm);
                     wrap.Controls.Add(new LiteralControl("<br />"));
                     cellanswertype.Controls.Add(wrap);
                 }
                 else if (item.Question.ChartQuestionTypeID == 5)
                 {
                     Panel wrap = new Panel();
                     wrap.ClientIDMode = ClientIDMode.Static;
                     wrap.ID = item.Question.ChartQuestionID + "-divq";
                     TextBox numberbox = new TextBox();
                     numberbox.EnableViewState = false;
                     numberbox.ID = "numberbox" + item.Question.ChartQuestionID;
                     numberbox.Attributes.Add("class", "Q-" + item.Question.ChartQuestionID);
                     FilteredTextBoxExtender txtfilter = new FilteredTextBoxExtender();
                     txtfilter.FilterType = FilterTypes.Custom;
                     txtfilter.ValidChars = "1234567890+-=/*().";
                     txtfilter.TargetControlID = "numberbox" + item.Question.ChartQuestionID;
                     wrap.Controls.Add(numberbox);
                     wrap.Controls.Add(txtfilter);
                     if (item.Question.NumericRangeMin != null && item.Question.NumericRangeMax != null)
                     {
                         RangeValidator rg = new RangeValidator();
                         rg.ControlToValidate = "numberbox" + item.Question.ChartQuestionID;
                         rg.MinimumValue = item.Question.NumericRangeMin.ToString();
                         rg.MaximumValue = item.Question.NumericRangeMax.ToString();
                         rg.Type = ValidationDataType.Double;
                         rg.ErrorMessage = "The range must be from " + item.Question.NumericRangeMin + " to " + item.Question.NumericRangeMax;
                         rg.SetFocusOnError = true;
                         wrap.Controls.Add(new LiteralControl("<br />"));
                         wrap.Controls.Add(rg);
                         wrap.Controls.Add(new LiteralControl("<br />"));


                     }
                     if (item.Question.Required == true && IsInlineValidation == true)
                     {
                         RequiredFieldValidator rq = new RequiredFieldValidator();
                         rq.ControlToValidate = "numberbox" + item.Question.ChartQuestionID;
                         rq.ErrorMessage = item.Question.ErrorMessage;
                         wrap.Controls.Add(new LiteralControl("<br />"));
                         wrap.Controls.Add(rq);
                         wrap.Controls.Add(new LiteralControl("<br />"));
                     }
                     wrap.Controls.Add(new LiteralControl("<br />"));
                     Label erm = new Label();
                     erm.ID = "ermes" + item.Question.ChartQuestionID;
                     erm.Text = item.Question.ErrorMessage;
                     erm.ForeColor = System.Drawing.Color.Red;
                     erm.Visible = false;
                     if (ErrorMesages.ContainsKey(item.Question.ChartQuestionID))
                     {
                         erm.Visible = true;
                     }
                     wrap.Controls.Add(erm);
                     wrap.Controls.Add(new LiteralControl("<br />"));
                     cellanswertype.Controls.Add(wrap);
                 }
                 else if (item.Question.ChartQuestionTypeID == 6)
                 {
                     cellanswertype.Attributes.Add("style", "border-left:none");
                 }
                 else if (item.Question.ChartQuestionTypeID == 7)
                 {
                     Panel wrap = new Panel();
                     wrap.ClientIDMode = ClientIDMode.Static;
                     wrap.ID = item.Question.ChartQuestionID + "-divq";
                     ABORadioButtonList radiolistrating = new ABORadioButtonList();
                     radiolistrating.EnableViewState = false;
                     radiolistrating.ID = "radiolistrating" + item.Question.ChartQuestionID;
                     for (int j = 1; j <= 10; j++)
                     {
                         ListItem ch = new ListItem(j.ToString(), j.ToString());
                         ch.Attributes.Add("class", "Q-" + item.Question.ChartQuestionID);
                         radiolistrating.Items.Add(ch);
                     }
                     radiolistrating.RepeatDirection = System.Web.UI.WebControls.RepeatDirection.Horizontal;
                     radiolistrating.Attributes.Add("class", "Q-" + item.Question.ChartQuestionID);
                     wrap.Controls.Add(radiolistrating);
                     wrap.Controls.Add(new LiteralControl("<br />"));
                     Label erm = new Label();
                     erm.ID = "ermes" + item.Question.ChartQuestionID;
                     erm.Text = item.Question.ErrorMessage;
                     erm.ForeColor = System.Drawing.Color.Red;
                     erm.Visible = false;
                     if (ErrorMesages.ContainsKey(item.Question.ChartQuestionID))
                     {
                         erm.Visible = true;
                     }
                     wrap.Controls.Add(erm);
                     wrap.Controls.Add(new LiteralControl("<br />"));

                     if (item.Question.Required == true && IsInlineValidation == true)
                     {
                         RequiredFieldValidator rq = new RequiredFieldValidator();
                         rq.ControlToValidate = "radiolistrating" + item.Question.ChartQuestionID;
                         rq.ErrorMessage = item.Question.ErrorMessage;
                         wrap.Controls.Add(new LiteralControl("<br />"));
                         wrap.Controls.Add(rq);
                         wrap.Controls.Add(new LiteralControl("<br />"));
                     }
                     cellanswertype.Controls.Add(wrap);
                 }
                 else if (item.Question.ChartQuestionTypeID == 8)
                 {
                     string DateOfBirth = "";
                     string InitialExam = "";
                     if (item.Question.ChartQuestion.Contains("Date of Birth") || item.Question.ChartQuestion.Contains("DOB"))
                     {
                         DateOfBirth = ViewState["DateOfBirth"].ToString();
                     }
                     if (item.Question.ChartQuestion.Contains("Initial Exam") || item.Question.ChartQuestion.Contains("Initial Visit"))
                     {
                         InitialExam = ViewState["InitialExam"].ToString();
                     }
                     Panel wrap = new Panel();
                     wrap.ClientIDMode = ClientIDMode.Static;
                     wrap.ID = item.Question.ChartQuestionID + "-divq";
                     DropDownList ddldatemonth = new DropDownList();
                    // ddldatemonth.EnableViewState = false;
                     ddldatemonth.ID = "dateboxmonth" + item.Question.ChartQuestionID;
                     for (int g = 1; g <= 12; g++)
                     {
                         var FinalDate = g.ToString();
                         if (g.ToString().Length == 1)
                         {
                             FinalDate = "0" + g.ToString();
                         }
                         ddldatemonth.Items.Add(new ListItem(FinalDate, FinalDate));
                     }


                     ddldatemonth.Items.Insert(0, new ListItem("---", String.Empty));
                     ddldatemonth.Attributes.Add("class", "Q-" + item.Question.ChartQuestionID);
                     DropDownList ddldatedate = new DropDownList();
                   //  ddldatedate.EnableViewState=false;
                     ddldatedate.ID = "ddldatedate" + item.Question.ChartQuestionID;
                     if (string.IsNullOrEmpty(DateOfBirth) && !string.IsNullOrEmpty(ViewState["DateOfBirth"].ToString()))
                     {
                         var YearOfDateOfBirth = ViewState["DateOfBirth"].ToString().Split('/')[1];
                         for (int g = 2018; g >= (Convert.ToInt32(YearOfDateOfBirth)); g--)
                         {
                             ddldatedate.Items.Add(new ListItem(g.ToString(), g.ToString()));
                         }

                     }
                     else
                     {
                         for (int g = 2018; g >= 1900; g--)
                         {
                             ddldatedate.Items.Add(new ListItem(g.ToString(), g.ToString()));
                         }
                     }

                     ddldatedate.Items.Insert(0, new ListItem("---", String.Empty));

                     ddldatedate.Attributes.Add("class", "Q-" + item.Question.ChartQuestionID);
                     if (item.UserReply == null && !string.IsNullOrEmpty(DateOfBirth))
                     {
                         var DatesOfDateOfBirth = DateOfBirth.Split('/');
                         if (DatesOfDateOfBirth[0].Length == 1)
                         {
                             DatesOfDateOfBirth[0] = "0" + DatesOfDateOfBirth[0];
                         }
                         ddldatemonth.Items.FindByValue(DatesOfDateOfBirth[0]).Selected = true;
                         ddldatedate.Items.FindByValue(DatesOfDateOfBirth[1]).Selected = true;

                     }
                     if (!string.IsNullOrEmpty(DateOfBirth))
                     {
                         ddldatedate.Enabled = false;
                         ddldatemonth.Enabled = false;
                     }
                     if (item.UserReply == null && !string.IsNullOrEmpty(InitialExam))
                     {
                         var DatesOfInitalExam = InitialExam.Split('/');
                         if (DatesOfInitalExam[0].Length == 1)
                         {
                             DatesOfInitalExam[0] = "0" + DatesOfInitalExam[0];
                         }
                         ddldatemonth.Items.FindByValue(DatesOfInitalExam[0]).Selected = true;
                         ddldatedate.Items.FindByValue(DatesOfInitalExam[1]).Selected = true;
                         ddldatedate.Enabled = false;
                     }
                     if (!string.IsNullOrEmpty(InitialExam))
                     {
                         ddldatedate.Enabled = false;
                         ddldatemonth.Enabled = false;
                     }

                     wrap.Controls.Add(ddldatemonth);
                     wrap.Controls.Add(ddldatedate);
                     wrap.Controls.Add(new LiteralControl("<br />"));
                     Label erm = new Label();
                     erm.ID = "ermes" + item.Question.ChartQuestionID;
                     erm.Text = item.Question.ErrorMessage;
                     erm.ForeColor = System.Drawing.Color.Red;
                     erm.Visible = false;
                     if (ErrorMesages.ContainsKey(item.Question.ChartQuestionID))
                     {
                         erm.Visible = true;
                     }
                     wrap.Controls.Add(erm);
                     wrap.Controls.Add(new LiteralControl("<br />"));
                     if (item.Question.Required == true && IsInlineValidation == true)
                     {
                         RequiredFieldValidator rq = new RequiredFieldValidator();
                         rq.ControlToValidate = "datebox" + item.Question.ChartQuestionID;
                         rq.ErrorMessage = item.Question.ErrorMessage;
                         wrap.Controls.Add(new LiteralControl("<br />"));
                         wrap.Controls.Add(rq);
                         wrap.Controls.Add(new LiteralControl("<br />"));
                     }
                     cellanswertype.Controls.Add(wrap);

                 }
                 else if (item.Question.ChartQuestionTypeID == 9)
                 {
                     Panel wrap = new Panel();
                     wrap.ClientIDMode = ClientIDMode.Static;
                     wrap.ID = item.Question.ChartQuestionID + "-divq";
                     TextBox multibox = new TextBox();
                     multibox.EnableViewState = false;
                     multibox.ID = "multibox" + item.Question.ChartQuestionID;
                     multibox.Attributes.Add("class", "Q-" + item.Question.ChartQuestionID);
                     multibox.TextMode = TextBoxMode.MultiLine;
                     multibox.Columns = 10;
                     multibox.Rows = 30;
                     wrap.Controls.Add(multibox);
                     wrap.Controls.Add(new LiteralControl("<br />"));
                     Label erm = new Label();
                     erm.ID = "ermes" + item.Question.ChartQuestionID;
                     erm.Text = item.Question.ErrorMessage;
                     erm.ForeColor = System.Drawing.Color.Red;
                     erm.Visible = false;
                     if (ErrorMesages.ContainsKey(item.Question.ChartQuestionID))
                     {
                         erm.Visible = true;
                     }
                     wrap.Controls.Add(erm);
                     wrap.Controls.Add(new LiteralControl("<br />"));
                     if (item.Question.Required == true && IsInlineValidation == true)
                     {
                         RequiredFieldValidator rq = new RequiredFieldValidator();
                         rq.ControlToValidate = "multibox" + item.Question.ChartQuestionID;
                         rq.ErrorMessage = item.Question.ErrorMessage;
                         wrap.Controls.Add(new LiteralControl("<br />"));
                         wrap.Controls.Add(rq);
                         wrap.Controls.Add(new LiteralControl("<br />"));
                     }
                     cellanswertype.Controls.Add(wrap);

                 }
                 else if (item.Question.ChartQuestionTypeID == 14)
                 {
                     Panel wrap = new Panel();
                     wrap.ClientIDMode = ClientIDMode.Static;
                     wrap.ID = item.Question.ChartQuestionID + "-divq";
                     DropDownList ddllist = new DropDownList();
                     ddllist.EnableViewState = false;
                     ddllist.ID = "ddllist" + item.Question.ChartQuestionID;
                     foreach (var choice in item.QuestionChoices)
                     {
                         ddllist.Items.Add(new ListItem { Text = choice.ChartChoiceName, Value = choice.ChartChoiceID.ToString() });
                     }
                     ddllist.Items.Insert(0, new ListItem { Text = "Please Select", Value = "" });
                     ddllist.Attributes.Add("class", "Q-" + item.Question.ChartQuestionID);
                     wrap.Controls.Add(ddllist);
                     wrap.Controls.Add(new LiteralControl("<br />"));
                     Label erm = new Label();
                     erm.ID = "ermes" + item.Question.ChartQuestionID;
                     erm.Text = item.Question.ErrorMessage;
                     erm.ForeColor = System.Drawing.Color.Red;
                     erm.Visible = false;
                     if (ErrorMesages.ContainsKey(item.Question.ChartQuestionID))
                     {
                         erm.Visible = true;
                     }
                     wrap.Controls.Add(erm);
                     wrap.Controls.Add(new LiteralControl("<br />"));
                     if (item.Question.Required == true && IsInlineValidation == true)
                     {
                         RequiredFieldValidator rq = new RequiredFieldValidator();
                         rq.ControlToValidate = "ddllist" + item.Question.ChartQuestionID;
                         rq.ErrorMessage = item.Question.ErrorMessage;
                         wrap.Controls.Add(new LiteralControl("<br />"));
                         wrap.Controls.Add(rq);
                         wrap.Controls.Add(new LiteralControl("<br />"));
                     }
                     cellanswertype.Controls.Add(wrap);

                 }
                 else if (item.Question.ChartQuestionTypeID == 15)
                 {
                     Panel wrap = new Panel();
                     wrap.ClientIDMode = ClientIDMode.Static;
                     wrap.ID = item.Question.ChartQuestionID + "-divq";

                     HtmlTable VAtable = new HtmlTable();


                     HtmlTableRow VAtableRowWith = new HtmlTableRow();
                     HtmlTableCell VAEyeWithCell1 = new HtmlTableCell();
                     VAEyeWithCell1.Controls.Add(new Label { Text = "" });
                     HtmlTableCell VAEyeWithCell2 = new HtmlTableCell();
                     DropDownList ddllistVAEyeWith = new DropDownList();
                     ddllistVAEyeWith.EnableViewState = false;
                     ddllistVAEyeWith.ID = "ddllist" + item.Question.ChartQuestionID + "VAEyeWith";
                     ddllistVAEyeWith.DataSource = CycleManager.getNewExamValues();
                     ddllistVAEyeWith.DataTextField = "examLabel";
                     ddllistVAEyeWith.DataValueField = "examValue";
                     ddllistVAEyeWith.DataBind();
                     ddllistVAEyeWith.Items.Insert(0, new ListItem("Select", String.Empty));
                     ddllistVAEyeWith.Attributes.Add("class", "Q-" + item.Question.ChartQuestionID);
                     VAEyeWithCell2.Controls.Add(ddllistVAEyeWith);
                     VAtableRowWith.Controls.Add(VAEyeWithCell1);
                     VAtableRowWith.Controls.Add(VAEyeWithCell2);
                     VAtable.Controls.Add(VAtableRowWith);
                     wrap.Controls.Add(VAtable);
                     wrap.Controls.Add(new LiteralControl("<br />"));
                     Label erm = new Label();
                     erm.ID = "ermes" + item.Question.ChartQuestionID;
                     erm.Text = item.Question.ErrorMessage;
                     erm.ForeColor = System.Drawing.Color.Red;
                     erm.Visible = false;
                     if (ErrorMesages.ContainsKey(item.Question.ChartQuestionID))
                     {
                         erm.Visible = true;
                     }
                     wrap.Controls.Add(erm);
                     wrap.Controls.Add(new LiteralControl("<br />"));
                     cellanswertype.Controls.Add(wrap);

                 }
                 else if (item.Question.ChartQuestionTypeID == 16)
                 {
                     Panel wrap = new Panel();
                     wrap.ClientIDMode = ClientIDMode.Static;
                     wrap.ID = item.Question.ChartQuestionID + "-divq";
                     HtmlTable CyclTable = new HtmlTable();

                     HtmlTableRow PlusMinusTableRowWith = new HtmlTableRow();
                     ABORadioButtonList plusminuscyl = new ABORadioButtonList();
                     plusminuscyl.EnableViewState = false;
                     plusminuscyl.ClientIDMode = ClientIDMode.Static;
                     plusminuscyl.ID = "cycl" + item.Question.ChartQuestionID + "plusminus";
                     ListItem chpls = new ListItem("+", "True");
                     chpls.Attributes.Add("class", "Q-" + item.Question.ChartQuestionID + "cl");
                     ListItem chpminus = new ListItem("-", "False");
                     chpminus.Attributes.Add("class", "Q-" + item.Question.ChartQuestionID + "cl");
                     chpminus.Attributes.Add("style", "margin-left: 10px;");
                     plusminuscyl.RepeatDirection = System.Web.UI.WebControls.RepeatDirection.Horizontal;
                     plusminuscyl.Items.Add(chpls);
                     plusminuscyl.Items.Add(chpminus);
                     HtmlTableCell tblcellplsminus = new HtmlTableCell();
                     tblcellplsminus.Controls.Add(plusminuscyl);
                     tblcellplsminus.Controls.Add(new LiteralControl("<br />"));
                     PlusMinusTableRowWith.Controls.Add(tblcellplsminus);
                   


                     //sph 
                     HtmlTableRow TextTableRowWith = new HtmlTableRow();
                     TextBox txsph = new TextBox();
                     txsph.EnableViewState = false;
                     txsph.ID = "cycl" + item.Question.ChartQuestionID + "sph";
                     txsph.ClientIDMode = ClientIDMode.Static;
                     txsph.Width = 40;
                     txsph.MaxLength = 2;
                     txsph.Attributes.Add("class", "Q-" + item.Question.ChartQuestionID + "sph");
                     DropDownList Dropdownlistsph = new DropDownList();
                     Dropdownlistsph.EnableViewState = false;
                     Dropdownlistsph.ID = "DropDown" + item.Question.ChartQuestionID + "sph";
                     Dropdownlistsph.Attributes.Add("class", "Q-DDL" + item.Question.ChartQuestionID + "sph");
                     Dropdownlistsph.ClientIDMode = ClientIDMode.Static;
                     string[] values = { ".0", ".25", ".5", ".75" };
                     foreach (var val in values)
                     {
                         Dropdownlistsph.Items.Add(new ListItem(val, val));
                     }
                     FilteredTextBoxExtender txtfiltersph = new FilteredTextBoxExtender();
                     txtfiltersph.FilterType = FilterTypes.Custom;
                     txtfiltersph.ValidChars = "1234567890";
                     txtfiltersph.TargetControlID = "cycl" + item.Question.ChartQuestionID + "sph";
                     txtfiltersph.ID = "cycl" + item.Question.ChartQuestionID + "sphfl";
                     HtmlTableCell tblcellsph = new HtmlTableCell();
                     tblcellsph.Controls.Add(txsph);
                     tblcellsph.Controls.Add(new LiteralControl(" "));
                     tblcellsph.Controls.Add(Dropdownlistsph);
                     tblcellsph.Controls.Add(txtfiltersph);
                     tblcellsph.Attributes.Add("style", "vertical-align: top;");
                     tblcellsph.Controls.Add(new LiteralControl { Text = " + " });
                     RangeValidator rg = new RangeValidator();
                     rg.ControlToValidate = "cycl" + item.Question.ChartQuestionID + "sph";
                     rg.MinimumValue = "0";
                     rg.MaximumValue = "99";
                     rg.Type = ValidationDataType.Double;
                     rg.ID = "range" + item.Question.ChartQuestionID + "sph";
                     rg.ErrorMessage = "The range must be from 0 to 30";
                     rg.Display = ValidatorDisplay.Dynamic;
                     rg.SetFocusOnError = true;
                     tblcellsph.Controls.Add(rg);
                     TextTableRowWith.Controls.Add(tblcellsph);
                  




                    

                     //cyl
                     TextBox txcyl = new TextBox();
                     txcyl.EnableViewState = false;
                     txcyl.ID = "cycl" + item.Question.ChartQuestionID + "cyl";
                     txcyl.ClientIDMode = ClientIDMode.Static;
                     txcyl.Width = 40;
                     txcyl.MaxLength = 2;
                     txcyl.Attributes.Add("class", "Q-" + item.Question.ChartQuestionID + "cyl");

                     DropDownList Dropdownlistcycl = new DropDownList();
                     Dropdownlistcycl.EnableViewState = false;
                     Dropdownlistcycl.ID = "DropDown" + item.Question.ChartQuestionID + "cycl";
                     Dropdownlistcycl.Attributes.Add("class", "Q-DDL" + item.Question.ChartQuestionID + "cycl");
                     Dropdownlistcycl.ClientIDMode = ClientIDMode.Static;
                     foreach (var val in values)
                     {
                         Dropdownlistcycl.Items.Add(new ListItem(val, val));
                     }
                     FilteredTextBoxExtender txtfiltercyl = new FilteredTextBoxExtender();
                     txtfiltercyl.FilterType = FilterTypes.Custom;
                     txtfiltercyl.ValidChars = "1234567890";
                     txtfiltercyl.TargetControlID = "cycl" + item.Question.ChartQuestionID + "cyl";
                     txtfiltercyl.ID = "cycl" + item.Question.ChartQuestionID + "cylfl";
                     HtmlTableCell tblcellcyl = new HtmlTableCell();
                     tblcellcyl.Controls.Add(txcyl);
                     tblcellcyl.Controls.Add(new LiteralControl(" "));
                     tblcellcyl.Controls.Add(Dropdownlistcycl);
                     tblcellcyl.Controls.Add(txtfiltercyl);
                     RangeValidator rgcyl = new RangeValidator();
                     rgcyl.ControlToValidate = "cycl" + item.Question.ChartQuestionID + "cyl";
                     rgcyl.MinimumValue = "0";
                     rgcyl.MaximumValue = "99";
                     rgcyl.ID = "range" + item.Question.ChartQuestionID + "cyl";
                     rgcyl.ErrorMessage = "The range must be from 0 to 20";
                     rgcyl.SetFocusOnError = true;
                     rgcyl.Type = ValidationDataType.Double;
                     rgcyl.Display = ValidatorDisplay.Dynamic;
                     tblcellcyl.Controls.Add(new LiteralControl { Text = " x " });
                     tblcellcyl.Attributes.Add("style", "vertical-align: top;");
                     tblcellcyl.Controls.Add(rgcyl);
                     TextTableRowWith.Controls.Add(tblcellcyl);


                     //axis 
                     TextBox txaxis = new TextBox();
                     txaxis.EnableViewState = false;
                     txaxis.ID = "cycl" + item.Question.ChartQuestionID + "axis";
                     txaxis.ClientIDMode = ClientIDMode.Static;
                     txaxis.Width = 30;
                     txaxis.Attributes.Add("class", "Q-" + item.Question.ChartQuestionID + "axis");
                     FilteredTextBoxExtender txtfilteraxis = new FilteredTextBoxExtender();
                     txtfilteraxis.FilterType = FilterTypes.Custom;
                     txtfilteraxis.ValidChars = "1234567890+-=/*().";
                     txtfilteraxis.TargetControlID = "cycl" + item.Question.ChartQuestionID + "axis";
                     txtfilteraxis.ID = "axis" + item.Question.ChartQuestionID + "axisfl";
                     HtmlTableCell tblcellaxis = new HtmlTableCell();
                     tblcellaxis.Controls.Add(txaxis);
                     tblcellaxis.Controls.Add(txtfilteraxis);
                     RangeValidator rgaxis = new RangeValidator();
                     rgaxis.ControlToValidate = "cycl" + item.Question.ChartQuestionID + "axis";
                     rgaxis.MinimumValue = "0";
                     rgaxis.MaximumValue = "180";
                     rgaxis.ID = "range" + item.Question.ChartQuestionID + "axis";
                     rgaxis.ErrorMessage = "The range must be from 0 to 180";
                     rgaxis.SetFocusOnError = true;
                     rgaxis.Type = ValidationDataType.Double;
                     rgaxis.Display = ValidatorDisplay.Dynamic;
                     tblcellaxis.Controls.Add(new LiteralControl { Text = "" });
                     tblcellaxis.Attributes.Add("style", "vertical-align: top;");
                     tblcellaxis.Controls.Add(rgaxis);
                     TextTableRowWith.Controls.Add(tblcellaxis);




                     HtmlTableRow CyclNARow = new HtmlTableRow();
                     HtmlTableCell CyclNACell = new HtmlTableCell();
                     CheckBox CyclNACheckBox = new CheckBox();
                     CyclNACheckBox.Text = "Not Performed";

                     CyclNACheckBox.EnableViewState = false;
                     CyclNACheckBox.ID = "Cycl" + item.Question.ChartQuestionID + "NA";
                     CyclNACheckBox.Attributes.Add("class", "Q-" + item.Question.ChartQuestionID + "NA");
                     CyclNACheckBox.ClientIDMode = ClientIDMode.Static;

                     CyclNACell.Controls.Add(new LiteralControl("<br />"));
                     CyclNACell.Controls.Add(CyclNACheckBox);
                     CyclNARow.Controls.Add(CyclNACell);


                     string JSScript = @" <script language='JavaScript' type='text/javascript'>

              $( document ).ready(function() {
                       if ($('#Cycl"+item.Question.ChartQuestionID+@"NA').is(':checked'))
                          {
                                 $('#cycl"+item.Question.ChartQuestionID+@"sph').attr('disabled', true);
                                 $('#cycl"+item.Question.ChartQuestionID+@"sph').val('');
                                 $('#DropDown" + item.Question.ChartQuestionID + @"sph').attr('disabled', true);
                                 $('#DropDown" + item.Question.ChartQuestionID + @"sph').val('.0');
                                 $('#cycl" + item.Question.ChartQuestionID + @"cyl').attr('disabled', true);
                                 $('#cycl" + item.Question.ChartQuestionID + @"cyl').val('');
                                 $('#DropDown" + item.Question.ChartQuestionID + @"cycl').attr('disabled', true);
                                 $('#DropDown" + item.Question.ChartQuestionID + @"cycl').val('.0');
                                 $('#cycl" + item.Question.ChartQuestionID + @"axis').attr('disabled', true);
                                 $('#cycl" + item.Question.ChartQuestionID + @"axis').val('');
                                 $('#cycl" + item.Question.ChartQuestionID + @"plusminus').find('input').prop('checked', false);
                                 $('#cycl" + item.Question.ChartQuestionID + @"plusminus').find('input').prop('disabled', true);
                          }
                          else
                          {
                                 $('#cycl" + item.Question.ChartQuestionID + @"sph').attr('disabled', false);
                                 $('#DropDown" + item.Question.ChartQuestionID + @"sph').attr('disabled', false);
                                 $('#cycl" + item.Question.ChartQuestionID + @"cyl').attr('disabled', false);
                                 $('#DropDown" + item.Question.ChartQuestionID + @"cycl').attr('disabled', false);
                                 $('#cycl" + item.Question.ChartQuestionID + @"axis').attr('disabled', false);
                                 $('#cycl" + item.Question.ChartQuestionID + @"plusminus').find('input').prop('disabled', false);
                          }
                     $('#Cycl" + item.Question.ChartQuestionID + @"NA').click(function() {
                          if ($('#Cycl" + item.Question.ChartQuestionID + @"NA').is(':checked'))
                          {
                                 $('#cycl" + item.Question.ChartQuestionID + @"sph').attr('disabled', true);
                                 $('#cycl" + item.Question.ChartQuestionID + @"sph').val('');
                                 $('#DropDown" + item.Question.ChartQuestionID + @"sph').attr('disabled', true);
                                 $('#DropDown" + item.Question.ChartQuestionID + @"sph').val('.0');
                                 $('#cycl" + item.Question.ChartQuestionID + @"cyl').attr('disabled', true);
                                 $('#cycl" + item.Question.ChartQuestionID + @"cyl').val('');
                                 $('#DropDown" + item.Question.ChartQuestionID + @"cycl').attr('disabled', true);
                                 $('#DropDown" + item.Question.ChartQuestionID + @"cycl').val('.0');
                                 $('#cycl" + item.Question.ChartQuestionID + @"axis').attr('disabled', true);
                                 $('#cycl" + item.Question.ChartQuestionID + @"axis').val('');
                                 $('#cycl" + item.Question.ChartQuestionID + @"plusminus').find('input').prop('checked', false);
                                 $('#cycl" + item.Question.ChartQuestionID + @"plusminus').find('input').prop('disabled', true);
                          }
                          else
                          {
                                 $('#cycl" + item.Question.ChartQuestionID + @"sph').attr('disabled', false);
                                 $('#DropDown" + item.Question.ChartQuestionID + @"sph').attr('disabled', false);
                                 $('#cycl" + item.Question.ChartQuestionID + @"cyl').attr('disabled', false);
                                 $('#DropDown" + item.Question.ChartQuestionID + @"cycl').attr('disabled', false);
                                 $('#cycl" + item.Question.ChartQuestionID + @"axis').attr('disabled', false);
                                 $('#cycl" + item.Question.ChartQuestionID + @"plusminus').find('input').prop('disabled', false);
                          }
                      });
               });

        </script>";
                     ClientScript.RegisterClientScriptBlock(this.GetType(), "JSScript" + item.Question.ChartQuestionID, JSScript);
                     CyclTable.Controls.Add(PlusMinusTableRowWith);
                     CyclTable.Controls.Add(TextTableRowWith);
                     CyclTable.Controls.Add(CyclNARow);
                     wrap.Controls.Add(CyclTable);
                     wrap.Controls.Add(new LiteralControl("<br />"));
                     Label erm = new Label();
                     erm.ID = "ermes" + item.Question.ChartQuestionID;
                     erm.Text = item.Question.ErrorMessage;
                     erm.ForeColor = System.Drawing.Color.Red;
                     erm.Visible = false;
                     if (ErrorMesages.ContainsKey(item.Question.ChartQuestionID))
                     {
                         erm.Visible = true;
                     }
                     wrap.Controls.Add(new LiteralControl("<br />"));
                     wrap.Controls.Add(erm);
                     wrap.Controls.Add(new LiteralControl("<br />"));
                     cellanswertype.Controls.Add(wrap);
                 }
                 else if (item.Question.ChartQuestionTypeID == 17)
                 {
                     Panel wrap = new Panel();
                     wrap.ClientIDMode = ClientIDMode.Static;
                     wrap.ID = item.Question.ChartQuestionID + "-divq";
                     HtmlTable CyclTable = new HtmlTable();

                     HtmlTableRow RowWithSecSter = new HtmlTableRow();
                     TextBox sterbox = new TextBox();
                     sterbox.EnableViewState = false;
                     sterbox.ID = "ster" + item.Question.ChartQuestionID + "sec";
                     sterbox.ClientIDMode = ClientIDMode.Static;
                     sterbox.Attributes.Add("class", "Q-" + item.Question.ChartQuestionID + "sec");
                     FilteredTextBoxExtender txtfilterSecSter = new FilteredTextBoxExtender();
                     txtfilterSecSter.FilterType = FilterTypes.Custom;
                     txtfilterSecSter.ValidChars = "1234567890+-=/*().";
                     txtfilterSecSter.TargetControlID = "ster" + item.Question.ChartQuestionID + "sec";
                     txtfilterSecSter.ID = "ster" + item.Question.ChartQuestionID + "secfl";
                     HtmlTableCell tblcellsec = new HtmlTableCell();
                     tblcellsec.Controls.Add(sterbox);
                     tblcellsec.Controls.Add(txtfilterSecSter);
                    /// HtmlTableCell tblcellseclbl = new HtmlTableCell();
                     tblcellsec.Controls.Add(new LiteralControl { Text = "sec." });
                     RowWithSecSter.Controls.Add(tblcellsec);
                    // RowWithSecSter.Controls.Add(tblcellseclbl);
                     CyclTable.Controls.Add(RowWithSecSter);


                     HtmlTableRow RowWithUnSter = new HtmlTableRow();
                     CheckBox chkun = new CheckBox();
                     chkun.EnableViewState = false;
                     chkun.ID = "ster" + item.Question.ChartQuestionID + "un";
                     chkun.ClientIDMode = ClientIDMode.Static;
                     chkun.Attributes.Add("class", "Q-" + item.Question.ChartQuestionID + "un");
                     HtmlTableCell tblcellSter = new HtmlTableCell();
                     tblcellSter.Controls.Add(chkun);
                     tblcellSter.Controls.Add(new LiteralControl { Text = "Unable to perform or complete test" });
                     RowWithUnSter.Controls.Add(tblcellSter);
                     CyclTable.Controls.Add(RowWithUnSter);
                     string srctipt = @"<script type='text/javascript'>$(document).ready(function() {
                                      $('#" + "ster" + item.Question.ChartQuestionID + "un" + @"').click(function() {
                                         if ($('#" + "ster" + item.Question.ChartQuestionID + "un" + @"').is(':checked')) 
                                         {
                                            $('#" + "ster" + item.Question.ChartQuestionID + "sec" + @"').attr('disabled', true); 
                                            $('#" + "ster" + item.Question.ChartQuestionID + "sec" + @"').val('');  
                                         }
                                         else
                                         {
                                             $('#" + "ster" + item.Question.ChartQuestionID + "sec" + @"').removeAttr('disabled');
                                         }
                                      });
                                    });</script>";
                     ClientScript.RegisterClientScriptBlock(this.GetType(), "JSScript" + item.Question.ChartQuestionID, srctipt);

                     Label erm = new Label();
                     erm.ID = "ermes" + item.Question.ChartQuestionID;
                     erm.Text = item.Question.ErrorMessage;
                     erm.ForeColor = System.Drawing.Color.Red;
                     erm.Visible = false;
                     if (ErrorMesages.ContainsKey(item.Question.ChartQuestionID))
                     {
                         erm.Visible = true;
                     }

                     wrap.Controls.Add(new LiteralControl("<br />"));
                     wrap.Controls.Add(CyclTable);
                     wrap.Controls.Add(new LiteralControl("<br />"));
                     wrap.Controls.Add(erm);
                     cellanswertype.Controls.Add(wrap);
                 }


                 else if (item.Question.ChartQuestionTypeID == 18)
                 {
                    Panel wrap = new Panel();
                    wrap.ClientIDMode = ClientIDMode.Static;
                    wrap.ID = item.Question.ChartQuestionID + "-divq";
                    HtmlTable CyclTable = new HtmlTable();

                    HtmlTableRow RowWithOrthAlignment = new HtmlTableRow();
                    CheckBox chkorth = new CheckBox();
                    chkorth.EnableViewState = false;
                    chkorth.ID = "align" + item.Question.ChartQuestionID + "orth";
                    chkorth.ClientIDMode = ClientIDMode.Static;
                    chkorth.Attributes.Add("class", "Q-" + item.Question.ChartQuestionID + "orth");
                    HtmlTableCell tblcellOrth = new HtmlTableCell();
                    tblcellOrth.Controls.Add(chkorth);
                    tblcellOrth.Controls.Add(new LiteralControl { Text = "Orthophoria" });
                    RowWithOrthAlignment.Controls.Add(tblcellOrth);
                    CyclTable.Controls.Add(RowWithOrthAlignment);

                    HtmlTableRow RowWithAlignmentPD = new HtmlTableRow();
                    TextBox alighnpd = new TextBox();
                    alighnpd.EnableViewState = false;
                    alighnpd.ID = "alighn" + item.Question.ChartQuestionID + "pd";
                    alighnpd.ClientIDMode = ClientIDMode.Static;
                    alighnpd.Attributes.Add("class", "Q-" + item.Question.ChartQuestionID + "pd");
                alighnpd.Attributes.Add("style", "width:50px; margin-right:5px;");
                    FilteredTextBoxExtender txtfilteralighnpd = new FilteredTextBoxExtender();
                    txtfilteralighnpd.FilterType = FilterTypes.Custom;
                    txtfilteralighnpd.ValidChars = "1234567890+-=/*().";
                    txtfilteralighnpd.TargetControlID = "alighn" + item.Question.ChartQuestionID + "pd";
                    txtfilteralighnpd.ID = "alighn" + item.Question.ChartQuestionID + "pdfl";
                    HtmlTableCell tblcellpd = new HtmlTableCell();
                    tblcellpd.Controls.Add(alighnpd);
                    tblcellpd.Controls.Add(txtfilteralighnpd);
                tblcellpd.Controls.Add(new LiteralControl { Text = "PD" });
                //HtmlTableCell tblcellpdtext = new HtmlTableCell();
                //tblcellpdtext.Controls.Add(new LiteralControl { Text = "PD" });
                RowWithAlignmentPD.Controls.Add(tblcellpd);
                //RowWithAlignmentPD.Controls.Add(tblcellpdtext);
                CyclTable.Controls.Add(RowWithAlignmentPD);

                    ABORadioButtonList alignetxt = new ABORadioButtonList();
                    alignetxt.EnableViewState = false;
                    alignetxt.ClientIDMode = ClientIDMode.Static;
                    alignetxt.ID = "alighn" + item.Question.ChartQuestionID + "etxt";
                    ListItem chpet = new ListItem("ET", "True");
                    chpet.Attributes.Add("class", "Q-" + item.Question.ChartQuestionID + "etxt");
                    ListItem chpxt = new ListItem("XT", "False");
                    chpxt.Attributes.Add("class", "Q-" + item.Question.ChartQuestionID + "etxt");
                    alignetxt.RepeatDirection = System.Web.UI.WebControls.RepeatDirection.Horizontal;
                    alignetxt.Items.Add(chpet);
                    alignetxt.Items.Add(chpxt);

                    HtmlTableCell tblcellAlignmentETXTl = new HtmlTableCell();
                    tblcellAlignmentETXTl.Controls.Add(alignetxt);
                    RowWithAlignmentPD.Controls.Add(tblcellAlignmentETXTl);
                    CyclTable.Controls.Add(RowWithAlignmentPD);

                    HtmlTableRow RowOrthUnableToPerform = new HtmlTableRow();
                    CheckBox chkunable = new CheckBox();
                    chkunable.EnableViewState = false;
                    chkunable.ID = "align" + item.Question.ChartQuestionID + "unable";
                    chkunable.ClientIDMode = ClientIDMode.Static;
                    chkunable.Attributes.Add("class", "Q-" + item.Question.ChartQuestionID + "unable");
                    HtmlTableCell tblcellUnable = new HtmlTableCell();
                    tblcellUnable.Controls.Add(chkunable);
                    tblcellUnable.Controls.Add(new LiteralControl { Text = "Unable to Perform" });
                    RowOrthUnableToPerform.Controls.Add(tblcellUnable);
                    CyclTable.Controls.Add(RowOrthUnableToPerform);

                    wrap.Controls.Add(CyclTable);
                //javascript for question

string script = "<script type='text/javascript'>$(document).ready(function() { if ($('#" + "align" + item.Question.ChartQuestionID + "orth" + @"').is(':checked'))
{
$('#" + "alighn" + item.Question.ChartQuestionID + "pd" + @"').attr('disabled', true);
$('#" + "alighn" + item.Question.ChartQuestionID + "pd" + @"').val('');
$('." + "Q-" + item.Question.ChartQuestionID + "etxt" + @"').attr('disabled', true);
$('." + "Q-" + item.Question.ChartQuestionID + "etxt" + @"').attr('checked', false);
$('#" + "align" + item.Question.ChartQuestionID + "unable" + @"').attr('disabled', true);
$('#" + "align" + item.Question.ChartQuestionID + "unable" + @"').attr('checked', false);
}
else
{
$('#" + "alighn" + item.Question.ChartQuestionID + "pd" + @"').removeAttr('disabled');
$('." + "Q-" + item.Question.ChartQuestionID + "etxt" + @"').removeAttr('disabled');
$('#" + "align" + item.Question.ChartQuestionID + "unable" + @"').removeAttr('disabled');
}
$('#" + "align" + item.Question.ChartQuestionID + "orth" + @"').click(function() {
if ($('#" + "align" + item.Question.ChartQuestionID + "orth" + @"').is(':checked')) 
{
$('#" + "alighn" + item.Question.ChartQuestionID + "pd" + @"').attr('disabled', true);
$('#" + "alighn" + item.Question.ChartQuestionID + "pd" + @"').val('');
$('." + "Q-" + item.Question.ChartQuestionID + "etxt" + @"').attr('disabled', true);
$('." + "Q-" + item.Question.ChartQuestionID + "etxt" + @"').attr('checked', false);
$('#" + "align" + item.Question.ChartQuestionID + "unable" + @"').attr('disabled', true);
$('#" + "align" + item.Question.ChartQuestionID + "unable" + @"').attr('checked', false);
}
else
{
$('#" + "alighn" + item.Question.ChartQuestionID + "pd" + @"').removeAttr('disabled');
$('." + "Q-" + item.Question.ChartQuestionID + "etxt" + @"').removeAttr('disabled');
$('#" + "align" + item.Question.ChartQuestionID + "unable" + @"').removeAttr('disabled');
}
});

if ($('#" + "align" + item.Question.ChartQuestionID + "unable" + @"').is(':checked'))
{
$('#" + "alighn" + item.Question.ChartQuestionID + "pd" + @"').attr('disabled', true);
$('#" + "alighn" + item.Question.ChartQuestionID + "pd" + @"').val('');
$('." + "Q-" + item.Question.ChartQuestionID + "etxt" + @"').attr('disabled', true);
$('." + "Q-" + item.Question.ChartQuestionID + "etxt" + @"').attr('checked', false);
$('#" + "align" + item.Question.ChartQuestionID + "orth" + @"').attr('disabled', true);
$('#" + "align" + item.Question.ChartQuestionID + "orth" + @"').attr('checked', false);
}
else
{
$('#" + "alighn" + item.Question.ChartQuestionID + "pd" + @"').removeAttr('disabled');
$('." + "Q-" + item.Question.ChartQuestionID + "etxt" + @"').removeAttr('disabled');
$('#" + "align" + item.Question.ChartQuestionID + "orth" + @"').removeAttr('disabled');
}
$('#" + "align" + item.Question.ChartQuestionID + "unable" + @"').click(function() {
if ($('#" + "align" + item.Question.ChartQuestionID + "unable" + @"').is(':checked')) 
{
$('#" + "alighn" + item.Question.ChartQuestionID + "pd" + @"').attr('disabled', true);
$('#" + "alighn" + item.Question.ChartQuestionID + "pd" + @"').val('');
$('." + "Q-" + item.Question.ChartQuestionID + "etxt" + @"').attr('disabled', true);
$('." + "Q-" + item.Question.ChartQuestionID + "etxt" + @"').attr('checked', false);
$('#" + "align" + item.Question.ChartQuestionID + "orth" + @"').attr('disabled', true);
$('#" + "align" + item.Question.ChartQuestionID + "orth" + @"').attr('checked', false);
}
else
{
$('#" + "alighn" + item.Question.ChartQuestionID + "pd" + @"').removeAttr('disabled');
$('." + "Q-" + item.Question.ChartQuestionID + "etxt" + @"').removeAttr('disabled');
$('#" + "align" + item.Question.ChartQuestionID + "orth" + @"').removeAttr('disabled');
}
});
});</script>";
                // other stuff
                ClientScript.RegisterClientScriptBlock(this.GetType(), "JSScript" + item.Question.ChartQuestionID, script);
                Label erm = new Label();
                erm.ID = "ermes" + item.Question.ChartQuestionID;
                erm.Text = item.Question.ErrorMessage;
                erm.ForeColor = System.Drawing.Color.Red;
                erm.Visible = false;
                if (ErrorMesages.ContainsKey(item.Question.ChartQuestionID))
                {
                    erm.Visible = true;
                }
                //other stuff

                cellanswertype.Controls.Add(wrap);
                wrap.Controls.Add(erm);

            }
                 else if (item.Question.ChartQuestionTypeID == 19)
                 {
                     Panel wrap = new Panel();
                     wrap.ClientIDMode = ClientIDMode.Static;
                     wrap.ID = item.Question.ChartQuestionID + "-divq";
                     HtmlTable CyclTable = new HtmlTable();

                     HtmlTableRow RowKeratometryOD = new HtmlTableRow();
                     HtmlTableCell tblcellodtext = new HtmlTableCell();
                     tblcellodtext.Controls.Add(new LiteralControl { Text = "OD " });
                     RowKeratometryOD.Controls.Add(tblcellodtext);
                     HtmlTableCell tblcellod = new HtmlTableCell();
                     TextBox keratometryod = new TextBox();
                     keratometryod.EnableViewState = false;
                     keratometryod.ID = "keratometry" + item.Question.ChartQuestionID + "OD";
                     keratometryod.Attributes.Add("class", "Q-" + item.Question.ChartQuestionID + "OD");
                     keratometryod.ClientIDMode = ClientIDMode.Static;
                     MaskedEditExtender mseditOD = new MaskedEditExtender();
                     mseditOD.ID = "mseeOD" + item.Question.ChartQuestionID;
                     mseditOD.TargetControlID = "keratometry" + item.Question.ChartQuestionID + "OD";
                     mseditOD.Mask = "99.99/99.99/9999";
                     mseditOD.ClearMaskOnLostFocus = false;
                     tblcellod.Controls.Add(keratometryod);
                     tblcellod.Controls.Add(mseditOD);
                     RowKeratometryOD.Controls.Add(tblcellodtext);
                     RowKeratometryOD.Controls.Add(tblcellod);


                     HtmlTableRow RowKeratometryOS = new HtmlTableRow();
                     HtmlTableCell tblcellostext = new HtmlTableCell();
                     tblcellostext.Controls.Add(new LiteralControl { Text = "OS " });
                     RowKeratometryOS.Controls.Add(tblcellostext);
                     HtmlTableCell tblcellos = new HtmlTableCell();
                     TextBox keratometryos = new TextBox();
                     keratometryos.EnableViewState = false;
                     keratometryos.ID = "keratometry" + item.Question.ChartQuestionID + "OS";
                     keratometryos.Attributes.Add("class", "Q-" + item.Question.ChartQuestionID + "OS");
                     keratometryos.ClientIDMode = ClientIDMode.Static;

                     MaskedEditExtender mseditOS = new MaskedEditExtender();
                     mseditOS.ID = "mseeOS" + item.Question.ChartQuestionID;
                     mseditOS.TargetControlID = "keratometry" + item.Question.ChartQuestionID + "OS";
                     mseditOS.Mask = "99.99/99.99/9999";

                     mseditOS.ClearMaskOnLostFocus = false;
                     tblcellos.Controls.Add(keratometryos);
                     tblcellos.Controls.Add(mseditOS);
                     RowKeratometryOS.Controls.Add(tblcellostext);
                     RowKeratometryOS.Controls.Add(tblcellos);
                     CyclTable.Controls.Add(RowKeratometryOD);
                     CyclTable.Controls.Add(RowKeratometryOS);
                     wrap.Controls.Add(CyclTable);
                     wrap.Controls.Add(new LiteralControl("<br />"));

                     CheckBox chkeratometryna = new CheckBox();
                     chkeratometryna.EnableViewState = false;
                     chkeratometryna.ID = "keratometry" + item.Question.ChartQuestionID + "NA";
                     chkeratometryna.Text = "Not Done";
                     chkeratometryna.ClientIDMode = ClientIDMode.Static;
                     chkeratometryna.Attributes.Add("class", "Q-" + item.Question.ChartQuestionID + "NA");
                     wrap.Controls.Add(chkeratometryna);
                     string script = "<script type='text/javascript'>$(document).ready(function() { if ($('#" + "keratometry" + item.Question.ChartQuestionID + "NA" + @"').is(':checked'))
                                         {
                                            $('#" + "keratometry" + item.Question.ChartQuestionID + "OD" + @"').attr('disabled', true);
                                            $('#" + "keratometry" + item.Question.ChartQuestionID + "OD" + @"').val('');
                                            $('#" + "keratometry" + item.Question.ChartQuestionID + "OS" + @"').attr('disabled', true);
                                            $('#" + "keratometry" + item.Question.ChartQuestionID + "OS" + @"').val('');
                                         }
                                         else
                                         {
                                             $('#" + "keratometry" + item.Question.ChartQuestionID + "OD" + @"').removeAttr('disabled');
					                         $('#" + "keratometry" + item.Question.ChartQuestionID + "OS" + @"').removeAttr('disabled');
                                         }
                         $('#" + "keratometry" + item.Question.ChartQuestionID + "NA" + @"').click(function() {
                                         if ($('#" + "keratometry" + item.Question.ChartQuestionID + "NA" + @"').is(':checked')) 
                                         {
                                            $('#" + "keratometry" + item.Question.ChartQuestionID + "OD" + @"').attr('disabled', true); 
                                            $('#" + "keratometry" + item.Question.ChartQuestionID + "OD" + @"').val(''); 
                                            $('#" + "keratometry" + item.Question.ChartQuestionID + "OS" + @"').attr('disabled', true); 
                                            $('#" + "keratometry" + item.Question.ChartQuestionID + "OS" + @"').val('');
                                         }
                                         else
                                         {
                                             $('#" + "keratometry" + item.Question.ChartQuestionID + "OD" + @"').removeAttr('disabled');
					                         $('#" + "keratometry" + item.Question.ChartQuestionID + "OS" + @"').removeAttr('disabled');
                                         }
                                      });
                                    });</script>";
                     ClientScript.RegisterClientScriptBlock(this.GetType(), "JSScript" + item.Question.ChartQuestionID, script);
                     Label erm = new Label();
                     erm.ID = "ermes" + item.Question.ChartQuestionID;
                     erm.Text = item.Question.ErrorMessage;
                     erm.ForeColor = System.Drawing.Color.Red;
                     erm.Visible = false;
                     if (ErrorMesages.ContainsKey(item.Question.ChartQuestionID))
                     {
                         erm.Visible = true;
                     }
                     wrap.Controls.Add(new LiteralControl("<br />"));
                     cellanswertype.Controls.Add(wrap);
                     wrap.Controls.Add(new LiteralControl("<br />"));
                     wrap.Controls.Add(erm);
                 }
                 else if (item.Question.ChartQuestionTypeID == 20)
                 {
                     Panel wrap = new Panel();
                     wrap.ClientIDMode = ClientIDMode.Static;
                     wrap.ID = item.Question.ChartQuestionID + "-divq";
                     HtmlTable CyclTable = new HtmlTable();


                     HtmlTableRow RowComealthicknessOD1 = new HtmlTableRow();
                     HtmlTableCell tblcellcrtextod = new HtmlTableCell();
                     tblcellcrtextod.Controls.Add(new LiteralControl { Text = "OD " });
                     HtmlTableCell tblcellcruestionod = new HtmlTableCell();
                     TextBox Comealthicknessod = new TextBox();
                     Comealthicknessod.EnableViewState = false;
                     Comealthicknessod.ClientIDMode = ClientIDMode.Static;
                     Comealthicknessod.ID = "Comealthickness" + item.Question.ChartQuestionID + "OD";
                     Comealthicknessod.Attributes.Add("class", "Q-" + item.Question.ChartQuestionID + "OD");
                     tblcellcruestionod.Controls.Add(Comealthicknessod);
                     tblcellcruestionod.Controls.Add(new LiteralControl { Text = "&#181;" });

                     FilteredTextBoxExtender txtfilterComealOD = new FilteredTextBoxExtender();
                     txtfilterComealOD.FilterType = FilterTypes.Custom;
                     txtfilterComealOD.ValidChars = "1234567890+-=/*().";
                     txtfilterComealOD.TargetControlID = "Comealthickness" + item.Question.ChartQuestionID + "OD";
                     txtfilterComealOD.ID = "ComealthicknessFIL" + item.Question.ChartQuestionID + "OD";
                     tblcellcruestionod.Controls.Add(txtfilterComealOD);




                     HtmlTableCell tblcellcrnaod = new HtmlTableCell();
                     CheckBox Comealthicknessnaod = new CheckBox();
                     Comealthicknessnaod.EnableViewState = false;
                     Comealthicknessnaod.ID = "Comealthicknessod" + item.Question.ChartQuestionID + "NA";
                     Comealthicknessnaod.Text = "Not Done";
                     Comealthicknessnaod.Attributes.Add("class", "Q-" + item.Question.ChartQuestionID + "NA");
                     Comealthicknessnaod.ClientIDMode = ClientIDMode.Static;
                     tblcellcrnaod.Controls.Add(Comealthicknessnaod);
                     RowComealthicknessOD1.Controls.Add(tblcellcrtextod);
                     RowComealthicknessOD1.Controls.Add(tblcellcruestionod);
                     HtmlTableRow RowComealthicknessOD2 = new HtmlTableRow();
                     RowComealthicknessOD2.Controls.Add(tblcellcrnaod);
                     CyclTable.Controls.Add(RowComealthicknessOD1);
                     CyclTable.Controls.Add(RowComealthicknessOD2);

                     HtmlTableRow RowComealthicknessOS1 = new HtmlTableRow();
                     HtmlTableCell tblcellcrtextos = new HtmlTableCell();
                     tblcellcrtextos.Controls.Add(new LiteralControl { Text = "OS " });
                     HtmlTableCell tblcellcruestionos = new HtmlTableCell();
                     TextBox Comealthicknessos = new TextBox();
                     Comealthicknessos.EnableViewState = false;
                     Comealthicknessos.ID = "Comealthickness" + item.Question.ChartQuestionID + "OS";
                     Comealthicknessos.Attributes.Add("class", "Q-" + item.Question.ChartQuestionID + "OS");
                     Comealthicknessos.ClientIDMode = ClientIDMode.Static;
                     tblcellcruestionos.Controls.Add(Comealthicknessos);
                     tblcellcruestionos.Controls.Add(new LiteralControl { Text = "&#181;" });

                     FilteredTextBoxExtender txtfilterComealOS = new FilteredTextBoxExtender();
                     txtfilterComealOS.FilterType = FilterTypes.Custom;
                     txtfilterComealOS.ValidChars = "1234567890+-=/*().";
                     txtfilterComealOS.TargetControlID = "Comealthickness" + item.Question.ChartQuestionID + "OS";
                     txtfilterComealOS.ID = "ComealthicknessFIL" + item.Question.ChartQuestionID + "OS";
                     tblcellcruestionos.Controls.Add(txtfilterComealOS);


                     HtmlTableCell tblcellcrnaos = new HtmlTableCell();
                     CheckBox Comealthicknessnaos = new CheckBox();
                     Comealthicknessnaos.EnableViewState = false;
                     Comealthicknessnaos.ID = "Comealthicknessos" + item.Question.ChartQuestionID + "NA";
                     Comealthicknessnaos.Text = "Not Done";
                     Comealthicknessnaos.Attributes.Add("class", "Q-" + item.Question.ChartQuestionID + "NA");
                     Comealthicknessnaos.ClientIDMode = ClientIDMode.Static;
                     tblcellcrnaos.Controls.Add(Comealthicknessnaos);
                     RowComealthicknessOS1.Controls.Add(tblcellcrtextos);
                     RowComealthicknessOS1.Controls.Add(tblcellcruestionos);
                     HtmlTableRow RowComealthicknessOS2 = new HtmlTableRow();
                     RowComealthicknessOS2.Controls.Add(tblcellcrnaos);
                     CyclTable.Controls.Add(RowComealthicknessOS1);
                     CyclTable.Controls.Add(RowComealthicknessOS2);
                     wrap.Controls.Add(CyclTable);
                     string script = "<script type='text/javascript'>$(document).ready(function() { if ($('#" + "Comealthicknessod" + item.Question.ChartQuestionID + "NA" + @"').is(':checked')) 
                                         {
                                           
                                            $('#" + "Comealthickness" + item.Question.ChartQuestionID + "OD" + @"').attr('disabled', true); 
                                            $('#" + "Comealthickness" + item.Question.ChartQuestionID + "OD" + @"').val('');
                                         }
                                         else
                                         {
                                             $('#" + "Comealthickness" + item.Question.ChartQuestionID + "OD" + @"').removeAttr('disabled');
					   
                                         }
                                       if ($('#" + "Comealthicknessos" + item.Question.ChartQuestionID + "NA" + @"').is(':checked')) 
                                         {
                                           
                                            $('#" + "Comealthickness" + item.Question.ChartQuestionID + "OS" + @"').attr('disabled', true); 
                                            $('#" + "Comealthickness" + item.Question.ChartQuestionID + "OS" + @"').val('');
                                         }
                                         else
                                         {
                                             $('#" + "Comealthickness" + item.Question.ChartQuestionID + "OS" + @"').removeAttr('disabled');
					   
                                         }
                         $('#" + "Comealthicknessod" + item.Question.ChartQuestionID + "NA" + @"').click(function() {
                                         if ($('#" + "Comealthicknessod" + item.Question.ChartQuestionID + "NA" + @"').is(':checked')) 
                                         {
                                           
                                            $('#" + "Comealthickness" + item.Question.ChartQuestionID + "OD" + @"').attr('disabled', true); 
                                            $('#" + "Comealthickness" + item.Question.ChartQuestionID + "OD" + @"').val('');
                                         }
                                         else
                                         {
                                             $('#" + "Comealthickness" + item.Question.ChartQuestionID + "OD" + @"').removeAttr('disabled');
					   
                                         }
                                      });
              $('#" + "Comealthicknessos" + item.Question.ChartQuestionID + "NA" + @"').click(function() {
                                         if ($('#" + "Comealthicknessos" + item.Question.ChartQuestionID + "NA" + @"').is(':checked')) 
                                         {
                                           
                                            $('#" + "Comealthickness" + item.Question.ChartQuestionID + "OS" + @"').attr('disabled', true); 
                                            $('#" + "Comealthickness" + item.Question.ChartQuestionID + "OS" + @"').val('');
                                         }
                                         else
                                         {
                                             $('#" + "Comealthickness" + item.Question.ChartQuestionID + "OS" + @"').removeAttr('disabled');
					   
                                         }
                                      });
                                    });</script>";
                     ClientScript.RegisterClientScriptBlock(this.GetType(), "JSScript" + item.Question.ChartQuestionID, script);
                     Label erm = new Label();
                     erm.ID = "ermes" + item.Question.ChartQuestionID;
                     erm.Text = item.Question.ErrorMessage;
                     erm.ForeColor = System.Drawing.Color.Red;
                     erm.Visible = false;
                     if (ErrorMesages.ContainsKey(item.Question.ChartQuestionID))
                     {
                         erm.Visible = true;
                     }

                     wrap.Controls.Add(new LiteralControl("<br />"));
                     cellanswertype.Controls.Add(wrap);
                     wrap.Controls.Add(new LiteralControl("<br />"));
                     wrap.Controls.Add(erm);
                 }
                 else if (item.Question.ChartQuestionTypeID == 21)
                 {
                     Panel wrap = new Panel();
                     wrap.ClientIDMode = ClientIDMode.Static;
                     wrap.ID = item.Question.ChartQuestionID + "-divq";
                     HtmlTable CyclTable = new HtmlTable();

                     HtmlTableRow RowRefractionPlusMinus = new HtmlTableRow();
                     HtmlTableCell tblcellRefractionPlusMinus = new HtmlTableCell();
                     ABORadioButtonList rblRefractionPlusMinus = new ABORadioButtonList();
                     rblRefractionPlusMinus.EnableViewState = false;
                     rblRefractionPlusMinus.ID = "RefractionPlusMinus" + item.Question.ChartQuestionID + "plusminus";
                     ListItem plus = new ListItem("+", "True");
                     plus.Attributes.Add("class", "Q-" + item.Question.ChartQuestionID + "plus");
                     ListItem minus = new ListItem("-", "False");
                     minus.Attributes.Add("class", "Q-" + item.Question.ChartQuestionID + "minus");
                     minus.Attributes.Add("style", "margin-left: 10px;");
                     rblRefractionPlusMinus.ClientIDMode = ClientIDMode.Static;
                     rblRefractionPlusMinus.Items.Add(plus);
                     rblRefractionPlusMinus.Items.Add(minus);
                     rblRefractionPlusMinus.RepeatDirection = System.Web.UI.WebControls.RepeatDirection.Horizontal;
                     rblRefractionPlusMinus.Attributes.Add("class", "yesno");
                     tblcellRefractionPlusMinus.Controls.Add(rblRefractionPlusMinus);


                     HtmlTableRow RowRefractionText = new HtmlTableRow();
                     HtmlTableCell tblcellRefractionText = new HtmlTableCell();
                     TextBox textboxRefraction = new TextBox();
                     textboxRefraction.EnableViewState = false;
                     textboxRefraction.Width = 40;
                     textboxRefraction.ID = "Refraction" + item.Question.ChartQuestionID + "Text";
                     textboxRefraction.Attributes.Add("class", "Q-" + item.Question.ChartQuestionID + "Text");
                     textboxRefraction.ClientIDMode = ClientIDMode.Static;

                     string ScriptNA = @"if ($('#Refraction" + item.Question.ChartQuestionID + @"NA').is(':checked'))
                                            { 
                                        
                                              $('#RefractionPlusMinus" + item.Question.ChartQuestionID + "plusminus').find('input').prop('checked', false);                                               $('#RefractionPlusMinus" + item.Question.ChartQuestionID + "plusminus').find('input').prop('disabled', true);                                               $('#Refraction" + item.Question.ChartQuestionID + @"Text').val(''); 
                                              $('#Refraction" + item.Question.ChartQuestionID + @"Text').prop('disabled', true); 
                                              $('#Refraction" + item.Question.ChartQuestionID + @"DropDown').val('.0'); 
                                              $('#Refraction" + item.Question.ChartQuestionID + @"DropDown').prop('disabled', true); 
                                           } 
                                           else 
                                           { 
                                              $('#RefractionPlusMinus" + item.Question.ChartQuestionID + "plusminus').find('input').prop('disabled', false);                                              $('#Refraction" + item.Question.ChartQuestionID + @"Text').prop('disabled', false); 
                                              $('#Refraction" + item.Question.ChartQuestionID + @"DropDown').prop('disabled', false); 
                                           } 



                                         $('#Refraction" + item.Question.ChartQuestionID + @"NA').click(function()
                                         {  
                                            if ($('#Refraction" + item.Question.ChartQuestionID + @"NA').is(':checked'))
                                            { 
                                        
                                              $('#RefractionPlusMinus" + item.Question.ChartQuestionID + "plusminus').find('input').prop('checked', false);                                               $('#RefractionPlusMinus" + item.Question.ChartQuestionID + "plusminus').find('input').prop('disabled', true);                                               $('#Refraction" + item.Question.ChartQuestionID + @"Text').val(''); 
                                              $('#Refraction" + item.Question.ChartQuestionID + @"Text').prop('disabled', true); 
                                              $('#Refraction" + item.Question.ChartQuestionID + @"DropDown').val('.0'); 
                                              $('#Refraction" + item.Question.ChartQuestionID + @"DropDown').prop('disabled', true); 
                                           } 
                                           else 
                                           { 
                                              $('#RefractionPlusMinus" + item.Question.ChartQuestionID + "plusminus').find('input').prop('disabled', false);                                              $('#Refraction" + item.Question.ChartQuestionID + @"Text').prop('disabled', false); 
                                              $('#Refraction" + item.Question.ChartQuestionID + @"DropDown').prop('disabled', false); 
                                           } 
                                    });
                               
                                   $('#Refraction" + item.Question.ChartQuestionID + @"DropDown').change(function() {
                                         if($('#Refraction" + item.Question.ChartQuestionID + @"Text').val().replace('__', '0').replace('_', '')+$('#Refraction179DropDown').val()=='0.0')
                                      {
                                              $('#RefractionPlusMinus" + item.Question.ChartQuestionID + @"plusminus').find('input').prop('checked',false);
                                              $('#RefractionPlusMinus" + item.Question.ChartQuestionID + @"plusminus').find('input').prop('disabled', true);
                                      }
                                      else
                                      {
                                             $('#RefractionPlusMinus" + item.Question.ChartQuestionID + @"plusminus').find('input').prop('disabled', false);
                                      }
                                   });";

                     string script = @"<script type='text/javascript'>
                                            $(document).ready(function() {
                                            $('#Refraction" + item.Question.ChartQuestionID + @"Text').keyup(function() {if(parseInt($(this).val())>=10)                                                 {  
                                                  alert('The value you entered is very high');
                                                  
                                                     
                                             } }); " + ScriptNA + " });</script>";

                     ClientScript.RegisterClientScriptBlock(this.GetType(), "JSScript" + item.Question.ChartQuestionID, script);

                     MaskedEditExtender mseditRefractionText = new MaskedEditExtender();
                     mseditRefractionText.ID = "mseeRefraction" + item.Question.ChartQuestionID;
                     mseditRefractionText.TargetControlID = "Refraction" + item.Question.ChartQuestionID + "Text";
                     mseditRefractionText.Mask = "99";
                     mseditRefractionText.ClearMaskOnLostFocus = false;

                     DropDownList RefractionDropdownlist = new DropDownList();
                     RefractionDropdownlist.EnableViewState = false;
                     RefractionDropdownlist.ID = "Refraction" + item.Question.ChartQuestionID + "DropDown";
                     RefractionDropdownlist.Attributes.Add("class", "Q-" + item.Question.ChartQuestionID + "DropDown");
                     RefractionDropdownlist.ClientIDMode = ClientIDMode.Static;
                     string[] values = { ".0", ".25", ".5", ".75" };
                     foreach (var val in values)
                     {
                         RefractionDropdownlist.Items.Add(new ListItem(val, val));
                     }


                     HtmlTableRow RowRefractionNA = new HtmlTableRow();
                     HtmlTableCell tblcellRefractionNA = new HtmlTableCell();
                     CheckBox ChkRefraction = new CheckBox();
                     ChkRefraction.EnableViewState = false;
                     ChkRefraction.ID = "Refraction" + item.Question.ChartQuestionID + "NA";
                     ChkRefraction.Attributes.Add("class", "Q-" + item.Question.ChartQuestionID + "NA");
                     ChkRefraction.ClientIDMode = ClientIDMode.Static;
                     ChkRefraction.Text = "Not Available";
                     tblcellRefractionNA.Controls.Add(new LiteralControl("<br />"));
                     tblcellRefractionNA.Controls.Add(ChkRefraction);



                     tblcellRefractionText.Controls.Add(mseditRefractionText);
                     tblcellRefractionText.Controls.Add(textboxRefraction);
                     tblcellRefractionText.Controls.Add(new LiteralControl("   "));
                     tblcellRefractionText.Controls.Add(RefractionDropdownlist);
                     tblcellRefractionText.Controls.Add(new LiteralControl(" d"));
                     RowRefractionText.Controls.Add(tblcellRefractionPlusMinus);
                     RowRefractionText.Controls.Add(tblcellRefractionText);

                     RowRefractionNA.Controls.Add(tblcellRefractionNA);
                     CyclTable.Controls.Add(RowRefractionPlusMinus);
                     CyclTable.Controls.Add(RowRefractionText);
                     CyclTable.Controls.Add(RowRefractionNA);
                     wrap.Controls.Add(CyclTable);
                     wrap.Controls.Add(new LiteralControl("<br />"));





                     Label erm = new Label();
                     erm.ID = "ermes" + item.Question.ChartQuestionID;
                     erm.Text = item.Question.ErrorMessage;
                     erm.ForeColor = System.Drawing.Color.Red;
                     erm.Visible = false;
                     if (ErrorMesages.ContainsKey(item.Question.ChartQuestionID))
                     {
                         erm.Visible = true;
                     }
                     wrap.Controls.Add(new LiteralControl("<br />"));
                     wrap.Controls.Add(erm);
                     wrap.Controls.Add(new LiteralControl("<br />"));
                     cellanswertype.Controls.Add(wrap);

                 }
                 else if (item.Question.ChartQuestionTypeID == 22)
                 {
                     Panel wrap = new Panel();
                     wrap.ClientIDMode = ClientIDMode.Static;
                     wrap.ID = item.Question.ChartQuestionID + "-divq";
                     HtmlTable EsotropiaPrimaryGazeTable = new HtmlTable();
                     HtmlTableRow RowEsotropiaPrimaryGaze = new HtmlTableRow();
                     HtmlTableCell tblcellEsotropiaPrimaryGaze = new HtmlTableCell();
                     TextBox EsotropiaPrimaryGazeText = new TextBox();
                     EsotropiaPrimaryGazeText.EnableViewState=false;
                     EsotropiaPrimaryGazeText.Width = 40;
                     EsotropiaPrimaryGazeText.ID = "EsotropiaPrimaryGaze" + item.Question.ChartQuestionID + "Text";
                     EsotropiaPrimaryGazeText.Attributes.Add("class", "Q-" + item.Question.ChartQuestionID + "Text");
                     EsotropiaPrimaryGazeText.ClientIDMode = ClientIDMode.Static;
                     MaskedEditExtender mseditRefractionText = new MaskedEditExtender();
                     mseditRefractionText.ID = "mseeEsotropiaPrimaryGaze" + item.Question.ChartQuestionID;
                     mseditRefractionText.TargetControlID = "EsotropiaPrimaryGaze" + item.Question.ChartQuestionID + "Text";
                     mseditRefractionText.Mask = "99";
                     mseditRefractionText.ClearMaskOnLostFocus = false;
                     tblcellEsotropiaPrimaryGaze.Controls.Add(EsotropiaPrimaryGazeText);
                     tblcellEsotropiaPrimaryGaze.Controls.Add(mseditRefractionText);
                     tblcellEsotropiaPrimaryGaze.Controls.Add(new LiteralControl(" PD"));
                     RowEsotropiaPrimaryGaze.Controls.Add(tblcellEsotropiaPrimaryGaze);
                     EsotropiaPrimaryGazeTable.Controls.Add(RowEsotropiaPrimaryGaze);

                     HtmlTableRow RowEsotropiaPrimaryGazeNA = new HtmlTableRow();
                     HtmlTableCell tblcellEsotropiaPrimaryGazeNA = new HtmlTableCell();
                     CheckBox CheckBoxEsotropiaPrimaryGazeNA = new CheckBox();
                     CheckBoxEsotropiaPrimaryGazeNA.EnableViewState=false;
                     CheckBoxEsotropiaPrimaryGazeNA.ID = "EsotropiaPrimaryGaze" + item.Question.ChartQuestionID + "NA";
                     CheckBoxEsotropiaPrimaryGazeNA.Attributes.Add("class", "Q-" + item.Question.ChartQuestionID + "NA");
                     CheckBoxEsotropiaPrimaryGazeNA.ClientIDMode = ClientIDMode.Static;
                     CheckBoxEsotropiaPrimaryGazeNA.Text = "Not Available";
                     tblcellEsotropiaPrimaryGazeNA.Controls.Add(new LiteralControl("<br />"));
                     tblcellEsotropiaPrimaryGazeNA.Controls.Add(CheckBoxEsotropiaPrimaryGazeNA);
                     RowEsotropiaPrimaryGazeNA.Controls.Add(tblcellEsotropiaPrimaryGazeNA);
                     EsotropiaPrimaryGazeTable.Controls.Add(RowEsotropiaPrimaryGazeNA);
                     wrap.Controls.Add(EsotropiaPrimaryGazeTable);
                     wrap.Controls.Add(new LiteralControl("<br />"));


                     string script = @"<script type='text/javascript'>$(document).ready(function() {
                                           if ($('#EsotropiaPrimaryGaze" + item.Question.ChartQuestionID + @"NA').is(':checked'))
                                            { 
                                              $('#EsotropiaPrimaryGaze" + item.Question.ChartQuestionID + @"Text').val(''); 
                                              $('#EsotropiaPrimaryGaze" + item.Question.ChartQuestionID + @"Text').prop('disabled', true); 
      
                                           } 
                                           else 
                                           { 
                                               $('#EsotropiaPrimaryGaze" + item.Question.ChartQuestionID + @"Text').prop('disabled', false); 
                    
                                           } 



                                         $('#EsotropiaPrimaryGaze" + item.Question.ChartQuestionID + @"NA').click(function()
                                         {  
                                            if ($('#EsotropiaPrimaryGaze" + item.Question.ChartQuestionID + @"NA').is(':checked'))
                                            { 
                                        
                                              $('#EsotropiaPrimaryGaze" + item.Question.ChartQuestionID + @"Text').val(''); 
                                              $('#EsotropiaPrimaryGaze" + item.Question.ChartQuestionID + @"Text').prop('disabled', true); 
                                           } 
                                           else 
                                           { 
                                              $('#EsotropiaPrimaryGaze" + item.Question.ChartQuestionID + @"Text').val(''); 
                                              $('#EsotropiaPrimaryGaze" + item.Question.ChartQuestionID + @"Text').prop('disabled', false);  
                                           } 
                                    }); });</script>";
                     ClientScript.RegisterClientScriptBlock(this.GetType(), "JSScript" + item.Question.ChartQuestionID, script);

                     Label erm = new Label();
                     erm.ID = "ermes" + item.Question.ChartQuestionID;
                     erm.Text = item.Question.ErrorMessage;
                     erm.ForeColor = System.Drawing.Color.Red;
                     erm.Visible = false;
                     if (ErrorMesages.ContainsKey(item.Question.ChartQuestionID))
                     {
                         erm.Visible = true;
                     }
                     wrap.Controls.Add(new LiteralControl("<br />"));
                     wrap.Controls.Add(erm);
                     wrap.Controls.Add(new LiteralControl("<br />"));
                     cellanswertype.Controls.Add(wrap);
                 }
                 else if (item.Question.ChartQuestionTypeID == 23)
                 {
                     Panel wrap = new Panel();
                     wrap.ClientIDMode = ClientIDMode.Static;
                     wrap.ID = item.Question.ChartQuestionID + "-divq";
                     HtmlTable IOPValueTable = new HtmlTable();
                     HtmlTableRow RowIOPValue = new HtmlTableRow();
                     HtmlTableCell tblcellIOPValue = new HtmlTableCell();
                     TextBox IOPValueText = new TextBox();
                     IOPValueText.EnableViewState=false;
                     IOPValueText.Width = 40;
                     IOPValueText.ID = "IOPValue" + item.Question.ChartQuestionID + "Text";
                     IOPValueText.Attributes.Add("class", "Q-" + item.Question.ChartQuestionID + "Text");
                     IOPValueText.ClientIDMode = ClientIDMode.Static;
                     MaskedEditExtender mseditRefractionText = new MaskedEditExtender();
                     mseditRefractionText.ID = "mseeIOPValue" + item.Question.ChartQuestionID;
                     mseditRefractionText.TargetControlID = "IOPValue" + item.Question.ChartQuestionID + "Text";
                     mseditRefractionText.Mask = "99";
                     mseditRefractionText.ClearMaskOnLostFocus = false;
                     tblcellIOPValue.Controls.Add(IOPValueText);
                     tblcellIOPValue.Controls.Add(mseditRefractionText);
                     tblcellIOPValue.Controls.Add(new LiteralControl(" mmHg"));
                     RowIOPValue.Controls.Add(tblcellIOPValue);
                     IOPValueTable.Controls.Add(RowIOPValue);

                     HtmlTableRow RowIOPValueNA = new HtmlTableRow();
                     HtmlTableCell tblcellIOPValueNA = new HtmlTableCell();




                     ABORadioButtonList RadioButtonListIOPValueNA = new ABORadioButtonList();
                     RadioButtonListIOPValueNA.EnableViewState=false;
                     RadioButtonListIOPValueNA.ID = "IOPValue" + item.Question.ChartQuestionID + "NA";
                     RadioButtonListIOPValueNA.Attributes.Add("class", "Q-" + item.Question.ChartQuestionID + "NA");
                     RadioButtonListIOPValueNA.ClientIDMode = ClientIDMode.Static;


                     ListItem NotRecorded = new ListItem("Not Recorded", "385");
                     NotRecorded.Attributes.Add("class", "Q-" + item.Question.ChartQuestionID);
                     ListItem UnabletoPerform = new ListItem("Unable to Perform", "386");
                     UnabletoPerform.Attributes.Add("class", "Q-" + item.Question.ChartQuestionID);

                     RadioButtonListIOPValueNA.Items.Add(NotRecorded);
                     RadioButtonListIOPValueNA.Items.Add(UnabletoPerform);
                     tblcellIOPValueNA.Controls.Add(new LiteralControl("<br />"));
                     tblcellIOPValueNA.Controls.Add(RadioButtonListIOPValueNA);
                     RowIOPValueNA.Controls.Add(tblcellIOPValueNA);
                     IOPValueTable.Controls.Add(RowIOPValueNA);
                     wrap.Controls.Add(IOPValueTable);
                     wrap.Controls.Add(new LiteralControl("<br />"));


                     string script = @"<script language='JavaScript' type='text/javascript'>
                                                    $(document).ready(function() { 
                                                               $('.Q-" + item.Question.ChartQuestionID + @"').click(function() {
                                                                       $('#IOPValue" + item.Question.ChartQuestionID + @"Text').val(''); 
                                                                }); 
                                                              $('#IOPValue" + item.Question.ChartQuestionID + @"Text').keyup(function() 
                                                                { 
                                                                      $('.Q-" + item.Question.ChartQuestionID + @"').prop('checked', false);  
                                                                }); 
                                                    }); </script>";
                     ClientScript.RegisterClientScriptBlock(this.GetType(), "JSScript" + item.Question.ChartQuestionID, script);

                     Label erm = new Label();
                     erm.ID = "ermes" + item.Question.ChartQuestionID;
                     erm.Text = item.Question.ErrorMessage;
                     erm.ForeColor = System.Drawing.Color.Red;
                     erm.Visible = false;
                     if (ErrorMesages.ContainsKey(item.Question.ChartQuestionID))
                     {
                         erm.Visible = true;
                     }
                     wrap.Controls.Add(new LiteralControl("<br />"));
                     wrap.Controls.Add(erm);
                     wrap.Controls.Add(new LiteralControl("<br />"));
                     cellanswertype.Controls.Add(wrap);
                 }
                 else if (item.Question.ChartQuestionTypeID == 26)
                 { 
                     Panel wrap = new Panel();
                     wrap.ClientIDMode = ClientIDMode.Static;
                     wrap.ID = item.Question.ChartQuestionID + "-divq";
                     HtmlTable CyclTable = new HtmlTable();
                     HtmlTableRow RowMeanDeviationPlusMinus = new HtmlTableRow();
                     HtmlTableCell tblcellMeanDeviationPlusMinus = new HtmlTableCell();
                     ABORadioButtonList rblMeanDeviationPlusMinus = new ABORadioButtonList();
                     rblMeanDeviationPlusMinus.EnableViewState=false;
                     rblMeanDeviationPlusMinus.ID = "MeanDeviationPlusMinus" + item.Question.ChartQuestionID+"plusminus";
                     ListItem plus = new ListItem(" + ", "True");
                     plus.Attributes.Add("class", "Q-" + item.Question.ChartQuestionID+"plus");
                     ListItem minus = new ListItem(" - ", "False");
                     minus.Attributes.Add("class", "Q-" + item.Question.ChartQuestionID + "minus");
                     rblMeanDeviationPlusMinus.ClientIDMode = ClientIDMode.Static;
                     rblMeanDeviationPlusMinus.Items.Add(plus);
                     rblMeanDeviationPlusMinus.Items.Add(minus);
                     rblMeanDeviationPlusMinus.RepeatDirection = System.Web.UI.WebControls.RepeatDirection.Horizontal;
                     rblMeanDeviationPlusMinus.Attributes.Add("class", "yesno");
                     tblcellMeanDeviationPlusMinus.Controls.Add(rblMeanDeviationPlusMinus);


                     HtmlTableRow RowMeanDeviationText = new HtmlTableRow();
                     HtmlTableCell tblcellMeanDeviationText = new HtmlTableCell();
                     TextBox textboxMeanDeviation = new TextBox();
                     textboxMeanDeviation.EnableViewState=false;
                     textboxMeanDeviation.Width = 40;
                     textboxMeanDeviation.ID = "MeanDeviation" + item.Question.ChartQuestionID + "Text";
                     textboxMeanDeviation.Attributes.Add("class", "Q-" + item.Question.ChartQuestionID + "Text");
                     textboxMeanDeviation.ClientIDMode = ClientIDMode.Static;

                     string ScriptNA = @"if ($('#MeanDeviation" + item.Question.ChartQuestionID + @"NA').is(':checked'))
                                            { 
                                        
                                              $('#MeanDeviationPlusMinus" + item.Question.ChartQuestionID + "plusminus').find('input').prop('checked', false);                                               $('#MeanDeviationPlusMinus" + item.Question.ChartQuestionID + "plusminus').find('input').prop('disabled', true);                                               $('#MeanDeviation" + item.Question.ChartQuestionID + @"Text').val(''); 
                                              $('#MeanDeviation" + item.Question.ChartQuestionID + @"Text').prop('disabled', true); 
                                              $('#MeanDeviation" + item.Question.ChartQuestionID + @"DropDown').val('.0'); 
                                              $('#MeanDeviation" + item.Question.ChartQuestionID + @"DropDown').prop('disabled', true); 
                                           } 
                                           else 
                                           { 
                                              $('#MeanDeviationPlusMinus" + item.Question.ChartQuestionID + "plusminus').find('input').prop('disabled', false);                                              $('#MeanDeviation" + item.Question.ChartQuestionID + @"Text').prop('disabled', false); 
                                              $('#MeanDeviation" + item.Question.ChartQuestionID + @"DropDown').prop('disabled', false); 
                                           } 



                                         $('#MeanDeviation" + item.Question.ChartQuestionID + @"NA').click(function()
                                         {  
                                            if ($('#MeanDeviation" + item.Question.ChartQuestionID + @"NA').is(':checked'))
                                            { 
                                        
                                              $('#MeanDeviationPlusMinus" + item.Question.ChartQuestionID + "plusminus').find('input').prop('checked', false);                                               $('#MeanDeviationPlusMinus" + item.Question.ChartQuestionID + "plusminus').find('input').prop('disabled', true);                                               $('#MeanDeviation" + item.Question.ChartQuestionID + @"Text').val(''); 
                                              $('#MeanDeviation" + item.Question.ChartQuestionID + @"Text').prop('disabled', true); 
                                              $('#MeanDeviation" + item.Question.ChartQuestionID + @"DropDown').val('.0'); 
                                              $('#MeanDeviation" + item.Question.ChartQuestionID + @"DropDown').prop('disabled', true); 
                                           } 
                                           else 
                                           { 
                                              $('#MeanDeviationPlusMinus" + item.Question.ChartQuestionID + "plusminus').find('input').prop('disabled', false);                                              $('#MeanDeviation" + item.Question.ChartQuestionID + @"Text').prop('disabled', false); 
                                              $('#MeanDeviation" + item.Question.ChartQuestionID + @"DropDown').prop('disabled', false); 
                                           } 
                                    });";

                     string script = @"<script type='text/javascript'>
                                            $(document).ready(function() {
                                            $('#MeanDeviation" + item.Question.ChartQuestionID + @"Text').keyup(function() {if(parseInt($(this).val())>=10)                                                 {  
                                                  alert('The value you entered is very high');
                                                  
                                                     
                                             } }); "+ScriptNA+" });</script>";

                     ClientScript.RegisterClientScriptBlock(this.GetType(), "JSScript"+item.Question.ChartQuestionID, script);

                     MaskedEditExtender mseditMeanDeviationText = new MaskedEditExtender();
                     mseditMeanDeviationText.ID = "mseeMeanDeviation" + item.Question.ChartQuestionID;
                     mseditMeanDeviationText.TargetControlID = "MeanDeviation" + item.Question.ChartQuestionID + "Text";
                     mseditMeanDeviationText.Mask = "99";
                     mseditMeanDeviationText.ClearMaskOnLostFocus = false;

                     DropDownList MeanDeviationDropdownlist = new DropDownList();
                     MeanDeviationDropdownlist.EnableViewState=false;
                     MeanDeviationDropdownlist.ID = "MeanDeviation" + item.Question.ChartQuestionID + "DropDown";
                     MeanDeviationDropdownlist.Attributes.Add("class", "Q-" + item.Question.ChartQuestionID + "DropDown");
                     MeanDeviationDropdownlist.ClientIDMode = ClientIDMode.Static;
                     string[] values={ ".0", ".25", ".5", ".75"};
                     foreach (var val in values)
                     {
                         MeanDeviationDropdownlist.Items.Add(new ListItem(val, val));
                     }
          

                     HtmlTableRow RowMeanDeviationNA = new HtmlTableRow();
                     HtmlTableCell tblcellMeanDeviationNA = new HtmlTableCell();
                     CheckBox ChkMeanDeviation = new CheckBox();
                     ChkMeanDeviation.EnableViewState=false;
                     ChkMeanDeviation.ID = "MeanDeviation" + item.Question.ChartQuestionID + "NA";
                     ChkMeanDeviation.Attributes.Add("class", "Q-" + item.Question.ChartQuestionID + "NA");
                     ChkMeanDeviation.ClientIDMode = ClientIDMode.Static;
                     ChkMeanDeviation.Text = "Not Applicable";
                     tblcellMeanDeviationNA.Controls.Add(new LiteralControl("<br />"));
                     tblcellMeanDeviationNA.Controls.Add(ChkMeanDeviation);



                     tblcellMeanDeviationText.Controls.Add(mseditMeanDeviationText);
                     tblcellMeanDeviationText.Controls.Add(textboxMeanDeviation);
                     tblcellMeanDeviationText.Controls.Add(new LiteralControl(" . "));
                     tblcellMeanDeviationText.Controls.Add(MeanDeviationDropdownlist);
                     tblcellMeanDeviationText.Controls.Add(new LiteralControl(" dB"));
                     RowMeanDeviationText.Controls.Add(tblcellMeanDeviationPlusMinus);
                     RowMeanDeviationText.Controls.Add(tblcellMeanDeviationText);

                     RowMeanDeviationNA.Controls.Add(tblcellMeanDeviationNA);
                     CyclTable.Controls.Add(RowMeanDeviationPlusMinus);
                     CyclTable.Controls.Add(RowMeanDeviationText);
                     CyclTable.Controls.Add(RowMeanDeviationNA);
                     wrap.Controls.Add(CyclTable);
                     wrap.Controls.Add(new LiteralControl("<br />"));
                     
                     

                     
                     
                     Label erm = new Label();
                     erm.ID = "ermes" + item.Question.ChartQuestionID;
                     erm.Text = item.Question.ErrorMessage;
                     erm.ForeColor = System.Drawing.Color.Red;
                     erm.Visible = false;
                     if (ErrorMesages.ContainsKey(item.Question.ChartQuestionID))
                     {
                         erm.Visible = true;
                     }
                     wrap.Controls.Add(new LiteralControl("<br />"));
                     wrap.Controls.Add(erm);
                     wrap.Controls.Add(new LiteralControl("<br />"));
                     cellanswertype.Controls.Add(wrap);
                   
                 }
                 else if (item.Question.ChartQuestionTypeID == 27)
                 {
                     Panel wrap = new Panel();
                     wrap.ClientIDMode = ClientIDMode.Static;
                     wrap.ID = item.Question.ChartQuestionID + "-divq";
                     HtmlTable FullDateTable = new HtmlTable();
                     HtmlTableRow RowDate = new HtmlTableRow();
                     HtmlTableCell tblcelmonth = new HtmlTableCell();
                     DropDownList ddldatemonth = new DropDownList();
                     ddldatemonth.EnableViewState=false;
                     ddldatemonth.ID = "dateboxmonth" + item.Question.ChartQuestionID+"fulldate";
                     for (int g = 1; g <= 12; g++)
                     {
                         var FinalDate = g.ToString();
                         if (g.ToString().Length == 1)
                         {
                             FinalDate = "0" + g.ToString();
                         }
                         ddldatemonth.Items.Add(new ListItem(FinalDate, FinalDate));
                     }
                     ddldatemonth.Items.Insert(0, new ListItem("---", String.Empty));
                     ddldatemonth.Attributes.Add("class", "Q-" + item.Question.ChartQuestionID);
                     tblcelmonth.Controls.Add(ddldatemonth);
                     RowDate.Controls.Add(tblcelmonth);

                     HtmlTableCell tblcelday = new HtmlTableCell();
                     DropDownList ddldateday = new DropDownList();
                     ddldateday.EnableViewState=false;
                     ddldateday.ID = "dateboxday" + item.Question.ChartQuestionID + "fulldate";
                     for (int g = 1; g <= 31; g++)
                     {
                         var FinalDate = g.ToString();
                         if (g.ToString().Length == 1)
                         {
                             FinalDate = "0" + g.ToString();
                         }
                         ddldateday.Items.Add(new ListItem(FinalDate, FinalDate));
                     }
                     ddldateday.Items.Insert(0, new ListItem("---", String.Empty));
                     ddldateday.Attributes.Add("class", "Q-" + item.Question.ChartQuestionID);
                     tblcelday.Controls.Add(ddldateday);
                     RowDate.Controls.Add(tblcelday);

                     HtmlTableCell tblcelyear= new HtmlTableCell();
                     DropDownList ddldatedate = new DropDownList();
                     ddldatedate.EnableViewState=false;
                     ddldatedate.ID = "ddldatedate" + item.Question.ChartQuestionID + "fulldate";
                     for (int g = 2018; g >= 1900; g--)
                     {
                         ddldatedate.Items.Add(new ListItem(g.ToString(), g.ToString()));
                     }
                     ddldatedate.Items.Insert(0, new ListItem("---", String.Empty));
                     ddldatedate.Attributes.Add("class", "Q-" + item.Question.ChartQuestionID);
                     tblcelyear.Controls.Add(ddldatedate);
                     RowDate.Controls.Add(tblcelyear);
                     FullDateTable.Controls.Add(RowDate);
                     wrap.Controls.Add(FullDateTable);
                     wrap.Controls.Add(new LiteralControl("<br />"));

                     Label erm = new Label();
                     erm.ID = "ermes" + item.Question.ChartQuestionID;
                     erm.Text = item.Question.ErrorMessage;
                     erm.ForeColor = System.Drawing.Color.Red;
                     erm.Visible = false;
                     if (ErrorMesages.ContainsKey(item.Question.ChartQuestionID))
                     {
                         erm.Visible = true;
                     }
                     wrap.Controls.Add(new LiteralControl("<br />"));
                     wrap.Controls.Add(erm);
                     wrap.Controls.Add(new LiteralControl("<br />"));
                     cellanswertype.Controls.Add(wrap);
                 }
                 row.Controls.Add(cellanswertype);
                 table.Controls.Add(row);
                 if (item.Question.IsGrouped != true)
                 {
                     l++;
                 }
             }
             pl.Controls.Add(table);
             return pl;
            
    }


    public void FillUpAnswers(ChartAbstractionViewModel question)
    {

        foreach (var item in question.QuestionsAndAnswers)
        {
            if (item.UserReply != null)
            {
                if (item.Question.ChartQuestionTypeID == 1 || item.Question.ChartQuestionTypeID == 10)
                {
                    var Control = (RadioButtonList)PanelContent.FindControl("yesno" + item.Question.ChartQuestionID);
                    if (item.UserReply.TrueFalseResponse != null)
                    {
                        Control.Items.FindByValue(item.UserReply.TrueFalseResponse.ToString()).Selected = true;
                    }
                }
                else if (item.Question.ChartQuestionTypeID == 2)
                {
                    var Control = (TextBox)PanelContent.FindControl("singletext" + item.Question.ChartQuestionID);
                    Control.Text = item.UserReply.ShortTextResponse;
                }

                else if (item.Question.ChartQuestionTypeID == 3)
                {
                    var Control = (RadioButtonList)PanelContent.FindControl("radiolist" + item.Question.ChartQuestionID);
                    if (item.UserReply.ChaoiceID != null)
                    {
                        Control.Items.FindByValue(item.UserReply.ChaoiceID.ToString()).Selected = true;
                    }
                    
                }

                else if (item.Question.ChartQuestionTypeID == 4)
                {
                    var Control = (CheckBoxList)PanelContent.FindControl("chklist" + item.Question.ChartQuestionID);
                    foreach (var choices in item.UserReply.Choices)
                    {
                        if (choices.ChartChoiceID != null)
                        {
                            Control.Items.FindByValue(choices.ChartChoiceID.ToString()).Selected = true;
                        }
                    }

                }
                else if (item.Question.ChartQuestionTypeID == 5)
                {
                    var Control = (TextBox)PanelContent.FindControl("numberbox" + item.Question.ChartQuestionID);
                    Control.Text = item.UserReply.NumericResponse.ToString();

                }
                else if (item.Question.ChartQuestionTypeID == 7)
                {
                    var Control = (RadioButtonList)PanelContent.FindControl("radiolistrating" + item.Question.ChartQuestionID);
                    if (item.UserReply.RatingReply != null)
                    {
                        Control.Items.FindByValue(item.UserReply.RatingReply.ToString()).Selected = true;
                    }
                }
                else if (item.Question.ChartQuestionTypeID == 8)
                {
                    var ControlMonth = (DropDownList)PanelContent.FindControl("dateboxmonth" + item.Question.ChartQuestionID);
                    var ControlDate = (DropDownList)PanelContent.FindControl("ddldatedate" + item.Question.ChartQuestionID);
                    if (item.UserReply.DateResponse != null)
                    {
                        try
                        {
                            var GivenDate = item.UserReply.DateResponse.Value.ToString("MM/dd/yyyy").Split('/');
                            ControlMonth.Items.FindByValue(GivenDate[0].Trim()).Selected = true;
                            ControlDate.Items.FindByValue(GivenDate[2]).Selected = true;
                        }
                        catch
                        {

                        }
                    }
                }
                else if (item.Question.ChartQuestionTypeID == 9)
                {

                    var Control = (TextBox)PanelContent.FindControl("multibox" + item.Question.ChartQuestionID);
                    Control.Text = item.UserReply.LongTextResponse;
                }
                else if (item.Question.ChartQuestionTypeID == 14)
                {
                    var Control = (DropDownList)PanelContent.FindControl("ddllist" + item.Question.ChartQuestionID);
                    if (item.UserReply.ChaoiceID != null)
                    {
                        Control.Items.FindByValue(item.UserReply.ChaoiceID.ToString()).Selected = true;
                    }

                }
                else if (item.Question.ChartQuestionTypeID == 15)
                {
                    var Control = (DropDownList)PanelContent.FindControl("ddllist" + item.Question.ChartQuestionID + "VAEyeWith");
                    if (item.UserReply.VAEyeWith != null && item.UserReply.VAEyeWith>0)
                    {
                        Control.Items.FindByValue(item.UserReply.VAEyeWith.ToString()).Selected = true;
                    }
                }
                else if (item.Question.ChartQuestionTypeID == 16)
                {
                    
                      var AnswerSph = item.UserReply.CyclSphere.ToString().Split('.');
                      var ControlSph = (TextBox)PanelContent.FindControl("cycl" + item.Question.ChartQuestionID + "sph");
                      if(item.UserReply.CyclSphere!=null)
                      {
                          ControlSph.Text = AnswerSph[0].ToString();
                      }
                      var ControlSphDropDown = (DropDownList)PanelContent.FindControl("DropDown" + item.Question.ChartQuestionID + "sph");
                      string SphDropDownVal = ".0";
                      if (AnswerSph.Length>1)
                      {
                          SphDropDownVal = "." + AnswerSph[1];
                      }
                      ControlSphDropDown.Items.FindByValue(SphDropDownVal).Selected = true;


                      var AnswerCyl = item.UserReply.CyclCyl.ToString().Split('.');
                      var ControlCyl = (TextBox)PanelContent.FindControl("cycl" + item.Question.ChartQuestionID + "cyl");
                      if (item.UserReply.CyclCyl != null)
                      {
                          ControlCyl.Text = AnswerCyl[0].ToString();
                      }
                      var ControlCylDropDown = (DropDownList)PanelContent.FindControl("DropDown" + item.Question.ChartQuestionID + "cycl");
                      string CylDropDownVal = ".0";
                      if (AnswerCyl.Length>1)
                      {
                          CylDropDownVal = "." + AnswerCyl[1];
                      }
                      ControlCylDropDown.Items.FindByValue(CylDropDownVal).Selected = true;
                      var ControlAxis = (TextBox)PanelContent.FindControl("cycl" + item.Question.ChartQuestionID + "axis");
                      if (item.UserReply.CyclAxis != null)
                      {
                          ControlAxis.Text = item.UserReply.CyclAxis.ToString();
                      }
                     var Controlplusminus = (RadioButtonList)PanelContent.FindControl("cycl" + item.Question.ChartQuestionID + "plusminus");
                     if (item.UserReply.CyclPlusMinus != null)
                     {
                         Controlplusminus.Items.FindByValue(item.UserReply.CyclPlusMinus.ToString()).Selected = true;
                     }
                     var ControlCyclNA = (CheckBox)PanelContent.FindControl("Cycl" + item.Question.ChartQuestionID + "NA");
                     if (item.UserReply.CyclNA != null)
                     {
                         ControlCyclNA.Checked =Convert.ToBoolean(item.UserReply.CyclNA);
                     }

                }
                else if (item.Question.ChartQuestionTypeID == 17)
                {
                    var ControlSter = (TextBox)PanelContent.FindControl("ster" + item.Question.ChartQuestionID + "sec");
                    if (item.UserReply.StereoacuitySec != null)
                    {
                        ControlSter.Text = item.UserReply.StereoacuitySec.ToString();
                    }
                    var ControlUn = (CheckBox)PanelContent.FindControl("ster" + item.Question.ChartQuestionID + "un");
                    if (ControlUn != null)
                    {
                        if (item.UserReply.StereoacuityUnable != null)
                        {
                            ControlUn.Checked = (bool)item.UserReply.StereoacuityUnable;
                        }
                    }
                }
                else if (item.Question.ChartQuestionTypeID == 18)
                {
                    var ControlPD = (TextBox)PanelContent.FindControl("alighn" + item.Question.ChartQuestionID + "pd");
                    if (item.UserReply.AlignmentPD != null)
                    {
                        ControlPD.Text = item.UserReply.AlignmentPD.ToString();
                    }

                    //var ControlCylPDHT = (TextBox)PanelContent.FindControl("alighn" + item.Question.ChartQuestionID + "pdht");
                    //if (item.UserReply.AlignmentPDHT != null)
                    //{
                    //    ControlCylPDHT.Text = item.UserReply.AlignmentPDHT.ToString();
                    //}

                    var ControlOrth = (CheckBox)PanelContent.FindControl("align" + item.Question.ChartQuestionID + "orth");
                    if (item.UserReply.AlignmentOrth != null)
                    {
                        if (ControlOrth != null)
                        {
                            ControlOrth.Checked = (bool)item.UserReply.AlignmentOrth;
                        }
                    }

                    var ControlUnable = (CheckBox)PanelContent.FindControl("align" + item.Question.ChartQuestionID + "unable");
                    if (item.UserReply.AlignmentUnableToPerform != null)
                    {
                        if (ControlUnable != null)
                        {
                            ControlUnable.Checked = (bool)item.UserReply.AlignmentUnableToPerform;
                        }
                    }

                    var ControlETXT = (RadioButtonList)PanelContent.FindControl("alighn" + item.Question.ChartQuestionID + "etxt");
                    if (item.UserReply.AlignmentETXT != null)
                    {
                        ControlETXT.Items.FindByValue(item.UserReply.AlignmentETXT.ToString()).Selected = true;
                    }

                }
                else if (item.Question.ChartQuestionTypeID == 19)
                {
                    var ControlOD = (TextBox)PanelContent.FindControl("keratometry" + item.Question.ChartQuestionID + "OD");
                    if (item.UserReply.KeratometryOD!=null)
                    {
                        ControlOD.Text = item.UserReply.KeratometryOD;
                    }
                    var ControlOS = (TextBox)PanelContent.FindControl("keratometry" + item.Question.ChartQuestionID + "OS");
                    if (item.UserReply.KeratometryOS!=null)
                    {
                        ControlOS.Text = item.UserReply.KeratometryOS;
                    }
                    var ControlNA = (CheckBox)PanelContent.FindControl("keratometry" + item.Question.ChartQuestionID + "NA");
                    if (item.UserReply.KeratometryNA!=null)
                    {
                        ControlNA.Checked =Convert.ToBoolean(item.UserReply.KeratometryNA);
                    }
                }
                else if (item.Question.ChartQuestionTypeID == 20)
                {
                    var ControlOD = (TextBox)PanelContent.FindControl("Comealthickness" + item.Question.ChartQuestionID + "OD");
                    if (item.UserReply.ComealthicknessOD!=null)
                    {
                        ControlOD.Text = item.UserReply.ComealthicknessOD.ToString();
                    }
                    var ControlNAOD = (CheckBox)PanelContent.FindControl("Comealthicknessod" + item.Question.ChartQuestionID + "NA");
                    if (item.UserReply.ComealthicknessODNA!=null)
                    {
                        ControlNAOD.Checked = Convert.ToBoolean(item.UserReply.ComealthicknessODNA);
                    }
                    var ControlOS = (TextBox)PanelContent.FindControl("Comealthickness" + item.Question.ChartQuestionID + "OS");
                    if (item.UserReply.ComealthicknessOS!=null)
                    {
                        ControlOS.Text = item.UserReply.ComealthicknessOS.ToString();
                    }
                    var ControlNAOS = (CheckBox)PanelContent.FindControl("Comealthicknessos" + item.Question.ChartQuestionID + "NA");
                    if (item.UserReply.ComealthicknessOSNA!=null)
                    {
                        ControlNAOS.Checked = Convert.ToBoolean(item.UserReply.ComealthicknessOSNA);
                    }
                }
                else if (item.Question.ChartQuestionTypeID == 21)
                {
                    var ControlRefractionPlusMinus = (RadioButtonList)PanelContent.FindControl("RefractionPlusMinus" + item.Question.ChartQuestionID + "plusminus");
                    if (item.UserReply.RefractionPlusMinus != null)
                    {
                        ControlRefractionPlusMinus.Items.FindByValue(item.UserReply.RefractionPlusMinus.ToString()).Selected = true;
                    }
                    var ControlRefractionText = (TextBox)PanelContent.FindControl("Refraction" + item.Question.ChartQuestionID + "Text");
                    var ControlRefractionDropDown = (DropDownList)PanelContent.FindControl("Refraction" + item.Question.ChartQuestionID + "DropDown");
                    var ControlRefractionNA = (CheckBox)PanelContent.FindControl("Refraction" + item.Question.ChartQuestionID + "NA");
                    if (item.UserReply.RefractionText != null)
                    {
                        var Refraction = item.UserReply.RefractionText.ToString().Split('.');
                        ControlRefractionText.Text = Refraction[0];
                        if (Refraction.Length>1 && !string.IsNullOrEmpty(Refraction[1]))
                        {
                            ControlRefractionDropDown.Items.FindByValue("." + Refraction[1]).Selected = true;
                        }
                    }
                    if (item.UserReply.RefractionNA != null)
                    {
                        ControlRefractionNA.Checked = Convert.ToBoolean(item.UserReply.RefractionNA);
                    }
                }
                else if (item.Question.ChartQuestionTypeID == 22)
                {
                    var ControlEsotropiaPrimaryGazeText = (TextBox)PanelContent.FindControl("EsotropiaPrimaryGaze" + item.Question.ChartQuestionID + "Text");
                    if (item.UserReply.EsotropiaPrimaryGaze != null)
                    {
                        ControlEsotropiaPrimaryGazeText.Text = item.UserReply.EsotropiaPrimaryGaze.ToString();
                    }
                    var ControlEsotropiaPrimaryGazeNA = (CheckBox)PanelContent.FindControl("EsotropiaPrimaryGaze" + item.Question.ChartQuestionID + "NA");
                    if (item.UserReply.EsotropiaPrimaryGazeNA != null)
                    {
                        ControlEsotropiaPrimaryGazeNA.Checked = Convert.ToBoolean(item.UserReply.EsotropiaPrimaryGazeNA);
                    }
                }


                else if (item.Question.ChartQuestionTypeID == 23)
                {
                    var ControlIOPValueText = (TextBox)PanelContent.FindControl("IOPValue" + item.Question.ChartQuestionID + "Text");
                    if (item.UserReply.IOPValue != null)
                    {
                        ControlIOPValueText.Text = item.UserReply.IOPValue.ToString();
                    }
                    var ControlIOPValueNA = (RadioButtonList)PanelContent.FindControl("IOPValue" + item.Question.ChartQuestionID + "NA");
                    if (item.UserReply.IOPValueNA != null && item.UserReply.IOPValueNA!=0 && item.UserReply.IOPValueNA!=1)
                    {
                        ControlIOPValueNA.Items.FindByValue(item.UserReply.IOPValueNA.ToString()).Selected=true;
                    }
                }
                else if (item.Question.ChartQuestionTypeID == 26)
                {
                    var ControlMeanDeviationPlusMinus = (RadioButtonList)PanelContent.FindControl("MeanDeviationPlusMinus" + item.Question.ChartQuestionID + "plusminus");
                    if (item.UserReply.MeanDeviationPlusMinus != null)
                    {
                        ControlMeanDeviationPlusMinus.Items.FindByValue(item.UserReply.MeanDeviationPlusMinus.ToString()).Selected = true;
                    }
                    var ControlMeanDeviationText = (TextBox)PanelContent.FindControl("MeanDeviation" + item.Question.ChartQuestionID + "Text");
                    var ControlMeanDeviationDropDown = (DropDownList)PanelContent.FindControl("MeanDeviation" + item.Question.ChartQuestionID + "DropDown");
                    var ControlMeanDeviationNA = (CheckBox)PanelContent.FindControl("MeanDeviation" + item.Question.ChartQuestionID + "NA");
                    if (item.UserReply.MeanDeviationText != null)
                    {
                        var MeanDeviation = item.UserReply.MeanDeviationText.ToString().Split('.');
                        ControlMeanDeviationText.Text = MeanDeviation[0];
                        if (MeanDeviation.Length > 1 && !string.IsNullOrEmpty(MeanDeviation[1]))
                        {
                            ControlMeanDeviationDropDown.Items.FindByValue("." + MeanDeviation[1]).Selected = true;
                        }
                    }
                    if (item.UserReply.MeanDeviationNA != null)
                    {
                        ControlMeanDeviationNA.Checked = Convert.ToBoolean(item.UserReply.MeanDeviationNA);
                    }          
                
                }
                else if (item.Question.ChartQuestionTypeID == 27)
                {
                    var ControlDateDay = (DropDownList)PanelContent.FindControl("dateboxday" + item.Question.ChartQuestionID + "fulldate");
                    var ControlDateMonth = (DropDownList)PanelContent.FindControl("dateboxmonth" + item.Question.ChartQuestionID + "fulldate");
                    var ControlDateYear = (DropDownList)PanelContent.FindControl("ddldatedate" + item.Question.ChartQuestionID + "fulldate");
                    if (item.UserReply.DateResponse != null)
                    {
                        try
                        {
                            var GivenDate = item.UserReply.DateResponse.Value.ToString("MM/dd/yyyy").Split('/');
                            ControlDateMonth.Items.FindByValue(GivenDate[0].Trim()).Selected = true;
                            ControlDateDay.Items.FindByValue(GivenDate[1].Trim()).Selected = true;
                            ControlDateYear.Items.FindByValue(GivenDate[2]).Selected = true;
                        }
                        catch
                        {

                        }
                    }
                }
            }
        }
    }



    public Dictionary<string, List<ChartQuestionViewModel>> GroupQuestionByMesureGroup(List<ChartAbstractionViewModel> model, bool? IsPqrsSelected)
    {
        Dictionary<Tuple<int?, string>, List<ChartQuestionViewModel>> chp = new Dictionary<Tuple<int?, string>, List<ChartQuestionViewModel>>();
        foreach (var item in model)
        {
            foreach (var question in item.QuestionsAndAnswers)
            {
                if (!string.IsNullOrEmpty(question.Question.QuestionGroupName))
                {
                    Tuple<int?, string> temp = new Tuple<int?, string>(question.Question.GroupSortOrder, question.Question.QuestionGroupName);
                    if (chp.ContainsKey(temp))
                    {
                        chp[temp].Add(question);
                    }
                    else
                    {
                        chp.Add(temp, new List<ChartQuestionViewModel>());
                        chp[temp].Add(question);
                    }
                }
            }
        }
        return chp.OrderBy(c=>c.Key.Item1).ToDictionary(c=>c.Key.Item2, c=>c.Value);

    }



    public void SubmitAction()
    {
        UserContext ctx = null;
        ctx = (UserContext)Session[Constants.SESSION_USERCONTEXT];
        string ChartAbstractionID = Request.QueryString["ri"];
        int ActiveModuleCycleID = Convert.ToInt32(ViewState["ParticipantModuleCycleID"]);
        IChartService cr = new ChartService();
        List<ChartAbstractionViewModel> chvlist = new List<ChartAbstractionViewModel>();
        bool completed = false;
        foreach (var item in (List<ChartAbstractionViewModel>)ViewState["questions"])
        {
            chvlist.Add(SubmitQuestionAnswer(item));
        }

        var DatesOfDateOfBirth = ViewState["DateOfBirth"].ToString().Split('/');
        var FinalDateOfBirth = DatesOfDateOfBirth[0] + "/01/" + DatesOfDateOfBirth[1];

        var DatesOfLastVisitDate = ViewState["LastVisitDate"].ToString().Split('/');
        var FinalLastVisitDate = DatesOfLastVisitDate[0] + "/01/" + DatesOfLastVisitDate[1];

        cr.InsertChart(chvlist, (int)ViewState["ModuleID"], ActiveModuleCycleID, ActiveModuleCycleID, ctx.ParticipantID, ChartAbstractionID, (int)ViewState["CycleNumber"], null, Convert.ToDateTime(FinalLastVisitDate), Convert.ToDateTime(FinalDateOfBirth), 1);


        ValidateChart(chvlist, (int)ViewState["ModuleID"], ActiveModuleCycleID, ActiveModuleCycleID, ctx.ParticipantID, ChartAbstractionID, (int)ViewState["CycleNumber"], null, Convert.ToDateTime(FinalLastVisitDate), Convert.ToDateTime(FinalDateOfBirth), 1);



        var ErrorMessages = cr.GetListOfErrorMesssages(ActiveModuleCycleID, ctx.ParticipantID, ChartAbstractionID);
        using (NetHealthPIMEntities db = new NetHealthPIMEntities())
        {
            var ChartRegistration = (from c in db.ChartRegistration
                                     where c.RecordIdentifier == ChartAbstractionID
                                     && c.ParticipantModuleCycleID == ActiveModuleCycleID
                                     select c).FirstOrDefault();
            if (ErrorMessages.Count > 0)
            {
                ChartRegistration.AbstractCompleted = false;
                completed = false;
            }
            else
            {
                ChartRegistration.AbstractCompleted = true;
                completed = true;
            }
            db.SaveChanges();
            if (completed == false)
            {
                var path = "ChartAbstraction.aspx?ri=" + Request.QueryString["ri"] + "&nr=" + Request.QueryString["nr"] + "&t=" + Request.QueryString["t"] + "&vl=on";
                Response.Redirect(path);

            }
            else
            {
                Response.Redirect("OpenModule.aspx?mid=" + ChartRegistration.ModuleID);
            }
        }
    }
    protected void ButtonSubmit_Click(object sender, EventArgs e)
    {
        SubmitAction();
    }


    public ChartAbstractionViewModel SubmitQuestionAnswer(ChartAbstractionViewModel question)
    {
        UserContext ctx = null;
        using (NetHealthPIMEntities db = new NetHealthPIMEntities())
        {
            int ActiveModuleCycleID = Convert.ToInt32(ViewState["ParticipantModuleCycleID"]);
            ctx = (UserContext)Session[Constants.SESSION_USERCONTEXT];
            string ChartAbstractionID = Request.QueryString["ri"];

            var ChartRegistration = (from c in db.ChartRegistration
                                     where c.RecordIdentifier == ChartAbstractionID 
                                     && c.ParticipantModuleCycleID == ActiveModuleCycleID
                                     select c).FirstOrDefault();

            foreach (var item in question.QuestionsAndAnswers)
            {
                ChartQuestionUserReplyModel uchp = new ChartQuestionUserReplyModel();
                uchp.ChartQuestionID = item.Question.ChartQuestionID;
                uchp.ModuleID = (int)ViewState["ModuleID"];
                if (item.UserReply != null)
                {
                    uchp.ChartAnswerID = item.UserReply.ChartAnswerID;
                }
                uchp.CtreatedDate = DateTime.Now;
                uchp.LastUpdateDate = DateTime.Now;
                uchp.ChartID = ChartAbstractionID;
                var datesplit = ChartRegistration.DOB.Split('/').ToArray();
                uchp.DOB = Convert.ToDateTime(datesplit[0]+"/01/"+datesplit[1]);
                uchp.StageID = 1;
                uchp.GroupID = null;
                var visitdate = ChartRegistration.InitialVisit.Split('/').ToArray();
                uchp.VisitDate = Convert.ToDateTime(visitdate[0]+"/01/"+visitdate[1]);
                uchp.Gender = 1;
                uchp.UncompletedErrorMessage = item.Question.ErrorMessage;
                uchp.Completed = true; //based on new validation
                uchp.ParticipantModuleCycleID = ActiveModuleCycleID;
                uchp.ParticipantID = ctx.ParticipantID;

                if (item.Question.ChartQuestionTypeID == 1 || item.Question.ChartQuestionTypeID == 10)
                {
                    var Control = (RadioButtonList)PanelContent.FindControl("yesno" + item.Question.ChartQuestionID);
                    if (Control != null && Control.SelectedItem != null)
                    {
                        uchp.TrueFalseResponse = Convert.ToBoolean(Control.SelectedItem.Value);
                    }
                    else
                    {
                        uchp.TrueFalseResponse = null;
                    }
                }
                else if (item.Question.ChartQuestionTypeID == 2)
                {
                    var Control = (TextBox)PanelContent.FindControl("singletext" + item.Question.ChartQuestionID);
                    uchp.ShortTextResponse = Control.Text;
                }

                else if (item.Question.ChartQuestionTypeID == 3)
                {
                    var Control = (RadioButtonList)PanelContent.FindControl("radiolist" + item.Question.ChartQuestionID);
                    if (Control != null && Control.SelectedItem != null)
                    {
                        uchp.ChaoiceID = Convert.ToInt32(Control.SelectedItem.Value);
                    }
                    else
                    {
                        uchp.ChaoiceID = null;
                    }
                }

                else if (item.Question.ChartQuestionTypeID == 4)
                {
                    var Control = (CheckBoxList)PanelContent.FindControl("chklist" + item.Question.ChartQuestionID);
                    var Values = Control.Items.Cast<ListItem>().Where(n => n.Selected).Select(n => n.Value).ToList();
                    uchp.Choices = new List<ChartQuestionChoicesUserReplyModel>();
                    foreach (var choice in Values)
                    {
                        ChartQuestionChoicesUserReplyModel choicerepl = new ChartQuestionChoicesUserReplyModel();
                        choicerepl.ChartQuestionID = item.Question.ChartQuestionID;
                        choicerepl.GroupID = null;
                        choicerepl.ParticipantModuleCycleID = ActiveModuleCycleID;
                        choicerepl.StageID = 1;
                        choicerepl.ChartChoiceID = Convert.ToInt32(choice);
                        uchp.Choices.Add(choicerepl);
                    }
                    uchp.SeveralChoices = true;
                }
                else if (item.Question.ChartQuestionTypeID == 5)
                {
                    var Control = (TextBox)PanelContent.FindControl("numberbox" + item.Question.ChartQuestionID);
                    if (!string.IsNullOrEmpty(Control.Text))
                    {
                        uchp.NumericResponse = Convert.ToDouble(Control.Text);
                    }
                    else
                    {
                        uchp.NumericResponse = null;

                    }
                }
                else if (item.Question.ChartQuestionTypeID == 7)
                {
                    var Control = (RadioButtonList)PanelContent.FindControl("radiolistrating" + item.Question.ChartQuestionID);
                    if (Control != null && Control.SelectedItem != null)
                    {
                        uchp.RatingReply = Convert.ToInt32(Control.SelectedItem.Value);
                    }
                    else
                    {
                        uchp.RatingReply = null;

                    }
                }
                else if (item.Question.ChartQuestionTypeID == 8)
                {
                    var ControlMonth= (DropDownList)PanelContent.FindControl("dateboxmonth" + item.Question.ChartQuestionID);
                    var ControlYear= (DropDownList)PanelContent.FindControl("ddldatedate" + item.Question.ChartQuestionID);
                    if ((ControlMonth!=null && ControlMonth.SelectedItem != null) && (ControlYear!=null && ControlYear.SelectedItem != null))
                    {
                        var finaldate = ControlMonth.SelectedItem.Value + "/01/" + ControlYear.SelectedItem.Value;
                        try
                        {
                            uchp.DateResponse = Convert.ToDateTime(finaldate);
                        }
                        catch
                        {
                            uchp.DateResponse = null;
                        }
                    }
                }
                else if (item.Question.ChartQuestionTypeID == 9)
                {

                    var Control = (TextBox)PanelContent.FindControl("multibox" + item.Question.ChartQuestionID);
                    uchp.LongTextResponse = Control.Text;
                }
                else if (item.Question.ChartQuestionTypeID == 14)
                {
                    var Control = (DropDownList)PanelContent.FindControl("ddllist" + item.Question.ChartQuestionID);
                    if (Control != null && Control.SelectedItem != null && !string.IsNullOrEmpty(Control.SelectedItem.Value))
                    {
                        uchp.ChaoiceID = Convert.ToInt32(Control.SelectedItem.Value);
                    }
                    else
                    {
                        uchp.ChaoiceID = null;

                    }
                }
                else if (item.Question.ChartQuestionTypeID == 15)
                {
                    var Control = (DropDownList)PanelContent.FindControl("ddllist" + item.Question.ChartQuestionID + "VAEyeWith");
                    if (Control != null && Control.SelectedItem != null && !string.IsNullOrEmpty(Control.SelectedItem.Value))
                    {
                        uchp.VAEyeWith = Convert.ToInt32(Control.SelectedItem.Value);
                    }
                    else
                    {
                        uchp.VAEyeWith = null;
                    }
                }
                else if (item.Question.ChartQuestionTypeID == 16)
                {
                    var ControlSph = (TextBox)PanelContent.FindControl("cycl" + item.Question.ChartQuestionID + "sph");
                    if (!string.IsNullOrEmpty(ControlSph.Text))
                    {
                        var DropDownListSphVal = (DropDownList)PanelContent.FindControl("DropDown" + item.Question.ChartQuestionID + "sph");
                        var SphVal = ControlSph.Text + DropDownListSphVal.SelectedItem.Value;
                        uchp.CyclSphere = Convert.ToDouble(SphVal);
                    }
                    else
                    {
                        uchp.CyclSphere = null;

                    }
                    var ControlCyl = (TextBox)PanelContent.FindControl("cycl" + item.Question.ChartQuestionID + "cyl");
                    if (!string.IsNullOrEmpty(ControlCyl.Text))
                    {
                        var DropDownListCyclVal = (DropDownList)PanelContent.FindControl("DropDown" + item.Question.ChartQuestionID + "cycl");
                        var CyclVal = ControlCyl.Text + DropDownListCyclVal.SelectedItem.Value;
                        uchp.CyclCyl = Convert.ToDouble(CyclVal);
                    }
                    else
                    {
                        uchp.CyclCyl = null;
                    }
                    var ControlAxis = (TextBox)PanelContent.FindControl("cycl" + item.Question.ChartQuestionID + "axis");
                    if (!string.IsNullOrEmpty(ControlAxis.Text))
                    {
                        uchp.CyclAxis = Convert.ToDouble(ControlAxis.Text);
                    }
                    else
                    {
                        uchp.CyclAxis = null;
                    }
                    var Controlplusminus = (RadioButtonList)PanelContent.FindControl("cycl" + item.Question.ChartQuestionID + "plusminus");
                    if (Controlplusminus != null && Controlplusminus.SelectedItem != null)
                    {
                        uchp.CyclPlusMinus = Convert.ToBoolean(Controlplusminus.SelectedItem.Value);
                    }
                    else
                    {
                        uchp.CyclPlusMinus = null;
                    }
                    var ControlCyclNA=(CheckBox)PanelContent.FindControl("Cycl" + item.Question.ChartQuestionID + "NA");
                    if (ControlCyclNA != null)
                    {
                        uchp.CyclNA = ControlCyclNA.Checked;
                    }
                    else
                    {
                        uchp.CyclNA = null;
                    }
                }
                else if (item.Question.ChartQuestionTypeID == 17)
                {
                    var ControlSter = (TextBox)PanelContent.FindControl("ster" + item.Question.ChartQuestionID + "sec");
                    if (!string.IsNullOrEmpty(ControlSter.Text))
                    {
                        uchp.StereoacuitySec = Convert.ToDouble(ControlSter.Text);
                    }
                    else
                    {
                        uchp.StereoacuitySec = null;
                    }
                    var ControlUn = (CheckBox)PanelContent.FindControl("ster" + item.Question.ChartQuestionID + "un");
                    if (ControlUn != null)
                    {
                        uchp.StereoacuityUnable = ControlUn.Checked;
                    }
                    else
                    {

                        uchp.StereoacuityUnable = null;
                    }
                }
                else if (item.Question.ChartQuestionTypeID == 18)
                {
                    var ControlPD = (TextBox)PanelContent.FindControl("alighn" + item.Question.ChartQuestionID + "pd");
                    if (!string.IsNullOrEmpty(ControlPD.Text))
                    {
                        uchp.AlignmentPD = Convert.ToDouble(ControlPD.Text);
                    }
                    else
                    {
                        uchp.AlignmentPD = null;
                    }
                    //var ControlCylPDHT = (TextBox)PanelContent.FindControl("alighn" + item.Question.ChartQuestionID + "pdht");
                    //if (!string.IsNullOrEmpty(ControlCylPDHT.Text))
                    //{
                    //    uchp.AlignmentPDHT = Convert.ToDouble(ControlCylPDHT.Text);
                    //}
                    //else
                    //{
                    //    uchp.AlignmentPDHT = null;
                    //}
                    var ControlOrth = (CheckBox)PanelContent.FindControl("align" + item.Question.ChartQuestionID + "orth");
                    if (ControlOrth != null)
                    {
                        uchp.AlignmentOrth = ControlOrth.Checked;
                    }
                    else
                    {
                        uchp.AlignmentOrth = null;
                    }
                    var ControlUnable = (CheckBox)PanelContent.FindControl("align" + item.Question.ChartQuestionID + "unable");
                    if (ControlUnable != null)
                    {
                        uchp.AlignmentUnableToPerform = ControlUnable.Checked;
                    }
                    else
                    {
                        uchp.AlignmentUnableToPerform = null;
                    }

                    var ControlETXT = (RadioButtonList)PanelContent.FindControl("alighn" + item.Question.ChartQuestionID + "etxt");
                    if (ControlETXT.SelectedItem != null)
                    {
                        uchp.AlignmentETXT = Convert.ToBoolean(ControlETXT.SelectedItem.Value);
                    }
                    else
                    {
                        uchp.AlignmentETXT = null;

                    }

                }
                else if (item.Question.ChartQuestionTypeID == 19)
                {
                    var ControlOD = (TextBox)PanelContent.FindControl("keratometry" + item.Question.ChartQuestionID + "OD");
                    if (!string.IsNullOrEmpty(ControlOD.Text))
                    {
                        uchp.KeratometryOD = ControlOD.Text;
                    }
                    else
                    {
                        uchp.KeratometryOD = null;
                    }
                    var ControlOS = (TextBox)PanelContent.FindControl("keratometry" + item.Question.ChartQuestionID + "OS");
                    if (!string.IsNullOrEmpty(ControlOS.Text))
                    {
                        uchp.KeratometryOS = ControlOS.Text;
                    }
                    else
                    {

                        uchp.KeratometryOS = null;
                    }
                    var ControlNA = (CheckBox)PanelContent.FindControl("keratometry" + item.Question.ChartQuestionID + "NA");
                    if (ControlNA != null)
                    {
                        uchp.KeratometryNA = ControlNA.Checked;
                    }
                    else
                    {
                        uchp.KeratometryNA = null;
                    }
                }
                else if (item.Question.ChartQuestionTypeID == 20)
                {
                    var ControlOD = (TextBox)PanelContent.FindControl("Comealthickness" + item.Question.ChartQuestionID + "OD");
                    if (!string.IsNullOrEmpty(ControlOD.Text))
                    {
                        uchp.ComealthicknessOD = Convert.ToDouble(ControlOD.Text);
                    }
                    else
                    {
                        uchp.ComealthicknessOD = null;
                    }
                    var ControlNAOD = (CheckBox)PanelContent.FindControl("Comealthicknessod" + item.Question.ChartQuestionID + "NA");
                    if (ControlNAOD != null)
                    {
                        uchp.ComealthicknessODNA = ControlNAOD.Checked;
                    }
                    else
                    {
                        uchp.ComealthicknessODNA = null;
                    }
                    var ControlOS = (TextBox)PanelContent.FindControl("Comealthickness" + item.Question.ChartQuestionID + "OS");
                    if (!string.IsNullOrEmpty(ControlOS.Text))
                    {
                        uchp.ComealthicknessOS = Convert.ToDouble(ControlOS.Text);
                    }
                    else
                    {
                        uchp.ComealthicknessOS = null;
                    }
                    var ControlNAOS = (CheckBox)PanelContent.FindControl("Comealthicknessos" + item.Question.ChartQuestionID + "NA");
                    if (ControlNAOS != null)
                    {
                        uchp.ComealthicknessOSNA = ControlNAOS.Checked;
                    }
                    else
                    {
                        uchp.ComealthicknessOSNA = null;
                    }
                }

                else if (item.Question.ChartQuestionTypeID == 21)
                {
                    var ControlRefractionPlusMinus = (RadioButtonList)PanelContent.FindControl("RefractionPlusMinus" + item.Question.ChartQuestionID + "plusminus");
                    if (ControlRefractionPlusMinus != null && ControlRefractionPlusMinus.SelectedItem != null)
                    {
                        uchp.RefractionPlusMinus = Convert.ToBoolean(ControlRefractionPlusMinus.SelectedItem.Value);
                    }
                    else
                    {
                        uchp.RefractionPlusMinus = null;
                    }
                    var ControlRefractionText = (TextBox)PanelContent.FindControl("Refraction" + item.Question.ChartQuestionID + "Text");
                    var ControlRefractionDropDown = (DropDownList)PanelContent.FindControl("Refraction" + item.Question.ChartQuestionID + "DropDown");
                    var ControlRefractionNA = (CheckBox)PanelContent.FindControl("Refraction" + item.Question.ChartQuestionID + "NA");
                    if (!string.IsNullOrEmpty(ControlRefractionText.Text) && ControlRefractionDropDown.SelectedItem != null)
                    {
                        string RefractionText = ControlRefractionText.Text.Replace("_", "");
                        if (!string.IsNullOrEmpty(RefractionText))
                        {
                            uchp.RefractionText = Convert.ToDouble(RefractionText + ControlRefractionDropDown.SelectedItem.Value);
                        }
                        else
                        {
                            uchp.RefractionText = null;
                        }
                    }
                    else
                    {
                        uchp.RefractionText = null;
                    }
                    if (ControlRefractionNA != null)
                    {
                        uchp.RefractionNA = ControlRefractionNA.Checked;
                    }
                    else
                    {
                        uchp.RefractionNA = null;
                    }
                    
                }
                else if (item.Question.ChartQuestionTypeID == 22)
                {
                    var ControlEsotropiaPrimaryGazeText = (TextBox)PanelContent.FindControl("EsotropiaPrimaryGaze" + item.Question.ChartQuestionID + "Text");
                    if (!string.IsNullOrEmpty(ControlEsotropiaPrimaryGazeText.Text.Replace("_", "")))
                    {
                        string EsotropiaPrimaryGazeText = ControlEsotropiaPrimaryGazeText.Text.Replace("_", "");
                        uchp.EsotropiaPrimaryGaze = Convert.ToDouble(EsotropiaPrimaryGazeText);
                    }
                    else
                    {
                        uchp.EsotropiaPrimaryGaze = null;
                    }
                    var ControlEsotropiaPrimaryGazeNA = (CheckBox)PanelContent.FindControl("EsotropiaPrimaryGaze" + item.Question.ChartQuestionID + "NA");
                    if (ControlEsotropiaPrimaryGazeNA != null)
                    {
                        uchp.EsotropiaPrimaryGazeNA = ControlEsotropiaPrimaryGazeNA.Checked;
                    }
                    else
                    {
                        uchp.EsotropiaPrimaryGazeNA = null;
                    }
                }
                else if (item.Question.ChartQuestionTypeID == 23)
                {
                    var ControlIOPValueText = (TextBox)PanelContent.FindControl("IOPValue" + item.Question.ChartQuestionID + "Text");
                    if (!string.IsNullOrEmpty(ControlIOPValueText.Text))
                    {
                        string IOPValueText = ControlIOPValueText.Text.Replace("_", "");
                        if (!string.IsNullOrEmpty(IOPValueText))
                        {
                            uchp.IOPValue = Convert.ToDouble(IOPValueText);
                        }
                    }
                    else
                    {
                        uchp.IOPValue = null;
                    }
                    var ControlIOPValueNA = (RadioButtonList)PanelContent.FindControl("IOPValue" + item.Question.ChartQuestionID + "NA");
                    if (ControlIOPValueNA != null && ControlIOPValueNA.SelectedItem != null)
                    {
                        uchp.IOPValueNA = Convert.ToInt32(ControlIOPValueNA.SelectedItem.Value);
                    }
                    else
                    {
                        uchp.IOPValueNA = null;
                    }
                }
                else if (item.Question.ChartQuestionTypeID == 26)
                {

                    var ControlMeanDeviationPlusMinus = (RadioButtonList)PanelContent.FindControl("MeanDeviationPlusMinus" + item.Question.ChartQuestionID + "plusminus");
                    if (ControlMeanDeviationPlusMinus != null && ControlMeanDeviationPlusMinus.SelectedItem != null)
                    {
                        uchp.MeanDeviationPlusMinus = Convert.ToBoolean(ControlMeanDeviationPlusMinus.SelectedItem.Value);
                    }
                    else
                    {
                        uchp.MeanDeviationPlusMinus = null;
                    }
                    var ControlMeanDeviationText = (TextBox)PanelContent.FindControl("MeanDeviation" + item.Question.ChartQuestionID + "Text");
                    var ControlMeanDeviationDropDown = (DropDownList)PanelContent.FindControl("MeanDeviation" + item.Question.ChartQuestionID + "DropDown");
                    var ControlMeanDeviationNA = (CheckBox)PanelContent.FindControl("MeanDeviation" + item.Question.ChartQuestionID + "NA");
                    if (!string.IsNullOrEmpty(ControlMeanDeviationText.Text) && ControlMeanDeviationDropDown.SelectedItem != null)
                    {
                        string MeanDeviationText = ControlMeanDeviationText.Text.Replace("_", "");
                        uchp.MeanDeviationText = Convert.ToDouble(MeanDeviationText + ControlMeanDeviationDropDown.SelectedItem.Value);
                    }
                    else
                    {
                        uchp.MeanDeviationText = null;
                    }
                    if (ControlMeanDeviationNA != null)
                    {
                        uchp.MeanDeviationNA = ControlMeanDeviationNA.Checked;
                    }
                    else
                    {
                        uchp.MeanDeviationNA = null;
                    }
                }
                else if (item.Question.ChartQuestionTypeID == 27)
                {
                    var ControlDateDay = (DropDownList)PanelContent.FindControl("dateboxday" + item.Question.ChartQuestionID+"fulldate");
                    var ControlDateMonth = (DropDownList)PanelContent.FindControl("dateboxmonth" + item.Question.ChartQuestionID+"fulldate");
                    var ControlDateYear = (DropDownList)PanelContent.FindControl("ddldatedate" + item.Question.ChartQuestionID+"fulldate");
                    if ((ControlDateDay != null && ControlDateDay.SelectedItem != null) && (ControlDateMonth != null && ControlDateMonth.SelectedItem != null) && (ControlDateYear != null && ControlDateYear.SelectedItem != null))
                    {
                        var finaldate = ControlDateMonth.SelectedItem.Value + "/" + ControlDateDay.SelectedItem.Value+ "/" + ControlDateYear.SelectedItem.Value;
                        try
                        {
                            uchp.DateResponse = Convert.ToDateTime(finaldate);
                        }
                        catch
                        {
                            uchp.DateResponse = null;
                        }
                    }
                }
                item.UserReply = uchp;
            }
            return question;
        }
    }




    public void ValidateChart(List<ChartAbstractionViewModel> model, int ModuleID, int ParticipantModuleCycleID, int MainParticipantCycleID, int ParticipantID, string DatEntryID, int StageID, int? GroupID, DateTime? VisitDate, DateTime? DOB, int? Gender)
    {

        IChartService cr = new ChartService();
        using (NetHealthPIMEntities db = new NetHealthPIMEntities())
        {
            foreach (var item in model)
            {
                foreach (var qs in cr.GetListOfQuestions(ModuleID, MainParticipantCycleID).Where(c => c.MeasureID == item.Measure.MeasureID))
                {
                    var getUserReply = item.QuestionsAndAnswers
                        .Where(c => c.Question.ChartQuestionID == qs.ChartQuestionID)
                        .Select(c => c.UserReply).FirstOrDefault();

                    if (getUserReply != null)
                    {
                        if (qs.Required == true)
                        {
                            UpdateCompleteUncomplete(qs.ChartQuestionTypeID, ModuleID, MainParticipantCycleID, qs.ChartQuestionID, qs.ErrorMessage, getUserReply.ChartID, getUserReply, qs.ChartQuestion);
                        }
                        else
                        {
                            if (qs.ChartQuestionTypeID != 6)
                            {
                                if (!string.IsNullOrEmpty(qs.CustomValidation))
                                {
                                    var result = db.ExecuteStoreQuery<int>(qs.CustomValidation.Replace("@DataEntryID", DatEntryID).Replace("@ParticipantModCycleID", ParticipantModuleCycleID.ToString()).Replace("@ModuleID", ModuleID.ToString())).FirstOrDefault();
                                    if (result >= 1)
                                    {
                                        UpdateCompleteUncomplete(qs.ChartQuestionTypeID, ModuleID, MainParticipantCycleID, qs.ChartQuestionID, (qs.ErrorMessage == null ? "" : qs.ErrorMessage), getUserReply.ChartID, getUserReply, qs.ChartQuestion);
                                    }
                                }
                            }
                        }
                    }
                }
            }
        }
    }


    public void UpdateCompleteUncomplete(int? ChartQuestionTypeID, int ModuleID, int ParticipantModuleCycleID, int? ChartQuestionID, string ErrorMessage, string DataEntryID, ChartQuestionUserReplyModel getUserReply, string QuestionText)
    {
        var DatesOfDateOfBirth=ViewState["DateOfBirth"].ToString().Split('/');
        var DateOfBirtAsDate=Convert.ToDateTime(DatesOfDateOfBirth[0]+"/01/"+DatesOfDateOfBirth[1]);
        using (NetHealthPIMEntities db = new NetHealthPIMEntities())
        {
            var question = (from c in db.ChartQuestionUserReply
                            where c.ModuleID == ModuleID
                            && c.ParticipantModuleCycleID == ParticipantModuleCycleID
                            && c.ChartID == DataEntryID
                            && c.PIMChartQuestion.ChartQuestionID == ChartQuestionID
                            select c).FirstOrDefault();
            if (question != null)
            {
                if ((ChartQuestionTypeID == 1 || ChartQuestionTypeID == 10) && getUserReply.TrueFalseResponse == null)
                {
                    question.Completed = false;
                    question.UncompletedErrorMessage = ErrorMessage;
                }
                else if (ChartQuestionTypeID == 2 && string.IsNullOrEmpty(getUserReply.ShortTextResponse))
                {
                    question.Completed = false;
                    question.UncompletedErrorMessage = ErrorMessage;
                }
                else if (ChartQuestionTypeID == 3 && getUserReply.ChaoiceID == null)
                {
                    question.Completed = false;
                    question.UncompletedErrorMessage = ErrorMessage;
                }
                else if (ChartQuestionTypeID == 4 && getUserReply.Choices.Count == 0)
                {
                    question.Completed = false;
                    question.UncompletedErrorMessage = ErrorMessage;
                }
                else if (ChartQuestionTypeID == 5 && getUserReply.NumericResponse == null)
                {
                    question.Completed = false;
                    question.UncompletedErrorMessage = ErrorMessage;
                }
                else if (ChartQuestionTypeID == 7 && getUserReply.RatingReply == null)
                {
                    question.Completed = false;
                    question.UncompletedErrorMessage = ErrorMessage;
                }
                else if (ChartQuestionTypeID == 8 && (getUserReply.DateResponse == null || getUserReply.DateResponse < DateOfBirtAsDate))
                {
                    question.Completed = false;
                    question.UncompletedErrorMessage = ErrorMessage;
                }
                else if (ChartQuestionTypeID == 8 && getUserReply.DateResponse != null && (QuestionText.Contains("Initial Exam") || QuestionText.Contains("Initial Visit")) && EvaluateDateOfInitialVisit((int)getUserReply.ModuleID, getUserReply.DateResponse.Value)==false)
                {
                    question.Completed = false;
                    question.UncompletedErrorMessage = ErrorMessage;
                }
                else if (ChartQuestionTypeID == 8 && getUserReply.DateResponse != null && (QuestionText.Contains("DOB") || QuestionText.Contains("Date of Birth")) && EvaluateDateOfBirth((int)getUserReply.ModuleID, getUserReply.DateResponse.Value.Year)==false)
                {
                    question.Completed = false;
                    question.UncompletedErrorMessage = ErrorMessage;
                }
                else if (ChartQuestionTypeID == 9 && string.IsNullOrEmpty(getUserReply.LongTextResponse))
                {
                    question.Completed = false;
                    question.UncompletedErrorMessage = ErrorMessage;
                }
                else if (ChartQuestionTypeID == 14 && getUserReply.ChaoiceID == null)
                {
                    question.Completed = false;
                    question.UncompletedErrorMessage = ErrorMessage;
                }
                else if (ChartQuestionTypeID == 15 && getUserReply.VAEyeWith == null)
                {
                    question.Completed = false;
                    question.UncompletedErrorMessage = ErrorMessage;
                }
                else if (ChartQuestionTypeID == 16 && getUserReply.CyclNA != true && (getUserReply.CyclPlusMinus == null || getUserReply.CyclSphere == null || getUserReply.CyclCyl == null))
                {
                    question.Completed = false;
                    question.UncompletedErrorMessage = ErrorMessage;
                }
                else if (ChartQuestionTypeID == 17 && ((getUserReply.StereoacuityUnable == null || getUserReply.StereoacuityUnable == false) && getUserReply.StereoacuitySec == null))
                {
                    question.Completed = false;
                    question.UncompletedErrorMessage = ErrorMessage;
                }
                else if (ChartQuestionTypeID == 18 && ((getUserReply.AlignmentOrth == null || getUserReply.AlignmentOrth == false) && (getUserReply.AlignmentPD == null || getUserReply.AlignmentETXT == null) && (getUserReply.AlignmentUnableToPerform == null || getUserReply.AlignmentUnableToPerform == false)) )
                {
                    question.Completed = false;
                    question.UncompletedErrorMessage = ErrorMessage;
                }
                else if (ChartQuestionTypeID == 19 && (getUserReply.KeratometryOD == null && getUserReply.KeratometryOS == null) && getUserReply.KeratometryNA != true)
                {
                    question.Completed = false;
                    question.UncompletedErrorMessage = ErrorMessage;
                }
                else if (ChartQuestionTypeID == 20 && (getUserReply.ComealthicknessOD == null && getUserReply.ComealthicknessODNA != true) && (getUserReply.ComealthicknessOS == null && getUserReply.ComealthicknessOSNA != true))
                {
                    question.Completed = false;
                    question.UncompletedErrorMessage = ErrorMessage;
                }
                else if (ChartQuestionTypeID == 21 && getUserReply.RefractionNA != true && (getUserReply.RefractionText == null || getUserReply.RefractionPlusMinus == null))
                {
                    question.Completed = false;
                    question.UncompletedErrorMessage = ErrorMessage;
                }
                else if (ChartQuestionTypeID == 22 && getUserReply.EsotropiaPrimaryGazeNA != true && (getUserReply.EsotropiaPrimaryGaze == null))
                {
                    question.Completed = false;
                    question.UncompletedErrorMessage = ErrorMessage;
                }
                else if (ChartQuestionTypeID == 23 && getUserReply.IOPValueNA == null && (getUserReply.IOPValue == null))
                {
                    question.Completed = false;
                    question.UncompletedErrorMessage = ErrorMessage;
                }
                else if (ChartQuestionTypeID == 27 && getUserReply.DateResponse == null)
                {
                    question.Completed = false;
                    question.UncompletedErrorMessage = ErrorMessage;
                }

                db.SaveChanges();
            }
        }
    }



    public string GenerateJS(int ModuleID, int MainParticipantModuleCycleID, int ParticipantCycleID, int UserID)
    {
        ChartService cr = new ChartService();
        Dictionary<int, List<ChartQuestionValidation>> measurevalidations = new Dictionary<int, List<ChartQuestionValidation>>();
        foreach (var item in  cr.GetAllParticipantSelectedMeasures(ModuleID, MainParticipantModuleCycleID))
        {
            measurevalidations.Add(item.MeasureID, cr.GetMeasureValidations(item.MeasureID));
        }

        List<ChartAbstractionViewModel> chartQuestions = null;
        if (Request.QueryString["ChartID"] == null)
        {
            chartQuestions = (List<ChartAbstractionViewModel>)ViewState["questions"];

        }
        else
        {

            string ChartID = Request.QueryString["ChartID"].ToString();
            chartQuestions = (List<ChartAbstractionViewModel>)ViewState["questions"];
        }

        string js = "";
        foreach (var item in (measurevalidations as Dictionary<int, List<PIM.Models.ChartQuestionValidation>>))
        {
            var distinctkeys = item.Value.DistinctBy(c => c.QuestionKey);
            var distinctatrgetkeys = item.Value.Where(c => c.CustomValidation != "").DistinctBy(c => c.TargetKey);
            foreach (var distkey in distinctatrgetkeys)
            {
                js = js + @"<script> 
                     $(document).ready(function () {
                         $('#" + distkey.TargetKey + @"-divq').hide();
                     });
                </script>";
            }

            /// LOAD ONCLICK FUNNCTION 
            foreach (var key in distinctkeys)
            {
                var validations = item.Value.Select(c => c).ToList();
                var questiontype = chartQuestions
                    .Where(c => c.Measure.MeasureID == key.MeasureID)
                    .Select(c => c.QuestionsAndAnswers)
                    .FirstOrDefault()
                    .Where(c => c.Question.ChartQuestionID == key.QuestionKey)
                    .Select(c => c.Question.ChartQuestionTypeID)
                    .FirstOrDefault();
                if (questiontype == 4)
                {
                    js = js + @"<script>
                            $(document).ready(function ()
                            {

                                $('input[lang=" + key.QuestionKey + @"]').click(function ()
                                {";
                                    foreach (var vl in validations)
                                    {
                                        js = js + @"
                                            if(" + vl.CustomValidation + @")
                                            {
                                               
                                              $('#" + vl.TargetKey + @"-divq').show();
                                              $('#" + vl.TargetKey + @"-divq').find('input').removeAttr('disabled');
                                            }
                                            else
                                            {  
                                                $('#" + vl.TargetKey + @"-divq').hide();
                                                $('#" + vl.TargetKey + @"-divq').find('input:text').val('');
                                                $('#" + vl.TargetKey + @"-divq').find('input').prop('checked', false);  
                                                $('#" + vl.TargetKey + @"-divq').find('input').attr('disabled', 'disabled');
                                                $('#" + vl.TargetKey + @"-divq').find('input').removeAttr('checked');
                                                $('#" + vl.TargetKey + @"-divq').find('select').val('');
                                            }
                                            if($('input[lang=" + key.QuestionKey + @"]:checked').length==0)
                                            {
                                                $('#" + vl.TargetKey + @"-divq').hide();
                                                $('#" + vl.TargetKey + @"-divq').find('input:text').val('');
                                                $('#" + vl.TargetKey + @"-divq').find('input').prop('checked', false);   
                                                $('#" + vl.TargetKey + @"-divq').find('input').attr('disabled', 'disabled');
                                                $('#" + vl.TargetKey + @"-divq').find('input').removeAttr('checked');
                                                $('#" + vl.TargetKey + @"-divq').find('select').val('');
                                            }
                                        ";
                                    }
                                js = js + @"});
                            });
                    </script>";
                }
                else
                {
                    js = js + @"<script>
                         $(document).ready(function () {

                             $("".Q-" + key.QuestionKey + @""").click(function () {    ";
                                 foreach (var vl in validations)
                                 {
                                     js = js + @"
                                         if(" + vl.CustomValidation + @")
                                         {
                                            $('#" + vl.TargetKey + @"-divq').show();
                                            $('#" + vl.TargetKey + @"-divq').find('input').removeAttr('disabled');
                                         }
                                         else
                                         {
                                              $('#" + vl.TargetKey + @"-divq').hide();
                                              $('#" + vl.TargetKey + @"-divq').find('input:text').val('');
                                              $('#" + vl.TargetKey + @"-divq').find('input').prop('checked', false);    
                                              $('#" + vl.TargetKey + @"-divq').find('input').attr('disabled', 'disabled');
                                              $('#" + vl.TargetKey + @"-divq').find('input').removeAttr('checked');
                                              $('#" + vl.TargetKey + @"-divq').find('select').val('');
                                          }
                                      
                                     ";
                                 }

                                 js = js + @"});
                         });
                    </script>";
                }

            }


            /// LOAD READY FUNNCTION 



            foreach (var key in distinctkeys)
            {
                var validations = item.Value.Select(c => c).ToList();
                var questiontype = chartQuestions
                    .Where(c => c.Measure.MeasureID == key.MeasureID)
                    .Select(c => c.QuestionsAndAnswers)
                    .FirstOrDefault()
                    .Where(c => c.Question.ChartQuestionID == key.QuestionKey)
                    .Select(c => c.Question.ChartQuestionTypeID)
                    .FirstOrDefault();
                if (questiontype == 4)
                {
                    js = js + @"<script>
                            $(document).ready(function ()
                            {";
                    foreach (var vl in validations)
                    {
                        js = js + @"
                                            if(" + vl.CustomValidation + @")
                                            {
                                               
                                                $('#" + vl.TargetKey + @"-divq').show();
                                                $('#" + vl.TargetKey + @"-divq').find('input').removeAttr('disabled');
                                            }
                                            else
                                            {  
                                                $('#" + vl.TargetKey + @"-divq').hide();
                                                $('#" + vl.TargetKey + @"-divq').find('input:text').val('');
                                                $('#" + vl.TargetKey + @"-divq').find('input').prop('checked', false); 
                                                $('#" + vl.TargetKey + @"-divq').find('input').attr('disabled', 'disabled');  
                                                $('#" + vl.TargetKey + @"-divq').find('input').removeAttr('checked');
                                                $('#" + vl.TargetKey + @"-divq').find('select').val('');
                                            }
                                            if($('input[lang=" + key.QuestionKey + @"]:checked').length==0)
                                            {
                                                $('#" + vl.TargetKey + @"-divq').hide();
                                                $('#" + vl.TargetKey + @"-divq').find('input:text').val('');
                                                $('#" + vl.TargetKey + @"-divq').find('input').prop('checked', false);  
                                                $('#" + vl.TargetKey + @"-divq').find('input').attr('disabled', 'disabled'); 
                                                $('#" + vl.TargetKey + @"-divq').find('input').removeAttr('checked');   
                                                $('#" + vl.TargetKey + @"-divq').find('select').val('');
                                            }
                                        ";
                    }
                    js = js + @"});
                    </script>";
                }
                else
                {
                    js = js + @"<script>
                         $(document).ready(function () {";
                    foreach (var vl in validations)
                    {
                        js = js + @"
                                         if(" + vl.CustomValidation + @")
                                         {
                                            $('#" + vl.TargetKey + @"-divq').show();
                                            $('#" + vl.TargetKey + @"-divq').find('input').removeAttr('disabled');
                                         }
                                         else
                                         {
                                              $('#" + vl.TargetKey + @"-divq').hide();
                                              $('#" + vl.TargetKey + @"-divq').find('input:text').val('');
                                              $('#" + vl.TargetKey + @"-divq').find('input').prop('checked', false); 
                                              $('#" + vl.TargetKey + @"-divq').find('input').attr('disabled', 'disabled');
                                              $('#" + vl.TargetKey + @"-divq').find('input').removeAttr('checked'); 
                                              $('#" + vl.TargetKey + @"-divq').find('select').val('');
                                          }
                                      
                                     ";
                    }

                    js = js + @"});
                    </script>";
                }

            }
        }
        return js;
    }



    public bool EvaluateDateOfBirth(int ModuleID, int BirthDateYear)
    {
        using (NetHealthPIMEntities db = new NetHealthPIMEntities())
        {
            var MeasureInfo = (from c in db.Module 
                               where c.ModuleID == ModuleID 
                               select new 
                               { 
                                   c.RegistrationMinAge,
                                   c.RegistrationMaxAge 

                               }).FirstOrDefault();
            if((DateTime.Now.Year-BirthDateYear)<MeasureInfo.RegistrationMinAge)
            {
                return false;
            }
            if ((DateTime.Now.Year - BirthDateYear) > MeasureInfo.RegistrationMaxAge)
            {
                return false;
            }
            else
            {
                return true;
            }
        }

    }

    public bool EvaluateDateOfInitialVisit(int ModuleID, DateTime InitialDateOfVisit)
    {
        using (NetHealthPIMEntities db = new NetHealthPIMEntities())
        {
            var MeasureInfo = (from c in db.Module
                               where c.ModuleID == ModuleID
                               select new
                               {
                                   c.RegistrationInitialVisitMonths

                               }).FirstOrDefault();

            

            if((((DateTime.Now.Year -InitialDateOfVisit.Year) * 12) + DateTime.Now.Month - InitialDateOfVisit.Month)>MeasureInfo.RegistrationInitialVisitMonths)
            {
                return false;
            }
            else
            {
                return true;
            }
        }
    }

    protected void LinkButtonReturn_Click(object sender, EventArgs e)
    {
        using (NetHealthPIMEntities db = new NetHealthPIMEntities())
        {
           int ActiveModuleCycleID = Convert.ToInt32(ViewState["ParticipantModuleCycleID"]);
           UserContext  ctx = (UserContext)Session[Constants.SESSION_USERCONTEXT];
            string ChartAbstractionID = Request.QueryString["ri"];

            var ChartRegistration = (from c in db.ChartRegistration
                                     where c.RecordIdentifier == ChartAbstractionID
                                     && c.ParticipantModuleCycleID == ActiveModuleCycleID
                                     select c).FirstOrDefault();
            Response.Redirect("OpenModule.aspx?mid=" + ChartRegistration.ModuleID);
        }
    }
}