﻿<%@ Page Title="" Language="C#" MasterPageFile="~/PIMMasterPage.master" AutoEventWireup="true" CodeFile="PECForms.aspx.cs" Inherits="abo_PECForms" %>

<asp:Content ID="Content1" ContentPlaceHolderID="head" Runat="Server">

<style>
    .button-table {
        width: 100%;
    }

    .button-table td {
        width: 50%;
        padding: 10px 0;
        text-align:center;
    }

    .button-table .button {
        width: 75%;
        text-align:center;
    }

    .action-box {
        padding:20px 0;
    }

</style>

</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder1" Runat="Server">

<p>
    To inform your patients about the three survey options and how to access them, there are two options:
</p>

<ul>
    <li>To provide your patients with the ability to complete a paper survey in the office or at home, a paper survey form is provided in English and Spanish and is available to print out.  (Your office staff will need to enter the survey responses on the ABO website.)</li>
    <li>To provide your patients with the ability to complete the survey by phone or online, a smaller instructional card is provided to print out and distribute.</li>
</ul> 

<p>To print a form, click one of the green buttons below that signifies the language you want and a .PDF file will open in another window.</p>
<p>(If you are using a Windows computer, you may receive a message from your browser asking you how you would like to view the file (sometimes it appears at the bottom of your browser window).  If this is the case, select the &quot;Open&quot; option).</p>
<p>Once a survey is open, enter your name and your five digit survey code in the top right corner. To do this, click on the box/field directly to the right of &quot;for Dr.&quot; and type in the required information.  Then click directly to the right of &quot;code #&quot; and enter your five digit code.  You can find this 5 digit code on your PEC Dashboard page in the &quot;Profile&quot; box.  This information is needed by the patient if s/he chooses to take the survey by phone or web.</p>
<p>Next, scroll to the bottom of the second page and:</p>
<ol>
    <li>Enter your name or your office’s name in the first field</li>
    <li>Your mailing address in the second field </li>
    <li>And your City, State and Zip in the third field.  </li>
</ol>             
<p>This information is needed for patients who choose to fill out the paper survey and return it to you by mail.  Note: If a patient does this, you or someone from your office will need to enter the paper survey by phone or web.</p>
<p>Once these fields are entered, your paper survey form is now ready to print. Print out as many forms as you need.</p>

<div class="action-box">
    <table class="button-table">
        <tr>
            <td>
                <asp:Hyperlink runat="server" id="HyperlinkEnglish" NavigateUrl="common/media/English_PEC_Survey.pdf" Target="_blank" Text="Print English PEC Survey" class="button">Print English PEC Survey</asp:Hyperlink>
            </td>
            <td>
                <asp:Hyperlink runat="server" id="HyperlinkSpanish" NavigateUrl="common/media/Spanish_PEC_Survey.pdf" Target="_blank" Text="Print Spanish PEC Survey" class="button">Print Spanish PEC Survey</asp:Hyperlink><br>
            </td>
	    </tr>
        <tr>
            <td>
                <asp:Hyperlink runat="server" id="HyperlinkEnglish2" NavigateUrl="common/media/English_Handouts.pdf" Target="_blank" Text="Print English Instruction Cards" class="button">Print English Instruction Cards</asp:Hyperlink>
		    </td>
            <td>
                <asp:Hyperlink runat="server" id="HyperlinkSpanish2" NavigateUrl="common/media/Spanish_Handouts.pdf" Target="_blank" Text="Print Spanish Instruction Cards" class="button">Print Spanish Instruction Cards</asp:Hyperlink>  
		    </td>
        </tr>
    </table>
</div>
<div class="button-box">
    <asp:Hyperlink runat="server" id="HyperlinkBack" NavigateUrl="SelectPEC.aspx" Text="Back" class="button">Back</asp:Hyperlink>
</div>
</asp:Content>
<asp:Content ID="Content3" ContentPlaceHolderID="javascript" Runat="Server">
</asp:Content>

