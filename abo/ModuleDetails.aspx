﻿<%@ Page Title="" Language="C#" MasterPageFile="~/PIMMasterPage.master" AutoEventWireup="true" CodeFile="ModuleDetails.aspx.cs" Inherits="abo_ModuleOverview" %>

<asp:Content ID="Content1" ContentPlaceHolderID="head" Runat="Server">   
    <script src="../common/js/jquery.slimscroll.min.js"></script>
        <link type="text/css" href="../common/css/atooltip.css" rel="stylesheet"  media="screen" />
    	<script type="text/javascript" src="../common/js/jquery.atooltip.js"></script>
    <script type="text/javascript" src="../common/js/jquery.atooltip2.js"></script>
    <style>
        .header-box {
            margin-bottom: 20px;
        }

        .header-box .body {
            padding: 0px;
        }
            
        .head {
            height: 28px;
        }

        .head h3 {
            float:left;
            margin:0;
            margin-left:10px;
        }

        .head .right {
            float:right;
            margin-right:10px;
        }

        .head .right p {
            margin:0;
        }

        .third {
            -moz-box-sizing: border-box;
            -webkit-box-sizing: border-box;
            box-sizing: border-box;
            float:left;
            width: 50%;
            margin-bottom: 15px;
            border-right:1px solid #eee;
            padding: 0 15px;
            
        }

        .last {
            border-right:none;
        }

        #PM1 {
            cursor: pointer;
        }

        .third ul {
            padding-left: 20px;
        }
    </style>
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder1" Runat="Server">
    <asp:HiddenField runat="server" ID="HiddenFieldNumberOfModules"/>
    <asp:HiddenField runat="server" ID="HiddenFieldModule1"/>
    <asp:HiddenField runat="server" ID="HiddenFieldModule2"/>
    <asp:HiddenField runat="server" ID="HiddenFieldModule3"/>
    <asp:HiddenField runat="server" ID="HiddenFieldModule4"/>
    <asp:HiddenField runat="server" ID="HiddenFieldTotalNumberOfCharts" />
    <!-- module detaile -->
    <p>
        Review your activity selections below. Please ensure these activities and corresponding measures are relevant to your practice and you see enough patients to meet your target number of charts. If not, return to the <a href="ModuleSelection.aspx">Activity Selection</a> page and update your selections. If you are satisfied with your selections, click the NEXT button below.
    </p>

    <asp:Repeater ID="RepeaterModules" runat="server" OnItemDataBound="RepeaterModules_ItemDataBound">
    <HeaderTemplate>
        </HeaderTemplate>
        <ItemTemplate>
        <div class="header-box" visible="true" runat="server" id="ModuleXX">
        <div class="head">
            <h3><%# Eval("ModuleDescription")%> Activity</h3>
            <div class="right">
                <p>Number of Charts: <%# Eval("NumberOfCharts")%></p>
            </div>
        </div>
        <div class="body" style="height: 300px;
overflow-y: scroll;">
            <div class="third">
                <h4>Patient Definition</h4>
                <p><%# Eval("PatientDefinition")%></p>
                <p>This activity requires an improvement period of <%# Eval("MinimumImprovementPeriod")%> days</p>
            </div>
            <div class="third last">
                <h4> Outcome Measures<a href="javascript:void(0)"> <img id="OMX" src="../common/images/tip.gif" alt="Tip" class="OutcomeMeasures"/></a></h4>
                <asp:ListView runat="server" ID="ListViewOutcomeMeasures">
                    <LayoutTemplate>
                        <ul>
                            <asp:PlaceHolder runat="server" ID="itemPlaceholder"></asp:PlaceHolder>
                        </ul>
                    </LayoutTemplate>                                
                    <ItemTemplate>
                        <li><p>
                            <%# Eval("MeasureLongDescription")%>
                            </p>
                        </li>
                        
                    </ItemTemplate> 
                </asp:ListView>
                <asp:Literal runat="server" ID="LiteralNoOutcomeMeasures" Visible="false" Text="There are no outcome measures for this activity."></asp:Literal>
            </div>
        </div>
        </div>
    </ItemTemplate>
    </asp:Repeater>
    
    
    
    
    <!--<div class="confirm_btn"><a href="#"><img src="../common/images/confirm_btn.png" alt="" /></a></div>-->
            
    <div class="button-box">
        <asp:LinkButton ID="ButtonSubmit" runat="server" Text="Next" PostBackUrl="SelectPEC.aspx" Visible="true" Class="button"/>
        <asp:LinkButton ID="LinkButtonReselectModules" runat="server" Text=" Previous " PostBackUrl="ModuleSelection.aspx" Visible="false" button="button" />
    </div>


        <!-- module detaile ends -->
</asp:Content>

<asp:Content runat="server" ID="Content4" ContentPlaceHolderID="javascript">
    <script type="text/javascript">
        $(function() {
        $('.ProcessMeasures').aToolTip({
            clickIt: true,
            tipContent: 'Process Measures refer to particular recommended procedures or system-related aspects of patient care in consideration of the diagnosis.'
            });
        });
        $(function() {
            $('.OutcomeMeasures').aToolTip({
                clickIt: true,
                xOffset: -200,
                tipContent: 'Outcome Measures refer to the desired result of treatment and can be used to consider quality of care and desired health outcomes.'
            });
        });
        
        $(function() {
            $('#PM1').aToolTip({
                clickIt: true,
                tipContent: 'Process Measures refer to particular recommended procedures or system-related aspects of patient care in consideration of the diagnosis.'
            });
        });
        $(function () {
            $('#OM1').aToolTip({
                clickIt: true,
                tipContent: 'Outcome Measures refer to the desired result of treatment and can be used to consider quality of care and desired health outcomes.'
            });
        });
        $(function () {
            $('#PM2').aToolTip({
                clickIt: true,
                tipContent: 'Process Measures refer to particular recommended procedures or system-related aspects of patient care in consideration of the diagnosis.'
            });
        });
        $(function () {
            $('#OM2').aToolTip({
                clickIt: true,
                tipContent: 'Outcome Measures refer to the desired result of treatment and can be used to consider quality of care and desired health outcomes.'
            });
        });
        $(function () {
            $('#OMX').aToolTip({
                clickIt: true,
                tipContent: 'Outcome Measures refer to the desired result of treatment and can be used to consider quality of care and desired health outcomes.'
            });
        });
        $(function () {
            $('#PM3').aToolTip({
                clickIt: true,
                tipContent: 'Process Measures refer to particular recommended procedures or system-related aspects of patient care in consideration of the diagnosis.'
            });
        });
        $(function () {
            $('#OM3').aToolTip({
                clickIt: true,
                tipContent: 'Outcome Measures refer to the desired result of treatment and can be used to consider quality of care and desired health outcomes.'
            });
        });
    </script>
    <script type="text/javascript">
        $(function () {
            $('.body').slimscroll({
                size: '5px'
            });

         
        });
</script>
<script type="text/javascript">




</script>
</asp:Content>