﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using NetHealthPIMModel;
using System.Transactions;

public partial class abo_ParticipantProfile : BasePage
{
    static string pageURLFrom = "";
    protected void Page_Load(object sender, EventArgs e)
    {
        ((PIMMasterPage)Page.Master).SetHeader("Welcome");
        ((PIMMasterPage)Page.Master).SetStatus(1);
        if (!IsPostBack)
        {
            pageURLFrom = "";
            if (Request.UrlReferrer != null)
            {
                if (!Request.UrlReferrer.ToString().ToUpper().Contains("LOGINTOABO"))
                    pageURLFrom = Request.UrlReferrer.ToString();
            }

          
            InitializePage();
        }
    }
    protected void InitializePage()
    {
        //((PIMMasterPage)Page.Master).SetTitle("Participant Profile");

        using (NetHealthPIMEntities pim = new NetHealthPIMEntities())
        {
            int cycleID = ctx.ActiveModuleCycleID;
            ParticipantModuleCycle cycle = pim.ParticipantModuleCycle.First(c => c.ParticipantModuleCycleID == cycleID);

            RadioButtonListCommunityType.DataSource = pim.PracticeCommunityType;
            RadioButtonListPracticePatientPerWeek.DataSource = pim.PracticePatientPerWeek;
            RadioButtonListPracticeType.DataSource = pim.PracticeType;
            RadioButtonListPracticePatientCharts.DataSource = pim.PracticePatientCharts;
          
            DataBind();

            ListItem li = new ListItem();
            li.Text = "";
            li.Value = "";
    
            RadioButtonListPracticePatientPerWeek.Items.Add(new ListItem(Constants.SELECTEDTEXT_OTHER, Constants.SELECTEDVALUE_OTHER));
            if (ctx != null)
            {
                //var count = pim.ParticipantProfile_COPD.Count(p => p.ParticipantModuleCycleID == ctx.ActiveModuleCycleID);
                var count = pim.Participant.Count(p => p.ParticipantID == ctx.ParticipantID);
                if (count > 0)
                {
                    using (TransactionScope transaction = new TransactionScope())
                    {

                        var countParticipantProfile = pim.ParticipantProfile_COPD.Count(p => p.ParticipantModuleCycleID == ctx.ActiveModuleCycleID);
                        ParticipantProfile_COPD participantProfile;
                        if (countParticipantProfile > 0)
                            participantProfile = pim.ParticipantProfile_COPD.First(p => p.ParticipantModuleCycleID == ctx.ActiveModuleCycleID);
                        else
                        {
                            participantProfile = new ParticipantProfile_COPD();
                            participantProfile.ParticipantModuleCycle = cycle;
                            participantProfile.LastUpdateDate = DateTime.Now;

                        }
                        

                        var countPracticeProfileCycle = pim.PracticeProfileCycle.Count(p => p.ParticipantModuleCycleID == ctx.ActiveModuleCycleID);
                        PracticeProfileCycle practiceProfile;
                        if (countPracticeProfileCycle > 0)
                            practiceProfile = pim.PracticeProfileCycle.First(p => p.ParticipantModuleCycleID == ctx.ActiveModuleCycleID);
                        else
                        {
                            practiceProfile = new PracticeProfileCycle();
                            practiceProfile.ParticipantModuleCycle = cycle;
                            practiceProfile.LastUpdateDate = DateTime.Now;
                        }
                        
                        var participant = pim.Participant.First(p => p.ParticipantID == ctx.ParticipantID);
                        participant.PracticeReference.Load();
                        participant.GenderTypeReference.Load();
                    
                        HiddenFieldRecordID.Value = participantProfile.ParticipantModuleCycleID.ToString();

                        TextBoxFirstName.Text = participant.ParticipantFirstName;
                        TextBoxLastName.Text = participant.ParticipantLastName;
                        TextBoxEmailAddress.Text = participant.ParticipantEmailAddress;
                        
                        // Phone Number
                        if (participant.PhoneNumber != null)
                        {
                            var PhoneNumber = participant.PhoneNumber.Split('-');
                            TextBoxPhone1.Text = PhoneNumber[0];
                            TextBoxPhone2.Text = PhoneNumber[1];
                            TextBoxPhone3.Text = PhoneNumber[2];
                        }

                        //Preferred Contact Method
                        if (participant.PreferredContactMethod != null)
                        {
                            if (!participant.PreferredContactMethod.Value) 
                            {
                                CheckBoxEmail.Checked = true;
                                CheckBoxPhone.Checked = false;
                            }
                            else
                            {
                                CheckBoxEmail.Checked = false;
                                CheckBoxPhone.Checked = true;
                            }
                        }

                        if (participant.PostalCode != null) TextBoxPostalCode.Text = participant.PostalCode;

                        // practice to be primarily serving
                        if (practiceProfile.PracticeCommunityType != null)
                        {
                            RadioButtonListCommunityType.Items.FindByValue(practiceProfile.PracticeCommunityType.PracticeCommunityTypeID.ToString()).Selected = true;
                        }

                        // Patient charts
                        if (practiceProfile.PracticePatientCharts != null)
                        {
                            RadioButtonListPracticePatientCharts.Items.FindByValue(practiceProfile.PracticePatientCharts.PracticePatientChartsID.ToString()).Selected = true;
                        }

                        // Patients Per Week
                        if (practiceProfile.PracticeLocationTypeOther != null && practiceProfile.PracticeLocationTypeOther.Length > 0)
                        {
                            TextBoxPatientPerWeekOther.Text = practiceProfile.PracticeLocationTypeOther;
                            RadioButtonListPracticePatientPerWeek.Items.FindByValue(Constants.SELECTEDVALUE_OTHER).Selected = true;
                        }
                        else
                        {
                            if (practiceProfile.PracticePatientPerWeek != null)
                            {
                                RadioButtonListPracticePatientPerWeek.Items.FindByValue(practiceProfile.PracticePatientPerWeek.PracticePatientPerWeekID.ToString()).Selected = true;
                            }
                        }
                        // Primary practice setting
                        if (practiceProfile.PracticeType != null)
                        {
                            RadioButtonListPracticeType.Items.FindByValue(practiceProfile.PracticeType.PracticeTypeID.ToString()).Selected = true;
                        }
                        //practice currently on pay-for-performance measures
                        if (practiceProfile.PayForPerformance != null)
                        {
                            if (practiceProfile.PayForPerformance.Value)
                                RadioButtonPayForPerformance.Items.FindByValue(ConstantsASN.ASN_RADIOBUTTONVALUE_YES).Selected = true;
                            else
                                RadioButtonPayForPerformance.Items.FindByValue(ConstantsASN.ASN_RADIOBUTTONVALUE_NO).Selected = true;
                        }

                        bool complete = isComplete();
                        bool valid = isValid();
                        pim.SaveChanges();
                        transaction.Complete();
                    }
                }
            }
        }
    }

    protected bool validForm()
    {
        return true;
    }

    protected bool isComplete()
    {
        return true;
    }

    protected bool isValid()
    {
        return true;
    }

    protected void ButtonSubmit_Click(object sender, EventArgs e)
    {
        bool editMode = (HiddenFieldRecordID.Value.Length > 0);
        if (validForm())
        {
            bool completeRecord = isComplete();
            bool validRecord = isValid();
            string ParticipantGUID = "";
            string CandidateID = "";
            int ApplicationID = 0;
            bool participantProfileComplete = false;

            using (NetHealthPIMEntities pim = new NetHealthPIMEntities())
            {
                using (TransactionScope transaction = new TransactionScope())
                {
                    CycleManager.CreateNewCycle();
                    int cycle1 = CycleManager.GetActiveCycleId(ctx.ParticipantID, ctx.ActiveModuleID, null, null);
                    ctx.ActiveModuleCycleID = cycle1;
                    Session[Constants.SESSION_USERCONTEXT] = ctx;
                    ParticipantModuleCycle cycle = pim.ParticipantModuleCycle.First(c => c.ParticipantModuleCycleID == ctx.ActiveModuleCycleID);
                    ParticipantProfile_COPD participantProfile;
                    PracticeProfileCycle practiceProfile;
                    Participant participant = pim.Participant.First(p => p.ParticipantID == ctx.ParticipantID);
                    participant.PracticeReference.Load();
                    Practice practice = participant.Practice;
                    var countParticipantProfile = pim.ParticipantProfile_COPD.Count(p => p.ParticipantModuleCycleID == ctx.ActiveModuleCycleID);
                    if (countParticipantProfile > 0)
                    {
                        participantProfile = pim.ParticipantProfile_COPD.First(p => p.ParticipantModuleCycleID == ctx.ActiveModuleCycleID);
                        practiceProfile = pim.PracticeProfileCycle.First(p => p.ParticipantModuleCycleID == ctx.ActiveModuleCycleID);
                    }
                    else
                    {
                        participantProfile = new ParticipantProfile_COPD();
                        practiceProfile = new PracticeProfileCycle();
                        participantProfile.ParticipantModuleCycle = cycle;
                        practiceProfile.ParticipantModuleCycle = cycle;
                    }

                    participant.ParticipantFirstName = TextBoxFirstName.Text;
                    participant.ParticipantLastName = TextBoxLastName.Text;
                    participant.ParticipantEmailAddress = TextBoxEmailAddress.Text;

                    // Phone Number
                    participant.PhoneNumber = TextBoxPhone1.Text + "-" + TextBoxPhone2.Text + "-" + TextBoxPhone3.Text;

                    //Preferred Contact Method
                    participant.PreferredContactMethod = CheckBoxPhone.Checked;
 
          
                    if (TextBoxPostalCode.Text.Length > 0)
                    {
                        participant.PostalCode = TextBoxPostalCode.Text;
                       
                    }

                    // practice primarily serving an
                    if (RadioButtonListCommunityType.SelectedIndex >= 0)
                    {
                        PracticeCommunityType community = new PracticeCommunityType { PracticeCommunityTypeID = Int32.Parse(RadioButtonListCommunityType.SelectedValue) };
                        pim.AttachTo("PracticeCommunityType", community);
                        //practice.PracticeCommunityType = community;
                        practiceProfile.PracticeCommunityType = community;
                    }

                    // Primary practice setting
                    if (RadioButtonListPracticeType.SelectedIndex >= 0)
                    {
                        PracticeType practicetype = new PracticeType { PracticeTypeID = Int32.Parse(RadioButtonListPracticeType.SelectedValue) };
                        pim.AttachTo("PracticeType", practicetype);
                        practiceProfile.PracticeType = practicetype;
                    }

                    // How does your practice currently maintain patient charts
                    if (RadioButtonListPracticePatientCharts.SelectedIndex >= 0)
                    {
                        PracticePatientCharts patientcharts = new PracticePatientCharts { PracticePatientChartsID = Int32.Parse(RadioButtonListPracticePatientCharts.SelectedValue) };
                        pim.AttachTo("PracticePatientCharts", patientcharts);
                        practiceProfile.PracticePatientCharts = patientcharts;
                    }

                    if (RadioButtonPayForPerformance.SelectedIndex >= 0) practiceProfile.PayForPerformance = Int32.Parse(RadioButtonPayForPerformance.SelectedValue) > 0;

                    // Approximately how many patients do you see each week
                    if (RadioButtonListPracticePatientPerWeek.SelectedIndex >= 0 && RadioButtonListPracticePatientPerWeek.SelectedValue != Constants.SELECTEDVALUE_OTHER)
                    {
                        PracticePatientPerWeek location = new PracticePatientPerWeek { PracticePatientPerWeekID = Int32.Parse(RadioButtonListPracticePatientPerWeek.SelectedValue) };
                        pim.AttachTo("PracticePatientPerWeek", location);
                        //practice.PracticeLocationType = location;
                        practiceProfile.PracticePatientPerWeek = location;
                        practiceProfile.PracticeLocationTypeOther = null;
                        TextBoxPatientPerWeekOther.Text = "";
                    }
                    if (TextBoxPatientPerWeekOther.Text.Length > 0)
                    {
                      
                        practiceProfile.PracticeLocationTypeOther = TextBoxPatientPerWeekOther.Text;
                    }

                    participant.LastUpdateDate = DateTime.Now;
                    participantProfile.LastUpdateDate = DateTime.Now;
                    practiceProfile.LastUpdateDate = DateTime.Now;

                    if (completeRecord)
                    {
                        if (cycle.ParticipantProfileComplete == true)
                            participantProfileComplete = true;

                        cycle.ParticipantProfileComplete = true;
                        cycle.ParticipantProfileCompetedDate = DateTime.Now;
                        cycle.LastUpdateDate = DateTime.Now;
                        ParticipantGUID = participant.ASNGUID;
                        CandidateID = participant.CandidateID;
                        ApplicationID = Convert.ToInt32(participant.ApplicationID);
                    }

                    pim.SaveChanges();
                    transaction.Complete();
                }
            }
            if (completeRecord)
            {
                ABOWebService.PracticeAssessmentManager proxyABO = new ABOWebService.PracticeAssessmentManager();
                DateTime currentDate = System.DateTime.Now;
                if (participantProfileComplete == false)
                {
                    proxyABO.UpdatePracticeAssessmentRecord(CandidateID, ParticipantGUID, ApplicationID, ABOWebService.PracticeAssessmentGradeEnum.Registered, currentDate, 1);
                }
				
                 if (pageURLFrom == "")
                     Response.Redirect("BusinessAssociateAgreement.aspx"); //FirstTutorial
                 else
                     Response.Redirect(pageURLFrom);
            }
            else
            {
                Response.Redirect("ParticipantProfile.aspx");
            }
        }
    }
}
