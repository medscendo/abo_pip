﻿<%@ Page Language="C#" AutoEventWireup="true" CodeFile="ModuleMenu.aspx.cs" Inherits="abo_ModuleMenu" %>

<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">

<html xmlns="http://www.w3.org/1999/xhtml">
<head runat="server">
<title>SELECT MODULE</title>
<style type="text/css">
    body, div, ul, li, h1, h2, h3, h4, h5, h6, form, fieldset, p { padding:0; margin:0; font-weight:normal; }
    body { font-family:Arial, Helvetica, sans-serif; color:#616265; font-size:12px; }
    .maincontent form{width:100%; float:left;}
    .maincontent .status_section{width:100%; float:left; padding:5px 0; min-height:550px; height:auto!important; height:550px; margin-left:10px;}
    .maincontent .status_section .status_right_section{width:218px; float:left;}
    .maincontent .status_section .status_right_section h3{width:100%; float:left; font-size:32px; line-height:38px; color:#013671; text-align:center; padding-bottom:20px; font-weight:bold;}
    .maincontent .status_section .status_right_section .performance_section{width:100%; float:left;}
    .maincontent .status_section .status_right_section .performance_section .col.blue{width:218px; float:left; background:url(../common/images/per.blue_middle_repaet.jpg) repeat-y 0 0;z-index:99;}
    .maincontent .status_section .status_right_section .performance_section .col.blue .col_details{width:218px; float:left; background:url(../common/images/per.blue_top_curve.jpg) no-repeat 0 0; min-height:144px; height:auto!important; height:144px;}
    .maincontent .status_section .status_right_section .performance_section .col .col_details h4{width:168px; float:left; height:25px; padding:15px 25px 0; text-align:center; font-weight:normal; font-weight:bold; }
    .maincontent .status_section .status_right_section .performance_section .col .col_details ul{width:202px; float:left; list-style:none; padding-left:9px;}
    .maincontent .status_section .status_right_section .performance_section .col .col_details ul li{width:202px; float:left; text-align:center; display:block; background:url(../common/images/pre_btn_bg.png) no-repeat 0 0; height:20px; padding-top:5px;}
    .maincontent .status_section .status_right_section .performance_section .col .col_details ul li a{font-size:12px; line-height:14px; color:#fff; text-decoration:none;}
</style>
<script type="text/javascript">
function openModule(moduleID) 
{
    window.returnValue = moduleID;
    window.close();
} 
</script>
</head>
<body>
<form id="form1" runat="server" style="margin:0px">
    <div class="maincontent">
        <div class="status_section">
            <div class="status_right_section">
            <div class="performance_section">
            <div class="col blue">
            <div class="col_details">
            <h4>Select Activity</h4>
                <asp:ListView runat="server" ID="ListViewModuleLinks">
                <LayoutTemplate>
                    <ul>
                    <asp:PlaceHolder runat="server" ID="itemPlaceholder"></asp:PlaceHolder>
                    </ul>
                </LayoutTemplate>
                <ItemTemplate>
                    <li><a href="#" onclick="openModule(<%# Eval("ModuleID")%>)" id="HtmlLinkOpenModule"><%# Eval("ModuleName")%></a>
                </ItemTemplate>
                </asp:ListView>
                            
            </div>
            </div>
            </div>
        </div>
        </div>
    </div>
</form>
</body>
</html>
