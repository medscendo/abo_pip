﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using NetHealthPIMModel;
using System.Transactions;
using System.Data.EntityClient;
using System.Data;

public partial class abo_ModuleOverview : BasePage
{
    protected void Page_Load(object sender, EventArgs e)
    {
        if (!IsPostBack)
        {
            InitializePage();
        }

    }
    protected void InitializePage()
    {
        string breadCrumb = Constants.BC_CREATE_PLAN_LINK + "" + Constants.BC_PQRS_INCENTIVE_PROGRAM_LINK + "" +
            Constants.BC_IMPORT_DATA_LINK + "" + Constants.BC_MODULE_SELECTION_LINK + "Activities Selected";
        ((PIMMasterPage)Page.Master).SetTitle(breadCrumb);
        ((PIMMasterPage)Page.Master).SetStatus(1);
        ((PIMMasterPage)Page.Master).SetHeader("Activities Selected");

        HiddenFieldTotalNumberOfCharts.Value = Constants.TOTAL_NUMBER_OF_CHARTS.ToString();

        using (NetHealthPIMEntities pim = new NetHealthPIMEntities())
        {
           
            ParticipantModuleCycle cycle = pim.ParticipantModuleCycle.First(c => c.ParticipantModuleCycleID == ctx.ActiveModuleCycleID);
            ParticipantModuleCycle cycle2 = null;

            if (cycle.CycleNumber == 2)
            {
                cycle2 = cycle;

                cycle.PrevParticipantModuleCycleReference.Load();
                ParticipantModuleCycle prev = cycle.PrevParticipantModuleCycle;
                cycle = prev;

            }
            int cycleID = cycle.ParticipantModuleCycleID;

            var moduleSelection = from m in pim.Module
                                  join pms in pim.ParticipantModuleSelection on m.ModuleID equals pms.ModuleID
                                  where pms.ParticipantModuleCycleID == cycle.ParticipantModuleCycleID
                                  select new ModuleDetails
                                  {
                                      ModuleID = m.ModuleID,
                                      PatientDefinition = m.PatientDefinition,
                                      ModuleDescription = m.ModuleDescription,
                                      ModuleConfirm = pms.ModuleConfirm == null ? null : pms.ModuleConfirm,
                                      NumberOfCharts = pms.NumberOfCharts,
                                      MinimumImprovementPeriod=m.MinimumImprovementPlan
                                  };
            RepeaterModules.DataSource = moduleSelection;
            RepeaterModules.DataBind();


        }
    }
    protected void ButtonConfirmModule1_Click(object sender, EventArgs e)
    {
        int moduleSelected = 1;
        ConfirmModule(moduleSelected);

    }
    protected void ButtonConfirmModule2_Click(object sender, EventArgs e)
    {
        int moduleSelected = 2;
        ConfirmModule(moduleSelected);

    }
    protected void ButtonConfirmModule3_Click(object sender, EventArgs e)
    {
        int moduleSelected = 3;
        ConfirmModule(moduleSelected);

    }
    protected void ButtonConfirmModule4_Click(object sender, EventArgs e)
    {
        int moduleSelected = 4;
        ConfirmModule(moduleSelected);

    }
    protected void ConfirmModule(int moduleSelected)
    {
        using (NetHealthPIMEntities pim = new NetHealthPIMEntities())
        {
            int cycleID = ctx.ActiveModuleCycleID;
            ParticipantModuleCycle cycle = pim.ParticipantModuleCycle.First(c => c.ParticipantModuleCycleID == cycleID);

            int ModuleID = Convert.ToInt32(((HiddenField)Page.Master.FindControl("ContentPlaceHolder1").FindControl("HiddenFieldModule" + moduleSelected.ToString())).Value);
            int ChartsToEnter = 0;
            if (HiddenFieldNumberOfModules.Value == "1")
                ChartsToEnter = Constants.TOTAL_NUMBER_OF_CHARTS;
            else
                if ((HiddenFieldNumberOfModules.Value == "3") || (HiddenFieldNumberOfModules.Value == "4"))
                    ChartsToEnter = Constants.TOTAL_NUMBER_OF_CHARTS / 3;
           
            AddInitialChartRegistration(cycle.ParticipantModuleCycleID, ModuleID, ChartsToEnter);

            var modulesNOTConfirm = (from m in pim.Module
                                   join pms in pim.ParticipantModuleSelection on m.ModuleID equals pms.ModuleID
                                   where pms.ParticipantModuleCycleID == cycle.ParticipantModuleCycleID
                                   && (pms.ModuleConfirm == false || pms.ModuleConfirm == null)
                                   select pms).Count();
            if (modulesNOTConfirm == 0)
            {
                if (cycle.SelectPreviousChartDataComplete == false)
                    cycle.SelectPreviousChartDataCompletedDate = DateTime.Today;
                cycle.SelectPreviousChartDataComplete = true;
                cycle.ModuleComplete = true;
                cycle.ModuleCompleteDate = DateTime.Today;
                pim.SaveChanges();
            }
        }
    }

    protected void AddInitialChartRegistration(int ParticipantModuleCycleID, int ModuleID, int ChartsToEnter)
    {
        using (EntityConnection conn = new EntityConnection("name=NetHealthPIMEntities"))
        {
            conn.Open();
            EntityCommand cmd = conn.CreateCommand();
            cmd.CommandText = "NetHealthPIMEntities.AddInitialChartRegistration";
            cmd.CommandType = CommandType.StoredProcedure;
            cmd.Parameters.AddWithValue("ParticipantModuleCycleID", ParticipantModuleCycleID);
            cmd.Parameters.AddWithValue("ModuleID", ModuleID);
            cmd.Parameters.AddWithValue("ChartsToEnter", ChartsToEnter);
            cmd.Parameters.AddWithValue("TOTAL_NUMBER_OF_CHARTS", Constants.TOTAL_NUMBER_OF_CHARTS);
            cmd.Parameters.AddWithValue("DeletePreviousRegistration", false);
            cmd.ExecuteNonQuery();
            conn.Close();
            ctx.WorkingModuleID = ModuleID;
            Session[Constants.SESSION_WORKINGMODULEID] = ModuleID;
            InitializePage();
        }
    }
    protected void DisplayCategory(int ModuleCategoryID, NetHealthPIMEntities pim, Repeater RepeaterCategory, System.Linq.IQueryable<ParticipantModuleSelection> ModuleSelected)
    {
        System.Linq.IQueryable report;
        report = from m in pim.Module
                 join mc in pim.ModuleCategory on m.ModuleCategory.ModuleCategoryID equals mc.ModuleCategoryID
                 where mc.ModuleCategoryID == ModuleCategoryID
                 select new ModuleSelectionData
                 {
                     ModuleID = m.ModuleID,
                     ModuleCategoryID = mc.ModuleCategoryID,
                     SortOrder = (int)mc.SortOrder,
                     DetailsURL = m.ModuleShortName,
                     ModuleCategoryName = mc.ModuleCategoryName,
                     ModuleDescription = m.ModuleDescription,
                     ModuleCode = m.ModuleCode
                 };

        RepeaterCategory.DataSource = report;
        RepeaterCategory.DataBind();
        if (RepeaterCategory.Items.Count > 0)
            ((System.Web.UI.HtmlControls.HtmlContainerControl)RepeaterCategory.Items[RepeaterCategory.Items.Count - 1].FindControl("liModuleID")).Attributes.Add("class", "none");//none

        for (var i = 0; i <= RepeaterCategory.Items.Count - 1; i++)
        {
            foreach (var m in ModuleSelected)
            {
                if (Convert.ToInt32(((HiddenField)RepeaterCategory.Items[i].FindControl("HiddenFieldModuleID")).Value) == m.ModuleID)
                    ((System.Web.UI.HtmlControls.HtmlInputCheckBox)RepeaterCategory.Items[i].FindControl("CheckBoxModule")).Checked = true;
            }
        }

    }
    protected void RepeaterModules_ItemDataBound(Object Sender, RepeaterItemEventArgs e)
    {
        if (e.Item.ItemType == ListItemType.Item || e.Item.ItemType == ListItemType.AlternatingItem)
        {
            ModuleDetails rd = (ModuleDetails)e.Item.DataItem;
            
            using (NetHealthPIMEntities pim = new NetHealthPIMEntities())
            {
                int ModuleID = Convert.ToInt32(rd.ModuleID);
                var reportProcessMeasures = (from me in pim.Measure
                                        join pms in pim.ParticipantModuleSelection on ModuleID equals pms.ModuleID
                                        where pms.ParticipantModuleCycleID == ctx.ActiveModuleCycleID
                                        & me.MeasureType.MeasureTypeID == 3
                                        & me.Module.ModuleID == ModuleID
                                        select new ModuleMeasure
                                        {
                                            ModuleID = pms.ModuleID,
                                            MeasureLongDescription = me.MeasureLongDescription
                                        }).ToList();


               

                var reportOutcomeMeasures = (from me in pim.Measure
                                        join pms in pim.ParticipantModuleSelection on ModuleID equals pms.ModuleID
                                            where pms.ParticipantModuleCycleID == ctx.ActiveModuleCycleID
                                        & me.MeasureType.MeasureTypeID == 4
                                        & me.Module.ModuleID == ModuleID
                                        select new ModuleMeasure
                                        {
                                            ModuleID = pms.ModuleID,
                                            MeasureLongDescription = me.MeasureLongDescription
                                        }).ToList();


                ListView listViewOutcomeMeasures = ((ListView)e.Item.FindControl("ListViewOutcomeMeasures"));
                listViewOutcomeMeasures.DataSource = reportOutcomeMeasures;
                listViewOutcomeMeasures.DataBind();
                if (listViewOutcomeMeasures.Items.Count == 0)
                {
                    Literal literalNoOutcomeMeasures = ((Literal)e.Item.FindControl("LiteralNoOutcomeMeasures"));
                    literalNoOutcomeMeasures.Visible = true;
                }


            }
        }
    }
}
