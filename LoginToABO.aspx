﻿<%@ Page Title="" Language="C#" MasterPageFile="~/PIMMasterPage.master" AutoEventWireup="true" CodeFile="LoginToABO.aspx.cs" Inherits="LoginToABO" %>

<asp:Content ID="Content1" ContentPlaceHolderID="head" Runat="Server">

    <style>
        .phase-header, .phase-shadow {
            display:none;
        }

        table {
            float:left;
            margin:30px 300px;
            width:300px;
        }

        #page-info {
            padding-top: 30px;
        }

    </style>

</asp:Content>

<asp:Content ID="Content4" ContentPlaceHolderID="javascript" runat="server">
<script type="text/javascript">
function validateForm() {
    var UserID = document.getElementById("<%= UserID.ClientID %>").value;
    var Password = document.getElementById("<%= Password.ClientID %>").value;

    var errors = [];
    if (UserID == 0) {
        errors[errors.length] = "User ID can not be blank.";
    }
    if (Password == 0) {
        errors[errors.length] = "Password can not be blank.";
    }
    if (errors.length > 0) {

        reportErrors(errors);
        return false;
    }
    return true;
}

function reportErrors(errors) {
    var msg = "Please Enter Valid Data...\n";
    for (var i = 0; i < errors.length; i++) {
        var numError = i + 1;
        msg += "\n" + numError + ". " + errors[i];
    }
    alert(msg);
}

</script>
</asp:Content>

<asp:Content ID="Content3" ContentPlaceHolderID="ContentPlaceHolder1" Runat="Server">
    <table>
        <tr>
            <td align="left">User Name ABO</td>
            <td align="left"><asp:TextBox ID="UserID" runat="server" Width="200px"></asp:TextBox></td>
        </tr>
        <tr>
            <td align="left">Password</td>
            <td align="left"><asp:TextBox ID="Password" runat="server" TextMode="Password" Width="200px"></asp:TextBox></td>
        </tr>
        <tr>
            <td colspan="2" align="left"><asp:Label ID="lblUserNOTExists" Text="User ID and Password combination is invalid. Please try again" runat="server" Visible="false" ForeColor="Red"></asp:Label></td>
        </tr>
        <tr>
            <td colspan="2" align="center"><asp:Button ID="ButtonSubmit" runat="server" Text="Log In" onclick="ButtonSubmit_Click" OnClientClick="javascript:return validateForm();" /></td>
        </tr>
    </table>
    <!--
    Please return to  home page and logon again to continue. <br />
    <asp:HyperLink ID="HyperLinkHome" runat="server" NavigateUrl="http://www.asn-online.org">American Board of Ophthalmology</asp:HyperLink>
    -->

</asp:Content>


