﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

/// <summary>
/// Summary description for ConstantsCOPD
/// </summary>
public class ConstantsASN
{

    public const string ASN_RADIOBUTTONVALUE_YES = "1";
    public const string ASN_RADIOBUTTONVALUE_NO = "0";
    public const int ASN_LIVING_STATUS_HOME_ALONE = 1;
    public const int ASN_LIVING_STATUS_HOME_OTHERS = 2;
    public const int ASN_LIVING_STATUS_INDEPENDENT_CARE = 3;
    public const int ASN_LIVING_STATUS_OTHER = 4;

    public const int ASN_FUNCTIONAL_INDEPENDENT = 1;
    public const int ASN_FUNCTIONAL_DEPENDENT = 2;

    public const int ASN_ETILOGY_DIABETES = 1;
    public const int ASN_ETILOGY_KIDNEY_DISEASE = 2;
    public const int ASN_ETILOGY_GLOMERULONEPHRITIS = 3;
    public const int ASN_ETILOGY_HYPERTENSION = 4;
    public const int ASN_ETILOGY_OTHER = 5;

    public const int ASN_MODALITY_HD3XWK = 1;
    public const int ASN_MODALITY_HDG3XWK = 2;
    public const int ASN_MODALITY_HD = 3;
    public const int ASN_MODALITY_PERITONEAL = 4;

    public const int ASN_DIALYSIS_ACCESS_AUTOLOGOUS = 1;
    public const int ASN_DIALYSIS_ACCESS_ARTIO_VENOUS  = 2;
    public const int ASN_DIALYSIS_ACCESS_PERITONEAL  = 3;
    public const int ASN_DIALYSIS_ACCESS_OTHER = 4;

    public const int ASN_PAYMENT_HMO20 = 1;
    public const int ASN_PAYMENT_HMO40 = 2;
    public const int ASN_PAYMENT_HMO60 = 3;
    public const int ASN_PAYMENT_HMO80 = 4;
    public const int ASN_PAYMENT_HMO100 = 5;

    public const int ASN_PAYMENT_PPO20 = 1;
    public const int ASN_PAYMENT_PPO40 = 2;
    public const int ASN_PAYMENT_PPO60 = 3;
    public const int ASN_PAYMENT_PPO80 = 4;
    public const int ASN_PAYMENT_PPO100 = 5;

    public const int ASN_PAYMENT_Indemnity20 = 1;
    public const int ASN_PAYMENT_Indemnity40 = 2;
    public const int ASN_PAYMENT_Indemnity60 = 3;
    public const int ASN_PAYMENT_Indemnity80 = 4;
    public const int ASN_PAYMENT_Indemnity100 = 5;

    public const int ASN_PAYMENT_Medicare20 = 1;
    public const int ASN_PAYMENT_Medicare40 = 2;
    public const int ASN_PAYMENT_Medicare60 = 3;
    public const int ASN_PAYMENT_Medicare80 = 4;
    public const int ASN_PAYMENT_Medicare100 = 5;

    public const int ASN_PAYMENT_Medicaid20 = 1;
    public const int ASN_PAYMENT_Medicaid40 = 2;
    public const int ASN_PAYMENT_Medicaid60 = 3;
    public const int ASN_PAYMENT_Medicaid80 = 4;
    public const int ASN_PAYMENT_Medicaid100 = 5;

    public const int ASN_PAYMENT_Compensation20 = 1;
    public const int ASN_PAYMENT_Compensation40 = 2;
    public const int ASN_PAYMENT_Compensation60 = 3;
    public const int ASN_PAYMENT_Compensation80 = 4;
    public const int ASN_PAYMENT_Compensation100 = 5;

    public const int ASN_PAYMENT_SelfPay20 = 1;
    public const int ASN_PAYMENT_SelfPay40 = 2;
    public const int ASN_PAYMENT_SelfPay60 = 3;
    public const int ASN_PAYMENT_SelfPay80 = 4;
    public const int ASN_PAYMENT_SelfPay100 = 5;

    public const int ASN_PAYMENT_NoCharge20 = 1;
    public const int ASN_PAYMENT_NoCharge40 = 2;
    public const int ASN_PAYMENT_NoCharge60 = 3;
    public const int ASN_PAYMENT_NoCharge80 = 4;
    public const int ASN_PAYMENT_NoCharge100 = 5;

    public const int ASN_PAYMENT_Other20 = 1;
    public const int ASN_PAYMENT_Other40 = 2;
    public const int ASN_PAYMENT_Other60 = 3;
    public const int ASN_PAYMENT_Other80 = 4;
    public const int ASN_PAYMENT_Other100 = 5;

}
