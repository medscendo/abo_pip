﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using PIM.Models;

namespace PIM.ViewModels
{
    [Serializable]
    public class ChartAbstractionViewModel
    {
        public MeasureModel Measure { get; set; }
        public List<ChartQuestionViewModel> QuestionsAndAnswers { get; set; }

        
    }
}