﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace PIM.Models
{
    [Serializable]
    public class SpecialtyModel
    {
        public int SpecialtyID { get; set; }
        public string SpecialtyName { get; set; }
    }
}