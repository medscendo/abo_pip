﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

/// <summary>
/// Summary description for ModuleMeasure
/// </summary>
public class ModuleMeasure
{
    public int ModuleID { get; set; }
    public string MeasureLongDescription { get; set; }
    public bool ModuleConfirm { get; set; }
}
