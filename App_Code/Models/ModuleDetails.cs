﻿using System;
using System.Collections.Generic;
using System.Web;

/// <summary>
/// Summary description for ModuleDetails
/// </summary>
public class ModuleDetails
{
    public int ModuleID { get; set; }
    public string PatientDefinition { get; set; }
    public string ModuleDescription { get; set; }
    public bool? ModuleConfirm { get; set; }
    public int? NumberOfCharts { get; set; }
    public int? MinimumImprovementPeriod { get; set; }
}
