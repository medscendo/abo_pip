﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace PIM.Models
{
    [Serializable]
    public partial class MeasureModel
    {
        public MeasureModel()
        {
            //this.Specialties = new List<SpecialtyModel>();
            //this.PracticeSettings = new List<PracticeSettingModel>();
            //this.DiagnosisSpecifics = new List<DiagnosisSpecificModel>(); 
        }

        public int MeasureID { get; set; }
        public string MeasureTitle { get; set; }
        public string MeasureClinicalRecommendation { get; set; }
        public string MeasureLongDescription { get; set; }
        public string MeasureComputationMethod { get; set; }
        public Nullable<int> MeasureTypeID { get; set; }
        public System.DateTime LastUpdateDate { get; set; }
        public Nullable<int> ModuleID { get; set; }
        public string MeasureQualityIndicator { get; set; }
        public string RationaleMeasureDescription { get; set; }
        public Nullable<int> MeasureLevelID { get; set; }
        //public int? PQRSYear { get; set; }
        //public List<SpecialtyModel> Specialties { get; set; }
        //public List<PracticeSettingModel> PracticeSettings { get; set; }
        //public List<DiagnosisSpecificModel> DiagnosisSpecifics { get; set; }
    }
}