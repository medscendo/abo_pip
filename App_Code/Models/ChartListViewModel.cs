﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace PIM.ViewModels
{
    [Serializable]
    public class ChartListViewModel
    {
        public string ChartID { get; set; }
        public bool? Status { get; set; }
        public string DateAdded { get; set; }
        public string DateUpdated { get; set; }

    }
}