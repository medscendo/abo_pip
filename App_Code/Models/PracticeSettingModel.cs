﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace PIM.Models
{
     [Serializable]
    public class PracticeSettingModel
    {
        public int PracticeSettingID { get; set; }
        public string PracticeSettingName { get; set; }
    }
}