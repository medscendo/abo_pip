﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace PIM.Models
{
    [Serializable]
    public class ChartChoiceModel
    {
        public int ChartChoiceID { get; set; }
        public string ChartChoiceName { get; set; }
        public Nullable<int> ChartQuestionID { get; set; }
        public string ToolTip { get; set; }
        public string CustomValidation { get; set; }
        public Nullable<int> ChartQuestionTypeID { get; set; }
        public Nullable<System.DateTime> LastUpdateDate { get; set; }
        public Nullable<System.DateTime> CreatedDate { get; set; }
        public string PQRSChoiceID { get; set; }
    }
}