﻿
/// <summary>
/// Summary description for ModuleSelectionData
/// </summary>
public class ModuleSelectionData
{
    public int ModuleID { get; set; }
    public int ModuleCategoryID { get; set; }
    public int? ParticipantModuleCycleID { get; set; }
    public int SortOrder { get; set; }
    public string DetailsURL { get; set; }
    public string ModuleCategoryName { get; set; }
    public string ModuleDescription { get; set; }
    public string ModuleCode { get; set; }
    public bool SelectedItem { get; set; }
    public string PQRSEnabled { get; set; }
    public int ParentModuleID { get; set; }
    public bool IsParent { get; set; }
    public int? RegisteredCharts { get; set; }
    public int? PreviousChartData { get; set; }
    public int? NumberOfCharts { get; set; }
}
