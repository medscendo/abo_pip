﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace PIM.Models
{
    [Serializable]
    public class ChartQuestionValidation
    {
        public int ValidationID { get; set; }
        public Nullable<int> MeasureID { get; set; }
        public Nullable<int> PQRSSubmissionYear { get; set; }
        public Nullable<int> QuestionKey { get; set; }
        public Nullable<int> TargetKey { get; set; }
        public Nullable<int> IfValue { get; set; }
        public Nullable<int> ThenValue { get; set; }
        public string CustomValidation { get; set; }
        public int? QuestionType { get; set; }
        public Nullable<System.DateTime> CreatedDate { get; set; }
        public Nullable<System.DateTime> LastUpdated { get; set; }
    }
}