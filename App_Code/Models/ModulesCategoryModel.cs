﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

/// <summary>
/// Summary description for ModulesCategoryModel
/// </summary>
public class ModulesCategoryModel
{
	public ModulesCategoryModel()
	{
		//
		// TODO: Add constructor logic here
		//
	}

        public int ModuleID {get; set;}
        public int    ModuleCategoryID {get; set;}
        public int    SortOrder {get; set;}
        public string  ModuleShortName {get; set;}
        public string  ModuleCategoryName {get; set;}
        public string     ModuleName {get; set;}
        public string   ModuleCode {get; set;}
        public bool?   ModuleNewVerActive {get; set;}
        public string PQRSEnabled {get; set;}
        public int  IsParent {get; set;}
        public int   ParentModuleID {get; set;}
        public bool PreviousModuleChartsAvailable {get; set;}                   
        public int ParticipantModuleCycleID {get; set;}
        public bool RegisteredCharts {get; set;}
        public bool PreviousChartData {get; set;}
        public int NumberOfCharts { get; set; }
    }