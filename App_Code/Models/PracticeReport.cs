﻿using System;
using System.Collections.Generic;
using System.Web;

/// <summary>
/// Summary description for PracticeReport
/// </summary>
public class PracticeReport
{
    public string ModuleName { get; set; }
    public int? MeasureID { get; set; }
    public int? MeasureSortOrder { get; set; }
    public string MeasureQualityIndicator { get; set; }
    public string MeasureLongDescription { get; set; }
    public int? MeasurePercentCurrent { get; set; }
    public int? MayNeedImprovement { get; set; }
    public DateTime? AbstractDataCompletedDate { get; set; }
}
