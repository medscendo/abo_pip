﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

/// <summary>
/// Summary description for ChartRegistration
/// </summary>
public class PatientChartRegistration
{
    public int ParticipantModuleCycleID { get; set; }
    public string RecordIdentifier { get; set; }
    public int ModuleID { get; set; }
    public string ChartID { get; set; }
    public string UserPatientID { get; set; }
    public bool PatientMeetsCategoryDef { get; set; }
    public bool DirectPatientCareResp { get; set; }
    public string YearOfBirth { get; set; }
    public string InitialVisit { get; set; }
    public string LastVisit { get; set; }
    public bool Completed { get; set; }
    public DateTime CompletedDate { get; set; }
    public bool AbstractCompleted { get; set; }
    public string DOB { get; set; }
}
