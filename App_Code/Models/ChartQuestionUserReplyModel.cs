﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace PIM.Models
{
    [Serializable]
    public class ChartQuestionUserReplyModel
    {
       
        public int ChartAnswerID { get; set; }
        public Nullable<int> ParticipantModuleCycleID { get; set; }
        public Nullable<int> ModuleID { get; set; }
        public Nullable<int> ParticipantID { get; set; }
        public Nullable<int> ChartQuestionID { get; set; }
        public Nullable<int> ChildQuestionID { get; set; }
        public Nullable<int> ChaoiceID { get; set; }
        public Nullable<bool> TrueFalseResponse { get; set; }
        public string ShortTextResponse { get; set; }
        public string LongTextResponse { get; set; }
        public Nullable<System.DateTime> DateResponse { get; set; }
        public Nullable<double> NumericResponse { get; set; }
        public Nullable<System.DateTime> LastUpdateDate { get; set; }
        public Nullable<System.DateTime> CtreatedDate { get; set; }
        public Nullable<int> RatingReply { get; set; }
        public Nullable<bool> SeveralChoices { get; set; }
        public List<ChartQuestionChoicesUserReplyModel> Choices { get; set; }
        public string ChartID { get; set; }
        public Nullable<bool> Completed { get; set; }
        public Nullable<int> GroupID { get; set; }
        public Nullable<int> StageID { get; set; }
        public string UncompletedErrorMessage { get; set; }
        public Nullable<System.DateTime> VisitDate { get; set; }
        public Nullable<int> Gender { get; set; }
        public Nullable<System.DateTime> DOB { get; set; }
        public Nullable<System.DateTime> DateTimeReply { get; set; }
        public Nullable<int> VAEyeWith { get; set; }
        public Nullable<int> VAFellowEye { get; set; }
        public Nullable<double> CyclSphere { get; set; }
        public Nullable<double> CyclCyl { get; set; }
        public Nullable<double> CyclAxis { get; set; }
        public Nullable<bool> CyclPlusMinus { get; set; }
        public Nullable<bool> CyclNA { get; set; }
        public Nullable<double> StereoacuitySec { get; set; }
        public Nullable<bool> StereoacuityUnable { get; set; }
        public Nullable<bool> AlignmentOrth { get; set; }
        public Nullable<double> AlignmentPD { get; set; }
        public Nullable<double> AlignmentPDHT { get; set; }
        public Nullable<bool> AlignmentETXT { get; set; }
        public Nullable<bool> AlignmentUnableToPerform { get; set; }
        public string KeratometryOD	{ get; set; }
        public string KeratometryOS	{ get; set; }
        public Nullable<bool>  KeratometryNA	{ get; set; }
        public Nullable<double>ComealthicknessOD	{ get; set; }
        public Nullable<bool> ComealthicknessODNA	{ get; set; }
        public Nullable<double> ComealthicknessOS	{ get; set; }
        public Nullable<bool> ComealthicknessOSNA { get; set; }
        public Nullable<bool> RefractionPlusMinus { get; set; }
        public Nullable<double> RefractionText { get; set; }
        public Nullable<bool> RefractionNA { get; set; }
        public Nullable<int> ChartQuestionType { get; set; }
        public Nullable<double>  EsotropiaPrimaryGaze { get; set; }
        public Nullable<bool> EsotropiaPrimaryGazeNA { get; set; }
        public Nullable<double> IOPValue { get; set; }
        public Nullable<int> IOPValueNA { get; set; }
        public Nullable<bool> MeanDeviationPlusMinus	{ get; set; }
        public Nullable<double> MeanDeviationText	{ get; set; }
        public Nullable<bool> MeanDeviationNA { get; set; }
    }
}