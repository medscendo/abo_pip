﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

/// <summary>
/// Summary description for SystemSurveyData
/// </summary>
public class SystemSurveyData
{
    public string ModuleShortName { get; set; }
    public string SSMeasureQuestion { get; set; }
    public int SSQuestionTrueFalse { get; set; }
    public string SSQuestionResponse { get; set; }
    public string Feedback { get; set; }
}

