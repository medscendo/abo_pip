﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace PIM.Models
{
     [Serializable]
    public class DiagnosisSpecificModel
    {
        public int DiagnosisSpecificID { get; set; }
        public string DiagnosisSpecificName { get; set; }
    }
}