﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using PIM.Models;

namespace PIM.ViewModels
{
     [Serializable]
    public class ChartQuestionViewModel
    {
        public ChartQuestionModel Question { get; set; }
        public ChartQuestionUserReplyModel UserReply { get; set; }
        public List<ChartChoiceModel> QuestionChoices { get; set; }
    }
}