﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

/// <summary>
/// Summary description for ChartRegistration
/// </summary>
public class ChartRegistration
{
    public string ModuleName { get; set; }
    public string RecordIdentifier { get; set; }
    public string UserPatientID { get; set; }
    public int PatientMeetsCategoryDef { get; set; }
    public int DirectPatientCareResp { get; set; }
    public int YearOfBirth { get; set; }
    public string InitialVisit { get; set; }
    public string LastVisit { get; set; }
    public string DOB { get; set; }
    public string DateSpecimenReceived { get; set; }
}
