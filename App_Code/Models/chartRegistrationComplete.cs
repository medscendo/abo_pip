﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

/// <summary>
/// Summary description for chartRegistrationComplete
/// </summary>
public class chartRegistrationComplete
{
    public string ModuleName { get; set; }
    public string RecordIdentifier { get; set; }
    public string UserPatientID { get; set; }
    public string PatientMeetsCategoryDef { get; set; }
    public string DirectPatientCareResp { get; set; }
    public int? YearOfBirth { get; set; }
    public string InitialVisit { get; set; }
    public string LastVisit { get; set; }
    public string DOB { get; set; }
    public string LinkToChart {get; set;}
    public string ChartStatus {get; set;}
    public int? ModuleVersion { get; set; }
    public string ColorChartStatus {get; set;}
    public string ModuleShortName {get; set;}
    public bool? PreviousChartData { get; set; }
    public string DateSpecimenReceived { get; set; }
}
