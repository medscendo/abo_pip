﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace PIM.Models
{
    [Serializable]
    public class ChartQuestionModel
    {
        public int ChartQuestionID { get; set; }
        public string ChartQuestion { get; set; }
        public Nullable<int> ModuleID { get; set; }
        public Nullable<int> MeasureID { get; set; }
        public string ToolTip { get; set; }
        public Nullable<bool> Required { get; set; }
        public string CustomValidation { get; set; }
        public Nullable<int> ChartQuestionTypeID { get; set; }
        public string ChartSectionName { get; set; }
        public Nullable<System.DateTime> LastUpdateDate { get; set; }
        public Nullable<System.DateTime> CreateDate { get; set; }
        public string ErrorMessage { get; set; }
        public Nullable<int> SortOrder { get; set; }
        public string CustomJavascript { get; set; }
        public string PQRSQuestionID { get; set; }
        public Nullable<int> GroupSortOrder { get; set; }
        public Nullable<int> MeasureQuestionGroupID { get; set; }
        public string MeasureQuestionGroupName { get; set; }
        public int? QuestionGroupID { get; set; }
        public string QuestionGroupName { get; set; }
        public int? NumericRangeMin { get; set; }
        public int? NumericRangeMax { get; set; }
        public bool? IsGrouped { get; set; }
    }
}