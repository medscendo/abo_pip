﻿using System;
using System.Collections.Generic;
using System.Web;

/// <summary>
/// Summary description for ParticipantDashboardModules
/// </summary>
public class ParticipantDashboardModules
{
    public int ModuleID { get; set; }
    public int ParticipantModuleCycleID { get; set; }
    public string ModuleShortName { get; set; }
    public DateTime? SelfAssessmentSurveyTargetDate { get; set; }
    public DateTime? SelfAssessmentSurveyCompletedDate { get; set; }
    public bool? SelfAssessmentSurveyComplete { get; set; }
    public DateTime? SystemSurveyTargetDate { get; set; }
    public DateTime? SystemSurveyCompletedDate { get; set; }
    public bool? SystemSurveyComplete { get; set; }
    public DateTime? AbstractDataTargetDate { get; set; }
    public DateTime? AbstractDataCompletedDate { get; set; }
    public bool? AbstractDataComplete { get; set; }
    public bool? ReportApproved { get; set; }
    public DateTime? ReportApprovedTargetDate { get; set; }
    public DateTime? ReportApprovedCompletedDate { get; set; }
    public bool? ImprovementPlanComplete { get; set; }
    public DateTime? ImprovementPlanTargetDate { get; set; }
    public DateTime? ImprovementPlanCompletedDate { get; set; }
    public int PatientChartRegistrationCompleted { get; set; }
    public int TotalCharts { get; set; }
    public int ChartsCompleted { get; set; }
}
