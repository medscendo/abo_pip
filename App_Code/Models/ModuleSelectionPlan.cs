﻿using System;
using System.Collections.Generic;
using System.Web;

/// <summary>
/// Summary description for ModuleSelectionPlan
/// </summary>
public class ModuleSelectionPlan
{
    public int ModuleID { get; set; }
    public int ModuleCategoryID { get; set; }
    public int SortOrder { get; set; }
    public string ModuleShortName { get; set; }
    public string ModuleCategoryName { get; set; }
    public string ModuleName { get; set; }
    public string ModuleCode { get; set; }
    public string PQRSEnabled { get; set; }
    public int IsParent { get; set; }
    public int ParentModuleID { get; set; }
    public int ParticipantModuleCycleID { get; set; }
    public int? RegisteredCharts { get; set; }
    public int? PreviousChartData { get; set; }
    public int? NumberOfCharts { get; set; }
}
