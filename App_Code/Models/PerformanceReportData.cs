﻿/// <summary>
/// Summary description for PerformanceReportData
/// </summary>
public class PerformanceReportData
{
    public string ModuleName { get; set; }
    public int MeasureID { get; set; }
    public int? MeasureGroupID { get; set; }
    public int? MeasureGroupSortOrder { get; set; }
    public string MeasureQualityIndicator { get; set; }
    public string MeasureTitle { get; set; }
    public string MeasureClinicalRecommendation { get; set; }
    public string MeasureLongDescription { get; set; }
    public string MeasureComputationMethod { get; set; }
    public int? AbstractRecordsIncluded { get; set; }
    public int? MeasurePercentCurrent { get; set; }
    public int? PeerData { get; set; }
    public double? SelfData { get; set; }
    public int? MeasurePercentPrevious { get; set; }
    public int? MeasureGoal { get; set; }
    public bool? IsPhase2Flag { get; set; }
    public bool? IncludeInImprovementPlan { get; set; }
    public int? PeerAbstractRecordsIncluded { get; set; }
  	public double? Peer25Percentile { get; set; }
  	public double? Peer75Percentile { get; set; }
    public int? prevMeasurePercent3To6 { get; set; }
    public int? MeasurePercent3To6 { get; set; }
    public int? PeerData3To6 { get; set; }
    public int? prevMeasurePercent7To12 { get; set; }
    public int? MeasurePercent7To12 { get; set; }
    public int? PeerData7To12 { get; set; }
    public int? prevMeasurePercentNewOnset { get; set; }
    public int? MeasurePercentNewOnset { get; set; }
    public int? PeerDataNewOnset { get; set; }
    public int? prevMeasurePercentRecurrence { get; set; }
    public int? MeasurePercentRecurrence { get; set; }
    public int? PeerDataRecurrence { get; set; }
    public int? MayNeedImprovement { get; set; }
    public int MeasureDenominator { get; set; }
    public int? MeasureTypeID { get; set; }
    public int? MeasureRelated { get; set; }
    public bool? MeanDisplayFlag { get; set; }
    public bool? IsnumericMeasure { get; set; }
    public string NumericMeasureName { get; set; }
    public bool? NumericFullGraph { get; set; }
    public string VisualAcuityResponse { get; set; }
    public int? MeasureMaxAge { get; set; }
    public string OUT20 { get; set; }

}
