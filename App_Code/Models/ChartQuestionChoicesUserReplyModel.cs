﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace PIM.Models
{
   [Serializable]
    public class ChartQuestionChoicesUserReplyModel
    {
        public int ChartQuestionChoicesUserReplyID { get; set; }
        public Nullable<int> ChartChoiceID { get; set; }
        public Nullable<int> ChartQuestionID { get; set; }
        public Nullable<int> ChartAnswerID { get; set; }
        public Nullable<int> ParticipantModuleCycleID { get; set; }
        public Nullable<int> GroupID { get; set; }
        public Nullable<int> StageID { get; set; }
        public string UncompletedErrorMessage { get; set; }
    }
}