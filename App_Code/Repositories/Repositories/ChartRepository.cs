﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using NetHealthPIMModel;
using PIM.Models;
using PIM.ViewModels;
using QIS.Cache;
using MoreLinq;
using PIM.Services;
using System.Data;
using System.Data.Objects;
using System.Data.Common;

namespace PIM.Repositories
{
    public class ChartRepository : IChartRepository
    {
        private NetHealthPIMEntities db;
        private IPQRSService pqrsserv;
        private ICache cacheservice;
        public ChartRepository()
        {
            this.db = new NetHealthPIMEntities();
            this.cacheservice = new Cache();
            this.pqrsserv = new PQRSService();
        }

        public int? GetNumberOfRequiredCharts(int moduleid, int? groupID)
        {
            int? NumberRequired = 0;
            if (groupID != null)
            {
                NumberRequired = (from c in db.Module
                                  where c.ModuleID == moduleid
                                  select c.MinAbstractsRequired).FirstOrDefault();
            }
            else
            {
                NumberRequired = (from c in db.Module
                                  where c.ModuleID == moduleid
                                  select c.MinAbstractsRequired).FirstOrDefault();

            }
            return NumberRequired;
        }

        public int GetNumberOfComcletedCharts(int cycleid)
        {
            ObjectContext c = null;
            int NumberCompleted;
            var entityConnection = (System.Data.EntityClient.EntityConnection)c.Connection;
            DbConnection conn = entityConnection.StoreConnection;
            ConnectionState initialState = conn.State;
            try
            {
                if (initialState != ConnectionState.Open)
                    conn.Open();  // open connection if not already open
                using (DbCommand cmd = conn.CreateCommand())
                {
                    cmd.CommandText = "select count(1) from(select ChartID, sum(case when completed=1 then 1 else 0 end) a, sum(case when completed=0 then 1 else 0 end) b  from  PIMChartQuestionUserReply where ParticipantModuleCycleID = " + cycleid + "  group by ChartID having sum(case when completed=0 then 1 else 0 end)=0) x";
                    NumberCompleted = cmd.ExecuteNonQuery();
                }
            }
            finally
            {
                if (initialState != ConnectionState.Open)
                    conn.Close(); // only close connection if not initially open
            }
            return NumberCompleted;
        }

        public void SetChartStatus(string ChartID, int ParticipantModuleCcyleID, int ModuleID, bool status)
        {
            var Charts = (from c in db.ChartQuestionUserReply
                          where c.ChartID == ChartID
                          && c.ParticipantModuleCycleID == ParticipantModuleCcyleID
                          && c.ModuleID == ModuleID
                          select c).ToList();

            foreach (var chart in Charts)
            {
                chart.Completed = status;
                db.SaveChanges();
            }

        }

        public bool IsChartCompleted(int ParticipantModuleCycleID, int ModuleID, int? GroupID)
        {
            int? RequiredCHARTS = 0;
            if (GroupID == null)
            {
                RequiredCHARTS = (from c in db.Module
                                  where c.ModuleID == ModuleID
                                  select c.MinAbstractsRequired).FirstOrDefault();
            }
            else
            {
                RequiredCHARTS = (from c in db.Module
                                  where c.ModuleID == ModuleID
                                  select c.MinAbstractsRequired).FirstOrDefault();

            }
            var ChartCompleted = (from c in db.ChartQuestionUserReply
                                  where c.ParticipantModuleCycleID == ParticipantModuleCycleID
                                  && c.ModuleID == ModuleID
                                  where c.Completed == true
                                  select c).DistinctBy(c => c.ChartID).ToList();

            if (ChartCompleted.Count >= RequiredCHARTS)
            {
                return true;
            }
            else
            {
                return false;
            }

        }

        public bool PromtUserforMoreCharts(int ParticipantID, int ParticipantModuleID, int? GroupID, int ModuleID)
        {

            return true;
        }

        public int? GetChartsConsidered(int ParticipantID, int MeasureID)
        {
            var ChartConsiderd = (from c in db.ParticipantMeasure
                                  where c.MeasureID == MeasureID
                                  && c.ParticipantModuleCycleID == ParticipantID
                                  select c.AbstractRecordsIncluded).FirstOrDefault();

            return ChartConsiderd;

        }

        public int GetNewNumberOfComcletedCharts(int cycleid, int ParticipantID, int ModuleID)
        {
            int NumberCompleted = 0;
            var Cycles = (from c in db.ParticipantModuleCycle
                          where c.ParticipantModule.ParticipantID == ParticipantID
                          && c.ParticipantModule.ModuleID == ModuleID
                          select new
                          {
                              c.ParticipantModuleCycleID,
                              c.ABMT
                          }).ToList();

            if (Cycles.Count > 3 && Cycles.Where(c => c.ABMT == true).FirstOrDefault() != null)
            {
                var NewNum = (from c in db.ParticipantModuleCycle
                              where c.ParticipantModuleCycleID == cycleid
                              && c.ParticipantModule.ModuleID == ModuleID
                              select c.PrevParticipantModuleCycle.ParticipantModuleCycleID).FirstOrDefault();


                NumberCompleted = GetNumberOfComcletedCharts((int)NewNum);
            }
            else if (Cycles.Count > 3 && Cycles.Where(c => c.ABMT == true).FirstOrDefault() == null)
            {
                var NewNum = (from c in db.ParticipantModuleCycle
                              where c.ParticipantModuleCycleID == cycleid
                              && c.ParticipantModule.ModuleID == ModuleID
                              select c.PrevParticipantModuleCycle.ParticipantModuleCycleID).FirstOrDefault();


                NumberCompleted = GetNumberOfComcletedCharts((int)NewNum);
            }
            else
            {
                NumberCompleted = GetNumberOfComcletedCharts((int)cycleid);
            }
            return NumberCompleted;
        }

        public List<PIMChartQuestion> GetChartQuestions(int ModuleID, int MainParticipantCycleID)
        {
            List<PIMChartQuestion> CachedChartQuestions = cacheservice.Get(CacheKeys.PIMChartQuestions + ModuleID) as List<PIMChartQuestion>;
            if (CachedChartQuestions != null && CheckIfCached(ModuleID) == true)
            {
                return CachedChartQuestions;
            }
            else
            {
                List<PIMChartQuestion> question = new List<PIMChartQuestion>();

                foreach (var item in GetChartMeasures(ModuleID, MainParticipantCycleID))
                {
                    var Questions = (from c in db.PIMChartQuestion.Include("Module")
                                     where c.Measure.MeasureID == item.MeasureID
                                     orderby c.SortOrder ascending
                                     select c).ToList();
                    question.AddRange(Questions);
                }

                cacheservice.Set(CacheKeys.PIMChartQuestions + ModuleID, question);
                return question;

            }
        }

        public List<PIMChartChoice> GetChartChoices(int ModuleID)
        {

            List<PIMChartChoice> CachedChartChoices = cacheservice.Get(CacheKeys.PIMChartChoices + ModuleID) as List<PIMChartChoice>;
            if (CachedChartChoices != null && CheckIfCached(ModuleID) == true)
            {
                return CachedChartChoices;
            }
            else
            {

                var Choices = (from c in db.PIMChartQuestion
                               from v in db.PIMChartChoices
                               where c.ChartQuestionID == v.PIMChartQuestion.ChartQuestionID
                               && c.Module.ModuleID == ModuleID
                               select v).ToList();

                cacheservice.Set(CacheKeys.PIMChartChoices + ModuleID, Choices);
                return Choices;

            }
        }

        public List<Measure> GetChartMeasures(int ModuleID, int MainParticipantModuleCycleID)
        {
            List<Measure> CachedMeasures = cacheservice.Get(CacheKeys.PIMMeasures + MainParticipantModuleCycleID) as List<Measure>;
            if (CachedMeasures != null && CheckIfCached(ModuleID) == true)
            {
                return CachedMeasures;
            }
            else
            {
                var participantmeasures = (from c in db.ParticipantModuleSelection where c.ParticipantModuleCycleID == c.ParticipantModuleCycleID && c.ModuleID == ModuleID select c.ModuleID).Distinct().ToList();
                List<Measure> mes = new List<Measure>();

                foreach (var item in participantmeasures)
                {
                    var Measures = (from c in db.Measure
                                    where c.Module.ModuleID == item
                                    select c).ToList();
                    mes.AddRange(Measures);
                }
                cacheservice.Set(CacheKeys.PIMMeasures + MainParticipantModuleCycleID, mes);
                return mes;

            }
        }

        public List<ChartAbstractionViewModel> GetChartQuestionsForEachMeasure(int ModuleID, int ParticipantModuleCycleID, int MainParticipantCycleID, int ParticipantID, string ChartID, bool? IsPQRS)
        {
            ///Gets The Questions groups
            List<PIMChartQuestionGroup> QuestionQroups = (from g in db.PIMChartQuestionGroups select g).ToList();
            //IF PQRS NOT ENABLED REMOVES PQRS GROUP 
            if (IsPQRS == null || IsPQRS == false)
            {
                QuestionQroups = QuestionQroups.Where(c => !c.QuestionGroupName.Contains("PQRS")).ToList();
            }

            ///Filters questions by group
            List<PIMChartQuestion> FilteredQuestions = (from c in GetChartQuestions(ModuleID, MainParticipantCycleID) from n in QuestionQroups where c.QuestionGroupID == n.QuestionGroupID select c).ToList();

            ///Gets all user reply
            var UserReply = (from t in db.ChartQuestionUserReply
                             where t.ParticipantID == ParticipantID
                             && t.ParticipantModuleCycleID == ParticipantModuleCycleID
                             && t.ChartID == ChartID
                             select new ChartQuestionUserReplyModel
                             {
                                 ChaoiceID = t.ChaoiceID,
                                 ChartAnswerID = t.ChartAnswerID,
                                 ChartQuestionID = t.PIMChartQuestion.ChartQuestionID,
                                 ChildQuestionID = t.ChildQuestionID,
                                 CtreatedDate = t.CtreatedDate,
                                 LongTextResponse = t.LongTextResponse,
                                 ModuleID = t.ModuleID,
                                 ChartID = t.ChartID,
                                 Gender = t.Gender,
                                 DOB = t.DOB,
                                 ChartQuestionType = t.PIMChartQuestion.ChartQuestionTypeID,
                                 VAEyeWith = t.VAEyeWith,
                                 VAFellowEye = t.VAFellowEye,
                                 CyclSphere = t.CyclSphere,
                                 CyclCyl = t.CyclCyl,
                                 CyclPlusMinus = t.CyclPlusMinus,
                                 CyclAxis = t.CyclAxis,
                                 CyclNA = t.CyclNA,
                                 StereoacuitySec = t.StereoacuitySec,
                                 StereoacuityUnable = t.StereoacuityUnable,
                                 AlignmentOrth = t.AlignmentOrth,
                                 AlignmentPD = t.AlignmentPD,
                                 AlignmentPDHT = t.AlignmentPDHT,
                                 AlignmentETXT = t.AlignmentETXT,
                                 AlignmentUnableToPerform = t.AlignmentUnableToPerform,
                                 ComealthicknessOD = t.ComealthicknessOD,
                                 ComealthicknessODNA = t.ComealthicknessODNA,
                                 ComealthicknessOS = t.ComealthicknessOS,
                                 ComealthicknessOSNA = t.ComealthicknessOSNA,
                                 KeratometryOD = t.KeratometryOD,
                                 KeratometryOS = t.KeratometryOS,
                                 KeratometryNA = t.KeratometryNA,
                                 RefractionPlusMinus = t.RefractionPlusMinus,
                                 RefractionText = t.RefractionText,
                                 RefractionNA = t.RefractionNA,
                                 Completed = t.Completed,
                                 EsotropiaPrimaryGaze = t.EsotropiaPrimaryGaze,
                                 EsotropiaPrimaryGazeNA = t.EsotropiaPrimaryGazeNA,
                                 IOPValue = t.IOPValue,
                                 IOPValueNA = t.IOPValueNA,
                                 MeanDeviationPlusMinus = t.MeanDeviationPlusMinus,
                                 MeanDeviationNA = t.MeanDeviationNA,
                                 MeanDeviationText = t.MeanDeviationText,
                                 VisitDate = t.VisitDate,
                                 NumericResponse = t.NumericResponse,
                                 ParticipantModuleCycleID = ParticipantModuleCycleID,
                                 TrueFalseResponse = t.TrueFalseResponse,
                                 RatingReply = t.RatingReply,
                                 ShortTextResponse = t.ShortTextResponse,
                                 DateResponse = t.DateResponse,
                                 LastUpdateDate = t.LastUpdateDate,
                                 SeveralChoices = t.SeveralChoices
                             })
                                                                         .ToList();

            /// get choices for checkbox questions 
            UserReply.Where(c => c.ChartQuestionType == 4).ForEach(v => v.Choices = (from g in db.PIMChartQuestionChoicesUserReply
                                                                                     where g.PIMChartQuestion.ChartQuestionID == v.ChartQuestionID && g.ChartID == v.ChartID
                                                                                     && g.ParticipantModuleCycleID == ParticipantModuleCycleID
                                                                                     select new ChartQuestionChoicesUserReplyModel
                                                                                     {
                                                                                         ChartAnswerID = v.ChartAnswerID,
                                                                                         ChartChoiceID = g.PIMChartChoices.ChartChoiceID,
                                                                                         ChartQuestionChoicesUserReplyID = g.ChartQuestionChoicesUserReplyID,
                                                                                         ChartQuestionID = g.PIMChartQuestion.ChartQuestionID,
                                                                                         ParticipantModuleCycleID = g.ParticipantModuleCycleID
                                                                                     }).ToList());

            // builds the test. 
            var Test = GetChartMeasures(ModuleID, MainParticipantCycleID).ToList().Select(l => new ChartAbstractionViewModel
            {
                Measure = (from t in GetChartMeasures(ModuleID, MainParticipantCycleID)
                           where t.MeasureID == l.MeasureID
                           select new MeasureModel
                           {
                               MeasureID = t.MeasureID,
                               MeasureTitle = t.MeasureTitle,
                               ModuleID = ModuleID,
                               RationaleMeasureDescription = t.RationaleMeasureDescription
                           }).FirstOrDefault(),
                QuestionsAndAnswers = (from c in FilteredQuestions
                                       where c.Measure.MeasureID == l.MeasureID
                                       select c).Select(c => new ChartQuestionViewModel
                                       {
                                           Question = (from v in FilteredQuestions
                                                       where v.ChartQuestionID == c.ChartQuestionID
                                                       orderby c.SortOrder ascending
                                                       select new ChartQuestionModel
                                                       {
                                                           ModuleID = ModuleID,
                                                           ChartQuestion = c.ChartQuestion,
                                                           ErrorMessage = c.ErrorMessage,
                                                           SortOrder = c.SortOrder,
                                                           CustomJavascript = c.CustomJavascript,
                                                           ChartQuestionID = c.ChartQuestionID,
                                                           ChartQuestionTypeID = c.ChartQuestionTypeID,
                                                           NumericRangeMax = c.NumericRangeMax,
                                                           NumericRangeMin = c.NumericRangeMin,
                                                           IsGrouped = c.IsGrouped,
                                                           ChartSectionName = c.ChartSectionName,
                                                           CreateDate = c.CreateDate,
                                                           QuestionGroupID = c.QuestionGroupID,
                                                           QuestionGroupName = QuestionQroups.Where(j => j.QuestionGroupID == c.QuestionGroupID).FirstOrDefault().QuestionGroupName,
                                                           GroupSortOrder = QuestionQroups.Where(j => j.QuestionGroupID == c.QuestionGroupID).FirstOrDefault().QuestionSortOrder,
                                                           CustomValidation = c.CustomValidation,
                                                           MeasureID = c.Measure.MeasureID,
                                                           MeasureQuestionGroupID = c.MeasureQuestionGroupID,
                                                           MeasureQuestionGroupName = "Pending",
                                                           PQRSQuestionID = c.PQRSQuestionID,
                                                           Required = c.Required,
                                                           ToolTip = c.ToolTip
                                                       }).FirstOrDefault(),
                                           UserReply = UserReply.Where(t => t.ChartQuestionID == c.ChartQuestionID).FirstOrDefault(),
                                           QuestionChoices = (from n in GetChartChoices(ModuleID)
                                                              where n.PIMChartQuestion.ChartQuestionID == c.ChartQuestionID
                                                              select new ChartChoiceModel
                                                              {
                                                                  ChartChoiceID = n.ChartChoiceID,
                                                                  ChartChoiceName = n.ChartChoiceName,
                                                                  ChartQuestionID = n.PIMChartQuestion.ChartQuestionID,
                                                                  ChartQuestionTypeID = n.ChartQuestionTypeID,
                                                                  CreatedDate = n.CreatedDate,
                                                                  ToolTip = n.ToolTip,
                                                                  PQRSChoiceID = n.PQRSChoiceID,
                                                                  CustomValidation = n.CustomValidation,
                                                                  LastUpdateDate = n.LastUpdateDate
                                                              }).ToList()
                                       }).OrderBy(c => c.Question.MeasureQuestionGroupID).ThenBy(c => c.Question.SortOrder).ToList()
            }).ToList();
            return Test;
        }

        public void InsertChart(List<ChartAbstractionViewModel> model, int ModuleID, int ParticipantModuleCycleID, int ParticipantID, string DatEntryID, DateTime? VisitDate, DateTime? DOB, int? Gender, int StageID, int? GroupID)
        {
            int PQRSGroupID = 0;
            int PQRSProgramID = 0;


            var PQRSIn = (from c in db.Module
                          where c.ModuleID == ModuleID
                          select new
                          {
                              c.PQRS,
                              c.PQRSGroupID,
                              c.PQRSMeasureID,
                              c.PQRSProgramID
                          }).FirstOrDefault();
            
            var ChartInformation = (from c in db.ChartRegistration
                                     where c.RecordIdentifier == DatEntryID
                                     && c.ParticipantModuleCycleID == ParticipantModuleCycleID
                                     && c.ModuleID == ModuleID
                                     select new
                                     {
                                         c.InitialVisit,
                                         c.LastVisit,
                                         c.DOB
                                     }).FirstOrDefault();

            if (!string.IsNullOrEmpty(PQRSIn.PQRSGroupID))
            {
                PQRSGroupID = Convert.ToInt32(PQRSIn.PQRSGroupID);

            }
            if (!string.IsNullOrEmpty(PQRSIn.PQRSProgramID))
            {
                PQRSProgramID = Convert.ToInt32(PQRSIn.PQRSProgramID);

            }

            ///PQRS TABLE
            bool? IsMedicare = false;
            DataTable values = new DataTable("PQRSPROVisit");
            values.Columns.Add("MeasureID", typeof(Int32));
            values.Columns.Add("QuestionType", typeof(String));
            values.Columns.Add("QuestionID", typeof(Int32));
            values.Columns.Add("QuestionIDSuffix", typeof(String));
            values.Columns.Add("Value", typeof(String));
            values.Columns.Add("CPT", typeof(String));
            values.Columns.Add("Modifier", typeof(String));


            var Check = (from c in db.ChartQuestionUserReply
                         where c.ChartID == DatEntryID
                         && c.ModuleID == ModuleID
                         && c.ParticipantID == ParticipantID
                         && c.ParticipantModuleCycleID == ParticipantModuleCycleID
                         select c).ToList();

            if (Check.Count == 0)
            {
                foreach (var item in model)
                {
                    foreach (var usereply in item.QuestionsAndAnswers)
                    {
                        if (usereply.UserReply != null)
                        {
                            if (usereply.UserReply.ChartAnswerID == 0)
                            {
                                var GivenChartQuestion = (from c in db.PIMChartQuestion
                                                          where c.ChartQuestionID == (int)usereply.UserReply.ChartQuestionID
                                                          select c).FirstOrDefault();
                                
                                ChartQuestionUserReply pimuserrep = new ChartQuestionUserReply();
                                pimuserrep.LongTextResponse = usereply.UserReply.LongTextResponse;
                                pimuserrep.NumericResponse = usereply.UserReply.NumericResponse;
                                pimuserrep.VAFellowEye = usereply.UserReply.VAFellowEye;
                                pimuserrep.VAEyeWith = usereply.UserReply.VAEyeWith;
                                pimuserrep.CyclSphere = usereply.UserReply.CyclSphere;
                                pimuserrep.CyclCyl = usereply.UserReply.CyclCyl;
                                pimuserrep.CyclAxis = usereply.UserReply.CyclAxis;
                                pimuserrep.CyclPlusMinus = usereply.UserReply.CyclPlusMinus;
                                pimuserrep.CyclNA = usereply.UserReply.CyclNA;
                                pimuserrep.StereoacuitySec = usereply.UserReply.StereoacuitySec;
                                pimuserrep.StereoacuityUnable = usereply.UserReply.StereoacuityUnable;
                                pimuserrep.AlignmentOrth = usereply.UserReply.AlignmentOrth;
                                pimuserrep.AlignmentPD = usereply.UserReply.AlignmentPD;
                                pimuserrep.AlignmentPDHT = usereply.UserReply.AlignmentPDHT;
                                pimuserrep.AlignmentETXT = usereply.UserReply.AlignmentETXT;
                                pimuserrep.AlignmentUnableToPerform = usereply.UserReply.AlignmentUnableToPerform;
                                pimuserrep.RefractionText = usereply.UserReply.RefractionText;
                                pimuserrep.RefractionPlusMinus = usereply.UserReply.RefractionPlusMinus;
                                pimuserrep.RefractionNA = usereply.UserReply.RefractionNA;
                                pimuserrep.TrueFalseResponse = usereply.UserReply.TrueFalseResponse;
                                pimuserrep.ShortTextResponse = usereply.UserReply.ShortTextResponse;
                                pimuserrep.RatingReply = usereply.UserReply.RatingReply;
                                pimuserrep.NumericResponse = usereply.UserReply.NumericResponse;
                                pimuserrep.PIMChartQuestion = GivenChartQuestion;
                                pimuserrep.PIMChartQuestion.ChartQuestionID = (int)usereply.UserReply.ChartQuestionID;
                                pimuserrep.ChaoiceID = usereply.UserReply.ChaoiceID;
                                pimuserrep.ModuleID = ModuleID;
                                pimuserrep.DateResponse = usereply.UserReply.DateResponse;
                                pimuserrep.CtreatedDate = DateTime.Now;
                                pimuserrep.LastUpdateDate = DateTime.Now;
                                pimuserrep.ChartID = DatEntryID;
                                pimuserrep.DOB = DOB;
                                pimuserrep.StageID = StageID;
                                pimuserrep.GroupID = GroupID;
                                pimuserrep.VisitDate = VisitDate;
                                pimuserrep.Gender = Gender;
                                pimuserrep.UncompletedErrorMessage = usereply.UserReply.UncompletedErrorMessage;
                                pimuserrep.Completed = true; //based on new validation
                                pimuserrep.ParticipantModuleCycleID = ParticipantModuleCycleID;
                                pimuserrep.ParticipantID = ParticipantID;
                                pimuserrep.SeveralChoices = usereply.UserReply.SeveralChoices;
                                pimuserrep.ComealthicknessOD = usereply.UserReply.ComealthicknessOD;
                                pimuserrep.ComealthicknessODNA = usereply.UserReply.ComealthicknessODNA;
                                pimuserrep.ComealthicknessOS = usereply.UserReply.ComealthicknessOS;
                                pimuserrep.ComealthicknessOSNA = usereply.UserReply.ComealthicknessOSNA;
                                pimuserrep.KeratometryOD = usereply.UserReply.KeratometryOD;
                                pimuserrep.KeratometryOS = usereply.UserReply.KeratometryOS;
                                pimuserrep.KeratometryNA = usereply.UserReply.KeratometryNA;
                                pimuserrep.EsotropiaPrimaryGaze = usereply.UserReply.EsotropiaPrimaryGaze;
                                pimuserrep.EsotropiaPrimaryGazeNA = usereply.UserReply.EsotropiaPrimaryGazeNA;
                                pimuserrep.MeanDeviationPlusMinus = usereply.UserReply.MeanDeviationPlusMinus;
                                pimuserrep.MeanDeviationNA = usereply.UserReply.MeanDeviationNA;
                                pimuserrep.MeanDeviationText = usereply.UserReply.MeanDeviationText;
                                pimuserrep.IOPValue = usereply.UserReply.IOPValue;
                                pimuserrep.IOPValueNA = usereply.UserReply.IOPValueNA;
                                db.ChartQuestionUserReply.AddObject(pimuserrep);
                                db.SaveChanges();

                                ///PQRS CHART ADD
                                if (PQRSIn.PQRS == true)
                                {
                                    string reply = "";
                                    if (!string.IsNullOrEmpty(usereply.UserReply.LongTextResponse))
                                    {
                                        reply = usereply.UserReply.LongTextResponse;
                                    }
                                    else if (usereply.UserReply.NumericResponse != null)
                                    {
                                        reply = usereply.UserReply.NumericResponse.ToString();
                                    }
                                    else if (usereply.UserReply.TrueFalseResponse != null)
                                    {
                                        reply = usereply.UserReply.TrueFalseResponse.ToString();
                                    }
                                    else if (!string.IsNullOrEmpty(usereply.UserReply.ShortTextResponse))
                                    {
                                        reply = usereply.UserReply.ShortTextResponse;
                                    }
                                    else if (usereply.UserReply.RatingReply != null)
                                    {
                                        reply = usereply.UserReply.RatingReply.ToString();
                                    }
                                    else if (usereply.UserReply.NumericResponse != null)
                                    {
                                        reply = usereply.UserReply.NumericResponse.ToString();
                                    }
                                    else if (usereply.UserReply.DateResponse != null)
                                    {
                                        reply = usereply.UserReply.DateResponse.Value.ToShortDateString();
                                    }
                                    else if (usereply.UserReply.ChaoiceID != null)
                                    {
                                        var pqrschoice = (from c in db.PIMChartChoices
                                                          where c.ChartChoiceID == usereply.UserReply.ChaoiceID
                                                          select c.PQRSChoiceID).FirstOrDefault();
                                        reply = pqrschoice;
                                    }

                                    if (usereply.Question.PQRSQuestionID == "IsMedicare")
                                    {
                                        IsMedicare = usereply.UserReply.TrueFalseResponse;
                                    }
                                    int n;
                                    bool isPqrsNumberNumeric = int.TryParse(usereply.Question.PQRSQuestionID, out n);
                                    if (isPqrsNumberNumeric == true)
                                    {
                                        string QuestionType = "G";
                                        if (n > 2000)
                                        {
                                            QuestionType = "M";
                                        }
                                        values.Rows.Add(GivenChartQuestion.PQRSMeasureID, QuestionType, usereply.Question.PQRSQuestionID, "", reply, "", "");
                                    }
                                }

                                if (usereply.UserReply.SeveralChoices == true)
                                {
                                    foreach (var choices in usereply.UserReply.Choices)
                                    {
                                        PIMChartQuestionChoicesUserReply chartuserreply = new PIMChartQuestionChoicesUserReply();
                                        chartuserreply.ChartQuestionUserReply = pimuserrep;
                                        chartuserreply.ChartQuestionUserReply.CtreatedDate = DateTime.Now;
                                        chartuserreply.ChartQuestionUserReply.LastUpdateDate = DateTime.Now;
                                        chartuserreply.ChartQuestionUserReply.ChartID = DatEntryID;
                                        chartuserreply.ChartQuestionUserReply.DOB = DOB;
                                        chartuserreply.ChartQuestionUserReply.StageID = StageID;
                                        chartuserreply.ChartQuestionUserReply.GroupID = GroupID;
                                        chartuserreply.ChartQuestionUserReply.VisitDate = VisitDate;
                                        chartuserreply.ChartQuestionUserReply.Gender = Gender;
                                        chartuserreply.ChartQuestionUserReply.ModuleID = ModuleID;
                                        chartuserreply.ChartQuestionUserReply.ParticipantID = ParticipantID;
                                        chartuserreply.ChartQuestionUserReply.UncompletedErrorMessage = usereply.UserReply.UncompletedErrorMessage;
                                        chartuserreply.ChartQuestionUserReply.Completed = true;
                                        chartuserreply.ChartQuestionUserReply.ParticipantModuleCycleID = ParticipantModuleCycleID;
                                        chartuserreply.PIMChartChoices = (from c in db.PIMChartChoices
                                                                          where c.ChartChoiceID == choices.ChartChoiceID
                                                                          select c).FirstOrDefault();
                                        chartuserreply.PIMChartChoices.ChartChoiceID = (int)choices.ChartChoiceID;
                                        chartuserreply.PIMChartQuestion = (from c in db.PIMChartQuestion
                                                                           where c.ChartQuestionID == usereply.Question.ChartQuestionID
                                                                           select c).FirstOrDefault();
                                        chartuserreply.PIMChartQuestion.ChartQuestionID = usereply.Question.ChartQuestionID;
                                        chartuserreply.ChartID = DatEntryID;
                                        chartuserreply.ParticipantModuleCycleID = ParticipantModuleCycleID;
                                        db.PIMChartQuestionChoicesUserReply.AddObject(chartuserreply);
                                        db.SaveChanges();

                                    }
                                }
                            }
                        }

                    }
                }
                if (PQRSIn.PQRS == true)
                {
                    var Year = Convert.ToInt32(ChartInformation.DOB.Split('/')[1].ToString());
                    var Age = DateTime.Now.Year - Year;
                    //pqrsserv.PQRSSubmitChart(ParticipantID, ModuleID, DatEntryID, PQRSProgramID, PQRSGroupID, VisitDate, values, IsMedicare, Age);
                }
            }
            else
            {
                foreach (var item in model)
                {
                    foreach (var usereply in item.QuestionsAndAnswers)
                    {
                        if (usereply.UserReply != null)
                        {
                            bool AddQuestion = false;
                            var GivenChartQuestion = (from c in db.PIMChartQuestion
                                                      where c.ChartQuestionID == (int)usereply.UserReply.ChartQuestionID
                                                      select c).FirstOrDefault();
                            ChartQuestionUserReply pimuserrep = (from c in db.ChartQuestionUserReply
                                                                 where c.ChartID == DatEntryID
                                                                 && c.ModuleID == ModuleID
                                                                 && c.ParticipantModuleCycleID == ParticipantModuleCycleID
                                                                 && c.ParticipantID == ParticipantID
                                                                 && c.DOB==usereply.UserReply.DOB
                                                                 && c.PIMChartQuestion.ChartQuestionID==usereply.UserReply.ChartQuestionID
                                                                 && c.ChartAnswerID == usereply.UserReply.ChartAnswerID
                                                                 select c).FirstOrDefault();
                            if (usereply.UserReply.ChartAnswerID==0 && pimuserrep == null)
                            {
                                AddQuestion = true;
                                pimuserrep = new ChartQuestionUserReply();
                            }
                            pimuserrep.LongTextResponse = usereply.UserReply.LongTextResponse;
                            pimuserrep.NumericResponse = usereply.UserReply.NumericResponse;
                            pimuserrep.VAFellowEye = usereply.UserReply.VAFellowEye;
                            pimuserrep.VAEyeWith = usereply.UserReply.VAEyeWith;
                            pimuserrep.CyclSphere = usereply.UserReply.CyclSphere;
                            pimuserrep.CyclCyl = usereply.UserReply.CyclCyl;
                            pimuserrep.CyclAxis = usereply.UserReply.CyclAxis;
                            pimuserrep.CyclNA = usereply.UserReply.CyclNA;
                            pimuserrep.CyclPlusMinus = usereply.UserReply.CyclPlusMinus;
                            pimuserrep.StereoacuitySec = usereply.UserReply.StereoacuitySec;
                            pimuserrep.StereoacuityUnable = usereply.UserReply.StereoacuityUnable;
                            pimuserrep.AlignmentOrth = usereply.UserReply.AlignmentOrth;
                            pimuserrep.AlignmentPD = usereply.UserReply.AlignmentPD;
                            pimuserrep.AlignmentPDHT = usereply.UserReply.AlignmentPDHT;
                            pimuserrep.AlignmentETXT = usereply.UserReply.AlignmentETXT;
                            pimuserrep.AlignmentUnableToPerform = usereply.UserReply.AlignmentUnableToPerform;
                            pimuserrep.RefractionText = usereply.UserReply.RefractionText;
                            pimuserrep.RefractionPlusMinus = usereply.UserReply.RefractionPlusMinus;
                            pimuserrep.RefractionNA = usereply.UserReply.RefractionNA;
                            pimuserrep.TrueFalseResponse = usereply.UserReply.TrueFalseResponse;
                            pimuserrep.ShortTextResponse = usereply.UserReply.ShortTextResponse;
                            pimuserrep.RatingReply = usereply.UserReply.RatingReply;
                            pimuserrep.NumericResponse = usereply.UserReply.NumericResponse;
                            pimuserrep.PIMChartQuestion = GivenChartQuestion;
                            pimuserrep.PIMChartQuestion.ChartQuestionID = usereply.Question.ChartQuestionID;
                            pimuserrep.ChaoiceID = usereply.UserReply.ChaoiceID;
                            pimuserrep.ModuleID = ModuleID;
                            pimuserrep.DOB = DOB;
                            pimuserrep.StageID = StageID;
                            pimuserrep.GroupID = GroupID;
                            pimuserrep.VisitDate = VisitDate;
                            pimuserrep.Gender = Gender;
                            pimuserrep.DateResponse = usereply.UserReply.DateResponse;
                            pimuserrep.LastUpdateDate = DateTime.Now;
                            pimuserrep.ChartID = DatEntryID;
                            pimuserrep.UncompletedErrorMessage = usereply.UserReply.UncompletedErrorMessage;
                            pimuserrep.Completed = true; // based on new validation
                            pimuserrep.ParticipantModuleCycleID = ParticipantModuleCycleID;
                            pimuserrep.ParticipantID = ParticipantID;
                            pimuserrep.SeveralChoices = usereply.UserReply.SeveralChoices;
                            pimuserrep.ComealthicknessOD = usereply.UserReply.ComealthicknessOD;
                            pimuserrep.ComealthicknessODNA = usereply.UserReply.ComealthicknessODNA;
                            pimuserrep.ComealthicknessOS = usereply.UserReply.ComealthicknessOS;
                            pimuserrep.ComealthicknessOSNA = usereply.UserReply.ComealthicknessOSNA;
                            pimuserrep.KeratometryOD = usereply.UserReply.KeratometryOD;
                            pimuserrep.KeratometryOS = usereply.UserReply.KeratometryOS;
                            pimuserrep.KeratometryNA = usereply.UserReply.KeratometryNA;
                            pimuserrep.EsotropiaPrimaryGaze = usereply.UserReply.EsotropiaPrimaryGaze;
                            pimuserrep.EsotropiaPrimaryGazeNA = usereply.UserReply.EsotropiaPrimaryGazeNA;
                            pimuserrep.MeanDeviationPlusMinus = usereply.UserReply.MeanDeviationPlusMinus;
                            pimuserrep.MeanDeviationNA = usereply.UserReply.MeanDeviationNA;
                            pimuserrep.MeanDeviationText = usereply.UserReply.MeanDeviationText;
                            pimuserrep.IOPValue = usereply.UserReply.IOPValue;
                            pimuserrep.IOPValueNA = usereply.UserReply.IOPValueNA;
                            if (AddQuestion == true)
                            {
                                db.ChartQuestionUserReply.AddObject(pimuserrep);
                                pimuserrep.CtreatedDate = DateTime.Now;
                                pimuserrep.LastUpdateDate = DateTime.Now;
                            }
                            db.SaveChanges();

                            ///PQRS CHART ADD
                            if (PQRSIn.PQRS == true)
                            {
                                string reply = "";
                                if (!string.IsNullOrEmpty(usereply.UserReply.LongTextResponse))
                                {
                                    reply = usereply.UserReply.LongTextResponse;
                                }
                                else if (usereply.UserReply.NumericResponse != null)
                                {
                                    reply = usereply.UserReply.NumericResponse.ToString();
                                }
                                else if (usereply.UserReply.TrueFalseResponse != null)
                                {
                                    reply = usereply.UserReply.TrueFalseResponse.ToString();
                                }
                                else if (!string.IsNullOrEmpty(usereply.UserReply.ShortTextResponse))
                                {
                                    reply = usereply.UserReply.ShortTextResponse;
                                }
                                else if (usereply.UserReply.RatingReply != null)
                                {
                                    reply = usereply.UserReply.RatingReply.ToString();
                                }
                                else if (usereply.UserReply.DateResponse != null)
                                {
                                    reply = usereply.UserReply.DateResponse.Value.ToShortDateString();
                                }
                                else if (usereply.UserReply.NumericResponse != null)
                                {
                                    reply = usereply.UserReply.NumericResponse.ToString();
                                }
                                else if (usereply.UserReply.ChaoiceID != null)
                                {
                                    var pqrschoice = (from c in db.PIMChartChoices
                                                      where c.ChartChoiceID == usereply.UserReply.ChaoiceID
                                                      select c.PQRSChoiceID).FirstOrDefault();
                                    reply = pqrschoice;
                                }

                                if (usereply.Question.PQRSQuestionID == "IsMedicare")
                                {
                                    IsMedicare = usereply.UserReply.TrueFalseResponse;
                                }
                                int n;
                                bool isPqrsNumberNumeric = int.TryParse(usereply.Question.PQRSQuestionID, out n);
                                if (isPqrsNumberNumeric == true)
                                {
                                    string QuestionType = "G";
                                    if (n > 2000)
                                    {
                                        QuestionType = "M";
                                    }
                                    values.Rows.Add(GivenChartQuestion.PQRSMeasureID == null ? 0 : GivenChartQuestion.PQRSMeasureID, QuestionType, usereply.Question.PQRSQuestionID, "", reply, "", "");
                                }
                            }

                            if (pimuserrep.SeveralChoices == true)
                            {

                                var CheckChoices = (from c in db.PIMChartQuestionChoicesUserReply
                                                    where c.ChartQuestionUserReply.ChartAnswerID == pimuserrep.ChartAnswerID
                                                    select c).ToList();

                                foreach (var choice in CheckChoices)
                                {

                                    db.DeleteObject(choice);
                                    db.SaveChanges();
                                }
                                foreach (var choices in usereply.UserReply.Choices)
                                {
                                    PIMChartQuestionChoicesUserReply chartuserreply = new PIMChartQuestionChoicesUserReply();
                                    chartuserreply.ChartQuestionUserReply = pimuserrep;
                                    chartuserreply.ChartQuestionUserReply.CtreatedDate = DateTime.Now;
                                    chartuserreply.ChartQuestionUserReply.LastUpdateDate = DateTime.Now;
                                    chartuserreply.ChartQuestionUserReply.ChartID = DatEntryID;
                                    chartuserreply.ChartQuestionUserReply.DOB = DOB;
                                    chartuserreply.ChartQuestionUserReply.StageID = StageID;
                                    chartuserreply.ChartQuestionUserReply.GroupID = GroupID;
                                    chartuserreply.ChartQuestionUserReply.VisitDate = VisitDate;
                                    chartuserreply.ChartQuestionUserReply.Gender = Gender;
                                    chartuserreply.ChartQuestionUserReply.ModuleID = ModuleID;
                                    chartuserreply.ChartQuestionUserReply.ParticipantID = ParticipantID;
                                    chartuserreply.ChartQuestionUserReply.UncompletedErrorMessage = usereply.UserReply.UncompletedErrorMessage;
                                    chartuserreply.ChartQuestionUserReply.Completed = true;
                                    chartuserreply.ChartQuestionUserReply.ParticipantModuleCycleID = ParticipantModuleCycleID;
                                    chartuserreply.PIMChartChoices = (from c in db.PIMChartChoices
                                                                      where c.ChartChoiceID == choices.ChartChoiceID
                                                                      select c).FirstOrDefault();
                                    chartuserreply.PIMChartChoices.ChartChoiceID = (int)choices.ChartChoiceID;
                                    chartuserreply.PIMChartQuestion = (from c in db.PIMChartQuestion
                                                                       where c.ChartQuestionID == usereply.Question.ChartQuestionID
                                                                       select c).FirstOrDefault();
                                    chartuserreply.PIMChartQuestion.ChartQuestionID = usereply.Question.ChartQuestionID;
                                    chartuserreply.ChartID = DatEntryID;
                                    chartuserreply.ParticipantModuleCycleID = ParticipantModuleCycleID;
                                    db.PIMChartQuestionChoicesUserReply.AddObject(chartuserreply);
                                    db.SaveChanges();

                                }
                            }
                        }
                    }

                }
                if (PQRSIn.PQRS == true)
                {
                    var Year = Convert.ToInt32(ChartInformation.DOB.Split('/')[1].ToString());
                    var Age = DateTime.Now.Year - Year;
                    //pqrsserv.PQRSSubmitChart(ParticipantID, ModuleID, DatEntryID, PQRSProgramID, PQRSGroupID, VisitDate, values, IsMedicare, Age);
                }
            }
        }

        public bool CheckIfChartIDIsUnique(int ParticipantModuleCycleID, int ModuleID, int ParticipantID, string ChartID)
        {
            var check = (from c in db.ChartQuestionUserReply
                         where c.ParticipantID == ParticipantID
                         && c.ParticipantModuleCycleID == ParticipantModuleCycleID
                         && c.ModuleID == ModuleID
                         && c.ChartID == ChartID
                         select c).FirstOrDefault();

            if (check == null)
            {
                return true;

            }
            else
            {

                return false;
            }
        }

        public List<ChartListViewModel> GetChartList(int ParticipantModuleCycleID, int ModuleID, int ParticipantID)
        {
            var GetList = (from c in db.ChartQuestionUserReply
                           where c.ParticipantModuleCycleID == ParticipantModuleCycleID
                           && c.ParticipantID == ParticipantID
                           && c.ModuleID == ModuleID
                           select new
                           {
                               c.ChartID,
                               c.CtreatedDate,
                               c.LastUpdateDate,
                               Status = (from v in db.ChartQuestionUserReply
                                         where v.ModuleID == ModuleID
                                         && v.ParticipantModuleCycleID == ParticipantModuleCycleID
                                         && v.ChartID == c.ChartID
                                         && (v.Completed == null || v.Completed == false)
                                         select v.Completed).ToList().Count() == 0 ? true : false
                           })
                                                  .ToList().Select(c => new ChartListViewModel
                                                  {
                                                      ChartID = c.ChartID,
                                                      Status = c.Status,
                                                      DateAdded = c.CtreatedDate.ToShortDateString(),
                                                      DateUpdated = c.LastUpdateDate.Value.ToShortDateString(),
                                                  }).ToList().DistinctBy(c => c.ChartID).ToList();
            return GetList;

        }

        public List<ChartQuestionModel> GetListOfQuestions(int ModuleID, int MainParticipantCycleID)
        {
            var Questions = (from c in GetChartQuestions(ModuleID, MainParticipantCycleID)
                             where c.Module.ModuleID == ModuleID
                             orderby c.SortOrder ascending
                             select new ChartQuestionModel
                             {
                                 ChartQuestion = c.ChartQuestion,
                                 ChartQuestionID = c.ChartQuestionID,
                                 ChartQuestionTypeID = c.ChartQuestionTypeID,
                                 ChartSectionName = c.ChartSectionName,
                                 CreateDate = c.CreateDate,
                                 CustomValidation = c.CustomValidation,
                                 LastUpdateDate = c.LastUpdateDate,
                                 MeasureID = c.Measure.MeasureID,
                                 ErrorMessage = c.ErrorMessage,
                                 ModuleID = c.Module.ModuleID,
                                 Required = c.Required,
                                 CustomJavascript = c.CustomJavascript,
                                 SortOrder = c.SortOrder,
                                 ToolTip = c.ToolTip
                             }).ToList();
            return Questions;
        }


        public Dictionary<int, string> GetListOfErrorMesssages(int ParticipantModuleCycleID, int ParticipantID, string ChartID)
        {
            var Messages = (from c in db.ChartQuestionUserReply
                            where c.ParticipantID == ParticipantID
                            && c.ParticipantModuleCycleID == ParticipantModuleCycleID
                            && c.ChartID == ChartID
                            && (c.Completed == null || c.Completed == false)
                            select new { c.UncompletedErrorMessage, c.PIMChartQuestion.ChartQuestionID }).ToDictionary(c => c.ChartQuestionID, c => c.UncompletedErrorMessage);
            return Messages;

        }

        public List<PIMChartQuestionValidation> GetMeasureValidations(int MeasureID)
        {
            var Validations = (from c in db.PIMChartQuestionValidations
                               where c.MeasureID == MeasureID
                               select c).ToList();

            return Validations;
        }

        public List<Measure> GetAllParticipantSelectedMeasures(int ModuleID, int MainParticipantModuleCycleID)
        {

            var Measures0 = (from c in db.Measure.Include("MeasureType").Include("Module").Include("MeasureLevel") select c).ToList();

            var Measures = (from c in
                                (from c in db.Measure.Include("MeasureType").Include("Module").Include("MeasureLevel") select c)
                            from v in db.ParticipantModuleSelection
                            where c.Module.ModuleID == ModuleID &&
                            c.Module.ModuleID == v.ModuleID &&
                            v.ParticipantModuleCycleID == MainParticipantModuleCycleID
                            select c).ToList();

            cacheservice.Set(CacheKeys.PIMUserSelectedMeasures + MainParticipantModuleCycleID, Measures);
            return Measures;
        }

        public void DeleteCharts(int ParticipantID, int ParticipantModuleCycleID, int ModuleID, string ChartID)
        {
            var DeleteChoices = (from c in db.PIMChartQuestionChoicesUserReply 
                                 where c.ParticipantModuleCycleID == ParticipantModuleCycleID 
                                 && c.ChartID == ChartID select c).ToList();
            foreach (var item in DeleteChoices)
            {
                db.PIMChartQuestionChoicesUserReply.DeleteObject(item);
                db.SaveChanges();
            }
            var DeleteQuestions = (from c in db.ChartQuestionUserReply 
                                   where c.ParticipantID == ParticipantID 
                                   && c.ParticipantModuleCycleID == ParticipantModuleCycleID 
                                   && c.ChartID == ChartID 
                                   select c).ToList();
            foreach(var item in DeleteQuestions)
            {
                db.ChartQuestionUserReply.DeleteObject(item);
                db.SaveChanges();
            }
        }

        public bool? CheckIfCached(int ModuleID)
        {
            var check = (from c in db.Module where c.ModuleID == ModuleID select c.Cached).FirstOrDefault();
            return check;
        }
    }
}
