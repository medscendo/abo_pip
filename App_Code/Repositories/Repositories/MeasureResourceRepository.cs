﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using NetHealthPIMModel;
using PIM.Models;
using PIM.ViewModels;
using QIS.Cache;
using MoreLinq;
using PIM.Services;
using System.Data;
using System.Data.Objects;
using System.Data.Common;
using System.Data.SqlClient;

namespace PIM.Repositories
{
    public class MeasureResourceRepository : IMeasureResourceRepository
    {
        private ICache cacheservice;
        public MeasureResourceRepository()
        {
            this.cacheservice = new Cache();
        }

        public dynamic GetParticipantMeasure(int ParticipantCycleId, int MainParticipantCycleID, int ModuleID)
        {
            using (NetHealthPIMEntities db = new NetHealthPIMEntities())
            {
				db.CommandTimeout = 60;
                var StageID = (from c in db.ParticipantModuleCycle
                               where c.ParticipantModuleCycleID == ParticipantCycleId
                               select c.CycleNumber).FirstOrDefault();

                int ParticipantMeasureTypeIDdefault = 1;
                var MeasuresList = (from c in db.Measure
                                    where c.Module.ModuleID == ModuleID
                                    && !string.IsNullOrEmpty(c.MeasureNumeratorSQL)
                                    && !string.IsNullOrEmpty(c.MeasureDenominatorSQL)
                                    select new
                                    {
                                        MeasureID = c.MeasureID,
                                        ModuleID = c.Module.ModuleID,
                                        IsNumericMeasure = c.IsNumericMeasure,
                                        MeasureDenominatorSQL = c.MeasureDenominatorSQL,
                                        Measure25Percentile = c.TwentyFifthPercentileSQL,
                                        Measure75Percentile = c.SeventyFifthPercentileSQL,
                                        MeasureNumeratorSQL = c.MeasureNumeratorSQL
                                    }
                                      ).ToList();

                foreach (var item in MeasuresList)
                {
                    string Denominator = item.MeasureDenominatorSQL.Replace("@ParticipantModuleCycleID", ParticipantCycleId.ToString()).Replace("@ModuleID", item.ModuleID.ToString());
                    string Numerator = item.MeasureNumeratorSQL.Replace("@ParticipantModuleCycleID", ParticipantCycleId.ToString()).Replace("@ModuleID", item.ModuleID.ToString());
                    string CountDenominator = "0";
                    string CountNumerator = "0";
                    try
                    {
                        var CountDenominatorDecimal = db.ExecuteStoreQuery<int?>(Denominator).FirstOrDefault();
                        CountDenominator = CountDenominatorDecimal == null ? "0" : CountDenominatorDecimal.ToString();
                    }
                    catch
                    {
                        var CountDenominatorDecimal = db.ExecuteStoreQuery<decimal?>(Denominator).FirstOrDefault();
                        CountDenominator = CountDenominatorDecimal == null ? "0" : CountDenominatorDecimal.ToString();
                    }
                    try
                    {
                        var CountNumeratorInt = db.ExecuteStoreQuery<int?>(Numerator).FirstOrDefault();
                        CountNumerator = CountNumeratorInt == null ? "0" : CountNumeratorInt.ToString();
                    }
                    catch
                    {
                        var CountNumeratorDecimal = db.ExecuteStoreQuery<decimal?>(Numerator).FirstOrDefault();
                        CountNumerator = CountNumeratorDecimal == null ? "0" : CountNumeratorDecimal.ToString();
                    }
                    if (item.IsNumericMeasure == true)
                    {
                        var check = (from c in db.ParticipantMeasure
                                     where c.ParticipantModuleCycleID == ParticipantCycleId
                                     && c.MeasureID == item.MeasureID
                                     && c.ParticipantMeasureTypeID == ParticipantMeasureTypeIDdefault
                                     select c).FirstOrDefault();
                        bool insert = false;
                        if (check == null)
                        {
                            check = new ParticipantMeasure();
                            insert = true;
                        }
                        check.MeasureID = item.MeasureID;
                        check.MeasureNumerator = Convert.ToInt32(Math.Round(Convert.ToDouble(CountNumerator), 0));
                        check.MeasurePercent = Convert.ToInt32(Math.Round(Convert.ToDouble(CountNumerator), 0));
                        check.AbstractRecordsIncluded = Convert.ToInt32(Math.Round(Convert.ToDouble(CountDenominator), 0));
                        check.MeasureDenominator = Convert.ToInt32(Math.Round(Convert.ToDouble(CountDenominator), 0));
                        check.ParticipantModuleCycleID = ParticipantCycleId;
                        check.ParticipantMeasureTypeID = ParticipantMeasureTypeIDdefault;

                        if (insert == true)
                        {
                            check.LastUpdateDate = DateTime.Now;
                            db.ParticipantMeasure.AddObject(check);
                        }
                        db.SaveChanges();
                    }
                    else
                    {
                        SqlParameter param1 = new SqlParameter("@ParticipantModuleCycleID", ParticipantCycleId.ToString());
                        SqlParameter param2 = new SqlParameter("@Measure_ID", item.MeasureID.ToString());
                        SqlParameter param3 = new SqlParameter("@ParticipantMeasure_TypeId", ParticipantMeasureTypeIDdefault);
                        SqlParameter param4 = new SqlParameter("@Records_Included", CountDenominator.ToString());
                        SqlParameter param5 = new SqlParameter("@Measure_Numerator", CountNumerator.ToString());
                        SqlParameter param7 = new SqlParameter("@Measure_Denominator", CountDenominator.ToString());
                        db.ExecuteStoreCommand("Record_CycleMeasures @ParticipantModuleCycleID, @Measure_ID, @ParticipantMeasure_TypeId, @Records_Included, @Measure_Numerator, @Measure_Denominator", param1, param2, param3, param4, param5, param7);
                    }

                }


                foreach (var item in MeasuresList)
                {
                    string Denominator = item.MeasureDenominatorSQL.Replace("ParticipantModuleCycleID =", "StageID =").Replace("ParticipantModuleCycleID=", "StageID=").Replace("@ParticipantModuleCycleID", "@StageID").Replace("@StageID", StageID.ToString()).Replace("@ModuleID", item.ModuleID.ToString());
                    string Numerator = item.MeasureNumeratorSQL.Replace("ParticipantModuleCycleID =", "StageID =").Replace("ParticipantModuleCycleID=", "StageID=").Replace("@ParticipantModuleCycleID", "@StageID").Replace("@StageID", StageID.ToString()).Replace("@ModuleID", item.ModuleID.ToString());
                    string CountDenominator = "0";
                    string CountNumerator = "0";
                    try
                    {
                        var CountDenominatorDecimal = db.ExecuteStoreQuery<int?>(Denominator).FirstOrDefault();
                        CountDenominator = CountDenominatorDecimal == null ? "0" : CountDenominatorDecimal.ToString();
                    }
                    catch
                    {
                        var CountDenominatorDecimal = db.ExecuteStoreQuery<decimal?>(Denominator).FirstOrDefault();
                        CountDenominator = CountDenominatorDecimal == null ? "0" : CountDenominatorDecimal.ToString();
                    }
                    try
                    {
                        var CountNumeratorInt = db.ExecuteStoreQuery<int?>(Numerator).FirstOrDefault();
                        CountNumerator = CountNumeratorInt == null ? "0" : CountNumeratorInt.ToString();
                    }
                    catch
                    {
                        var CountNumeratorDecimal = db.ExecuteStoreQuery<decimal?>(Numerator).FirstOrDefault();
                        CountNumerator = CountNumeratorDecimal == null ? "0" : CountNumeratorDecimal.ToString();
                    }

                    if (item.IsNumericMeasure == true)
                    {
                        string CountMeasure25Percentile = "0";
                        string CountMeasure75Percentile = "0";

                        if (!string.IsNullOrEmpty(item.Measure25Percentile) && !string.IsNullOrEmpty(item.Measure75Percentile))
                        {
                            string Measure25Percentile = item.Measure25Percentile.Replace("@ParticipantModuleCycleID", ParticipantCycleId.ToString()).Replace("@ModuleID", item.ModuleID.ToString());
                            string Measure75Percentile = item.Measure75Percentile.Replace("@ParticipantModuleCycleID", ParticipantCycleId.ToString()).Replace("@ModuleID", item.ModuleID.ToString());
                            CountMeasure25Percentile = db.ExecuteStoreQuery<decimal?>(Measure25Percentile).FirstOrDefault().ToString();
                            CountMeasure75Percentile = db.ExecuteStoreQuery<decimal?>(Measure75Percentile).FirstOrDefault().ToString();
                        }

                        var check = (from c in db.MeasureAggregate
                                     where c.ModuleID == ModuleID
                                     && c.Measure.MeasureID == item.MeasureID
                                     select c).FirstOrDefault();
                        bool insert = false;
                        if (check == null)
                        {
                            check = new MeasureAggregate();
                            insert = true;
                        }
                        check.Measure = (from c in db.Measure where c.MeasureID == item.MeasureID select c).FirstOrDefault();
                        check.MeasureNumerator = Convert.ToInt32(Math.Round(Convert.ToDouble(CountNumerator), 0));
                        check.MeasurePercent = Convert.ToInt32(Math.Round(Convert.ToDouble(CountNumerator), 0));
                        check.AbstractRecordsIncluded = Convert.ToInt32(Math.Round(Convert.ToDouble(CountDenominator), 0));
                        check.MeasureDenominator = Convert.ToInt32(Math.Round(Convert.ToDouble(CountDenominator), 0));
                        if (!string.IsNullOrEmpty(item.Measure25Percentile) && !string.IsNullOrEmpty(item.Measure75Percentile)) ;
                        {
                            check.Peer75Percentile = Math.Round(Convert.ToDouble(string.IsNullOrEmpty(CountMeasure75Percentile) ? "0" : CountMeasure75Percentile), 0);
                            check.Peer25Percentile = Math.Round(Convert.ToDouble(string.IsNullOrEmpty(CountMeasure25Percentile) ? "0" : CountMeasure25Percentile), 0);
                        }
                        check.ModuleID = ModuleID;

                        if (insert == true)
                        {
                            check.LastUpdateDate = DateTime.Now;
                            db.MeasureAggregate.AddObject(check);

                        }
                        db.SaveChanges();
                    }
                    else
                    {

                        SqlParameter param1 = new SqlParameter("@ModuleID", ModuleID.ToString());
                        SqlParameter param2 = new SqlParameter("@Measure_ID", item.MeasureID.ToString());
                        SqlParameter param4 = new SqlParameter("@Records_Included", CountDenominator.ToString());
                        SqlParameter param5 = new SqlParameter("@Measure_Numerator", CountNumerator.ToString());
                        SqlParameter param7 = new SqlParameter("@Measure_Denominator", CountDenominator.ToString());
                        db.ExecuteStoreCommand("Record_CycleMeasuresPeers @ModuleID, @Measure_ID,  @Records_Included, @Measure_Numerator, @Measure_Denominator", param1, param2, param4, param5, param7);
                    }

                }



                var Mesures = (from c in db.ParticipantMeasure
                               from n in db.Measure
                               where c.MeasureID == n.MeasureID
                               where c.ParticipantModuleCycleID == ParticipantCycleId
                               select new { c, n }).ToList();

                return Mesures;
            }


        }
    }
}