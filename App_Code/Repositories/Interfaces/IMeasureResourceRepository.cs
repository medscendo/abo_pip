﻿using System;
using System.Collections.Generic;
using PIM.Models;
using PIM.ViewModels;


namespace PIM.Repositories
{
    public interface IMeasureResourceRepository 
    {
        dynamic GetParticipantMeasure(int ParticipantCycleId, int MainParticipantCycleID, int ModuleID);
    }
}
