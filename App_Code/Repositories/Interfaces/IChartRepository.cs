﻿using System;
using NetHealthPIMModel;
using System.Collections.Generic;
using PIM.ViewModels;
using PIM.Models;


namespace PIM.Repositories
{
    public interface IChartRepository 
    {
        int? GetChartsConsidered(int ParticipantID, int MeasureID);
        int? GetNumberOfRequiredCharts(int moduleid, int? groupID);
        bool IsChartCompleted(int ParticipantModuleCycleID, int ModuleID, int? GroupID);
        int GetNewNumberOfComcletedCharts(int cycleid, int ParticipantID, int ModuleID);
        int GetNumberOfComcletedCharts(int cycleid);
        bool PromtUserforMoreCharts(int ParticipantID, int ParticipantModuleID, int? GroupID, int ModuleID);
        void SetChartStatus(string ChartID, int ParticipantModuleCcyleID, int ModuleID, bool status);
        List<ChartAbstractionViewModel> GetChartQuestionsForEachMeasure(int ModuleID, int ParticipantModuleCycleID, int MainParticipantCycleID, int ParticipantID, string ChartID, bool? IsPQRS);
        void InsertChart(List<ChartAbstractionViewModel> model, int ModuleID, int ParticipantModuleCycleID, int ParticipantID, string DatEntryID, DateTime? VisitDate, DateTime? DOB, int? Gender, int StageID, int? GroupID);
        bool CheckIfChartIDIsUnique(int ParticipantModuleCycleID, int ModuleID, int ParticipantID, string ChartID);
        List<ChartListViewModel> GetChartList(int ParticipantModuleCycleID, int ModuleID, int ParticipantID);
        List<ChartQuestionModel> GetListOfQuestions(int ModuleID, int MainParticipantCycleID);
        Dictionary<int, string> GetListOfErrorMesssages(int ParticipantModuleCycleID, int ParticipantID, string ChartID);
        List<PIMChartQuestionValidation> GetMeasureValidations(int MeasureID);
        List<Measure> GetAllParticipantSelectedMeasures(int ModuleID, int MainParticipantModuleCycleID);
        void DeleteCharts(int ParticipantID, int ParticipantModuleCycleID, int ModuleID, string ChartID);
       
    }
}
