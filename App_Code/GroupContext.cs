﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using NetHealthPIMModel;

/// <summary>
/// Summary description for GroupContext
/// </summary>
public class GroupContext
{
    public GroupContext()
    {
    }

    public static void Initialize(int groupID)
    {
        ParticipantGroup ctx = (ParticipantGroup)HttpContext.Current.Session[Constants.SESSION_GROUPCONTEXT];
        if (ctx == null || ctx.ParticipantGroupID != groupID)
        {
            // Load static module instance to session.
            using (NetHealthPIMEntities pimEntities = new NetHealthPIMEntities())
            {
                var module = (from m in pimEntities.ParticipantGroup
                              where m.ParticipantGroupID == groupID
                              select m).First<ParticipantGroup>();
                HttpContext.Current.Session[Constants.SESSION_GROUPCONTEXT] = module;
            }
        }
    }

    public static ParticipantGroup getGroup()
    {
        ParticipantGroup ctx = (ParticipantGroup)HttpContext.Current.Session[Constants.SESSION_GROUPCONTEXT];
        return ctx;
    }

    public static ParticipantGroup getGroup(int GroupID)
    {
        Initialize(GroupID);
        ParticipantGroup ctx = (ParticipantGroup)HttpContext.Current.Session[Constants.SESSION_GROUPCONTEXT];
        return ctx;
    }
}
