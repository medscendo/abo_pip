﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using NetHealthPIMModel;

/// <summary>
/// Summary description for UserContext
/// </summary>
public class UserContext
{
    private int participantID;
    private String participantName;
    private bool isAuthenticated;
    private int activeModuleID;
    private int activeModuleCycleID;
    private int roleTypeID;
    private int participantGroupID;
    private int leaderParticipantID;
    private int minAbstractsRequired;
    private int workingModuleID;
    private int softwareVersion;
    private int? _PrevParticipantModuleCycleID; 
    public UserContext()
    {
    }

    public int ParticipantID
    {
        get { return participantID; }
        set { participantID = value; }
    }

    public String ParticipantName
    {
        get { return participantName; }
        set { participantName = value; }
    }

    public bool IsAuthenticated
    {
        get { return isAuthenticated; }
        set { isAuthenticated = value; }
    }

    public int ActiveModuleID
    {
        get { return activeModuleID; }
        set { activeModuleID = value; }
    }

    public int ActiveModuleCycleID
    {
        get { return activeModuleCycleID; }
        set { activeModuleCycleID = value; }
    }

    public int RoleTypeID
    {
        get { return roleTypeID; }
        set { roleTypeID = value; }
    }

    public int ParticipantGroupID
    {
        get { return participantGroupID; }
        set { participantGroupID = value; }
    }

    public int LeaderParticipantID
    {
        get { return leaderParticipantID; }
        set { leaderParticipantID = value; }
    }

    public int MinAbstractsRequired
    {
        get { return minAbstractsRequired; }
        set { minAbstractsRequired = value; }
    }

    public int WorkingModuleID
    {
        get { return workingModuleID; }
        set { workingModuleID = value; }
    }
    public int SoftwareVersion
    {
        get { return softwareVersion; }
        set { softwareVersion = value; }
    }

    public int? PrevParticipantModuleCycleID
    {
        get { return _PrevParticipantModuleCycleID; }
        set { _PrevParticipantModuleCycleID = value; }
    }

}
