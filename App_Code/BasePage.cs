﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using NetHealthPIMModel;

/// <summary>
/// Summary description for BasePage
/// </summary>
public class BasePage : System.Web.UI.Page
{
    public UserContext ctx { get; set; }
    public Module module { get; set; }

	public BasePage()
	{
		//
		// TODO: Add constructor logic here
		//
	}

    protected override void OnPreInit(EventArgs e)
    {
        base.OnPreInit(e);

        //if (Session != null)
        //{
        //    ctx = (UserContext)Session[Constants.SESSION_USERCONTEXT];
        //    module = ModuleContext.getModule();

        if (Session.IsNewSession)
        {
            string cookie = Request.Headers["Cookie"];
            //if (cookie != null)
            //    cookie = null;
            if ((null != cookie) && (cookie.IndexOf("ASP.NET_SessionId") >= 0))
                Response.Redirect("~/SessionExpired.aspx");
        }
        //}

        module = ModuleContext.getModule();
        if (module == null)
        {
            int moduleId = Constants.MODULE_COPD;
            String mid = Request.Params["mid"];
            if (mid != null)
            {
                moduleId = Int32.Parse(mid);
            }
            ModuleContext.Initialize(moduleId);
            module = ModuleContext.getModule();
        }
        this.Page.Theme = module.ModuleCode.ToLower();
        ctx = (UserContext)Session[Constants.SESSION_USERCONTEXT];
        Session[Constants.SESSION_PAGE_ROLETYPE_LEVEL] = Constants.ROLETYPEID_PARTICIPANT;

        if (ctx != null && ctx.IsAuthenticated)
        {
            if (ctx.ActiveModuleCycleID <= 0)
            {
                CycleManager.CheckCycle();
            }
        }
    }

    public string GetDefaultRedirectByRole(int RoleTypeID)
    {
        string ret = "~/" + module.ModuleCode.ToLower() + "/Default.aspx";
        if (RoleTypeID == Constants.ROLETYPEID_PARTICIPANT)
        {
            ret = "~/" + module.ModuleCode.ToLower() + "/PIPStatus.aspx";
        }
        else if (RoleTypeID == Constants.ROLETYPEID_CLIENT_ADMIN)
        {
            ret = "~/sa/Administration.aspx";
        }
        else if (RoleTypeID == Constants.ROLETYPEID_SYSTEM_ADMIN)
        {
            ret = "~/sa/SystemAdministration.aspx";
        }
        return ret;
    }
}
