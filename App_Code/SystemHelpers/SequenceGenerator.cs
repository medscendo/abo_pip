﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Data.EntityClient;
using System.Data;

/// <summary>
/// Summary description for SequenceGenerator
/// </summary>
public class SequenceGenerator
{
    public static int GetNextSequenceValue(string SequenceName)
    {
        using (EntityConnection conn = new EntityConnection("name=NetHealthPIMEntities"))
        {
            Int32 ret = 0;
            conn.Open();
            EntityCommand cmd = conn.CreateCommand();
            cmd.CommandText = "NetHealthPIMEntities.GetSequenceNextValue";
            cmd.CommandType = CommandType.StoredProcedure;
            cmd.Parameters.AddWithValue("SequenceName", SequenceName);
            cmd.Parameters.AddWithValue("NextValue", ret);
            cmd.ExecuteNonQuery();
            ret = (Int32)cmd.Parameters[1].Value;
            conn.Close();
            return ret;
        }
    }
}
