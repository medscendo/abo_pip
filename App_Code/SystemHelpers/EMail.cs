﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Net.Mail;

/// <summary>
/// Summary description for EMail
/// </summary>
public class EMail
{
	public EMail()
	{
	}

    public static void SendEMail(string fromEMail, string toEMail, bool isHTML, string subject, string message)
    {
        string Bcc = System.Configuration.ConfigurationSettings.AppSettings["Bcc"];
        MailAddress mabcc = new MailAddress(Bcc);
        MailMessage msg = new MailMessage(fromEMail, toEMail);
        msg.IsBodyHtml = isHTML;
        msg.Subject = subject;
        msg.Body = message;
        msg.Bcc.Add(mabcc);
        SmtpClient smtp = new SmtpClient();
        smtp.Send(msg);
    }

}
