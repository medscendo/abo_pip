﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Data.Objects;

/// <summary>
/// Summary description for Support
/// </summary>
public class Support
{

    public static string TraceLinqSQL(IQueryable linq)
    {
        var q = (ObjectQuery)linq;
        return q.ToTraceString();
    }

    public static string LegacyConnectionString()
    {
        return System.Configuration.ConfigurationManager.ConnectionStrings["NetHealthLegacy"].ConnectionString;
    }
}
