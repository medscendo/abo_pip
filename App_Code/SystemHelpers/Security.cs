﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Text;
using System.Security.Cryptography;

/// <summary>
/// Summary description for Security
/// </summary>
public class Security
{
    public static string EncryptPassword(string userName, string password)
    {
        string tmpPassword = null;
        string filler = "123456789";
        string salt = userName.ToLower();
        if (salt.Length < 10)
        {
            salt = salt + filler.Substring(0, 10 - salt.Length);
        }
        tmpPassword = salt + password;

        //Convert the password string into an Array of bytes.
        UTF8Encoding textConverter = new UTF8Encoding();
        byte[] passBytes = textConverter.GetBytes(tmpPassword);

        //Return the encrypted bytes
        byte[] bytes = new SHA384Managed().ComputeHash(passBytes);
        return ByteArrayToStr(bytes);
    }

    // C# to convert a string to a byte array.
    public static byte[] StrToByteArray(string str)
    {
        ASCIIEncoding encoding = new ASCIIEncoding();
        return encoding.GetBytes(str);
    }

    public static string ByteArrayToStr(byte[] bytes)
    {
        ASCIIEncoding enc = new System.Text.ASCIIEncoding();
        return enc.GetString(bytes);
    }

}
