﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using NetHealthPIMModel;
using System.Data.EntityClient;
using System.Data;
/// <summary>
/// Summary description for Validation
/// </summary>
public class Validation
{
    NetHealthPIMEntities pim = new NetHealthPIMEntities();
	public Validation()
	{
		//
		// TODO: Add constructor logic here
		//
	}

    public static string validateRegistrationMinAge(int initial, int DOB,  int RegistrationMinAge, string error)
    {

        string errormessage="";
        if ((initial - DOB) >= RegistrationMinAge)
        {
            errormessage = "";
        }
        else
        {
            errormessage = error;
        }
        return errormessage;
    
    
    }
    public static string validateRegistrationMaxAge(int initial, int DOB, int RegistrationMaxAge, string error)
    {

        string errormessage = "";
        if ((initial - DOB) <= RegistrationMaxAge)
        {
            errormessage = "";
        }
        else
        {
            errormessage = error;

        }
        return errormessage;


    }
    public static string validateRegistrationInitialVisitMonth(DateTime todaylast, DateTime  initial, int RegistrationInitialVisitMonths, string error)
    {
        int years = todaylast.Year-initial.Year;
        int months = todaylast.Month - initial.Month;
        int intitialm = todaylast.Month;
        if (years > 0)
        {
            intitialm = (years * 12)+months;
        }
        string errormessage = "";
        
        if ((intitialm - initial.Month) <= RegistrationInitialVisitMonths)
        {
            errormessage = "";
        }
        else
        {
            errormessage = error;
        }
        return errormessage;


    }
    public static string validateRegistrationFolowUpMonth(DateTime   todaylast, DateTime initial, int RegistrationFollowupMonths, string error)
    {
        int years = initial.Year - todaylast.Year;
        int intitialm = initial.Month;
        int somemonth = 0;
        if (years > 0)
        {
            intitialm =(years * 12);
        }
        string errormessage = "";
        if (initial.Month != todaylast.Month)
        {
            somemonth = todaylast.Month;
        }
        if(years>0)
        {
        if (initial.Month > todaylast.Month)
        {
            intitialm += initial.Month - todaylast.Month;

        }
        if (initial.Month < todaylast.Month)
        {
            intitialm -= todaylast.Month - initial.Month;

        }
    }
        if (intitialm >= RegistrationFollowupMonths)
        {
            errormessage = "";
        }
        else
        {
            errormessage = error;
        }
        return errormessage;


    }







}
