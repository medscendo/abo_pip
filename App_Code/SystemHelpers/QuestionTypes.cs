﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace PIM.SystemHelpers
{
    public enum QuestionTypes
    {
        TF=1,
        SHORT=2,
        CHOOSE1=3,
        CHOOSEN=4,
        NUMBER=5,
        NA=6,
        RATING=7,
        DROP=14,
        DATETIME=8
    }
}