﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

using NetHealthPIMModel;
using System.Data.Objects;

/// <summary>
/// Summary description for CustomSiteMap
/// </summary>
public class CustomSiteMap : StaticSiteMapProvider
{
    private SiteMapNode parentNode
    {
        get;
        set;
    }

    public override SiteMapNode BuildSiteMap()
    {

        lock (this)
        {
            UserContext ctx = (UserContext)HttpContext.Current.Session[Constants.SESSION_USERCONTEXT];
            bool isAuthenticated = false;
            if (ctx != null)
            {
                isAuthenticated = ctx.IsAuthenticated;
            }
            Module moduleContext = ModuleContext.getModule();
            int moduleId = 0;
            if (moduleContext != null)
            {
                moduleId = moduleContext.ModuleID;
            }

            // TODO: Use session variables to retrieve sitemap private and public. 
            base.Clear();
            using (NetHealthPIMEntities pimEntities = new NetHealthPIMEntities())
            {
                
                parentNode = new SiteMapNode(this, "0");
                ObjectQuery<ModuleSiteMap> moduleSiteMap = pimEntities.ModuleSiteMap;
                IQueryable<ModuleSiteMap> moduleSiteMapQuery;
                if (isAuthenticated) {

                    // Determine if the participant is also "runnng" a module. 
                    var partInModule = (from pm in pimEntities.ParticipantModule
                                        where pm.ModuleID == moduleId && pm.ParticipantID == ctx.ParticipantID
                                        select pm).Count();
                    if (ctx.RoleTypeID > Constants.ROLETYPEID_PARTICIPANT && partInModule > 0)
                    {
                        // Combine both the admin and user settings if the Participant/User has admin permissions and is registered in a module
                        moduleSiteMapQuery = from msm in moduleSiteMap
                                             where msm.Module.ModuleID == moduleId && (msm.RoleTypeID == ctx.RoleTypeID || msm.RoleTypeID == Constants.ROLETYPEID_PARTICIPANT)
                                             orderby msm.ModuleSiteMapSortOrder
                                             select msm;
                    }
                    else
                    {
                        moduleSiteMapQuery = from msm in moduleSiteMap
                                             where msm.Module.ModuleID == moduleId && msm.RoleTypeID == ctx.RoleTypeID
                                             orderby msm.ModuleSiteMapSortOrder
                                             select msm;
                    }
                }
                else {
                    moduleSiteMapQuery = from msm in moduleSiteMap
                    where msm.Module.ModuleID == moduleId && msm.RoleTypeID == Constants.ROLETYPEID_GUEST
                    orderby msm.ModuleSiteMapSortOrder
                    select msm;
                }

                foreach (var row in moduleSiteMapQuery)
                {
                    SiteMapNode node = new SiteMapNode(this,
                        row.ModuleSiteMapCode,
                        row.ModuleSiteMapURL,
                        row.ModuleSitemapTitle,
                        row.ModuleSiteMapDescription);
                    AddNode(node, parentNode);
                }
            }

            return parentNode;
        }
    }

    
    protected override SiteMapNode GetRootNodeCore()
    {
        return BuildSiteMap();
    }

}
