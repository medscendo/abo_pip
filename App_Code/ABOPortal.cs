﻿using System;
using System.Collections.Generic;
using System.Web;
using System.Xml;
using NetHealthPIMModel;

/// <summary>
/// Summary description for ABOPortal
/// </summary>
public class ABOPortal
{
	public ABOPortal()
	{
		//
		// TODO: Add constructor logic here
		//
	}
    public enum PSMStatus
    {
        Registered = 1,
        Passed = 2
    }
    public bool updatePatientSafetyModule(string ParticipantGUID, string CandidateID, ParticipantModuleCycle cycle, Participant participant, PSMStatus newStatus)
    {
        DateTime currentDate = System.DateTime.Now;

        PSMM.PatientSafetyModuleManager proxyABO_PSMM = new PSMM.PatientSafetyModuleManager();
        string xmlpreviousStatus = proxyABO_PSMM.GetPatientSafetyModuleRecordStatus(ParticipantGUID);
        string currentValue = getXMLReturnValues(xmlpreviousStatus, "/PatientSafetyModuleWebServiceResult/recordStatus");
        string retXML = "";
        bool errorFound = false;
        string errorMsg = "";

        switch (currentValue)
        {
            case "RecordDoesNotExist":
                if (newStatus == PSMStatus.Registered)
                    retXML = proxyABO_PSMM.UpdatePatientSafetyModuleRecord(CandidateID, ParticipantGUID, PSMM.PatientSafetyModuleGradeEnum.Registered, currentDate, cycle.ParticipantProfileCompetedDate);
                else
                {
                    errorMsg = "[" + currentValue + "] " + "ParticipantGUID: " + ParticipantGUID + " record does not exists.";
                    errorFound = true;
                }
                break;
            case "Registered":
                if (newStatus == PSMStatus.Passed)
                    retXML = proxyABO_PSMM.UpdatePatientSafetyModuleRecord(CandidateID, ParticipantGUID, PSMM.PatientSafetyModuleGradeEnum.Passed, currentDate, cycle.ParticipantProfileCompetedDate);
                break;
            case "Complete":
                break;
            default:
                if (newStatus == PSMStatus.Registered)
                    retXML = proxyABO_PSMM.UpdatePatientSafetyModuleRecord(CandidateID, ParticipantGUID, PSMM.PatientSafetyModuleGradeEnum.Registered, currentDate, cycle.ParticipantProfileCompetedDate);
                else
                {
                    errorMsg = "[" + currentValue + "] " + " Error found while verifying participant status.  ParticipantGUID: " + ParticipantGUID + ".";
                    errorFound = true;
                }
                break;
        }

        if (retXML != "")
        {
            string updatedValue = "";
            updatedValue = getXMLReturnValues(retXML, "/PatientSafetyModuleWebServiceResult/updateResultStatus");
            switch (updatedValue)
            {
                case "Success":
                    break;
                case "Failure_SystemError":
                    errorMsg = "[" + updatedValue + "] " + " Error found while updating participant status. ParticipantGUID: " + ParticipantGUID + ".";
                    errorFound = true;
                    break;
                case "Failure_RecordDoesNotExist":
                    errorMsg = "[" + updatedValue + "] " + " Error found while updating participant status. ParticipantGUID: " + ParticipantGUID + ".";
                    errorFound = true;
                    break;
                default:
                    errorMsg = "[" + updatedValue + "] " + " Error found while updating participant status. ParticipantGUID: " + ParticipantGUID + ".";
                    errorFound = true;
                    break;
            }
        }
        if (errorFound)
        {
            string fromEMail = participant.ParticipantEmailAddress;
            string toEMail = System.Configuration.ConfigurationManager.AppSettings["EmailTo"];
            bool isHTML = true;
            string subject = "Patient Safety Activity Update Error";
            string message = errorMsg;
            
            EMail.SendEMail(fromEMail, toEMail, isHTML, subject, message);
        }

        return (errorFound == false);
    }

    public string getXMLReturnValues(string xmlDocument, string xmlNodeName)
    {
        XmlDocument xml = new XmlDocument();
        xml.LoadXml(xmlDocument);
        XmlNodeList xnList = xml.SelectNodes(xmlNodeName);
        XmlNode xmlnode = xml.SelectSingleNode(xmlNodeName);
        string status = xmlnode.InnerText;

        return status;
    }

}
