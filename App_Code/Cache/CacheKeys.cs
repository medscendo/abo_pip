﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace QIS.Cache
{
    public static class CacheKeys
    {
        public const int CacheDays = 1;
        public const int CacheHours = 10;
        public const int CacheMinutes = 0;
        public const string PIMChartQuestions = "PIMChartQuestions";
        public const string PIMChartChoices = "PIMChartChoices";
        public const string PIMMeasures = "PIMMeasures";
        public const string PIMResources = "PIMResources";
        public const string PIMModuleMeasures = "PIMModuleMeasures";
        public const string PIMModuleInfo = "PIMModuleInfo";
        public const string PIMUserSelectedMeasures = "PIMUserSelectedMeasures";
    }
}