﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

/// <summary>
/// Summary description for ABOCheckBoxList
/// </summary>
public class ABOCheckBoxList : CheckBoxList
{
    private bool isFirstItem = true;

    protected override void RenderItem(ListItemType itemType, int repeatIndex, RepeatInfo repeatInfo, HtmlTextWriter writer)
    {
        if (isFirstItem)
        {
            
            var writerStub = new HtmlTextWriter(new StringWriter());
            base.RenderItem(itemType, repeatIndex, repeatInfo, writerStub);
            isFirstItem = false;
        }

        var ChkButton = this.Controls[0] as  CheckBox;
        ChkButton.InputAttributes.Clear();
        var item = Items[repeatIndex];
        foreach (string attribute in item.Attributes.Keys)
        {
            ChkButton.InputAttributes.Add(attribute, item.Attributes[attribute]);
        }
        item.Attributes.Clear();

        base.RenderItem(itemType, repeatIndex, repeatInfo, writer);

    }
   

}