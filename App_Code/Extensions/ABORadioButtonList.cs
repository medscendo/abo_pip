﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

/// <summary>
/// Summary description for ABORadioButtonList
/// </summary>
public class ABORadioButtonList : RadioButtonList
{
    private bool isFirstItem = true;

    protected override void RenderItem(ListItemType itemType, int repeatIndex, RepeatInfo repeatInfo, HtmlTextWriter writer)
    {
        if (isFirstItem)
        {
            // this.ControlToRepeat will be created during this first call, and then it will be placed into Controls[0], so  we can get it from here and update for each item.
            var writerStub = new HtmlTextWriter(new StringWriter());
            base.RenderItem(itemType, repeatIndex, repeatInfo, writerStub);
            isFirstItem = false;
        }

        var radioButton = this.Controls[0] as RadioButton;

        radioButton.InputAttributes.Clear();
        var item = Items[repeatIndex];
        foreach (string attribute in item.Attributes.Keys)
        {
            radioButton.InputAttributes.Add(attribute, item.Attributes[attribute]);
        }
        // if you want to clear attributes for top element, in that case it's a span, then you need to call
        item.Attributes.Clear();

        base.RenderItem(itemType, repeatIndex, repeatInfo, writer);

    }
}