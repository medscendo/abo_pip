﻿using System;
using System.Data;
namespace PIM.Services
{
    public interface IPQRSService
    {
        int CreateGetPqrsUser(int participantID, int ModuleID, int ProgramID, int GroupID);
        bool PQRSSubmitChart(int participantID, int ModuleID, string PatientID, int ProgramiD, int GroupID, DateTime? VisitDate, DataTable AnsverValues, bool? IsMedicare, int Age);
    }
}
