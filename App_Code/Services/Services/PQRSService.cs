﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Web;
using NetHealthPIMModel;
using PIM.Repositories;

namespace PIM.Services
{
    public class PQRSService : IPQRSService
    {

         public PQRSService()
         {
           
         }




         public int CreateGetPqrsUser(int participantID, int ModuleID, int ProgramID, int GroupID)
         {
             int PQRSParticipantID = 0;

             using (PQRSWebService.PPWebServiceClient client = new PQRSWebService.PPWebServiceClient())
             {
                using (NetHealthPIMEntities pim = new NetHealthPIMEntities())
                {
                    var participant = (from c in pim.Participant 
                                       where c.ParticipantID == participantID 
                                       select new 
                                       { 
                                          UserName= c.ParticipantEmailAddress, 
                                          LastName= c.ParticipantLastName, 
                                          FirstName= c.ParticipantFirstName

                                       }).FirstOrDefault();

                    var moduleinfo = (from c in pim.Module
                                      where c.ModuleID == ModuleID 
                                      select new
                                      { 
                                          c.ModuleID, 
                                          c.PQRSYear, 
                                          c.PQRSEnabled,
                                          c.PQRSGroupID, 
                                          c.PQRSMeasureID,
                                          c.PQRSProgramID 

                                      }).FirstOrDefault();

                 int PqrsYear = DateTime.Now.Year;
               


                 client.ClientCredentials.UserName.UserName = "partner:2016";
                 client.ClientCredentials.UserName.Password = "48C1A443-54B0-4D00-A4A2-FE5E46C2F02E";

                 if (!string.IsNullOrEmpty(moduleinfo.PQRSYear))
                 {
                     PqrsYear = Convert.ToInt32(moduleinfo.PQRSYear);
                 }
                 PQRSParticipantID = client.GetParticipantID2(participant.UserName, participant.LastName);
                     if (PQRSParticipantID == 0)
                     {
                         PQRSParticipantID = client.AddUser(GroupID, "0", participant.FirstName, participant.LastName, participant.UserName, participant.UserName, participant.LastName, "AAPA");
                     }
                 }
             }
             return PQRSParticipantID;
         }






         public bool PQRSSubmitChart(int participantID, int ModuleID, string PatientID, int ProgramiD, int GroupID, DateTime? VisitDate, DataTable AnsverValues, bool? IsMedicare, int Age)
         {
             using (NetHealthPIMEntities pim = new NetHealthPIMEntities())
             {
                 int PQRSParticipantID = CreateGetPqrsUser(participantID, ModuleID, ProgramiD, GroupID);

                 var moduleinfo = (from c in pim.Module
                                   where c.ModuleID == ModuleID
                                   select new
                                   {
                                       c.ModuleID,
                                       c.PQRSYear,
                                       c.PQRSEnabled,
                                       c.PQRSGroupID,
                                       c.PQRSMeasureID,
                                       c.PQRSProgramID

                                   }).FirstOrDefault();

                 if (PQRSParticipantID != 0)
                 {
                     try
                     {
                         using (PQRSWebService.PPWebServiceClient client = new PQRSWebService.PPWebServiceClient())
                         {
                             client.ClientCredentials.UserName.UserName = "partner:2016";
                             client.ClientCredentials.UserName.Password = "48C1A443-54B0-4D00-A4A2-FE5E46C2F02E";

                             bool Medicare = false;
                             string VisitDateString = "";
                             if (VisitDate != null)
                             {
                                 VisitDateString = VisitDate.Value.ToShortDateString();
                             }
                             if (IsMedicare != null)
                             {
                                 Medicare = Convert.ToBoolean(IsMedicare);
                             }
                             client.AddChart((Int32)PQRSParticipantID, GroupID, PatientID, VisitDateString, Medicare, AnsverValues);
                         }
                         return true;
                     }
                     catch
                     {
                         return false;

                     }
                 }
                 else
                 {
                     return false;
                 }
             }

         }

    }
}