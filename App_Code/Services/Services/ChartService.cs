﻿using System;
using System.CodeDom.Compiler;
using System.Collections.Generic;
using System.Linq;
using System.Reflection;
using System.Web;
using AutoMapper;
using Microsoft.CSharp;
using NetHealthPIMModel;
using PIM.Models;
using PIM.Repositories;
using PIM.SystemHelpers;
using PIM.ViewModels;

namespace PIM.Services
{
    public class ChartService : IChartService 
    {
        private IChartRepository crep;

        public ChartService()
        {
            this.crep = new ChartRepository();
            
        }


        public int? GetChartsConsidered(int ParticipantID, int MeasureID)
        {

            return crep.GetChartsConsidered(ParticipantID, MeasureID);

        }
    


      public int? GetNumberOfRequiredCharts(int moduleid, int? groupID)
        {
            return crep.GetNumberOfRequiredCharts(moduleid, groupID);

        }
       public bool IsChartCompleted(int ParticipantModuleCycleID, int ModuleID, int? GroupID)
        {
            return crep.IsChartCompleted(ParticipantModuleCycleID, ModuleID, GroupID);

        }


       public int GetNewNumberOfComcletedCharts(int cycleid, int ParticipantID, int ModuleID)
        {
            return crep.GetNewNumberOfComcletedCharts(cycleid, ParticipantID, ModuleID);

        }



      public int GetNumberOfComcletedCharts(int cycleid)
        {

            return crep.GetNumberOfComcletedCharts(cycleid);
        }



      public  bool PromtUserforMoreCharts(int ParticipantID, int ParticipantModuleID, int? GroupID, int ModuleID)
        {
            return crep.PromtUserforMoreCharts(ParticipantID, ParticipantModuleID, GroupID, ModuleID);
        }


       public void SetChartStatus(string ChartID, int ParticipantModuleCcyleID, int ModuleID, bool status)
        {
            crep.SetChartStatus(ChartID, ParticipantModuleCcyleID, ModuleID, status);
        }



       public List<ChartAbstractionViewModel> GetChartQuestionsForEachMeasure(int ModuleID, int ParticipantModuleCycleID, int MainParticipantCycleID, int ParticipantID, string ChartID, bool? IsPQRS)
       {
          return  crep.GetChartQuestionsForEachMeasure(ModuleID, ParticipantModuleCycleID, MainParticipantCycleID, ParticipantID, ChartID, IsPQRS);

       }

      public void InsertChartPQRS(List<ChartAbstractionViewModel> model, int ModuleID, int ParticipantModuleCycleID, int MainParticipantCycleID, int ParticipantID, string DatEntryID, int StageID, int? GroupID, DateTime? VisitDate, DateTime? DOB, int? Gender)
      {
             crep.InsertChart(model, ModuleID, ParticipantModuleCycleID, ParticipantID, DatEntryID, VisitDate, DOB, Gender, StageID, GroupID);
      }


      public List<ChartQuestionModel> GetListOfQuestions(int ModuleID, int MainParticipantCycleID)
      {
          return crep.GetListOfQuestions(ModuleID, MainParticipantCycleID);
      }




      public void InsertChart(List<ChartAbstractionViewModel> model, int ModuleID, int ParticipantModuleCycleID, int MainParticipantCycleID, int ParticipantID, string DatEntryID, int StageID, int? GroupID, DateTime? VisitDate, DateTime? DOB, int? Gender)
       {
           crep.InsertChart(model, ModuleID, ParticipantModuleCycleID, ParticipantID, DatEntryID, VisitDate, DOB,  Gender, StageID, GroupID);
       }


      public Dictionary<int, string> GetListOfErrorMesssages(int ParticipantModuleCycleID, int ParticipantID, string ChartID)
      {
          return crep.GetListOfErrorMesssages(ParticipantModuleCycleID, ParticipantID, ChartID);

      }



       public bool CheckIfChartIDIsUnique(int ParticipantModuleCycleID, int ModuleID, int ParticipantID, string ChartID)
       {
           return crep.CheckIfChartIDIsUnique(ParticipantModuleCycleID, ModuleID, ParticipantID, ChartID);

       }


      public List<ChartListViewModel> GetChartList(int ParticipantModuleCycleID, int ModuleID, int ParticipantID)
       {
           return crep.GetChartList(ParticipantModuleCycleID, ModuleID, ParticipantID);
       }



      public List<ChartQuestionValidation> GetMeasureValidations(int MeasureID)
      {
          //return Mapper.Map<List<PIMChartQuestionValidation>, List<ChartQuestionValidation>>(crep.GetMeasureValidations(MeasureID));
           //NetHealthPIMEntities db=new NetHealthPIMEntities();
          return crep.GetMeasureValidations(MeasureID).Select(c=>new ChartQuestionValidation{
                ValidationID = c.ValidationID,
                MeasureID = c.MeasureID,
                PQRSSubmissionYear = c.PQRSSubmissionYear,
                QuestionKey = c.QuestionKey,
                TargetKey = c.TargetKey,
                IfValue = c.IfValue,
                ThenValue = c.ThenValue,
                CustomValidation = c.CustomValidation,
              //  QuestionType = (from v in db.QuestionType where v.QuestionTypeID==c.
                CreatedDate = c.CreatedDate,
                LastUpdated = c.LastUpdated
          }).ToList();
      }

      public List<MeasureModel> GetAllParticipantSelectedMeasures(int ModuleID, int MainParticipantModuleCycleID)
      {
          return crep.GetAllParticipantSelectedMeasures(ModuleID, MainParticipantModuleCycleID).Select(c=>new MeasureModel{ 
                MeasureID = c.MeasureID,
                MeasureTitle = c.MeasureTitle,
                MeasureClinicalRecommendation = c.MeasureClinicalRecommendation,
                MeasureLongDescription = c.MeasureLongDescription,
                MeasureComputationMethod= c.MeasureComputationMethod,
                MeasureTypeID = (int)c.MeasureType.MeasureTypeID,
                LastUpdateDate = (DateTime)c.LastUpdateDate,
                ModuleID = (int)c.Module.ModuleID,
                MeasureQualityIndicator = c.MeasureQualityIndicator,
                RationaleMeasureDescription = c.RationaleMeasureDescription,
                MeasureLevelID = (int)c.MeasureLevel.MeasureLevelID
          }).ToList();
      }
      public void DeleteCharts(int ParticipantID, int ParticipantModuleCycleID, int ModuleID, string ChartID)
      {
          crep.DeleteCharts(ParticipantID, ParticipantModuleCycleID, ModuleID, ChartID);
      }
    

    }
}