﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

/// <summary>
/// Summary description for Constants
/// </summary>
public class Constants
{
    public const string SESSION_MODULEID = "ModuleID";
    public const string SESSION_MENUCODE = "MenuCode";
    public const string SESSION_PAGE_ROLETYPE_LEVEL = "RoleType";
    public const string SESSION_USERCONTEXT = "UserContext";
    public const string SESSION_MODULECONTEXT = "ModuleContext";
    public const string SESSION_GROUPCONTEXT = "GroupContext";
    public const string SESSION_SITEMAP_PUBLIC = "SiteMapPublic";
    public const string SESSION_SITEMAP_PRIVATE = "SiteMapPrivate";
    public const string SESSION_START_NEW_MODULE_ALERT = "StartNewModuleAlert";
    public const string SESSION_WORKINGMODULEID = "WorkingModuleID";
    public const string SESSION_PREVIOUSCYCLE = "PreviousCycle";

    public const string MENUCODE_HOME = "HOME";
    public const string MENUCODE_INDEX = "INDEX";
    public const string MENUCODE_INSTRUCTIONS = "INSTRUCTIONS";
    public const string MENUCODE_MEASURES = "MEASURES";
    public const string MENUCODE_RESOURCES = "RESOURCES";
    public const string MENUCODE_REPORTS = "REPORTS";

    public const string SESSION_Y = "Y";
    public const string SESSION_N = "N";

    public const string SELECTEDVALUE_OTHER = "0";
    public const string SELECTEDTEXT_OTHER = "Other";

    public const string SEQUENCE_PARTICIPANTID = "ParticipantID";

    public const string RADIOBUTTONVALUE_YES = "1";
    public const string RADIOBUTTONVALUE_NO = "2";
    public const string RADIOBUTTONVALUE_NONE = "0";
    public const string RADIOBUTTONTEXT_NONE = "None of the above";

    public const string QUERYSTRING_ACTION = "action";
    public const string QUERYSTRING_ACTION_SAVE = "save";
    public const string QUERYSTRING_ACTION_VIEW = "view";
    public const string QUERYSTRING_CYCLEID = "cid";
    public const string QUERYSTRING_PHASE = "phase";

    public const int INTVALUE_NONE = 0;
    public const int INTVALUE_YES = 1;
    public const int INTVALUE_NO = 2;
    public const int INTVALUE_CANIMPROVE = 3;

    public const int RESOURCETYPE_PATIENTEDUCATION = 1;
    public const int RESOURCETYPE_CME = 2;              // Continued medical education
    public const int RESOURCETYPE_RESOURCE = 3;
    public const int RESOURCETYPE_PRACTICE = 4;
    public const int RESOURCETYPE_ACTIVITY = 5;
    public const int RESOURCETYPE_INTERVENTION = 6;
    public const int RESOURCETYPE_QIR = 7;              // Improvement in Medical Practice reference

    public const string SELECTEDVALUE_COUNTRY_USA = "233";
    public const int COUNTRYID_USA = 233;

    public const int MEASURE_AGGREGATE_TYPE_PEER = 1;
    public const int MEASURE_AGGREGATE_TYPE_PEER_PART2 = 4;
    public const int MEASURE_AGGREGATE_TYPE_POPULATION = 6;
    public const int MEASURE_TYPE_PIM = 1;
    public const int MEASURE_TYPE_AGGREGATE = 2;

    public const int ROLETYPEID_GUEST = 0;
    public const int ROLETYPEID_PARTICIPANT = 1;
    public const int ROLETYPEID_CLIENT_ADMIN = 2;
    public const int ROLETYPEID_SYSTEM_ADMIN = 3;
    
    // TODO: Remove this constant (?)
    public const int MODULE_COPD = 1;

    public const int INPUT_SOURCE_MANUAL = 1;

    public const int ASN_IMPROVEMENTPLAN_PARTI = 1;
    public const int ASN_IMPROVEMENTPLAN_PARTII = 2;
    public const int ASN_IMPROVEMENTPLAN_PARTIII = 3;
    public const int ASN_IMPROVEMENTPLAN_PARTIV = 4;
    public const int ASN_IMPROVEMENTPLAN_PARTV = 5;

    // activities
    public const int ABO_CATARACT_MODULEID = 3;
    public const int ABO_COM_MODULEID = 10;
    public const int ABO_YAG_MODULEID = 11;
    public const int ABO_CE_MODULEID = 12;
    public const int ABO_IK_MODULEID = 13;
    public const int ABO_PRESBYOPIA_MODULEID = 14;
    public const int ABO_PAB_MODULEID = 15;
    public const int ABO_DRYEYE_MODULEID = 16;
    public const int ABO_MD_MODULEID = 17;
    public const int ABO_POAG_MODULEID = 20;
    public const int ABO_PACG_MODULEID = 21;
    public const int ABO_POAGS_MODULEID = 22;
    public const int ABO_OAGS_MODULEID = 23;
    public const int ABO_SPOA_MODULEID = 24;
    public const int ABO_LT_MODULEID = 27;
    public const int ABO_ION_MODULEID = 28;
    public const int ABO_SNP_MODULEID = 29;
    public const int ABO_AION_MODULEID = 30;
    public const int ABO_TNP_MODULEID = 31;
    public const int ABO_TAB_MODULEID = 32;
    public const int ABO_CSU_MODULEID = 33;
    public const int ABO_RAAU_MODULEID = 34;
    public const int ABO_EM_MODULEID = 35;
    public const int ABO_PTOSIS_MODULEID = 36;
    public const int ABO_TO_MODULEID = 37;
    public const int ABO_BFO_MODULEID = 38;
    public const int ABO_EPN_MODULEID = 39;
    public const int ABO_LEEE_MODULEID = 40;
    public const int ABO_AAB_MODULEID = 41;
    public const int ABO_RETINOBLASTOMA_MODULEID = 42;
    public const int ABO_CM_MODULEID = 43;
    public const int ABO_OP_MODULEID = 44;
    public const int ABO_OPES_MODULEID = 45;
    public const int ABO_MDE_MODULEID = 46;
    public const int ABO_OPCI_MODULEID = 47;
    public const int ABO_AMBLYOPIA_MODULEID = 48;
    public const int ABO_CCE_MODULEID = 49;
    public const int ABO_ESOTROPIA_MODULEID = 50;
    public const int ABO_LM_MODULEID = 51;
    public const int ABO_TIOL_MODULEID = 52;
    public const int ABO_AMD_MODULEID = 53;
    public const int ABO_AMDW_MODULEID = 54;
    public const int ABO_RAI_MODULEID = 55;
    public const int ABO_DR_MODULEID = 56;
    public const int ABO_RD_MODULEID = 57;
    public const int ABO_CRVO_MODULEID = 58;
    public const int ABO_RRDS_MODULEID = 59;

    public const int ABO_CHARTS_REMEASURE = 10;
    public const int TOTAL_NUMBER_OF_CHARTS = 30;
    public const int MIN_NUMBER_OF_MEASURES = 1;

    public const string PIM_REGISTRATION = "1";
    public const string PIMandPEC_REGISTRATION = "3";

    public const string SESSION_PREVIOUS_MODULES = "SESSION_PREVIOUS_MODULES";
    public const string SESSION_PREVIOUS_NUMBEROFCHARTS = "SESSION_PREVIOUS_NUMBEROFCHARTS";
    public const string BC_CREATE_PLAN = "Create Your Experience";
    public const string BC_PQRS_INCENTIVE_PROGRAM = "PQRS Incentive Program";
    public const string BC_IMPORT_DATA = "Import Data";
    public const string BC_MODULE_SELECTION = "Activity Selection";
    public const string BC_MODULE_DETAILS = "Activities Selected";
    public const string BC_SELECT_PEC = "PEC";
    public const string BC_DASHBOARD = "Status";
    public const string BC_DASHBOARD_ANALYSIS = "Status";
    public const string BC_PERFORMANCE_REPORT = "Improvement in Medical Practice Report";
    public const string BC_SELECT_MEASURES = "Select Measures";
    public const string BC_IMPROVEMENT_PLAN = "Improvement in Medical Practice Plan";
    public const string BC_IMPROVEMENT_PLAN_COMPLETE = "Complete";
    public const string BC_IMPACT_STATEMENT = "Impact Statement";
    public const string BC_MEASURE_RATIONALE = "Measure Rationale";
    public const string BC_PEC_PRINT_FORMS = "Print Forms";
    public const string BC_SUPPORT = "Support";
    public const string BC_MODULES_COMPLETED_TO_DATE = "Activities Completed to Date";
    public const string BC_PRACTICE_REVIEW = "Improvement in Medical Practice Review";
    public const string BC_SELECTED_MEASURES_FOR_IMPROVEMENT = "Selected Measures for Improvement";
    public const string BC_CREATE_PLAN_LINK = "<a href='ModuleSelectionReview.aspx'>" + BC_CREATE_PLAN + "</a>";
    public const string BC_PQRS_INCENTIVE_PROGRAM_LINK = "<a href='PQRSSelection.aspx'>" + BC_PQRS_INCENTIVE_PROGRAM + "</a>";
    public const string BC_IMPORT_DATA_LINK = "<a href='SelectPriorModules.aspx'>" + BC_IMPORT_DATA + "</a>";
    public const string BC_MODULE_SELECTION_LINK = "<a href='ModuleSelection.aspx'>" + BC_MODULE_SELECTION + "</a>";
    public const string BC_MODULE_DETAILS_LINK = "<a href='ModuleDetails.aspx'>" + BC_MODULE_DETAILS + "</a>";
    public const string BC_SELECT_PEC_LINK = "<a href='SelectPEC.aspx'>" + BC_SELECT_PEC + "</a>";
    public const string BC_DASHBOARD_LINK = "<a href='Dashboard.aspx'>" + BC_DASHBOARD + "</a>";
    public const string BC_DASHBOARD_ANALYSIS_LINK = "<a href='Dashboard3.aspx'>" + BC_DASHBOARD_ANALYSIS + "</a>";
    public const string BC_PERFORMANCE_REPORT_LINK = "<a href='PerformanceReport.aspx'>" + BC_PERFORMANCE_REPORT + "</a>";
    public const string BC_SELECT_MEASURES_LINK = "<a href='SelectMeasures.aspx'>" + BC_SELECT_MEASURES + "</a>";
    public const string BC_IMPROVEMENT_PLAN_LINK = "<a href='ImprovementPlanPartI.aspx'>" + BC_IMPROVEMENT_PLAN + "</a>";
    public const string BC_IMPACT_STATEMENT_LINK = "<a href='ImpactStatement.aspx'>" + BC_IMPACT_STATEMENT + "</a>";
    public const string BC_MODULES_COMPLETED_TO_DATE_LINK = "<a href='ModuleCompletedToDate.aspx'>" + BC_MODULES_COMPLETED_TO_DATE + "</a>";
    
}
