﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using NetHealthPIMModel;
using System.Data.EntityClient;
using System.Data;
using System.Transactions;
using System.Data.SqlClient;

/// <summary>
/// Summary description for CycleManager
/// </summary>
public class CycleManager
{
	public CycleManager()
	{
	}

    // TODO: Need to add attributes to indicate the earliest date possible for abstract charts. This is required when new cycles are created (not the first one).
    public static int GetActiveCycleId(int participantID, int moduleID, Int32? previousCycleID, DateTime? eventDate)
    {
        using (NetHealthPIMEntities pim = new NetHealthPIMEntities())
        {
            int cycle2Exists = (from c in pim.ParticipantModuleCycle
                              where c.ParticipantModule.ParticipantID == participantID &&
                                     c.ParticipantModule.ModuleID == moduleID &&
                                     c.CycleNumber == 2
                              select c.ParticipantModuleCycleID).Count<Int32>();

            if (cycle2Exists == 1)
            {
                int cycle = (from c in pim.ParticipantModuleCycle
                             where c.ParticipantModule.ParticipantID == participantID &&
                                   c.ParticipantModule.ModuleID == moduleID &&
                                   c.CycleNumber == 2
                             select c.ParticipantModuleCycleID).First<Int32>();
                return cycle;
            }

            int cycle1Exists = (from c in pim.ParticipantModuleCycle
                              where c.ParticipantModule.ParticipantID == participantID &&
                                     c.ParticipantModule.ModuleID == moduleID &&
                                     c.CycleNumber == 1
                              select c.ParticipantModuleCycleID).Count<Int32>();
            if (cycle1Exists == 1)
            {
                int cycle = 0;
                ParticipantModuleCycle participantModuleCycle = (from c in pim.ParticipantModuleCycle
                             where c.ParticipantModule.ParticipantID == participantID &&
                                   c.ParticipantModule.ModuleID == moduleID &&
                                   c.CycleNumber == 1
                               orderby c.ParticipantModuleCycleID descending
                             select c).First();
                cycle = participantModuleCycle.ParticipantModuleCycleID;
                if ((DateTime.Today >= participantModuleCycle.RemeasureDate) && (participantModuleCycle.ImprovementPlanComplete == true))
                {
                    previousCycleID = participantModuleCycle.ParticipantModuleCycleID;
                    cycle = AddNewCycle(participantID, moduleID, previousCycleID, eventDate);
                    AddPatientRegistrationData(participantModuleCycle.ParticipantModuleCycleID, cycle); // cycle is the new created one
                }
                return cycle;
            }
            // Return the cycle record that is NOT complete (there must be only one).
            int cycleCount = (from   c in pim.ParticipantModuleCycle
                              where  c.ParticipantModule.ParticipantID == participantID &&
                                     c.ParticipantModule.ModuleID == moduleID && 
                                     c.IsComplete == false
                              select c.ParticipantModuleCycleID).Count<Int32>();
            if (cycleCount == 1)
            {
                int cycle = (from c in pim.ParticipantModuleCycle
                             where c.ParticipantModule.ParticipantID == participantID &&
                                   c.ParticipantModule.ModuleID == moduleID && 
                                   c.IsComplete == false
                             select c.ParticipantModuleCycleID).First<Int32>();
                return cycle;
            }
            else
            {
                return AddNewCycle(participantID, moduleID, previousCycleID, eventDate);
            }
        }
    }

    public static int AddNewCycle(int participantID, int moduleID, Int32? previousCycleID, DateTime? eventDate)
    {
        using (NetHealthPIMEntities pim = new NetHealthPIMEntities())
        {
            ParticipantModuleCycle cycle = new ParticipantModuleCycle();
            bool isPhase2 = false;
            int cycleNumber = 1;
            DateTime dte = eventDate ?? DateTime.Today;
            DateTime baselineDate = DateTime.Today;
            if (previousCycleID != null && previousCycleID > 0)
            {
                ParticipantModuleCycle prevCycle = pim.ParticipantModuleCycle.First(c => c.ParticipantModuleCycleID == previousCycleID);
                cycleNumber = prevCycle.CycleNumber + 1;
                cycle.PrevParticipantModuleCycle = prevCycle;
                isPhase2 = true;
                // TODO: Use a different logic to define this baseline date (Module information or Input parameter?)
                baselineDate = DateTime.Today.AddDays(90);

                // Set the Min Month and Year to the current date to make sure charts used in subsequent cycles are "fresh"
                cycle.MinLastVisitMonth = DateTime.Today.Month;
                cycle.MinLastVisitYear = DateTime.Today.Year;
                // Max values are NULL so that they are initialized to the current values in the Abstract input screen.
            }
            else
            {
                // This is the first cycle and therefore we initialize the dates. 
                cycle.MinLastVisitMonth = 1;
                cycle.MinLastVisitYear = DateTime.Today.Year - 4; // Matches the drop down year values. 
                cycle.MaxLastVisitMonth = dte.Month;
                cycle.MaxLastVisitYear = dte.Year;
            }
            cycle.CycleNumber = cycleNumber;
            ParticipantModule pm = new ParticipantModule { ModuleID = moduleID, ParticipantID = participantID };
            pim.AttachTo("ParticipantModule", pm);
            cycle.ParticipantModule = pm;
            cycle.CycleStartDate = DateTime.Now;
            cycle.IsComplete = false;
            cycle.Phase2Flag = isPhase2;

            // TODO: Update target dates (pending final logic for the update)
            cycle.SelfAssessmentSurveyTargetDate = baselineDate.AddDays(28);
            cycle.PatientSurveyTargetDate = baselineDate.AddDays(28);
            cycle.AbstractDataTargetDate = baselineDate.AddDays(28);
            cycle.ParticipantProfileTargetDate = baselineDate.AddDays(28);
            cycle.ExamineSystemsTargetDate = baselineDate.AddDays(28);
            cycle.ReportApprovedTargetDate = baselineDate.AddDays(28);
            cycle.MeasuresSelectedTargetDate = baselineDate.AddDays(42);
            cycle.ImprovementPlanTargetDate = baselineDate.AddDays(42);
            cycle.ImpactAssessmentTargetDate = baselineDate.AddDays(42);
            cycle.ModuleCompleteTargetDate = baselineDate.AddDays(84);

            cycle.LastUpdateDate = DateTime.Now;
            pim.SaveChanges();
            return cycle.ParticipantModuleCycleID;
        }
    }

    public static void CheckCycle()
    {
        UserContext ctx = (UserContext)HttpContext.Current.Session[Constants.SESSION_USERCONTEXT];
        Module module = ModuleContext.getModule();
        if (ctx.ActiveModuleCycleID == null || ctx.ActiveModuleCycleID <= 0)
        {
            int cycleFound = GetActiveCycleId(ctx.ParticipantID, module.ModuleID, null, null);
            ctx.ActiveModuleCycleID = cycleFound;
            if (ctx.ActiveModuleID <= 0) ctx.ActiveModuleID = module.ModuleID;
            using (NetHealthPIMEntities pim = new NetHealthPIMEntities())
            {
                var PrevCycle = (from c in pim.ParticipantModuleCycle
                                   where c.ParticipantModuleCycleID == ctx.ActiveModuleCycleID
                                   select c.PrevParticipantModuleCycle).FirstOrDefault();
                if (PrevCycle != null)
                {
                    ctx.PrevParticipantModuleCycleID = PrevCycle.ParticipantModuleCycleID; 
                }
            }


            HttpContext.Current.Session[Constants.SESSION_USERCONTEXT] = ctx;
        }
    }

    public static object[] getMonthList()
    {
        int i = 0;
        string[] months = { "Month", "January", "February", "March", "April", "May", "June", "July", "August", "September", "October", "November", "December" };
        var monthList = from m in months
                        select new { MonthID = i++, MonthName = m };
        return monthList.ToArray();
    }

    public static object[] getExamValues()
    {
        string[] examValues = { "", "10", "12", "15", "20", "25", "30", "40", "50", "60", "70", "80", "100", "125", "150", "200", "250", "300", "400", "800", "1000", "1200", "1600", "2000", "4000", "7777", "8888", "9999"};
        var examList = from e in examValues
                       select new { examValue = e, examLabel = e };
        return examList.ToArray();
    }

    public static object[] getNewExamValues()
    {
        using (NetHealthPIMEntities pim = new NetHealthPIMEntities())
        {
            var examList = (from c in pim.RadioButtonValues where c.FirstModule == "logMAR" select new { examValue = c.TextValue, examLabel = c.TextDescriptiveValue }).ToArray() ;
            return examList;
        }
    }


    public static object[] getPercentages()
    {
        int i = 0;
        string[] pct = { "", "10%", "20%", "30%", "40%", "50%", "60%", "70%", "80%", "90%", "100%" };
        var pctList = from e in pct
                      select new { pctValue = i++, pctLabel = e };
        return pctList.ToArray();
    }

    public static object[] getMostEffectiveMethodAmblyopiaTherapy()
    {
        int i = 0;
        string[] method = { "", "Option A", "Option B", "Option C" };
        var methodList = from e in method
                         select new { methodValue = i++, methodLabel = e };
        return methodList.ToArray();
    }

    public static object[] getNonSnellenOptotypes()
    {
        int i = 0;
        string[] nonSellenOptotypes = { "", "Lea", "Allen", "HOTV", "Other" };
        var nonSellenOptotypesList = from e in nonSellenOptotypes
                                     select new { nonSellenOptotypesValue = i++, nonSellenOptotypesLabel = e };
        return nonSellenOptotypesList.ToArray();
    }

    public static object[] getWorth4dot()
    {
        int i = 0;
        string[] worth4dot = { "", "Fuse", "Suppress OD", "Suppress OS", "Alternate Suppression", "Diplopia" };
        var worth4dotList = from w in worth4dot
                                     select new { worth4dotValue = i++, worth4dotLabel = w };
        return worth4dotList.ToArray();
    }

    public static object[] getYearList()
    {
        int[] years = new int[16] {-5,-4,-3,-2,-1, 0, 1, 2, 3, 4, 5, 6, 7, 8, 9, 10};
        var yearList = from y in years
                       select new { YearID = DateTime.Today.Year - y, YearName = (DateTime.Today.Year - y).ToString() };
        return yearList.ToArray();
    }

    public static object[] getYearListRemeasure() 
    {
        int[] years = new int[11] {-5,-4,-3,-2,-1, 0, 1, 2, 3, 4, 5};
        var yearList = from y in years
                       select new { YearID = DateTime.Today.Year + y, YearName = (DateTime.Today.Year + y).ToString() };
        return yearList.ToArray();
    }

    public static object[] getDOBYearList(int MinimunAge, int MaxNumberOfYears)
    {
        int[] years = new int[MaxNumberOfYears];
        for (var i = 0; i <= MaxNumberOfYears - 1; i++)
            years[i] = i;
        var yearList = from y in years
                       select new { YearID = DateTime.Today.Year - y - MinimunAge, YearName = (DateTime.Today.Year - y - MinimunAge).ToString() };
        return yearList.ToArray();
    }


    public static bool ParticipantPatientIDFound(int participantID, string participantPatientID)
    {
        using (NetHealthPIMEntities pim = new NetHealthPIMEntities())
        {
            var count = (from p in pim.Patient
                         where p.Participant.ParticipantID == participantID && p.ParticipantPatientID == participantPatientID
                         select p).Count<Patient>();
            return (count > 0);
        }
    }

    public static void ComputeCycleMeasures_ABO(int ParticipantModuleCycleID)
    {
        using (EntityConnection conn = new EntityConnection("name=NetHealthPIMEntities"))
        {
            conn.Open();
            EntityCommand cmd = conn.CreateCommand();
            cmd.CommandText = "NetHealthPIMEntities.ComputeCycleMeasures_Amblyopia_P";
            cmd.CommandType = CommandType.StoredProcedure;
            cmd.Parameters.AddWithValue("ParticipantModuleCycleID", ParticipantModuleCycleID);
            cmd.ExecuteNonQuery();
            conn.Close();
        }
    }

    public static void ComputeCycleMeasures_ABO_EXT(int ParticipantModuleCycleID, string ComputeCycleMeasuresProcedure)
    {
        using (EntityConnection conn = new EntityConnection("name=NetHealthPIMEntities"))
        {
            conn.Open();
            EntityCommand cmd = conn.CreateCommand();
            cmd.CommandText = "NetHealthPIMEntities." + ComputeCycleMeasuresProcedure;
            cmd.CommandType = CommandType.StoredProcedure;
            cmd.Parameters.AddWithValue("ParticipantModuleCycleID", ParticipantModuleCycleID);
            cmd.ExecuteNonQuery();
            conn.Close();
        }
    }

    public static void UpdateParticipantCompletedModules(int ParticipantModuleCycleID, int ModuleID)
    {
        using (EntityConnection conn = new EntityConnection("name=NetHealthPIMEntities"))
        {
            conn.Open();
            EntityCommand cmd = conn.CreateCommand();
            cmd.CommandText = "NetHealthPIMEntities.UpdateParticipantCompletedModules_P";
            cmd.CommandType = CommandType.StoredProcedure;
            cmd.Parameters.AddWithValue("ParticipantModuleCycleID", ParticipantModuleCycleID);
            cmd.Parameters.AddWithValue("ModuleID", ModuleID);
            cmd.ExecuteNonQuery();
            conn.Close();
        }
    }

    public static void ComputeCycleMeasures_ASN_Participants(int ParticipantModuleCycleID, int ParticipantGroupID)
    {
        using (EntityConnection conn = new EntityConnection("name=NetHealthPIMEntities"))
        {
            conn.Open();
            EntityCommand cmd = conn.CreateCommand();
            cmd.CommandText = "NetHealthPIMEntities.ComputeCycleMeasures_ASN_Participants";
            cmd.CommandType = CommandType.StoredProcedure;
            cmd.Parameters.AddWithValue("ParticipantModuleCycleID", ParticipantModuleCycleID);
            cmd.Parameters.AddWithValue("ParticipantGroupID", ParticipantGroupID);
            cmd.ExecuteNonQuery();
            conn.Close();
        }
    }

    public static void ComputeCycleMeasuresGroup_ASN(int ParticipantGroupID, int CycleNumber)
    {
        using (EntityConnection conn = new EntityConnection("name=NetHealthPIMEntities"))
        {
            conn.Open();
            EntityCommand cmd = conn.CreateCommand();
            cmd.CommandText = "NetHealthPIMEntities.ComputeCycleMeasuresGroup_ASN";
            cmd.CommandType = CommandType.StoredProcedure;
            cmd.Parameters.AddWithValue("ParticipantGroupID", ParticipantGroupID);
            cmd.Parameters.AddWithValue("CycleNumber", CycleNumber);
            cmd.ExecuteNonQuery();
            conn.Close();
        }
    }

    public static void CompleteGroupMemberCharts_ASN(int ParticipantGroupID, int CycleNumber)
    {
        using (EntityConnection conn = new EntityConnection("name=NetHealthPIMEntities"))
        {
            conn.Open();
            EntityCommand cmd = conn.CreateCommand();
            cmd.CommandText = "NetHealthPIMEntities.CompleteGroupMemberCharts_ASN";
            cmd.CommandType = CommandType.StoredProcedure;
            cmd.Parameters.AddWithValue("ParticipantGroupID", ParticipantGroupID);
            cmd.Parameters.AddWithValue("CycleNumber", CycleNumber);
            cmd.ExecuteNonQuery();
            conn.Close();
        }
    }

    public static void ApprovedImprovementGroupPlan(int ParticipantGroupID, int CycleNumber)
    {
        using (EntityConnection conn = new EntityConnection("name=NetHealthPIMEntities"))
        {
            conn.Open();
            EntityCommand cmd = conn.CreateCommand();
            cmd.CommandText = "NetHealthPIMEntities.ApprovedImprovementGroupPlan";
            cmd.CommandType = CommandType.StoredProcedure;
            cmd.Parameters.AddWithValue("ParticipantGroupID", ParticipantGroupID);
            cmd.Parameters.AddWithValue("CycleNumber", CycleNumber);
            cmd.ExecuteNonQuery();
            conn.Close();
        }
    }

    public static void ApproveImpactGroupStatement(int ParticipantGroupID, int CycleNumber)
    {
        using (EntityConnection conn = new EntityConnection("name=NetHealthPIMEntities"))
        {
            conn.Open();
            EntityCommand cmd = conn.CreateCommand();
            cmd.CommandText = "NetHealthPIMEntities.ApproveImpactGroupStatement";
            cmd.CommandType = CommandType.StoredProcedure;
            cmd.Parameters.AddWithValue("ParticipantGroupID", ParticipantGroupID);
            cmd.Parameters.AddWithValue("CycleNumber", CycleNumber);
            cmd.ExecuteNonQuery();
            conn.Close();
        }
    }
    

    public static void UpdateParticipantGroupMeasureRationale(int ParticipantGroupID, int measureID, int rationaleTypeID, int CycleNumber)
    {
        using (EntityConnection conn = new EntityConnection("name=NetHealthPIMEntities"))
        {
            conn.Open();
            EntityCommand cmd = conn.CreateCommand();
            cmd.CommandText = "NetHealthPIMEntities.UpdateParticipantGroupMeasureRationale";
            cmd.CommandType = CommandType.StoredProcedure;
            cmd.Parameters.AddWithValue("ParticipantGroupID", ParticipantGroupID);
            cmd.Parameters.AddWithValue("measureID", measureID);
            cmd.Parameters.AddWithValue("rationaleTypeID", rationaleTypeID);
            cmd.Parameters.AddWithValue("CycleNumber", CycleNumber);
            cmd.ExecuteNonQuery();
            conn.Close();
        }
    }

    public static void CreateCycleGroup(int ParticipantGroupID, int CycleNumber, int LeaderParticipantID)
    {
        using (EntityConnection conn = new EntityConnection("name=NetHealthPIMEntities"))
        {
            conn.Open();
            EntityCommand cmd = conn.CreateCommand();
            cmd.CommandText = "NetHealthPIMEntities.CreateCycleGroup";
            cmd.CommandType = CommandType.StoredProcedure;
            cmd.Parameters.AddWithValue("ParticipantGroupID", ParticipantGroupID);
            cmd.Parameters.AddWithValue("CycleNumber", CycleNumber);
            cmd.Parameters.AddWithValue("LeaderParticipantID", LeaderParticipantID);
            cmd.ExecuteNonQuery();
            conn.Close();
        }
    }

    public static void CreateNewCycle()
    {
        UserContext ctx = (UserContext)HttpContext.Current.Session[Constants.SESSION_USERCONTEXT];
        Module module = ModuleContext.getModule();
        int cycle = GetActiveCycleId(ctx.ParticipantID, module.ModuleID, ctx.ActiveModuleCycleID, null);
        ctx.ActiveModuleCycleID = cycle;
        HttpContext.Current.Session[Constants.SESSION_USERCONTEXT] = ctx;
    }

    public static void AddInitialChartRegistration(int ParticipantModuleCycleID, int ModuleID, int ChartsToEnter)
    {
        using (EntityConnection conn = new EntityConnection("name=NetHealthPIMEntities"))
        {
            conn.Open();
            EntityCommand cmd = conn.CreateCommand();
            cmd.CommandText = "NetHealthPIMEntities.AddInitialChartRegistration";
            cmd.CommandType = CommandType.StoredProcedure;
            cmd.Parameters.AddWithValue("ParticipantModuleCycleID", ParticipantModuleCycleID);
            cmd.Parameters.AddWithValue("ModuleID", ModuleID);
            cmd.Parameters.AddWithValue("ChartsToEnter", ChartsToEnter);
            cmd.Parameters.AddWithValue("TOTAL_NUMBER_OF_CHARTS", Constants.TOTAL_NUMBER_OF_CHARTS);
            cmd.Parameters.AddWithValue("DeletePreviousRegistration", true);
            cmd.ExecuteNonQuery();
            conn.Close();
        }
    }

    public static void AddPreviousChartRegistration(int ParticipantModuleCycleID, string CandidateID, int ModuleID, int NumberOfCharts)
    {
        using (EntityConnection conn = new EntityConnection("name=NetHealthPIMEntities"))
        {
            conn.Open();
            EntityCommand cmd = conn.CreateCommand();
            cmd.CommandText = "NetHealthPIMEntities.AddPreviousChartRegistration";
            cmd.CommandType = CommandType.StoredProcedure;
            cmd.Parameters.AddWithValue("ParticipantModuleCycleID", ParticipantModuleCycleID);
            cmd.Parameters.AddWithValue("CandidateID", CandidateID);
            cmd.Parameters.AddWithValue("ModuleID", ModuleID);
            cmd.Parameters.AddWithValue("NumberOfCharts", NumberOfCharts);
            cmd.ExecuteNonQuery();
            conn.Close();
        }
    }

    public static void CopyChartAbstraction(int ParticipantModuleCycleID, string CandidateID, int ModuleID, string CopyProcedureName)
    {
        using (EntityConnection conn = new EntityConnection("name=NetHealthPIMEntities"))
        {
            conn.Open();
            EntityCommand cmd = conn.CreateCommand();
            cmd.CommandText = "NetHealthPIMEntities." + CopyProcedureName;
            cmd.CommandType = CommandType.StoredProcedure;
            cmd.Parameters.AddWithValue("ParticipantModuleCycleID", ParticipantModuleCycleID);
            cmd.Parameters.AddWithValue("CandidateID", CandidateID);
            cmd.Parameters.AddWithValue("ModuleID", ModuleID);
            cmd.ExecuteNonQuery();
            conn.Close();
        }
    }

    public static void DeletePreviousChartRegistration(int ParticipantModuleCycleID, int ModuleID)
    {
        using (EntityConnection conn = new EntityConnection("name=NetHealthPIMEntities"))
        {
            conn.Open();
            EntityCommand cmd = conn.CreateCommand();
            cmd.CommandText = "NetHealthPIMEntities.DeletePreviousChartRegistration";
            cmd.CommandType = CommandType.StoredProcedure;
            cmd.Parameters.AddWithValue("ParticipantModuleCycleID", ParticipantModuleCycleID);
            cmd.Parameters.AddWithValue("ModuleID", ModuleID);
            cmd.ExecuteNonQuery();
            conn.Close();
        }
    }

    public static void RemoveSelectedMeasures(int ParticipantModuleCycleID)
    {
        using (EntityConnection conn = new EntityConnection("name=NetHealthPIMEntities"))
        {
            conn.Open();
            EntityCommand cmd = conn.CreateCommand();
            cmd.CommandText = "NetHealthPIMEntities.RemoveSelectedMeasures_P";
            cmd.CommandType = CommandType.StoredProcedure;
            cmd.Parameters.AddWithValue("ParticipantModuleCycleID", ParticipantModuleCycleID);
            cmd.ExecuteNonQuery();
            conn.Close();
        }
    }

    public static void DeleteMeasure(int PREVParticipantModuleCycleID, int ParticipantModuleCycleID, int MeasureID)
    {
        using (EntityConnection conn = new EntityConnection("name=NetHealthPIMEntities"))
        {
            conn.Open();
            EntityCommand cmd = conn.CreateCommand();
            cmd.CommandText = "NetHealthPIMEntities.DeleteMeasure_P";
            cmd.CommandType = CommandType.StoredProcedure;
            cmd.Parameters.AddWithValue("PREVParticipantModuleCycleID", PREVParticipantModuleCycleID);
            cmd.Parameters.AddWithValue("ParticipantModuleCycleID", ParticipantModuleCycleID);
            cmd.Parameters.AddWithValue("MeasureID", MeasureID);
            cmd.ExecuteNonQuery();
            conn.Close();
        }
    }

    public static void DropModulesAndAbstractions(int ParticipantModuleCycleID)
    {
        using (EntityConnection conn = new EntityConnection("name=NetHealthPIMEntities"))
        {
            conn.Open();
            EntityCommand cmd = conn.CreateCommand();
            cmd.CommandText = "NetHealthPIMEntities.DropModulesAndAbstractions_P";
            cmd.CommandType = CommandType.StoredProcedure;
            cmd.Parameters.AddWithValue("ParticipantModuleCycleID", ParticipantModuleCycleID);
            cmd.ExecuteNonQuery();
            conn.Close();
        }
    }

    public static void UpdateChartDates(int ParticipantModuleCycleID, string RecordIdentifier, int ModuleID, bool RegistrationUpdate)
    {
        using (EntityConnection conn = new EntityConnection("name=NetHealthPIMEntities"))
        {
            conn.Open();
            EntityCommand cmd = conn.CreateCommand();
            cmd.CommandText = "NetHealthPIMEntities.UpdateChartDates_P";
            cmd.CommandType = CommandType.StoredProcedure;
            cmd.Parameters.AddWithValue("ParticipantModuleCycleID", ParticipantModuleCycleID);
            cmd.Parameters.AddWithValue("RecordIdentifier", RecordIdentifier);
            cmd.Parameters.AddWithValue("ModuleID", ModuleID);
            cmd.Parameters.AddWithValue("RegistrationUpdate", RegistrationUpdate);
            cmd.ExecuteNonQuery();
            conn.Close();
        }
    }

    public static void AddPECMeasuresToParticipant(int ParticipantModuleCycleID)
    {
        using (EntityConnection conn = new EntityConnection("name=NetHealthPIMEntities"))
        {
            conn.Open();
            EntityCommand cmd = conn.CreateCommand();
            cmd.CommandText = "NetHealthPIMEntities.AddPECMeasuresToParticipant_P";
            cmd.CommandType = CommandType.StoredProcedure;
            cmd.Parameters.AddWithValue("ParticipantModuleCycleID", ParticipantModuleCycleID);
            cmd.ExecuteNonQuery();
            conn.Close();
        }
    }

    public static void ResetIncludeInImprovementPlanFlag(int ParticipantModuleCycleID)
    {
        using (EntityConnection conn = new EntityConnection("name=NetHealthPIMEntities"))
        {
            conn.Open();
            EntityCommand cmd = conn.CreateCommand();
            cmd.CommandText = "NetHealthPIMEntities.ResetIncludeInImprovementPlanFlag_P";
            cmd.CommandType = CommandType.StoredProcedure;
            cmd.Parameters.AddWithValue("ParticipantModuleCycleID", ParticipantModuleCycleID);
            cmd.ExecuteNonQuery();
            conn.Close();
        }
    }

    public static void SetReOpenModule_NextCycle(int ParticipantModuleCycleID)
    {
        using (EntityConnection conn = new EntityConnection("name=NetHealthPIMEntities"))
        {
            conn.Open();
            EntityCommand cmd = conn.CreateCommand();
            cmd.CommandText = "NetHealthPIMEntities.SetReOpenModule_NextCycle_P";
            cmd.CommandType = CommandType.StoredProcedure;
            cmd.Parameters.AddWithValue("ParticipantModuleCycleID", ParticipantModuleCycleID);
            cmd.ExecuteNonQuery();
            conn.Close();
        }
    }

    public static EntityDataReader GetModulesByCategory(int ModuleCategoryID, int ParticipantModuleCycleID, bool PQRSUsed, EntityConnection conn)
    {

        EntityCommand cmd = conn.CreateCommand();
        cmd.CommandText = "NetHealthPIMEntities.GetModulesByCategory_P";
        cmd.CommandType = CommandType.StoredProcedure;
        cmd.Parameters.AddWithValue("ModuleCategoryID", ModuleCategoryID);
        cmd.Parameters.AddWithValue("ParticipantModuleCycleID", ParticipantModuleCycleID);
        cmd.Parameters.AddWithValue("PQRSUsed", PQRSUsed);

        EntityDataReader dr = cmd.ExecuteReader(CommandBehavior.SequentialAccess);

        return dr;
    }

    public static void DeleteChartRegistration(int ParticipantModuleCycleID, string RecordIdentifier, string AbstractionTable)
    {
        if (AbstractionTable.Trim() != "")
        {
            using (EntityConnection conn = new EntityConnection("name=NetHealthPIMEntities"))
            {
                //conn.Open();

                SqlConnection sqlConnection = new SqlConnection(conn.StoreConnection.ConnectionString);
                sqlConnection.Open();
                SqlCommand cmd = new SqlCommand();
                cmd.Connection = sqlConnection;
                cmd.CommandText = "DELETE FROM " + AbstractionTable + " WHERE ParticipantModuleCycleID = " + ParticipantModuleCycleID.ToString() + " AND RecordIdentifier = '" + RecordIdentifier + "'";
                cmd.CommandType = CommandType.Text;
                cmd.ExecuteNonQuery();
                sqlConnection.Close();
                using (NetHealthPIMEntities pim = new NetHealthPIMEntities())
                {
                    var PIMChartQuestionChoicesUserReplyList = (from c in pim.PIMChartQuestionChoicesUserReply
                                                                where c.ParticipantModuleCycleID == ParticipantModuleCycleID 
                                                                && c.ChartID == RecordIdentifier
                                                                select c).ToList();
                    foreach(var item in PIMChartQuestionChoicesUserReplyList)
                    {
                        pim.PIMChartQuestionChoicesUserReply.DeleteObject(item);
                        pim.SaveChanges();
                    }
                    var ChartQuestionUserReply = (from c in pim.ChartQuestionUserReply
                                                  where c.ParticipantModuleCycleID == ParticipantModuleCycleID 
                                                  && c.ChartID == RecordIdentifier 
                                                  select c).ToList();
                    foreach (var item in ChartQuestionUserReply)
                    {
                        pim.ChartQuestionUserReply.DeleteObject(item);
                        pim.SaveChanges();
                    }
                }

            }
        }
    }

    public static void DeletePreviousModule(int ParticipantModuleCycleID, NetHealthPIMEntities pim)
    {
        var moduleToDelete = (from pms in pim.ParticipantModuleSelection
                              where pms.ParticipantModuleCycleID == ParticipantModuleCycleID
                              && pms.PreviousModuleCharts == true
                              select pms);
        foreach (var m in moduleToDelete)
        {
            pim.DeleteObject(m);
        }
        var chartsToDelete = (from cr in pim.ChartRegistration
                              from pms in pim.ParticipantModuleSelection
                              where cr.ModuleID == pms.ModuleID
                              && pms.ParticipantModuleCycleID == cr.ParticipantModuleCycleID
                              && cr.ParticipantModuleCycleID == ParticipantModuleCycleID
                              && pms.PreviousModuleCharts == true
                              select cr);
        foreach (var c in chartsToDelete)
        {
            pim.DeleteObject(c);
        }

        pim.SaveChanges();
    }
    public static string InitialWhereYouLeftOff(int ParticipantModuleCycleID, int ParticipantID)
    {
        string url = "";
        using (NetHealthPIMEntities pim = new NetHealthPIMEntities())
        {
            ParticipantModuleCycle cycle = pim.ParticipantModuleCycle.First(c => c.ParticipantModuleCycleID == ParticipantModuleCycleID);
            Participant participant = pim.Participant.First(p => p.ParticipantID == ParticipantID);
            if ((cycle.CycleNumber == 1) && (participant.SoftwareVersion > 0))
            {
                if (cycle.ParticipantProfileComplete!=true)
                {
                    url = "ParticipantProfile.aspx";
                }
                else
                    if (cycle.BusinessAssociateAgreementComplete!=true)
                    {
                        url = "BusinessAssociateAgreement.aspx";
                    }
                    else
                        if (participant.AcknowledgmentDate == null)
                        {
                            url = "Frontmatter.aspx";
                        }
            }
        }
        return url;
    }

    public static string FirstDashboardWhereYouLeftOff(int ParticipantModuleCycleID, int ParticipantID)
    {
        string urlto = "";
        using (NetHealthPIMEntities pim = new NetHealthPIMEntities())
        {
            ParticipantModuleCycle cycle = pim.ParticipantModuleCycle.First(p => p.ParticipantModuleCycleID == ParticipantModuleCycleID);
            ParticipantModule mod = (from c in pim.ParticipantModule where c.ParticipantID == ParticipantID select c).FirstOrDefault();
            // PQRS selected?
            if (mod.PQRS == null)
                urlto = "PQRSSelection.aspx";
            else
            {
                // Previous Charts selected?
                if ((cycle.SelectPreviousChartDataComplete != true) && (cycle.SelectNewModulesComplete != true))
                    urlto = "SelectPriorModules.aspx";
                else
                {
                    // New Modules selected?
                    if (cycle.SelectNewModulesComplete != true)
                        urlto = "ModuleSelection.aspx";
                    else
                    {
                        // PEC selected?
                        if (cycle.SelectPECComplete != true)
                            urlto = "SelectPEC.aspx";
                    }
                }
            }

            if (urlto == "")
                urlto = WhereYouLeftOff(cycle.ParticipantModuleCycleID);
        }
        return urlto;
    }

    public static string WhereYouLeftOff(int ParticipantModuleCycleID)
    {
        string url = "";
        using (NetHealthPIMEntities pim = new NetHealthPIMEntities())
        {
            ParticipantModuleCycle cycle = pim.ParticipantModuleCycle.First(c => c.ParticipantModuleCycleID == ParticipantModuleCycleID);
            if (cycle.CycleNumber == 1)
            {
                if (!cycle.ReportApproved)
                {
                    if (cycle.SelectNewModulesComplete == true) // modules selected?
                    {
                        // check registrations
                        var missingChartRegistration = (from cr in pim.ParticipantPlanStatus_V
                                                        where cr.ParticipantModuleCycleID == ParticipantModuleCycleID
                                                         & (cr.RegisteredCharts + cr.PreviousChartData) != cr.NumberOfCharts
                                                        select cr).Count();
                        if (missingChartRegistration > 0)
                        {
                            var firstModule = (from cr in pim.ParticipantPlanStatus_V
                                               where cr.ParticipantModuleCycleID == ParticipantModuleCycleID
                                               & (cr.RegisteredCharts + cr.PreviousChartData) != cr.NumberOfCharts
                                               select cr).First();
                            url = "OpenModule.aspx?mid=" + firstModule.ModuleID;

                        }
                        else
                        {
                            // check System and Self Assessment Surveys
                            var missingSystemOrSelfAssessment = (from pms in pim.ParticipantModuleSelection
                                                                 where pms.ParticipantModuleCycleID == ParticipantModuleCycleID
                                                                  & (pms.SelfAssessmentSurveyComplete == null | pms.SystemSurveyComplete == null
                                                                  | pms.SelfAssessmentSurveyComplete == false | pms.SystemSurveyComplete == false)
                                                                 select pms).Count();
                            if (missingSystemOrSelfAssessment > 0)
                            {
                                var SystemOrSelfAssessment = (from pms in pim.ParticipantModuleSelection
                                                      where pms.ParticipantModuleCycleID == ParticipantModuleCycleID
                                                       & (pms.SelfAssessmentSurveyComplete == null | pms.SystemSurveyComplete == null
                                                       | pms.SelfAssessmentSurveyComplete == false | pms.SystemSurveyComplete == false)
                                                      select pms).First();
                                HttpContext.Current.Session[Constants.SESSION_WORKINGMODULEID] = Convert.ToInt32(SystemOrSelfAssessment.ModuleID);
                                if (SystemOrSelfAssessment.SelfAssessmentSurveyComplete != true)
                                    url = "SelfAssessment.aspx?CycleNumber=1";
                                else
                                    url = "SystemSurvey.aspx?CycleNumber=1";

                            }
                            else
                            {
                                // check System and Self Assessment Surveys
                                var missingChartAbstractions = (from pms in pim.ParticipantDashboardModules_V
                                                                where pms.ParticipantModuleCycleID == ParticipantModuleCycleID
                                                                 & pms.ChartsCompleted != pms.TotalCharts
                                                                select pms).Count();
                                if (missingChartAbstractions > 0)
                                {
                                    var ChartAbstractions = (from pms in pim.ParticipantDashboardModules_V
                                                             where pms.ParticipantModuleCycleID == ParticipantModuleCycleID
                                                              & pms.ChartsCompleted != pms.TotalCharts
                                                             select pms).First();
                                    url = "OpenModule.aspx?mid=" + ChartAbstractions.ModuleID;

                                }
                                else
                                    url = "PerformanceReport.aspx";
                            }
                        }
                    }
                }
                else
                {
                    if (cycle.MeasuresSelected == false)
                        url = "SelectMeasures.aspx";
                    else
                        if (cycle.ImprovementPlanComplete == false)
                            url = "ImprovementPlanPartI.aspx";
                        else
                            url = "Dashboard.aspx";
                }
            }
            else
            {
                if (!cycle.ReportApproved)
                {
                    // check registrations
                    var missingChartRegistration = (from cr in pim.ParticipantPlanStatus_V
                                                    where cr.ParticipantModuleCycleID == ParticipantModuleCycleID
                                                     & (cr.RegisteredCharts + cr.PreviousChartData) != cr.NumberOfCharts
                                                    select cr).Count();
                    if (missingChartRegistration > 0)
                    {
                        var firstModule = (from cr in pim.ParticipantPlanStatus_V
                                           where cr.ParticipantModuleCycleID == ParticipantModuleCycleID
                                           & (cr.RegisteredCharts + cr.PreviousChartData) != cr.NumberOfCharts
                                           select cr).First();
                        url = "OpenModule.aspx?mid=" + firstModule.ModuleID;
                    }
                    else
                    {
                        // check System and Self Assessment Surveys
                        var missingSystemOrSelfAssessment = (from pms in pim.ParticipantModuleSelection
                                                             where pms.ParticipantModuleCycleID == ParticipantModuleCycleID
                                                              & (pms.SelfAssessmentSurveyComplete == null | pms.SystemSurveyComplete == null
                                                              | pms.SelfAssessmentSurveyComplete == false | pms.SystemSurveyComplete == false)
                                                             select pms).Count();
                        if (missingSystemOrSelfAssessment > 0)
                        {
                            var SystemOrSelfAssessment = (from pms in pim.ParticipantModuleSelection
                                                          where pms.ParticipantModuleCycleID == ParticipantModuleCycleID
                                                           & (pms.SelfAssessmentSurveyComplete == null | pms.SystemSurveyComplete == null
                                                           | pms.SelfAssessmentSurveyComplete == false | pms.SystemSurveyComplete == false)
                                                          select pms).First();
                            HttpContext.Current.Session[Constants.SESSION_WORKINGMODULEID] = Convert.ToInt32(SystemOrSelfAssessment.ModuleID);
                            if (SystemOrSelfAssessment.SelfAssessmentSurveyComplete != true)
                                url = "SelfAssessment.aspx";
                            else
                                url = "SystemSurvey.aspx";

                        }
                        else
                        {
                            // check System and Self Assessment Surveys
                            var missingChartAbstractions = (from pms in pim.ParticipantDashboardModules_V
                                                            where pms.ParticipantModuleCycleID == ParticipantModuleCycleID
                                                             & pms.ChartsCompleted != pms.TotalCharts
                                                            select pms).Count();
                            if (missingChartAbstractions > 0)
                            {
                                var ChartAbstractions = (from pms in pim.ParticipantDashboardModules_V
                                                         where pms.ParticipantModuleCycleID == ParticipantModuleCycleID
                                                          & pms.ChartsCompleted != pms.TotalCharts
                                                         select pms).First();
                                url = "OpenModule.aspx?mid=" + ChartAbstractions.ModuleID;

                            }
                            else
                                url = "PerformanceReport.aspx";
                        }
                    }
                }
                else
                {
                    if (cycle.ImpactAssessmentComplete == false)
                        url = "ImpactStatement.aspx";
                    else
                        url = "Dashboard3.aspx";
                }
            }

        }
        return url;
    }

    public static void AddPatientRegistrationData(int PreviousParticipantModuleCycleID, int ActiveModuleCycleID)
    {
        using (NetHealthPIMEntities pim = new NetHealthPIMEntities())
        {
            using (TransactionScope transaction = new TransactionScope())
            {
                
                int cycleID = ActiveModuleCycleID;
                ParticipantModuleCycle cycle = pim.ParticipantModuleCycle.First(c => c.ParticipantModuleCycleID == cycleID);

                var modules = (from m in pim.ParticipantModulesForRemeasure_V
                               where m.ParticipantModuleCycleID == PreviousParticipantModuleCycleID

                               select new { m.ModuleID }).ToList();

                foreach (var _module in modules)
                {
                    int ModuleID = _module.ModuleID.Value;
                    DateTime baselineDate = DateTime.Today;

                    Module module = pim.Module.First(c => c.ModuleID == ModuleID);
                    ParticipantModuleSelection pms = new ParticipantModuleSelection();
                    pms.Module = module;
                    pms.ParticipantModuleCycle = cycle;
                    pms.ModuleComplete = false;
                    pms.Created = baselineDate;
                    pms.SelfAssessmentSurveyTargetDate = baselineDate.AddDays(28);
                    pms.SystemSurveyTargetDate = baselineDate.AddDays(28);
                    pms.ModuleCompleteTargetDate = baselineDate.AddDays(84);
                    pms.AbstractDataTargetDate = baselineDate.AddDays(28);
                    pms.ReportApprovedTargetDate = baselineDate.AddDays(28);
                    pms.MeasuresSelectedTargetDate = baselineDate.AddDays(42);
                    pms.ImprovementPlanTargetDate = baselineDate.AddDays(42);
                    pms.ImpactAssessmentTargetDate = baselineDate.AddDays(42);
                    pim.SaveChanges();


                }
                foreach (var _module in modules)
                {
                    int ModuleID = _module.ModuleID.Value;
                    if (ModuleID > 0)
                        AddInitialChartRegistration(ActiveModuleCycleID, ModuleID, Constants.ABO_CHARTS_REMEASURE);
                }
                transaction.Complete();

            }
        }
    }
}