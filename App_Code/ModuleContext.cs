﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using NetHealthPIMModel;

/// <summary>
/// Summary description for ModuleContext
/// </summary>
public class ModuleContext
{
	public ModuleContext()
	{
	}

    public static void Initialize(int moduleID)
    {
        Module ctx = (Module)HttpContext.Current.Session[Constants.SESSION_MODULECONTEXT];
        if (ctx == null || ctx.ModuleID != moduleID)
        {
            // Load static module instance to session.
            using (NetHealthPIMEntities pimEntities = new NetHealthPIMEntities())
            {
                var module = (from m in pimEntities.Module
                              where m.ModuleID == moduleID
                              select m).FirstOrDefault();
                HttpContext.Current.Session[Constants.SESSION_MODULECONTEXT] = module;
            }
        }
    }

    public static Module getModule()
    {
        Module ctx = (Module)HttpContext.Current.Session[Constants.SESSION_MODULECONTEXT];
        return ctx;
    }

    public static Module getModule(int ModuleID)
    {
        Initialize(ModuleID);
        Module ctx = (Module)HttpContext.Current.Session[Constants.SESSION_MODULECONTEXT];
        return ctx;
    }
}
