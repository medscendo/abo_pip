﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Net;
using System.Net.Mail;

/// <summary>
/// Summary description for SMTPEmail
/// </summary>
public class SMTPEmail
{
    public SMTPEmail()
    {
        //
        // TODO: Add constructor logic here
        //
    }
    public bool SendEmail(string EmailTo, string EmailSubject, string EmailBody, string ParticipantEmailAddress, string ParticipantName)
    {
        bool EmailSent = false;
        SmtpClient client = new SmtpClient("localhost");
        if (ParticipantEmailAddress == "")
        {
            ParticipantEmailAddress = "lpatrick@nethealthllc.com";
            ParticipantName = "Lauren Patrick";
        }
        string Bcc = System.Configuration.ConfigurationManager.AppSettings["Bcc"];
        MailAddress mabcc = new MailAddress(Bcc);
        MailAddress from = new MailAddress(ParticipantEmailAddress, ParticipantName, System.Text.Encoding.UTF8);
        MailAddress to = new MailAddress(EmailTo);
        MailMessage message = new MailMessage(from, to);
        message.Body = EmailBody;
        message.Subject = EmailSubject;
        message.SubjectEncoding = System.Text.Encoding.UTF8;
        message.Bcc.Add(mabcc);
        message.IsBodyHtml = true;
        try
        {
            client.Send(message);
            EmailSent = true;
        }
        catch (Exception e)
        {
        }
        return EmailSent;
    }

}
