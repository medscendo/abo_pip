﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.ComponentModel;
using System.ComponentModel.DataAnnotations;
using System.Web.DynamicData;


namespace NetHealthPIMModel
{
    /// <summary>
    /// Metadata configuration for the display of Resource Type records. 
    /// DisplayColumn = ResourceTypeName indicates that this is the field used in drop down controls.
    /// </summary>
    [DisplayColumn("ResourceTypeName")]
    [MetadataType(typeof(ResourceTypeMetadata))]
    public partial class ResourceType
    {
    }

    public class ResourceTypeMetadata
    {
    }
}