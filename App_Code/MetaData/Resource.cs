﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.ComponentModel;
using System.ComponentModel.DataAnnotations;

namespace NetHealthPIMModel
{
    /// <summary>
    /// Metadata configuration for the Resource entity.
    /// </summary>
    [MetadataType(typeof(ResourceMetadata))]
    public partial class Resource
    {
    }

    /// <summary>
    /// The attribute LastUpdateDate renders a hidden field thus eliminating the need to edit. 
    /// Descriptions are entered using the MultiLineText control
    /// The ResourceName uses a TextBox that provides a Width/Height property using the SizeAttribute attribute 
    /// </summary>
    public class ResourceMetadata
    {
        [ScaffoldColumn(false)]
        public object ResourceID { get; set; }

        [DisplayName("")]
        [UIHint("LastUpdateDate")]
        [DisplayFormat(DataFormatString = "{0:d/M/yyyy}")]
        public object LastUpdateDate { get; set; }

        [DisplayName("Resource Description: ")]
        [UIHint("MultiLineText")]
        public object ResourceDescription { get; set; }
        
        [DisplayName("Resource Name: ")]
        [UIHint("TextSize")]
        public object ResourceName { get; set; }

        [DisplayName("URL: ")]
        [UIHint("TextSize")]
        public object URL { get; set; }

        [DisplayName("Recommended Flag: ")]
        public object RecommendedFlag { get; set; }

        [DisplayName("Client Flag: ")]
        public object ClientFlag { get; set; }

        [UIHint("ManyToMany")]
        public object ResourceGroup { get; set; }

    }
}
