﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

/// <summary>
/// This attribute is used to add width to a input box used to edit Dynamic entities
/// </summary>
[AttributeUsage(AttributeTargets.Property | AttributeTargets.Field, AllowMultiple = false)]
public class SizeAttribute : Attribute
{
    public static SizeAttribute Default = new SizeAttribute(660, 0);
    public int Width { get; set; }
    public int Height { get; set; }
    public SizeAttribute(int width, int height)
    {
        Width = width;
        Height = height;
    }
}
