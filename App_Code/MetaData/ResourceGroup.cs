﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.ComponentModel;
using System.ComponentModel.DataAnnotations;

namespace NetHealthPIMModel
{
    /// <summary>
    /// Summary description for ResourceGroup
    /// </summary>
    [MetadataType(typeof(ResourceGroupMetadata))]
    public partial class ResourceGroup
    {
    }

    public class ResourceGroupMetadata
    {
        [ScaffoldColumn(false)]
        public object ResourceGroupID { get; set; }

        [DisplayName("Resource Category Name: ")]
        [UIHint("TextSize")]
        public object ResourceGroupName { get; set; }

        [DisplayName("")]
        [UIHint("LastUpdateDate")]
        [DisplayFormat(DataFormatString = "{0:d/M/yyyy}")]
        public object LastUpdateDate { get; set; }

    }
}