﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using NetHealthPIMModel;

public partial class copd_ParticipantFeedback : BasePage
{
    protected void Page_Load(object sender, EventArgs e)
    {
        if (!IsPostBack)
        {
            InitializePage();
        }
    }

    protected void InitializePage()
    {
        Session[Constants.SESSION_PAGE_ROLETYPE_LEVEL] = Constants.ROLETYPEID_GUEST;
        ((PIMMasterPage)Page.Master).SetTitle("Feedback");
        if (ctx != null && ctx.ParticipantID > 0)
        {
            using (NetHealthPIMEntities pim = new NetHealthPIMEntities())
            {
                Participant participant = pim.Participant.First(p => p.ParticipantID == ctx.ParticipantID);
                TextBoxEmailAddress.Text = participant.ParticipantEmailAddress;
            }
        }        
    }
    
    protected bool validForm()
    {
        bool ret = true;

        return ret;
    }

    protected bool isValid()
    {
        return true;
    }

    protected bool isComplete()
    {
        return true;
    }

    protected void ButtonSubmit_Click(object sender, EventArgs e)
    {
        if (validForm())
        {
            bool completeRecord = isComplete();
            bool validRecord = isValid();

            using (NetHealthPIMEntities pim = new NetHealthPIMEntities())
            {
                ParticipantFeedback feedback = new ParticipantFeedback();
                if (ctx != null && ctx.ParticipantID > 0) {
                    Participant participant = pim.Participant.First(p => p.ParticipantID == ctx.ParticipantID);
                    feedback.Participant = participant;
                }
                feedback.Module = pim.Module.First(m => m.ModuleID == module.ModuleID);
                feedback.ParticipantEmailAddress = TextBoxEmailAddress.Text;
                feedback.PhoneNumber = TextBoxPhoneNumber.Text;
                feedback.FeedbackText = TextBoxFeedbackText.Text;
                if (RadioButtonListContactRequested.SelectedIndex >= 0) {
                    feedback.ContactRequested = (RadioButtonListContactRequested.SelectedValue == Constants.RADIOBUTTONVALUE_YES); 
                }
                if (RadioButtonListContactOption.SelectedIndex >= 0)
                {
                    feedback.ContactOption = Int32.Parse(RadioButtonListContactOption.SelectedValue);
                }
                feedback.LastUpdateDate = DateTime.Now;
                pim.SaveChanges();
            }

            // TODO: Use a template base e-mail instead of hard-coding here (as well as the password reset) Send the e-mail 
            string messageBody = "<html><head><title>NetHealth - Contact</title></head><body>";
            messageBody += "Participant ID: " + ctx.ParticipantID + "<br />";
            if (RadioButtonListContactRequested.SelectedIndex >= 0) {
                messageBody += "Requested a Contact: " + RadioButtonListContactRequested.Items[RadioButtonListContactRequested.SelectedIndex].Text + "<br />";
            }
            messageBody += "E-Mail: " + TextBoxEmailAddress.Text + "<br />";
            messageBody += "Phone: " + TextBoxPhoneNumber.Text + "<br />";
            if (RadioButtonListContactOption.SelectedIndex >= 0)
            {
                messageBody += "Contact Method: " + RadioButtonListContactOption.Items[RadioButtonListContactOption.SelectedIndex].Text + "<bt />";
            }
            messageBody += "Feedback: <br /><pre>" + TextBoxFeedbackText.Text + "</pre><br />";
            messageBody += "</body></html>";
            EMail.SendEMail(module.ModuleCode.ToLower() + ".info@nethealthinc.com", "lpatrick@yahoo.com,lpatrick@nethealthinc.com", true, module.ModuleCode + " Contact", messageBody);

            Response.Redirect("~/ParticipantFeedbackSent.aspx");
        }
    }
}
