﻿<%@ Page Title="" Language="C#" MasterPageFile="~/PIMMasterPage.master" AutoEventWireup="true" CodeFile="SessionExpired.aspx.cs" Inherits="SessionExpired" %>

<asp:Content ID="Content1" ContentPlaceHolderID="head" Runat="Server">
</asp:Content>
<asp:Content ID="Content3" ContentPlaceHolderID="ContentPlaceHolder1" Runat="Server">
<center>
<br />
<br />
Your session has expired. Please return to the home page and logon again to continue. <br />
    <asp:HyperLink ID="HyperLinkHome" runat="server" NavigateUrl="~/Default.aspx" >Home Page</asp:HyperLink>
<br />
<br />
</center>

</asp:Content>
