﻿<%@ Page Language="C#" AutoEventWireup="true" CodeFile="DeleteModules.aspx.cs" Inherits="DeleteModules" EnableEventValidation="false"%>

<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">

<html xmlns="http://www.w3.org/1999/xhtml">
<head runat="server">
    <title></title>
    <script language="javascript">
        function areyousure(_this) {
            var className = (_this).className;
            //var className = document.getElementById(_this.id).className
            //alert(_this.id);
            return confirm("Are you sure you want to delete the Chart Abstractions for Activity " + className + "?");
        }
        function getObjectClass(obj) {
            if (obj && obj.constructor && obj.constructor.toString) {
                var arr = obj.constructor.toString().match(/function\s*(\w+)/);

                if (arr && arr.length == 2) {
                    return arr[1];
                }
            }

            return undefined;
        }

    </script>
</head>
<body>
    <form id="form1" runat="server">
    <div>
        
        
        
        
        <asp:ScriptManager ID="ScriptManager1" runat="server" EnablePageMethods="true">
        </asp:ScriptManager>
        
        
        <asp:Panel runat="server" ID="Panel1">
            
            
        </asp:Panel>
        
        <br />
        <br />
        <asp:UpdatePanel ID="UpdatePanel1" runat="server">
        <ContentTemplate>
        <asp:DropDownList runat="server" ID="DropDownListParticipant" AutoPostBack="true" OnSelectedIndexChanged="getCycles"></asp:DropDownList>
        <asp:DropDownList runat="server" ID="DropDownListPMC" AutoPostBack="true" OnSelectedIndexChanged="getModules"></asp:DropDownList>
        <br />
        <br />
        <table border="1px" cellpadding="2px">
        <asp:Repeater ID="RepeaterModules" runat="server">
            <HeaderTemplate>
            </HeaderTemplate>
            <ItemTemplate>
            <asp:HiddenField ID="HiddenFieldModuleID" Value='<%# Eval("ModuleID") %>' runat="server" />
            <tr class="bg">
                <td class="first" style="padding-left:20px;"><asp:LinkButton runat="server" ID="LinkButtonDelete" CssClass='<%# Eval("ModuleName")%>' CommandName="DeleteCharts" CommandArgument='<%# Eval("ModuleID") + "," + Eval("ParticipantModuleCycleID") %>' OnCommand="DeleteCharts_Click" OnClientClick='return areyousure(this);'>delete</asp:LinkButton></td>
                <td><%# Eval("ModuleName")%></td>
                <td><%# Eval("LabelModulesStatus")%></td>
                <td class="prior-data"><%# Eval("LabelPriorData")%></td>
                <td class="last"><%# Eval("LabelComments")%></td>
            </tr>
            </ItemTemplate>
            <FooterTemplate>
            </FooterTemplate>
        </asp:Repeater>  
        </table>          
        
        </ContentTemplate>
        </asp:UpdatePanel>
        

    </div>
    </form>
</body>
</html>
