﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using NetHealthPIMModel;
using System.Transactions;
using System.Data.SqlClient;
using System.Data.EntityClient;
using System.Data;

public partial class DeleteModules : System.Web.UI.Page
{
    string participantID = "";
    protected void Page_Load(object sender, EventArgs e)
    {
        if (!IsPostBack)
        {
            using (NetHealthPIMEntities pim = new NetHealthPIMEntities())
            {
                var participant = from pms in pim.Participant
                                  where pms.SystemToRegister == "1" || pms.SystemToRegister == "3"
                                  orderby pms.ParticipantLastName, pms.ParticipantID
                                  select new
                                  {
                                      ParticipantID = pms.ParticipantID,
                                      ParticipantName = pms.ParticipantLastName + " " + pms.ParticipantFirstName
                                      //ParticipantName = (pms.ParticipantLastName == null ? " " : pms.ParticipantLastName) + " " +
                                      //(pms.ParticipantFirstName == null ? " " : pms.ParticipantFirstName) + " (" + pms.ASNGUID + ")"
                                      //string.Format("{0}, {1}", pms.ParticipantLastName, pms.ParticipantFirstName)
                                  };
                DropDownListParticipant.DataTextField = "ParticipantName";
                DropDownListParticipant.DataValueField = "ParticipantID";
                DropDownListParticipant.DataSource = participant;
                DropDownListParticipant.DataBind();
            }
        }
        participantID = DropDownListParticipant.SelectedItem.Value;
    }

    protected void getCycles(object sender, EventArgs e)
    {
        int participantID = Convert.ToInt32(DropDownListParticipant.SelectedItem.Value);

        using (NetHealthPIMEntities pim = new NetHealthPIMEntities())
        {
            var participant = from pms in pim.ParticipantModuleCycle
                              where pms.ParticipantModule.ParticipantID == participantID
                              orderby pms.ParticipantModuleCycleID
                              select new 
                              {
                                  ParticipantModuleCycleID = pms.ParticipantModuleCycleID,
                                  CycleNumber = pms.CycleNumber,
                                  CycleStartDate = pms.CycleStartDate
                                  //string.Format("{0}, {1}", pms.ParticipantLastName, pms.ParticipantFirstName)
                              };
            var x = from c in participant.AsEnumerable()
                    select new PairValue
                    {
                        KeyID = c.ParticipantModuleCycleID,
                        KeyName = "Cycle #:" + c.CycleNumber.ToString() + " Started Date: " + c.CycleStartDate.ToString()
                    };
            DropDownListPMC.DataTextField = "KeyName";
            DropDownListPMC.DataValueField = "KeyID";
            DropDownListPMC.DataSource = x;
            DropDownListPMC.DataBind();
            PopulateCycles();
        }
        DropDownListParticipant.Focus();
    }
    protected void getModules(object sender, EventArgs e)
    {
        PopulateCycles();
    }
    protected void DeleteCharts_Click(object sender, CommandEventArgs e)
    {
        
        var args = (string)e.CommandArgument;
        string[] arrArgs = args.Split(',');
        int ModuleID = Convert.ToInt32(arrArgs[0]);
        int ParticipantModuleCycleID = Convert.ToInt32(arrArgs[1]);
        DeleteRegistration(ModuleID, ParticipantModuleCycleID);
        PopulateCycles();

    }
    public void PopulateCycles()
    {
        using (NetHealthPIMEntities pim = new NetHealthPIMEntities())
        {
            if (DropDownListPMC.SelectedIndex >= 0)
            {
                int ActiveModuleCycleID = Convert.ToInt32(DropDownListPMC.SelectedItem.Value);

                var participantModulesSelected = (from pms in pim.ParticipantPlanStatus_V
                                                  where pms.ParticipantModuleCycleID == ActiveModuleCycleID
                                                  select pms);
                RepeaterModules.DataSource = participantModulesSelected;
                RepeaterModules.DataBind();
            }
        }
    }

    public  void DeleteRegistration(int ModuleID, int ParticipantModuleCycleID)
    {
        
        using (NetHealthPIMEntities pim = new NetHealthPIMEntities())
        {
            var module = (from pms in pim.Module
                          where pms.ModuleID == ModuleID
                          select pms).FirstOrDefault();
            var chartAbstrationTable = module.ChartAbstrationTable;
            
            var chartsToDelete = (from cr in pim.ChartRegistration
                                  where cr.ParticipantModuleCycleID == ParticipantModuleCycleID
                                  && cr.ModuleID == ModuleID
                                  select cr);
            foreach (var c in chartsToDelete)
            {
                pim.DeleteObject(c);
            }

            DeleteChartRegistration(ParticipantModuleCycleID, chartAbstrationTable);

            pim.SaveChanges();
            
        }
    }

    public static void DeleteChartRegistration(int ParticipantModuleCycleID, string AbstractionTable)
    {
        if (AbstractionTable.Trim() != "")
        {
            using (EntityConnection conn = new EntityConnection("name=NetHealthPIMEntities"))
            {

                SqlConnection sqlConnection = new SqlConnection(conn.StoreConnection.ConnectionString);
                sqlConnection.Open();
                SqlCommand cmd = new SqlCommand();
                cmd.Connection = sqlConnection;
                cmd.CommandText = "DELETE FROM " + AbstractionTable + " WHERE ParticipantModuleCycleID = " + ParticipantModuleCycleID.ToString();
                cmd.CommandType = CommandType.Text;
                cmd.ExecuteNonQuery();
                sqlConnection.Close();

                //EntityCommand cmd = conn.CreateCommand();
                //cmd.CommandType = CommandType.Text;
                //cmd.CommandText = "DELETE FROM " + AbstractionTable + " WHERE ParticipantModuleCycleID = " + ParticipantModuleCycleID.ToString() + " AND RecordIdentifier = '" + RecordIdentifier + "'";
                //cmd.ExecuteNonQuery();
                //conn.Close();
            }
        }
    }

}