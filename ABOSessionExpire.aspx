﻿<%@ Page Language="C#" AutoEventWireup="true" CodeFile="ABOSessionExpire.aspx.cs" Inherits="ABOSessionExpire" %>

<!DOCTYPE html>
<!--[if lt IE 7]>      <html class="no-js lt-ie9 lt-ie8 lt-ie7"> <![endif]-->
<!--[if IE 7]>         <html class="no-js lt-ie9 lt-ie8"> <![endif]-->
<!--[if IE 8]>         <html class="no-js lt-ie9"> <![endif]-->
<!--[if gt IE 8]><!--> <html class="no-js"> <!--<![endif]-->
<head id="Head1" runat="server">
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <title>Improvement in Medical Practice Platform</title>

    <link href="common/css/normalize.css" rel="stylesheet" type="text/css" />
    <link href="common/css/style.css" rel="stylesheet" type="text/css" />
    <script src="//ajax.googleapis.com/ajax/libs/jquery/1.10.2/jquery.min.js"></script>
    <script>window.jQuery || document.write('<script src="common/js/jquery-1.10.2.min.js"><\/script>')</script>
    <link href="common/css/jquery-ui-1.9.2.custom.css" rel="stylesheet" type="text/css" />
    <script src="//ajax.googleapis.com/ajax/libs/jqueryui/1.10.3/jquery-ui.min.js"></script>

    <!--[if IE 6]>
    <script  type="text/javascript" src="common/js/jq-png-min.js"></script>
    <link href="common/css/ie.css" rel="stylesheet" type="text/css" />
    <![endif]-->
    <!--[if gte IE 9]>
      <style type="text/css">
        .gradient {
           filter: none;
        }
      </style>
    <![endif]-->
</head>
<body>
<form id="form2" runat="server">
<!-- wrapper starts -->

<div id="main">
  <div class="wrapper">
    <!-- header starts -->
    <div id="header">
      <div class="logo"><a id="A1" href="~/abo/PIPStatus.aspx" runat="server" ><img src="common/images/logo.png" alt="" /></a></div>
      <div class="header_right" >
        <ul>
<%--          <li class="first"><a href="~/abo/PIPStatus.aspx" runat="server" id="HyperLinkMainPage" style="font-size: 11pt;">Main Page</a></li>
          <li><a href="~/abo/Support.aspx" runat="server" id="HyperLinkSupport" style="font-size: 11pt;">Support</a></li>
          <li><a href="~/abo/ContactUs.aspx" runat="server" id="HyperLinkContactUs" style="font-size: 11pt;">Contact Us</a></li>
          <li><a href="~/abo/MyAccount.aspx" runat="server" id="HyperLinkMyAccount" style="font-size: 11pt;">My Account</a></li>
--%>        </ul>
         </div>
    </div>
    <!-- header ends -->
  </div>
  <!-- body container -->
  <div id="outer"><!-- just added-->
  <div id="outer_body_container">
    <div class="wrapper">
      <div id="body_container">
        <div class="top_section">
          <div class="bread_crumb">
            <ul>
                Session Expired
<%--              <li><a href="../abo/PIPStatus.aspx">Main Page »</a></li><asp:Literal ID="LiteralPageTitle" runat="server"></asp:Literal>
--%>            </ul>
          </div>
          <div class="time"><asp:Literal ID="LiteralCurrentDateTime" runat="server"></asp:Literal></div>
        </div>
            <!-- main content -->
            <table border="0" cellspacing="0" class="edit_table center_area ">
                <tr>
                    <td style="padding: 30px;">Your session of the ABO Improvement in Medical Practice Platform has expired. 
                    <br /><br />To log on again, go to <a href="http://abop.org">Login to ABO</a></td>
                </tr>
            </table>
            <!-- main content ends -->
        <div class="bottom_strip"></div>
        <span class="clear"></span> </div>
      <span class="clear"></span> </div>
  </div>
  </div> <!-- just added-->
  <!-- body container ends -->
  <!-- footer -->
  <div id="outer_footer">
    <div class="wrapper">
      <div id="footer">
        <div class="col1" style="width: 500;">
          <h4>Our Office</h4>
          <p><strong>American Board of Ophthalmology</strong><br />
            111 Presidential Boulevard, Suite 241<br />
            Bala Cynwyd, PA 19004-1075</p>

          <p>&copy; 2012 American Board of Ophthalmology</p>
        </div>
        <div  class="col2">
          <ul>
            <li class="none"><a href="sitemap.aspx">Sitemap</a></li>
            <li><a href="http://abop.org/privacy-policy/" target="_blank">Privacy Policy </a></li>
             <li ><a href="http://www.abop.org">Back to ABO Main Site</a></li>
          </ul>
          <p>A Founding Member Board of the American <br />
            Board of Medical Specialties</p>
          <span class="board_logo"><a href="#"><img src="common/images/footer_logo.png" alt="" /></a></span>
        </div>
    
      </div>
    </div>
  </div>
  <!-- footer ends -->
</div>
<!-- wrapper starts -->
</form>
</body>
</html>
