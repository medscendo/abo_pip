﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using NetHealthPIMModel;

public partial class copd_Default : BasePage
{
    protected void Page_Load(object sender, EventArgs e)
    {
        if (!IsPostBack)
        {
            InitializePage();
        }
        // If authenticated redirect to the index page
        Module module = ModuleContext.getModule();
        UserContext ctx = (UserContext)Session[Constants.SESSION_USERCONTEXT];
        String UserCSTKey = Request.QueryString["CST_KEY"];
        //UserCSTKey = "abo430";
        using (NetHealthPIMEntities pim = new NetHealthPIMEntities())
        {
            var participantRegistered = (from p in pim.Participant where p.ASNGUID == UserCSTKey && (p.SystemToRegister == "1" || p.SystemToRegister == "3") select p).Count();
            if (participantRegistered > 0)
                ((PIMMasterPage)Page.Master).AuthenticateViaGUID(UserCSTKey);
            else
                Response.Redirect("http://abop.org/");
        }

        if (ctx != null && ctx.IsAuthenticated)
            Response.Redirect(GetDefaultRedirectByRole(ctx.RoleTypeID), true); //Go to next page based on Role

    }

    protected void InitializePage()
    {
        ModuleContext.Initialize(Constants.MODULE_COPD);
        Module module = ModuleContext.getModule();

        Session[Constants.SESSION_PAGE_ROLETYPE_LEVEL] = Constants.ROLETYPEID_GUEST;
        ((PIMMasterPage)Page.Master).SetTitle("American Board of Ophthalmology Improvement in Medical Practice Activity");
    }
}
