﻿using NetHealthPIMModel;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Transactions;
using System.IO;
using System.Xml;
using System.CodeDom;
using System.CodeDom.Compiler;
using System.Security.Permissions;
using System.Web.Services;
using System.Web.Services.Description;
using System.Web.Services.Discovery;
// Optional: For use with the strongly-typed "Generated Class"
//using System.Xml.Serialization;
//using Avectra.XWebLab.Generated;

public partial class PIMMasterPage : System.Web.UI.MasterPage
{
    private UserContext ctx = null;
    private Module module = null;

    /// <summary>
    /// The Page_Load method is executed for all the pages and all the modules in the system.
    /// </summary>
    /// <remarks>
    /// </remarks>
    /// <param name="sender"></param>
    /// <param name="e"></param>
    protected void Page_Load(object sender, System.EventArgs e) {
        if (!IsPostBack) {
            LiteralCurrentDateTime.Text = System.DateTime.Now.ToLongDateString();
            // If no user has connected to the site try to read the information from the Cookie to authenticate. 
            if (ctx == null) {
                //Check if the browser support cookies 
                if (Request.Browser.Cookies) {
                    //Check if the cookies with name PBLOGIN exist on user's machine 
                    string path = Request.Path.ToUpper();
                    if (!path.Contains("LOGOFF") && !path.Contains("LOGINTOABO") && !path.Contains("SUPPORT") && !path.Contains("CONTACTUS") && !path.Contains("PARTICIPANTPROFILE")) {
                        // Password is encrypted in the cookie
                        String UserCSTKey = Request.QueryString["CST_KEY"];
                        if (UserCSTKey == null) {
                            Response.Redirect("~/LoginToABO.aspx");
                        }
                        else {
                            using (NetHealthPIMEntities pim = new NetHealthPIMEntities()) {
                                var participantRegistered = (from p in pim.Participant where p.ASNGUID == UserCSTKey && (p.SystemToRegister == "1" || p.SystemToRegister == "3") select p).Count();
                                if (participantRegistered > 0)
                                    AuthenticateViaGUID(UserCSTKey);
                                else
                                    Response.Redirect("http://abop.org/");
                            }
                        }
                    }
                }
            }
            if (ctx != null && ctx.IsAuthenticated && module != null) {
                // Verify that the user has accepted the module overview or take the user to the Oveview page.  
                using (NetHealthPIMEntities pim = new NetHealthPIMEntities()) 
                {

                    ParticipantModuleCycle cycle = pim.ParticipantModuleCycle.FirstOrDefault(c => c.ParticipantModuleCycleID == ctx.ActiveModuleCycleID);
                    if (cycle != null && cycle.CycleNumber==1)
                    {
                        A4.HRef = "javascript:void(0)";
                        if (cycle.BusinessAssociateAgreementComplete == null)
                        {
                            A2.HRef = "javascript:void(0)";
                            A1.HRef = "javascript:void(0)";

                        }
                        else
                        {
                            A2.HRef = "~/abo/Dashboard.aspx";
                            A1.HRef = "~/abo/ModuleSelectionReview.aspx";
                        }

                    }
                    else if (cycle != null && cycle.CycleNumber == 2)
                    {
                        if (cycle.ImpactAssessmentCompletedDate== null)
                        {
                            A4.HRef = "javascript:void(0)";
                    
                        }
                        else
                        {
                            A4.HRef = "~/abo/ModuleCompletedToDate.aspx";
                          
                        }

                    }

                    // Check if the participant is registered in the module. 
                    var partInModule = (from pm in pim.ParticipantModule
                                        where pm.ModuleID == module.ModuleID && pm.ParticipantID == ctx.ParticipantID
                                        select pm).Count();
                    // If the participant has already registered to the module redirect to the index or the overview if they have not cleared that step
                    if (partInModule > 0) {
                        ParticipantModule participantModule = (from pm in pim.ParticipantModule
                                                               where pm.ModuleID == module.ModuleID && pm.ParticipantID == ctx.ParticipantID
                                                               select pm).First<ParticipantModule>();
                    }
                    // If the participant has not registered redirect to an admin page or a "register to module" page. 
                    // TODO: Register to module page for multi-module systems
                    else {
                        switch (ctx.RoleTypeID) {
                            case Constants.ROLETYPEID_CLIENT_ADMIN:
                                Response.Redirect("~/sa/Administration.aspx", true);
                                return;
                            case Constants.ROLETYPEID_SYSTEM_ADMIN:
                                Response.Redirect("~/sa/SystemAdministration.aspx", true);
                                return;
                        }
                    }
                }
                DisplayWelcomeMessage();
            }
        }
    }

    protected override void OnInit(EventArgs e)
    {
        base.OnInit(e);
            ctx = (UserContext)Session[Constants.SESSION_USERCONTEXT];
            module = ModuleContext.getModule();

    }

    public bool AuthenticateViaGUID(string GUID)
    {
        bool retValue = true;
        string UserName = "";
        string Password = "";
        int count = 0;
        using (NetHealthPIMEntities pim = new NetHealthPIMEntities())
        {
            count = (from p in pim.Participant
                         where p.ASNGUID == GUID 
                         select p).Count();
            if (count == 1)
            {
                Participant participantInfo = (from p in pim.Participant
                                               where p.ASNGUID == GUID
                                               select p).First<Participant>();
                UserName = GUID; //participantInfo.ParticipantEmailAddress;
                Password = participantInfo.Password;
            }
        }
        if ((count == 1) && (UserName != ""))
            AuthenticateUser(UserName, Password, true);
        else
            retValue = false;
        return retValue;
    }

    public bool AuthenticateUser(string UserName, string Password, bool refreshCookie)
    {
        string strEmailAddress = "";
        int numberOfGroups = 0; 

        using (NetHealthPIMEntities pim = new NetHealthPIMEntities())
        {
            var count = (from p in pim.Participant
                         where p.ASNGUID == UserName && p.Password == Password
                         select p).Count();
            if (count == 1)
            {
                var participantExists = (from p in pim.Participant where p.ASNGUID == UserName && p.Password == Password
                                   select new { p.ParticipantID, p.ParticipantLastName, p.ParticipantFirstName, p.DegreeType.DegreeTypeName, p.RoleTypeID , p.SoftwareVersion}).First();
                if (participantExists != null)
                {
                    Module module = ModuleContext.getModule();
                    ctx = new UserContext();
                    ctx.ActiveModuleID = module.ModuleID;
                    ctx.ParticipantID = participantExists.ParticipantID;
                    ctx.ParticipantName = participantExists.ParticipantFirstName + " " + participantExists.ParticipantLastName;// +", " + participant.DegreeTypeName;
                    ctx.IsAuthenticated = true;
                    ctx.RoleTypeID = Convert.ToInt32(participantExists.RoleTypeID);
                    var groupsParticipant = from g in pim.ParticipantGroup
                                            join gm in pim.ParticipantGroupMember
                                            on g.ParticipantGroupID equals gm.ParticipantGroupID
                                            where gm.ParticipantID == ctx.ParticipantID
                                            select new { g.ParticipantGroupID, g.ParticipantGroupName, g.Participant.ParticipantID, g.MinAbstractsRequired };
                    foreach (var v in groupsParticipant)
                    {
                        numberOfGroups = numberOfGroups + 1;
                        ctx.LeaderParticipantID = v.ParticipantID;
                        ctx.ParticipantGroupID = v.ParticipantGroupID;
                        ctx.MinAbstractsRequired = (int)v.MinAbstractsRequired;
                    }

                    if (participantExists.SoftwareVersion == null)
                        ctx.SoftwareVersion = 0;
                    else
                        ctx.SoftwareVersion = (int)participantExists.SoftwareVersion; 
                    Session[Constants.SESSION_USERCONTEXT] = ctx;
                    LabelParticipantName.Text = ctx.ParticipantName;
                    // If the request cookie is there, update the expiration date.
                    if (Request.Browser.Cookies && refreshCookie)
                    {
                        // Password is stored in encrypted form in the cookie
                        Response.Cookies["PBLOGIN"]["UNAME"] = Server.UrlEncode(strEmailAddress);
                        Response.Cookies["PBLOGIN"].Expires = DateTime.Now.AddDays(30);
                    }
                }
            }
        }

        if (ctx != null && ctx.IsAuthenticated) //&& module != null
        {
            // Verify that the user has accepted the module overview or take the user to the Oveview page.  
            using (NetHealthPIMEntities pim = new NetHealthPIMEntities())
            {
                Session[Constants.SESSION_PREVIOUSCYCLE] = null;
                // Check if the participant is registered in the module. 
                var partInModule = (from pm in pim.ParticipantModule
                                    where pm.ModuleID == module.ModuleID && pm.ParticipantID == ctx.ParticipantID
                                    select pm).Count();
                // If the participant has already registered to the module redirect to the index or the overview if they have not cleared that step
                if (partInModule > 0)
                {
                    ParticipantModule participantModule = (from pm in pim.ParticipantModule
                                                           where pm.ModuleID == module.ModuleID && pm.ParticipantID == ctx.ParticipantID
                                                           select pm).First<ParticipantModule>();
                        CycleManager.CheckCycle();
                        if (ctx != null)
                        {
                            ParticipantModuleCycle participantModuleCycle = (from pmc in pim.ParticipantModuleCycle
                                                                             where pmc.ParticipantModuleCycleID == ctx.ActiveModuleCycleID
                                                                             select pmc).First<ParticipantModuleCycle>();

                            //Session[Constants.SESSION_CYCLENUMBER] = participantModuleCycle.CycleNumber;
                            ParticipantModuleCycle cycle = pim.ParticipantModuleCycle.First(c => c.ParticipantModuleCycleID == ctx.ActiveModuleCycleID);
                            if (cycle.CycleNumber == 2)
                            {
                                cycle.PrevParticipantModuleCycleReference.Load();
                                ParticipantModuleCycle prev = cycle.PrevParticipantModuleCycle;
                                cycle = prev;

                            }
                            Session[Constants.SESSION_PREVIOUSCYCLE] = cycle;
                        }
                        DisplayWelcomeMessage();
                        String url = Request.AppRelativeCurrentExecutionFilePath.ToUpper();
                        if ((url.EndsWith("/LOGINTOABO.ASPX")) || (url.EndsWith("/DEFAULT.ASPX")))
                        {
                            ParticipantModuleCycle cycle = pim.ParticipantModuleCycle.First(c =>c.ParticipantModule.ParticipantID==ctx.ParticipantID && c.CycleNumber==1);
                            string newaspx = CycleManager.InitialWhereYouLeftOff(ctx.ActiveModuleCycleID, ctx.ParticipantID);
                            if (cycle.BusinessAssociateAgreementComplete == null)
                            {
                                Response.Redirect(url.Substring(0, url.LastIndexOf("/")) + "/abo/ParticipantProfile.aspx");
                            }
                            else
                            {
                                if (newaspx == "")
                                    Response.Redirect(url.Substring(0, url.LastIndexOf("/")) + "/abo/PIPStatus.aspx");
                                else
                                    Response.Redirect(url.Substring(0, url.LastIndexOf("/")) + "/abo/" + newaspx);
                            }
                        }
                }
                // If the participant has not registered redirect to an admin page or a "register to module" page. 
                // TODO: Register to module page for multi-module systems
                else
                {
                    switch (ctx.RoleTypeID) {
                        case Constants.ROLETYPEID_CLIENT_ADMIN:
                            Response.Redirect("~/sa/Administration.aspx", true);
                            return true;
                        case Constants.ROLETYPEID_SYSTEM_ADMIN:
                            Response.Redirect("~/sa/SystemAdministration.aspx", true);
                            return true;
                    }
                }
            }
            DisplayWelcomeMessage();
        }

        return false;
    }

    public bool AuthenticateUser_OLD(string UserName, string Password, bool refreshCookie)
    {
        string email = UserName.ToLower();
        using (NetHealthPIMEntities pim = new NetHealthPIMEntities())
        {
            var count = (from p in pim.Participant where p.ParticipantEmailAddress == email && p.Password == Password select p).Count();
            if (count == 1)
            {
                var participant = (from p in pim.Participant
                                   where p.ParticipantEmailAddress == email && p.Password == Password
                                   select new { p.ParticipantID, p.ParticipantLastName, p.ParticipantFirstName, p.DegreeType.DegreeTypeName, p.RoleTypeID }).First();
                if (participant != null)
                {
                    Module module = ModuleContext.getModule();
                    ctx = new UserContext();
                    ctx.ActiveModuleID = module.ModuleID;
                    ctx.ParticipantID = participant.ParticipantID;
                    ctx.ParticipantName = participant.ParticipantFirstName + " " + participant.ParticipantLastName + ", " + participant.DegreeTypeName;
                    ctx.IsAuthenticated = true;
                    ctx.RoleTypeID = Convert.ToInt32(participant.RoleTypeID);
                    Session[Constants.SESSION_USERCONTEXT] = ctx;
                    // If the request cookie is there, update the expiration date.
                    if (Request.Browser.Cookies && refreshCookie)
                    {
                        // Password is stored in encrypted form in the cookie
                        Response.Cookies["PBLOGIN"]["UNAME"] = Server.UrlEncode(UserName);
                        Response.Cookies["PBLOGIN"]["UPASS"] = Server.UrlEncode(Password);
                        Response.Cookies["PBLOGIN"].Expires = DateTime.Now.AddDays(30);
                    }
                    return true;
                }
                else
                {
                    if (Request.Browser.Cookies)
                        Response.Cookies["PBLOGIN"].Expires = DateTime.Now.AddDays(-1);
                    return false;
                }
            }
            else
            {
                if (Request.Browser.Cookies)
                    Response.Cookies["PBLOGIN"].Expires = DateTime.Now.AddDays(-1);
                return false;
            }
        }
    }

    protected bool AuthenticateUserViaQS(string UserCSTKey, bool refreshCookie, bool requiredASNAuthentication)
    {
        string strFirstName = "";
        string strLastName = "";
        string strEmailAddress = "";
        string strGroup = "";
        string strEventKey = System.Configuration.ConfigurationSettings.AppSettings["EventKey"];
        string strParticipantTitle = "";
        string strAddress1 = "";
        string strAddress2 = "";
        string strCity = "";
        string strPhoneNumber = "";
        string strPostalCode = "";
        bool retValue = true;
        int numberOfGroups = 0;

        if (requiredASNAuthentication)
        {
            netForumXML.netForumXML nfXml = new netForumXML.netForumXML();
            nfXml.Url = System.Configuration.ConfigurationSettings.AppSettings["netForumXML.netforumxml"];//"http://members.asn-online.org/xweb/secure/netforumxml.asmx";
            nfXml.Authenticate("nethealth", "n3phrology"); // Replace with actual password.

            string strObject = "EventsRegistrant @TOP 1";
            string strColumns = "reg_egp_key, reg_evt_key, reg_add_date, reg_registration_date, reg_complimentary_event_registered_flag, reg_guest_flag, rdm_custom_flag_01, cst_key, cst_org_name_dn, cst_recno, cst_eml_address_dn, cst_phn_number_complete_dn, evt_key, evt_title, ind_first_name, ind_mid_name, ind_last_name, ind_designation, adr_line1, adr_line2, adr_line3, adr_city, adr_state, adr_post_code, adr_country";
            string strWhere = "cst_key='" + UserCSTKey + "' AND evt_key='" + strEventKey + "'";
            string strOrderBy = "";

            try
            {
                // Make the call to GetQuery
                XmlNode node = nfXml.GetQuery(
                    strObject,
                    strColumns,
                    strWhere,
                    strOrderBy
                );

                // Lists the ind_first_name for each Individual node.
                foreach (XmlNode xnIndividual in node.ChildNodes)
                {
                    strFirstName = xnIndividual["ind_first_name"].InnerText.ToString();
                    strLastName = xnIndividual["ind_last_name"].InnerText.ToString();
                    strEmailAddress = xnIndividual["cst_eml_address_dn"].InnerText.ToString();
                    strGroup = xnIndividual["reg_egp_key"].InnerText.ToString();
                    strParticipantTitle = xnIndividual["ind_designation"].InnerText.ToString();
                    strAddress1 = xnIndividual["adr_line1"].InnerText.ToString();
                    strAddress2 = xnIndividual["adr_line2"].InnerText.ToString();
                    strCity = xnIndividual["adr_city"].InnerText.ToString();
                    strPhoneNumber = xnIndividual["cst_phn_number_complete_dn"].InnerText.ToString();
                    strPostalCode = xnIndividual["adr_post_code"].InnerText.ToString();
                    if (xnIndividual["cst_key"].InnerText.ToString() != UserCSTKey) // this is an invalid token
                    {
                        Response.Redirect("http://www.asn-online.org/");
                    }
                }
            }
            catch (Exception ex)
            {
                Response.Redirect("http://www.asn-online.org/");
            }
        }
        else
            strLastName = "lpatrick";

        if (strLastName != "") // User does exists on ASN Database
        {
            using (NetHealthPIMEntities pim = new NetHealthPIMEntities())
            {
                var activeModule = (from m in pim.Module
                                    where m.ModuleID == Constants.MODULE_COPD
                                    select m).First<Module>();

                var participant = (from p in pim.Participant
                                   where p.ASNGUID == UserCSTKey
                                   select p).Count<Participant>();
                if (participant > 0)
                {
                    // User already exists
                    var participantExists = (from p in pim.Participant
                                             where p.ASNGUID == UserCSTKey
                                             select new { p.ParticipantID, p.ParticipantLastName, p.ParticipantFirstName, p.DegreeType.DegreeTypeName, p.RoleTypeID }).First();
                    if (participantExists != null)
                    {
                        Module module = ModuleContext.getModule();
                        ctx = new UserContext();
                        ctx.ActiveModuleID = module.ModuleID;
                        ctx.ParticipantID = participantExists.ParticipantID;
                        ctx.ParticipantName = participantExists.ParticipantFirstName + " " + participantExists.ParticipantLastName;// +", " + participant.DegreeTypeName;
                        ctx.IsAuthenticated = true;
                        ctx.RoleTypeID = Convert.ToInt32(participantExists.RoleTypeID);;
                        var groupsParticipant = from g in pim.ParticipantGroup
                                                join gm in pim.ParticipantGroupMember
                                                on g.ParticipantGroupID equals gm.ParticipantGroupID
                                                where gm.ParticipantID == ctx.ParticipantID
                                                select new { g.ParticipantGroupID, g.ParticipantGroupName, g.Participant.ParticipantID, g.MinAbstractsRequired };
                        foreach (var v in groupsParticipant)
                        {
                            numberOfGroups = numberOfGroups + 1;
                            ctx.LeaderParticipantID = v.ParticipantID;
                            ctx.ParticipantGroupID = v.ParticipantGroupID;
                            ctx.MinAbstractsRequired = (int)v.MinAbstractsRequired;
                        }

                        Session[Constants.SESSION_USERCONTEXT] = ctx;
                        LabelParticipantName.Text = ctx.ParticipantName;
                        // If the request cookie is there, update the expiration date.
                        if (Request.Browser.Cookies && refreshCookie)
                        {
                            // Password is stored in encrypted form in the cookie
                            Response.Cookies["PBLOGIN"]["UNAME"] = Server.UrlEncode(strEmailAddress);
                            Response.Cookies["PBLOGIN"].Expires = DateTime.Now.AddDays(30);
                        }
                    }
                    else
                    {
                        if (Request.Browser.Cookies)
                        {
                            Response.Cookies["PBLOGIN"].Expires = DateTime.Now.AddDays(-1);
                        }
                        retValue = false;
                    }

                }
                else
                {
                    int ParticipantID = SequenceGenerator.GetNextSequenceValue(Constants.SEQUENCE_PARTICIPANTID);

                    Participant p = new Participant();
                    p.ParticipantID = ParticipantID;
                    p.ParticipantLastName = strLastName;
                    p.ParticipantFirstName = strFirstName;
                    p.ParticipantEmailAddress = strEmailAddress;
                    p.ParticipantTitle = strParticipantTitle;
                    p.AddressLine1 = strAddress1;
                    p.AddressLine2 = strAddress2;
                    p.City = strCity;
                    p.PhoneNumber = strPhoneNumber;
                    p.PostalCode = strPostalCode;
                    p.RoleTypeID = Constants.ROLETYPEID_PARTICIPANT;
                    p.Password = Security.EncryptPassword(strEmailAddress, strLastName);
                    p.ASNGUID = UserCSTKey;
                    p.DateAdded = DateTime.Now;
                    p.LastUpdateDate = DateTime.Now;
                    p.DateOfBirth = null;
                    //p.DegreeType = strDegree;
                    // Save all changes
                    // Add now the Participant Module record. 
                    ParticipantModule pm = new ParticipantModule();
                    pm.Module = activeModule;
                    pm.Participant = p;
                    pm.LastUpdateDate = DateTime.Now;

                    pim.SaveChanges();

                    Module module = ModuleContext.getModule();
                    ctx = new UserContext();
                    ctx.ActiveModuleID = module.ModuleID;
                    ctx.ParticipantID = p.ParticipantID;
                    ctx.ParticipantName = p.ParticipantFirstName + " " + p.ParticipantLastName;// +", " + participant.DegreeTypeName;
                    ctx.IsAuthenticated = true;
                    ctx.RoleTypeID = Convert.ToInt32(p.RoleTypeID);
                    ctx.ParticipantGroupID = 0;
                    ctx.LeaderParticipantID = 0;
                    if (p.SoftwareVersion == null)
                        ctx.SoftwareVersion = 0;
                    else
                        ctx.SoftwareVersion = (int)p.SoftwareVersion;
                    Session[Constants.SESSION_USERCONTEXT] = ctx;
                    LabelParticipantName.Text = ctx.ParticipantName;

                }
            }
            if (strGroup!="") {
                CreateGroup(strGroup, strEventKey);
            }

            if (numberOfGroups > 1)
            {
                RedirectToSelectGroup();
            }

            return retValue;
        }
        else
        {
            Response.Redirect("http://www.asn-online.org/");
            return false;
        }

    }

    protected void CreateGroup(string reg_egp_key, string evt_key)
    {
        using (NetHealthPIMEntities pim = new NetHealthPIMEntities())
        {
            Module module = ModuleContext.getModule();
            ctx = new UserContext(); 
            using (TransactionScope transaction = new TransactionScope())
            {
                int ModuleID = module.ModuleID;
                var activeModule = (from m in pim.Module
                                    where m.ModuleID == ModuleID
                                    select m).First<Module>();
                ParticipantGroup partGroup;
                var groupExists = (from g in pim.ParticipantGroup
                                   where g.ParticipantGroupName == reg_egp_key
                                   select g).Count();
                if (groupExists == 0)
                {
                    partGroup = new ParticipantGroup();
                    partGroup.ParticipantGroupCode = reg_egp_key;
                    partGroup.ParticipantGroupName = reg_egp_key;
                    partGroup.Module = activeModule; // moduleParticipant;
                    partGroup.LastUpdateDate = DateTime.Now;
                }
                else
                {
                    partGroup = (from g in pim.ParticipantGroup
                                 where g.ParticipantGroupName == reg_egp_key
                                 select g).First<ParticipantGroup>();
                }

                string strCSTKey = "";
                string strLastName = "";
                string strFirstName = "";
                string strEmailAddress = "";
                string LeaderCSTKey = "";
                string strParticipantTitle = "";
                string strAddress1 = "";
                string strAddress2 = "";
                string strCity = "";
                string strPhoneNumber = "";
                string strPostalCode = "";
                int ParticipantID = 0;
                int userExists = 0;
                int participant = 0;
                netForumXML.netForumXML nfXml = new netForumXML.netForumXML();
                nfXml.Url = System.Configuration.ConfigurationSettings.AppSettings["netForumXML.netforumxml"];
                nfXml.Authenticate("nethealth", "n3phrology"); // Replace with actual password.

                string strObject = "EventsRegistrant @TOP 10";
                string strColumns = "reg_egp_key, reg_evt_key, reg_add_date, reg_registration_date, reg_complimentary_event_registered_flag, reg_guest_flag, rdm_custom_flag_01, cst_key, cst_org_name_dn, cst_recno, cst_eml_address_dn, cst_phn_number_complete_dn, evt_key, evt_title, ind_first_name, ind_mid_name, ind_last_name, ind_designation, adr_line1, adr_line2, adr_line3, adr_city, adr_state, adr_post_code, adr_country";
                string strWhere = "reg_egp_key = '" + reg_egp_key  + "' AND evt_key='" + evt_key + "'";
                string strOrderBy = "";

                // Make the call to GetQuery
                XmlNode node = nfXml.GetQuery(
                    strObject,
                    strColumns,
                    strWhere,
                    strOrderBy
                );

                // Lists the ind_first_name for each Individual node.
                foreach (XmlNode xnIndividual in node.ChildNodes)
                {
                    strFirstName = xnIndividual["ind_first_name"].InnerText.ToString();
                    strLastName = xnIndividual["ind_last_name"].InnerText.ToString();
                    strEmailAddress = xnIndividual["cst_eml_address_dn"].InnerText.ToString();
                    strCSTKey = xnIndividual["cst_key"].InnerText.ToString();
                    LeaderCSTKey = xnIndividual["rdm_custom_flag_01"].InnerText.ToString();
                    strParticipantTitle = xnIndividual["ind_designation"].InnerText.ToString();
                    strAddress1 = xnIndividual["adr_line1"].InnerText.ToString();
                    strAddress2 = xnIndividual["adr_line2"].InnerText.ToString();
                    strCity = xnIndividual["adr_city"].InnerText.ToString();
                    strPhoneNumber = xnIndividual["cst_phn_number_complete_dn"].InnerText.ToString();
                    strPostalCode = xnIndividual["adr_post_code"].InnerText.ToString();

                    Participant p;
                    participant = (from part in pim.Participant
                                   where part.ASNGUID == strCSTKey
                                   select part).Count<Participant>();
                    if (participant == 0) //if participant selected does not exists on ASN database then add the user
                    {
                        ParticipantID = SequenceGenerator.GetNextSequenceValue(Constants.SEQUENCE_PARTICIPANTID);

                        p = new Participant();
                        p.ParticipantID = ParticipantID;
                        p.ParticipantLastName = strLastName;
                        p.ParticipantFirstName = strFirstName;
                        p.ParticipantEmailAddress = strEmailAddress;
                        p.ParticipantTitle = strParticipantTitle;
                        p.AddressLine1 = strAddress1;
                        p.AddressLine2 = strAddress2;
                        p.City = strCity;
                        p.PhoneNumber = strPhoneNumber;
                        p.PostalCode = strPostalCode;
                        p.RoleTypeID = Constants.ROLETYPEID_PARTICIPANT;
                        p.Password = Security.EncryptPassword(strEmailAddress, strLastName);
                        p.ASNGUID = strCSTKey;
                        p.DateAdded = DateTime.Now;
                        p.LastUpdateDate = DateTime.Now;
                        p.DateOfBirth = null;

                        pim.SaveChanges();
                        // Save all changes
                        // Add now the Participant Module record. 
                        ParticipantModule pm = new ParticipantModule();
                        pm.Module = activeModule;
                        pm.Participant = p;
                        pm.LastUpdateDate = DateTime.Now;

                        pim.SaveChanges();

                    }
                    else
                    {
                        p = (from part in pim.Participant
                             where part.ASNGUID == strCSTKey
                             select part).First<Participant>();
                        ParticipantID = p.ParticipantID;

                    }

                    // select leader 
                    if (LeaderCSTKey == "1")
                    {
                        partGroup.Participant = p;
                    }


                    userExists = (from g in pim.ParticipantGroupMember
                                  where g.ParticipantGroupID == partGroup.ParticipantGroupID && g.ParticipantID == ParticipantID
                                  select g).Count();
                    if (userExists == 0)
                    {
                        ParticipantGroupMember pgm = new ParticipantGroupMember();
                        pgm.ParticipantGroup = partGroup;
                        pgm.Participant = p;
                        pgm.LastUpdateDate = DateTime.Now;
                    }
                    pim.SaveChanges();

                }

                pim.SaveChanges();

                var groupCount = (from g in pim.ParticipantGroupMember
                                  where g.ParticipantGroupID == partGroup.ParticipantGroupID
                                  select g).Count();
                int chartsRequired = 0;
                switch (groupCount)
                {
                    case 1:
                        chartsRequired = 25;
                        break;
                    case 2:
                        chartsRequired = 13;
                        break;
                    case 3:
                        chartsRequired = 9;
                        break;
                    case 4:
                        chartsRequired = 7;
                        break;
                    default:
                        chartsRequired = 5;
                        break;

                }
                partGroup.MinAbstractsRequired = chartsRequired;
                pim.SaveChanges();

                transaction.Complete();

            }
        }
    }

    protected bool AuthenticateUserViaQS_OLD(string UserCSTKey, bool refreshCookie, bool requiredASNAuthentication)
    {
        string strFirstName = "";
        string strLastName = "";
        string strEmailAddress = "";
        string strDegree = "";
        bool retValue = true;
        int numberOfGroups = 0;

        if (requiredASNAuthentication)
        {
            netForumXML.netForumXML nfXml = new netForumXML.netForumXML();
            nfXml.Url = "http://members.asn-online.org/xweb/secure/netforumxml.asmx";
            nfXml.Authenticate("nethealth", "n3phrology"); // Replace with actual password.

            string strObject = "Individual @TOP 1";
            string strColumns = "ind_first_name,ind_last_name,ind_full_name_cp,ind_deceased_flag,ind_add_date,eml_address,ind_cst_key,ind_designation";
            string strWhere = "ind_cst_key='" + UserCSTKey + "'";
            string strOrderBy = "";

            // Make the call to GetQuery
            XmlNode node = nfXml.GetQuery(
                strObject,
                strColumns,
                strWhere,
                strOrderBy
            );

            // Lists the ind_first_name for each Individual node.
            foreach (XmlNode xnIndividual in node.ChildNodes)
            {
                strFirstName = xnIndividual["ind_first_name"].InnerText.ToString();
                strLastName = xnIndividual["ind_last_name"].InnerText.ToString();
                strEmailAddress = xnIndividual["eml_address"].InnerText.ToString();
                strDegree = xnIndividual["ind_designation"].InnerText.ToString();
            }
        }
        else
            strLastName = "lpatrick";

        if (strLastName != "") // User does exists on ASN Database
        {
            using (NetHealthPIMEntities pim = new NetHealthPIMEntities())
            {
                var activeModule = (from m in pim.Module
                                    where m.ModuleID == Constants.MODULE_COPD
                                    select m).First<Module>();

                var participant = (from p in pim.Participant
                                   where p.ASNGUID == UserCSTKey
                                   select p).Count<Participant>();
                if (participant > 0)
                {
                    // User already exists
                    var participantExists = (from p in pim.Participant
                                             where p.ASNGUID == UserCSTKey
                                       select new { p.ParticipantID, p.ParticipantLastName, p.ParticipantFirstName, p.DegreeType.DegreeTypeName, p.RoleTypeID }).First();
                    if (participantExists != null)
                    {
                        Module module = ModuleContext.getModule();
                        ctx = new UserContext();
                        ctx.ActiveModuleID = module.ModuleID;
                        ctx.ParticipantID = participantExists.ParticipantID;
                        ctx.ParticipantName = participantExists.ParticipantFirstName + " " + participantExists.ParticipantLastName;// +", " + participant.DegreeTypeName;
                        ctx.IsAuthenticated = true;
                        ctx.RoleTypeID = Convert.ToInt32(participantExists.RoleTypeID);;
                        var groupsParticipant = from g in pim.ParticipantGroup
                                                join gm in pim.ParticipantGroupMember
                                                on g.ParticipantGroupID equals gm.ParticipantGroupID
                                                where gm.ParticipantID == ctx.ParticipantID
                                                select new { g.ParticipantGroupID, g.ParticipantGroupName, g.Participant.ParticipantID, g.MinAbstractsRequired };
                        foreach (var v in groupsParticipant)
                        {
                            numberOfGroups = numberOfGroups + 1;
                            ctx.LeaderParticipantID = v.ParticipantID;
                            ctx.ParticipantGroupID = v.ParticipantGroupID;
                            ctx.MinAbstractsRequired = (int)v.MinAbstractsRequired;
                        }
                       
                        Session[Constants.SESSION_USERCONTEXT] = ctx;
                        LabelParticipantName.Text = ctx.ParticipantName;
                        // If the request cookie is there, update the expiration date.
                        if (Request.Browser.Cookies && refreshCookie)
                        {
                            // Password is stored in encrypted form in the cookie
                            Response.Cookies["PBLOGIN"]["UNAME"] = Server.UrlEncode(strEmailAddress);
                            Response.Cookies["PBLOGIN"].Expires = DateTime.Now.AddDays(30);
                        }
                    }
                    else
                    {
                        if (Request.Browser.Cookies)
                        {
                            Response.Cookies["PBLOGIN"].Expires = DateTime.Now.AddDays(-1);
                        }
                        retValue = false;
                    }

                }
                else
                {
                    int ParticipantID = SequenceGenerator.GetNextSequenceValue(Constants.SEQUENCE_PARTICIPANTID);

                    Participant p = new Participant();
                    p.ParticipantID = ParticipantID;
                    p.ParticipantLastName = strLastName;
                    p.ParticipantFirstName = strFirstName;
                    p.ParticipantEmailAddress = strEmailAddress;
                    p.RoleTypeID = Constants.ROLETYPEID_PARTICIPANT;
                    p.Password = Security.EncryptPassword(strEmailAddress, strLastName);
                    p.ASNGUID = UserCSTKey;
                    p.DateAdded = DateTime.Now;
                    p.LastUpdateDate = DateTime.Now;
                    p.DateOfBirth = null;
                 
                    // Save all changes
                    // Add now the Participant Module record. 
                    ParticipantModule pm = new ParticipantModule();
                    pm.Module = activeModule;
                    pm.Participant = p;
                    pm.LastUpdateDate = DateTime.Now;

                    pim.SaveChanges();

                    Module module = ModuleContext.getModule();
                    ctx = new UserContext();
                    ctx.ActiveModuleID = module.ModuleID;
                    ctx.ParticipantID = p.ParticipantID;
                    ctx.ParticipantName = p.ParticipantFirstName + " " + p.ParticipantLastName;// +", " + participant.DegreeTypeName;
                    ctx.IsAuthenticated = true;
                    ctx.RoleTypeID = Convert.ToInt32(p.RoleTypeID);
                    ctx.ParticipantGroupID = 0;
                    ctx.LeaderParticipantID = 0;
                    Session[Constants.SESSION_USERCONTEXT] = ctx;
                    LabelParticipantName.Text = ctx.ParticipantName;

                }
            }
            if (numberOfGroups > 1)
            {
                RedirectToSelectGroup();
            }
            return retValue;
        }
        else
        {
            return false;
        }
        
    }

    public void DisplayWelcomeMessage()
    {
        if (LabelParticipantName != null)
        {
            LabelParticipantName.Text = ctx.ParticipantName;
           
        }
    }

    protected void HideWelcomeMessage()
    {
        
    }

    protected void RedirectToModuleHome() {
        module = ModuleContext.getModule();
        int pageRoleLevel = Constants.ROLETYPEID_PARTICIPANT;
        if (Session[Constants.SESSION_PAGE_ROLETYPE_LEVEL] != null)
            pageRoleLevel = (int)Session[Constants.SESSION_PAGE_ROLETYPE_LEVEL];

        if (pageRoleLevel > Constants.ROLETYPEID_GUEST) {
            String url = Request.AppRelativeCurrentExecutionFilePath;
            if (!url.EndsWith("/Default.aspx") && !url.EndsWith("/SessionExpired.aspx"))
                Response.Redirect("~/" + module.ModuleCode.ToLower() + "/Default.aspx");
        }
    }

    protected void RedirectToSelectGroup() {
        module = ModuleContext.getModule();
        int pageRoleLevel = Constants.ROLETYPEID_PARTICIPANT;
        if (Session[Constants.SESSION_PAGE_ROLETYPE_LEVEL] != null)
            pageRoleLevel = (int)Session[Constants.SESSION_PAGE_ROLETYPE_LEVEL];

        if (pageRoleLevel > Constants.ROLETYPEID_GUEST) {
            String url = Request.AppRelativeCurrentExecutionFilePath;
            if (!url.EndsWith("/SessionExpired.aspx"))
                Response.Redirect("~/" + module.ModuleCode.ToLower() + "/ParticipantGroup.aspx");
        }
    }

    protected void RedirectToLogOutPage()
    {
        module = ModuleContext.getModule();
        int pageRoleLevel = Constants.ROLETYPEID_PARTICIPANT;
        if (Session[Constants.SESSION_PAGE_ROLETYPE_LEVEL] != null)
        {
            pageRoleLevel = (int)Session[Constants.SESSION_PAGE_ROLETYPE_LEVEL];
        }
        if (pageRoleLevel > Constants.ROLETYPEID_GUEST)
        {
            String url = Request.AppRelativeCurrentExecutionFilePath;
            if (!url.EndsWith("/Default.aspx") && !url.EndsWith("/SessionExpired.aspx"))
            {
                Response.Redirect("~/Logoff.aspx");
            }
        }
    }


    /// <summary>
    /// Called before we render the main menu. The code checks and highlights the selected item.
    /// </summary>
    /// <param name="sender"></param>
    /// <param name="e"></param>
    protected void menuMain_PreRender(object sender, EventArgs e)
    {
        String menuCode = (String)Session[Constants.SESSION_MENUCODE];
        Menu m = (Menu)sender;
        foreach (var x in m.Items)
        {
            MenuItem mi = (MenuItem)x;
            if (mi.DataPath == menuCode)
            {
                mi.Selected = true;
            }
        }

    }

    public void SetTitle(string pageTitle)
    {
        LiteralPageTitle.Text = pageTitle;
    }

    public void SetHeader(string pageHeader)
    {
        LiteralHeader.Text = pageHeader;
    }

    public void SetStatus(int PhaseID)
    {
        if (ctx == null)
        {
            Response.Redirect("../Logoff.aspx");
        }
        divPhaseI.Attributes.Remove("class");
        divPhaseII.Attributes.Remove("class");
        divPhaseIII.Attributes.Remove("class");
        divPhaseIV.Attributes.Remove("class");
        divPhaseI.Attributes.Add("class", "phase one");
        divPhaseII.Attributes.Add("class", "phase two");
        divPhaseIII.Attributes.Add("class", "phase three");
        divPhaseIV.Attributes.Add("class", "phase four");
        switch (PhaseID)
        {
            case 1:
                divPhaseI.Attributes.Remove("class");
                divPhaseI.Attributes.Add("class", "phase one active");
                
                break;
            case 2:
                divPhaseII.Attributes.Remove("class");
                divPhaseII.Attributes.Add("class", "phase two active");
                
                break;
            case 3:
                divPhaseIII.Attributes.Remove("class");
                divPhaseIII.Attributes.Add("class", "phase three active");
              
                break;
            case 4:
                divPhaseIV.Attributes.Remove("class");
                divPhaseIV.Attributes.Add("class", "phase four active");
             
                break;
        }
        using (NetHealthPIMEntities pim = new NetHealthPIMEntities())
        {
            ParticipantModuleCycle cycle = pim.ParticipantModuleCycle.First(c => c.ParticipantModuleCycleID == ctx.ActiveModuleCycleID);
            if (cycle.CycleNumber == 1)
            {
                divPhaseIII.Attributes.Remove("onclick");
                divPhaseIII.Attributes.Add("onclick", "javascript:alert('Analysis & Impact Phase is not yet available.');");
            }
            Participant participant = pim.Participant.First(p => p.ParticipantID == ctx.ParticipantID);
            if ((participant.AcknowledgmentDate == null) && (ctx.SoftwareVersion > 0) && (cycle.CycleNumber == 1))
            {
                   // divPhaseI.Attributes.Remove("onclick");
                  //  divPhaseI.Attributes.Add("onclick", "javascript:alert('Create Plan Phase is not yet available.');");
                 //divPhaseII.Attributes.Remove("onclick");
                //  divPhaseII.Attributes.Add("onclick", "javascript:alert('Assess & Improve Phase is not yet available.');");
                divPhaseIV.Attributes.Remove("onclick");
                divPhaseIV.Attributes.Add("onclick", "javascript:alert('Prior Data is not yet available.');");

                HyperLinkSupport.Visible = false;
                HyperLinkContactUs.Visible = false;
                HyperLinkProfile.Visible = false;
                hrefConflictofInterestPolicy.Visible = false;
            }
            else
            {
                aboMainSite.Attributes.Remove("class");
                aboMainSite.Attributes.Add("class", "none");
            }
            if (ctx.SoftwareVersion == 0)
                hrefConflictofInterestPolicy.Visible = false;
        }
    }
  
    protected void LinkButtonLogout_Click(object sender, EventArgs e)
    {
        if (Request.Browser.Cookies)
        {
            if (Response.Cookies["PBLOGIN"] != null)
            {
                Response.Cookies["PBLOGIN"].Expires = DateTime.Now.AddDays(-1);
            }
        }
        HideWelcomeMessage();

        Session[Constants.SESSION_USERCONTEXT] = null;
        RedirectToLogOutPage();
      
    }
}
