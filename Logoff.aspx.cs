﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

public partial class Logoff : BasePage
{
    protected void Page_Load(object sender, EventArgs e)
    {

        if (!IsPostBack)
        {
            Session[Constants.SESSION_PAGE_ROLETYPE_LEVEL] = Constants.ROLETYPEID_GUEST;
            UserContext ctx = (UserContext)Session[Constants.SESSION_USERCONTEXT];
            this.Master.FindControl("LabelParticipantName").Visible = false;
            this.Master.FindControl("LinkButtonLogOut").Visible = false;
            this.Master.FindControl("hrefConflictofInterestPolicy").Visible = false;
            this.Master.FindControl("HyperLinkProfile").Visible = false;
            this.Master.FindControl("divPhaseI").Visible = false;
            this.Master.FindControl("divPhaseII").Visible = false;
            this.Master.FindControl("divPhaseIII").Visible = false;
            this.Master.FindControl("divPhaseIV").Visible = false;
        }

        Session[Constants.SESSION_PAGE_ROLETYPE_LEVEL] = Constants.ROLETYPEID_GUEST;
        Response.Cookies["PBLOGIN"]["UNAME"] = null;
        Response.Cookies["PBLOGIN"].Value = null;
        Response.Cookies["PBLOGIN"].Expires = DateTime.Now.AddDays(-1);

        
    }
}
