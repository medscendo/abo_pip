﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

public partial class abo_ContactUs : System.Web.UI.Page
{
    protected void Page_Load(object sender, EventArgs e)
    {
        if (!IsPostBack)
        {
            ButtonSubmit.PostBackUrl = Request.UrlReferrer.ToString();
        }
    }
    protected void ButtonSubmit_Click(object sender, EventArgs e)
    {
        //DataAccess da = new DataAccess();
        int ContactPerson = Convert.ToInt32(rdoYes.Checked);
        int BestWayToContact = 0;
        string ContactBy = "";
        if (rdoEmail.Checked)
        {
            BestWayToContact = 1;
            ContactBy = "Email";

        }
        if (rdoPhone.Checked)
        {
            BestWayToContact = 2;
            ContactBy = "Phone";
        }
        //var result = da.ContactInformation(EmailAddress.Text.ToString(), PhoneNumber.Text.ToString(), Comments.Text.ToString(), ContactPerson, BestWayToContact);
        //da.Dispose();

        string EmailTo = "Part4Support@abop.org";
        string EmailSubject = "Part4Support@abop.org.";
        string EmailBody = "Feedback Message:" + Comments.Text.ToString() + "\n";
        if (ContactPerson == 1)
        {
            EmailBody = EmailBody + "Please contact this person.";
            if ((rdoEmail.Checked) || (rdoPhone.Checked))
                EmailBody = EmailBody + "\n The best way to reach this person is by " + ContactBy;
            EmailBody = EmailBody + "\n Email Address: " + EmailAddress.Text.ToString();
            EmailBody = EmailBody + "\n Phone Number: " + PhoneNumber.Text.ToString();
        }
        SMTPEmail mail = new SMTPEmail();
        mail.SendEmail(EmailTo, EmailSubject, EmailBody, "", "");
        lblMessage.Visible = true;

        //Response.Redirect(HFURL.Value);
    }

}
