﻿<%@ Page Title="" Language="C#" MasterPageFile="~/PIMMasterPage.master" AutoEventWireup="true" CodeFile="ParticipantFeedbackSent.aspx.cs" Inherits="ParticipantFeedbackSent" EnableTheming="true" %>

<asp:Content ID="Content1" ContentPlaceHolderID="head" Runat="Server">
</asp:Content>
<asp:Content ID="Content3" ContentPlaceHolderID="ContentPlaceHolder1" Runat="Server">
    <br />
    <h3>Feedback Sent</h3>
    Thank you for your comments and questions.  If you requested a response, we will follow up with you within 1 business day.
    <center>
    <br />
    <asp:Button ID="ButtonOK" runat="server" Text="Button" onclick="ButtonOK_Click" />
    </center>
    <br />
</asp:Content>

