﻿<%@ Page Title="" Language="C#" MasterPageFile="~/PIMMasterPage.master" AutoEventWireup="true" CodeFile="Support.aspx.cs" Inherits="abo_Support" %>

<asp:Content ID="Content1" ContentPlaceHolderID="head" Runat="Server">
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder1" Runat="Server">
         <!-- activity detaile -->
            <div class="main_content" style="padding-right: 100px;">
            <div class="heading_box" >
               
              <h2 ><strong>Support</strong></h2>
              <h3> </h3>
            </div>
            <div class="imporvenment_plan_details">
              <div class="top_details" style="padding-left:50px; padding-right: 50px;padding-top: 30px; line-height: 1.2;">
                <p>If you need assistance with this Improvement in Medical Practice Platform, the following resources are available:</p>
               <p>&nbsp;</p>
                <p><b>Tutorials:</b></p>
                <p>&nbsp;</p>
                <p>There is a tutorial available which you can view at any time:</p>
                <p>&nbsp;</p>
                <div style="padding-left:30px;">
                <p><a href="http://www.nethealthinteractive.com/ABO_Tutorials/ABO_PIP_Tutorial/player.html" target="_blank">Improvement in Medical Practice Platform</a></p>
                
                </div>
                  <p>&nbsp;</p>
                  <p>&nbsp;</p>
                <p><b>Personalized Support:</b></p>
                <p>&nbsp;</p>
                <p>You can <a href="ContactUs.aspx">contact us</a> for support.
                <p>&nbsp;</p>
                <p>We will respond to all inquiries within one business day.</p>
              </div>
              <div class="bginput" ><asp:LinkButton ID="ButtonSubmit" runat="server" Text=" Back " PostBackUrl="Dashboard.aspx"/></div>
              </div>
            </div>
          <!-- activity detaile ends --> 
</asp:Content>

