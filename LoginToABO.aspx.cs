﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

public partial class LoginToABO : BasePage
{
    protected void Page_Load(object sender, EventArgs e)
    {
        if (!IsPostBack)
        {
            Session[Constants.SESSION_PAGE_ROLETYPE_LEVEL] = Constants.ROLETYPEID_GUEST;
            UserContext ctx = (UserContext)Session[Constants.SESSION_USERCONTEXT];
            //((PIMMasterPage)Page.Master).SetTitle("Login to ASN");
            this.Master.FindControl("LabelParticipantName").Visible = false;
            this.Master.FindControl("LinkButtonLogOut").Visible = false;
            this.Master.FindControl("hrefConflictofInterestPolicy").Visible = false;
            this.Master.FindControl("HyperLinkProfile").Visible = false;
            this.Master.FindControl("divPhaseI").Visible = false;
             this.Master.FindControl("divPhaseII").Visible = false;
             this.Master.FindControl("divPhaseIII").Visible = false;
             this.Master.FindControl("divPhaseIV").Visible = false;
        }

    }

    protected void ButtonSubmit_Click(object sender, EventArgs e)
    {
        if (!((PIMMasterPage)Page.Master).AuthenticateUser(UserID.Text.ToString(), Password.Text.ToString(), false))
            lblUserNOTExists.Visible = true;
    }
}
