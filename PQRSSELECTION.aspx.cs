﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using NetHealthPIMModel;
using System.Transactions;
public partial class abo_PQRSSELECTION  : BasePage
{
    protected void Page_Load(object sender, EventArgs e)
    {
        using (NetHealthPIMEntities db = new NetHealthPIMEntities())
        {
            int participantid = ctx.ParticipantID;
            var mod = (from c in db.ParticipantModule where c.ParticipantID == participantid select c.PQRS).FirstOrDefault();
            RadioButtonList1.SelectedValue = mod.ToString();
        }

    }
    protected void Button1_Click(object sender, EventArgs e)
    {
        using(NetHealthPIMEntities db=new NetHealthPIMEntities())
        {
        int participantid = ctx.ParticipantID;
        bool selection = Convert.ToBoolean(RadioButtonList1.SelectedItem.Value);
        ParticipantModule mod = (from c in db.ParticipantModule where c.ParticipantID == participantid select c).FirstOrDefault();
        mod.PQRS = selection;
        db.SaveChanges();
        Response.Redirect("ModuleSelection.aspx");

        }
    }
}
