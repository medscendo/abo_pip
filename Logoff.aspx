﻿<%@ Page Language="C#" AutoEventWireup="true" MasterPageFile="~/PIMMasterPage.master" CodeFile="Logoff.aspx.cs" Inherits="Logoff" %>


<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder1" Runat="Server">
    <style>
        .phase-header, .phase-shadow {
            display:none;
        }

        table {
            float:left;
            margin:30px 300px;
            width:300px;
        }

        #page-info {
            padding-top: 30px;
        }

    </style>
            <!-- main content -->
            <table border="0" cellspacing="0" class="edit_table center_area ">
                <tr>
                    <td style="padding: 30px;">You have been logged off of the ABO Improvement in Medical Practice Activity. 
                    <br /><br />To log on again, go to <a href="http://www.abop.org/">Login to ABO</a></td>
                </tr>
            </table>
 </asp:Content>

