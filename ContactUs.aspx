﻿<%@ Page Title="" Language="C#" MasterPageFile="~/PIMMasterPage.master" AutoEventWireup="true" CodeFile="ContactUs.aspx.cs" Inherits="abo_ContactUs" %>

<asp:Content ID="Content1" ContentPlaceHolderID="head" Runat="Server">
<script language="javascript" type="text/javascript">

    function validateForm() {
        var re_email = /^([\w-]+(?:\.[\w-]+)*)@((?:[\w-]+\.)*\w[\w-]{0,66})\.([a-z]{2,6}(?:\.[a-z]{2})?)$/i
        var email = document.getElementById("<%= EmailAddress.ClientID %>").value;
        var comments = document.getElementById("<%= Comments.ClientID %>").value;
    
        var errors = [];
        if (comments == 0) {
            errors[errors.length] = "Please provide feedback.";
        }
        if (!re_email.test(email)&&email!=0) {
            errors[errors.length] = "Please enter a valid email address.";
        }
        if (errors.length > 0) {

            reportErrors(errors);
            return false;
        }
        return true;
    }

    function reportErrors(errors) {
        var msg = "Please Enter Valid Data...\n";
        for (var i = 0; i < errors.length; i++) {
            var numError = i + 1;
            msg += "\n" + numError + ". " + errors[i];
        }
        alert(msg);
    }

</script>
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder1" Runat="Server">
         <!-- module detaile -->
            <div class="main_content" style="padding-right: 100px;">
            <div class="heading_box" >
              <h2 ><strong>Contact Us</strong></h2>
            </div>
            <div class="imporvenment_plan_details">
              <div class="top_details" style="padding-left:50px; padding-right: 50px;padding-top: 30px; line-height: 1.2; ">
                <p>
                <br /><br />For non-technical questions related to content, MOC Part IV or CME, please contact: 
                The American Board of Ophthalmology at 610-664-1175 during the hours of 8:30 – 4:30 EST or email at 
                <a href="mailto:part4support@abop.org?subject=ABO Improvement in Medical Practice Activity Support">part4support@abop.org</a>.

                <br /><br />For website and technical questions, please contact:
                <br />NetHealth at 610.590.2229 during the hours of 9 - 5 EST or email at 
                <br /><a href="mailto:ABOPIMSupport@nethealthinc.com?subject=ABO Improvement in Medical Practice Activity Support">ABOPIMSupport@nethealthinc.com</a>
                <br />
                <br />Or leave feedback and we will respond within one business day:</p><br />
                <table class="table-form-no-lines"  border="0" cellpadding="3" cellspacing="0">
                            <tbody>
                            <tr>
                                <td><asp:TextBox ID="Comments" runat="server" TextMode="MultiLine" Rows="5" Columns="100"></asp:TextBox></td>
                            </tr>
                            <tr>
                                <td>Would you like us to contact you regarding your feedback?</td>
                            </tr>
                            <tr>
                                <td><asp:RadioButton ID="rdoYes" runat="server" GroupName="Feedback" Text="Yes"/></td>
                            </tr>
                            <tr>
                                <td><asp:RadioButton ID="rdoNo" runat="server" GroupName="Feedback" Text="No"/></td>
                            </tr>
                            <tr>
                                <td>Your email address:<br /><asp:TextBox ID="EmailAddress" runat="server" Width="300px"></asp:TextBox></td>
                            </tr>
                            <tr>
                                <td>Your phone number:<br /><asp:TextBox ID="PhoneNumber" runat="server" Width="300px"></asp:TextBox></td>
                            </tr>
                            <tr>
                                <td>Best way to reach you:</td>
                            </tr>
                            <tr>
                                <td><asp:RadioButton ID="rdoEmail" runat="server" GroupName="Contact" Text="Email"/></td>
                            </tr>
                            <tr>
                                <td><asp:RadioButton ID="rdoPhone" runat="server" GroupName="Contact" Text="Phone"/></td>
                            </tr>
                            </tbody>
               </table>
						<br />	
						<asp:Label ID="lblMessage" runat="server" Visible="false" ForeColor="Blue" Text="Your feedback has been sent.  Thank You for you feedback." style="font-size:14px;"></asp:Label>
              </div>
              <div class="bginput" ><asp:LinkButton ID="ButtonSubmit" runat="server" Text=" Back " PostBackUrl="Dashboard.aspx"/></div>
              <div class="bginput" ><asp:LinkButton ID="LinkButtonSendEmail" runat="server" Text=" Submit "  OnClick="ButtonSubmit_Click"/></div>
              </div>
            </div>
          <!-- module detaile ends --> 
</asp:Content>

